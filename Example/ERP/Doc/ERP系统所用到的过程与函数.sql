
CREATE TRIGGER  Approved_Message_Update    ON [dbo].[Approved_Message]  
FOR  UPDATE    
AS 
  declare @EndExecSQL varchar(2000)
  declare @Me_NoAgreeWhy varchar(2000)
  declare @Bt_NoAgreeExecSQL varchar(2000)
  declare @OperatorGuid varchar(100)
  declare @tempPubid varchar(100)
  declare @temp_Ru_Guid varchar(100) 
  declare @temp_Bt_Guid varchar(100)
  declare @temp_Step_Guid varchar(100)
  declare @tempState varchar(100) 
  declare @RequestTitle varchar(100) 
  declare @tempNeedPassCount int  
  declare @tempRealPassCount int  
  declare @tempStepOrder int  
  declare @tempValue3 int  

  set @tempValue3=0
  set @tempStepOrder=0
  set @tempNeedPassCount=0
  set @tempRealPassCount=0
  set @EndExecSQL=''
  set @Bt_NoAgreeExecSQL=''
  set @Me_NoAgreeWhy=''
  set @temp_Bt_Guid=''
  set @tempPubid=''
  set @temp_Step_Guid=''
  set @temp_Ru_Guid=''
  set @tempState=''
  set @OperatorGuid=''
  set @RequestTitle=''


  select @Me_NoAgreeWhy=Me_NoAgreeWhy from inserted 
  select @RequestTitle=Me_RequestTitle from inserted 
  select @temp_Bt_Guid=Me_Bt_Guid from inserted 
  select @EndExecSQL= Bt_EndExecSQL from Approved_BillType where  Bt_Guid=@temp_Bt_Guid
  select @Bt_NoAgreeExecSQL= Bt_NoAgreeExecSQL from Approved_BillType where  Bt_Guid=@temp_Bt_Guid
  select @temp_Step_Guid=Me_St_Guid from inserted 
  select @temp_Ru_Guid=St_Ru_Guid from  Approved_Step where St_Guid=@temp_Step_Guid 
  select @tempStepOrder=St_Order from  Approved_Step where St_Guid=@temp_Step_Guid 
  select @OperatorGuid=Me_RequestOperatorGuid from inserted 
  select @tempState= Me_State from inserted 
  select @tempPubid= Me_PubidValue from inserted 

  if (UPDATE(Me_State)) 
  begin
	  if (@tempState in ('同意')) 
	  begin  
	    --必须审批的人员是否都已审批同意
	    if not exists(
					     select *
					     from  Approved_StepOperator 
					     where So_St_Guid=@temp_Step_Guid and So_IsNeed=1 and So_IsActive=1 and 
					           So_OperatorGuid not in (select Me_ApprovedOperatorGuid 
					                                         from  Approved_Message  
					                                         where Me_State='同意' and  Me_St_Guid=@temp_Step_Guid and Me_PubidValue=@tempPubid
					                                         ) 
	                 )
	    begin
	      --是否达到审批阶段的最少人数
	      select @tempNeedPassCount=St_NeedPassCount from  Approved_Step where St_Guid=@temp_Step_Guid and St_IsActive=1
	      select @tempRealPassCount=count(Me_ApprovedOperatorGuid) from  Approved_Message where Me_State='同意' and  Me_PubidValue=@tempPubid
	      if @tempRealPassCount>=@tempNeedPassCount  
	      begin
	        set @temp_Step_Guid=''
			    set @temp_Step_Guid= isnull((select top 1 b.St_Guid  from Approved_Step b   
			                          where b.St_IsActive=1 and b.St_Ru_Guid=@temp_Ru_Guid and St_order>@tempStepOrder 
		                            order by b.St_order asc),'')  
		
		      if @temp_Step_Guid<>'' and  @temp_Bt_Guid<>'' and  @tempPubid<>'' 
	        begin   
		        exec Proc_BuildApprovedStepMessage @temp_Step_Guid,@temp_Bt_Guid,@OperatorGuid,@tempPubid,@RequestTitle 
            delete Approved_Message where Me_PubidValue = @tempPubid and Me_St_Guid =(select top 1 Me_St_Guid from inserted ) and  Me_State='未审批'
	        end
	        if isnull(@temp_Step_Guid,'') ='' and isnull(@EndExecSQL,'')<>''
	        begin 
		        set @tempValue3=CHARINDEX('@tempMainPubid', @EndExecSQL)         
		        if @tempValue3>0           
		 	        set @EndExecSQL= substring(@EndExecSQL,1,@tempValue3-1)+@tempPubid+substring(@EndExecSQL,@tempValue3+len('@tempMainPubid'),len(@EndExecSQL)) 
	         
		        exec(@EndExecSQL)
	        end   
		    end 
		  end
	  end
	  else
	  begin
	    set @tempValue3=CHARINDEX('@tempMainPubid', @Bt_NoAgreeExecSQL)         
	    if @tempValue3>0           
	        set @Bt_NoAgreeExecSQL= substring(@Bt_NoAgreeExecSQL,1,@tempValue3-1)+@tempPubid+substring(@Bt_NoAgreeExecSQL,@tempValue3+len('@tempMainPubid'),len(@Bt_NoAgreeExecSQL)) 
	
	    set @tempValue3=CHARINDEX('NoAgreeMessage', @Bt_NoAgreeExecSQL)         
	    if @tempValue3>0           
	        set @Bt_NoAgreeExecSQL= substring(@Bt_NoAgreeExecSQL,1,@tempValue3-1)+@Me_NoAgreeWhy+substring(@Bt_NoAgreeExecSQL,@tempValue3+len('NoAgreeMessage'),len(@Bt_NoAgreeExecSQL)) 
	   
	    exec(@Bt_NoAgreeExecSQL)
	  end
  end




--统一增加与删除业务单据的是否可删除过程
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_IsCanDellBusinessBill]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Proc_IsCanDellBusinessBill]
go
CREATE PROCEDURE [dbo].[Proc_IsCanDellBusinessBill]
(
@BillType varchar(100),
@Guid varchar(100),
@IsCan int output
)  
AS
set nocount on 
set @IsCan=1

if @BillType='btBuy_Order' 
begin
	if exists(select * from Buy_GetProductItem      where Gi_ComeFromItemCode=@Guid)  
	  set @IsCan=0
end
else if @BillType='btBuy_GetProduct' 
begin
	if exists(select * from Buy_ReturnProductItem      where Ri_ComeFromItemCode=@Guid) 
	  set @IsCan=0
end
else if @BillType='btSale_Order' 
begin
	if exists(select * from Sale_OutStorageItem      where Oi_ComeFromItemCode=@Guid) 
	  set @IsCan=0
end
else if @BillType='btSale_OutStorage' 
begin
	if exists(select * from Sale_ReturnProductItem      where Ri_ComeFromItemCode=@Guid)  
	  set @IsCan=0
end
 
GO
--exec Proc_IsCanDellBusinessBill  
 
go
 

--统一增加与删除产品单位是否可删除过程
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_IsCanDellProductUnit]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Proc_IsCanDellProductUnit]
go
CREATE PROCEDURE [dbo].[Proc_IsCanDellProductUnit]
(
@Pu_Guid varchar(100),
@ResStr varchar(1000) output
)  
AS
set nocount on 
set @ResStr=''

if exists(select * from ERP_Base_MaterielVersionBom where Pb_Pu_Guid=@Pu_Guid) Or
   exists(select * from Buy_GetProductItem where Gi_Pu_Guid=@Pu_Guid) Or
   exists(select * from Buy_OrderItem where Oi_Pu_Guid=@Pu_Guid) Or
   exists(select * from Buy_PlanItem where Pi_Pu_Guid=@Pu_Guid) Or
   exists(select * from Buy_ReturnProductItem where Ri_Pu_Guid=@Pu_Guid) Or
   exists(select * from Produce_OrderBom where Ob_Pu_Guid=@Pu_Guid) Or
   exists(select * from Produce_OrderItem where Oi_Pu_Guid=@Pu_Guid) Or
   exists(select * from Produce_PlanItem where Pi_Pu_Guid=@Pu_Guid) Or
   exists(select * from Sale_OrderItem where Oi_Pu_Guid=@Pu_Guid) Or
   exists(select * from Sale_OutStorageItem where Oi_Pu_Guid=@Pu_Guid) Or
   exists(select * from Sale_PlanItem where Pi_Pu_Guid=@Pu_Guid) Or
   exists(select * from Sale_QuoteItem where Qi_Pu_Guid=@Pu_Guid) Or
   exists(select * from Sale_ReturnProductItem where Ri_Pu_Guid=@Pu_Guid) Or
   exists(select * from Service_OrderItem where Oi_Pu_Guid=@Pu_Guid) Or
   exists(select * from Storage_MoveStorageItem where Mi_Pu_Guid=@Pu_Guid) Or
   exists(select * from Sale_CustSaleItem where Ci_Pu_Guid=@Pu_Guid) Or
   exists(select * from Storage_OtherInStorageItem where Oi_Pu_Guid=@Pu_Guid) Or
   exists(select * from Storage_OtherOutStorageItem where Oi_Pu_Guid=@Pu_Guid)

   select  @ResStr=isnull(@ResStr,'')+isnull(aa,'') from
(
   select '生产版本'+isnull(Pv_Name,'')+char(10)+char(13)  as aa from ERP_Base_MaterielVersion left join ERP_Base_MaterielVersionBom on pv_Guid=Pb_pv_Guid where Pb_Pu_Guid=@Pu_Guid Union
   select '采购收货'+isnull(Gp_Code,'')+char(10)+char(13)  as aa from Buy_GetProduct left join Buy_GetProductItem  on Gp_Guid=Gi_Gp_Guid where Gi_Pu_Guid=@Pu_Guid Union
   select '采购订单'+isnull(Or_Code,'')+char(10)+char(13)  as aa from Buy_Order left join Buy_OrderItem  on Or_Guid=Oi_Or_Guid  where Oi_Pu_Guid=@Pu_Guid Union
   select '采购计划'+isnull(Pl_Code,'')+char(10)+char(13)  as aa from Buy_Plan left join  Buy_PlanItem  on Pl_Guid=Pi_Pl_Guid where Pi_Pu_Guid=@Pu_Guid Union
   select '采购退货'+isnull(Rp_Code,'')+char(10)+char(13)  as aa from Buy_ReturnProduct left join  Buy_ReturnProductItem  on Rp_Guid=Ri_Rp_Guid where Ri_Pu_Guid=@Pu_Guid Union
   select '生产BOM'+isnull(Or_Code,'') +char(10)+char(13)  as aa from Produce_Order left join  Produce_OrderBom  on Or_Guid=Ob_Or_Guid where Ob_Pu_Guid=@Pu_Guid Union
   select '生产任务'+isnull(Or_Code,'')+char(10)+char(13)  as aa from Produce_Order left join  Produce_OrderItem  on Or_Guid=Oi_Or_Guid where Oi_Pu_Guid=@Pu_Guid Union
   select '生产计划'+isnull(Pl_Code,'')+char(10)+char(13)  as aa from Produce_Plan left join  Produce_PlanItem on Pl_Guid=Pi_Pl_Guid where Pi_Pu_Guid=@Pu_Guid Union
   select '销售订单'+isnull(Or_Code,'')+char(10)+char(13)  as aa from Sale_Order left join  Sale_OrderItem  on Or_Guid=Oi_Or_Guid where Oi_Pu_Guid=@Pu_Guid Union
   select '销售出库'+isnull(Os_Code,'')+char(10)+char(13)  as aa from Sale_OutStorage left join  Sale_OutStorageItem  on Os_Guid=Oi_Os_Guid where Oi_Pu_Guid=@Pu_Guid Union
   select '销售计划'+isnull(Pl_Code,'')+char(10)+char(13)  as aa from Sale_Plan left join  Sale_PlanItem on Pl_Guid=Pi_Pl_Guid where Pi_Pu_Guid=@Pu_Guid Union
   select '销售报价'+isnull(Qu_Code,'')+char(10)+char(13)  as aa from Sale_Quote left join  Sale_QuoteItem  on Qu_Guid=Qi_Qu_Guid where Qi_Pu_Guid=@Pu_Guid Union
   select '销售退货'+isnull(Rp_Code,'')+char(10)+char(13)  as aa from Sale_ReturnProduct left join  Sale_ReturnProductItem on Rp_Guid=Ri_Rp_Guid where Ri_Pu_Guid=@Pu_Guid Union
   select '服务单'+isnull(Or_Code,'') +char(10)+char(13)   as aa from Service_Order left join  Service_OrderItem on Or_Guid=Oi_Or_Guid where Oi_Pu_Guid=@Pu_Guid Union
   select '转库单'+isnull(Ms_Code,'') +char(10)+char(13)   as aa from Storage_MoveStorage left join  Storage_MoveStorageItem on Ms_Guid=Mi_Ms_Guid where Mi_Pu_Guid=@Pu_Guid Union
   select '其它入库'+isnull(Oi_Code,'')+char(10)+char(13)  as aa from Storage_OtherInStorage left join  Storage_OtherInStorageItem on Storage_OtherInStorage.Oi_Guid=Oi_Oi_Guid where Oi_Pu_Guid=@Pu_Guid Union
   select '客户销售'+isnull(Cs_Code,'')+char(10)+char(13)  as aa from Sale_CustSale left join Sale_CustSaleItem on Cs_Guid=Ci_Cs_Guid where Ci_Pu_Guid=@Pu_Guid Union
   select '其它出库'+isnull(Oo_Code,'')+char(10)+char(13)  as aa from Storage_OtherOutStorage left join  Storage_OtherOutStorageItem on Oo_Guid=Oi_Oo_Guid where Oi_Pu_Guid=@Pu_Guid

   ) aa

GO
--exec Proc_IsCanDellProductUnit  '{C7DB9B0F-D323-4816-BBA2-15AE94C53C32}',1
 
--以采购平均价更新产品的成本价
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_UpdateProductCost]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Proc_UpdateProductCost]
go
CREATE PROCEDURE [dbo].[Proc_UpdateProductCost] 
(
@Guid varchar(100),
@AddVale varchar(100)='0',
@Type int =0

)
as  

update tt set Pr_CostPrice=
case 
when charindex('%',@AddVale)>0 then 
  b.采购平均单价*(1+cast(substring(@AddVale,1,len(@AddVale)-1) as decimal(18,4) )/100)
else
  b.采购平均单价+@AddVale

end
	 
from ERP_Base_Materiel tt , 
     (
     select Oi_Pr_Guid,  

           case
           when  sum(isnull(采购数量,0))=0 then 0
           else round(sum(isnull(产品金额,0))/sum(isnull(采购数量,0)) ,4) 
           end as  采购平均单价 

      from                                                                                       
     (
     select Oi_Pr_Guid,                                                                                       
       isnull(f.Oi_Quantity ,0)*(isnull(h.Pu_BaseUnitNum,0)) as 采购数量 ,                         
       isnull(f.Oi_Quantity,0)*isnull(f.Oi_Price,0)*(1-isnull(f.Oi_Discount,0)) as 产品金额    
     from Buy_Order a                                                                             
          left join Buy_OrderItem        f on a.Or_Guid        =f.Oi_Or_Guid                       
          left join ERP_Base_MaterielUnit     h on f.Oi_Pu_Guid     =h.Pu_Guid                          
     where  a.Or_State='查询'        
     ) a 
     group by  Oi_Pr_Guid
    ) b   
where tt.Pr_Guid=b.Oi_Pr_Guid and 
( 
  (Pr_Guid=@Guid and @type=0) or (Pr_Ps_Guid=@Guid and @type=1) or (@type=2)
)
 
go
   

--删除产品的业务单据
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_DelProductBill]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Proc_DelProductBill]
go
CREATE PROCEDURE [dbo].[Proc_DelProductBill]
(
@Pr_Guid varchar(100)  
)   
as 
	delete Buy_PlanItem where Pi_Pr_Guid =@Pr_Guid 
	delete Buy_OrderItem where Oi_Pr_Guid =@Pr_Guid 
	delete Buy_GetProductItem where Gi_Pr_Guid =@Pr_Guid 
	delete Buy_ReturnProductItem where Ri_Pr_Guid =@Pr_Guid 


	delete Sale_PlanItem where Pi_Pr_Guid =@Pr_Guid 
	delete Sale_QuoteItem where Qi_Pr_Guid =@Pr_Guid 
	delete Sale_OrderItem where Oi_Pr_Guid =@Pr_Guid 
	delete Sale_OutStorageItem where Oi_Pr_Guid =@Pr_Guid 
	delete Sale_ReturnProductItem where Ri_Pr_Guid =@Pr_Guid 

	delete Storage_MoveStorageItem where Mi_Pr_Guid =@Pr_Guid 
	delete Storage_OtherInStorageItem where Oi_Pr_Guid =@Pr_Guid 
	delete Storage_OtherOutStorageItem where Oi_Pr_Guid =@Pr_Guid 
	delete Storage_StorageProduct where Sp_Pr_Guid =@Pr_Guid 

	delete Produce_PlanItem where Pi_Pr_Guid =@Pr_Guid 
	delete Produce_OrderItem where Oi_Pr_Guid =@Pr_Guid 
	delete Produce_OrderBom where Ob_Pr_Guid =@Pr_Guid 
	delete Sale_CustSaleItem where Ci_Pr_Guid =@Pr_Guid 
--exec Proc_DelProductBill ''
go



--在业务单据中更新产品的类型
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_UpdateBillPr_Ps_Guid]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Proc_UpdateBillPr_Ps_Guid]
go
CREATE PROCEDURE [dbo].[Proc_UpdateBillPr_Ps_Guid]
(
@Pr_Guid varchar(100),  
@Ps_Guid varchar(100) 
)   
as 
	update Buy_PlanItem set Pi_Ps_Guid=@Ps_Guid where Pi_Pr_Guid =@Pr_Guid
	update Buy_OrderItem set Oi_Ps_Guid=@Ps_Guid  where Oi_Pr_Guid =@Pr_Guid
	update Buy_GetProductItem set Gi_Ps_Guid=@Ps_Guid  where Gi_Pr_Guid =@Pr_Guid
	update Buy_ReturnProductItem set Ri_Ps_Guid=@Ps_Guid  where Ri_Pr_Guid =@Pr_Guid


	update Sale_PlanItem set Pi_Ps_Guid=@Ps_Guid  where Pi_Pr_Guid =@Pr_Guid
	update Sale_QuoteItem set Qi_Ps_Guid=@Ps_Guid  where Qi_Pr_Guid =@Pr_Guid
	update Sale_OrderItem set Oi_Ps_Guid=@Ps_Guid  where Oi_Pr_Guid =@Pr_Guid
	update Sale_OutStorageItem set Oi_Ps_Guid=@Ps_Guid  where Oi_Pr_Guid =@Pr_Guid
	update Sale_ReturnProductItem set Ri_Ps_Guid=@Ps_Guid  where Ri_Pr_Guid =@Pr_Guid

	update Storage_MoveStorageItem set Mi_Ps_Guid=@Ps_Guid  where Mi_Pr_Guid =@Pr_Guid
	update Storage_OtherInStorageItem set Oi_Ps_Guid=@Ps_Guid  where Oi_Pr_Guid =@Pr_Guid
	update Storage_OtherOutStorageItem set Oi_Ps_Guid=@Ps_Guid  where Oi_Pr_Guid =@Pr_Guid
	update Storage_StorageProduct set Sp_Ps_Guid=@Ps_Guid  where Sp_Pr_Guid =@Pr_Guid

	update Produce_PlanItem set Pi_Ps_Guid=@Ps_Guid  where Pi_Pr_Guid =@Pr_Guid
	update Produce_OrderItem set Oi_Ps_Guid=@Ps_Guid  where Oi_Pr_Guid =@Pr_Guid
	update Produce_OrderBom set Ob_Ps_Guid=@Ps_Guid  where Ob_Pr_Guid =@Pr_Guid
	update Sale_CustSaleItem set Ci_Ps_Guid=@Ps_Guid  where Ci_Pr_Guid =@Pr_Guid
--exec Proc_DelProductBill ''
go

--删除客户的业务单据
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_DelCustBill]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Proc_DelCustBill]
go
CREATE PROCEDURE [dbo].[Proc_DelCustBill]
(
@Cu_Guid varchar(100)  
)   
as 
	if exists (select * from tempdb.dbo.sysobjects where id = object_id(N'tempdb.[dbo].[##Proc_DelCustBill_temp1]'))
	  drop table  ##Proc_DelCustBill_temp1

	if exists (select * from tempdb.dbo.sysobjects where id = object_id(N'tempdb.[dbo].[##Proc_DelCustBill_temp2]'))
	  drop table  ##Proc_DelCustBill_temp2

	if exists (select * from tempdb.dbo.sysobjects where id = object_id(N'tempdb.[dbo].[##Proc_DelCustBill_temp3]'))
	  drop table  ##Proc_DelCustBill_temp3

  --1.将客户的订单数据暂存
	select Or_Guid into ##Proc_DelCustBill_temp1 
  from Sale_Order  
  where Or_Cu_Guid= @Cu_Guid
	
  --2.将客户的出库数据暂存
	select Os_Guid 
  into ##Proc_DelCustBill_temp2 
  from Sale_OutStorage where Os_Guid in (
																					select DISTINCT  Oi_Os_Guid 
																					from Sale_OutStorageItem 
																					where Oi_ComeFromCode in (select Or_Guid 
                                                                    from ##Proc_DelCustBill_temp1 
                                                                    )

                                          )
  --3.将客户的退货数据暂存
	select Rp_Guid 
  into ##Proc_DelCustBill_temp3 
  from Sale_ReturnProduct where Rp_Guid in (
																					select DISTINCT  Ri_Rp_Guid 
																					from Sale_ReturnProductItem 
																					where Ri_ComeFromCode in (select Os_Guid 
                                                                    from ##Proc_DelCustBill_temp2 
                                                                    )

                                          )
  --删除此客户的报价数据
	delete Sale_Quote where Qu_Cu_Guid=@Cu_Guid
  --删除此客户的自定义出库数据
	delete Storage_OtherOutStorage where Oo_Cu_Guid=@Cu_Guid

  --删除此客户的订单数据
	delete Sale_Order where Or_Guid in (
																			select Or_Guid 
																			from ##Proc_DelCustBill_temp1
                                      )
  --删除此客户的出库单数据
	delete Sale_OutStorage where Os_Guid in (
																					select Os_Guid 
																					from ##Proc_DelCustBill_temp2
	                                        )
  --删除此客户的退货单数据
	delete Sale_ReturnProduct where Rp_Guid in (
																					select Rp_Guid 
																					from ##Proc_DelCustBill_temp3
	                                        )
	
	if exists (select * from tempdb.dbo.sysobjects where id = object_id(N'tempdb.[dbo].[##Proc_DelCustBill_temp1]'))
	  drop table  ##Proc_DelCustBill_temp1

	if exists (select * from tempdb.dbo.sysobjects where id = object_id(N'tempdb.[dbo].[##Proc_DelCustBill_temp2]'))
	  drop table  ##Proc_DelCustBill_temp2

	if exists (select * from tempdb.dbo.sysobjects where id = object_id(N'tempdb.[dbo].[##Proc_DelCustBill_temp3]'))
	  drop table  ##Proc_DelCustBill_temp3

--exec Proc_DelCustBill ''
go

--删除供应商的业务单据
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_DelSupplierBill]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Proc_DelSupplierBill]
go
CREATE PROCEDURE [dbo].[Proc_DelSupplierBill]
(
@Su_Guid varchar(100)  
)   
as 
	if exists (select * from tempdb.dbo.sysobjects where id = object_id(N'tempdb.[dbo].[##Proc_DelCustBill_temp1]'))
	  drop table  ##Proc_DelCustBill_temp1

	if exists (select * from tempdb.dbo.sysobjects where id = object_id(N'tempdb.[dbo].[##Proc_DelCustBill_temp2]'))
	  drop table  ##Proc_DelCustBill_temp2

	if exists (select * from tempdb.dbo.sysobjects where id = object_id(N'tempdb.[dbo].[##Proc_DelCustBill_temp3]'))
	  drop table  ##Proc_DelCustBill_temp3

  --1.将供应商的订单数据暂存
	select Or_Guid into ##Proc_DelCustBill_temp1 
  from Buy_Order  
  where Or_Su_Guid= @Su_Guid
	
  --2.将供应商的入库数据暂存
	select Gp_Guid 
  into ##Proc_DelCustBill_temp2 
  from Buy_GetProduct where Gp_Guid in (
																					select DISTINCT  Gi_Gp_Guid 
																					from Buy_GetProductItem 
																					where Gi_ComeFromCode in (select Gp_Guid 
                                                                    from ##Proc_DelCustBill_temp1 
                                                                    )

                                          )
  --3.将供应商的退货数据暂存
	select Rp_Guid 
  into ##Proc_DelCustBill_temp3 
  from Buy_ReturnProduct where Rp_Guid in (
																					select DISTINCT  Ri_Rp_Guid 
																					from Buy_ReturnProductItem 
																					where Ri_ComeFromCode in (select Gp_Guid 
                                                                    from ##Proc_DelCustBill_temp2 
                                                                    )

                                          )
  --删除此供应商的自定义出库数据
	delete Storage_OtherInStorage where Oi_Su_Guid=@Su_Guid

  --删除此供应商的订单数据
	delete Buy_Order where Or_Guid in (
																			select Or_Guid 
																			from ##Proc_DelCustBill_temp1
                                      )
  --删除此供应商的入库单数据
	delete Buy_GetProduct where Gp_Guid in (
																					select Gp_Guid 
																					from ##Proc_DelCustBill_temp2
	                                        )
  --删除此供应商的退货单数据
	delete Buy_ReturnProduct where Rp_Guid in (
																					select Rp_Guid 
																					from ##Proc_DelCustBill_temp3
	                                        )
	
	if exists (select * from tempdb.dbo.sysobjects where id = object_id(N'tempdb.[dbo].[##Proc_DelCustBill_temp1]'))
	  drop table  ##Proc_DelCustBill_temp1

	if exists (select * from tempdb.dbo.sysobjects where id = object_id(N'tempdb.[dbo].[##Proc_DelCustBill_temp2]'))
	  drop table  ##Proc_DelCustBill_temp2

	if exists (select * from tempdb.dbo.sysobjects where id = object_id(N'tempdb.[dbo].[##Proc_DelCustBill_temp3]'))
	  drop table  ##Proc_DelCustBill_temp3

--exec Proc_DelSupplierBill ''

go
--测试材料:
--在开始测试前,先运行下面的这二条命令
--这二条命令将清除SQL Server的数据和过程缓冲区，
--这样能够使我们在每次执行查询时在同一个起点上，否则，每次执行查询得到的结果就不具有可比性了
--DBCC DROPCLEANBUFFERS   --从缓冲池中删除所有清除缓冲区。
--DBCC FREEPROCCACHE      --从过程高速缓存中删除所有元素。
--打开查询的描述
--SET STATISTICS IO on
--SET STATISTICS TIME on
--exec Proc_QueryCurStorage '2005-1-1','2007-12-11'
--exec Proc_QuerySumCurStorage '2005-1-1','2007-11-11'
--exec Proc_QuerySumWillStorage '2005-1-1','2007-11-11',1
--你的SQL语句

--SET STATISTICS IO off
--SET STATISTICS TIME off

go



--更新一下产品的属性
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_UpdateProductProperty]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Proc_UpdateProductProperty]
go
CREATE PROCEDURE [dbo].[Proc_UpdateProductProperty]

AS
	update ERP_Base_Materiel set Pr_From='外购',Pr_To='销售',Pr_ProduceStep='成品'
	if (select count(*) from Base_Storage)=1  
	begin
	  update ERP_Base_Materiel set Pr_DefaultStorage=(select top 1 St_Guid from Base_Storage)
	end

--exec Proc_UpdateProductProperty


go
--改变产品分类的默认仓库时更新此子分类与所属产品的默认仓库
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_ChangProductSortDefaultStorage]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Proc_ChangProductSortDefaultStorage]
go
CREATE PROCEDURE [dbo].[Proc_ChangProductSortDefaultStorage]
(
@Ps_Guid varchar(100) ,@St_Guid varchar(100)  
)   
AS 
 
if exists (select * from tempdb.dbo.sysobjects where id = object_id(N'tempdb.[dbo].[##Proc_ChangProductSortDefaultStorage1]'))
  drop table  ##Proc_ChangProductSortDefaultStorage1
      
select *
into ##Proc_ChangProductSortDefaultStorage1
from 
(                                                           
select distinct a.Ps_Guid                                                        
from ERP_Base_MaterielSort a   
where   a.Ps_Guid=@Ps_Guid   
union
select distinct b.Ps_Guid                                                        
from ERP_Base_MaterielSort a left join ERP_Base_MaterielSort b on a.Ps_Guid=b.Ps_FatherGuid   
where   a.Ps_Guid=@Ps_Guid   
union
select distinct c.Ps_Guid                                                        
from ERP_Base_MaterielSort a left join ERP_Base_MaterielSort b on a.Ps_Guid=b.Ps_FatherGuid  
                       left join ERP_Base_MaterielSort c on b.Ps_Guid=c.Ps_FatherGuid
where a.Ps_Guid=@Ps_Guid   

union
select distinct d.Ps_Guid                                                        
from ERP_Base_MaterielSort a left join ERP_Base_MaterielSort b on a.Ps_Guid=b.Ps_FatherGuid  
                       left join ERP_Base_MaterielSort c on b.Ps_Guid=c.Ps_FatherGuid  
                       left join ERP_Base_MaterielSort d on c.Ps_Guid=d.Ps_FatherGuid 
where  a.Ps_Guid=@Ps_Guid   

union
select distinct e.Ps_Guid                                                        
from ERP_Base_MaterielSort a left join ERP_Base_MaterielSort b on a.Ps_Guid=b.Ps_FatherGuid  
                       left join ERP_Base_MaterielSort c on b.Ps_Guid=c.Ps_FatherGuid  
                       left join ERP_Base_MaterielSort d on c.Ps_Guid=d.Ps_FatherGuid  
                       left join ERP_Base_MaterielSort e on d.Ps_Guid=e.Ps_FatherGuid  
where  a.Ps_Guid=@Ps_Guid   

union
select distinct f.Ps_Guid                                                        
from ERP_Base_MaterielSort a left join ERP_Base_MaterielSort b on a.Ps_Guid=b.Ps_FatherGuid  
                       left join ERP_Base_MaterielSort c on b.Ps_Guid=c.Ps_FatherGuid  
                       left join ERP_Base_MaterielSort d on c.Ps_Guid=d.Ps_FatherGuid  
                       left join ERP_Base_MaterielSort e on d.Ps_Guid=e.Ps_FatherGuid  
                       left join ERP_Base_MaterielSort f on e.Ps_Guid=f.Ps_FatherGuid 
where a.Ps_Guid=@Ps_Guid   

union
select distinct g.Ps_Guid                                                        
from ERP_Base_MaterielSort a left join ERP_Base_MaterielSort b on a.Ps_Guid=b.Ps_FatherGuid  
                       left join ERP_Base_MaterielSort c on b.Ps_Guid=c.Ps_FatherGuid  
                       left join ERP_Base_MaterielSort d on c.Ps_Guid=d.Ps_FatherGuid  
                       left join ERP_Base_MaterielSort e on d.Ps_Guid=e.Ps_FatherGuid  
                       left join ERP_Base_MaterielSort f on e.Ps_Guid=f.Ps_FatherGuid  
                       left join ERP_Base_MaterielSort g on f.Ps_Guid=g.Ps_FatherGuid 
where  a.Ps_Guid=@Ps_Guid   

union
select distinct h.Ps_Guid                                                        
from ERP_Base_MaterielSort a left join ERP_Base_MaterielSort b on a.Ps_Guid=b.Ps_FatherGuid  
                       left join ERP_Base_MaterielSort c on b.Ps_Guid=c.Ps_FatherGuid  
                       left join ERP_Base_MaterielSort d on c.Ps_Guid=d.Ps_FatherGuid  
                       left join ERP_Base_MaterielSort e on d.Ps_Guid=e.Ps_FatherGuid  
                       left join ERP_Base_MaterielSort f on e.Ps_Guid=f.Ps_FatherGuid  
                       left join ERP_Base_MaterielSort g on f.Ps_Guid=g.Ps_FatherGuid  
                       left join ERP_Base_MaterielSort h on g.Ps_Guid=h.Ps_FatherGuid
where  a.Ps_Guid=@Ps_Guid   
  
)  aa                                                                                  
            

--更新分类的默认仓库                                                                      ;
update tt set Ps_DefaultStorage=@St_Guid from ERP_Base_MaterielSort tt where tt.Ps_Guid in
(select distinct  Ps_Guid from  ##Proc_ChangProductSortDefaultStorage1 where  Ps_Guid<>@Ps_Guid)
               
--更新产品的默认仓库                                                                      ;
update tt set Pr_DefaultStorage=@St_Guid from ERP_Base_Materiel tt where tt.Pr_Ps_Guid in
(select distinct  Ps_Guid from  ##Proc_ChangProductSortDefaultStorage1)
                                                               

go


--生成初始库存的数据
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_BillBegStorage]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Proc_BillBegStorage]
go
CREATE PROCEDURE [dbo].[Proc_BillBegStorage]
AS
  --先处理有默认仓库的一个产品生成一条记录
  insert into Storage_StorageProduct(Sp_St_Guid,Sp_Ps_Guid,Sp_Pr_Guid,pubid)                     
  select a.St_Guid,b.Pr_Ps_Guid,b.Pr_Guid ,newid()                                                
  from Base_Storage a , ERP_Base_Materiel b                                                    
  where a.St_Guid=b.Pr_DefaultStorage and b.Pr_DefaultStorage<>'' and      
        a.St_Guid+b.Pr_Guid not in (select Sp_St_Guid+Sp_Pr_Guid from Storage_StorageProduct)  
  --再处理没有默认仓库的一个产品生成多条记录(有几个仓库生成几条记录)
  insert into Storage_StorageProduct(Sp_St_Guid,Sp_Ps_Guid,Sp_Pr_Guid,pubid)                    
  select a.St_Guid,b.Pr_Ps_Guid,b.Pr_Guid ,newid()                                                 
  from Base_Storage a , ERP_Base_Materiel b                                                    
  where (b.Pr_DefaultStorage='' or b.Pr_DefaultStorage is null) and        
        a.St_Guid+b.Pr_Guid not in (select Sp_St_Guid+Sp_Pr_Guid from Storage_StorageProduct)   
--删除不存在的仓库与产品
  delete Storage_StorageProduct where Sp_St_Guid not in (select St_Guid from Base_Storage)    
  delete Storage_StorageProduct where Sp_Pr_Guid not in (select Pr_Guid from ERP_Base_Materiel)    
 
go


--统计资金的收与未疏,付与未付
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_SumMoneyAndSetMoneyState]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Proc_SumMoneyAndSetMoneyState]
go
CREATE PROCEDURE [dbo].[Proc_SumMoneyAndSetMoneyState]
(
@BillType varchar(100),
@BillGuid varchar(8000) ,@BillDiscount ERPDefaultDecimal,@IsSetState bit=0,@IsSetCustOrSuppSumMoney bit=0,
@ResultStr varchar(1000) output,
@NoMoney ERPDefaultDecimal output
)  
AS
  declare @tempSumMoney   ERPDefaultDecimal 
  declare @tempEndMoney   ERPDefaultDecimal 
  declare @tempCostMoney  ERPDefaultDecimal 
  declare @tempSetupMoney ERPDefaultDecimal 
  declare @tempState      varchar(100) 
  declare @tempIsViewSaleCost  bit 

  declare @tempCu_Guid      varchar(100) 
  declare @tempSu_Guid      varchar(100) 
  set @NoMoney=0
  set @ResultStr=''
  set @BillDiscount=isnull(@BillDiscount,0)

  set @tempIsViewSaleCost=0
  select @tempIsViewSaleCost=isnull(SYSVALUE,0) from YBSYSINFO where YBSYSNAME='IsViewSaleCost'  
   

  if isnull((select SYSVALUE from YBSYSINFO where YBSYSNAME='IsSumMoneyAndSetMoneyState'),'0')<>'1'  
    RETURN

  if @BillType ='btSale_Plan'  
  begin
	  select @tempSumMoney=isnull(sum(isnull(Pi_SalePrice ,0)*(1-isnull(Pi_Discount,0))*(isnull(Pi_Quantity,0))),0)*(1-@BillDiscount), 
	         @tempCostMoney=isnull(sum(isnull(Pi_CostPrice ,0)*(isnull(Pi_Quantity,0))),0),  
	         @tempSetupMoney=isnull(sum(isnull(Pi_SetupPrice,0)*(isnull(Pi_Quantity,0))),0)   
	  from Sale_PlanItem where Pi_Pl_Guid=@BillGuid 
                                            
    if @tempSumMoney<>0  
      set @ResultStr=@ResultStr+'计划:'+ltrim(str(@tempSumMoney,20,2))

    if @tempCostMoney<>0 and @tempIsViewSaleCost=1
      set @ResultStr=@ResultStr+'成本:'+ltrim(str(@tempCostMoney,20,2))

    if @tempSetupMoney<>0
      set @ResultStr=@ResultStr+'安装:'+ltrim(str(@tempSetupMoney,20,2))
  end
  else if @BillType ='btSale_Quote'
  begin
		 select @tempSumMoney=isnull(sum(isnull(Qi_SalePrice ,0)*(1-isnull(Qi_Discount,0))*(isnull(Qi_Quantity,0))),0)*(1-@BillDiscount), 
		        @tempCostMoney =isnull(sum(isnull(Qi_CostPrice ,0)*(isnull(Qi_Quantity,0))),0),  
		        @tempSetupMoney=isnull(sum(isnull(Qi_SetupPrice,0)*(isnull(Qi_Quantity,0))),0)   
		 from Sale_QuoteItem where Qi_Qu_Guid=@BillGuid
                                             
    if @tempSumMoney<>0
      set @ResultStr=@ResultStr+'报价:'+ltrim(str(@tempSumMoney,20,2))

    if (@tempCostMoney<>0)   and @tempIsViewSaleCost=1
      set @ResultStr=@ResultStr+'成本:'+ltrim(str(@tempCostMoney,20,2))

    if @tempSetupMoney<>0
      set @ResultStr=@ResultStr+'安装:'+ltrim(str(@tempSetupMoney,20,2))
  end
  else if @BillType ='btSale_Order'
  begin
	 select @tempSumMoney=isnull(sum(isnull(Oi_SalePrice ,0)*(1-isnull(Oi_Discount,0))*(isnull(Oi_Quantity,0))),0)*(1-@BillDiscount), 
	        @tempCostMoney =isnull(sum(isnull(Oi_CostPrice ,0)*(isnull(Oi_Quantity,0))),0),  
	        @tempSetupMoney=isnull(sum(isnull(Oi_SetupPrice,0)*(isnull(Oi_Quantity,0))),0)   
	 from Sale_OrderItem where Oi_Or_Guid=@BillGuid
                                            
	 select @tempEndMoney=isnull(sum(isnull(Og_Money,0)),0) 
	 from Sale_OrderGetMoney where Og_Or_Guid=@BillGuid and Og_Type='正常'

    if @tempSumMoney<>0
      set @ResultStr=@ResultStr+'销售:'+ltrim(str(@tempSumMoney,20,2))
    if @tempSumMoney-@tempEndMoney<>0
    begin
      set @ResultStr=@ResultStr+'未收:'+ltrim(str(@tempSumMoney-@tempEndMoney,20,2))
      set @NoMoney=@tempSumMoney-@tempEndMoney 
    end
    if @tempEndMoney<>0
      set @ResultStr=@ResultStr+'已收:'+ltrim(str(@tempEndMoney,20,2))

    if (@tempCostMoney<>0)    and @tempIsViewSaleCost=1
      set @ResultStr=@ResultStr+'成本:'+ltrim(str(@tempCostMoney,20,2))

    if @tempSetupMoney<>0
      set @ResultStr=@ResultStr+'安装:'+ltrim(str(@tempSetupMoney,20,2))

    if @IsSetState=1
    begin
		  if @tempEndMoney>0 
		  begin
		    if @tempEndMoney<@tempSumMoney
		      set @tempState='部分收款'
		    else
		      set @tempState='全部收款'
		  end
		  else
		    set @tempState='未收款'
	    
	    update Sale_Order set Or_GetMoneyState=@tempState where Or_Guid=@BillGuid
    end

    if @IsSetCustOrSuppSumMoney=1  
    begin
	    select @tempCu_Guid=isnull(Or_Cu_Guid,'') from Sale_Order where Or_Guid=@BillGuid
      
      if @tempCu_Guid is not null and @tempCu_Guid<>'' 
      begin  
				select @tempSumMoney=isnull(sum(isnull(SumMoney,0)),0)
				from 
				(
				select  isnull(sum(isnull(b.Oi_SalePrice ,0)*(1-isnull(b.Oi_Discount,0))*(isnull(b.Oi_Quantity,0))),0)*(1-isnull(a.Or_Discount,0)) as SumMoney
				from Sale_Order a left join Sale_OrderItem b on a.Or_Guid=b.Oi_Or_Guid 
				where a.Or_Cu_Guid=@tempCu_Guid
				group by a.Or_Guid,a.Or_Discount
				) aa
	
				select  @tempEndMoney=isnull(sum(isnull(b.Og_Money ,0)),0)  
				from Sale_Order a left join Sale_OrderGetMoney b on a.Or_Guid=b.Og_Or_Guid 
				where a.Or_Cu_Guid=@tempCu_Guid

		    update Base_Cust set Cu_PrepayMoney=@tempEndMoney-@tempSumMoney where Cu_Guid=@tempCu_Guid and Cu_PrepayMoney<>@tempEndMoney-@tempSumMoney
         if @tempEndMoney-@tempSumMoney>0
           set @ResultStr=@ResultStr+'总预付:'+ltrim(str(@tempEndMoney-@tempSumMoney,20,2))
         else
           set @ResultStr=@ResultStr+'总未付:'+ltrim(str(abs(@tempEndMoney-@tempSumMoney),20,2))
      end
    end  
  end
  else if @BillType ='btSale_ReturnProduct'
  begin
		 select @tempSumMoney=isnull(sum(isnull(Ri_Price ,0)*(1-isnull(Ri_Discount,0))*(isnull(Ri_Quantity,0))),0)*(1-@BillDiscount)  
		 from Sale_ReturnProductItem where Ri_Rp_Guid=@BillGuid
		 
     select @tempEndMoney=isnull(sum(isnull(Rm_Money,0)),0) 
		 from Sale_ReturnProductMoney where Rm_Rp_Guid=@BillGuid
                                             
    if @tempSumMoney<>0
      set @ResultStr=@ResultStr+'应退:'+ltrim(str(@tempSumMoney,20,2))
    if @tempEndMoney<>0
      set @ResultStr=@ResultStr+'已退:'+ltrim(str(@tempEndMoney,20,2))

    if @tempSumMoney-@tempEndMoney<>0
    begin
      set @ResultStr=@ResultStr+'未退:'+ltrim(str(@tempSumMoney-@tempEndMoney,20,2))
      set @NoMoney=@tempSumMoney-@tempEndMoney 
    end
  end
  else if @BillType ='btSale_CustSale'
  begin
		 select @tempSumMoney=isnull(sum(isnull(Ci_SalePrice ,0)*(1-isnull(Ci_Discount,0))*(isnull(Ci_Quantity,0))),0)*(1-@BillDiscount)  
		 from Sale_CustSaleItem where Ci_Cs_Guid=@BillGuid
		 
                                            
    if @tempSumMoney<>0
      set @ResultStr=@ResultStr+'客户销售:'+ltrim(str(@tempSumMoney,20,2))
    
  end
  else if @BillType ='btBuy_Plan'
  begin
	 select @tempSumMoney=isnull(sum(isnull(Pi_Price ,0)*(1-isnull(Pi_Discount,0))*(isnull(Pi_Quantity,0))),0)*(1-@BillDiscount)  
	 from Buy_PlanItem where Pi_Pl_Guid=@BillGuid

    if @tempSumMoney<>0
      set @ResultStr=@ResultStr+'计划:'+ltrim(str(@tempSumMoney,20,2))
  end
  else if @BillType ='btBuy_Order'
  begin
	 select @tempSumMoney=isnull(sum(isnull(Oi_Price ,0)*(1-isnull(Oi_Discount,0))*(isnull(Oi_Quantity,0))),0)*(1-@BillDiscount)  
	 from Buy_OrderItem where Oi_Or_Guid=@BillGuid
	 
   select @tempEndMoney=isnull(sum(isnull(Op_Money,0)),0) 
	 from Buy_OrderPayMoney where Op_Or_Guid=@BillGuid  and Op_Type='正常'

    if @tempSumMoney<>0
      set @ResultStr=@ResultStr+'采购:'+ltrim(str(@tempSumMoney,20,2))
    if @tempSumMoney-@tempEndMoney<>0
    begin
      set @ResultStr=@ResultStr+'未付:'+ltrim(str(@tempSumMoney-@tempEndMoney,20,2))
      set @NoMoney=@tempSumMoney-@tempEndMoney 
    end

    if @tempSumMoney-@tempEndMoney<>0
      set @ResultStr=@ResultStr+'已付:'+ltrim(str(@tempEndMoney,20,2))

    if @IsSetState=1
    begin
		  if @tempEndMoney>0  
		  begin
		    if @tempEndMoney<@tempSumMoney
		      set @tempState='部分付款'
		    else
		      set @tempState='全部付款'
		  end
		  else
		    set @tempState='未付款'

	    update Buy_Order set Or_PayMoneyState=@tempState where Or_Guid=@BillGuid
    end

    if @IsSetCustOrSuppSumMoney=1  
    begin
	    select @tempSu_Guid=isnull(Or_Su_Guid,'') from Buy_Order where Or_Guid=@BillGuid
      
      if @tempSu_Guid is not null and @tempSu_Guid<>'' 
      begin  
				select @tempSumMoney=isnull(sum(isnull(SumMoney,0)),0)
				from 
				(
				select  isnull(sum(isnull(b.Oi_Price ,0)*(1-isnull(b.Oi_Discount,0))*(isnull(b.Oi_Quantity,0))),0)*(1-isnull(a.Or_Discount,0)) as SumMoney
				from Buy_Order a left join Buy_OrderItem b on a.Or_Guid=b.Oi_Or_Guid 
				where a.Or_Su_Guid=@tempSu_Guid
				group by a.Or_Guid,a.Or_Discount
				) aa
	
				select  @tempEndMoney=isnull(sum(isnull(b.Op_Money ,0)),0)  
				from Buy_Order a left join Buy_OrderPayMoney b on a.Or_Guid=b.Op_Or_Guid 
				where a.Or_Su_Guid=@tempSu_Guid

		    update Base_Supplier set Su_PrepayMoney=@tempEndMoney-@tempSumMoney where Su_Guid=@tempSu_Guid and Su_PrepayMoney<>@tempEndMoney-@tempSumMoney
         if @tempEndMoney-@tempSumMoney>0
           set @ResultStr=@ResultStr+'总预付:'+ltrim(str(@tempEndMoney-@tempSumMoney,20,2))
         else
           set @ResultStr=@ResultStr+'总未付:'+ltrim(str(abs(@tempEndMoney-@tempSumMoney),20,2))
      end
    end  
  end
  else if @BillType ='btBuy_ReturnProduct'
  begin
	 select @tempSumMoney=isnull(sum(isnull(Ri_Price ,0)*(1-isnull(Ri_Discount,0))*(isnull(Ri_Quantity,0))),0)*(1-@BillDiscount)  
	 from Buy_ReturnProductItem where Ri_Rp_Guid=@BillGuid
                                           
	 select @tempEndMoney=isnull(sum(isnull(Rm_Money,0)),0) 
	 from Buy_ReturnProductMoney where Rm_Rp_Guid=@BillGuid

    if @tempSumMoney<>0
      set @ResultStr=@ResultStr+'应退:'+ltrim(str(@tempSumMoney,20,2))
    if @tempEndMoney<>0
      set @ResultStr=@ResultStr+'已退:'+ltrim(str(@tempEndMoney,20,2))

    if @tempSumMoney-@tempEndMoney<>0
    begin
      set @ResultStr=@ResultStr+'未退:'+ltrim(str(@tempSumMoney-@tempEndMoney,20,2))
      set @NoMoney=@tempSumMoney-@tempEndMoney 
    end
  end
  else if @BillType ='btProduce_Order'
  begin
	 select @tempSumMoney=isnull(sum(isnull(Oi_Price ,0)*(1-isnull(Oi_Discount,0))*(isnull(Oi_Quantity,0))),0)*(1-@BillDiscount)  
	 from Produce_OrderItem where Oi_Or_Guid=@BillGuid
                                           
	 select @tempEndMoney=isnull(sum(isnull(Ob_Price ,0)*(1-isnull(Ob_Discount,0))*(isnull(Ob_Quantity,0))),0)   
	 from Produce_OrderBom where Ob_Or_Guid=@BillGuid

    if @tempSumMoney<>0
      set @ResultStr=@ResultStr+'可销售:'+ltrim(str(@tempSumMoney,20,2))
    if @tempEndMoney<>0  
      set @ResultStr=@ResultStr+'原料成本:'+ltrim(str(@tempEndMoney,20,2))

    if @tempSumMoney-@tempEndMoney<>0   
    begin
      set @ResultStr=@ResultStr+'利润:'+ltrim(str(@tempSumMoney-@tempEndMoney,20,2))
      set @NoMoney=@tempSumMoney-@tempEndMoney 
    end
  end
  else if @BillType ='btStorage_OtherInStorage'
  begin
	 select @tempSumMoney=isnull(sum(isnull(Oi_Price ,0)*(1-isnull(Oi_Discount,0))*(isnull(Oi_Quantity,0))),0)*(1-@BillDiscount)  
	 from Storage_OtherInStorageItem where Oi_Oi_Guid=@BillGuid
                                           
    if @tempSumMoney<>0
      set @ResultStr=@ResultStr+'入库:'+ltrim(str(@tempSumMoney,20,2))
  end
  else if @BillType ='btStorage_OtherOutStorage'
  begin
	 select @tempSumMoney  =isnull(sum(isnull(Oi_SalePrice ,0)*(1-isnull(Oi_Discount,0))*(isnull(Oi_Quantity,0))),0)*(1-@BillDiscount), 
	        @tempCostMoney =isnull(sum(isnull(Oi_CostPrice ,0)*(isnull(Oi_Quantity,0))),0),  
	        @tempSetupMoney=isnull(sum(isnull(Oi_SetupPrice,0)*(isnull(Oi_Quantity,0))),0)   
	 from Storage_OtherOutStorageItem where Oi_Oo_Guid=@BillGuid
                                            
    if @tempSumMoney<>0
      set @ResultStr=@ResultStr+'出库:'+ltrim(str(@tempSumMoney,20,2))
    if (@tempCostMoney<>0)  and @tempIsViewSaleCost=1
      set @ResultStr=@ResultStr+'成本:'+ltrim(str(@tempCostMoney,20,2))
    if @tempSetupMoney<>0
      set @ResultStr=@ResultStr+'安装:'+ltrim(str(@tempSetupMoney,20,2))
  end
  else if @BillType ='btService_Order'
  begin
    set @tempSumMoney=0
  end

  if isnull((select SYSVALUE from YBSYSINFO where YBSYSNAME='DefaultAddMoneyIsZero'),'0')='1'  
    set @NoMoney=0 

go 

--导入上级数据中设置源的主从表
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_IntroductionData_SetMainDetailSql]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Proc_IntroductionData_SetMainDetailSql]
go
CREATE PROCEDURE [dbo].[Proc_IntroductionData_SetMainDetailSql]
(
@BillType varchar(100),
@MainSql varchar(8000) output,@MainTableGuid varchar(100) output,
@DetailSql varchar(8000) output,
@FormCaption varchar(100)  output
)  
AS
  if @BillType ='btSale_Quote' 
  begin
    set @FormCaption='导出销售报价到销售订单'
    set @MainSql=
      ' select  * from (                                                                                               '+
      ' select a.*,                                                                                                    '+
		  '            isnull((                                                                                            '+
			' 					          select sum(Qi_Quantity)                                                                    '+
			' 					          from                                                                                       '+
			' 					            ( select sum(isnull(c.Qi_Quantity,0))*(max(isnull(d.Pu_BaseUnitNum,0))) as Qi_Quantity   '+
			' 					              from Sale_QuoteItem c left join ERP_Base_MaterielUnit d on c.Qi_Pu_Guid     =d.Pu_Guid      '+
			' 					              where  c.Qi_Qu_Guid = a.Qu_Guid                                                        '+
			' 					              group by c.Qi_Guid                                                                     '+
			' 					            ) aa                                                                                     '+
			' 				           )                                                                                           '+
			' 			             ,0) as  总计数量,                                                                           '+
      '                                                                                                                '+
		  '            isnull((                                                                                            '+
			' 					          select sum(Oi_Quantity)                                                                    '+
			' 					          from                                                                                       '+
			' 					            ( select sum(isnull(c.Oi_Quantity,0))*(max(isnull(d.Pu_BaseUnitNum,0))) as Oi_Quantity   '+
			' 					              from Sale_OrderItem      c left join ERP_Base_MaterielUnit d on c.Oi_Pu_Guid     =d.Pu_Guid '+
			' 					              where  c.Oi_ComeFromCode = a.Qu_Guid                                                   '+
			' 					              group by c.Oi_Guid                                                                     '+
			' 					            ) aa                                                                                     '+
			' 				           )                                                                                           '+
			' 			             ,0) as 已导入数量                                                                            '+
      '  from Sale_Quote a                                                                                             '+
      '  where convert(varchar(10),a.Qu_Datetime,121) >= :BegDate and convert(varchar(10),a.Qu_Datetime,121)<= :EndDate and a.Qu_State =''查询''                          '+
      '        and ( :IsBillCode =0  or a.Qu_Code= :BillCode )                                                        '+
      '  ) a  where   ( :IsViewIntroduction = 1  or  总计数量-已导入数量>0)                                             '+
      '  order by a.Qu_Datetime                                                                                        '
    set @DetailSql=
      ' select  * from (                                                                                               '+
      '  select a.*,                                                                                                   '+
      '            isnull(a.Qi_Quantity,0)*isnull(b.Pu_BaseUnitNum,0)-                                                 '+
		  '            isnull((                                                                                            '+
			' 					          select sum(Oi_Quantity)                                                                    '+
			' 					          from                                                                                       '+
			' 					            ( select sum(isnull(c.Oi_Quantity,0))*(max(isnull(d.Pu_BaseUnitNum,0))) as Oi_Quantity   '+
			' 					              from Sale_OrderItem c left join ERP_Base_MaterielUnit d on c.Oi_Pu_Guid     =d.Pu_Guid      '+
			' 					              where  c.Oi_ComeFromItemCode =  a.Qi_Guid                                              '+
			' 					              group by c.Oi_Guid                                                                     '+
			' 					            ) aa                                                                                     '+
			' 				           )                                                                                           '+
			' 			             ,0) as 未导入数量,b.Pu_BaseUnitNum                                                          '+
      '  from Sale_QuoteItem a left join ERP_Base_MaterielUnit b on a.Qi_Pu_Guid=b.Pu_Guid                                  '+
      '  where  a.Qi_Qu_Guid = :Qu_Guid                                                                                '+
      '  ) a  where   ( :IsViewIntroduction = 1  or  未导入数量>0)                                             ' 
    set @MainTableGuid= 'Qu_Guid'
  end
  else if @BillType ='btSale_Order' 
  begin
    set @FormCaption='导出销售订单到销售出库'
    set @MainSql=
      ' select  * from (                                                                                               '+
      ' select a.*,                                                                                                    '+
		  '            isnull((                                                                                            '+
			' 					          select sum(Oi_Quantity)                                                                    '+        
			' 					          from                                                                                       '+  
			' 					            ( select sum(isnull(c.Oi_Quantity,0))*(max(isnull(d.Pu_BaseUnitNum,0))) as Oi_Quantity   '+                
			' 					              from Sale_OrderItem c left join ERP_Base_MaterielUnit d on c.Oi_Pu_Guid     =d.Pu_Guid      '+
			' 					              where  c.Oi_Or_Guid = a.Or_Guid                                                        '+
			' 					              group by c.Oi_Guid                                                                     '+
			' 					            ) aa                                                                                     '+  
			' 				           )                                                                                           '+
			' 			             ,0) as  总计数量,                                                                           '+
      '                                                                                                                '+
		  '            isnull((                                                                                            '+
			' 					          select sum(Oi_Quantity)                                                                    '+        
			' 					          from                                                                                       '+  
			' 					            ( select sum(isnull(c.Oi_Quantity,0))*(max(isnull(d.Pu_BaseUnitNum,0))) as Oi_Quantity   '+                
			' 					              from Sale_OutStorageItem c left join ERP_Base_MaterielUnit d on c.Oi_Pu_Guid     =d.Pu_Guid '+    
			' 					              where  c.Oi_ComeFromCode = a.Or_Guid                                                   '+   
			' 					              group by c.Oi_Guid                                                                     '+     
			' 					            ) aa                                                                                     '+  
			' 				           )                                                                                           '+
			' 			             ,0) as 已导入数量                                                                            '+
      '  from Sale_Order a                                                                                             '+
      '  where convert(varchar(10),a.Or_Datetime,121)>= :BegDate and convert(varchar(10),a.Or_Datetime,121)<= :EndDate and a.Or_State =''查询''                          '+
      '        and ( :IsBillCode =0  or a.Or_Code= :BillCode )                                                         '+
      '  ) a  where   ( :IsViewIntroduction = 1  or  总计数量-已导入数量>0)                                             '+
      '  order by a.Or_Datetime                                                                                        '
    set @DetailSql=
      ' select  * from (                                                                                               '+
      '  select a.*,                                                                                                   '+
      '            isnull(a.Oi_Quantity,0)*isnull(b.Pu_BaseUnitNum,0)-                                                 '+
		  '            isnull((                                                                                            '+
			' 					          select sum(Oi_Quantity)                                                                    '+
			' 					          from                                                                                       '+
			' 					            ( select sum(isnull(c.Oi_Quantity,0))*(max(isnull(d.Pu_BaseUnitNum,0))) as Oi_Quantity   '+
			' 					              from Sale_OutStorageItem c left join ERP_Base_MaterielUnit d on c.Oi_Pu_Guid     =d.Pu_Guid '+
			' 					              where  c.Oi_ComeFromItemCode =  a.Oi_Guid                                              '+
			' 					              group by c.Oi_Guid                                                                     '+
			' 					            ) aa                                                                                     '+
			' 				           )                                                                                           '+
			' 			             ,0) as 未导入数量,b.Pu_BaseUnitNum                                                          '+
      '  from Sale_OrderItem a left join ERP_Base_MaterielUnit b on a.Oi_Pu_Guid=b.Pu_Guid                                  '+
      '  where  a.Oi_Or_Guid = :Or_Guid                                                                                '+
      '  ) a  where   ( :IsViewIntroduction = 1  or  未导入数量>0)                                             ' 
    set @MainTableGuid= 'Or_Guid'
  end
  else if @BillType ='btSale_OutStorage' 
  begin
    set @FormCaption='导出销售出库到销售退货'
    set @MainSql=
      ' select  * from (                                                                                               '+
      ' select a.*,                                                                                                    '+
		  '            isnull((                                                                                            '+
			' 					          select sum(Oi_Quantity)                                                                    '+
			' 					          from                                                                                       '+
			' 					            ( select sum(isnull(c.Oi_Quantity,0))*(max(isnull(d.Pu_BaseUnitNum,0))) as Oi_Quantity   '+
			' 					              from Sale_OutStorageItem c left join ERP_Base_MaterielUnit d on c.Oi_Pu_Guid     =d.Pu_Guid '+
			' 					              where  c.Oi_Os_Guid = a.Os_Guid                                                        '+
			' 					              group by c.Oi_Guid                                                                     '+
			' 					            ) aa                                                                                     '+
			' 				           )                                                                                           '+
			' 			             ,0) as  总计数量,                                                                           '+
      '                                                                                                                '+
		  '            isnull((                                                                                            '+
			' 					          select sum(Ri_Quantity)                                                                    '+
			' 					          from                                                                                       '+
			' 					            ( select sum(isnull(c.Ri_Quantity,0))*(max(isnull(d.Pu_BaseUnitNum,0))) as Ri_Quantity   '+
			' 					              from Sale_ReturnProductItem  c left join ERP_Base_MaterielUnit d on c.Ri_Pu_Guid=d.Pu_Guid  '+
			' 					              where  c.Ri_ComeFromCode = a.Os_Guid                                                   '+
			' 					              group by c.Ri_Guid                                                                     '+
			' 					            ) aa                                                                                     '+
			' 				           )                                                                                           '+
			' 			             ,0) as 已导入数量                                                                           '+
      '  from Sale_OutStorage a                                                                                        '+
      '  where convert(varchar(10),a.Os_Datetime,121)>= :BegDate and convert(varchar(10),a.Os_Datetime,121)<= :EndDate and a.Os_State =''查询''                          '+
      '        and ( :IsBillCode =0  or a.Os_Code= :BillCode )                                                        '+
      '  ) a  where   ( :IsViewIntroduction = 1  or  总计数量-已导入数量>0)                                             '+
      '  order by a.Os_Datetime                                                                                        '
    set @DetailSql=
      ' select  * from (                                                                                               '+
      '  select a.*,                                                                                                   '+
      '            isnull(a.Oi_Quantity,0)*isnull(b.Pu_BaseUnitNum,0)-                                                 '+
		  '            isnull((                                                                                            '+
			' 					          select sum(Ri_Quantity)                                                                    '+
			' 					          from                                                                                       '+
			' 					            ( select sum(isnull(c.Ri_Quantity,0))*(max(isnull(d.Pu_BaseUnitNum,0))) as Ri_Quantity   '+
			' 					              from Sale_ReturnProductItem c left join ERP_Base_MaterielUnit d on c.Ri_Pu_Guid =d.Pu_Guid  '+
			' 					              where  c.Ri_ComeFromItemCode =  a.Oi_Guid                                              '+
			' 					              group by c.Ri_Guid                                                                     '+
			' 					            ) aa                                                                                     '+
			' 				           )                                                                                           '+
			' 			             ,0) as 未导入数量,b.Pu_BaseUnitNum                                                          '+
      '  from Sale_OutStorageItem a left join ERP_Base_MaterielUnit b on a.Oi_Pu_Guid=b.Pu_Guid                             '+
      '  where  a.Oi_Os_Guid = :Os_Guid                                                                                '+
      '  ) a  where   ( :IsViewIntroduction = 1  or  未导入数量>0)                                             ' 
    set @MainTableGuid= 'Os_Guid'
  end
  else if @BillType = 'btBuy_Plan' 
  begin
    set @FormCaption='导出采购计划到采购订单'
    set @MainSql=
      ' select  * from (                                                                                               '+
      ' select a.*,                                                                                                    '+
		  '            isnull((                                                                                            '+
			' 					          select sum(Pi_Quantity)                                                                    '+
			' 					          from                                                                                       '+
			' 					            ( select sum(isnull(c.Pi_Quantity,0))*(max(isnull(d.Pu_BaseUnitNum,0))) as Pi_Quantity   '+
			' 					              from Buy_PlanItem c left join ERP_Base_MaterielUnit d on c.Pi_Pu_Guid     =d.Pu_Guid        '+
			' 					              where  c.Pi_Pl_Guid = a.Pl_Guid                                                        '+
			' 					              group by c.Pi_Guid                                                                     '+
			' 					            ) aa                                                                                     '+
			' 				           )                                                                                           '+
			' 			             ,0) as  总计数量,                                                                           '+
      '                                                                                                                '+
		  '            isnull((                                                                                            '+
			' 					          select sum(Oi_Quantity)                                                                    '+
			' 					          from                                                                                       '+
			' 					            ( select sum(isnull(c.Oi_Quantity,0))*(max(isnull(d.Pu_BaseUnitNum,0))) as Oi_Quantity   '+
			' 					              from Buy_OrderItem      c left join ERP_Base_MaterielUnit d on c.Oi_Pu_Guid     =d.Pu_Guid  '+
			' 					              where  c.Oi_ComeFromCode = a.Pl_Guid                                                   '+
			' 					              group by c.Oi_Guid                                                                     '+
			' 					            ) aa                                                                                     '+
			' 				           )                                                                                           '+
			' 			             ,0) as 已导入数量                                                                           '+
      '  from Buy_Plan a                                                                                               '+
      '  where convert(varchar(10),a.Pl_Datetime,121)>= :BegDate and convert(varchar(10),a.Pl_Datetime,121)<= :EndDate and a.Pl_State =''查询''                          '+
      '        and ( :IsBillCode =0  or a.Pl_Code= :BillCode )                                                        '+
      '  ) a  where   ( :IsViewIntroduction = 1  or  总计数量-已导入数量>0)                                             '+
      '  order by a.Pl_Datetime                                                                                        '
    set @DetailSql=
      ' select  * from (                                                                                               '+
      '  select a.*,                                                                                                   '+
      '            isnull(a.Pi_Quantity,0)*isnull(b.Pu_BaseUnitNum,0)-                                                 '+
		  '            isnull((                                                                                            '+
			' 					          select sum(Oi_Quantity)                                                                    '+
			' 					          from                                                                                       '+
			' 					            ( select sum(isnull(c.Oi_Quantity,0))*(max(isnull(d.Pu_BaseUnitNum,0))) as Oi_Quantity   '+
			' 					              from Buy_OrderItem          c left join ERP_Base_MaterielUnit d on c.Oi_Pu_Guid =d.Pu_Guid  '+
			' 					              where  c.Oi_ComeFromItemCode =  a.Pi_Guid                                              '+
			' 					              group by c.Oi_Guid                                                                     '+
			' 					            ) aa                                                                                     '+
			' 				           )                                                                                           '+
			' 			             ,0) as 未导入数量,b.Pu_BaseUnitNum                                                          '+
      '  from Buy_PlanItem a left join ERP_Base_MaterielUnit b on a.Pi_Pu_Guid=b.Pu_Guid                                    '+
      '  where  a.Pi_Pl_Guid = :Pl_Guid                                                                                '+
      '  ) a  where   ( :IsViewIntroduction = 1  or  未导入数量>0)                                             ' 
    set @MainTableGuid= 'Pl_Guid'
  end
  else if @BillType = 'btBuy_Order' 
  begin
    set @FormCaption='导出采购订单到采购收货'
    set @MainSql=
      ' select  * from (                                                                                               '+
      ' select a.*,                                                                                                    '+
		  '            isnull((                                                                                            '+
			' 					          select sum(Oi_Quantity)                                                                    '+
			' 					          from                                                                                       '+
			' 					            ( select sum(isnull(c.Oi_Quantity,0))*(max(isnull(d.Pu_BaseUnitNum,0))) as Oi_Quantity   '+
			' 					              from Buy_OrderItem c left join ERP_Base_MaterielUnit d on c.Oi_Pu_Guid     =d.Pu_Guid       '+
			' 					              where  c.Oi_Or_Guid = a.Or_Guid                                                        '+
			' 					              group by c.Oi_Guid                                                                     '+
			' 					            ) aa                                                                                     '+
			' 				           )                                                                                           '+
			' 			             ,0) as  总计数量,                                                                           '+
      '                                                                                                                '+
		  '            isnull((                                                                                            '+
			' 					          select sum(Gi_Quantity)                                                                    '+
			' 					          from                                                                                       '+
			' 					            ( select sum(isnull(c.Gi_Quantity,0))*(max(isnull(d.Pu_BaseUnitNum,0))) as Gi_Quantity   '+
			' 					              from Buy_GetProductItem  c left join ERP_Base_MaterielUnit d on c.Gi_Pu_Guid     =d.Pu_Guid '+
			' 					              where  c.Gi_ComeFromCode = a.Or_Guid                                                   '+
			' 					              group by c.Gi_Guid                                                                     '+
			' 					            ) aa                                                                                     '+
			' 				           )                                                                                           '+
			' 			             ,0) as 已导入数量                                                                           '+
      '  from Buy_Order a                                                                                              '+
      '  where convert(varchar(10),a.Or_Datetime,121)>= :BegDate and convert(varchar(10),a.Or_Datetime,121)<= :EndDate and a.Or_State =''查询''                          '+
      '        and ( :IsBillCode =0  or a.Or_Code= :BillCode )                                                        '+
      '  ) a  where   ( :IsViewIntroduction = 1  or  总计数量-已导入数量>0)                                             '+
      '  order by a.Or_Datetime                                                                                        '
    set @DetailSql=
      ' select  * from (                                                                                               '+
      '  select a.*,                                                                                                   '+
      '            isnull(a.Oi_Quantity,0)*isnull(b.Pu_BaseUnitNum,0)-                                                 '+
		  '            isnull((                                                                                            '+
			' 					          select sum(Gi_Quantity)                                                                    '+
			' 					          from                                                                                       '+
			' 					            ( select sum(isnull(c.Gi_Quantity,0))*(max(isnull(d.Pu_BaseUnitNum,0))) as Gi_Quantity   '+
			' 					              from Buy_GetProductItem     c left join ERP_Base_MaterielUnit d on c.Gi_Pu_Guid =d.Pu_Guid  '+
			' 					              where  c.Gi_ComeFromItemCode =  a.Oi_Guid                                              '+
			' 					              group by c.Gi_Guid                                                                     '+
			' 					            ) aa                                                                                     '+
			' 				           )                                                                                           '+
			' 			             ,0) as 未导入数量,b.Pu_BaseUnitNum                                                          '+
      '  from Buy_OrderItem a left join ERP_Base_MaterielUnit b on a.Oi_Pu_Guid=b.Pu_Guid                                   '+
      '  where  a.Oi_Or_Guid = :OR_Guid                                                                                '+
      '  ) a  where   ( :IsViewIntroduction = 1  or  未导入数量>0)                                             ' 
    set @MainTableGuid= 'Or_Guid'
  end
  else if @BillType ='btBuy_GetProduct' 
  begin
    set @FormCaption='导出采购收货到采购退货'
    set @MainSql=
      ' select  * from (                                                                                               '+
      ' select a.*,                                                                                                    '+
		  '            isnull((                                                                                            '+
			' 					          select sum(Gi_Quantity)                                                                    '+
			' 					          from                                                                                       '+
			' 					            ( select sum(isnull(c.Gi_Quantity,0))*(max(isnull(d.Pu_BaseUnitNum,0))) as Gi_Quantity   '+
			' 					              from Buy_GetProductItem c left join ERP_Base_MaterielUnit d on c.Gi_Pu_Guid     =d.Pu_Guid  '+
			' 					              where  c.Gi_Gp_Guid = a.Gp_Guid                                                        '+
			' 					              group by c.Gi_Guid                                                                     '+
			' 					            ) aa                                                                                     '+
			' 				           )                                                                                           '+
			' 			             ,0) as  总计数量,                                                                           '+
      '                                                                                                                '+
		  '            isnull((                                                                                            '+
			' 					          select sum(Ri_Quantity)                                                                    '+
			' 					          from                                                                                       '+
			' 					            ( select sum(isnull(c.Ri_Quantity,0))*(max(isnull(d.Pu_BaseUnitNum,0))) as Ri_Quantity   '+
			' 					              from Buy_ReturnProductItem  c left join ERP_Base_MaterielUnit d on c.Ri_Pu_Guid =d.Pu_Guid  '+
			' 					              where  c.Ri_ComeFromCode = a.Gp_Guid                                                   '+
			' 					              group by c.Ri_Guid                                                                     '+
			' 					            ) aa                                                                                     '+
			' 				           )                                                                                           '+
			' 			             ,0) as 已导入数量                                                                           '+
      '  from Buy_GetProduct a                                                                                         '+
      '  where convert(varchar(10),a.Gp_Datetime,121)>= :BegDate and convert(varchar(10),a.Gp_Datetime,121)<= :EndDate and a.Gp_State =''查询''                          '+
      '        and ( :IsBillCode =0  or a.Gp_Code= :BillCode )                                                        '+
      '  ) a  where   ( :IsViewIntroduction = 1  or  总计数量-已导入数量>0)                                             '+
      '  order by a.Gp_Datetime                                                                                        '
    set @DetailSql=
      ' select  * from (                                                                                               '+
      '  select a.*,                                                                                                   '+
      '            isnull(a.Gi_Quantity,0)*isnull(b.Pu_BaseUnitNum,0)-                                                 '+
		  '            isnull((                                                                                            '+
			' 					          select sum(Ri_Quantity)                                                                    '+
			' 					          from                                                                                       '+
			' 					            ( select sum(isnull(c.Ri_Quantity,0))*(max(isnull(d.Pu_BaseUnitNum,0))) as Ri_Quantity   '+
			' 					              from Buy_ReturnProductItem c left join ERP_Base_MaterielUnit d on c.Ri_Pu_Guid=d.Pu_Guid    '+
			' 					              where  c.Ri_ComeFromItemCode =  a.Gi_Guid                                              '+
			' 					              group by c.Ri_Guid                                                                     '+
			' 					            ) aa                                                                                     '+
			' 				           )                                                                                           '+
			' 			             ,0) as 未导入数量,b.Pu_BaseUnitNum                                                          '+
      '  from Buy_GetProductItem a left join ERP_Base_MaterielUnit b on a.Gi_Pu_Guid=b.Pu_Guid                              '+
      '  where  a.Gi_Gp_Guid = :Gp_Guid                                                                                '+
      '  ) a  where   ( :IsViewIntroduction = 1  or  未导入数量>0)                                             ' 
    set @MainTableGuid= 'Gp_Guid'
  end
  else if @BillType ='btProduce_Plan' 
  begin
    set @FormCaption='导出生产计划到生产订单'
    set @MainSql=
      ' select  * from (                                                                                               '+
      ' select a.*,                                                                                                    '+
		  '            isnull((                                                                                            '+
			' 					          select sum(Pi_Quantity)                                                                    '+
			' 					          from                                                                                       '+
			' 					            ( select sum(isnull(c.Pi_Quantity,0))*(max(isnull(d.Pu_BaseUnitNum,0))) as Pi_Quantity   '+
			' 					              from Produce_PlanItem c left join ERP_Base_MaterielUnit d on c.Pi_Pu_Guid     =d.Pu_Guid    '+
			' 					              where  c.Pi_Pl_Guid = a.Pl_Guid                                                        '+
			' 					              group by c.Pi_Guid                                                                     '+
			' 					            ) aa                                                                                     '+
			' 				           )                                                                                           '+
			' 			             ,0) as  总计数量,                                                                           '+
      '                                                                                                                '+
		  '            isnull((                                                                                            '+
			' 					          select sum(Oi_Quantity)                                                                    '+
			' 					          from                                                                                       '+
			' 					            ( select sum(isnull(c.Oi_Quantity,0))*(max(isnull(d.Pu_BaseUnitNum,0))) as Oi_Quantity   '+
			' 					              from Produce_OrderItem  c left join ERP_Base_MaterielUnit d on c.Oi_Pu_Guid     =d.Pu_Guid  '+
			' 					              where  c.Oi_ComeFromCode = a.Pl_Guid                                                   '+
			' 					              group by c.Oi_Guid                                                                     '+
			' 					            ) aa                                                                                     '+
			' 				           )                                                                                           '+
			' 			             ,0) as 已导入数量                                                                           '+
      '  from Produce_Plan a                                                                                           '+
      '  where convert(varchar(10),a.Pl_Datetime,121)>= :BegDate and convert(varchar(10),a.Pl_Datetime,121)<= :EndDate and a.Pl_State =''查询''                          '+
      '        and ( :IsBillCode =0  or a.Pl_Code= :BillCode )                                                        '+
      '  ) a  where   ( :IsViewIntroduction = 1  or  总计数量-已导入数量>0)                                             '+
      '  order by a.Pl_Datetime                                                                                        '
    set @DetailSql=
      ' select  * from (                                                                                               '+
      '  select a.*,                                                                                                   '+
      '            isnull(a.Pi_Quantity,0)*isnull(b.Pu_BaseUnitNum,0)-                                                 '+
		  '            isnull((                                                                                            '+
			' 					          select sum(Oi_Quantity)                                                                    '+
			' 					          from                                                                                       '+
			' 					            ( select sum(isnull(c.Oi_Quantity,0))*(max(isnull(d.Pu_BaseUnitNum,0))) as Oi_Quantity   '+
			' 					              from Produce_OrderItem      c left join ERP_Base_MaterielUnit d on c.Oi_Pu_Guid =d.Pu_Guid  '+
			' 					              where  c.Oi_ComeFromItemCode =  a.Pi_Guid                                              '+
			' 					              group by c.Oi_Guid                                                                     '+
			' 					            ) aa                                                                                     '+
			' 				           )                                                                                           '+
			' 			             ,0) as 未导入数量,b.Pu_BaseUnitNum                                                          '+
      '  from Produce_PlanItem a left join ERP_Base_MaterielUnit b on a.Pi_Pu_Guid=b.Pu_Guid                                '+
      '  where  a.Pi_Pl_Guid = :Pl_Guid                                                                                '+
      '  ) a  where   ( :IsViewIntroduction = 1  or  未导入数量>0)                                             ' 
    set @MainTableGuid= 'Pl_Guid'
  end



go
 
--销售订单处理过程查询
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_ShowSaleOrderProcess]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Proc_ShowSaleOrderProcess]
go
CREATE PROCEDURE [dbo].[Proc_ShowSaleOrderProcess]
(
@BegDate varchar(100),@EndDate varchar(100),@Cu_Guid varchar(100)='',@Or_Guid varchar(100)=''  
)  
AS
--declare @BegDate varchar(100)
--declare @EndDate varchar(100)
--declare @Cu_Guid varchar(100)
--declare @Or_Guid varchar(100)  
--set @BegDate='2005-2-1'
--set @EndDate='2005-12-11'
--set @Cu_Guid='D8ECB4CE-E897-400A-B170-9253FBF3B7F4'
--set @Or_Guid=''
 
select aa.订单编号,aa.操作类型,aa.相关单据,aa.操作时间,aa.数据操作人+bb.Cl_Name as 数据操作人  
from 
(
select newid() as Pubid,a.Or_Code as 订单编号,1 as 编号,'订单签定' as 操作类型, '订单编号:'+ a.Or_Code  as 相关单据,'销售人员:' as 数据操作人,Or_OperatorGuid  as OperatorGuid,'签定时间:'+convert(varchar(10),Or_Datetime,121)  as 操作时间
from  Sale_Order a 
where a.Or_State in ('查询','正在审批') and convert(varchar(10),a.Or_Datetime,121)>=@BegDate and convert(varchar(10),a.Or_Datetime,121)<=@EndDate and
      (@Cu_Guid = '' or a.Or_Cu_Guid=@Cu_Guid ) and (@Or_Guid = '' or a.Or_Guid=@Or_Guid)
 
union
select newid() as Pubid,a.Or_Code as 订单编号,2 as 编号,'订单收款' as 操作类型, '发票编号:'+ b.Og_InvoiceCode  as 相关单据,'收款人员:'  as 数据操作人,max(b.Og_OperatorGuid)  as OperatorGuid,'收款时间:'+convert(varchar(10),max(b.Og_Datetime),121) as 操作时间 
from  Sale_Order a left join Sale_OrderGetMoney b on b.Og_Or_Guid=a.Or_Guid 
where a.Or_State in ('查询','正在审批') and convert(varchar(10),a.Or_Datetime,121)>=@BegDate and convert(varchar(10),a.Or_Datetime,121)<=@EndDate and
      (@Cu_Guid = '' or a.Or_Cu_Guid=@Cu_Guid ) and (@Or_Guid = '' or a.Or_Guid=@Or_Guid)
group by a.Or_Code,b.Og_InvoiceCode 
union
select newid() as Pubid,a.Or_Code as 订单编号,3 as 编号,'销售出库' as 操作类型, '出库单编号:'+ c.Os_Code  as 相关单据,'出库人员:'  as 数据操作人,max(c.Os_OperatorGuid)  as OperatorGuid,'出库时间:'+convert(varchar(10),max(c.Os_Datetime),121) as 操作时间
from  Sale_Order a left join Sale_OutStorageItem b  on a.Or_Guid =b.Oi_ComeFromCode left join Sale_OutStorage c on b.Oi_Os_Guid=c.Os_Guid
where a.Or_State in ('查询','正在审批') and convert(varchar(10),a.Or_Datetime,121)>=@BegDate and convert(varchar(10),a.Or_Datetime,121)<=@EndDate and
      (@Cu_Guid = '' or a.Or_Cu_Guid=@Cu_Guid ) and (@Or_Guid = '' or a.Or_Guid=@Or_Guid)
group by a.Or_Code,c.Os_Code 
union
select newid() as Pubid,a.Or_Code as 订单编号,4 as 编号,'销售退货' as 操作类型, '退货单编号:'+ d.Rp_Code  as 相关单据,'退货受理人员:'  as 数据操作人,max(d.Rp_OperatorGuid)  as OperatorGuid,'退货时间:'+convert(varchar(10),max(d.Rp_Datetime),121) as 操作时间
from  Sale_Order a left join Sale_OutStorageItem b  on a.Or_Guid =b.Oi_ComeFromCode left join Sale_ReturnProductItem c on b.Oi_Os_Guid=c.Ri_ComeFromCode left join Sale_ReturnProduct d on d.Rp_Guid=c.Ri_Rp_Guid
where a.Or_State in ('查询','正在审批') and convert(varchar(10),a.Or_Datetime,121)>=@BegDate and convert(varchar(10),a.Or_Datetime,121)<=@EndDate and
      (@Cu_Guid = '' or a.Or_Cu_Guid=@Cu_Guid ) and (@Or_Guid = '' or a.Or_Guid=@Or_Guid)
group by a.Or_Code,d.Rp_Code 
union
select newid() as Pubid,a.Or_Code as 订单编号,5 as 编号,'销售退款' as 操作类型, '发票编号:'+ e.Rm_InvoiceCode  as 相关单据,'退款人员:'  as 数据操作人,max(e.Rm_OperatorGuid)  as OperatorGuid,'退款时间:'+convert(varchar(10),max(e.Rm_Datetime),121) as 操作时间
from  Sale_Order a left join Sale_OutStorageItem b  on a.Or_Guid =b.Oi_ComeFromCode left join Sale_ReturnProductItem c on b.Oi_Os_Guid=c.Ri_ComeFromCode left join Sale_ReturnProduct d on d.Rp_Guid=c.Ri_Rp_Guid left join Sale_ReturnProductMoney e on e.Rm_Rp_Guid=d.Rp_Guid
where a.Or_State in ('查询','正在审批') and convert(varchar(10),a.Or_Datetime,121)>=@BegDate and convert(varchar(10),a.Or_Datetime,121)<=@EndDate and
      (@Cu_Guid = '' or a.Or_Cu_Guid=@Cu_Guid ) and (@Or_Guid = '' or a.Or_Guid=@Or_Guid)
group by a.Or_Code,e.Rm_InvoiceCode 
) aa 
left join Base_CustLinkman bb on bb.Cl_Guid=aa.OperatorGuid and bb.Cl_Cu_Guid in (select Cu_Guid  from Base_Cust where Cu_Type in ('本部公司','本部公司分部'))   
order by 订单编号,编号
go
  
 
--采购订单处理过程查询
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_ShowBuyOrderProcess]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Proc_ShowBuyOrderProcess]
go
CREATE PROCEDURE [dbo].[Proc_ShowBuyOrderProcess]
(
@BegDate varchar(100),@EndDate varchar(100),@Su_Guid varchar(100)='',@Or_Guid varchar(100)=''  
)  
AS
--declare @BegDate varchar(100)
--declare @EndDate varchar(100)
--declare @Su_Guid varchar(100)
--declare @Or_Guid varchar(100)  
--set @BegDate='2005-2-1'
--set @EndDate='2005-12-11'
--set @Su_Guid='D8ECB4CE-E897-400A-B170-9253FBF3B7F4'
--set @Or_Guid=''
 
select aa.订单编号,aa.操作类型,aa.相关单据,aa.操作时间,aa.数据操作人+bb.Cl_Name as 数据操作人  
from 
(

select newid() as Pubid,a.Or_Code as 订单编号,1 as 编号,'订单签定' as 操作类型, '订单编号:'+isnull(a.Or_Code,'') as 相关单据,'采购人员:' as 数据操作人,Or_OperatorGuid  as OperatorGuid,'签定时间:'+convert(varchar(10),Or_Datetime,121)  as 操作时间
from  Buy_Order a 
where a.Or_State in ('查询','正在审批') and convert(varchar(10),a.Or_Datetime,121)>=@BegDate and convert(varchar(10),a.Or_Datetime,121)<=@EndDate and
      (@Su_Guid = '' or a.Or_Su_Guid=@Su_Guid ) and (@Or_Guid = '' or a.Or_Guid=@Or_Guid)
 
union
select newid() as Pubid,a.Or_Code as 订单编号,2 as 编号,'订单收款' as 操作类型, '发票编号:'+ b.Op_InvoiceCode  as 相关单据,'收款人员:'  as 数据操作人,max(b.Op_OperatorGuid)  as OperatorGuid,'收款时间:'+convert(varchar(10),max(b.Op_Datetime),121) as 操作时间
from  Buy_Order a left join Buy_OrderPayMoney b on b.Op_Or_Guid=a.Or_Guid 
where a.Or_State in ('查询','正在审批') and convert(varchar(10),a.Or_Datetime,121)>=@BegDate and convert(varchar(10),a.Or_Datetime,121)<=@EndDate and
      (@Su_Guid = '' or a.Or_Su_Guid=@Su_Guid ) and (@Or_Guid = '' or a.Or_Guid=@Or_Guid)
group by a.Or_Code,b.Op_InvoiceCode
union
select newid() as Pubid,a.Or_Code as 订单编号,3 as 编号,'采购收货' as 操作类型, '收货单编号:'+ c.Gp_Code as 相关单据,'收货人员:'  as 数据操作人,max(c.Gp_OperatorGuid)  as OperatorGuid,'收货时间:'+convert(varchar(10),max(c.Gp_Datetime),121) as 操作时间
from  Buy_Order a left join Buy_GetProductItem b  on a.Or_Guid =b.Gi_ComeFromCode left join Buy_GetProduct c on b.Gi_Gp_Guid=c.Gp_Guid
where a.Or_State in ('查询','正在审批') and convert(varchar(10),a.Or_Datetime,121)>=@BegDate and convert(varchar(10),a.Or_Datetime,121)<=@EndDate and
      (@Su_Guid = '' or a.Or_Su_Guid=@Su_Guid ) and (@Or_Guid = '' or a.Or_Guid=@Or_Guid)
group by a.Or_Code,c.Gp_Code
union
select newid() as Pubid,a.Or_Code as 订单编号,4 as 编号,'采购退货' as 操作类型, '退货单编号:'+ d.Rp_Code  as 相关单据,'退货受理人员:'  as 数据操作人,max(d.Rp_OperatorGuid)  as OperatorGuid,'退货时间:'+convert(varchar(10),max(d.Rp_Datetime),121) as 操作时间
from  Buy_Order a left join Buy_GetProductItem b  on a.Or_Guid =b.Gi_ComeFromCode left join Buy_ReturnProductItem c on b.Gi_Gp_Guid=c.Ri_ComeFromCode left join Buy_ReturnProduct d on d.Rp_Guid=c.Ri_Rp_Guid
where a.Or_State in ('查询','正在审批') and convert(varchar(10),a.Or_Datetime,121)>=@BegDate and convert(varchar(10),a.Or_Datetime,121)<=@EndDate and
      (@Su_Guid = '' or a.Or_Su_Guid=@Su_Guid ) and (@Or_Guid = '' or a.Or_Guid=@Or_Guid)
group by a.Or_Code,d.Rp_Code
union
select newid() as Pubid,a.Or_Code as 订单编号,5 as 编号,'采购退款' as 操作类型, '发票编号:'+ e.Rm_InvoiceCode  as 相关单据,'退款人员:'  as 数据操作人,max(e.Rm_OperatorGuid)  as OperatorGuid,'退款时间:'+convert(varchar(10),max(e.Rm_Datetime),121) as 操作时间
from  Buy_Order a left join Buy_GetProductItem b  on a.Or_Guid =b.Gi_ComeFromCode left join Buy_ReturnProductItem c on b.Gi_Gp_Guid=c.Ri_ComeFromCode left join Buy_ReturnProduct d on d.Rp_Guid=c.Ri_Rp_Guid left join Buy_ReturnProductMoney e on e.Rm_Rp_Guid=d.Rp_Guid
where a.Or_State in ('查询','正在审批') and convert(varchar(10),a.Or_Datetime,121)>=@BegDate and convert(varchar(10),a.Or_Datetime,121)<=@EndDate and
      (@Su_Guid = '' or a.Or_Su_Guid=@Su_Guid ) and (@Or_Guid = '' or a.Or_Guid=@Or_Guid)
group by a.Or_Code,e.Rm_InvoiceCode 
) aa
left join Base_CustLinkman bb on bb.Cl_Guid=aa.OperatorGuid and bb.Cl_Cu_Guid in (select Cu_Guid  from Base_Cust where Cu_Type in ('本部公司','本部公司分部'))   
order by 订单编号,编号
 go
 


--exec Proc_InitStorage '','51BDAD19-2151-47E4-A272-F58ED10F458F' 

--select * from table1
--select * from table2
--drop table table2
--初始化库存 
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_InitStorage]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Proc_InitStorage]
go
CREATE PROCEDURE [dbo].[Proc_InitStorage]
(
@InitSt_Guid varchar(100)='',
@InitPr_Guid varchar(100)='' 

)  
  
AS

declare @ProductList table(Pr_Ps_Guid varchar(100),Pr_Guid varchar(100)  )
declare @StorageList table(St_Guid varchar(100)  )

update ERP_Base_Materiel set Pr_BuyOrderWillIn=0,Pr_SaleOrderWillOut=0,Pr_ProduceWillIn=0,Pr_ProduceWillOut=0 
where (@InitPr_Guid='' or @InitPr_Guid= Pr_Guid)  

update Storage_StorageProduct set Sp_Buy_Get=0,Sp_Buy_Return=0,Sp_Sale_Out=0,Sp_Sale_Return=0,Sp_Produce_In=0,Sp_Produce_Out=0,
                                    Sp_Storage_OtherIn=0,Sp_Storage_OtherOut=0,Sp_Service_Out=0,Sp_Storage_MoveOut=0,Sp_Storage_MoveIn=0 
where (@InitSt_Guid='' or @InitSt_Guid= Sp_St_Guid) and (@InitPr_Guid='' or @InitPr_Guid= Sp_Pr_Guid)  
  

declare @tempstr1 Nvarchar(4000)
declare @tempValue1 decimal(18,3) 
declare @Pr_Ps_Guid varchar(100),@Pr_Guid varchar(100),@St_Guid  varchar(100) 

insert into @ProductList(Pr_Ps_Guid,Pr_Guid)
select Pr_Ps_Guid ,Pr_Guid 
from ERP_Base_Materiel  
where Pr_Guid = @InitPr_Guid  or @InitPr_Guid=''  --'51BDAD19-2151-47E4-A272-F58ED10F458F' 

declare CurProductCursor cursor  for 
select Pr_Ps_Guid ,Pr_Guid  from @ProductList


insert into @StorageList(St_Guid)
select  St_Guid 
from  Base_Storage   
where (@InitSt_Guid='' or @InitSt_Guid= St_Guid)  

declare CurStorageCursor cursor  for 
select St_Guid  from @StorageList

--产品表中的待出入的初始化
open  CurProductCursor  
fetch  next  from  CurProductCursor  into  @Pr_Ps_Guid,@Pr_Guid  
while  @@fetch_status  =  0  
begin 
  --update table1 set a='aa'
  --采购订单
  set @tempstr1=
'              select @tempvalue1=isnull(sum(isnull(tempSum,0)),0) from '+CHAR(10)+ 
'								( '+CHAR(10)+ 
'								select sum(isnull(b.Oi_Quantity,0)*isnull((d.Pu_BaseUnitNum),0))- '+CHAR(10)+ 
'								       isnull((select  sum(tempSum) from ( select sum(c.Gi_Quantity)*max(isnull(e.Pu_BaseUnitNum,0)) as tempSum  from Buy_GetProductItem c  left join ERP_Base_MaterielUnit e on c.Gi_Pr_Guid=e.Pu_Pr_Guid and c.Gi_Pu_Guid=e.Pu_Guid  left join Buy_GetProduct f on c.Gi_Gp_Guid=f.Gp_Guid where f.Gp_State=''查询'' and  (b.Oi_Guid)=c.Gi_ComeFromItemCode group by c.Gi_Guid ) as aa ),0) as tempSum  '+CHAR(10)+ 
'								from  Buy_Order a left join Buy_OrderItem b on a.Or_Guid=b.Oi_Or_Guid and  '+CHAR(10)+ 
'								      b.Oi_Pr_Guid=@temp_Pr_Guid and a.Or_State=''查询'''+CHAR(10)+ 
'								      left join ERP_Base_MaterielUnit d on b.Oi_Pr_Guid=d.Pu_Pr_Guid and b.Oi_Pu_Guid=d.Pu_Guid '+CHAR(10)+ 
'								group by b.Oi_Pr_Guid,Oi_Guid  '+CHAR(10)+ 
'								) aa ' 

  if @tempstr1<>''
  begin
    exec  sp_executesql @tempstr1 ,N' @tempvalue1 decimal(18,3)  OUTPUT  ,@temp_Pr_Guid varchar(100),@temp_St_Guid varchar(100)',@tempvalue1  OUTPUT ,@temp_Pr_Guid =@Pr_Guid , @temp_St_Guid =@St_Guid  
    set @tempstr1=' update  ERP_Base_Materiel set Pr_BuyOrderWillIn=isnull('+str(@tempvalue1,20,4)+',0) where Pr_Guid ='+''''+@Pr_Guid+''''
    exec(@tempstr1 ) 
  end

--采购收货
  set @tempstr1=
'              select @tempvalue1=isnull(sum(isnull(tempSum,0)),0) from '+CHAR(10)+ 
'								( '+CHAR(10)+ 
'								select sum(isnull(b.Oi_Quantity,0)*isnull((d.Pu_BaseUnitNum),0))- '+CHAR(10)+ 
'								       isnull((select  sum(tempSum) from ( select sum(c.Gi_Quantity)*max(isnull(e.Pu_BaseUnitNum,0)) as tempSum  from Buy_GetProductItem c  left join ERP_Base_MaterielUnit e on c.Gi_Pr_Guid=e.Pu_Pr_Guid and c.Gi_Pu_Guid=e.Pu_Guid  left join Buy_GetProduct f on c.Gi_Gp_Guid=f.Gp_Guid where f.Gp_State=''查询'' and  (b.Oi_Guid)=c.Gi_ComeFromItemCode group by c.Gi_Guid ) as aa ),0) as tempSum  '+CHAR(10)+ 
'								from  Buy_Order a left join Buy_OrderItem b on a.Or_Guid=b.Oi_Or_Guid and  '+CHAR(10)+ 
'								      b.Oi_Pr_Guid=@temp_Pr_Guid and a.Or_State=''查询''    '+CHAR(10)+ 
'								      left join ERP_Base_MaterielUnit d on b.Oi_Pr_Guid=d.Pu_Pr_Guid and b.Oi_Pu_Guid=d.Pu_Guid '+CHAR(10)+ 
'								group by b.Oi_Pr_Guid,Oi_Guid  '+CHAR(10)+ 
'								) aa ' 

  if @tempstr1<>''
  begin
    exec  sp_executesql @tempstr1 ,N' @tempvalue1 decimal(18,3)  OUTPUT  ,@temp_Pr_Guid varchar(100),@temp_St_Guid varchar(100)',@tempvalue1  OUTPUT ,@temp_Pr_Guid =@Pr_Guid , @temp_St_Guid =@St_Guid  
    set @tempstr1=' update  ERP_Base_Materiel set Pr_BuyOrderWillIn=isnull('+str(@tempvalue1,20,4)+',0) where Pr_Guid ='+''''+@Pr_Guid+''''
    exec(@tempstr1 ) 
	end

  --销售订单
  set @tempstr1=
'								select @tempvalue1=isnull(sum(isnull(tempSum,0)),0) from '+CHAR(10)+ 
'								( '+CHAR(10)+ 
'								select sum(isnull(b.Oi_Quantity,0)*isnull((d.Pu_BaseUnitNum),0))- '+CHAR(10)+
'								       isnull((select  sum(tempSum) from ( select  sum(c.Oi_Quantity)*max(isnull(e.Pu_BaseUnitNum,0)) as tempSum  from Sale_OutStorageItem c  left join ERP_Base_MaterielUnit e on c.Oi_Pr_Guid=e.Pu_Pr_Guid and c.Oi_Pu_Guid=e.Pu_Guid left join Sale_OutStorage f on c.Oi_Os_Guid=f.Os_Guid where f.Os_State=''查询'' and  (b.Oi_Guid)=c.Oi_ComeFromItemCode group by c.Oi_Guid ) as aa),0) as tempSum  '+CHAR(10)+
'								from  Sale_Order a left join Sale_OrderItem b on a.Or_Guid=b.Oi_Or_Guid and  '+CHAR(10)+
'								      b.Oi_Pr_Guid=@temp_Pr_Guid and a.Or_State=''查询''   '+CHAR(10)+
'								      left join ERP_Base_MaterielUnit d on b.Oi_Pr_Guid=d.Pu_Pr_Guid and b.Oi_Pu_Guid=d.Pu_Guid '+CHAR(10)+
'								group by b.Oi_Pr_Guid,Oi_Guid  '+CHAR(10)+
'								) aa ' 

  if @tempstr1<>''
  begin
    exec  sp_executesql @tempstr1 ,N' @tempvalue1 decimal(18,3)  OUTPUT  ,@temp_Pr_Guid varchar(100),@temp_St_Guid varchar(100)',@tempvalue1  OUTPUT ,@temp_Pr_Guid =@Pr_Guid , @temp_St_Guid =@St_Guid  
    set @tempstr1=' update  ERP_Base_Materiel set Pr_SaleOrderWillOut=isnull('+str(@tempvalue1,20,4)+',0) where Pr_Guid ='+''''+@Pr_Guid+''''
    exec(@tempstr1 ) 
	end

--销售出库
  set @tempstr1=
'								select @tempvalue1=isnull(sum(isnull(tempSum,0)),0) from '+CHAR(10)+ 
'								( '+CHAR(10)+ 
'								select sum(isnull(b.Oi_Quantity,0)*isnull((d.Pu_BaseUnitNum),0))- '+CHAR(10)+
'								       isnull((select  sum(tempSum) from ( select  sum(c.Oi_Quantity)*max(isnull(e.Pu_BaseUnitNum,0)) as tempSum  from Sale_OutStorageItem c  left join ERP_Base_MaterielUnit e on c.Oi_Pr_Guid=e.Pu_Pr_Guid and c.Oi_Pu_Guid=e.Pu_Guid left join Sale_OutStorage f on c.Oi_Os_Guid=f.Os_Guid where f.Os_State=''查询'' and  (b.Oi_Guid)=c.Oi_ComeFromItemCode group by c.Oi_Guid ) as aa),0) as tempSum  '+CHAR(10)+
'								from  Sale_Order a left join Sale_OrderItem b on a.Or_Guid=b.Oi_Or_Guid and  '+CHAR(10)+
'								      b.Oi_Pr_Guid=@temp_Pr_Guid and a.Or_State=''查询''   '+CHAR(10)+
'								      left join ERP_Base_MaterielUnit d on b.Oi_Pr_Guid=d.Pu_Pr_Guid and b.Oi_Pu_Guid=d.Pu_Guid '+CHAR(10)+
'								group by b.Oi_Pr_Guid,Oi_Guid  '+CHAR(10)+
'								) aa ' 

  if @tempstr1<>''
  begin
    exec  sp_executesql @tempstr1 ,N' @tempvalue1 decimal(18,3)  OUTPUT  ,@temp_Pr_Guid varchar(100),@temp_St_Guid varchar(100)',@tempvalue1  OUTPUT ,@temp_Pr_Guid =@Pr_Guid , @temp_St_Guid =@St_Guid  
    set @tempstr1=' update  ERP_Base_Materiel set Pr_SaleOrderWillOut=isnull('+str(@tempvalue1,20,4)+',0) where Pr_Guid ='+''''+@Pr_Guid+''''
    exec(@tempstr1 ) 
	end
 
--生产任务待入库
  set @tempstr1=
'               select @tempvalue1=isnull(sum(isnull(b.Oi_Quantity,0)*isnull(d.Pu_BaseUnitNum,0)),0) '+CHAR(10)+
'               from  Produce_Order a left join Produce_OrderItem b on a.Or_Guid=b.Oi_Or_Guid and  '+CHAR(10)+
'                     b.Oi_Pr_Guid=@temp_Pr_Guid and a.Or_State=''查询'' and a.Or_FinishingState=''录入''    '+CHAR(10)+
'                     left join ERP_Base_MaterielUnit d on b.Oi_Pr_Guid=d.Pu_Pr_Guid and b.Oi_Pu_Guid=d.Pu_Guid '  

  if @tempstr1<>''
  begin
    exec  sp_executesql @tempstr1 ,N' @tempvalue1 decimal(18,3)  OUTPUT  ,@temp_Pr_Guid varchar(100),@temp_St_Guid varchar(100)',@tempvalue1  OUTPUT ,@temp_Pr_Guid =@Pr_Guid , @temp_St_Guid =@St_Guid  
    set @tempstr1=' update  ERP_Base_Materiel set Pr_ProduceWillIn=isnull('+str(@tempvalue1,20,4)+',0) where Pr_Guid ='+''''+@Pr_Guid+''''
    exec(@tempstr1 ) 
	end

--生产原料待出库
  set @tempstr1=
'               select @tempvalue1=isnull(sum(isnull(b.Ob_Quantity,0)*isnull(d.Pu_BaseUnitNum,0)),0) '+CHAR(10)+
'               from  Produce_Order a left join Produce_OrderBom b on a.Or_Guid=b.Ob_Or_Guid and  '+CHAR(10)+
'                     b.Ob_Pr_Guid=@temp_Pr_Guid and   a.Or_BomState=''录入'' and Or_BomWillOutState=''查询''   '+CHAR(10)+
'                     left join ERP_Base_MaterielUnit d on b.Ob_Pr_Guid=d.Pu_Pr_Guid and b.Ob_Pu_Guid=d.Pu_Guid '  

  if @tempstr1<>''
  begin
    exec  sp_executesql @tempstr1 ,N' @tempvalue1 decimal(18,3)  OUTPUT  ,@temp_Pr_Guid varchar(100),@temp_St_Guid varchar(100)',@tempvalue1  OUTPUT ,@temp_Pr_Guid =@Pr_Guid , @temp_St_Guid =@St_Guid  
    set @tempstr1=' update  ERP_Base_Materiel set Pr_ProduceWillOut=isnull('+str(@tempvalue1,20,4)+',0) where Pr_Guid ='+''''+@Pr_Guid+''''
    exec(@tempstr1 ) 
	end
 
  fetch  next  from  CurProductCursor  into  @Pr_Ps_Guid,@Pr_Guid  
end

close  CurProductCursor
open  CurProductCursor  
fetch  next  from  CurProductCursor  into  @Pr_Ps_Guid,@Pr_Guid  
while  @@fetch_status  =  0  
begin 
	open  CurStorageCursor  
	fetch  next  from  CurStorageCursor  into  @St_Guid
	while  @@fetch_status  =  0  
	begin 
	  --采购收货 
	  set @tempstr1=
'               select @tempvalue1=isnull(sum(isnull(b.Gi_Quantity,0)*isnull(d.Pu_BaseUnitNum,0)),0) '+CHAR(10)+
'               from  Buy_GetProduct a left join Buy_GetProductItem b on a.Gp_Guid=b.Gi_Gp_Guid and  '+CHAR(10)+
'                     b.Gi_Pr_Guid=@temp_Pr_Guid and b.Gi_St_Guid=@temp_St_Guid and a.Gp_State=''查询''    '+CHAR(10)+
'                     left join ERP_Base_MaterielUnit d on b.Gi_Pr_Guid=d.Pu_Pr_Guid and b.Gi_Pu_Guid=d.Pu_Guid ' 
	  if @tempstr1<>''
	  begin
	    exec  sp_executesql @tempstr1 ,N' @tempvalue1 decimal(18,3)  OUTPUT  ,@temp_Pr_Guid varchar(100),@temp_St_Guid varchar(100)',@tempvalue1  OUTPUT ,@temp_Pr_Guid =@Pr_Guid , @temp_St_Guid =@St_Guid  
	    if isnull(@tempvalue1,'0')<>'0'
      begin 
		    if not EXISTS(select * from Storage_StorageProduct where Sp_St_Guid=@St_Guid and Sp_Pr_Guid = @Pr_Guid ) 
		    begin  
				  insert into Storage_StorageProduct(Sp_St_Guid,Sp_Ps_Guid,Sp_Pr_Guid,pubid) values(@St_Guid,@Pr_Ps_Guid,@Pr_Guid ,newid()) 
		    end  
	            set @tempstr1=' update  Storage_StorageProduct set  Sp_Buy_Get=isnull('+str(@tempvalue1,20,4)+
		                ',0) where Sp_St_Guid ='+''''+ @St_Guid+''''  +' and  Sp_Pr_Guid  ='+''''+@Pr_Guid+''''
		    exec(@tempstr1 ) 
		  end
	  end

--采购退货
	  set @tempstr1=
'               select @tempvalue1=isnull(sum(isnull(b.Ri_Quantity,0)*isnull(d.Pu_BaseUnitNum,0)),0) '+CHAR(10)+
'               from  Buy_ReturnProduct a left join Buy_ReturnProductItem b on a.Rp_Guid=b.Ri_Rp_Guid and  '+CHAR(10)+
'                     b.Ri_Pr_Guid=@temp_Pr_Guid and b.Ri_St_Guid=@temp_St_Guid and a.Rp_State=''查询''    '+CHAR(10)+
'                     left join ERP_Base_MaterielUnit d on b.Ri_Pr_Guid=d.Pu_Pr_Guid and b.Ri_Pu_Guid=d.Pu_Guid '
	  if @tempstr1<>''
	  begin

	    exec  sp_executesql @tempstr1 ,N' @tempvalue1 decimal(18,3)  OUTPUT  ,@temp_Pr_Guid varchar(100),@temp_St_Guid varchar(100)',@tempvalue1  OUTPUT ,@temp_Pr_Guid =@Pr_Guid , @temp_St_Guid =@St_Guid  
	    if isnull(@tempvalue1,'0')<>'0'
      begin 
		    if not EXISTS(select * from Storage_StorageProduct where Sp_St_Guid=@St_Guid and Sp_Pr_Guid = @Pr_Guid ) 
				begin  
				  insert into Storage_StorageProduct(Sp_St_Guid,Sp_Ps_Guid,Sp_Pr_Guid,pubid) values(@St_Guid,@Pr_Ps_Guid,@Pr_Guid ,newid()) 
			  end  
	      set @tempstr1=' update  Storage_StorageProduct set  Sp_Buy_Return=isnull('+str(@tempvalue1,20,4)+
		         ',0) where Sp_St_Guid ='+''''+ @St_Guid+''''  +' and  Sp_Pr_Guid  ='+''''+@Pr_Guid+''''
		    exec(@tempstr1 ) 
		  end
		end

--销售出货
	  set @tempstr1=
'               select @tempvalue1=isnull(sum(isnull(b.Oi_Quantity,0)*isnull(d.Pu_BaseUnitNum,0)),0) '+CHAR(10)+
'               from  Sale_OutStorage a left join Sale_OutStorageItem b on a.Os_Guid=b.Oi_Os_Guid and  '+CHAR(10)+
'                     b.Oi_Pr_Guid=@temp_Pr_Guid and b.Oi_St_Guid=@temp_St_Guid and a.Os_State=''查询''    '+CHAR(10)+
'                     left join ERP_Base_MaterielUnit d on b.Oi_Pr_Guid=d.Pu_Pr_Guid and b.Oi_Pu_Guid=d.Pu_Guid '
	  if @tempstr1<>''
	  begin

	    exec  sp_executesql @tempstr1 ,N' @tempvalue1 decimal(18,3)  OUTPUT  ,@temp_Pr_Guid varchar(100),@temp_St_Guid varchar(100)',@tempvalue1  OUTPUT ,@temp_Pr_Guid =@Pr_Guid , @temp_St_Guid =@St_Guid  
	    if isnull(@tempvalue1,'0')<>'0'
      begin 
		    if not EXISTS(select * from Storage_StorageProduct where Sp_St_Guid=@St_Guid and Sp_Pr_Guid = @Pr_Guid ) 
				begin  
				  insert into Storage_StorageProduct(Sp_St_Guid,Sp_Ps_Guid,Sp_Pr_Guid,pubid) values(@St_Guid,@Pr_Ps_Guid,@Pr_Guid ,newid()) 
			  end  
	      set @tempstr1=' update  Storage_StorageProduct set  Sp_Sale_Out=isnull('+str(@tempvalue1,20,4)+
		         ',0) where Sp_St_Guid ='+''''+ @St_Guid+''''  +' and  Sp_Pr_Guid  ='+''''+@Pr_Guid+''''
		    exec(@tempstr1 ) 
		  end
		end
--销售退货
	  set @tempstr1=
'               select @tempvalue1=isnull(sum(isnull(b.Ri_Quantity,0)*isnull(d.Pu_BaseUnitNum,0)),0) '+CHAR(10)+
'               from  Sale_ReturnProduct a left join Sale_ReturnProductItem b on a.Rp_Guid=b.Ri_Rp_Guid and  '+CHAR(10)+
'                     b.Ri_Pr_Guid=@temp_Pr_Guid and b.Ri_St_Guid=@temp_St_Guid and a.Rp_State=''查询''    '+CHAR(10)+
'                     left join ERP_Base_MaterielUnit d on b.Ri_Pr_Guid=d.Pu_Pr_Guid and b.Ri_Pu_Guid=d.Pu_Guid ' 
	  if @tempstr1<>''
	  begin

	    exec  sp_executesql @tempstr1 ,N' @tempvalue1 decimal(18,3)  OUTPUT  ,@temp_Pr_Guid varchar(100),@temp_St_Guid varchar(100)',@tempvalue1  OUTPUT ,@temp_Pr_Guid =@Pr_Guid , @temp_St_Guid =@St_Guid  
	    if isnull(@tempvalue1,'0')<>'0'
      begin 
		    if not EXISTS(select * from Storage_StorageProduct where Sp_St_Guid=@St_Guid and Sp_Pr_Guid = @Pr_Guid ) 
				begin  
				  insert into Storage_StorageProduct(Sp_St_Guid,Sp_Ps_Guid,Sp_Pr_Guid,pubid) values(@St_Guid,@Pr_Ps_Guid,@Pr_Guid ,newid()) 
			  end  
	      set @tempstr1=' update  Storage_StorageProduct set  Sp_Sale_Return=isnull('+str(@tempvalue1,20,4)+
		         ',0) where Sp_St_Guid ='+''''+ @St_Guid+''''  +' and  Sp_Pr_Guid  ='+''''+@Pr_Guid+''''
		    exec(@tempstr1 ) 
		  end
		end

--生产完工入库
	  set @tempstr1=
'               select @tempvalue1=isnull(sum(isnull(b.Oi_Quantity,0)*isnull(d.Pu_BaseUnitNum,0)),0) '+CHAR(10)+
'               from  Produce_Order a left join Produce_OrderItem b on a.Or_Guid=b.Oi_Or_Guid and  '+CHAR(10)+
'                     b.Oi_Pr_Guid=@temp_Pr_Guid and b.Oi_St_Guid=@temp_St_Guid and   a.Or_FinishingState=''查询''    '+CHAR(10)+
'                     left join ERP_Base_MaterielUnit d on b.Oi_Pr_Guid=d.Pu_Pr_Guid and b.Oi_Pu_Guid=d.Pu_Guid ' 
	  if @tempstr1<>''
	  begin

	    exec  sp_executesql @tempstr1 ,N' @tempvalue1 decimal(18,3)  OUTPUT  ,@temp_Pr_Guid varchar(100),@temp_St_Guid varchar(100)',@tempvalue1  OUTPUT ,@temp_Pr_Guid =@Pr_Guid , @temp_St_Guid =@St_Guid  
	    if isnull(@tempvalue1,'0')<>'0'
      begin 
		    if not EXISTS(select * from Storage_StorageProduct where Sp_St_Guid=@St_Guid and Sp_Pr_Guid = @Pr_Guid ) 
				begin  
				  insert into Storage_StorageProduct(Sp_St_Guid,Sp_Ps_Guid,Sp_Pr_Guid,pubid) values(@St_Guid,@Pr_Ps_Guid,@Pr_Guid ,newid()) 
			  end  
	      set @tempstr1=' update  Storage_StorageProduct set  Sp_Produce_In=isnull('+str(@tempvalue1,20,4)+
		         ',0) where Sp_St_Guid ='+''''+ @St_Guid+''''  +' and  Sp_Pr_Guid  ='+''''+@Pr_Guid+''''
  	    exec(@tempstr1 ) 
		  end
		end
 
--生产原料出库
	  set @tempstr1=
'               select @tempvalue1=isnull(sum(isnull(b.Ob_Quantity,0)*isnull(d.Pu_BaseUnitNum,0)),0) '+CHAR(10)+
'               from  Produce_Order a left join Produce_OrderBom b on a.Or_Guid=b.Ob_Or_Guid and  '+CHAR(10)+
'                     b.Ob_Pr_Guid=@temp_Pr_Guid and b.Ob_St_Guid=@temp_St_Guid and  a.Or_BomState=''查询''    '+CHAR(10)+
'                     left join ERP_Base_MaterielUnit d on b.Ob_Pr_Guid=d.Pu_Pr_Guid and b.Ob_Pu_Guid=d.Pu_Guid ' 
	  if @tempstr1<>''
	  begin

	    exec  sp_executesql @tempstr1 ,N' @tempvalue1 decimal(18,3)  OUTPUT  ,@temp_Pr_Guid varchar(100),@temp_St_Guid varchar(100)',@tempvalue1  OUTPUT ,@temp_Pr_Guid =@Pr_Guid , @temp_St_Guid =@St_Guid  
	    if isnull(@tempvalue1,'0')<>'0'
      begin 
		    if not EXISTS(select * from Storage_StorageProduct where Sp_St_Guid=@St_Guid and Sp_Pr_Guid = @Pr_Guid ) 
				begin  
				  insert into Storage_StorageProduct(Sp_St_Guid,Sp_Ps_Guid,Sp_Pr_Guid,pubid) values(@St_Guid,@Pr_Ps_Guid,@Pr_Guid ,newid()) 
			  end  
	      set @tempstr1=' update  Storage_StorageProduct set  Sp_Produce_Out=isnull('+str(@tempvalue1,20,4)+
		         ',0) where Sp_St_Guid ='+''''+ @St_Guid+''''  +' and  Sp_Pr_Guid  ='+''''+@Pr_Guid+''''
		    exec(@tempstr1 ) 
		  end
		end
--自定义出库
	  set @tempstr1=
'               select @tempvalue1=isnull(sum(isnull(b.Oi_Quantity,0)*isnull(d.Pu_BaseUnitNum,0)),0) '+CHAR(10)+
'               from  Storage_OtherOutStorage a left join Storage_OtherOutStorageItem b on a.Oo_Guid=b.Oi_Oo_Guid and  '+CHAR(10)+
'                     b.Oi_Pr_Guid=@temp_Pr_Guid and b.Oi_St_Guid=@temp_St_Guid and a.Oo_State=''查询''    '+CHAR(10)+
'                     left join ERP_Base_MaterielUnit d on b.Oi_Pr_Guid=d.Pu_Pr_Guid and b.Oi_Pu_Guid=d.Pu_Guid ' 
	  if @tempstr1<>''
	  begin

	    exec  sp_executesql @tempstr1 ,N' @tempvalue1 decimal(18,3)  OUTPUT  ,@temp_Pr_Guid varchar(100),@temp_St_Guid varchar(100)',@tempvalue1  OUTPUT ,@temp_Pr_Guid =@Pr_Guid , @temp_St_Guid =@St_Guid  
	    if isnull(@tempvalue1,'0')<>'0'
      begin 
		    if not EXISTS(select * from Storage_StorageProduct where Sp_St_Guid=@St_Guid and Sp_Pr_Guid = @Pr_Guid ) 
				begin  
				  insert into Storage_StorageProduct(Sp_St_Guid,Sp_Ps_Guid,Sp_Pr_Guid,pubid) values(@St_Guid,@Pr_Ps_Guid,@Pr_Guid ,newid()) 
			  end  
	      set @tempstr1=' update  Storage_StorageProduct set  Sp_Storage_OtherOut=isnull('+str(@tempvalue1,20,4)+
		         ',0) where Sp_St_Guid ='+''''+ @St_Guid+''''  +' and  Sp_Pr_Guid  ='+''''+@Pr_Guid+''''
	      exec(@tempstr1 ) 
		  end
		end
--自定义入库
	  set @tempstr1=
'               select @tempvalue1=isnull(sum(isnull(b.Oi_Quantity,0)*isnull(d.Pu_BaseUnitNum,0)),0) '+CHAR(10)+
'               from  Storage_OtherInStorage a left join Storage_OtherInStorageItem b on a.Oi_Guid=b.Oi_Oi_Guid and  '+CHAR(10)+
'                     b.Oi_Pr_Guid=@temp_Pr_Guid and b.Oi_St_Guid=@temp_St_Guid and a.Oi_State=''查询''    '+CHAR(10)+
'                     left join ERP_Base_MaterielUnit d on b.Oi_Pr_Guid=d.Pu_Pr_Guid and b.Oi_Pu_Guid=d.Pu_Guid ' 
	  if @tempstr1<>''
	  begin

	    exec  sp_executesql @tempstr1 ,N' @tempvalue1 decimal(18,3)  OUTPUT  ,@temp_Pr_Guid varchar(100),@temp_St_Guid varchar(100)',@tempvalue1  OUTPUT ,@temp_Pr_Guid =@Pr_Guid , @temp_St_Guid =@St_Guid  
	    if isnull(@tempvalue1,'0')<>'0'
      begin 
		    if not EXISTS(select * from Storage_StorageProduct where Sp_St_Guid=@St_Guid and Sp_Pr_Guid = @Pr_Guid ) 
				begin  
				  insert into Storage_StorageProduct(Sp_St_Guid,Sp_Ps_Guid,Sp_Pr_Guid,pubid) values(@St_Guid,@Pr_Ps_Guid,@Pr_Guid ,newid()) 
			  end  
	      set @tempstr1=' update  Storage_StorageProduct set  Sp_Storage_OtherIn=isnull('+str(@tempvalue1,20,4)+
		         ',0) where Sp_St_Guid ='+''''+ @St_Guid+''''  +' and  Sp_Pr_Guid  ='+''''+@Pr_Guid+''''
		    exec(@tempstr1 ) 
		  end
		end
--转库入库
	  set @tempstr1=
'               select @tempvalue1=isnull(sum(isnull(b.Mi_Quantity,0)*isnull(d.Pu_BaseUnitNum,0)),0) '+CHAR(10)+
'               from  Storage_MoveStorage a left join Storage_MoveStorageItem b on a.Ms_Guid=b.Mi_Ms_Guid and  '+CHAR(10)+
'                     b.Mi_Pr_Guid=@temp_Pr_Guid and b.Mi_In_St_Guid=@temp_St_Guid and a.Ms_State=''查询''    '+CHAR(10)+
'                     left join ERP_Base_MaterielUnit d on b.Mi_Pr_Guid=d.Pu_Pr_Guid and b.Mi_Pu_Guid=d.Pu_Guid ' 
	  if @tempstr1<>''
	  begin

	    exec  sp_executesql @tempstr1 ,N' @tempvalue1 decimal(18,3)  OUTPUT  ,@temp_Pr_Guid varchar(100),@temp_St_Guid varchar(100)',@tempvalue1  OUTPUT ,@temp_Pr_Guid =@Pr_Guid , @temp_St_Guid =@St_Guid  
	    if isnull(@tempvalue1,'0')<>'0'
      begin 
		    if not EXISTS(select * from Storage_StorageProduct where Sp_St_Guid=@St_Guid and Sp_Pr_Guid = @Pr_Guid ) 
				begin  
				  insert into Storage_StorageProduct(Sp_St_Guid,Sp_Ps_Guid,Sp_Pr_Guid,pubid) values(@St_Guid,@Pr_Ps_Guid,@Pr_Guid ,newid()) 
			  end  
	      set @tempstr1=' update  Storage_StorageProduct set  Sp_Storage_MoveIn=isnull('+str(@tempvalue1,20,4)+
		         ',0) where Sp_St_Guid ='+''''+ @St_Guid+''''  +' and  Sp_Pr_Guid  ='+''''+@Pr_Guid+''''
		    exec(@tempstr1 ) 
		  end
		end
--转库出库
	  set @tempstr1=
'               select @tempvalue1=isnull(sum(isnull(b.Mi_Quantity,0)*isnull(d.Pu_BaseUnitNum,0)),0) '+CHAR(10)+
'               from  Storage_MoveStorage a left join Storage_MoveStorageItem b on a.Ms_Guid=b.Mi_Ms_Guid and  '+CHAR(10)+
'                     b.Mi_Pr_Guid=@temp_Pr_Guid and b.Mi_Out_St_Guid=@temp_St_Guid and a.Ms_State=''查询''    '+CHAR(10)+
'                     left join ERP_Base_MaterielUnit d on b.Mi_Pr_Guid=d.Pu_Pr_Guid and b.Mi_Pu_Guid=d.Pu_Guid ' 
	  if @tempstr1<>''
	  begin

	    exec  sp_executesql @tempstr1 ,N' @tempvalue1 decimal(18,3)  OUTPUT  ,@temp_Pr_Guid varchar(100),@temp_St_Guid varchar(100)',@tempvalue1  OUTPUT ,@temp_Pr_Guid =@Pr_Guid , @temp_St_Guid =@St_Guid  
	    if isnull(@tempvalue1,'0')<>'0'
      begin 
		    if not EXISTS(select * from Storage_StorageProduct where Sp_St_Guid=@St_Guid and Sp_Pr_Guid = @Pr_Guid ) 
				begin  
				  insert into Storage_StorageProduct(Sp_St_Guid,Sp_Ps_Guid,Sp_Pr_Guid,pubid) values(@St_Guid,@Pr_Ps_Guid,@Pr_Guid ,newid()) 
			  end  
	      set @tempstr1=' update  Storage_StorageProduct set  Sp_Storage_MoveOut=isnull('+str(@tempvalue1,20,4)+
		         ',0) where Sp_St_Guid ='+''''+ @St_Guid+''''  +' and  Sp_Pr_Guid  ='+''''+@Pr_Guid+''''
		    exec(@tempstr1 ) 
		  end
		end
--维修出库
	  set @tempstr1=
'               select @tempvalue1=isnull(sum(isnull(b.Oi_Quantity,0)*isnull(d.Pu_BaseUnitNum,0)),0) '+CHAR(10)+
'               from  Service_Order a left join Service_OrderItem b on a.Or_Guid=b.Oi_Or_Guid and  '+CHAR(10)+
'                     b.Oi_Pr_Guid=@temp_Pr_Guid and b.Oi_St_Guid=@temp_St_Guid and a.Or_State=''查询''    '+CHAR(10)+
'                     left join ERP_Base_MaterielUnit d on b.Oi_Pr_Guid=d.Pu_Pr_Guid and b.Oi_Pu_Guid=d.Pu_Guid ' 
	  if @tempstr1<>''
	  begin

	    exec  sp_executesql @tempstr1 ,N' @tempvalue1 decimal(18,3)  OUTPUT  ,@temp_Pr_Guid varchar(100),@temp_St_Guid varchar(100)',@tempvalue1  OUTPUT ,@temp_Pr_Guid =@Pr_Guid , @temp_St_Guid =@St_Guid  
	    if isnull(@tempvalue1,'0')<>'0'
      begin 
		    if not EXISTS(select * from Storage_StorageProduct where Sp_St_Guid=@St_Guid and Sp_Pr_Guid = @Pr_Guid ) 
				begin  
				  insert into Storage_StorageProduct(Sp_St_Guid,Sp_Ps_Guid,Sp_Pr_Guid,pubid) values(@St_Guid,@Pr_Ps_Guid,@Pr_Guid ,newid()) 
			  end  
	      set @tempstr1=' update  Storage_StorageProduct set  Sp_Service_Out=isnull('+str(@tempvalue1,20,4)+
		         ',0) where Sp_St_Guid ='+''''+ @St_Guid+'			'''  +' and  Sp_Pr_Guid  ='+''''+@Pr_Guid+''''
		    exec(@tempstr1 ) 
		  end
		end
    fetch  next  from  CurStorageCursor  into  @St_Guid  
  end
  close  CurStorageCursor
  fetch  next  from  CurProductCursor  into  @Pr_Ps_Guid,@Pr_Guid  
end
close  CurProductCursor
deallocate  CurProductCursor
deallocate  CurStorageCursor
go 
--exec Proc_InitStorage '','BF87A26B-2A36-46AD-B3F4-A42DE5914B24'
--exec Proc_InitStorage '',''
go

go
 
--统一增加与删除检测销售出库是否已终审
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_IsEndExamine]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Proc_IsEndExamine]
go
CREATE PROCEDURE [dbo].[Proc_IsEndExamine]
(
@Os_Guid varchar(100),
@Is int output
)  
AS
set nocount on 
set @Is=0

if  
	exists(
	select * 
	from Sale_Order 
	where Or_EndExamineState='查询' and Or_Guid in (select distinct Oi_ComeFromCode from Sale_OutStorageItem where Oi_Os_Guid=@Os_Guid )
	)
begin
  set @Is=1
end
 
GO
--exec Proc_IsEndExamine  '{C7DB9B0F-D323-4816-BBA2-15AE94C53C32}',1
 

 
--计算出库单中的已放容积与重量
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_CalcTrckTonnageVolume]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Proc_CalcTrckTonnageVolume]
go
CREATE PROCEDURE [dbo].[Proc_CalcTrckTonnageVolume]
(
@Os_Guid varchar(100),
@ReStr varchar(100) output,
@Tonnage ERPDefaultDecimal output,
@Volume ERPDefaultDecimal output
)  
AS
set nocount on 
set @ReStr=''
set @Tonnage=0
set @Volume=0

select @Tonnage=sum(isnull(b.Oi_Quantity,0)*isnull(d.Pu_BaseUnitNum,0)*isnull(c.Pr_Weight,0)) 
from  Sale_OutStorage a left join Sale_OutStorageItem b on a.Os_Guid=b.Oi_Os_Guid
      left join ERP_Base_Materiel c on b.Oi_Pr_Guid=c.Pr_Guid
      left join ERP_Base_MaterielUnit d on b.Oi_Pu_Guid=d.Pu_Guid
where a.Os_Guid=@Os_Guid


select @Volume=sum(isnull(b.Oi_Quantity,0)*isnull(d.Pu_BaseUnitNum,0)*isnull(c.Pr_Volume,0)) 
from  Sale_OutStorage a left join Sale_OutStorageItem b on a.Os_Guid=b.Oi_Os_Guid
      left join ERP_Base_Materiel c on b.Oi_Pr_Guid=c.Pr_Guid
      left join ERP_Base_MaterielUnit d on b.Oi_Pu_Guid=d.Pu_Guid
where a.Os_Guid=@Os_Guid
 
set @Tonnage=isnull(@Tonnage,0)
set @Volume =isnull(@Volume,0) 

set @ReStr='已配体积:'+str(@Tonnage,20,2)+'已配重量:'+str(@Volume,20,2)
 
GO
--exec Proc_CalcTrckTonnageVolume  
 
go
 

--从其它价格体系中导入
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_CopyPriceSystem]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Proc_CopyPriceSystem]
go
CREATE PROCEDURE [dbo].[Proc_CopyPriceSystem]
(
@SGuid varchar(100),@SName varchar(100),@TGuid  varchar(100),@UpdateObject Int, --0表示其它的价格体系,1表示客户的产品表,2表示供应商的产品表
@IsUpdateSame bit=0
)  
AS
  if @SName<>'' and (@SGuid='' or @SGuid is null )
    select @SGuid=Ps_Guid from Base_PriceSystem where Ps_Name=@SName

  if @UpdateObject=0 
  begin
	  --更新已有的产品
	  if @IsUpdateSame =1 
    begin
	    update t
	    set t.Pi_Price = s.Pi_Price  
	    from Base_PriceSystemItem s ,Base_PriceSystemItem t
	    where t.Pi_Ps_Guid=@TGuid and s.Pi_Pr_Guid=t.Pi_Pr_Guid and
	          s.Pi_Ps_Guid=@SGuid  

	    update t
	    set  t.Pi_LinkPr_Code=s.Pi_LinkPr_Code
	    from Base_PriceSystemItem s ,Base_PriceSystemItem t
	    where t.Pi_Ps_Guid=@TGuid and s.Pi_Pr_Guid=t.Pi_Pr_Guid and
	          s.Pi_Ps_Guid=@SGuid and isnull(t.Pi_LinkPr_Code,'')<>'' 
	  end
	  --新增没有的产品
		insert into  Base_PriceSystemItem(Pi_Ps_Guid,Pi_Pss_Guid,Pi_Pr_Guid,Pi_Price,PubID,Pi_LinkPr_Code) 
		select @TGuid,s.Pi_Pss_Guid,s.Pi_Pr_Guid,s.Pi_Price,newid(),Pi_LinkPr_Code from Base_PriceSystemItem s  
		where  s.Pi_Ps_Guid=@SGuid and s.Pi_Pr_Guid not in 
		      (select t.Pi_Pr_Guid from Base_PriceSystemItem t  
		       where t.Pi_Ps_Guid=@TGuid)
  end
  else if @UpdateObject=1 
  begin
	  --更新已有的产品
	  if @IsUpdateSame =1 
    begin
	    update t
	    set t.Cp_SalePrice = s.Pi_Price 
	    from Base_PriceSystemItem s ,Base_CustProduct t
	    where t.Cp_Cu_Guid=@TGuid and s.Pi_Pr_Guid=t.Cp_Pr_Guid and
	          s.Pi_Ps_Guid=@SGuid  

	    update t
	    set  t.Cp_LinkPr_Code=s.Pi_LinkPr_Code
	    from Base_PriceSystemItem s ,Base_CustProduct t
	    where t.Cp_Cu_Guid=@TGuid and s.Pi_Pr_Guid=t.Cp_Pr_Guid and
	          s.Pi_Ps_Guid=@SGuid and isnull(t.Cp_LinkPr_Code,'')<>''  
	  end
	
	  --新增没有的产品
		insert into  Base_CustProduct(Cp_Cu_Guid,Cp_Ps_Guid,Cp_Pr_Guid,Cp_SalePrice,PubID,Cp_LinkPr_Code) 
		select @TGuid,s.Pi_Pss_Guid,s.Pi_Pr_Guid,s.Pi_Price,newid(),Pi_LinkPr_Code from Base_PriceSystemItem s  
		where  s.Pi_Ps_Guid=@SGuid and s.Pi_Pr_Guid not in 
		      (select t.Cp_Pr_Guid from Base_CustProduct t  
		       where t.Cp_Cu_Guid=@TGuid)

  end
  else if @UpdateObject=2 
  begin
	  --更新已有的产品
	  if @IsUpdateSame =1 
    begin
	    update t
	    set t.Sp_Price = s.Pi_Price 
	    from Base_PriceSystemItem s ,Base_SupplierProduct t
	    where t.Sp_Su_Guid=@TGuid and s.Pi_Pr_Guid=t.Sp_Pr_Guid and
	          s.Pi_Ps_Guid=@SGuid  

	    update t
	    set  t.Sp_LinkPr_Code=s.Pi_LinkPr_Code
	    from Base_PriceSystemItem s ,Base_SupplierProduct t
	    where t.Sp_Su_Guid=@TGuid and s.Pi_Pr_Guid=t.Sp_Pr_Guid and
	          s.Pi_Ps_Guid=@SGuid and isnull(t.Sp_LinkPr_Code,'')<>''   
	  end
	
	  --新增没有的产品
		insert into  Base_SupplierProduct(Sp_Su_Guid,Sp_Ps_Guid,Sp_Pr_Guid,Sp_Price,PubID,Sp_LinkPr_Code) 
		select @TGuid,s.Pi_Pss_Guid,s.Pi_Pr_Guid,s.Pi_Price,newid(),Pi_LinkPr_Code from Base_PriceSystemItem s  
		where  s.Pi_Ps_Guid=@SGuid and s.Pi_Pr_Guid not in 
		      (select t.Sp_Pr_Guid from Base_SupplierProduct t  
		       where t.Sp_Su_Guid=@TGuid)

  end 
go




--检测此单所属的结算方式,最大信用额度是否许可出库操作,出库数量不能大于订单数量
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_CheckSaleOutStorage]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure   [dbo].[Proc_CheckSaleOutStorage]
go
CREATE PROCEDURE [dbo].[Proc_CheckSaleOutStorage]
(
@Guid varchar(100),
@Code varchar(100),
@State varchar(200) OUTPUT
)  
AS

declare @Sale_OutStorageItem table(Oi_ComeFromCode varchar(100), Oi_ComeFromItemCode varchar(100)  )
declare @Or_Guid varchar(100),@Oi_Guid varchar(100)
declare @Or_Cu_Guid varchar(100),@Cu_Name varchar(100),@Or_Code varchar(100)
declare @NotPayMoney ERPDefaultDecimal  
declare @Cu_MaxCreditMoney ERPDefaultDecimal  
declare @IsMoreOutThanOrder bit
set @IsMoreOutThanOrder=0
set @State=''
set @NotPayMoney =0
set @Cu_MaxCreditMoney =0


insert into @Sale_OutStorageItem(Oi_ComeFromCode,Oi_ComeFromItemCode)
select  Oi_ComeFromCode,Oi_ComeFromItemCode 
from  Sale_OutStorageItem 
where Oi_Os_Guid=@Guid

declare Sale_OutStorageItemCursor cursor  for 
select Oi_ComeFromCode,Oi_ComeFromItemCode  from @Sale_OutStorageItem

open  Sale_OutStorageItemCursor
fetch  next  from  Sale_OutStorageItemCursor  into  @Or_Guid ,@Oi_Guid
while  (@@fetch_status  =  0) and (@Cu_MaxCreditMoney<0 or @NotPayMoney<=@Cu_MaxCreditMoney) and (@IsMoreOutThanOrder=0)
begin
   
	set @NotPayMoney=0
	set @Cu_MaxCreditMoney=0
	--得到订单所属客户的GUID
	select @Or_Cu_Guid=Or_Cu_Guid ,@Or_Code=Or_Code
  from Sale_Order 
  where Or_Guid=@Or_Guid
	
	--得到订单所属客户最大信用额度
	select @Cu_MaxCreditMoney=isnull(Cu_MaxCreditMoney,-1),@Cu_Name=Cu_Name 
	from Base_Cust 
	where Cu_Guid=@Or_Cu_Guid
	
  if @Cu_MaxCreditMoney>=0 
  begin
		--得到订单未付的金额


		 
		
		select @NotPayMoney=sum(isnull(NotPayMoneyForOrder,0))
	  from 
	  (
		select 
		   isnull(sum((isnull(Oi_SalePrice,0))*(1-isnull(Oi_Discount,0))*isnull(Oi_Quantity,0)),0)-  --历史订单销售金额
		   isnull((select isnull(sum(isnull(Og_Money,0)),0) from  Sale_OrderGetMoney aa where a.Or_Guid=aa.Og_Or_Guid ),0) as NotPayMoneyForOrder --历史订单已收金额    
		from Sale_Order a  left join Sale_OrderItem b on a.Or_Guid=b.Oi_Or_Guid                                                                       
		where a.Or_state='查询' and a.Or_GetMoneyState <>'全部收款' and a.Or_Cu_Guid=@Or_Cu_Guid                                       
	        group by a.Or_Guid
	  ) aa

	  if not (@NotPayMoney<=@Cu_MaxCreditMoney)
	    set @State='出库单'+@Code+' 中客户:'+@Cu_Name+''+'的未付金额'+str(@NotPayMoney)+' 超出了最大信用额度'+str(@Cu_MaxCreditMoney)+',不能出库.'
  end


  if       isnull(( select sum(isnull(b.Oi_Quantity,0)*isnull(d.Pu_BaseUnitNum,0)) 
              from  Sale_OutStorage a left join Sale_OutStorageItem b on a.Os_Guid=b.Oi_Os_Guid and 
                    b.Oi_ComeFromItemCode=@Oi_Guid  
                    left join ERP_Base_MaterielUnit d on b.Oi_Pu_Guid=d.Pu_Guid
            ),0)>

            isnull(( select sum(isnull(b.Oi_Quantity,0)*isnull(d.Pu_BaseUnitNum,0)) 
              from   Sale_OrderItem b left join ERP_Base_MaterielUnit d on b.Oi_Pu_Guid=d.Pu_Guid
              where  b.Oi_Guid=@Oi_Guid  
                    
            ),0) 
  begin
    set @State='出库单'+@Code+' 中产品的出库数量不能大于订单数量.'
    set @IsMoreOutThanOrder=1
  end

  fetch  next  from  Sale_OutStorageItemCursor  into  @Or_Guid ,@Oi_Guid
end
--关闭与释放游标
close  Sale_OutStorageItemCursor
deallocate  Sale_OutStorageItemCursor


go

--根据产品的BOM设置创建生产订单的BOM详细
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_CopyBomToProduceOrder]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Proc_CopyBomToProduceOrder]
go
CREATE PROCEDURE [dbo].[Proc_CopyBomToProduceOrder]
(
@Guid varchar(100) 
)  
AS

if @Guid<>'' and @Guid is not null 
begin
	delete from Produce_OrderBom where Ob_Or_Guid=@Guid
	
	insert into Produce_OrderBom(Ob_Or_Guid,Ob_Guid,Ob_Ps_Guid,Ob_Pr_Guid,Ob_Quantity,Ob_Pu_Guid,PubID,Ob_St_Guid,Ob_Price)
	select a.Oi_Or_Guid as Oi_Or_Guid,
	       newid() as Ob_Guid,
	       c.Pb_Ps_Guid as Pb_Ps_Guid,
	       c.Pb_Pr_Guid as Pb_Pr_Guid,
	
	       isnull(a.Oi_Quantity,0)*isnull(c.Pb_Quantity,0) as Pb_Quantity, 
	       c.Pb_Pu_Guid as Ob_Pu_Guid,
	       newid() as Ob_Guid,bb.Pr_DefaultStorage,bb.Pr_CostPrice
	from Produce_OrderItem a 
	     left join ERP_Base_MaterielVersion b on a.Oi_Pv_Guid=b.Pv_Guid 
	     left join ERP_Base_MaterielVersionBom c on b.Pv_Guid=c.Pb_Pv_Guid 
	     left join ERP_Base_Materiel bb on c.Pb_Pr_Guid=bb.Pr_Guid 
	where a.Oi_Or_Guid=@Guid and c.Pb_IsActive =1
 
end

go


--根据产品的BOM设置创建另一产品的BOM表
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_CopyBomToProduce]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Proc_CopyBomToProduce]
go
CREATE PROCEDURE [dbo].[Proc_CopyBomToProduce]
(
@S_Pv_Guid varchar(100) ,
@T_Pv_Guid varchar(100)
)  
AS

if (@S_Pv_Guid<>'' and @S_Pv_Guid is not null) and  (@T_Pv_Guid<>'' and @T_Pv_Guid is not null)
begin
	delete from ERP_Base_MaterielVersionBom where Pb_Pv_Guid=@T_Pv_Guid
	
	insert into ERP_Base_MaterielVersionBom(Pb_Ps_Guid,Pb_Pr_Guid,Pb_Quantity,Pb_TechnicsRemark,Pb_Remark,Pb_Pu_Guid,Pb_Pv_Guid,PubID,Pb_IsActive)
	select  Pb_Ps_Guid,Pb_Pr_Guid,Pb_Quantity,Pb_TechnicsRemark,Pb_Remark,Pb_Pu_Guid,@T_Pv_Guid,PubID,Pb_IsActive
	from ERP_Base_MaterielVersionBom a 
	where Pb_Pv_Guid=@S_Pv_Guid
 
end

go





--查询分库的实际库存,以仓库+产品为单位
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_QueryCurStorage]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Proc_QueryCurStorage]
go
CREATE PROCEDURE [dbo].[Proc_QueryCurStorage]
(
@BegDate varchar(100),@EndDate varchar(100),@Pr_Guid varchar(100)='',@St_Guid varchar(100)='',@IsViewStorageMoney bit=0
)  
AS

--declare @BegDate varchar(100)
--declare @EndDate varchar(100)
--declare @Pr_Guid varchar(100)
--declare @St_Guid varchar(100)
--declare @IsNotViewWillStorage bit
--declare @IsMrp bit
--set @BegDate='2001-1-1'
--set @EndDate='2006-1-1'
--set @Pr_Guid=''
--set @St_Guid=''
--set @IsMrp=0


declare @NowDate varchar(100)
declare @BeginUseDate datetime
declare @IslessThanBeginUseDate bit
declare @IsMoreThanNowDate bit
 
print @BegDate
print @EndDate

--set @BegDate=REPLACE(@BegDate,'''''','''')
--set @EndDate=REPLACE(@EndDate,'''''','''')

print @BegDate
print @EndDate


set @NowDate=convert(varchar(10),getdate(),121)
select @BeginUseDate=SYSVALUE  from YBSYSINFO where YBSYSNAME='BeginUseDate'

if @BegDate < =@BeginUseDate 
  set @IslessThanBeginUseDate=1 
else
  set @IslessThanBeginUseDate=0 

if @EndDate>=@NowDate 
  set @IsMoreThanNowDate=1 
else
  set @IsMoreThanNowDate=0 


if exists (select * from tempdb.dbo.sysobjects where id = object_id(N'tempdb.[dbo].[##Proc_QueryCurStorage_temp1]'))
  drop table  ##Proc_QueryCurStorage_temp1

select *    
into ##Proc_QueryCurStorage_temp1
from 
(
select
  sp.Pubid,Sp_Ps_Guid as Pr_Ps_Guid,Sp_Pr_Guid as Pr_Guid,sp.Sp_St_Guid as St_Guid,Pu_Guid,Pu_Name as 基本单位,pr.Pr_Code as 产品编号,Pr.Pr_Name as 产品名称,
  (select top 1 Ps_Name from ERP_Base_MaterielSort where Ps_Guid=sp.Sp_Ps_Guid) as 产品分类,
  (select St_Name from Base_Storage where sp.Sp_St_Guid=St_Guid ) as 仓库名称,
  isnull(Sp_BegStorage,0) as 期初库存        ,isnull(Sp_Buy_Get,0) as 采购收货,
  isnull(Sp_Buy_Return,0) as 采购退货        ,isnull(Sp_Sale_Out,0) as 销售出货,
  isnull(Sp_Sale_Return,0) as 销售退货       ,isnull(Sp_Produce_In,0) as 生产完工入库,
  isnull(Sp_Produce_Out,0) as 生产原料出库   ,
  isnull(Sp_Storage_OtherIn,0) as 自定义入库,
  isnull(Sp_Storage_OtherOut,0) as 自定义出库  ,
  isnull(Sp_Storage_MoveIn,0) as 转库入库,
  isnull(Sp_Storage_MoveOut,0) as 转库出库  ,
  isnull(Sp_Service_Out,0) as 维修出库,
  isnull(Sp_EndStorage,0) as 期未库存
from Storage_StorageProduct sp 
            left join ERP_Base_Materiel Pr on Pr.Pr_Guid=sp.Sp_Pr_Guid  
            left join ERP_Base_MaterielUnit pu on pu.Pu_Pr_Guid=sp.Sp_Pr_Guid and pu.Pu_IsBaseUnit=1 
           
where (@Pr_Guid='' or sp.Sp_Pr_Guid=@Pr_Guid ) and (@St_Guid='' or sp.Sp_St_Guid=@St_Guid )
) aaaa
order by 产品分类,产品名称
 
if exists (select * from tempdb.dbo.sysobjects where id = object_id(N'tempdb.[dbo].[##Proc_QueryCurStorage]'))
  drop table  ##Proc_QueryCurStorage
  
select
  Pubid,Pr_Ps_Guid,Pr_Guid,St_Guid,Pu_Guid,基本单位,产品编号,产品名称,产品分类,仓库名称,

      (
       case
       when @IslessThanBeginUseDate=1  then 期初库存
       else 
          期未库存-(采购收货-采购退货-销售出货+销售退货+生产完工入库-生产原料出库+自定义入库-自定义出库+转库入库-转库出库-维修出库)
       end
       ) as 期初库存,
      采购收货,采购退货,销售出货,销售退货,生产完工入库,生产原料出库,自定义入库,自定义出库,转库入库,转库出库,维修出库,期未库存

into ##Proc_QueryCurStorage
    
from
(
select 
  Pubid,Pr_Ps_Guid,Pr_Guid,St_Guid,Pu_Guid,基本单位,产品编号,产品名称,产品分类,仓库名称,


       期初库存,   
       case
       when @IsMoreThanNowDate=1  then 期未库存
       else
         期未库存-  
       (isnull(
              ( select sum(isnull(b.Gi_Quantity,0)*isnull(d.Pu_BaseUnitNum,0)) 
                from  Buy_GetProduct a left join Buy_GetProductItem b on a.Gp_Guid=b.Gi_Gp_Guid and 
                      aaaa.St_Guid=b.Gi_St_Guid and aaaa.Pr_Guid=b.Gi_Pr_Guid and a.Gp_State='查询' and convert(varchar(10),a.Gp_Datetime,121)>@EndDate 
                      left join ERP_Base_MaterielUnit d on b.Gi_Pu_Guid=d.Pu_Guid
              )
        ,0))  --采购收货
        +
        (isnull(
              ( select sum(isnull(b.Ri_Quantity,0)*isnull(d.Pu_BaseUnitNum,0)) 
                from  Buy_ReturnProduct a left join Buy_ReturnProductItem b on a.Rp_Guid=b.Ri_Rp_Guid and 
                      aaaa.St_Guid=b.Ri_St_Guid and aaaa.Pr_Guid=b.Ri_Pr_Guid and a.Rp_State='查询' and convert(varchar(10),a.Rp_Datetime,121)>@EndDate  
                      left join ERP_Base_MaterielUnit d on b.Ri_Pu_Guid=d.Pu_Guid
              )
        ,0))  -- 采购退货
        +   
       (isnull(
              ( select sum(isnull(b.Oi_Quantity,0)*isnull(d.Pu_BaseUnitNum,0)) 
                from  Sale_OutStorage a left join Sale_OutStorageItem b on a.Os_Guid=b.Oi_Os_Guid and 
                      aaaa.St_Guid=b.Oi_St_Guid and aaaa.Pr_Guid=b.Oi_Pr_Guid and a.Os_State='查询' and convert(varchar(10),a.Os_Datetime,121)>@EndDate  
                      left join ERP_Base_MaterielUnit d on b.Oi_Pu_Guid=d.Pu_Guid
              )
        ,0))   --销售出货
       -
      (isnull(
              ( select sum(isnull(b.Ri_Quantity,0)*isnull(d.Pu_BaseUnitNum,0)) 
                from  Sale_ReturnProduct a left join Sale_ReturnProductItem b on a.Rp_Guid=b.Ri_Rp_Guid and 
                      aaaa.St_Guid=b.Ri_St_Guid and aaaa.Pr_Guid=b.Ri_Pr_Guid and a.Rp_State='查询' and convert(varchar(10),a.Rp_Datetime,121)>@EndDate  
                      left join ERP_Base_MaterielUnit d on b.Ri_Pu_Guid=d.Pu_Guid
              )
        ,0))   --销售退货
        -
       (isnull(
              ( select sum(isnull(b.Oi_Quantity,0)*isnull(d.Pu_BaseUnitNum,0)) 
                from  Produce_Order a left join Produce_OrderItem b on a.Or_Guid=b.Oi_Or_Guid and 
                      aaaa.St_Guid=b.Oi_St_Guid and aaaa.Pr_Guid=b.Oi_Pr_Guid and a.Or_FinishingState='查询' and convert(varchar(10),a.Or_FinishingInDatetime,121)>@EndDate  
                      left join ERP_Base_MaterielUnit d on b.Oi_Pu_Guid=d.Pu_Guid
              )
        ,0)) --生产完工入库  
        +  
       (isnull(
              ( select sum(isnull(b.Ob_Quantity,0)*isnull(d.Pu_BaseUnitNum,0)) 
                from  Produce_Order a left join Produce_OrderBom b on a.Or_Guid=b.Ob_Or_Guid and 
                      aaaa.St_Guid=b.Ob_St_Guid and aaaa.Pr_Guid=b.Ob_Pr_Guid and a.Or_BomState='查询' and convert(varchar(10),a.Or_BomOutDatetime,121)>@EndDate  
                      left join ERP_Base_MaterielUnit d on b.Ob_Pu_Guid=d.Pu_Guid
              )
        ,0)) --生产原料出库  
       -  
       (isnull(
              ( select sum(isnull(b.Oi_Quantity,0)*isnull(d.Pu_BaseUnitNum,0))  
                from  Storage_OtherInStorage a left join Storage_OtherInStorageItem b on a.Oi_Guid=b.Oi_Oi_Guid and 
                      aaaa.St_Guid=b.Oi_St_Guid and aaaa.Pr_Guid=b.Oi_Pr_Guid and a.Oi_State='查询' and convert(varchar(10),a.Oi_Datetime,121)>@EndDate  
                      left join ERP_Base_MaterielUnit d on b.Oi_Pu_Guid=d.Pu_Guid
              )
        ,0)) --自定义入库  
        +
       (isnull(
              ( select sum(isnull(b.Oi_Quantity,0)*isnull(d.Pu_BaseUnitNum,0)) 
                from  Storage_OtherOutStorage a left join Storage_OtherOutStorageItem b on a.Oo_Guid=b.Oi_Oo_Guid and 
                      aaaa.St_Guid=b.Oi_St_Guid and aaaa.Pr_Guid=b.Oi_Pr_Guid and a.Oo_State='查询' and convert(varchar(10),a.Oo_Datetime,121)>@EndDate 
                      left join ERP_Base_MaterielUnit d on b.Oi_Pu_Guid=d.Pu_Guid
              )
        ,0)) --自定义出库 
       -  
       (isnull(
              ( select sum(isnull(b.Mi_Quantity,0)*isnull(d.Pu_BaseUnitNum,0))  
                from  Storage_MoveStorage a left join Storage_MoveStorageItem b on a.Ms_Guid=b.Mi_Ms_Guid and 
                      aaaa.St_Guid=b.Mi_In_St_Guid and aaaa.Pr_Guid=b.Mi_Pr_Guid and a.Ms_State='查询' and convert(varchar(10),a.Ms_Datetime,121)>@EndDate  
                      left join ERP_Base_MaterielUnit d on b.Mi_Pu_Guid=d.Pu_Guid
              )
        ,0)) --转库入库  
        +
       (isnull(
              ( select sum(isnull(b.Mi_Quantity,0)*isnull(d.Pu_BaseUnitNum,0))  
                from  Storage_MoveStorage a left join Storage_MoveStorageItem b on a.Ms_Guid=b.Mi_Ms_Guid and 
                      aaaa.St_Guid=b.Mi_Out_St_Guid and aaaa.Pr_Guid=b.Mi_Pr_Guid and a.Ms_State='查询' and convert(varchar(10),a.Ms_Datetime,121)>@EndDate  
                      left join ERP_Base_MaterielUnit d on b.Mi_Pu_Guid=d.Pu_Guid
              )
        ,0)) --转库出库  
        +   
       (isnull(
              ( select sum(isnull(b.Oi_Quantity,0)*isnull(d.Pu_BaseUnitNum,0)) 
                from  Service_Order a left join Service_OrderItem b on a.Or_Guid=b.Oi_Or_Guid and 
                      aaaa.St_Guid=b.Oi_St_Guid and aaaa.Pr_Guid=b.Oi_Pr_Guid and a.Or_State='查询' and convert(varchar(10),a.Or_FactCompDatetime,121)>@EndDate 
                      left join ERP_Base_MaterielUnit d on b.Oi_Pu_Guid=d.Pu_Guid
              )
        ,0))  --维修出库
       end as 期未库存,   
--期内部分
--采购部分

   (case
    when @IslessThanBeginUseDate=1 and @IsMoreThanNowDate=1  then 采购收货
    else   
     (isnull(
            ( select sum(isnull(b.Gi_Quantity,0)*isnull(d.Pu_BaseUnitNum,0)) 
              from  Buy_GetProduct a left join Buy_GetProductItem b on a.Gp_Guid=b.Gi_Gp_Guid and 
                    aaaa.St_Guid=b.Gi_St_Guid and aaaa.Pr_Guid=b.Gi_Pr_Guid and a.Gp_State='查询' and convert(varchar(10),a.Gp_Datetime,121)>=@BegDate and convert(varchar(10),a.Gp_Datetime,121)<=@EndDate
                    left join ERP_Base_MaterielUnit d on b.Gi_Pu_Guid=d.Pu_Guid
            )
      ,0))  
    end
    )  as 采购收货,
   (case
    when @IslessThanBeginUseDate=1 and @IsMoreThanNowDate=1  then 采购退货
    else   
     (isnull(
            ( select sum(isnull(b.Ri_Quantity,0)*isnull(d.Pu_BaseUnitNum,0)) 
              from  Buy_ReturnProduct a left join Buy_ReturnProductItem b on a.Rp_Guid=b.Ri_Rp_Guid and 
                    aaaa.St_Guid=b.Ri_St_Guid and aaaa.Pr_Guid=b.Ri_Pr_Guid and a.Rp_State='查询' and convert(varchar(10),a.Rp_Datetime,121)>=@BegDate and convert(varchar(10),a.Rp_Datetime,121)<=@EndDate
                    left join ERP_Base_MaterielUnit d on b.Ri_Pu_Guid=d.Pu_Guid
            )
      ,0))  
    end
    )   as 采购退货, 
--销售部分
   (case
    when @IslessThanBeginUseDate=1 and @IsMoreThanNowDate=1  then 销售出货
    else   
     (isnull(
            ( select sum(isnull(b.Oi_Quantity,0)*isnull(d.Pu_BaseUnitNum,0)) 
              from  Sale_OutStorage a left join Sale_OutStorageItem b on a.Os_Guid=b.Oi_Os_Guid and 
                    aaaa.St_Guid=b.Oi_St_Guid and aaaa.Pr_Guid=b.Oi_Pr_Guid and a.Os_State='查询' and convert(varchar(10),a.Os_Datetime,121)>=@BegDate and convert(varchar(10),a.Os_Datetime,121)<=@EndDate
                    left join ERP_Base_MaterielUnit d on b.Oi_Pu_Guid=d.Pu_Guid
            )
      ,0))  
    end
    )   as 销售出货,
   (case
    when @IslessThanBeginUseDate=1 and @IsMoreThanNowDate=1  then 销售退货
    else   
     (isnull(
            ( select sum(isnull(b.Ri_Quantity,0)*isnull(d.Pu_BaseUnitNum,0)) 
              from  Sale_ReturnProduct a left join Sale_ReturnProductItem b on a.Rp_Guid=b.Ri_Rp_Guid and 
                    aaaa.St_Guid=b.Ri_St_Guid and aaaa.Pr_Guid=b.Ri_Pr_Guid and a.Rp_State='查询' and convert(varchar(10),a.Rp_Datetime,121)>=@BegDate and convert(varchar(10),a.Rp_Datetime,121)<=@EndDate
                    left join ERP_Base_MaterielUnit d on b.Ri_Pu_Guid=d.Pu_Guid
            )
      ,0))  
    end
    )   as 销售退货, 
--生产部分

   (case
    when @IslessThanBeginUseDate=1 and @IsMoreThanNowDate=1  then 生产完工入库
    else   
     (isnull(
            ( select sum(isnull(b.Oi_Quantity,0)*isnull(d.Pu_BaseUnitNum,0)) 
              from  Produce_Order a left join Produce_OrderItem b on a.Or_Guid=b.Oi_Or_Guid and 
                    aaaa.St_Guid=b.Oi_St_Guid and aaaa.Pr_Guid=b.Oi_Pr_Guid and a.Or_FinishingState='查询' and convert(varchar(10),a.Or_FinishingInDatetime,121)>=@BegDate and convert(varchar(10),a.Or_FinishingInDatetime,121)<=@EndDate
                    left join ERP_Base_MaterielUnit d on b.Oi_Pu_Guid=d.Pu_Guid
            )
      ,0))  
    end
    )   as 生产完工入库,
   (case
    when @IslessThanBeginUseDate=1 and @IsMoreThanNowDate=1  then 生产原料出库
    else   
     (isnull(
            ( select sum(isnull(b.Ob_Quantity,0)*isnull(d.Pu_BaseUnitNum,0)) 
              from  Produce_Order a left join Produce_OrderBom b on a.Or_Guid=b.Ob_Or_Guid and 
                    aaaa.St_Guid=b.Ob_St_Guid and aaaa.Pr_Guid=b.Ob_Pr_Guid and a.Or_BomState='查询' and convert(varchar(10),a.Or_BomOutDatetime,121)>=@BegDate and convert(varchar(10),a.Or_BomOutDatetime,121)<=@EndDate
                    left join ERP_Base_MaterielUnit d on b.Ob_Pu_Guid=d.Pu_Guid
            )
      ,0))  
    end
    )   as 生产原料出库,
--转库
   (case
    when @IslessThanBeginUseDate=1 and @IsMoreThanNowDate=1  then 转库入库
    else   
     (isnull(
            ( select sum(isnull(b.Mi_Quantity,0)*isnull(d.Pu_BaseUnitNum,0))  
              from  Storage_MoveStorage a left join Storage_MoveStorageItem b on a.Ms_Guid=b.Mi_Ms_Guid and 
                    aaaa.St_Guid=b.Mi_In_St_Guid and aaaa.Pr_Guid=b.Mi_Pr_Guid and a.Ms_State='查询' and convert(varchar(10),a.Ms_Datetime,121)>=@BegDate and convert(varchar(10),a.Ms_Datetime,121)<=@EndDate
                    left join ERP_Base_MaterielUnit d on b.Mi_Pu_Guid=d.Pu_Guid
            )
      ,0))  
    end
    )   as 转库入库,
   (case
    when @IslessThanBeginUseDate=1 and @IsMoreThanNowDate=1  then 转库出库
    else   
     (isnull(
            ( select sum(isnull(b.Mi_Quantity,0)*isnull(d.Pu_BaseUnitNum,0))  
              from  Storage_MoveStorage a left join Storage_MoveStorageItem b on a.Ms_Guid=b.Mi_Ms_Guid and 
                    aaaa.St_Guid=b.Mi_Out_St_Guid and aaaa.Pr_Guid=b.Mi_Pr_Guid and a.Ms_State='查询' and convert(varchar(10),a.Ms_Datetime,121)>=@BegDate and convert(varchar(10),a.Ms_Datetime,121)<=@EndDate
                    left join ERP_Base_MaterielUnit d on b.Mi_Pu_Guid=d.Pu_Guid
            )
      ,0))  
    end
    )   as 转库出库,     
--其它出入库
   (case
    when @IslessThanBeginUseDate=1 and @IsMoreThanNowDate=1  then 自定义入库
    else   
     (isnull(
            ( select sum(isnull(b.Oi_Quantity,0)*isnull(d.Pu_BaseUnitNum,0))  
              from  Storage_OtherInStorage a left join Storage_OtherInStorageItem b on a.Oi_Guid=b.Oi_Oi_Guid and 
                    aaaa.St_Guid=b.Oi_St_Guid and aaaa.Pr_Guid=b.Oi_Pr_Guid and a.Oi_State='查询' and convert(varchar(10),a.Oi_Datetime,121)>=@BegDate and convert(varchar(10),a.Oi_Datetime,121)<=@EndDate
                    left join ERP_Base_MaterielUnit d on b.Oi_Pu_Guid=d.Pu_Guid
            )
      ,0))  
    end
    )   as 自定义入库,
   (case
    when @IslessThanBeginUseDate=1 and @IsMoreThanNowDate=1  then 自定义出库
    else   
     (isnull(
            ( select sum(isnull(b.Oi_Quantity,0)*isnull(d.Pu_BaseUnitNum,0)) 
              from  Storage_OtherOutStorage a left join Storage_OtherOutStorageItem b on a.Oo_Guid=b.Oi_Oo_Guid and 
                    aaaa.St_Guid=b.Oi_St_Guid and aaaa.Pr_Guid=b.Oi_Pr_Guid and a.Oo_State='查询' and convert(varchar(10),a.Oo_Datetime,121)>=@BegDate and convert(varchar(10),a.Oo_Datetime,121)<=@EndDate
                    left join ERP_Base_MaterielUnit d on b.Oi_Pu_Guid=d.Pu_Guid
            )
      ,0))  
    end
    )   as 自定义出库,       
   (case
    when @IslessThanBeginUseDate=1 and @IsMoreThanNowDate=1  then 维修出库
    else   
     (isnull(
            ( select sum(isnull(b.Oi_Quantity,0)*isnull(d.Pu_BaseUnitNum,0)) 
              from  Service_Order a left join Service_OrderItem b on a.Or_Guid=b.Oi_Or_Guid and 
                    aaaa.St_Guid=b.Oi_St_Guid and aaaa.Pr_Guid=b.Oi_Pr_Guid and a.Or_State='查询' and convert(varchar(10),a.Or_FactCompDatetime,121)>@EndDate and convert(varchar(10),a.Or_FactCompDatetime,121)<=@EndDate
                    left join ERP_Base_MaterielUnit d on b.Oi_Pu_Guid=d.Pu_Guid
            )
      ,0))  
    end
    ) as 维修出库
from ##Proc_QueryCurStorage_temp1 aaaa
) bbbb


 	select a.*,
	           case
	           when @IsViewStorageMoney=1 then isnull(b.采购平均单价,0) 
	           else 0 
	           end as  采购平均单价, 
	           case
	           when @IsViewStorageMoney=1 then isnull(a.期未库存,0)*isnull(b.采购平均单价,0) 
	           else 0 
	           end as  库存资金占用   
  from ##Proc_QueryCurStorage a left join  
	     (
	     select Oi_Pr_Guid,  
	
	           case
	           when  sum(isnull(采购数量,0))=0 then 0
	           else round(sum(isnull(产品金额,0))/sum(isnull(采购数量,0)) ,4) 
	           end as  采购平均单价 
	
	      from                                                                                       
	     (
	     select Oi_Pr_Guid,                                                                                       
	       isnull(f.Oi_Quantity ,0)*(isnull(h.Pu_BaseUnitNum,0)) as 采购数量 ,                         
	       isnull(f.Oi_Quantity,0)*isnull(f.Oi_Price,0)*(1-isnull(f.Oi_Discount,0)) as 产品金额    
	     from Buy_Order a                                                                             
	          left join Buy_OrderItem        f on a.Or_Guid        =f.Oi_Or_Guid                       
	          left join ERP_Base_MaterielUnit     h on f.Oi_Pu_Guid     =h.Pu_Guid                          
	     where  a.Or_State='查询' and  convert(varchar(10),a.Or_Datetime,121) >=convert(varchar(10),@BeginUseDate,121)  and convert(varchar(10),a.Or_Datetime,121)<=convert(varchar(10),@EndDate,121)         
	     ) a 
	     group by  Oi_Pr_Guid
	    ) b on a.Pr_Guid=b.Oi_Pr_Guid
	
	order by  a.产品分类,a.产品名称 
go
 

--查询总计的实际库存,以产品为单位
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_QuerySumCurStorage]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Proc_QuerySumCurStorage]
go
CREATE PROCEDURE [dbo].[Proc_QuerySumCurStorage]
(
@BegDate varchar(100),@EndDate varchar(100),@Pr_Guid varchar(100)='',@IsViewStorageMoney bit=0
)  
AS

declare @NowDate varchar(100)
declare @BeginUseDate datetime
declare @IslessThanBeginUseDate bit
declare @IsMoreThanNowDate bit

set @NowDate=convert(varchar(10),getdate(),121)
select @BeginUseDate=SYSVALUE  from YBSYSINFO where YBSYSNAME='BeginUseDate'


if @BegDate<=@BeginUseDate 
  set @IslessThanBeginUseDate=1 
else
  set @IslessThanBeginUseDate=0 

if @EndDate>=@NowDate 
  set @IsMoreThanNowDate=1 
else
  set @IsMoreThanNowDate=0 


if exists (select * from tempdb.dbo.sysobjects where id = object_id(N'tempdb.[dbo].[##Proc_QuerySumCurStorage_temp1]'))
  drop table  ##Proc_QuerySumCurStorage_temp1

select *    
into ##Proc_QuerySumCurStorage_temp1
from 
(
select
  max(ap.Pubid) as Pubid, max(ap.Pr_Guid) as  Pr_Guid,max(ap.Pr_Ps_Guid) as Pr_Ps_Guid,max(Pu.Pu_Guid) as Pu_Guid,
  (select top 1 Ps_Name from ERP_Base_MaterielSort where Ps_Guid=max(ap.Pr_Ps_Guid)) as 产品分类,
  max(ap.Pr_Code) as 产品编号,max(ap.Pr_Name) as 产品名称,max(pu.Pu_Name) as 基本单位,max(ap.Pr_From) as  产品来源,
  isnull(max(isnull(Pr_MInStorageNum,0)),0) as 最小库存,isnull(max(isnull(Pr_MaxStorageNum,0)),0) as 最大库存,
  isnull(max(isnull(Pr_BuyOrderWillIn,0)),0) as 采购单待入库,isnull(max(isnull(Pr_SaleOrderWillOut,0)),0) as 销售单待出库,
  isnull(max(isnull(Pr_ProduceWillIn,0)),0) as 生产待入库   ,isnull(max(isnull(Pr_ProduceWillOut,0)),0) as 生产原料待出库,

  isnull(sum(isnull(Sp_BegStorage,0)),0) as 期初库存        ,isnull(sum(isnull(Sp_Buy_Get,0)),0) as 采购收货,
  isnull(sum(isnull(Sp_Buy_Return,0)),0) as 采购退货        ,isnull(sum(isnull(Sp_Sale_Out,0)),0) as 销售出货,
  isnull(sum(isnull(Sp_Sale_Return,0)),0) as 销售退货       ,isnull(sum(isnull(Sp_Produce_In,0)),0) as 生产完工入库,
  isnull(sum(isnull(Sp_Produce_Out,0)),0) as 生产原料出库   ,
  isnull(sum(isnull(Sp_Storage_OtherIn,0)),0) as 自定义入库,
  isnull(sum(isnull(Sp_Storage_OtherOut,0)),0) as 自定义出库  ,
  isnull(sum(isnull(Sp_Storage_MoveIn,0)),0) as 转库入库,
  isnull(sum(isnull(Sp_Storage_MoveOut,0)),0) as 转库出库  ,
  isnull(sum(isnull(Sp_Service_Out,0)),0) as 维修出库,
  isnull(sum(isnull(Sp_EndStorage,0)),0) as 期未库存
from ERP_Base_Materiel ap 
            left join ERP_Base_MaterielUnit pu on pu.Pu_Pr_Guid=ap.Pr_Guid and pu.Pu_IsBaseUnit=1 
            left join Storage_StorageProduct sp on ap.Pr_Guid=sp.Sp_Pr_Guid 
where @Pr_Guid='' or ap.Pr_Guid=@Pr_Guid
group by ap.Pr_Guid
) aaaa
order by 产品分类,产品名称
 
if exists (select * from tempdb.dbo.sysobjects where id = object_id(N'tempdb.[dbo].[##Proc_QuerySumCurStorage]'))
  drop table  ##Proc_QuerySumCurStorage
  
select
   Pubid ,  Pr_Guid , Pr_Ps_Guid , Pu_Guid ,产品分类,
   产品编号,  产品名称, 基本单位,  产品来源,最小库存,最大库存,
   采购单待入库, 销售单待出库,
   生产待入库, 生产原料待出库,

      (
       case
       when @IslessThanBeginUseDate=1  then 期初库存
       else 
          期未库存-(采购收货-采购退货-销售出货+销售退货+生产完工入库-生产原料出库+自定义入库-自定义出库+转库入库-转库出库-维修出库)
       end
       ) as 期初库存,
      采购收货,采购退货,销售出货,销售退货,生产完工入库,生产原料出库,自定义入库,自定义出库,转库入库,转库出库,维修出库,期未库存
into ##Proc_QuerySumCurStorage
    
from
(
select 
   Pubid ,  Pr_Guid , Pr_Ps_Guid , Pu_Guid ,产品分类,
   产品编号,  产品名称, 基本单位,  产品来源,最小库存,最大库存,
   采购单待入库, 销售单待出库,
   生产待入库, 生产原料待出库,

       期初库存,   
       case
       when @IsMoreThanNowDate=1  then 期未库存
       else
         期未库存-  
       (isnull(
              ( select sum(isnull(b.Gi_Quantity,0)*isnull(d.Pu_BaseUnitNum,0)) 
                from  Buy_GetProduct a left join Buy_GetProductItem b on a.Gp_Guid=b.Gi_Gp_Guid and 
                      aaaa.Pr_Guid=b.Gi_Pr_Guid and a.Gp_State='查询' and convert(varchar(10),a.Gp_Datetime,121)>@EndDate 
                      left join ERP_Base_MaterielUnit d on  b.Gi_Pu_Guid=d.Pu_Guid
              )
        ,0))  --采购收货
        +
        (isnull(
              ( select sum(isnull(b.Ri_Quantity,0)*isnull(d.Pu_BaseUnitNum,0)) 
                from  Buy_ReturnProduct a left join Buy_ReturnProductItem b on a.Rp_Guid=b.Ri_Rp_Guid and 
                      aaaa.Pr_Guid=b.Ri_Pr_Guid and a.Rp_State='查询' and convert(varchar(10),a.Rp_Datetime,121)>@EndDate  
                      left join ERP_Base_MaterielUnit d on b.Ri_Pu_Guid=d.Pu_Guid
              )
        ,0))  -- 采购退货
        +   
       (isnull(
              ( select sum(isnull(b.Oi_Quantity,0)*isnull(d.Pu_BaseUnitNum,0)) 
                from  Sale_OutStorage a left join Sale_OutStorageItem b on a.Os_Guid=b.Oi_Os_Guid and 
                      aaaa.Pr_Guid=b.Oi_Pr_Guid and a.Os_State='查询' and convert(varchar(10),a.Os_Datetime,121)>@EndDate  
                      left join ERP_Base_MaterielUnit d on b.Oi_Pu_Guid=d.Pu_Guid
              )
        ,0))   --销售出货
       -
      (isnull(
              ( select sum(isnull(b.Ri_Quantity,0)*isnull(d.Pu_BaseUnitNum,0)) 
                from  Sale_ReturnProduct a left join Sale_ReturnProductItem b on a.Rp_Guid=b.Ri_Rp_Guid and 
                      aaaa.Pr_Guid=b.Ri_Pr_Guid and a.Rp_State='查询' and convert(varchar(10),a.Rp_Datetime,121)>@EndDate  
                      left join ERP_Base_MaterielUnit d on  b.Ri_Pu_Guid=d.Pu_Guid
              )
        ,0))   --销售退货
        -
       (isnull(
              ( select sum(isnull(b.Oi_Quantity,0)*isnull(d.Pu_BaseUnitNum,0)) 
                from  Produce_Order a left join Produce_OrderItem b on a.Or_Guid=b.Oi_Or_Guid and 
                      aaaa.Pr_Guid=b.Oi_Pr_Guid and a.Or_FinishingState='查询' and convert(varchar(10),a.Or_FinishingInDatetime,121)>@EndDate  
                      left join ERP_Base_MaterielUnit d on b.Oi_Pu_Guid=d.Pu_Guid
              )
        ,0)) --生产完工入库  
        +  
       (isnull(
              ( select sum(isnull(b.Ob_Quantity,0)*isnull(d.Pu_BaseUnitNum,0)) 
                from  Produce_Order a left join Produce_OrderBom b on a.Or_Guid=b.Ob_Or_Guid and 
                      aaaa.Pr_Guid=b.Ob_Pr_Guid and a.Or_BomState='查询' and convert(varchar(10),a.Or_BomOutDatetime,121)>@EndDate  
                      left join ERP_Base_MaterielUnit d on b.Ob_Pu_Guid=d.Pu_Guid
              )
        ,0)) --生产原料出库  
       -  
       (isnull(
              ( select sum(isnull(b.Oi_Quantity,0)*isnull(d.Pu_BaseUnitNum,0))  
                from  Storage_OtherInStorage a left join Storage_OtherInStorageItem b on a.Oi_Guid=b.Oi_Oi_Guid and 
                      aaaa.Pr_Guid=b.Oi_Pr_Guid and a.Oi_State='查询' and convert(varchar(10),a.Oi_Datetime,121)>@EndDate  
                      left join ERP_Base_MaterielUnit d on b.Oi_Pu_Guid=d.Pu_Guid
              )
        ,0)) --自定义入库  
        +
       (isnull(
              ( select sum(isnull(b.Oi_Quantity,0)*isnull(d.Pu_BaseUnitNum,0)) 
                from  Storage_OtherOutStorage a left join Storage_OtherOutStorageItem b on a.Oo_Guid=b.Oi_Oo_Guid and 
                      aaaa.Pr_Guid=b.Oi_Pr_Guid and a.Oo_State='查询' and convert(varchar(10),a.Oo_Datetime,121)>@EndDate 
                      left join ERP_Base_MaterielUnit d on b.Oi_Pu_Guid=d.Pu_Guid
              )
        ,0)) --自定义出库 
       -  
       (isnull(
              ( select sum(isnull(b.Mi_Quantity,0)*isnull(d.Pu_BaseUnitNum,0))  
                from  Storage_MoveStorage a left join Storage_MoveStorageItem b on a.Ms_Guid=b.Mi_Ms_Guid and 
                      aaaa.Pr_Guid=b.Mi_Pr_Guid and a.Ms_State='查询' and convert(varchar(10),a.Ms_Datetime,121)>@EndDate  
                      left join ERP_Base_MaterielUnit d on b.Mi_Pu_Guid=d.Pu_Guid
              )
        ,0)) --转库入库  
        +
       (isnull(
              ( select sum(isnull(b.Mi_Quantity,0)*isnull(d.Pu_BaseUnitNum,0))  
                from  Storage_MoveStorage a left join Storage_MoveStorageItem b on a.Ms_Guid=b.Mi_Ms_Guid and 
                      aaaa.Pr_Guid=b.Mi_Pr_Guid and a.Ms_State='查询' and convert(varchar(10),a.Ms_Datetime,121)>@EndDate  
                      left join ERP_Base_MaterielUnit d on b.Mi_Pu_Guid=d.Pu_Guid
              )
        ,0)) --转库出库  
        +   
       (isnull(
              ( select sum(isnull(b.Oi_Quantity,0)*isnull(d.Pu_BaseUnitNum,0)) 
                from  Service_Order a left join Service_OrderItem b on a.Or_Guid=b.Oi_Or_Guid and 
                      aaaa.Pr_Guid=b.Oi_Pr_Guid and a.Or_State='查询' and convert(varchar(10),a.Or_FactCompDatetime,121)>@EndDate 
                      left join ERP_Base_MaterielUnit d on b.Oi_Pu_Guid=d.Pu_Guid
              )
        ,0))  --维修出库
       end as 期未库存,   
--期内部分
--采购部分

   (case
    when @IslessThanBeginUseDate=1 and @IsMoreThanNowDate=1  then 采购收货
    else   
     (isnull(
            ( select sum(isnull(b.Gi_Quantity,0)*isnull(d.Pu_BaseUnitNum,0)) 
              from  Buy_GetProduct a left join Buy_GetProductItem b on a.Gp_Guid=b.Gi_Gp_Guid and 
                    aaaa.Pr_Guid=b.Gi_Pr_Guid and a.Gp_State='查询' and convert(varchar(10),a.Gp_Datetime,121)>=@BegDate and convert(varchar(10),a.Gp_Datetime,121)<=@EndDate
                    left join ERP_Base_MaterielUnit d on b.Gi_Pu_Guid=d.Pu_Guid
            )
      ,0))  
    end
    )  as 采购收货,
   (case
    when @IslessThanBeginUseDate=1 and @IsMoreThanNowDate=1  then 采购退货
    else   
     (isnull(
            ( select sum(isnull(b.Ri_Quantity,0)*isnull(d.Pu_BaseUnitNum,0)) 
              from  Buy_ReturnProduct a left join Buy_ReturnProductItem b on a.Rp_Guid=b.Ri_Rp_Guid and 
                    aaaa.Pr_Guid=b.Ri_Pr_Guid and a.Rp_State='查询' and convert(varchar(10),a.Rp_Datetime,121)>=@BegDate and convert(varchar(10),a.Rp_Datetime,121)<=@EndDate
                    left join ERP_Base_MaterielUnit d on b.Ri_Pu_Guid=d.Pu_Guid
            )
      ,0))  
    end
    )   as 采购退货, 
--销售部分
   (case
    when @IslessThanBeginUseDate=1 and @IsMoreThanNowDate=1  then 销售出货
    else   
     (isnull(
            ( select sum(isnull(b.Oi_Quantity,0)*isnull(d.Pu_BaseUnitNum,0)) 
              from  Sale_OutStorage a left join Sale_OutStorageItem b on a.Os_Guid=b.Oi_Os_Guid and 
                    aaaa.Pr_Guid=b.Oi_Pr_Guid and a.Os_State='查询' and convert(varchar(10),a.Os_Datetime,121)>=@BegDate and convert(varchar(10),a.Os_Datetime,121)<=@EndDate
                    left join ERP_Base_MaterielUnit d on b.Oi_Pu_Guid=d.Pu_Guid
            )
      ,0))  
    end
    )   as 销售出货,
   (case
    when @IslessThanBeginUseDate=1 and @IsMoreThanNowDate=1  then 销售退货
    else   
     (isnull(
            ( select sum(isnull(b.Ri_Quantity,0)*isnull(d.Pu_BaseUnitNum,0)) 
              from  Sale_ReturnProduct a left join Sale_ReturnProductItem b on a.Rp_Guid=b.Ri_Rp_Guid and 
                    aaaa.Pr_Guid=b.Ri_Pr_Guid and a.Rp_State='查询' and convert(varchar(10),a.Rp_Datetime,121)>=@BegDate and convert(varchar(10),a.Rp_Datetime,121)<=@EndDate
                    left join ERP_Base_MaterielUnit d on b.Ri_Pu_Guid=d.Pu_Guid
            )
      ,0))  
    end
    )   as 销售退货, 
--生产部分

   (case
    when @IslessThanBeginUseDate=1 and @IsMoreThanNowDate=1  then 生产完工入库
    else   
     (isnull(
            ( select sum(isnull(b.Oi_Quantity,0)*isnull(d.Pu_BaseUnitNum,0)) 
              from  Produce_Order a left join Produce_OrderItem b on a.Or_Guid=b.Oi_Or_Guid and 
                    aaaa.Pr_Guid=b.Oi_Pr_Guid and a.Or_FinishingState='查询' and convert(varchar(10),a.Or_FinishingInDatetime,121)>=@BegDate and convert(varchar(10),a.Or_FinishingInDatetime,121)<=@EndDate
                    left join ERP_Base_MaterielUnit d on b.Oi_Pu_Guid=d.Pu_Guid
            )
      ,0))  
    end
    )   as 生产完工入库,
   (case
    when @IslessThanBeginUseDate=1 and @IsMoreThanNowDate=1  then 生产原料出库
    else   
     (isnull(
            ( select sum(isnull(b.Ob_Quantity,0)*isnull(d.Pu_BaseUnitNum,0)) 
              from  Produce_Order a left join Produce_OrderBom b on a.Or_Guid=b.Ob_Or_Guid and 
                    aaaa.Pr_Guid=b.Ob_Pr_Guid and a.Or_BomState='查询' and convert(varchar(10),a.Or_BomOutDatetime,121)>=@BegDate and convert(varchar(10),a.Or_BomOutDatetime,121)<=@EndDate
                    left join ERP_Base_MaterielUnit d on b.Ob_Pu_Guid=d.Pu_Guid
            )
      ,0))  
    end
    )   as 生产原料出库,

--转库
   (case
    when @IslessThanBeginUseDate=1 and @IsMoreThanNowDate=1  then 转库入库
    else   
     (isnull(
            ( select sum(isnull(b.Mi_Quantity,0)*isnull(d.Pu_BaseUnitNum,0))  
              from  Storage_MoveStorage a left join Storage_MoveStorageItem b on a.Ms_Guid=b.Mi_Ms_Guid and 
                    aaaa.Pr_Guid=b.Mi_Pr_Guid and a.Ms_State='查询' and convert(varchar(10),a.Ms_Datetime,121)>=@BegDate and convert(varchar(10),a.Ms_Datetime,121)<=@EndDate
                    left join ERP_Base_MaterielUnit d on b.Mi_Pu_Guid=d.Pu_Guid
            )
      ,0))  
    end
    )   as 转库入库,
   (case
    when @IslessThanBeginUseDate=1 and @IsMoreThanNowDate=1  then 转库出库
    else   
     (isnull(
            ( select sum(isnull(b.Mi_Quantity,0)*isnull(d.Pu_BaseUnitNum,0))  
              from  Storage_MoveStorage a left join Storage_MoveStorageItem b on a.Ms_Guid=b.Mi_Ms_Guid and 
                    aaaa.Pr_Guid=b.Mi_Pr_Guid and a.Ms_State='查询' and convert(varchar(10),a.Ms_Datetime,121)>=@BegDate and convert(varchar(10),a.Ms_Datetime,121)<=@EndDate
                    left join ERP_Base_MaterielUnit d on b.Mi_Pu_Guid=d.Pu_Guid
            )
      ,0))  
    end
    )   as 转库出库,     


--其它出入库
   (case
    when @IslessThanBeginUseDate=1 and @IsMoreThanNowDate=1  then 自定义入库
    else   
     (isnull(
            ( select sum(isnull(b.Oi_Quantity,0)*isnull(d.Pu_BaseUnitNum,0))  
              from  Storage_OtherInStorage a left join Storage_OtherInStorageItem b on a.Oi_Guid=b.Oi_Oi_Guid and 
                    aaaa.Pr_Guid=b.Oi_Pr_Guid and a.Oi_State='查询' and convert(varchar(10),a.Oi_Datetime,121)>=@BegDate and convert(varchar(10),a.Oi_Datetime,121)<=@EndDate
                    left join ERP_Base_MaterielUnit d on b.Oi_Pu_Guid=d.Pu_Guid
            )
      ,0))  
    end
    )   as 自定义入库,
   (case
    when @IslessThanBeginUseDate=1 and @IsMoreThanNowDate=1  then 自定义出库
    else   
     (isnull(
            ( select sum(isnull(b.Oi_Quantity,0)*isnull(d.Pu_BaseUnitNum,0)) 
              from  Storage_OtherOutStorage a left join Storage_OtherOutStorageItem b on a.Oo_Guid=b.Oi_Oo_Guid and 
                    aaaa.Pr_Guid=b.Oi_Pr_Guid and a.Oo_State='查询' and convert(varchar(10),a.Oo_Datetime,121)>=@BegDate and convert(varchar(10),a.Oo_Datetime,121)<=@EndDate
                    left join ERP_Base_MaterielUnit d on b.Oi_Pu_Guid=d.Pu_Guid
            )
      ,0))  
    end
    )   as 自定义出库,     

   (case
    when @IslessThanBeginUseDate=1 and @IsMoreThanNowDate=1  then 维修出库
    else   
     (isnull(
            ( select sum(isnull(b.Oi_Quantity,0)*isnull(d.Pu_BaseUnitNum,0)) 
              from  Service_Order a left join Service_OrderItem b on a.Or_Guid=b.Oi_Or_Guid and 
                    aaaa.Pr_Guid=b.Oi_Pr_Guid and a.Or_State='查询' and convert(varchar(10),a.Or_FactCompDatetime,121)>@EndDate and convert(varchar(10),a.Or_FactCompDatetime,121)<=@EndDate
                    left join ERP_Base_MaterielUnit d on b.Oi_Pu_Guid=d.Pu_Guid
            )
      ,0))  
    end
    ) as 维修出库
from ##Proc_QuerySumCurStorage_temp1 aaaa
) bbbb


 
	select a.*,
	           case
	           when @IsViewStorageMoney=1 then isnull(b.采购平均单价,0) 
	           else 0 
	           end as  采购平均单价, 
	           case
	           when @IsViewStorageMoney=1 then isnull(a.期未库存,0)*isnull(b.采购平均单价,0) 
	           else 0 
	           end as  库存资金占用   
  from ##Proc_QuerySumCurStorage a left join  
	     (
	     select Oi_Pr_Guid,  
	
	           case
	           when  sum(isnull(采购数量,0))=0 then 0
	           else round(sum(isnull(产品金额,0))/sum(isnull(采购数量,0)) ,4) 
	           end as  采购平均单价 
	
	      from                                                                                       
	     (
	     select Oi_Pr_Guid,                                                                                       
	       isnull(f.Oi_Quantity ,0)*(isnull(h.Pu_BaseUnitNum,0)) as 采购数量 ,                         
	       isnull(f.Oi_Quantity,0)*isnull(f.Oi_Price,0)*(1-isnull(f.Oi_Discount,0)) as 产品金额    
	     from Buy_Order a                                                                             
	          left join Buy_OrderItem        f on a.Or_Guid        =f.Oi_Or_Guid                       
	          left join ERP_Base_MaterielUnit     h on f.Oi_Pu_Guid     =h.Pu_Guid                          
	     where  a.Or_State='查询' and  convert(varchar(10),a.Or_Datetime,121) >=convert(varchar(10),@BeginUseDate,121)  and convert(varchar(10),a.Or_Datetime,121)<=convert(varchar(10),@EndDate,121)         
	     ) a 
	     group by  Oi_Pr_Guid
	    ) b on a.Pr_Guid=b.Oi_Pr_Guid
	
	order by  a.产品分类,a.产品名称 

go





--查询总计的虚拟库存,以产品为单位
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_QuerySumWillStorage]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Proc_QuerySumWillStorage]
go
CREATE PROCEDURE [dbo].[Proc_QuerySumWillStorage]
(
@BegDate varchar(100),@EndDate varchar(100),@Pr_Guid varchar(100)='',
@IsMrp bit=0,@IsViewStorageMoney bit=0,@IsViewZero  bit=1
)  
AS
 
declare @NowDate varchar(100)
declare @BeginUseDate datetime
declare @IslessThanBeginUseDate bit
declare @IsMoreThanNowDate bit

set @NowDate=convert(varchar(10),getdate(),121)
select @BeginUseDate=SYSVALUE  from YBSYSINFO where YBSYSNAME='BeginUseDate'

if @BegDate<=@BeginUseDate 
  set @IslessThanBeginUseDate=1 
else
  set @IslessThanBeginUseDate=0 

if @EndDate>=@NowDate 
  set @IsMoreThanNowDate=1 
else
  set @IsMoreThanNowDate=0 

if exists (select * from tempdb.dbo.sysobjects where id = object_id(N'tempdb.[dbo].[##Proc_QuerySumWillStorage]'))
  drop table  ##Proc_QuerySumWillStorage
  
select * ,
				case 
				when 期未库存-最小库存+采购计划+生产计划>0 then 0
				else abs(期未库存-最小库存+采购计划+生产计划)
				end as 需求数量
into ##Proc_QuerySumWillStorage
from
(
select 
       Pubid ,  Pr_Guid , Pr_Ps_Guid, Pu_Guid ,产品分类,
       产品编号,  产品名称, 基本单位,  产品来源,最小库存,最大库存,采购计划,生产计划,
       期初库存,
 
       采购单待入库,采购收货,采购退货,销售单待出库,销售出货,销售退货,生产待入库,生产完工入库,生产原料待出库,生产原料出库,自定义入库,自定义出库,转库入库,转库出库,维修出库,
       期未库存+(采购单待入库-销售单待出库+生产待入库-生产原料待出库) as  期未库存
 
from 
(

select 
       Pubid ,  Pr_Guid , Pr_Ps_Guid , Pu_Guid ,产品分类,
       产品编号,  产品名称, 基本单位,  产品来源,最小库存,最大库存,
       case
       when @IsMrp=1 then
       (isnull(
              ( 
								select sum(tempSum) from
								(
								select sum(isnull(b.Pi_Quantity,0)*isnull((d.Pu_BaseUnitNum),0))-
								       isnull((select  sum(tempSum) from ( select sum(c.Oi_Quantity)*max(isnull(e.Pu_BaseUnitNum,0))  as tempSum  from Buy_OrderItem c left join ERP_Base_MaterielUnit e on c.Oi_Pu_Guid=e.Pu_Guid  left join Buy_Order f on c.Oi_Or_Guid=f.Or_Guid where f.Or_State='查询' and   (b.Pi_Guid)=c.Oi_ComeFromItemCode  group by c.Oi_Guid ) as aa ),0) as tempSum
								from  Buy_Plan a left join Buy_PlanItem b on a.Pl_Guid=b.Pi_Pl_Guid and 
								      b.Pi_Pr_Guid=aaaa.Pr_Guid  
								      left join ERP_Base_MaterielUnit d on  b.Pi_Pu_Guid=d.Pu_Guid
								group by b.Pi_Pr_Guid,Pi_Guid 
								) aa
              )
        ,0))
       else 0
       end as 采购计划,
       case
       when @IsMrp=1 then
       (isnull(
              ( 
								select sum(tempSum) from
								(
								select sum(isnull(b.Pi_Quantity,0)*isnull((d.Pu_BaseUnitNum),0))-
								       isnull((select  sum(tempSum) from ( select sum(c.Oi_Quantity)*max(isnull(e.Pu_BaseUnitNum,0)) as tempSum   from Produce_OrderItem c left join ERP_Base_MaterielUnit e on c.Oi_Pu_Guid=e.Pu_Guid  left join Produce_Order f on c.Oi_Or_Guid=f.Or_Guid where f.Or_State='查询' and   (b.Pi_Guid)=c.Oi_ComeFromItemCode  group by c.Oi_Guid ) as aa ),0) as tempSum
								from  Produce_Plan a left join Produce_PlanItem b on a.Pl_Guid=b.Pi_Pl_Guid and 
								      b.Pi_Pr_Guid=aaaa.Pr_Guid   
								      left join ERP_Base_MaterielUnit d on  b.Pi_Pu_Guid=d.Pu_Guid
								group by b.Pi_Pr_Guid,Pi_Guid 
								) aa
              )
        ,0))
       else 0
       end  as 生产计划,

       期初库存,
       采购收货,采购退货,销售出货,销售退货,生产完工入库,生产原料出库,自定义入库,自定义出库,转库入库,转库出库,维修出库,

      期未库存,
       
	   (case
	    when @IslessThanBeginUseDate=1 and @IsMoreThanNowDate=1  then 采购单待入库
	    else   
			      采购单待入库 -
			       (isnull(
			              ( 
											select sum(tempSum) from
											(
											select sum(isnull(b.Oi_Quantity,0)*isnull((d.Pu_BaseUnitNum),0))-
											       isnull((select  sum(tempSum) from ( select sum(c.Gi_Quantity)*max(isnull(e.Pu_BaseUnitNum,0))  as tempSum  from Buy_GetProductItem c  left join ERP_Base_MaterielUnit e on  c.Gi_Pu_Guid=e.Pu_Guid   left join Buy_GetProduct f on c.Gi_Gp_Guid=f.Gp_Guid where f.Gp_State='查询' and    (b.Oi_Guid)=c.Gi_ComeFromItemCode  group by c.Gi_Guid ) as aa ),0) as tempSum 
											from  Buy_Order a left join Buy_OrderItem b on a.Or_Guid=b.Oi_Or_Guid and 
											      aaaa.Pr_Guid=b.Oi_Pr_Guid and a.Or_State='查询' and convert(varchar(10),a.Or_PlanGetDatetime,121)>@EndDate
											      left join ERP_Base_MaterielUnit d on b.Oi_Pu_Guid=d.Pu_Guid
											group by b.Oi_Pr_Guid,Oi_Guid 
											) aa
			              )
			        ,0))  
	    end
	    ) as 采购单待入库,

	   (case
	    when @IslessThanBeginUseDate=1 and @IsMoreThanNowDate=1  then 销售单待出库
	    else   
		       销售单待出库-
			       (isnull(
			              ( 
											select sum(tempSum) from
											(
											select sum(isnull(b.Oi_Quantity,0)*isnull((d.Pu_BaseUnitNum),0))-
											       isnull((select  sum(tempSum) from ( select  sum(c.Oi_Quantity)*max(isnull(e.Pu_BaseUnitNum,0))  as tempSum  from Sale_OutStorageItem c left join ERP_Base_MaterielUnit e on c.Oi_Pu_Guid=e.Pu_Guid  left join Sale_OutStorage f on c.Oi_Os_Guid=f.Os_Guid where f.Os_State='查询' and   (b.Oi_Guid)=c.Oi_ComeFromItemCode  group by c.Oi_Guid ) as aa ),0) as tempSum 
											from  Sale_Order a left join Sale_OrderItem b on a.Or_Guid=b.Oi_Or_Guid and 
											      aaaa.Pr_Guid=b.Oi_Pr_Guid and a.Or_State='查询' and convert(varchar(10),a.Or_PlanOutDatetime,121)>@EndDate
											      left join ERP_Base_MaterielUnit d on b.Oi_Pu_Guid=d.Pu_Guid
											group by b.Oi_Pr_Guid,Oi_Guid 
											) aa
			              )
			        ,0))  
	    end
	    ) as 销售单待出库,


	   (case
	    when @IslessThanBeginUseDate=1 and @IsMoreThanNowDate=1  then 生产待入库
	    else   
	        生产待入库-
		       (isnull(
		              ( select sum(isnull(b.Oi_Quantity,0)*isnull(d.Pu_BaseUnitNum,0)) 
		                from  Produce_Order a left join Produce_OrderItem b on a.Or_Guid=b.Oi_Or_Guid and 
		                      aaaa.Pr_Guid=b.Oi_Pr_Guid and a.Or_State='查询' and a.Or_FinishingState='录入' and convert(varchar(10),a.Or_PlanFinishingInDatetime,121)>@EndDate
		                      left join ERP_Base_MaterielUnit d on b.Oi_Pu_Guid=d.Pu_Guid
		              )
		        ,0))  
	    end
	    ) as 生产待入库,

	   (case
	    when @IslessThanBeginUseDate=1 and @IsMoreThanNowDate=1  then 生产原料待出库
	    else   
		       生产原料待出库- 
			       (isnull(
			              ( select sum(isnull(b.Ob_Quantity,0)*isnull(d.Pu_BaseUnitNum,0)) 
			                from  Produce_Order a left join Produce_OrderBom b on a.Or_Guid=b.Ob_Or_Guid and 
			                      aaaa.Pr_Guid=b.Ob_Pr_Guid and a.Or_BomWillOutState='查询' and   a.Or_BomState='录入' and convert(varchar(10),a.Or_PlanBomOutDatetime,121)>@EndDate
			                      left join ERP_Base_MaterielUnit d on b.Ob_Pu_Guid=d.Pu_Guid
			              )
			        ,0))   
	    end
	    ) as 生产原料待出库 
   
 
from ##Proc_QuerySumCurStorage aaaa
) bbbb
) cccc
 
 


	select a.*,
	           case
	           when @IsViewStorageMoney=1 then isnull(b.采购平均单价,0) 
	           else 0 
	           end as  采购平均单价, 
	           case
	           when @IsViewStorageMoney=1 then isnull(a.期未库存,0)*isnull(b.采购平均单价,0) 
	           else 0 
	           end as  库存资金占用   
  from ##Proc_QuerySumWillStorage a left join  
	     (
	     select Oi_Pr_Guid,  
	
	           case
	           when  sum(isnull(采购数量,0))=0 then 0
	           else round(sum(isnull(产品金额,0))/sum(isnull(采购数量,0)) ,4) 
	           end as  采购平均单价 
	
	      from                                                                                       
	     (
	     select Oi_Pr_Guid,                                                                                       
	       isnull(f.Oi_Quantity ,0)*(isnull(h.Pu_BaseUnitNum,0)) as 采购数量 ,                         
	       isnull(f.Oi_Quantity,0)*isnull(f.Oi_Price,0)*(1-isnull(f.Oi_Discount,0)) as 产品金额    
	     from Buy_Order a                                                                             
	          left join Buy_OrderItem        f on a.Or_Guid        =f.Oi_Or_Guid                       
	          left join ERP_Base_MaterielUnit     h on f.Oi_Pu_Guid     =h.Pu_Guid                          
	     where  a.Or_State='查询' and  convert(varchar(10),a.Or_Datetime,121) >=convert(varchar(10),@BeginUseDate,121)     and convert(varchar(10),a.Or_Datetime,121)<=convert(varchar(10),@EndDate,121)         
	     ) a 
	     group by  Oi_Pr_Guid
	    ) b on a.Pr_Guid=b.Oi_Pr_Guid
	where (@IsViewZero=1) or (需求数量<>0 and @IsViewZero=0)  


	order by  a.产品分类,a.产品名称 

go



if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_CreateSaleOrderProduct]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Proc_CreateSaleOrderProduct]
go
CREATE PROCEDURE [dbo].[Proc_CreateSaleOrderProduct]
(
@BegDate varchar(100),  @EndDate varchar(100)  
)   
as

  	 if exists (select * from tempdb.dbo.sysobjects where id = object_id(N'tempdb.[dbo].[##TBase_AnalyseForm_Sale_SaleOrderProduct]'))
		 drop table  ##TBase_AnalyseForm_Sale_SaleOrderProduct
select *,
       isnull(已出库数量,0)*isnull(Oi_SalePrice,0)*(1-isnull(Oi_Discount,0))  as 已出库金额, 
        (isnull(销售数量,0)-isnull(已出库数量,0))*isnull(Oi_SalePrice,0)*(1-isnull(Oi_Discount,0))  as 未出库金额, 
       isnull(已出库数量,0)*(isnull(Oi_SalePrice,0)*(1-isnull(Oi_Discount,0))-isnull(Oi_CostPrice,0))  as 已出库毛利 


into ##TBase_AnalyseForm_Sale_SaleOrderProduct
from 
(
     select                                                                                        
       newid()       as  Pubid  ,                                                                  
       a.Or_Code     as  订单编号  ,                                                               
       l.Cl_Name     as  销售人员  ,  
       a.Or_InputOperator as  录入人员           ,                                                             
       a.Or_Datetime as  签订时间  , 
       a.Or_CustBillCode as 客户单号,

       b.Cu_Type     as  客户类型  ,                                                               
       b.Cu_Name     as  客户名称  ,                                                               
       c.Cl_Name     as  客户联系人,                                                               
       d.Cu_Name     as  供方名称  ,                                                               
       e.Cl_Name     as  供方联系人,                                                               
                                                                                                   
       isnull(g.Pr_Code,'已删除')     as  产品编号  ,                                                               
       isnull(g.Pr_Name,'已删除')     as  产品名称  ,                                                               
       ( select top 1  Pu_Name  from ERP_Base_MaterielUnit where Pu_Pr_Guid=f.Oi_Pr_Guid  and Pu_IsBaseUnit=1                                                             
        )as  基本单位  ,                                                               
       isnull(f.Oi_Quantity ,0)*(isnull(h.Pu_BaseUnitNum,0)) as 销售数量 ,                         
       isnull((                                                                                    
	     select sum(Oi_Quantity)                                                                     
	     from                                                                                        
	      ( select sum(isnull(a.Oi_Quantity,0))*(max(isnull(b.Pu_BaseUnitNum,0))) as Oi_Quantity     
	        from Sale_OutStorageItem a left join ERP_Base_MaterielUnit b on a.Oi_Pu_Guid = b.Pu_Guid   
                                left join Sale_OutStorage c on a.Oi_Os_Guid=c.Os_Guid 
	        where  a.Oi_ComeFromItemCode =  f.Oi_Guid  and  c.Os_State='查询'                                        
	        group by a.Oi_Guid                                                                       
	       ) aa                                                                                      
	      )                                                                                          
             ,0) as 已出库数量,   
     (isnull(
            ( select sum(isnull(b.Ri_Quantity,0)*isnull(d.Pu_BaseUnitNum,0)) 
              from  Sale_OutStorageItem a left join Sale_ReturnProductItem b on a.Oi_Guid=b.Ri_ComeFromItemCode 
                                                            left join Sale_ReturnProduct  ab on  ab.Rp_Guid=b.Ri_Rp_Guid
                                                            left join ERP_Base_MaterielUnit d on b.Ri_Pu_Guid=d.Pu_Guid
                                                            left join Sale_OutStorage e on a.Oi_Os_Guid=e.Os_Guid

              where  a.Oi_ComeFromItemCode =  f.Oi_Guid and  e.Os_State='查询'  and ab.Rp_State='查询'      
            )
      ,0))  退货数量,
 
 

                                                            
       isnull(f.Oi_Discount,0) as 产品折扣,                                                      
       isnull(f.Oi_SalePrice,0) as 产品单价,                                                      
       isnull(f.Oi_Quantity,0)*isnull(f.Oi_SalePrice,0)*(1-isnull(f.Oi_Discount,0)) as 产品金额,   
       isnull(f.Oi_Quantity,0)*isnull(f.Oi_CostPrice,0) as 成本金额,                               
       isnull(a.Or_Discount,0)  as 订单折扣    ,                                                   

                                                              
	     convert(varchar(4),Or_Datetime,121)+'年' as 年,                                                   
	     cast(datepart(year,Or_Datetime) as varchar)+'年第'+cast(datepart(Month,Or_Datetime)/4+1 as varchar)+'季度' as 季度,                        
	     cast(datepart(year,Or_Datetime) as varchar)+'年第'+right('0'+ cast(datepart(Month,Or_Datetime) as varchar) ,2)  +'月'   as 月  ,                          
	     cast(datepart(year,Or_Datetime) as varchar)+'年第'+right('0'+ cast(datepart(week ,Or_Datetime) as varchar) ,2)  +'周'   as 周  ,                          
	     convert(varchar(10),Or_Datetime,121) as 日期,                                                       

       a.Or_GetMoneyState       as 收款状态    ,                                                   
       a.Or_PlanOutDatetime  as 计划出库时间,                                                   
                                                                                                   
       b.Cu_Code           as 客户编号,                                                            
                                                              
       b.Cu_PaymentMode    as 客户付款方式,                                                        
       b.Cu_PayOffMode     as 结算方式,                                                            
       b.Cu_PayOffModeDay  as 结算方式天数,                                                        
       b.Cu_MaxCreditMoney as 最大信用额,                                                          
       b.Cu_DutyPeople     as 负责人,                                                              
       isnull(b.Cu_Country,'')        as 国家,                                                                
       isnull(b.Cu_Province,'')       as 省份,                                                                
       isnull(b.Cu_ProvinceDWArea,'') as 地区,                                                                
       b.Cu_BankName       as 开户行,                                                              
       b.Cu_CreditCardCode as 帐号,                                                                
       b.Cu_TaxNumber      as 税号,                                                                
       b.Cu_EmailAddress   as 邮箱,                                                                
       b.Cu_WebAddress     as 网址,                                                                
                                                                                                   
       c.Cl_Handset      as 客户联系人手机,                                                        
       c.Cl_OfficePhone  as 客户联系人电话,                                                        
       c.Cl_Fax          as 客户联系人传真,                                                        
                                                                                                                
                                                                                                   
       e.Cl_Handset     as  供方联系人手机 ,                                                       
       e.Cl_OfficePhone as  供方联系人电话,                                                        
       e.Cl_Fax         as  供方联系人传真,                                                        
                                                                                                   
       f.Oi_CostPrice  as 成本价 ,  
f.Oi_SalePrice,
f.Oi_Discount,
f.Oi_CostPrice,                                                               
       f.Oi_SetupPrice as 安装价 ,                                                                 
       f.Oi_Remark     as 产品说明,                                                                
       k.Ps_Name       as 产品分类,  
                                                                
                                                                                                   
       b.Cu_Ps_Guid       ,                                                                        
       a.Or_Cu_Guid       ,                                                                        
       a.Or_Cl_Guid       ,                                                                        
       a.Or_Su_Guid       ,                                                                        
       a.Or_Sl_Guid       ,                                                                        
       a.Or_Guid          ,                                                                        
       f.Oi_Guid          ,                                                                        
       f.Oi_Pr_Guid       ,                                                                        
       f.Oi_Pu_Guid       ,                                                                        
       f.Oi_Ps_Guid       ,                                                                        
       a.Or_OperatorGuid  
  
     from Sale_Order a                                                                             
          left join Base_Cust            b on a.Or_Cu_Guid     =b.Cu_Guid                          
          left join Base_CustLinkMan     c on a.Or_Cl_Guid     =c.Cl_Guid                          
          left join Base_Cust            d on a.Or_Su_Guid     =d.Cu_Guid                          
          left join Base_CustLinkMan     e on a.Or_Sl_Guid     =e.Cl_Guid                          
          left join Sale_OrderItem       f on a.Or_Guid        =f.Oi_Or_Guid                       
          left join ERP_Base_Materiel         g on f.Oi_Pr_Guid     =g.Pr_Guid                          
          left join ERP_Base_MaterielUnit     h on f.Oi_Pu_Guid     =h.Pu_Guid                          
          left join ERP_Base_MaterielSort     k on f.Oi_Ps_Guid     =k.Ps_Guid                          
          left join Base_CustLinkMan     l on a.Or_OperatorGuid=l.Cl_Guid                          
     where  Or_State='查询' and convert(varchar(10),Or_Datetime,121) >= @BegDate and convert(varchar(10),Or_Datetime,121)<= @EndDate        
) aa

go
--Proc_CreateSaleOrderProduct  '2001-9-7', '2006-2-12'


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_CreateSaleOrder]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Proc_CreateSaleOrder]
go
CREATE PROCEDURE [dbo].[Proc_CreateSaleOrder]
(
@BegDate varchar(100),  @EndDate varchar(100)  
)   
as

if exists (select * from tempdb.dbo.sysobjects where id = object_id(N'tempdb.[dbo].[##TBase_AnalyseForm_Sale_SaleOrder]'))
  drop table  ##TBase_AnalyseForm_Sale_SaleOrder
    
select * ,产品金额-已收金额 as 未收金额
 into ##TBase_AnalyseForm_Sale_SaleOrder

from 
(
 select
          isnull((select sum(isnull(Og_Money,0))                                                      
                  from Sale_OrderGetMoney                                                             
                  where Og_Or_Guid= cc.Or_Guid  and Og_Type='正常'
                   )
                 ,0)  as 已收金额,          
          * 
 

             
     from
      (
       select
		       newid()               as  Pubid              ,
		       max(订单编号         ) as  订单编号           ,
		       max(销售人员         ) as  销售人员           ,
		       max(录入人员         ) as  录入人员           ,
		       max(签订时间         ) as  签订时间           ,
                                                         
		       max(客户名称         ) as  客户名称           ,
		       max(客户编号         ) as  客户编号           ,
		       max(客户类型         ) as  客户类型           ,
		       max(客户联系人       ) as  客户联系人         ,
		       max(供方名称         ) as  供方名称           ,
		       max(供方联系人       ) as  供方联系人         ,
                                                         
		       Sum(销售数量         ) as  销售数量           ,
		       Sum(已出库数量       ) as  已出库数量         ,
		       Sum(已出库金额       ) as  已出库金额         ,
		       Sum(未出库金额       ) as  未出库金额         ,
		       Sum(已出库毛利       ) as  已出库毛利         ,
 

		        Sum(销售数量         ) -Sum(已出库数量       ) as  未出库数量         ,
                                       sum(退货数量) as 退货数量,     

		       max(基本单位        ) as  基本单位            ,
		       avg(产品折扣         ) as  产品折扣           ,
		       Sum(产品金额         ) as  产品金额           ,
		       Sum(成本金额         ) as  成本金额           ,
		       max(订单折扣         ) as  订单折扣           ,
                                                         
			     max(年               ) as  年                 ,
			     max(季度             ) as  季度               ,
			     max(月               ) as  月                 ,
			     max(周               ) as  周                 ,
			     max(日期             ) as  日期               ,
                                                         
		       max(收款状态         ) as  收款状态           ,
		       max(计划出库时间     ) as  计划出库时间       ,

		       max(客户付款方式     ) as  客户付款方式       ,
		       max(结算方式         ) as  结算方式           ,
		       max(结算方式天数     ) as  结算方式天数       ,
		       max(最大信用额       ) as  最大信用额         ,
		       max(负责人           ) as  负责人             ,
		       max(国家             ) as  国家               ,
		       max(省份             ) as  省份               ,
		       max(地区             ) as  地区               ,
		       max(开户行           ) as  开户行             ,
		       max(帐号             ) as  帐号               ,
		       max(税号             ) as  税号               ,
		       max(邮箱             ) as  邮箱               ,
		       max(网址             ) as  网址               ,
                                                         
		       max(客户联系人手机   ) as  客户联系人手机     ,
		       max(客户联系人电话   ) as  客户联系人电话     ,
		       max(客户联系人传真   ) as  客户联系人传真     ,
                                                          
		       max(供方联系人手机   ) as  供方联系人手机     ,
		       max(供方联系人电话   ) as  供方联系人电话     ,
		       max(供方联系人传真   ) as  供方联系人传真     ,
                                                         
		       avg(成本价           ) as  成本价             ,
		       avg(安装价           ) as  安装价             , 
                                                          
		       max(Cu_Ps_Guid     ) as  Cu_Ps_Guid       ,
		       max(Or_Cu_Guid     ) as  Or_Cu_Guid       ,
		       max(Or_Cl_Guid     ) as  Or_Cl_Guid       ,
		       max(Or_Su_Guid     ) as  Or_Su_Guid       ,
		       max(Or_Sl_Guid     ) as  Or_Sl_Guid       ,
		       max(Or_Guid        ) as  Or_Guid          ,
		       max(Or_OperatorGuid) as  Or_OperatorGuid

       from ##TBase_AnalyseForm_Sale_SaleOrderProduct bb                                                                                            
       group by 订单编号
     ) cc                                                                                               
) dd


--Proc_CreateSaleOrder  '2001-9-7', '2006-2-12'


go


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_CreateBuyOrderProduct]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Proc_CreateBuyOrderProduct]
go
CREATE PROCEDURE [dbo].[Proc_CreateBuyOrderProduct]
(
@BegDate varchar(100),  @EndDate varchar(100)  
)   
as

if exists (select * from tempdb.dbo.sysobjects where id = object_id(N'tempdb.[dbo].[##TBase_AnalyseForm_Buy_BuyOrderProduct]'))
  drop table  ##TBase_AnalyseForm_Buy_BuyOrderProduct
      
select * ,
       isnull(已入库数量,0)*isnull(Oi_Price,0)*(1-isnull(Oi_Discount,0))  as 已入库金额,   
       (isnull(采购数量 ,0)-isnull(已入库数量,0))*isnull(Oi_Price,0)*(1-isnull(Oi_Discount,0))  as 未入库金额    

into ##TBase_AnalyseForm_Buy_BuyOrderProduct                                                                                           

from 
(
   select                                                                                        
       newid()       as  Pubid  ,                                                                  
       a.Or_Code     as  订单编号  ,                                                               
       l.Cl_Name     as  采购人员  ,                                                               
       a.Or_Datetime as  签订时间  , 

       b.Su_Name     as  供应商名称  ,                                                               
       c.Sl_Name     as  供应商联系人,                                                               
                                                                                                   
       isnull(g.Pr_Code,'已删除')     as  产品编号  ,                                                               
       isnull(g.Pr_Name,'已删除')     as  产品名称  ,                                                               
       ( select top 1 Pu_Name  from ERP_Base_MaterielUnit where Pu_Pr_Guid=f.Oi_Pr_Guid  and Pu_IsBaseUnit=1                                                             
        )as  基本单位  ,                                                               
       isnull(f.Oi_Quantity ,0)*(isnull(h.Pu_BaseUnitNum,0)) as 采购数量 ,                         
       isnull((                                                                                    
	     select sum(Oi_Quantity)                                                                     
	     from                                                                                        
	      ( select sum(isnull(a.Gi_Quantity,0))*(max(isnull(b.Pu_BaseUnitNum,0))) as Oi_Quantity     
	        from Buy_GetProductItem a left join ERP_Base_MaterielUnit b on a.Gi_Pu_Guid     =b.Pu_Guid   
	        where  a.Gi_ComeFromItemCode =  f.Oi_Guid                                                
	        group by a.Gi_Guid                                                                       
	       ) aa                                                                                       
	      )                                                                                          
             ,0) as 已入库数量,   

        

     (isnull(
            ( select sum(isnull(b.Ri_Quantity,0)*isnull(d.Pu_BaseUnitNum,0)) 
              from  Buy_GetProductItem a left join Buy_ReturnProductItem b on a.Gi_Guid=b.Ri_ComeFromItemCode 
                                                            left join Buy_ReturnProduct  ab on  ab.Rp_Guid=b.Ri_Rp_Guid
                                                            left join ERP_Base_MaterielUnit d on b.Ri_Pu_Guid=d.Pu_Guid
                                                            left join Buy_GetProduct e on a.Gi_Gp_Guid=e.Gp_Guid

              where  a.Gi_ComeFromItemCode =  f.Oi_Guid and  e.Gp_State='查询'  and ab.Rp_State='查询'      
            )
      ,0)) as 退货数量,


                                                                
       isnull(f.Oi_Price,0) as 产品单价,                                                      
       isnull(f.Oi_Discount,0) as 产品折扣,                                                      
       isnull(f.Oi_Quantity,0)*isnull(f.Oi_Price,0)*(1-isnull(f.Oi_Discount,0)) as 产品金额,   
       isnull(a.Or_Discount,0)  as 订单折扣    ,                                                   
                                                              
	     convert(varchar(4),Or_Datetime,121)+'年' as 年,                                                  
	     cast(datepart(year,Or_Datetime) as varchar)+'年第'+cast(datepart(Month,Or_Datetime)/4+1 as varchar)+'季度' as 季度,                        
	     cast(datepart(year,Or_Datetime) as varchar)+'年第'+right('0'+ cast(datepart(Month,Or_Datetime) as varchar) ,2)   +'月'   as 月  ,                          
	     cast(datepart(year,Or_Datetime) as varchar)+'年第'+right('0'+ cast(datepart(week ,Or_Datetime) as varchar) ,2)   +'周'   as 周  ,                          
	     convert(varchar(10),Or_Datetime,121) as 日期,                                                       

       a.Or_PayMoneyState       as 付款状态    ,                                                   
       a.Or_PlanGetDatetime  as 计划入库时间,                                                   
                                                                                                   
       b.Su_Code           as 供应商编号,                                                            
                                                              
       b.Su_PaymentMode    as 供应商付款方式,                                                        
       b.Su_PayOffMode     as 结算方式,                                                            
       b.Su_PayOffModeDay  as 结算方式天数,                                                        
       b.Su_DutyPeople     as 负责人,                                                              
       isnull(b.Su_Country,'')        as 国家,                                                                
       isnull(b.Su_Province,'')       as 省份,                                                                
       isnull(b.Su_ProvinceDWArea,'') as 地区,                                                                
       b.Su_BankName       as 开户行,                                                              
       b.Su_CreditCardCode as 帐号,                                                                                                                      
       b.Su_EmailAddress   as 邮箱,                                                                
       b.Su_WebAddress     as 网址,                                                                
                                                                                                   
       c.Sl_Handset      as 供应商联系人手机,                                                        
       c.Sl_OfficePhone  as 供应商联系人电话,                                                        
       c.Sl_Fax          as 供应商联系人传真,                                                        
                                                                                                                
                                     
f.Oi_Price,
 
f.Oi_Discount,
                                                              
       f.Oi_Remark     as 产品说明,                                                                
       k.Ps_Name       as 产品分类,                                                                
                                                                                                   
       b.Su_Ps_Guid       ,                                                                        
       a.Or_Su_Guid       ,                                                                        
       a.Or_Sl_Guid       ,                                                                        
       a.Or_Guid          ,                                                                        
       f.Oi_Guid          ,                                                                        
       f.Oi_Pr_Guid       ,                                                                        
       f.Oi_Pu_Guid       ,                                                                        
       f.Oi_Ps_Guid       ,                                                                        
       a.Or_OperatorGuid                                                                           
     from Buy_Order a                                                                             
          left join Base_Supplier        b on a.Or_Su_Guid     =b.Su_Guid                          
          left join Base_SupplierLinkMan c on a.Or_Sl_Guid     =c.Sl_Guid                          
          left join Buy_OrderItem        f on a.Or_Guid        =f.Oi_Or_Guid                       
          left join ERP_Base_Materiel         g on f.Oi_Pr_Guid     =g.Pr_Guid                          
          left join ERP_Base_MaterielUnit     h on f.Oi_Pu_Guid     =h.Pu_Guid                          
          left join ERP_Base_MaterielSort     k on f.Oi_Ps_Guid     =k.Ps_Guid                          
          left join Base_CustLinkMan     l on a.Or_OperatorGuid=l.Cl_Guid                          
     where  Or_State='查询' and convert(varchar(10),Or_Datetime,121) >= @BegDate and convert(varchar(10),Or_Datetime,121)<= @EndDate        
) aa
go
--Proc_CreateBuyOrderProduct  '2001-9-7', '2006-2-12'




if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_CreateBuyOrder]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Proc_CreateBuyOrder]
go
CREATE PROCEDURE [dbo].[Proc_CreateBuyOrder]
(
@BegDate varchar(100),  @EndDate varchar(100)  
)   
as
if exists (select * from tempdb.dbo.sysobjects where id = object_id(N'tempdb.[dbo].[##TBase_AnalyseForm_Buy_BuyOrder]'))
  drop table  ##TBase_AnalyseForm_Buy_BuyOrder
    
select * ,产品金额-已付金额 as 未付金额
 into ##TBase_AnalyseForm_Buy_BuyOrder


from 
(
 select
          isnull((select sum(isnull(Op_Money,0))                                                      
                  from Buy_OrderPayMoney                                                          
                  where Op_Or_Guid= cc.Or_Guid  and Op_Type='正常'
                   )
                 ,0)  as 已付金额,          
          * 

     from
      (
       select
		       newid()               as  Pubid              ,
		       max(订单编号         ) as  订单编号           ,
		       max(采购人员         ) as  采购人员           ,
		       max(签订时间         ) as  签订时间           ,
                                                         
		       max(供应商编号         ) as  供应商编号           ,
		       max(供应商名称         ) as  供应商名称           ,
		       max(供应商联系人       ) as  供应商联系人         ,
                                                         
		       Sum(采购数量         ) as  采购数量           ,
		       Sum(已入库数量       ) as  已入库数量         ,
		       Sum(已入库金额       ) as  已入库金额         ,
		       Sum(未入库金额       ) as  未入库金额         ,
 
           sum(退货数量) as 退货数量, 
		       Sum(采购数量         ) -Sum(已入库数量       ) as  未入库数量         ,
		       max(基本单位        ) as  基本单位            ,
		       avg(产品折扣         ) as  产品折扣           ,
		       Sum(产品金额         ) as  产品金额           ,
		       max(订单折扣         ) as  订单折扣           ,
                                                         
			     max(年               ) as  年                 ,
			     max(季度             ) as  季度               ,
			     max(月               ) as  月                 ,
			     max(周               ) as  周                 ,
			     max(日期             ) as  日期               ,
                                                         
		       max(付款状态         ) as  付款状态           ,
		       max(计划入库时间     ) as  计划入库时间       ,

		       max(供应商付款方式     ) as  供应商付款方式       ,
		       max(结算方式         ) as  结算方式           ,
		       max(结算方式天数     ) as  结算方式天数       ,
		       max(负责人           ) as  负责人             ,
		       max(国家             ) as  国家               ,
		       max(省份             ) as  省份               ,
		       max(地区             ) as  地区               ,
		       max(开户行           ) as  开户行             ,
		       max(帐号             ) as  帐号               ,
		       max(邮箱             ) as  邮箱               ,
		       max(网址             ) as  网址               ,
                                                         
		       max(供应商联系人手机   ) as  供应商联系人手机     ,
		       max(供应商联系人电话   ) as  供应商联系人电话     ,
		       max(供应商联系人传真   ) as  供应商联系人传真     ,
                                                          
		       max(Su_Ps_Guid     ) as  Su_Ps_Guid       ,
		       max(Or_Su_Guid     ) as  Or_Su_Guid       ,
		       max(Or_Sl_Guid     ) as  Or_Sl_Guid       ,
		       max(Or_Guid        ) as  Or_Guid          ,
		       max(Or_OperatorGuid) as  Or_OperatorGuid

       from ##TBase_AnalyseForm_Buy_BuyOrderProduct bb                                                                                            
       group by 订单编号
     ) cc                                                                                               
) dd


go
--Proc_CreateBuyOrder  '2001-9-7', '2006-2-12'


