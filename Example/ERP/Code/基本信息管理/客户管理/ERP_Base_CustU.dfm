object ERP_Base_CustForm: TERP_Base_CustForm
  Left = 0
  Top = 0
  Caption = #23458#25143#20449#24687#31649#29702
  ClientHeight = 550
  ClientWidth = 750
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object ABDBStatusBar1: TABdxDBStatusBar
    Left = 0
    Top = 531
    Width = 750
    Height = 19
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Width = 50
      end>
    PaintStyle = stpsUseLookAndFeel
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    StopUpdate = False
    DataSource = ABDatasource1
  end
  object ABcxPageControl1: TABcxPageControl
    Left = 0
    Top = 0
    Width = 750
    Height = 531
    Align = alClient
    TabOrder = 1
    Properties.ActivePage = cxTabSheet1
    Properties.CustomButtons.Buttons = <>
    Properties.HideTabs = True
    LookAndFeel.Kind = lfFlat
    ActivePageIndex = 0
    ClientRectBottom = 530
    ClientRectLeft = 1
    ClientRectRight = 749
    ClientRectTop = 1
    object cxTabSheet1: TcxTabSheet
      Caption = 'cxTabSheet1'
      ImageIndex = 0
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel1: TPanel
        Left = 0
        Top = 25
        Width = 748
        Height = 504
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitWidth = 744
        ExplicitHeight = 500
        object Panel3: TPanel
          Left = 201
          Top = 0
          Width = 547
          Height = 504
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          ExplicitLeft = 197
          ExplicitHeight = 500
          object ABcxPageControl2: TABcxPageControl
            Left = 0
            Top = 0
            Width = 547
            Height = 185
            Align = alTop
            TabOrder = 0
            Properties.ActivePage = cxTabSheet_11
            Properties.CustomButtons.Buttons = <>
            LookAndFeel.Kind = lfFlat
            ActivePageIndex = 0
            ClientRectBottom = 184
            ClientRectLeft = 1
            ClientRectRight = 546
            ClientRectTop = 21
            object cxTabSheet_11: TcxTabSheet
              Caption = #22522#26412#20449#24687
              ImageIndex = 0
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object ABDBPanel1: TABDBPanel
                Left = 0
                Top = 0
                Width = 541
                Height = 159
                Align = alClient
                BevelOuter = bvNone
                Caption = 'ABDBPanel1'
                ShowCaption = False
                TabOrder = 0
                ReadOnly = False
                DataSource = ABDatasource1
                AddAnchors_akRight = True
                AddAnchors_akBottom = True
                AutoHeight = True
                AutoWidth = True
              end
            end
            object cxTabSheet_12: TcxTabSheet
              Caption = #20301#32622#20449#24687
              ImageIndex = 1
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object ABDBPanel2: TABDBPanel
                Left = 0
                Top = 0
                Width = 541
                Height = 159
                Align = alClient
                BevelOuter = bvNone
                Caption = 'ABDBPanel1'
                ShowCaption = False
                TabOrder = 0
                ReadOnly = False
                DataSource = ABDatasource1
                AddAnchors_akRight = True
                AddAnchors_akBottom = True
                AutoHeight = True
                AutoWidth = True
              end
            end
            object cxTabSheet_13: TcxTabSheet
              Caption = #36152#26131#20449#24687
              ImageIndex = 2
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object ABDBPanel3: TABDBPanel
                Left = 0
                Top = 0
                Width = 541
                Height = 159
                Align = alClient
                BevelOuter = bvNone
                Caption = 'ABDBPanel1'
                ShowCaption = False
                TabOrder = 0
                ReadOnly = False
                DataSource = ABDatasource1
                AddAnchors_akRight = True
                AddAnchors_akBottom = True
                AutoHeight = True
                AutoWidth = True
              end
            end
            object cxTabSheet_14: TcxTabSheet
              Caption = #20854#23427#20449#24687
              ImageIndex = 3
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object ABDBPanel4: TABDBPanel
                Left = 0
                Top = 0
                Width = 541
                Height = 159
                Align = alClient
                BevelOuter = bvNone
                Caption = 'ABDBPanel1'
                ShowCaption = False
                TabOrder = 0
                ReadOnly = False
                DataSource = ABDatasource1
                AddAnchors_akRight = True
                AddAnchors_akBottom = True
                AutoHeight = True
                AutoWidth = True
              end
            end
          end
          object ABcxPageControl3: TABcxPageControl
            Left = 0
            Top = 193
            Width = 547
            Height = 311
            Align = alClient
            TabOrder = 1
            Properties.ActivePage = cxTabSheet1_1
            Properties.CustomButtons.Buttons = <>
            LookAndFeel.Kind = lfFlat
            ActivePageIndex = 0
            ExplicitHeight = 307
            ClientRectBottom = 310
            ClientRectLeft = 1
            ClientRectRight = 546
            ClientRectTop = 21
            object cxTabSheet1_1: TcxTabSheet
              Caption = #32852#31995#20154#20449#24687
              ImageIndex = 0
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object ABcxGrid2: TABcxGrid
                Left = 0
                Top = 25
                Width = 348
                Height = 256
                Align = alClient
                TabOrder = 0
                object ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView
                  PopupMenu.AutoHotkeys = maManual
                  PopupMenu.CloseFootStr = False
                  PopupMenu.LinkTableView = ABcxGridDBBandedTableView1
                  PopupMenu.AutoApplyBestFit = True
                  PopupMenu.AutoCreateAllItem = True
                  Navigator.Buttons.CustomButtons = <>
                  DataController.DataSource = ABDatasource1_1
                  DataController.Filter.Options = [fcoCaseInsensitive]
                  DataController.Filter.AutoDataSetFilter = True
                  DataController.Filter.TranslateBetween = True
                  DataController.Filter.TranslateIn = True
                  DataController.Filter.TranslateLike = True
                  DataController.Summary.DefaultGroupSummaryItems = <>
                  DataController.Summary.FooterSummaryItems = <>
                  DataController.Summary.SummaryGroups = <>
                  OptionsBehavior.AlwaysShowEditor = True
                  OptionsBehavior.FocusCellOnTab = True
                  OptionsBehavior.GoToNextCellOnEnter = True
                  OptionsCustomize.ColumnsQuickCustomization = True
                  OptionsCustomize.DataRowSizing = True
                  OptionsSelection.MultiSelect = True
                  OptionsView.GroupByBox = False
                  Bands = <
                    item
                    end>
                  ExtPopupMenu.AutoHotkeys = maManual
                  ExtPopupMenu.CloseFootStr = False
                  ExtPopupMenu.LinkTableView = ABcxGridDBBandedTableView1
                  ExtPopupMenu.AutoApplyBestFit = True
                  ExtPopupMenu.AutoCreateAllItem = True
                end
                object cxGridLevel1: TcxGridLevel
                  GridView = ABcxGridDBBandedTableView1
                end
              end
              object ABDBNavigator2: TABDBNavigator
                Left = 0
                Top = 0
                Width = 541
                Height = 25
                Align = alTop
                BevelOuter = bvNone
                Caption = 'ABDBNavigator1'
                ShowCaption = False
                TabOrder = 1
                BigGlyph = False
                ImageLayout = blGlyphLeft
                DataSource = ABDatasource1_1
                VisibleButtons = [nbInsertSpacer, nbInsert, nbCopy, nbDelete, nbEdit, nbPostSpacer, nbPost, nbCancel]
                ButtonRangeType = RtDetail
                BtnCustom1ImageIndex = -1
                BtnCustom2ImageIndex = -1
                BtnCustom3ImageIndex = -1
                BtnCustom4ImageIndex = -1
                BtnCustom5ImageIndex = -1
                BtnCustom6ImageIndex = -1
                BtnCustom7ImageIndex = -1
                BtnCustom8ImageIndex = -1
                BtnCustom9ImageIndex = -1
                BtnCustom10ImageIndex = -1
                BtnCustom1Caption = #33258#23450#20041'1'
                BtnCustom2Caption = #33258#23450#20041'2'
                BtnCustom3Caption = #33258#23450#20041'3'
                BtnCustom4Caption = #33258#23450#20041'4'
                BtnCustom5Caption = #33258#23450#20041'5'
                BtnCustom6Caption = #33258#23450#20041'6'
                BtnCustom7Caption = #33258#23450#20041'7'
                BtnCustom8Caption = #33258#23450#20041'8'
                BtnCustom9Caption = #33258#23450#20041'9'
                BtnCustom10Caption = #33258#23450#20041'10'
                BtnCustom1Kind = cxbkStandard
                BtnCustom2Kind = cxbkStandard
                BtnCustom3Kind = cxbkStandard
                BtnCustom4Kind = cxbkStandard
                BtnCustom5Kind = cxbkStandard
                BtnCustom6Kind = cxbkStandard
                BtnCustom7Kind = cxbkStandard
                BtnCustom8Kind = cxbkStandard
                BtnCustom9Kind = cxbkStandard
                BtnCustom10Kind = cxbkStandard
                ApprovedRollbackButton = nbNull
                ApprovedCommitButton = nbNull
                ButtonNativeStyle = False
                BtnCaptions.Strings = (
                  'BtnFirstRecord='
                  'BtnPreviousRecord='
                  'BtnNextRecord='
                  'BtnLastRecord='
                  'BtnInsert='
                  'btnCopy='
                  'BtnDelete='
                  'BtnEdit='
                  'BtnPost='
                  'BtnCancel='
                  'btnQuery='
                  'BtnReport='
                  'BtnCustom1='
                  'BtnCustom2='
                  'BtnCustom3='
                  'BtnCustom4='
                  'BtnCustom5='
                  'BtnCustom6='
                  'BtnCustom7='
                  'BtnCustom8='
                  'BtnCustom9='
                  'BtnCustom10='
                  'BtnExit=')
              end
              object ABcxDBImage1: TABcxDBImage
                Left = 356
                Top = 25
                Align = alRight
                DataBinding.DataField = 'Cl_Picture'
                DataBinding.DataSource = ABDatasource1_1_1
                Properties.FitMode = ifmProportionalStretch
                Properties.GraphicClassName = 'TJPEGImage'
                Properties.PopupMenuLayout.MenuItems = [pmiCut, pmiCopy, pmiPaste, pmiDelete, pmiLoad, pmiWebCam, pmiSave, pmiCustom]
                Properties.PopupMenuLayout.CustomMenuItemCaption = #21387#32553
                Properties.ZipWidth = 0
                Properties.ZipHeight = 0
                TabOrder = 2
                Height = 264
                Width = 185
              end
              object ABcxSplitter3: TABcxSplitter
                Left = 352
                Top = 25
                Width = 8
                Height = 264
                HotZoneClassName = 'TcxMediaPlayer8Style'
                AlignSplitter = salRight
                InvertDirection = True
                Control = ABcxDBImage1
                ExplicitLeft = 348
                ExplicitHeight = 256
              end
            end
            object cxTabSheet1_2: TcxTabSheet
              Caption = #22320#22336#20449#24687
              ImageIndex = 1
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object ABDBNavigator3: TABDBNavigator
                Left = 0
                Top = 0
                Width = 541
                Height = 25
                Align = alTop
                BevelOuter = bvNone
                Caption = 'ABDBNavigator1'
                ShowCaption = False
                TabOrder = 0
                BigGlyph = False
                ImageLayout = blGlyphLeft
                DataSource = ABDatasource1_2
                VisibleButtons = [nbInsertSpacer, nbInsert, nbCopy, nbDelete, nbEdit, nbPostSpacer, nbPost, nbCancel]
                ButtonRangeType = RtDetail
                BtnCustom1ImageIndex = -1
                BtnCustom2ImageIndex = -1
                BtnCustom3ImageIndex = -1
                BtnCustom4ImageIndex = -1
                BtnCustom5ImageIndex = -1
                BtnCustom6ImageIndex = -1
                BtnCustom7ImageIndex = -1
                BtnCustom8ImageIndex = -1
                BtnCustom9ImageIndex = -1
                BtnCustom10ImageIndex = -1
                BtnCustom1Caption = #33258#23450#20041'1'
                BtnCustom2Caption = #33258#23450#20041'2'
                BtnCustom3Caption = #33258#23450#20041'3'
                BtnCustom4Caption = #33258#23450#20041'4'
                BtnCustom5Caption = #33258#23450#20041'5'
                BtnCustom6Caption = #33258#23450#20041'6'
                BtnCustom7Caption = #33258#23450#20041'7'
                BtnCustom8Caption = #33258#23450#20041'8'
                BtnCustom9Caption = #33258#23450#20041'9'
                BtnCustom10Caption = #33258#23450#20041'10'
                BtnCustom1Kind = cxbkStandard
                BtnCustom2Kind = cxbkStandard
                BtnCustom3Kind = cxbkStandard
                BtnCustom4Kind = cxbkStandard
                BtnCustom5Kind = cxbkStandard
                BtnCustom6Kind = cxbkStandard
                BtnCustom7Kind = cxbkStandard
                BtnCustom8Kind = cxbkStandard
                BtnCustom9Kind = cxbkStandard
                BtnCustom10Kind = cxbkStandard
                ApprovedRollbackButton = nbNull
                ApprovedCommitButton = nbNull
                ButtonNativeStyle = False
                BtnCaptions.Strings = (
                  'BtnFirstRecord='
                  'BtnPreviousRecord='
                  'BtnNextRecord='
                  'BtnLastRecord='
                  'BtnInsert='
                  'btnCopy='
                  'BtnDelete='
                  'BtnEdit='
                  'BtnPost='
                  'BtnCancel='
                  'btnQuery='
                  'BtnReport='
                  'BtnCustom1='
                  'BtnCustom2='
                  'BtnCustom3='
                  'BtnCustom4='
                  'BtnCustom5='
                  'BtnCustom6='
                  'BtnCustom7='
                  'BtnCustom8='
                  'BtnCustom9='
                  'BtnCustom10='
                  'BtnExit=')
              end
              object ABcxGrid3: TABcxGrid
                Left = 0
                Top = 25
                Width = 541
                Height = 256
                Align = alClient
                TabOrder = 1
                object ABcxGridDBBandedTableView2: TABcxGridDBBandedTableView
                  PopupMenu.AutoHotkeys = maManual
                  PopupMenu.CloseFootStr = False
                  PopupMenu.LinkTableView = ABcxGridDBBandedTableView2
                  PopupMenu.AutoApplyBestFit = True
                  PopupMenu.AutoCreateAllItem = True
                  Navigator.Buttons.CustomButtons = <>
                  DataController.DataSource = ABDatasource1_2
                  DataController.Filter.Options = [fcoCaseInsensitive]
                  DataController.Filter.AutoDataSetFilter = True
                  DataController.Filter.TranslateBetween = True
                  DataController.Filter.TranslateIn = True
                  DataController.Filter.TranslateLike = True
                  DataController.Summary.DefaultGroupSummaryItems = <>
                  DataController.Summary.FooterSummaryItems = <>
                  DataController.Summary.SummaryGroups = <>
                  OptionsBehavior.AlwaysShowEditor = True
                  OptionsBehavior.FocusCellOnTab = True
                  OptionsBehavior.GoToNextCellOnEnter = True
                  OptionsCustomize.ColumnsQuickCustomization = True
                  OptionsCustomize.DataRowSizing = True
                  OptionsSelection.MultiSelect = True
                  OptionsView.GroupByBox = False
                  Bands = <
                    item
                    end>
                  ExtPopupMenu.AutoHotkeys = maManual
                  ExtPopupMenu.CloseFootStr = False
                  ExtPopupMenu.LinkTableView = ABcxGridDBBandedTableView2
                  ExtPopupMenu.AutoApplyBestFit = True
                  ExtPopupMenu.AutoCreateAllItem = True
                end
                object cxGridLevel2: TcxGridLevel
                  GridView = ABcxGridDBBandedTableView2
                end
              end
            end
            object cxTabSheet1_3: TcxTabSheet
              Caption = #29289#26009#20449#24687
              ImageIndex = 2
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object ABDBNavigator4: TABDBNavigator
                Left = 0
                Top = 0
                Width = 541
                Height = 25
                Align = alTop
                BevelOuter = bvNone
                Caption = 'ABDBNavigator1'
                ShowCaption = False
                TabOrder = 0
                BigGlyph = False
                ImageLayout = blGlyphLeft
                DataSource = ABDatasource1_3
                VisibleButtons = [nbInsertSpacer, nbInsert, nbCopy, nbDelete, nbEdit, nbPostSpacer, nbPost, nbCancel]
                ButtonRangeType = RtDetail
                BtnCustom1ImageIndex = -1
                BtnCustom2ImageIndex = -1
                BtnCustom3ImageIndex = -1
                BtnCustom4ImageIndex = -1
                BtnCustom5ImageIndex = -1
                BtnCustom6ImageIndex = -1
                BtnCustom7ImageIndex = -1
                BtnCustom8ImageIndex = -1
                BtnCustom9ImageIndex = -1
                BtnCustom10ImageIndex = -1
                BtnCustom1Caption = #33258#23450#20041'1'
                BtnCustom2Caption = #33258#23450#20041'2'
                BtnCustom3Caption = #33258#23450#20041'3'
                BtnCustom4Caption = #33258#23450#20041'4'
                BtnCustom5Caption = #33258#23450#20041'5'
                BtnCustom6Caption = #33258#23450#20041'6'
                BtnCustom7Caption = #33258#23450#20041'7'
                BtnCustom8Caption = #33258#23450#20041'8'
                BtnCustom9Caption = #33258#23450#20041'9'
                BtnCustom10Caption = #33258#23450#20041'10'
                BtnCustom1Kind = cxbkStandard
                BtnCustom2Kind = cxbkStandard
                BtnCustom3Kind = cxbkStandard
                BtnCustom4Kind = cxbkStandard
                BtnCustom5Kind = cxbkStandard
                BtnCustom6Kind = cxbkStandard
                BtnCustom7Kind = cxbkStandard
                BtnCustom8Kind = cxbkStandard
                BtnCustom9Kind = cxbkStandard
                BtnCustom10Kind = cxbkStandard
                ApprovedRollbackButton = nbNull
                ApprovedCommitButton = nbNull
                ButtonNativeStyle = False
                BtnCaptions.Strings = (
                  'BtnFirstRecord='
                  'BtnPreviousRecord='
                  'BtnNextRecord='
                  'BtnLastRecord='
                  'BtnInsert='
                  'btnCopy='
                  'BtnDelete='
                  'BtnEdit='
                  'BtnPost='
                  'BtnCancel='
                  'btnQuery='
                  'BtnReport='
                  'BtnCustom1='
                  'BtnCustom2='
                  'BtnCustom3='
                  'BtnCustom4='
                  'BtnCustom5='
                  'BtnCustom6='
                  'BtnCustom7='
                  'BtnCustom8='
                  'BtnCustom9='
                  'BtnCustom10='
                  'BtnExit=')
              end
              object ABcxGrid4: TABcxGrid
                Left = 0
                Top = 25
                Width = 541
                Height = 256
                Align = alClient
                TabOrder = 1
                object ABcxGridDBBandedTableView3: TABcxGridDBBandedTableView
                  PopupMenu.AutoHotkeys = maManual
                  PopupMenu.CloseFootStr = False
                  PopupMenu.LinkTableView = ABcxGridDBBandedTableView3
                  PopupMenu.AutoApplyBestFit = True
                  PopupMenu.AutoCreateAllItem = True
                  Navigator.Buttons.CustomButtons = <>
                  DataController.DataSource = ABDatasource1_3
                  DataController.Filter.Options = [fcoCaseInsensitive]
                  DataController.Filter.AutoDataSetFilter = True
                  DataController.Filter.TranslateBetween = True
                  DataController.Filter.TranslateIn = True
                  DataController.Filter.TranslateLike = True
                  DataController.Summary.DefaultGroupSummaryItems = <>
                  DataController.Summary.FooterSummaryItems = <>
                  DataController.Summary.SummaryGroups = <>
                  OptionsBehavior.AlwaysShowEditor = True
                  OptionsBehavior.FocusCellOnTab = True
                  OptionsBehavior.GoToNextCellOnEnter = True
                  OptionsCustomize.ColumnsQuickCustomization = True
                  OptionsCustomize.DataRowSizing = True
                  OptionsSelection.MultiSelect = True
                  OptionsView.GroupByBox = False
                  Bands = <
                    item
                    end>
                  ExtPopupMenu.AutoHotkeys = maManual
                  ExtPopupMenu.CloseFootStr = False
                  ExtPopupMenu.LinkTableView = ABcxGridDBBandedTableView3
                  ExtPopupMenu.AutoApplyBestFit = True
                  ExtPopupMenu.AutoCreateAllItem = True
                end
                object cxGridLevel3: TcxGridLevel
                  GridView = ABcxGridDBBandedTableView3
                end
              end
            end
            object cxTabSheet1_4: TcxTabSheet
              Caption = #21512#20316#27963#21160
              ImageIndex = 3
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object ABDBNavigator5: TABDBNavigator
                Left = 0
                Top = 0
                Width = 541
                Height = 25
                Align = alTop
                BevelOuter = bvNone
                Caption = 'ABDBNavigator1'
                ShowCaption = False
                TabOrder = 0
                BigGlyph = False
                ImageLayout = blGlyphLeft
                DataSource = ABDatasource1_4
                VisibleButtons = [nbInsertSpacer, nbInsert, nbCopy, nbDelete, nbEdit, nbPostSpacer, nbPost, nbCancel]
                ButtonRangeType = RtDetail
                BtnCustom1ImageIndex = -1
                BtnCustom2ImageIndex = -1
                BtnCustom3ImageIndex = -1
                BtnCustom4ImageIndex = -1
                BtnCustom5ImageIndex = -1
                BtnCustom6ImageIndex = -1
                BtnCustom7ImageIndex = -1
                BtnCustom8ImageIndex = -1
                BtnCustom9ImageIndex = -1
                BtnCustom10ImageIndex = -1
                BtnCustom1Caption = #33258#23450#20041'1'
                BtnCustom2Caption = #33258#23450#20041'2'
                BtnCustom3Caption = #33258#23450#20041'3'
                BtnCustom4Caption = #33258#23450#20041'4'
                BtnCustom5Caption = #33258#23450#20041'5'
                BtnCustom6Caption = #33258#23450#20041'6'
                BtnCustom7Caption = #33258#23450#20041'7'
                BtnCustom8Caption = #33258#23450#20041'8'
                BtnCustom9Caption = #33258#23450#20041'9'
                BtnCustom10Caption = #33258#23450#20041'10'
                BtnCustom1Kind = cxbkStandard
                BtnCustom2Kind = cxbkStandard
                BtnCustom3Kind = cxbkStandard
                BtnCustom4Kind = cxbkStandard
                BtnCustom5Kind = cxbkStandard
                BtnCustom6Kind = cxbkStandard
                BtnCustom7Kind = cxbkStandard
                BtnCustom8Kind = cxbkStandard
                BtnCustom9Kind = cxbkStandard
                BtnCustom10Kind = cxbkStandard
                ApprovedRollbackButton = nbNull
                ApprovedCommitButton = nbNull
                ButtonNativeStyle = False
                BtnCaptions.Strings = (
                  'BtnFirstRecord='
                  'BtnPreviousRecord='
                  'BtnNextRecord='
                  'BtnLastRecord='
                  'BtnInsert='
                  'btnCopy='
                  'BtnDelete='
                  'BtnEdit='
                  'BtnPost='
                  'BtnCancel='
                  'btnQuery='
                  'BtnReport='
                  'BtnCustom1='
                  'BtnCustom2='
                  'BtnCustom3='
                  'BtnCustom4='
                  'BtnCustom5='
                  'BtnCustom6='
                  'BtnCustom7='
                  'BtnCustom8='
                  'BtnCustom9='
                  'BtnCustom10='
                  'BtnExit=')
              end
              object ABcxGrid5: TABcxGrid
                Left = 0
                Top = 25
                Width = 541
                Height = 256
                Align = alClient
                TabOrder = 1
                object ABcxGridDBBandedTableView4: TABcxGridDBBandedTableView
                  PopupMenu.AutoHotkeys = maManual
                  PopupMenu.CloseFootStr = False
                  PopupMenu.LinkTableView = ABcxGridDBBandedTableView4
                  PopupMenu.AutoApplyBestFit = True
                  PopupMenu.AutoCreateAllItem = True
                  Navigator.Buttons.CustomButtons = <>
                  DataController.DataSource = ABDatasource1_4
                  DataController.Filter.Options = [fcoCaseInsensitive]
                  DataController.Filter.AutoDataSetFilter = True
                  DataController.Filter.TranslateBetween = True
                  DataController.Filter.TranslateIn = True
                  DataController.Filter.TranslateLike = True
                  DataController.Summary.DefaultGroupSummaryItems = <>
                  DataController.Summary.FooterSummaryItems = <>
                  DataController.Summary.SummaryGroups = <>
                  OptionsBehavior.AlwaysShowEditor = True
                  OptionsBehavior.FocusCellOnTab = True
                  OptionsBehavior.GoToNextCellOnEnter = True
                  OptionsCustomize.ColumnsQuickCustomization = True
                  OptionsCustomize.DataRowSizing = True
                  OptionsSelection.MultiSelect = True
                  OptionsView.GroupByBox = False
                  Bands = <
                    item
                    end>
                  ExtPopupMenu.AutoHotkeys = maManual
                  ExtPopupMenu.CloseFootStr = False
                  ExtPopupMenu.LinkTableView = ABcxGridDBBandedTableView4
                  ExtPopupMenu.AutoApplyBestFit = True
                  ExtPopupMenu.AutoCreateAllItem = True
                end
                object cxGridLevel4: TcxGridLevel
                  GridView = ABcxGridDBBandedTableView4
                end
              end
            end
          end
          object ABcxSplitter2: TABcxSplitter
            Left = 0
            Top = 185
            Width = 547
            Height = 8
            HotZoneClassName = 'TcxMediaPlayer8Style'
            AlignSplitter = salTop
            InvertDirection = True
            Control = ABcxPageControl2
          end
        end
        object ABcxGrid1: TABcxGrid
          Left = 0
          Top = 0
          Width = 193
          Height = 504
          Align = alClient
          TabOrder = 1
          ExplicitWidth = 189
          ExplicitHeight = 500
          object ABcxGrid1ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView
            PopupMenu.AutoHotkeys = maManual
            PopupMenu.CloseFootStr = False
            PopupMenu.LinkTableView = ABcxGrid1ABcxGridDBBandedTableView1
            PopupMenu.AutoApplyBestFit = True
            PopupMenu.AutoCreateAllItem = True
            Navigator.Buttons.CustomButtons = <>
            DataController.DataSource = ABDatasource1
            DataController.Filter.Options = [fcoCaseInsensitive]
            DataController.Filter.AutoDataSetFilter = True
            DataController.Filter.TranslateBetween = True
            DataController.Filter.TranslateIn = True
            DataController.Filter.TranslateLike = True
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsBehavior.AlwaysShowEditor = True
            OptionsBehavior.FocusCellOnTab = True
            OptionsBehavior.GoToNextCellOnEnter = True
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsCustomize.DataRowSizing = True
            OptionsSelection.MultiSelect = True
            OptionsView.GroupByBox = False
            Bands = <
              item
              end>
            ExtPopupMenu.AutoHotkeys = maManual
            ExtPopupMenu.CloseFootStr = False
            ExtPopupMenu.LinkTableView = ABcxGrid1ABcxGridDBBandedTableView1
            ExtPopupMenu.AutoApplyBestFit = True
            ExtPopupMenu.AutoCreateAllItem = True
          end
          object ABcxGrid1Level1: TcxGridLevel
            GridView = ABcxGrid1ABcxGridDBBandedTableView1
          end
        end
        object ABcxSplitter1: TABcxSplitter
          Left = 193
          Top = 0
          Width = 8
          Height = 504
          HotZoneClassName = 'TcxMediaPlayer8Style'
          AlignSplitter = salRight
          InvertDirection = True
          Control = Panel3
          ExplicitLeft = 189
          ExplicitHeight = 500
        end
      end
      object ABDBNavigator1: TABDBNavigator
        Left = 0
        Top = 0
        Width = 748
        Height = 25
        Align = alTop
        BevelOuter = bvNone
        Caption = 'ABDBNavigator1'
        ShowCaption = False
        TabOrder = 1
        BigGlyph = False
        ImageLayout = blGlyphLeft
        DataSource = ABDatasource1
        VisibleButtons = [nbInsertSpacer, nbInsert, nbCopy, nbDelete, nbEdit, nbPostSpacer, nbPost, nbCancel, nbQuerySpacer, nbQuery, nbReport, nbExitSpacer, nbExit]
        ButtonRangeType = RtMain
        BtnCustom1ImageIndex = -1
        BtnCustom2ImageIndex = -1
        BtnCustom3ImageIndex = -1
        BtnCustom4ImageIndex = -1
        BtnCustom5ImageIndex = -1
        BtnCustom6ImageIndex = -1
        BtnCustom7ImageIndex = -1
        BtnCustom8ImageIndex = -1
        BtnCustom9ImageIndex = -1
        BtnCustom10ImageIndex = -1
        BtnCustom1Caption = #33258#23450#20041'1'
        BtnCustom2Caption = #33258#23450#20041'2'
        BtnCustom3Caption = #33258#23450#20041'3'
        BtnCustom4Caption = #33258#23450#20041'4'
        BtnCustom5Caption = #33258#23450#20041'5'
        BtnCustom6Caption = #33258#23450#20041'6'
        BtnCustom7Caption = #33258#23450#20041'7'
        BtnCustom8Caption = #33258#23450#20041'8'
        BtnCustom9Caption = #33258#23450#20041'9'
        BtnCustom10Caption = #33258#23450#20041'10'
        BtnCustom1Kind = cxbkStandard
        BtnCustom2Kind = cxbkStandard
        BtnCustom3Kind = cxbkStandard
        BtnCustom4Kind = cxbkStandard
        BtnCustom5Kind = cxbkStandard
        BtnCustom6Kind = cxbkStandard
        BtnCustom7Kind = cxbkStandard
        BtnCustom8Kind = cxbkStandard
        BtnCustom9Kind = cxbkStandard
        BtnCustom10Kind = cxbkStandard
        ApprovedRollbackButton = nbNull
        ApprovedCommitButton = nbNull
        ButtonNativeStyle = False
        BtnCaptions.Strings = (
          'BtnFirstRecord='
          'BtnPreviousRecord='
          'BtnNextRecord='
          'BtnLastRecord='
          'BtnInsert='
          'btnCopy='
          'BtnDelete='
          'BtnEdit='
          'BtnPost='
          'BtnCancel='
          'btnQuery='
          'BtnReport='
          'BtnCustom1='
          'BtnCustom2='
          'BtnCustom3='
          'BtnCustom4='
          'BtnCustom5='
          'BtnCustom6='
          'BtnCustom7='
          'BtnCustom8='
          'BtnCustom9='
          'BtnCustom10='
          'BtnExit=')
        ExplicitWidth = 744
      end
    end
  end
  object ABQuery1: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    SQL.Strings = (
      'select * from ERP_Base_Cust')
    ConnName = 'ERP'
    SqlUpdateDatetime = 42209.738933101850000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    EditControlType = ctSingle
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ERP_Base_Cust')
    IndexListDefs = <
      item
        Name = 'IX_ERP_Base_Cust'
        Fields = 'Cu_Order'
      end
      item
        Name = 'IX_Cu_Code'
        Fields = 'Cu_Code'
        Options = [ixUnique]
      end
      item
        Name = 'IX_Cu_Name'
        Fields = 'Cu_Name'
        Options = [ixUnique]
      end
      item
        Name = 'PK_Cu_Guid'
        Fields = 'Cu_Guid'
        Options = [ixPrimary, ixUnique]
      end>
    LoadTables.Strings = (
      'ERP_Base_Cust')
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 248
    Top = 296
  end
  object ABDatasource1: TABDatasource
    AutoEdit = False
    DataSet = ABQuery1
    Left = 320
    Top = 296
  end
  object ABQuery1_1: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    MasterSource = ABDatasource1
    MasterFields = 'Cu_Guid'
    DetailFields = 'Cl_Cu_Guid'
    SQL.Strings = (
      'select '
      'GetFieldNames_NoBigField_NullFlag('#39'ERP_Base_CustLinkman'#39') '
      'from ERP_Base_CustLinkman '
      'where CL_CU_GUID=:CU_GUID')
    ConnName = 'ERP'
    SqlUpdateDatetime = 42443.481257800920000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    EditControlType = ctSingle
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ERP_Base_CustLinkman')
    IndexListDefs = <
      item
        Name = 'IX_ERP_Base_CustLinkman'
        Fields = 'Cl_Cu_Guid;Cl_Order'
      end
      item
        Name = 'IX_Cl_Cu_Guid_Cl_Name'
        Fields = 'Cl_Cu_Guid;Cl_Name'
        Options = [ixUnique]
      end
      item
        Name = 'PK_Cl_Guid'
        Fields = 'Cl_Guid'
        Options = [ixPrimary, ixUnique]
      end>
    LoadTables.Strings = (
      'ERP_Base_CustLinkman')
    BlobFieldDefs = <
      item
        Datasource = ABDatasource1_1_1
        FieldName = 'Cl_Picture'
      end>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 248
    Top = 352
    ParamData = <
      item
        Name = 'CU_GUID'
        DataType = ftString
        ParamType = ptInput
      end>
  end
  object ABDatasource1_1: TABDatasource
    AutoEdit = False
    DataSet = ABQuery1_1
    Left = 248
    Top = 408
  end
  object ABDatasource1_2: TABDatasource
    AutoEdit = False
    DataSet = ABQuery1_2
    Left = 344
    Top = 408
  end
  object ABQuery1_2: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    MasterSource = ABDatasource1
    MasterFields = 'Cu_Guid'
    DetailFields = 'CA_Cu_Guid'
    SQL.Strings = (
      'select * '
      'from ERP_Base_CustAddress '
      'where CA_CU_GUID=:CU_GUID')
    ConnName = 'ERP'
    SqlUpdateDatetime = 42211.455641157410000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    EditControlType = ctSingle
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ERP_Base_CustAddress')
    IndexListDefs = <
      item
        Name = 'IX_ERP_Base_Cust'
        Fields = 'Cu_Order'
      end
      item
        Name = 'IX_Cu_Code'
        Fields = 'Cu_Code'
        Options = [ixUnique]
      end
      item
        Name = 'IX_Cu_Name'
        Fields = 'Cu_Name'
        Options = [ixUnique]
      end
      item
        Name = 'PK_Cu_Guid'
        Fields = 'Cu_Guid'
        Options = [ixPrimary, ixUnique]
      end>
    LoadTables.Strings = (
      'ERP_Base_CustAddress')
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 344
    Top = 352
    ParamData = <
      item
        Name = 'CU_GUID'
        ParamType = ptInput
      end>
  end
  object ABDatasource1_3: TABDatasource
    AutoEdit = False
    DataSet = ABQuery1_3
    Left = 440
    Top = 408
  end
  object ABQuery1_3: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    MasterSource = ABDatasource1
    MasterFields = 'Cu_Guid'
    DetailFields = 'CM_Cu_Guid'
    SQL.Strings = (
      'select * '
      'from ERP_Base_CustMateriel '
      'where CM_CU_GUID=:CU_GUID')
    ConnName = 'ERP'
    SqlUpdateDatetime = 42211.456725509260000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    EditControlType = ctSingle
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ERP_Base_CustMateriel')
    IndexListDefs = <
      item
        Name = 'IX_ERP_Base_Cust'
        Fields = 'Cu_Order'
      end
      item
        Name = 'IX_Cu_Code'
        Fields = 'Cu_Code'
        Options = [ixUnique]
      end
      item
        Name = 'IX_Cu_Name'
        Fields = 'Cu_Name'
        Options = [ixUnique]
      end
      item
        Name = 'PK_Cu_Guid'
        Fields = 'Cu_Guid'
        Options = [ixPrimary, ixUnique]
      end>
    LoadTables.Strings = (
      'ERP_Base_CustMateriel')
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 440
    Top = 352
    ParamData = <
      item
        Name = 'CU_GUID'
        ParamType = ptInput
      end>
  end
  object ABDatasource1_4: TABDatasource
    AutoEdit = False
    DataSet = ABQuery1_4
    Left = 544
    Top = 408
  end
  object ABQuery1_4: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    MasterSource = ABDatasource1
    MasterFields = 'Cu_Guid'
    DetailFields = 'CR_Cu_Guid'
    SQL.Strings = (
      'select * '
      'from ERP_Base_CustReMark '
      'where CR_CU_GUID=:CU_GUID')
    ConnName = 'ERP'
    SqlUpdateDatetime = 42213.499508356480000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    EditControlType = ctSingle
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ERP_Base_CustReMark')
    IndexListDefs = <
      item
        Name = 'IX_ERP_Base_Cust'
        Fields = 'Cu_Order'
      end
      item
        Name = 'IX_Cu_Code'
        Fields = 'Cu_Code'
        Options = [ixUnique]
      end
      item
        Name = 'IX_Cu_Name'
        Fields = 'Cu_Name'
        Options = [ixUnique]
      end
      item
        Name = 'PK_Cu_Guid'
        Fields = 'Cu_Guid'
        Options = [ixPrimary, ixUnique]
      end>
    LoadTables.Strings = (
      'ERP_Base_CustReMark')
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 544
    Top = 352
    ParamData = <
      item
        Name = 'CU_GUID'
        ParamType = ptInput
      end>
  end
  object ABQuery1_1_1: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    MasterSource = ABDatasource1_1
    MasterFields = 'Cl_Guid'
    DetailFields = 'Cl_Guid'
    SQL.Strings = (
      'select '
      'Cl_Guid,'
      'Cl_Picture '
      'from ERP_Base_CustLinkman '
      'where  Cl_Guid=:Cl_Guid')
    ConnName = 'ERP'
    SqlUpdateDatetime = 42213.282561493060000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    EditControlType = ctSingle
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ERP_Base_CustLinkman')
    IndexListDefs = <
      item
        Name = 'IX_ERP_Base_Cust'
        Fields = 'Cu_Order'
      end
      item
        Name = 'IX_Cu_Code'
        Fields = 'Cu_Code'
        Options = [ixUnique]
      end
      item
        Name = 'IX_Cu_Name'
        Fields = 'Cu_Name'
        Options = [ixUnique]
      end
      item
        Name = 'PK_Cu_Guid'
        Fields = 'Cu_Guid'
        Options = [ixPrimary, ixUnique]
      end>
    LoadTables.Strings = (
      'ERP_Base_CustLinkman')
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 248
    Top = 456
    ParamData = <
      item
        Name = 'CL_GUID'
        ParamType = ptInput
      end>
  end
  object ABDatasource1_1_1: TABDatasource
    AutoEdit = False
    DataSet = ABQuery1_1_1
    Left = 248
    Top = 496
  end
end
