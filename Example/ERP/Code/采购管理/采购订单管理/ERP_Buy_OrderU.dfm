object ERP_Buy_OrderForm: TERP_Buy_OrderForm
  Left = 0
  Top = 0
  Caption = #37319#36141#35746#21333#31649#29702
  ClientHeight = 550
  ClientWidth = 750
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object ABDBStatusBar1: TABdxDBStatusBar
    Left = 0
    Top = 531
    Width = 750
    Height = 19
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Width = 50
      end>
    PaintStyle = stpsUseLookAndFeel
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    StopUpdate = False
    DataSource = ABDatasource1
  end
  object ABcxPageControl1: TABcxPageControl
    Left = 0
    Top = 25
    Width = 750
    Height = 506
    Align = alClient
    TabOrder = 1
    Properties.ActivePage = cxTabSheet3
    Properties.CustomButtons.Buttons = <>
    LookAndFeel.Kind = lfFlat
    OnChange = ABcxPageControl1Change
    ActivePageIndex = 2
    ClientRectBottom = 502
    ClientRectLeft = 2
    ClientRectRight = 746
    ClientRectTop = 22
    object cxTabSheet1: TcxTabSheet
      Caption = #24050#24405#20837
      ImageIndex = 0
      object Panel1: TPanel
        Left = 0
        Top = 25
        Width = 744
        Height = 455
        Align = alClient
        BevelOuter = bvLowered
        TabOrder = 0
        object Panel3: TPanel
          Left = 196
          Top = 1
          Width = 547
          Height = 453
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          object ABcxPageControl2: TABcxPageControl
            Left = 0
            Top = 0
            Width = 547
            Height = 185
            Align = alTop
            TabOrder = 0
            Properties.ActivePage = cxTabSheet_11
            Properties.CustomButtons.Buttons = <>
            Properties.HideTabs = True
            LookAndFeel.Kind = lfFlat
            ActivePageIndex = 0
            ClientRectBottom = 181
            ClientRectLeft = 2
            ClientRectRight = 543
            ClientRectTop = 2
            object cxTabSheet_11: TcxTabSheet
              Caption = #22522#26412#20449#24687
              ImageIndex = 0
              object ABDBPanel1: TABDBPanel
                Left = 0
                Top = 0
                Width = 541
                Height = 179
                Align = alClient
                BevelOuter = bvNone
                Caption = 'ABDBPanel1'
                ShowCaption = False
                TabOrder = 0
                ReadOnly = False
                DataSource = ABDatasource1
                AddAnchors_akRight = True
                AddAnchors_akBottom = True
                AutoHeight = True
                AutoWidth = True
              end
            end
          end
          object ABcxPageControl3: TABcxPageControl
            Left = 0
            Top = 193
            Width = 547
            Height = 260
            Align = alClient
            TabOrder = 1
            Properties.ActivePage = cxTabSheet1_1
            Properties.CustomButtons.Buttons = <>
            Properties.HideTabs = True
            LookAndFeel.Kind = lfFlat
            ActivePageIndex = 0
            ClientRectBottom = 256
            ClientRectLeft = 2
            ClientRectRight = 543
            ClientRectTop = 2
            object cxTabSheet1_1: TcxTabSheet
              Caption = #32852#31995#20154#20449#24687
              ImageIndex = 0
              object ABcxGrid2: TABcxGrid
                Left = 0
                Top = 25
                Width = 541
                Height = 229
                Align = alClient
                TabOrder = 0
                object ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView
                  PopupMenu.AutoHotkeys = maManual
                  PopupMenu.CloseFootStr = False
                  PopupMenu.LinkTableView = ABcxGridDBBandedTableView1
                  PopupMenu.AutoApplyBestFit = True
                  PopupMenu.AutoCreateAllItem = True
                  Navigator.Buttons.CustomButtons = <>
                  DataController.DataSource = ABDatasource1_1
                  DataController.Filter.Options = [fcoCaseInsensitive]
                  DataController.Filter.AutoDataSetFilter = True
                  DataController.Filter.TranslateBetween = True
                  DataController.Filter.TranslateIn = True
                  DataController.Filter.TranslateLike = True
                  DataController.Summary.DefaultGroupSummaryItems = <>
                  DataController.Summary.FooterSummaryItems = <>
                  DataController.Summary.SummaryGroups = <>
                  OptionsBehavior.AlwaysShowEditor = True
                  OptionsBehavior.FocusCellOnTab = True
                  OptionsBehavior.GoToNextCellOnEnter = True
                  OptionsCustomize.ColumnsQuickCustomization = True
                  OptionsCustomize.DataRowSizing = True
                  OptionsSelection.MultiSelect = True
                  OptionsView.GroupByBox = False
                  OptionsView.Indicator = True
                  Bands = <
                    item
                    end>
                  ExtPopupMenu.AutoHotkeys = maManual
                  ExtPopupMenu.CloseFootStr = False
                  ExtPopupMenu.LinkTableView = ABcxGridDBBandedTableView1
                  ExtPopupMenu.AutoApplyBestFit = True
                  ExtPopupMenu.AutoCreateAllItem = True
                end
                object cxGridLevel1: TcxGridLevel
                  GridView = ABcxGridDBBandedTableView1
                end
              end
              object ABDBNavigator2: TABDBNavigator
                Left = 0
                Top = 0
                Width = 541
                Height = 25
                Align = alTop
                BevelOuter = bvNone
                BiDiMode = bdLeftToRight
                Caption = 'ABDBNavigator1'
                ParentBiDiMode = False
                ShowCaption = False
                TabOrder = 1
                BigGlyph = False
                ImageLayout = blGlyphLeft
                DataSource = ABDatasource1_1
                VisibleButtons = [nbInsertSpacer, nbInsert, nbCopy, nbDelete, nbEdit, nbPostSpacer, nbPost, nbCancel]
                ButtonRangeType = RtCustom
                BtnCustom1ImageIndex = 1
                BtnCustom2ImageIndex = -1
                BtnCustom3ImageIndex = -1
                BtnCustom4ImageIndex = -1
                BtnCustom5ImageIndex = -1
                BtnCustom6ImageIndex = -1
                BtnCustom7ImageIndex = -1
                BtnCustom8ImageIndex = -1
                BtnCustom9ImageIndex = -1
                BtnCustom10ImageIndex = -1
                BtnCustom1Caption = #23548#20837
                BtnCustom2Caption = #33258#23450#20041'2'
                BtnCustom3Caption = #33258#23450#20041'3'
                BtnCustom4Caption = #33258#23450#20041'4'
                BtnCustom5Caption = #33258#23450#20041'5'
                BtnCustom6Caption = #33258#23450#20041'6'
                BtnCustom7Caption = #33258#23450#20041'7'
                BtnCustom8Caption = #33258#23450#20041'8'
                BtnCustom9Caption = #33258#23450#20041'9'
                BtnCustom10Caption = #33258#23450#20041'10'
                BtnCustom1Kind = cxbkStandard
                BtnCustom2Kind = cxbkStandard
                BtnCustom3Kind = cxbkStandard
                BtnCustom4Kind = cxbkStandard
                BtnCustom5Kind = cxbkStandard
                BtnCustom6Kind = cxbkStandard
                BtnCustom7Kind = cxbkStandard
                BtnCustom8Kind = cxbkStandard
                BtnCustom9Kind = cxbkStandard
                BtnCustom10Kind = cxbkStandard
                ApprovedRollbackButton = nbCustom1
                ApprovedCommitButton = nbCustom1
                ButtonNativeStyle = False
              end
            end
          end
          object ABcxSplitter2: TABcxSplitter
            Left = 0
            Top = 185
            Width = 547
            Height = 8
            HotZoneClassName = 'TcxMediaPlayer8Style'
            AlignSplitter = salTop
            InvertDirection = True
            Control = ABcxPageControl2
          end
        end
        object ABcxSplitter1: TABcxSplitter
          Left = 188
          Top = 1
          Width = 8
          Height = 453
          HotZoneClassName = 'TcxMediaPlayer8Style'
          AlignSplitter = salRight
          InvertDirection = True
          Control = Panel3
        end
        object Panel7: TPanel
          Left = 1
          Top = 1
          Width = 187
          Height = 453
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 2
          object ABcxGrid1: TABcxGrid
            Left = 0
            Top = 0
            Width = 187
            Height = 412
            Align = alClient
            TabOrder = 0
            object ABcxGrid1ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView
              PopupMenu.AutoHotkeys = maManual
              PopupMenu.CloseFootStr = False
              PopupMenu.LinkTableView = ABcxGrid1ABcxGridDBBandedTableView1
              PopupMenu.AutoApplyBestFit = True
              PopupMenu.AutoCreateAllItem = True
              Navigator.Buttons.CustomButtons = <>
              DataController.DataSource = ABDatasource1
              DataController.Filter.Options = [fcoCaseInsensitive]
              DataController.Filter.AutoDataSetFilter = True
              DataController.Filter.TranslateBetween = True
              DataController.Filter.TranslateIn = True
              DataController.Filter.TranslateLike = True
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              OptionsBehavior.AlwaysShowEditor = True
              OptionsBehavior.FocusCellOnTab = True
              OptionsBehavior.GoToNextCellOnEnter = True
              OptionsCustomize.ColumnsQuickCustomization = True
              OptionsCustomize.DataRowSizing = True
              OptionsSelection.MultiSelect = True
              OptionsView.GroupByBox = False
              OptionsView.Indicator = True
              Bands = <
                item
                end>
              ExtPopupMenu.AutoHotkeys = maManual
              ExtPopupMenu.CloseFootStr = False
              ExtPopupMenu.LinkTableView = ABcxGrid1ABcxGridDBBandedTableView1
              ExtPopupMenu.AutoApplyBestFit = True
              ExtPopupMenu.AutoCreateAllItem = True
            end
            object ABcxGrid1Level1: TcxGridLevel
              GridView = ABcxGrid1ABcxGridDBBandedTableView1
            end
          end
          object Panel8: TPanel
            Left = 0
            Top = 412
            Width = 187
            Height = 41
            Align = alBottom
            TabOrder = 1
            object ABBillInputButton1: TABBillInputButton
              Left = 8
              Top = 6
              Width = 169
              Height = 25
              Caption = #20174#37319#36141#35745#21010#23548#20837
              LookAndFeel.Kind = lfFlat
              TabOrder = 0
              ShowProgressBar = False
              Code = 'FromERP_Buy_Plan'
              MainDataSet = ABQuery1
              DetailDataSet = ABQuery1_1
              DetailFieldNames = 
                'Oi_ComeFromCode,Oi_ComeFromItemCode,Oi_Ma_Guid,Oi_Mu_Guid,Oi_Qua' +
                'ntity,Oi_Price,Oi_Discount,Oi_Quality,Oi_Order,Oi_ReMark'
            end
          end
        end
      end
      object ABDBNavigator1: TABDBNavigator
        Left = 0
        Top = 0
        Width = 744
        Height = 25
        Align = alTop
        BevelOuter = bvNone
        Caption = 'ABDBNavigator1'
        ShowCaption = False
        TabOrder = 1
        BigGlyph = False
        ImageLayout = blGlyphLeft
        DataSource = ABDatasource1
        VisibleButtons = [nbInsertSpacer, nbInsert, nbCopy, nbDelete, nbEdit, nbPostSpacer, nbPost, nbCancel, nbQuerySpacer, nbQuery, nbCustom1]
        ButtonRangeType = RtCustom
        BtnCustom1ImageIndex = -1
        BtnCustom2ImageIndex = -1
        BtnCustom3ImageIndex = -1
        BtnCustom4ImageIndex = -1
        BtnCustom5ImageIndex = -1
        BtnCustom6ImageIndex = -1
        BtnCustom7ImageIndex = -1
        BtnCustom8ImageIndex = -1
        BtnCustom9ImageIndex = -1
        BtnCustom10ImageIndex = -1
        BtnCustom1Caption = #35831#27714#23457#25209
        BtnCustom2Caption = #33258#23450#20041'2'
        BtnCustom3Caption = #33258#23450#20041'3'
        BtnCustom4Caption = #33258#23450#20041'4'
        BtnCustom5Caption = #33258#23450#20041'5'
        BtnCustom6Caption = #33258#23450#20041'6'
        BtnCustom7Caption = #33258#23450#20041'7'
        BtnCustom8Caption = #33258#23450#20041'8'
        BtnCustom9Caption = #33258#23450#20041'9'
        BtnCustom10Caption = #33258#23450#20041'10'
        BtnCustom1Kind = cxbkStandard
        BtnCustom2Kind = cxbkStandard
        BtnCustom3Kind = cxbkStandard
        BtnCustom4Kind = cxbkStandard
        BtnCustom5Kind = cxbkStandard
        BtnCustom6Kind = cxbkStandard
        BtnCustom7Kind = cxbkStandard
        BtnCustom8Kind = cxbkStandard
        BtnCustom9Kind = cxbkStandard
        BtnCustom10Kind = cxbkDropDown
        ApproveBill = 'ERP_Buy_Order'
        ApproveMainGuidField = 'Or_Guid'
        ApproveStateField = 'Or_State'
        ApprovedRollbackButton = nbNull
        ApprovedCommitButton = nbCustom1
        ButtonNativeStyle = False
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = #23457#25209#20013
      ImageIndex = 1
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 744
        Height = 480
        Align = alClient
        BevelOuter = bvLowered
        TabOrder = 0
        object Panel4: TPanel
          Left = 196
          Top = 1
          Width = 547
          Height = 478
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          object ABcxPageControl4: TABcxPageControl
            Left = 0
            Top = 0
            Width = 547
            Height = 185
            Align = alTop
            TabOrder = 0
            Properties.ActivePage = cxTabSheet4
            Properties.CustomButtons.Buttons = <>
            Properties.HideTabs = True
            LookAndFeel.Kind = lfFlat
            ActivePageIndex = 0
            ClientRectBottom = 181
            ClientRectLeft = 2
            ClientRectRight = 543
            ClientRectTop = 2
            object cxTabSheet4: TcxTabSheet
              Caption = #22522#26412#20449#24687
              ImageIndex = 0
              object ABDBPanel2: TABDBPanel
                Left = 0
                Top = 0
                Width = 541
                Height = 179
                Align = alClient
                BevelOuter = bvNone
                Caption = 'ABDBPanel1'
                ShowCaption = False
                TabOrder = 0
                ReadOnly = False
                DataSource = ABDatasource2
                AddAnchors_akRight = True
                AddAnchors_akBottom = True
                AutoHeight = True
                AutoWidth = True
              end
            end
          end
          object ABcxPageControl5: TABcxPageControl
            Left = 0
            Top = 193
            Width = 547
            Height = 285
            Align = alClient
            TabOrder = 1
            Properties.ActivePage = cxTabSheet5
            Properties.CustomButtons.Buttons = <>
            Properties.HideTabs = True
            LookAndFeel.Kind = lfFlat
            ActivePageIndex = 0
            ClientRectBottom = 281
            ClientRectLeft = 2
            ClientRectRight = 543
            ClientRectTop = 2
            object cxTabSheet5: TcxTabSheet
              Caption = #32852#31995#20154#20449#24687
              ImageIndex = 0
              object ABcxGrid3: TABcxGrid
                Left = 0
                Top = 0
                Width = 541
                Height = 279
                Align = alClient
                TabOrder = 0
                ExplicitTop = 25
                ExplicitHeight = 254
                object ABcxGridDBBandedTableView2: TABcxGridDBBandedTableView
                  PopupMenu.AutoHotkeys = maManual
                  PopupMenu.CloseFootStr = False
                  PopupMenu.LinkTableView = ABcxGridDBBandedTableView2
                  PopupMenu.AutoApplyBestFit = True
                  PopupMenu.AutoCreateAllItem = True
                  Navigator.Buttons.CustomButtons = <>
                  DataController.DataSource = ABDatasource2_1
                  DataController.Filter.Options = [fcoCaseInsensitive]
                  DataController.Filter.AutoDataSetFilter = True
                  DataController.Filter.TranslateBetween = True
                  DataController.Filter.TranslateIn = True
                  DataController.Filter.TranslateLike = True
                  DataController.Summary.DefaultGroupSummaryItems = <>
                  DataController.Summary.FooterSummaryItems = <>
                  DataController.Summary.SummaryGroups = <>
                  OptionsBehavior.AlwaysShowEditor = True
                  OptionsBehavior.FocusCellOnTab = True
                  OptionsBehavior.GoToNextCellOnEnter = True
                  OptionsCustomize.ColumnsQuickCustomization = True
                  OptionsCustomize.DataRowSizing = True
                  OptionsSelection.MultiSelect = True
                  OptionsView.GroupByBox = False
                  OptionsView.Indicator = True
                  Bands = <
                    item
                    end>
                  ExtPopupMenu.AutoHotkeys = maManual
                  ExtPopupMenu.CloseFootStr = False
                  ExtPopupMenu.LinkTableView = ABcxGridDBBandedTableView2
                  ExtPopupMenu.AutoApplyBestFit = True
                  ExtPopupMenu.AutoCreateAllItem = True
                end
                object cxGridLevel2: TcxGridLevel
                  GridView = ABcxGridDBBandedTableView2
                end
              end
            end
          end
          object ABcxSplitter3: TABcxSplitter
            Left = 0
            Top = 185
            Width = 547
            Height = 8
            HotZoneClassName = 'TcxMediaPlayer8Style'
            AlignSplitter = salTop
            InvertDirection = True
            Control = ABcxPageControl4
          end
        end
        object ABcxGrid4: TABcxGrid
          Left = 1
          Top = 1
          Width = 187
          Height = 478
          Align = alClient
          TabOrder = 1
          object ABcxGridDBBandedTableView3: TABcxGridDBBandedTableView
            PopupMenu.AutoHotkeys = maManual
            PopupMenu.CloseFootStr = False
            PopupMenu.LinkTableView = ABcxGridDBBandedTableView3
            PopupMenu.AutoApplyBestFit = True
            PopupMenu.AutoCreateAllItem = True
            Navigator.Buttons.CustomButtons = <>
            DataController.DataSource = ABDatasource2
            DataController.Filter.Options = [fcoCaseInsensitive]
            DataController.Filter.AutoDataSetFilter = True
            DataController.Filter.TranslateBetween = True
            DataController.Filter.TranslateIn = True
            DataController.Filter.TranslateLike = True
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsBehavior.AlwaysShowEditor = True
            OptionsBehavior.FocusCellOnTab = True
            OptionsBehavior.GoToNextCellOnEnter = True
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsCustomize.DataRowSizing = True
            OptionsSelection.MultiSelect = True
            OptionsView.GroupByBox = False
            OptionsView.Indicator = True
            Bands = <
              item
              end>
            ExtPopupMenu.AutoHotkeys = maManual
            ExtPopupMenu.CloseFootStr = False
            ExtPopupMenu.LinkTableView = ABcxGridDBBandedTableView3
            ExtPopupMenu.AutoApplyBestFit = True
            ExtPopupMenu.AutoCreateAllItem = True
          end
          object cxGridLevel3: TcxGridLevel
            GridView = ABcxGridDBBandedTableView3
          end
        end
        object ABcxSplitter4: TABcxSplitter
          Left = 188
          Top = 1
          Width = 8
          Height = 478
          HotZoneClassName = 'TcxMediaPlayer8Style'
          AlignSplitter = salRight
          InvertDirection = True
          Control = Panel4
        end
      end
    end
    object cxTabSheet3: TcxTabSheet
      Caption = #24050#23436#25104
      ImageIndex = 2
      object Panel5: TPanel
        Left = 0
        Top = 41
        Width = 744
        Height = 439
        Align = alClient
        BevelOuter = bvLowered
        TabOrder = 0
        object Panel6: TPanel
          Left = 196
          Top = 1
          Width = 547
          Height = 437
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          object ABcxPageControl6: TABcxPageControl
            Left = 0
            Top = 0
            Width = 547
            Height = 185
            Align = alTop
            TabOrder = 0
            Properties.ActivePage = cxTabSheet6
            Properties.CustomButtons.Buttons = <>
            Properties.HideTabs = True
            LookAndFeel.Kind = lfFlat
            ActivePageIndex = 0
            ClientRectBottom = 181
            ClientRectLeft = 2
            ClientRectRight = 543
            ClientRectTop = 2
            object cxTabSheet6: TcxTabSheet
              Caption = #22522#26412#20449#24687
              ImageIndex = 0
              object ABDBPanel3: TABDBPanel
                Left = 0
                Top = 0
                Width = 541
                Height = 179
                Align = alClient
                BevelOuter = bvNone
                Caption = 'ABDBPanel1'
                ShowCaption = False
                TabOrder = 0
                ReadOnly = False
                DataSource = ABDatasource3
                AddAnchors_akRight = True
                AddAnchors_akBottom = True
                AutoHeight = True
                AutoWidth = True
              end
            end
          end
          object ABcxPageControl7: TABcxPageControl
            Left = 0
            Top = 193
            Width = 547
            Height = 244
            Align = alClient
            TabOrder = 1
            Properties.ActivePage = cxTabSheet7
            Properties.CustomButtons.Buttons = <>
            Properties.HideTabs = True
            LookAndFeel.Kind = lfFlat
            ActivePageIndex = 0
            ClientRectBottom = 240
            ClientRectLeft = 2
            ClientRectRight = 543
            ClientRectTop = 2
            object cxTabSheet7: TcxTabSheet
              Caption = #32852#31995#20154#20449#24687
              ImageIndex = 0
              object ABcxGrid5: TABcxGrid
                Left = 0
                Top = 0
                Width = 541
                Height = 238
                Align = alClient
                TabOrder = 0
                ExplicitTop = 25
                ExplicitHeight = 213
                object ABcxGridDBBandedTableView4: TABcxGridDBBandedTableView
                  PopupMenu.AutoHotkeys = maManual
                  PopupMenu.CloseFootStr = False
                  PopupMenu.LinkTableView = ABcxGridDBBandedTableView4
                  PopupMenu.AutoApplyBestFit = True
                  PopupMenu.AutoCreateAllItem = True
                  Navigator.Buttons.CustomButtons = <>
                  DataController.DataSource = ABDatasource3_1
                  DataController.Filter.Options = [fcoCaseInsensitive]
                  DataController.Filter.AutoDataSetFilter = True
                  DataController.Filter.TranslateBetween = True
                  DataController.Filter.TranslateIn = True
                  DataController.Filter.TranslateLike = True
                  DataController.Summary.DefaultGroupSummaryItems = <>
                  DataController.Summary.FooterSummaryItems = <>
                  DataController.Summary.SummaryGroups = <>
                  OptionsBehavior.AlwaysShowEditor = True
                  OptionsBehavior.FocusCellOnTab = True
                  OptionsBehavior.GoToNextCellOnEnter = True
                  OptionsCustomize.ColumnsQuickCustomization = True
                  OptionsCustomize.DataRowSizing = True
                  OptionsSelection.MultiSelect = True
                  OptionsView.GroupByBox = False
                  OptionsView.Indicator = True
                  Bands = <
                    item
                    end>
                  ExtPopupMenu.AutoHotkeys = maManual
                  ExtPopupMenu.CloseFootStr = False
                  ExtPopupMenu.LinkTableView = ABcxGridDBBandedTableView4
                  ExtPopupMenu.AutoApplyBestFit = True
                  ExtPopupMenu.AutoCreateAllItem = True
                end
                object cxGridLevel4: TcxGridLevel
                  GridView = ABcxGridDBBandedTableView4
                end
              end
            end
          end
          object ABcxSplitter5: TABcxSplitter
            Left = 0
            Top = 185
            Width = 547
            Height = 8
            HotZoneClassName = 'TcxMediaPlayer8Style'
            AlignSplitter = salTop
            InvertDirection = True
            Control = ABcxPageControl6
          end
        end
        object ABcxGrid6: TABcxGrid
          Left = 1
          Top = 1
          Width = 187
          Height = 437
          Align = alClient
          TabOrder = 1
          object ABcxGridDBBandedTableView5: TABcxGridDBBandedTableView
            PopupMenu.AutoHotkeys = maManual
            PopupMenu.CloseFootStr = False
            PopupMenu.LinkTableView = ABcxGridDBBandedTableView5
            PopupMenu.AutoApplyBestFit = True
            PopupMenu.AutoCreateAllItem = True
            Navigator.Buttons.CustomButtons = <>
            DataController.DataSource = ABDatasource3
            DataController.Filter.Options = [fcoCaseInsensitive]
            DataController.Filter.AutoDataSetFilter = True
            DataController.Filter.TranslateBetween = True
            DataController.Filter.TranslateIn = True
            DataController.Filter.TranslateLike = True
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsBehavior.AlwaysShowEditor = True
            OptionsBehavior.FocusCellOnTab = True
            OptionsBehavior.GoToNextCellOnEnter = True
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsCustomize.DataRowSizing = True
            OptionsSelection.MultiSelect = True
            OptionsView.GroupByBox = False
            OptionsView.Indicator = True
            Bands = <
              item
              end>
            ExtPopupMenu.AutoHotkeys = maManual
            ExtPopupMenu.CloseFootStr = False
            ExtPopupMenu.LinkTableView = ABcxGridDBBandedTableView5
            ExtPopupMenu.AutoApplyBestFit = True
            ExtPopupMenu.AutoCreateAllItem = True
          end
          object cxGridLevel5: TcxGridLevel
            GridView = ABcxGridDBBandedTableView5
          end
        end
        object ABcxSplitter6: TABcxSplitter
          Left = 188
          Top = 1
          Width = 8
          Height = 437
          HotZoneClassName = 'TcxMediaPlayer8Style'
          AlignSplitter = salRight
          InvertDirection = True
          Control = Panel6
        end
      end
      object Panel9: TPanel
        Left = 0
        Top = 0
        Width = 744
        Height = 41
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object ABQuerySelectFieldPanel1: TABQuerySelectFieldPanel
          Left = 0
          Top = -1
          Width = 561
          Height = 42
          BevelOuter = bvNone
          Caption = 'ABQuerySelectFieldPanel1'
          ShowCaption = False
          TabOrder = 0
          DataSource = ABDatasource3
          DFMControl = 
            'object ABQuerySelectFieldPanelDesignForm: TABQuerySelectFieldPan' +
            'elDesignForm'#13#10'  Left = 201'#13#10'  Top = 130'#13#10'  HorzScrollBar.Visible' +
            ' = False'#13#10'  VertScrollBar.Visible = False'#13#10'  ActiveControl = But' +
            'ton2'#13#10'  BorderIcons = [biMinimize, biMaximize]'#13#10'  Caption = #357' +
            '74#35745#26597#35810#39029#38754'#13#10'  ClientHeight = 500'#13#10'  Client' +
            'Width = 585'#13#10'  Color = clBtnFace'#13#10'  Font.Charset = DEFAULT_CHARS' +
            'ET'#13#10'  Font.Color = clWindowText'#13#10'  Font.Height = -11'#13#10'  Font.Nam' +
            'e = '#39'MS Sans Serif'#39#13#10'  Font.Style = []'#13#10'  KeyPreview = True'#13#10'  O' +
            'ldCreateOrder = False'#13#10'  Position = poDesigned'#13#10'  ShowHint = Tru' +
            'e'#13#10'  PixelsPerInch = 96'#13#10'  TextHeight = 13'#13#10'  object InnerQueryC' +
            'ontrolPanel: TABCustomPanel'#13#10'    Left = 0'#13#10'    Top = 0'#13#10'    Widt' +
            'h = 585'#13#10'    Height = 462'#13#10'    Align = alClient'#13#10'    Caption = '#39 +
            'InnerQueryControlPanel'#39#13#10'    ShowCaption = False'#13#10'    TabOrder =' +
            ' 1'#13#10'    object InnerQueryControlPanel_ABQuery3_InnerQueryPanel: ' +
            'TABcxGroupBox'#13#10'      AlignWithMargins = True'#13#10'      Left = 4'#13#10'  ' +
            '    Top = 4'#13#10'      Align = alClient'#13#10'      Ctl3D = False'#13#10'      ' +
            'ParentCtl3D = False'#13#10'      Style.BorderStyle = ebsNone'#13#10'      Ta' +
            'bOrder = 0'#13#10'      Height = 454'#13#10'      Width = 577'#13#10'      object ' +
            'InnerQueryControlPanel_ABQuery3_InnerQueryPanel_OR_Datetime_BegE' +
            'ndQueryBegin_Control: TABcxDateTimeEdit'#13#10'        Left = 107'#13#10'   ' +
            '     Top = 7'#13#10'        Properties.Kind = ckDateTime'#13#10'        Prop' +
            'erties.ReadOnly = False'#13#10'        Style.BorderStyle = ebsFlat'#13#10'  ' +
            '      TabOrder = 1'#13#10'        Width = 150'#13#10'      end'#13#10'      object' +
            ' InnerQueryControlPanel_ABQuery3_InnerQueryPanel_OR_Datetime_Beg' +
            'EndQueryBegin_Label: TABcxLabel'#13#10'        Left = 29'#13#10'        Top ' +
            '= 7'#13#10'        AutoSize = False'#13#10'        Caption = #31614#23450#26' +
            '102#38388'#13#10'        FocusControl = InnerQueryControlPanel_ABQuery' +
            '3_InnerQueryPanel_OR_Datetime_BegEndQueryBegin_Control'#13#10'        ' +
            'ParentFont = False'#13#10'        Style.Font.Charset = DEFAULT_CHARSET' +
            #13#10'        Style.Font.Color = clWindowText'#13#10'        Style.Font.He' +
            'ight = -14'#13#10'        Style.Font.Name = #23435#20307'#13#10'        Styl' +
            'e.Font.Style = []'#13#10'        Style.IsFontAssigned = True'#13#10'        ' +
            'Properties.Alignment.Vert = taVCenter'#13#10'        Properties.WordWr' +
            'ap = True'#13#10'        Transparent = True'#13#10'        Height = 21'#13#10'    ' +
            '    Width = 77'#13#10'        AnchorY = 18'#13#10'      end'#13#10'      object In' +
            'nerQueryControlPanel_ABQuery3_InnerQueryPanel_OR_Datetime_BegEnd' +
            'QueryEnd_Control: TABcxDateTimeEdit'#13#10'        Left = 315'#13#10'       ' +
            ' Top = 7'#13#10'        Properties.Kind = ckDateTime'#13#10'        Properti' +
            'es.ReadOnly = False'#13#10'        Style.BorderStyle = ebsFlat'#13#10'      ' +
            '  TabOrder = 3'#13#10'        Width = 150'#13#10'      end'#13#10'      object Inn' +
            'erQueryControlPanel_ABQuery3_InnerQueryPanel_OR_Datetime_BegEndQ' +
            'ueryEnd_Label: TABcxLabel'#13#10'        Left = 261'#13#10'        Top = 7'#13#10 +
            '        AutoSize = False'#13#10'        Caption = #27490'#13#10'        Focu' +
            'sControl = InnerQueryControlPanel_ABQuery3_InnerQueryPanel_OR_Da' +
            'tetime_BegEndQueryEnd_Control'#13#10'        ParentFont = False'#13#10'     ' +
            '   Style.Font.Charset = DEFAULT_CHARSET'#13#10'        Style.Font.Colo' +
            'r = clWindowText'#13#10'        Style.Font.Height = -14'#13#10'        Style' +
            '.Font.Name = #23435#20307'#13#10'        Style.Font.Style = []'#13#10'      ' +
            '  Style.IsFontAssigned = True'#13#10'        Properties.Alignment.Vert' +
            ' = taVCenter'#13#10'        Properties.WordWrap = True'#13#10'        Transp' +
            'arent = True'#13#10'        Height = 21'#13#10'        Width = 51'#13#10'        A' +
            'nchorY = 18'#13#10'      end'#13#10'    end'#13#10'  end'#13#10'end'
          QueryButtonPanelWidth = 88
          HideInputQueryButton = True
        end
        object ABDBNavigator7: TABDBNavigator
          Left = 560
          Top = 7
          Width = 179
          Height = 27
          BevelOuter = bvNone
          Caption = 'ABDBNavigator1'
          ShowCaption = False
          TabOrder = 1
          BigGlyph = False
          ImageLayout = blGlyphLeft
          DataSource = ABDatasource3
          VisibleButtons = [nbQuery, nbCustom1]
          ButtonRangeType = RtCustom
          BtnCustom1ImageIndex = -1
          BtnCustom2ImageIndex = -1
          BtnCustom3ImageIndex = -1
          BtnCustom4ImageIndex = -1
          BtnCustom5ImageIndex = -1
          BtnCustom6ImageIndex = -1
          BtnCustom7ImageIndex = -1
          BtnCustom8ImageIndex = -1
          BtnCustom9ImageIndex = -1
          BtnCustom10ImageIndex = -1
          BtnCustom1Caption = #25764#28040#24403#21069#21333#25454
          BtnCustom2Caption = #33258#23450#20041'2'
          BtnCustom3Caption = #33258#23450#20041'3'
          BtnCustom4Caption = #33258#23450#20041'4'
          BtnCustom5Caption = #33258#23450#20041'5'
          BtnCustom6Caption = #33258#23450#20041'6'
          BtnCustom7Caption = #33258#23450#20041'7'
          BtnCustom8Caption = #33258#23450#20041'8'
          BtnCustom9Caption = #33258#23450#20041'9'
          BtnCustom10Caption = #33258#23450#20041'10'
          BtnCustom1Kind = cxbkStandard
          BtnCustom2Kind = cxbkCommandLink
          BtnCustom3Kind = cxbkStandard
          BtnCustom4Kind = cxbkStandard
          BtnCustom5Kind = cxbkStandard
          BtnCustom6Kind = cxbkStandard
          BtnCustom7Kind = cxbkStandard
          BtnCustom8Kind = cxbkStandard
          BtnCustom9Kind = cxbkStandard
          BtnCustom10Kind = cxbkDropDown
          ApproveBill = 'ERP_Buy_Order'
          ApproveMainGuidField = 'OR_Guid'
          ApproveStateField = 'OR_State'
          ApprovedRollbackButton = nbCustom1
          ApprovedCommitButton = nbNull
          ButtonNativeStyle = False
        end
      end
    end
  end
  object ABDBNavigator3: TABDBNavigator
    Left = 0
    Top = 0
    Width = 750
    Height = 25
    Align = alTop
    BevelOuter = bvNone
    Caption = 'ABDBNavigator1'
    ShowCaption = False
    TabOrder = 2
    BigGlyph = False
    ImageLayout = blGlyphLeft
    DataSource = ABDatasource1
    VisibleButtons = [nbReport, nbExitSpacer, nbExit]
    ButtonRangeType = RtReportAndExit
    BtnCustom1ImageIndex = -1
    BtnCustom2ImageIndex = -1
    BtnCustom3ImageIndex = -1
    BtnCustom4ImageIndex = -1
    BtnCustom5ImageIndex = -1
    BtnCustom6ImageIndex = -1
    BtnCustom7ImageIndex = -1
    BtnCustom8ImageIndex = -1
    BtnCustom9ImageIndex = -1
    BtnCustom10ImageIndex = -1
    BtnCustom1Caption = #33258#23450#20041'1'
    BtnCustom2Caption = #33258#23450#20041'2'
    BtnCustom3Caption = #33258#23450#20041'3'
    BtnCustom4Caption = #33258#23450#20041'4'
    BtnCustom5Caption = #33258#23450#20041'5'
    BtnCustom6Caption = #33258#23450#20041'6'
    BtnCustom7Caption = #33258#23450#20041'7'
    BtnCustom8Caption = #33258#23450#20041'8'
    BtnCustom9Caption = #33258#23450#20041'9'
    BtnCustom10Caption = #33258#23450#20041'10'
    BtnCustom1Kind = cxbkStandard
    BtnCustom2Kind = cxbkStandard
    BtnCustom3Kind = cxbkStandard
    BtnCustom4Kind = cxbkStandard
    BtnCustom5Kind = cxbkStandard
    BtnCustom6Kind = cxbkStandard
    BtnCustom7Kind = cxbkStandard
    BtnCustom8Kind = cxbkStandard
    BtnCustom9Kind = cxbkStandard
    BtnCustom10Kind = cxbkStandard
    ApprovedRollbackButton = nbCustom1
    ApprovedCommitButton = nbCustom1
    ButtonNativeStyle = False
  end
  object ABDatasource1: TABDatasource
    AutoEdit = False
    DataSet = ABQuery1
    Left = 96
    Top = 112
  end
  object ABQuery1_1: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    MasterSource = ABDatasource1
    MasterFields = 'OR_GUID'
    DetailFields = 'OI_OR_GUID'
    SQL.Strings = (
      'select *'
      'from ERP_Buy_OrderItem'
      'where OI_OR_GUID=:OR_GUID')
    SqlUpdateDatetime = 42213.538699618050000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    ConnName = 'ERP'
    EditControlType = ctSingle
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ERP_Buy_OrderItem')
    IndexListDefs = <
      item
        Name = 'IX_ERP_Base_Cust'
        Fields = 'Cu_Order'
      end
      item
        Name = 'IX_Cu_Code'
        Fields = 'Cu_Code'
        Options = [ixUnique]
      end
      item
        Name = 'IX_Cu_Name'
        Fields = 'Cu_Name'
        Options = [ixUnique]
      end
      item
        Name = 'PK_Cu_Guid'
        Fields = 'Cu_Guid'
        Options = [ixPrimary, ixUnique]
      end>
    LoadTables.Strings = (
      'ERP_Buy_OrderItem')
    BlobFieldDefs = <
      item
        FieldName = 'Cl_Picture'
      end>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 184
    Top = 112
    ParamData = <
      item
        Name = 'OR_GUID'
        ParamType = ptInput
      end>
  end
  object ABDatasource1_1: TABDatasource
    AutoEdit = False
    DataSet = ABQuery1_1
    Left = 264
    Top = 112
  end
  object ABDatasource2_1: TABDatasource
    AutoEdit = False
    DataSet = ABQuery2_1
    Left = 264
    Top = 176
  end
  object ABQuery2_1: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    MasterSource = ABDatasource2
    MasterFields = 'OR_GUID'
    DetailFields = 'OI_OR_GUID'
    SQL.Strings = (
      'select *'
      'from ERP_Buy_OrderItem'
      'where OI_OR_GUID=:OR_GUID')
    SqlUpdateDatetime = 42213.538699618050000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    ConnName = 'ERP'
    EditControlType = ctSingle
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ERP_Buy_OrderItem')
    IndexListDefs = <
      item
        Name = 'IX_ERP_Base_Cust'
        Fields = 'Cu_Order'
      end
      item
        Name = 'IX_Cu_Code'
        Fields = 'Cu_Code'
        Options = [ixUnique]
      end
      item
        Name = 'IX_Cu_Name'
        Fields = 'Cu_Name'
        Options = [ixUnique]
      end
      item
        Name = 'PK_Cu_Guid'
        Fields = 'Cu_Guid'
        Options = [ixPrimary, ixUnique]
      end>
    LoadTables.Strings = (
      'ERP_Buy_OrderItem')
    BlobFieldDefs = <
      item
        FieldName = 'Cl_Picture'
      end>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 184
    Top = 176
    ParamData = <
      item
        Name = 'OR_GUID'
        ParamType = ptInput
      end>
  end
  object ABDatasource2: TABDatasource
    AutoEdit = False
    DataSet = ABQuery2
    Left = 96
    Top = 176
  end
  object ABQuery2: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    SQL.Strings = (
      'select  * '
      'from ERP_Buy_Order'
      'where Or_State='#39'ApprovedIng'#39)
    SqlUpdateDatetime = 42230.847178287040000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    ConnName = 'ERP'
    EditControlType = ctSingle
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ERP_Buy_Order')
    IndexListDefs = <
      item
        Name = 'IX_ERP_Base_Cust'
        Fields = 'Cu_Order'
      end
      item
        Name = 'IX_Cu_Code'
        Fields = 'Cu_Code'
        Options = [ixUnique]
      end
      item
        Name = 'IX_Cu_Name'
        Fields = 'Cu_Name'
        Options = [ixUnique]
      end
      item
        Name = 'PK_Cu_Guid'
        Fields = 'Cu_Guid'
        Options = [ixPrimary, ixUnique]
      end>
    LoadTables.Strings = (
      'ERP_Buy_Order')
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 24
    Top = 176
  end
  object ABDatasource3_1: TABDatasource
    AutoEdit = False
    DataSet = ABQuery3_1
    Left = 264
    Top = 248
  end
  object ABQuery3_1: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    MasterSource = ABDatasource3
    MasterFields = 'OR_GUID'
    DetailFields = 'OI_OR_GUID'
    SQL.Strings = (
      'select *'
      'from ERP_Buy_OrderItem'
      'where OI_OR_GUID=:OR_GUID')
    SqlUpdateDatetime = 42213.538699618050000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    ConnName = 'ERP'
    EditControlType = ctSingle
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ERP_Buy_OrderItem')
    IndexListDefs = <
      item
        Name = 'IX_ERP_Base_Cust'
        Fields = 'Cu_Order'
      end
      item
        Name = 'IX_Cu_Code'
        Fields = 'Cu_Code'
        Options = [ixUnique]
      end
      item
        Name = 'IX_Cu_Name'
        Fields = 'Cu_Name'
        Options = [ixUnique]
      end
      item
        Name = 'PK_Cu_Guid'
        Fields = 'Cu_Guid'
        Options = [ixPrimary, ixUnique]
      end>
    LoadTables.Strings = (
      'ERP_Buy_OrderItem')
    BlobFieldDefs = <
      item
        FieldName = 'Cl_Picture'
      end>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 184
    Top = 248
    ParamData = <
      item
        Name = 'OR_GUID'
        ParamType = ptInput
      end>
  end
  object ABDatasource3: TABDatasource
    AutoEdit = False
    DataSet = ABQuery3
    Left = 96
    Top = 248
  end
  object ABQuery3: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    SQL.Strings = (
      'select  * '
      'from ERP_Buy_Order'
      'where Or_State='#39'Finish'#39)
    SqlUpdateDatetime = 42226.558527372680000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    ConnName = 'ERP'
    EditControlType = ctSingle
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ERP_Buy_Order')
    IndexListDefs = <
      item
        Name = 'IX_ERP_Base_Cust'
        Fields = 'Cu_Order'
      end
      item
        Name = 'IX_Cu_Code'
        Fields = 'Cu_Code'
        Options = [ixUnique]
      end
      item
        Name = 'IX_Cu_Name'
        Fields = 'Cu_Name'
        Options = [ixUnique]
      end
      item
        Name = 'PK_Cu_Guid'
        Fields = 'Cu_Guid'
        Options = [ixPrimary, ixUnique]
      end>
    LoadTables.Strings = (
      'ERP_Buy_Order')
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 24
    Top = 248
  end
  object ABQuery1: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    SQL.Strings = (
      'select    * '
      'from ERP_Buy_Order'
      'where Or_State='#39'Input'#39)
    SqlUpdateDatetime = 42226.557943969910000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    ConnName = 'ERP'
    EditControlType = ctSingle
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ERP_Buy_Order')
    IndexListDefs = <
      item
        Name = 'IX_ERP_Buy_Order'
        Fields = 'Or_Datetime;Or_State'
      end
      item
        Name = 'IX_ERP_Buy_Order_1'
        Fields = 'Or_Code'
        Options = [ixUnique]
      end
      item
        Name = 'PK_ERP_Buy_Order'
        Fields = 'Or_Guid'
        Options = [ixPrimary, ixUnique]
      end>
    LoadTables.Strings = (
      'ERP_Buy_Order')
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 24
    Top = 112
  end
end
