/*
 
exec  ABFramework.dbo.Proc_BeforeTest

--测试的SQL.....

exec  ABFramework.dbo.Proc_AfterTest
 

print db_Name()

EXEC sp_renamedb 'tempSQL', 'ABFramework'
EXEC sp_renamedb 'tempSQL_20110321100130', 'tempSQL'


--语句中事务的操作
BEGIN TRANSACTION 
  
  --第一表等更新与删除操作,@ReturnCode可为第一操作成功的标志
  IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback 
  --第二表等更新与删除操作
  IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback 

  --提交事务
  COMMIT TRANSACTION 
GOTO EndSave 

--异常撤消事务
QuitWithRollback: 
  IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION 
EndSave: 
 */
go
--所有库都需要的过程
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_GetCOLUMNPROPERTY]') and xtype in (N'FN', N'IF', N'TF')) 
  drop function  Func_GetCOLUMNPROPERTY 
go 
CREATE function  Func_GetCOLUMNPROPERTY(@TableName nvarchar(100),@FieldName nvarchar(100),@Type nvarchar(100)) 
RETURNS varchar(1000) 
as 
begin 
  declare @tempValue varchar(1000) 
  set  @tempValue=COLUMNPROPERTY(OBJECT_ID(@TableName),@FieldName,@Type) 
  return(@tempValue) 
end 
go

--仅框架库需要的过程
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_BeforeLargeData]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) 
drop procedure [dbo].[Proc_BeforeLargeData] 
GO 
CREATE PROCEDURE Proc_BeforeLargeData(@aDatabaseName nvarchar(100))
as  
  set nocount on
  
  if @aDatabaseName=''
    set @aDatabaseName=db_name()

	--停止所有触发器 
	exec Proc_DisableTrigger @aDatabaseName 
  --停止外键
	exec Proc_DisableForeignKey @aDatabaseName  

  set nocount off

go
--大数据量操作后做的过程
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_AfterLargeData]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure Proc_AfterLargeData
go
CREATE PROCEDURE Proc_AfterLargeData(@aDatabaseName nvarchar(100))
as  
  set nocount on

  if @aDatabaseName=''
    set @aDatabaseName=db_name()

	--启动所有触发器 
	exec Proc_EnableTrigger @aDatabaseName 
  --启动外键
	exec Proc_EnableForeignKey @aDatabaseName 

  set nocount off
go



--测试前做的 
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_BeforeTest]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) 
drop procedure [dbo].[Proc_BeforeTest] 
GO 
create proc Proc_BeforeTest
as 
	--测试材料:
	--在开始测试前,先运行下面的这二条命令
	--这二条命令将清除SQL Server的数据和过程缓冲区，
	--这样能够使我们在每次执行查询时在同一个起点上，否则，每次执行查询得到的结果就不具有可比性了
	CHECKPOINT --强制将当前数据库的所有脏页写到磁盘上
	DBCC DROPCLEANBUFFERS   --从缓冲池中删除所有清除缓冲区。
	DBCC FREEPROCCACHE      --从过程高速缓存中删除所有元素。
	--打开查询的描述
	SET STATISTICS IO on
	SET STATISTICS TIME on

  print convert(nvarchar(100),getdate(),121)+' '+'测试准备完成'
go 
--测试后做的 
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_AfterTest]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) 
drop procedure [dbo].[Proc_AfterTest] 
GO 
create proc Proc_AfterTest
as 
	--关闭查询的描述
  SET STATISTICS IO off
  SET STATISTICS TIME off
  print convert(nvarchar(100),getdate(),121)+' '+'测试完成'
go 

/*
exec  ABFramework.dbo.Proc_BeforeTest

--测试的SQL.....

exec  ABFramework.dbo.Proc_AfterTest
*/

--设置查询字段或表信息等处理中不包含的临时表或系统表
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_InExcludeTableName]') and xtype in (N'FN', N'IF', N'TF'))
drop function  Func_InExcludeTableName
go
CREATE function  Func_InExcludeTableName(@TableName nvarchar(100))
RETURNS bit
as
begin
  declare @tempBit bit
  if charindex(','+@TableName+',',',syssegments,sysconstraints,dtproperties,ABSys_Temp_GetTableInfo,ABSys_Temp_GetFieldInfo,ABSys_Temp_GetTableIndexInfo,ABSys_Temp_SysProperties,PublicVarTable,' )>0
    set @tempBit=1
  else 
    set @tempBit=0

  return(@tempBit)
end
go 
--select  dbo.Func_InExcludeTableName('syssegments') select  dbo.Func_InExcludeTableName('aaaaaaa')

/*
取得表占用空间的信息
1. exec sp_spaceused '表名'            （SQL统计数据，大量事务操作后可能不准）
2. exec sp_spaceused '表名', true       (更新表的空间大小，准确的表空大小，但可能会花些统计时间）
3. exec sp_spaceused                    (数据库大小查询）
4. exec sp_MSforeachtable "exec sp_spaceused '?'"     (所有用户表空间表小，SQL统计数据，，大量事务操作后可能不准）
5. exec sp_MSforeachtable "exec sp_spaceused '?',true"    (所有用户表空间表小，大数据库慎用）
*/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_GetTableSizeInfo]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) 
drop procedure [dbo].[Proc_GetTableSizeInfo] 
GO 
create proc Proc_GetTableSizeInfo
as 
	if exists (select * from tempdb.dbo.sysobjects where id = object_id(N'tempdb.[dbo].[##Proc_GetTableSizeInfo_table1]'))
  	drop table ##Proc_GetTableSizeInfo_table1

	create table ##Proc_GetTableSizeInfo_table1(name varchar(255), rows bigint, reserved varchar(20), data varchar(20), index_size varchar(20), unused varchar(20))
	exec sp_MSforeachtable "insert into ##Proc_GetTableSizeInfo_table1 exec sp_spaceused '?',true" 
  
  select * from 
  (
	  select name '表名',Rows '行数', reserved/1024.0 '保留的空间量MB(后面几项之和)',Data/1024.0 '数据使用的空间量MB',index_size/1024.0 '索引所使用的空间量MB',Unused/1024.0 '未用的空间量MB' ,
	          (reserved +Data+index_size+Unused)/1024.0  '总计MB'
	  from (
		select  CASE WHEN (GROUPING(name) = 1) THEN '总计'
	            ELSE name
	          END  name,
	          sum(Rows) Rows,
	          sum(cast(replace(reserved,' KB','') as decimal(18,4))) reserved,
	          sum(cast(replace(Data,' KB','') as decimal(18,4))) Data,
	          sum(cast(replace(index_size,' KB','') as decimal(18,4))) index_size,
	          sum(cast(replace(Unused,' KB','') as decimal(18,4))) Unused 
	  from ##Proc_GetTableSizeInfo_table1 
	  group by name with CUBE
	  ) aa
	) bb
	order by 总计MB desc

  exec sp_spaceused 
go 

--exec Proc_GetTableSizeInfo
--取得数据库文件目录
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_Getdbpath]') and xtype in (N'FN', N'IF', N'TF')) 
drop function [dbo].[Func_Getdbpath] 
GO
create function Func_Getdbpath(@dbname sysname) 
returns nvarchar(260) 
as 
begin 
	declare @re nvarchar(260) 
	if @dbname is null or db_id(@dbname) is null 
	select @re=rtrim(reverse(filename)) from master..sysdatabases where name='master' 
	else 
	select @re=rtrim(reverse(filename)) from master..sysdatabases where name=@dbname 
	
	if @dbname is null 
	set @re=reverse(substring(@re,charindex('＼',@re)+5,260))+'BACKUP' 
	else 
	set @re=reverse(substring(@re,charindex('＼',@re),260)) 
	return(@re)
end 
go 

--select 数据库文件目录=dbo.Func_Getdbpath('tempdb') ,[默认SQL SERVER数据目录]=dbo.Func_Getdbpath('') ,[默认SQL SERVER备份目录]=dbo.Func_Getdbpath(null) 
 


--备份数据库到文件 
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_BackupDB]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) 
drop procedure [dbo].[Proc_BackupDB] 
GO 
create proc Proc_BackupDB
	@dbname sysname='', --要备份的数据库名称,不指定则备份当前数据库 
	@bkpath nvarchar(260)='', --备份文件的存放目录,不指定则使用SQL默认的备份目录 
	@bkfname nvarchar(260)='', --备份文件名,文件名中能用\DBNAME\代表数据库名,\DATE\代表日期,\TIME\代表时间 
	@bktype nvarchar(10)='DB', --备份类型:'DB'备份数据库,'DF' 差异备份,'LOG' 日志备份 
	@appendfile bit=1 --追加/覆盖备份文件 
as 
	declare @sql varchar(8000) 
	if isnull(@dbname,'')='' set @dbname=db_name() 
	if isnull(@bkpath,'')='' set @bkpath=dbo.f_getdbpath(null) 
	if isnull(@bkfname,'')='' set @bkfname='\DBNAME\_\DATE\_\TIME\.BAK' 
	set @bkfname=replace(replace(replace(@bkfname,'\DBNAME\',@dbname) 
	,'\DATE\',convert(varchar,getdate(),112)) 
	,'\TIME\',replace(convert(varchar,getdate(),108),':','')) 
	set @sql='backup '+case @bktype when 'LOG' then 'log ' else 'database ' end +@dbname 
	+' to disk='''+@bkpath+@bkfname 
	+''' with '+case @bktype when 'DF' then 'DIFFERENTIAL,' else '' end 
	+case @appendfile when 1 then 'NOINIT' else 'INIT' end 
	print @sql 
	exec(@sql) 
go 
--备份当前数据库 
--exec Proc_BackupDB @bkpath='c:\',@bkfname='db_\DATE\_db.bak' 

--差异备份当前数据库 
--exec Proc_BackupDB @bkpath='c:\',@bkfname='db_\DATE\_df.bak',@bktype='DF' 

--备份当前数据库日志 
--exec Proc_BackupDB @bkpath='c:\',@bkfname='db_\DATE\_log.bak',@bktype='LOG' 
go

--从文件恢复数据库 
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_RestoreDb]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) 
drop procedure [dbo].[Proc_RestoreDb] 
GO 
create proc Proc_RestoreDb 
@bkfile nvarchar(1000), --定义要恢复的备份文件名 
@dbname sysname='', --定义恢复后的数据库名,默认为备份的文件名 
@dbpath nvarchar(260)='', --恢复后的数据库存放目录,不指定则为SQL的默认数据目录 
@retype nvarchar(10)='DB', --恢复类型:'DB'完事恢复数据库,'DBNOR' 为差异恢复,日志恢复进行完整恢复,'DF' 差异备份的恢复,'LOG' 日志恢复 
@filenumber int=1, --恢复的文件号 
@overexist bit=1, --是否覆盖已存在的数据库,仅@retype为 
@killuser bit=1 --是否关闭用户使用进程,仅@overexist=1时有效 
as 
	declare @sql varchar(8000) 
	
	--得到恢复后的数据库名 
	if isnull(@dbname,'')='' 
	select @sql=reverse(@bkfile) 
	,@sql=case when charindex('.',@sql)=0 then @sql 
	else substring(@sql,charindex('.',@sql)+1,1000) end 
	,@sql=case when charindex('\',@sql)=0 then @sql 
	else left(@sql,charindex('\',@sql)-1) end 
	,@dbname=reverse(@sql) 
	
	--得到恢复后的数据库存放目录 
	if isnull(@dbpath,'')='' set @dbpath=dbo.f_getdbpath('') 
	
	--生成数据库恢复语句 
	set @sql='restore '+case @retype when 'LOG' then 'log ' else 'database ' end+@dbname 
	+' from disk='''+@bkfile+'''' 
	+' with file='+cast(@filenumber as varchar) 
	+case when @overexist=1 and @retype in('DB','DBNOR') then ',replace' else '' end 
	+case @retype when 'DBNOR' then ',NORECOVERY' else ',RECOVERY' end 
	print @sql 
	--添加移动逻辑文件的处理 
	if @retype='DB' or @retype='DBNOR' 
	begin 
	--从备份文件中获取逻辑文件名 
	declare @lfn nvarchar(128),@tp char(1),@i int 
	
	--创建临时表,保存获取的信息 
	create table #tb(ln nvarchar(128),pn nvarchar(260),tp char(1),fgn nvarchar(128),sz numeric(20,0),Msz numeric(20,0)) 
	--从备份文件中获取信息 
	insert into #tb exec('restore filelistonly from disk='''+@bkfile+'''') 
	declare #f cursor for select ln,tp from #tb 
	open #f 
	fetch next from #f into @lfn,@tp 
	set @i=0 
	while @@fetch_status=0 
	begin 
	select @sql=@sql+',move '''+@lfn+''' to '''+@dbpath+@dbname+cast(@i as varchar) 
	+case @tp when 'D' then '.mdf''' else '.ldf''' end 
	,@i=@i+1 
	fetch next from #f into @lfn,@tp 
	end 
	close #f 
	deallocate #f 
	end 
	
	--关闭用户进程处理 
	if @overexist=1 and @killuser=1 
	begin 
	declare @spid varchar(20) 
	declare #spid cursor for 
	select spid=cast(spid as varchar(20)) from master..sysprocesses where dbid=db_id(@dbname) 
	open #spid 
	fetch next from #spid into @spid 
	while @@fetch_status=0 
	begin 
	exec('kill '+@spid) 
	fetch next from #spid into @spid 
	end 
	close #spid 
	deallocate #spid 
	end 
	
	--恢复数据库 
	exec(@sql) 

go 
--完整恢复数据库 
--exec Proc_RestoreDb @bkfile='c:\db_20031015_db.bak',@dbname='db' 

--差异备份恢复 
--exec Proc_RestoreDb @bkfile='c:\db_20031015_db.bak',@dbname='db',@retype='DBNOR' 
--exec Proc_Backupdb @bkfile='c:\db_20031015_df.bak',@dbname='db',@retype='DF' 

--日志备份恢复 
--exec Proc_RestoreDb @bkfile='c:\db_20031015_db.bak',@dbname='db',@retype='DBNOR' 
--exec Proc_Backupdb @bkfile='c:\db_20031015_log.bak',@dbname='db',@retype='LOG' 

--创建作业
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_Createjob]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) 
drop procedure [dbo].[Proc_Createjob] 
GO 
create proc Proc_Createjob 
@jobname varchar(100), --作业名称 
@sql varchar(8000), --要执行的命令 
@dbname sysname='', --默认为当前的数据库名 
@freqtype varchar(6)='day', --时间周期,month 月,week 周,day 日 
@fsinterval int=1, --相对于每日的重复次数 
@time int=170000 --开始执行时间,对于重复执行的作业,将从0点到23:59分 
as 
  declare @tempBegDatetime int
  set @tempBegDatetime=convert(varchar(10),getdate()-10,112)

	if isnull(@dbname,'')='' set @dbname=db_name() 
 
  if exists(select 1 from msdb..sysjobs where name=@jobname)
    EXEC msdb..sp_delete_job @job_name =@jobname

	--创建作业 
	exec msdb..sp_add_job @job_name=@jobname ,
  @category_name = N'Database Maintenance'
	
	--创建作业步骤 
	exec msdb..sp_add_jobstep @job_name=@jobname, 
	@step_name = '数据处理', 
	@subsystem = 'TSQL', 
	@database_name=@dbname, 
	@command = @sql,  
	@retry_attempts = 5, --重试次数 
	@retry_interval = 5 --重试间隔 
	
	--创建调度 
	declare @ftype int,@fstype int,@ffactor int 
	select @ftype=case @freqtype when 'day' then 4 
	when 'week' then 8 
	when 'month' then 16 end 
	,@fstype=case @fsinterval when 1 then 0 else 8 end 
	if @fsinterval<>1 set @time=0 
	set @ffactor=case @freqtype when 'day' then 0 else 1 end 
	
	EXEC msdb..sp_add_jobschedule @job_name=@jobname, 
	@name = '时间安排', 
	@freq_type=@ftype , --每天,8 每周,16 每月 
	@freq_interval=1, --重复执行次数 
	@freq_subday_type=@fstype, --是否重复执行 
	@freq_subday_interval=@fsinterval, --重复周期 
	@freq_recurrence_factor=@ffactor,
  @active_start_date= @tempBegDatetime,
	@active_start_time=@time --下午17:00:00分执行  
 


go 
--每日执行的作业 
--exec Proc_Createjob @jobname='mm',@sql='select * from syscolumns',@freqtype='month' 
--exec Proc_Createjob @jobname='一卡通数据库每日备份',@sql='exec Proc_BackupDB @bkpath=''D:\其它项目\一卡通\DataBase\周备份\'',@bkfname=''db_\DATE\_db.bak''',@freqtype='day',@time=0 

 
--每周执行的作业 
--exec Proc_Createjob @jobname='ww',@sql='select * from syscolumns',@freqtype='week' 

--每日执行的作业 
--exec Proc_Createjob @jobname='a',@sql='select * from syscolumns' 

--每日执行的作业,每天隔4小时重复的作业 
--exec Proc_Createjob @jobname='b',@sql='select * from syscolumns',@fsinterval=4 

--完整备份（每个星期天一次）+差异备份（每天备份一次）+日志备份（每2小时备份一次） 
/*
declare @Fsql varchar(8000) 
--完整备份（每个星期天一次） 
set @Fsql='exec p_backupdb @dbname=''要备份的数据库名''' 
exec Proc_Createjob @jobname='每周备份',@sql=@Fsql,@freqtype='week' 


--差异备份（每天备份一次） 
set @Fsql='exec p_backupdb @dbname=''要备份的数据库名'',@bktype='DF'' 
exec Proc_Createjob @jobname='每天差异备份',@sql=@Fsql,@freqtype='day' 

--日志备份（每2小时备份一次） 
set @Fsql='exec p_backupdb @dbname=''要备份的数据库名'',@bktype='LOG'' 
exec Proc_Createjob @jobname='每2小时日志备份',@sql=@Fsql,@freqtype='day',@fsinterval=2 

--应用案例2 

生产数据核心库：PRODUCE 

备份方案如下： 
1.设置三个作业,分别对PRODUCE库进行每日备份,每周备份,每月备份 
2.新建三个新库,分别命名为:每日备份,每周备份,每月备份 
3.建立三个作业，分别把三个备份库还原到以上的三个新库。 

目的:当用户在produce库中有所有的数据丢失时,均能从上面的三个备份库中导入相应的TABLE数据。 
 

declare @sql varchar(8000) 

--1.建立每月备份和生成月备份数据库的作业,每月每1天下午16:40分进行: 
set @sql=' 
declare @path nvarchar(260),@fname nvarchar(100) 
set @fname=''PRODUCE_''+convert(varchar(10),getdate(),112)+''_m.bak'' 
set @path=dbo.f_getdbpath(null)+@fname 

--备份 
exec p_backupdb @dbname=''PRODUCE'',@bkfname=@fname 

--根据备份生成每月新库 
exec p_RestoreDb @bkfile=@path,@dbname=''PRODUCE_月'' 

--为周数据库恢复准备基础数据库 
exec p_RestoreDb @bkfile=@path,@dbname=''PRODUCE_周'',@retype=''DBNOR'' 

--为日数据库恢复准备基础数据库 
exec p_RestoreDb @bkfile=@path,@dbname=''PRODUCE_日'',@retype=''DBNOR'' 
' 
exec p_createjob @jobname='每月备份',@sql,@freqtype='month',@time=164000 

--2.建立每周差异备份和生成周备份数据库的作业,每周日下午17:00分进行: 
set @sql=' 
declare @path nvarchar(260),@fname nvarchar(100) 
set @fname=''PRODUCE_''+convert(varchar(10),getdate(),112)+''_w.bak'' 
set @path=dbo.f_getdbpath(null)+@fname 

--差异备份 
exec p_backupdb @dbname=''PRODUCE'',@bkfname=@fname,@bktype=''DF'' 

--差异恢复周数据库 
exec p_backupdb @bkfile=@path,@dbname=''PRODUCE_周'',@retype=''DF'' 
' 
exec p_createjob @jobname='每周差异备份',@sql,@freqtype='week',@time=170000 

--3.建立每日日志备份和生成日备份数据库的作业,每周日下午17:15分进行: 
set @sql=' 
declare @path nvarchar(260),@fname nvarchar(100) 
set @fname=''PRODUCE_''+convert(varchar(10),getdate(),112)+''_l.bak'' 
set @path=dbo.f_getdbpath(null)+@fname 

--日志备份 
exec p_backupdb @dbname=''PRODUCE'',@bkfname=@fname,@bktype=''LOG'' 

--日志恢复日数据库 
exec p_backupdb @bkfile=@path,@dbname=''PRODUCE_日'',@retype=''LOG'' 
' 
exec p_createjob @jobname='每周差异备份',@sql,@freqtype='day',@time=171500 
*/
go

--因为GUID索引导致插入效率很底，所有用时间模拟下有序的GUID  
if exists (select * from sysobjects where name=N'View_GetOrderGuid' and xtype=N'V')
  drop view   View_GetOrderGuid    
go
CREATE VIEW View_GetOrderGuid
AS
  select replace(replace(replace(replace(SYSDATETIME(),'-',''),' ',''),':',''),'.','')+cast(cast((rand()+rand())*100000000000000 as bigint) as varchar(15)) AS tempGuid
go                 
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_GetOrderGuid]') and xtype in (N'FN', N'IF', N'TF'))
drop function  Func_GetOrderGuid
go
CREATE function  Func_GetOrderGuid()
RETURNS varchar(40)
as
begin	
  RETURN(SELECT tempGuid FROM [View_GetOrderGuid])
end
go 
--select dbo.Func_GetOrderGuid()

go
--得到SQL主版本号                          
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_GetSQLServerMainVersion]') and xtype in (N'FN', N'IF', N'TF'))
drop function  Func_GetSQLServerMainVersion
go
CREATE function  Func_GetSQLServerMainVersion()
RETURNS int
as
begin	
  return(select cast(substring(cast(SERVERPROPERTY('productversion') as varchar),1,charindex('.',cast(SERVERPROPERTY('productversion') as varchar))-1) as int))
end
go 
--select dbo.Func_GetSQLServerMainVersion()
 
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_GetSQLServerDetailVersion]') and xtype in (N'FN', N'IF', N'TF'))
drop function  Func_GetSQLServerDetailVersion
go
CREATE function  Func_GetSQLServerDetailVersion()
RETURNS table
as
  return(select  SERVERPROPERTY('productversion') versionNo, SERVERPROPERTY ('productlevel') Sp, SERVERPROPERTY ('edition') Remark)

go 

--select * from dbo.Func_GetSQLServerDetailVersion ()


--显示外键约束
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_ViewForeignKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure Proc_ViewForeignKey
go
CREATE PROCEDURE Proc_ViewForeignKey(@aDatabaseName nvarchar(100),@aTableName nvarchar(100)='',@aForeignKeyName nvarchar(100)='')
as  
begin	
	exec('
  set nocount on
	declare   @tr_id   varchar(200)  
	declare   @tr_Name   varchar(200)  
	declare   @t_Name   varchar(200)  
		select  tr.id,t.name tablename,tr.name name from 
		(select ID,parent_obj,name from '+@aDatabaseName+'.dbo.sysobjects where xtype=''F'')
		tr  
		left join 
		(
		select Id,name from '+@aDatabaseName+'.dbo.sysobjects where xtype in (''U'',''V'') and  dbo.Func_InExcludeTableName(name)=0  
		)t
		on t.id=tr.parent_obj
    where ('''+@aTableName+'''='''' or '''+@aTableName+'''=t.name) and  ('''+@aForeignKeyName+'''='''' or '''+@aForeignKeyName+'''=tr.name)
		
  set nocount OFF
	')
end
go
--exec Proc_ViewForeignKey 'ABFramework'
go
--停止外键约束
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_DisableForeignKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure Proc_DisableForeignKey
go
CREATE PROCEDURE Proc_DisableForeignKey(@aDatabaseName nvarchar(100),@aTableName nvarchar(100)='',@aForeignKeyName nvarchar(100)='')
as  
begin	
	exec('
  set nocount on
	declare   @tr_id   varchar(200)  
	declare   @tr_Name   varchar(200)  
	declare   @t_Name   varchar(200)  
	declare tempCursor cursor  for 
		select  tr.id,t.name ,tr.name from 
		(select ID,parent_obj,name from '+@aDatabaseName+'.dbo.sysobjects where xtype=''F'')
		tr  
		left join 
		(
		select Id,name from '+@aDatabaseName+'.dbo.sysobjects where xtype in (''U'',''V'') and dbo.Func_InExcludeTableName(name)=0
		)t
		on t.id=tr.parent_obj
    where ('''+@aTableName+'''='''' or '''+@aTableName+'''=t.name) and  ('''+@aForeignKeyName+'''='''' or '''+@aForeignKeyName+'''=tr.name)
		
	open  tempCursor
	fetch  next  from  tempCursor  into  @tr_id,@t_Name ,@tr_Name
	while  @@fetch_status  =  0
	begin
	
		exec('' alter table '+@aDatabaseName+'.dbo.''+@t_Name +'' NOCHECK CONSTRAINT ''+@tr_Name)
	
	  fetch  next  from  tempCursor  into  @tr_id,@t_Name ,@tr_Name
	end
	close  tempCursor
	deallocate  tempCursor
  set nocount OFF
	')
end

go
--删除外键约束
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_DropForeignKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure Proc_DropForeignKey
go
CREATE PROCEDURE Proc_DropForeignKey(@aDatabaseName nvarchar(100),@aTableName nvarchar(100)='',@aForeignKeyName nvarchar(100)='')
as  
begin	
	exec('
  set nocount on
	declare   @tr_id   varchar(200)  
	declare   @tr_Name   varchar(200)  
	declare   @t_Name   varchar(200)  
	declare tempCursor cursor  for 
		select  tr.id,t.name ,tr.name from 
		(select ID,parent_obj,name from '+@aDatabaseName+'.dbo.sysobjects where xtype=''F'')
		tr  
		left join 
		(
		select Id,name from '+@aDatabaseName+'.dbo.sysobjects where xtype in (''U'',''V'') and  dbo.Func_InExcludeTableName(name)=0
		)t
		on t.id=tr.parent_obj
    where ('''+@aTableName+'''='''' or '''+@aTableName+'''=t.name) and  ('''+@aForeignKeyName+'''='''' or '''+@aForeignKeyName+'''=tr.name)
		
	open  tempCursor
	fetch  next  from  tempCursor  into  @tr_id,@t_Name ,@tr_Name
	while  @@fetch_status  =  0
	begin
	
		exec('' alter table '+@aDatabaseName+'.dbo.''+@t_Name +'' DROP CONSTRAINT ''+@tr_Name)
	
	  fetch  next  from  tempCursor  into  @tr_id,@t_Name ,@tr_Name
	end
	close  tempCursor
	deallocate  tempCursor
  set nocount OFF
	')
end

go
--启动外键约束
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_EnableForeignKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure Proc_EnableForeignKey
go
CREATE PROCEDURE Proc_EnableForeignKey(@aDatabaseName nvarchar(100),@aTableName nvarchar(100)='',@aForeignKeyName nvarchar(100)='')
as  
begin	
exec(' 
set nocount on
declare   @tr_id   varchar(200)  
declare   @tr_Name   varchar(200)  
declare   @t_Name   varchar(200)  
declare tempCursor cursor  for 
	select  tr.id,t.name ,tr.name from 
	(select ID,parent_obj,name from '+@aDatabaseName+'.dbo.sysobjects where xtype=''F'')
	tr  
	left join 
	(
	select Id,name from '+@aDatabaseName+'.dbo.sysobjects where xtype in (''U'',''V'') and  dbo.Func_InExcludeTableName(name)=0
	)t
	on t.id=tr.parent_obj
  where ('''+@aTableName+'''='''' or '''+@aTableName+'''=t.name) and  ('''+@aForeignKeyName+'''='''' or '''+@aForeignKeyName+'''=tr.name)
	
open  tempCursor
fetch  next  from  tempCursor  into  @tr_id,@t_Name ,@tr_Name
while  @@fetch_status  =  0
begin

	exec('' alter table '+@aDatabaseName+'.dbo.''+@t_Name +'' CHECK CONSTRAINT ''+@tr_Name)

  fetch  next  from  tempCursor  into  @tr_id,@t_Name ,@tr_Name
end
close  tempCursor
deallocate  tempCursor
set nocount OFF
')
end
go

/*
declare @tempDatabaseName nvarchar(100)
set @tempDatabaseName= db_name ()
exec Proc_DisableForeignKey @tempDatabaseName
exec Proc_EnableForeignKey @tempDatabaseName 

*/

--显示触发器
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_ViewTrigger]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure Proc_ViewTrigger
go
CREATE PROCEDURE Proc_ViewTrigger(@aDatabaseName nvarchar(100),@aTableName nvarchar(100)='',@aTriggerName nvarchar(100)='')
as  
begin	
	exec('
  set nocount on
	declare   @tr_id   varchar(200)  
	declare   @tr_Name   varchar(200)  
	declare   @t_Name   varchar(200)  
		select  tr.id,t.name ,tr.name from 
		(select ID,parent_obj,name from '+@aDatabaseName+'.dbo.sysobjects where xtype=''TR'')
		tr  
		left join 
		(
		select Id,name from '+@aDatabaseName+'.dbo.sysobjects where xtype in (''U'',''V'') and dbo.Func_InExcludeTableName(name)=0
		)t
		on t.id=tr.parent_obj
    where ('''+@aTableName+'''='''' or '''+@aTableName+'''=t.name) and  ('''+@aTriggerName+'''='''' or '''+@aTriggerName+'''=tr.name)
		
  set nocount OFF
	')
end
go
--exec Proc_ViewTrigger 'ABFramework'
go
--停止触发器
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_DisableTrigger]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure Proc_DisableTrigger
go
CREATE PROCEDURE Proc_DisableTrigger(@aDatabaseName nvarchar(100),@aTableName nvarchar(100)='',@aTriggerName nvarchar(100)='')
as  
begin	
	exec('
  set nocount on
	declare   @tr_id   varchar(200)  
	declare   @tr_Name   varchar(200)  
	declare   @t_Name   varchar(200)  
	declare tempCursor cursor  for 
		select  tr.id,t.name ,tr.name from 
		(select ID,parent_obj,name from '+@aDatabaseName+'.dbo.sysobjects where xtype=''TR'')
		tr  
		left join 
		(
		select Id,name from '+@aDatabaseName+'.dbo.sysobjects where xtype in (''U'',''V'') and  dbo.Func_InExcludeTableName(name)=0
		)t
		on t.id=tr.parent_obj
    where ('''+@aTableName+'''='''' or '''+@aTableName+'''=t.name) and  ('''+@aTriggerName+'''='''' or '''+@aTriggerName+'''=tr.name)
		
	open  tempCursor
	fetch  next  from  tempCursor  into  @tr_id,@t_Name ,@tr_Name
	while  @@fetch_status  =  0
	begin
	
		exec('' alter table '+@aDatabaseName+'.dbo.''+@t_Name +'' Disable trigger ''+@tr_Name)
	
	  fetch  next  from  tempCursor  into  @tr_id,@t_Name ,@tr_Name
	end
	close  tempCursor
	deallocate  tempCursor
  set nocount OFF
	')
end
go
go
--删除触发器
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_DropTrigger]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure Proc_DropTrigger
go
CREATE PROCEDURE Proc_DropTrigger(@aDatabaseName nvarchar(100),@aTableName nvarchar(100)='',@aTriggerName nvarchar(100)='')
as  
begin	
	exec('
  set nocount on
	declare   @tr_id   varchar(200)  
	declare   @tr_Name   varchar(200)  
	declare   @t_Name   varchar(200)  
	declare tempCursor cursor  for 
		select  tr.id,t.name ,tr.name from 
		(select ID,parent_obj,name from '+@aDatabaseName+'.dbo.sysobjects where xtype=''TR'')
		tr  
		left join 
		(
		select Id,name from '+@aDatabaseName+'.dbo.sysobjects where xtype in (''U'',''V'') and  dbo.Func_InExcludeTableName(name)=0
		)t
		on t.id=tr.parent_obj
    where ('''+@aTableName+'''='''' or '''+@aTableName+'''=t.name) and  ('''+@aTriggerName+'''='''' or '''+@aTriggerName+'''=tr.name)
		
	open  tempCursor
	fetch  next  from  tempCursor  into  @tr_id,@t_Name ,@tr_Name
	while  @@fetch_status  =  0
	begin
	
    exec('' drop trigger [dbo].[''+@tr_Name+'']'')
	
	  fetch  next  from  tempCursor  into  @tr_id,@t_Name ,@tr_Name
	end
	close  tempCursor
	deallocate  tempCursor
  set nocount OFF
	')
end
go
--
go
--启动触发器
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_EnableTrigger]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure Proc_EnableTrigger
go
CREATE PROCEDURE Proc_EnableTrigger(@aDatabaseName nvarchar(100),@aTableName nvarchar(100)='',@aTriggerName nvarchar(100)='')
as  
begin	
exec(' 
set nocount on
declare   @tr_id   varchar(200)  
declare   @tr_Name   varchar(200)  
declare   @t_Name   varchar(200)  
declare tempCursor cursor  for 
	select  tr.id,t.name ,tr.name from 
	(select ID,parent_obj,name from '+@aDatabaseName+'.dbo.sysobjects where xtype=''TR'')
	tr  
	left join 
	(
	select Id,name from '+@aDatabaseName+'.dbo.sysobjects where xtype in (''U'',''V'') and  dbo.Func_InExcludeTableName(name)=0
	)t
	on t.id=tr.parent_obj
  where ('''+@aTableName+'''='''' or '''+@aTableName+'''=t.name) and  ('''+@aTriggerName+'''='''' or '''+@aTriggerName+'''=tr.name)
	
open  tempCursor
fetch  next  from  tempCursor  into  @tr_id,@t_Name ,@tr_Name
while  @@fetch_status  =  0
begin

	exec('' alter table '+@aDatabaseName+'.dbo.''+@t_Name +'' Enable trigger ''+@tr_Name)

  fetch  next  from  tempCursor  into  @tr_id,@t_Name ,@tr_Name
end
close  tempCursor
deallocate  tempCursor
set nocount OFF
')
end
go

/*
declare @tempDatabaseName nvarchar(100)
set @tempDatabaseName= db_name ()
exec Proc_DisableTrigger @tempDatabaseName
exec Proc_EnableTrigger @tempDatabaseName 

*/


--创建数据表及字段的标签信息(SQL2000/2005/2008兼容)到临时表 ABSys_Temp_SysProperties
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_CreateSysPropertiesToABSys_Temp_SysProperties]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure Proc_CreateSysPropertiesToABSys_Temp_SysProperties
go
CREATE PROCEDURE Proc_CreateSysPropertiesToABSys_Temp_SysProperties
as  
begin	
	if exists (select * from  sysobjects where  name='ABSys_Temp_SysProperties' and xtype='u')
	  drop table ABSys_Temp_SysProperties

	exec('CREATE TABLE [dbo].[ABSys_Temp_SysProperties] (
		[DatabaseName] [nvarchar] (100) COLLATE Chinese_PRC_CI_AS NOT NULL ,
		[id] [nvarchar] (100) COLLATE Chinese_PRC_CI_AS NOT NULL ,
		[smallid] [nvarchar] (100) COLLATE Chinese_PRC_CI_AS NOT NULL ,
		[value] [nvarchar] (1000) NULL 
	  ) ON [PRIMARY]')
	exec('ALTER TABLE [dbo].[ABSys_Temp_SysProperties] WITH NOCHECK ADD 
		CONSTRAINT [PK_ABSys_Temp_SysProperties] PRIMARY KEY  CLUSTERED 
		(
			[DatabaseName],
			[id],
			[smallid]
		)  ON [PRIMARY] ')
	 
  declare @tempDatabaseName nvarchar(100)
  set @tempDatabaseName=db_name()

	if exists (select * from tempdb.dbo.sysobjects where id = object_id(N'tempdb.[dbo].[##tempABSys_Org_ConnList]'))
	  drop table ##tempABSys_Org_ConnList
	create table ##tempABSys_Org_ConnList(DatabaseName nvarchar(100))

	if exists(select id from sysobjects where name=N'ABSys_Org_ConnList' and xtype=N'u')  
  begin
    exec('update ABSys_Org_ConnList set CL_DatabaseName=db_name() where CL_Name=''Main'' and CL_DatabaseName<>db_name()')
    exec('insert into ##tempABSys_Org_ConnList(DatabaseName) select CL_DatabaseName from ABSys_Org_ConnList where isnull(CL_NoUse,0)=0 ')
  end
  else 
    exec('insert into ##tempABSys_Org_ConnList(DatabaseName) select '''+@tempDatabaseName+'''  ')

	declare tempCursor cursor for select DatabaseName from ##tempABSys_Org_ConnList
	open tempCursor
	fetch next from tempCursor into @tempDatabaseName
	while @@fetch_status = 0
	begin
	  if (select dbo.Func_GetSQLServerMainVersion())>8
	  begin
	    exec('
      insert into ABSys_Temp_SysProperties(DatabaseName,id,smallid,value) 
	    select '''+@tempDatabaseName+''' DatabaseName,major_id ID,Minor_ID smallid,cast(value as nvarchar(1000))
			from '+@tempDatabaseName+'.sys.extended_properties
	    where name=''MS_Description'' and isnull(value,'''')<>''''
	         ')
	  end
	  else 
	  begin
	    exec('
      insert into ABSys_Temp_SysProperties(DatabaseName,id,smallid,value) 
	    select '''+@tempDatabaseName+''' DatabaseName,ID,smallid,cast(value as nvarchar(1000)) 
			from '+@tempDatabaseName+'.dbo.sysproperties
	    where name=''MS_Description''and isnull(value,'''')<>''''
	         ')
	  end

		fetch next from tempCursor into @tempDatabaseName
  end

	close tempCursor
	deallocate tempCursor
end

go
--exec Proc_CreateSysPropertiesToABSys_Temp_SysProperties
--select * from  ABSys_Temp_SysProperties


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_CreateABSys_TempAfterEditStructure]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure Proc_CreateABSys_TempAfterEditStructure
go
CREATE PROCEDURE Proc_CreateABSys_TempAfterEditStructure
as  
begin	
  set nocount on
  exec Proc_CreateSysPropertiesToABSys_Temp_SysProperties

  --创建数据库表信息
	if exists (select * from  sysobjects where  name='ABSys_Temp_GetTableInfo' and xtype='u')
	  drop table ABSys_Temp_GetTableInfo

	exec('CREATE TABLE [dbo].[ABSys_Temp_GetTableInfo] (
		[DatabaseName] [nvarchar] (100) COLLATE Chinese_PRC_CI_AS NOT NULL ,
		[TableName] [nvarchar] (100) COLLATE Chinese_PRC_CI_AS NOT NULL ,
		[TableID] [int] NULL ,
		[xtype] [nvarchar] (100) COLLATE Chinese_PRC_CI_AS NOT NULL ,
		[caption] [nvarchar] (100) COLLATE Chinese_PRC_CI_AS NULL ,
	) ON [PRIMARY]')

	exec('ALTER TABLE [dbo].[ABSys_Temp_GetTableInfo] WITH NOCHECK ADD 
		CONSTRAINT [PK_ABSys_Temp_GetTableInfo] PRIMARY KEY  CLUSTERED 
		(
			[DatabaseName],
			[TableName]
		)  ON [PRIMARY] ')

  --创建数据库字段信息
	if exists (select * from  sysobjects where  name='ABSys_Temp_GetFieldInfo' and xtype='u')
	  drop table ABSys_Temp_GetFieldInfo

	exec('CREATE TABLE [dbo].[ABSys_Temp_GetFieldInfo] (
		[DatabaseName] [nvarchar] (100) COLLATE Chinese_PRC_CI_AS NOT NULL ,
		[TableName] [nvarchar] (100) COLLATE Chinese_PRC_CI_AS NOT NULL ,
		[TableID] [int] NULL ,
		[ID] [smallint]  NOT NULL ,
		[Name] [nvarchar] (100) COLLATE Chinese_PRC_CI_AS NOT NULL ,
		[IsAutoAdd] [varchar] (5) COLLATE Chinese_PRC_CI_AS NULL ,
		[IsKey] [varchar] (5) COLLATE Chinese_PRC_CI_AS NULL ,
		[Type] [nvarchar] (128) COLLATE Chinese_PRC_CI_AS NULL ,
		[Byte] [smallint] NULL ,
		[Length] [int] NULL ,
		[EecimalDigits] [int] NULL ,
		[CanNull] [varchar] (5) COLLATE Chinese_PRC_CI_AS NULL ,
		[DefaultValue] [nvarchar] (3000) COLLATE Chinese_PRC_CI_AS NULL ,
		[Caption] [varchar] (100) COLLATE Chinese_PRC_CI_AS NULL ,
		[iscomputed] [int] NULL 
	) ON [PRIMARY]')
	exec('ALTER TABLE [dbo].[ABSys_Temp_GetFieldInfo] WITH NOCHECK ADD 
		CONSTRAINT [PK_ABSys_Temp_GetFieldInfo] PRIMARY KEY  CLUSTERED 
		(
			[DatabaseName],
			[TableName],
			[ID]
		)  ON [PRIMARY] ')

  --创建数据库表索引信息
	if exists (select * from  sysobjects where  name='ABSys_Temp_GetTableIndexInfo' and xtype='u')
	  drop table ABSys_Temp_GetTableIndexInfo

	exec('CREATE TABLE [dbo].[ABSys_Temp_GetTableIndexInfo] (
		[DatabaseName] [nvarchar] (100) COLLATE Chinese_PRC_CI_AS NOT NULL ,
		[TableName] [nvarchar] (100) COLLATE Chinese_PRC_CI_AS NOT NULL ,
		[TableID] [int] NULL ,
		[INDEX_NAME] [nvarchar] (100) COLLATE Chinese_PRC_CI_AS NOT NULL ,
		[COLUMN_NAME] [nvarchar] (100) COLLATE Chinese_PRC_CI_AS NOT NULL ,
		[PRIMARY_KEY] [bit] NULL ,
		[UNIQUE] [bit] NULL ,
		[COLLATION] [smallint] NULL ,
		[CLUSTERED] [bit] NULL ,
	  [ORDINAL_POSITION] [int] NULL 
	) ON [PRIMARY]')

	exec('ALTER TABLE [dbo].[ABSys_Temp_GetTableIndexInfo] WITH NOCHECK ADD 
		CONSTRAINT [PK_ABSys_Temp_GetTableIndexInfo] PRIMARY KEY  CLUSTERED 
		(
			[DatabaseName],
			[TableName],
			[INDEX_NAME],
      [COLUMN_NAME]
		)  ON [PRIMARY] ')

  declare @tempDatabaseName nvarchar(100)
  set @tempDatabaseName=db_name()

	if exists (select * from tempdb.dbo.sysobjects where id = object_id(N'tempdb.[dbo].[##tempABSys_Org_ConnList]'))
	  drop table ##tempABSys_Org_ConnList
	create table ##tempABSys_Org_ConnList(DatabaseName nvarchar(100))

	if exists(select id from sysobjects where name=N'ABSys_Org_ConnList' and xtype=N'u')  
  begin
    exec('update ABSys_Org_ConnList set CL_DatabaseName=db_name() where CL_Name=''Main'' and CL_DatabaseName<>db_name()')
    exec('insert into ##tempABSys_Org_ConnList(DatabaseName) select CL_DatabaseName from ABSys_Org_ConnList where isnull(CL_NoUse,0)=0')
  end
  else 
    exec('insert into ##tempABSys_Org_ConnList(DatabaseName) select '''+@tempDatabaseName+'''  ')


	declare tempCursor cursor for select DatabaseName from ##tempABSys_Org_ConnList
	open tempCursor
	fetch next from tempCursor into @tempDatabaseName
	while @@fetch_status = 0
	begin
    exec('
    insert into ABSys_Temp_GetTableInfo(DatabaseName,TableName,TableID,[xtype],[caption]) 
    select '''+@tempDatabaseName+''' DatabaseName,
	          d.name TableName,
            d.ID TableID,
						d.xtype , cast(f.value as varchar(100)) caption
		FROM   '+@tempDatabaseName+'.dbo.sysobjects d 
					 left   join   ABSys_Temp_SysProperties   f   on '''+@tempDatabaseName+'''=f.DatabaseName and  d.id=f.id   and   f.smallid=0   
		where d.xtype in (''U'',''V'') and  dbo.Func_InExcludeTableName(d.name)=0
    ')  
	
    exec('
    insert into ABSys_Temp_GetTableIndexInfo(DatabaseName,TableName,TableID,[INDEX_NAME],[COLUMN_NAME],[PRIMARY_KEY],[UNIQUE],[COLLATION],[CLUSTERED],[ORDINAL_POSITION]) 
    select '''+@tempDatabaseName+''' DatabaseName,
					TableName		= o.name,
          TableID=o.ID ,
					INDEX_NAME		= x.name,
					COLUMN_NAME		= c.name,
					PRIMARY_KEY		= convert(bit,(x.status & 0x800)/0x800),
					"UNIQUE"		= convert(bit,(x.status & 2)/2),
					COLLATION	= convert(smallint,
									case when indexkey_property(o.id, x.indid, xk.keyno, ''IsDescending'') =1
									then 2		/* DB_COLLATION_DESC */ 
									else 1		/* DB_COLLATION_ASC */ 
									end),
					"CLUSTERED"		= convert(bit,(x.status & 16)/16),
					ORDINAL_POSITION 	= convert(int, xk.keyno)

				from	'+@tempDatabaseName+'.dbo.sysobjects o, '+@tempDatabaseName+'.dbo.sysindexes x, '+@tempDatabaseName+'.dbo.syscolumns c, '+@tempDatabaseName+'.dbo.sysindexkeys xk
				where	o.xtype in (''U'',''V'') and  dbo.Func_InExcludeTableName(o.name)=0
				and	x.id = o.id
				and	o.id = c.id
				and	o.id = xk.id
				and	x.indid = xk.indid
				and	c.colid = xk.colid
				and	xk.keyno <= x.keycnt
				--and	permissions(o.id, c.name) <> 0
				and     (x.status&32) = 0  -- No hypothetical indexes		 
    ')


    exec('
    insert into ABSys_Temp_GetFieldInfo(DatabaseName,TableName,TableID,[ID],[Name],[IsAutoAdd],[IsKey],[Type],[Byte],[Length],[EecimalDigits],[CanNull],[DefaultValue],[Caption],[iscomputed]) 
    select '''+@tempDatabaseName+''' DatabaseName,
						 d.name TableName,
             d.ID TableID,
						 a.colorder ID,
						 a.name Name,
						 (case when (select '+@tempDatabaseName+'.dbo.Func_GetCOLUMNPROPERTY(d.name,a.name,''IsIdentity''))=1 then ''True''else ''False'' end) IsAutoAdd,
						 (case when (SELECT count(*)
						 FROM '+@tempDatabaseName+'.dbo.sysobjects
						 WHERE (name in
											 (SELECT name
											FROM '+@tempDatabaseName+'.dbo.sysindexes
											WHERE (id = a.id) AND (indid in
																(SELECT indid
															 FROM '+@tempDatabaseName+'.dbo.sysindexkeys
															 WHERE (id = a.id) AND (colid in
																				 (SELECT colid
																				FROM '+@tempDatabaseName+'.dbo.syscolumns
																				WHERE (id = a.id) AND (name = a.name))))))) AND
										(xtype = ''PK''))>0 then ''True'' else ''False'' end) IsKey,
						 b.name Type,
						 a.length Byte,
             (select '+@tempDatabaseName+'.dbo.Func_GetCOLUMNPROPERTY(d.name,a.name,''PRECISION''))  as Length,
              
						 isnull((select '+@tempDatabaseName+'.dbo.Func_GetCOLUMNPROPERTY(d.name,a.name,''Scale'')),0) as EecimalDigits,
						 (case when a.isnullable=1 then ''True''else ''False'' end) CanNull,
						 isnull(e.text,'''') DefaultValue,
				     isnull(cast(g.[value] as varchar(100)),'''') AS Caption,
		         a.iscomputed
		  FROM  '+@tempDatabaseName+'.dbo.syscolumns  a left join '+@tempDatabaseName+'.dbo.systypes b
			on  a.xtype=b.xusertype
			inner join '+@tempDatabaseName+'.dbo.sysobjects d
			on a.id=d.id  and  d.xtype in (''U'',''V'') and  dbo.Func_InExcludeTableName(d.name)=0
			left join '+@tempDatabaseName+'.dbo.syscomments e
			on a.cdefault=e.id
	    left join ABSys_Temp_SysProperties g on '''+@tempDatabaseName+'''=g.DatabaseName and a.id=g.id AND a.colid = g.smallid   
		  order by  d.name,a.colorder ')


		fetch next from tempCursor into @tempDatabaseName
  end
	close tempCursor
	deallocate tempCursor
  set nocount off
end
go
exec Proc_CreateABSys_TempAfterEditStructure
/*

select *  from ABSys_Temp_GetTableInfo
select *  from ABSys_Temp_GetTableIndexInfo
select *  from ABSys_Temp_GetFieldInfo
*/
go

 
--取表的信息过程 
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_GetTableInfo]') and OBJECTPROPERTY(id,N'IsProcedure') = 1)
drop procedure [dbo].[Proc_GetTableInfo]
go
CREATE  PROCEDURE  Proc_GetTableInfo(@TableName  varchar (1000)='')
AS
begin	
	SELECT * 
	FROM   dbo.Func_GetTableInfo(@TableName)
	order by TableName
end

go

--exec Proc_GetTableInfo

--取表的信息函数
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_GetTableInfo]') and xtype in (N'FN', N'IF', N'TF'))
drop function Func_GetTableInfo
go
create function Func_GetTableInfo (@TableName  varchar (1000))
RETURNS table
AS 
	RETURN (
		select * 
	  from ABSys_Temp_GetTableInfo
		where DatabaseName=db_name() and 
         (@TableName='' or charindex(','+TableName+',',','+@TableName+',')>0)  
	)
 
go
--select * from dbo.Func_GetTableInfo ('')

--取表的索引过程 
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_GetTableIndexInfo]') and OBJECTPROPERTY(id,N'IsProcedure') = 1)
drop procedure [dbo].[Proc_GetTableIndexInfo]
go
CREATE  PROCEDURE  Proc_GetTableIndexInfo(@TableName  varchar (1000)='')
AS
begin	
	SELECT * 
	FROM   dbo.Func_GetTableIndexInfo(@TableName)
	order by tablename,index_name,ordinal_position
end

go

--exec Proc_GetTableIndexInfo

--取表的索引函数
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_GetTableIndexInfo]') and xtype in (N'FN', N'IF', N'TF'))
drop function Func_GetTableIndexInfo
go
create function Func_GetTableIndexInfo (@TableName  varchar (1000))
RETURNS table
AS 
 
	RETURN (
		select * 
	  from ABSys_Temp_GetTableIndexInfo
		where DatabaseName=db_name() and 
         (@TableName='' or charindex(','+TableName+',',','+@TableName+',')>0)  
	)
 
go
--select * from dbo.Func_GetTableIndexInfo ('')

--取表的字段信息过程 
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_GetFieldInfo]') and OBJECTPROPERTY(id,N'IsProcedure') = 1)
drop procedure [dbo].[Proc_GetFieldInfo]
go
CREATE  PROCEDURE  Proc_GetFieldInfo(
   @TableName  varchar  (1000)='' ,
   @FieldName  varchar  (100)='', 
   @FieldType  varchar  (100)='' 
)
AS
begin	
	select * 
	from Func_GetFieldInfo(@TableName,@FieldName,@FieldType)  
	order by DatabaseName,TableName,ID
end
--exec Proc_GetFieldInfo '','','bigint'
go
--取表的字段信息函数 
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_GetFieldInfo]') and xtype in (N'FN', N'IF', N'TF'))
drop function Func_GetFieldInfo
go
create function Func_GetFieldInfo (
   @Table  varchar  (1000),
   @FieldName  varchar  (100), 
   @FieldType  varchar  (100) 
)
RETURNS table
AS
 	
	RETURN (
	select * 
  from ABSys_Temp_GetFieldInfo
	where DatabaseName=db_name() and 
        (@Table=''     or charindex(','+TableName+',',','+@Table+','    )>0) and 
				(@Table=''     or charindex(','+TableName+',',','+@Table+','    )>0) and 
				(@FieldType='' or charindex(','+Type+','     ,','+@FieldType+',')>0) and
				(@FieldName='' or charindex(','+Name+','     ,','+@FieldName+',')>0)  

	) 
 

go
 
--字符是否由数字组成
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_ISNUMERIC]') and xtype in (N'FN', N'IF', N'TF'))
drop function Func_ISNUMERIC
go
create function Func_ISNUMERIC(@aCode nvarchar(1000))
RETURNS nvarchar(1000)
AS
BEGIN 
	declare @i int
	declare @Res int

  set @i=1
  set @Res=1
  while @i<=len(@aCode) 
  begin 
    if substring(@aCode,@i,1)  NOT BETWEEN '0' AND '9'
    begin
      set @Res=0
      break;
    end
    set @i=@i+1
  end
  RETURN(@Res)
end
--select dbo.Func_ISNUMERIC('1234')

go
--取得字符中的数字
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_GetNUMERICStr]') and xtype in (N'FN', N'IF', N'TF'))
drop function Func_GetNUMERICStr
go
create function Func_GetNUMERICStr(@aCode nvarchar(100))
RETURNS nvarchar(100)
AS
BEGIN 
  declare @tempCode nvarchar(100)
  set @tempCode=''
	declare @i int
	declare @j int
  set @j=len(@aCode)
  set @i=len(@aCode) 
  while @i>0 
  begin 
    if  dbo.Func_ISNUMERIC(substring(@aCode,@j-@i+1,1))=1
    begin
      set @tempCode=@tempCode+substring(@aCode,@j-@i+1,1)
    end
    set @i=@i-1
  end

  set @tempCode=replace(@tempCode,'.','')
  set @tempCode=replace(@tempCode,'-','')
  set @tempCode=replace(@tempCode,',','')
  RETURN(@tempCode)
end
go


--select dbo.Func_GetNUMERICStr('123abcw.-,.sdwe23')
go
--取得加1的版本号
if exists(select 1 from sysobjects where id=object_id('Func_GetNewVersionNo') and objectproperty(id,'IsInlineFunction')=0)
 drop function Func_GetNewVersionNo
go
Create function Func_GetNewVersionNo(@OldVersionNo nvarchar(100))
returns nvarchar(100)
as
begin
  declare @REVERSEVersionNo nvarchar(100)  
  declare @NewVersionNo nvarchar(100)  
  if ltrim(rtrim(@OldVersionNo))=''
    set @OldVersionNo='0'

  if charindex('.',@OldVersionNo)>0
  begin
    set @REVERSEVersionNo=REVERSE(@OldVersionNo)
    set @NewVersionNo=substring(@OldVersionNo,1,len(@OldVersionNo)-charindex('.',@REVERSEVersionNo)+1)+cast(cast(REVERSE(substring(@REVERSEVersionNo,1,charindex('.',@REVERSEVersionNo)-1)) as int)+1 as nvarchar)
  end
  else
  begin
    set @NewVersionNo=cast(@OldVersionNo as int)+1
  end


  return @NewVersionNo 
end
--select dbo.Func_GetNewVersionNo('123')
--select dbo.Func_GetNewVersionNo('12.4.678.11')
go
--SQL Server中的类似于Delphi的FormatDateTime的函数
if exists(select 1 from sysobjects where id=object_id('Func_FormatDateTime') and objectproperty(id,'IsInlineFunction')=0)
 drop function Func_FormatDateTime
go
Create function Func_FormatDateTime(@Date datetime,@formatStr varchar(20))
returns nvarchar(16)
as
begin
  declare @tempstr varchar(20),@index int,@retStr varchar(20),@formatLen int,@str1 varchar(6),@str2 varchar(6),@str3 varchar(6),@j int
  declare @tempformat varchar(20)   
  select @tempformat=@formatStr,@formatStr = Upper(@formatStr),@index=-1,@retstr=''
  if @formatStr='MM/DD/YYYY' 
    set @retstr= convert(varchar(10),@date,101) 
  else if @formatstr='YYYY-MM-DD'
    set @retstr = Convert(char(10),@Date,20)
  else if @formatStr='YYYY.MM.DD'
    set @retstr= Convert(varchar(10),@Date,102)
  else if @formatStr='YYYY/MM/DD'
    set @retstr= Convert(varchar(10),@Date,111)
  else if @formatStr='DD/MM/YYYY'
    set @retstr= Convert(varchar(10),@Date,103)
  else if @formatStr='DD.MM.YYYY'
    set @retstr= Convert(varchar(10),@Date,104)
  else if @formatStr='DD-MM-YYYY'
    set @retstr= Convert(varchar(10),@Date,105)
  else if @formatStr='YYYYMMDD'
    set @retstr= Convert(varchar(10),@Date,112)
  else 
  begin
    select @tempformat=@formatStr,@formatLen = len(@formatStr)
    if @formatLen>8
    begin
      set @index=charindex('M',@tempformat)
      select @str1=right(left(@tempformat,@index-1),@index-5),@str2=right(@tempformat,@formatLen-@index-1)
      select @index=charindex('D',@str2),@str3=@str2
      set @str2=left(@str2,@index-1)       
      set @str3=right(@str3,len(@str3)-@index-1)
    end    
    select @tempstr = Convert(char(10),@Date,20),@str1=isnull(@str1,''),@str2=isnull(@str2,''),@str3=isnull(@str3,''),@j=0            
    while @index <> 0
    begin 
      set @index = charindex('-',@tempstr)
      if @j=0 
        select @retstr=left(@tempstr,@index-1)+@str1,@j=@j+1
      else set @retstr=@retstr+left(@tempstr,@index-1)+@str2    
      select @tempstr=right(@tempstr,len(@tempstr)-@index)
      set @index= charindex('-',@tempstr)      
    end
    set @retstr=@retstr+@tempstr+@str3 
  end
  return @retstr 
end

/*
用法
select dbo.Func_FormatDatetime(GetDate(),'YYYY年MM月DD日')
@formatStr格式串支持：
MM/DD/YYYY
YYYY-MM-DD
YYYY.MM.DD
YYYY/MM/DD
DD/MM/YYYY
DD.MM.YYYY
DD-MM-YYYY
YYYYMMDD或者
类似YYYY年MM月DD日
YYYY  MM之间最多支持两个汉字，MM  DD之间也最多支持两个个汉字
select dbo. FormatDatetime(GetDate(),'YYYY元年MM月份DD日') 
*/
go
--停止所有SQLSERVER的跟踪器,以防止程序被別人跟踪(效果同于手工停止SQL的事件探查器一样		)
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_CloseAllTrack]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure Proc_CloseAllTrack
go
CREATE PROCEDURE Proc_CloseAllTrack
as  
begin	
  declare @TID integer  
  declare Trac Cursor For  
  SELECT Distinct Traceid FROM  :: fn_trace_getinfo(default) 

  open Trac  
  Fetch Next From Trac into @TID  

  while @@fetch_status=0 
  begin 
    exec sp_trace_setstatus @TID,0 
    exec sp_trace_setstatus @TID,2 

    Fetch Next From Trac into @TID 
  end 

  Close Trac 
  deallocate Trac
end;    

--exec Proc_CloseAllTrack
go
--数据保存时非NULL值的唯一性检测
--方法2-借助触发器
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_SetNotNullUnique]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure Proc_SetNotNullUnique
go
CREATE PROCEDURE Proc_SetNotNullUnique(@aTableName Nvarchar(100),@aKeyFieldName Nvarchar(100),@aNotNullUniqueFieldName Nvarchar(100),
                                       @DeclareSql Nvarchar(4000),@CheckSelectSql Nvarchar(4000),@CheckSelectAddWhereSql Nvarchar(4000),@aThanType int=0) --@aThanType=0 原文比较,@aThanType=1取整比较，@aThanType=2 去除开头0比较
as  
begin	
  declare @tempSql nvarchar(4000)
  declare @aNotNullUniqueFieldNameWhere nvarchar(1000)

  if @aThanType=0
  begin
    set @aNotNullUniqueFieldNameWhere=@aTableName+'.'+@aNotNullUniqueFieldName+' = inserted.'+@aNotNullUniqueFieldName
  end
  else if @aThanType=1
  begin
    set @aNotNullUniqueFieldNameWhere=
				'(case when dbo.Func_ISNUMERIC('+@aTableName+'.'+@aNotNullUniqueFieldName+')=1 then cast(cast('+@aTableName+'.'+@aNotNullUniqueFieldName+' as bigint) as nvarchar) else '+@aTableName+'.'+@aNotNullUniqueFieldName+' end)= '+
				'(case when dbo.Func_ISNUMERIC(inserted.'+@aNotNullUniqueFieldName+')=1 then cast(cast(inserted.'+@aNotNullUniqueFieldName+' as bigint) as nvarchar) else inserted.'+@aNotNullUniqueFieldName+' end) '
  end
  else if @aThanType=2
  begin
    set @aNotNullUniqueFieldNameWhere=
				'dbo.Func_DelBegin0('+@aTableName+'.'+@aNotNullUniqueFieldName+')= '+'dbo.Func_DelBegin0(inserted.'+@aNotNullUniqueFieldName+') '
  end
 
  if @CheckSelectAddWhereSql<>'' 
    set @CheckSelectAddWhereSql=' and '+@CheckSelectAddWhereSql

  if @CheckSelectSql='' 
    set @CheckSelectSql=
		      ' SELECT  *  '+CHAR(10)+ 
		      '                      FROM '+@aTableName+' '+CHAR(10)+ 
		      '                      WHERE '+@aNotNullUniqueFieldNameWhere+' AND '+CHAR(10)+ 
		      '                            '+@aTableName+'.'+@aKeyFieldName+'<> inserted.'+@aKeyFieldName

  set @tempSql=
			' IF EXISTS (SELECT name FROM sysobjects WHERE name = '+''''+@aTableName+'_NotNullUnique_'+replace(@aNotNullUniqueFieldName,' ','') +''''+' AND type = ''TR'')'+CHAR(10)+
			' drop TRIGGER '+@aTableName+'_NotNullUnique_'+replace(@aNotNullUniqueFieldName,' ','')
  exec(@tempSql)
  set @tempSql=
			' CREATE TRIGGER '+@aTableName+'_NotNullUnique_'+replace(@aNotNullUniqueFieldName,' ','')+' ON [dbo].['+@aTableName+'] '+CHAR(10)+ 
			' FOR INSERT,update '+CHAR(10)+ 
			' AS '+CHAR(10)  +
 
			' IF UPDATE('+@aNotNullUniqueFieldName+') '+CHAR(10)+ 
			' BEGIN '+CHAR(10)+@DeclareSql+CHAR(10)+
			'   IF exists(select * from inserted where '+@aNotNullUniqueFieldName+' is not null)  '+CHAR(10)+ 
			'   BEGIN '+CHAR(10)+ 
			'     IF (SELECT count( * ) '+CHAR(10)+ 
      '        FROM inserted '+CHAR(10)+ 
			' 	     WHERE exists ( ' +@CheckSelectSql+@CheckSelectAddWhereSql+'))>0'+CHAR(10)+ 
			' 	  BEGIN '+CHAR(10)+ 
			' 	      ROLLBACK TRANSACTION '+CHAR(10)+ 
			' 	      raiserror(''ABNotNullUnique->'+@aNotNullUniqueFieldName+','+''',16,3) '+CHAR(10)+ 
			' 	  END '+CHAR(10)+ 
  		' 	END '+CHAR(10)+ 
			' END '
  exec(@tempSql)
--print @tempSql
end	

go
 
/*
方法2-借助视图
CREATE TABLE [Table1] (
	[pk] [int] IDENTITY (1, 1) NOT NULL ,
	[Col1] [int] NULL ,
	[Col2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_Table1_Col2] DEFAULT ('SomeValue')
) ON [PRIMARY]

CREATE VIEW dbo.V_Table1
WITH SCHEMABINDING 
AS
SELECT    pk, Col1, Col2
FROM         dbo.Table1
WHERE     (Col1 IS NOT NULL)

Set QUOTED_IDENTIFIER ON
Set ARITHABORT ON
CREATE UNIQUE CLUSTERED INDEX IDX1 ON V_Table1( Col1 )
方法3-借助计算列
CREATE TABLE [Table2] (
	[pk] [int] IDENTITY (1, 1) NOT NULL , 
	[Col1] [int] NULL ,
	[Col2] AS ( 	
			CASE 
				WHEN Col1 IS NULL THEN pk 
				ELSE 0 
			END
		    )
	CONSTRAINT UNQ_NULLS UNIQUE ( Col1,Col2 )
)


INSERT INTO Table1( Col1 ) VALUES( 1 )
INSERT INTO Table1( Col1 ) VALUES( 1 ) *
--這個插入陳述式失敗，並且您會收到這個錯誤訊息

下列语句则正常
INSERT INTO Table1( Col1 ) VALUES( 2 )
INSERT INTO Table1( Col1 ) VALUES( 3 )
INSERT INTO Table1( Col1 ) VALUES( NULL ) 
INSERT INTO Table1( Col1 ) VALUES( NULL ) 

*/


-- 删除字串中开始的0
if exists(select 1 from sysobjects where id=object_id('Func_DelBegin0') and objectproperty(id,'IsInlineFunction')=0)
 drop function Func_DelBegin0
go
create function Func_DelBegin0(
 @str nvarchar(4000)  
) returns nvarchar(4000)
as
begin
  declare @tempResult nvarchar(4000)
  while substring(@str,1,1)='0' 
  begin
    set @str=substring(@str,2,len(@str))
  end
  return(@str)
end

--select dbo.Func_DelBegin0('0023423423')
go


-- 获取数据库中某个对象的创建脚本
if exists(select 1 from sysobjects where id=object_id('Func_GetScript') and objectproperty(id,'IsInlineFunction')=0)
 drop function Func_GetScript
go
create function Func_GetScript(
 @servername varchar(50)     --服务器名
 ,@userid varchar(50)='sa'    --用户名,如果为nt验证方式,则为空
 ,@password varchar(50)=''    --密码
 ,@databasename varchar(50)    --数据库名称
 ,@objectname varchar(250)    --表名或其它对象名

) returns varchar(8000)
as
begin
 declare @re varchar(8000)        --返回脚本
 declare @srvid int,@dbsid int       --定义服务器、数据库集id
 declare @dbid int,@tbid int        --数据库、表id
 declare @err int,@src varchar(255), @desc varchar(255) --错误处理变量

--创建sqldmo对象
 exec @err=sp_oacreate 'sqldmo.sqlserver',@srvid output
 if @err<>0 goto lberr

--连接服务器
 if isnull(@userid,'')='' --如果是 Nt验证方式
 begin
  exec @err=sp_oasetproperty @srvid,'loginsecure',1
  if @err<>0 goto lberr

  exec @err=sp_oamethod @srvid,'connect',null,@servername
 end
 else
  exec @err=sp_oamethod @srvid,'connect',null,@servername,@userid,@password

 if @err<>0 goto lberr

--获取数据库集
 exec @err=sp_oagetproperty @srvid,'databases',@dbsid output
 if @err<>0 goto lberr

--获取要取得脚本的数据库id
 exec @err=sp_oamethod @dbsid,'item',@dbid output,@databasename
 if @err<>0 goto lberr

--获取要取得脚本的对象id
 exec @err=sp_oamethod @dbid,'getobjectbyname',@tbid output,@objectname
 if @err<>0 goto lberr

--取得脚本
 exec @err=sp_oamethod @tbid,'script',@re output
 if @err<>0 goto lberr

 --print @re
 return(@re)

lberr:
 exec sp_oageterrorinfo NULL, @src out, @desc out 
 declare @errb varbinary(4)
 set @errb=cast(@err as varbinary(4))
 exec master..xp_varbintohexstr @errb,@re out
 set @re='错误号: '+@re
   +char(13)+'错误源: '+@src
   +char(13)+'错误描述: '+@desc
 return(@re)
end
go

--select dbo.Func_getscript('grj','sa','123456','AICDatabase','Func_GetSQLServerVersion')



--因为SQL Server 没有全局变量的概念，所以暂时用一临时实体表作为存放全局变量的地方，编程时模拟全局变量的功能
--定义存放全局变量的临时表
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PublicVarTable]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[PublicVarTable]
GO
CREATE TABLE [dbo].[PublicVarTable] (
	[Name] [nvarchar] (100) COLLATE Chinese_PRC_CI_AS NOT NULL ,
	[int1] [bigint] NULL ,
	[str1] [nvarchar] (1000) COLLATE Chinese_PRC_CI_AS NULL ,
	[float1] [decimal](18, 4) NULL ,
	[datetime1] [datetime] NULL ,
	[text1] [ntext] COLLATE Chinese_PRC_CI_AS NULL ,
	[image1] [image] NULL ,
	[PubID] [nvarchar] (100) COLLATE Chinese_PRC_CI_AS NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[PublicVarTable] ADD 
	CONSTRAINT [PK_PublicVarTable] PRIMARY KEY  CLUSTERED 
	(
		[Name]
	)  ON [PRIMARY] 
GO


--设置变量值
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_SetPublicVar_int1]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure Proc_SetPublicVar_int1
go
CREATE PROCEDURE Proc_SetPublicVar_int1(@aName Nvarchar(100),@int1 bigint)
as  
begin	
	if not  exists(select * from PublicVarTable where [Name]=@aName)
	  insert into PublicVarTable([Name]) values(@aName)
  update PublicVarTable set int1=@int1 where [Name]=@aName
end

go
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_SetPublicVar_str1]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure Proc_SetPublicVar_str1
go
CREATE PROCEDURE Proc_SetPublicVar_str1(@aName Nvarchar(100),@str1 Nvarchar(1000))
as  
begin	
  if not  exists(select * from PublicVarTable where [Name]=@aName)
    insert into PublicVarTable([Name]) values(@aName)

  update PublicVarTable set str1=@str1 where [Name]=@aName
end	

go
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_SetPublicVar_float1]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure Proc_SetPublicVar_float1
go
CREATE PROCEDURE Proc_SetPublicVar_float1(@aName Nvarchar(100),@float1 decimal(18, 4))
as  
begin	
	if not  exists(select * from PublicVarTable where [Name]=@aName)
		insert into PublicVarTable([Name]) values(@aName)

	update PublicVarTable set float1=@float1 where [Name]=@aName
end
go
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_SetPublicVar_datetime1]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure Proc_SetPublicVar_datetime1
go
CREATE PROCEDURE Proc_SetPublicVar_datetime1(@aName Nvarchar(100),@datetime1 datetime)
as  
begin	
	if not  exists(select * from PublicVarTable where [Name]=@aName)
	insert into PublicVarTable([Name]) values(@aName)

	update PublicVarTable set datetime1=@datetime1 where [Name]=@aName
end
go
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_SetPublicVar_text1]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure Proc_SetPublicVar_text1
go
CREATE PROCEDURE Proc_SetPublicVar_text1(@aName Nvarchar(100),@text1 text)
as  
begin	
	if not  exists(select * from PublicVarTable where [Name]=@aName)
	insert into PublicVarTable([Name]) values(@aName)

	update PublicVarTable set text1=@text1 where [Name]=@aName
end
go
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_SetPublicVar_image1]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure Proc_SetPublicVar_image1
go
CREATE PROCEDURE Proc_SetPublicVar_image1(@aName Nvarchar(100),@image1 image)
as  
begin	
	if not  exists(select * from PublicVarTable where [Name]=@aName)
	insert into PublicVarTable([Name]) values(@aName)

	update PublicVarTable set image1=@image1 where [Name]=@aName
end
go
 
--读取变量值

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_GetPublicVar_int1]') and xtype in (N'FN', N'IF', N'TF'))
drop function Func_GetPublicVar_int1
go
create function Func_GetPublicVar_int1 (@aName Nvarchar(100))
RETURNS bigint
AS
BEGIN
  RETURN(select int1 from PublicVarTable where [Name]=@aName)
end
go
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_GetPublicVar_str1]') and xtype in (N'FN', N'IF', N'TF'))
drop function Func_GetPublicVar_str1
go
create function Func_GetPublicVar_str1 (@aName Nvarchar(100))
RETURNS nvarchar(1000)
AS
BEGIN
  RETURN(select str1 from PublicVarTable where [Name]=@aName)
end
go
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_GetPublicVar_float1]') and xtype in (N'FN', N'IF', N'TF'))
drop function Func_GetPublicVar_float1
go
create function Func_GetPublicVar_float1 (@aName Nvarchar(100))
RETURNS  decimal(18, 4)
AS
BEGIN
  RETURN(select float1 from PublicVarTable where [Name]=@aName)
end
go

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_GetPublicVar_datetime1]') and xtype in (N'FN', N'IF', N'TF'))
drop function Func_GetPublicVar_datetime1
go
create function Func_GetPublicVar_datetime1 (@aName Nvarchar(100))
RETURNS  datetime
AS
BEGIN
  RETURN(select datetime1 from PublicVarTable where [Name]=@aName)
end


go


--调用Word中 简体转繁体 
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_DCMSconvert]') and xtype in (N'FN', N'IF', N'TF'))
drop function Func_DCMSconvert
go
create function Func_DCMSconvert (@sInText Nvarchar(4000),@TransType int)
RETURNS Nvarchar(4000)
AS
BEGIN
	DECLARE @sOutText Nvarchar(4000)

	DECLARE @Document int
	EXEC sp_OACreate 'Word.Document', @Document OUT
	EXEC sp_OASetProperty @Document, 'Application.Selection.Text', @sInText
	EXEC sp_OAMethod @Document,'Application.Selection.Range.TCSCConverter',NULL,@TransType,1,1
	EXEC sp_OAGetProperty @Document, 'Application.Selection.Text', @sOutText OUT
	EXEC sp_OADestroy @Document
	EXEC sp_OAStop
	RETURN(@sOutText)
END

GO
--select dbo.Func_DCMSconvert('中华人民',0)
 

--得到日期时间的日期部分                           
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_GetDateAsVarchar]') and xtype in (N'FN', N'IF', N'TF'))
drop function  Func_GetDateAsVarchar
go
CREATE function  Func_GetDateAsVarchar( @Datetime datetime,@aResturnLength int=10) 
returns varchar(100)
as
begin	
  return(substring(convert(varchar,@Datetime,121),1,@aResturnLength))
end
go
--select dbo.Func_GetDateAsVarchar(getdate(),10)
go

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_GetDateAsDate]') and xtype in (N'FN', N'IF', N'TF'))
drop function Func_GetDateAsDate
go
CREATE FUNCTION Func_GetDateAsDate(@Datetime Datetime,@aResturnLength int=10)
RETURNS DATETIME
BEGIN
  return(Convert(DateTime, substring(convert(varchar(10),@Datetime,121),1,@aResturnLength),121))
END

--select dbo.Func_GetDateAsDate(getdate(),10)
go
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_GetDatetimeAsVarchar]') and xtype in (N'FN', N'IF', N'TF'))
drop function  Func_GetDatetimeAsVarchar
go
CREATE function  Func_GetDatetimeAsVarchar( @Datetime datetime,@aResturnLength int=19) 
returns varchar(100)
as
begin	
  return(substring(convert(varchar,@Datetime,121),1,@aResturnLength))
end
go
--select dbo.Func_GetDatetimeAsVarchar(getdate(),19)
go

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_GetDatetimeAsDate]') and xtype in (N'FN', N'IF', N'TF'))
drop function Func_GetDatetimeAsDate
go
CREATE FUNCTION Func_GetDatetimeAsDate(@Datetime Datetime,@aResturnLength int=19)
RETURNS DATETIME
BEGIN
  return(Convert(DateTime, substring(convert(varchar(19),@Datetime,121),1,@aResturnLength),120))
END

--select dbo.Func_GetDatetimeAsDate(getdate(),19)
go

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_GetBeginDatetimeAsDate]') and xtype in (N'FN', N'IF', N'TF'))
drop function Func_GetBeginDatetimeAsDate
go
CREATE FUNCTION Func_GetBeginDatetimeAsDate(@Datetime Datetime)
RETURNS DATETIME
BEGIN
  return(Convert(DateTime,substring(convert(varchar,@Datetime,121),1,10)+' 00:00:00',121))
END

--select dbo.Func_GetBeginDatetimeAsDate(getdate())
go
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_GetBeginDatetimeAsVarchar]') and xtype in (N'FN', N'IF', N'TF'))
drop function  Func_GetBeginDatetimeAsVarchar
go
CREATE function  Func_GetBeginDatetimeAsVarchar( @Datetime datetime) 
returns varchar(100)
as
begin	
  return( substring(convert(varchar,@Datetime,121),1,10)+' 00:00:00')
end
go
--select dbo.Func_GetBeginDatetimeAsVarchar(getdate())
go
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_GetEndDatetimeAsDate]') and xtype in (N'FN', N'IF', N'TF'))
drop function Func_GetEndDatetimeAsDate
go
CREATE FUNCTION Func_GetEndDatetimeAsDate(@Datetime Datetime)
RETURNS DATETIME
BEGIN
  return(Convert(DateTime,substring(convert(varchar,@Datetime,121),1,10)+' 23:59:59',121))
END

--select dbo.Func_GetEndDatetimeAsDate(getdate())
go
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_GetEndDatetimeAsVarchar]') and xtype in (N'FN', N'IF', N'TF'))
drop function  Func_GetEndDatetimeAsVarchar
go
CREATE function  Func_GetEndDatetimeAsVarchar( @Datetime datetime) 
returns varchar(100)
as
begin	
  return( substring(convert(varchar,@Datetime,121),1,10)+' 23:59:59')
end
go

--取字段的ID序号 
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_GetFieldID]') and xtype in (N'FN', N'IF', N'TF'))
drop function Func_GetFieldID
go
create function Func_GetFieldID (@aTableName varchar(100),@aFieldName varchar(100)  )
RETURNS varchar(100)
AS
BEGIN
	DECLARE @sOutText varchar(100)
	SELECT @sOutText=colorder 
	FROM  syscolumns  
	where ID=(SELECT ID FROM sysobjects where name=@aTableName) and Name=@aFieldName
	RETURN(@sOutText)
END
go


--实现 文件与数据表字段的交互  
go

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_FileToField]') and OBJECTPROPERTY(id,N'IsProcedure') = 1)
drop procedure [dbo].[Proc_FileToField]
go
CREATE  PROCEDURE  Proc_FileToField(
   @Textcopy         varchar  (1000),
   @DBName           varchar  (100),
   @Login            varchar  (100),
   @Password         varchar  (100),
   @TableName        varchar  (100),
   @ColName          varchar  (100),
   @WhereSql         varchar  (1000),
   @BackFileName     varchar  (1000) 
)
AS
begin	
	DECLARE  @exec_str  varchar  (255)
	SELECT  @exec_str  =
								 @Textcopy+'  /S  '  + 'localhost'  +
								 '  /U  '  +  @Login  +
								 '  /P  '  +  @Password  +
								 '  /D  '  +  @DBName  +
								 '  /T  '  +  @TableName  +
								 '  /C  '  +  @ColName  +
								 '  /W  "'  +  @WhereSql  +
								 '"  /F  '  +  @BackFileName  +
								 '  /'  + 'I'
	--EXEC  master..xp_cmdshell  @exec_str
  print @exec_str
end
go
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_FieldToFile]') and OBJECTPROPERTY(id,N'IsProcedure') = 1)
drop procedure [dbo].[Proc_FieldToFile]
go
CREATE  PROCEDURE  Proc_FieldToFile(
	 @Textcopy         varchar  (1000),
	 @DBName           varchar  (100),
	 @Login            varchar  (100),
	 @Password         varchar  (100),
	 @TableName        varchar  (100),
	 @ColName          varchar  (100), 
	 @WhereSql         varchar  (1000),
	 @BackFileName     varchar  (1000) 
)
AS
begin
	DECLARE  @exec_str  varchar  (255)
	SELECT  @exec_str  =
								 @Textcopy+'  /S  '  + 'localhost'  +
								 '  /U  '  +  @Login  +
								 '  /P  '  +  @Password  +
								 '  /D  '  +  @DBName  +
								 '  /T  '  +  @TableName  +
								 '  /C  '  +  @ColName  +
								 '  /W  "'  +  @WhereSql  +
								 '"  /F  '  +  @BackFileName  +
								 '  /'  + 'O'
	EXEC  master..xp_cmdshell  @exec_str
end

go
--Textcopy.exe是将文本和image数据类型从单一服务器行或列移入或移出的优秀工具
-- create  table   表名 (编号  int,image列名  image)
-- insert  表名  values(1,0x)
-- insert  表名  values(2,0x)
-- --写入
-- --exec Proc_FileToField  'c:\textcopy.exe','yberp_demo','sa','asd123', '表名','image列名','where  编号=1','c:\aa.doc'   
-- --读出
-- --exec Proc_FieldToFile 'c:\textcopy.exe','yberp_demo','sa','asd123', '表名','image列名','where  编号=1','c:\aa.doc' 
-- drop table 表名
go


--断开数据库的所有连接
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_KillConn]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Proc_KillConn]
go
create procedure Proc_KillConn    (@DBName    varchar(100))      
as      
begin      
	declare    @sql    nvarchar(500)      
	declare    @spid    int      
	set    @sql='declare    getspid    cursor    for  select    spid    from    master.dbo.sysprocesses    
                where    dbid=db_id('''+@dbname+''')'      
	exec    (@sql)     
  print  @sql
	open    getspid      
	fetch    next    from    getspid    into    @spid      
	while    @@fetch_status    <>-1      
		begin      
		exec(' kill    '+@spid)      
		fetch    next    from    getspid    into    @spid      
	end      
	close    getspid      
	deallocate    getspid      
end      
 
--用法 不要在当前库的连接中执行，如下则不能在tempsql库中执行，因为当前库的连接是KILL不掉的，要在其它库的连接中执行      
--exec   Proc_KillConn    'tempsql'
--select    count(*)    from    master.dbo.sysprocesses    where    dbid=db_id('yberp_demo')
go

--通用的新增记录的存储过程
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_PubInsert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Proc_PubInsert]
go
CREATE PROCEDURE [dbo].[Proc_PubInsert]  
  @TableName varchar(100),
  @FieldNames varchar(8000),
  @InserValues  varchar(8000),
  @WhereSql varchar(8000)='',
  @CheckSame bit=1,
  @DeleteSame bit=1
  
AS
begin	
	set nocount on
	declare @Sql varchar(8000)
	declare @tempstr1 varchar(8000)

	declare @tempOk int

	set @tempOk=1
	if (@WhereSql<>'' and @WhereSql is not null)  
	begin
		if @CheckSame=1  
		begin
			if @DeleteSame=1  
			begin
				set @Sql=' delete '+@TableName+' where '+@WhereSql
				exec (@Sql)
			end
			else
			begin
				set @tempstr1='select @tempvalue1=count(*) from '+@TableName+' where '+@WhereSql
				if @tempstr1<>''
				begin
					exec  sp_executesql @tempstr1 ,N' @tempvalue1 int  OUTPUT ',@tempOk  OUTPUT 
					if isnull(@tempOk,0)>0
						set @tempOk=0
					else
						set @tempOk=1
				end 
			end
		end
	end

	if @tempOk>0 
	begin
		set @Sql=' Insert Into '+@TableName+'('+@FieldNames+') values '+'('+@InserValues+')' 
		exec (@Sql)
	end
	set nocount off
end

GO

--通用的修改记录的存储过程
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_PubUpdate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Proc_PubUpdate]
go
CREATE PROCEDURE [dbo].[Proc_PubUpdate]  
  @TableName varchar(100),
  @FieldAssign varchar(8000),
  @WhereSql  varchar(8000),
  @From varchar(8000)=''
AS
begin	
	set nocount on
	declare @Sql varchar(8000)
	set @Sql=' Update '+@TableName+' set '+@FieldAssign
	if (@From<>'' and @From is not null)  
		set @Sql=@Sql+' from '+@From
	if (@WhereSql<>'' and @WhereSql is not null)  
		set @Sql=@Sql+' where '+@WhereSql
	 
	exec (@Sql)
	set nocount off
end

GO


--通用的删除记录的存储过程
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_PubDelete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Proc_PubDelete]
go
CREATE PROCEDURE [dbo].[Proc_PubDelete]  
  @TableName varchar(100),
  @WhereSql  varchar(8000)
AS
begin	
	set nocount on
	declare @Sql varchar(8000)
	set @Sql=' Delete '+@TableName 
	if (@WhereSql<>'' and @WhereSql is not null)  
		set @Sql=@Sql+' where '+@WhereSql

	exec (@Sql)
	set nocount off
end

GO

  


/*交叉表使用

1	张三	语文	70
2	张三	英语	71
3	张三	数学	72
4	李四	语文	80
5	李四	英语	81
6	李四	数学	82
7	王五	语文	90
8	王五	英语	91
9	王五	数学	92

要求输出
        数学    英语    语文        
李四	82	81	80
王五	92	91	90
张三	72	71	70
*/

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_DecussateTable]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Proc_DecussateTable]
go
CREATE PROCEDURE [dbo].[Proc_DecussateTable]
(
  @TableName varchar(100),
  @RowFieldName varchar(100),
  @ColFieldName  varchar(100),
  @ValueFieldName varchar(100)
)
AS
begin	
	set nocount on
	declare @sql Nvarchar(4000)
	declare @Returnsql Nvarchar(4000)
	set @sql = ' set @Returnsql ='' select '+@RowFieldName+','' '+
						 ' select @Returnsql = @Returnsql+'' sum(case '+@ColFieldName+' when'''''' +'+@ColFieldName+'+ '''''' then '+@ValueFieldName+' else 0  end) as ''''''+ '+@ColFieldName+'+'''''','''+
						 '        from (select distinct '+@ColFieldName+' from '+@TableName+') as a  '
	  
	--得到结果的表语句(核心部分)
	exec sp_executesql @Sql,N'@Returnsql  Nvarchar(4000) output',@Returnsql output  
	set @sql = left(@Returnsql,len(@Returnsql)-1) + ' from '+@TableName+' group by '+@RowFieldName+''

	--执行语句
	exec sp_executesql @Sql 

	 
	-- CREATE TABLE [Test] (
	--        [id] [int] IDENTITY (1, 1) NOT NULL ,
	--        [name] [nvarchar] (50) COLLATE Chinese_PRC_CI_AS NULL ,
	--        [subject] [nvarchar] (50) COLLATE Chinese_PRC_CI_AS NULL ,
	--        [Source] [numeric](18, 0) NULL 
	-- ) ON [PRIMARY]
	-- GO
	-- INSERT INTO [test] ([name],[subject],[Source]) values (N'张三',N'语文',70)
	-- INSERT INTO [test] ([name],[subject],[Source]) values (N'张三',N'英语',71)
	-- INSERT INTO [test] ([name],[subject],[Source]) values (N'张三',N'数学',72)
	-- INSERT INTO [test] ([name],[subject],[Source]) values (N'李四',N'语文',80)
	-- INSERT INTO [test] ([name],[subject],[Source]) values (N'李四',N'英语',81)
	-- INSERT INTO [test] ([name],[subject],[Source]) values (N'李四',N'数学',82)
	-- INSERT INTO [test] ([name],[subject],[Source]) values (N'王五',N'语文',90)
	-- INSERT INTO [test] ([name],[subject],[Source]) values (N'王五',N'英语',91)
	-- INSERT INTO [test] ([name],[subject],[Source]) values (N'王五',N'数学',92)
	-- go
	-- exec Proc_DecussateTable 'test','name','subject','Source'
	-- drop table test
end
 

go


--根据表中数据生成insert语句的存储过程
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_GetInsertSQL]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Proc_GetInsertSQL]
go
CREATE   PROCEDURE Proc_GetInsertSQL(@Tablename varchar(100),@Where varchar(8000),@KeyFieldNames  varchar(8000))
as
begin
  declare @sql varchar(8000)
  declare @KeyWheres varchar(8000)
  declare @UpdateStr varchar(8000)
  declare @FieldNames varchar(8000)
  declare @FieldValues varchar(8000)

  if substring(@Where,1,5)<>'where'
    set @Where=' where '+@Where

  set @sql =''
  set @KeyWheres = ''
  set @UpdateStr = ''
  set @FieldNames = ''
  set @FieldValues = '' 
 
 
   select @KeyWheres =@KeyWheres+
                      Case when charindex(','+name+',',','+@KeyFieldNames+',')>0 then 
                           case when @KeyWheres='' then  ''''+name+'=''' + ' + '+cols else  '+'' and ''+'+ ''''+name+'=''' + ' + '+cols end 
                      else '' 
                      end ,         
          @UpdateStr =@UpdateStr+
                      Case when charindex(','+name+',',','+@KeyFieldNames+',')<=0 then 
                           case when @UpdateStr='' then  ''''+name+'=''' + ' + '+cols else  '+'' , ''+'+ ''''+name+'=''' + ' + '+cols end 
                      else '' 
                      end ,         
         @FieldNames = @FieldNames + 
                       Case when @FieldNames='' then  '['+name + ']' else  ',[' + name + ']' end ,
         @FieldValues =@FieldValues + 
                       Case when @FieldValues='' then  cols else  ','','',' + cols end          
    from
        (select case
                  when xtype in (48,52,56,59,60,62,104,106,108,122,127)
                       then 'case when '+ name +' is null then ''NULL'' else ' + 'cast('+ name + ' as varchar)'+' end'
                  when xtype in (58,61)
                       then 'case when '+ name +' is null then ''NULL'' else '+''''''''' + ' + 'cast('+ name +' as varchar)'+ '+'''''''''+' end'
                  when xtype in (167)
                       then 'case when '+ name +' is null then ''NULL'' else '+''''''''' + ' + 'replace('+ name+','''''''','''''''''''')' + '+'''''''''+' end'
                  when xtype in (231)
                       then 'case when '+ name +' is null then ''NULL'' else '+'''N'''''' + ' + 'replace('+ name+','''''''','''''''''''')' + '+'''''''''+' end'
                  when xtype in (175)
                       then 'case when '+ name +' is null then ''NULL'' else '+''''''''' + ' + 'cast(replace('+ name+','''''''','''''''''''') as Char(' + cast(length as varchar)  + '))+'''''''''+' end'
                  when xtype in (239)
                       then 'case when '+ name +' is null then ''NULL'' else '+'''N'''''' + ' + 'cast(replace('+ name+','''''''','''''''''''') as Char(' + cast(length as varchar)  + '))+'''''''''+' end'
                else '''NULL'''
                end as Cols,
           name
           from syscolumns
          where id = object_id(@Tablename)
        ) T
--  print @FieldNames
--  print @FieldValues
--  print @KeyWheres
--  print @UpdateStr
  if @KeyWheres<>''
  begin
	  set @sql =' select ''if not exists (select * from '+@Tablename+' where '+''','+ @KeyWheres+','+''''+')'+''','+''''+
	            ' INSERT INTO ['+ @Tablename + ']' +'('+@FieldNames+') ' +
	            ' values ('','+@FieldValues + ','')'''+

              ','+''''+
              ' update '+@Tablename+
              ' set '+
              ''','+
              @UpdateStr+
              '+'' where '+''','+ @KeyWheres+
              ' from '+@Tablename+' '+@Where
              
  end
  else 
  begin
	  set @sql =' select '''+
	            ' INSERT INTO ['+ @Tablename + ']' +'('+@FieldNames+') ' +
	            ' values ('','+@FieldValues + ','')'''+'  from '+@Tablename+' '+@Where
  end
  print @sql
  exec (@sql)
end
go 
--exec Proc_GetInsertSQL 'ABSys_Org_LoadSQL', 'where charindex(''_hide'',Ls_SQL)>0',''
--exec Proc_GetInsertSQL 'ABSys_Org_LoadSQL', 'where charindex(''_hide'',Ls_SQL)>0','LS_FormName,LS_Name'


--抓取到建表语句procedure
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_GetCreateTableSql]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Proc_GetCreateTableSql]
go
create  procedure  Proc_GetCreateTableSql
@v_tableName  varchar(256)
as
begin	
	declare  @i_objectId  int,  --  對象id
												 @i_indId  smallint,  --  索引id
												 @v_pkInfo  varchar(100),  --  主鍵信息
												 @v_clusteredInfo  varchar(20),  --  clustered信息
												 @v_pkCol  varchar(100),  --  主鍵字段
												 @v_key  varchar(100),
												 @i_i  smallint
	set  @i_objectId  =  object_id(@v_tableName)
	if  @i_objectId  is  null  --  判斷對象是否存在
	begin
						 print  'The  object  not  exists'
						 return
	end
	if  OBJECTPROPERTY(@i_objectId,'IsTable')  <>  1  --  判斷對象是否是table
	begin
						 print  'The  object  is  not  table'
						 return
	end
	set  nocount  on
	create  table  #temp1
	(
						 i_id  int  identity,
						 v_desc  varchar(200)
	)

	insert  into  #temp1(v_desc)
	values('create  table  '+@v_tableName+'(')  --

	insert  into  #temp1(v_desc)  --  將表的字段信息存入臨時表
	select  a.name+space(4)+b.name+
						 case  when  b.xtype  in  (167,175)  then  '('+cast(a.length  as  varchar)+')'
												 when  b.xtype  in  (231,239)  then  '('+cast(a.length/2  as  varchar)+')'
												 when  b.xtype  in  (106,108)  then  '('+cast(a.xprec  as  varchar)+','+cast(a.xscale  as varchar)+')'
												 else  ''  end+space(4)+
						 case  when  (a.colstat  &  1  =  1)  then  'identity('+cast(ident_seed(@v_tableName)  as varchar)+','  +
																		 cast(ident_incr(@v_tableName)  as  varchar)+')'  else  ''  end  +space(4)+
						 case  a.isnullable  when  0  then  'not  null'  else  'null'  end+'  |'
	from  syscolumns  a,systypes  b
	where  a.id  =  @i_objectId  and  a.xtype  =  b.xusertype
	order  by  a.colid

	if  exists(select  1  from  sysobjects  where  parent_obj  =  @i_objectId  and  xtype  =  'PK')  --  如果存在主鍵
	begin
						 select  @v_pkInfo  =  b.name,@i_indId  =  indid,  --  得到主鍵名,id及是否clustered信息
												 @v_clusteredInfo  =  (case  when  (a.status  &  16)=16  then  'clustered'  else'nonclustered'  end  )
						 from  sysindexes  a,sysobjects  b
						 where  a.id  =  b.parent_obj  and  a.name  =  b.name  and  b.xtype  =  'PK'  and  b.parent_obj  =@i_objectId

						 select  @v_pkCol  =  index_col(@v_tableName,  @i_indId,  1),  @i_i  =  2  --  得到主鍵的第1個字段名
						 select  @v_key  =  index_col(@v_tableName,  @i_indId,  @i_i)  --  得到主鍵的第2個字段名
						 while  (@v_key  is  not  null)
						 begin
												 select  @v_pkCol  =  @v_pkCol  +  ','  +  @v_key,  @i_i  =  @i_i  +  1
												 select  @v_key  =  index_col(@v_tableName,  @i_indId,  @i_i)  --  得到主鍵的第@i_i個字段名
						 end  --  組合成主鍵信息
						 set  @v_pkInfo  =  'constraint  '+@v_pkInfo+'  primary  key  '+@v_clusteredInfo+'('+@v_pkCol+')'
						 insert  into  #temp1(v_desc)  values(@v_pkInfo)  --  將主鍵信息插入臨時表
	end
	else
	begin
						 select  @i_i  =  count(1)  from  #temp1
						 --  如果沒有主鍵,那麼將最後一筆紀錄的'  |'去掉
						 update  #temp1  set  v_desc  =  replace(v_desc,'  |','')  where  i_id  =  @i_i
	end
	insert  into  #temp1(v_desc)  values(')')  --
	update  #temp1  set  v_desc  =  replace(v_desc,'  |',',')
	select  v_desc  from  #temp1  order  by  i_id
	drop  table  #temp1
end
go
--exec Proc_GetCreateTableSql sysusers
go
--解密加密的存储过程，视图，触发器
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_DecryptObject]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Proc_DecryptObject]
go
CREATE PROCEDURE Proc_DecryptObject(@objectName varchar(100))
AS
begin
	set nocount on
	--CSDN：j9988 copyright:2004.04.15 
	--V3.1 
	--破解字节不受限制，适用于SQLSERVER2000存储过程，函数，视图，触发器
	--修正上一版视图触发器不能正确解密错误
	--发现有错，请E_MAIL：CSDNj9988@tom.com
	begin tran
	declare @objectname1 varchar(100),@orgvarbin varbinary(8000)
	declare @sql1 nvarchar(4000),@sql2 varchar(8000),@sql3 nvarchar(4000),@sql4 nvarchar(4000)
	DECLARE @OrigSpText1 nvarchar(4000), @OrigSpText2 nvarchar(4000) , @OrigSpText3 nvarchar(4000), @resultsp nvarchar(4000)
	declare @i int,@status int,@type varchar(10),@parentid int
	declare @colid int,@n int,@q int,@j int,@k int,@encrypted int,@number int
	select @type=xtype,@parentid=parent_obj from sysobjects where id=object_id(@ObjectName)
	create table #temp(number int,colid int,ctext varbinary(8000),encrypted int,status int)
	insert #temp SELECT number,colid,ctext,encrypted,status FROM syscomments WHERE id = object_id(@objectName)
	select @number=max(number) from #temp
	set @k=0

	while @k<=@number 
	begin
		if exists(select 1 from syscomments where id=object_id(@objectname) and number=@k)
		begin
			if @type='P'
	  		set @sql1=(case 
									 when @number>1 then 'ALTER PROCEDURE '+ @objectName +';'+rtrim(@k)+' WITH ENCRYPTION AS '
									 else 'ALTER PROCEDURE '+ @objectName+' WITH ENCRYPTION AS '
									 end)
			
			if @type='TR'
			begin
				declare @parent_obj varchar(255),@tr_parent_xtype varchar(10)
				select @parent_obj=parent_obj from sysobjects where id=object_id(@objectName)
				select @tr_parent_xtype=xtype from sysobjects where id=@parent_obj
				if @tr_parent_xtype='V'
				begin
	  			set @sql1='ALTER TRIGGER '+@objectname+' ON '+OBJECT_NAME(@parentid)+' WITH ENCRYPTION INSTERD OF INSERT AS PRINT 1 '
				end
				else
				begin
	  			set @sql1='ALTER TRIGGER '+@objectname+' ON '+OBJECT_NAME(@parentid)+' WITH ENCRYPTION FOR INSERT AS PRINT 1 '
	  		end
			end

			if @type='FN' or @type='TF' or @type='IF'
			set @sql1=(
								case @type 
								when 'TF' then 
										'ALTER FUNCTION '+ @objectName+'(@a char(1)) returns @b table(a varchar(10)) with encryption as begin insert @b select @a return end '
								when 'FN' then
										'ALTER FUNCTION '+ @objectName+'(@a char(1)) returns char(1) with encryption as begin return @a end'
								when 'IF' then
										 'ALTER FUNCTION '+ @objectName+'(@a char(1)) returns table with encryption as return select @a as a'
								end)
			
			if @type='V'
				set @sql1='ALTER VIEW '+@objectname+' WITH ENCRYPTION AS SELECT 1 as f'
			
			set @q=len(@sql1)
			set @sql1=@sql1+REPLICATE('-',4000-@q)
			select @sql2=REPLICATE('-',8000)
			set @sql3='exec(@sql1'
			select @colid=max(colid) from #temp where number=@k 
			set @n=1
			while @n<=CEILING(1.0*(@colid-1)/2) and len(@sQL3)<=3996
			begin 
				set @sql3=@sql3+'+@'
				set @n=@n+1
			end
			set @sql3=@sql3+')'
			exec sp_executesql @sql3,N'@Sql1 nvarchar(4000),@ varchar(8000)',@sql1=@sql1,@=@sql2
		end
		set @k=@k+1
	end

	set @k=0
	while @k<=@number 
	begin
		if exists(select 1 from syscomments where id=object_id(@objectname) and number=@k)
		begin
			select @colid=max(colid) from #temp where number=@k 
			set @n=1

			while @n<=@colid
			begin
				select @OrigSpText1=ctext,@encrypted=encrypted,@status=status FROM #temp WHERE colid=@n and number=@k

				SET @OrigSpText3=(SELECT ctext FROM syscomments WHERE id=object_id(@objectName) and colid=@n and number=@k)
				if @n=1
				begin
					if @type='P'
					SET @OrigSpText2=(
														case when @number>1 then 'CREATE PROCEDURE '+ @objectName +';'+rtrim(@k)+' WITH ENCRYPTION AS '
														else 'CREATE PROCEDURE '+ @objectName +' WITH ENCRYPTION AS '
														end
														)


					if @type='FN' or @type='TF' or @type='IF'
					SET @OrigSpText2=(
														case @type when 'TF' then 
														'CREATE FUNCTION '+ @objectName+'(@a char(1)) returns @b table(a varchar(10)) with encryption as begin insert @b select @a return end '
														when 'FN' then
														'CREATE FUNCTION '+ @objectName+'(@a char(1)) returns char(1) with encryption as begin return @a end'
														when 'IF' then
														'CREATE FUNCTION '+ @objectName+'(@a char(1)) returns table with encryption as return select @a as a'
														end
														)

					if @type='TR' 
					begin
						if @tr_parent_xtype='V'
						begin
  						set @OrigSpText2='CREATE TRIGGER '+@objectname+' ON '+OBJECT_NAME(@parentid)+' WITH ENCRYPTION INSTEAD OF INSERT AS PRINT 1 '
						end
						else
						begin
							set @OrigSpText2='CREATE TRIGGER '+@objectname+' ON '+OBJECT_NAME(@parentid)+' WITH ENCRYPTION FOR INSERT AS PRINT 1 '
						end
					end

					if @type='V'
					set @OrigSpText2='CREATE VIEW '+@objectname+' WITH ENCRYPTION AS SELECT 1 as f'

					set @q=4000-len(@OrigSpText2)
					set @OrigSpText2=@OrigSpText2+REPLICATE('-',@q)
				end
				else
				begin
					SET @OrigSpText2=REPLICATE('-', 4000)
				end
				SET @i=1

				SET @resultsp = replicate(N'A', (datalength(@OrigSpText1) / 2))

				WHILE @i<=datalength(@OrigSpText1)/2
				BEGIN
					SET @resultsp = stuff(@resultsp, @i, 1, NCHAR(UNICODE(substring(@OrigSpText1, @i, 1)) ^
													(UNICODE(substring(@OrigSpText2, @i, 1)) ^UNICODE(substring(@OrigSpText3, @i, 1)))))
					SET @i=@i+1
				END
				set @orgvarbin=cast(@OrigSpText1 as varbinary(8000))
				set @resultsp=( case 
												when @encrypted=1 then @resultsp 
												else convert(nvarchar(4000),case when @status&2=2 then uncompress(@orgvarbin) else @orgvarbin end)
												end
												)
				print @resultsp
				set @n=@n+1
			end
		end
		set @k=@k+1
	end

	drop table #temp
	rollback tran
end

go
--exec Proc_DecryptObject '存储过程明字'
 
/*批量加密SQLSERVER  2000  的存储过程*/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_EncryptedProc]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Proc_EncryptedProc]
go
CREATE PROCEDURE Proc_EncryptedProc
as
begin	
	DECLARE  @sp_name  nvarchar(400)
	DECLARE  @sp_content  nvarchar(4000)
	DECLARE  @asbegin  int
	declare  @now  datetime
	select  @now  =  getdate()
	DECLARE  sp_cursor  CURSOR  FOR

	SELECT  object_name(id)
	FROM  sysobjects
	WHERE  xtype  =  'P'
	AND  type  =  'P'
	AND  crdate  <  @now
	AND  OBJECTPROPERTY(id,  'IsMSShipped')=0

	OPEN  sp_cursor
	FETCH  NEXT  FROM  sp_cursor
	INTO  @sp_name

	WHILE  @@FETCH_STATUS  =  0
	BEGIN
		SELECT  @sp_content  =  text  FROM  syscomments  WHERE  id  =  OBJECT_ID(@sp_name)
		SELECT  @asbegin  =  charINDEX  (  'AS ',  @sp_content)
		
		SELECT  @sp_content  =  SUBSTRING(@sp_content,  1,  @asbegin  -  1)
		+  '  WITH  ENCRYPTION  AS '
		+  SUBSTRING  (@sp_content,  @asbegin+3,  LEN(@sp_content))
		SELECT  @sp_name  =  'DROP  PROCEDURE  ['  +  @sp_name  +  ']'
		EXEC  sp_executesql  @sp_name
		EXEC  sp_executesql  @sp_content
		FETCH  NEXT  FROM  sp_cursor INTO  @sp_name
	END

	CLOSE  sp_cursor
	DEALLOCATE  sp_cursor 
end
go
--exec Proc_EncryptedProc



go 
 
--典型的数据累加问题。
-- CREATE TABLE [Test] (
--        [id] [nvarchar] (50) COLLATE Chinese_PRC_CI_AS NULL ,
--        [Parent] [nvarchar] (50) COLLATE Chinese_PRC_CI_AS NULL 
-- ) ON [PRIMARY]
-- GO
-- INSERT INTO [test] ([ID],[Parent]) values (N'01',N'0')
-- INSERT INTO [test] ([ID],[Parent]) values (N'02',N'0')
-- INSERT INTO [test] ([ID],[Parent]) values (N'03',N'0')
-- INSERT INTO [test] ([ID],[Parent]) values (N'04',N'03')
-- INSERT INTO [test] ([ID],[Parent]) values (N'05',N'03')
-- INSERT INTO [test] ([ID],[Parent]) values (N'06',N'02')
-- go
-- select * from test
-- exec Proc_DeleteTreeNode 'test','id','Parent','03'
-- select * from test
-- drop table test
  


 
-- 如何用SQL语句实现这样的统计 
-- 已知表:
-- Name            Qty
-- A                100
-- B                200
-- C                300
-- B                400
-- A                500
-- C                600
-- A                700
-- 
-- 按Name逐行从前向后累加,想得到:
-- Name              Qty                          TTL
--    A                100                        100
--    A                500                        600
--    A                700                        1300    
--    B                200                        200
--    B                400                        600
--    C                300                        300
--    C                600                        900  
--  
-- 典型的数据累加问题。  
 


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_DataAdd]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Proc_DataAdd]
go
CREATE PROCEDURE Proc_DataAdd
(
 @TableName varchar(100),
 @AddFieldName varchar(100),
 @GroupFieldName varchar(100) ,
 @NewCreatFieldName varchar(100)
)
as
begin	
	declare  @SQLText   Nvarchar(4000)
	set @SQLText=' select '+@GroupFieldName+','+@AddFieldName+','+'IDENTITY (int)  AS  Id into  ##Temp  from  '+@TableName 
	exec sp_executesql @SQLText 
	set @SQLText=' select '+@GroupFieldName+','+@AddFieldName+','+
							 '(select  sum('+@AddFieldName+')  from  ##Temp  where '+ 
								@GroupFieldName+'=a.'+@GroupFieldName+'  and  Id<=a.Id)  as  '+@NewCreatFieldName+' from  ##Temp  a order by '+@GroupFieldName
	exec sp_executesql @SQLText 
	drop table ##Temp  
end
go
 
-- CREATE TABLE [Test] (
--        [Name] [nvarchar] (50) COLLATE Chinese_PRC_CI_AS NULL ,
--        [Qty] int 
-- ) ON [PRIMARY]
-- GO
-- INSERT INTO [test] ([Name],[Qty]) values ('A',100)
-- INSERT INTO [test] ([Name],[Qty]) values ('B',200)
-- INSERT INTO [test] ([Name],[Qty]) values ('C',300)
-- INSERT INTO [test] ([Name],[Qty]) values ('B',400)
-- INSERT INTO [test] ([Name],[Qty]) values ('A',500)
-- INSERT INTO [test] ([Name],[Qty]) values ('C',600)
-- INSERT INTO [test] ([Name],[Qty]) values ('A',700)
-- exec Proc_DataAdd 'test','qty','name','tts'
-- drop table  test 
 
 
go


go 
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_Lockinfo]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) 
drop procedure [dbo].[Proc_Lockinfo] 
GO 
/*--处理死锁 
exec Proc_Lockinfo

 查看当前进程,或死锁进程,并能自动杀掉死进程 
 因为是针对死的,所以如果有死锁进程,只能查看死锁进程 
 当然,你可以通过参数控制,不管有没有死锁,都只查看死锁进程 
--*/ 
create proc Proc_Lockinfo 
@kill_lock_spid bit=0,  --是否杀掉死锁的进程,1 杀掉, 0 仅显示 
@show_spid_if_nolock bit=1 --如果没有死锁的进程,是否显示正常进程信息,1 显示,0 不显示 
as 
declare @count int,@s nvarchar(1000),@i int 
select id=identity(int,1,1),标志, 
 进程ID=spid,线程ID=kpid,块进程ID=blocked,数据库ID=dbid, 
 数据库名=db_name(dbid),用户ID=uid,用户名=loginame,累计CPU时间=CPU, 
 登陆时间=login_time,
 '等待时间(秒)'=waittime/1000,
 上次执行时间=last_batch,

 打开事务数=open_tran, 进程状态=status, 
 工作站名=hostname,应用程序名=program_name,工作站进程ID=hostprocess, 
 域名=nt_domain,网卡地址=net_address 
into #t from( 
 select 标志='死锁的进程', 
  spid,kpid,a.blocked,dbid,uid,loginame,CPU,login_time,waittime,last_batch,open_tran, 
  status,hostname,program_name,hostprocess,nt_domain,net_address, 
  s1=a.spid,s2=0
 from master..sysprocesses a join ( 
  select blocked from master..sysprocesses group by blocked 
  )b on a.spid=b.blocked where a.blocked=0 
 union all 
 select '|_牺牲品_>', 
  spid,kpid,blocked,dbid,uid,loginame,CPU,login_time,waittime,last_batch,open_tran, 
  status,hostname,program_name,hostprocess,nt_domain,net_address, 
  s1=blocked,s2=1 
 from master..sysprocesses a where blocked<>0 
)a 
order by s1,s2 

select @count=@@rowcount,@i=1 
if @count=0 and @show_spid_if_nolock=1 
begin 
 insert #t 
 select 标志='正常的进程', 
  spid,kpid,blocked,dbid,db_name(dbid),uid,loginame,CPU,login_time,waittime,last_batch, 
  open_tran,status,hostname,program_name,hostprocess,nt_domain,net_address 
 from master..sysprocesses 
 where db_name(dbid)=db_name()
 set @count=@@rowcount 
end 

if @count>0 
begin 
 create table #t1(id int identity(1,1),a nvarchar(30),b Int,EventInfo nvarchar(255)) 
 if @kill_lock_spid=1 
 begin 
  declare @spid varchar(10),@标志 varchar(10) 
  while @i<=@count 
  begin 
   select @spid=进程ID,@标志=标志 from #t where id=@i 
   insert #t1 exec('dbcc inputbuffer('+@spid+')') 
   if @标志='死锁的进程' exec('kill '+@spid) 
   set @i=@i+1 
  end 
 end 
 else 
  while @i<=@count 
  begin 
   select @s='dbcc inputbuffer('+cast(进程ID as varchar)+')' from #t where id=@i 
   insert #t1 exec(@s) 
   set @i=@i+1 
  end 

 select a.*,进程的SQL语句=b.EventInfo 
 from #t a join #t1 b on a.id=b.id 
 where 数据库名=db_name()
end 
go  

 
--能不能用sql查到当前有哪些表被哪些用户锁住了?
--用sp_who可以看到用户的状态,还有一个syslocks  表sp_lock可以看锁的情况syslocks是6.X的系统表syslockinfo  7.0  和2000的系统表。
--这是我写的一个存储过程，查询存储过程所在的数据库的对象的锁住情况，适用于ms  sql  server7.0  和  ms  sql  server 2000。
 
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_LockTableInfo]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Proc_LockTableInfo]
go
CREATE  PROCEDURE  dbo.Proc_LockTableInfo  
AS
begin	
	set  nocount  on
	select              p.hostname,
						 l.req_spid  as  spid,
								 l.rsc_objid,
						 left(v.name,4)  As  Type,
						 left(u.name,8)  As  Mode,
						 left(x.name,6)  As  lockStatus,
						 p.program_name,
						 p.loginame,
						 p.status  as  status,
						 p.blocked
	into  #locks
	from              master.dbo.syslockinfo  l
						 join  (select  name,number  from  master.dbo.spt_values  v  where  type  =  'LR')  v  on  l.rsc_type =  v.number
						 join  (select  name,number  from  master.dbo.spt_values  v  where  type  =  'L')    u  on l.req_mode  +  1  =  u.number
						 join  (select  name,number  from  master.dbo.spt_values  v  where  type  =  'LS')  x  on l.req_status  =  x.number
						 join  master.dbo.sysprocesses  p  on  l.req_spid=p.spid
	where      l.rsc_dbid=db_id()
								 and  l.rsc_objid>0

	select    l.hostname,
						 l.spid,
								 isnull(o.name,'  ')  as  objname,
						 l.Type,
						 l.Mode,
						 l.lockStatus,
						 l.program_name,
						 l.loginame,
						 l.status,
								 l.blocked,
								 isnull(p.hostname,'  ')  as  blockhostname,
								 isnull(p.program_name,'  ')  as  blockprogram
	from    #locks  l
					 join  sysobjects  o  on  l.rsc_objid=o.id
					 left  join  master.dbo.sysprocesses  p  on  l.blocked=p.spid
	order  by  o.name,l.hostname

	drop  table  #locks
end
GO

--exec Proc_LockTableInfo

go


--清除数据库的日志部分                           
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_ClearLog]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Proc_ClearLog]
go
CREATE  PROCEDURE  dbo.Proc_ClearLog 
(
 @DataBaseName varchar(100) 
)
as
begin	
	if dbo.Func_GetSQLServerMainVersion()<10 
	begin
		declare @BegSize varchar(100)  
		declare @EndSize varchar(100)  
		declare @Sqltext Nvarchar(4000)
		exec('use master ')
		set @Sqltext='select  @BegSize=cast(size/7 as varchar(10))  from  '+@DataBaseName+'.dbo.sysfiles '
		EXEC  sp_executesql  @Sqltext,N'@BegSize  varchar(100)  OUTPUT ' ,@BegSize  OUTPUT  

		exec('backup log ['+@DataBaseName+'] with no_log')
		exec('dbcc shrinkdatabase(['+@DataBaseName+'])')

		set @Sqltext='select  @EndSize=cast(size/7 as varchar(10))  from  '+@DataBaseName+'.dbo.sysfiles '
		EXEC  sp_executesql  @Sqltext,N'@EndSize  varchar(100)  OUTPUT ' ,@EndSize  OUTPUT
		select @BegSize BegSize,@EndSize EndSize
	end
	else 
	begin
		--SQL2008 清除数据库日志
		--日志逻辑名
		--这里的日志逻辑名如果不知道是什么名字的话，可以用以下注释的语句进行查询
	 
		exec('
		USE [master]
		
		ALTER DATABASE '+@DataBaseName+' SET RECOVERY SIMPLE WITH NO_WAIT
		
		ALTER DATABASE '+@DataBaseName+' SET RECOVERY SIMPLE   --简单模式
		
		USE '+@DataBaseName+' 
		declare @tempLogName nvarchar(100)
		SELECT  @tempLogName=name FROM sys.database_files where type=1

		DBCC SHRINKFILE (@tempLogName , 11, TRUNCATEONLY)
		USE [master]
		ALTER DATABASE '+@DataBaseName+'  SET RECOVERY FULL WITH NO_WAIT
		ALTER DATABASE '+@DataBaseName+'  SET RECOVERY FULL  --还原为完全模式
		')
	end
end
  
go 

  
 --exec Proc_ClearLog 'AIC' 
 
go
--用PING的方式通过机器名得到机器IP地址                           
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_GetHostIP]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Proc_GetHostIP]
go
CREATE  PROCEDURE  dbo.Proc_GetHostIP 
(
 @HostName varchar(100),
 @HostIP varchar(100) output
)
as
begin	
	declare @str varchar(100) 
  if isnull(@HostName,'')='' 
    set @HostName=Host_Name() 

	set @str='PING '+@HostName
	create table #tmp(aa varchar(200)) 
	insert #tmp exec master..xp_cmdshell @str 
	select top 1 @HostIP = replace(left(aa,charindex(':',aa)-1),'Reply from ','') 
	from #tmp where aa like 'reply from %:%' 
	drop table #tmp  
end

go
/*
declare @OutIP varchar(100) 
exec Proc_GetHostIP '',@OutIP output
print @OutIP
*/
go
--将IP地址段转成每三位用点号分开                        
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_GetFormatIP]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[Func_GetFormatIP]
go
create function Func_GetFormatIP(@IP varchar(15)) 
returns varchar(15) 
As 
begin 
	declare @s varchar(15) 
	set @s = '' 
	while charindex('.',@IP) > 0 
	begin 
		set @s = @s + right('000' + left(@IP,charindex('.',@IP)),4) 
		set @IP = right(@IP,len(@IP)-charindex('.',@IP)) 
	end 
	set @s = @s + right('000' + @IP,3) 
	return @s 
end 
go
--Select dbo.Func_GetFormatIP('202.1.110.2') 
go
--将IP地址段多余的0删除                       
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_GetSingerIP]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[Func_GetSingerIP]
go
create function Func_GetSingerIP(@IP varchar(15)) 
returns varchar(15) 
As 
begin 
	declare @s varchar(15) 
	set @s =REPLACE(REPLACE(REPLACE(REPLACE(@IP,' ',''),'.00','.'),'.0','.'),'..','.0.')
	return @s 
end 
go
--Select dbo.Func_GetSingerIP('202.01.110.002') 
go

--将传入SQL语句的多行转换成一个字符串的自定义函数                           
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].Proc_ColToRow') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure Proc_ColToRow
go
CREATE PROCEDURE Proc_ColToRow(@SQlText nvarchar(4000),@CompartSign varchar(100) ,@OrderbyFields varchar(1000),@OutStr varchar(8000) output ) 
as
begin	
  declare  @OrderbyFields1   varchar(1000)
  declare  @OrderbyFields2   varchar(1000)
  
  declare  @resultStr   varchar(8000)
  declare  @SelectFlag  varchar(10)
  declare  @FormFlag    varchar(10)
  declare  @SelectIndex int
  declare  @FormIndex   int
  declare  @VariableCount int
  if @OrderbyFields=''  
	begin
    set @OrderbyFields1=''
    set @OrderbyFields2=''
	end
  else 
	begin
    set @OrderbyFields1=','+@OrderbyFields
    set @OrderbyFields2=' order by '+@OrderbyFields
	end

  set  @resultStr  =''
  set  @SelectFlag ='SELECT '
  set  @FormFlag   =' FROM '
  set  @SQlText    =upper(@SQlText)
  set  @SelectIndex=charindex(@SelectFlag,@SQlText)
  set  @FormIndex  =charindex(@FormFlag,@SQlText)
  set  @SQlText=' set @result=''''   SELECT  @result=@result +'+''''+@CompartSign+''''+'+a'+' from ( select distinct '+
         substring(@SQlText,@SelectIndex+len(@SelectFlag),
                   @FormIndex-(@SelectIndex+len(@SelectFlag)))+' as a '+@OrderbyFields1+
         substring(@SQlText,@FormIndex,len(@SQlText))+') aa   '+@OrderbyFields2+'   '

 	EXEC  sp_executesql  @Sqltext,N'@result varchar(8000)  OUTPUT ' ,@resultStr  OUTPUT
 
	set @OutStr=substring(@resultStr,len(replace(@CompartSign,' ','a'))+1,len(@resultStr))


end
go 
 
--将传入SQL语句的多行转换成一个字符串的自定义函数                           
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_ColToRow]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[Func_ColToRow]
go
CREATE function  Func_ColToRow(@SQlText varchar(8000),@CompartSign varchar(100) ,@OrderbyFields varchar(1000)='' ) 
returns varchar(8000)
as
begin	
  declare  @OrderbyFields1   varchar(1000)
  declare  @OrderbyFields2   varchar(1000)
  
  declare  @resultStr   varchar(8000)
  declare  @SelectFlag  varchar(10)
  declare  @FormFlag    varchar(10)
  declare  @SelectIndex int
  declare  @FormIndex   int
  declare  @VariableCount int
  if @OrderbyFields=''  
	begin
    set @OrderbyFields1=''
    set @OrderbyFields2=''
	end
  else 
	begin
    set @OrderbyFields1=','+@OrderbyFields
    set @OrderbyFields2=' order by '+@OrderbyFields
	end

  set  @resultStr  =''
  set  @SelectFlag ='SELECT '
  set  @FormFlag   =' FROM '
  set  @SQlText    =upper(@SQlText)
  set  @SelectIndex=charindex(@SelectFlag,@SQlText)
  set  @FormIndex  =charindex(@FormFlag,@SQlText)
  set  @SQlText='declare @result varchar(8000) set @result=''''   SELECT  @result=@result +'+''''+@CompartSign+''''+'+a'+' from ( select distinct '+
         substring(@SQlText,@SelectIndex+len(@SelectFlag),
                   @FormIndex-(@SelectIndex+len(@SelectFlag)))+' as a '+@OrderbyFields1+
         substring(@SQlText,@FormIndex,len(@SQlText))+') aa   '+@OrderbyFields2+' select @result '

--用分布式查询来替代OLE(速度快但需要用户名与密码,1000次5秒钟,而用OLE,1000次为16秒钟)


--    select  @resultStr=field1    
--    from  OPENROWSET('SQLOLEDB','SERVER=grj;uid=sa;pwd=ybsoft;Database=yberp_demo',
--    'declare @result varchar(1000) select @result=isnull(@result,'''')+code from table1   select @result as field1 ')  as  a 
 
  --用OLE查询 
	declare @obj int,@con Nvarchar(1000) 
	set @con=N'Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;Initial Catalog='+replace(db_name(),N'''',N'''''')
	exec sp_oacreate 'adodb.recordset',@obj out
	exec sp_oamethod @obj,'open',null,@SQlText,@con
	exec sp_oagetproperty @obj,N'fields(0).value',@resultStr output
	exec sp_oadestroy @obj

	set @resultStr=substring(@resultStr,len(replace(@CompartSign,' ','a'))+1,len(@resultStr))
	if rtrim(ltrim(@resultStr))=''   
    set @resultStr=null
	 
	return(@resultStr)
end
go
 
/*
select  name,id,
  dbo.Func_ColToRow('select c.Name 
                          from  syscolumns c 
                          where c.id='+cast(id as varchar(100)),',','') 
from sysobjects 
where xtype in ('U','V') and  dbo.Func_InExcludeTableName(name)=0
*/
go

--分解字符串
 
-- 输入:'aa,ab,cc,dd'
-- 得到:
-- aa
-- ab
-- cc
-- dd
 
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_SplitStr]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[Func_SplitStr]
go
CREATE FUNCTION Func_SplitStr (@SplitStr varchar(8000), @CompartSign varchar(100),@isDelKH bit)
RETURNS @returnTable table(col_Value varchar(8000))
AS
BEGIN
	declare @thisSplitStr varchar(100)
	declare @thisCompartIndex int
	declare @lastCompartIndex int

	set @lastCompartIndex = 0
  if @isDelKH=1
  begin
    if substring(@SplitStr,1,1)='('
      set @SplitStr=substring(@SplitStr,2,len(@SplitStr))
    if substring(@SplitStr,len(@SplitStr),1)=')'
      set @SplitStr=substring(@SplitStr,1,len(@SplitStr)-1)
  end

	if Right(@SplitStr ,len(@CompartSign)) <> @CompartSign set @SplitStr = @SplitStr + @CompartSign
	set @thisCompartIndex = CharIndex(@CompartSign,@SplitStr ,@lastCompartIndex)

	while @lastCompartIndex <= @thisCompartIndex
	begin
					 set @thisSplitStr = SubString(@SplitStr ,@lastCompartIndex,@thisCompartIndex-@lastCompartIndex)
					 set @lastCompartIndex = @thisCompartIndex + 1
					 set @thisCompartIndex = CharIndex(@CompartSign,@SplitStr ,@lastCompartIndex)
					 insert into @returnTable values(@thisSplitStr)
	end
	return
END
go
--select * from dbo.Func_SplitStr('aa,ab,cc,dd',',')

--取体积
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_GetArea]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[Func_GetArea]
go
CREATE FUNCTION Func_GetArea (@Str varchar(100))
RETURNS varchar(100)
AS
BEGIN
	declare @tempArea decimal(18,4)
	declare @i int
	declare @str1 varchar(100)
	declare @str2 varchar(100)
	declare @str3 varchar(100)
	set @str1=''
	set @str2=''
	set @str3=''
	set @i=0
	set @tempArea=0
	set @i=charindex('*',@Str)
	if @i>0 --第一个*
	begin
	  set @str1=substring(@Str,1,@i-1)
	  set @str=substring(@Str,@i+1,len(@Str))
		set @i=charindex('*',@Str)
		if @i>0 --第二个*
		begin
		  set @str2=substring(@Str,1,@i-1)
		  set @str3=substring(@Str,@i+1,len(@Str))
	    if @str1<>'' and @str3<>''
      begin
		    set @i=charindex('m',@str3)
	      if @i>0 
	      begin
			    set @str3=REPLACE(@str3,'m','')  
  	      set @tempArea=cast(@str1 as decimal(18,4))*cast(@str3 as decimal(18,4))/1000
	      end
        else
	      begin
  	      set @tempArea=cast(@str1 as decimal(18,4))*cast(@str3 as decimal(18,4))/1000000
	      end
      end
		end
	end
	--return(@str1+@str2+@str3)
	return(@tempArea)
end
go
--select dbo.Func_GetArea('1030*1.95*3000')

--输出日期加流水号                        
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].Proc_OutDateTimeAndCode') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure Proc_OutDateTimeAndCode
go
CREATE PROCEDURE Proc_OutDateTimeAndCode(@TableName nvarchar(100),
                                         @FieldName nvarchar(100) ,
                                         @OutCode nvarchar(100) output ,
                                         @DateTimeLength int=8,@CodeLength int=4) 
as
begin	
  declare  @SQlText   nvarchar(4000)

  set  @SQlText=		
		' select  @result=convert(nvarchar('+cast(@DateTimeLength as nvarchar)+'),getdate(),112)+'+
		'           RIGHT(''00000000000000000000000000000000''+cast(isnull((max(cast(substring('+@FieldName+','+cast(@DateTimeLength+1 as nvarchar)+','+cast(@CodeLength as nvarchar)+') as int))),0)+1 as nvarchar),'+cast(@CodeLength as nvarchar)+')'+
		' from '+@TableName+' '+
		' where substring('+@FieldName+',1,'+cast(@DateTimeLength as nvarchar)+')=convert(nvarchar('+cast(@DateTimeLength as nvarchar)+'),getdate(),112) and dbo.Func_ISNUMERIC(substring('+@FieldName+','+cast(@DateTimeLength+1 as nvarchar)+','+cast(@CodeLength as nvarchar)+'))=1 '
	print @SQlText
 	EXEC  sp_executesql  @Sqltext,N'@result nvarchar(100)  OUTPUT ' ,@OutCode  OUTPUT
end
go 


--int数据类型自动编号的问题
--如果有空缺会自己填上，自动递增。取得最小的可用ID号
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_AutoID]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[Func_AutoID]
go
CREATE function  Func_AutoID
(
  @TableName varchar(100),
  @IDFieldName varchar(100) 
) 
returns int
AS
begin
	declare @SQlText Nvarchar(4000)
	declare @ReturntID int
	set @SQlText=
				 ' SELECT (CASE '+
				 '         WHEN EXISTS(SELECT * FROM '+@TableName+' b WHERE b.'+@IDFieldName+' = 1 '+      --最小值
				 '                      ) THEN MIN('+@IDFieldName+') + 1 ELSE 1 '+
				 '         END) as '+@IDFieldName+
				 ' FROM '+@TableName+
				 ' WHERE NOT '+@IDFieldName+' IN (SELECT a.'+@IDFieldName+' - 1 FROM '+@TableName+ ' a) '

	declare @obj int,@con Nvarchar(1000) 
	set @con=N'Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;Initial Catalog='+replace(db_name(),N'''',N'''''')
	exec sp_oacreate 'adodb.recordset',@obj out
	exec sp_oamethod @obj,'open',null,@SQlText,@con
	exec sp_oagetproperty @obj,N'fields(0).value',@ReturntID output
	exec sp_oadestroy @obj
	return(@ReturntID)
end
go
 
-- CREATE TABLE [Handle] (
--        [HandleID] int   
-- ) ON [PRIMARY]
-- GO
-- INSERT INTO [Handle] ([HandleID]) values (1)
-- INSERT INTO [Handle] ([HandleID]) values (2)
-- INSERT INTO [Handle] ([HandleID]) values (6)
-- INSERT INTO [Handle] ([HandleID]) values (5)
-- 
-- select   dbo.Func_AutoID('Handle','HandleID')
-- --得到3
-- drop table Handle

--取某一个月的最后一天,
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_LastDayOfMonth]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[Func_LastDayOfMonth]
go
CREATE function  Func_LastDayOfMonth
(
  @date  datetime
) 
returns datetime
AS
begin
  --SELECT 32-Day(@date+(32-Day(@date)))  as  該月份的天數
  SELECT @date=cast(str(Year(@date))+'/'+str(Month(@date))+'/'+str(32-Day(@date+(32-Day(@date))))  as  datetime)
  return @date
end
go
--select dbo.Func_LastDayOfMonth('2004-1-1')


--二进制到其它 ,2,4,8,16
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_BinToOther]') and xtype in (N'FN', N'IF', N'TF'))
drop function Func_BinToOther
go
create function Func_BinToOther (@number varchar(300),@type int)
returns varchar(300)
as
begin
  declare @return varchar(300),@i bigint
  declare @ret bigint
  select @ret = 0
  select @return='',@i=1
  if @type=2
  begin
      select @return=@number
  end
  if @type=8
  begin
      while len(@number)%3 <> 0
      begin
          select @number='0'+@number
      end
      while @i<= len(@number)/3
      begin
          select @ret=cast(substring(@number,(@i-1)*3+1,1) as bigint)*4+
              cast(substring(@number,(@i-1)*3+2,1) as bigint)*2+
              cast(substring(@number,@i*3,1) as bigint)
          select @return=@return+convert(varchar(30),@ret)
          select @i=@i+1
      end
  end
  if @type=10
  begin
      while @i<=len(@number)
          begin
          select     @ret=case substring(@number,@i,1) when '0' then @ret*2 else @ret*2+1 end
          select @i=@i+1
          end
      select @return = convert(varchar(30),@ret)
  end
  if @type=16
  begin
      while len(@number)%4 <> 0
      begin
          select @number='0'+@number
      end
      while @i<= len(@number)/4
      begin
          select @return=@return+case substring(@number,(@i-1)*4+1,4) when '0000' then '0' when '0001' then '1'
              when '0010' then '2' when '0011' then '3' when '0100' then '4' when '0101' then '5'
              when '0110' then '6' when '0111' then '7' when '1000' then '8' when '1001' then '9'
              when '1010' then 'A' when '1011' then 'B' when '1100' then 'C' when '1101' then 'D'
              when '1110' then 'E' when '1111' then 'F' end
          select @i=@i+1
      end
  end
  return @return
end
go
--select dbo.Func_BinToOther('1111',10)
go

--其它到二进制 ,2,4,8,16
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_OtherToBin]') and xtype in (N'FN', N'IF', N'TF'))
drop function Func_OtherToBin
go
create function Func_OtherToBin (@number varchar(300),@type int)
returns varchar(300)
as
begin
    declare @return varchar(300),@i bigint
    select @return='',@i=1
    if @type=2
    begin
        select @return=@number
    end
    else if @type=8
    begin
        while @i<=len(@number)
        begin
            select @return=@return+case substring(@number,@i,1) when '0' then '000' 
                when '1' then '001' when '2' then '010' when '3' then '011'
                when '4' then '100' when '5' then '101' when '6' then '110' 
                when '7' then '111' end
            select @i=@i+1
        end
    end
    else if @type=10
    begin
        while cast(@number as bigint)>=1
        begin
            select @return=ltrim(cast(@number as bigint)%2)+@return
            select @number=ltrim((cast(@number as bigint)-cast(@number as bigint)%2)/2)
        end
    end
    else if @type=16
    begin
        while @i<=len(@number)
        begin
            select @return=@return+case substring(@number,@i,1) when '0' then '0000' 
                when '1' then '0001' when '2' then '0010' when '3' then '0011'
                when '4' then '0100' when '5' then '0101' when '6' then '0110' 
                when '7' then '0111' when '8' then '1000' when '9' then '1001' 
                when 'A' then '' when 'B' then '1011' when 'C' then '1100' 
                when 'D' then '1101' when 'E' then '1110' when 'F' then '1111' end
            select @i=@i+1
        end
    end
    return @return
end
go
--select dbo.Func_OtherToBin('11259375',10)

go
--10进制转16进制
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_DecTOHex]') and xtype in (N'FN', N'IF', N'TF'))
drop function Func_DecTOHex
go
create function Func_DecTOHex(@int10 int)
returns varchar(8)
begin
  declare @str16 nvarchar(8)
  set @str16=''

  if(@int10>0)
  begin
    while @int10>0
    begin
        set @str16=substring('0123456789ABCDEF',@int10%16+1,1)+@str16
        set @int10=@int10/16
    end
  end
  else
  begin
      set @str16='0'
  end
  return @str16
end
go
--select dbo.Func_DecTOHex(65535)
--更快的方法
--select cast(255 as varbinary(4))
--select convert(varbinary(4), 255)
go

--16进制转10进制
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_HexToDec]') and xtype in (N'FN', N'IF', N'TF'))
drop function Func_HexToDec
go
create function Func_HexToDec(@str16 varchar(8)) 
returns int 
begin 
  declare @int10 int 
  declare @i int     
  set @int10=0 
  set @i=1 

  while @i<=len(@str16) 
  begin 
    set @int10=@int10+
        convert(int,
            (case   when substring(@str16,@i,1)<='9' then substring(@str16,@i,1)
                when substring(@str16,@i,1)<='A' then '10'
                when substring(@str16,@i,1)<='B' then '11'
                when substring(@str16,@i,1)<='C' then '12'
                when substring(@str16,@i,1)<='D' then '13'
                when substring(@str16,@i,1)<='E' then '14'
                when substring(@str16,@i,1)<='F' then '15' end )) * power(16,len(@str16)-@i) 
    set @i=@i+1
  end
  return @int10 
end
go
--select dbo.Func_HexToDec('1')
go
--  把全角数据转换成半角数字（注意，这个只能转换全是全角的，在第一个半角数据时返回）
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_WideStrToAnsi]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[Func_WideStrToAnsi]
go
create function  Func_WideStrToAnsi
(
 @WideStr varchar(8000)
)
returns varchar(4000)
as
begin
 declare @strReturn varchar(4000)
  ,@bin  varbinary(4000)
  ,@str  varchar(4000)
  ,@stmp varchar(4)
  ,@i   int
  ,@len  int
 set @strReturn=''
 set @bin=convert(varbinary(4000),@WideStr)
 exec master..xp_varbintohexstr @bin, @str out
 
 select @str=stuff(@str,1,2,'') 
 set @len=len(@str)
 set @i=1
 while @i<@len
 begin
 set @stmp = substring(@str,@i,4)
 set @i=@i+4
 if(substring(@stmp,1,1) <> 'A')
 begin
   set @i=@i-2
   set @stmp=left(@stmp,2)
 end
 set @stmp = replace(@stmp,'A','')
 set @stmp = replace(@stmp,'B','')
 
 set @stmp = char(dbo.Func_ToDecimalFromOther(@stmp,16)) 
 set @strReturn = @strReturn + @stmp
 end
 return  @strReturn
end
go
--示例
--select '０３１８５１００１８４５' as ORG,dbo.Func_WideStrToAnsi('０３１８５１００１８４５') DES

--SQLSERVER中快速获海量数据的记录总数 
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_GetBigTableRecount_Table]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[Func_GetBigTableRecount_Table]
go
CREATE function  Func_GetBigTableRecount_Table(@TableName nvarchar(100)) 
returns table
as
	RETURN (
	select top 1000000 object_name(id) tablename,8*reserved/1024 reserved,rtrim(8*dpages/1024)+'Mb' used,8*(reserved-dpages)/1024 unused,8*dpages/1024-rows/1024*minlen/1024 free,  rows 
	from sysindexes 
	WHERE (@TableName ='' or object_name(id) =@TableName) and  indid=1 
  order by reserved desc 
  )
go
 
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_GetBigTableRecount]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[Func_GetBigTableRecount]
go
CREATE function  Func_GetBigTableRecount(@TableName nvarchar(100)) 
returns varchar(8000)
as
begin	
  declare @Count int
  set @Count=0
	SELECT @Count=rows 
	FROM sysobjects a INNER JOIN
	      sysindexes b ON a.id = b.id
	WHERE  (a.name =@TableName) AND (indid= 0 or b.indid = 1)
  return(@Count) 
end
go  
-- CREATE TABLE [Handle] (
--        [HandleID] int   
-- ) ON [PRIMARY]
-- GO
-- INSERT INTO [Handle] ([HandleID]) values (1)
-- INSERT INTO [Handle] ([HandleID]) values (2)
-- INSERT INTO [Handle] ([HandleID]) values (6)
-- INSERT INTO [Handle] ([HandleID]) values (5)
-- 
-- select dbo.Func_GetBigTableRecount('Handle')
-- --得到4
-- drop table Handle
  
-- 如何在SQL Server2000中处理半个汉字的问题
-- 汉字是由两个字节存储的，每个字节的数值都>127。所以上面的问题的解决方案就是：把字符串按字节顺序截取，当出现第一个字节是>127,
-- 但是后面一个字节是<127的这种情况，就丢弃掉。
-- 接下来，我们要明确些技术细节：
-- (1) 在SQL Server中有很多字符串相关的操作函数，但是直接去操作包含了汉字的字符串，会有点问题。
-- 说明如下：LEN计算长度的时候，会把汉字和英文字符都看成长度一样的；substring也是这样，ascii是返回第一个字节的ASCII 码。
-- Example:
-- select len('你好a'),substring('你好a',1,1),ascii('你')
-- 结果是
-- 3           你          196
-- 一定要把字符串要转换成varbinary来处理，才可以。
-- (2) SQL Server中，如何把ASCII码组合成汉字，就是把高字节和底字节分别转成字符再拼接起来。如char(210)+char(166)就是姚这个汉字。
-- 再明确了上面的技术细节后，接下来就可以解决问题了。我通过编写一个函数来解决问题。
-- 我们还可以发现这样一个有趣的现象，由于我是截了半个汉字出来，结果你的前半个汉字的字符和后面一个英文字母a 组合，成了一个怪怪的字符腶。
-- 总结，通过这个问题的解决，大家可以了解汉字在SQL Server2000中存储和处理的基本方法，像如何区分一个字符串中是否包含了汉字，
-- 和分离字符串中中文和英文都可以套用本文中方法。
 

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_DelSemiChinese]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[Func_DelSemiChinese]
go
create function Func_DelSemiChinese(@str varchar(8000))
returns  varchar(8000)
as
begin
	declare @sTmp varchar(8000),@i int,@itmp int,@itmp2 int ,@stmp2 varchar(2) 
	select @sTmp=''
  select @i=1
  while @i<=len(@str)
  begin
		select @itmp=convert(int,substring(convert(varbinary,substring(@str ,@i,1)),1,1)) --截取一个字节
		if @itmp>127  
		begin
			--大于127检查后面一个字节
			select @itmp2=convert(int,substring(convert(varbinary,substring(@str ,@i,1)),2,1))
			if @itmp2>127 
			begin
				select @stmp2=char(@itmp)+char(@itmp2)   --是一个完整的汉字 
			end 
			else
			begin
				select @stmp2=char(@itmp2)   --丢弃半个汉字
			end
		end
		else
		begin
			select @sTmp2=char(@itmp)
		end 
		select @sTmp=@sTmp+@stmp2 
		select @i=@i+1
  end
  return @stmp
end
 
go
 
--测试如下:
--DECLARE @str varchar(4000)
--select @str='b'+char(convert(int,substring(convert(varbinary,'你'),1,1)))+'a你'
--select @str  带有半个汉字, DATALENGTH (@str) 长度,dbo.Func_DelSemiChinese(@str) 去掉半个汉字, DATALENGTH (dbo.Func_DelSemiChinese(@str)) 长度
--带有半个汉字   长度    去掉半个汉字   长度          
--b腶你          5       	ba你         	4	
 
--返回一个字符串中某一个字符第n次出现的位置的函数
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_GetStrNPositionInStr]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[Func_GetStrNPositionInStr]
go
create  function Func_GetStrNPositionInStr(@aFindStr varchar(100),@aStrList varchar(8000),@NValue int)
returns int
as
begin
  declare  @i int --当前找到第@i个
  declare  @position int--所在位置
  set @position=1;
  set @i=0;
  while charindex(@aFindStr,@aStrList,@position)>0
  begin
    set @position=charindex(@aFindStr,@aStrList,@position)+1;
    set @i=@i+1;
    if @i=@NValue
    begin
     return @position-1;
    end
  end
  return 0;--0表示未找到
end 
go
--select dbo.Func_GetStrNPositionInStr(',','abc,def,ccc,ged',1) 
go

--根据aStr1在aStr1List的位置得到aStr2List对应位置的字串
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_GetStrByStrListPosition]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[Func_GetStrByStrListPosition]
go
create function Func_GetStrByStrListPosition(@aStr1 varchar(100),
                                             @aStr1List varchar(8000),
                                             @aStr2List varchar(8000),
                                             @aSpaceStr varchar(100)
                                                 )
returns  varchar(100)
as
begin
	declare @tempResult varchar(100)
  if charindex(@aSpaceStr,@aStr2List)<=0 
  begin
    set @tempResult= @aStr2List
  end
  else 
  begin
		declare @tempStr varchar(8000)
	  declare @tempPosition int
	  declare @tempPosition1 int
	  declare @tempPosition2 int
	  set @tempPosition=charindex(@aStr1,@aStr1List)
	  set @tempStr=substring(@aStr1List,1,@tempPosition-1)
    if isnull(@tempStr,'')='' 
	  begin
	    set @tempResult= substring(@aStr2List,1,charindex(@aSpaceStr,@aStr2List)-1)
	  end
	  else 
	  begin
		  --简单计算一字符在另一子串中出现的次数
		  set @tempPosition=(len(replace(@tempStr,@aSpaceStr,@aSpaceStr+'----------'))-len(@tempStr))/10
	
	    set @tempPosition1=dbo.Func_GetStrNPositionInStr(@aSpaceStr,@aStr2List,@tempPosition)
	    set @tempPosition2=dbo.Func_GetStrNPositionInStr(@aSpaceStr,@aStr2List,@tempPosition+1)
	    if @tempPosition2=0 
	      set @tempPosition2=len(@aStr2List)+1
	    
	    if @tempPosition1>=0 and @tempPosition2-@tempPosition1-1>0
	    set @tempResult= substring(@aStr2List,@tempPosition1+1,@tempPosition2-@tempPosition1-1)
	  end
  end
  return @tempResult
end
 
go
--select dbo.Func_GetStrByStrListPosition('123','123,4567,891022','00000001,002,00003',',')

go

--sql server实现掩码功能 
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_NumberMask]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[Func_NumberMask]
GO  

CREATE FUNCTION Func_NumberMask (@InputNumber varchar(100),@BitNum int,@FillChar varchar(1)='0')
RETURNS varchar(100)
AS
BEGIN
	DECLARE @OutputString varchar(100)
	SET @OutputString = REPLICATE(@FillChar, @BitNum-datalength(@InputNumber)) + @InputNumber
	RETURN @OutputString
END

go
--select dbo.Func_NumberMask('1',5,'0') 
--得到00001
    
--实现从文本中读取  
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_ReadText]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[Func_ReadText]
go
Create    function    Func_ReadText
(
 @FilePath varchar(1000)
)   
returns varchar(8000)
as
begin  
	DECLARE  @Scriptingobject  int ,@i int 
	DECLARE  @TextFile  int  
	DECLARE  @Sqltext  varchar(255), @tempstr2  Nvarchar(4000)
	set @Sqltext='dir '+@FilePath
	exec  @i=master.dbo.xp_cmdshell @Sqltext ,NO_OUTPUT  
	if  @i=0  
	begin
		--創建Scripting組件實例  
		EXEC sp_OACreate  'Scripting.FileSystemObject',  @Scriptingobject  OUT  
		EXEC sp_OAMethod  @Scriptingobject,  'OpenTextFile',  @TextFile  OUTPUT  ,@FilePath  
		EXEC sp_OAMethod  @TextFile,  'Read',  @tempstr2  OUT,3000  
    EXEC sp_OAMethod  @TextFile,  'Close',NULL  
	end
	return(@tempstr2)
end

go
--select dbo.Func_ReadText('C:\aa.Txt')
 

--实现写入到文本中  
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_WriteText]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[Func_WriteText]
go
Create    function    Func_WriteText
(
 @FilePath varchar(1000),
 @WriteText  varchar(8000)
)   
returns Nvarchar(4000)
as
begin  
	DECLARE  @Scriptingobject  int ,@i int 
	DECLARE  @TextFile  int  
	DECLARE  @Sqltext  varchar(255) 
	
  --創建Scripting組件實例  
	EXEC sp_OACreate  'Scripting.FileSystemObject',  @Scriptingobject  OUT  
	set @Sqltext='dir '+@FilePath
	exec  @i=master.dbo.xp_cmdshell @Sqltext ,NO_OUTPUT  
	--if  @i=0  
	--begin
		EXEC sp_OAMethod  @Scriptingobject,  'CreateTextFile',  @TextFile  OUTPUT  ,@FilePath  
	--end
  EXEC sp_OAMethod  @TextFile,  'Write',NULL,  @WriteText  
  EXEC sp_OAMethod  @TextFile,  'Close',NULL  
	return(@WriteText)
  -- 也可参照如下方法
	/*
	DECLARE  @var  varchar(100)  
	DECLARE  @i  int  
	DECLARE  @cmd  sysname  
	set  @i=120  
	SET  @var  =  '@i='+cast(@i  as  varchar(10))  
	SET  @cmd  =  'echo  '  +  @var  +  '  >  c:\VarOut.txt'  
	EXEC  master..xp_cmdshell  @cmd  
	*/
end
go
--select dbo.Func_WriteText('C:\aa.Txt','aabbccdd')


/*
查当前SQL Server连接的工作站信息,包括:操作的数据库名,计算机名,用户名,网卡物理地址,IP地址,程序名
           获取连接SQL服务器的信息
           所有连接本机的:操作的数据库名,计算机名,用户名,网卡物理地址,IP地址,程序名
--*/
if  exists  (select  *  from  dbo.sysobjects  where  id  =object_id(N'[dbo].[Proc_Getlinkinfo]') and OBJECTPROPERTY(id,  N'IsProcedure')=1)
  drop  procedure  [dbo].[Proc_Getlinkinfo]
GO

create  procedure  Proc_Getlinkinfo
@dbname  sysname=null,                               --要查询的数据库名,默认查询所有数据库的连接信息
@includeip  bit=0                                    --是否显示IP地址,因为查询IP地址比较费时,所以增加此控制
as
begin	
	declare  @dbid  int
	set  @dbid=db_id(@dbname)

	create  table  #tb(id  int  identity(1,1),dbname  sysname,hostname  nchar(128),loginname nchar(128),net_address  nchar(12),net_ip  nvarchar(15),prog_name  nchar(128))
	insert  into  #tb(hostname,dbname,net_address,loginname,prog_name)
	select  distinct  hostname,db_name(dbid),net_address,loginame,program_name  from  master..sysprocesses
						 where  hostname<>''  and  (@dbid  is  null  or  dbid=@dbid)

	if  @includeip=0  goto  lb_show    --如果不显示IP地址,就直接显示

	declare  @sql  varchar(500),@hostname  nchar(128),@id  int
	create  table  #ip(hostname  nchar(128),a  varchar(200))
	declare  tb  cursor  local  for  select  distinct  hostname  from  #tb
	open  tb
	fetch  next  from  tb  into  @hostname
	while  @@fetch_status=0
	begin
		set  @sql='ping  '+@hostname+'  -a  -n  1  -l  1'
		insert  #ip(a)  exec  master..xp_cmdshell  @sql
		update  #ip  set  hostname=@hostname  where  hostname  is  null
		fetch  next  from  tb  into  @hostname
	end

	update  #tb  set  net_ip=left(a,patindex('%:%',a)-1)
	from  #tb  a  inner  join  (
	select  hostname,a=substring(a,patindex('Ping  statistics  for  %:%',a)+20,20)  from  #ip
						 where  a  like  'Ping  statistics  for  %:%')  b  on  a.hostname=b.hostname

	lb_show:
	select  id,数据库名=dbname,客户机名=hostname,用户名=loginname
						 ,网卡物理地址=net_address,IP地址=net_ip,应用程序名称=prog_name  from  #tb
end
go
--显示所有本机的连接信息
--exec  p_getlinkinfo

--显示所有本机的连接信息,包含ip地址
--exec  p_getlinkinfo  @includeip=1

--显示连接指定数据库的信息
--exec  p_getlinkinfo  'tempsql'

--exec  sp_who
--exec sp_helplogins

go
 
--数据转换成星期的数字
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_ToWeek]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[Func_ToWeek]
go
Create    function    Func_ToWeek
(
@Num int
)   
returns nvarchar(100)
as
begin 
	Declare @tempStr nvarchar(100)  
  if @Num=1 
    set @tempStr='一'
  else if @Num=2 
    set @tempStr='二'
  else if @Num=3 
    set @tempStr='三'
  else if @Num=4 
    set @tempStr='四'
  else if @Num=5 
    set @tempStr='五'
  else if @Num=6 
    set @tempStr='六'
  else if @Num=0 
    set @tempStr='天'

	return(@tempStr)
end
go 
--功能：小写金额转换成大写,最多支持到分
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_LMoneyToUMoney]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[Func_LMoneyToUMoney]
go
Create    function    Func_LMoneyToUMoney
(
@n_LowerMoney numeric(15,2),
@v_TransType int=2 ,
@IsViewLowerStr bit=0 ,
@SpaceNum int=5, 
@IsLowerStrBeg bit=1
)   
returns varchar(200)
as
begin  
	Declare @tempStr VARCHAR(200)  
	Declare @v_LowerStr VARCHAR(200) -- 小写金额 
	Declare @v_UpperPart VARCHAR(200) 
	Declare @v_UpperStr VARCHAR(200) -- 大写金额
	Declare @i_I int

	 
	select @v_LowerStr = LTRIM(RTRIM(STR(@n_LowerMoney,20,2))) --四舍五入为指定的精度并删除数据左右空格

	select @i_I = 1
	select @v_UpperStr = ''

	while ( @i_I <= len(@v_LowerStr))
	begin
		select @v_UpperPart = case substring(@v_LowerStr,len(@v_LowerStr) - @i_I + 1,1)
													WHEN  '.' THEN  '元'
													WHEN  '0' THEN  '零'
													WHEN  '1' THEN  '壹'
													WHEN  '2' THEN  '贰'
													WHEN  '3' THEN  '叁'
													WHEN  '4' THEN  '肆'
													WHEN  '5' THEN  '伍'
													WHEN  '6' THEN  '陆'
													WHEN  '7' THEN  '柒'
													WHEN  '8' THEN  '捌'
													WHEN  '9' THEN  '玖'
													END
												+ 
													case @i_I
													WHEN  1  THEN  '分'
													WHEN  2  THEN  '角'
													WHEN  3  THEN  ''
													WHEN  4  THEN  ''
													WHEN  5  THEN  '拾'
													WHEN  6  THEN  '佰'
													WHEN  7  THEN  '仟'
													WHEN  8  THEN  '万'
													WHEN  9  THEN  '拾'
													WHEN  10  THEN  '佰'
													WHEN  11  THEN  '仟'
													WHEN  12  THEN  '亿'
													WHEN  13  THEN  '拾'
													WHEN  14  THEN  '佰'
													WHEN  15  THEN  '仟'
													WHEN  16  THEN  '万'
													ELSE ''
													END
		select @v_UpperStr = @v_UpperPart + @v_UpperStr
		select @i_I = @i_I + 1
	end

	--------print  '//v_UpperStr ='+@v_UpperStr +'//'

	if ( @v_TransType=0 )
	begin
		select @v_UpperStr = REPLACE(@v_UpperStr,'零拾','零') 
		select @v_UpperStr = REPLACE(@v_UpperStr,'零佰','零') 
		select @v_UpperStr = REPLACE(@v_UpperStr,'零仟','零') 
		select @v_UpperStr = REPLACE(@v_UpperStr,'零零零','零')
		select @v_UpperStr = REPLACE(@v_UpperStr,'零零','零')
		select @v_UpperStr = REPLACE(@v_UpperStr,'零角零分','整')
		select @v_UpperStr = REPLACE(@v_UpperStr,'零分','整')
		select @v_UpperStr = REPLACE(@v_UpperStr,'零角','零')
		select @v_UpperStr = REPLACE(@v_UpperStr,'零亿零万零元','亿元')
		select @v_UpperStr = REPLACE(@v_UpperStr,'亿零万零元','亿元')
		select @v_UpperStr = REPLACE(@v_UpperStr,'零亿零万','亿')
		select @v_UpperStr = REPLACE(@v_UpperStr,'零万零元','万元')
		select @v_UpperStr = REPLACE(@v_UpperStr,'万零元','万元')
		select @v_UpperStr = REPLACE(@v_UpperStr,'零亿','亿')
		select @v_UpperStr = REPLACE(@v_UpperStr,'零万','万')
		select @v_UpperStr = REPLACE(@v_UpperStr,'零元','元')
		select @v_UpperStr = REPLACE(@v_UpperStr,'零零','零')
	end
	-- 对壹元以下的金额的处理 
	if ( substring(@v_UpperStr,1,1)='元' )
	begin
			 select @v_UpperStr = substring(@v_UpperStr,2,(len(@v_UpperStr) - 1))
	end

	if (substring(@v_UpperStr,1,1)= '零')
	begin
			 select @v_UpperStr = substring(@v_UpperStr,2,(len(@v_UpperStr) - 1))
	end

	if (substring(@v_UpperStr,1,1)='角')
	begin
			 select @v_UpperStr = substring(@v_UpperStr,2,(len(@v_UpperStr) - 1))
	end

	if ( substring(@v_UpperStr,1,1)='分')
	begin
			 select @v_UpperStr = substring(@v_UpperStr,2,(len(@v_UpperStr) - 1))
	end

	if (substring(@v_UpperStr,1,1)='整')
	begin
			 select @v_UpperStr = '零元整'
	end

  if right(@v_UpperStr,2)='零分'
    set @v_UpperStr=left(@v_UpperStr,len(@v_UpperStr)-2)
  if right(@v_UpperStr,2)='零角'
    set @v_UpperStr=left(@v_UpperStr,len(@v_UpperStr)-2)
  if right(@v_UpperStr,2)='零元'
    set @v_UpperStr=left(@v_UpperStr,len(@v_UpperStr)-2)
  if right(@v_UpperStr,2)='零拾'
    set @v_UpperStr=left(@v_UpperStr,len(@v_UpperStr)-2)
  if right(@v_UpperStr,2)='零佰'
    set @v_UpperStr=left(@v_UpperStr,len(@v_UpperStr)-2)
  if right(@v_UpperStr,2)='零仟'
    set @v_UpperStr=left(@v_UpperStr,len(@v_UpperStr)-2)
  if right(@v_UpperStr,2)='零万'
    set @v_UpperStr=left(@v_UpperStr,len(@v_UpperStr)-2)

  if right(@v_UpperStr,2)='零分'
    set @v_UpperStr=left(@v_UpperStr,len(@v_UpperStr)-2)
  if right(@v_UpperStr,2)='零角'
    set @v_UpperStr=left(@v_UpperStr,len(@v_UpperStr)-2)
  if right(@v_UpperStr,2)='零元'
    set @v_UpperStr=left(@v_UpperStr,len(@v_UpperStr)-2)
  if right(@v_UpperStr,2)='零拾'
    set @v_UpperStr=left(@v_UpperStr,len(@v_UpperStr)-2)
  if right(@v_UpperStr,2)='零佰'
    set @v_UpperStr=left(@v_UpperStr,len(@v_UpperStr)-2)
  if right(@v_UpperStr,2)='零仟'
    set @v_UpperStr=left(@v_UpperStr,len(@v_UpperStr)-2)
  if right(@v_UpperStr,2)='零万'
    set @v_UpperStr=left(@v_UpperStr,len(@v_UpperStr)-2)
	if (right(@v_UpperStr,1)='元')   
	begin
			 set @v_UpperStr =@v_UpperStr+'整'
	end
	if  (right(@v_UpperStr,1)='亿') or (right(@v_UpperStr,1)='万') or (right(@v_UpperStr,1)='仟') or (right(@v_UpperStr,1)='佰')
		 or (right(@v_UpperStr,1)='拾')
	begin
			 set @v_UpperStr =@v_UpperStr+'元整'
	end
	if @v_UpperStr='元整'    
	begin
			 set @v_UpperStr ='零'+@v_UpperStr 
	end
  if @IsViewLowerStr=1  
  begin
    if @IsLowerStrBeg =1
      set @v_UpperStr=ltrim(rtrim((cast(@n_LowerMoney as varchar(50)))))+space(@SpaceNum)+@v_UpperStr
    else
      set @v_UpperStr=@v_UpperStr+space(@SpaceNum)+ltrim(rtrim((cast(@n_LowerMoney as varchar(50)))))
  end
	return(@v_UpperStr)
end

GO

--select dbo.LMoneyToUMoney (800.00,2,1,1,1)
--检测表描述重复的字段
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_GetSameFieldCaption]') and xtype in (N'FN', N'IF', N'TF'))
drop function Func_GetSameFieldCaption
go
create function Func_GetSameFieldCaption()  
RETURNS table
AS 
 	
	RETURN (
					select * from
					(
					select  Tablename,Caption,count(Name) as Qty
					from dbo.Func_GetFieldInfo ('','','')  
					where caption<>'' 
					group by tablename,caption
					) a
					where qty>1
				 )
 
 
go
--select * from dbo.Func_GetSameFieldCaption()

/*
--告诉你类别名，删除这条类别和所有子类别,字段有：类别名    父类别名
*/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_DeleteTreeNode]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Proc_DeleteTreeNode]
go
CREATE PROCEDURE Proc_DeleteTreeNode
(
 @TableName varchar(100),
 @IDFieldName varchar(100),
 @ParentIDFieldName varchar(100), 
 @IDValue varchar(100)
)
as
begin
	declare  @Tempstr2  Nvarchar(4000)
	declare  @SQLText   Nvarchar(4000)
	set @Sqltext=@IDValue     
	set @Tempstr2=@IDValue     
	while  len(isnull(@Sqltext,''''))<>0
	begin
		set @SQLText=' select @Sqltext=isnull(@Sqltext,'''')+'',''+cast('+@IDFieldName+'  as  varchar) '+
								 ' from  '+@TableName+'  where  '+@ParentIDFieldName+'  in ('+@Sqltext+')'
		exec sp_executesql @SQLText,
											 N'@Sqltext Nvarchar(4000) output',@Sqltext output 
		set @Tempstr2=@Tempstr2+@Sqltext
		if left(@Sqltext,1)=',' 
			set @Sqltext=substring(@Sqltext,2,len(@Sqltext))
	end
	set @SQLText='delete  from  '+@TableName+'  where  '+@IDFieldName+'  in  ('+@Tempstr2 +')'
	exec sp_executesql @SQLText
end

go

--得到类别和所有子类别,字段有：类别名    父类别名

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_GetChildNode_table]') and xtype in (N'FN', N'IF', N'TF'))
drop function Func_GetChildNode_table
go
Create function [dbo].[Func_GetChildNode_table](@ID varchar(100))   
returns @tempResturnTable table(ID varchar(100),Name varchar(100),ParentID varchar(100),Level int)   
as  
begin  
  declare @i int  
  set @i = 1   
  insert into @tempResturnTable select De_Guid,De_Name,De_ParentGuid,@i from AIC_Base_Depart where De_Guid = @ID
  insert into @tempResturnTable select De_Guid,De_Name,De_ParentGuid,@i from AIC_Base_Depart where De_ParentGuid = @ID   

  while @@rowcount<>0   
  begin  
      set @i = @i + 1   
      insert into @tempResturnTable   
      select a.De_Guid,a.De_Name,a.De_ParentGuid,@i   
      from AIC_Base_Depart a,@tempResturnTable b   
      where a.De_ParentGuid=b.ID and b.Level = @i-1   
  end  
 
  return  
end   

/*
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_GetChildNode_str]') and xtype in (N'FN', N'IF', N'TF'))
drop function Func_GetChildNode_str
 
Create function [dbo].[Func_GetChildNode_str](@ID varchar(100))   
returns varchar(8000)
as  
begin  
  declare @tempTable table(ID varchar(100),Name varchar(100),ParentID varchar(100),Level int)   
  declare @resultStr varchar(8000)
  declare @i int  
  set @i = 1   
  set @resultStr=''
  
  insert into @tempTable select De_Guid,De_Name,De_ParentGuid,@i from AIC_Base_Depart where De_Guid = @ID
  insert into @tempTable select De_Guid,De_Name,De_ParentGuid,@i from AIC_Base_Depart where De_ParentGuid = @ID   

  while @@rowcount<>0   
  begin  
      set @i = @i + 1   
      insert into @tempTable   
      select a.De_Guid,a.De_Name,a.De_ParentGuid,@i   
      from AIC_Base_Depart a,@tempTable b   
      where a.De_ParentGuid=b.ID and b.Level = @i-1   
  end  

  select @resultStr=@resultStr+Name+',' from @tempTable 
 
  return (@resultStr) 
end  
 
select dbo.Func_GetChildNode_str ('{D9383D88-3031-4E0F-9674-C9FC636BBD07}')

*/

go


--OLE方式取得SQL的值
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_GetSQLValue]') and xtype in (N'FN', N'IF', N'TF'))
drop function Func_GetSQLValue
go
create function Func_GetSQLValue (
 @aSQL varchar(8000)
 )
RETURNS varchar(1000)
AS
BEGIN
  declare @tempStr2 varchar(1000)
  declare @obj int,@con Nvarchar(1000) 
  set @con=N'Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;Initial Catalog='+replace(db_name(),N'''',N'''''')
  exec sp_oacreate 'adodb.recordset',@obj out
  exec sp_oamethod @obj,'open',null,@aSQL,@con
  exec sp_oagetproperty @obj,N'fields(0).value',@tempStr2 output
  exec sp_oadestroy @obj

  RETURN(@tempStr2) 
end
go
--OLE方式执行SQL
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_ExecSQL]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) 
drop procedure [dbo].[Proc_ExecSQL] 
GO 
create proc Proc_ExecSQL
 @aSQL varchar(8000),
 @aDatabaseName varchar(100)
as 
  declare @tempStr2 varchar(1000)
  if @aDatabaseName=''
    set @aDatabaseName=db_name()   

  declare @obj int,@con Nvarchar(1000) 
  set @con=N'Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;Initial Catalog='+replace(@aDatabaseName,N'''',N'''''')
  exec sp_oacreate 'adodb.recordset',@obj out
  exec sp_oamethod @obj,'open',null,@aSQL,@con
  exec sp_oagetproperty @obj,N'fields(0).value',@tempStr2 output
  exec sp_oadestroy @obj
go

--取得表中指定字段的值
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_GetTableFieldValue]') and xtype in (N'FN', N'IF', N'TF'))
drop function Func_GetTableFieldValue
go
create function Func_GetTableFieldValue (
 @aTableName varchar(100),
 @aWhere varchar(1000),
 @ResturnFieldNames varchar(1000) )
RETURNS varchar(1000)
AS
BEGIN
  declare @tempStr1 varchar(1000)
  declare @tempStr2 varchar(1000)

  if @aWhere<>'' 
  set @aWhere=' where '+@aWhere


  set @ResturnFieldNames=REPLACE(@ResturnFieldNames,',','+')
  set @ResturnFieldNames=REPLACE(@ResturnFieldNames,';','+')

  set @tempstr1=' declare @tempvalue1 varchar(1000) '+
    ' select @tempvalue1='+@ResturnFieldNames+' from '+@aTableName+@aWhere+
    ' select @tempvalue1 '
  set @tempStr2=dbo.Func_GetSQLValue(@tempstr1)

  RETURN(@tempStr2)
end
go

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_GetSQLValue]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) 
drop procedure [dbo].[Proc_GetSQLValue] 
GO 
create proc Proc_GetSQLValue
	@SQlText Nvarchar(4000),
  @ResultStr  varchar(8000) output
as 
  declare  @SelectFlag  varchar(100)
  declare  @SelectIndex int

  set  @SelectFlag ='SELECT '
  set  @SQlText    =upper(@SQlText)
  set  @SelectIndex=charindex(@SelectFlag,@SQlText)
  set  @SQlText= 
         substring(@SQlText,1,@SelectIndex+len(@SelectFlag))+' @result= '+
         substring(@SQlText,@SelectIndex+len(@SelectFlag)+1,len(@SQlText))
   
 	EXEC  sp_executesql  @SQlText,N'@result varchar(8000)  OUTPUT ' ,@ResultStr  OUTPUT 
go
/*
declare @tempStr1 varchar(800)
exec Proc_GetSQLValue 'select count(*) from absys_org_field',@tempStr1 output
print @tempStr1
*/  
go

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_Addlinkedserver]') and OBJECTPROPERTY(id,N'IsProcedure') = 1)
drop procedure [dbo].[Proc_Addlinkedserver]
go
--@aType='SQLSERVER' or 'Oracle'
CREATE  PROCEDURE  Proc_Addlinkedserver(
  @aType Nvarchar(100),@aLinkName  Nvarchar(100),
  @aSource Nvarchar(100),@aUserName Nvarchar(100),@aPassWord Nvarchar(100),
  @aProduct_name Nvarchar(100)
   )
AS
BEGIN 
	--两个数据库相互操作的模板_生成连接
	set nocount on
	exec Proc_SetPublicVar_str1 'UpdateDatabaseState','Begin'
	
  if @aLinkName<>'' 
  begin
		declare @tempint int
		
		declare @product_name nvarchar(100)
		declare @provider_name nvarchar(100)
		declare @Source nvarchar(100)
		declare @OS_Authentication nvarchar(100)
		declare @UserName nvarchar(100)
		declare @PassWord nvarchar(100)
			
	/*
		set @product_name     =''              --SQL Server=SQL SERVER; Oracle=Oracle
		set @provider_name    ='SQLOLEDB'      --SQLOLEDB=SQL SERVER; MSDAORA=Oracle
		set @Source           ='.\sql2005'     --SQL SERVER时表示数据机器的IP或注册的别名也可是本机的.;Oracle时用于Oracle数据库的 SQL*Net 别名
		set @OS_Authentication='False'         --SQL SERVER时True 时表示windows身份认证;False 时表示SQL SERVER身份认证;Oracle时固定为False
		set @UserName         =''              --登录的用户名  
		set @PassWord         ='asd123'        --登录的用密码
		if @UserName='' 
		  set @OS_Authentication='True'
		*/
	
		if @aType='SQLSERVER'
		begin
			set @product_name     =''
			if @aProduct_name=''
			  set @provider_name    ='SQLOLEDB'
			else 
			  set @provider_name    =@aProduct_name
			set @Source               =@aSource
			set @OS_Authentication='False'
			set @UserName         =@aUserName
			set @PassWord         =@aPassWord
			if @UserName=''
			  set @OS_Authentication='True'
		end
		else if @aType='Oracle'
		begin
			set @product_name     ='Oracle'
			if @aProduct_name=''
			  set @provider_name    ='MSDAORA'
			else 
			  set @provider_name    =@aProduct_name
			set @Source               =@aSource
			set @OS_Authentication='False'
			set @UserName         =@aUserName
			set @PassWord         =@aPassWord
		end
		
		--创建链接服务器 
		if   exists(select   *   from   master..sysservers   where   srvname=@aLinkName)
		begin
		  exec @tempint=sp_dropserver @aLinkName,'droplogins'
		  IF (@tempint <> 0) goto QuitWithError
		end
		
		exec @tempint=sp_addlinkedserver   @aLinkName, @product_name, @provider_name, @Source
		IF (@tempint <> 0) goto QuitWithError
		exec @tempint=sp_addlinkedsrvlogin @aLinkName, @OS_Authentication,null, @UserName, @PassWord 
		IF (@tempint <> 0) goto QuitWithError	
		GOTO EndSave 
		
		--异常
		QuitWithError: 
		  exec Proc_SetPublicVar_str1 'UpdateDatabaseState','Error'
		  goto AllEnd  
	
		--当所有操作正常结束时
		EndSave: 
		  exec Proc_SetPublicVar_str1 'UpdateDatabaseState','End'
		
		AllEnd: 

  end
	set nocount oFF
end;
go
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_Droplinkedserver]') and OBJECTPROPERTY(id,N'IsProcedure') = 1)
drop procedure [dbo].[Proc_Droplinkedserver]
go
CREATE  PROCEDURE  Proc_Droplinkedserver(@aLinkName  Nvarchar(100))
AS
BEGIN 
	--两个数据库相互操作的模板_生成连接
	set nocount on
	exec Proc_SetPublicVar_str1 'UpdateDatabaseState','Begin'
  if @aLinkName<>'' 
  begin
		
		declare @tempint int
		
		exec @tempint=sp_dropserver @aLinkName,'droplogins'
		IF (@tempint <> 0) goto QuitWithError
	
		GOTO EndSave 
		
		--异常
		QuitWithError: 
		  exec Proc_SetPublicVar_str1 'UpdateDatabaseState','Error'
		  goto AllEnd  
	
		--当所有操作正常结束时
		EndSave: 
		  exec Proc_SetPublicVar_str1 'UpdateDatabaseState','End'
		
		AllEnd: 
	end
	set nocount oFF
end 
go


/*
--SQLServer
exec Proc_Addlinkedserver 'SQLSERVER','New_Link','.','',''
--下行的go 不加的话会出错 “在 sysservers 中未能找到服务器 'Old_Link'。请执行 sp_addlinkedserver 以将服务器添加到 sysservers。”,
--原因可能是解析SQL时发现没有定义_Link而报错
--go 
select * from  New_Link.ABFramework.dbo.AIC_Base_people  
exec Proc_Droplinkedserver 'New_Link'

--Oracle
exec Proc_Addlinkedserver 'Oracle','Old_Link','oracsvw','ykt1','ykt1'
--下行的go 不加的话会出错 “在 sysservers 中未能找到服务器 'Old_Link'。请执行 sp_addlinkedserver 以将服务器添加到 sysservers。”,
--原因可能是解析SQL时发现没有定义_Link而报错
--go 
select * from  Old_Link..YKT1.AIC_TM_PRICELIST  
exec Proc_Droplinkedserver 'Old_Link'
*/
--取表在两个数据库中的导入SQL 
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_GetMoveTableDataSQL]') and OBJECTPROPERTY(id,N'IsProcedure') = 1)
drop procedure [dbo].[Proc_GetMoveTableDataSQL]
go
CREATE  PROCEDURE  Proc_GetMoveTableDataSQL(@OldFlagName  varchar (1000),
	                                          @NameFlagName  varchar (1000),
	                                          @TableName  varchar (1000),
	                                          @DeleteOldData bit=1 )
as 
begin 
  declare @tempMaxFi_Order int
	select @tempMaxFi_Order=max(Fi_Order)
	from ABSys_Org_Table left join ABSys_Org_Field on ta_guid=fi_ta_guid
	where @TableName=ta_name


  select Fi_Name
  from (
  select -9 Ta_Order,-9 Fi_Order,'delete '+@NameFlagName+@TableName Fi_Name where @DeleteOldData=1
  union
  select -8 Ta_Order,-8 Fi_Order,'go' Fi_Name where @DeleteOldData=1
  union
  select -7 Ta_Order,-7 Fi_Order,'insert into '+@NameFlagName+@TableName+'(' Fi_Name 
  union
	select Ta_Order,Fi_Order,case when @tempMaxFi_Order=Fi_Order then Fi_Name else Fi_Name+',' end
	from ABSys_Org_Table left join ABSys_Org_Field on ta_guid=fi_ta_guid
	where @TableName=ta_name
  union
  select 10000 Ta_Order,10000 Fi_Order,') ' Fi_Name 


  union
  select 10001 Ta_Order,10001 Fi_Order,'select  ' Fi_Name 
  union
	select 10001+Ta_Order,10001+Fi_Order,case when @tempMaxFi_Order=Fi_Order then Fi_Name else Fi_Name+',' end
	from ABSys_Org_Table left join ABSys_Org_Field on ta_guid=fi_ta_guid
	where @TableName=ta_name
  union
  select 20000 Ta_Order,20000 Fi_Order,'from  '+@OldFlagName+@TableName Fi_Name 
  ) aa
  order by Ta_Order,Fi_Order 
end

go



--取得年份的列表 @aLowNumber,@aUppNumber 最大值为20
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_GetDownYear]') and xtype in (N'FN', N'IF', N'TF'))
drop function Func_GetDownYear
go
create function Func_GetDownYear(@aLowNumber int,@aUppNumber int,@aCurYear int)
RETURNS table
AS
 
  return(
       select tempyear
       from (
              select  @aCurYear-20 tempyear,-20 tempint
        union select  @aCurYear-19 tempyear,-19 tempint
        union select  @aCurYear-18 tempyear,-18 tempint
        union select  @aCurYear-17 tempyear,-17 tempint
        union select  @aCurYear-16 tempyear,-16 tempint
        union select  @aCurYear-15 tempyear,-15 tempint
        union select  @aCurYear-14 tempyear,-14 tempint
        union select  @aCurYear-13 tempyear,-13 tempint
        union select  @aCurYear-12 tempyear,-12 tempint
        union select  @aCurYear-11 tempyear,-11 tempint
        union select  @aCurYear-10 tempyear,-10 tempint
        union select  @aCurYear-9 tempyear,-9 tempint
        union select  @aCurYear-8 tempyear,-8 tempint
        union select  @aCurYear-7 tempyear,-7 tempint
        union select  @aCurYear-6 tempyear,-6 tempint
        union select  @aCurYear-5 tempyear,-5 tempint
        union select  @aCurYear-4 tempyear,-4 tempint
        union select  @aCurYear-3 tempyear,-3 tempint
        union select  @aCurYear-2 tempyear,-2 tempint
        union select  @aCurYear-1 tempyear,-1 tempint
        union select  @aCurYear   tempyear,0 tempint
        union select  @aCurYear+1  tempyear,1 tempint
        union select  @aCurYear+2  tempyear,2 tempint
        union select  @aCurYear+3  tempyear,3 tempint
        union select  @aCurYear+4  tempyear,4 tempint
        union select  @aCurYear+5  tempyear,5 tempint
        union select  @aCurYear+6  tempyear,6 tempint
        union select  @aCurYear+7  tempyear,7 tempint
        union select  @aCurYear+8  tempyear,8 tempint
        union select  @aCurYear+9  tempyear,9 tempint
        union select  @aCurYear+10 tempyear,10 tempint
        union select  @aCurYear+11 tempyear,11 tempint
        union select  @aCurYear+12 tempyear,12 tempint
        union select  @aCurYear+13 tempyear,13 tempint
        union select  @aCurYear+14 tempyear,14 tempint
        union select  @aCurYear+15 tempyear,15 tempint
        union select  @aCurYear+16 tempyear,16 tempint
        union select  @aCurYear+17 tempyear,17 tempint
        union select  @aCurYear+18 tempyear,18 tempint
        union select  @aCurYear+19 tempyear,19 tempint
        union select  @aCurYear+20 tempyear,20 tempint
      ) aa
      where tempint<0 and tempint*-1 <= @aLowNumber or tempint>0 and tempint<=@aUppNumber
         )
go
--select * from dbo.Func_GetDownYear(2,10,2010)

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_GetDownMonth]') and xtype in (N'FN', N'IF', N'TF'))
drop function Func_GetDownMonth
go
create function Func_GetDownMonth()
RETURNS table
AS
 
  return(
       select tempMonth
       from (
              select  1  tempMonth
        union select  2  tempMonth
        union select  3  tempMonth
        union select  4  tempMonth
        union select  5  tempMonth
        union select  6  tempMonth
        union select  7  tempMonth
        union select  8  tempMonth
        union select  9  tempMonth
        union select  10 tempMonth
        union select  11 tempMonth
        union select  12 tempMonth
      ) aa
         )
go
--select * from dbo.Func_GetDownMonth(30)
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_GetDownDay]') and xtype in (N'FN', N'IF', N'TF'))
drop function Func_GetDownDay
go
create function Func_GetDownDay(@aMaxDay int=31)
RETURNS table
AS
 
  return(
       select tempDay
       from (
              select  1  tempDay
        union select  2  tempDay
        union select  3  tempDay
        union select  4  tempDay
        union select  5  tempDay
        union select  6  tempDay
        union select  7  tempDay
        union select  8  tempDay
        union select  9  tempDay
        union select  10 tempDay
        union select  11 tempDay
        union select  12 tempDay
        union select  13 tempDay
        union select  14 tempDay
        union select  15 tempDay
        union select  16 tempDay
        union select  17 tempDay
        union select  18 tempDay
        union select  19 tempDay
        union select  20 tempDay
        union select  21 tempDay
        union select  22 tempDay
        union select  23 tempDay
        union select  24 tempDay
        union select  25 tempDay
        union select  26 tempDay
        union select  27 tempDay
        union select  28 tempDay
        union select  29 tempDay
        union select  30 tempDay
        union select  31 tempDay
      ) aa
      where tempDay<=@aMaxDay
         )
go
--select * from dbo.Func_GetDownDay(30)

--取得字符序列
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_GetNumberStrList]') and xtype in (N'FN', N'IF', N'TF'))
drop function Func_GetNumberStrList
go
create function Func_GetNumberStrList(@aBeginNumber int,@aEndNumber int,@aStr nvarchar(1000),@aSpace nvarchar(100),@aRes1 bit)
RETURNS nvarchar(1000)
AS
BEGIN 
  declare @tempResStr nvarchar(1000)
  set @tempResStr=''
  declare @tempAddStr nvarchar(100)
  set @tempAddStr=''

	declare @i int
  set @i=@aBeginNumber
  while @i<= @aEndNumber
  begin 
    if charindex(@aSpace+cast(@i as nvarchar)+@aSpace,@aSpace+@aStr+@aSpace)>0
    begin
      if @aRes1=1
        set @tempAddStr='1'
      else 
        set @tempAddStr=cast(@i as nvarchar)
    end
    else 
      set @tempAddStr='0'

    if @tempResStr=''
      set @tempResStr=@tempAddStr
    else 
      set @tempResStr=@tempResStr+@aSpace+@tempAddStr


    set @i=@i+1
  end

  RETURN(@tempResStr)
end
go
--select  dbo.Func_GetNumberStrList(1,16,'1,16',',',1)


--框架的解密的过程， 密码不支持中文
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_DoPassword]') and xtype in (N'FN', N'IF', N'TF'))
drop function Func_DoPassword
go
create function Func_DoPassword(@aPassWord Nvarchar(4000),@aRandInt int, @aKey Nvarchar(4000)='ABSoft',@aPassCharIntLength int =6)
RETURNS nvarchar(4000)
AS
BEGIN 
	declare @OutPassWord Nvarchar(4000)

  set @OutPassWord=@aPassWord
  if @aPassWord <> ''  
  begin
	  set @OutPassWord =''; 
		declare @i int
		declare @j int
 
    if @aRandInt<0
      set @aRandInt=0
    if @aRandInt<128
      set @aRandInt=@aRandInt+128

		set @aRandInt=@aRandInt % 128
		set @i=1

	  while @i<= len(@aPassWord) 
	  begin
	    if @i > len(@aKey)  
	      set @j = len(@aKey)
	    else
	      set @j = @i;

	    set @OutPassWord  = @OutPassWord + dbo.Func_NumberMask(ASCII(substring(@aPassWord,@i,1)) + @aRandInt + @i +
	      ASCII(substring(@aKey,@j,1)), @aPassCharIntLength,'0');
      set @i=@i+1
	  end;
	  set @OutPassWord  = @OutPassWord + dbo.Func_NumberMask(@aRandInt,@aPassCharIntLength,'0');

  end;

  RETURN(@OutPassWord); 
end
go

/*

print RAND( DATEPART(mm, GETDATE()) * 100000 + 
                                             DATEPART(ss, GETDATE()) * 1000+ 
                                             DATEPART(ms, GETDATE()) 
                                            )*1000000
print dbo.Func_DoPassword('MasterKey',RAND( DATEPART(mm, GETDATE()) * 100000 + 
                                             DATEPART(ss, GETDATE()) * 1000+ 
                                             DATEPART(ms, GETDATE()) 
                                            )*1000000,'absoft',6)

*/


--框架的解密的过程， 密码不支持中文
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_UnDoPassword]') and xtype in (N'FN', N'IF', N'TF'))
drop function Func_UnDoPassword
go
create function Func_UnDoPassword(@aPassWord Nvarchar(4000),@aKey Nvarchar(4000)='ABSoft',@aPassCharIntLength int =6)
RETURNS nvarchar(4000)
AS
BEGIN 
	declare @OutPassWord Nvarchar(4000)

  set @OutPassWord=@aPassWord
  if @aPassWord <> ''  
  begin
	  set @OutPassWord ='';

		declare @tempint bigint
		declare @i int
		declare @j int
	  set @tempint  = substring(@aPassWord, Len(@aPassWord) - @aPassCharIntLength +1, @aPassCharIntLength)
	  set @aPassWord  = substring(@aPassWord, 1, Len(@aPassWord) - @aPassCharIntLength)

	  set @i=1
	  while @i <= cast(len(@aPassWord) / @aPassCharIntLength as int) 
	  begin
	    if @i > len(@aKey)  
	      set @j = len(@aKey)
	    else
	      set @j = @i;
	

	    set @OutPassWord  = @OutPassWord + Char(cast(substring(@aPassWord, (@i - 1) * @aPassCharIntLength
	      + 1, @aPassCharIntLength) as int) - @tempInt - @i - ASCII(substring(@aKey,@j,1)));
      set @i=@i+1
	  end;
  end;
  RETURN(@OutPassWord); 

end

go

/*
print dbo.Func_UnDoPassword('000206000228000264000262000239000267000229000256000277000031','absoft',6)

*/



--比较两个数据库的表结构差异   
if   exists   (select   *   from   dbo.sysobjects   where   id   =   object_id(N'[dbo].[Proc_CompareStructure]')   and   OBJECTPROPERTY(id,   N'IsProcedure')   =   1)   
  drop   procedure   [dbo].[Proc_CompareStructure]   
GO   
create   proc   Proc_CompareStructure   
@dbname_Old   varchar(250),   --要比较的数据库名1   
@dbname_New   varchar(250)   --要比较的数据库名2   
as   
begin
	if exists (select * from tempdb.dbo.sysobjects where id = object_id(N'tempdb.[dbo].[##Proc_Comparestructure_OldTable]'))
  	drop table ##Proc_Comparestructure_OldTable
	if exists (select * from tempdb.dbo.sysobjects where id = object_id(N'tempdb.[dbo].[##Proc_Comparestructure_NewTable]'))
  	drop table ##Proc_Comparestructure_NewTable

	if exists (select * from tempdb.dbo.sysobjects where id = object_id(N'tempdb.[dbo].[##Proc_Comparestructure_OldTable_SysProperties]'))
  	drop table ##Proc_Comparestructure_OldTable_SysProperties
	if exists (select * from tempdb.dbo.sysobjects where id = object_id(N'tempdb.[dbo].[##Proc_Comparestructure_NewTable_SysProperties]'))
  	drop table ##Proc_Comparestructure_NewTable_SysProperties


  if (select cast(substring(cast(SERVERPROPERTY('productversion') as varchar),1,charindex('.',cast(SERVERPROPERTY('productversion') as varchar))-1) as int))>8
  begin
    exec('
    select '''+@dbname_Old+''' DatabaseName,major_id ID,Minor_ID smallid,cast(value as nvarchar(1000)) value
    into ##Proc_Comparestructure_OldTable_SysProperties
		from '+@dbname_Old+'.sys.extended_properties
    where name=''MS_Description'' and isnull(value,'''')<>''''
         ')
    exec('
    select '''+@dbname_New+''' DatabaseName,major_id ID,Minor_ID smallid,cast(value as nvarchar(1000)) value
    into ##Proc_Comparestructure_NewTable_SysProperties
		from '+@dbname_New+'.sys.extended_properties
    where name=''MS_Description'' and isnull(value,'''')<>''''
         ')
  end
  else 
  begin
    exec('
    select '''+@dbname_Old+''' DatabaseName,ID,smallid,cast(value as nvarchar(1000))  value
    into ##Proc_Comparestructure_OldTable_SysProperties
		from '+@dbname_Old+'.dbo.sysproperties
    where name=''MS_Description''and isnull(value,'''')<>''''
         ')
    exec('
    select '''+@dbname_New+''' DatabaseName,ID,smallid,cast(value as nvarchar(1000))  value
    into ##Proc_Comparestructure_NewTable_SysProperties
		from '+@dbname_New+'.dbo.sysproperties
    where name=''MS_Description''and isnull(value,'''')<>''''
         ')
  end  


	--得到数据库1的结构   
	exec(    'select '''+@dbname_Old+''' DatabaseName,
						 d.name TableName,
             d.ID TableID,
						 a.colorder ID,
						 a.name Name,  
						 (case when (select '+@dbname_Old+'.dbo.Func_GetCOLUMNPROPERTY(d.name,a.name,''IsIdentity''))=1 then ''True''else ''False'' end) IsAutoAdd,
						 (case when (SELECT count(*)
						 FROM '+@dbname_Old+'.dbo.sysobjects
						 WHERE (name in
											 (SELECT name
											FROM '+@dbname_Old+'.dbo.sysindexes
											WHERE (id = a.id) AND (indid in
																(SELECT indid
															 FROM '+@dbname_Old+'.dbo.sysindexkeys
															 WHERE (id = a.id) AND (colid in
																				 (SELECT colid
																				FROM '+@dbname_Old+'.dbo.syscolumns
																				WHERE (id = a.id) AND (name = a.name))))))) AND
										(xtype = ''PK''))>0 then ''True'' else ''False'' end) IsKey,
						 b.name Type,
						 a.length Byte,
             (select '+@dbname_Old+'.dbo.Func_GetCOLUMNPROPERTY(d.name,a.name,''PRECISION'')) as Length,
						 isnull((select '+@dbname_Old+'.dbo.Func_GetCOLUMNPROPERTY(d.name,a.name,''Scale'')) ,0) as EecimalDigits,
						 (case when a.isnullable=1 then ''True''else ''False'' end) CanNull,
						 isnull(e.text,'''') DefaultValue,
				     isnull(cast(g.[value] as varchar(100)),'''') AS Caption,
		         a.iscomputed
		  into  ##Proc_Comparestructure_OldTable  
		  FROM  '+@dbname_Old+'.dbo.syscolumns  a left join '+@dbname_Old+'.dbo.systypes b
			on  a.xtype=b.xusertype
			inner join '+@dbname_Old+'.dbo.sysobjects d
			on a.id=d.id  and  d.xtype in (''U'',''V'') and  dbo.Func_InExcludeTableName(d.name)=0
			left join '+@dbname_Old+'.dbo.syscomments e
			on a.cdefault=e.id
	    left join ##Proc_Comparestructure_OldTable_SysProperties g on '''+@dbname_Old+'''=g.DatabaseName and a.id=g.id AND a.colid = g.smallid   
		  order by  d.name,a.colorder ' 
          )   

   
	--得到数据库2的结构   
	exec(    'select '''+@dbname_New+''' DatabaseName,
						 d.name TableName,
             d.ID TableID,
						 a.colorder ID,
						 a.name Name,
						 (case when (select '+@dbname_New+'.dbo.Func_GetCOLUMNPROPERTY(d.name,a.name,''IsIdentity''))=1 then ''True''else ''False'' end) IsAutoAdd,
						 (case when (SELECT count(*)
						 FROM '+@dbname_New+'.dbo.sysobjects
						 WHERE (name in
											 (SELECT name
											FROM '+@dbname_New+'.dbo.sysindexes
											WHERE (id = a.id) AND (indid in
																(SELECT indid
															 FROM '+@dbname_New+'.dbo.sysindexkeys
															 WHERE (id = a.id) AND (colid in
																				 (SELECT colid
																				FROM '+@dbname_New+'.dbo.syscolumns
																				WHERE (id = a.id) AND (name = a.name))))))) AND
										(xtype = ''PK''))>0 then ''True'' else ''False'' end) IsKey,
						 b.name Type,
						 a.length Byte,
             (select '+@dbname_New+'.dbo.Func_GetCOLUMNPROPERTY(d.name,a.name,''PRECISION'')) as Length,
						 isnull((select '+@dbname_New+'.dbo.Func_GetCOLUMNPROPERTY(d.name,a.name,''Scale''))  ,0) as EecimalDigits,
						 (case when a.isnullable=1 then ''True''else ''False'' end) CanNull,
						 isnull(e.text,'''') DefaultValue,
				     isnull(cast(g.[value] as varchar(100)),'''') AS Caption,
		         a.iscomputed
		  into  ##Proc_Comparestructure_NewTable  
		  FROM  '+@dbname_New+'.dbo.syscolumns  a left join '+@dbname_New+'.dbo.systypes b
			on  a.xtype=b.xusertype
			inner join '+@dbname_New+'.dbo.sysobjects d
			on a.id=d.id  and  d.xtype in (''U'',''V'') and  dbo.Func_InExcludeTableName(d.name)=0
			left join '+@dbname_New+'.dbo.syscomments e
			on a.cdefault=e.id
	    left join ##Proc_Comparestructure_NewTable_SysProperties g on '''+@dbname_New+'''=g.DatabaseName and a.id=g.id AND a.colid = g.smallid   
		  order by  d.name,a.colorder ' 
          )   


  select a.* from 
  (
	select   比较结果=
   case when   old.tablename is null  and new.id=1 and
              (not exists(select * from ##Proc_Comparestructure_OldTable a where a.tablename=New.tablename)) and 
              (exists(select * from ##Proc_Comparestructure_NewTable a where a.tablename=New.tablename))    then   @dbname_Old+' 缺少表：'+new.tablename   
				when   old.tablename is null  and 
              (exists(select * from ##Proc_Comparestructure_OldTable a where a.tablename=New.tablename)) and 
              (not exists(select * from ##Proc_Comparestructure_OldTable a where a.tablename=New.tablename and a.Name=New.Name)) and 
              (exists(select * from ##Proc_Comparestructure_NewTable a where a.tablename=New.tablename and a.Name=New.Name))    then   @dbname_Old+' ['+new.tablename+']   缺少字段：'+new.Name    

        when   new.tablename is null    and old.id=1 and 
              (not exists(select * from ##Proc_Comparestructure_newTable a where a.tablename=Old.tablename)) and 
              (exists(select * from ##Proc_Comparestructure_OldTable a where a.tablename=Old.tablename))    then   @dbname_new+' 缺少表：'+Old.tablename   
				when   new.tablename is null  and 
              (exists(select * from ##Proc_Comparestructure_newTable a where a.tablename=Old.tablename)) and 
              (not exists(select * from ##Proc_Comparestructure_newTable a where a.tablename=Old.tablename and a.Name=Old.Name)) and 
              (exists(select * from ##Proc_Comparestructure_OldTable a where a.tablename=Old.tablename and a.Name=Old.Name))    then   @dbname_new+' ['+Old.tablename+']   缺少字段：'+Old.Name    


				when   old.IsAutoAdd<>new.IsAutoAdd   then   '标识不同'   
				when   old.IsKey<>new.IsKey   then   '主键设置不同'   
				when   old.Type<>new.Type   then   '字段类型不同'   
				when   old.Byte<>new.Byte   then   '占用字节数'   
				when   old.Length>new.Length   then   '长度不同'   
				when   old.EecimalDigits<>new.EecimalDigits   then   '小数位数不同'   
				when   old.CanNull<>new.CanNull   then   '是否允许空不同'   
				when   old.DefaultValue<>new.DefaultValue   then   '默认值不同'   
				when   old.Caption<>new.Caption   then   '字段说明不同'   
		else   ''   end,
		old.DatabaseName oldDatabaseName,
		New.DatabaseName NewDatabaseName,
		old.TableName oldTableName,
		New.TableName NewTableName,
		old.Name oldName,
		New.Name NewName,
		old.IsAutoAdd oldIsAutoAdd,
		New.IsAutoAdd NewIsAutoAdd,
		old.IsKey oldIsKey,
		New.IsKey NewIsKey,
		old.Type oldType,
		New.Type NewType,
		old.Byte oldByte,
		New.Byte NewByte,
		old.Length oldLength,
		New.Length NewLength,
		old.EecimalDigits oldEecimalDigits,
		New.EecimalDigits NewEecimalDigits,
		old.CanNull oldCanNull,
		New.CanNull NewCanNull,
		old.DefaultValue oldDefaultValue,
		New.DefaultValue NewDefaultValue,
		old.Caption oldCaption,
		New.Caption NewCaption
		from  ##Proc_Comparestructure_OldTable old full join ##Proc_Comparestructure_NewTable new on   old.tablename=new.tablename   and   old.Name=new.Name   
    ) a
    where 比较结果<>''
    order by 比较结果
end
go   

--exec   Proc_CompareStructure   'aaaa','ABFramework'   
 
go
--在数据库结构一样时比较所有表的数据行
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_CompareData]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) 
drop procedure [dbo].[Proc_CompareData] 
GO 
create proc Proc_CompareData 
@dbname_Old   varchar(250),   --要比较的数据库名1   
@dbname_New   varchar(250)   --要比较的数据库名2   
as 
	declare @tempSQL varchar(8000)
	set @tempSQL=''
	 
	if exists (select * from tempdb.dbo.sysobjects where id = object_id(N'tempdb.[dbo].[##tempResultList]'))
	  drop table ##tempResultList
	CREATE TABLE ##tempResultList(Name1 nvarchar(100) ,Name2 nvarchar(100) ,Name3 nvarchar(100) ,Name4 nvarchar(100) ,Name5 nvarchar(100) ,Name6 nvarchar(100))
	
	declare tempCursor cursor for 
		select 'select '''+Name+''' 表名,'+
		       '(select count(*) from '+@dbname_Old+'.dbo.'+Name+') '+@dbname_Old+'库行数,'+
		       '(select count(*) from '+@dbname_New+'.dbo.'+Name+')  '+@dbname_New+'库行数,'+
		       '(select checksum_agg(binary_checksum(*)) from '+@dbname_Old+'.dbo.'+Name+') '+@dbname_Old+'库校验值,'+
		       '(select checksum_agg(binary_checksum(*)) from '+@dbname_New+'.dbo.'+Name+')  '+@dbname_New+'库校验值 '	
	 from  sysobjects 
	 where xtype in ('U','V')  
	 
	open tempCursor 
	fetch next from tempCursor into @tempSQL
	while @@fetch_status = 0
	begin
	  
	  exec('insert into ##tempResultList(Name1,Name2,Name3,Name4,Name5)'+@tempSQL)
	
		fetch next from tempCursor into  @tempSQL
	end
	close tempCursor
	deallocate tempCursor
	 
	 
	select Name1 表名 ,
         Name2 当前库行数 ,
         Name3 检测库行数 ,
         Name4 当前库校验值 ,
         Name5 检测库校验值 ,
	       ' select binary_checksum(*) 行校验值,* from '+@dbname_Old+'.dbo.'+Name1+' where binary_checksum(*) not in ( select binary_checksum(*) from '+@dbname_New+'.dbo.'+Name1+') ' old多出的行, 
	       ' select binary_checksum(*) 行校验值,* from '+@dbname_New+'.dbo.'+Name1+' where binary_checksum(*) not in ( select binary_checksum(*) from '+@dbname_Old+'.dbo.'+Name1+') ' New多出的行
	         
	from ##tempResultList
	where Name2<>Name3 or Name4<>Name5
go	
	 

--exec   Proc_CompareData   'aaaa','ABFramework'   

--导出到EXCEL
--   EXEC master..xp_cmdshell 'del c:\aa.xls'
--   Proc_ExportExcel   @sqlstr='select   *   from  tempsql.dbo.table1',@path='c:\',@fname='aa1.xls' 
if   exists   (select   *   from   dbo.sysobjects   where   id   =   object_id(N'[dbo].[Proc_ExportExcel]')   and   OBJECTPROPERTY(id,   N'IsProcedure')   =   1)   
drop   procedure   [dbo].[Proc_ExportExcel]   
GO   
create proc Proc_ExportExcel
@sqlstr sysname, --查询语句,如果查询语句中使用了order by ,请加上top 100 percent
@path nvarchar(1000), --文件存放目录
@fname nvarchar(250), --文件名
@sheetname varchar(250)='' --要创建的工作表名,默认为文件名
as 
begin
	declare @err int,@src nvarchar(255),@desc nvarchar(255),@out int
	declare @obj int,@constr nvarchar(1000),@sql varchar(8000),@fdlist varchar(8000)

	declare @tempsql  nvarchar(1000)
	--参数检测
	if isnull(@fname,'')='' set @fname='temp.xls'
	if isnull(@sheetname,'')='' set @sheetname=replace(@fname,'.','#')

	--检查文件是否已存在
	if right(@path,1)<>'＼' set @path=@path+'＼'
	create table #tb(a bit,b bit,c bit)
	set @sql=@path+@fname
	insert into #tb exec master..xp_fileexist @sql



	--数据库创建语句
	set @sql=@path+@fname
	if exists(select 1 from #tb where a=1)
	set @constr='DRIVER={Microsoft Excel Driver (*.xls)};DSN='''';READONLY=FALSE'
			+';CREATE_DB="'+@sql+'";DBQ='+@sql
	else
	set @constr='Provider=Microsoft.Jet.OLEDB.4.0;Extended Properties="Excel 5.0;HDR=YES'
	+';DATABASE='+@sql+'"'

	--连接数据库
	exec @err=sp_oacreate 'adodb.connection',@obj out
	if @err<>0 goto lberr

	exec @err=sp_oamethod @obj,'open',null,@constr
	if @err<>0 goto lberr

	--创建表的SQL
	declare @tbname sysname
	set @tbname='##tmp_'+convert(varchar(38),dbo.Func_GetOrderGuid())
	set @sql='select * into ['+@tbname+'] from('+@sqlstr+') a'
	exec(@sql)

	select @sql='',@fdlist=''
	select @fdlist=@fdlist+','+a.name
	,@sql=@sql+',['+a.name+'] '
	+case when b.name in('char','nchar','varchar','nvarchar') then
	'text('+cast(case when a.length>255 then 255 else a.length end as varchar)+')'
	when b.name in('tynyint','int','bigint','tinyint') then 'int'
	when b.name in('smalldatetime','datetime') then 'datetime'
	when b.name in('money','smallmoney') then 'money'
	else b.name end
	FROM tempdb..syscolumns a left join tempdb..systypes b on a.xtype=b.xusertype
	where b.name not in('image','text','uniqueidentifier','sql_variant','ntext','varbinary','binary','timestamp')
	and a.id=(select id from tempdb..sysobjects where name=@tbname)
	select @sql='create table ['+@sheetname
	+']('+substring(@sql,2,8000)+')'
	,@fdlist=substring(@fdlist,2,8000)

	exec @err=sp_oamethod @obj,'execute',@out out,@sql
	if @err<>0 goto lberr

	exec @err=sp_oadestroy @obj

	--导入数据
	set @sql='openrowset(''MICROSOFT.JET.OLEDB.4.0'',''Excel 5.0;HDR=YES
	;DATABASE='+@path+@fname+''',['+@sheetname+'$])'

	exec('insert into '+@sql+'('+@fdlist+') select '+@fdlist+' from ['+@tbname+']')

	set @sql='drop table ['+@tbname+']'
	exec(@sql)
	return

	lberr:
	exec sp_oageterrorinfo 0,@src out,@desc out
	lbexit:
	select cast(@err as varbinary(4)) as 错误号
	,@src as 错误源,@desc as 错误描述
	select @sql,@constr,@fdlist

end

GO
--取汉字的拼音首字母
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_GetFirstPY]') and xtype in (N'FN', N'IF', N'TF'))
drop function Func_GetFirstPY
go
create function Func_GetFirstPY(@str Nvarchar(4000))
RETURNS  Nvarchar(4000)
as   
begin   
	declare @strLen int
	declare @index int
	declare @VChControl char(2)
	DECLARE @VChSpell VARCHAR(30)
	declare @return varchar(500)
	set @return=''
	set @strlen=len(@str)
	set @index=1
	while @index<=@strlen
	begin
		select @VChControl=substring(@str,@index,1)
		 IF @VChControl>'啊' AND @VChControl<'芭' 
			SELECT @VChSpell='A'
		ELSE IF @VChControl>='芭' AND @VChControl<'擦' 
			SELECT @VChSpell='B'
		ELSE IF @VChControl>='擦' AND @VChControl<'搭' 
			SELECT @VChSpell='C'
		ELSE IF @VChControl>='搭' AND @VChControl<'娥' 
			SELECT @VChSpell='D'
		ELSE IF @VChControl>='娥' AND @VChControl<'发' 
			SELECT @VChSpell='E'
		ELSE IF @VChControl>='发' AND @VChControl<='嘎'
			SELECT @VChSpell='F'
		ELSE IF @VChControl>'嘎' AND @VChControl<'哈' 
			SELECT @VChSpell='G'
		ELSE IF @VChControl>='哈' AND @VChControl<'击' 
			SELECT @VChSpell='H'
		ELSE IF @VChControl>='击' AND @VChControl<'喀'
			SELECT @VChSpell='J'
		ELSE IF @VChControl>='喀' AND @VChControl<'垃' 
			SELECT @VChSpell='K'
		ELSE IF @VChControl>='垃' AND @VChControl<'妈' 
			SELECT @VChSpell='L'
		ELSE IF @VChControl>='妈' AND @VChControl<'拿' 
			SELECT @VChSpell='M'
		ELSE IF @VChControl>='拿' AND @VChControl<'哦' 
			SELECT @VChSpell='N'
		ELSE IF @VChControl>='哦' AND @VChControl<'啪' 
			SELECT @VChSpell='O'
		ELSE IF @VChControl>='啪' AND @VChControl<'期'
			SELECT @VChSpell='P'
		ELSE IF @VChControl>='期' AND @VChControl<'然' 
			SELECT @VChSpell='Q'
		ELSE IF @VChControl>='然' AND @VChControl<'撒' 
			SELECT @VChSpell='R'
		ELSE IF @VChControl>='撒' AND @VChControl<'塌' 
			SELECT @VChSpell='S'
		ELSE IF @VChControl>='塌' AND @VChControl<'挖'
			SELECT @VChSpell='T'
		ELSE IF @VChControl>='挖' AND @VChControl<'昔' 
			SELECT @VChSpell='W'
		ELSE IF @VChControl>='昔' AND @VChControl<'压'
			SELECT @VChSpell='X'
		ELSE IF @VChControl>='压' AND @VChControl<'匝' 
			SELECT @VChSpell='Y'
		ELSE IF @VChControl>='匝' AND @VChControl<='座' 
			SELECT @VChSpell='Z'
		ELSE
			SELECT @VChSpell=@VChControl

		SELECT @return=@return + RTRIM(UPPER(@VChSpell))
		set @index=@index+1
	end
	return(@return)   
end
go
--select dbo.Func_GetFirstPY('根据功能菜单')

--根据功能菜单编号得到菜单的唯一ID号
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_GetFuncID]') and xtype in (N'FN', N'IF', N'TF'))
drop function Func_GetFuncID
go
create function Func_GetFuncID(@Fu_FileName Nvarchar(100))
RETURNS int
AS
BEGIN
	DECLARE  @tempID int
	set @tempID= 0
 
	if @Fu_FileName='Bug_Org_BugG.bpl'                                        set @tempID=1
	else if @Fu_FileName='Bug_Org_WaitJobG.bpl'                               set @tempID=2
	else if @Fu_FileName='Bug_Org_JobLogG.bpl'                                set @tempID=3
	else if @Fu_FileName='AIC_Caller_PeopleCheckG.bpl'                        set @tempID=4
	else if @Fu_FileName='AIC_Caller_ExitEnrolG.bpl'                          set @tempID=5
	else if @Fu_FileName='AIC_Caller_BookingG.bpl'                            set @tempID=6
	else if @Fu_FileName='AIC_Caller_ComeEnrolG.bpl'                          set @tempID=7
	else if @Fu_FileName='ABERP_Base_CustG.bpl'                               set @tempID=8
	else if @Fu_FileName='ABERP_Base_MaterielG.bpl'                           set @tempID=9
	else if @Fu_FileName='ABERP_Base_PriceSystemG.bpl'                        set @tempID=10
	else if @Fu_FileName='ABERP_Base_ProducePlantG.bpl'                       set @tempID=11
	else if @Fu_FileName='ABERP_Base_StorageG.bpl'                            set @tempID=12
	else if @Fu_FileName='ABERP_Base_SupplierG.bpl'                           set @tempID=13
	else if @Fu_FileName='ABERP_Base_TruckG.bpl'                              set @tempID=14
	else if @Fu_FileName='ABERP_Base_testPeopleG.bpl'                         set @tempID=15
	else if @Fu_FileName='AIC_AutoTestG.bpl'                                  set @tempID=16
	else if @Fu_FileName='ABERP_Buy_OrderG.bpl'                               set @tempID=17
	else if @Fu_FileName='AIC_KQ_ReaderG.bpl'                                 set @tempID=18
	else if @Fu_FileName='AIC_KQ_ShiftG.bpl'                                  set @tempID=19
	else if @Fu_FileName='AIC_Meeting_MeetingG.bpl'                           set @tempID=20
	else if @Fu_FileName='AIC_Meeting_MeetingAlarmG.bpl'                      set @tempID=21
	else if @Fu_FileName='AIC_Base_DepartG.bpl'                               set @tempID=22
	else if @Fu_FileName='AIC_Base_PeopleG.bpl'                               set @tempID=23
	else if @Fu_FileName='AIC_Base_KeyG.bpl'                                  set @tempID=24
	else if @Fu_FileName='AIC_Base_HostG.bpl'                                 set @tempID=25
	else if @Fu_FileName='AIC_Base_LocalParamsSetupG.bpl'                     set @tempID=26
	else if @Fu_FileName='AIC_Base_PublicParamsSetupG.bpl'                    set @tempID=27
	else if @Fu_FileName='AIC_Access_InterfaceBoardTypeG.bpl'                 set @tempID=28
	else if @Fu_FileName='AIC_Access_KeyPadFileG.bpl'                         set @tempID=29
	else if @Fu_FileName='AIC_Access_CardFormatG.bpl'                         set @tempID=30
	else if @Fu_FileName='AIC_Access_OutGroupG.bpl'                           set @tempID=31
	else if @Fu_FileName='AIC_Access_IOLinkerRulesChangeless.bpl'             set @tempID=32
	else if @Fu_FileName='AIC_Access_IOLinkerRulesG.bpl'                      set @tempID=33
	else if @Fu_FileName='AIC_Access_ControllerDataG.bpl'                     set @tempID=34
	else if @Fu_FileName='AIC_Access_InterfaceBoardDataG.bpl'                 set @tempID=35
	else if @Fu_FileName='AIC_Access_CfgFileG.bpl'                            set @tempID=36
	else if @Fu_FileName='AIC_Access_TaskG.bpl'                               set @tempID=37
	else if @Fu_FileName='AIC_Access_ClassCodeG.bpl'                          set @tempID=38
	else if @Fu_FileName='AIC_Access_TaskEventClassCodeG.bpl'                 set @tempID=39
	else if @Fu_FileName='AIC_Access_TaskEventMsgTypeG.bpl'                   set @tempID=40
	else if @Fu_FileName='AIC_Access_ErrorInfoG.bpl'                          set @tempID=41
	else if @Fu_FileName='AIC_Access_CommandInfoG.bpl'                        set @tempID=42
	else if @Fu_FileName='AIC_Access_EquipmentG.bpl'                              set @tempID=43
	else if @Fu_FileName='AIC_Access_AccessGroupG.bpl'                        set @tempID=44
	else if @Fu_FileName='AIC_Access_ElevatorGroupG.bpl'                      set @tempID=45
	else if @Fu_FileName='AIC_Access_CardG.bpl'                               set @tempID=46
	else if @Fu_FileName='AIC_Access_CardPictureG.bpl'                        set @tempID=47
	else if @Fu_FileName='AIC_Access_MapG.bpl'                                set @tempID=48
	else if @Fu_FileName='AIC_Access_AreaPeopleG.bpl'                         set @tempID=49
	else if @Fu_FileName='AIC_Access_MultiCardRuleG.bpl'                      set @tempID=50
	else if @Fu_FileName='AIC_Monitoring_CardG.bpl'                           set @tempID=51
	else if @Fu_FileName='AIC_Monitoring_EventGroupG.bpl'                     set @tempID=52
	else if @Fu_FileName='AIC_Monitoring_LineG.bpl'                           set @tempID=53
	else if @Fu_FileName='AIC_Monitoring_TaskG.bpl'                           set @tempID=54
	else if @Fu_FileName='AIC_Consume_TimeDefineG.bpl'                        set @tempID=55
	else if @Fu_FileName='AIC_Consume_TimeControlG.bpl'                       set @tempID=56
	else if @Fu_FileName='AIC_Consume_KeyCardG.bpl'                           set @tempID=57
	else if @Fu_FileName='AIC_Consume_AreaWordG.bpl'                          set @tempID=58
	else if @Fu_FileName='AIC_Consume_TerminalG.bpl'                          set @tempID=59
	else if @Fu_FileName='AIC_Consume_TerminalAlarmG.bpl'                     set @tempID=60
	else if @Fu_FileName='AIC_Consume_TerminalPeopleG.bpl'                    set @tempID=61
	else if @Fu_FileName='AIC_Consume_PeopleClassG.bpl'                       set @tempID=62
	else if @Fu_FileName='AIC_Consume_CardG.bpl'                              set @tempID=63
	else if @Fu_FileName='AIC_Consume_NotecaseOutputInputG.bpl'               set @tempID=64
	else if @Fu_FileName='AIC_Consume_PartitionMonthG.bpl'                    set @tempID=65
	else if @Fu_FileName='AIC_Consume_ManualTypeG.bpl'                        set @tempID=66
	else if @Fu_FileName='AIC_Consume_AllowanceAlarmClockG.bpl'               set @tempID=67
	else if @Fu_FileName='AIC_Consume_AllowanceG.bpl'                         set @tempID=68
	else if @Fu_FileName='AIC_UToolsG.bpl'                                        set @tempID=69
	else if @Fu_FileName='AIC_Consume_WaitAffirmConsumeRecordG.bpl'           set @tempID=70
	else if @Fu_FileName='AIC_Consume_DataCorrectG.bpl'                       set @tempID=71
	else if @Fu_FileName='ABSys_Approved_RuleG.bpl'                           set @tempID=72
	else if @Fu_FileName='ABSys_Approved_MessageG.bpl'                        set @tempID=73
	else if @Fu_FileName='ABSys_Org_ExtendReportDesignG.bpl'                  set @tempID=74
	else if @Fu_FileName='ABSys_Org_ExtendReportPrintG.bpl'                   set @tempID=75
	else if @Fu_FileName='MainHelp.chm'                                       set @tempID=80
	else if @Fu_FileName='ABSys_Org_AboutG.bpl'                               set @tempID=81
	else if @Fu_FileName='ABSys_Org_RegisteredG.bpl'                          set @tempID=82
	else if @Fu_FileName='ABSys_Org_UpdateG.bpl'                              set @tempID=83
	else if @Fu_FileName='Run:http://www.aibosoft.cn'                         set @tempID=84
	else if @Fu_FileName='Run:http://www.aibosoft.cn/QuestionAnswer.htm'      set @tempID=85
	else if @Fu_FileName='Run:http://www.aibosoft.cn/leaveWord/index.asp'     set @tempID=86
	else if @Fu_FileName='Run:http://www.aibosoft.cn/LinkMe.htm'              set @tempID=87
	else if @Fu_FileName='ABBackSQLServerP.exe'                               set @tempID=88
	else if @Fu_FileName='ABSys_Org_FieldG.bpl'                               set @tempID=89
	else if @Fu_FileName='ABSys_Org_DownListG.bpl'                            set @tempID=91
	else if @Fu_FileName in ('ABSys_Org_FuncControlPropertyG.bpl','ABSys_Org_ControlPropertyG.bpl')  set @tempID=92
	else if @Fu_FileName='ABSys_Org_InputOutputG.bpl'                          set @tempID=93
	else if @Fu_FileName='ABSys_Org_ParameterG.bpl'                           set @tempID=94
	else if @Fu_FileName='ABSys_Org_MailG.bpl'                                set @tempID=95
	else if @Fu_FileName='ABSys_Org_ConnG.bpl'                                set @tempID=96
	else if @Fu_FileName='ABSqlQueryP.exe'                                    set @tempID=97
	else if @Fu_FileName='ABMssqldellogP.exe'                                 set @tempID=98
	else if @Fu_FileName='ABSearchTxtP.exe'                                   set @tempID=99
	else if @Fu_FileName='ABLanExplorerP.exe'                                 set @tempID=100
	else if @Fu_FileName='ABSys_Log_CurLinkG.bpl'                             set @tempID=101
	else if @Fu_FileName='ABSys_Log_LoginG.bpl'                               set @tempID=102
	else if @Fu_FileName='ABSys_Log_SQLMonitorG.bpl'                          set @tempID=103
	else if @Fu_FileName='ABSys_Log_FieldChangeG.bpl'                         set @tempID=104
	else if @Fu_FileName='ABSys_Log_ErrorInfoG.bpl'                           set @tempID=105
	else if @Fu_FileName='ABSys_Log_CustomEventG.bpl'                         set @tempID=106
	else if @Fu_FileName='ABSys_Org_MakeBuildCodG.bpl'                        set @tempID=107
	else if @Fu_FileName='ABSys_Org_FunctionG.bpl'                            set @tempID=108
	else if @Fu_FileName in ('ABSys_Org_FuncSQLG.bpl','ABSys_Org_LoadSQLG.bpl')    set @tempID=109
	else if @Fu_FileName='ABSys_Org_KeyG.bpl'                                 set @tempID=110
	else if @Fu_FileName='ABSys_Org_EnterpriseG.bpl'                          set @tempID=111
	else if @Fu_FileName='ABSys_Org_PubDatasetG.bpl'                          set @tempID=112
	else if @Fu_FileName='ABSys_Org_FieldCodeG.bpl'                          set @tempID=113
	else if @Fu_FileName='ABSys_Org_PubQueryG.bpl'                          set @tempID=114
	else if @Fu_FileName='AIC_Caller_RecordG.bpl'                          set @tempID=115
	else if @Fu_FileName='AIC_Caller_CardG.bpl'                          set @tempID=116
	else if @Fu_FileName='AIC_Access_MultiCardRuleKeyG.bpl'                          set @tempID=117
	else if @Fu_FileName='AIC_Access_MultiCardRule_HG.bpl'                          set @tempID=118
	else if @Fu_FileName='AIC_Access_IOLinkerWindowRulesG.bpl'                      set @tempID=119
	else if @Fu_FileName='AIC_Access_PTPG.bpl'                      set @tempID=120
	else if @Fu_FileName='AIC_Base_PeopleCheckG.bpl'                      set @tempID=121
	else if @Fu_FileName='ABSys_Org_historyDataG.bpl'                      set @tempID=122
	else if @Fu_FileName='AIC_Consume_TerminalCardInfoG.bpl'                      set @tempID=123
 
	if @Fu_FileName='AIC_Consume_OFFCard_YYCG.bpl'                      set @tempID=124
  else if @Fu_FileName='AIC_HR_TimePart_3MG.bpl'                           set @tempID=125 
	else if @Fu_FileName='AIC_Consume_IDMoneyOutPut_DZG.bpl'                 set @tempID=126
	else if @Fu_FileName='AIC_Base_DepartRight_DZG.bpl'                      set @tempID=127
	else if @Fu_FileName='AIC_Consume_DeptWeb_TYG.bpl'                       set @tempID=128
	else if @Fu_FileName='AIC_Consume_ShuttleBus_TYYTG.bpl'                  set @tempID=129
	else if @Fu_FileName='AIC_Consume_ShuttleBusInOutAffirm_TYYTG.bpl'       set @tempID=130
	else if @Fu_FileName='AIC_Consume_ShuttleBusReport_TYYTG.bpl'            set @tempID=131

	else if @Fu_FileName='AIC_Consume_Custom1G.bpl'            set @tempID=132
	else if @Fu_FileName='AIC_Consume_Custom2G.bpl'            set @tempID=133
	else if @Fu_FileName='AIC_Consume_Custom3G.bpl'            set @tempID=134
	else if @Fu_FileName='AIC_Consume_Custom4G.bpl'            set @tempID=135
	else if @Fu_FileName='AIC_Consume_Custom5G.bpl'            set @tempID=136

 
  RETURN(@tempID)
end
go
 
 

--根据检测的功能串（类似010101101）得到菜单的文件名列表
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_GetFuncFileNameList]') and xtype in (N'FN', N'IF', N'TF'))
drop function Func_GetFuncFileNameList
go
create function Func_GetFuncFileNameList(@CheckStr Nvarchar(1000))
RETURNS table
AS

 RETURN (
	select 'Bug_Org_BugG.bpl'  tempFileName                               where substring(@CheckStr,1  ,1)='1'
	union all select 'Bug_Org_WaitJobG.bpl'                               where substring(@CheckStr,2  ,1)='1'
	union all select 'Bug_Org_JobLogG.bpl'                                where substring(@CheckStr,3  ,1)='1'
	union all select 'AIC_Caller_PeopleCheckG.bpl'                        where substring(@CheckStr,4  ,1)='1'
	union all select 'AIC_Caller_ExitEnrolG.bpl'                          where substring(@CheckStr,5  ,1)='1'
	union all select 'AIC_Caller_BookingG.bpl'                            where substring(@CheckStr,6  ,1)='1'
	union all select 'AIC_Caller_ComeEnrolG.bpl'                          where substring(@CheckStr,7  ,1)='1'
	union all select 'ABERP_Base_CustG.bpl'                               where substring(@CheckStr,8  ,1)='1'
	union all select 'ABERP_Base_MaterielG.bpl'                           where substring(@CheckStr,9  ,1)='1'
	union all select 'ABERP_Base_PriceSystemG.bpl'                        where substring(@CheckStr,10 ,1)='1'
	union all select 'ABERP_Base_ProducePlantG.bpl'                       where substring(@CheckStr,11 ,1)='1'
	union all select 'ABERP_Base_StorageG.bpl'                            where substring(@CheckStr,12 ,1)='1'
	union all select 'ABERP_Base_SupplierG.bpl'                           where substring(@CheckStr,13 ,1)='1'
	union all select 'ABERP_Base_TruckG.bpl'                              where substring(@CheckStr,14 ,1)='1'
	union all select 'ABERP_Base_testPeopleG.bpl'                         where substring(@CheckStr,15 ,1)='1'
	union all select 'AIC_AutoTestG.bpl'                                  where substring(@CheckStr,16 ,1)='1'
	union all select 'ABERP_Buy_OrderG.bpl'                               where substring(@CheckStr,17 ,1)='1'
	union all select 'AIC_KQ_ReaderG.bpl'                                 where substring(@CheckStr,18 ,1)='1'
	union all select 'AIC_KQ_ShiftG.bpl'                                  where substring(@CheckStr,19 ,1)='1'
	union all select 'AIC_Meeting_MeetingG.bpl'                           where substring(@CheckStr,20 ,1)='1'
	union all select 'AIC_Meeting_MeetingAlarmG.bpl'                      where substring(@CheckStr,21 ,1)='1'
	union all select 'AIC_Base_DepartG.bpl'                               where substring(@CheckStr,22 ,1)='1'
	union all select 'AIC_Base_PeopleG.bpl'                               where substring(@CheckStr,23 ,1)='1'
	union all select 'AIC_Base_KeyG.bpl'                                  where substring(@CheckStr,24 ,1)='1'
	union all select 'AIC_Base_HostG.bpl'                                 where substring(@CheckStr,25 ,1)='1'
	union all select 'AIC_Base_LocalParamsSetupG.bpl'                     where substring(@CheckStr,26 ,1)='1'
	union all select 'AIC_Base_PublicParamsSetupG.bpl'                    where substring(@CheckStr,27 ,1)='1'
	union all select 'AIC_Access_InterfaceBoardTypeG.bpl'                 where substring(@CheckStr,28 ,1)='1'
	union all select 'AIC_Access_KeyPadFileG.bpl'                         where substring(@CheckStr,29 ,1)='1'
	union all select 'AIC_Access_CardFormatG.bpl'                         where substring(@CheckStr,30 ,1)='1'
	union all select 'AIC_Access_OutGroupG.bpl'                           where substring(@CheckStr,31 ,1)='1'
	union all select 'AIC_Access_IOLinkerRulesChangeless.bpl'             where substring(@CheckStr,32 ,1)='1'
	union all select 'AIC_Access_IOLinkerRulesG.bpl'                      where substring(@CheckStr,33 ,1)='1'
	union all select 'AIC_Access_ControllerDataG.bpl'                     where substring(@CheckStr,34 ,1)='1'
	union all select 'AIC_Access_InterfaceBoardDataG.bpl'                 where substring(@CheckStr,35 ,1)='1'
	union all select 'AIC_Access_CfgFileG.bpl'                            where substring(@CheckStr,36 ,1)='1'
	union all select 'AIC_Access_TaskG.bpl'                               where substring(@CheckStr,37 ,1)='1'
	union all select 'AIC_Access_ClassCodeG.bpl'                          where substring(@CheckStr,38 ,1)='1'
	union all select 'AIC_Access_TaskEventClassCodeG.bpl'                 where substring(@CheckStr,39 ,1)='1'
	union all select 'AIC_Access_TaskEventMsgTypeG.bpl'                   where substring(@CheckStr,40 ,1)='1'
	union all select 'AIC_Access_ErrorInfoG.bpl'                          where substring(@CheckStr,41 ,1)='1'
	union all select 'AIC_Access_CommandInfoG.bpl'                        where substring(@CheckStr,42 ,1)='1'
	union all select 'AIC_Access_EquipmentG.bpl'                          where substring(@CheckStr,43 ,1)='1'
	union all select 'AIC_Access_AccessGroupG.bpl'                        where substring(@CheckStr,44 ,1)='1'
	union all select 'AIC_Access_ElevatorGroupG.bpl'                      where substring(@CheckStr,45 ,1)='1'
	union all select 'AIC_Access_CardG.bpl'                               where substring(@CheckStr,46 ,1)='1'
	union all select 'AIC_Access_CardPictureG.bpl'                        where substring(@CheckStr,47 ,1)='1'
	union all select 'AIC_Access_MapG.bpl'                                where substring(@CheckStr,48 ,1)='1'
	union all select 'AIC_Access_AreaPeopleG.bpl'                         where substring(@CheckStr,49 ,1)='1'
	union all select 'AIC_Access_MultiCardRuleG.bpl'                      where substring(@CheckStr,50 ,1)='1'
	union all select 'AIC_Monitoring_CardG.bpl'                           where substring(@CheckStr,51 ,1)='1'
	union all select 'AIC_Monitoring_EventGroupG.bpl'                     where substring(@CheckStr,52 ,1)='1'
	union all select 'AIC_Monitoring_LineG.bpl'                           where substring(@CheckStr,53 ,1)='1'
	union all select 'AIC_Monitoring_TaskG.bpl'                           where substring(@CheckStr,54 ,1)='1'
	union all select 'AIC_Consume_TimeDefineG.bpl'                        where substring(@CheckStr,55 ,1)='1'
	union all select 'AIC_Consume_TimeControlG.bpl'                       where substring(@CheckStr,56 ,1)='1'
	union all select 'AIC_Consume_KeyCardG.bpl'                           where substring(@CheckStr,57 ,1)='1'
	union all select 'AIC_Consume_AreaWordG.bpl'                          where substring(@CheckStr,58 ,1)='1'
	union all select 'AIC_Consume_TerminalG.bpl'                          where substring(@CheckStr,59 ,1)='1'
	union all select 'AIC_Consume_TerminalAlarmG.bpl'                     where substring(@CheckStr,60 ,1)='1'
	union all select 'AIC_Consume_TerminalPeopleG.bpl'                    where substring(@CheckStr,61 ,1)='1'
	union all select 'AIC_Consume_PeopleClassG.bpl'                       where substring(@CheckStr,62 ,1)='1'
	union all select 'AIC_Consume_CardG.bpl'                              where substring(@CheckStr,63 ,1)='1'
	union all select 'AIC_Consume_NotecaseOutputInputG.bpl'               where substring(@CheckStr,64 ,1)='1'
	union all select 'AIC_Consume_PartitionMonthG.bpl'                    where substring(@CheckStr,65 ,1)='1'
	union all select 'AIC_Consume_ManualTypeG.bpl'                        where substring(@CheckStr,66 ,1)='1'
	union all select 'AIC_Consume_AllowanceAlarmClockG.bpl'               where substring(@CheckStr,67 ,1)='1'
	union all select 'AIC_Consume_AllowanceG.bpl'                         where substring(@CheckStr,68 ,1)='1'
	union all select 'AIC_UToolsG.bpl'                                    where substring(@CheckStr,69 ,1)='1'
	union all select 'AIC_Consume_WaitAffirmConsumeRecordG.bpl'           where substring(@CheckStr,70 ,1)='1'
	union all select 'AIC_Consume_DataCorrectG.bpl'                       where substring(@CheckStr,71 ,1)='1'
	union all select 'ABSys_Approved_RuleG.bpl'                           where substring(@CheckStr,72 ,1)='1'
	union all select 'ABSys_Approved_MessageG.bpl'                        where substring(@CheckStr,73 ,1)='1'
	union all select 'ABSys_Org_ExtendReportDesignG.bpl'                  where substring(@CheckStr,74 ,1)='1'
	union all select 'ABSys_Org_ExtendReportPrintG.bpl'                   where substring(@CheckStr,75 ,1)='1'
	union all select 'MainHelp.chm'                                       where substring(@CheckStr,80 ,1)='1'
	union all select 'ABSys_Org_AboutG.bpl'                               where substring(@CheckStr,81 ,1)='1'
	union all select 'ABSys_Org_RegisteredG.bpl'                          where substring(@CheckStr,82 ,1)='1'
	union all select 'ABSys_Org_UpdateG.bpl'                              where substring(@CheckStr,83 ,1)='1'
	union all select 'Run:http://www.aibosoft.cn'                         where substring(@CheckStr,84 ,1)='1'
	union all select 'Run:http://www.aibosoft.cn/QuestionAnswer.htm'      where substring(@CheckStr,85 ,1)='1'
	union all select 'Run:http://www.aibosoft.cn/leaveWord/index.asp'     where substring(@CheckStr,86 ,1)='1'
	union all select 'Run:http://www.aibosoft.cn/LinkMe.htm'              where substring(@CheckStr,87 ,1)='1'
	union all select 'ABBackSQLServerP.exe'                               where substring(@CheckStr,88 ,1)='1'
	union all select 'ABSys_Org_FieldG.bpl'                               where substring(@CheckStr,89 ,1)='1'
	union all select 'ABSys_Org_DownListG.bpl'                            where substring(@CheckStr,91 ,1)='1'
	union all select 'ABSys_Org_FuncControlPropertyG.bpl'                 where substring(@CheckStr,92 ,1)='1'
	union all select 'ABSys_Org_ControlPropertyG.bpl'                     where substring(@CheckStr,92 ,1)='1'
	union all select 'ABSys_Org_InputOutputG.bpl'                         where substring(@CheckStr,93 ,1)='1'
	union all select 'ABSys_Org_ParameterG.bpl'                           where substring(@CheckStr,94 ,1)='1'
	union all select 'ABSys_Org_MailG.bpl'                                where substring(@CheckStr,95 ,1)='1'
	union all select 'ABSys_Org_ConnG.bpl'                                where substring(@CheckStr,96 ,1)='1'
	union all select 'ABSqlQueryP.exe'                                    where substring(@CheckStr,97 ,1)='1'
	union all select 'ABMssqldellogP.exe'                                 where substring(@CheckStr,98 ,1)='1'
	union all select 'ABSearchTxtP.exe'                                   where substring(@CheckStr,99 ,1)='1'
	union all select 'ABLanExplorerP.exe'                                 where substring(@CheckStr,100,1)='1'
	union all select 'ABSys_Log_CurLinkG.bpl'                             where substring(@CheckStr,101,1)='1'
	union all select 'ABSys_Log_LoginG.bpl'                               where substring(@CheckStr,102,1)='1'
	union all select 'ABSys_Log_SQLMonitorG.bpl'                          where substring(@CheckStr,103,1)='1'
	union all select 'ABSys_Log_FieldChangeG.bpl'                         where substring(@CheckStr,104,1)='1'
	union all select 'ABSys_Log_ErrorInfoG.bpl'                           where substring(@CheckStr,105,1)='1'
	union all select 'ABSys_Log_CustomEventG.bpl'                         where substring(@CheckStr,106,1)='1'
	union all select 'ABSys_Org_MakeBuildCodG.bpl'                        where substring(@CheckStr,107,1)='1'
	union all select 'ABSys_Org_FunctionG.bpl'                            where substring(@CheckStr,108,1)='1'
	union all select 'ABSys_Org_FuncSQLG.bpl'                             where substring(@CheckStr,109,1)='1'
	union all select 'ABSys_Org_LoadSQLG.bpl'                             where substring(@CheckStr,109,1)='1'
	union all select 'ABSys_Org_KeyG.bpl'                                 where substring(@CheckStr,110,1)='1'
	union all select 'ABSys_Org_EnterpriseG.bpl'                          where substring(@CheckStr,111,1)='1'
	union all select 'ABSys_Org_PubDatasetG.bpl'                          where substring(@CheckStr,112,1)='1'
	union all select 'ABSys_Org_FieldCodeG.bpl'                           where substring(@CheckStr,113,1)='1'
	union all select 'ABSys_Org_PubQueryG.bpl'                            where substring(@CheckStr,114,1)='1'
	union all select 'AIC_Caller_RecordG.bpl'                             where substring(@CheckStr,115,1)='1'
	union all select 'AIC_Caller_CardG.bpl'                               where substring(@CheckStr,116,1)='1'
	union all select 'AIC_Access_MultiCardRuleKeyG.bpl'                               where substring(@CheckStr,117,1)='1'
	union all select 'AIC_Access_MultiCardRule_HG.bpl'                               where substring(@CheckStr,118,1)='1'
	union all select 'AIC_Access_IOLinkerWindowRulesG.bpl'             where substring(@CheckStr,119 ,1)='1'
	union all select 'AIC_Access_PTPG.bpl'             where substring(@CheckStr,120 ,1)='1'
	union all select 'AIC_Base_PeopleCheckG.bpl'             where substring(@CheckStr,121 ,1)='1'
	union all select 'ABSys_Org_historyDataG.bpl'             where substring(@CheckStr,122 ,1)='1'
	union all select 'AIC_Consume_TerminalCardInfoG.bpl'      where substring(@CheckStr,123 ,1)='1'
 
	union all select 'AIC_Consume_OFFCard_YYCG.bpl'                      where substring(@CheckStr,124,1)='1'
	union all select 'AIC_HR_TimePart_3MG.bpl'                           where substring(@CheckStr,125,1)='1'
	union all select 'AIC_Consume_IDMoneyOutPut_DZG.bpl'                 where substring(@CheckStr,126,1)='1'
	union all select 'AIC_Base_DepartRight_DZG.bpl'                      where substring(@CheckStr,127,1)='1'
	union all select 'AIC_Consume_DeptWeb_TYG.bpl'                       where substring(@CheckStr,128,1)='1'
	union all select 'AIC_Consume_ShuttleBus_TYYTG.bpl'                  where substring(@CheckStr,129,1)='1'
	union all select 'AIC_Consume_ShuttleBusInOutAffirm_TYYTG.bpl'       where substring(@CheckStr,130,1)='1'
	union all select 'AIC_Consume_ShuttleBusReport_TYYTG.bpl'            where substring(@CheckStr,131,1)='1'


	union all select 'AIC_Consume_Custom1G.bpl'            where substring(@CheckStr,132,1)='1'
	union all select 'AIC_Consume_Custom2G.bpl'            where substring(@CheckStr,133,1)='1'
	union all select 'AIC_Consume_Custom3G.bpl'            where substring(@CheckStr,134,1)='1'
	union all select 'AIC_Consume_Custom4G.bpl'            where substring(@CheckStr,135,1)='1'
	union all select 'AIC_Consume_Custom5G.bpl'            where substring(@CheckStr,136,1)='1'

 )
go