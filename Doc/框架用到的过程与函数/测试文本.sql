--为解决GUID索引带来的效率问题，用有序的模拟GUID replace(replace(replace(replace(SYSDATETIME(),'-',''),' ',''),':',''),'.','')+cast(cast((rand()+rand())*100000000000000 as bigint) as varchar(15))

--主键效率测试

 
exec  ABFramework.dbo.Proc_BeforeTest

--测试无主键/Identity/Uniqueidentifier/varchar类型主键插入表时的效率
set nocount on
declare @now datetime,@i int
set @i=1
set @now=getdate()
Create table  test1(nopkey int,col uniqueidentifier )
while @i<1000000
Begin
	insert into test1 values (1,ABFramework.dbo.Func_GetOrderGuid())
	set @i=@i+1
end
Print'新表无主键插入100万条数据所耗时间:'+convert(varchar(200),datediff(ms,@now,Getdate()))+'毫秒'

set @i=1
set @now=getdate()
while @i<10000 
Begin
	insert into test1 values (1,ABFramework.dbo.Func_GetOrderGuid())
	set @i=@i+1
end
Print'100万行中再插入10000条数据时间:'+convert(varchar(200),datediff(ms,@now,Getdate()))+'毫秒'

GO
Create table Test2(pkey int identity(1,1) primary key , col uniqueidentifier default(ABFramework.dbo.Func_GetOrderGuid()))
declare @now datetime,@i int
set @i=1
set @now=getdate()
while @i<1000000
Begin
	insert into test2(col) values (ABFramework.dbo.Func_GetOrderGuid())
	set @i=@i+1
end
Print '新表以int为主键插入100万条数据所耗时间:'+convert(varchar(200),datediff(ms,@now,Getdate()))+'毫秒'

set @i=1
set @now=getdate()
while @i<10000
Begin
	insert into test2(col) values (ABFramework.dbo.Func_GetOrderGuid())
	set @i=@i+1
end
Print'100万行中再插入10000条数据所耗时间:'+convert(varchar(200),datediff(ms,@now,Getdate()))+'毫秒'
GO

Create table test3(Pkey uniqueidentifier Primary key, col int)
declare @now datetime,@i int
set @i=1
set @now=getdate()
while @i<1000000
Begin
	insert into test3 values (ABFramework.dbo.Func_GetOrderGuid(),3)
	set @i=@i+1
end

Print '新表以uniqueidentifier主键插入100万条数据所耗时间:'+convert(varchar(200),datediff(ms,@now,Getdate()))+'毫秒'

set @i=1
set @now=getdate()
while @i<10000
Begin
	insert into test3 values (ABFramework.dbo.Func_GetOrderGuid(),3)
	set @i=@i+1
end
Print'100万行中再插入10000条数据所耗时间:'+convert(varchar(200),datediff(ms,@now,Getdate()))+'毫秒'

GO

Create table test4(Pkey varchar(36) Primary key, col int)
declare @now datetime,@i int
set @i=1
set @now=getdate()
while @i<1000000
Begin
	insert into test4 values (ABFramework.dbo.Func_GetOrderGuid(),3)
	set @i=@i+1
end
Print '新表以varchar(36)类型为主键插入100万条数据:所耗时间：'+convert(varchar(200),datediff(ms,@now,Getdate()))+'毫秒'
set @i=1
set @now=getdate()
while @i<10000
Begin
	insert into test4 values (convert(varchar(36),ABFramework.dbo.Func_GetOrderGuid()),3)
	set @i=@i+1
end
Print'100万行中再插入10000条数据所耗时间:'+convert(varchar(200),datediff(ms,@now,Getdate()))+'毫秒'

GO
/*
drop table test1
drop table test2
drop table test3
drop table test4
*/

exec  ABFramework.dbo.Proc_AfterTest













--插入消费记录表

exec  ABFramework.dbo.Proc_BeforeTest

 
set nocount on 
declare @i int

   
truncate table AIC_Consume_ConsumeRecord      
 if exists (select * from tempdb.dbo.sysobjects where id = object_id(N'tempdb.[dbo].[##tempAIC_Consume_ConsumeRecord]'))
	drop table ##tempAIC_Consume_ConsumeRecord
 
select * into ##tempAIC_Consume_ConsumeRecord from AIC_Consume_ConsumeRecord 
    
set @i=1
while @i<=5000000
begin 
if @i % 10000=0 
   RAISERROR('%d/5000000', 10, 1, @i) WITH NOWAIT

	insert into ##tempAIC_Consume_ConsumeRecord(CR_Guid,CR_TE_Code,CR_TE_Number,CR_DateTime,
												CR_CardCode,CR_WA_Code,CR_TradeType,CR_Money,CR_Balance,
												CR_Number,CR_AllowanceType,CR_InsertDateTime,PubID)
	values(ABFramework.dbo.Func_GetOrderGuid(),@i,0,GETDATE(),@i,
		   @i,1,1,100,0,'',GETDATE(),ABFramework.dbo.Func_GetOrderGuid()) 

  set @i=@i+1
end
   
exec master..xp_cmdshell 'bcp "SELECT top 1000000 * FROM ##tempAIC_Consume_ConsumeRecord" queryout "c:\tempAIC_Consume_ConsumeRecord.txt" -S"." -c -T -a65535'
exec master..xp_cmdshell 'bcp "AIC..AIC_Consume_ConsumeRecord" in "c:\tempAIC_Consume_ConsumeRecord.txt"  -S"." -c -T -a65535 -h"TABLOCK'
   
set nocount OFF
 

 

 
exec  ABFramework.dbo.Proc_AfterTest
  
  