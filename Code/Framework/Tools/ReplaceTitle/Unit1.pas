unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls,ABPubFuncU,ABPubFormU;

type
  TForm1 = class(TABPubForm)
    btn1: TButton;
    edt1: TEdit;
    edt2: TEdit;
    lbl1: TLabel;
    lbl2: TLabel;
    Memo1: TMemo;
    Button1: TButton;
    Button2: TButton;
    Label1: TLabel;
    Edit1: TEdit;
    Button3: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    procedure GetAllRunningWindows(aFilterCaption: string);
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}
var
  FFilterCaption:string;

procedure TForm1.GetAllRunningWindows(aFilterCaption:string);
  function EnumchildWindowsCode(Wnd: hWnd; Strs: TStrings): Boolean; stdcall;
  var
    WndCaption: array[0..255-1] of char;
    WndClassName: array[0..255-1] of char;
    tempStr: string;
    tempWndClass:TWndClass;
  begin
    if (GetWindowText(Wnd, WndCaption, 255)<>0) and
       (GetClassName(Wnd, WndClassName, 255)<>0) and
       (GetClassInfo(Wnd, WndClassName, tempWndClass))
        then
    begin
      tempStr:='Wnd='+inttostr(Wnd)+' '+'ClassName='+StrPas(WndClassName)+' '+'Name='+StrPas(WndClassName)+' '+'Caption='+StrPas(WndCaption);
      if Strs.IndexOf(tempStr)<0 then
        Strs.Add(tempStr);
    end;

    Result := True;
  end;
  function EnumWindowsCode(Wnd: hWnd; Strs: TStrings): Boolean; stdcall;
  var
    WndCaption: array[0..255-1] of char;
    WndClassName: array[0..255-1] of char;
    tempStr: string;
  begin
    if (GetWindowText(Wnd, WndCaption, 255)<>0) and
       ((FFilterCaption='') or (ABPos(FFilterCaption,StrPas(WndCaption))>0)) and
       (GetClassName(Wnd, WndClassName, 255)<>0)
        then
    begin
      tempStr:='Wnd='+inttostr(Wnd)+' '+'ClassName='+StrPas(WndClassName)+' '+'Caption='+StrPas(WndCaption);
      if Strs.IndexOf(tempStr)<0 then
        Strs.Add(tempStr);

      Enumchildwindows(Wnd,@EnumchildWindowsCode,LongInt(Strs));//獲得本窗體所有句柄及類名
    end;

    Result := True;
  end;
begin
  FFilterCaption:= aFilterCaption;
  EnumWindows(@EnumWindowsCode, LongInt(Memo1.Lines));
end;

procedure TForm1.btn1Click(Sender: TObject);
begin
  SendMessage(hWnd(StrToInt(Edit1.Text)), WM_SETTEXT, 0, Integer(edt2.Text));
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  Memo1.Lines.Clear;
  GetAllRunningWindows('');
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  Memo1.Lines.Clear;
  GetAllRunningWindows(edt1.Text);
end;

procedure TForm1.Button3Click(Sender: TObject);
var
  tempFindFindHWnd: HWnd;
begin
  ABReplaceControlCaption(tempFindFindHWnd,edt1.Text,edt2.Text,true);
end;

end.
