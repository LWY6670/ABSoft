unit Main;

interface

uses
  ABPubFormU,

  WinTypes,WinProcs,Classes,Graphics,Forms,Controls,Buttons,StdCtrls,ExtCtrls,
  SysUtils,Dialogs,Detail;

type
  TMainForm = class(TABPubForm)
    OKBtn: TBitBtn;
    CancelBtn: TBitBtn;
    Edit1: TEdit;
    Button1: TButton;
    Button2: TButton;
    Edit2: TEdit;
    OpenDialog1: TOpenDialog;
    procedure CancelBtnClick(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  public
    procedure SetFileName(const Name:string);
  end;

var
  MainForm: TMainForm;

implementation

{$R *.DFM}


procedure TMainForm.CancelBtnClick(Sender: TObject);
begin
  Close;
end;

procedure TMainForm.OKBtnClick(Sender: TObject);
begin
  if not FileExists(Edit1.Text) then begin
    Edit1.SetFocus;
    Exit;
  end;
  if not FileExists(Edit2.Text) then begin
    Edit2.SetFocus;
    Exit;
  end;
  Hide;
  Dateivergleich.Dateien_vergleichen(Edit1.Text,Edit2.Text);
  Dateivergleich.ShowModal;
  Close;
end;


procedure TMainForm.Button1Click(Sender: TObject);
var
  ext: string[4];
begin
  if OpenDialog1.Execute then begin
    Edit1.Text := LowerCase(OpenDialog1.FileName);
    if Edit2.Text='' then begin
      if FileExists(ChangeFileExt(Edit1.Text,'.bak')) then
        Edit2.Text := ChangeFileExt(Edit1.Text,'.bak')
      else begin
        ext := ExtractFileExt(Edit1.Text);
        Insert ('~',ext,2);
        if FileExists(ChangeFileExt(Edit1.Text,ext)) then
          Edit2.Text := ChangeFileExt(Edit1.Text,ext);
      end;
    end;
  end;
end;


procedure TMainForm.Button2Click(Sender: TObject);
begin
  if OpenDialog1.Execute then
    Edit2.Text := LowerCase(OpenDialog1.FileName);
end;


procedure TMainForm.SetFileName(const Name:string);
var
  ext: string[4];
begin
  if FileExists(Name) then begin
    Edit1.Text := LowerCase(Name);
    ext := ExtractFileExt(Edit1.Text);
    Insert ('~',ext,2);
    if FileExists(ChangeFileExt(Edit1.Text,ext)) then
      Edit2.Text := ChangeFileExt(Edit1.Text,ext)
    else if (ExtractFileExt(Edit1.Text)<>'.bak')
            and FileExists(ChangeFileExt(Edit1.Text,'.bak')) then
      Edit2.Text := ChangeFileExt(Edit1.Text,'.bak');
  end;
end;

end.
