unit Main;

interface

uses


  Windows,Messages,SysUtils,Variants,Classes,Graphics,Controls,Forms,Dialogs,
  StdCtrls,ExtCtrls,Grids;

type
  TMainForm = class(TForm)
    Memo1: TMemo;
    Memo2: TMemo;
    Panel1: TPanel;
    Button5: TButton;
    Button6: TButton;
    Button4: TButton;
    Button3: TButton;
    Button2: TButton;
    Button1: TButton;
    Panel2: TPanel;
    Panel3: TPanel;
    Button7: TButton;
    Button8: TButton;
    Button9: TButton;
    Button10: TButton;
    ListBox1: TListBox;
    StringGrid1: TStringGrid;
    Panel4: TPanel;
    Button11: TButton;
    Button12: TButton;
    Button13: TButton;
    Button14: TButton;
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure Button9Click(Sender: TObject);
    procedure Button10Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ListBox1Click(Sender: TObject);
    procedure Button11Click(Sender: TObject);
    procedure Button12Click(Sender: TObject);
    procedure Button13Click(Sender: TObject);
    procedure Button14Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  MainForm: TMainForm;

implementation

{$R *.dfm}
function GetPyChar(const HZ: AnsiString): string;
const
  HZCode: array[0..25, 0..1] of Integer = ((1601, 1636), (1637, 1832), (1833, 2077),
    (2078, 2273), (2274, 2301), (2302, 2432), (2433, 2593), (2594, 2786), (9999, 0000),
    (2787, 3105), (3106, 3211), (3212, 3471), (3472, 3634), (3635, 3722), (3723, 3729),
    (3730, 3857), (3858, 4026), (4027, 4085), (4086, 4389), (4390, 4557), (9999, 0000),
    (9999, 0000), (4558, 4683), (4684, 4924), (4925, 5248), (5249, 5589));
var
  i,j,HzOrd: Integer;
begin
  i := 1;
  while i <= Length(HZ) do
    begin
    if (HZ[i] >= #160) and (HZ[i + 1] >= #160) then
    begin
      HzOrd := (Ord(HZ[i]) - 160) * 100 + Ord(HZ[i + 1]) - 160;
      for j := 0 to 25 do
      begin
        if (HzOrd >= HZCode[j][0]) and (HzOrd <= HZCode[j][1]) then
        begin
          Result := Result + Char(Byte('A') + j);
          Break;
        end;
      end;
      Inc(i);
    end else Result := Result + HZ[i];
      Inc(i);
  end;
end;

//只单独汉字：
function GetPyFitstChar(HZ: AnsiString): Char;
begin
  case LoWord(HZ[1]) shl 8 + LoWord(HZ[2]) of
    $B0A1..$B0C4: Result := 'A';
    $B0C5..$B2C0: Result := 'B';
    $B2C1..$B4ED: Result := 'C';
    $B4EE..$B6E9: Result := 'D';
    $B6EA..$B7A1: Result := 'E';
    $B7A2..$B8C0: Result := 'F';
    $B8C1..$B9FD: Result := 'G';
    $B9FE..$BBF6: Result := 'H';
    $BBF7..$BFA5: Result := 'J';
    $BFA6..$C0AB: Result := 'K';
    $C0AC..$C2E7: Result := 'L';
    $C2E8..$C4C2: Result := 'M';
    $C4C3..$C5B5: Result := 'N';
    $C5B6..$C5BD: Result := 'O';
    $C5BE..$C6D9: Result := 'P';
    $C6DA..$C8BA: Result := 'Q';
    $C8BB..$C8F5: Result := 'R';
    $C8F6..$CBF9: Result := 'S';
    $CBFA..$CDD9: Result := 'T';
    $CDDA..$CEF3: Result := 'W';
    $CEF4..$D188: Result := 'X';
    $D1B9..$D4D0: Result := 'Y';
    $D4D1..$D7F9: Result := 'Z';
  else
    Result := Char(0);
  end;
end;


{查汉字区位码}
function Str2GB(const s: AnsiString): string;
const
  G = 160;
begin
  Result := Format('%d%d', [Ord(s[1])-G, Ord(s[2])-G]);
end;

{通过区位码查汉字}
function GB2Str(const n: Word): string;
const
  G = 160;
begin
  Result := string(AnsiChar(n div 100 + G) + AnsiChar(n mod 100 + G));
end;

function IsChinese(const aChar: Char): Boolean;
begin
  //汉字的 UniCode 编码范围是: $4E00..$9FA5
  Result:=true;
  if (ord(aChar)<19968) or (ord(aChar)>40869) then
    Result:=false;
end;

function HaveChinese(const Str: String): Boolean;
var
  i:LongInt;
begin
  Result:=false;
  for I := 1 to Length(Str) do
  begin
    if IsChinese(Str[i]) then
    begin
      Result:=true;
      break;
    end;
  end;
end;

//转换
function Str_Gb2UniCode(text: string): String;
var
  i,len: Integer;
  cur: Integer;
  t: String;
  ws: WideString;
begin
  Result := '';
  ws := text;
  len := Length(ws);
  i := 1;
  while i <= len do
  begin
    cur := Ord(ws[i]);
    FmtStr(t,'%4.4X',[cur]);
    Result := Result + t;
    Inc(i);
  end;
end;

//恢复
function Unicode_str(text: string):string;
var
  i,len: Integer;
  ws: WideString;
begin
  ws := '';
  i := 1;
  len := Length(text);
  while i < len do
  begin
    ws := ws + Widechar(StrToInt('$' + Copy(text,i,4)));
    i := i+4;
  end;
  Result := ws;
end;
              

//从字符串到十六进制的函数
function StrToHex(str: string; AEncoding: TEncoding): string;
var
  ss: TStringStream;
  i: Integer;
begin                                             
  Result := '';
  ss := TStringStream.Create(str, AEncoding);
  for i := 0 to ss.Size - 1 do
    Result := Result + Format('%.2x ', [ss.Bytes[i]]);
  ss.Free;
end;

procedure TMainForm.Button11Click(Sender: TObject);
begin
  Memo2.Text := Str_Gb2UniCode(Memo1.Text);  //万一=4E074E00
end;

procedure TMainForm.Button12Click(Sender: TObject);
begin
  Memo2.Text := Unicode_str(Memo1.Text);  //'4E074E00'=万一
end;

procedure TMainForm.Button13Click(Sender: TObject);
begin
  Memo2.Text := GetPyChar(Memo1.Text);  //结果：'万一的 Delphi 博客' WYD Delphi BK
end;

procedure TMainForm.Button14Click(Sender: TObject);
begin
  Memo2.Text := GetPyFitstChar(Memo1.Text);  //结果：'万一的 Delphi 博客'  W
end;

procedure TMainForm.Button1Click(Sender: TObject);
begin
  Memo2.Text := StrToHex(Memo1.Text, TEncoding.ASCII);
end;

procedure TMainForm.Button2Click(Sender: TObject);
begin
  Memo2.Text := StrToHex(Memo1.Text, TEncoding.Unicode);
end;

procedure TMainForm.Button3Click(Sender: TObject);
begin
  Memo2.Text := StrToHex(Memo1.Text, TEncoding.UTF7);
end;

procedure TMainForm.Button4Click(Sender: TObject);
begin
  Memo2.Text := StrToHex(Memo1.Text, TEncoding.UTF8);
end;

procedure TMainForm.Button5Click(Sender: TObject);
begin
  //国标码 万=CDF2
  Memo2.Text := StrToHex(Memo1.Text, TEncoding.Default);
end;

procedure TMainForm.Button6Click(Sender: TObject);
begin
  //万=4E07=19975
  Memo2.Text :=StrToHex(Memo1.Text, TEncoding.BigEndianUnicode);
end;

procedure TMainForm.Button7Click(Sender: TObject);
begin
  ShowMessage(GB2Str(StrToInt(Memo1.Text))); {万}
end;

procedure TMainForm.Button8Click(Sender: TObject);
begin
  ShowMessage(Str2GB(Memo1.Text)); {4582}
end;

procedure TMainForm.Button9Click(Sender: TObject);
begin
  if IsChinese(Memo1.Text[1]) then
    ShowMessage('是')
  else
    ShowMessage('不是');
end;

procedure TMainForm.FormCreate(Sender: TObject);
var
  i: Integer;
begin
  ListBox1.Align := alLeft;

  i := 0;
  while i < 65535 do
  begin
    ListBox1.Items.Add(Format('#%d - #%d', [i, i+255]));
    Inc(i, 255);
  end;

  with StringGrid1 do
  begin
    Align := alClient;
    DefaultColWidth := 24;
    DefaultRowHeight := 24;
    RowCount := 16;
    ColCount := 16;
    FixedCols := 0;
    FixedRows := 0;
    Font.Size := 13;
  end;
end;

procedure TMainForm.ListBox1Click(Sender: TObject);
var
  w,i: Word;
begin
  w := ListBox1.ItemIndex * 255;
  for i := 0 to 255 do
    StringGrid1.Cells[i div 16, i mod 16] := Char(w + i);
end;

procedure TMainForm.Button10Click(Sender: TObject);
begin
  if HaveChinese(Memo1.Text) then
    ShowMessage('有')
  else
    ShowMessage('没有');
end;

end.

