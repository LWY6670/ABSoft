object MainForm: TMainForm
  Left = 341
  Top = 149
  Caption = #23383#31526#32534#30721#36716#25442'('#20165#25903#25345'D2009'#21450#20197#21518#29256#26412')'
  ClientHeight = 515
  ClientWidth = 667
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TMemo
    Left = 0
    Top = 0
    Width = 305
    Height = 46
    Align = alLeft
    Lines.Strings = (
      #19968)
    ScrollBars = ssVertical
    TabOrder = 0
  end
  object Memo2: TMemo
    Left = 305
    Top = 0
    Width = 362
    Height = 46
    Align = alClient
    ScrollBars = ssVertical
    TabOrder = 1
  end
  object Panel1: TPanel
    Left = 0
    Top = 46
    Width = 667
    Height = 38
    Align = alBottom
    TabOrder = 2
    object Button1: TButton
      Left = 16
      Top = 6
      Width = 73
      Height = 25
      Caption = 'To ASCII'
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 95
      Top = 6
      Width = 74
      Height = 25
      Caption = 'To Unicode'
      TabOrder = 1
      OnClick = Button2Click
    end
    object Button3: TButton
      Left = 175
      Top = 6
      Width = 82
      Height = 25
      Caption = 'To UTF7'
      TabOrder = 2
      OnClick = Button3Click
    end
    object Button4: TButton
      Left = 263
      Top = 6
      Width = 74
      Height = 25
      Caption = 'To UTF8'
      TabOrder = 3
      OnClick = Button4Click
    end
    object Button5: TButton
      Left = 343
      Top = 6
      Width = 96
      Height = 25
      Caption = 'To Default'
      TabOrder = 4
      OnClick = Button5Click
    end
    object Button6: TButton
      Left = 445
      Top = 6
      Width = 207
      Height = 25
      Caption = 'To BigEndianUnicode'
      TabOrder = 5
      OnClick = Button6Click
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 84
    Width = 667
    Height = 36
    Align = alBottom
    TabOrder = 3
    object Button7: TButton
      Left = 14
      Top = 6
      Width = 98
      Height = 25
      Caption = #21306#20301#30721#26597#27721#23383
      TabOrder = 0
      OnClick = Button7Click
    end
    object Button8: TButton
      Left = 118
      Top = 6
      Width = 98
      Height = 25
      Caption = #27721#23383#26597#21306#20301#30721
      TabOrder = 1
      OnClick = Button8Click
    end
    object Button9: TButton
      Left = 232
      Top = 6
      Width = 73
      Height = 25
      Caption = #26159#21542#20013#25991
      TabOrder = 2
      OnClick = Button9Click
    end
    object Button10: TButton
      Left = 320
      Top = 6
      Width = 81
      Height = 25
      Caption = #26159#21542#26377#20013#25991
      TabOrder = 3
      OnClick = Button10Click
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 120
    Width = 667
    Height = 354
    Align = alBottom
    Caption = 'Panel3'
    TabOrder = 4
    object ListBox1: TListBox
      Left = 1
      Top = 1
      Width = 120
      Height = 352
      Align = alLeft
      ItemHeight = 13
      TabOrder = 0
      OnClick = ListBox1Click
    end
    object StringGrid1: TStringGrid
      Left = 121
      Top = 1
      Width = 545
      Height = 352
      Align = alClient
      TabOrder = 1
    end
  end
  object Panel4: TPanel
    Left = 0
    Top = 474
    Width = 667
    Height = 41
    Align = alBottom
    Caption = 'Panel4'
    TabOrder = 5
    object Button11: TButton
      Left = 16
      Top = 6
      Width = 105
      Height = 25
      Caption = #27721#23383'TOUnicode'
      TabOrder = 0
      OnClick = Button11Click
    end
    object Button12: TButton
      Left = 127
      Top = 6
      Width = 114
      Height = 25
      Caption = 'UnicodeTO'#27721#23383
      TabOrder = 1
      OnClick = Button12Click
    end
    object Button13: TButton
      Left = 254
      Top = 7
      Width = 75
      Height = 25
      Caption = 'GetPyChar'
      TabOrder = 2
      OnClick = Button13Click
    end
    object Button14: TButton
      Left = 335
      Top = 6
      Width = 104
      Height = 25
      Caption = 'GetPyFitstChar'
      TabOrder = 3
      OnClick = Button14Click
    end
  end
end
