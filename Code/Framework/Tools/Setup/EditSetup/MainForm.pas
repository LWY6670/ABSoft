unit MainForm;

interface

uses
  ABPubFuncU,
  ABPubVarU,
  ABPubPassU,
  ABPubMessageU,

  ShlObj,Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls, jpeg,registry,shellapi, DdeMan, Buttons;

type
  TfrmEditSetup = class(TForm)
    Memo1: TMemo;
    Panel1: TPanel;
    btnFinish: TButton;
    Button1: TButton;
    procedure FormCreate(Sender: TObject);
    procedure btnFinishClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    FFileName:string;
    procedure LoadPassFileName(aFileName: string);
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmEditSetup: TfrmEditSetup;

implementation

{$R *.dfm}

procedure TfrmEditSetup.btnFinishClick(Sender: TObject);
var
  tempStr:string;
begin
  if ABCheckFileExists(FFileName) then
  begin
    tempStr:= ABDOPassWord(Memo1.Text);
    ABwritetxt(FFileName,tempStr);
    ABShow('保存成功');
  end;
end;

procedure TfrmEditSetup.FormCreate(Sender: TObject);
begin
  LoadPassFileName(ABAppPath+'setup.ini');
end;

procedure TfrmEditSetup.LoadPassFileName(aFileName:string);
begin
  if ABCheckFileExists(aFileName) then
  begin
    FFileName:= aFileName;
    Memo1.Lines.LoadFromFile(FFileName);
    Memo1.Text:= ABUnDOPassWord(Memo1.Text);
  end;
end;

procedure TfrmEditSetup.SpeedButton1Click(Sender: TObject);
var
  tempStr:string;
begin
  tempStr:=ABSelectFile('',emptystr,'加密 文件|*.*');
  if tempStr<>EmptyStr then
  begin
    LoadPassFileName(tempStr);
  end;
end;

end.
