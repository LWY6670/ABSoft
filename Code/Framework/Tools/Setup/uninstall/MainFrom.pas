unit MainFrom;

interface

uses
  ABPubFuncU,
  ABPubVarU,
  ABPubConstU,

  ShlObj,Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls, jpeg,registry,shellapi;

type
  Tfrmuninstall = class(TForm)
    Animate1: TAnimate;
    ProgressBar: TProgressBar;
    btnFinish: TButton;
    procedure btnFinishClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmuninstall: Tfrmuninstall;

implementation

{$R *.dfm}

procedure Tfrmuninstall.btnFinishClick(Sender: TObject);
var
  Ts : TStringList;
  myreg:tregistry;
  tempSetupPath:string;
begin
  Animate1.Active:=True;
  myreg:=tregistry.Create;
  ProgressBar.Max:=12;
  ProgressBar.Position:=0;
  try
    //在删除程序中删除反安装
    myreg.RootKey:=hkey_local_machine;
    myreg.OpenKey('software\Armous',true);
    tempSetupPath:=Trim(myreg.ReadString('SetupPath'));
    myreg.CloseKey;

    ProgressBar.Position:=ProgressBar.Position+1;

    //删除桌面快捷方式
    ABDeleteFile(ABGetShortCutTypePath(CSIDL_DESKTOP)+'一卡通客户端.lnk');
    //删除开始快捷方式
    ABDeltree(ABGetShortCutTypePath(CSIDL_COMMON_PROGRAMS)+'Armous一卡通');
    ProgressBar.Position:=ProgressBar.Position+1;

    SetCurrentDir(ABAppPath+'Client\');
    ABWaitAppEnd('taskkill /f /im WaukeenCliP.exe',SW_Hide);
    ProgressBar.Position:=ProgressBar.Position+1;

    SetCurrentDir(ABAppPath+'Access\');
    ABWaitAppEnd('taskkill /f /im CtrlSrv.exe',SW_Hide);
    ProgressBar.Position:=ProgressBar.Position+1;

    SetCurrentDir(ABAppPath+'Consume\');
    ABWaitAppEnd(ABAppPath+'Consume\Uninstall.bat',sw_shownormal);

    ProgressBar.Position:=ProgressBar.Position+1;

    SetCurrentDir(ABAppPath+'SoftDog\');
    ABWaitAppEnd(ABAppPath+'SoftDog\Uninstall.bat',sw_shownormal);
    ProgressBar.Position:=ProgressBar.Position+1;

    ABDeltree(ABAppPath+'Client\');
    ProgressBar.Position:=ProgressBar.Position+1;
    ABDeltree(ABAppPath+'Access\');
    ProgressBar.Position:=ProgressBar.Position+1;
    ABDeltree(ABAppPath+'Consume\');
    ProgressBar.Position:=ProgressBar.Position+1;
    ABDeltree(ABAppPath+'SoftDog\');
    ProgressBar.Position:=ProgressBar.Position+1;
    ABDeltree(ABAppPath+'ABSoft_Set\');
    ProgressBar.Position:=ProgressBar.Position+1;

    myreg.RootKey:=hkey_local_machine;
    myreg.deleteKey('software\microsoft\windows\currentversion\uninstall\Armous');
    myreg.deletekey('software\Armous');
    myreg.CloseKey;

    ProgressBar.Position:=ProgressBar.Position+1;
    //删除自己和目录
    Ts := TStringList.Create;
    Ts.Add('@echo off');
    //延迟1秒执行bat
    Ts.Add('ping 0.0.0.1 -n 1 -w 1000>nul');
    Ts.Add('del "' + Application.ExeName+'"');
    Ts.Add('CD \');
    Ts.Add('RD /s/q "'+ExtractFileDir(Application.ExeName)+'"');
    Ts.Add('del %0');
    Ts.Add('exit');
    Ts.SaveToFile(changefileext(Application.ExeName, '.bat'));
    Ts.Free;
    Shellexecute(Handle,'Open',PChar(changefileext(Application.ExeName, '.bat')),'',nil, SW_HIDE);
    Application.Terminate
  finally
    myreg.Free;
    Animate1.Active:=False;
  end;
end;

end.
