object MainForm: TMainForm
  Left = 241
  Top = 135
  BorderStyle = bsDialog
  Caption = 'Armous'#19968#21345#36890#23433#35013#31243#24207
  ClientHeight = 405
  ClientWidth = 600
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 600
    Height = 69
    Align = alTop
    BevelOuter = bvNone
    Color = clWhite
    ParentBackground = False
    TabOrder = 0
    object label_Title: TLabel
      Left = 32
      Top = 16
      Width = 289
      Height = 13
      AutoSize = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object imgleft: TImage
      Left = 534
      Top = 4
      Width = 62
      Height = 59
      AutoSize = True
      Picture.Data = {
        0A544A504547496D61676575080000FFD8FFE000104A46494600010001006000
        600000FFFE001F4C45414420546563686E6F6C6F6769657320496E632E205631
        2E303100FFDB00840008050607060508070607090808090C140D0C0B0B0C1811
        120E141D191E1E1C191C1B20242E2720222B221B1C2836282B2F313334331F26
        383C38323C2E323331010809090C0A0C170D0D1731211C213131313131313131
        3131313131313131313131313131313131313131313131313131313131313131
        31313131313131313131FFC401A2000001050101010101010000000000000000
        0102030405060708090A0B010003010101010101010101000000000000010203
        0405060708090A0B100002010303020403050504040000017D01020300041105
        122131410613516107227114328191A1082342B1C11552D1F02433627282090A
        161718191A25262728292A3435363738393A434445464748494A535455565758
        595A636465666768696A737475767778797A838485868788898A929394959697
        98999AA2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3
        D4D5D6D7D8D9DAE1E2E3E4E5E6E7E8E9EAF1F2F3F4F5F6F7F8F9FA1100020102
        0404030407050404000102770001020311040521310612415107617113223281
        08144291A1B1C109233352F0156272D10A162434E125F11718191A262728292A
        35363738393A434445464748494A535455565758595A636465666768696A7374
        75767778797A82838485868788898A92939495969798999AA2A3A4A5A6A7A8A9
        AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE2E3E4E5
        E6E7E8E9EAF2F3F4F5F6F7F8F9FAFFC0001108003B003E030111000211010311
        01FFDA000C03010002110311003F00E9F53D46DF468749B68B44D1E75974B827
        9249ECC3BB3B641C9C8CF4AEEF7A5297BCF77D4C2E925A10DB788ADCE31A0E82
        87DAC40FFD9A8B4BF99FDE175D8D1835A81FFE60FA30FA590FFE2AA6D2FE67F7
        8EFE45A8F5385BAE95A47E1663FF008AA5EF777F785FC8B2925B4D80DA4E907E
        B620FF00ECD53792EAFEF1FC8A5A85FE9D6A485D1B45623D6C17FF008AAA5CEF
        ED3FBC4DAEC62CDE298A37DB0E83A113FF005E3FFD955F2CBF99FDE2E6F23535
        E8EDEF3C0BA7EA2BA7D95A5CCD7255CDB4023181E60C7AF61DE9D16FDA34D8DF
        C3733FC4B6E669F4A5033B745B7FE6D52B797AB13E9E873F6F6574F722182092
        4627036A939AA724B72523B1D1FC2379200D764C23D3A1AC2555742D44E9ACFC
        3B696E801F9D8772335939B655917574EB6036AC6A3E8A2A799AEA330759F085
        B6A058DBCD240DEA0E466AD56E5DC1D3D2E71D77E1BBAD2E731CC9BC750EA3AD
        7446A296C66E0D2BF43A2D4E32BF0EB4D4C74BB6FF00D0A5A74BF8AFD3FC86FE
        1425CC5E65F69E02EF76D1AD511075662CD81512972DFD58ED7B7A1D4E8FA4C5
        A6423015A723E7900FD17D07F3AE66DBDCAD8B577730D95ACB757522C504085E
        476E8A077A4073BA578B86AD7005B42B6B6C7959673F311D8EDED9A7663D0D2F
        ED85F3442EA40738122F21BFFD759CE2F72A2D2D49CDD0C60E140E80726B274A
        B4BE1D049B6EECCDBCB78E5CBE1F70C903A56B4B0CE9BE672D4E975DB8385B42
        86B6D1BF816C5A3E51AEDCAFD33257751D6A3F4FF238A5F091F9891F88F48467
        DA7FB1A265E703237F3FA9ACE6D26DCBBB34A69B768ABE874303CCE32B3703D3
        9AE39E2A8C5D96A69284A3BA29F893497F1068179A55C4F24115CA806518CA90
        7238EFCF6A5ED5CB6899ABC9D9239CF0F785B57D2A1960B9D3E1B969093E7C37
        6514E7D54F3F864D74AAAE4AE897149D8E974AD07ECF219AE18025D5C428728A
        40C0A4E4D82D36360C311EB1AFE54AEC7CCCE6FC75ACDB787F4AC431AC97F759
        8EDA2E4E4F7623D00E6AA3793B0F9DA4664993F0C348DC4E45C30E7D8CA2BAA9
        2B556BCBFC8CA4FDD32BC5E25377A44F6AE12EA0D22DE4889E84E5B20FB1159C
        E0A6A4BCD9509BA72524741E1AF19E893DAC697F2AE9779D1A3BA7DA18FF00B2
        C783F85717B1E4D91B4AABA8EED9D3DADD5B5D7EF22B886651D3648187E94F97
        4B09CADA22C647A8FCEAF6332ADDEABA6D9216BCD42D2DD47532CCAA07E66803
        93F117C46B3B43F66D0E23A8DCB0F965FBB02FBEFEFF00866AE34E4C2E91C9C1
        29BAB97BDD527FB65F4BC34A46028FEEA0FE15FF0026BA630E4466DDCEBB510B
        FF000AF34ED9F77ED6F8FF00BEA5A74BF8AFD3FC872F81183E2C936DCE907D74
        5B7FE6D4D6F2F56297430ED6D96EE5FDF20743FC2C3229B251D269DE19D2A0B7
        334569E5487BC52327E808AC1C55CD13650D52C176322C97014F18170E3FAD5A
        847B0AECE7134EB7B0BB17115BA9753D5FE63F99AB518AE84DD9D2F9169ACDBA
        B711DC63A8FE2346B10DCC8B9B2B9B090AB0254552698B63B3B97CFC32D29BD6
        E9BFF4296A697F15FA7F9152F81106AFE19D5B588B45BAB0B6F3601A45BC65BC
        C55F9864918247622A79E319493EEC6E2DA5624B0F07EAF011BEC88C7FD358FF
        00F8AA4EA47B8283379346D412DC27D91F3FEFA7FF001559F322B9599B79E19D
        565CECB26FFBF89FFC5552A9142E56655CF82B5B933B6C4FFDFD8FFF008AAA55
        23DC5C8CAF6FE0BF12DB4BBA2B0E33DE78FF00F8AA7ED21DC5C923722F0FEB13
        C423BED349007DE12C79FF00D0AA39E2B66572B25F1369EFA7F81EC6CE4428D1
        DD93B4904804C8474E3A11574257A8DF90A6AD1B1E7F06B9ABC5124516AB7D1C
        6802AA2DC380A0740067A5773A706F6465CCFB85C78875B0A98D6350193DAE9F
        FC69AA50FE540E4FB97EC35DD5DD46ED56F8FD6E5FFC6A5D387641CCFB932EB7
        AB79AC3FB52F700FFCFC3FF8D4FB387643E67DCA1A56A9A8C3AF5D3C5A8DEA9B
        CB9B833FFA43E1CC70588438CE010247191C918CE768C734631F6AD5B434BBE5
        4CDE4D6353E7FE26377D7FE7BB7F8D6AE11EC4733EE2FF006BEA7FF411BBFF00
        BFEDFE347247B05DF71AD7B77763CBBABA9E741F3059242C01F5E4D3E54B6417
        BEE7FFD9}
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 369
    Width = 600
    Height = 36
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object PriButton: TButton
      Left = 276
      Top = 2
      Width = 90
      Height = 25
      Caption = #19978#19968#27493
      TabOrder = 0
      OnClick = PriButtonClick
    end
    object NextButton: TButton
      Left = 372
      Top = 2
      Width = 90
      Height = 25
      Caption = #19979#19968#27493
      TabOrder = 1
      OnClick = NextButtonClick
    end
    object ExitButton: TButton
      Left = 497
      Top = 2
      Width = 90
      Height = 25
      Caption = #36864#20986
      TabOrder = 2
      OnClick = ExitButtonClick
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 347
    Width = 600
    Height = 22
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object label_ShortTitle: TLabel
      Left = 3
      Top = 7
      Width = 80
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Armous'#19968#21345#36890
    end
    object Bevel1: TBevel
      Left = 87
      Top = 12
      Width = 509
      Height = 2
    end
  end
  object cxPageControl1: TcxPageControl
    Left = 0
    Top = 69
    Width = 600
    Height = 278
    Align = alClient
    TabOrder = 3
    Properties.ActivePage = cxTabSheet2
    Properties.ActivateFocusedTab = False
    Properties.CustomButtons.Buttons = <>
    Properties.Style = 6
    LookAndFeel.NativeStyle = True
    OnChange = cxPageControl1Change
    ClientRectBottom = 278
    ClientRectRight = 600
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = #35768#21487#21327#35758
      ImageIndex = 0
      DesignSize = (
        600
        254)
      object Label_license: TLabel
        Left = 17
        Top = 15
        Width = 570
        Height = 233
        Anchors = [akLeft, akTop, akRight, akBottom]
        AutoSize = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        Layout = tlCenter
        WordWrap = True
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = #32452#20214#19982#30446#24405#36873#25321
      ImageIndex = 1
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 600
        Height = 254
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object GroupBox1: TGroupBox
          Left = 0
          Top = 0
          Width = 241
          Height = 229
          Align = alLeft
          Caption = #23433#35013#21151#33021#36873#25321
          TabOrder = 0
          object ABCheckTreeView1: TABCheckTreeView
            Left = 2
            Top = 15
            Width = 237
            Height = 159
            MoveAfirm = False
            Align = alClient
            Ctl3D = True
            Flatness = cfAlwaysFlat
            GrayedIsChecked = False
            HideSelection = False
            Indent = 19
            Items.NodeData = {
              0303000000240000000000000000000000FFFFFFFFFFFFFFFF00000000000000
              0000000000010370656E63935E260000000000000000000000FFFFFFFFFFFFFF
              FF0000000000000000060000000104945E28759F52FD80260000000000000000
              000000FFFFFFFFFFFFFFFF0000000000000000000000000104E8958179FB7CDF
              7E260000000000000000000000FFFFFFFFFFFFFFFF0000000000000000000000
              000104E15DF466FB7CDF7E260000000000000000000000FFFFFFFFFFFFFFFF00
              00000000000000000000000104BF8BA25BFB7CDF7E2A00000000000000000000
              00FFFFFFFFFFFFFFFF00000000000000000000000001061A4FAE8B7E7B3052FB
              7CDF7E260000000000000000000000FFFFFFFFFFFFFFFF000000000000000000
              0000000104886D398DFB7CDF7E280000000000000000000000FFFFFFFFFFFFFF
              FF0000000000000000000000000105A052C65BD7720D67A15228000000000000
              0000000000FFFFFFFFFFFFFFFF0000000000000000000000000105A25B3762EF
              7A6F8FF64E}
            ParentCtl3D = False
            ReadOnly = True
            TabOrder = 0
            OnChange = ABCheckTreeView1Change
            OnClick = ABCheckTreeView1Click
            OnCollapsing = ABCheckTreeView1Collapsing
            CheckNodesData = {010101010101010101010101010101010101}
          end
          object Panel10: TPanel
            Left = 2
            Top = 174
            Width = 237
            Height = 53
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 1
            object RadioGroup4: TRadioGroup
              Left = 0
              Top = 0
              Width = 237
              Height = 53
              Align = alClient
              Caption = #24555#36895#36873#25321
              Columns = 2
              Items.Strings = (
                #20165#38376#31105
                #20165#28040#36153
                #20165#23458#25143#31471
                #20840#37096)
              TabOrder = 0
              OnClick = RadioGroup4Click
            end
          end
        end
        object Panel5: TPanel
          Left = 0
          Top = 229
          Width = 600
          Height = 25
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 1
          DesignSize = (
            600
            25)
          object Label5: TLabel
            Left = 0
            Top = 3
            Width = 77
            Height = 20
            Alignment = taCenter
            AutoSize = False
            Caption = #23433#35013#30446#24405#65306
            Layout = tlCenter
          end
          object SpeedButton1: TSpeedButton
            Left = 570
            Top = 2
            Width = 24
            Height = 23
            Anchors = [akTop, akRight]
            Caption = '...'
            OnClick = SpeedButton1Click
            ExplicitLeft = 564
          end
          object Edit1: TcxTextEdit
            Left = 83
            Top = 3
            Anchors = [akLeft, akTop, akRight]
            TabOrder = 0
            Width = 486
          end
        end
        object GroupBox2: TGroupBox
          Left = 241
          Top = 0
          Width = 359
          Height = 229
          Align = alClient
          Caption = #35828#26126
          TabOrder = 2
          object cxPageControl2: TcxPageControl
            Left = 10
            Top = 17
            Width = 340
            Height = 197
            TabOrder = 0
            Properties.ActivePage = cxTabSheet10
            Properties.CustomButtons.Buttons = <>
            Properties.Style = 3
            LookAndFeel.NativeStyle = True
            ClientRectBottom = 197
            ClientRectRight = 340
            ClientRectTop = 23
            object cxTabSheet13: TcxTabSheet
              Caption = 'cxTabSheet13'
              ImageIndex = 6
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object Label1: TLabel
                Left = 0
                Top = 0
                Width = 336
                Height = 32
                Align = alClient
                Caption = '    '#25968#25454#24211#26159#19968#21345#36890#31995#32479#30340#21518#21488#65292#21482#21487#22312#25968#25454#24211#26381#21153#22120#19978#31532#19968#27425#23433#35013#26102#36873#25321#12290
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                Layout = tlCenter
                WordWrap = True
              end
            end
            object cxTabSheet7: TcxTabSheet
              Caption = 'cxTabSheet7'
              ImageIndex = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object Label2: TLabel
                Left = 0
                Top = 0
                Width = 336
                Height = 64
                Align = alClient
                Caption = 
                  '    '#38376#31105#31995#32479#20134#31216#20986#20837#21475#25511#21046#31995#32479#65292#20027#35201#29992#20110#21150#20844#22823#27004#12289#20844#21496#12289#38134#34892#35777#21048#31561#21333#20301#30340#26234#33021#21270#31649#29702#65292#21487#26377#25928#30340#31649#29702#20154#21592#36827#20986#12289#38450#27490#27809#26377#25480#26435#30340#20154#36827 +
                  #20837#39640#20445#23433#21306#22495#21450#36827#34892#21306#22495#20154#21592#32479#35745#31649#29702#31561#65292#30830#20445#21150#20844#22823#27004#12289#20844#21496#12289#21333#20301#31561#22330#25152#23433#20840#39640#25928#30340#36816#20316#12290
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                Layout = tlCenter
                WordWrap = True
              end
            end
            object cxTabSheet8: TcxTabSheet
              Caption = 'cxTabSheet8'
              ImageIndex = 1
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object Label14: TLabel
                Left = 0
                Top = 0
                Width = 339
                Height = 80
                Align = alClient
                Caption = 
                  '    '#24033#26356#31995#32479#20854#23454#23601#26159#38376#31105#31995#32479#30340#19968#20010#21464#31181#65292#19968#31181#23545#38376#31105#31995#32479#30340#28789#27963#36816#29992#12290#24033#26356#20449#24687#28857#30456#24403#20110#38376#31105#31995#32479#37324#30340#21345#29255','#24033#26356#26834#23601#30456#24403#20110#35835#21345#22120#21644#20648 +
                  #23384#22120#12290#24033#26356#31995#32479#30456#24403#20110#32771#21220#65292#22312#35268#23450#30340#26102#38388#25353#35268#23450#30340#36335#32447#21435#35835#21462#35268#23450#30340#19968#20010#20010#24033#26356#20449#24687#28857#65292#23436#25104#23545#23433#38450#20154#21592#30340#24037#20316#32771#21220#12290
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                Layout = tlCenter
                WordWrap = True
              end
            end
            object cxTabSheet9: TcxTabSheet
              Caption = 'cxTabSheet9'
              ImageIndex = 2
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object Label3: TLabel
                Left = 0
                Top = 0
                Width = 340
                Height = 112
                Align = alClient
                Caption = 
                  '    '#35775#23458#31995#32479#26159#21033#29992#29616#20195#21270#20449#24687#25216#26415#65292#30495#27491#20570#21040#20154#21592#12289#35777#20214#12289#29031#29255#19977#32773#32479#19968#12290#23454#29616#20102#8220#36827#38376#30331#35760#12289#20986#38376#27880#38144#12289#20154#20687#19968#19968#23545#24212#12289#38543#36523#29289#21697#30331#35760#12289 +
                  #20998#32423#31649#29702#12289#21382#21490#35760#24405#26597#35810#12289#25253#34920#27719#24635#8221#31561#21151#33021#65292#33021#22815#39640#25928#35760#24405#12289#23384#20648#12289#26597#35810#27719#24635#35775#23458#30340#30456#20851#20449#24687#12290#25552#39640#20102#38376#21355#31649#29702#24037#20316#30340#36136#37327#21644#25928#29575#65292#21152#24378#21333#20301#23433 +
                  #20840#20445#38556#65292#21516#26102#20063#39034#24212#20102#26102#20195#30340#35201#27714#65292#23454#29616#30005#23376#21270'/'#25968#23383#21270#12289#20449#24687#21270'/'#20154#38450#19982#25216#38450#30456#32467#21512#65292#25552#21319#21333#20301#20449#24687#21270#21150#20844#24418#35937#21644#32508#21512#23454#21147#12290
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                Layout = tlCenter
                WordWrap = True
              end
            end
            object cxTabSheet10: TcxTabSheet
              Caption = 'cxTabSheet10'
              ImageIndex = 3
              object Label4: TLabel
                Left = 0
                Top = 0
                Width = 340
                Height = 174
                Align = alClient
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                Layout = tlCenter
                WordWrap = True
                ExplicitWidth = 3
                ExplicitHeight = 16
              end
            end
            object cxTabSheet11: TcxTabSheet
              Caption = 'cxTabSheet11'
              ImageIndex = 4
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object Label6: TLabel
                Left = 0
                Top = 0
                Width = 3
                Height = 16
                Align = alClient
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                Layout = tlCenter
                WordWrap = True
              end
            end
            object cxTabSheet14: TcxTabSheet
              Caption = 'cxTabSheet14'
              ImageIndex = 7
              object Label16: TLabel
                Left = 0
                Top = 0
                Width = 340
                Height = 174
                Align = alClient
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                Layout = tlCenter
                WordWrap = True
                ExplicitWidth = 3
                ExplicitHeight = 16
              end
            end
            object cxTabSheet12: TcxTabSheet
              Caption = 'cxTabSheet12'
              ImageIndex = 5
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object Label7: TLabel
                Left = 0
                Top = 0
                Width = 336
                Height = 32
                Align = alClient
                Caption = '    '#23458#25143#31471#26159#29992#26469#31649#29702#25110#26174#31034#19968#21345#36890#25968#25454#30340#36733#20307#65292#25805#20316#21592#30331#24405#23458#25143#31471#21518#21487#35843#29992#20998#37197#32473#27492#25805#20316#21592#30340#25152#26377#21151#33021#27169#22359#12290
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                Layout = tlCenter
                WordWrap = True
              end
            end
            object cxTabSheet15: TcxTabSheet
              Caption = 'cxTabSheet15'
              ImageIndex = 8
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
            end
          end
        end
      end
    end
    object cxTabSheet5: TcxTabSheet
      Caption = #25968#25454#24211#36830#25509
      ImageIndex = 4
      object Label9: TLabel
        Left = 16
        Top = 16
        Width = 57
        Height = 20
        AutoSize = False
        Caption = #26381#21153#22120#65306
        Layout = tlCenter
      end
      object SpeedButton2: TSpeedButton
        Left = 236
        Top = 14
        Width = 42
        Height = 25
        Caption = #21047#26032
        OnClick = SpeedButton2Click
      end
      object Label11: TLabel
        Left = 48
        Top = 149
        Width = 50
        Height = 20
        AutoSize = False
        Caption = #30331#24405#21517#65306
        Layout = tlCenter
      end
      object Label12: TLabel
        Left = 48
        Top = 177
        Width = 50
        Height = 20
        AutoSize = False
        Caption = #23494#30721#65306
        Layout = tlCenter
      end
      object Label10: TLabel
        Left = 16
        Top = 43
        Width = 57
        Height = 20
        AutoSize = False
        Caption = #25968#25454#24211#65306
        Layout = tlCenter
      end
      object SpeedButton3: TSpeedButton
        Left = 236
        Top = 41
        Width = 42
        Height = 25
        Caption = #21047#26032
        OnClick = SpeedButton3Click
      end
      object ComboBox1: TcxComboBox
        Left = 71
        Top = 16
        TabOrder = 0
        Text = 'ComboBox1'
        Width = 159
      end
      object Edit2: TcxTextEdit
        Left = 104
        Top = 148
        TabOrder = 1
        Width = 118
      end
      object Edit3: TcxTextEdit
        Left = 104
        Top = 176
        TabOrder = 2
        Width = 118
      end
      object ComboBox2: TcxComboBox
        Left = 71
        Top = 43
        TabOrder = 3
        Text = 'ComboBox1'
        Width = 159
      end
      object RadioGroup5: TRadioGroup
        Left = 16
        Top = 73
        Width = 505
        Height = 60
        Caption = #39564#35777#26041#24335
        ItemIndex = 0
        Items.Strings = (
          'Windows '#39564#35777
          #20351#29992#20197#19979#30331#24405#21517#21644#23494#30721#36827#34892#25968#25454#24211#26381#21153#22120#39564#35777)
        TabOrder = 4
        OnClick = RadioGroup5Click
      end
    end
    object cxTabSheet4: TcxTabSheet
      Caption = #24320#22987#23433#35013
      ImageIndex = 3
      object Label8: TLabel
        Left = 0
        Top = 41
        Width = 600
        Height = 179
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Layout = tlCenter
        WordWrap = True
        ExplicitWidth = 3
        ExplicitHeight = 16
      end
      object Label18: TLabel
        Left = 0
        Top = 0
        Width = 600
        Height = 41
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -24
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        Layout = tlCenter
        WordWrap = True
      end
      object ProgressBar1: TProgressBar
        Left = 0
        Top = 237
        Width = 600
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
      object ProgressBar2: TProgressBar
        Left = 0
        Top = 220
        Width = 600
        Height = 17
        Align = alBottom
        TabOrder = 1
      end
    end
    object cxTabSheet6: TcxTabSheet
      Caption = #24555#25463#19982#21551#21160#39033#35774#32622
      ImageIndex = 5
      object Panel7: TPanel
        Left = 0
        Top = 0
        Width = 600
        Height = 254
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label17: TLabel
          Left = 64
          Top = 72
          Width = 473
          Height = 113
          Alignment = taCenter
          AutoSize = False
          Caption = #23433#35013#23436#25104
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          Layout = tlCenter
        end
        object chbrun: TCheckBox
          Left = 16
          Top = 15
          Width = 233
          Height = 17
          Caption = #23433#35013#23436#25104','#33258#21160#36816#34892#26381#21153#25110#23458#25143#31471
          Checked = True
          State = cbChecked
          TabOrder = 0
        end
      end
    end
  end
  object ADOConnection1: TADOConnection
    Left = 552
    Top = 248
  end
end
