object MainForm: TMainForm
  Left = 228
  Top = 178
  Caption = #20013'<->'#33521#32763#35793
  ClientHeight = 389
  ClientWidth = 651
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 651
    Height = 65
    Align = alTop
    TabOrder = 0
    DesignSize = (
      651
      65)
    object Label1: TLabel
      Left = 16
      Top = 21
      Width = 33
      Height = 13
      AutoSize = False
      Caption = #36755#20837
    end
    object Button1: TButton
      Left = 506
      Top = 9
      Width = 110
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #26597#35810
      TabOrder = 0
      OnClick = Button1Click
    end
    object CheckBox1: TCheckBox
      Left = 490
      Top = 40
      Width = 145
      Height = 17
      Anchors = [akTop, akRight]
      Caption = #29992'WORD'#36827#34892#31616#32321#32763#35793
      TabOrder = 1
    end
    object Memo1: TMemo
      Left = 55
      Top = 13
      Width = 429
      Height = 39
      Anchors = [akLeft, akTop, akRight]
      ImeMode = imChinese
      ScrollBars = ssVertical
      TabOrder = 2
      OnKeyPress = Memo1KeyPress
    end
  end
  object RadioGroup1: TRadioGroup
    Left = 0
    Top = 344
    Width = 651
    Height = 45
    Align = alBottom
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      #20013#25991'->'#33521#35821
      #33521#35821'->'#20013#25991)
    TabOrder = 1
  end
  object Memo2: TMemo
    Left = 0
    Top = 65
    Width = 651
    Height = 279
    Align = alClient
    ReadOnly = True
    TabOrder = 2
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 65
    Width = 651
    Height = 279
    Align = alClient
    DataSource = DataSource1
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'word'
        Title.Caption = #33521#25991
        Width = 126
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'explain'
        Title.Caption = #20013#25991
        Width = 478
        Visible = True
      end>
  end
  object ADOConnection1: TADOConnection
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 528
    Top = 152
  end
  object ADOQuery1: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    Filter = 'explain like %ii%'
    Filtered = True
    Parameters = <>
    SQL.Strings = (
      'select * from word order by difficult')
    Left = 528
    Top = 200
  end
  object DataSource1: TDataSource
    DataSet = ADOQuery1
    Left = 528
    Top = 120
  end
end
