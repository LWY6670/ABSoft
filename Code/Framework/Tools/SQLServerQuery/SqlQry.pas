unit SqlQry;

interface

uses


  Windows,Messages,SysUtils,Variants,Classes,Graphics,Controls,Forms,Dialogs,
  StdCtrls,ExtCtrls,ComCtrls,ADOInt,Pub,ActiveX,OleDb,Menus;

type
  TSqlQryFrm = class(TForm)
    Pgc1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Splitter1: TSplitter;
    MoSql: TMemo;
    SbxResult: TScrollBox;
    MoMsg: TMemo;
    SBar: TStatusBar;
    PnlMain: TPanel;
    OpenDialog1: TOpenDialog;
    SaveDialog1: TSaveDialog;
    PnlResult: TPanel;
    GridMenu: TPopupMenu;
    MnCopyGridText: TMenuItem;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure PnlMainResize(Sender: TObject);
    procedure Splitter1Moved(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure MnCopyGridTextClick(Sender: TObject);
  private
    FConnection: _Connection;

//    FUserName: string;
//    FServer: string;
//    FPassword: string;
//    FAuthType: TAuthType;
    FDatabase: string;

    FFileName: string;        // 当前打开的文件

    FExecuting: Boolean;      // 当前是否在执行SQL?
    FExecThread: TThread;     // 执行的线程.
    FParseOnly: Boolean;      // 仅分析, 不执行.
    FCanceling: Boolean;      // 取消按钮已经按下.
    FConnClosed: Boolean;     // 连接已经中断
    
    FServerName: string;
    FSUserName: string;

    FPnlRate: Double;

    function ExecuteSql(const ASql: WideString): Integer;
    function ExecuteRst(const ASql: WideString): _Recordset;

    procedure ClearResults;
    procedure SetDatabase(const Value: string);
    procedure UpdateTitle;
    procedure UpdateDatabase;

    procedure ProcessResults;
    procedure AddMsg(Msg: string);
    procedure AddMsgs(MsgList: TStringList);
    procedure AddRecordsets(DataList: TList);
    procedure ClearVarRef;

    procedure OnThreadTerminate(Sender: TObject);
    procedure OnGridResize(Sender: TObject);

    function GetResultBoxVisible: Boolean;
    function GetConnectionString: string;
  public
//    property Server: string read FServer write FServer;
//    property UserName: string read FUserName write FUserName;
//    property Password: string read FPassword write FPassword;
//    property AuthType: TAuthType read FAuthType write FAuthType;
    property Database: string read FDatabase write SetDatabase;
    property ConnectionString: string read GetConnectionString;

    property ServerName: string read FServerName;
    property SUserName: string read FSUserName;

    // True:当前正在执行SQL
    property Executing: Boolean read FExecuting;

    procedure LoadFile;
    function SaveFile: Boolean;
    function SaveAs: Boolean;

    procedure Initialize(Conn: _Connection);

    procedure Execute;
    procedure ParseSQL;
    procedure CancelExecute;

    procedure ToggleResultBox;
    property ResultBoxVisible: Boolean read GetResultBoxVisible;

    function GetDatabaseList: TStringList;
  end;

var
  SqlQryFrm: TSqlQryFrm;

implementation

uses DataGrid, RowData, Clipbrd, ComObj;

{$R *.dfm}

type
// ADO stdcall interface
  ADOStd = interface;
  ConnectionStd15 = interface;
  CommandStd15 = interface;
  CommandStd = interface;
  RecordsetStd15 = interface;
  RecordsetStd = interface;

  ADOStd = interface(IDispatch)
    ['{00000534-0000-0010-8000-00AA006D2EA4}']
    function Get_Properties(out ppvObject: Properties): HResult; stdcall;
  end;

  ConnectionStd15 = interface(_ADO)
    ['{00000515-0000-0010-8000-00AA006D2EA4}']
    function Get_ConnectionString(out pbstr: WideString): HResult; stdcall;
    function Set_ConnectionString(const pbstr: WideString): HResult; stdcall;
    function Get_CommandTimeout(out plTimeout: Integer): HResult; stdcall;
    function Set_CommandTimeout(plTimeout: Integer): HResult; stdcall;
    function Get_ConnectionTimeout(out plTimeout: Integer): HResult; stdcall;
    function Set_ConnectionTimeout(plTimeout: Integer): HResult; stdcall;
    function Get_Version(out pbstr: WideString): HResult; stdcall;
    function Close: HResult; stdcall;
    function Execute(const CommandText: WideString; out RecordsAffected: OleVariant; 
                     Options: Integer; out ppiRset: _Recordset): HResult; stdcall;
    function BeginTrans(out TransactionLevel: Integer): HResult; stdcall;
    function CommitTrans: HResult; stdcall;
    function RollbackTrans: HResult; stdcall;
    function Open(const ConnectionString: WideString; const UserID: WideString; 
                  const Password: WideString; Options: Integer): HResult; stdcall;
    function Get_Errors(out ppvObject: Errors): HResult; stdcall;
    function Get_DefaultDatabase(out pbstr: WideString): HResult; stdcall;
    function Set_DefaultDatabase(const pbstr: WideString): HResult; stdcall;
    function Get_IsolationLevel(out Level: IsolationLevelEnum): HResult; stdcall;
    function Set_IsolationLevel(Level: IsolationLevelEnum): HResult; stdcall;
    function Get_Attributes(out plAttr: Integer): HResult; stdcall;
    function Set_Attributes(plAttr: Integer): HResult; stdcall;
    function Get_CursorLocation(out plCursorLoc: CursorLocationEnum): HResult; stdcall;
    function Set_CursorLocation(plCursorLoc: CursorLocationEnum): HResult; stdcall;
    function Get_Mode(out plMode: ConnectModeEnum): HResult; stdcall;
    function Set_Mode(plMode: ConnectModeEnum): HResult; stdcall;
    function Get_Provider(out pbstr: WideString): HResult; stdcall;
    function Set_Provider(const pbstr: WideString): HResult; stdcall;
    function Get_State(out plObjState: Integer): HResult; stdcall;
    function OpenSchema(Schema: SchemaEnum; Restrictions: OleVariant; SchemaID: OleVariant; 
                        out pprset: _Recordset): HResult; stdcall;
  end;

  ConnectionStd = interface(ConnectionStd15)
    ['{00000550-0000-0010-8000-00AA006D2EA4}']
    function Cancel: HResult; stdcall;
  end;

  CommandStd15 = interface(ADOStd)
    ['{00000508-0000-0010-8000-00AA006D2EA4}']
    function Get_ActiveConnection(out ppvObject: _Connection): HResult; stdcall;
    function _Set_ActiveConnection(const ppvObject: _Connection): HResult; stdcall;
    function Set_ActiveConnection(ppvObject: OleVariant): HResult; stdcall;
    function Get_CommandText(out pbstr: WideString): HResult; stdcall;
    function Set_CommandText(const pbstr: WideString): HResult; stdcall;
    function Get_CommandTimeout(out pl: Integer): HResult; stdcall;
    function Set_CommandTimeout(pl: Integer): HResult; stdcall;
    function Get_Prepared(out pfPrepared: WordBool): HResult; stdcall;
    function Set_Prepared(pfPrepared: WordBool): HResult; stdcall;
    function Execute(out RecordsAffected: OleVariant; var Parameters: OleVariant; Options: Integer; 
                     out ppiRs: _Recordset): HResult; stdcall;
    function CreateParameter(const Name: WideString; Type_: DataTypeEnum; 
                             Direction: ParameterDirectionEnum; Size: Integer; Value: OleVariant; 
                             out ppiprm: _Parameter): HResult; stdcall;
    function Get_Parameters(out ppvObject: Parameters): HResult; stdcall;
    function Set_CommandType(plCmdType: CommandTypeEnum): HResult; stdcall;
    function Get_CommandType(out plCmdType: CommandTypeEnum): HResult; stdcall;
    function Get_Name(out pbstrName: WideString): HResult; stdcall;
    function Set_Name(const pbstrName: WideString): HResult; stdcall;
  end;

  CommandStd = interface(CommandStd15)
    ['{0000054E-0000-0010-8000-00AA006D2EA4}']
    function Get_State(out plObjState: Integer): HResult; stdcall;
    function Cancel: HResult; stdcall;
  end;

  RecordsetStd15 = interface(ADOStd)
    ['{0000050E-0000-0010-8000-00AA006D2EA4}']
    function Get_AbsolutePosition(out pl: PositionEnum): HResult; stdcall;
    function Set_AbsolutePosition(pl: PositionEnum): HResult; stdcall;
    function _Set_ActiveConnection(const pvar: IDispatch): HResult; stdcall;
    function Set_ActiveConnection(pvar: OleVariant): HResult; stdcall;
    function Get_ActiveConnection(out pvar: OleVariant): HResult; stdcall;
    function Get_BOF(out pb: WordBool): HResult; stdcall;
    function Get_Bookmark(out pvBookmark: OleVariant): HResult; stdcall;
    function Set_Bookmark(pvBookmark: OleVariant): HResult; stdcall;
    function Get_CacheSize(out pl: Integer): HResult; stdcall;
    function Set_CacheSize(pl: Integer): HResult; stdcall;
    function Get_CursorType(out plCursorType: CursorTypeEnum): HResult; stdcall;
    function Set_CursorType(plCursorType: CursorTypeEnum): HResult; stdcall;
    function Get_EOF(out pb: WordBool): HResult; stdcall;
    function Get_Fields(out ppvObject: Fields): HResult; stdcall;
    function Get_LockType(out plLockType: LockTypeEnum): HResult; stdcall;
    function Set_LockType(plLockType: LockTypeEnum): HResult; stdcall;
    function Get_MaxRecords(out plMaxRecords: Integer): HResult; stdcall;
    function Set_MaxRecords(plMaxRecords: Integer): HResult; stdcall;
    function Get_RecordCount(out pl: Integer): HResult; stdcall;
    function _Set_Source(const pvSource: IDispatch): HResult; stdcall;
    function Set_Source(const pvSource: WideString): HResult; stdcall;
    function Get_Source(out pvSource: OleVariant): HResult; stdcall;
    function AddNew(FieldList: OleVariant; Values: OleVariant): HResult; stdcall;
    function CancelUpdate: HResult; stdcall;
    function Close: HResult; stdcall;
    function Delete(AffectRecords: AffectEnum): HResult; stdcall;
    function GetRows(Rows: Integer; Start: OleVariant; Fields: OleVariant; out pvar: OleVariant): HResult; stdcall;
    function Move(NumRecords: Integer; Start: OleVariant): HResult; stdcall;
    function MoveNext: HResult; stdcall;
    function MovePrevious: HResult; stdcall;
    function MoveFirst: HResult; stdcall;
    function MoveLast: HResult; stdcall;
    function Open(Source: OleVariant; ActiveConnection: OleVariant; CursorType: CursorTypeEnum; 
                  LockType: LockTypeEnum; Options: Integer): HResult; stdcall;
    function Requery(Options: Integer): HResult; stdcall;
    function _xResync(AffectRecords: AffectEnum): HResult; stdcall;
    function Update(Fields: OleVariant; Values: OleVariant): HResult; stdcall;
    function Get_AbsolutePage(out pl: PositionEnum): HResult; stdcall;
    function Set_AbsolutePage(pl: PositionEnum): HResult; stdcall;
    function Get_EditMode(out pl: EditModeEnum): HResult; stdcall;
    function Get_Filter(out Criteria: OleVariant): HResult; stdcall;
    function Set_Filter(Criteria: OleVariant): HResult; stdcall;
    function Get_PageCount(out pl: Integer): HResult; stdcall;
    function Get_PageSize(out pl: Integer): HResult; stdcall;
    function Set_PageSize(pl: Integer): HResult; stdcall;
    function Get_Sort(out Criteria: WideString): HResult; stdcall;
    function Set_Sort(const Criteria: WideString): HResult; stdcall;
    function Get_Status(out pl: Integer): HResult; stdcall;
    function Get_State(out plObjState: Integer): HResult; stdcall;
    function _xClone(out ppvObject: _Recordset): HResult; stdcall;
    function UpdateBatch(AffectRecords: AffectEnum): HResult; stdcall;
    function CancelBatch(AffectRecords: AffectEnum): HResult; stdcall;
    function Get_CursorLocation(out plCursorLoc: CursorLocationEnum): HResult; stdcall;
    function Set_CursorLocation(plCursorLoc: CursorLocationEnum): HResult; stdcall;
    function NextRecordset(out RecordsAffected: OleVariant; out ppiRs: _Recordset): HResult; stdcall;
    function Supports(CursorOptions: CursorOptionEnum; out pb: WordBool): HResult; stdcall;
    function Get_Collect(Index: OleVariant; out pvar: OleVariant): HResult; stdcall;
    function Set_Collect(Index: OleVariant; pvar: OleVariant): HResult; stdcall;
    function Get_MarshalOptions(out peMarshal: MarshalOptionsEnum): HResult; stdcall;
    function Set_MarshalOptions(peMarshal: MarshalOptionsEnum): HResult; stdcall;
    function Find(const Criteria: WideString; SkipRecords: Integer; 
                  SearchDirection: SearchDirectionEnum; Start: OleVariant): HResult; stdcall;
  end;

  RecordsetStd = interface(RecordsetStd15)
    ['{0000054F-0000-0010-8000-00AA006D2EA4}']
    function Cancel: HResult; stdcall;
    function Get_DataSource(out ppunkDataSource: IUnknown): HResult; stdcall;
    function _Set_DataSource(const ppunkDataSource: IUnknown): HResult; stdcall;
    function Save(const FileName: WideString; PersistFormat: PersistFormatEnum): HResult; stdcall;
    function Get_ActiveCommand(out ppCmd: IDispatch): HResult; stdcall;
    function Set_StayInSync(pbStayInSync: WordBool): HResult; stdcall;
    function Get_StayInSync(out pbStayInSync: WordBool): HResult; stdcall;
    function GetString(StringFormat: StringFormatEnum; NumRows: Integer; 
                       const ColumnDelimeter: WideString; const RowDelimeter: WideString; 
                       const NullExpr: WideString; out pRetString: WideString): HResult; stdcall;
    function Get_DataMember(out pbstrDataMember: WideString): HResult; stdcall;
    function Set_DataMember(const pbstrDataMember: WideString): HResult; stdcall;
    function CompareBookmarks(Bookmark1: OleVariant; Bookmark2: OleVariant; 
                              out pCompare: CompareEnum): HResult; stdcall;
    function Clone(LockType: LockTypeEnum; out ppvObject: RecordsetStd): HResult; stdcall;
    function Resync(AffectRecords: AffectEnum; ResyncValues: ResyncEnum): HResult; stdcall;
  end;

// SQLServer Error Info for OleDB extendsion
  PSSERRORINFO = ^SSERRORINFO;
  SSERRORINFO = record
    pwszMessage: PWideChar;
    pwszServer: PWideChar;
    pwszProcedure: PWideChar;
    lNative: LongInt;
    bState: Byte;
    bClass: Byte;
    wLineNumber: Word;
  end;

  ISQLServerErrorInfo = interface
  ['{5CF4CA12-EF21-11d0-97E7-00C04FC2AD98}']
    function GetErrorInfo( var ppErrorInfo: PSSERRORINFO;
                          var ppStringsBuffer: Pointer): HResult; stdcall;
  end;

// OleDB interface
  IErrorRecords = interface
  ['{0c733a67-2a1c-11ce-ade5-00aa0044773d}']
    function AddErrorRecord(
                    const pErrorInfo: TErrorInfo;
                    dwLookupID: LongWord;
                    const pdispparams: TDispParams;
                    pUnkCustomError: IUnknown;
                    dwDynamicErrorID: LongWord
                  ): HResult; stdcall;
    function GetBasicErrorInfo(
                    ulRecordNum: LongWord;
                    out pErrorInfo: TErrorInfo
                  ): HResult; stdcall;
    function GetCustomErrorObject(
                    ulRecordNum: LongWord;
                    const riid: TGuid;
                    out ppObject: IUnknown
                  ): HResult; stdcall;
    function GetErrorInfo(
                    ulRecordNum: LongWord;
                    lcid: LCID;
                    out ppErrorInfo: IErrorInfo
                  ): HResult; stdcall;
    function GetErrorParameters(
                    ulRecordNum: LongWord;
                    out pdispparams: TDispParams
                  ): HResult; stdcall;
    function GetRecordCount(var pcRecords: Integer): HResult; stdcall;
  end;

type
  TStringDynArray = array of string;

function SplitSQL(const sql: string): TStringDynArray;
var
  list: TStringList;
  I, J: Integer;
  sqls: TStringDynArray;

  procedure AddSql(L1, L2: Integer);
  var
    OldLen, II: Integer;
    s: string;
  begin            
    for II := L1 to L2 do
      s := s + list[II] + #13#10;
    if Trim(s) <> '' then
    begin
      OldLen := Length(sqls);
      SetLength(sqls, OldLen + 1);
      sqls[OldLen] := s;
    end;
  end;
begin
  SetLength(sqls, 0);

  // 分解SQL语句, 两个'GO'之间的语句为一个执行单位
  list := TStringList.Create;
  try
    list.Text := sql;
    I := 0;
    J := 0;
    while I < list.Count do
    begin
      if SameText(Trim(list[I]), 'GO') then
      begin
        AddSql(J, I - 1);
        J := I + 1;
      end;
      Inc(I);
    end;
    AddSql(J, I - 1);
  finally
    list.Free;
  end;
  Result := sqls;
end;

// 处理OLE DB错误.
// Msg: 错误信息字符串
// 返回值: 0 无错误
//         1 一般错误
//         2 消息性错误
//         3 连接断开
function ProcessResult(var ErrorMsg: string): Integer;
var
  OleErr: IErrorInfo;
  ErrRecords: IErrorRecords;
  SSqlErr: ISQLServerErrorInfo;
  I, RecCnt: Integer;
  hr: HResult;
  InfoMsg, ConnClosed: Boolean;

  // 取得出错信息.
  // 返回 TRUE: 成功通过ErrInfo取得出错信息, ErrMsg为出错信息的字符串.
  //    FALSE: 失败.
  function GetSSqlErrInfo(ErrInfo: ISQLServerErrorInfo; var ErrMsg: string): Boolean;
  var
    SSErrInfo: PSSERRORINFO;
    pStrBuf: Pointer;
    ProcName: String;
  begin
    Result := False;
    if ErrInfo.GetErrorInfo(SSErrInfo, pStrBuf) = S_OK then
    begin
      // 即使GetErrorInfo返回S_OK, SSErrInfo也可能为nil
      if SSErrInfo = nil then Exit;

      if (SSErrInfo.lNative = 0) or (SSErrInfo.lNative = 5701) then
      begin // 应该是PRINT 返回的消息, 或更改数据库信息
        if SSErrInfo.lNative = 0 then
          ErrMsg := String(SSErrInfo.pwszMessage)
        else
          ErrMsg := '';
        InfoMsg := True;
      end
      else
      begin
        ErrMsg := Format('服务器: 消息 %d, 级别 %d, 状态 %d',
                      [SSErrInfo.lNative, SSErrInfo.bClass, SSErrInfo.bState]);
        ProcName := String(SSErrInfo.pwszProcedure);
        if ProcName <> '' then
          ErrMsg := ErrMsg + Format(', 过程 %s', [ProcName]);
        ErrMsg := ErrMsg + Format(', 行 %d', [SSErrInfo.wLineNumber]);
        ErrMsg := ErrMsg + #13#10 + String(SSErrInfo.pwszMessage);

        // 11 可能是连接中断问题, 但未见正式的文档说明.
        if SSErrInfo.lNative = 11 then ConnClosed := True;
      end;

      Result := True;
      ActiveX.CoTaskMemFree(SSErrInfo);
      ActiveX.CoTaskMemFree(pStrBuf);
    end;
  end;

  function GetErrInfo(ErrInfo: IErrorInfo): string;
  var
    Msg: WideString;
  begin
    if ErrInfo.GetDescription(Msg) = S_OK then
      Result := Format('%s', [string(Msg)])
    else
      Result := '';
  end;
begin
  InfoMsg := False;
  ConnClosed := False;
  Result := 0;

  GetErrorInfo(0, OleErr);
  if OleErr <> nil then
  begin
    if Supports(OleErr, IErrorRecords, ErrRecords) then
    begin
      hr := ErrRecords.GetRecordCount(RecCnt);
      if hr <> S_OK then Exit;

      for I := 0 to RecCnt - 1 do
      begin
        if ErrRecords.GetCustomErrorObject(I, ISQLServerErrorInfo, IUnknown(SSqlErr)) = S_OK then
        begin
          if SSqlErr = nil then Continue;
          if not GetSSqlErrInfo(SSqlErr, ErrorMsg) then
          begin
            ErrorMsg := GetErrInfo(OleErr);
          end;
        end
        else
        begin
          ErrorMsg := GetErrInfo(OleErr);
        end;
      end;
    end
    else
    begin
      ErrorMsg := GetErrInfo(OleErr);
    end;

    if InfoMsg then
      Result := 2 else
    if ConnClosed then
      Result := 3 else
      Result := 1;
  end;
end;

procedure InitConnection(Conn: _Connection);
var
  RecsAffected: OleVariant;
begin
  Conn.Execute('set showplan_text off', RecsAffected, -1);
  Conn.Execute('SET NOEXEC OFF SET PARSEONLY OFF', RecsAffected, -1);
  Conn.Execute('set showplan_all off', RecsAffected, -1);
end;

function IsConnectionActive(Conn: _Connection): Boolean;
begin
  Result := (Conn.State and adStateOpen) = adStateOpen;
end;

type
  TExecuteThread = class(TThread)
  private
    FConnection: _Connection;
    FMain: TSqlQryFrm;
    FSQL: string;
    FParseOnly: Boolean;

    FCommand: CommandStd;
    FCanceled: Boolean;

    FInfoMsg: Boolean;  // 当前是否有消息性错误.

    FConnClosed: Boolean; // 是否遇到连接错误.
    FHasErrors: Boolean;
    FTimeCost: TList;
    FMsgList: TStringList;
    FDataList: TList;

    FLock: TRTLCriticalSection;

    procedure AddMsg(AMsg: string);
    procedure AddRecordset(Rst: _Recordset);
    procedure ProcessResults;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Execute; override;
    procedure Cancel;
    procedure Lock;
    procedure Unlock;
  end;

{ TExecuteThread }

procedure TExecuteThread.AddMsg(AMsg: string);
begin
  FMsgList.Add(AMsg);
end;

procedure TExecuteThread.AddRecordset(Rst: _Recordset);
var
  RowData: TRowDataList;
  bNormal: Boolean;
begin
  RowData := TRowDataList.Create;
  try
    bNormal := RowData.ImportRecordset(Rst, FCanceled);
    if not bNormal and (RowData.Data.Count = 0) then
      FreeAndNil(RowData)
    else
      FDataList.Add(Pointer(RowData));

    if (RowData <> nil) and bNormal then
      AddMsg(Format(#13#10'（所影响的行数为 %d 行）'#13#10, [RowData.Data.Count]));
  except
    on E: Exception do
    begin
      RowData.Free;
      AddMsg(E.Message);
    end;
  end;
end;

procedure TExecuteThread.Cancel;
begin
  Lock;
  try
    FCanceled := True;
    if FCommand <> nil then FCommand.Cancel;
  finally
    Unlock;
  end;
end;

constructor TExecuteThread.Create;
begin
  inherited Create(True); // Create suspended

  InitializeCriticalSection(FLock);
  FTimeCost := TList.Create;
  FDataList := TList.Create;
  FMsgList := TStringList.Create;
end;

destructor TExecuteThread.Destroy;
begin
  FTimeCost.Free;
  FDataList.Free;
  FMsgList.Free;

  Lock;
  Unlock;
  DeleteCriticalSection(FLock);

  inherited;
end;

procedure TExecuteThread.Execute;
var
  Rst, Rst2: _Recordset;
  I, OldTick, NewTick, Affected: Integer;
  VarRecsAffected, Params: OleVariant;
  sqls: TStringDynArray;
  hr: HResult;
begin
  if FSQL = '' then Exit;

  CoInitialize(Nil);
  try

    SetLength(sqls, 0);
    sqls := SplitSQL(FSQL);

    FMain.FExecuting := True;

    // 如果连接已经关闭, 重新建立.
    if not IsConnectionActive(FConnection) then
    begin
      (FConnection as ConnectionStd).Open('', '', '', -1);
      ProcessResults;
    end;

    // 连接无法建立. 退出.
    if not IsConnectionActive(FConnection) then Exit;

    // 建立Command 对象
    FCommand := CoCommand.Create as CommandStd;
    FCommand.Set_ActiveConnection(FConnection);

    if FParseOnly then
    begin
      hr := (FConnection as ConnectionStd).Execute('set parseonly on',
                  VarRecsAffected, adExecuteNoRecords, Rst2);
      if hr <> S_OK then Exit;
    end;

    for I := 0 to Length(sqls) - 1 do
    begin
      if FCanceled or FConnClosed then Break;

      OldTick := GetTickCount;
      try
        FCommand.Set_CommandText(sqls[I]);
        Params := EmptyParam;
        hr := FCommand.Execute(VarRecsAffected, Params, 0, Rst);

        NewTick := GetTickCount;
        FTimeCost.Add(Pointer(NewTick - OldTick));

        if hr <> S_OK then
        begin
          if LoWord(hr) <> adErrOperationCancelled then FHasErrors := True;
          ProcessResults;
          Continue;
        end;
      except
        On E: Exception do begin
          NewTick := GetTickCount;
          FTimeCost.Add(Pointer(NewTick - OldTick));
          AddMsg(E.Message);
          FHasErrors := True;
          Continue;
        end;
      end;

      while (Rst <> nil) and (not FCanceled) and (not FConnClosed) do
      begin
        ProcessResults;

        Affected := VarRecsAffected;
        if (Rst.State and adStateOpen) = adStateOpen then
        begin
          AddRecordset(Rst);
        end
        else if not FInfoMsg and (Affected >= 0) then
        begin
          AddMsg(Format(#13#10'（所影响的行数为 %d 行）'#13#10, [Affected]));
        end;

        if FCanceled then Break;

        hr := RecordsetStd(Rst).NextRecordset(VarRecsAffected, Rst2);
        if hr <> S_OK then
          if LoWord(hr) <> adErrOperationCancelled then FHasErrors := True;
        Rst := Rst2;
        Rst2 := nil;
      end;
    end;

    // 有时候释放Recordset要花很长时间, 特别SELECT带有image类型的字段
    Rst := nil;

    Lock;
    try
      FCommand := nil;
    finally
      Unlock;
    end;

    if FParseOnly then
    begin
      hr := (FConnection as ConnectionStd).Execute('set parseonly off',
                    VarRecsAffected, adExecuteNoRecords, Rst2);
      if hr <> S_OK then Exit;
    end;

    if FCanceled then
    begin
      AddMsg('用户已取消查询');
    end;

    // 清除TSqlQryFrm中的线程变量.
    Self.Synchronize(FMain.ClearVarRef);

  finally
    CoUninitialize;
  end;
end;

procedure TExecuteThread.Lock;
begin
  EnterCriticalSection(FLock);
end;

procedure TExecuteThread.ProcessResults;
var
  err_type: Integer;
  ErrMsg: string;
begin
  err_type := ProcessResult(ErrMsg);
  if ErrMsg <> '' then AddMsg(ErrMsg);
  FInfoMsg := err_type = 2;
  FConnClosed := err_type = 3;
end;

procedure TExecuteThread.Unlock;
begin
  LeaveCriticalSection(FLock);
end;

{ TSqlQryFrm }

procedure TSqlQryFrm.CancelExecute;
begin
  FCanceling := True;
  if FExecThread <> nil then
  begin
    TExecuteThread(FExecThread).Cancel;
    SBar.SimpleText := '正在取消批查询, 请等待...';
  end;
end;

procedure TSqlQryFrm.ClearResults;
var
  I: Integer;
  Ctrl: TControl;
begin
  Pgc1.ActivePageIndex := 0;
  for I := PnlResult.ControlCount - 1 downto 0 do
  begin
    Ctrl := PnlResult.Controls[I];
    Ctrl.Free;
  end;
  PnlResult.Align := alTop;
  PnlResult.Height := SbxResult.ClientHeight;

  MoMsg.Clear;
end;

procedure TSqlQryFrm.Execute;
var
  sql: string;
begin
  if Self.Executing or (MoSql.Text = '') then Exit;

  if MoSql.SelLength = 0 then
    sql := MoSql.Text
  else
    sql := MoSql.SelText;

  // 清除结果.
  ClearResults;

  FCanceling := False;

  if FConnClosed and IsConnectionActive(FConnection) then
  begin
    // 即使是执行过程中, 遇到通讯问题, State仍然不会变成adStateClosed
    // 所以必须重新关闭, 再建立联接
    FConnection.Close;
    FConnClosed := False;
  end;

  SBar.SimpleText := '正在执行批查询...';

  FExecThread := TExecuteThread.Create;
  FExecThread.FreeOnTerminate := True;
  FExecThread.OnTerminate := OnThreadTerminate;
  TExecuteThread(FExecThread).FConnection := FConnection;
  TExecuteThread(FExecThread).FMain := Self;
  TExecuteThread(FExecThread).FSQL := sql;
  TExecuteThread(FExecThread).FParseOnly := FParseOnly;
  FExecThread.Resume;
end;

procedure TSqlQryFrm.ParseSQL;
begin
  if Self.Executing then Exit;

  FParseOnly := True;
  try Execute; except end;
  FParseOnly := False;
end;

procedure TSqlQryFrm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

function TSqlQryFrm.GetDatabaseList: TStringList;
const
  sel_db_sql = 'exec master.dbo.sp_MShasdbaccess';
var
  Rst: _Recordset;
begin
  Result := nil;
  ExecuteSql('set showplan_text off');
  ExecuteSql('set showplan_all off');
  ExecuteSql('SET NOEXEC OFF SET PARSEONLY OFF SET ROWCOUNT 0');
  Rst := ExecuteRst(sel_db_sql);
  if Rst <> nil then
  begin
    Result := TStringList.Create;
    while not Rst.EOF do
    begin
      Result.Add(Rst.Fields['dbname'].Value);
      Rst.MoveNext;
    end;
  end;
end;

procedure TSqlQryFrm.OnGridResize(Sender: TObject);
var
  H, I: Integer;
  Ctrl: TControl;
begin
  if PnlResult.ControlCount = 2 then Exit;

  H := 0;

  for I := PnlResult.ControlCount - 1 downto 0 do
  begin
    Ctrl := PnlResult.Controls[I];
    Inc(H, Ctrl.Height);
  end;

  PnlResult.Height := H + 1000;
  SbxResult.VertScrollBar.Range := H + 20;
  SbxResult.VertScrollBar.Visible := SbxResult.ClientHeight < (H + 20);
end;

function TSqlQryFrm.ExecuteSql(const ASql: WideString): Integer;
var
  Affected: OleVariant;
  hr: HResult;
  Rst: _Recordset;
begin
  hr := (FConnection as ConnectionStd).Execute(ASql, Affected, adExecuteNoRecords, Rst);
  if hr < 0 then
    ProcessResults;
  Result := Affected;
end;

function TSqlQryFrm.ExecuteRst(const ASql: WideString): _Recordset;
var
  Affected: OleVariant;
  hr: HResult;
begin
  hr := (FConnection as ConnectionStd).Execute(ASql, Affected, -1, Result);
  if hr < 0 then
    ProcessResults;
end;

procedure TSqlQryFrm.Initialize(Conn: _Connection);
var
  Rst: _Recordset;
begin
  FConnection := Conn;
  FDatabase := FConnection.DefaultDatabase;

  Rst := ExecuteRst('select convert(sysname, serverproperty(N''servername''))');
  if not VarIsNull(Rst.Fields[0].Value) then
    FServerName := Rst.Fields[0].Value;

  Rst := ExecuteRst('SELECT ISNULL(SUSER_SNAME(), SUSER_NAME())');
  if not VarIsNull(Rst.Fields[0].Value) then
    FSUserName := Rst.Fields[0].Value;

  FFileName := Format('无标题%d', [NewFileCount]);
  UpdateTitle;

  Inc(NewFileCount);

  FPnlRate := 0.5;
  PnlMainResize(PnlMain);

  Visible := True;
end;

procedure TSqlQryFrm.PnlMainResize(Sender: TObject);
begin
  if FPnlRate = 0 then FPnlRate := 0.5;

  Pgc1.Height := Round(PnlMain.Height * FPnlRate);
end;

procedure TSqlQryFrm.Splitter1Moved(Sender: TObject);
begin
  FPnlRate := Pgc1.Height / PnlMain.Height;
end;

procedure TSqlQryFrm.FormActivate(Sender: TObject);
begin
  PostMessage(Application.MainForm.Handle, WM_QRYFRMACTIVATE, 0, 0);
end;

function TSqlQryFrm.GetConnectionString: string;
begin
  if FConnection <> nil then
    Result := FConnection.ConnectionString
  else
    Result := '';
end;

procedure TSqlQryFrm.SetDatabase(const Value: string);
begin
  try
    ExecuteSql('set showplan_text off');
    ExecuteSql('set showplan_all off');
    ExecuteSql('SET NOEXEC OFF SET PARSEONLY OFF SET ROWCOUNT 0');
    ExecuteSql(Format('use [%s]', [Value]));
  except
    Application.HandleException(Self);
    Exit;
  end;

  FDatabase := Value;
  UpdateTitle;
end;

procedure TSqlQryFrm.ProcessResults;
var
  err_type: Integer;
  Msg: string;
begin
  err_type := ProcessResult(Msg);
  if Msg <> '' then AddMsg(Msg);
  FConnClosed := err_type = 3;
  if err_type <> 0 then Abort;
end;

procedure TSqlQryFrm.AddMsg(Msg: string);
begin
  MoMsg.Lines.Add(Msg);
end;

procedure TSqlQryFrm.AddMsgs(MsgList: TStringList);
begin
  MoMsg.Lines.Assign(MsgList);
end;

procedure TSqlQryFrm.AddRecordsets(DataList: TList);
var
  Grd: TDataGrid;
  Split: TSplitter;
  I: Integer;
begin
  for I := 0 to DataList.Count-1 do
  begin
    Grd := TDataGrid.Create(Self);
    Grd.Height := 120;
    Grd.Top := 65535;
    Grd.Align := alTop;
    Grd.Parent := PnlResult;
    Grd.Flat := True;
    Grd.PopupMenu := GridMenu;
    Grd.SetData(TRowDataList(DataList[I]));
    Grd.OnResize := OnGridResize;

    Split := TSplitter.Create(Self);
    Split.Parent := PnlResult;
    Split.Align := alTop;
    Split.Height := 2;
    Split.MinSize := 10;
    Split.AutoSnap := False;
  end;
end;

procedure TSqlQryFrm.ClearVarRef;
begin
  FExecThread := nil;
end;

procedure TSqlQryFrm.OnThreadTerminate(Sender: TObject);
var
  I: Integer;
  Thread: TExecuteThread;
begin
  FExecuting := False;
  Thread := TExecuteThread(Sender);

  FConnClosed := Thread.FConnClosed;
  UpdateTitle;
  UpdateDatabase;

  AddMsgs(Thread.FMsgList);
  AddRecordsets(Thread.FDataList);

  if MoMsg.Lines.Count = 0 then MoMsg.Lines.Add('命令执行成功.');

  for I := 0 to Thread.FTimeCost.Count - 1 do
  begin
    if I = 0 then MoMsg.Lines.Add(#13#10'耗时: ');
    MoMsg.Lines.Add(' ' + IntToStr(Integer(Thread.FTimeCost[I])));
  end;

  if Thread.FHasErrors then
    SBar.SimpleText := '批查询已完成, 但有错误.'
  else if Thread.FCanceled then
    SBar.SimpleText := '批查询已取消.'
  else
    SBar.SimpleText := '批查询已完成.';

  OnGridResize(nil);
  if PnlResult.ControlCount = 0 then
    Pgc1.ActivePageIndex := 1
  else
  begin
    if TExecuteThread(Sender).FHasErrors then
      Pgc1.ActivePageIndex := 1
    else
      Pgc1.ActivePageIndex := 0;

    if PnlResult.ControlCount = 2 then
    begin
      PnlResult.Align := alClient;

      for I := 0 to PnlResult.ControlCount-1 do
        if PnlResult.Controls[I] is TDataGrid then
          PnlResult.Controls[I].Align := alClient
        else
          PnlResult.Controls[I].Visible := False;
    end;
  end;
end;

procedure TSqlQryFrm.UpdateTitle;
begin
  Self.Caption := Format('查询 -- %s.%s.%s -- %s',
          [FServerName, FDatabase, FSUserName, FFileName]);
end;

procedure TSqlQryFrm.UpdateDatabase;
begin
  if IsConnectionActive(FConnection) then
  begin
    Self.FDatabase := FConnection.DefaultDatabase;
    PostMessage(Application.MainForm.Handle, WM_DATABASECHANGED, 0, 0);
  end;
end;

procedure TSqlQryFrm.FormDestroy(Sender: TObject);
begin
  PostMessage(Application.MainForm.Handle, WM_QRYFRMCLOSE, 0, 0);
end;

procedure TSqlQryFrm.LoadFile;
begin
  if OpenDialog1.Execute then
  begin
    MoSql.Lines.LoadFromFile(OpenDialog1.FileName);
    FFileName := OpenDialog1.FileName;
    UpdateTitle;
  end;
end;

function TSqlQryFrm.SaveFile: Boolean;
begin
  if FileExists(FFileName) then
  begin
    if MoSql.Modified then
      MoSql.Lines.SaveToFile(FFileName);
    Result := True;
  end
  else
    Result := SaveAs;
end;

function TSqlQryFrm.SaveAs: Boolean;
begin
  Result := SaveDialog1.Execute;
  if Result then
  begin
    MoSql.Lines.SaveToFile(SaveDialog1.FileName);
    FFileName := SaveDialog1.FileName;
    UpdateTitle;
  end;
end;

procedure TSqlQryFrm.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
var
  Msg: string;
  trans: Integer;
  Rst: _Recordset;
begin
  // 检查是否正在运行.
  if Self.Executing then
  begin
    Msg := '是否要取消此次查询?';
    case Application.MessageBox(PChar(Msg), PChar(Application.Title),
              MB_YESNOCANCEL or MB_ICONWARNING) of
      ID_YES:
        begin
          if Self.Executing then
          begin
            Self.CancelExecute;                          
            repeat
              CheckSynchronize; // 因为我们用了TThread.Synchronize
              Sleep(50);
            until not Self.Executing or Application.Terminated;
          end;
        end;
    else
      CanClose := False;
    end;
  end;

  // 检查是否有未完成的事务.
  try
    Rst := ExecuteRst('select @@trancount');
    trans := Rst.Fields[0].Value;
    if trans > 0 then
    begin
      Msg := '有未提交的事务。'#13#10#13#10'是否希望在关闭窗口之前提交这些事务？';
      case Application.MessageBox(PChar(Msg), PChar(Application.Title),
                MB_YESNOCANCEL or MB_ICONWARNING) of
        ID_YES:
          begin
            try ExecuteSql('commit tran'); except end;
          end;
        ID_NO:
          try  ExecuteSql('rollback tran'); except end;
        ID_CANCEL:
          begin
            CanClose := False;
            Exit;
          end;
      end;
    end;
  except
    
  end;

  // 检查是否需要保存文件.
  if MoSql.Modified then
  begin
    Msg := Format('%s 中的文本已经修改'#13#10#13#10'是否保存更改?', [Self.FFileName]);
    case Application.MessageBox(PChar(Msg), PChar(Application.Title),
              MB_YESNOCANCEL or MB_ICONWARNING) of
      ID_YES:
        try
          CanClose := SaveFile;
        except
          Application.HandleException(Self);
          CanClose := False;
        end;
      ID_CANCEL:
        CanClose := False;
    end;
  end;
end;

function TSqlQryFrm.GetResultBoxVisible: Boolean;
begin
  Result := Pgc1.Visible;
end;

procedure TSqlQryFrm.ToggleResultBox;
begin
  if Pgc1.Visible then
  begin
    Pgc1.Visible := False;
    Splitter1.Visible := False;
  end
  else
  begin
    Pgc1.Visible := True;
    Splitter1.Visible := True;
  end;
end;

procedure TSqlQryFrm.MnCopyGridTextClick(Sender: TObject);
var
  Grid: TDataGrid;
begin
  if Self.ActiveControl is TDataGrid then
  begin
    Grid := TDataGrid(Self.ActiveControl);
    Clipboard.AsText := Grid.SelectionText;
  end;
end;

end.
