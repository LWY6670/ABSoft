unit Main;

interface

uses
  ABPubFormU,
  ABPubMessageU,
  abpubFuncu,

  cxDropDownEdit,
  cxMaskEdit,
  cxTextEdit,
  cxEdit,
  cxContainer,
  cxLookAndFeelPainters,
  cxLookAndFeels,
  cxControls,
  cxGraphics,

  Windows,Messages,SysUtils,Variants,Classes,Graphics,Controls,Forms,Menus,Dialogs,
  StdCtrls,Buttons,ExtCtrls,ComCtrls,ImgList,FileCtrl,IdBaseComponent,IdComponent,
  IdTCPConnection,IdTCPClient,IdFTP,IdFTPList,IdAntiFreezeBase,IdAntiFreeze,
  IdExplicitTLSClientServerBase,IdAllFTPListParsers;

type
  TMainForm = class(TABPubForm)
    GroupBox4: TGroupBox;
    ImageList1: TImageList;
    FTPClient: TIdFTP;
    PopupMenuLocal: TPopupMenu;
    LocalCreateDirectory: TMenuItem;
    LocalDeleteFile: TMenuItem;
    N3: TMenuItem;
    UploadFile: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    BigIcon: TMenuItem;
    SmallIcon: TMenuItem;
    List: TMenuItem;
    Detail: TMenuItem;
    ListViewSendFile: TListView;
    PopupMenuRemote: TPopupMenu;
    RemoteRenameFile: TMenuItem;
    RemoteDeleteFile: TMenuItem;
    N4: TMenuItem;
    RemoteCreateDirectory: TMenuItem;
    N8: TMenuItem;
    DownloadFile: TMenuItem;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    EditPassword: TEdit;
    LinkServer: TBitBtn;
    BitBtn1: TBitBtn;
    SpeedButton1: TSpeedButton;
    Panel1: TPanel;
    GroupBox6: TGroupBox;
    Panel2: TPanel;
    DriveComboBox1: TDriveComboBox;
    ListViewLocalFile: TListView;
    GroupBox2: TGroupBox;
    ListViewRemoteFile: TListView;
    Splitter2: TSplitter;
    cxComboBox2: TcxComboBox;
    cxComboBox1: TcxComboBox;
    cxComboBox3: TcxComboBox;
    Edit1: TEdit;
    IdAntiFreeze1: TIdAntiFreeze;
    Splitter1: TSplitter;
    Label6: TLabel;
    Label7: TLabel;
    StatusBar1: TStatusBar;
    ProgressBar1: TProgressBar;
    procedure DriveComboBox1Change(Sender: TObject);
    procedure ListViewLocalFileDblClick(Sender: TObject);
    procedure LinkServerClick(Sender: TObject);
    procedure ListViewRemoteFileDblClick(Sender: TObject);
    procedure BigIconClick(Sender: TObject);
    procedure SmallIconClick(Sender: TObject);
    procedure ListClick(Sender: TObject);
    procedure DetailClick(Sender: TObject);
    procedure ListViewLocalFileContextPopup(Sender: TObject;
      MousePos: TPoint; var Handled: Boolean);
    procedure UploadFileClick(Sender: TObject);
    procedure ListViewRemoteFileContextPopup(Sender: TObject;
      MousePos: TPoint; var Handled: Boolean);
    procedure DownloadFileClick(Sender: TObject);
    procedure RemoteCreateDirectoryClick(Sender: TObject);
    procedure RemoteDeleteFileClick(Sender: TObject);
    procedure LocalCreateDirectoryClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure LocalDeleteFileClick(Sender: TObject);
    procedure RemoteRenameFileClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure cxComboBox1PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure cxComboBox3PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure cxComboBox2PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure FTPClientStatus(ASender: TObject; const AStatus: TIdStatus;
      const AStatusText: string);
    procedure FTPClientWorkBegin(ASender: TObject; AWorkMode: TWorkMode;
      AWorkCountMax: Integer);
    procedure FTPClientWorkEnd(ASender: TObject; AWorkMode: TWorkMode);
    procedure FTPClientWork(ASender: TObject; AWorkMode: TWorkMode;
      AWorkCount: Integer);
    procedure SpeedButton2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure cxComboBox1KeyPress(Sender: TObject; var Key: Char);
  private
    tempRomoteCurDirFileList: TStrings;

    AbortTransfer: Boolean;
    TransferrignData: Boolean;
    BytesToTransfer: LongWord;
    STime: TDateTime;
    AverageSpeed:Double;

    FRemotePath: string;
    FLocalPath: string;
    procedure DisplayLocalFileList();
    procedure DisplayRemoteFileList();
    procedure SetRemotePath(const Value: string);
    procedure SetLocalPath(const Value: string);
  public
    IsTransferring: Boolean; //是否正在传送文件
    property LocalPath: string read FLocalPath write SetLocalPath; //当前本地路径
    property RemotePath: string read FRemotePath write SetRemotePath; //当前服务器路径
  end;

var
  MainForm: TMainForm;

implementation

{$R *.dfm}
  //连接到服务器

procedure TMainForm.LinkServerClick(Sender: TObject);
begin
  BitBtn1Click(nil);
  if FTPClient.Connected then
    exit;

  try
    FTPClient.Host := cxComboBox1.Text;
    if edit1.Text <> '' then
      FTPClient.Port := StrToInt(edit1.Text);

    FTPClient.UserName := cxComboBox3.Text;
    FTPClient.Password := EditPassWord.Text;
    FTPClient.Connect; //连接
  except
    ShowMessage('不能和服务器建立连接！');
  end;

  if copy(cxComboBox2.Text, 1, 1) = '' then
    cxComboBox2.Text := '/' + cxComboBox2.Text;

  try
    //服务器路径初始化

    RemotePath := cxComboBox2.Text;
    //刷新服务器文件列表
    DisplayRemoteFileList();
  except
    ShowMessage('取服务器文件列表失败！');
  end;
end;

procedure TMainForm.BitBtn1Click(Sender: TObject);
begin
  if FTPClient.Connected then //已经建立了连接吗？
  begin
    if ABShow('断开当前连接吗?',[],['是','否'],1)=1 then
    begin
      try
        if IsTransferring then //正在传送文件吗？
          FTPClient.Abort; // 断开连接
        FTPClient.Quit; //退出连接
      except
        ShowMessage('正在传送文件，请稍候！');
      end;
    end
    else
      exit; //退出本过程
  end;
end;

procedure TMainForm.DisplayRemoteFileList();
var
  FileCount: Integer;
  newItem: TListItem;
begin
  ListViewRemoteFile.Items.Clear; //清空远程文件列表

  FTPClient.ChangeDir(RemotePath); //设置FTP服务器当前目录
  FTPClient.List(tempRomoteCurDirFileList); //得到文件和目录列表
  //处理tempRomoteCurDirFileList的每个项目
  for FileCount := 0 to tempRomoteCurDirFileList.Count - 1 do
  begin
    //是目录吗？
    if FTPClient.DirectoryListing.Items[FileCount].ItemType = ditDirectory then
    begin
      //添加目录项目
      newItem := ListViewRemoteFile.Items.Insert(0);
      newItem.ImageIndex := 0; //图标序号
      newItem.Caption := FTPClient.DirectoryListing.Items[FileCount].FileName; //文件名
      newItem.subItems.Add(IntToStr(FTPClient.DirectoryListing.Items[FileCount].Size)); //文件大小
      newItem.subItems.Add(ABDateTimeToStr(FTPClient.DirectoryListing.Items[FileCount].ModifiedDate)); //时间
      newItem.subItems.Add('文件夹');
    end
    else
    begin
      //添加文件项目
      newItem := ListViewRemoteFile.Items.Add;
      newItem.ImageIndex := 1; //图标序号
      newItem.Caption := FTPClient.DirectoryListing.Items[FileCount].FileName; //文件名
      newItem.subItems.Add(IntToStr(FTPClient.DirectoryListing.Items[FileCount].Size)); //文件大小
      newItem.subItems.Add(ABDateTimeToStr(FTPClient.DirectoryListing.Items[FileCount].ModifiedDate)); //时间
      newItem.subItems.Add('');
    end;
  end;
  if Length(RemotePath) > 1 then //不是根目录
  begin
    //填加上级目录项目
    newItem := ListViewRemoteFile.Items.Insert(0);
    newItem.ImageIndex := 0; //图标序号
    newItem.Caption := '..';
    newItem.SubItems.Add('');
    newItem.SubItems.Add('');
    newItem.SubItems.Add('上级目录');
  end;
end;

//显示本地文件列表

procedure TMainForm.DisplayLocalFileList();
var
  sr: TSearchRec;
  Item: TListItem;
begin
  SetCurrentDir(LocalPath); //设置当前路径
  ListViewLocalFile.Clear; //清空本地文件列表
  if Length(LocalPath) > 3 then //不是根目录
  begin
    //填加上级目录项目
    Item := ListViewLocalFile.Items.Add;
    Item.ImageIndex := 0;
    Item.Caption := '..';
    Item.SubItems.Add('');
    Item.SubItems.Add('');
    Item.SubItems.Add('上级目录');
  end;
  //填加文件夹
  if FindFirst('*', faDirectory, sr) = 0 then
  begin
    repeat
      if ((sr.Attr and faDirectory) = sr.Attr) and (sr.Name <> '.') and (sr.Name <> '..') then
      begin
        Item := ListViewLocalFile.Items.Add;
        Item.ImageIndex := 0; //图标序号
        Item.Caption := sr.Name; //文件夹名
        Item.SubItems.Add('');
        Item.SubItems.Add(ABDateTimeToStr(FileDateToDateTime(sr.Time))); //时间
        Item.SubItems.Add('文件夹');
      end;
    until FindNext(sr) <> 0; //下一个
    FindClose(sr); //结束查找
  end;

  //填加文件
  if FindFirst('*', faArchive, sr) = 0 then
  begin
    repeat
      Item := ListViewLocalFile.Items.Add;
      Item.ImageIndex := 1; //图标序号
      Item.Caption := sr.Name; //文件名
      Item.SubItems.Add(IntToStr(sr.Size)); //文件大小
      Item.SubItems.Add(ABDateTimeToStr(FileDateToDateTime(sr.Time))); //时间
      Item.SubItems.Add('');
    until FindNext(sr) <> 0; //下一个
    FindClose(sr); //结束查找
  end;
end;

//本地驱动器选择

procedure TMainForm.DriveComboBox1Change(Sender: TObject);
begin
  LocalPath := DriveComboBox1.Drive + ':\'; //本地路径
  DisplayLocalFileList(); //刷新本地文件列表
end;

//本地文件列表双击事件

procedure TMainForm.ListViewLocalFileDblClick(Sender: TObject);
var
  TmpFileName: string;
begin
  if ListViewLocalFile.Selected = nil then //  没有选择列表项目
    Exit;
  TmpFileName := ListViewLocalFile.Selected.Caption; // 得到文件名
  if ListViewLocalFile.Selected.SubItems[2] = '上级目录' then //是上级目录吗？
  begin
    LocalPath := LocalPath + '.txt';
    LocalPath := ExtractFileDir(LocalPath); //得到上级目录完整路径
    DisplayLocalFileList(); //刷新本地文件列表
  end
  else if ListViewLocalFile.Selected.SubItems[2] = '文件夹' then //是文件夹吗？
  begin
    if Length(LocalPath) = 3 then //是根目录吗？
      LocalPath := LocalPath + TmpFileName
    else
      LocalPath := LocalPath + '\' + TmpFileName;
    DisplayLocalFileList(); //刷新本地文件列表
  end
  else //是文件，上传文件
  begin
    if not FTPClient.Connected then //和服务器建立连接否？
      Exit;
    if ABShow('是否上传该文件?',[],['是','否'],1)=1 then
      UploadFileClick(Sender); //上传文件
  end;
end;

//服务器文件列表双击事件

procedure TMainForm.ListViewRemoteFileDblClick(Sender: TObject);
var
  FileName: string;
begin
  if not FTPClient.Connected then //没有连接到服务器，退出
    Exit;
  if ListViewRemoteFile.Selected = nil then //没有选中文件，退出
    Exit;
  FileName := ListViewRemoteFile.Selected.Caption; //文件名
  if ListViewRemoteFile.Selected.SubItems[2] = '上级目录' then //是上级目录吗？
  begin
    FTPClient.ChangeDirUp; //到上级目录
    RemotePath := FtpClient.RetrieveCurrentDir; //FTP服务器的当前目录名称
    DisplayRemoteFileList(); //刷新服务器文件列表
  end
  else if ListViewRemoteFile.Selected.SubItems[2] = '文件夹' then
  begin
    RemotePath := RemotePath + '/' + FileName; //下级目录名称
    //刷新服务器文件列表
    DisplayRemoteFileList();
  end
  else  if ABShow('是否下载该文件?',[],['是','否'],1)=1 then
    DownloadFileClick(Sender); //下载文件
end;

procedure TMainForm.cxComboBox1KeyPress(Sender: TObject;
  var Key: Char);
var
  i: LongInt;
  aUserName, aPassWord, aIP,aPort, aPath: string;
begin
  if Key=#13 then
  begin
    if trim(cxComboBox1.Text) <> emptystr then
    begin
      i := cxComboBox1.Properties.Items.IndexOf(cxComboBox1.Text);
      if i < 0 then
      begin
        cxComboBox1.Properties.Items.Add(cxComboBox1.Text);
      end;
    end;

    ABGetFtpInfo(cxComboBox1.Text, aUserName, aPassWord, aIP, aPort,aPath);
    if aUserName <> 'anonymous' then
    begin
      cxComboBox3.Text := aUserName;
      if aPassWord <> emptystr then
      begin
        EditPassword.Text := aPassWord;
      end;
    end;
    if aIP <> emptystr then
    begin
      if cxComboBox1.Text <> aIP then
        cxComboBox1.Text := aIP;
    end;
    if aPort <> emptystr then
    begin
      if Edit1.Text <> aPort then
        Edit1.Text := aPort;
    end;
  end;
end;

procedure TMainForm.cxComboBox1PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
begin
  //
end;

procedure TMainForm.cxComboBox2PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  i: LongInt;
begin
  if trim(cxComboBox2.text) = emptystr then
    exit;

  i := cxComboBox2.Properties.Items.IndexOf(cxComboBox2.text);
  if i < 0 then
  begin
    cxComboBox2.Properties.Items.Add(cxComboBox2.text);
  end;
end;

procedure TMainForm.cxComboBox3PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  i: LongInt;
begin
  if trim(cxComboBox3.text) = emptystr then
    exit;

  i := cxComboBox3.Properties.Items.IndexOf(cxComboBox3.text);
  if i < 0 then
  begin
    cxComboBox3.Properties.Items.Add(cxComboBox3.text);
  end;
end;

procedure TMainForm.BigIconClick(Sender: TObject);
begin       
  ListViewLocalFile.ViewStyle := vsIcon;
end;

procedure TMainForm.SetLocalPath(const Value: string);
begin
  FLocalPath := Value;
  label7.Caption:=Value;
end;

procedure TMainForm.SetRemotePath(const Value: string);
begin
  FRemotePath := Value;
  cxComboBox2.Text := Value;
end;

procedure TMainForm.SmallIconClick(Sender: TObject);
begin
  ListViewLocalFile.ViewStyle := vsSmallIcon;
end;

procedure TMainForm.SpeedButton1Click(Sender: TObject);
begin
  RemotePath := cxComboBox2.Text;
  //刷新服务器文件列表
  DisplayRemoteFileList();
end;

procedure TMainForm.SpeedButton2Click(Sender: TObject);
begin
  AbortTransfer:=true;
end;

procedure TMainForm.ListClick(Sender: TObject);
begin
  ListViewLocalFile.ViewStyle := vsList;
end;

procedure TMainForm.DetailClick(Sender: TObject);
begin
  ListViewLocalFile.ViewStyle := vsReport;
end;

procedure TMainForm.ListViewLocalFileContextPopup(Sender: TObject;
  MousePos: TPoint; var Handled: Boolean);
begin
  if ListViewLocalFile.Selected = nil then //没有选中项目
  begin
    //设置弹出菜单是否可用
    LocalDeleteFile.Enabled := False;
    LocalCreateDirectory.Enabled := true;
    BigIcon.Enabled := true;
    SmallIcon.Enabled := true;
    List.Enabled := True;
    Detail.Enabled := True;
    UpLoadFile.Enabled := False;
  end
  else
  begin
    //设置弹出菜单是否可用
    LocalCreateDirectory.Enabled := False;
    BigIcon.Enabled := False;
    SmallIcon.Enabled := False;
    List.Enabled := False;
    Detail.Enabled := False;
    if ListViewLocalFile.Selected.SubItems[2] <> '' then //是否选中文件？
    begin
      UpLoadFile.Enabled := False;
      LocalDeleteFile.Enabled := False;
    end
    else
    begin
      UpLoadFile.Enabled := True;
      LocalDeleteFile.Enabled := True;
    end;
  end;
end;

procedure TMainForm.ListViewRemoteFileContextPopup(Sender: TObject;
  MousePos: TPoint; var Handled: Boolean);
begin
  if ListViewRemoteFile.Selected = nil then //没有选中项目
  begin
    //设置弹出菜单是否可用
    RemoteCreateDirectory.Enabled := True;
    DownLoadFile.Enabled := False;
    RemoteRenameFile.Enabled := False;
    RemoteDeleteFile.Enabled := False;
  end
  else
  begin
    //设置弹出菜单是否可用
    RemoteCreateDirectory.Enabled := False;
    RemoteRenameFile.Enabled := true;
    RemoteDeleteFile.Enabled := True;
    if ListViewRemoteFile.Selected.SubItems[2] <> '' then //是否选中文件？
    begin
      DownLoadFile.Enabled := False;
      RemoteRenameFile.Enabled := False;
    end
    else
    begin
      downloadFile.Enabled := True;
      if ListViewRemoteFile.SelCount = 1 then
        RemoteRenameFile.Enabled := True
      else
        RemoteRenameFile.Enabled := False;
    end;
  end;
end;

//上传文件

procedure TMainForm.UploadFileClick(Sender: TObject);
var
  FileName: string;
  Item: TListItem;
  SendFileItem: TListItem;
begin
  if not FTPClient.Connected then //没有连接到服务器，退出
    Exit;
  if ListViewLocalFile.Selected = nil then //没有选中文件，退出
    Exit;

  Item := ListViewLocalFile.Selected; //得到选中的文件
  ListViewSendFile.Clear; //清空要传送的文件列表
  //处理所有选中文件
  while Item <> nil do
  begin
    //列表项赋值
    SendFileItem := ListViewSendFile.Items.Add; //增加到列表
    SendFileItem.Caption := Item.Caption; //文件名
    SendFileItem.SubItems.Add(Item.SubItems[1]); //文件大小
    SendFileItem.SubItems.Add(LocaLPath); //本地路径
    SendFileItem.SubItems.Add('==>'); //图示
    SendFileItem.SubItems.Add(RemotePath); //远程路径
    SendFileItem.SubItems.Add(''); //状态
    Item := ListViewLocalFile.GetNextItem(Item, sdAll, [isSelected]); //下一个选中的项目
  end;
  ListViewSendFile.Refresh; //刷新传送文件列表

  //传送文件
  ListViewLocalFile.Enabled := false; //禁止操作相关控件
  DriveComboBox1.Enabled := false;
  ListViewRemoteFile.Enabled := False;
  IsTransferring := True; //设置正在传送
  try
    //处理所有要传送的文件
    while ListViewSendFile.Items.Count > 0 do
    begin
      FileName := ListViewSendFile.Items[0].Caption; //文件名
      ListViewSendFile.Items[0].SubItems[4] := '正在上传...';
      FTPClient.Put(LocalPath + '\' + FileName, FileName); //上传
      DisplayRemoteFileList();

      ListViewSendFile.items[0].Delete; //传送完毕，删除待传文件列表
      ListViewSendFile.Refresh;
    end;
    //设置相关控件是否可用
    ListViewLocalFile.Enabled := True;
    DriveComboBox1.Enabled := True;
    ListViewRemoteFile.Enabled := True;
    IsTransferring := False; //没有传送状态
  except
    ListViewSendFile.Items[0].SubItems[4] := '上传错误';
    //设置相关控件是否可用
    ListViewLocalFile.Enabled := True;
    DriveComboBox1.Enabled := True;
    ListViewRemoteFile.Enabled := True;
    IsTransferring := False; //没有传送状态
    ShowMessage('上传文件发生错误！');
    raise;
  end;
end;
//下载文件

procedure TMainForm.DownloadFileClick(Sender: TObject);
var
  FileName: string;
  Item, SendFileItem: TListItem;
begin
  if not FTPClient.Connected then //没有连接到服务器，退出
    Exit;
  if ListViewRemoteFile.Selected = nil then //没有选中文件，退出
    Exit;
  Item := ListViewRemoteFile.Selected; //得到选中的文件
  ListViewSendFile.Clear; //清空要传送的文件列表
  //处理所有选中文件
  while Item <> nil do
  begin
    SendFileItem := ListViewSendFile.Items.Add; //增加到列表
    //列表项赋值
    SendFileItem.Caption := Item.Caption; //文件名
    SendFileItem.SubItems.Add(Item.SubItems[1]); //文件大小
    SendFileItem.SubItems.Add(LocaLPath); //本地路径
    SendFileItem.SubItems.Add('<=='); //图示
    SendFileItem.SubItems.Add(RemotePath); //远程路径
    SendFileItem.SubItems.Add(''); //状态
    Item := ListViewRemoteFile.GetNextItem(Item, sdAll, [isSelected]); //下一个选中的项目
  end;
  ListViewSendFile.Refresh; //刷新传送文件列表

  //传送文件
  ListViewRemoteFile.Enabled := false; //禁止操作相关控件
  IsTransferring := True; //设置正在传送                         
  try
    //处理所有要传送的文件
    while ListViewSendFile.Items.Count > 0 do                       
    begin
      FileName := ListViewSendFile.Items[0].Caption; //文件名
      ListViewSendFile.Items[0].SubItems[4] := '正在下载...';
      ListViewSendFile.Refresh; //刷新传送文件列表
      BytesToTransfer := FTPClient.Size(FileName);
      
      if ABCheckFileExists(FileName) then //判断文件是否存在
      begin
        if ABShow('文件已存在，是否续传？',[],['是','否'],1) = 1 then
          FTPClient.Get(FileName, LocalPath + '\' + FileName, false, true) //续传
        else
          FTPClient.Get(FileName, LocalPath + '\' + FileName, true); //覆盖
      end
      else
        FTPClient.Get(FileName, LocalPath + '\' + FileName, true); //下载

      ListViewSendFile.items[0].Delete; //传送完毕，删除待传文件列表
      ListViewSendFile.Refresh; //刷新待传文件列表
      DisplayLocalFileList(); //刷新本地文件列表
    end;
    ListViewRemoteFile.Enabled := True;
    IsTransferring := False; //没有传送状态
  except
    ListViewSendFile.Items[0].SubItems[4] := '下载错误';
    ListViewSendFile.Refresh; //刷新待传文件列表
    ShowMessage('下载文件发生错误！');
    ListViewRemoteFile.Enabled := True;
    IsTransferring := False; //没有传送状态
  end;
end;

procedure TMainForm.RemoteCreateDirectoryClick(Sender: TObject);
var
  NewDirectory: string;
begin
  if not FTPClient.Connected then //没有和服务器建立连接，退出
    exit;
  NewDirectory := '新建文件夹';
  if InputQuery('新建服务器文件夹', '文件夹名字：', NewDirectory) = TRUE then
    //提示输入新文件夹名
    if NewDirectory <> '' then
    try
      FTPClient.MakeDir(NewDirectory); //创建新文件夹
      DisplayRemoteFileList(); //刷新服务器文件列表
    except
      ShowMessage('新建服务器文件夹发生错误！'); //出错提示信息
    end
    else
      ShowMessage('文件夹名称不能为空！'); //出错提示信息
end;

procedure TMainForm.RemoteDeleteFileClick(Sender: TObject);
var
  Item: TListItem;
begin
  if not FTPClient.Connected then //没有建立连接，退出
    exit;
  if ListViewRemoteFile.Selected = nil then //没有选中文件，退出
    exit;
  if ABShow('确认删除这些文件？',[],['是','否'],1)=1 then
  begin
    Item := ListViewRemoteFile.Selected; //选择的文件
    //    处理所有选中的文件
    while Item <> nil do
    begin
      if Item.SubItems[2] = '文件夹' then //是文件夹，删除文件夹
        FTPClient.RemoveDir(Item.Caption);
      if Item.SubItems[2] = '' then //是文件，删除文件
        FtpClient.Delete(Item.Caption);
      Item := ListViewRemoteFile.GetNextItem(Item, sdAll, [isSelected]); //得到下一个选中的文件
    end;
    DisplayRemoteFileList(); //刷新服务器文件列表
  end;
end;

procedure TMainForm.LocalCreateDirectoryClick(Sender: TObject);
var
  NewDirectory: string; //临时文件夹名
begin
  NewDirectory := '新建文件夹';
  if InputQuery('新建本地文件夹', '文件夹名字：', NewDirectory) = TRUE then
    //提示输入新文件夹名
    if NewDirectory <> '' then
    try
      CreateDir(NewDirectory); //在当前路径创建新文件夹
      DisplayLocalFileList(); //刷新列表
    except
      ShowMessage('新建本地文件夹发生错误！'); //出错提示信息
    end
    else
      ShowMessage('文件夹名称不能为空！'); //出错提示信息
end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
  tempRomoteCurDirFileList := TStringList.Create;
end;

procedure TMainForm.FormDestroy(Sender: TObject);
begin
  tempRomoteCurDirFileList.Free; //释放TStringList
end;

procedure TMainForm.FormShow(Sender: TObject);
begin
  //初始化变量
  IsTransferring := False; //未传送文件状态
end;

procedure TMainForm.FTPClientStatus(ASender: TObject;
  const AStatus: TIdStatus; const AStatusText: string);
begin
  if ABpos('Disconnected', AStatusText) <> 0 then
    StatusBar1.Panels[1].Text := '断开'
  else if ABpos('Connection', AStatusText) <> 0 then
    StatusBar1.Panels[1].Text := '连接';
end;

procedure TMainForm.LocalDeleteFileClick(Sender: TObject);
var
  Item: TListItem;
begin
  if ListViewLocalFile.Selected = nil then // 没有选择文件，退出
    Exit;
  if ABShow('确认删除这些文件？',[],['是','否'],1)=1 then
  begin
    Item := ListViewLocalFile.Selected; //选择的文件
    //    处理所有选中的文件
    while Item <> nil do
    begin
      DeleteFile(LocalPath + '\' + Item.Caption); //删除该文件
      Item := ListViewLocalFile.GetNextItem(Item, sdAll, [isSelected]); //得到下一个选中的文件
    end;
    DisplayLocalFileList(); //刷新本地文件列表
  end;
end;

procedure TMainForm.RemoteRenameFileClick(Sender: TObject);
var
  NewFileName: string;
begin
  if not FTPClient.Connected then //没有和服务器建立连接，退出
    exit;
  NewFileName := ListViewRemoteFile.Selected.Caption;
  if InputQuery('重命名', '新文件名：', NewFileName) = TRUE then //提示输入新文件名
    if NewFileName <> '' then
    try
      FTPClient.Rename(ListViewRemoteFile.Selected.Caption, NewFileName); //重命名
      DisplayRemoteFileList(); //刷新服务器文件列表
    except
      ShowMessage('重命名文件发生错误！'); //出错提示信息
    end
    else
      ShowMessage('文件名称不能为空！'); //出错提示信息
end;

procedure TMainForm.FTPClientWork(ASender: TObject; AWorkMode: TWorkMode;
  AWorkCount: Integer);
var
  S: string;
  TotalTime: TDateTime;
  H, M, Sec, MS: Word;
  DLTime: Double;            
begin
  if AbortTransfer then
  begin //中断下载
    FTPClient.Disconnect;
    FTPClient.Abort;
  end
  else
  begin
    TotalTime := Now - STime;
    DecodeTime(TotalTime, H, M, Sec, MS);
    Sec := Sec + M * 60 + H * 3600;
    DLTime := Sec + MS / 1000;
    if DLTime > 0 then
      AverageSpeed := {(AverageSpeed + }(AWorkCount / 1024) / DLTime {) / 2};

    S := FormatFloat('0.00 KB/s', AverageSpeed);
    case AWorkMode of
      wmRead: StatusBar1.Panels[3].Text := ' 下载 ' + S;
      wmWrite: StatusBar1.Panels[3].Text := ' 上传 ' + S;
    end;
    ProgressBar1.Position := AWorkCount;
    AbortTransfer := false;
  end;
  application.ProcessMessages;
end;

procedure TMainForm.FTPClientWorkBegin(ASender: TObject;
  AWorkMode: TWorkMode; AWorkCountMax: Integer);
begin
  TransferrignData := true;
  AbortTransfer := false;
  STime := Now;
  if AWorkCountMax > 0 then
    ProgressBar1.Max := AWorkCountMax
  else
    ProgressBar1.Max := BytesToTransfer;
  AverageSpeed := 0;
end;

procedure TMainForm.FTPClientWorkEnd(ASender: TObject;
  AWorkMode: TWorkMode);
begin
  StatusBar1.Panels[3].Text := '';
  BytesToTransfer := 0;
  TransferrignData := false;
  ProgressBar1.Position := 0;
  AverageSpeed := 0;
end;

end.

