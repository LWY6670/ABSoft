object MainForm: TMainForm
  Left = 173
  Top = 129
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  BorderStyle = bsSingle
  Caption = 'SQLServer'#25968#25454#24211#24674#22797
  ClientHeight = 307
  ClientWidth = 341
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 339
    Height = 100
    Caption = #36830#25509#20449#24687
    TabOrder = 0
    object Label8: TLabel
      Left = 41
      Top = 45
      Width = 42
      Height = 14
      Caption = #31649#29702#21592
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -14
      Font.Name = #26032#23435#20307
      Font.Style = []
      ParentFont = False
    end
    object Label9: TLabel
      Left = 13
      Top = 71
      Width = 70
      Height = 14
      Caption = #31649#29702#21592#23494#30721
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -14
      Font.Name = #26032#23435#20307
      Font.Style = []
      ParentFont = False
    end
    object Label22: TLabel
      Left = 27
      Top = 20
      Width = 56
      Height = 14
      Caption = #26381#21153#22120#21517
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -14
      Font.Name = #26032#23435#20307
      Font.Style = []
      ParentFont = False
    end
    object SpeedButton1: TSpeedButton
      Left = 203
      Top = 14
      Width = 23
      Height = 22
      Caption = '...'
      OnClick = SpeedButton1Click
    end
    object RadioGroup1: TRadioGroup
      Tag = 91
      Left = 230
      Top = 10
      Width = 102
      Height = 28
      Columns = 2
      Font.Charset = GB2312_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = #23435#20307
      Font.Style = []
      ItemIndex = 0
      Items.Strings = (
        'Sink'
        'OLE')
      ParentFont = False
      TabOrder = 3
    end
    object Edit1: TEdit
      Tag = 91
      Left = 90
      Top = 43
      Width = 135
      Height = 22
      Font.Charset = GB2312_CHARSET
      Font.Color = clWindowText
      Font.Height = -14
      Font.Name = #23435#20307
      Font.Style = []
      ImeName = #24555#20048#20116#31508
      ParentFont = False
      TabOrder = 1
      OnChange = Edit2Change
    end
    object Edit2: TEdit
      Tag = 91
      Left = 90
      Top = 69
      Width = 135
      Height = 22
      Font.Charset = GB2312_CHARSET
      Font.Color = clWindowText
      Font.Height = -14
      Font.Name = #23435#20307
      Font.Style = []
      ImeName = #24555#20048#20116#31508
      ParentFont = False
      PasswordChar = '*'
      TabOrder = 2
      OnChange = Edit2Change
    end
    object ComboBox1: TComboBox
      Tag = 91
      Left = 89
      Top = 15
      Width = 113
      Height = 22
      Font.Charset = GB2312_CHARSET
      Font.Color = clWindowText
      Font.Height = -14
      Font.Name = #23435#20307
      Font.Style = []
      ImeName = #24555#20048#20116#31508
      ParentFont = False
      TabOrder = 0
      OnChange = Edit2Change
    end
    object Button4: TButton
      Left = 230
      Top = 70
      Width = 100
      Height = 21
      Caption = #27979#35797#36830#25509
      Default = True
      TabOrder = 5
      OnClick = Button4Click
    end
    object RadioGroup2: TRadioGroup
      Tag = 91
      Left = 230
      Top = 37
      Width = 102
      Height = 28
      Columns = 2
      ItemIndex = 1
      Items.Strings = (
        'WIN'
        'SQL')
      TabOrder = 4
    end
  end
  object GroupBox4: TGroupBox
    Left = 0
    Top = 172
    Width = 339
    Height = 95
    Caption = #20174#25991#20214#38468#21152
    Color = clBtnFace
    ParentColor = False
    TabOrder = 1
    object Label20: TLabel
      Left = 24
      Top = 73
      Width = 56
      Height = 14
      Caption = #25968#25454#24211#21517
      Font.Charset = GB2312_CHARSET
      Font.Color = clWindowText
      Font.Height = -14
      Font.Name = #26032#23435#20307
      Font.Style = []
      ParentFont = False
    end
    object Label21: TLabel
      Left = 10
      Top = 20
      Width = 70
      Height = 14
      Caption = #25968#25454#25991#20214#21517
      Font.Charset = GB2312_CHARSET
      Font.Color = clWindowText
      Font.Height = -14
      Font.Name = #26032#23435#20307
      Font.Style = []
      ParentFont = False
    end
    object SpeedButton3: TSpeedButton
      Left = 308
      Top = 15
      Width = 23
      Height = 22
      Caption = '...'
      OnClick = SpeedButton3Click
    end
    object Label1: TLabel
      Left = 10
      Top = 46
      Width = 70
      Height = 14
      Caption = #26085#24535#25991#20214#21517
      Font.Charset = GB2312_CHARSET
      Font.Color = clWindowText
      Font.Height = -14
      Font.Name = #26032#23435#20307
      Font.Style = []
      ParentFont = False
    end
    object SpeedButton4: TSpeedButton
      Left = 308
      Top = 40
      Width = 23
      Height = 22
      Caption = '...'
      OnClick = SpeedButton4Click
    end
    object Edit6: TEdit
      Tag = 91
      Left = 86
      Top = 68
      Width = 139
      Height = 22
      Font.Charset = GB2312_CHARSET
      Font.Color = clWindowText
      Font.Height = -14
      Font.Name = #23435#20307
      Font.Style = []
      ImeName = #24555#20048#20116#31508
      ParentFont = False
      TabOrder = 2
    end
    object Edit5: TEdit
      Tag = 91
      Left = 86
      Top = 16
      Width = 219
      Height = 22
      Font.Charset = GB2312_CHARSET
      Font.Color = clWindowText
      Font.Height = -14
      Font.Name = #23435#20307
      Font.Style = []
      ImeName = #24555#20048#20116#31508
      ParentFont = False
      TabOrder = 0
    end
    object Button3: TButton
      Left = 230
      Top = 68
      Width = 100
      Height = 21
      Caption = #20174#25991#20214#38468#21152
      TabOrder = 3
      OnClick = Button3Click
    end
    object Edit7: TEdit
      Tag = 91
      Left = 86
      Top = 40
      Width = 219
      Height = 22
      Font.Charset = GB2312_CHARSET
      Font.Color = clWindowText
      Font.Height = -14
      Font.Name = #23435#20307
      Font.Style = []
      ImeName = #24555#20048#20116#31508
      ParentFont = False
      TabOrder = 1
    end
  end
  object GroupBox3: TGroupBox
    Left = 0
    Top = 100
    Width = 339
    Height = 72
    Caption = #20174#25991#20214#23433#35013
    Color = clBtnFace
    ParentColor = False
    TabOrder = 2
    object Label18: TLabel
      Left = 27
      Top = 41
      Width = 56
      Height = 14
      Caption = #25968#25454#24211#21517
      Font.Charset = GB2312_CHARSET
      Font.Color = clWindowText
      Font.Height = -14
      Font.Name = #26032#23435#20307
      Font.Style = []
      ParentFont = False
    end
    object Label19: TLabel
      Left = 40
      Top = 19
      Width = 42
      Height = 14
      Caption = #25991#20214#21517
      Font.Charset = GB2312_CHARSET
      Font.Color = clWindowText
      Font.Height = -14
      Font.Name = #26032#23435#20307
      Font.Style = []
      ParentFont = False
    end
    object SpeedButton2: TSpeedButton
      Left = 307
      Top = 14
      Width = 23
      Height = 22
      Caption = '...'
      OnClick = SpeedButton2Click
    end
    object Edit9: TEdit
      Tag = 91
      Left = 87
      Top = 41
      Width = 139
      Height = 22
      Font.Charset = GB2312_CHARSET
      Font.Color = clWindowText
      Font.Height = -14
      Font.Name = #23435#20307
      Font.Style = []
      ImeName = #24555#20048#20116#31508
      ParentFont = False
      TabOrder = 1
    end
    object Edit4: TEdit
      Tag = 91
      Left = 87
      Top = 14
      Width = 215
      Height = 22
      Font.Charset = GB2312_CHARSET
      Font.Color = clWindowText
      Font.Height = -14
      Font.Name = #23435#20307
      Font.Style = []
      ImeName = #24555#20048#20116#31508
      ParentFont = False
      TabOrder = 0
    end
    object Button2: TButton
      Left = 232
      Top = 42
      Width = 100
      Height = 21
      Caption = #20174#25991#20214#23433#35013
      TabOrder = 2
      OnClick = Button2Click
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 288
    Width = 341
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object ProgressBar1: TProgressBar
    Left = 0
    Top = 267
    Width = 341
    Height = 21
    Align = alBottom
    TabOrder = 4
  end
  object ADOConnection1: TADOConnection
    LoginPrompt = False
    Left = 184
    Top = 48
  end
  object SaveDialog1: TSaveDialog
    Left = 136
    Top = 48
  end
  object SQLDMOBackupSink1: TSQLDMOBackupSink
    PercentComplete = SQLDMOBackupSink1PercentComplete
    Left = 176
    Top = 80
  end
  object SQLDMORestoreSink1: TSQLDMORestoreSink
    PercentComplete = SQLDMOBackupSink1PercentComplete
    Left = 144
    Top = 76
  end
  object Timer1: TTimer
    Enabled = False
    OnTimer = Timer1Timer
    Left = 216
    Top = 152
  end
end
