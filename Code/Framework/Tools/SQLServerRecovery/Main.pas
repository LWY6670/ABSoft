//127.0.0.1 '' '' RUN1 "D:\其它项目\一卡通\DataBase\ABFramework" ABFramework A
unit Main;

interface
                              
uses
  ABPubServiceU,
  ABPubInPutStrU,
  ABPubMessageU,
  ABPubUserU,
  ABPubVarU,
  ABPubFuncU,
  ABPubFormU,
  ABPubConstU,

  ABThirdDBU,
  ABThirdConnU,
  ABThirdCustomQueryU,
  ABThirdConnDatabaseU,

  Dialogs,Menus,Variants,SQLDMOEvents,DB,ADODB,ExtCtrls,Buttons,StdCtrls,Controls,
  Classes,ComCtrls,SQLDMO_TLB,ComObj,SysUtils,Forms;

type
  TMainForm = class(TForm)
    GroupBox1: TGroupBox;
    Label8: TLabel;
    Label9: TLabel;
    Label22: TLabel;
    RadioGroup1: TRadioGroup;
    Edit1: TEdit;
    Edit2: TEdit;
    ComboBox1: TComboBox;
    Button4: TButton;
    RadioGroup2: TRadioGroup;
    ADOConnection1: TADOConnection;
    SaveDialog1: TSaveDialog;
    SQLDMOBackupSink1: TSQLDMOBackupSink;
    SQLDMORestoreSink1: TSQLDMORestoreSink;
    GroupBox4: TGroupBox;
    Label20: TLabel;
    Label21: TLabel;
    SpeedButton3: TSpeedButton;
    Label1: TLabel;
    SpeedButton4: TSpeedButton;
    Edit6: TEdit;
    Edit5: TEdit;
    Button3: TButton;
    Edit7: TEdit;
    SpeedButton1: TSpeedButton;
    GroupBox3: TGroupBox;
    Label18: TLabel;
    Label19: TLabel;
    SpeedButton2: TSpeedButton;
    Edit9: TEdit;
    Edit4: TEdit;
    Button2: TButton;
    StatusBar1: TStatusBar;
    Timer1: TTimer;
    ProgressBar1: TProgressBar;
    function SQLDMOBackupSink1PercentComplete(Sender: TObject;
      const Message: WideString; Percent: Integer): HRESULT;
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Edit2Change(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Button4Click(Sender: TObject);
  private
    FClientType,FSysCode:string;
    FshowMsg_ByParams :boolean;
    function CheckLogin: boolean;
    { Private declarations }
  public
    tempSQLDataPath:string;
    procedure AutoRunByParams;
    { Public declarations }
  end;

var
  MainForm: TMainForm;

implementation

                                               
{$R *.dfm}

procedure TMainForm.Button2Click(Sender: TObject);
var
  sv:_SQLServer;
  Re:_Restore;             
  SQLServer:Variant;
  Restore: Variant;
  tempstr1,tempstr2:string;
  tempADODataSet:TADODataSet;
begin
  if  (((Edit4.Text)='')  or (not ABCheckFileExists(Edit4.Text))) then
  begin
    if (FshowMsg_ByParams) then
    begin
      ShowMessage('请先选择要安装的数据库文件.');
      if Edit4.CanFocus then
        Edit4.SetFocus;
    end;
    Exit;
  end;
  if  ((Edit9.Text)='')   then
  begin
    if (FshowMsg_ByParams) then
    begin
      ShowMessage('请先输入要安装的数据库名.');
      if Edit9.CanFocus then
        Edit9.SetFocus;
    end;
    Exit;
  end;

  if (ABGetSQLValue('Main','use master  select count(*) from sysdatabases where name='+ABQuotedStr(Edit9.Text),[],0)<'1') or
     (ABShow('数据库已存在,是否要覆盖,覆盖后原数据库的数据将全部丢失?',[],['是','否'],2)=1) then
  begin
    if (Edit1.Text =emptystr)  then
    begin
      RadioGroup2.ItemIndex:=0;
    end;
    tempADODataSet:=nil;
    Button2.Enabled:=False;
    StatusBar1.Panels[0].Text:=('正在从文件安装数据库,请稍等');
    Application.ProcessMessages;
    try
      if (ABGetSQLValue('Main','use master  select count(*) from  sysprocesses where dbid=db_id('+ABQuotedStr(Edit9.Text)+')',[],0)>='1')   then
      begin
        tempstr2:=
        ' use master                                        '+
        ' declare    @sql    nvarchar(500)                  '+
        ' declare    @spid    int                           '+
        ' set    @sql='' declare    getspid    cursor    for  '+
                       ' select    spid                       '+
                       ' from    master.dbo.sysprocesses      '+
                       ' where    dbid=db_id('+''''''+Edit9.Text+''''''+')'''+      
        ' exec    (@sql)                                    '+
        ' open    getspid                                   '+
        ' fetch    next    from    getspid    into    @spid '+
        ' while    @@fetch_status    <>-1                   '+
        '   begin                                           '+
        '   exec('' kill    ''+@spid)                         '+
        '   fetch    next    from    getspid    into    @spid '+
        ' end                                                 '+
        ' close    getspid                                    '+
        ' deallocate    getspid                               ';
        ADOConnection1.Execute(tempstr2);
      end;

      if ABIsLocal(ComboBox1.Text) then
      begin
        //'此处指定恢复的物理文件名
        tempADODataSet:=TADODataSet.Create(nil);
        tempADODataSet.connection:= ADOConnection1;
        tempADODataSet.commandtext:=' RESTORE FILELISTONLY FROM DISK ='+ABQuotedStr(ABGetShortFileName(Edit4.Text));
        tempADODataSet.Open;
        tempstr1 :='[' + tempADODataSet.Fields[0].AsString+']' + ',' + '[' + ABGetShortPath(ABGetpath(tempSQLDataPath))+ Trim(Edit9.Text) + '.mdf'+']';
        tempADODataSet.Next;
        tempstr1 :=tempstr1 +
                  ',[' + tempADODataSet.Fields[0].AsString+']' + ',' + '[' +ABGetShortPath(ABGetpath(tempSQLDataPath)) + Trim(Edit9.Text) + '.LDF'+']';
        ProgressBar1.Position:=0;
        if RadioGroup1.ItemIndex=0 then
        begin
          sv := CoSQLServer.Create;
          sv.ApplicationName :='_SQLServer';
          Re:=CoRestore.Create;

          if RadioGroup2.ItemIndex=0 then
          begin
            sv.LoginSecure := True;
            sv.AutoReConnect := False;
            sv.Connect(ComboBox1.Text,edit1.Text,edit2.Text)  ;
            //设置为WINDOWS登录模式
            sv.IntegratedSecurity.SecurityMode:=1;
          end
          else
          begin
            sv.LoginSecure := False;
            sv.AutoReConnect := False;
            sv.Connect(ComboBox1.Text,edit1.Text,edit2.Text)  ;
            //设置为混合登录模式
            sv.IntegratedSecurity.SecurityMode:=2;
          end;

          Re.Action := SQLDMORestore_Database;
          Re.Database := edit9.Text;
          Re.Replacedatabase:=true;
          Re.RelocateFiles := tempstr1;
          Re.Files :=ABGetShortFileName(Edit4.Text);

          SQLDMORestoreSink1.Connect((Re));
          Re.SQLRestore(sv);
        end
        else if RadioGroup1.ItemIndex=1 then
        begin
          sqlserver := CreateOleObject( 'SQLDMO.SQLServer');
          Restore :=   CreateOleObject( 'SQLDMO.Restore');

          if RadioGroup2.ItemIndex=0 then
          begin
            sqlserver.LoginSecure:=True;
            sqlserver.AutoReConnect := False;
            sqlserver.Connect(ComboBox1.Text)  ;
            //设置为WINDOWS登录模式
            sqlserver.IntegratedSecurity.SecurityMode:=1;
          end
          else
          begin
            sqlserver.LoginSecure:=False;
            sqlserver.AutoReConnect := False;
            sqlserver.Connect(ComboBox1.Text,edit1.Text,edit2.Text)  ;
            //设置为混合登录模式
            sqlserver.IntegratedSecurity.SecurityMode:=2;
          end;

          Restore.Action:=SQLDMORestore_Database;
          Restore.Database := edit9.Text;
          Restore.Replacedatabase:=true;
          Restore.RelocateFiles := tempstr1;
          Restore.Files := ABGetShortFileName(Edit4.Text);
          SQLDMORestoreSink1.Connect(IUnknown(Restore));
          Restore.SQLRestore(sqlserver);
          sqlserver.close;
        end;
      end
      else
      begin
        ProgressBar1.Min:=0;
        ProgressBar1.Max:=10;
        ProgressBar1.Position:=0;

        ABUpFileToField('Main','##Temp_BackDatabase','Pkg','where Name='+ABQuotedStr('Back'),Edit4.Text);
        ProgressBar1.Position:=6;
        tempStr1:=

        ' execute master..xp_cmdshell ''c:\TextCopy.exe /S localhost /U '+edit1.Text+' /P '+
                                       edit2.Text+' /D '+'master'+' /T '+
                                       '##Temp_BackDatabase'+' /C '+'Pkg'+' /W "'+
                                       'where Name=''''Back'''''+'" /F '+
                                       'c:\tempUnBackDataBase.bak'+' /O ''';
        ADOConnection1.Execute(tempStr1);
        ProgressBar1.Position:=8;
        tempADODataSet:=TADODataSet.Create(nil);
        tempADODataSet.connection:= ADOConnection1;
        tempADODataSet.commandtext:=' RESTORE FILELISTONLY FROM DISK ='+ABQuotedStr('c:\tempUnBackDataBase.bak');
        tempADODataSet.Open;

        tempstr1 :=
        ' RESTORE DATABASE ['+Edit9.Text+'] FROM  DISK = N'''+'c:\tempUnBackDataBase.bak'+''''+
        ' WITH  FILE = 1,  NOUNLOAD ,  STATS = 10,  RECOVERY ,  REPLACE ,                        '+
        ' MOVE N'''+tempADODataSet.Fields[0].AsString+''' TO N'''+ABGetShortPath(ABGetpath(tempSQLDataPath))+ Trim(Edit9.Text) + '.mdf'+''',';
          tempADODataSet.Next;
        tempstr1 :=tempstr1+
        ' MOVE N'''+tempADODataSet.Fields[0].AsString+''' TO N'''+ABGetShortPath(ABGetpath(tempSQLDataPath))+ Trim(Edit9.Text) + '.ldf'+'''';
        ADOConnection1.Execute(tempStr1);
        ProgressBar1.Position:=10;
      end;

      if FClientType<>emptystr then
      begin
        ADOConnection1.Execute('use '+edit9.Text);
        ADOConnection1.Execute('exec Proc_SetClientType '+QuotedStr(FSysCode)+','+QuotedStr(FClientType)+',1');
      end;

      if Assigned(Sender) then
      begin
        StatusBar1.Panels[0].Text:=('从文件安装数据库成功');
        if FshowMsg_ByParams then
          ABShow('从文件安装数据库成功.');
      end;
    finally
      tempADODataSet.Free;
      Button2.Enabled:=true;
      ProgressBar1.Position:=0;
      StatusBar1.Panels[0].Text:=' ';

      Re:= nil;
      sv:= nil;
      sqlserver := null;
      Restore := null;
      DeleteFile('c:\tempUnBackDataBase.bak');
    end;
  end;
end;

procedure TMainForm.Button3Click(Sender: TObject);
var
  tempstr1:string;
begin
  if  (((Edit5.Text)='')  or (not ABCheckFileExists(Edit5.Text))) then
  begin
    if (FshowMsg_ByParams) then
    begin
      ShowMessage('请先选择要附加的数据库数据文件.');
      if Edit5.CanFocus then
        Edit5.SetFocus;
    end;
    Exit;
  end;
  if  (((Edit7.Text)='')  or (not ABCheckFileExists(Edit7.Text))) then
  begin
    if (FshowMsg_ByParams) then
    begin
      ShowMessage('请先选择要附加的数据库日志文件.');
      if Edit7.CanFocus then
        Edit7.SetFocus;
    end;
    Exit;
  end;
  if  (Edit6.Text='') then
  begin
    if (FshowMsg_ByParams) then
    begin
      ShowMessage('请先输入要附加的数据库名.');
      if Edit6.CanFocus then                                                         
        Edit6.SetFocus;
    end;
    Exit;
  end;

  Button3.Enabled:=False;
  StatusBar1.Panels[0].Text:=('正在附加文件,请稍等');
  Application.ProcessMessages;
  ProgressBar1.Min:=0;
  ProgressBar1.Max:=10;
  ProgressBar1.Position:=0;
  try
    if ABIsLocal(ComboBox1.Text) then
    begin
      RenameFile(Edit5.Text,ABGetFilepath(Edit5.Text)+Edit6.Text+'.mdf');
      RenameFile(Edit7.Text,ABGetFilepath(Edit7.Text)+Edit6.Text+'.ldf');
      tempstr1 :='EXEC sp_attach_db @dbname = N'+ABQuotedStr(Edit6.Text)+','  +
       '@filename1 = N'+ABQuotedStr(ABGetShortPath(ABGetFilepath(Edit5.Text))+Edit6.Text+'.mdf')+',' ;
      tempstr1 :=tempstr1 +
       '@filename2 = N'+ABQuotedStr(ABGetShortPath(ABGetFilepath(Edit7.Text))+Edit6.Text+'.ldf')  ;
    end
    else
    begin
      ABUpFileToField('Main','##Temp_BackDatabase','Pkg','where Name='+ABQuotedStr('Back'),Edit5.Text);
      ProgressBar1.Position:=3;
      tempStr1:=
      ' execute master..xp_cmdshell ''c:\TextCopy.exe /S localhost /U '+edit1.Text+' /P '+
                                     edit2.Text+' /D '+'master'+' /T '+
                                     '##Temp_BackDatabase'+' /C '+'Pkg'+' /W "'+
                                     'where Name=''''Back'''''+'" /F '+
                                     ABGetShortPath(ABGetpath(tempSQLDataPath))+Edit6.Text+'.mdf'+' /O ''';
      ADOConnection1.Execute(tempStr1);
      ProgressBar1.Position:=4;
      ABUpFileToField('Main','##Temp_BackDatabase','Pkg','where Name='+ABQuotedStr('Back'),Edit7.Text);
      ProgressBar1.Position:=7;
      tempStr1:=
      ' execute master..xp_cmdshell ''c:\TextCopy.exe /S localhost /U '+edit1.Text+' /P '+
                                     edit2.Text+' /D '+'master'+' /T '+
                                     '##Temp_BackDatabase'+' /C '+'Pkg'+' /W "'+
                                     'where Name=''''Back'''''+'" /F '+
                                     ABGetShortPath(ABGetpath(tempSQLDataPath))+Edit6.Text+'.ldf'+' /O ''';
      ADOConnection1.Execute(tempStr1);
      ProgressBar1.Position:=8;

      tempstr1 :='EXEC sp_attach_db @dbname = N'+ABQuotedStr(Edit6.Text)+','  +
       '@filename1 = N'+ABQuotedStr(ABGetShortPath(ABGetpath(tempSQLDataPath))+Edit6.Text+'.mdf')+',' ;
      tempstr1 :=tempstr1 +
       '@filename2 = N'+ABQuotedStr(ABGetShortPath(ABGetpath(tempSQLDataPath))+Edit6.Text+'.ldf')  ;
    end;

    ADOConnection1.Execute(tempstr1);
    ProgressBar1.Position:=10;
    if Assigned(Sender) then
    begin
      StatusBar1.Panels[0].Text:=('附加成功');
      if FshowMsg_ByParams then
        ShowMessage('数据库附加成功.');
    end;
  finally
    StatusBar1.Panels[0].Text:='';
    ProgressBar1.Position:=0;
    Button3.Enabled:=true;
  end;
end;

procedure TMainForm.Button4Click(Sender: TObject);
begin
  CheckLogin;
end;

function TMainForm.CheckLogin:boolean;
var
  tempStr:string;
begin
  result:=false;
  if  (Edit1.Text =emptystr)   then
  begin
    RadioGroup2.ItemIndex:=0;
  end;

  Button4.Enabled:=False;
  tempStr:='';
  try
    try
      ADOConnection1.Close;
      if RadioGroup2.ItemIndex=0 then
      begin
        tempStr:='Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False';
        if ComboBox1.Text<>EmptyStr then
          tempStr:=tempStr+';Data Source='+ComboBox1.Text;
      end
      else if RadioGroup2.ItemIndex=1 then
      begin
        tempStr:='Provider=SQLOLEDB.1';
        if Edit2.Text<>EmptyStr then
          tempStr:=tempStr+';Password='+Edit2.Text;
        tempStr:=tempStr+';Persist Security Info=True';
        if Edit1.Text<>EmptyStr then
          tempStr:=tempStr+';User ID='+Edit1.Text;
        if ComboBox1.Text<>EmptyStr then
          tempStr:=tempStr+';Data Source='+ComboBox1.Text;
      end;
      ADOConnection1.ConnectionString:=tempStr;
      ADOConnection1.Open;

      tempSQLDataPath := ExtractFilePath(
        ABGetSQLValue('Main','select filename from master..sysdatabases where name=''master''',[],'')
        );

      if ABIsLocal(ComboBox1.Text) then
      begin

      end
      else
      begin
        //因为TextCopy.exe执行时路径不能包含空格，故先将其copy到根目录下
        tempStr:=
        ' declare  @tempSQL  varchar(2000)                           '+
        ' exec  master.dbo.xp_regread                                     '+
        '            ''HKEY_LOCAL_MACHINE'',                                '+
        '            ''SOFTWARE\Microsoft\MSSQLSERVER\setup'',              '+
        '            ''SQLPath'',@tempSQL  output                      '+
        ' set  @tempSQL  = ''copy "''+ @tempSQL  +  ''\binn\TextCopy.exe " "c:\TextCopy.exe"'''+

        ' execute master..xp_cmdshell @tempSQL '+
        ' if exists (select * from tempdb.dbo.sysobjects where id = object_id(N''tempdb.[dbo].[##Temp_BackDatabase]'')  )' +
        ' drop table [##Temp_BackDatabase]' +
        ' CREATE TABLE  [##Temp_BackDatabase] (' +
        '	[Name] [varchar] (100) COLLATE Chinese_PRC_CI_AS NULL ,' +
        '	[Pkg] [image] NULL' +
        ' ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]' +
        ' insert into ##Temp_BackDatabase(Name,Pkg) values(''Back'' ,'''') ';

        ADOConnection1.Execute(tempStr);
      end;

      if RadioGroup2.ItemIndex=0 then
      begin
        result:=True;
        GroupBox3.Enabled:=true;
        GroupBox4.Enabled:=true;
        if FshowMsg_ByParams then
          ShowMessage('连接成功.');
      end
      else if RadioGroup2.ItemIndex=1 then
      begin
        result:=True;
        GroupBox3.Enabled:=true;
        GroupBox4.Enabled:=true;
        if FshowMsg_ByParams then
          ShowMessage('连接成功.');
      end;
    except
      GroupBox3.Enabled:=false;
      GroupBox4.Enabled:=false;
      ShowMessage('连接失败.');
    end;
  finally
    Button4.Enabled:=True;
  end;
end;

procedure TMainForm.Edit2Change(Sender: TObject);
begin
  GroupBox3.Enabled:=false;
  GroupBox4.Enabled:=false;
end;

procedure TMainForm.FormCreate(Sender: TObject);
var
  tempStr:string;
begin
  ABAddToConnList('Main',ADOConnection1);

  FClientType:=emptystr;
  FSysCode:=emptystr;
  FshowMsg_ByParams:=True;

  GroupBox3.Enabled:=false;
  GroupBox4.Enabled:=false;

  tempStr:=ABPubUser.HostIP;
  if ComboBox1.Items.IndexOf(tempStr)<0 then
    ComboBox1.Items.Add(tempStr);

  tempStr:=ABPubUser.HostName;
  if ComboBox1.Items.IndexOf(tempStr)<0 then
    ComboBox1.Items.Add(tempStr);

  if (ParamCount>=4) then
  begin
    if (LowerCase(ParamStr(4)) = 'run1')  then
    begin
      GroupBox4.Visible:=false;
      Height:=Height-GroupBox4.Height;
    end
    else if (LowerCase(ParamStr(4)) = 'run2')  then
    begin
      GroupBox3.Visible:=false;
      Height:=Height-GroupBox3.Height;
    end;

    bordericons:= bordericons-[biSystemMenu];
    BorderStyle:=bsToolWindow;
    Height:=64;
  end;
  Timer1.Enabled:=true;
end;

procedure TMainForm.FormDestroy(Sender: TObject);
begin
  if ABIsLocal(ComboBox1.Text) then
  begin

  end
  else
  begin
    if ADOConnection1.Connected then
      ADOConnection1.Execute(' execute master..xp_cmdshell ''del "c:\TextCopy.exe"'''+
                             ' drop table ##Temp_BackDatabase '
                             );
  end;
end;

procedure TMainForm.AutoRunByParams;
var
  tempStr:string;
  tempLogint:boolean;
begin
  if (ParamCount>=1) then
  begin
    //服务器名
    ComboBox1.Text :=ParamStr(1);
    if (ParamCount>=2) then
    begin
      //管理员
      Edit1.Text :=ParamStr(2);
      if (Edit1.Text='Null') or  (Edit1.Text='''')   then
        Edit1.Text:=emptystr;
    end;
    if (ParamCount>=3) then
    begin
      //管理员密码
      Edit2.Text :=ParamStr(3);
      if  ((Edit2.Text)='Null') or ((Edit2.Text)='''')   then
        Edit2.Text:=emptystr;
    end;

    if (ParamCount>=4) then
    begin
      try
        FshowMsg_ByParams:=false;

        repeat
          tempLogint:=CheckLogin;
          if not tempLogint then
          begin
            tempLogint:= ABShow('连接数据库失败,是否重新连接?',[],['重新连接','退出'],1)=2;
            if not tempLogint then
            begin
              if ABInPutStr('服务器',tempStr,'请输入服务器服务器名,可为IP或机器名称') then
              begin
                if tempStr<>EmptyStr then
                  ComboBox1.Text:=tempStr;
              end;
            end;
          end;
        until tempLogint;

        if (LowerCase(ParamStr(4)) = 'run1')  then
        begin
          if (ParamCount>=5) then
          begin
            //文件名
            Edit4.Text :=ParamStr(5);
          end;
          if (ParamCount>=6) then
          begin
            //数据库名
            Edit9.Text :=ParamStr(6);
          end;

          if (ParamCount>=7) then
          begin
            //设置数据库中客户端类型
            FClientType :=ParamStr(7);
          end;

          if (ParamCount>=8) then
          begin
            //设置数据库中系统码
            FSysCode :=ParamStr(8);
          end;


          Button2Click(Button2);
          //ShowMessage('安装数据库完成,请点击下一步继续.');
        end
        else if (LowerCase(ParamStr(4)) = 'run2')  then
        begin
          if (ParamCount>=5) then
          begin
            //数据文件名
            Edit5.Text :=ParamStr(5);
          end;
          if (ParamCount>=6) then
          begin
            //日志文件名
            Edit7.Text :=ParamStr(6);
          end;
          if (ParamCount>=7) then
          begin
            //数据库名
            Edit6.Text :=ParamStr(7);
          end;
          if (ParamCount>=8) then
          begin
            //设置数据库中客户端类型
            FClientType :=ParamStr(8);
          end;

          if (ParamCount>=9) then
          begin
            //设置数据库中系统码
            FSysCode :=ParamStr(9);
          end;

          Button3Click(Button3);
          //ShowMessage('安装数据库完成,请点击下一步继续.');
        end;
      finally
        Close;
      end;
    end;
  end;
end;

procedure TMainForm.SpeedButton1Click(Sender: TObject);
begin
  ABGetSqlServerList(ComboBox1.Items);
end;

procedure TMainForm.SpeedButton2Click(Sender: TObject);
begin
  inherited;
  Edit4.Text:=ABSelectFile(Edit4.Text);
  Edit9.Text:=ABGetFilename(Edit4.Text);
end;

procedure TMainForm.SpeedButton3Click(Sender: TObject);
begin
  inherited;
  Edit5.Text:=ABSelectFile(Edit5.Text,'','mdf *.mdf|*.mdf');
  Edit6.Text:=ABGetFilename(Edit5.Text);
end;                                                                             

procedure TMainForm.SpeedButton4Click(Sender: TObject);
begin
  inherited;
  Edit7.Text:=ABSelectFile(Edit7.Text,'','ldf *.ldf|*.ldf');
end;

function TMainForm.SQLDMOBackupSink1PercentComplete(Sender: TObject;
  const Message: WideString; Percent: Integer): HRESULT;
begin
  inherited;
  result:=1;
  ProgressBar1.Position := percent;
  Application.ProcessMessages;
end;


procedure TMainForm.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled:=false;
  try
    AutoRunByParams;
  finally
  end;
end;

end.








