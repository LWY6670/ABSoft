unit PathSetU;

interface

uses
  ABPubDownButtonU,
  ABPubFormU,
  ABPubVarU,
  ABPubConstU,
  ABPubFuncU,

  Windows,Messages,SysUtils,Variants,Classes,Graphics,Controls,Forms,Dialogs,
  StdCtrls,Menus,ExtCtrls;

type
  TPathSetForm = class(TABPubForm)
    GroupBox1: TGroupBox;
    lst1: TListBox;
    Panel2: TPanel;
    edtLibraryDir: TEdit;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Button1: TButton;
    Button22: TButton;
    Button2: TButton;
    procedure Button22Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure lst1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    FDelphiVersion:TABDelphiVersion;

  end;

var
  PathSetForm:TPathSetForm;

implementation

{$R *.dfm}

procedure TPathSetForm.Button1Click(Sender: TObject);
begin
  if lst1.ItemIndex>=0 then
    lst1.DeleteSelected;
end;

procedure TPathSetForm.Button2Click(Sender: TObject);
var
  I: Integer;
begin
  for I := lst1.Count-1 downto 0 do
  begin
    if not ABCheckDirExists(ABReplaceEnvironment(lst1.Items[i])) then
    begin
      lst1.Items.Delete(i);
    end;
  end;
end;

procedure TPathSetForm.Button22Click(Sender: TObject);
begin
  ABStrsToStrings(ABGetDelphiSeachPath(FDelphiVersion),';',lst1.Items);
end;

procedure TPathSetForm.Button3Click(Sender: TObject);
begin
  if (edtLibraryDir.Text<>EmptyStr) and
     (lst1.Items.IndexOf(edtLibraryDir.Text)<0)  then
  begin
    lst1.Items.Add(edtLibraryDir.Text);
    lst1.ItemIndex:=lst1.Count-1;
  end;
end;

procedure TPathSetForm.Button4Click(Sender: TObject);
begin
  if (edtLibraryDir.Text<>EmptyStr) and
     (lst1.ItemIndex>=0) then
    lst1.Items[lst1.ItemIndex]:=edtLibraryDir.Text;
end;

procedure TPathSetForm.Button5Click(Sender: TObject);
var
  tempStr:string;
begin
  tempStr:= ABSelectDirectory(edtLibraryDir.Text);
  if tempStr<>EmptyStr then
  begin
    edtLibraryDir.Text:=tempStr;
  end;
end;

procedure TPathSetForm.lst1Click(Sender: TObject);
begin
  if lst1.ItemIndex>=0 then
    edtLibraryDir.Text:=lst1.Items[lst1.ItemIndex];
end;

end.

