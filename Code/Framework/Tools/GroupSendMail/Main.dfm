object MainForm: TMainForm
  Left = 220
  Top = 137
  Caption = #37038#20214#32676#21457#31995#32479'('#20165#25903#25345'D2007'#21450#20197#21518#29256#26412')'
  ClientHeight = 432
  ClientWidth = 647
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnPaint = FormPaint
  PixelsPerInch = 96
  TextHeight = 13
  object Panel3: TPanel
    Left = 0
    Top = 393
    Width = 647
    Height = 39
    Align = alBottom
    TabOrder = 0
    DesignSize = (
      647
      39)
    object btn1: TButton
      Left = 561
      Top = 9
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #36864#20986
      TabOrder = 0
      OnClick = btn1Click
    end
    object Button1: TButton
      Left = 16
      Top = 6
      Width = 146
      Height = 25
      Caption = #21457#36865
      TabOrder = 1
      OnClick = Button1Click
    end
  end
  object pb1: TProgressBar
    Left = 0
    Top = 376
    Width = 647
    Height = 17
    Align = alBottom
    TabOrder = 1
    Visible = False
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 647
    Height = 376
    ActivePage = TabSheet3
    Align = alClient
    TabOrder = 2
    object TabSheet1: TTabSheet
      Caption = #37038#20214#20869#23481
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 639
        Height = 348
        Align = alClient
        TabOrder = 0
        DesignSize = (
          639
          348)
        object lbl1: TLabel
          Left = 29
          Top = 37
          Width = 50
          Height = 13
          AutoSize = False
          Caption = #37038#20214#20869#23481
        end
        object Label2: TLabel
          Left = 29
          Top = 149
          Width = 51
          Height = 13
          AutoSize = False
          Caption = #38468#20214
        end
        object btn2: TSpeedButton
          Left = 86
          Top = 121
          Width = 70
          Height = 22
          Caption = #21152#36733
          Flat = True
          OnClick = btn2Click
        end
        object SpeedButton1: TSpeedButton
          Left = 162
          Top = 121
          Width = 70
          Height = 22
          Caption = #20445#23384
          Flat = True
          OnClick = SpeedButton1Click
        end
        object SpeedButton2: TSpeedButton
          Left = 236
          Top = 321
          Width = 70
          Height = 22
          Caption = #21024#38500
          Flat = True
          OnClick = SpeedButton2Click
        end
        object SpeedButton3: TSpeedButton
          Left = 160
          Top = 321
          Width = 70
          Height = 22
          Hint = #30446#24405#21387#32553#21518#20316#20026#38468#20214#21457#36865','#35831#30830#35748#26426#22120#20013#26377'WinRAR'#31243#24207
          Caption = #21152#36733#30446#24405
          Flat = True
          ParentShowHint = False
          ShowHint = True
          OnClick = SpeedButton3Click
        end
        object SpeedButton4: TSpeedButton
          Left = 84
          Top = 321
          Width = 70
          Height = 22
          Hint = #25991#20214#21333#29420#20316#20026#38468#20214#21457#36865
          Caption = #21152#36733#25991#20214
          Flat = True
          ParentShowHint = False
          ShowHint = True
          OnClick = SpeedButton4Click
        end
        object mmo1: TMemo
          Tag = 910
          Left = 80
          Top = 36
          Width = 548
          Height = 79
          Anchors = [akLeft, akTop, akRight]
          ImeName = #24555#20048#20116#31508
          Lines.Strings = (
            'mmo1')
          TabOrder = 0
        end
        object LabeledEdit4: TLabeledEdit
          Tag = 91
          Left = 80
          Top = 10
          Width = 548
          Height = 21
          Anchors = [akLeft, akTop, akRight]
          AutoSize = False
          EditLabel.Width = 48
          EditLabel.Height = 13
          EditLabel.Caption = #37038#20214#26631#39064
          ImeName = #24555#20048#20116#31508
          LabelPosition = lpLeft
          TabOrder = 1
        end
        object lst1: TListBox
          Tag = 910
          Left = 86
          Top = 149
          Width = 548
          Height = 141
          Anchors = [akLeft, akTop, akRight]
          ImeName = #24555#20048#20116#31508
          ItemHeight = 13
          Items.Strings = (
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa')
          TabOrder = 2
        end
        object LabeledEdit5: TLabeledEdit
          Tag = 91
          Left = 82
          Top = 294
          Width = 548
          Height = 21
          Anchors = [akLeft, akTop, akRight]
          AutoSize = False
          EditLabel.Width = 63
          EditLabel.Height = 13
          EditLabel.Caption = 'WinRAR'#30446#24405
          ImeName = #24555#20048#20116#31508
          LabelPosition = lpLeft
          TabOrder = 3
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = #32676#21457#22320#22336
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object DBGrid1: TDBGrid
        Left = 0
        Top = 0
        Width = 639
        Height = 323
        Align = alClient
        DataSource = ds1
        ImeName = #24555#20048#20116#31508
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
      end
      object ABMultilingualDBNavigator1: TABMultilingualDBNavigator
        Left = 0
        Top = 323
        Width = 639
        Height = 25
        DataSource = ds1
        VisibleButtons = [nbInsert, nbDelete, nbEdit, nbPost, nbCancel]
        Align = alBottom
        Flat = True
        TabOrder = 1
      end
    end
    object TabSheet3: TTabSheet
      Caption = #39640#32423#35774#32622
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Label1: TLabel
        Left = 3
        Top = 159
        Width = 51
        Height = 13
        AutoSize = False
        Caption = #23450#26102#26102#38388
      end
      object Label3: TLabel
        Left = 3
        Top = 188
        Width = 51
        Height = 13
        AutoSize = False
        Caption = #38388#38548#31186
      end
      object Label4: TLabel
        Left = 3
        Top = 212
        Width = 51
        Height = 13
        AutoSize = False
        Caption = #38388#38548#31186
      end
      object Label5: TLabel
        Left = 221
        Top = 156
        Width = 409
        Height = 13
        AutoSize = False
        Caption = #23384#22312#25351#23450#31243#24207#30340#36827#31243#26102#19981#21457#36865#37038#20214'('#22914':QQ.exe)'
      end
      object DateTimePicker1: TDateTimePicker
        Tag = 91
        Left = 78
        Top = 157
        Width = 130
        Height = 21
        Date = 40694.500000000000000000
        Time = 40694.500000000000000000
        ImeName = #24555#20048#20116#31508
        Kind = dtkTime
        TabOrder = 0
      end
      object RadioGroup1: TRadioGroup
        Tag = 91
        Left = 0
        Top = 105
        Width = 639
        Height = 40
        Align = alTop
        Caption = #33258#21160#21457#36865
        Columns = 3
        Ctl3D = True
        ItemIndex = 0
        Items.Strings = (
          #27599#26085#23450#26102#33258#21160#21457#36865
          #27599#36807'N'#31186#33258#21160#21457#36865
          #20851#38381#33258#21160#21457#36865)
        ParentCtl3D = False
        TabOrder = 1
        OnClick = RadioGroup1Click
      end
      object SpinEdit1: TSpinEdit
        Tag = 91
        Left = 78
        Top = 184
        Width = 130
        Height = 22
        MaxValue = 100000000
        MinValue = 1
        TabOrder = 2
        Value = 60
      end
      object grp1: TGroupBox
        Left = 0
        Top = 0
        Width = 639
        Height = 105
        Align = alTop
        Caption = #30331#24405#26381#21153#22120#35774#32622
        TabOrder = 3
        object LabeledEdit1: TLabeledEdit
          Tag = 91
          Left = 80
          Top = 16
          Width = 242
          Height = 21
          EditLabel.Width = 62
          EditLabel.Height = 13
          EditLabel.Caption = 'SMTP'#26381#21153#22120
          ImeName = #24555#20048#20116#31508
          LabelPosition = lpLeft
          TabOrder = 0
        end
        object LabeledEdit2: TLabeledEdit
          Tag = 91
          Left = 80
          Top = 43
          Width = 121
          Height = 21
          EditLabel.Width = 36
          EditLabel.Height = 13
          EditLabel.Caption = #29992#25143#21517
          ImeName = #24555#20048#20116#31508
          LabelPosition = lpLeft
          TabOrder = 1
        end
        object LabeledEdit3: TLabeledEdit
          Tag = 91
          Left = 80
          Top = 70
          Width = 121
          Height = 21
          EditLabel.Width = 24
          EditLabel.Height = 13
          EditLabel.Caption = #23494#30721
          ImeName = #24555#20048#20116#31508
          LabelPosition = lpLeft
          PasswordChar = '*'
          TabOrder = 2
        end
      end
      object CheckBox1: TCheckBox
        Tag = 91
        Left = 78
        Top = 237
        Width = 130
        Height = 17
        Caption = #38543#31995#32479#33258#21160#21551#21160
        TabOrder = 4
      end
      object HotKey1: THotKey
        Tag = 91
        Left = 78
        Top = 212
        Width = 130
        Height = 19
        HotKey = 49242
        Modifiers = [hkCtrl, hkAlt]
        TabOrder = 5
        OnChange = HotKey1Change
      end
      object HotKey2: THotKey
        Left = 209
        Top = 213
        Width = 130
        Height = 19
        HotKey = 49220
        Modifiers = [hkCtrl, hkAlt]
        TabOrder = 6
        Visible = False
        OnChange = HotKey1Change
      end
      object Memo1: TMemo
        Tag = 911
        Left = 221
        Top = 175
        Width = 409
        Height = 107
        ImeName = #24555#20048#20116#31508
        TabOrder = 7
      end
      object LabeledEdit6: TLabeledEdit
        Tag = 91
        Left = 78
        Top = 260
        Width = 121
        Height = 21
        EditLabel.Width = 72
        EditLabel.Height = 13
        EditLabel.Caption = #30028#38754#35775#38382#23494#30721
        ImeName = #24555#20048#20116#31508
        LabelPosition = lpLeft
        TabOrder = 8
      end
    end
  end
  object qry1: TADOQuery
    Connection = con1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from ABSendMails')
    Left = 520
    Top = 208
    object qry1Group: TWideStringField
      DisplayLabel = #37038#20214#32452
      DisplayWidth = 15
      FieldName = 'Group'
      Size = 50
    end
    object qry1Name: TWideStringField
      DisplayLabel = #37038#20214#21517#31216
      DisplayWidth = 16
      FieldName = 'Name'
      Size = 50
    end
    object qry1Mail: TWideStringField
      DisplayLabel = #37038#20214#22320#22336
      DisplayWidth = 32
      FieldName = 'Mail'
      Size = 50
    end
    object qry1IsSend: TWideStringField
      DisplayLabel = #19978#27425#26159#21542#21457#20986
      DisplayWidth = 16
      FieldName = 'IsSend'
      Size = 10
    end
  end
  object ds1: TDataSource
    DataSet = qry1
    Left = 560
    Top = 208
  end
  object con1: TADOConnection
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=D:\ABSoft\Code\Item' +
      's\Tools\'#37038#20214#32676#21457#31995#32479'\ABMailSendsP_Set\ABMailSendsP_data.mdb;Persist Se' +
      'curity Info=False'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 488
    Top = 208
  end
  object IdMsg: TIdMessage
    AttachmentEncoding = 'MIME'
    BccList = <>
    CCList = <>
    Encoding = meMIME
    FromList = <
      item
      end>
    Recipients = <>
    ReplyTo = <>
    ConvertPreamble = True
    Left = 424
    Top = 208
  end
  object SMTP1: TIdSMTP
    SASLMechanisms = <>
    Left = 432
    Top = 112
  end
  object Timer1: TTimer
    OnTimer = Timer1Timer
    Left = 408
    Top = 272
  end
end
