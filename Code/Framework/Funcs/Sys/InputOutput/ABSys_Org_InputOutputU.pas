unit ABSys_Org_InputOutputU;

interface                         
                              
uses
  ABPubVarU,
  ABPubUserU,
  ABPubFuncU,
  ABPubLogU,
  ABPubConstU,
  ABPubDBU,
  ABPubPanelU,
  ABPubPassU,
  ABPubMessageU,
  ABPubPakandZipU,
  ABPubSelectCheckTreeViewU,

  ABThirdQueryU,
  ABThirdConnU,
  ABThirdDBU,
  ABThirdCustomQueryU,
  ABThirdFuncU,

  ABFrameworkcxGridU,
  ABFrameworkDBPanelU,
  ABFrameworkDictionaryQueryU,
  ABFrameworkDBNavigatorU,
  ABFrameworkFuncFormU,
  ABFrameworkControlU,
  ABFrameworkQueryU,
  ABFrameworkFuncU,

  cxTLdxBarBuiltInMenu,
  cxLookAndFeels,
  cxTLData,
  cxDBTL,
  cxInplaceContainer,
  cxMaskEdit,
  cxTL,
  cxButtons,
  cxLookAndFeelPainters,
  cxGrid,
  cxGridCustomView,
  cxControls,
  cxClasses,
  cxGridDBBandedTableView,
  cxGridBandedTableView,
  cxGridTableView,
  cxGridCustomTableView,
  cxGridLevel,
  cxSplitter,
  cxPC,
  cxGroupBox,
  cxContainer,
  cxDBData,
  cxEdit,
  cxDataStorage,
  cxData,
  cxFilter,
  cxGraphics,
  cxCustomData,
  cxStyles,
  dxStatusBar,
  dxdbtree,
  dxtree,

  types,
  Forms,StdConvs,DB,DBClient,StdCtrls,ComCtrls,Classes,Controls,ExtCtrls,SysUtils,Variants,
  Menus,Grids,DBGrids, cxCheckBox, cxTextEdit,
  cxDropDownEdit, cxCheckComboBox, Buttons, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, cxRadioGroup;

type
  TABSys_Org_InputOutputForm = class(TABFuncForm)
    Panel1: TPanel;
    Button1: TABcxButton;
    Button2: TABcxButton;
    Button3: TABcxButton;
    Splitter1: TSplitter;
    Panel3: TPanel;
    Memo1: TMemo;
    Button9: TABcxButton;
    ProgressBar1: TProgressBar;
    ABQuery_DatabaseTable: TABQuery;
    ABQuery_Func: TABQuery;
    ABQuery_ExtendReport: TABQuery;
    ABQuery_Operator: TABQuery;
    ABQuery_Key: TABQuery;
    ABQuery_Tree: TABQuery;
    ABQuery_Parameter: TABQuery;
    Label1: TLabel;
    ABQuery_Mail: TABQuery;
    ABQuery_HistoryData: TABQuery;
    ABQuery_Log: TABQuery;
    ABQuery_Approved: TABQuery;
    PageControl2: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    PageControl1: TPageControl;
    TabSheet6: TTabSheet;
    Panel20: TPanel;
    Panel21: TPanel;
    Button8: TABcxButton;
    Memo9: TMemo;
    Panel10: TPanel;
    Panel11: TPanel;
    SpeedButton2: TABcxButton;
    Memo3: TMemo;
    Panel2: TPanel;
    Panel6: TPanel;
    Button11: TABcxButton;
    Memo2: TMemo;
    Panel22: TPanel;
    Panel23: TPanel;
    Button10: TABcxButton;
    Memo10: TMemo;
    Panel7: TPanel;
    Panel8: TPanel;
    Button12: TABcxButton;
    Memo4: TMemo;
    TabSheet7: TTabSheet;
    Panel4: TPanel;
    Panel14: TPanel;
    Button5: TABcxButton;
    Memo5: TMemo;
    TabSheet3: TTabSheet;
    Panel16: TPanel;
    Panel17: TPanel;
    Button6: TABcxButton;
    Memo7: TMemo;
    Panel5: TPanel;
    Panel15: TPanel;
    Button4: TABcxButton;
    Memo6: TMemo;
    TabSheet4: TTabSheet;
    Panel9: TPanel;
    Panel12: TPanel;
    Button13: TABcxButton;
    Memo11: TMemo;
    TabSheet5: TTabSheet;
    Panel18: TPanel;
    Panel19: TPanel;
    Button7: TABcxButton;
    Memo8: TMemo;
    TabSheet8: TTabSheet;
    Panel13: TPanel;
    Panel24: TPanel;
    Button14: TABcxButton;
    Memo12: TMemo;
    TabSheet9: TTabSheet;
    Memo13: TMemo;
    Label2: TLabel;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    ABcxRadioGroup1: TABcxRadioGroup;
    ProgressBar2: TProgressBar;
    Panel25: TPanel;
    Panel26: TPanel;
    ABcxButton1: TABcxButton;
    Memo14: TMemo;
    ABQuery_BillInput: TABQuery;
    procedure Button3Click(Sender: TObject);
    procedure Memo2KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure Button1Click(Sender: TObject);
    procedure Button9Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure Button10Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button12Click(Sender: TObject);
    procedure Button11Click(Sender: TObject);
    procedure Button13Click(Sender: TObject);
    procedure Button14Click(Sender: TObject);
    procedure ABcxButton1Click(Sender: TObject);
  private
    procedure SetButtonRun;
    procedure SetButtonStop;
    procedure DoOutput;
    procedure DoInput;
    procedure DoClearDatasetFilter(Sender: TObject);
    procedure DoSetDatasetFilter(Sender: TObject);
    function DONextDetailProgressBar: boolean;
    function DONextMainProgressBar: boolean;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ABSys_Org_InputOutputForm: TABSys_Org_InputOutputForm;
  tempBegDown:TDateTime;

implementation



{$R *.dfm}

procedure ABRegister(var aFormClassName:string);stdcall;
begin
  aFormClassName  :=TABSys_Org_InputOutputForm.ClassName;
end;

exports
   ABRegister ;


procedure TABSys_Org_InputOutputForm.Button2Click(Sender: TObject);
begin
  SetButtonStop;
end;

procedure TABSys_Org_InputOutputForm.SetButtonRun;
begin
  ABSetControlEnabled(PageControl2,false);
  ABSetControlEnabled(Panel3,false);

  Button1.Enabled:=false;
  Button9.Enabled:=false;
  Button3.Enabled:=false;
  PageControl1.Enabled:=false;
  Button2.Enabled:=true;
end;

procedure TABSys_Org_InputOutputForm.SetButtonStop;
begin
  ABSetControlEnabled(PageControl2,true);
  ABSetControlEnabled(Panel3,true);

  Button1.Enabled:=true;
  Button9.Enabled:=true;
  Button3.Enabled:=true;
  PageControl1.Enabled:=true;
  Button2.Enabled:=false;
end;

procedure TABSys_Org_InputOutputForm.Button3Click(Sender: TObject);
begin
  close;
end;

procedure TABSys_Org_InputOutputForm.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  CanClose:=not Button2.Enabled;
  if not CanClose then
  begin
    abshow('请先停止');
  end;
end;

procedure TABSys_Org_InputOutputForm.FormCreate(Sender: TObject);
begin
  SetButtonStop;
  PageControl1.ActivePageIndex:=0;
  PageControl2.ActivePageIndex:=0;
end;

procedure TABSys_Org_InputOutputForm.Memo2KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if (Shift=[ssCtrl]) then
  begin
    if  (Key=65) then
    begin
      TMemo(Sender).SelectAll;
    end
  end;
end;

procedure TABSys_Org_InputOutputForm.DoSetDatasetFilter(Sender: TObject);
var
  tempFilterFieldName:string;
  tempDataset:TDataSet;
begin
  if (ABcxRadioGroup1.ItemIndex=1) or (ABcxRadioGroup1.ItemIndex=2) then
  begin
    tempFilterFieldName:=EmptyStr;
    tempDataset:=nil;
    if Sender=Button2 then
    begin
      tempFilterFieldName:='CL_Name';
      tempDataset:=ABQuery_DatabaseTable;
      if ABcxRadioGroup1.ItemIndex=1 then
      begin
        absetdatasetfilter(tempDataset,tempFilterFieldName+'='+QuotedStr('Main'));
      end
      else if ABcxRadioGroup1.ItemIndex=2 then
      begin
        absetdatasetfilter(tempDataset,tempFilterFieldName+'<>'+QuotedStr('Main'));
      end;
    end
    else
    begin
      if Sender=Button8 then
      begin
        tempFilterFieldName:='Tr_SysUse';
        tempDataset:=ABQuery_Tree;
      end
      else if Sender=Button10 then
      begin
        tempFilterFieldName:='PA_SysUse';
        tempDataset:=ABQuery_Parameter;
      end
      else if Sender=Button5 then
      begin
        tempFilterFieldName:='FU_SysUse';
        tempDataset:=ABQuery_Func;
      end
      else if Sender=Button7 then
      begin
        tempFilterFieldName:='ER_SysUse';
        tempDataset:=ABQuery_ExtendReport;
      end
      else if Sender=Button11 then
      begin
        tempFilterFieldName:='HD_SysUse';
        tempDataset:=ABQuery_HistoryData;
      end;

      if Assigned(tempDataset) then
      begin
        if ABcxRadioGroup1.ItemIndex=1 then
        begin
          absetdatasetfilter(tempDataset,ABGetBooleanFilter(tempDataset,tempFilterFieldName,True));
        end
        else if ABcxRadioGroup1.ItemIndex=2 then
        begin
          absetdatasetfilter(tempDataset,ABGetBooleanFilter(tempDataset,tempFilterFieldName,False));
        end;
      end;
    end;
  end;
end;

procedure TABSys_Org_InputOutputForm.DoClearDatasetFilter(Sender: TObject);
var
  tempDataset:TDataSet;
begin
  tempDataset:=nil;
  if Sender=Button2 then
  begin
    tempDataset:=ABQuery_DatabaseTable;
  end
  else if Sender=Button8 then
  begin
    tempDataset:=ABQuery_Tree;
  end
  else if Sender=Button10 then
  begin
    tempDataset:=ABQuery_Parameter;
  end
  else if Sender=Button5 then
  begin
    tempDataset:=ABQuery_Func;
  end
  else if Sender=Button7 then
  begin
    tempDataset:=ABQuery_ExtendReport;
  end
  else if Sender=Button11 then
  begin
    tempDataset:=ABQuery_HistoryData;
  end;

  if Assigned(tempDataset) then
  begin
    ABSetDatasetFilter(tempDataset,emptystr);
  end;
end;

procedure TABSys_Org_InputOutputForm.Button8Click(Sender: TObject);
begin
  if not ABQuery_Tree.Active then
    ABInitFormDataSet([],[],[],ABQuery_Tree);

  DoSetDatasetFilter(Sender);
  try
    ABSelectCheckTreeView(
      ABQuery_Tree,
      'Tr_Code',
      'Tr_Code',
      'Tr_Name',
      False,

      nil,
      '',
      '',

      Memo9.Lines,
      'Tr_Code'
      );
  finally
    DoClearDatasetFilter(Sender);
  end;
end;

procedure TABSys_Org_InputOutputForm.SpeedButton2Click(Sender: TObject);
begin
  if not ABQuery_DatabaseTable.Active then
    ABInitFormDataSet([],[],[],ABQuery_DatabaseTable);

  DoSetDatasetFilter(Sender);
  try
    ABSelectCheckTreeView(
      ABGetConstSqlPubDataset('ABSys_Org_TreeItem',['Table Dir']),
      'Ti_ParentGuid',
      'Ti_Guid',
      'Ti_Name',
      False,

      ABQuery_DatabaseTable,
      'TA_Ti_Guid',
      'tempName',

      Memo3.Lines,
      'tempCode'
      );
  finally
    DoClearDatasetFilter(Sender);
  end;
end;

procedure TABSys_Org_InputOutputForm.Button10Click(Sender: TObject);
begin
  if not ABQuery_Parameter.Active then
    ABInitFormDataSet([],[],[],ABQuery_Parameter);

  DoSetDatasetFilter(Sender);
  try
    ABSelectCheckTreeView(
      ABQuery_Parameter,
      'Pa_Name',
      'Pa_Name',
      'Pa_Name',
      False,

      nil,
      '',
      '',

      Memo10.Lines,
      'Pa_Name'
      );
  finally
    DoClearDatasetFilter(Sender);
  end;
end;

procedure TABSys_Org_InputOutputForm.Button12Click(Sender: TObject);
begin
  if not ABQuery_Mail.Active then
    ABInitFormDataSet([],[],[],ABQuery_Mail);

  ABSelectCheckTreeView(
    ABQuery_Mail,
    'tempCode',
    'tempCode',
    'tempName',
    False,

    nil,
    '',
    '',

    Memo4.Lines,
    'tempCode'
    );
end;

procedure TABSys_Org_InputOutputForm.Button11Click(Sender: TObject);
begin
  if not ABQuery_HistoryData.Active then
    ABInitFormDataSet([],[],[],ABQuery_HistoryData);

  DoSetDatasetFilter(Sender);
  try
    ABSelectCheckTreeView(
      ABQuery_HistoryData,
      'tempCode',
      'tempCode',
      'tempName',
      False,

      nil,
      '',
      '',

      Memo2.Lines,
      'tempCode'
      );
  finally
    DoClearDatasetFilter(Sender);
  end;
end;

procedure TABSys_Org_InputOutputForm.ABcxButton1Click(Sender: TObject);
begin
  if not ABQuery_BillInput.Active then
    ABInitFormDataSet([],[],[],ABQuery_BillInput);

  ABSelectCheckTreeView(
    ABQuery_BillInput,
    'Bi_Code',
    'Bi_Code',
    'Bi_Name',
    False,

    nil,
    '',
    '',

    Memo14.Lines,
    'Bi_Code'
    );
end;

procedure TABSys_Org_InputOutputForm.Button5Click(Sender: TObject);
begin
  if not ABQuery_Func.Active then
    ABInitFormDataSet([],[],[],ABQuery_Func);

  DoSetDatasetFilter(Sender);
  try
    ABSelectCheckTreeView(
      ABGetConstSqlPubDataset('ABSys_Org_TreeItem',['Function Dir']),
      'Ti_ParentGuid',
      'Ti_Guid',
      'Ti_Name',
      False,

      ABQuery_Func,
      'Fu_Ti_Guid',
      'Fu_Name',

      Memo5.Lines,
      'Fu_FileName'
      );
  finally
    DoClearDatasetFilter(Sender);
  end;
end;

procedure TABSys_Org_InputOutputForm.Button4Click(Sender: TObject);
begin
  if not ABQuery_Key.Active then
    ABInitFormDataSet([],[],[],ABQuery_Key);

  ABSelectCheckTreeView(
    ABQuery_Key,
    'Ke_Code',
    'Ke_Code',
    'Ke_Name',
    False,

    nil,
    '',
    '',

    Memo6.Lines,
    'Ke_Code'
    );
end;

procedure TABSys_Org_InputOutputForm.Button6Click(Sender: TObject);
begin
  if not ABQuery_Operator.Active then
    ABInitFormDataSet([],[],[],ABQuery_Operator);

  ABSelectCheckTreeView(
    ABGetConstSqlPubDataset('ABSys_Org_TreeItem',['Enterprise Dir']),
    'Ti_ParentGuid',
    'Ti_Guid',
    'Ti_Name',
    False,

    ABQuery_Operator,
    'op_Ti_Guid',
    'op_Name',

    Memo7.Lines,
    'op_Code'
    );
end;

procedure TABSys_Org_InputOutputForm.Button13Click(Sender: TObject);
begin
  if not ABQuery_Approved.Active then
    ABInitFormDataSet([],[],[],ABQuery_Approved);

  ABSelectCheckTreeView(
    ABQuery_Approved,
    'tempCode',
    'tempCode',
    'tempName',
    False,

    nil,
    '',
    '',

    Memo11.Lines,
    'tempCode'
    );
end;

procedure TABSys_Org_InputOutputForm.Button14Click(Sender: TObject);
begin
  if not ABQuery_Log.Active then
    ABInitFormDataSet([],[],[],ABQuery_Log);

  ABSelectCheckTreeView(
    ABQuery_Log,
    'tempCode',
    'tempCode',
    'tempName',
    False,

    nil,
    '',
    '',

    Memo12.Lines,
    'tempCode'
    );
end;

procedure TABSys_Org_InputOutputForm.Button7Click(Sender: TObject);
begin
  if not ABQuery_ExtendReport.Active then
    ABInitFormDataSet([],[],[],ABQuery_ExtendReport);

  DoSetDatasetFilter(Sender);
  try
    ABSelectCheckTreeView(
      ABGetConstSqlPubDataset('ABSys_Org_TreeItem',['Extend ReportType']),
      'Ti_ParentGuid',
      'Ti_Guid',
      'Ti_Name',
      False,

      ABQuery_ExtendReport,
      'ER_Ti_Guid',
      'ER_Name',

      Memo8.Lines,
      'ER_Name'
      );
  finally
    DoClearDatasetFilter(Sender);
  end;
end;

procedure TABSys_Org_InputOutputForm.Button9Click(Sender: TObject);
begin
  DoOutput;
end;

procedure TABSys_Org_InputOutputForm.Button1Click(Sender: TObject);
begin
  DoInput;
end;

function TABSys_Org_InputOutputForm.DONextMainProgressBar:boolean;
begin
  Result:=True ;
  Application.ProcessMessages;
  ProgressBar1.Position:=ProgressBar1.Position+1;
  if not Button2.Enabled then
  begin
    Result:=false;
  end;
end;
function TABSys_Org_InputOutputForm.DONextDetailProgressBar:boolean;
begin
  Result:=True ;
  Application.ProcessMessages;
  ProgressBar2.Position:=ProgressBar2.Position+1;
  if not Button2.Enabled then
  begin
    Result:=false;
  end;
end;

procedure TABSys_Org_InputOutputForm.DoOutput;
var
  //包文件名
  tempOutPath:String;
  tempOutFileName:String;

  //读取数据的数据集
  tempDatasetConnList                 ,
  tempDatasetTable                    ,
  tempDatasetField                    ,
  tempDatasetFunction                 ,
  tempDatasetFuncTemplate             ,
  tempDatasetFuncReport               ,
  tempDatasetFuncControlProperty      ,
  tempDatasetFuncSQL                  ,
  tempDatasetExtendReport             ,
  tempDatasetExtendReportDatasets     ,
  tempDatasetOperator                 ,
  tempDatasetKey                      ,
  tempDatasetOperatorKey              ,
  tempDatasetKeyFunction              ,
  tempDatasetKeyExtendReport          ,
  tempDatasetKeyTable                 ,
  tempDatasetKeyField                 ,
  tempDatasetKeyTableRow              ,
  tempDatasetKeyCustomObject          ,
  tempDatasetParameter                ,
  tempDatasetTree                     ,
  tempDatasetTreeItem                 ,

  tempDatasetMail                     ,
  tempDatasetMailFile                 ,
  tempDatasetHistoryData              ,
  tempDatasetBillInput                ,
  tempDatasetMessage                  ,
  tempDatasetRule                     ,
  tempDatasetBill                     ,
  tempDatasetConditionParam           ,
  tempDatasetPassRuleOperator         ,
  tempDatasetStep                     ,
  tempDatasetStepOperator             ,
  tempDatasetCurLink                  ,
  tempDatasetCustomEvent              ,
  tempDatasetErrorInfo                ,
  tempDatasetFieldChange              ,
  tempDatasetLogin                    :TDataSet;


  tempFieldNames_ABSys_Org_ConnList             :string;
  tempFieldNames_ABSys_Org_Table                :string;
  tempFieldNames_ABSys_Org_Field                :string;
  tempFieldNames_ABSys_Org_Function             :string;
  tempFieldNames_ABSys_Org_FuncTemplate         :string;
  tempFieldNames_ABSys_Org_FuncReport           :string;
  tempFieldNames_ABSys_Org_FuncSQL              :string;
  tempFieldNames_ABSys_Org_FuncControlProperty  :string;
  tempFieldNames_ABSys_Org_ExtendReport         :string;
  tempFieldNames_ABSys_Org_ExtendReportDatasets :string;
  tempFieldNames_ABSys_Org_Operator             :string;
  tempFieldNames_ABSys_Org_Key                  :string;
  tempFieldNames_ABSys_Right_OperatorKey        :string;
  tempFieldNames_ABSys_Right_KeyExtendReport    :string;
  tempFieldNames_ABSys_Right_KeyField           :string;
  tempFieldNames_ABSys_Right_KeyFunction        :string;
  tempFieldNames_ABSys_Right_KeyTable           :string;
  tempFieldNames_ABSys_Right_KeyTableRow        :string;
  tempFieldNames_ABSys_Right_KeyCustomObject    :string;

  tempFieldNames_ABSys_Org_Parameter            :string;
  tempFieldNames_ABSys_Org_Tree                 :string;
  tempFieldNames_ABSys_Org_TreeItem             :string;

  tempFieldNames_ABSys_Org_Mail                 :string;
  tempFieldNames_ABSys_Org_MailFile             :string;
  tempFieldNames_ABSys_Org_HistoryData          :string;
  tempFieldNames_ABSys_Org_BillInput            :string;
  tempFieldNames_ABSys_Approved_Message         :string;
  tempFieldNames_ABSys_Approved_Bill            :string;
  tempFieldNames_ABSys_Approved_Rule            :string;
  tempFieldNames_ABSys_Approved_ConditionParam  :string;
  tempFieldNames_ABSys_Approved_PassRuleOperator:string;
  tempFieldNames_ABSys_Approved_Step            :string;
  tempFieldNames_ABSys_Approved_StepOperator    :string;
  tempFieldNames_ABSys_Log_CurLink              :string;
  tempFieldNames_ABSys_Log_CustomEvent          :string;
  tempFieldNames_ABSys_Log_ErrorInfo            :string;
  tempFieldNames_ABSys_Log_FieldChange          :string;
  tempFieldNames_ABSys_Log_Login                :string;

  tempSQL_ABSys_Org_ConnList                    :string;
  tempSQL_ABSys_Org_Table                       :string;
  tempSQL_ABSys_Org_Field                       :string;
  tempSQL_ABSys_Org_Function                    :string;
  tempSQL_ABSys_Org_FuncTemplate                :string;
  tempSQL_ABSys_Org_FuncReport                  :string;
  tempSQL_ABSys_Org_FuncSQL                     :string;
  tempSQL_ABSys_Org_FuncControlProperty         :string;
  tempSQL_ABSys_Org_ExtendReport                :string;
  tempSQL_ABSys_Org_ExtendReportDatasets        :string;
  tempSQL_ABSys_Org_Operator                    :string;
  tempSQL_ABSys_Org_Key                         :string;
  tempSQL_ABSys_Right_OperatorKey               :string;
  tempSQL_ABSys_Right_KeyExtendReport           :string;
  tempSQL_ABSys_Right_KeyField                  :string;
  tempSQL_ABSys_Right_KeyFunction               :string;
  tempSQL_ABSys_Right_KeyTable                  :string;
  tempSQL_ABSys_Right_KeyTableRow               :string;
  tempSQL_ABSys_Right_KeyCustomObject           :string;
  tempSQL_ABSys_Org_Parameter                   :string;
  tempSQL_ABSys_Org_Tree                        :string;
  tempSQL_ABSys_Org_TreeItem                    :string;
  tempSQL_ABSys_Org_Mail                        :string;
  tempSQL_ABSys_Org_MailFile                    :string;
  tempSQL_ABSys_Org_HistoryData                 :string;
  tempSQL_ABSys_Org_BillInput                   :string;
  tempSQL_ABSys_Approved_Message                :string;
  tempSQL_ABSys_Approved_Bill                   :string;
  tempSQL_ABSys_Approved_Rule                   :string;
  tempSQL_ABSys_Approved_ConditionParam         :string;
  tempSQL_ABSys_Approved_PassRuleOperator       :string;
  tempSQL_ABSys_Approved_Step                   :string;
  tempSQL_ABSys_Approved_StepOperator           :string;
  tempSQL_ABSys_Log_CurLink                     :string;
  tempSQL_ABSys_Log_CustomEvent                 :string;
  tempSQL_ABSys_Log_ErrorInfo                   :string;
  tempSQL_ABSys_Log_FieldChange                 :string;
  tempSQL_ABSys_Log_Login                       :string;

  tempFieldNamesArray_ABSys_Org_ConnList             :TStringDynArray;
  tempFieldNamesArray_ABSys_Org_Table                :TStringDynArray;
  tempFieldNamesArray_ABSys_Org_Field                :TStringDynArray;
  tempFieldNamesArray_ABSys_Org_Function             :TStringDynArray;
  tempFieldNamesArray_ABSys_Org_FuncTemplate         :TStringDynArray;
  tempFieldNamesArray_ABSys_Org_FuncReport           :TStringDynArray;
  tempFieldNamesArray_ABSys_Org_FuncSQL              :TStringDynArray;
  tempFieldNamesArray_ABSys_Org_FuncControlProperty  :TStringDynArray;
  tempFieldNamesArray_ABSys_Org_ExtendReport         :TStringDynArray;
  tempFieldNamesArray_ABSys_Org_ExtendReportDatasets :TStringDynArray;
  tempFieldNamesArray_ABSys_Org_Operator             :TStringDynArray;
  tempFieldNamesArray_ABSys_Org_Key                  :TStringDynArray;
  tempFieldNamesArray_ABSys_Right_OperatorKey        :TStringDynArray;
  tempFieldNamesArray_ABSys_Right_KeyExtendReport    :TStringDynArray;
  tempFieldNamesArray_ABSys_Right_KeyField           :TStringDynArray;
  tempFieldNamesArray_ABSys_Right_KeyFunction        :TStringDynArray;
  tempFieldNamesArray_ABSys_Right_KeyTable           :TStringDynArray;
  tempFieldNamesArray_ABSys_Right_KeyTableRow        :TStringDynArray;
  tempFieldNamesArray_ABSys_Right_KeyCustomObject    :TStringDynArray;
  tempFieldNamesArray_ABSys_Org_Parameter            :TStringDynArray;
  tempFieldNamesArray_ABSys_Org_Tree                 :TStringDynArray;
  tempFieldNamesArray_ABSys_Org_TreeItem             :TStringDynArray;

  tempFieldNamesArray_ABSys_Org_Mail                 :TStringDynArray;
  tempFieldNamesArray_ABSys_Org_MailFile             :TStringDynArray;
  tempFieldNamesArray_ABSys_Org_HistoryData          :TStringDynArray;
  tempFieldNamesArray_ABSys_Org_BillInput            :TStringDynArray;
  tempFieldNamesArray_ABSys_Approved_Message         :TStringDynArray;
  tempFieldNamesArray_ABSys_Approved_Bill            :TStringDynArray;
  tempFieldNamesArray_ABSys_Approved_Rule            :TStringDynArray;
  tempFieldNamesArray_ABSys_Approved_ConditionParam  :TStringDynArray;
  tempFieldNamesArray_ABSys_Approved_PassRuleOperator:TStringDynArray;
  tempFieldNamesArray_ABSys_Approved_Step            :TStringDynArray;
  tempFieldNamesArray_ABSys_Approved_StepOperator    :TStringDynArray;
  tempFieldNamesArray_ABSys_Log_CurLink              :TStringDynArray;
  tempFieldNamesArray_ABSys_Log_CustomEvent          :TStringDynArray;
  tempFieldNamesArray_ABSys_Log_ErrorInfo            :TStringDynArray;
  tempFieldNamesArray_ABSys_Log_FieldChange          :TStringDynArray;
  tempFieldNamesArray_ABSys_Log_Login                :TStringDynArray;

  tempDoStringsList:TStrings;
  procedure DoItem(aStringsList:TStrings);
  var
    i,tempCount:LongInt;
    tempStrings:TStrings;
    tempSQL:string;
    tempOK:Boolean;

    tempAllFileNameStrings:Tstrings;
    tempAllSQLFileNameStrings:Tstrings;
    tempAllOtherFileNameStrings:Tstrings;

    tempStrings0,
    tempStrings1,
    tempStrings2,
    tempStrings3,
    tempStrings4,
    tempStrings5,
    tempStrings6,
    tempStrings7:TStrings;

    tempItem1,tempItem2,tempItem3:string;
    function GetCurTreeSQL(aTr_Code:string):string;
    var
      tempStrings:TStrings;
    begin
      result:=EmptyStr;
      if (tempDatasetTree.Locate('Tr_Code',aTr_Code,[])) then
      begin
        result:=ABGetSQLByDataset(tempDatasetTree,
                                 'ABSys_Org_Tree',
                                 ['Tr_Code'],
                                 ABDeleteInArrayItem(tempFieldNamesArray_ABSys_Org_Tree,['Tr_Code','Tr_Guid','PubID']),
                                 tempFieldNamesArray_ABSys_Org_Tree,
                                 nil,[]);
        if (tempDatasetTreeItem.Locate('Ti_Tr_Guid',tempDatasetTree.FieldByName('Tr_Guid').AsString,[])) then
        begin
          ABSetDatasetFilter(tempDatasetTreeItem,'Ti_Tr_Guid='+QuotedStr(tempDatasetTree.FieldByName('Tr_Guid').AsString));
          try
            if not tempDatasetTreeItem.IsEmpty then
            begin
              tempStrings:=TStringList.Create;
              try
                tempStrings.Add('Ti_Tr_Guid=Tr_Guid=(select Tr_Guid from ABSys_Org_Tree where Tr_Code=:Tr_Code)');
                ABAddstr(result,ABGetSQLByDataset(
                                       tempDatasetTreeItem,
                                       'ABSys_Org_TreeItem',
                                       ['Ti_Tr_Guid','Ti_Code'],
                                       ABDeleteInArrayItem(tempFieldNamesArray_ABSys_Org_TreeItem,['Ti_Tr_Guid','Ti_Code','Ti_ParentGuid','Ti_Guid','PubID']),
                                       tempFieldNamesArray_ABSys_Org_TreeItem,
                                       tempStrings,[tempDatasetTree],true
                                       ),ABEnterWrapStr+'go'+ABEnterWrapStr);

              finally
                tempStrings.Free;
              end;
            end;
          finally
            ABSetDatasetFilter(tempDatasetTreeItem,emptystr);
          end;
        end;
      end;
    end;
    function DoTree(var aSQL:string;aStrings:TStrings;aItemName:string):boolean;
    var
      I: Integer;
    begin
      result:=True;
      for i := 0 to aStrings.Count-1 do
      begin
        result:=DONextMainProgressBar;
        if not result then
          Break;

        ABAddstr(aSQL,GetCurTreeSQL(aStrings[i]),ABEnterWrapStr+'go'+ABEnterWrapStr);
      end;
    end;
    function DoDictionaryInfo(var aSQL:string;aStrings:TStrings;aItemName:string):boolean;
    var
      I: Integer;
    begin
      result:=True;
      ABAddstr(aSQL,GetCurTreeSQL('Table Dir'),ABEnterWrapStr+'go'+ABEnterWrapStr);

      //导出表SQL
      tempStrings1.Clear;
      tempStrings1.Add('Ta_Ti_Guid=Ti_Guid='+'( select Ti_Guid '+
                                             ' from ABSys_Org_TreeItem  '+
                                             ' where Ti_tr_Guid=(select Tr_Guid from ABSys_Org_Tree where Tr_Code=''Table Dir'') and '+
                                             '       Ti_Code=:Ti_Code)');
      tempStrings1.Add('TA_CL_Guid=CL_Guid='+'(select CL_Guid '+
                                             ' from ABSys_Org_ConnList  '+
                                             ' where CL_Name=:CL_Name)');
      tempStrings2.Clear;
      tempStrings2.Add('Fi_Ta_Guid=Ta_Guid='+'(select Ta_Guid '+
                                             ' from ABSys_Org_ConnList,ABSys_Org_Table  '+
                                             ' where CL_Guid=Ta_CL_Guid and '+
                                             '       CL_Name=:CL_Name and Ta_Name=:Ta_Name)');
      for i := 0 to tempStrings.Count-1 do
      begin
        result:=DONextMainProgressBar;
        if not result then
          Break;

        tempItem1:=aStrings.Names[i];
        tempItem2:=aStrings.ValueFromIndex[i];

        if (tempDatasetConnList.Locate('CL_Name',tempItem1,[])) then
        begin
          ABAddstr(aSQL,ABGetSQLByDataset(tempDatasetConnList,
                                            'ABSys_Org_ConnList',

                                           ['CL_Name'],
                                           ABDeleteInArrayItem(tempFieldNamesArray_ABSys_Org_ConnList,['CL_Name','CL_Guid','PubID']),
                                           tempFieldNamesArray_ABSys_Org_ConnList,
                                           nil,[]
                                             ),ABEnterWrapStr+'go'+ABEnterWrapStr);

          if (tempDatasetTable.Locate('Ta_CL_Guid;Ta_Name',VarArrayOf([tempDatasetConnList.FieldByName('CL_Guid').AsString,tempItem2]),[])) then
          begin
            ABAddstr(aSQL,ABGetSQLByDataset(tempDatasetTable,
                                              'ABSys_Org_Table',

                                             ['Ta_Ti_Guid','TA_CL_Guid','Ta_Name'],
                                             ABDeleteInArrayItem(tempFieldNamesArray_ABSys_Org_Table,['Ta_Ti_Guid','TA_CL_Guid','Ta_Name','Ta_Guid','Ta_ID','PubID']),
                                             tempFieldNamesArray_ABSys_Org_Table,
                                             tempStrings1,[tempDatasetTreeItem,tempDatasetConnList]
                                               ),ABEnterWrapStr+'go'+ABEnterWrapStr);

            if (tempDatasetField.Locate('Fi_Ta_Guid',tempDatasetTable.FieldByName('Ta_Guid').AsString,[])) then
            begin
              ABSetDatasetFilter(tempDatasetField,'Fi_Ta_Guid='+QuotedStr(tempDatasetTable.FieldByName('Ta_Guid').AsString));
              try
                ABAddstr(aSQL,ABGetSQLByDataset(tempDatasetField,
                                                  'ABSys_Org_Field',
                                     ['Fi_Ta_Guid','Fi_Name'],
                                     ABDeleteInArrayItem(tempFieldNamesArray_ABSys_Org_Field,['Fi_Ta_Guid','Fi_Name','Fi_Guid','Fi_Order','FubID']),
                                     tempFieldNamesArray_ABSys_Org_Field,
                                     tempStrings2,[tempDatasetConnList,tempDatasetTable],true
                                           ),ABEnterWrapStr+'go'+ABEnterWrapStr);
              finally
                ABSetDatasetFilter(tempDatasetField,emptystr);
              end;
            end;
          end;
        end;
      end;
    end;
    function DoParameter(var aSQL:string;aStrings:TStrings;aItemName:string):boolean;
    var
      tempFileName:string;
      I: Integer;
    begin
      result:=True;
      for i := 0 to aStrings.Count-1 do
      begin
        result:=DONextMainProgressBar;
        if not result then
          Break;

        if (tempDatasetParameter.Locate('Pa_Name',aStrings[i],[])) then
        begin
          ABAddstr(aSQL,ABGetSQLByDataset(tempDatasetParameter,
                   'ABSys_Org_Parameter',
                   ['Pa_Name'],
                   ABDeleteInArrayItem(tempFieldNamesArray_ABSys_Org_Parameter,['Pa_Name','Pa_Guid','PubID']),
                   tempFieldNamesArray_ABSys_Org_Parameter,
                   nil,[]
                   ),ABEnterWrapStr+'go'+ABEnterWrapStr);
          tempFileName:=aItemName+'='+aStrings[i];
          if ABDownToFileFromField('Main','ABSys_Org_Parameter','Pa_ExpandValue',
                                   'where Pa_Name='+ABQuotedStr(aStrings[i]),
                                   ABTempPath+ tempFileName) then
          begin
            tempAllFileNameStrings.Add(ABTempPath+ tempFileName);
            ABWriteIniInStrings(aItemName,aStrings[i],tempFileName,tempAllOtherFileNameStrings);
          end;
        end;
      end;
    end;
    function DoMail(var aSQL:string;aStrings:TStrings;aItemName:string):boolean;
    var
      tempFileName:string;
    begin
      result:=True;
      if AnsiCompareText(aStrings[0],'ABSys_Org_Mail')=0 then
      begin
        result:=DONextMainProgressBar;
        if result then
        begin
          ProgressBar2.MIN:=0;
          ProgressBar2.Max:=tempDatasetMail.RecordCount;
          ProgressBar2.visible:=True;
          tempDatasetMail.First;
          try
            while not tempDatasetMail.Eof do
            begin
              result:=DONextDetailProgressBar;
              if not result then
                Break;

              ABAddstr(aSQL,ABGetSQLByDataset(tempDatasetMail,
                       'ABSys_Org_Mail',
                       ['MA_Guid'],
                       ABDeleteInArrayItem(tempFieldNamesArray_ABSys_Org_Mail,['MA_Content','MA_Guid','FubID']),
                       tempFieldNamesArray_ABSys_Org_Mail,
                       nil,[]
                       ),ABEnterWrapStr+'go'+ABEnterWrapStr);

              tempFileName:=aItemName+'='+tempDatasetMail.FieldByName('MA_Guid').AsString;
              if ABDownToFileFromField('Main','ABSys_Org_Mail','MA_Content',
                                       'where MA_Guid='+ABQuotedStr(tempDatasetMail.FieldByName('MA_Guid').AsString),
                                        ABTempPath+tempFileName) then
              begin
                tempAllFileNameStrings.Add( ABTempPath+tempFileName);
                ABWriteIniInStrings(aItemName,tempDatasetMail.FieldByName('MA_Guid').AsString,tempFileName,tempAllOtherFileNameStrings);
              end;
              if (tempDatasetMailFile.Locate('MF_MA_Guid',tempDatasetMail.FieldByName('MA_Guid').AsString,[])) then
              begin
                ABSetDatasetFilter(tempDatasetMailFile,'MF_MA_Guid='+QuotedStr(tempDatasetMail.FieldByName('MA_Guid').AsString));
                try
                  tempDatasetMailFile.First;
                  while not tempDatasetMailFile.Eof do
                  begin
                    ABAddstr(aSQL,ABGetSQLByDataset(tempDatasetMailFile,
                                                      'ABSys_Org_MailFile',
                                         ['MF_MA_Guid','MF_Guid'],
                                         ABDeleteInArrayItem(tempFieldNamesArray_ABSys_Org_MailFile,['MF_File','MF_MA_Guid','MF_Guid','FubID']),
                                         tempFieldNamesArray_ABSys_Org_MailFile,
                                         nil,[]
                                               ),ABEnterWrapStr+'go'+ABEnterWrapStr);
                    tempFileName:= aItemName+'=MF_File='+tempDatasetMail.FieldByName('MA_Guid').AsString+'='+tempDatasetMailFile.FieldByName('MF_Guid').AsString;
                    if ABDownToFileFromField('Main','ABSys_Org_MailFile','MF_File',
                                             'where MF_Guid='+ABQuotedStr(tempDatasetMailFile.FieldByName('MF_Guid').AsString),
                                             ABTempPath+ tempFileName) then
                    begin
                      tempAllFileNameStrings.Add(ABTempPath+ tempFileName);
                      ABWriteIniInStrings(aItemName+'=MF_File',tempDatasetMailFile.FieldByName('MF_Guid').AsString,tempFileName,tempAllOtherFileNameStrings);
                    end;
                    tempDatasetMailFile.Next;
                  end;
                finally
                  ABSetDatasetFilter(tempDatasetMailFile,emptystr);
                end;
              end;
              tempDatasetMail.Next;
            end;
          finally
            ProgressBar2.Position:=0;
            ProgressBar2.visible:=False;
          end;
        end;
      end;
    end;
    function DoHistoryData(var aSQL:string;aStrings:TStrings;aItemName:string):boolean;
    var
      I: Integer;
    begin
      result:=True;

      //导出表SQL
      tempStrings1.Clear;
      tempStrings1.Add('HD_Ta_Guid=Ta_Guid='+'(select Ta_Guid '+
                                             ' from ABSys_Org_Table left join  ABSys_Org_ConnList  on  CL_Guid=Ta_CL_Guid '+
                                             ' where  CL_Name=:CL_Name and Ta_Name=:Ta_Name)');
      tempStrings1.Add('HD_Fi_Guid=Fi_Guid='+'(select Fi_Guid '+
                                             ' from ABSys_Org_ConnList,ABSys_Org_Table,ABSys_Org_Field  '+
                                             ' where CL_Guid=Ta_CL_Guid and Fi_Ta_Guid=Ta_Guid and '+
                                             '       CL_Name=:CL_Name and Ta_Name=:Ta_Name and Fi_Name=:Fi_Name )');


      for i := 0 to tempStrings.Count-1 do
      begin
        result:=DONextMainProgressBar;
        if not result then
          Break;

        tempItem1:=aStrings.Names[i];
        tempItem2:=ABGetLeftRightStr(aStrings.ValueFromIndex[i]);
        tempItem3:=ABGetLeftRightStr(aStrings.ValueFromIndex[i],axdRight);

        if (tempDatasetConnList.Locate('CL_Name',tempItem1,[])) then
        begin
          if (tempDatasetTable.Locate('Ta_CL_Guid;Ta_Name',VarArrayOf([tempDatasetConnList.FieldByName('CL_Guid').AsString,tempItem2]),[])) then
          begin
            if (tempDatasetField.Locate('Fi_Ta_Guid;Fi_Name',VarArrayOf([tempDatasetTable.FieldByName('Ta_Guid').AsString,tempItem3]),[])) then
            begin
              if (tempDatasetHistoryData.Locate('HD_Ta_Guid;HD_Fi_Guid',VarArrayOf([tempDatasetTable.FieldByName('Ta_Guid').AsString,tempDatasetField.FieldByName('Fi_Guid').AsString]),[])) then
              begin
                ABAddstr(aSQL,ABGetSQLByDataset(tempDatasetHistoryData,
                                                  'ABSys_Org_HistoryData',
                                                 ['HD_Ta_Guid','HD_Fi_Guid'],
                                                 ABDeleteInArrayItem(tempFieldNamesArray_ABSys_Org_HistoryData,['HD_Ta_Guid','HD_Fi_Guid','HD_Guid','PubID']),
                                                 tempFieldNamesArray_ABSys_Org_HistoryData,
                                                 tempStrings1,[tempDatasetConnList,tempDatasetTable,tempDatasetField]
                                                   ),ABEnterWrapStr+'go'+ABEnterWrapStr);
              end;
            end;
          end;
        end;
      end;
    end;
    function DoBillInput(var aSQL:string;aStrings:TStrings;aItemName:string):boolean;
    var
      I: Integer;
    begin
      result:=True;

      //导出表SQL
      for i := 0 to tempStrings.Count-1 do
      begin
        result:=DONextMainProgressBar;
        if not result then
          Break;

        if (tempDatasetBillInput.Locate('BI_Code',aStrings[i],[])) then
        begin
          ABAddstr(aSQL,ABGetSQLByDataset(tempDatasetBillInput,
                   'ABSys_Org_BillInput',
                   ['BI_Code'],
                   ABDeleteInArrayItem(tempFieldNamesArray_ABSys_Org_BillInput,['BI_Code','BI_Guid','PubID']),
                   tempFieldNamesArray_ABSys_Org_BillInput,
                   nil,[]
                   ),ABEnterWrapStr+'go'+ABEnterWrapStr);
        end;
      end;
    end;

    function DoFunc(var aSQL:string;aStrings:TStrings;aItemName:string):boolean;
    var
      I: Integer;
      tempFileName:string;
    begin
      result:=True;
      ABAddstr(aSQL,GetCurTreeSQL('Function Dir'),ABEnterWrapStr+'go'+ABEnterWrapStr);
      //导出功能SQL
      tempStrings1.Clear;
      tempStrings1.Add('FU_Ti_Guid=Ti_Guid='+'(select Ti_Guid '+
                                             ' from ABSys_Org_TreeItem  '+
                                             ' where Ti_tr_Guid=(select Tr_Guid from ABSys_Org_Tree where Tr_Code=''Function Dir'') and '+
                                             '       Ti_Code=:Ti_Code)');
      tempStrings2.Clear;
      tempStrings2.Add('Ft_Fu_Guid=Fu_Guid='+'(select Fu_Guid '+
                                             ' from ABSys_Org_Function  '+
                                             ' where Fu_FileName=:Fu_FileName)');
      tempStrings3.Clear;
      tempStrings3.Add('FR_Fu_Guid=Fu_Guid='+'(select Fu_Guid '+
                                             ' from ABSys_Org_Function  '+
                                             ' where Fu_FileName=:Fu_FileName)');
      tempStrings4.Clear;
      tempStrings4.Add('FP_FU_Guid=Fu_Guid='+'(select Fu_Guid '+
                                             ' from ABSys_Org_Function  '+
                                             ' where Fu_FileName=:Fu_FileName)');
      tempStrings5.Clear;
      tempStrings5.Add('FS_FU_Guid=Fu_Guid='+'(select Fu_Guid '+
                                             ' from ABSys_Org_Function  '+
                                             ' where Fu_FileName=:Fu_FileName)');
      for i := 0 to aStrings.Count-1 do
      begin
        result:=DONextMainProgressBar;
        if not result then
          Break;

        if (tempDatasetFunction.Locate('Fu_FileName',aStrings[i],[])) then
        begin
          ABAddstr(aSQL,ABGetSQLByDataset(tempDatasetFunction,
                       'ABSys_Org_Function',
                       ['Fu_Ti_Guid','Fu_Code'],
                       ABDeleteInArrayItem(tempFieldNamesArray_ABSys_Org_Function,['Fu_Ti_Guid','Fu_Code','Fu_Guid','PubID']),
                       tempFieldNamesArray_ABSys_Org_Function,
                               tempStrings1,[tempDatasetTreeItem]
                                     ),ABEnterWrapStr+'go'+ABEnterWrapStr);

          tempFileName:= aItemName+'=FU_Pkg='+aStrings[i];
          if ABDownToFileFromField('Main','ABSys_Org_Function','FU_Pkg',
                                   'where Fu_FileName='+ABQuotedStr(aStrings[i]),
                                   ABTempPath+ tempFileName) then
          begin
            tempAllFileNameStrings.Add(ABTempPath+ tempFileName);
            ABWriteIniInStrings(aItemName+'=FU_Pkg',aStrings[i],tempFileName,tempAllOtherFileNameStrings);
          end;

          tempFileName:= aItemName+'=FU_Bitmap='+aStrings[i];
          if ABDownToFileFromField('Main','ABSys_Org_Function','FU_Bitmap',
                                   'where Fu_FileName='+ABQuotedStr(aStrings[i]),
                                   ABTempPath+ tempFileName) then
          begin
            tempAllFileNameStrings.Add(ABTempPath+ tempFileName);
            ABWriteIniInStrings(aItemName+'=FU_Bitmap',aStrings[i],tempFileName,tempAllOtherFileNameStrings);
          end;

          if (tempDatasetFuncTemplate.Locate('Ft_Fu_Guid',tempDatasetFunction.FieldByName('Fu_Guid').AsString,[])) then
          begin
            ABSetDatasetFilter(tempDatasetFuncTemplate,'Ft_Fu_Guid='+QuotedStr(tempDatasetFunction.FieldByName('Fu_Guid').AsString));
            try
              ABAddstr(aSQL,ABGetSQLByDataset(tempDatasetFuncTemplate,
                                                 'ABSys_Org_FuncTemplate',
                                                 ['Ft_Fu_Guid','Ft_GroupName','Ft_Name'],
                                                 ABDeleteInArrayItem(tempFieldNamesArray_ABSys_Org_FuncTemplate,['Ft_Fu_Guid','Ft_GroupName','Ft_Name','Ft_Guid','PubID']),
                                                 tempFieldNamesArray_ABSys_Org_FuncTemplate,
                                   tempStrings2,[tempDatasetFunction],true
                                         ),ABEnterWrapStr+'go'+ABEnterWrapStr);

              tempFileName:= aItemName+'=ABSys_Org_FuncTemplate='+aStrings[i]+'='+
                                   tempDatasetFuncTemplate.FieldByName('Ft_GroupName').AsString+'='+
                                   tempDatasetFuncTemplate.FieldByName('Ft_Name').AsString;
              if ABWriteTxt(ABTempPath+ tempFileName, ABDownToTextFromField('Main','ABSys_Org_FuncTemplate','Ft_FieldNameValue',
                                       'where Ft_Fu_Guid='+ABQuotedStr(tempDatasetFunction.FieldByName('Fu_Guid').AsString)+' and '+
                                       '      Ft_GroupName='+ABQuotedStr(tempDatasetFuncTemplate.FieldByName('Ft_GroupName').AsString)+' and '+
                                       '      Ft_Name='+ABQuotedStr(tempDatasetFuncTemplate.FieldByName('Ft_Name').AsString)
                                       )) then
              begin
                tempAllFileNameStrings.Add(ABTempPath+ tempFileName);
                ABWriteIniInStrings(aItemName+'=ABSys_Org_FuncTemplate',
                                   aStrings[i]+'='+
                                   tempDatasetFuncTemplate.FieldByName('Ft_GroupName').AsString+'='+
                                   tempDatasetFuncTemplate.FieldByName('Ft_Name').AsString,
                                   tempFileName,tempAllOtherFileNameStrings);
              end;
            finally
              ABSetDatasetFilter(tempDatasetFuncTemplate,emptystr);
            end;
          end;
          if (tempDatasetFuncReport.Locate('FR_FU_Guid',tempDatasetFunction.FieldByName('Fu_Guid').AsString,[])) then
          begin
            ABSetDatasetFilter(tempDatasetFuncReport,'FR_FU_Guid='+QuotedStr(tempDatasetFunction.FieldByName('Fu_Guid').AsString));
            try
              ABAddstr(aSQL,ABGetSQLByDataset(tempDatasetFuncReport,
                                                 'ABSys_Org_FuncReport',
                                                 ['Fr_Fu_Guid','Fr_GroupName','Fr_Name'],
                                                 ABDeleteInArrayItem(tempFieldNamesArray_ABSys_Org_FuncReport,['Fr_Fu_Guid','Fr_GroupName','Fr_Name','Fr_Guid','PubID']),
                                                 tempFieldNamesArray_ABSys_Org_FuncReport,
                                                 tempStrings3,[tempDatasetFunction],true
                                                 ),ABEnterWrapStr+'go'+ABEnterWrapStr);

              tempFileName:= aItemName+'=ABSys_Org_FuncReport='+aStrings[i]+
                                   tempDatasetFuncReport.FieldByName('Fr_GroupName').AsString+'='+
                                   tempDatasetFuncReport.FieldByName('Fr_Name').AsString;
              if ABDownToFileFromField('Main','ABSys_Org_FuncReport','FR_Format',
                                       'where Fr_Fu_Guid='+ABQuotedStr(tempDatasetFunction.FieldByName('Fu_Guid').AsString)+' and '+
                                       '      Fr_GroupName='+ABQuotedStr(tempDatasetFuncReport.FieldByName('Fr_GroupName').AsString)+' and '+
                                       '      Fr_Name='+ABQuotedStr(tempDatasetFuncReport.FieldByName('Fr_Name').AsString),
                                       ABTempPath+ tempFileName) then
              begin
                tempAllFileNameStrings.Add(ABTempPath+ tempFileName);
                ABWriteIniInStrings(aItemName+'=ABSys_Org_FuncReport',
                                   aStrings[i]+'='+
                                   tempDatasetFuncReport.FieldByName('Fr_GroupName').AsString+'='+
                                   tempDatasetFuncReport.FieldByName('Fr_Name').AsString,
                                   tempFileName,tempAllOtherFileNameStrings);
              end;
            finally
              ABSetDatasetFilter(tempDatasetFuncReport,emptystr);
            end;
          end;
          if (tempDatasetFuncControlProperty.Locate('FP_FU_Guid',tempDatasetFunction.FieldByName('Fu_Guid').AsString,[])) then
          begin
            ABSetDatasetFilter(tempDatasetFuncControlProperty,'FP_FU_Guid='+QuotedStr(tempDatasetFunction.FieldByName('Fu_Guid').AsString));
            try
              ABAddstr(aSQL,ABGetSQLByDataset(tempDatasetFuncControlProperty,
                                                 'ABSys_Org_FuncControlProperty',
                                                 ['FP_FU_Guid','FP_FormName','FP_ControlName','FP_PropertyName'],
                                                 ABDeleteInArrayItem(tempFieldNamesArray_ABSys_Org_FuncControlProperty,['FP_FU_Guid','FP_FormName','FP_ControlName','FP_PropertyName','FP_Guid','PubID']),
                                                 tempFieldNamesArray_ABSys_Org_FuncControlProperty,
                                   tempStrings4,[tempDatasetFunction],true
                                         ),ABEnterWrapStr+'go'+ABEnterWrapStr);
            finally
              ABSetDatasetFilter(tempDatasetFuncControlProperty,emptystr);
            end;
          end;

          if (tempDatasetFuncSQL.Locate('FS_FU_Guid',tempDatasetFunction.FieldByName('Fu_Guid').AsString,[])) then
          begin
            ABSetDatasetFilter(tempDatasetFuncSQL,'FS_FU_Guid='+QuotedStr(tempDatasetFunction.FieldByName('Fu_Guid').AsString));
            try
              ABAddstr(aSQL,ABGetSQLByDataset(tempDatasetFuncSQL,
                                                 'ABSys_Org_FuncSQL',
                                                 ['FS_FU_Guid','FS_FormName','FS_Name'],
                                                 ABDeleteInArrayItem(tempFieldNamesArray_ABSys_Org_FuncSQL,['FS_FU_Guid','FS_FormName','FS_Name','FS_Guid','PubID']),
                                                 tempFieldNamesArray_ABSys_Org_FuncSQL,
                                                 tempStrings5,[tempDatasetFunction],true
                                                       ),ABEnterWrapStr+'go'+ABEnterWrapStr);

              tempFileName:= aItemName+'=ABSys_Org_FuncSQL='+aStrings[i]+
                                   tempDatasetFuncSQL.FieldByName('FS_FormName').AsString+'='+
                                   tempDatasetFuncSQL.FieldByName('FS_Name').AsString;
              if ABWriteTxt(ABTempPath+ tempFileName, ABDownToTextFromField('Main','ABSys_Org_FuncSQL','FS_Sql',
                                       'where FS_FU_Guid='+ABQuotedStr(tempDatasetFunction.FieldByName('Fu_Guid').AsString)+' and '+
                                       '      FS_FormName='+ABQuotedStr(tempDatasetFuncSQL.FieldByName('FS_FormName').AsString)+' and '+
                                       '      FS_Name='+ABQuotedStr(tempDatasetFuncSQL.FieldByName('FS_Name').AsString))
                                  ) then
              begin
                tempAllFileNameStrings.Add(ABTempPath+ tempFileName);
                ABWriteIniInStrings(aItemName+'=ABSys_Org_FuncSQL',
                                    aStrings[i]+'='+
                                   tempDatasetFuncSQL.FieldByName('FS_FormName').AsString+'='+
                                   tempDatasetFuncSQL.FieldByName('FS_Name').AsString,
                                    tempFileName,tempAllOtherFileNameStrings);
              end;
            finally
              ABSetDatasetFilter(tempDatasetFuncSQL,emptystr);
            end;
          end;
        end;
      end;
    end;
    function DoKey(var aSQL:string;aStrings:TStrings;aItemName:string):boolean;
    var
      I: Integer;
    begin
      result:=True;
      ABAddstr(aSQL,GetCurTreeSQL('CustomObject'),ABEnterWrapStr+'go'+ABEnterWrapStr);
      ABAddstr(aSQL,GetCurTreeSQL('CustomObjectType'),ABEnterWrapStr+'go'+ABEnterWrapStr);
      tempStrings1.Clear;
      tempStrings1.Add('KF_KE_Guid=KE_Guid='+'(select KE_Guid '+
                                             ' from ABSys_Org_Key  '+
                                             ' where KE_Code=:KE_Code)');
      tempStrings1.Add('KF_FU_Guid=FU_Guid='+'(select FU_Guid '+
                                             ' from ABSys_Org_Function  '+
                                             ' where Fu_FileName=:Fu_FileName)');
      tempStrings2.Clear;
      tempStrings2.Add('KR_KE_Guid=KE_Guid='+'(select KE_Guid '+
                                             ' from ABSys_Org_Key  '+
                                             ' where KE_Code=:KE_Code)');
      tempStrings2.Add('KR_ER_Guid=ER_Guid='+'(select ER_Guid '+
                                             ' from ABSys_Org_ExtendReport  '+
                                             ' where ER_Name=:ER_Name)');

      tempStrings3.Clear;
      tempStrings3.Add('KT_KE_Guid=KE_Guid='+'(select KE_Guid '+
                                             ' from ABSys_Org_Key  '+
                                             ' where KE_Code=:KE_Code)');
      tempStrings3.Add('KT_Ta_Guid=Ta_Guid='+'(select Ta_Guid '+
                                             ' from ABSys_Org_ConnList,ABSys_Org_Table  '+
                                             ' where CL_Guid=Ta_CL_Guid and '+
                                             '       CL_Name=:CL_Name and Ta_Name=:Ta_Name)');
      tempStrings4.Clear;
      tempStrings4.Add('KF_KE_Guid=KE_Guid='+'(select KE_Guid '+
                                             ' from ABSys_Org_Key  '+
                                             ' where KE_Code=:KE_Code)');
      tempStrings4.Add('KF_Ta_Guid=Ta_Guid='+'(select Ta_Guid '+
                                             ' from ABSys_Org_ConnList,ABSys_Org_Table  '+
                                             ' where CL_Guid=Ta_CL_Guid and '+
                                             '       CL_Name=:CL_Name and Ta_Name=:Ta_Name)');
      tempStrings4.Add('KF_FI_Guid=Fi_Guid='+'(select Fi_Guid '+
                                             ' from ABSys_Org_ConnList,ABSys_Org_Table,ABSys_Org_Field  '+
                                             ' where CL_Guid=Ta_CL_Guid and Fi_Ta_Guid=Ta_Guid and '+
                                             '       CL_Name=:CL_Name and Ta_Name=:Ta_Name and Fi_Name=:Fi_Name )');
      tempStrings5.Clear;
      tempStrings5.Add('KC_KE_Guid=KE_Guid='+'(select KE_Guid '+
                                             ' from ABSys_Org_Key  '+
                                             ' where KE_Code=:KE_Code)');
      tempStrings5.Add('KC_TI_Guid=Ti_Guid='+'( select Ti_Guid '+
                                             ' from ABSys_Org_TreeItem  '+
                                             ' where Ti_tr_Guid=(select Tr_Guid from ABSys_Org_Tree where Tr_Code=''CustomObject'') and '+
                                             '       Ti_Code=:Ti_Code)');
      tempStrings6.Clear;
      tempStrings6.Add('KR_KT_Guid=KT_Guid='+'(select KT_Guid '+
                                             ' from ABSys_Right_KeyTable  '+
                                             ' where KT_KE_Guid=(select KE_Guid from ABSys_Org_Key where KE_Code=:KE_Code) and '+
                                             '       KT_Ta_Guid=(select Ta_Guid from ABSys_Org_ConnList,ABSys_Org_Table where CL_Guid=Ta_CL_Guid and CL_Name=:CL_Name and Ta_Name=:Ta_Name)   '+
                                             ')');

      for i := 0 to aStrings.Count-1 do
      begin
        result:=DONextMainProgressBar;
        if not result then
          Break;

        if (tempDatasetKey.Locate('Ke_Code',aStrings[i],[])) then
        begin
          ABAddstr(aSQL,ABGetSQLByDataset(tempDatasetKey,
                   'ABSys_Org_Key',
                   ['Ke_Code'],
                   ABDeleteInArrayItem(tempFieldNamesArray_ABSys_Org_Key,['Ke_Code','Ke_Guid','PubID']),
                   tempFieldNamesArray_ABSys_Org_Key,
                   nil,[]
                   ),ABEnterWrapStr+'go'+ABEnterWrapStr);

          if (tempDatasetKeyFunction.Locate('KF_Ke_Guid',tempDatasetKey.FieldByName('Ke_Guid').AsString,[])) then
          begin
            ABSetDatasetFilter(tempDatasetKeyFunction,'KF_Ke_Guid='+QuotedStr(tempDatasetKey.FieldByName('Ke_Guid').AsString));
            try
              ABAddstr(aSQL,ABGetSQLByDataset(tempDatasetKeyFunction,
                                                 'ABSys_Right_KeyFunction',
                                                 ['KF_KE_Guid','KF_FU_Guid'],
                                                 ABDeleteInArrayItem(tempFieldNamesArray_ABSys_Right_KeyFunction,['KF_KE_Guid','KF_FU_Guid','KF_Guid','FubID']),
                                                 tempFieldNamesArray_ABSys_Right_KeyFunction,
                                                 tempStrings1,[tempDatasetFunction,tempDatasetKey],true
                                                       ),ABEnterWrapStr+'go'+ABEnterWrapStr);
            finally
              ABSetDatasetFilter(tempDatasetKeyFunction,emptystr);
            end;
          end;

          if (tempDatasetKeyExtendReport.Locate('KR_KE_Guid',tempDatasetKey.FieldByName('Ke_Guid').AsString,[])) then
          begin
            ABSetDatasetFilter(tempDatasetKeyExtendReport,'KR_KE_Guid='+QuotedStr(tempDatasetKey.FieldByName('Ke_Guid').AsString));
            try
              ABAddstr(aSQL,ABGetSQLByDataset(tempDatasetKeyExtendReport,
                                                 'ABSys_Right_KeyExtendReport',
                                                 ['KR_KE_Guid','KR_ER_Guid'],
                                                 ABDeleteInArrayItem(tempFieldNamesArray_ABSys_Right_KeyExtendReport,['KR_KE_Guid','KR_ER_Guid','KR_Guid','FubID']),
                                                 tempFieldNamesArray_ABSys_Right_KeyExtendReport,
                                                 tempStrings2,[tempDatasetExtendReport,tempDatasetKey],true
                                                       ),ABEnterWrapStr+'go'+ABEnterWrapStr);
            finally
              ABSetDatasetFilter(tempDatasetKeyExtendReport,emptystr);
            end;
          end;

          if (tempDatasetKeyTable.Locate('KT_KE_Guid',tempDatasetKey.FieldByName('Ke_Guid').AsString,[])) then
          begin
            ABSetDatasetFilter(tempDatasetKeyTable,'KT_KE_Guid='+QuotedStr(tempDatasetKey.FieldByName('Ke_Guid').AsString));
            try
              tempDatasetKeyTable.First;
              while not tempDatasetKeyTable.Eof do
              begin
                if (tempDatasetTable.Locate('Ta_Guid',tempDatasetKeyTable.FieldByName('KT_TA_Guid').AsString,[])) and
                   (tempDatasetConnList.Locate('CL_Guid',tempDatasetTable.FieldByName('Ta_CL_Guid').AsString,[]))
                     then
                begin
                  ABAddstr(aSQL,ABGetSQLByDataset(tempDatasetKeyTable,
                                                     'ABSys_Right_KeyTable',
                                                     ['KT_KE_Guid','KT_TA_Guid'],
                                                     ABDeleteInArrayItem(tempFieldNamesArray_ABSys_Right_KeyTable,['KT_KE_Guid','KT_TA_Guid','KT_Guid','FubID']),
                                                     tempFieldNamesArray_ABSys_Right_KeyTable,
                                                     tempStrings3,[tempDatasetConnList,tempDatasetTable,tempDatasetKey]
                                                           ),ABEnterWrapStr+'go'+ABEnterWrapStr);
                  if (tempDatasetKeyTableRow.Locate('KR_KT_Guid',tempDatasetKeyTable.FieldByName('KT_Guid').AsString,[])) then
                  begin
                    ABSetDatasetFilter(tempDatasetKeyTableRow,'KR_KT_Guid='+QuotedStr(tempDatasetKeyTable.FieldByName('KT_Guid').AsString));
                    try
                      ABAddstr(aSQL,ABGetSQLByDataset(tempDatasetKeyTableRow,
                                                         'ABSys_Right_KeyTableRow',
                                                         ['KR_KT_Guid','KR_TA_KeyFieldValue'],
                                                         ABDeleteInArrayItem(tempFieldNamesArray_ABSys_Right_KeyTableRow,['KR_KT_Guid','KR_TA_KeyFieldValue','KR_Guid','FubID']),
                                                         tempFieldNamesArray_ABSys_Right_KeyTableRow,
                                                         tempStrings6,[tempDatasetConnList,tempDatasetTable,tempDatasetKey,tempDatasetKeyTable],true
                                                               ),ABEnterWrapStr+'go'+ABEnterWrapStr);
                    finally
                      ABSetDatasetFilter(tempDatasetKeyTableRow,emptystr);
                    end;
                  end;
                end;

                tempDatasetKeyTable.Next;
              end;
            finally
              ABSetDatasetFilter(tempDatasetKeyTable,emptystr);
            end;
          end;

          if (tempDatasetKeyField.Locate('KF_KE_Guid',tempDatasetKey.FieldByName('Ke_Guid').AsString,[])) then
          begin
            ABSetDatasetFilter(tempDatasetKeyField,'KF_KE_Guid='+QuotedStr(tempDatasetKey.FieldByName('Ke_Guid').AsString));
            try
              tempDatasetKeyField.First;
              while not tempDatasetKeyField.Eof do
              begin
                if (tempDatasetField.Locate('FI_Guid',tempDatasetKeyField.FieldByName('KF_FI_Guid').AsString,[])) and
                   (tempDatasetTable.Locate('Ta_Guid',tempDatasetField.FieldByName('FI_Ta_Guid').AsString,[])) and
                   (tempDatasetConnList.Locate('CL_Guid',tempDatasetTable.FieldByName('Ta_CL_Guid').AsString,[]))
                     then
                begin
                  ABAddstr(aSQL,ABGetSQLByDataset(tempDatasetKeyField,
                                                     'ABSys_Right_KeyField',
                                                     ['KF_KE_Guid','KF_Ta_Guid','KF_FI_Guid'],
                                                     ABDeleteInArrayItem(tempFieldNamesArray_ABSys_Right_KeyField,['KF_KE_Guid','KF_Ta_Guid','KF_FI_Guid','KF_Guid','FubID']),
                                                     tempFieldNamesArray_ABSys_Right_KeyField,
                                                     tempStrings4,[tempDatasetConnList,tempDatasetTable,tempDatasetField,tempDatasetKey],true
                                                           ),ABEnterWrapStr+'go'+ABEnterWrapStr);
                end;

                tempDatasetKeyField.Next;
              end;
            finally
              ABSetDatasetFilter(tempDatasetKeyField,emptystr);
            end;
          end;

          if (tempDatasetKeyCustomObject.Locate('KC_KE_Guid',tempDatasetKey.FieldByName('Ke_Guid').AsString,[])) then
          begin
            ABSetDatasetFilter(tempDatasetKeyCustomObject,'KC_KE_Guid='+QuotedStr(tempDatasetKey.FieldByName('Ke_Guid').AsString));
            try
              ABAddstr(aSQL,ABGetSQLByDataset(tempDatasetKeyCustomObject,
                                                 'ABSys_Right_KeyCustomObject',
                                                 ['KC_KE_Guid','KC_TI_Guid'],
                                                 ABDeleteInArrayItem(tempFieldNamesArray_ABSys_Right_KeyCustomObject,['KC_KE_Guid','KC_TI_Guid','KC_Guid','FubID']),
                                                 tempFieldNamesArray_ABSys_Right_KeyCustomObject,
                                                 tempStrings5,[tempDatasetTreeItem,tempDatasetKey],true
                                                       ),ABEnterWrapStr+'go'+ABEnterWrapStr);
            finally
              ABSetDatasetFilter(tempDatasetKeyCustomObject,emptystr);
            end;
          end;
        end;
      end;
    end;
    function DoOperator(var aSQL:string;aStrings:TStrings;aItemName:string):boolean;
      function DoItem:boolean;
      var
        I: Integer;
      begin
        result:=False;
        for i := 0 to aStrings.Count-1 do
        begin
          result:=DONextMainProgressBar;
          if not result then
            Break;

          if (tempDatasetOperator.Locate('OP_Code',aStrings[i],[])) then
          begin
            ABAddstr(aSQL,ABGetSQLByDataset(tempDatasetOperator,
                         'ABSys_Org_Operator',
                                 ['OP_Ti_Guid','OP_Code'],
                                 ABDeleteInArrayItem(tempFieldNamesArray_ABSys_Org_Operator,['OP_Ti_Guid','OP_Code','OP_Guid','PubID']),
                                 tempFieldNamesArray_ABSys_Org_Operator,
                                 tempStrings1,[tempDatasetTreeItem]
                                       ),ABEnterWrapStr+'go'+ABEnterWrapStr);
            if (tempDatasetOperatorKey.Locate('OK_OP_Guid',tempDatasetOperator.FieldByName('OP_Guid').AsString,[])) then
            begin
              ABSetDatasetFilter(tempDatasetOperatorKey,'OK_OP_Guid='+QuotedStr(tempDatasetOperator.FieldByName('OP_Guid').AsString));
              try
                ABAddstr(aSQL,ABGetSQLByDataset(tempDatasetOperatorKey,
                                                  'ABSys_Right_OperatorKey',
                                     ['OK_OP_Guid','OK_KE_Guid'],
                                     ABDeleteInArrayItem(tempFieldNamesArray_ABSys_Right_OperatorKey,['OK_OP_Guid','OK_KE_Guid','OK_Guid','FubID']),
                                     tempFieldNamesArray_ABSys_Right_OperatorKey,
                                     tempStrings2,[tempDatasetOperator,tempDatasetKey],true
                                           ),ABEnterWrapStr+'go'+ABEnterWrapStr);
              finally
                ABSetDatasetFilter(tempDatasetOperatorKey,emptystr);
              end;
            end;
          end;
        end;
      end;
    var
      tempStringDoOperator:TStrings;
    begin
      result:=True;
      tempStringDoOperator := TStringList.Create;
      try
        //导出操作员SQL
        tempStrings1.Clear;
        tempStrings1.Add('OP_Ti_Guid=Ti_Guid='+'(select Ti_Guid '+
                                               ' from ABSys_Org_TreeItem  '+
                                               ' where Ti_tr_Guid=(select Tr_Guid from ABSys_Org_Tree where Tr_Code=''Enterprise Dir'') and '+
                                               '       Ti_Code=:Ti_Code)');
        tempStrings2.Clear;
        tempStrings2.Add('OK_OP_Guid=OP_Guid='+'(select OP_Guid '+
                                               ' from ABSys_Org_Operator  '+
                                               ' where OP_Code=:OP_Code)');
        tempStrings2.Add('OK_KE_Guid=KE_Guid='+'(select KE_Guid '+
                                               ' from ABSys_Org_Key  '+
                                               ' where KE_Code=:KE_Code)');
        tempStringDoOperator.AddStrings(aStrings);
        ABAddstr(aSQL,GetCurTreeSQL('Enterprise Dir'),ABEnterWrapStr+'go'+ABEnterWrapStr);
        DoItem;

        aStrings.Clear;
        aStrings.Add('admin');
        aStrings.Add('sysuser');
        tempStrings1.Clear;
        tempStrings1.Add('OP_Ti_Guid=Ti_Guid='+'(select Ti_Guid '+
                                               ' from ABSys_Org_TreeItem  '+
                                               ' where Ti_tr_Guid=(select Tr_Guid from ABSys_Org_Tree where Tr_Code=''Extend Type'') and '+
                                               '       Ti_Code=:Ti_Code)');
        ABAddstr(aSQL,GetCurTreeSQL('Extend Type'),ABEnterWrapStr+'go'+ABEnterWrapStr);
        DoItem;
        aStrings.Clear;
        aStrings.AddStrings(tempStringDoOperator);
      finally
        tempStringDoOperator.Free;
      end;
    end;
    function DoExtendReport(var aSQL:string;aStrings:TStrings;aItemName:string):boolean;
    var
      I: Integer;
      tempFileName:string;
    begin
      result:=True;
      ABAddstr(aSQL,GetCurTreeSQL('Extend ReportType'),ABEnterWrapStr+'go'+ABEnterWrapStr);
      //导出功能SQL
      tempStrings1.Clear;
      tempStrings1.Add('ER_TI_Guid=Ti_Guid='+'(select Ti_Guid '+
                                             ' from ABSys_Org_TreeItem  '+
                                             ' where Ti_tr_Guid=(select Tr_Guid from ABSys_Org_Tree where Tr_Code=''Extend ReportType'') and '+
                                             '       Ti_Code=:Ti_Code)');
      for i := 0 to aStrings.Count-1 do
      begin
        result:=DONextMainProgressBar;
        if not result then
          Break;

        if (tempDatasetExtendReport.Locate('ER_Name',aStrings[i],[])) then
        begin
          ABAddstr(aSQL,ABGetSQLByDataset(tempDatasetExtendReport,
                   'ABSys_Org_ExtendReport',
                   ['ER_TI_Guid','ER_Name'],
                   ABDeleteInArrayItem(tempFieldNamesArray_ABSys_Org_ExtendReport,['ER_TI_Guid','ER_Name','ER_Guid','PubID']),
                   tempFieldNamesArray_ABSys_Org_ExtendReport,
                   tempStrings1,[tempDatasetTreeItem]
                         ),ABEnterWrapStr+'go'+ABEnterWrapStr);

          tempFileName:= aItemName+'='+aStrings[i];
          if ABDownToFileFromField('Main','ABSys_Org_ExtendReport','Er_Format',
                                   'where Er_Name='+ABQuotedStr(aStrings[i]),
                                   ABTempPath+ tempFileName) then
          begin
            tempAllFileNameStrings.Add(ABTempPath+ tempFileName);
            ABWriteIniInStrings(aItemName,aStrings[i],tempFileName,tempAllOtherFileNameStrings);
          end;

          if (tempDatasetExtendReportDatasets.Locate('ED_ER_Guid',tempDatasetExtendReport.FieldByName('ER_Guid').AsString,[])) then
          begin
            ABSetDatasetFilter(tempDatasetExtendReportDatasets,'ED_ER_Guid='+QuotedStr(tempDatasetExtendReport.FieldByName('ER_Guid').AsString));
            try
              ABAddstr(aSQL,ABGetSQLByDataset(tempDatasetExtendReportDatasets,
                                                'ABSys_Org_ExtendReportDatasets',
                                   ['ED_ER_Guid','ED_Name'],
                                   ABDeleteInArrayItem(tempFieldNamesArray_ABSys_Org_ExtendReportDatasets,['ED_ER_Guid','ED_Name','ED_ParentGuid','ED_Guid','FubID']),
                                   tempFieldNamesArray_ABSys_Org_ExtendReportDatasets,
                                   tempStrings3,[tempDatasetTable,tempDatasetField],true
                                         ),ABEnterWrapStr+'go'+ABEnterWrapStr);
              tempFileName:= aItemName+'=ABSys_Org_ExtendReportDatasets='+aStrings[i]+'='+
                             tempDatasetExtendReportDatasets.FieldByName('ED_Name').AsString;
              if ABWriteTxt(ABTempPath+ tempFileName,ABDownToTextFromField('Main','ABSys_Org_ExtendReportDatasets','ED_SQL',
                                       'where Ed_Guid='+ABQuotedStr(tempDatasetExtendReportDatasets.FieldByName('ED_Guid').AsString))
                                      ) then
              begin
                tempAllFileNameStrings.Add(ABTempPath+ tempFileName);
                ABWriteIniInStrings(aItemName+'=ABSys_Org_ExtendReportDatasets',
                                    aStrings[i]+'='+tempDatasetExtendReportDatasets.FieldByName('ED_Name').AsString,
                                    tempFileName,tempAllOtherFileNameStrings);
              end;
            finally
              ABSetDatasetFilter(tempDatasetExtendReportDatasets,emptystr);
            end;
          end;
        end;
      end;
    end;
    function DoApproved(var aSQL:string;aStrings:TStrings;aItemName:string):boolean;
    var
      i:LongInt;
    begin
      result:=True;
      tempStrings1.Clear;
      tempStrings1.Add('CP_RU_Guid=RU_Guid='+'(select RU_Guid '+
                                             ' from ABSys_Approved_Rule  '+
                                             ' where RU_Name=:RU_Name)');
      tempStrings1.Add('CP_Bi_Guid=Bi_Guid='+'(select Bi_Guid '+
                                             ' from ABSys_Approved_Bill  '+
                                             ' where Bi_Name=:Bi_Name)');
      tempStrings2.Clear;
      tempStrings2.Add('RO_RU_Guid=RU_Guid='+'(select RU_Guid '+
                                             ' from ABSys_Approved_Rule  '+
                                             ' where RU_Name=:RU_Name)');
      tempStrings3.Clear;
      tempStrings3.Add('ST_RU_Guid=RU_Guid='+'(select RU_Guid '+
                                             ' from ABSys_Approved_Rule  '+
                                             ' where RU_Name=:RU_Name)');
      tempStrings4.Clear;
      tempStrings4.Add('SO_ST_Guid=ST_Guid='+'(select ST_Guid '+
                                             ' from ABSys_Approved_Step  '+
                                             ' where ST_RU_Guid=(select RU_Guid  from ABSys_Approved_Rule where RU_Name=:RU_Name) and '+
                                             '       ST_Name=:ST_Name '+
                                             ')');
      for i := 0 to aStrings.Count-1 do
      begin
        result:=DONextMainProgressBar;
        if not result then
          Break;

        if AnsiCompareText(aStrings[i],'ABSys_Approved_Message')=0 then
        begin
          ProgressBar2.MIN:=0;
          ProgressBar2.Max:=tempDatasetMessage.RecordCount;
          ProgressBar2.visible:=True;
          tempDatasetMessage.First;
          try
            while not tempDatasetMessage.Eof do
            begin
              result:=DONextDetailProgressBar;
              if not result then
                Break;

              ABAddstr(aSQL,ABGetSQLByDataset(tempDatasetMessage,
                       'ABSys_Approved_Message',
                       ['ME_Guid'],
                       ABDeleteInArrayItem(tempFieldNamesArray_ABSys_Approved_Message,['ME_Guid','FubID']),
                       tempFieldNamesArray_ABSys_Approved_Message,
                       nil,[]
                       ),ABEnterWrapStr+'go'+ABEnterWrapStr);

              tempDatasetMessage.Next;
            end;
          finally
            ProgressBar2.Position:=0;
            ProgressBar2.visible:=False;
          end;
        end
        else if AnsiCompareText(aStrings[i],'ABSys_Approved_Bill')=0 then
        begin
          ProgressBar2.MIN:=0;
          ProgressBar2.Max:=tempDatasetBill.RecordCount;
          ProgressBar2.visible:=True;
          tempDatasetBill.First;
          try
            while not tempDatasetBill.Eof do
            begin
              result:=DONextDetailProgressBar;
              if not result then
                Break;

              ABAddstr(aSQL,ABGetSQLByDataset(tempDatasetBill,
                       'ABSys_Approved_Bill',
                       ['Bi_Guid'],
                       ABDeleteInArrayItem(tempFieldNamesArray_ABSys_Approved_Bill,['Bi_Guid','FubID']),
                       tempFieldNamesArray_ABSys_Approved_Bill,
                       nil,[]
                       ),ABEnterWrapStr+'go'+ABEnterWrapStr);

              tempDatasetBill.Next;
            end;
          finally
            ProgressBar2.Position:=0;
            ProgressBar2.visible:=False;
          end;
        end
        else if AnsiCompareText(aStrings[i],'ABSys_Approved_Rule')=0 then
        begin
          ProgressBar2.MIN:=0;
          ProgressBar2.Max:=tempDatasetRule.RecordCount;
          ProgressBar2.visible:=True;
          tempDatasetRule.First;
          try
            while not tempDatasetRule.Eof do
            begin
              result:=DONextDetailProgressBar;
              if not result then
                Break;

              ABAddstr(aSQL,ABGetSQLByDataset(tempDatasetRule,
                       'ABSys_Approved_Rule',
                       ['RU_Name'],
                       ABDeleteInArrayItem(tempFieldNamesArray_ABSys_Approved_Rule,['RU_Name','RU_Guid','FubID']),
                       tempFieldNamesArray_ABSys_Approved_Rule,
                       nil,[]
                       ),ABEnterWrapStr+'go'+ABEnterWrapStr);

              if (tempDatasetConditionParam.Locate('CP_RU_Guid',tempDatasetRule.FieldByName('RU_Guid').AsString,[])) then
              begin
                ABSetDatasetFilter(tempDatasetConditionParam,'CP_RU_Guid='+QuotedStr(tempDatasetRule.FieldByName('RU_Guid').AsString));
                try
                  tempDatasetConditionParam.First;
                  while not tempDatasetConditionParam.Eof do
                  begin
                    ABAddstr(aSQL,ABGetSQLByDataset(tempDatasetConditionParam,
                                                      'ABSys_Approved_ConditionParam',
                                         ['CP_RU_Guid','CP_Guid'],
                                         ABDeleteInArrayItem(tempFieldNamesArray_ABSys_Approved_ConditionParam,['CP_RU_Guid','CP_Guid','FubID']),
                                         tempFieldNamesArray_ABSys_Approved_ConditionParam,
                                         tempStrings1,[tempDatasetRule,tempDatasetBill]
                                               ),ABEnterWrapStr+'go'+ABEnterWrapStr);
                    tempDatasetConditionParam.Next;
                  end;
                finally
                  ABSetDatasetFilter(tempDatasetConditionParam,emptystr);
                end;
              end;

              if (tempDatasetPassRuleOperator.Locate('RO_RU_Guid',tempDatasetRule.FieldByName('RU_Guid').AsString,[])) then
              begin
                ABSetDatasetFilter(tempDatasetPassRuleOperator,'RO_RU_Guid='+QuotedStr(tempDatasetRule.FieldByName('RU_Guid').AsString));
                try
                  tempDatasetPassRuleOperator.First;
                  while not tempDatasetPassRuleOperator.Eof do
                  begin
                    ABAddstr(aSQL,ABGetSQLByDataset(tempDatasetPassRuleOperator,
                                                      'ABSys_Approved_PassRuleOperator',
                                         ['RO_RU_Guid','RO_Guid'],
                                         ABDeleteInArrayItem(tempFieldNamesArray_ABSys_Approved_PassRuleOperator,['RO_RU_Guid','RO_Guid','FubID']),
                                         tempFieldNamesArray_ABSys_Approved_PassRuleOperator,
                                         tempStrings2,[tempDatasetRule]
                                               ),ABEnterWrapStr+'go'+ABEnterWrapStr);
                    tempDatasetPassRuleOperator.Next;
                  end;
                finally
                  ABSetDatasetFilter(tempDatasetPassRuleOperator,emptystr);
                end;
              end;
              if (tempDatasetStep.Locate('ST_RU_Guid',tempDatasetRule.FieldByName('RU_Guid').AsString,[])) then
              begin
                ABSetDatasetFilter(tempDatasetStep,'ST_RU_Guid='+QuotedStr(tempDatasetRule.FieldByName('RU_Guid').AsString));
                try
                  tempDatasetStep.First;
                  while not tempDatasetStep.Eof do
                  begin
                    ABAddstr(aSQL,ABGetSQLByDataset(tempDatasetStep,
                                                      'ABSys_Approved_Step',
                                         ['ST_RU_Guid','ST_Name'],
                                         ABDeleteInArrayItem(tempFieldNamesArray_ABSys_Approved_Step,['ST_RU_Guid','ST_Name','ST_Guid','FubID']),
                                         tempFieldNamesArray_ABSys_Approved_Step,
                                         tempStrings3,[tempDatasetRule]
                                               ),ABEnterWrapStr+'go'+ABEnterWrapStr);
                    if (tempDatasetStepOperator.Locate('SO_ST_Guid',tempDatasetStep.FieldByName('ST_Guid').AsString,[])) then
                    begin
                      ABSetDatasetFilter(tempDatasetStepOperator,'SO_ST_Guid='+QuotedStr(tempDatasetStep.FieldByName('ST_Guid').AsString));
                      try
                        tempDatasetStepOperator.First;
                        while not tempDatasetStepOperator.Eof do
                        begin
                          ABAddstr(aSQL,ABGetSQLByDataset(tempDatasetStepOperator,
                                                            'ABSys_Approved_StepOperator',
                                               ['SO_ST_Guid','SO_Guid'],
                                               ABDeleteInArrayItem(tempFieldNamesArray_ABSys_Approved_StepOperator,['SO_ST_Guid','SO_Guid','FubID']),
                                               tempFieldNamesArray_ABSys_Approved_StepOperator,
                                               tempStrings4,[tempDatasetRule,tempDatasetStep]
                                                     ),ABEnterWrapStr+'go'+ABEnterWrapStr);
                          tempDatasetStepOperator.Next;
                        end;
                      finally
                        ABSetDatasetFilter(tempDatasetStepOperator,emptystr);
                      end;
                    end;
                    tempDatasetStep.Next;
                  end;
                finally
                  ABSetDatasetFilter(tempDatasetStep,emptystr);
                end;
              end;
              tempDatasetRule.Next;
            end;
          finally
            ProgressBar2.Position:=0;
            ProgressBar2.visible:=False;
          end;
        end
      end;
    end;
    function DoLog(var aSQL:string;aStrings:TStrings;aItemName:string):boolean;
    var
      i:LongInt;
    begin
      result:=True;
      for i := 0 to aStrings.Count-1 do
      begin
        result:=DONextMainProgressBar;
        if not result then
          Break;

        if AnsiCompareText(aStrings[i],'ABSys_Log_CurLink')=0 then
        begin
          ProgressBar2.MIN:=0;
          ProgressBar2.Max:=tempDatasetCurLink.RecordCount;
          ProgressBar2.visible:=True;
          tempDatasetCurLink.First;
          try
            while not tempDatasetCurLink.Eof do
            begin
              result:=DONextDetailProgressBar;
              if not result then
                Break;

              ABAddstr(aSQL,ABGetSQLByDataset(tempDatasetCurLink,
                       'ABSys_Log_CurLink',
                       ['CL_Guid'],
                       ABDeleteInArrayItem(tempFieldNamesArray_ABSys_Log_CurLink,['CL_Guid','FubID']),
                       tempFieldNamesArray_ABSys_Log_CurLink,
                       nil,[]
                       ),ABEnterWrapStr+'go'+ABEnterWrapStr);

              tempDatasetCurLink.Next;
            end;
          finally
            ProgressBar2.Position:=0;
            ProgressBar2.visible:=False;
          end;
        end
        else if AnsiCompareText(aStrings[i],'ABSys_Log_CustomEvent')=0 then
        begin
          ProgressBar2.MIN:=0;
          ProgressBar2.Max:=tempDatasetCustomEvent.RecordCount;
          ProgressBar2.visible:=True;
          tempDatasetCustomEvent.First;
          try
            while not tempDatasetCustomEvent.Eof do
            begin
              result:=DONextDetailProgressBar;
              if not result then
                Break;

              ABAddstr(aSQL,ABGetSQLByDataset(tempDatasetCustomEvent,
                       'ABSys_Log_CustomEvent',
                       ['CE_Guid'],
                       ABDeleteInArrayItem(tempFieldNamesArray_ABSys_Log_CustomEvent,['CE_Guid','FubID']),
                       tempFieldNamesArray_ABSys_Log_CustomEvent,
                       nil,[]
                       ),ABEnterWrapStr+'go'+ABEnterWrapStr);

              tempDatasetCustomEvent.Next;
            end;
          finally
            ProgressBar2.Position:=0;
            ProgressBar2.visible:=False;
          end;
        end
        else if AnsiCompareText(aStrings[i],'ABSys_Log_ErrorInfo')=0 then
        begin
          ProgressBar2.MIN:=0;
          ProgressBar2.Max:=tempDatasetErrorInfo.RecordCount;
          ProgressBar2.visible:=True;
          tempDatasetErrorInfo.First;
          try
            while not tempDatasetErrorInfo.Eof do
            begin
              result:=DONextDetailProgressBar;
              if not result then
                Break;

              ABAddstr(aSQL,ABGetSQLByDataset(tempDatasetErrorInfo,
                       'ABSys_Log_ErrorInfo',
                       ['EI_Guid'],
                       ABDeleteInArrayItem(tempFieldNamesArray_ABSys_Log_ErrorInfo,['EI_Guid','FubID']),
                       tempFieldNamesArray_ABSys_Log_ErrorInfo,
                       nil,[]
                       ),ABEnterWrapStr+'go'+ABEnterWrapStr);

              tempDatasetErrorInfo.Next;
            end;
          finally
            ProgressBar2.Position:=0;
            ProgressBar2.visible:=False;
          end;
        end
        else if AnsiCompareText(aStrings[i],'ABSys_Log_FieldChange')=0 then
        begin
          ProgressBar2.MIN:=0;
          ProgressBar2.Max:=tempDatasetFieldChange.RecordCount;
          ProgressBar2.visible:=True;
          tempDatasetFieldChange.First;
          try
            while not tempDatasetFieldChange.Eof do
            begin
              result:=DONextDetailProgressBar;
              if not result then
                Break;

              ABAddstr(aSQL,ABGetSQLByDataset(tempDatasetFieldChange,
                       'ABSys_Log_FieldChange',
                       ['FC_Guid'],
                       ABDeleteInArrayItem(tempFieldNamesArray_ABSys_Log_FieldChange,['FC_Guid','FubID']),
                       tempFieldNamesArray_ABSys_Log_FieldChange,
                       nil,[]
                       ),ABEnterWrapStr+'go'+ABEnterWrapStr);

              tempDatasetFieldChange.Next;
            end;
          finally
            ProgressBar2.Position:=0;
            ProgressBar2.visible:=False;
          end;
        end
        else if AnsiCompareText(aStrings[i],'ABSys_Log_Login')=0 then
        begin
          ProgressBar2.MIN:=0;
          ProgressBar2.Max:=tempDatasetLogin.RecordCount;
          ProgressBar2.visible:=True;
          tempDatasetLogin.First;
          try
            while not tempDatasetLogin.Eof do
            begin
              result:=DONextDetailProgressBar;
              if not result then
                Break;

              ABAddstr(aSQL,ABGetSQLByDataset(tempDatasetLogin,
                       'ABSys_Log_Login',
                       ['LO_Guid'],
                       ABDeleteInArrayItem(tempFieldNamesArray_ABSys_Log_Login,['LO_Guid','FubID']),
                       tempFieldNamesArray_ABSys_Log_Login,
                       nil,[]
                       ),ABEnterWrapStr+'go'+ABEnterWrapStr);

              tempDatasetLogin.Next;
            end;
          finally
            ProgressBar2.Position:=0;
            ProgressBar2.visible:=False;
          end;
        end;
      end;
    end;
  begin
    //admin sysuser
    tempCount:=2;
    for I := 0 to aStringsList.Count -1 do
    begin
      tempCount:=tempCount+TStrings(aStringsList.Objects[i]).Count;
    end;
    ProgressBar1.MIN:=0;
    ProgressBar1.Max:=tempCount;
    ProgressBar1.visible:=True;
    tempAllFileNameStrings:=TStringList.Create;
    tempAllSQLFileNameStrings:=TStringList.Create;
    tempAllOtherFileNameStrings:=TStringList.Create;
    tempStrings0:=TStringList.Create;
    tempStrings1:=TStringList.Create;
    tempStrings2:=TStringList.Create;
    tempStrings3:=TStringList.Create;
    tempStrings4:=TStringList.Create;
    tempStrings5:=TStringList.Create;
    tempStrings6:=TStringList.Create;
    tempStrings7:=TStringList.Create;
    Memo1.Clear;
    tempOk:=false;
    try
      for I := 0 to aStringsList.Count -1 do
      begin
        tempStrings:=TStrings(aStringsList.Objects[i]);
        if tempStrings.Count>0 then
        begin
          tempSQL:='set nocount on';
              if tempStrings=Memo9.Lines then
          begin
            tempOk:=DoTree(tempSQL,tempStrings,aStringsList[i]);
          end
          else if tempStrings=Memo3.Lines then
          begin
            tempOk:=DoDictionaryInfo(tempSQL,tempStrings,aStringsList[i]);
          end
          else if tempStrings=Memo10.Lines then
          begin
            tempOk:=DoParameter(tempSQL,tempStrings,aStringsList[i]);
          end
          else if tempStrings=Memo4.Lines then
          begin
            tempOk:=DoMail(tempSQL,tempStrings,aStringsList[i]);
          end
          else if tempStrings=Memo2.Lines then
          begin
            tempOk:=DoHistoryData(tempSQL,tempStrings,aStringsList[i]);
          end
          else if tempStrings=Memo14.Lines then
          begin
            tempOk:=DoBillInput(tempSQL,tempStrings,aStringsList[i]);
          end
          else if tempStrings=Memo5.Lines then
          begin
            tempOk:=DoFunc(tempSQL,tempStrings,aStringsList[i]);
          end
          else if tempStrings=Memo6.Lines then
          begin
            tempOk:=DoKey(tempSQL,tempStrings,aStringsList[i]);
          end
          else if tempStrings=Memo7.Lines then
          begin
            tempOk:=DoOperator(tempSQL,tempStrings,aStringsList[i]);
          end
          else if tempStrings=Memo11.Lines then
          begin
            tempOk:=DoApproved(tempSQL,tempStrings,aStringsList[i]);
          end
          else if tempStrings=Memo8.Lines then
          begin
            tempOk:=DoExtendReport(tempSQL,tempStrings,aStringsList[i]);
          end
          else if tempStrings=Memo12.Lines then
          begin
            tempOk:=DoLog(tempSQL,tempStrings,aStringsList[i]);
          end;

          if tempOk then
          begin
            ABAddstr(tempSQL,'set nocount oFF',ABEnterWrapStr+'go'+ABEnterWrapStr);
            ABWriteTxt(ABTempPath+aStringsList[i]+'.SQL',tempSQL);
            tempAllFileNameStrings.Add(ABTempPath+aStringsList[i]+'.SQL');
            tempAllSQLFileNameStrings.Add(aStringsList[i]+'.SQL');
            Memo1.Lines.Add(ABTempPath+aStringsList[i]+'.SQL'+' OK.');
          end;
        end;
      end;

      if tempOk then
      begin
        tempAllSQLFileNameStrings.SaveToFile(ABTempPath+'SQLFileList.txt');
        tempAllOtherFileNameStrings.SaveToFile(ABTempPath+'OtherFileList.txt');
        tempAllFileNameStrings.Add(ABTempPath+'SQLFileList.txt');
        tempAllFileNameStrings.Add(ABTempPath+'OtherFileList.txt');
        if ABDoPak(tempAllFileNameStrings,tempOutFileName) then
        begin
          //ABDeleteFile(tempAllFileNameStrings);
          ABShow('导出成功');
        end;
      end;
    finally
      ProgressBar1.Position:=0;
      ProgressBar1.visible:=False;
      tempAllFileNameStrings.free;
      tempAllSQLFileNameStrings.free;
      tempAllOtherFileNameStrings.free;
      tempStrings0.Free;
      tempStrings1.Free;
      tempStrings2.Free;
      tempStrings3.Free;
      tempStrings4.Free;
      tempStrings5.Free;
      tempStrings6.Free;
      tempStrings7.Free;
    end;
  end;
begin
  SetButtonRun;
  try
    tempOutFileName:= ABSaveFile('','','PAK FILES|*.pak');
    if tempOutFileName<>EmptyStr  then
    begin
      tempOutPath:=ABGetFilePath(tempOutFileName);

      tempFieldNames_ABSys_Org_ConnList             :=ABGetTableFieldNames_CheckTypeNames('',trim('ABSys_Org_ConnList             '),['image','ntext','text']);
      tempFieldNames_ABSys_Org_Table                :=ABGetTableFieldNames_CheckTypeNames('',trim('ABSys_Org_Table                '),['image','ntext','text']);
      tempFieldNames_ABSys_Org_Field                :=ABGetTableFieldNames_CheckTypeNames('',trim('ABSys_Org_Field                '),['image','ntext','text']);
      tempFieldNames_ABSys_Org_Function             :=ABGetTableFieldNames_CheckTypeNames('',trim('ABSys_Org_Function             '),['image','ntext','text']);
      tempFieldNames_ABSys_Org_FuncTemplate         :=ABGetTableFieldNames_CheckTypeNames('',trim('ABSys_Org_FuncTemplate         '),['image','ntext','text']);
      tempFieldNames_ABSys_Org_FuncReport           :=ABGetTableFieldNames_CheckTypeNames('',trim('ABSys_Org_FuncReport           '),['image','ntext','text']);
      tempFieldNames_ABSys_Org_FuncSQL              :=ABGetTableFieldNames_CheckTypeNames('',trim('ABSys_Org_FuncSQL              '),['image','ntext','text']);
      tempFieldNames_ABSys_Org_FuncControlProperty  :=ABGetTableFieldNames_CheckTypeNames('',trim('ABSys_Org_FuncControlProperty  '),['image','ntext','text']);
      tempFieldNames_ABSys_Org_ExtendReport         :=ABGetTableFieldNames_CheckTypeNames('',trim('ABSys_Org_ExtendReport         '),['image','ntext','text']);
      tempFieldNames_ABSys_Org_ExtendReportDatasets :=ABGetTableFieldNames_CheckTypeNames('',trim('ABSys_Org_ExtendReportDatasets '),['image','ntext','text']);
      tempFieldNames_ABSys_Org_Operator             :=ABGetTableFieldNames_CheckTypeNames('',trim('ABSys_Org_Operator             '),['image','ntext','text']);
      tempFieldNames_ABSys_Org_Key                  :=ABGetTableFieldNames_CheckTypeNames('',trim('ABSys_Org_Key                  '),['image','ntext','text']);
      tempFieldNames_ABSys_Right_OperatorKey        :=ABGetTableFieldNames_CheckTypeNames('',trim('ABSys_Right_OperatorKey        '),['image','ntext','text']);
      tempFieldNames_ABSys_Right_KeyExtendReport    :=ABGetTableFieldNames_CheckTypeNames('',trim('ABSys_Right_KeyExtendReport    '),['image','ntext','text']);
      tempFieldNames_ABSys_Right_KeyField           :=ABGetTableFieldNames_CheckTypeNames('',trim('ABSys_Right_KeyField           '),['image','ntext','text']);
      tempFieldNames_ABSys_Right_KeyFunction        :=ABGetTableFieldNames_CheckTypeNames('',trim('ABSys_Right_KeyFunction        '),['image','ntext','text']);
      tempFieldNames_ABSys_Right_KeyTable           :=ABGetTableFieldNames_CheckTypeNames('',trim('ABSys_Right_KeyTable           '),['image','ntext','text']);
      tempFieldNames_ABSys_Right_KeyTableRow        :=ABGetTableFieldNames_CheckTypeNames('',trim('ABSys_Right_KeyTableRow        '),['image','ntext','text']);
      tempFieldNames_ABSys_Right_KeyCustomObject    :=ABGetTableFieldNames_CheckTypeNames('',trim('ABSys_Right_KeyCustomObject    '),['image','ntext','text']);
      tempFieldNames_ABSys_Org_Parameter            :=ABGetTableFieldNames_CheckTypeNames('',trim('ABSys_Org_Parameter            '),['image','ntext','text']);
      tempFieldNames_ABSys_Org_Tree                 :=ABGetTableFieldNames_CheckTypeNames('',trim('ABSys_Org_Tree                 '),['image','ntext','text']);
      tempFieldNames_ABSys_Org_TreeItem             :=ABGetTableFieldNames_CheckTypeNames('',trim('ABSys_Org_TreeItem             '),['image','ntext','text']);
      tempFieldNames_ABSys_Org_Mail                 :=ABGetTableFieldNames_CheckTypeNames('',trim('ABSys_Org_Mail                 '),['image','ntext','text']);
      tempFieldNames_ABSys_Org_MailFile             :=ABGetTableFieldNames_CheckTypeNames('',trim('ABSys_Org_MailFile             '),['image','ntext','text']);
      tempFieldNames_ABSys_Org_HistoryData          :=ABGetTableFieldNames_CheckTypeNames('',trim('ABSys_Org_HistoryData          '),['image','ntext','text']);
      tempFieldNames_ABSys_Org_BillInput            :=ABGetTableFieldNames_CheckTypeNames('',trim('ABSys_Org_BillInput            '),['image','ntext','text']);
      tempFieldNames_ABSys_Approved_Message         :=ABGetTableFieldNames_CheckTypeNames('',trim('ABSys_Approved_Message         '),['image','ntext','text']);
      tempFieldNames_ABSys_Approved_Bill            :=ABGetTableFieldNames_CheckTypeNames('',trim('ABSys_Approved_Bill            '),['image','ntext','text']);
      tempFieldNames_ABSys_Approved_Rule            :=ABGetTableFieldNames_CheckTypeNames('',trim('ABSys_Approved_Rule            '),['image','ntext','text']);
      tempFieldNames_ABSys_Approved_ConditionParam  :=ABGetTableFieldNames_CheckTypeNames('',trim('ABSys_Approved_ConditionParam  '),['image','ntext','text']);
      tempFieldNames_ABSys_Approved_PassRuleOperator:=ABGetTableFieldNames_CheckTypeNames('',trim('ABSys_Approved_PassRuleOperator'),['image','ntext','text']);
      tempFieldNames_ABSys_Approved_Step            :=ABGetTableFieldNames_CheckTypeNames('',trim('ABSys_Approved_Step            '),['image','ntext','text']);
      tempFieldNames_ABSys_Approved_StepOperator    :=ABGetTableFieldNames_CheckTypeNames('',trim('ABSys_Approved_StepOperator    '),['image','ntext','text']);
      tempFieldNames_ABSys_Log_CurLink              :=ABGetTableFieldNames_CheckTypeNames('',trim('ABSys_Log_CurLink              '),['image','ntext','text']);
      tempFieldNames_ABSys_Log_CustomEvent          :=ABGetTableFieldNames_CheckTypeNames('',trim('ABSys_Log_CustomEvent          '),['image','ntext','text']);
      tempFieldNames_ABSys_Log_ErrorInfo            :=ABGetTableFieldNames_CheckTypeNames('',trim('ABSys_Log_ErrorInfo            '),['image','ntext','text']);
      tempFieldNames_ABSys_Log_FieldChange          :=ABGetTableFieldNames_CheckTypeNames('',trim('ABSys_Log_FieldChange          '),['image','ntext','text']);
      tempFieldNames_ABSys_Log_Login                :=ABGetTableFieldNames_CheckTypeNames('',trim('ABSys_Log_Login                '),['image','ntext','text']);

      tempSQL_ABSys_Org_ConnList                    :='select '+tempFieldNames_ABSys_Org_ConnList             +' from ABSys_Org_ConnList              ';
      tempSQL_ABSys_Org_Table                       :='select '+tempFieldNames_ABSys_Org_Table                +' from ABSys_Org_Table                 ';
      tempSQL_ABSys_Org_Field                       :='select '+tempFieldNames_ABSys_Org_Field                +' from ABSys_Org_Field                 ';
      tempSQL_ABSys_Org_Function                    :='select '+tempFieldNames_ABSys_Org_Function             +' from ABSys_Org_Function              ';
      tempSQL_ABSys_Org_FuncTemplate                :='select '+tempFieldNames_ABSys_Org_FuncTemplate         +' from ABSys_Org_FuncTemplate          ';
      tempSQL_ABSys_Org_FuncReport                  :='select '+tempFieldNames_ABSys_Org_FuncReport           +' from ABSys_Org_FuncReport            ';
      tempSQL_ABSys_Org_FuncSQL                     :='select '+tempFieldNames_ABSys_Org_FuncSQL              +' from ABSys_Org_FuncSQL               ';
      tempSQL_ABSys_Org_FuncControlProperty         :='select '+tempFieldNames_ABSys_Org_FuncControlProperty  +' from ABSys_Org_FuncControlProperty   ';
      tempSQL_ABSys_Org_ExtendReport                :='select '+tempFieldNames_ABSys_Org_ExtendReport         +' from ABSys_Org_ExtendReport          ';
      tempSQL_ABSys_Org_ExtendReportDatasets        :='select '+tempFieldNames_ABSys_Org_ExtendReportDatasets +' from ABSys_Org_ExtendReportDatasets  ';
      tempSQL_ABSys_Org_Operator                    :='select '+tempFieldNames_ABSys_Org_Operator             +' from ABSys_Org_Operator              ';
      tempSQL_ABSys_Org_Key                         :='select '+tempFieldNames_ABSys_Org_Key                  +' from ABSys_Org_Key                   ';
      tempSQL_ABSys_Right_OperatorKey               :='select '+tempFieldNames_ABSys_Right_OperatorKey        +' from ABSys_Right_OperatorKey         ';
      tempSQL_ABSys_Right_KeyExtendReport           :='select '+tempFieldNames_ABSys_Right_KeyExtendReport    +' from ABSys_Right_KeyExtendReport     ';
      tempSQL_ABSys_Right_KeyField                  :='select '+tempFieldNames_ABSys_Right_KeyField           +' from ABSys_Right_KeyField            ';
      tempSQL_ABSys_Right_KeyFunction               :='select '+tempFieldNames_ABSys_Right_KeyFunction        +' from ABSys_Right_KeyFunction         ';
      tempSQL_ABSys_Right_KeyTable                  :='select '+tempFieldNames_ABSys_Right_KeyTable           +' from ABSys_Right_KeyTable            ';
      tempSQL_ABSys_Right_KeyTableRow               :='select '+tempFieldNames_ABSys_Right_KeyTableRow        +' from ABSys_Right_KeyTableRow         ';
      tempSQL_ABSys_Right_KeyCustomObject           :='select '+tempFieldNames_ABSys_Right_KeyCustomObject    +' from ABSys_Right_KeyCustomObject     ';
      tempSQL_ABSys_Org_Parameter                   :='select '+tempFieldNames_ABSys_Org_Parameter            +' from ABSys_Org_Parameter             ';
      tempSQL_ABSys_Org_Tree                        :='select '+tempFieldNames_ABSys_Org_Tree                 +' from ABSys_Org_Tree                  ';
      tempSQL_ABSys_Org_TreeItem                    :='select '+tempFieldNames_ABSys_Org_TreeItem             +' from ABSys_Org_TreeItem              ';
      tempSQL_ABSys_Org_Mail                        :='select '+tempFieldNames_ABSys_Org_Mail                 +' from ABSys_Org_Mail                  ';
      tempSQL_ABSys_Org_MailFile                    :='select '+tempFieldNames_ABSys_Org_MailFile             +' from ABSys_Org_MailFile              ';
      tempSQL_ABSys_Org_HistoryData                 :='select '+tempFieldNames_ABSys_Org_HistoryData          +' from ABSys_Org_HistoryData           ';
      tempSQL_ABSys_Org_BillInput                   :='select '+tempFieldNames_ABSys_Org_BillInput            +' from ABSys_Org_BillInput             ';
      tempSQL_ABSys_Approved_Message                :='select '+tempFieldNames_ABSys_Approved_Message         +' from ABSys_Approved_Message          ';
      tempSQL_ABSys_Approved_Bill                   :='select '+tempFieldNames_ABSys_Approved_Bill            +' from ABSys_Approved_Bill             ';
      tempSQL_ABSys_Approved_Rule                   :='select '+tempFieldNames_ABSys_Approved_Rule            +' from ABSys_Approved_Rule             ';
      tempSQL_ABSys_Approved_ConditionParam         :='select '+tempFieldNames_ABSys_Approved_ConditionParam  +' from ABSys_Approved_ConditionParam   ';
      tempSQL_ABSys_Approved_PassRuleOperator       :='select '+tempFieldNames_ABSys_Approved_PassRuleOperator+' from ABSys_Approved_PassRuleOperator ';
      tempSQL_ABSys_Approved_Step                   :='select '+tempFieldNames_ABSys_Approved_Step            +' from ABSys_Approved_Step             ';
      tempSQL_ABSys_Approved_StepOperator           :='select '+tempFieldNames_ABSys_Approved_StepOperator    +' from ABSys_Approved_StepOperator     ';
      tempSQL_ABSys_Log_CurLink                     :='select '+tempFieldNames_ABSys_Log_CurLink              +' from ABSys_Log_CurLink               ';
      tempSQL_ABSys_Log_CustomEvent                 :='select '+tempFieldNames_ABSys_Log_CustomEvent          +' from ABSys_Log_CustomEvent           ';
      tempSQL_ABSys_Log_ErrorInfo                   :='select '+tempFieldNames_ABSys_Log_ErrorInfo            +' from ABSys_Log_ErrorInfo             ';
      tempSQL_ABSys_Log_FieldChange                 :='select '+tempFieldNames_ABSys_Log_FieldChange          +' from ABSys_Log_FieldChange           ';
      tempSQL_ABSys_Log_Login                       :='select '+tempFieldNames_ABSys_Log_Login                +' from ABSys_Log_Login                 ';

      tempFieldNamesArray_ABSys_Org_ConnList             :=ABStrToStringArray(tempFieldNames_ABSys_Org_ConnList            );
      tempFieldNamesArray_ABSys_Org_Table                :=ABStrToStringArray(tempFieldNames_ABSys_Org_Table               );
      tempFieldNamesArray_ABSys_Org_Field                :=ABStrToStringArray(tempFieldNames_ABSys_Org_Field               );
      tempFieldNamesArray_ABSys_Org_Function             :=ABStrToStringArray(tempFieldNames_ABSys_Org_Function            );
      tempFieldNamesArray_ABSys_Org_FuncTemplate         :=ABStrToStringArray(tempFieldNames_ABSys_Org_FuncTemplate        );
      tempFieldNamesArray_ABSys_Org_FuncReport           :=ABStrToStringArray(tempFieldNames_ABSys_Org_FuncReport          );
      tempFieldNamesArray_ABSys_Org_FuncSQL              :=ABStrToStringArray(tempFieldNames_ABSys_Org_FuncSQL             );
      tempFieldNamesArray_ABSys_Org_FuncControlProperty  :=ABStrToStringArray(tempFieldNames_ABSys_Org_FuncControlProperty );
      tempFieldNamesArray_ABSys_Org_ExtendReport         :=ABStrToStringArray(tempFieldNames_ABSys_Org_ExtendReport        );
      tempFieldNamesArray_ABSys_Org_ExtendReportDatasets :=ABStrToStringArray(tempFieldNames_ABSys_Org_ExtendReportDatasets);
      tempFieldNamesArray_ABSys_Org_Operator             :=ABStrToStringArray(tempFieldNames_ABSys_Org_Operator            );
      tempFieldNamesArray_ABSys_Org_Key                  :=ABStrToStringArray(tempFieldNames_ABSys_Org_Key                 );
      tempFieldNamesArray_ABSys_Right_OperatorKey        :=ABStrToStringArray(tempFieldNames_ABSys_Right_OperatorKey       );
      tempFieldNamesArray_ABSys_Right_KeyExtendReport    :=ABStrToStringArray(tempFieldNames_ABSys_Right_KeyExtendReport   );
      tempFieldNamesArray_ABSys_Right_KeyField           :=ABStrToStringArray(tempFieldNames_ABSys_Right_KeyField          );
      tempFieldNamesArray_ABSys_Right_KeyFunction        :=ABStrToStringArray(tempFieldNames_ABSys_Right_KeyFunction       );
      tempFieldNamesArray_ABSys_Right_KeyTable           :=ABStrToStringArray(tempFieldNames_ABSys_Right_KeyTable          );
      tempFieldNamesArray_ABSys_Right_KeyTableRow        :=ABStrToStringArray(tempFieldNames_ABSys_Right_KeyTableRow       );
      tempFieldNamesArray_ABSys_Right_KeyCustomObject    :=ABStrToStringArray(tempFieldNames_ABSys_Right_KeyCustomObject   );
      tempFieldNamesArray_ABSys_Org_Parameter            :=ABStrToStringArray(tempFieldNames_ABSys_Org_Parameter           );
      tempFieldNamesArray_ABSys_Org_Tree                 :=ABStrToStringArray(tempFieldNames_ABSys_Org_Tree                );
      tempFieldNamesArray_ABSys_Org_TreeItem             :=ABStrToStringArray(tempFieldNames_ABSys_Org_TreeItem            );
      tempFieldNamesArray_ABSys_Org_Mail                 :=ABStrToStringArray(tempFieldNames_ABSys_Org_Mail                 );
      tempFieldNamesArray_ABSys_Org_MailFile             :=ABStrToStringArray(tempFieldNames_ABSys_Org_MailFile             );
      tempFieldNamesArray_ABSys_Org_HistoryData          :=ABStrToStringArray(tempFieldNames_ABSys_Org_HistoryData          );
      tempFieldNamesArray_ABSys_Org_BillInput            :=ABStrToStringArray(tempFieldNames_ABSys_Org_BillInput            );
      tempFieldNamesArray_ABSys_Approved_Message         :=ABStrToStringArray(tempFieldNames_ABSys_Approved_Message         );
      tempFieldNamesArray_ABSys_Approved_Bill            :=ABStrToStringArray(tempFieldNames_ABSys_Approved_Bill            );
      tempFieldNamesArray_ABSys_Approved_Rule            :=ABStrToStringArray(tempFieldNames_ABSys_Approved_Rule            );
      tempFieldNamesArray_ABSys_Approved_ConditionParam  :=ABStrToStringArray(tempFieldNames_ABSys_Approved_ConditionParam  );
      tempFieldNamesArray_ABSys_Approved_PassRuleOperator:=ABStrToStringArray(tempFieldNames_ABSys_Approved_PassRuleOperator);
      tempFieldNamesArray_ABSys_Approved_Step            :=ABStrToStringArray(tempFieldNames_ABSys_Approved_Step            );
      tempFieldNamesArray_ABSys_Approved_StepOperator    :=ABStrToStringArray(tempFieldNames_ABSys_Approved_StepOperator    );
      tempFieldNamesArray_ABSys_Log_CurLink              :=ABStrToStringArray(tempFieldNames_ABSys_Log_CurLink              );
      tempFieldNamesArray_ABSys_Log_CustomEvent          :=ABStrToStringArray(tempFieldNames_ABSys_Log_CustomEvent          );
      tempFieldNamesArray_ABSys_Log_ErrorInfo            :=ABStrToStringArray(tempFieldNames_ABSys_Log_ErrorInfo            );
      tempFieldNamesArray_ABSys_Log_FieldChange          :=ABStrToStringArray(tempFieldNames_ABSys_Log_FieldChange          );
      tempFieldNamesArray_ABSys_Log_Login                :=ABStrToStringArray(tempFieldNames_ABSys_Log_Login                );

      tempDatasetConnList            := ABGetDataset('Main',tempSQL_ABSys_Org_ConnList             ,[]);
      tempDatasetTable               := ABGetDataset('Main',tempSQL_ABSys_Org_Table                ,[]);
      tempDatasetField               := ABGetDataset('Main',tempSQL_ABSys_Org_Field                ,[]);
      tempDatasetFunction            := ABGetDataset('Main',tempSQL_ABSys_Org_Function             ,[]);
      tempDatasetFuncTemplate        := ABGetDataset('Main',tempSQL_ABSys_Org_FuncTemplate         ,[]);
      tempDatasetFuncReport          := ABGetDataset('Main',tempSQL_ABSys_Org_FuncReport           ,[]);
      tempDatasetFuncSQL             := ABGetDataset('Main',tempSQL_ABSys_Org_FuncSQL              ,[]);
      tempDatasetFuncControlProperty := ABGetDataset('Main',tempSQL_ABSys_Org_FuncControlProperty  ,[]);
      tempDatasetExtendReport        := ABGetDataset('Main',tempSQL_ABSys_Org_ExtendReport         ,[]);
      tempDatasetExtendReportDatasets:= ABGetDataset('Main',tempSQL_ABSys_Org_ExtendReportDatasets ,[]);
      tempDatasetOperator            := ABGetDataset('Main',tempSQL_ABSys_Org_Operator             ,[]);
      tempDatasetKey                 := ABGetDataset('Main',tempSQL_ABSys_Org_Key                  ,[]);
      tempDatasetOperatorKey         := ABGetDataset('Main',tempSQL_ABSys_Right_OperatorKey        ,[]);
      tempDatasetKeyExtendReport     := ABGetDataset('Main',tempSQL_ABSys_Right_KeyExtendReport    ,[]);
      tempDatasetKeyField            := ABGetDataset('Main',tempSQL_ABSys_Right_KeyField           ,[]);
      tempDatasetKeyFunction         := ABGetDataset('Main',tempSQL_ABSys_Right_KeyFunction        ,[]);
      tempDatasetKeyTable            := ABGetDataset('Main',tempSQL_ABSys_Right_KeyTable           ,[]);
      tempDatasetKeyTableRow         := ABGetDataset('Main',tempSQL_ABSys_Right_KeyTableRow        ,[]);
      tempDatasetKeyCustomObject     := ABGetDataset('Main',tempSQL_ABSys_Right_KeyCustomObject    ,[]);
      tempDatasetParameter           := ABGetDataset('Main',tempSQL_ABSys_Org_Parameter            ,[]);
      tempDatasetTree                := ABGetDataset('Main',tempSQL_ABSys_Org_Tree                 ,[]);
      tempDatasetTreeItem            := ABGetDataset('Main',tempSQL_ABSys_Org_TreeItem             ,[]);
      tempDatasetMail                := ABGetDataset('Main',tempSQL_ABSys_Org_Mail                 ,[]);
      tempDatasetMailFile            := ABGetDataset('Main',tempSQL_ABSys_Org_MailFile             ,[]);
      tempDatasetHistoryData         := ABGetDataset('Main',tempSQL_ABSys_Org_HistoryData          ,[]);
      tempDatasetBillInput           := ABGetDataset('Main',tempSQL_ABSys_Org_BillInput            ,[]);
      tempDatasetMessage             := ABGetDataset('Main',tempSQL_ABSys_Approved_Message         ,[]);
      tempDatasetBill                := ABGetDataset('Main',tempSQL_ABSys_Approved_Bill            ,[]);
      tempDatasetRule                := ABGetDataset('Main',tempSQL_ABSys_Approved_Rule            ,[]);
      tempDatasetConditionParam      := ABGetDataset('Main',tempSQL_ABSys_Approved_ConditionParam  ,[]);
      tempDatasetPassRuleOperator    := ABGetDataset('Main',tempSQL_ABSys_Approved_PassRuleOperator,[]);
      tempDatasetStep                := ABGetDataset('Main',tempSQL_ABSys_Approved_Step            ,[]);
      tempDatasetStepOperator        := ABGetDataset('Main',tempSQL_ABSys_Approved_StepOperator    ,[]);
      tempDatasetCurLink             := ABGetDataset('Main',tempSQL_ABSys_Log_CurLink              ,[]);
      tempDatasetCustomEvent         := ABGetDataset('Main',tempSQL_ABSys_Log_CustomEvent          ,[]);
      tempDatasetErrorInfo           := ABGetDataset('Main',tempSQL_ABSys_Log_ErrorInfo            ,[]);
      tempDatasetFieldChange         := ABGetDataset('Main',tempSQL_ABSys_Log_FieldChange          ,[]);
      tempDatasetLogin               := ABGetDataset('Main',tempSQL_ABSys_Log_Login                ,[]);
      tempDoStringsList:=TStringList.Create;
      try
        tempDoStringsList.AddObject('Tree',Memo9.Lines);
        tempDoStringsList.AddObject('DictionaryInfo',Memo3.Lines);
        tempDoStringsList.AddObject('Func',Memo5.Lines);
        tempDoStringsList.AddObject('Parameter',Memo10.Lines);
        tempDoStringsList.AddObject('Mail',Memo4.Lines);
        tempDoStringsList.AddObject('HistoryData',Memo2.Lines);
        tempDoStringsList.AddObject('BillInput',Memo14.Lines);
        tempDoStringsList.AddObject('ExtendReport',Memo8.Lines);
        tempDoStringsList.AddObject('Key',Memo6.Lines);
        tempDoStringsList.AddObject('Operator',Memo7.Lines);
        tempDoStringsList.AddObject('Approved',Memo11.Lines);
        tempDoStringsList.AddObject('Log',Memo12.Lines);
        DoItem(tempDoStringsList);
      finally
        tempDoStringsList.Free;
        tempDatasetConnList.free;
        tempDatasetTable.free;
        tempDatasetField.free;

        tempDatasetFunction.free;
        tempDatasetFuncTemplate.free;
        tempDatasetFuncReport.free;
        tempDatasetFuncControlProperty.free;
        tempDatasetFuncSQL.free;

        tempDatasetExtendReport.free;
        tempDatasetExtendReportDatasets.free;

        tempDatasetOperator.free;
        tempDatasetKey.free;

        tempDatasetOperatorKey.free;
        tempDatasetKeyFunction.free;
        tempDatasetKeyExtendReport.free;
        tempDatasetKeyTable.free;
        tempDatasetKeyField.free;
        tempDatasetKeyTableRow.free;
        tempDatasetKeyCustomObject.free;

        tempDatasetParameter.free;
        tempDatasetTree.free;
        tempDatasetTreeItem.free;

        tempDatasetMail              .free;
        tempDatasetMailFile          .free;
        tempDatasetHistoryData       .free;
        tempDatasetBillInput         .free;
        tempDatasetMessage           .free;
        tempDatasetRule              .free;
        tempDatasetConditionParam    .free;
        tempDatasetPassRuleOperator  .free;
        tempDatasetStep              .free;
        tempDatasetStepOperator      .free;
        tempDatasetCurLink           .free;
        tempDatasetCustomEvent       .free;
        tempDatasetErrorInfo         .free;
        tempDatasetFieldChange       .free;
        tempDatasetLogin             .free;
      end;
    end;
  finally
    SetButtonStop;
  end;
end;

procedure TABSys_Org_InputOutputForm.DoInput;
var
  i:LongInt;
  tempStr,
  tempFileName:String;
  tempString:Tstrings;
  //输入输出的中间文件名
  tempAllFileNameStrings:Tstrings;
  tempAllSQLFileNameStrings:Tstrings;
  tempAllOtherFileNameStrings:Tstrings;
  tempAllOtherTypeStrings:Tstrings;
  tempCurOtherFileNameStrings:Tstrings;
  procedure DoExecSQLs(aString:Tstrings;aFieldName:string) ;
  var
    tempSQL:String;
    j:LongInt;
  begin
    tempSQL:=EmptyStr;
    ProgressBar2.MIN:=0;
    ProgressBar2.Max:=aString.Count;
    ProgressBar2.visible:=True;
    try
      for j := 0 to aString.Count-1 do
      begin
        if not DONextDetailProgressBar then
          Break;

        if AnsiCompareText(trim(aString[j]),'go')=0 then
        begin
          if tempSQL<>EmptyStr then
          begin
            Label1.Caption:=ABGetFilename(aFieldName,false)+' '+inttostr(j)+'/'+inttostr(aString.Count);
            ABExecSQL('Main',tempSQL,false);
            tempSQL:=EmptyStr;
          end;
        end
        else
        begin
          ABAddstr(tempSQL,aString[j],ABEnterWrapStr);
        end;
      end;

      if tempSQL<>EmptyStr then
      begin
        Label1.Caption:=ABGetFilename(aFieldName,false)+' '+inttostr(aString.Count)+'/'+inttostr(aString.Count);
        ABExecSQL('Main',tempSQL,false);
        tempSQL:=EmptyStr;
      end;
    finally
      ProgressBar2.Position:=0;
      ProgressBar2.visible:=False;
    end;
  end;

  procedure DoOtherFile(aType:string) ;
  var
    tempFileName,
    tempName,
    tempName_1,tempName_2,tempName_3,
    tempValue :String;
    i:LongInt;
  begin
    ABReadIniInStrings(aType,tempAllOtherFileNameStrings,tempCurOtherFileNameStrings);
    ProgressBar2.MIN:=0;
    ProgressBar2.Max:=tempCurOtherFileNameStrings.Count;
    ProgressBar2.visible:=True;
    try
      for i := 0 to tempCurOtherFileNameStrings.Count-1 do
      begin
        if not DONextDetailProgressBar then
          Break;

        tempName:=tempCurOtherFileNameStrings.Names[i];
        tempValue:=tempCurOtherFileNameStrings.ValueFromIndex[i];
        tempFileName:=ABTempPath+tempValue;
        if AnsiCompareText(aType,'Parameter')=0 then
        begin
          ABUpFileToField('Main','ABSys_Org_Parameter','Pa_ExpandValue',
                             'where Pa_Name='+ABQuotedStr(tempName),
                              tempFileName);
        end
        else if AnsiCompareText(aType,'Mail')=0 then
        begin
          ABUpFileToField('Main','ABSys_Org_Mail','MA_Content',
                             'where MA_Guid='+ABQuotedStr(tempName),
                              tempFileName);
        end
        else if AnsiCompareText(aType,'Mail=MF_File')=0 then
        begin
          ABUpFileToField('Main','ABSys_Org_MailFile','MF_File',
                             'where MF_Guid='+ABQuotedStr(tempName),
                              tempFileName);
        end
        else if AnsiCompareText(aType,'Func=FU_Pkg')=0 then
        begin
          ABUpFileToField('Main','ABSys_Org_Function','FU_Pkg',
                             'where Fu_FileName='+ABQuotedStr(tempName),
                              tempFileName);
        end
        else if AnsiCompareText(aType,'Func_FU=Bitmap')=0 then
        begin
          ABUpFileToField('Main','ABSys_Org_Function','FU_Bitmap',
                             'where Fu_FileName='+ABQuotedStr(tempName),
                              tempFileName);
        end
        else if AnsiCompareText(aType,'Func=ABSys_Org_FuncTemplate')=0 then
        begin
          tempName:=tempCurOtherFileNameStrings[i];

          tempName_1:=ABGetLeftRightStr(tempName,axdLeft);
          tempName:=ABGetLeftRightStr(tempName,axdRight);

          tempName_2:=ABGetLeftRightStr(tempName,axdLeft);
          tempName:=ABGetLeftRightStr(tempName,axdRight);

          tempName_3:=ABGetLeftRightStr(tempName,axdLeft);
          tempFileName:=ABTempPath+ABGetLeftRightStr(tempName,axdRight);

          ABUpTextToField( 'Main','ABSys_Org_FuncTemplate','Ft_FieldNameValue',
                           'where Ft_Fu_Guid=(select Fu_Guid '+
                           '                  from ABSys_Org_Function '+
                           '                   where Fu_FileName='+ABQuotedStr(tempName_1)+
                           '                  ) and '+
                           '      Ft_GroupName='+ABQuotedStr(tempName_2)+' and '+
                           '      Ft_Name='+ABQuotedStr(tempName_3),
                           ABReadTxt(tempFileName));
        end
        else if AnsiCompareText(aType,'Func_ABSys_Org_FuncReport')=0 then
        begin
          tempName:=tempCurOtherFileNameStrings[i];

          tempName_1:=ABGetLeftRightStr(tempName,axdLeft);
          tempName:=ABGetLeftRightStr(tempName,axdRight);

          tempName_2:=ABGetLeftRightStr(tempName,axdLeft);
          tempName:=ABGetLeftRightStr(tempName,axdRight);

          tempName_3:=ABGetLeftRightStr(tempName,axdLeft);
          tempFileName:=ABTempPath+ABGetLeftRightStr(tempName,axdRight);

          ABUpFileToField( 'Main','ABSys_Org_FuncReport','FR_Format',
                           'where Fr_Fu_Guid=(select Fu_Guid '+
                           '                  from ABSys_Org_Function '+
                           '                   where Fu_FileName='+ABQuotedStr(tempName_1)+
                           '                  ) and '+
                           '      Fr_GroupName='+ABQuotedStr(tempName_2)+' and '+
                           '      Fr_Name='+ABQuotedStr(tempName_3),
                           tempFileName);
        end
        else if AnsiCompareText(aType,'Func_ABSys_Org_FuncSQL')=0 then
        begin
          tempName:=tempCurOtherFileNameStrings[i];

          tempName_1:=ABGetLeftRightStr(tempName,axdLeft);
          tempName:=ABGetLeftRightStr(tempName,axdRight);

          tempName_2:=ABGetLeftRightStr(tempName,axdLeft);
          tempName:=ABGetLeftRightStr(tempName,axdRight);

          tempName_3:=ABGetLeftRightStr(tempName,axdLeft);
          tempFileName:=ABTempPath+ABGetLeftRightStr(tempName,axdRight);

          ABUpTextToField( 'Main','ABSys_Org_FuncSQL','FS_Sql',
                           'where FS_FU_Guid=(select Fu_Guid '+
                           '                  from ABSys_Org_Function '+
                           '                   where Fu_FileName='+ABQuotedStr(tempName_1)+
                           '                  ) and '+
                           '      FS_FormName='+ABQuotedStr(tempName_2)+' and '+
                           '      FS_Name='+ABQuotedStr(tempName_3),
                           ABReadTxt(tempFileName));
        end
        else if AnsiCompareText(aType,'ExtendReport')=0 then
        begin
          ABUpFileToField('Main','ABSys_Org_ExtendReport','Er_Format',
                             'where Er_Name='+ABQuotedStr(tempName),
                              tempFileName);
        end
        else if AnsiCompareText(aType,'ExtendReport_ABSys_Org_ExtendReportDatasets')=0 then
        begin
          tempName:=tempCurOtherFileNameStrings[i];

          tempName_1:=ABGetLeftRightStr(tempName,axdLeft);
          tempName:=ABGetLeftRightStr(tempName,axdRight);

          tempName_2:=ABGetLeftRightStr(tempName,axdLeft);
          tempFileName:=ABTempPath+ABGetLeftRightStr(tempName,axdRight);

          ABUpTextToField( 'Main','ABSys_Org_ExtendReportDatasets','ED_SQL',
                           'where ED_ER_Guid=(select ER_Guid '+
                           '                  from ABSys_Org_ExtendReport '+
                           '                   where Er_Name='+ABQuotedStr(tempName_1)+
                           '                  ) and '+
                           '      ED_Name='+ABQuotedStr(tempName_2),
                           ABReadTxt(tempFileName));
        end;
        Memo1.Lines.Add(tempFileName+' OK.');
      end;
    finally
      ProgressBar2.Position:=0;
      ProgressBar2.visible:=False;
    end;
   end;
begin
  tempFileName:=ABSelectFile('','','PAK FILES|*.pak');
  if ABCheckFileExists(tempFileName)  then
  begin
    tempString:=TStringList.Create;
    tempAllFileNameStrings:=TStringList.Create;
    tempAllSQLFileNameStrings:=TStringList.Create;
    tempAllOtherFileNameStrings:=TStringList.Create;
    tempAllOtherTypeStrings:=TStringList.Create;
    tempCurOtherFileNameStrings:=TStringList.Create;
    Memo1.Clear;
    SetButtonRun;
    Label1.Caption:=EmptyStr;
    try
      ABDeltree(ABTempPath,false);
      ABUnDoPak(tempFileName,ABTempPath,tempAllFileNameStrings);
      tempAllSQLFileNameStrings.LoadFromFile(ABTempPath+'SQLFileList.txt');
      tempAllOtherFileNameStrings.LoadFromFile(ABTempPath+'OtherFileList.txt');

      ProgressBar1.Max:=tempAllSQLFileNameStrings.Count+10;
      ProgressBar1.MIN:=0;
      ProgressBar1.visible:=True;
      try
        ABExecSQL('main','exec Proc_SetFieldChangeLog 0,'+QuotedStr(ABGetConnInfoByConnName('main').Conninfo.Database)+',''''');
        Memo1.Lines.Add('exec Proc_SetFieldChangeLog 0');
        if (N1.Checked) and (Memo13.Text<>EmptyStr) then
        begin
          Memo1.Lines.Add('初始化SQL Begin.');
          DoExecSQLs(Memo13.Lines,'初始化SQL');
          Memo1.Lines.Add('初始化SQL End.');
        end;

        for I := 0 to tempAllSQLFileNameStrings.Count-1 do
        begin
          if not DONextMainProgressBar then
            Break;

          tempStr:=ABTempPath+tempAllSQLFileNameStrings[i];
          tempString.LoadFromFile(tempStr);
          Memo1.Lines.Add(tempStr+' Begin.');
          DoExecSQLs(tempString,tempStr);
          Memo1.Lines.Add(tempStr+' End.');
        end;

        Label1.Caption:=EmptyStr;
        tempAllOtherTypeStrings.add('Parameter');
        tempAllOtherTypeStrings.add('Mail');
        tempAllOtherTypeStrings.add('Mail=MF_File');
        tempAllOtherTypeStrings.add('Func=FU_Pkg');
        tempAllOtherTypeStrings.add('Func=Bitmap');
        tempAllOtherTypeStrings.add('Func=ABSys_Org_FuncTemplate');
        tempAllOtherTypeStrings.add('Func=Func_ABSys_Org_FuncReport');
        tempAllOtherTypeStrings.add('Func=Func_ABSys_Org_FuncSQL');
        tempAllOtherTypeStrings.add('ExtendReport');
        tempAllOtherTypeStrings.add('ExtendReport=ABSys_Org_ExtendReportDatasets');

        for I := 0 to tempAllOtherTypeStrings.Count-1 do
        begin
          if not DONextMainProgressBar then
            Break;

          DoOtherFile(tempAllOtherTypeStrings[i]);
        end;

        ABExecSQL('main','exec Proc_SetFieldChangeLog 1,'+QuotedStr(ABGetConnInfoByConnName('main').Conninfo.Database)+',''''');
        Memo1.Lines.Add('exec Proc_SetFieldChangeLog 1');
        if Button2.Enabled then
          ABShow('导入成功');
      finally
        ProgressBar1.Position:=0;
        ProgressBar1.visible:=False;
      end;
    finally
      Label1.Caption:=EmptyStr;
      SetButtonStop;
      tempString.Free;
      tempAllFileNameStrings.Free;
      tempAllSQLFileNameStrings.Free;
      tempAllOtherFileNameStrings.Free;
      tempAllOtherTypeStrings.Free;
      tempCurOtherFileNameStrings.Free;
    end;
  end;
end;


Initialization
  RegisterClass(TABSys_Org_InputOutputForm);
Finalization
  UnRegisterClass(TABSys_Org_InputOutputForm);


end.




