object ABSys_Org_InputOutputForm: TABSys_Org_InputOutputForm
  Left = 457
  Top = 116
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  Caption = #23548#20837#23548#20986#31649#29702
  ClientHeight = 550
  ClientWidth = 750
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 0
    Top = 377
    Width = 750
    Height = 3
    Cursor = crVSplit
    Align = alTop
    ExplicitLeft = 797
    ExplicitTop = 0
    ExplicitWidth = 492
  end
  object Panel1: TPanel
    Left = 0
    Top = 509
    Width = 750
    Height = 41
    Align = alBottom
    TabOrder = 0
    DesignSize = (
      750
      41)
    object Label1: TLabel
      Left = 182
      Top = 13
      Width = 392
      Height = 20
      Anchors = [akLeft, akTop, akRight]
      AutoSize = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Button1: TABcxButton
      Left = 4
      Top = 8
      Width = 80
      Height = 25
      Caption = #23548#20837
      DropDownMenu = PopupMenu1
      Kind = cxbkDropDownButton
      LookAndFeel.Kind = lfFlat
      TabOrder = 0
      OnClick = Button1Click
      ShowProgressBar = False
    end
    object Button2: TABcxButton
      Left = 580
      Top = 8
      Width = 80
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #20572#27490
      LookAndFeel.Kind = lfFlat
      TabOrder = 1
      OnClick = Button2Click
      ShowProgressBar = False
    end
    object Button3: TABcxButton
      Left = 666
      Top = 8
      Width = 80
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #36864#20986
      LookAndFeel.Kind = lfFlat
      TabOrder = 2
      OnClick = Button3Click
      ShowProgressBar = False
    end
    object Button9: TABcxButton
      Left = 90
      Top = 8
      Width = 80
      Height = 25
      Caption = #23548#20986
      LookAndFeel.Kind = lfFlat
      TabOrder = 3
      OnClick = Button9Click
      ShowProgressBar = False
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 380
    Width = 750
    Height = 129
    Align = alClient
    TabOrder = 1
    object Memo1: TMemo
      Tag = 910
      Left = 1
      Top = 1
      Width = 748
      Height = 93
      Align = alClient
      ScrollBars = ssVertical
      TabOrder = 0
      OnKeyDown = Memo2KeyDown
    end
    object ProgressBar1: TProgressBar
      Left = 1
      Top = 111
      Width = 748
      Height = 17
      Align = alBottom
      TabOrder = 1
      Visible = False
    end
    object ProgressBar2: TProgressBar
      Left = 1
      Top = 94
      Width = 748
      Height = 17
      Align = alBottom
      TabOrder = 2
      Visible = False
    end
  end
  object PageControl2: TPageControl
    Left = 0
    Top = 0
    Width = 750
    Height = 377
    ActivePage = TabSheet2
    Align = alTop
    TabOrder = 2
    object TabSheet1: TTabSheet
      Caption = #23548#20837
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Label2: TLabel
        Left = 0
        Top = 0
        Width = 742
        Height = 13
        Align = alTop
        Alignment = taCenter
        Caption = #21021#22987#21270'SQL'
        ExplicitWidth = 57
      end
      object Memo13: TMemo
        Tag = 910
        Left = 0
        Top = 13
        Width = 742
        Height = 336
        Align = alClient
        Lines.Strings = (
          'truncate table ABSys_Log_CurLink'
          'truncate table ABSys_Log_CustomEvent'
          'truncate table ABSys_Log_ErrorInfo'
          'truncate table ABSys_Log_FieldChange'
          'truncate table ABSys_Log_Login'
          ''
          'truncate table ABSys_Approved_Message'
          'delete  ABSys_Approved_Rule'
          'delete  ABSys_Approved_ConditionParam'
          'delete  ABSys_Approved_PassRuleOperator'
          'delete  ABSys_Approved_Step'
          'delete  ABSys_Approved_StepOperator'
          ''
          'delete ABSys_Org_HistoryData'
          'delete ABSys_Org_Mail'
          'delete ABSys_Org_MailFile'
          'delete ABSys_Right_KeyCustomObject'
          'delete ABSys_Right_KeyExtendReport'
          'delete ABSys_Right_KeyField'
          'delete ABSys_Right_KeyFunction'
          'delete ABSys_Right_KeyTable'
          'delete ABSys_Right_KeyTableRow'
          'delete ABSys_Right_OperatorKey'
          'delete ABSys_Org_Key'
          'delete ABSys_Org_Operator'
          ''
          
            'delete  ABSys_Org_Field where Fi_Ta_Guid in (select Ta_Guid from' +
            ' ABSys_Org_Table where Ta_CL_Guid in (select CL_Guid from  ABSys' +
            '_Org_ConnList where Cl_Name<>'#39'Main'#39'))'
          
            'delete  ABSys_Org_Table where Ta_CL_Guid in (select CL_Guid from' +
            '  ABSys_Org_ConnList where Cl_Name<>'#39'Main'#39')'
          'delete  ABSys_Org_ConnList where Cl_Name<>'#39'Main'#39
          ''
          
            'delete ABSys_Org_ExtendReportDatasets where ED_ER_Guid in (selec' +
            't ER_Guid from ABSys_Org_ExtendReport where isnull(ER_SysUse,0)=' +
            '0  )'
          
            'delete ABSys_Org_ExtendReport         where isnull(ER_SysUse,0)=' +
            '0 '
          
            'delete ABSys_Org_FuncControlProperty  where isnull(FP_SysUse,0)=' +
            '0'
          
            'delete ABSys_Org_FuncReport           where isnull(FR_SysUse,0)=' +
            '0 '
          
            'delete ABSys_Org_FuncSQL              where isnull(FS_SysUse,0)=' +
            '0 '
          
            'delete ABSys_Org_FuncTemplate         where isnull(FT_SysUse,0)=' +
            '0  '
          
            'delete ABSys_Org_Function             where isnull(FU_SysUse,0)=' +
            '0 '
          ''
          
            'delete ABSys_Org_Parameter            where isnull(PA_Group,'#39#39')<' +
            '>'#39#26694#26550#20351#29992#39' '
          
            'delete ABSys_Org_TreeItem             where Ti_Tr_Guid in (selec' +
            't Tr_Guid from ABSys_Org_Tree where isnull(TR_Group,'#39#39')<>'#39#26694#26550#20351#29992#39' ' +
            ')'
          
            'delete ABSys_Org_Tree                 where isnull(TR_Group,'#39#39')<' +
            '>'#39#26694#26550#20351#29992#39' ')
        ScrollBars = ssBoth
        TabOrder = 0
        OnKeyDown = Memo2KeyDown
      end
    end
    object TabSheet2: TTabSheet
      Caption = #23548#20986
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object PageControl1: TPageControl
        Left = 0
        Top = 0
        Width = 742
        Height = 319
        ActivePage = TabSheet6
        Align = alClient
        TabOrder = 0
        object TabSheet6: TTabSheet
          Caption = #31995#32479#31649#29702
          ImageIndex = 5
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel20: TPanel
            Left = 0
            Top = 0
            Width = 110
            Height = 291
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 0
            object Panel21: TPanel
              Left = 0
              Top = 250
              Width = 110
              Height = 41
              Align = alBottom
              BevelOuter = bvNone
              TabOrder = 0
              DesignSize = (
                110
                41)
              object Button8: TABcxButton
                Left = 0
                Top = 6
                Width = 110
                Height = 25
                Anchors = [akLeft, akTop, akRight]
                Caption = #36873#25321#19979#25289#21015#39033
                LookAndFeel.Kind = lfFlat
                TabOrder = 0
                OnClick = Button8Click
                ShowProgressBar = False
              end
            end
            object Memo9: TMemo
              Tag = 910
              Left = 0
              Top = 0
              Width = 110
              Height = 250
              Align = alClient
              ScrollBars = ssBoth
              TabOrder = 1
              OnKeyDown = Memo2KeyDown
            end
          end
          object Panel10: TPanel
            Left = 110
            Top = 0
            Width = 110
            Height = 291
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 1
            object Panel11: TPanel
              Left = 0
              Top = 250
              Width = 110
              Height = 41
              Align = alBottom
              BevelOuter = bvNone
              TabOrder = 0
              DesignSize = (
                110
                41)
              object SpeedButton2: TABcxButton
                Left = 0
                Top = 6
                Width = 110
                Height = 25
                Anchors = [akLeft, akTop, akRight]
                Caption = #36873#25321#25968#25454#23383#20856
                LookAndFeel.Kind = lfFlat
                TabOrder = 0
                OnClick = SpeedButton2Click
                ShowProgressBar = False
              end
            end
            object Memo3: TMemo
              Tag = 910
              Left = 0
              Top = 0
              Width = 110
              Height = 250
              Align = alClient
              ScrollBars = ssBoth
              TabOrder = 1
              OnKeyDown = Memo2KeyDown
            end
          end
          object Panel2: TPanel
            Left = 440
            Top = 0
            Width = 130
            Height = 291
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 2
            object Panel6: TPanel
              Left = 0
              Top = 250
              Width = 130
              Height = 41
              Align = alBottom
              BevelOuter = bvNone
              TabOrder = 0
              DesignSize = (
                130
                41)
              object Button11: TABcxButton
                Left = 0
                Top = 6
                Width = 130
                Height = 25
                Anchors = [akLeft, akTop, akRight]
                Caption = #36873#25321#21382#21490#25968#25454#28165#38500#35774#32622
                LookAndFeel.Kind = lfFlat
                TabOrder = 0
                OnClick = Button11Click
                ShowProgressBar = False
              end
            end
            object Memo2: TMemo
              Tag = 910
              Left = 0
              Top = 0
              Width = 130
              Height = 250
              Align = alClient
              ScrollBars = ssBoth
              TabOrder = 1
              OnKeyDown = Memo2KeyDown
            end
          end
          object Panel22: TPanel
            Left = 220
            Top = 0
            Width = 110
            Height = 291
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 3
            object Panel23: TPanel
              Left = 0
              Top = 250
              Width = 110
              Height = 41
              Align = alBottom
              BevelOuter = bvNone
              TabOrder = 0
              DesignSize = (
                110
                41)
              object Button10: TABcxButton
                Left = 0
                Top = 6
                Width = 110
                Height = 25
                Anchors = [akLeft, akTop, akRight]
                Caption = #36873#25321#21442#25968
                LookAndFeel.Kind = lfFlat
                TabOrder = 0
                OnClick = Button10Click
                ShowProgressBar = False
              end
            end
            object Memo10: TMemo
              Tag = 910
              Left = 0
              Top = 0
              Width = 110
              Height = 250
              Align = alClient
              ScrollBars = ssBoth
              TabOrder = 1
              OnKeyDown = Memo2KeyDown
            end
          end
          object Panel7: TPanel
            Left = 330
            Top = 0
            Width = 110
            Height = 291
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 4
            object Panel8: TPanel
              Left = 0
              Top = 250
              Width = 110
              Height = 41
              Align = alBottom
              BevelOuter = bvNone
              TabOrder = 0
              DesignSize = (
                110
                41)
              object Button12: TABcxButton
                Left = 0
                Top = 6
                Width = 110
                Height = 25
                Anchors = [akLeft, akTop, akRight]
                Caption = #36873#25321#37038#20214
                LookAndFeel.Kind = lfFlat
                TabOrder = 0
                OnClick = Button12Click
                ShowProgressBar = False
              end
            end
            object Memo4: TMemo
              Tag = 910
              Left = 0
              Top = 0
              Width = 110
              Height = 250
              Align = alClient
              ScrollBars = ssBoth
              TabOrder = 1
              OnKeyDown = Memo2KeyDown
            end
          end
          object Panel25: TPanel
            Left = 570
            Top = 0
            Width = 121
            Height = 291
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 5
            object Panel26: TPanel
              Left = 0
              Top = 250
              Width = 121
              Height = 41
              Align = alBottom
              BevelOuter = bvNone
              TabOrder = 0
              DesignSize = (
                121
                41)
              object ABcxButton1: TABcxButton
                Left = 0
                Top = 6
                Width = 121
                Height = 25
                Anchors = [akLeft, akTop, akRight]
                Caption = #36873#25321#21333#25454#23548#20837#35774#32622
                LookAndFeel.Kind = lfFlat
                TabOrder = 0
                OnClick = ABcxButton1Click
                ShowProgressBar = False
              end
            end
            object Memo14: TMemo
              Tag = 910
              Left = 0
              Top = 0
              Width = 121
              Height = 250
              Align = alClient
              ScrollBars = ssBoth
              TabOrder = 1
              OnKeyDown = Memo2KeyDown
            end
          end
        end
        object TabSheet7: TTabSheet
          Caption = #21151#33021#31649#29702
          ImageIndex = 6
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel4: TPanel
            Left = 0
            Top = 0
            Width = 250
            Height = 291
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 0
            object Panel14: TPanel
              Left = 0
              Top = 250
              Width = 250
              Height = 41
              Align = alBottom
              BevelOuter = bvNone
              TabOrder = 0
              DesignSize = (
                250
                41)
              object Button5: TABcxButton
                Left = 0
                Top = 6
                Width = 250
                Height = 25
                Anchors = [akLeft, akTop, akRight]
                Cancel = True
                Caption = #36873#25321#21151#33021#33756#21333
                LookAndFeel.Kind = lfFlat
                TabOrder = 0
                OnClick = Button5Click
                ShowProgressBar = False
              end
            end
            object Memo5: TMemo
              Tag = 910
              Left = 0
              Top = 0
              Width = 250
              Height = 250
              Align = alClient
              ScrollBars = ssBoth
              TabOrder = 1
              OnKeyDown = Memo2KeyDown
            end
          end
        end
        object TabSheet3: TTabSheet
          Caption = #26435#38480#31649#29702
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel16: TPanel
            Left = 250
            Top = 0
            Width = 250
            Height = 291
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 0
            object Panel17: TPanel
              Left = 0
              Top = 250
              Width = 250
              Height = 41
              Align = alBottom
              BevelOuter = bvNone
              TabOrder = 0
              DesignSize = (
                250
                41)
              object Button6: TABcxButton
                Left = 0
                Top = 6
                Width = 250
                Height = 25
                Anchors = [akLeft, akTop, akRight]
                Caption = #36873#25321#25805#20316#21592
                LookAndFeel.Kind = lfFlat
                TabOrder = 0
                OnClick = Button6Click
                ShowProgressBar = False
              end
            end
            object Memo7: TMemo
              Tag = 910
              Left = 0
              Top = 0
              Width = 250
              Height = 250
              Align = alClient
              ScrollBars = ssBoth
              TabOrder = 1
              OnKeyDown = Memo2KeyDown
            end
          end
          object Panel5: TPanel
            Left = 0
            Top = 0
            Width = 250
            Height = 291
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 1
            object Panel15: TPanel
              Left = 0
              Top = 250
              Width = 250
              Height = 41
              Align = alBottom
              BevelOuter = bvNone
              TabOrder = 0
              DesignSize = (
                250
                41)
              object Button4: TABcxButton
                Left = 0
                Top = 6
                Width = 250
                Height = 25
                Anchors = [akLeft, akTop, akRight]
                Caption = #36873#25321#35282#33394
                LookAndFeel.Kind = lfFlat
                TabOrder = 0
                OnClick = Button4Click
                ShowProgressBar = False
              end
            end
            object Memo6: TMemo
              Tag = 910
              Left = 0
              Top = 0
              Width = 250
              Height = 250
              Align = alClient
              ScrollBars = ssBoth
              TabOrder = 1
              OnKeyDown = Memo2KeyDown
            end
          end
        end
        object TabSheet4: TTabSheet
          Caption = #23457#25209#31649#29702
          ImageIndex = 2
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel9: TPanel
            Left = 0
            Top = 0
            Width = 250
            Height = 291
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 0
            object Panel12: TPanel
              Left = 0
              Top = 250
              Width = 250
              Height = 41
              Align = alBottom
              BevelOuter = bvNone
              TabOrder = 0
              DesignSize = (
                250
                41)
              object Button13: TABcxButton
                Left = 0
                Top = 6
                Width = 250
                Height = 25
                Anchors = [akLeft, akTop, akRight]
                Caption = #36873#25321#23457#25209
                LookAndFeel.Kind = lfFlat
                TabOrder = 0
                OnClick = Button13Click
                ShowProgressBar = False
              end
            end
            object Memo11: TMemo
              Tag = 910
              Left = 0
              Top = 0
              Width = 250
              Height = 250
              Align = alClient
              ScrollBars = ssBoth
              TabOrder = 1
              OnKeyDown = Memo2KeyDown
            end
          end
        end
        object TabSheet5: TTabSheet
          Caption = #25253#34920#31649#29702
          ImageIndex = -1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel18: TPanel
            Left = 0
            Top = 0
            Width = 250
            Height = 291
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 0
            object Panel19: TPanel
              Left = 0
              Top = 250
              Width = 250
              Height = 41
              Align = alBottom
              BevelOuter = bvNone
              TabOrder = 0
              DesignSize = (
                250
                41)
              object Button7: TABcxButton
                Left = 0
                Top = 6
                Width = 250
                Height = 25
                Anchors = [akLeft, akTop, akRight]
                Caption = #36873#25321#25253#34920
                LookAndFeel.Kind = lfFlat
                TabOrder = 0
                OnClick = Button7Click
                ShowProgressBar = False
              end
            end
            object Memo8: TMemo
              Tag = 910
              Left = 0
              Top = 0
              Width = 250
              Height = 250
              Align = alClient
              ScrollBars = ssBoth
              TabOrder = 1
              OnKeyDown = Memo2KeyDown
            end
          end
        end
        object TabSheet8: TTabSheet
          Caption = #26085#24535#31649#29702
          ImageIndex = 3
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel13: TPanel
            Left = 0
            Top = 0
            Width = 250
            Height = 291
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 0
            object Panel24: TPanel
              Left = 0
              Top = 250
              Width = 250
              Height = 41
              Align = alBottom
              BevelOuter = bvNone
              TabOrder = 0
              DesignSize = (
                250
                41)
              object Button14: TABcxButton
                Left = 0
                Top = 6
                Width = 250
                Height = 25
                Anchors = [akLeft, akTop, akRight]
                Caption = #36873#25321#26085#24535
                Kind = cxbkDropDownButton
                LookAndFeel.Kind = lfFlat
                TabOrder = 0
                OnClick = Button14Click
                ShowProgressBar = False
              end
            end
            object Memo12: TMemo
              Tag = 910
              Left = 0
              Top = 0
              Width = 250
              Height = 250
              Align = alClient
              ScrollBars = ssBoth
              TabOrder = 1
              OnKeyDown = Memo2KeyDown
            end
          end
        end
        object TabSheet9: TTabSheet
          Caption = #20854#23427
          ImageIndex = 7
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
        end
      end
      object ABcxRadioGroup1: TABcxRadioGroup
        Left = 0
        Top = 319
        Align = alBottom
        Alignment = alCenterCenter
        Properties.Columns = 3
        Properties.DefaultValue = 0
        Properties.Items = <
          item
            Caption = #36873#25321#25152#26377#25968#25454
          end
          item
            Caption = #36873#25321#26694#26550#25968#25454
          end
          item
            Caption = #36873#25321#29992#25143#25968#25454
          end>
        ItemIndex = 0
        Style.LookAndFeel.NativeStyle = True
        StyleDisabled.LookAndFeel.NativeStyle = True
        StyleFocused.LookAndFeel.NativeStyle = True
        StyleHot.LookAndFeel.NativeStyle = True
        TabOrder = 1
        Height = 30
        Width = 742
      end
    end
  end
  object ABQuery_DatabaseTable: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    FilterOptions = [foCaseInsensitive]
    SQL.Strings = (
      
        'select  CL_Name,TA_Ti_Guid,CL_Name+'#39'='#39'+ta_Name  tempCode, CL_Nam' +
        'e+'#39'='#39'+Ta_Caption tempName'
      'from ABSys_Org_table'
      '     left join  ABSys_Org_ConnList  on  TA_CL_Guid=CL_Guid'
      'order by TA_Ti_Guid,TA_CL_Guid,Ta_Order'
      ''
      ' ')
    SqlUpdateDatetime = 42196.321867766200000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    EditControlType = ctSingle
    UseTrans = False
    UpdateDatabase = False
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'absys_Org_Table')
    IndexListDefs = <>
    LoadTables.Strings = (
      'absys_Org_Table')
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 136
    Top = 40
  end
  object ABQuery_Func: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    FilterOptions = [foCaseInsensitive]
    SQL.Strings = (
      'SELECT  FU_SysUse,Fu_Ti_Guid,Fu_Name,Fu_FileName'
      'FROM ABSys_Org_Function')
    SqlUpdateDatetime = 42196.321498472220000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    EditControlType = ctSingle
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ABSys_Org_Function')
    IndexListDefs = <>
    LoadTables.Strings = (
      'ABSys_Org_Function')
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 40
    Top = 104
  end
  object ABQuery_ExtendReport: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    FilterOptions = [foCaseInsensitive]
    SQL.Strings = (
      'select ER_SysUse,Er_Ti_Guid,Er_Guid,Er_Name '
      'from ABSys_Org_ExtendReport')
    SqlUpdateDatetime = 42196.321650023150000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    EditControlType = ctSingle
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ABSys_Org_ExtendReport')
    IndexListDefs = <>
    LoadTables.Strings = (
      'ABSys_Org_ExtendReport')
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 24
    Top = 272
  end
  object ABQuery_Operator: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    FilterOptions = [foCaseInsensitive]
    SQL.Strings = (
      'select Op_Ti_Guid,Op_Code,OP_Name '
      'from ABSys_Org_Operator'
      '')
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    EditControlType = ctSingle
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ABSys_Org_Operator')
    IndexListDefs = <>
    LoadTables.Strings = (
      'ABSys_Org_Operator')
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 112
    Top = 168
  end
  object ABQuery_Key: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    FilterOptions = [foCaseInsensitive]
    SQL.Strings = (
      'select Ke_Code,Ke_Name '
      'from ABSys_Org_Key'
      ' '
      '')
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    EditControlType = ctSingle
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ABSys_Org_Key')
    IndexListDefs = <>
    LoadTables.Strings = (
      'ABSys_Org_Key')
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 32
    Top = 160
  end
  object ABQuery_Tree: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    FilterOptions = [foCaseInsensitive]
    SQL.Strings = (
      'select Tr_SysUse,Tr_Code,Tr_Name '
      'from ABSys_Org_Tree')
    SqlUpdateDatetime = 42196.321203877310000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    EditControlType = ctSingle
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ABSys_Org_Tree')
    IndexListDefs = <>
    LoadTables.Strings = (
      'ABSys_Org_Tree')
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 32
    Top = 40
  end
  object ABQuery_Parameter: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    FilterOptions = [foCaseInsensitive]
    SQL.Strings = (
      'select  PA_SysUse,Pa_Name '
      'from ABSys_Org_Parameter')
    SqlUpdateDatetime = 42196.321350000000000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    EditControlType = ctSingle
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ABSys_Org_Parameter')
    IndexListDefs = <>
    LoadTables.Strings = (
      'ABSys_Org_Parameter')
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 240
    Top = 40
  end
  object ABQuery_Mail: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    FilterOptions = [foCaseInsensitive]
    SQL.Strings = (
      'select  '#39'ABSys_Org_Mail'#39' tempCode,'#39#25152#26377#37038#20214#39' tempName')
    SqlUpdateDatetime = 42190.649365543990000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    EditControlType = ctSingle
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    IndexListDefs = <>
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 344
    Top = 40
  end
  object ABQuery_HistoryData: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    FilterOptions = [foCaseInsensitive]
    SQL.Strings = (
      
        'select HD_SysUse,CL_Name+'#39'='#39'+ta_Name+'#39'='#39'+Fi_Name tempCode,CL_Nam' +
        'e+'#39'='#39'+ta_Caption+'#39'='#39'+Fi_Caption tempName'
      'from ABSys_Org_HistoryData '
      '     left join  ABSys_Org_table  on  Ta_Guid=HD_Ta_Guid'
      
        '     left join  ABSys_Org_Field  on  Fi_Ta_Guid=Ta_Guid and Fi_G' +
        'uid=HD_Fi_Guid'
      '     left join  ABSys_Org_ConnList  on  CL_Guid=Ta_CL_Guid')
    SqlUpdateDatetime = 42196.408848877310000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    EditControlType = ctSingle
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ABSys_Org_HistoryData')
    IndexListDefs = <>
    LoadTables.Strings = (
      'ABSys_Org_HistoryData')
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 448
    Top = 40
  end
  object ABQuery_Log: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    FilterOptions = [foCaseInsensitive]
    SQL.Strings = (
      ''
      'select * from '
      
        '(         select  '#39'ABSys_Log_Login'#39' tempCode,'#39#30331#24405#26085#24535#39' tempName,1 t' +
        'empOrder'
      'union select  '#39'ABSys_Log_CurLink'#39'  ,'#39#36830#25509#26085#24535#39'  ,2'
      'union select  '#39'ABSys_Log_ErrorInfo'#39'  ,'#39#24322#24120#26085#24535#39'  ,3'
      'union select  '#39'ABSys_Log_CustomEvent'#39'  ,'#39#33258#23450#20041#25805#20316#26085#24535#39',4  '
      'union select  '#39'ABSys_Log_FieldChange'#39'  ,'#39#25968#25454#20462#25913#26085#24535#39'   ,5'
      ') a'
      'order by tempOrder')
    SqlUpdateDatetime = 42196.293105277780000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    EditControlType = ctSingle
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    IndexListDefs = <>
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 24
    Top = 344
  end
  object ABQuery_Approved: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    FilterOptions = [foCaseInsensitive]
    SQL.Strings = (
      'select * from '
      '('
      
        '      select  '#39'ABSys_Approved_Bill'#39'  tempCode,'#39#23457#25209#21333#25454#34920#39'   tempName' +
        ',1 tempOrder'
      'union select  '#39'ABSys_Approved_Rule'#39' ,'#39#23457#25209#35268#21017#34920#39' ,2'
      'union select  '#39'ABSys_Approved_Message'#39'  ,'#39#23457#25209#28040#24687#34920#39'   ,3'
      ') a'
      'order by tempOrder')
    SqlUpdateDatetime = 42228.671122858800000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    EditControlType = ctSingle
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    IndexListDefs = <>
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 24
    Top = 216
  end
  object PopupMenu1: TPopupMenu
    Left = 364
    Top = 232
    object N1: TMenuItem
      Tag = 91
      AutoCheck = True
      Caption = #23548#20837#21069#25191#34892#21021#22987#21270'SQL'
    end
  end
  object ABQuery_BillInput: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    FilterOptions = [foCaseInsensitive]
    SQL.Strings = (
      'select Bi_Code,Bi_Name'
      'from ABSys_Org_BillInput')
    SqlUpdateDatetime = 42239.549158194440000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    EditControlType = ctSingle
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ABSys_Org_BillInput')
    IndexListDefs = <>
    LoadTables.Strings = (
      'ABSys_Org_BillInput')
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 536
    Top = 40
  end
end
