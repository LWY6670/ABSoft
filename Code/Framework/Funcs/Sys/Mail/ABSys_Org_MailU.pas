unit ABSys_Org_MailU;

interface

uses
  ABPubVarU,
  ABPubUserU,
  ABPubFuncU,
  ABPubConstU,
  ABPubDBU,
  ABPubPanelU,
  ABPubMessageU,

  ABThirdQueryU,
  ABThirdConnU,
  ABThirdDBU,
  ABThirdCustomQueryU,
  ABThirdFuncU,

  ABFrameworkcxGridU,
  ABFrameworkDBNavigatorU,
  ABFrameworkUserU,
  ABFrameworkFuncFormU,
  ABFrameworkControlU,
  ABFrameworkQueryU,
  ABFrameworkFuncU,
  ABFrameworkDictionaryQueryU,

  cxLookAndFeels,
  cxSplitter,
  cxRichEdit,
  cxMemo,
  cxButtons,
  cxLookAndFeelPainters,
  cxDropDownEdit,
  cxMaskEdit,
  cxTextEdit,
  cxContainer,
  cxGridDBBandedTableView,
  cxGridBandedTableView,
  cxGrid,
  cxGridDBTableView,
  cxGridTableView,
  cxGridCustomTableView,
  cxGridCustomView,
  cxClasses,
  cxGridLevel,
  cxDBData,
  cxEdit,
  cxDataStorage,
  cxData,
  cxFilter,
  cxGraphics,
  cxCustomData,
  cxStyles,
  cxDBNavigator,
  cxNavigator,
  cxControls,
  cxPC,
  dxNavBar,
  dxNavBarBase,
  dxNavBarCollns,
  dxNavBarStyles,
  dxBar,
  dxStatusBar,

  ABSys_Org_Mail_1U,Windows,Messages,SysUtils,Variants,Classes,Graphics,Controls,
  Forms,Dialogs,DB,DBClient,ExtCtrls,ComCtrls,StdCtrls,DBCtrls,ImgList,Provider,
  Grids,DBGrids,Menus,Buttons,ToolWin, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, dxBarExtItems;

type
  TABSys_Org_MailForm = class(TABFuncForm)
    ABDatasource1: TABDatasource;
    ABDBStatusBar1: TABdxDBStatusBar;
    Panel1: TPanel;
    ABDBcxGrid1: TABcxGrid;
    ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView;
    cxGridLevel1: TcxGridLevel;
    Splitter1: TABcxSplitter;
    Splitter2: TABcxSplitter;
    ABDatasource1_1: TABDatasource;
    ABQuery1_1: TABQuery;
    Panel2: TPanel;
    Splitter3: TABcxSplitter;
    Panel3: TPanel;
    ABDBcxGrid2: TABcxGrid;
    ABcxGridDBBandedTableView2: TABcxGridDBBandedTableView;
    cxGridLevel2: TcxGridLevel;
    Label1: TLabel;
    Panel4: TPanel;
    SpeedButton1: TABcxButton;
    SpeedButton2: TABcxButton;
    Label2: TLabel;
    ABQuery2_1: TABQuery;
    ABQuery2: TABQuery;
    ABDatasource2: TABDatasource;
    ABcxRichEdit1: TABcxRichEdit;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarButton1: TdxBarButton;
    dxBarButton2: TdxBarButton;
    dxBarButton3: TdxBarButton;
    dxBarButton4: TdxBarButton;
    dxBarButton5: TdxBarButton;
    dxBarButton6: TdxBarButton;
    dxBarButton7: TdxBarButton;
    dxBarButton8: TdxBarButton;
    dxBarButton9: TdxBarButton;
    dxBarButton10: TdxBarButton;
    dxBarButton11: TdxBarButton;
    Rpt_ToPageEdit: TdxBarEdit;
    dxbarComboBox1: TdxBarCombo;
    dxBarStatic1: TdxBarStatic;
    ABQuery1: TABQuery;
    mydxNavBar: TdxNavBar;
    Group1: TdxNavBarGroup;
    stBackground: TdxNavBarStyleItem;
    stGroup1Background: TdxNavBarStyleItem;
    stGroup2Background: TdxNavBarStyleItem;
    stGroup3Background: TdxNavBarStyleItem;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ABQuery1AfterScroll(DataSet: TDataSet);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure ABcxGridDBBandedTableView2CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure ABcxRichEdit1Exit(Sender: TObject);
    procedure dxBarButton1Click(Sender: TObject);
    procedure dxBarButton4Click(Sender: TObject);
    procedure dxBarButton5Click(Sender: TObject);
    procedure dxBarButton3Click(Sender: TObject);
    procedure dxBarButton6Click(Sender: TObject);
    procedure dxBarButton7Click(Sender: TObject);
    procedure dxBarButton2Click(Sender: TObject);
    procedure dxBarButton10Click(Sender: TObject);
    procedure dxbarComboBox1Change(Sender: TObject);
    procedure ABcxGridDBBandedTableView1DblClick(Sender: TObject);
    procedure dxbarComboBox1CurChange(Sender: TObject);
  private
    FMemoryStream: TMemoryStream;
    FLocalFile:string;
    FEditForm:TABSys_Org_Mail_1Form;
    procedure Send;
    procedure FilterMailClick(Sender: TObject);
    procedure UpdateABcxRichEdit1;
    { Private declarations }
  public
    function Save:boolean;
    { Public declarations }
  end;

var
  ABSys_Org_MailForm: TABSys_Org_MailForm;

implementation

{$R *.dfm}

procedure ABRegister(var aFormClassName:string);stdcall;
begin
  aFormClassName  :=TABSys_Org_MailForm.ClassName;
end;

exports                        
   ABRegister ;

procedure TABSys_Org_MailForm.FormCreate(Sender: TObject);
var
  i:LongInt;
begin
  FMemoryStream:=TMemoryStream.Create;
  FLocalFile:=ABTempPath+'~tempcurmail.txt';
  FEditForm:=TABSys_Org_Mail_1Form.Create(self);

  ABInitFormDataSet([],[ABcxGridDBBandedTableView1],[],ABQuery1);
  ABInitFormDataSet([],[ABcxGridDBBandedTableView2],[],ABQuery1_1);

  //加载邮箱类别
  mydxNavBar.Items.Clear;
  Group1.ClearLinks;

  mydxNavBar.BeginUpdate;
  try
    ABFillTreeItemByTr_Code('Mail Type',['Ti_Name'],dxbarComboBox1.Items);
    for I := 0 to dxbarComboBox1.Items.Count - 1 do
    begin
      ABAddNavBarGroupItem(mydxNavBar,Group1,dxbarComboBox1.Items[i],dxbarComboBox1.Items[i],FilterMailClick,false,-1);
    end;
    
    if Group1.LinkCount >0 then
    begin
      Group1.Links[0].Selected:=true;
      FilterMailClick(Group1.Links[0].Item);
    end;
  finally
    mydxNavBar.EndUpdate;
  end;
end;

//切换信箱
procedure TABSys_Org_MailForm.FilterMailClick(Sender: TObject);
begin
  ABcxRichEdit1Exit(ABcxRichEdit1);
  dxbarComboBox1.Text:= EmptyStr;
  ABQuery1.Filter:=
        'Ma_State='+ABQuotedStr(
          ABGetFieldValue(ABGetConstSqlPubDataset('ABSys_Org_TreeItem',['Mail Type']),['Ti_Name'],[TdxNavBarItem(Sender).Caption],['Ti_Code'],'')
          );

  ABQuery1AfterScroll(ABQuery1);
end;

procedure TABSys_Org_MailForm.ABQuery1AfterScroll(DataSet: TDataSet);
begin
  Panel2.Enabled:=not ABDatasetIsEmpty(DataSet);
  dxbarComboBox1.Enabled:=Panel2.Enabled;
  if (Panel2.Enabled) then
  begin
    if (trim(dxbarComboBox1.Text)=EmptyStr) then
      dxbarComboBox1.Text:=
        ABGetFieldValue(ABGetConstSqlPubDataset('ABSys_Org_TreeItem',['Mail Type']),
                                 ['Ti_Code'],[ABQuery1.FindField('Ma_State').AsString],['Ti_Name'],'');
  end;

  if not (Panel2.Enabled) then
  begin
    ABcxRichEdit1.text:=EmptyStr;
    ABcxRichEdit1.Hint:='';
    exit;
  end
  else if ABcxRichEdit1.Hint<>ABQuery1.FindField('Ma_Guid').AsString then
  begin
    UpdateABcxRichEdit1;
  end;
end;

procedure TABSys_Org_MailForm.UpdateABcxRichEdit1;
begin
  ABDownToStreamFromField( 'Main',
                           'ABSys_Org_Mail','Ma_Content',
                           'Ma_Guid='+ABQuotedStr(ABQuery1.FindField('Ma_Guid').AsString),
                           FMemoryStream);
  ABcxRichEdit1.Lines.LoadFromStream(FMemoryStream);

  ABcxRichEdit1.Hint:=ABQuery1.FindField('Ma_Guid').AsString;
end;

procedure TABSys_Org_MailForm.FormDestroy(Sender: TObject);
begin
  FMemoryStream.Free;
  FEditForm.Free;
end;

procedure TABSys_Org_MailForm.ABcxGridDBBandedTableView1DblClick(
  Sender: TObject);
begin
  dxBarButton5Click(dxBarButton5);
end;

procedure TABSys_Org_MailForm.ABcxGridDBBandedTableView2CellDblClick(
  Sender: TcxCustomGridTableView; ACellViewInfo: TcxGridTableDataCellViewInfo;
  AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
var
  tempStr1:string;
begin
  tempStr1:=ABSaveFile('',ABQuery1_1.FindField('Mf_FileName').AsString);
  if (tempStr1<>EmptyStr) and 
     (ABQuery1_1.Active) and
     (not ABDatasetIsEmpty(ABQuery1_1)) then
  begin
    ABDownToFileFromField('Main',
               'ABSys_Org_MailFile','Mf_File',
               'Mf_Guid='+ABQuotedStr(ABQuery1_1.FindField('Mf_Guid').AsString),
               tempStr1);
  end;
end;

//修改邮件
procedure TABSys_Org_MailForm.ABcxRichEdit1Exit(Sender: TObject);
begin
  if ABDatasetIsEmpty(ABQuery1) then
    exit;

  if ABcxRichEdit1.EditModified then
  begin
    ABcxRichEdit1.Lines.SaveToFile(FLocalFile);
    ABUpFileToField( 'Main',
                     'ABSys_Org_Mail','Ma_Content',
                     'Ma_Guid='+ABQuotedStr(ABQuery1.FindField('Ma_Guid').AsString),
                      FLocalFile);
  end;

end;

procedure TABSys_Org_MailForm.dxBarButton10Click(Sender: TObject);
begin
  close;
end;

//收邮件
procedure TABSys_Org_MailForm.dxBarButton1Click(Sender: TObject);
begin
  ABReFreshQuery(ABQuery1,[],True,false);
  if Group1.LinkCount >0 then
  begin
    Group1.Links[0].Selected:=true;
    FilterMailClick(Group1.Links[0].Item);
  end;
end;

procedure TABSys_Org_MailForm.dxBarButton2Click(Sender: TObject);
begin
  if (ABQuery1.Active) and (not ABDatasetIsEmpty(ABQuery1)) then
  begin
    if ABQuery1.FindField('Ma_State').AsString='Recycle' then
    begin
      ABQuery1.Delete;
    end
    else
    begin
      ABQuery1.Edit;
      ABQuery1.FindField('Ma_State').AsString:='Recycle';
      ABQuery1.Post;
    end;
    ABQuery1.Filtered := false;
    ABQuery1.Filtered := true;
  end;
end;

procedure TABSys_Org_MailForm.dxBarButton3Click(Sender: TObject);
begin
  Send;
end;

procedure TABSys_Org_MailForm.dxBarButton4Click(Sender: TObject);
var
  tempStr1:string;
begin
  FEditForm.Caption:=('写邮件');
  FEditForm.Ma_Guid:='';

  FEditForm.ABcxButtonEdit1.Text:='';
  FEditForm.cxMemo1.Lines.Clear;
  FEditForm.Editor.Lines.Clear;
  if FEditForm.ShowModal=mrOk then
  begin
    tempStr1:=ABQuery1.Filter;
    ABQuery1.DisableControls;
    ABSetDatasetFilter(ABQuery1,EmptyStr);
    try
      if Save then
        Send;
    finally
      ABQuery1.Filter:=tempStr1;
      ABQuery1.EnableControls;
    end;
  end;
end;

procedure TABSys_Org_MailForm.dxBarButton5Click(Sender: TObject);
begin
  FEditForm.Caption:=('修改邮件');
  FEditForm.cxMemo1.Text:=ABQuery1.FindField('Ma_Title').AsString;
  ABDownToStreamFromField( 'Main',
                           'ABSys_Org_Mail','Ma_Content',
                           'Ma_Guid='+ABQuotedStr(ABQuery1.FindField('Ma_Guid').AsString),
                           FMemoryStream);
  FEditForm.Editor.Lines.LoadFromStream(FMemoryStream);
  FEditForm.ABcxButtonEdit1.Text:=ABQuery1.FindField('Ma_ReceiverOperator').AsString;
  FEditForm.dxBarCombo1.Text:=
        ABGetFieldValue(ABGetConstSqlPubDataset('ABSys_Org_TreeItem',['PRI']),
                                 ['Ti_Code'],[ABQuery1.FindField('Ma_Grade').AsString],['Ti_Name'],'');
  if FEditForm.dxBarCombo1.Text=EmptyStr then
     FEditForm.dxBarCombo1.ItemIndex:=0;

  FEditForm.Ma_Guid:=ABQuery1.FindField('Ma_Guid').AsString;
  if FEditForm.ShowModal=mrOk then
  begin
    ABQuery1.edit;
    if Save then
      Send;
    Send;
  end;
end;

procedure TABSys_Org_MailForm.dxBarButton6Click(Sender: TObject);
begin
  FEditForm.Caption:=('回复邮件');

  FEditForm.cxMemo1.Text:=('回复:')+ABQuery1.FindField('Ma_Title').AsString;
  ABDownToStreamFromField('Main',
                 'ABSys_Org_Mail','Ma_Content',
                 'Ma_Guid='+ABQuotedStr(ABQuery1.FindField('Ma_Guid').AsString),
                 FMemoryStream);
  FEditForm.Editor.Lines.LoadFromStream(FMemoryStream);
  FEditForm.ABcxButtonEdit1.Text:=ABQuery1.FindField('Ma_SendOperator').AsString;
  FEditForm.dxBarCombo1.Text:=
        ABGetFieldValue(ABGetConstSqlPubDataset('ABSys_Org_TreeItem',['PRI']),
                                 ['Ti_Code'],[ABQuery1.FindField('Ma_Grade').AsString],['Ti_Name'],'');

  FEditForm.Ma_Guid:='';
  if FEditForm.ShowModal=mrOk then
  begin
    if Save then
      Send;
    Send;
  end;
end;

procedure TABSys_Org_MailForm.dxBarButton7Click(Sender: TObject);
begin
  FEditForm.Caption:=('转发邮件');
  FEditForm.cxMemo1.Text:=('转发:')+ABQuery1.FindField('Ma_Title').AsString;
  ABDownToStreamFromField('Main',
                 'ABSys_Org_Mail','Ma_Content',
                 'Ma_Guid='+ABQuotedStr(ABQuery1.FindField('Ma_Guid').AsString),
                 FMemoryStream);
  FEditForm.Editor.Lines.LoadFromStream(FMemoryStream);
  FEditForm.ABcxButtonEdit1.Text:='';
  FEditForm.dxBarCombo1.Text:=
        ABGetFieldValue(ABGetConstSqlPubDataset('ABSys_Org_TreeItem',['PRI']),
                                 ['Ti_Code'],[ABQuery1.FindField('Ma_Grade').AsString],['Ti_Name'],'');

  FEditForm.Ma_Guid:='';
  if FEditForm.ShowModal=mrOk then
  begin
    if Save then
      Send;
    Send;
  end;
end;

//转移邮件箱
procedure TABSys_Org_MailForm.dxbarComboBox1Change(Sender: TObject);
begin
  if (dxbarComboBox1.Text<>EmptyStr) and
     (ABQuery1.Active) and
     (not ABDatasetIsEmpty(ABQuery1)) and
     (AnsiCompareText(dxbarComboBox1.Text,
        ABGetFieldValue(ABGetConstSqlPubDataset('ABSys_Org_TreeItem',['Mail Type']),
                                 ['Ti_Code'],[ABQuery1.FindField('Ma_State').AsString],['Ti_Name'],'')
                      )<>0) then
  begin
    ABQuery1.Edit;
    ABQuery1.FindField('Ma_State').AsString:=
        ABGetFieldValue(ABGetConstSqlPubDataset('ABSys_Org_TreeItem',['Mail Type']),
                                 ['Ti_Name'],[dxbarComboBox1.Text],['Ti_Code'],'');
    ABQuery1.Post;
    dxbarComboBox1.Text:=
        ABGetFieldValue(ABGetConstSqlPubDataset('ABSys_Org_TreeItem',['Mail Type']),
                                 ['Ti_Code'],[ABQuery1.FindField('Ma_State').AsString],['Ti_Name'],'');
    ABQuery1AfterScroll(ABQuery1);
  end;
end;

procedure TABSys_Org_MailForm.dxbarComboBox1CurChange(Sender: TObject);
begin

end;

//发送邮件
procedure TABSys_Org_MailForm.Send;
var
  tempStr1,tempStr2:string;
  i:LongInt;
  tempSendTime:TDateTime;
begin
  if (ABQuery1.Active) and
     (not ABDatasetIsEmpty(ABQuery1)) then
  begin
    ABQuery2.Close;
    ABQuery2.Open;
    ABQuery2_1.Close;
    ABQuery2_1.Open;
    tempSendTime:=ABGetServerDateTime;
    ABQuery1_1.DisableControls;
    try
      //新增发送的邮件主表数据
      tempStr1:=ABQuery1.FindField('Ma_ReceiverOperator').AsString;
      for I := 1 to  ABGetSpaceStrCount(tempStr1,',') do
      begin
        tempStr2:=trim(ABGetSpaceStr(tempStr1,i,','));
        ABQuery2.Append;
        ABQuery2.FindField('Ma_Title'       ).AsString  :=ABQuery1.FindField('Ma_Title').AsString;
        ABQuery2.FindField('Ma_State'       ).AsString  :='Inbox';
        ABQuery2.FindField('Ma_Grade'       ).AsString  :=ABQuery1.FindField('Ma_Grade').AsString;
        ABQuery2.FindField('Ma_SendOperator').AsString:=ABQuery1.FindField('Ma_SendOperator').AsString;
        ABQuery2.FindField('Ma_SendDatetime').AsDateTime:=tempSendTime;
        
        ABQuery2.FindField('Ma_ReceiverOperator').AsString:=ABQuery1.FindField('Ma_ReceiverOperator').AsString;
        ABQuery2.FindField('Ma_OwnerOperator').AsString:=tempStr2;
        
        ABQuery2.Post;
        ABDownToStreamFromField('Main',
                       'ABSys_Org_Mail','Ma_Content',
                       'Ma_Guid='+ABQuotedStr(ABQuery1.FindField('Ma_Guid').AsString),
                       FMemoryStream);
        ABUpStreamToField(   'Main',
                       'ABSys_Org_Mail','Ma_Content',
                       'Ma_Guid='+ABQuotedStr(ABQuery2.FindField('Ma_Guid').AsString),
                       FMemoryStream);

        ABQuery1_1.First;
        while not ABQuery1_1.Eof do
        begin
          ABQuery2_1.Append;
          ABQuery2_1.FindField('Mf_FileName').AsString  :=ABQuery1_1.FindField('Mf_FileName').AsString;
          ABQuery2_1.Post;
          ABDownToStreamFromField('Main',
                     'ABSys_Org_MailFile','Mf_File',
                     'Mf_Guid='+ABQuotedStr(ABQuery1_1.FindField('Mf_Guid').AsString),
                     FMemoryStream);
          ABUpStreamToField('Main',
                     'ABSys_Org_MailFile','Mf_File',
                     'Mf_Guid='+ABQuotedStr(ABQuery2_1.FindField('Mf_Guid').AsString),
                     FMemoryStream);

          ABQuery1_1.Next;
        end;
      end;

      ABSetFieldValue(tempSendTime,ABQuery1.FindField('Ma_SendDatetime'));
      ABSetFieldValue('Addresser',ABQuery1.FindField('Ma_State'));
      ABPostDataset(ABQuery1);
      ABShow('邮件发送成功.');
      ABReFreshQuery(ABQuery1,[]);
      ABReFreshQuery(ABQuery1_1,[]);
    finally
      ABQuery1_1.EnableControls;
      ABQuery2.Close;
      ABQuery2_1.Close;

    end;
  end;
end;

//增加附件
procedure TABSys_Org_MailForm.SpeedButton1Click(Sender: TObject);
var
  tempStr1:string;
begin
  tempStr1:=ABSelectFile;
  if ABCheckFileExists(tempStr1) then
  begin
    ABQuery1_1.Append;
    ABQuery1_1.FindField('Mf_FileName').AsString:=ABGetFilename(tempStr1,false);
    ABQuery1_1.Post;
    ABUpFileToField('Main',
               'ABSys_Org_MailFile','Mf_File',
               'Mf_Guid='+ABQuotedStr(ABQuery1_1.FindField('Mf_Guid').AsString),
               tempStr1);
  end;
end;

//删除附件                                                      
procedure TABSys_Org_MailForm.SpeedButton2Click(Sender: TObject);
begin
  if (ABQuery1_1.Active) and
     (not ABDatasetIsEmpty(ABQuery1_1) ) then
  begin
    ABQuery1_1.Delete;
  end;
end;

//保存邮件
function TABSys_Org_MailForm.Save:boolean;
var
  tempMa_Guid:string;
begin
  result:=false;
  ABDoBusy;
  try
    if FEditForm.Ma_Guid=EmptyStr then
    begin
      ABQuery1.Append;
    end
    else
    begin
      if (AnsiCompareText(FEditForm.Ma_Guid,ABQuery1.FindField('Ma_Guid').AsString)=0) or
         (ABQuery1.Locate('Ma_Guid',FEditForm.Ma_Guid,[])) then
        ABQuery1.Edit
      else
        exit;
    end;

    try
      tempMa_Guid:=ABQuery1.FindField('Ma_Guid').AsString;
      ABQuery1.FindField('Ma_Title').AsString:=FEditForm.cxMemo1.Text;
      ABQuery1.FindField('Ma_Grade').AsString:=
        ABGetFieldValue(ABGetConstSqlPubDataset('ABSys_Org_TreeItem',['PRI']),
                                 ['Ti_Name'],[FEditForm.dxBarCombo1.Text],['Ti_Code'],'');

      ABQuery1.FindField('Ma_ReceiverOperator').AsString:=FEditForm.ABcxButtonEdit1.Text;
      ABQuery1.Post;

      FEditForm.Editor.Lines.SaveToFile(FLocalFile);
      ABUpFileToField( 'Main',
                       'ABSys_Org_Mail','Ma_Content',
                       'Ma_Guid='+ABQuotedStr(tempMa_Guid),
                       FLocalFile);

      FEditForm.Ma_Guid:=tempMa_Guid;
      UpdateABcxRichEdit1;
      result:=true;
    except
      if ABQuery1.State in [dsEdit,dsInsert] then
        ABQuery1.Cancel;
      raise;
    end;
  finally
    ABDoBusy(false);
  end;
end;

Initialization
  RegisterClass(TABSys_Org_MailForm);

Finalization
  UnRegisterClass(TABSys_Org_MailForm);

end.


