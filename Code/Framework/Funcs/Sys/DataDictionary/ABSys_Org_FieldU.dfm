object ABSys_Org_FieldForm: TABSys_Org_FieldForm
  Left = 173
  Top = 129
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  Caption = #25968#25454#23383#20856#31649#29702
  ClientHeight = 550
  ClientWidth = 750
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter2: TABcxSplitter
    Left = 180
    Top = 0
    Width = 8
    Height = 550
    HotZoneClassName = 'TcxMediaPlayer8Style'
    InvertDirection = True
    Control = Panel1
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 180
    Height = 550
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 0
    object Splitter1: TABcxSplitter
      Left = 0
      Top = 137
      Width = 180
      Height = 8
      Cursor = crVSplit
      HotZoneClassName = 'TcxMediaPlayer8Style'
      AlignSplitter = salTop
      InvertDirection = True
      Control = ABcxDBTreeView1
    end
    object ABcxDBTreeView1: TABcxDBTreeView
      Left = 0
      Top = 0
      Width = 180
      Height = 137
      Align = alTop
      Bands = <
        item
        end>
      DataController.DataSource = ABDatasource_TreeItem
      DataController.ParentField = 'Ti_ParentGuid'
      DataController.KeyField = 'Ti_Guid'
      DragMode = dmAutomatic
      Navigator.Buttons.CustomButtons = <>
      OptionsBehavior.ExpandOnIncSearch = True
      OptionsBehavior.IncSearch = True
      OptionsBehavior.IncSearchItem = ABcxDBTreeView1cxDBTreeListColumn1
      OptionsData.Editing = False
      OptionsData.Deleting = False
      OptionsSelection.CellSelect = False
      OptionsSelection.HideFocusRect = False
      OptionsView.ColumnAutoWidth = True
      OptionsView.Headers = False
      PopupMenu.AutoHotkeys = maManual
      PopupMenu.DefaultParentValue = '0'
      RootValue = -1
      TabOrder = 0
      OnEnter = ABcxDBTreeView1Enter
      Active = True
      ExtFullExpand = False
      CanSelectParent = True
      object ABcxDBTreeView1cxDBTreeListColumn1: TcxDBTreeListColumn
        DataBinding.FieldName = 'TI_Name'
        Position.ColIndex = 0
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object ABcxDBTreeView1cxDBTreeListColumn2: TcxDBTreeListColumn
        Visible = False
        DataBinding.FieldName = 'TI_Order'
        Position.ColIndex = 1
        Position.RowIndex = 0
        Position.BandIndex = 0
        SortOrder = soAscending
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
    end
    object ABcxSplitter1: TABcxSplitter
      Left = 0
      Top = 313
      Width = 180
      Height = 8
      Cursor = crVSplit
      HotZoneClassName = 'TcxMediaPlayer8Style'
      AlignSplitter = salTop
      InvertDirection = True
      Control = ABcxGrid1
    end
    object ABcxGrid1: TABcxGrid
      Left = 0
      Top = 145
      Width = 180
      Height = 168
      Align = alTop
      TabOrder = 3
      OnEnter = ABcxGrid1Enter
      LookAndFeel.Kind = lfFlat
      LookAndFeel.NativeStyle = False
      object ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView
        PopupMenu.AutoHotkeys = maManual
        PopupMenu.CloseFootStr = False
        PopupMenu.LinkTableView = ABcxGridDBBandedTableView1
        PopupMenu.AutoApplyBestFit = True
        PopupMenu.AutoCreateAllItem = True
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = ABDatasource_Table
        DataController.Filter.Options = [fcoCaseInsensitive]
        DataController.Filter.AutoDataSetFilter = True
        DataController.Filter.TranslateBetween = True
        DataController.Filter.TranslateIn = True
        DataController.Filter.TranslateLike = True
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsBehavior.AlwaysShowEditor = True
        OptionsBehavior.FocusCellOnTab = True
        OptionsBehavior.GoToNextCellOnEnter = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsCustomize.DataRowSizing = True
        OptionsData.Deleting = False
        OptionsData.Inserting = False
        OptionsSelection.HideFocusRectOnExit = False
        OptionsSelection.MultiSelect = True
        OptionsView.CellAutoHeight = True
        OptionsView.GroupByBox = False
        OptionsView.Indicator = True
        OptionsView.BandHeaders = False
        Bands = <
          item
          end>
        ExtPopupMenu.AutoHotkeys = maManual
        ExtPopupMenu.CloseFootStr = False
        ExtPopupMenu.LinkTableView = ABcxGridDBBandedTableView1
        ExtPopupMenu.AutoApplyBestFit = True
        ExtPopupMenu.AutoCreateAllItem = True
      end
      object cxGridLevel2: TcxGridLevel
        GridView = ABcxGridDBBandedTableView1
      end
    end
    object ABcxGrid2: TABcxGrid
      Left = 0
      Top = 321
      Width = 180
      Height = 229
      Align = alClient
      TabOrder = 4
      OnEnter = ABcxGrid2Enter
      LookAndFeel.Kind = lfFlat
      LookAndFeel.NativeStyle = False
      object ABcxGridDBBandedTableView2: TABcxGridDBBandedTableView
        PopupMenu.AutoHotkeys = maManual
        PopupMenu.CloseFootStr = False
        PopupMenu.LinkTableView = ABcxGridDBBandedTableView2
        PopupMenu.AutoApplyBestFit = True
        PopupMenu.AutoCreateAllItem = True
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = ABDatasource_Field
        DataController.Filter.Options = [fcoCaseInsensitive]
        DataController.Filter.AutoDataSetFilter = True
        DataController.Filter.TranslateBetween = True
        DataController.Filter.TranslateIn = True
        DataController.Filter.TranslateLike = True
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsBehavior.AlwaysShowEditor = True
        OptionsBehavior.FocusCellOnTab = True
        OptionsBehavior.GoToNextCellOnEnter = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsCustomize.DataRowSizing = True
        OptionsData.Deleting = False
        OptionsData.Inserting = False
        OptionsSelection.HideFocusRectOnExit = False
        OptionsSelection.MultiSelect = True
        OptionsView.CellAutoHeight = True
        OptionsView.GroupByBox = False
        OptionsView.Indicator = True
        OptionsView.BandHeaders = False
        Bands = <
          item
          end>
        ExtPopupMenu.AutoHotkeys = maManual
        ExtPopupMenu.CloseFootStr = False
        ExtPopupMenu.LinkTableView = ABcxGridDBBandedTableView2
        ExtPopupMenu.AutoApplyBestFit = True
        ExtPopupMenu.AutoCreateAllItem = True
      end
      object cxGridLevel1: TcxGridLevel
        GridView = ABcxGridDBBandedTableView2
      end
    end
  end
  object Panel5: TPanel
    Left = 188
    Top = 0
    Width = 562
    Height = 550
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object ABcxPageControl1: TABcxPageControl
      Left = 0
      Top = 0
      Width = 562
      Height = 513
      Align = alClient
      ParentShowHint = False
      ShowHint = False
      TabOrder = 0
      Properties.ActivePage = cxTabSheet5
      Properties.CustomButtons.Buttons = <>
      Properties.HideTabs = True
      Properties.TabSlants.Kind = skCutCorner
      Properties.TabSlants.Positions = []
      LookAndFeel.Kind = lfFlat
      LookAndFeel.NativeStyle = False
      TabSlants.Kind = skCutCorner
      TabSlants.Positions = []
      ActivePageIndex = 0
      ClientRectBottom = 512
      ClientRectLeft = 1
      ClientRectRight = 561
      ClientRectTop = 1
      object cxTabSheet5: TcxTabSheet
        Caption = 'cxTabSheet5'
        ImageIndex = 2
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
      end
      object tempTable: TcxTabSheet
        Caption = 'tempTable'
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object ABDBPanel2: TABDBPanel
          Left = 0
          Top = 0
          Width = 560
          Height = 511
          Align = alClient
          BevelOuter = bvNone
          ShowCaption = False
          TabOrder = 0
          ReadOnly = False
          DataSource = ABDatasource_Table
          AddAnchors_akRight = True
          AddAnchors_akBottom = True
          AutoHeight = True
          AutoWidth = True
        end
      end
      object tempField: TcxTabSheet
        Caption = 'tempField'
        ImageIndex = 2
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object ABcxPageControl2: TABcxPageControl
          Left = 0
          Top = 92
          Width = 560
          Height = 419
          Align = alClient
          TabOrder = 1
          Properties.ActivePage = cxTabSheet2
          Properties.CustomButtons.Buttons = <>
          LookAndFeel.Kind = lfFlat
          LookAndFeel.NativeStyle = False
          ActivePageIndex = 0
          ClientRectBottom = 418
          ClientRectLeft = 1
          ClientRectRight = 559
          ClientRectTop = 21
          object cxTabSheet2: TcxTabSheet
            Caption = #19979#25289#26694#20449#24687
            ImageIndex = 1
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object ABDBPanel5: TABDBPanel
              Left = 0
              Top = 0
              Width = 558
              Height = 397
              Align = alClient
              BevelOuter = bvNone
              ShowCaption = False
              TabOrder = 0
              ReadOnly = False
              DataSource = ABDatasource_Field
              AddAnchors_akRight = True
              AddAnchors_akBottom = True
              AutoHeight = True
              AutoWidth = True
            end
          end
          object cxTabSheet1: TcxTabSheet
            Caption = #25511#21046#20449#24687
            ImageIndex = 0
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object ABDBPanel4: TABDBPanel
              Left = 0
              Top = 0
              Width = 558
              Height = 397
              Align = alClient
              BevelOuter = bvNone
              ShowCaption = False
              TabOrder = 0
              ReadOnly = False
              DataSource = ABDatasource_Field
              AddAnchors_akRight = True
              AddAnchors_akBottom = True
              AutoHeight = True
              AutoWidth = True
            end
          end
          object cxTabSheet3: TcxTabSheet
            Caption = #20301#32622#20449#24687
            ImageIndex = 2
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object ABDBPanel6: TABDBPanel
              Left = 0
              Top = 0
              Width = 558
              Height = 397
              Align = alClient
              BevelOuter = bvNone
              ShowCaption = False
              TabOrder = 0
              ReadOnly = False
              DataSource = ABDatasource_Field
              AddAnchors_akRight = True
              AddAnchors_akBottom = True
              AutoHeight = True
              AutoWidth = True
            end
          end
          object cxTabSheet4: TcxTabSheet
            Caption = #20854#23427#20449#24687
            ImageIndex = 3
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object ABDBPanel7: TABDBPanel
              Left = 0
              Top = 0
              Width = 558
              Height = 397
              Align = alClient
              BevelOuter = bvNone
              ShowCaption = False
              TabOrder = 0
              ReadOnly = False
              DataSource = ABDatasource_Field
              AddAnchors_akRight = True
              AddAnchors_akBottom = True
              AutoHeight = True
              AutoWidth = True
            end
          end
        end
        object ABcxGroupBox1: TABcxGroupBox
          AlignWithMargins = True
          Left = 3
          Top = 3
          Align = alTop
          Caption = #23383#27573#22522#26412#20449#24687
          LookAndFeel.Kind = lfFlat
          ParentFont = False
          Style.BorderStyle = ebsUltraFlat
          Style.Font.Charset = ANSI_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'MS Sans Serif'
          Style.Font.Style = []
          Style.LookAndFeel.Kind = lfOffice11
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = []
          Style.IsFontAssigned = True
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.NativeStyle = False
          TabOrder = 0
          Height = 86
          Width = 554
          object ABDBPanel3: TABDBPanel
            Left = 2
            Top = 18
            Width = 550
            Height = 66
            Align = alClient
            BevelOuter = bvNone
            ShowCaption = False
            TabOrder = 0
            ReadOnly = False
            DataSource = ABDatasource_Field
            AddAnchors_akRight = True
            AddAnchors_akBottom = True
            AutoHeight = True
            AutoWidth = True
          end
        end
      end
    end
    object Panel2: TPanel
      Left = 0
      Top = 513
      Width = 562
      Height = 37
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 1
      DesignSize = (
        562
        37)
      object Button3: TABcxButton
        Left = 481
        Top = 6
        Width = 75
        Height = 25
        Anchors = [akTop, akRight]
        Caption = #36864#20986
        LookAndFeel.Kind = lfFlat
        TabOrder = 0
        OnClick = Button3Click
        ShowProgressBar = False
      end
      object Button1: TABcxButton
        Left = 315
        Top = 6
        Width = 86
        Height = 25
        Hint = #34920#32467#26500#21464#21160#26102#25191#34892
        Caption = #20854#23427#21151#33021
        DropDownMenu = PopupMenu1
        Kind = cxbkOfficeDropDown
        LookAndFeel.Kind = lfFlat
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        ShowProgressBar = False
      end
      object ABcxButton3: TABcxButton
        Left = 3
        Top = 6
        Width = 150
        Height = 25
        Caption = #21047#26032#24403#21069#36830#25509#24211#25968#25454#23383#20856
        LookAndFeel.Kind = lfFlat
        TabOrder = 2
        OnClick = ABcxButton3Click
        ShowProgressBar = False
      end
      object ABcxButton4: TABcxButton
        Left = 159
        Top = 6
        Width = 150
        Height = 25
        Caption = #21047#26032#25152#26377#36830#25509#25968#25454#23383#20856
        LookAndFeel.Kind = lfFlat
        TabOrder = 3
        OnClick = ABcxButton4Click
        ShowProgressBar = False
      end
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 214
    Top = 456
    object N8: TMenuItem
      Caption = #19979#25289#39033#30446#31649#29702
      OnClick = N8Click
    end
    object N9: TMenuItem
      Caption = #21047#26032#25968#25454#23383#20856#32531#23384
      OnClick = N9Click
    end
    object N10: TMenuItem
      Caption = '-'
    end
    object N2: TMenuItem
      Caption = #21516#27493#24403#21069#34920#12289#35270#22270#12289#23383#27573#26631#39064#21040#25968#25454#24211#35828#26126
      OnClick = N2Click
    end
    object N1: TMenuItem
      Caption = #21516#27493#24403#21069#20998#31867#19979#34920#12289#35270#22270#12289#23383#27573#26631#39064#21040#25968#25454#24211#35828#26126
      OnClick = N1Click
    end
    object N4: TMenuItem
      Caption = #21516#27493#25152#26377#34920#12289#35270#22270#12289#23383#27573#26631#39064#21040#25968#25454#24211#35828#26126
      OnClick = N4Click
    end
    object N7: TMenuItem
      Caption = '-'
    end
    object N3: TMenuItem
      Caption = #21516#27493#25968#25454#24211#35828#26126#21040#24403#21069#34920#12289#35270#22270#12289#23383#27573#26631#39064
      OnClick = N3Click
    end
    object N6: TMenuItem
      Caption = #21516#27493#25968#25454#24211#35828#26126#21040#24403#21069#20998#31867#19979#34920#12289#35270#22270#12289#23383#27573#26631#39064
      OnClick = N6Click
    end
    object N5: TMenuItem
      Caption = #21516#27493#25968#25454#24211#35828#26126#21040#25152#26377#34920#12289#35270#22270#12289#23383#27573#26631#39064
      OnClick = N5Click
    end
  end
  object ABQuery_Field: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    MasterSource = ABDatasource_Table
    MasterFields = 'Ta_Guid'
    DetailFields = 'Fi_Ta_Guid'
    SQL.Strings = (
      'select * from ABSys_Org_Field'
      'where Fi_Ta_Guid=:Ta_Guid')
    SqlUpdateDatetime = 42188.638240925930000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    EditControlType = ctSingle
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ABSys_Org_Field')
    IndexListDefs = <>
    LoadTables.Strings = (
      'ABSys_Org_Field')
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 432
    Top = 343
    ParamData = <
      item
        Name = 'TA_GUID'
        ParamType = ptInput
      end>
  end
  object ABQuery_Table: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    MasterSource = ABDatasource_TreeItem
    MasterFields = 'TI_Guid '
    DetailFields = 'TA_TI_Guid'
    SQL.Strings = (
      'select CL_Name,CL_DatabaseName,b.* '
      
        'from ABSys_Org_ConnList a  left join ABSys_Org_Table b  on TA_CL' +
        '_Guid=CL_Guid'
      'where TA_TI_Guid=:TI_Guid ')
    SqlUpdateDatetime = 42189.340337615740000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    EditControlType = ctSingle
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ABSys_Org_Table')
    IndexListDefs = <>
    LoadTables.Strings = (
      'ABSys_Org_ConnList'
      'ABSys_Org_Table')
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 432
    Top = 271
    ParamData = <
      item
        Name = 'TI_GUID'
        ParamType = ptInput
      end>
  end
  object ABDatasource_Table: TABDatasource
    DataSet = ABQuery_Table
    Left = 560
    Top = 280
  end
  object ABDatasource_Field: TABDatasource
    DataSet = ABQuery_Field
    Left = 560
    Top = 352
  end
  object ABQuery_GetRepeatedFieldCaption: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    SQL.Strings = (
      
        'select databaseName '#25968#25454#24211#21517',TableName '#34920#21517',caption '#23383#27573#35828#26126',count(*) '#37325#22797#27425#25968 +
        ' '
      'from ABSys_Temp_GetFieldInfo'
      'where caption<>'#39#39'                 '
      'group by databaseName,TableName,caption        '
      'having count(*)>1                 '
      'order by   count(*)  desc ')
    SqlUpdateDatetime = 42304.702317731480000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    EditControlType = ctSingle
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ABSys_Temp_GetFieldInfo')
    IndexListDefs = <>
    LoadTables.Strings = (
      'ABSys_Temp_GetFieldInfo')
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 424
    Top = 423
  end
  object ABQuery_TreeItem: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    AfterScroll = ABQuery_TreeItemAfterScroll
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    SQL.Strings = (
      'select * from ABSys_Org_TreeItem '
      'where Ti_Tr_Guid=dbo.Func_GetTreeGuid('#39'Table Dir'#39')  '
      'order by ti_order')
    SqlUpdateDatetime = 42074.708370046300000000
    BeforeDeleteAsk = True
    FieldDefaultValues = 'Ti_Tr_Guid=select dbo.Func_GetTreeGuid('#39'Table Dir'#39')  '
    MaxRecordCount = 0
    EditControlType = ctSingle
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ABSys_Org_TreeItem')
    IndexListDefs = <>
    LoadTables.Strings = (
      'ABSys_Org_TreeItem')
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 432
    Top = 207
  end
  object ABDatasource_TreeItem: TABDatasource
    AutoEdit = False
    DataSet = ABQuery_TreeItem
    Left = 552
    Top = 208
  end
end
