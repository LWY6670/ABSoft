object ABSys_Org_KeyForm: TABSys_Org_KeyForm
  Left = 555
  Top = 135
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  Caption = #35282#33394#31649#29702
  ClientHeight = 550
  ClientWidth = 750
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object ABDBNavigator1: TABDBNavigator
    Left = 0
    Top = 0
    Width = 750
    Height = 27
    Align = alTop
    BevelOuter = bvNone
    ShowCaption = False
    TabOrder = 0
    BigGlyph = False
    ImageLayout = blGlyphLeft
    DataSource = ABDatasource1
    VisibleButtons = [nbInsertSpacer, nbInsert, nbCopy, nbDelete, nbEdit, nbPostSpacer, nbPost, nbCancel, nbQuerySpacer, nbQuery, nbReport, nbExitSpacer, nbExit]
    ButtonRangeType = RtMain
    BtnCustom1ImageIndex = -1
    BtnCustom2ImageIndex = -1
    BtnCustom3ImageIndex = -1
    BtnCustom4ImageIndex = -1
    BtnCustom5ImageIndex = -1
    BtnCustom6ImageIndex = -1
    BtnCustom7ImageIndex = -1
    BtnCustom8ImageIndex = -1
    BtnCustom9ImageIndex = -1
    BtnCustom10ImageIndex = -1
    BtnCustom1Caption = #33258#23450#20041'1'
    BtnCustom2Caption = #33258#23450#20041'2'
    BtnCustom3Caption = #33258#23450#20041'3'
    BtnCustom4Caption = #33258#23450#20041'4'
    BtnCustom5Caption = #33258#23450#20041'5'
    BtnCustom6Caption = #33258#23450#20041'6'
    BtnCustom7Caption = #33258#23450#20041'7'
    BtnCustom8Caption = #33258#23450#20041'8'
    BtnCustom9Caption = #33258#23450#20041'9'
    BtnCustom10Caption = #33258#23450#20041'10'
    BtnCustom1Kind = cxbkStandard
    BtnCustom2Kind = cxbkStandard
    BtnCustom3Kind = cxbkStandard
    BtnCustom4Kind = cxbkStandard
    BtnCustom5Kind = cxbkStandard
    BtnCustom6Kind = cxbkStandard
    BtnCustom7Kind = cxbkStandard
    BtnCustom8Kind = cxbkStandard
    BtnCustom9Kind = cxbkStandard
    BtnCustom10Kind = cxbkStandard
    ApprovedRollbackButton = nbNull
    ApprovedCommitButton = nbNull
    ButtonNativeStyle = False
    BtnCaptions.Strings = (
      'BtnFirstRecord='
      'BtnPreviousRecord='
      'BtnNextRecord='
      'BtnLastRecord='
      'BtnInsert='
      'btnCopy='
      'BtnDelete='
      'BtnEdit='
      'BtnPost='
      'BtnCancel='
      'btnQuery='
      'BtnReport='
      'BtnCustom1='
      'BtnCustom2='
      'BtnCustom3='
      'BtnCustom4='
      'BtnCustom5='
      'BtnCustom6='
      'BtnCustom7='
      'BtnCustom8='
      'BtnCustom9='
      'BtnCustom10='
      'BtnExit=')
  end
  object ABDBcxGrid1: TABcxGrid
    Left = 0
    Top = 27
    Width = 180
    Height = 523
    Align = alLeft
    TabOrder = 1
    LookAndFeel.NativeStyle = False
    object ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView
      PopupMenu.AutoHotkeys = maManual
      PopupMenu.CloseFootStr = False
      PopupMenu.LinkTableView = ABcxGridDBBandedTableView1
      PopupMenu.AutoApplyBestFit = True
      PopupMenu.AutoCreateAllItem = True
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = ABDatasource1
      DataController.Filter.Options = [fcoCaseInsensitive]
      DataController.Filter.AutoDataSetFilter = True
      DataController.Filter.TranslateBetween = True
      DataController.Filter.TranslateIn = True
      DataController.Filter.TranslateLike = True
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.AlwaysShowEditor = True
      OptionsBehavior.FocusCellOnTab = True
      OptionsBehavior.GoToNextCellOnEnter = True
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsCustomize.DataRowSizing = True
      OptionsData.Deleting = False
      OptionsData.Inserting = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.MultiSelect = True
      OptionsView.CellAutoHeight = True
      OptionsView.GroupByBox = False
      OptionsView.BandHeaders = False
      Bands = <
        item
        end>
      ExtPopupMenu.AutoHotkeys = maManual
      ExtPopupMenu.CloseFootStr = False
      ExtPopupMenu.LinkTableView = ABcxGridDBBandedTableView1
      ExtPopupMenu.AutoApplyBestFit = True
      ExtPopupMenu.AutoCreateAllItem = True
    end
    object cxGridLevel1: TcxGridLevel
      GridView = ABcxGridDBBandedTableView1
    end
  end
  object Sheet1_Panel1: TPanel
    Left = 180
    Top = 27
    Width = 570
    Height = 523
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object Sheet1_Splitter1: TABcxSplitter
      Left = 0
      Top = 0
      Width = 8
      Height = 523
      HotZoneClassName = 'TcxMediaPlayer8Style'
      InvertDirection = True
      Control = ABDBcxGrid1
      ExplicitLeft = 179
    end
    object Sheet1_Panel2: TPanel
      Left = 8
      Top = 0
      Width = 562
      Height = 523
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Sheet1_PageControl2: TABcxPageControl
        Left = 0
        Top = 25
        Width = 562
        Height = 498
        Align = alClient
        TabOrder = 0
        Properties.ActivePage = Sheet1_Detail_Sheet4
        Properties.CustomButtons.Buttons = <>
        LookAndFeel.Kind = lfFlat
        LookAndFeel.NativeStyle = False
        OnChange = Sheet1_PageControl2Change
        ActivePageIndex = 3
        ClientRectBottom = 497
        ClientRectLeft = 1
        ClientRectRight = 561
        ClientRectTop = 21
        object Sheet1_Detail_Sheet1: TcxTabSheet
          Caption = #21151#33021#33756#21333#26435#38480
          ImageIndex = 0
          object RzCheckTree1: TABCheckTreeView
            Left = 0
            Top = 0
            Width = 560
            Height = 437
            MoveAfirm = False
            OnStateChang = RzCheckTree1StateChang
            Align = alClient
            Ctl3D = False
            Flatness = cfAlwaysFlat
            GrayedIsChecked = False
            Indent = 19
            ParentCtl3D = False
            TabOrder = 0
          end
          object Panel1: TPanel
            Left = 0
            Top = 437
            Width = 560
            Height = 39
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 1
            object Button1: TABcxButton
              Left = 20
              Top = 6
              Width = 75
              Height = 25
              Caption = #20840#36873
              LookAndFeel.Kind = lfFlat
              LookAndFeel.NativeStyle = False
              TabOrder = 0
              OnClick = Button1Click
              ShowProgressBar = False
            end
            object Button2: TABcxButton
              Left = 101
              Top = 6
              Width = 75
              Height = 25
              Caption = #20840#19981#36873
              LookAndFeel.Kind = lfFlat
              LookAndFeel.NativeStyle = False
              TabOrder = 1
              OnClick = Button2Click
              ShowProgressBar = False
            end
            object Button3: TABcxButton
              Left = 182
              Top = 6
              Width = 75
              Height = 25
              Caption = #21453#21521#36873#25321
              LookAndFeel.Kind = lfFlat
              LookAndFeel.NativeStyle = False
              TabOrder = 2
              OnClick = Button3Click
              ShowProgressBar = False
            end
          end
        end
        object Sheet1_Detail_Sheet3: TcxTabSheet
          Caption = #25253#34920#26435#38480
          ImageIndex = 2
          object RzCheckTree3: TABCheckTreeView
            Left = 0
            Top = 0
            Width = 560
            Height = 437
            MoveAfirm = False
            OnStateChang = RzCheckTree3StateChang
            Align = alClient
            Ctl3D = False
            Flatness = cfAlwaysFlat
            GrayedIsChecked = False
            Indent = 19
            ParentCtl3D = False
            TabOrder = 0
          end
          object Panel2: TPanel
            Left = 0
            Top = 437
            Width = 560
            Height = 39
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 1
            object ABcxButton1: TABcxButton
              Left = 20
              Top = 6
              Width = 75
              Height = 25
              Caption = #20840#36873
              LookAndFeel.Kind = lfFlat
              LookAndFeel.NativeStyle = False
              TabOrder = 0
              OnClick = Button1Click
              ShowProgressBar = False
            end
            object ABcxButton2: TABcxButton
              Left = 101
              Top = 6
              Width = 75
              Height = 25
              Caption = #20840#19981#36873
              LookAndFeel.Kind = lfFlat
              LookAndFeel.NativeStyle = False
              TabOrder = 1
              OnClick = Button2Click
              ShowProgressBar = False
            end
            object ABcxButton3: TABcxButton
              Left = 182
              Top = 6
              Width = 75
              Height = 25
              Caption = #21453#21521#36873#25321
              LookAndFeel.Kind = lfFlat
              LookAndFeel.NativeStyle = False
              TabOrder = 2
              OnClick = Button3Click
              ShowProgressBar = False
            end
          end
        end
        object cxTabSheet1: TcxTabSheet
          Caption = #23450#21046#23545#35937#26435#38480
          ImageIndex = 4
          object RzCheckTree4: TABCheckTreeView
            Left = 0
            Top = 0
            Width = 560
            Height = 437
            MoveAfirm = False
            OnStateChang = RzCheckTree4StateChang
            Align = alClient
            Ctl3D = False
            Flatness = cfAlwaysFlat
            GrayedIsChecked = False
            Indent = 19
            ParentCtl3D = False
            TabOrder = 0
          end
          object Panel5: TPanel
            Left = 0
            Top = 437
            Width = 560
            Height = 39
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 1
            object ABcxButton8: TABcxButton
              Left = 20
              Top = 6
              Width = 75
              Height = 25
              Caption = #20840#36873
              LookAndFeel.Kind = lfFlat
              LookAndFeel.NativeStyle = False
              TabOrder = 0
              OnClick = Button1Click
              ShowProgressBar = False
            end
            object ABcxButton9: TABcxButton
              Left = 101
              Top = 6
              Width = 75
              Height = 25
              Caption = #20840#19981#36873
              LookAndFeel.Kind = lfFlat
              LookAndFeel.NativeStyle = False
              TabOrder = 1
              OnClick = Button2Click
              ShowProgressBar = False
            end
            object ABcxButton10: TABcxButton
              Left = 182
              Top = 6
              Width = 75
              Height = 25
              Caption = #21453#21521#36873#25321
              LookAndFeel.Kind = lfFlat
              LookAndFeel.NativeStyle = False
              TabOrder = 2
              OnClick = Button3Click
              ShowProgressBar = False
            end
          end
        end
        object Sheet1_Detail_Sheet4: TcxTabSheet
          Caption = #34920#26435#38480
          ImageIndex = 3
          object ABDBcxGrid2: TABcxGrid
            Left = 0
            Top = 27
            Width = 560
            Height = 166
            Align = alTop
            TabOrder = 0
            LookAndFeel.NativeStyle = False
            object ABcxGridDBBandedTableView2: TABcxGridDBBandedTableView
              PopupMenu.AutoHotkeys = maManual
              PopupMenu.CloseFootStr = False
              PopupMenu.LinkTableView = ABcxGridDBBandedTableView2
              PopupMenu.AutoApplyBestFit = True
              PopupMenu.AutoCreateAllItem = True
              Navigator.Buttons.CustomButtons = <>
              Navigator.Buttons.PriorPage.Visible = False
              Navigator.Buttons.NextPage.Visible = False
              Navigator.Buttons.Refresh.Visible = False
              Navigator.Buttons.SaveBookmark.Visible = False
              Navigator.Buttons.GotoBookmark.Visible = False
              DataController.DataSource = ABDatasource1_4
              DataController.Filter.Options = [fcoCaseInsensitive]
              DataController.Filter.AutoDataSetFilter = True
              DataController.Filter.TranslateBetween = True
              DataController.Filter.TranslateIn = True
              DataController.Filter.TranslateLike = True
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              OptionsBehavior.AlwaysShowEditor = True
              OptionsBehavior.FocusCellOnTab = True
              OptionsBehavior.GoToNextCellOnEnter = True
              OptionsCustomize.ColumnsQuickCustomization = True
              OptionsCustomize.DataRowSizing = True
              OptionsData.Deleting = False
              OptionsData.Inserting = False
              OptionsSelection.HideFocusRectOnExit = False
              OptionsSelection.MultiSelect = True
              OptionsView.CellAutoHeight = True
              OptionsView.GroupByBox = False
              OptionsView.BandHeaders = False
              Bands = <
                item
                end>
              ExtPopupMenu.AutoHotkeys = maManual
              ExtPopupMenu.CloseFootStr = False
              ExtPopupMenu.LinkTableView = ABcxGridDBBandedTableView2
              ExtPopupMenu.AutoApplyBestFit = True
              ExtPopupMenu.AutoCreateAllItem = True
            end
            object cxGridLevel2: TcxGridLevel
              GridView = ABcxGridDBBandedTableView2
            end
          end
          object Panel3: TPanel
            Left = 0
            Top = 437
            Width = 560
            Height = 39
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 1
            object ABcxButton7: TABcxButton
              Left = 5
              Top = 6
              Width = 100
              Height = 25
              Caption = #25209#37327#22686#21152'/'#21024#38500
              LookAndFeel.Kind = lfFlat
              LookAndFeel.NativeStyle = False
              TabOrder = 0
              OnClick = ABcxButton7Click
              ShowProgressBar = False
            end
            object ABcxButton5: TABcxButton
              Left = 111
              Top = 6
              Width = 100
              Height = 25
              Caption = #25209#37327#35774#32622
              DropDownMenu = PopupMenu1
              Kind = cxbkDropDownButton
              LookAndFeel.Kind = lfFlat
              LookAndFeel.NativeStyle = False
              TabOrder = 1
              OnClick = ABcxButton5Click
              ShowProgressBar = False
            end
            object ABcxLabel2: TABcxLabel
              Left = 224
              Top = 6
              AutoSize = False
              Caption = #31354#25968#25454#34920#31034#25317#26377#25152#26377#34920#30340#25152#26377#26435#38480
              ParentFont = False
              Style.Font.Charset = DEFAULT_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = #23435#20307
              Style.Font.Style = []
              Style.IsFontAssigned = True
              Properties.Alignment.Horz = taLeftJustify
              Properties.Alignment.Vert = taVCenter
              Transparent = True
              Height = 25
              Width = 306
              AnchorY = 19
            end
          end
          object ABDBNavigator2: TABDBNavigator
            Left = 0
            Top = 0
            Width = 560
            Height = 27
            Align = alTop
            BevelOuter = bvNone
            ShowCaption = False
            TabOrder = 2
            BigGlyph = False
            ImageLayout = blGlyphLeft
            DataSource = ABDatasource1_4
            VisibleButtons = [nbInsertSpacer, nbInsert, nbCopy, nbDelete, nbEdit, nbPostSpacer, nbPost, nbCancel]
            ButtonRangeType = RtDetail
            BtnCustom1ImageIndex = -1
            BtnCustom2ImageIndex = -1
            BtnCustom3ImageIndex = -1
            BtnCustom4ImageIndex = -1
            BtnCustom5ImageIndex = -1
            BtnCustom6ImageIndex = -1
            BtnCustom7ImageIndex = -1
            BtnCustom8ImageIndex = -1
            BtnCustom9ImageIndex = -1
            BtnCustom10ImageIndex = -1
            BtnCustom1Caption = #33258#23450#20041'1'
            BtnCustom2Caption = #33258#23450#20041'2'
            BtnCustom3Caption = #33258#23450#20041'3'
            BtnCustom4Caption = #33258#23450#20041'4'
            BtnCustom5Caption = #33258#23450#20041'5'
            BtnCustom6Caption = #33258#23450#20041'6'
            BtnCustom7Caption = #33258#23450#20041'7'
            BtnCustom8Caption = #33258#23450#20041'8'
            BtnCustom9Caption = #33258#23450#20041'9'
            BtnCustom10Caption = #33258#23450#20041'10'
            BtnCustom1Kind = cxbkStandard
            BtnCustom2Kind = cxbkStandard
            BtnCustom3Kind = cxbkStandard
            BtnCustom4Kind = cxbkStandard
            BtnCustom5Kind = cxbkStandard
            BtnCustom6Kind = cxbkStandard
            BtnCustom7Kind = cxbkStandard
            BtnCustom8Kind = cxbkStandard
            BtnCustom9Kind = cxbkStandard
            BtnCustom10Kind = cxbkStandard
            ApprovedRollbackButton = nbNull
            ApprovedCommitButton = nbNull
            ButtonNativeStyle = False
            BtnCaptions.Strings = (
              'BtnFirstRecord='
              'BtnPreviousRecord='
              'BtnNextRecord='
              'BtnLastRecord='
              'BtnInsert='
              'btnCopy='
              'BtnDelete='
              'BtnEdit='
              'BtnPost='
              'BtnCancel='
              'btnQuery='
              'BtnReport='
              'BtnCustom1='
              'BtnCustom2='
              'BtnCustom3='
              'BtnCustom4='
              'BtnCustom5='
              'BtnCustom6='
              'BtnCustom7='
              'BtnCustom8='
              'BtnCustom9='
              'BtnCustom10='
              'BtnExit=')
          end
          object ProgressBar1: TProgressBar
            Left = 0
            Top = 420
            Width = 560
            Height = 17
            Align = alBottom
            TabOrder = 3
            Visible = False
          end
          object Panel7: TPanel
            Left = 0
            Top = 226
            Width = 560
            Height = 194
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 4
            object Panel8: TPanel
              Left = 0
              Top = 0
              Width = 248
              Height = 194
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object ABcxLabel6: TABcxLabel
                Left = 0
                Top = 0
                Align = alTop
                AutoSize = False
                Caption = #24050#20998#37197#30340#34920#34892
                ParentFont = False
                Style.Font.Charset = DEFAULT_CHARSET
                Style.Font.Color = clWindowText
                Style.Font.Height = -11
                Style.Font.Name = #23435#20307
                Style.Font.Style = []
                Style.IsFontAssigned = True
                Properties.Alignment.Horz = taCenter
                Properties.Alignment.Vert = taVCenter
                Transparent = True
                Height = 25
                Width = 248
                AnchorX = 124
                AnchorY = 13
              end
              object ABcxGrid1: TABcxGrid
                Left = 0
                Top = 25
                Width = 248
                Height = 169
                Align = alClient
                TabOrder = 1
                LookAndFeel.NativeStyle = False
                object ABcxGridDBBandedTableView4: TABcxGridDBBandedTableView
                  PopupMenu.AutoHotkeys = maManual
                  PopupMenu.CloseFootStr = False
                  PopupMenu.LinkTableView = ABcxGridDBBandedTableView4
                  PopupMenu.AutoApplyBestFit = True
                  PopupMenu.AutoCreateAllItem = True
                  OnDblClick = ABcxGridDBBandedTableView4DblClick
                  Navigator.Buttons.CustomButtons = <>
                  Navigator.Buttons.PriorPage.Visible = False
                  Navigator.Buttons.NextPage.Visible = False
                  Navigator.Buttons.Refresh.Visible = False
                  Navigator.Buttons.SaveBookmark.Visible = False
                  Navigator.Buttons.GotoBookmark.Visible = False
                  DataController.DataSource = ABDatasource1_4_1
                  DataController.Filter.Options = [fcoCaseInsensitive]
                  DataController.Filter.AutoDataSetFilter = True
                  DataController.Filter.TranslateBetween = True
                  DataController.Filter.TranslateIn = True
                  DataController.Filter.TranslateLike = True
                  DataController.Summary.DefaultGroupSummaryItems = <>
                  DataController.Summary.FooterSummaryItems = <>
                  DataController.Summary.SummaryGroups = <>
                  OptionsBehavior.AlwaysShowEditor = True
                  OptionsBehavior.FocusCellOnTab = True
                  OptionsBehavior.GoToNextCellOnEnter = True
                  OptionsCustomize.ColumnsQuickCustomization = True
                  OptionsCustomize.DataRowSizing = True
                  OptionsData.Deleting = False
                  OptionsData.Inserting = False
                  OptionsSelection.CellSelect = False
                  OptionsSelection.HideFocusRectOnExit = False
                  OptionsSelection.MultiSelect = True
                  OptionsView.CellAutoHeight = True
                  OptionsView.GroupByBox = False
                  OptionsView.BandHeaders = False
                  Bands = <
                    item
                    end>
                  ExtPopupMenu.AutoHotkeys = maManual
                  ExtPopupMenu.CloseFootStr = False
                  ExtPopupMenu.LinkTableView = ABcxGridDBBandedTableView4
                  ExtPopupMenu.AutoApplyBestFit = True
                  ExtPopupMenu.AutoCreateAllItem = True
                end
                object cxGridLevel4: TcxGridLevel
                  GridView = ABcxGridDBBandedTableView4
                end
              end
            end
            object ABcxSplitter2: TABcxSplitter
              Left = 248
              Top = 0
              Width = 8
              Height = 194
              HotZoneClassName = 'TcxMediaPlayer8Style'
              AlignSplitter = salRight
              InvertDirection = True
              Control = Panel6
            end
            object Panel9: TPanel
              Left = 282
              Top = 0
              Width = 278
              Height = 194
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 2
              object ABcxLabel7: TABcxLabel
                Left = 0
                Top = 0
                Align = alTop
                AutoSize = False
                Caption = #21487#20998#37197#30340#34920#34892
                ParentFont = False
                Style.Font.Charset = DEFAULT_CHARSET
                Style.Font.Color = clWindowText
                Style.Font.Height = -11
                Style.Font.Name = #23435#20307
                Style.Font.Style = []
                Style.IsFontAssigned = True
                Properties.Alignment.Horz = taCenter
                Properties.Alignment.Vert = taVCenter
                Transparent = True
                Height = 25
                Width = 278
                AnchorX = 139
                AnchorY = 13
              end
              object ABcxGrid2: TABcxGrid
                Left = 0
                Top = 25
                Width = 278
                Height = 169
                Align = alClient
                TabOrder = 1
                LookAndFeel.NativeStyle = False
                object ABcxGridDBBandedTableView5: TABcxGridDBBandedTableView
                  PopupMenu.AutoHotkeys = maManual
                  PopupMenu.CloseFootStr = False
                  PopupMenu.LinkTableView = ABcxGridDBBandedTableView5
                  PopupMenu.AutoApplyBestFit = True
                  PopupMenu.AutoCreateAllItem = True
                  OnDblClick = ABcxGridDBBandedTableView5DblClick
                  Navigator.Buttons.CustomButtons = <>
                  Navigator.Buttons.PriorPage.Visible = False
                  Navigator.Buttons.NextPage.Visible = False
                  Navigator.Buttons.Refresh.Visible = False
                  Navigator.Buttons.SaveBookmark.Visible = False
                  Navigator.Buttons.GotoBookmark.Visible = False
                  DataController.DataSource = ABDatasource1_4_2
                  DataController.Filter.Options = [fcoCaseInsensitive]
                  DataController.Filter.AutoDataSetFilter = True
                  DataController.Filter.TranslateBetween = True
                  DataController.Filter.TranslateIn = True
                  DataController.Filter.TranslateLike = True
                  DataController.Summary.DefaultGroupSummaryItems = <>
                  DataController.Summary.FooterSummaryItems = <>
                  DataController.Summary.SummaryGroups = <>
                  OptionsBehavior.AlwaysShowEditor = True
                  OptionsBehavior.FocusCellOnTab = True
                  OptionsBehavior.GoToNextCellOnEnter = True
                  OptionsCustomize.ColumnsQuickCustomization = True
                  OptionsCustomize.DataRowSizing = True
                  OptionsData.Deleting = False
                  OptionsData.Inserting = False
                  OptionsSelection.CellSelect = False
                  OptionsSelection.HideFocusRectOnExit = False
                  OptionsSelection.MultiSelect = True
                  OptionsView.CellAutoHeight = True
                  OptionsView.GroupByBox = False
                  OptionsView.BandHeaders = False
                  Bands = <
                    item
                    end>
                  ExtPopupMenu.AutoHotkeys = maManual
                  ExtPopupMenu.CloseFootStr = False
                  ExtPopupMenu.LinkTableView = ABcxGridDBBandedTableView5
                  ExtPopupMenu.AutoApplyBestFit = True
                  ExtPopupMenu.AutoCreateAllItem = True
                end
                object cxGridLevel5: TcxGridLevel
                  GridView = ABcxGridDBBandedTableView5
                end
              end
            end
            object Panel6: TPanel
              Left = 256
              Top = 0
              Width = 26
              Height = 194
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 3
              object Panel10: TPanel
                Left = 2
                Top = 51
                Width = 24
                Height = 88
                BevelOuter = bvNone
                TabOrder = 0
                object SpeedButton1: TSpeedButton
                  Left = 0
                  Top = 0
                  Width = 22
                  Height = 22
                  Flat = True
                  Glyph.Data = {
                    36040000424D3604000000000000360000002800000010000000100000000100
                    20000000000000040000C40E0000C40E00000000000000000000FF00FF00FF00
                    FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF00FF00FF00FF00FF009C3000009C300000FF00FF00FF00FF00FF00
                    FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF00FF00FF00FF00FF009C300000CE6100009C300000FF00FF00FF00
                    FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF00FF00FF00FF00FF009C300000CE610000CE6100009C300000FF00
                    FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF009C3000009C3000009C300000CE610000CE610000CE6100009C30
                    0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF009C300000CE610000CE610000CE610000CE610000CE610000CE61
                    00009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF009C300000CE610000CE610000CE610000CE610000CE610000CE61
                    0000CE6100009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF009C300000CE610000CE610000CE610000CE610000CE610000CE61
                    0000CE610000CE6100009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF009C300000CE610000CE610000CE610000CE610000CE610000CE61
                    0000CE6100009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF009C300000CE610000CE610000CE610000CE610000CE610000CE61
                    00009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF009C3000009C3000009C300000CE610000CE610000CE6100009C30
                    0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF00FF00FF00FF00FF009C300000CE610000CE6100009C300000FF00
                    FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF00FF00FF00FF00FF009C300000CE6100009C300000FF00FF00FF00
                    FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF00FF00FF00FF00FF009C3000009C300000FF00FF00FF00FF00FF00
                    FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
                  OnClick = SpeedButton1Click
                end
                object SpeedButton2: TSpeedButton
                  Left = 0
                  Top = 22
                  Width = 22
                  Height = 22
                  Flat = True
                  Glyph.Data = {
                    36040000424D3604000000000000360000002800000010000000100000000100
                    20000000000000040000C40E0000C40E00000000000000000000FF00FF00FF00
                    FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF009C3000009C300000FF00FF00FF00FF009C3000009C300000FF00
                    FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF009C300000CE6100009C300000FF00FF009C300000CE6100009C30
                    0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF009C300000CE610000CE6100009C3000009C300000CE610000CE61
                    00009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF009C30
                    00009C3000009C300000CE610000CE610000CE6100009C300000CE610000CE61
                    0000CE6100009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF009C30
                    0000CE610000CE610000CE610000CE610000CE610000CE6100009C300000CE61
                    0000CE610000CE6100009C300000FF00FF00FF00FF00FF00FF00FF00FF009C30
                    0000CE610000CE610000CE610000CE610000CE610000CE610000CE6100009C30
                    0000CE610000CE610000CE6100009C300000FF00FF00FF00FF00FF00FF009C30
                    0000CE610000CE610000CE610000CE610000CE610000CE610000CE610000CE61
                    00009C300000CE610000CE610000CE6100009C300000FF00FF00FF00FF009C30
                    0000CE610000CE610000CE610000CE610000CE610000CE610000CE6100009C30
                    0000CE610000CE610000CE6100009C300000FF00FF00FF00FF00FF00FF009C30
                    0000CE610000CE610000CE610000CE610000CE610000CE6100009C300000CE61
                    0000CE610000CE6100009C300000FF00FF00FF00FF00FF00FF00FF00FF009C30
                    00009C3000009C300000CE610000CE610000CE6100009C300000CE610000CE61
                    0000CE6100009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF009C300000CE610000CE6100009C3000009C300000CE610000CE61
                    00009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF009C300000CE6100009C300000FF00FF009C300000CE6100009C30
                    0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF009C3000009C300000FF00FF00FF00FF009C3000009C300000FF00
                    FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
                  OnClick = SpeedButton2Click
                end
                object SpeedButton3: TSpeedButton
                  Left = 0
                  Top = 44
                  Width = 22
                  Height = 22
                  Flat = True
                  Glyph.Data = {
                    36040000424D3604000000000000360000002800000010000000100000000100
                    20000000000000040000C40E0000C40E00000000000000000000FF00FF00FF00
                    FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF009C30
                    00009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF009C300000CE61
                    00009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF009C300000CE610000CE61
                    00009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF00FF00FF00FF00FF00FF00FF009C300000CE610000CE610000CE61
                    00009C3000009C3000009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF00FF00FF00FF00FF009C300000CE610000CE610000CE610000CE61
                    0000CE610000CE6100009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF00FF00FF009C300000CE610000CE610000CE610000CE610000CE61
                    0000CE610000CE6100009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF009C300000CE610000CE610000CE610000CE610000CE610000CE61
                    0000CE610000CE6100009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF00FF00FF009C300000CE610000CE610000CE610000CE610000CE61
                    0000CE610000CE6100009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF00FF00FF00FF00FF009C300000CE610000CE610000CE610000CE61
                    0000CE610000CE6100009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF00FF00FF00FF00FF00FF00FF009C300000CE610000CE610000CE61
                    00009C3000009C3000009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF009C300000CE610000CE61
                    00009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF009C300000CE61
                    00009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF009C30
                    00009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
                  OnClick = SpeedButton3Click
                end
                object SpeedButton4: TSpeedButton
                  Left = 0
                  Top = 66
                  Width = 22
                  Height = 22
                  Flat = True
                  Glyph.Data = {
                    36040000424D3604000000000000360000002800000010000000100000000100
                    20000000000000040000C40E0000C40E00000000000000000000FF00FF00FF00
                    FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF009C3000009C300000FF00
                    FF00FF00FF009C3000009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF00FF00FF00FF00FF00FF00FF009C300000CE6100009C300000FF00
                    FF009C300000CE6100009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF00FF00FF00FF00FF009C300000CE610000CE6100009C3000009C30
                    0000CE610000CE6100009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF00FF00FF009C300000CE610000CE610000CE6100009C300000CE61
                    0000CE610000CE6100009C3000009C3000009C300000FF00FF00FF00FF00FF00
                    FF00FF00FF009C300000CE610000CE610000CE6100009C300000CE610000CE61
                    0000CE610000CE610000CE610000CE6100009C300000FF00FF00FF00FF00FF00
                    FF009C300000CE610000CE610000CE6100009C300000CE610000CE610000CE61
                    0000CE610000CE610000CE610000CE6100009C300000FF00FF00FF00FF009C30
                    0000CE610000CE610000CE6100009C300000CE610000CE610000CE610000CE61
                    0000CE610000CE610000CE610000CE6100009C300000FF00FF00FF00FF00FF00
                    FF009C300000CE610000CE610000CE6100009C300000CE610000CE610000CE61
                    0000CE610000CE610000CE610000CE6100009C300000FF00FF00FF00FF00FF00
                    FF00FF00FF009C300000CE610000CE610000CE6100009C300000CE610000CE61
                    0000CE610000CE610000CE610000CE6100009C300000FF00FF00FF00FF00FF00
                    FF00FF00FF00FF00FF009C300000CE610000CE610000CE6100009C300000CE61
                    0000CE610000CE6100009C3000009C3000009C300000FF00FF00FF00FF00FF00
                    FF00FF00FF00FF00FF00FF00FF009C300000CE610000CE6100009C3000009C30
                    0000CE610000CE6100009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF00FF00FF00FF00FF00FF00FF009C300000CE6100009C300000FF00
                    FF009C300000CE6100009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF009C3000009C300000FF00
                    FF00FF00FF009C3000009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                    FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
                  OnClick = SpeedButton4Click
                end
              end
            end
          end
          object ABcxSplitter1: TABcxSplitter
            Left = 0
            Top = 193
            Width = 560
            Height = 8
            HotZoneClassName = 'TcxMediaPlayer8Style'
            AlignSplitter = salTop
            InvertDirection = True
            Control = ABDBcxGrid2
          end
          object ABcxLabel5: TABcxLabel
            Left = 0
            Top = 201
            Align = alTop
            AutoSize = False
            Caption = #34920#34892#26435#38480#20998#37197
            ParentFont = False
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = #23435#20307
            Style.Font.Style = []
            Style.IsFontAssigned = True
            Properties.Alignment.Horz = taCenter
            Properties.Alignment.Vert = taVCenter
            Transparent = True
            Height = 25
            Width = 560
            AnchorX = 280
            AnchorY = 214
          end
        end
        object Sheet1_Detail_Sheet5: TcxTabSheet
          Caption = #23383#27573#26435#38480
          ImageIndex = 4
          object ABDBcxGrid3: TABcxGrid
            Left = 0
            Top = 27
            Width = 560
            Height = 393
            Align = alClient
            TabOrder = 0
            LookAndFeel.NativeStyle = False
            object ABcxGridDBBandedTableView3: TABcxGridDBBandedTableView
              PopupMenu.AutoHotkeys = maManual
              PopupMenu.CloseFootStr = False
              PopupMenu.LinkTableView = ABcxGridDBBandedTableView3
              PopupMenu.AutoApplyBestFit = True
              PopupMenu.AutoCreateAllItem = True
              Navigator.Buttons.CustomButtons = <>
              Navigator.Buttons.PriorPage.Visible = False
              Navigator.Buttons.NextPage.Visible = False
              Navigator.Buttons.Append.Visible = False
              Navigator.Buttons.Refresh.Visible = False
              Navigator.Buttons.SaveBookmark.Visible = False
              Navigator.Buttons.GotoBookmark.Visible = False
              DataController.DataSource = ABDatasource1_5
              DataController.Filter.Options = [fcoCaseInsensitive]
              DataController.Filter.AutoDataSetFilter = True
              DataController.Filter.TranslateBetween = True
              DataController.Filter.TranslateIn = True
              DataController.Filter.TranslateLike = True
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              OptionsBehavior.AlwaysShowEditor = True
              OptionsBehavior.FocusCellOnTab = True
              OptionsBehavior.GoToNextCellOnEnter = True
              OptionsCustomize.ColumnsQuickCustomization = True
              OptionsCustomize.DataRowSizing = True
              OptionsData.Deleting = False
              OptionsData.Inserting = False
              OptionsSelection.HideFocusRectOnExit = False
              OptionsSelection.MultiSelect = True
              OptionsView.CellAutoHeight = True
              OptionsView.GroupByBox = False
              OptionsView.BandHeaders = False
              Bands = <
                item
                end>
              ExtPopupMenu.AutoHotkeys = maManual
              ExtPopupMenu.CloseFootStr = False
              ExtPopupMenu.LinkTableView = ABcxGridDBBandedTableView3
              ExtPopupMenu.AutoApplyBestFit = True
              ExtPopupMenu.AutoCreateAllItem = True
            end
            object cxGridLevel3: TcxGridLevel
              GridView = ABcxGridDBBandedTableView3
            end
          end
          object Panel4: TPanel
            Left = 0
            Top = 437
            Width = 560
            Height = 39
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 1
            object ABcxLabel3: TABcxLabel
              Left = 224
              Top = 6
              AutoSize = False
              Caption = #31354#25968#25454#34920#31034#25317#26377#25152#26377#23383#27573#30340#25152#26377#26435#38480
              ParentFont = False
              Style.Font.Charset = DEFAULT_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -14
              Style.Font.Name = #23435#20307
              Style.Font.Style = []
              Style.IsFontAssigned = True
              Properties.Alignment.Horz = taLeftJustify
              Properties.Alignment.Vert = taVCenter
              Transparent = True
              Height = 25
              Width = 306
              AnchorY = 19
            end
            object ABcxButton4: TABcxButton
              Left = 5
              Top = 6
              Width = 100
              Height = 25
              Caption = #25209#37327#22686#21152'/'#21024#38500
              LookAndFeel.Kind = lfFlat
              LookAndFeel.NativeStyle = False
              TabOrder = 1
              OnClick = ABcxButton4Click
              ShowProgressBar = False
            end
            object ABcxButton6: TABcxButton
              Left = 111
              Top = 6
              Width = 100
              Height = 25
              Caption = #25209#37327#35774#32622
              DropDownMenu = PopupMenu2
              Kind = cxbkDropDownButton
              LookAndFeel.Kind = lfFlat
              LookAndFeel.NativeStyle = False
              TabOrder = 2
              OnClick = ABcxButton6Click
              ShowProgressBar = False
            end
          end
          object ABDBNavigator3: TABDBNavigator
            Left = 0
            Top = 0
            Width = 560
            Height = 27
            Align = alTop
            BevelOuter = bvNone
            ShowCaption = False
            TabOrder = 2
            BigGlyph = False
            ImageLayout = blGlyphLeft
            DataSource = ABDatasource1_5
            VisibleButtons = [nbInsertSpacer, nbInsert, nbCopy, nbDelete, nbEdit, nbPostSpacer, nbPost, nbCancel]
            ButtonRangeType = RtDetail
            BtnCustom1ImageIndex = -1
            BtnCustom2ImageIndex = -1
            BtnCustom3ImageIndex = -1
            BtnCustom4ImageIndex = -1
            BtnCustom5ImageIndex = -1
            BtnCustom6ImageIndex = -1
            BtnCustom7ImageIndex = -1
            BtnCustom8ImageIndex = -1
            BtnCustom9ImageIndex = -1
            BtnCustom10ImageIndex = -1
            BtnCustom1Caption = #33258#23450#20041'1'
            BtnCustom2Caption = #33258#23450#20041'2'
            BtnCustom3Caption = #33258#23450#20041'3'
            BtnCustom4Caption = #33258#23450#20041'4'
            BtnCustom5Caption = #33258#23450#20041'5'
            BtnCustom6Caption = #33258#23450#20041'6'
            BtnCustom7Caption = #33258#23450#20041'7'
            BtnCustom8Caption = #33258#23450#20041'8'
            BtnCustom9Caption = #33258#23450#20041'9'
            BtnCustom10Caption = #33258#23450#20041'10'
            BtnCustom1Kind = cxbkStandard
            BtnCustom2Kind = cxbkStandard
            BtnCustom3Kind = cxbkStandard
            BtnCustom4Kind = cxbkStandard
            BtnCustom5Kind = cxbkStandard
            BtnCustom6Kind = cxbkStandard
            BtnCustom7Kind = cxbkStandard
            BtnCustom8Kind = cxbkStandard
            BtnCustom9Kind = cxbkStandard
            BtnCustom10Kind = cxbkStandard
            ApprovedRollbackButton = nbNull
            ApprovedCommitButton = nbNull
            ButtonNativeStyle = False
            BtnCaptions.Strings = (
              'BtnFirstRecord='
              'BtnPreviousRecord='
              'BtnNextRecord='
              'BtnLastRecord='
              'BtnInsert='
              'btnCopy='
              'BtnDelete='
              'BtnEdit='
              'BtnPost='
              'BtnCancel='
              'btnQuery='
              'BtnReport='
              'BtnCustom1='
              'BtnCustom2='
              'BtnCustom3='
              'BtnCustom4='
              'BtnCustom5='
              'BtnCustom6='
              'BtnCustom7='
              'BtnCustom8='
              'BtnCustom9='
              'BtnCustom10='
              'BtnExit=')
          end
          object ProgressBar2: TProgressBar
            Left = 0
            Top = 420
            Width = 560
            Height = 17
            Align = alBottom
            TabOrder = 3
            Visible = False
          end
        end
      end
      object ABcxLabel1: TABcxLabel
        Left = 0
        Top = 0
        Align = alTop
        AutoSize = False
        Caption = #24403#21069#35282#33394#20855#26377#30340#26435#38480
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = #23435#20307
        Style.Font.Style = []
        Style.IsFontAssigned = True
        Properties.Alignment.Horz = taCenter
        Properties.Alignment.Vert = taVCenter
        Transparent = True
        Height = 25
        Width = 562
        AnchorX = 281
        AnchorY = 13
      end
    end
  end
  object ABDatasource1: TABDatasource
    AutoEdit = False
    DataSet = ABQuery1
    Left = 110
    Top = 67
  end
  object ABQuery1_1: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    MasterSource = ABDatasource1
    MasterFields = 'Ke_Guid'
    DetailFields = 'Kf_Ke_Guid'
    SQL.Strings = (
      'select * from ABSys_Right_KeyFunction where Kf_Ke_Guid=:Ke_Guid'
      '')
    SqlUpdateDatetime = 42451.273317372690000000
    BeforeDeleteAsk = False
    MaxRecordCount = 0
    EditControlType = ctSingle
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ABSys_Right_KeyFunction')
    IndexListDefs = <>
    LoadTables.Strings = (
      'ABSys_Right_KeyFunction')
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 27
    Top = 126
    ParamData = <
      item
        Name = 'Ke_Guid'
        ParamType = ptInput
      end>
  end
  object ABQuery1_2: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    MasterSource = ABDatasource1
    MasterFields = 'Ke_Guid'
    DetailFields = 'Kr_Ke_Guid'
    SQL.Strings = (
      
        'select * from ABSys_Right_KeyExtendReport where Kr_Ke_Guid=:Ke_G' +
        'uid'
      '')
    SqlUpdateDatetime = 42451.273317372690000000
    BeforeDeleteAsk = False
    MaxRecordCount = 0
    EditControlType = ctSingle
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ABSys_Right_KeyExtendReport')
    IndexListDefs = <>
    LoadTables.Strings = (
      'ABSys_Right_KeyExtendReport')
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 27
    Top = 189
    ParamData = <
      item
        Name = 'Ke_Guid'
        ParamType = ptInput
      end>
  end
  object ABQuery1_4: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    AfterScroll = ABQuery1_4AfterScroll
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    MasterSource = ABDatasource1
    MasterFields = 'Ke_Guid'
    DetailFields = 'Kt_Ke_Guid'
    SQL.Strings = (
      'select  *   '
      'from ABSys_Right_KeyTable '
      'where Kt_Ke_Guid=:Ke_Guid'
      '')
    SqlUpdateDatetime = 42199.689130486110000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    EditControlType = ctSingle
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ABSys_Right_KeyTable')
    IndexListDefs = <>
    LoadTables.Strings = (
      'ABSys_Right_KeyTable')
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 24
    Top = 327
    ParamData = <
      item
        Name = 'Ke_Guid'
        ParamType = ptInput
      end>
  end
  object ABQuery1_4_1: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    MasterSource = ABDatasource1_4
    MasterFields = 'KT_Guid'
    DetailFields = 'KR_KT_Guid'
    SQL.Strings = (
      'select  *  '
      'from ABSys_Right_KeyTableRow  '
      'where KR_KT_Guid=:KT_Guid'
      '')
    SqlUpdateDatetime = 42199.689255625000000000
    BeforeDeleteAsk = False
    MaxRecordCount = 0
    EditControlType = ctSingle
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ABSys_Right_KeyTableRow')
    IndexListDefs = <>
    LoadTables.Strings = (
      'ABSys_Right_KeyTableRow')
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 224
    Top = 343
    ParamData = <
      item
        Name = 'KT_GUID'
        ParamType = ptInput
      end>
  end
  object ABDatasource1_4: TABDatasource
    AutoEdit = False
    DataSet = ABQuery1_4
    Left = 117
    Top = 328
  end
  object ABDatasource1_4_1: TABDatasource
    AutoEdit = False
    DataSet = ABQuery1_4_1
    Left = 308
    Top = 352
  end
  object PopupMenu1: TPopupMenu
    Left = 298
    Top = 415
    object N4: TMenuItem
      Caption = 'X'#36724#33539#22260
      object N9: TMenuItem
        Tag = 91
        AutoCheck = True
        Caption = #24403#21069#34920
        GroupIndex = 1
        RadioItem = True
        OnClick = MenuItem7Click
      end
      object N10: TMenuItem
        Tag = 91
        AutoCheck = True
        Caption = #36873#20013#30340#34920
        GroupIndex = 1
        RadioItem = True
        OnClick = MenuItem7Click
      end
      object N11: TMenuItem
        Tag = 91
        AutoCheck = True
        Caption = #20174#24403#21069#34920#24320#22987
        GroupIndex = 1
        RadioItem = True
        OnClick = MenuItem7Click
      end
      object N12: TMenuItem
        Tag = 91
        AutoCheck = True
        Caption = #25152#26377#34920
        Checked = True
        Default = True
        GroupIndex = 1
        RadioItem = True
        OnClick = MenuItem7Click
      end
    end
    object N3: TMenuItem
      Caption = 'Y'#36724#33539#22260
      object N5: TMenuItem
        Tag = 91
        AutoCheck = True
        Caption = #26159#21542#21487#26032#22686
        Checked = True
      end
      object N6: TMenuItem
        Tag = 91
        AutoCheck = True
        Caption = #26159#21542#21487#21024#38500
        Checked = True
      end
      object N7: TMenuItem
        Tag = 91
        AutoCheck = True
        Caption = #26159#21542#21487#20462#25913
        Checked = True
      end
      object N8: TMenuItem
        Tag = 91
        AutoCheck = True
        Caption = #26159#21542#21487#25171#21360
        Checked = True
      end
    end
    object N1: TMenuItem
      Caption = #35774#32622'/'#28165#38500
      object N2: TMenuItem
        Tag = 91
        AutoCheck = True
        Caption = #35774#32622#26435#38480
        Checked = True
        Default = True
        GroupIndex = 2
        RadioItem = True
        OnClick = MenuItem7Click
      end
      object N13: TMenuItem
        Tag = 91
        AutoCheck = True
        Caption = #28165#38500#26435#38480
        GroupIndex = 2
        RadioItem = True
        OnClick = MenuItem7Click
      end
    end
  end
  object PopupMenu2: TPopupMenu
    Left = 386
    Top = 423
    object MenuItem6: TMenuItem
      Caption = 'X'#36724#33539#22260
      object MenuItem7: TMenuItem
        Tag = 91
        AutoCheck = True
        Caption = #24403#21069#34920'+'#23383#27573
        GroupIndex = 11
        RadioItem = True
        OnClick = MenuItem7Click
      end
      object MenuItem8: TMenuItem
        Tag = 91
        AutoCheck = True
        Caption = #36873#20013#30340#34920'+'#23383#27573
        GroupIndex = 11
        RadioItem = True
        OnClick = MenuItem7Click
      end
      object MenuItem9: TMenuItem
        Tag = 91
        AutoCheck = True
        Caption = #20174#24403#21069#34920'+'#23383#27573#24320#22987
        GroupIndex = 11
        RadioItem = True
        OnClick = MenuItem7Click
      end
      object MenuItem10: TMenuItem
        Tag = 91
        AutoCheck = True
        Caption = #25152#26377#34920'+'#23383#27573
        Checked = True
        Default = True
        GroupIndex = 11
        RadioItem = True
        OnClick = MenuItem7Click
      end
    end
    object MenuItem1: TMenuItem
      Caption = 'Y'#36724#33539#22260
      object MenuItem2: TMenuItem
        Tag = 91
        AutoCheck = True
        Caption = #32534#36753#33021#20462#25913
        Checked = True
      end
      object MenuItem3: TMenuItem
        Tag = 91
        AutoCheck = True
        Caption = #26032#22686#33021#20462#25913
        Checked = True
      end
      object MenuItem4: TMenuItem
        Tag = 91
        AutoCheck = True
        Caption = #26159#21542#26174#31034
        Checked = True
      end
    end
    object N14: TMenuItem
      Caption = #35774#32622'/'#28165#38500
      object N16: TMenuItem
        Tag = 91
        AutoCheck = True
        Caption = #35774#32622#26435#38480
        Checked = True
        Default = True
        GroupIndex = 2
        RadioItem = True
        OnClick = MenuItem7Click
      end
      object N15: TMenuItem
        Tag = 91
        AutoCheck = True
        Caption = #28165#38500#26435#38480
        GroupIndex = 2
        RadioItem = True
        OnClick = MenuItem7Click
      end
    end
  end
  object ABQuery1_3: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    MasterSource = ABDatasource1
    MasterFields = 'Ke_Guid'
    DetailFields = 'KC_Ke_Guid'
    SQL.Strings = (
      'select  * '
      'from ABSys_Right_KeyCustomObject'
      'where KC_Ke_Guid=:Ke_Guid')
    SqlUpdateDatetime = 42199.689045486110000000
    BeforeDeleteAsk = False
    MaxRecordCount = 0
    EditControlType = ctSingle
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ABSys_Right_KeyCustomObject')
    IndexListDefs = <>
    LoadTables.Strings = (
      'ABSys_Right_KeyCustomObject')
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 27
    Top = 254
    ParamData = <
      item
        Name = 'Ke_Guid'
        ParamType = ptInput
      end>
  end
  object ABQuery1_3Base: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    FilterOptions = [foCaseInsensitive]
    SQL.Strings = (
      
        'select Ti_Varchar1 TI_ParentGuid,Ti_Guid,TI_Code,TI_Name ,cast(1' +
        ' as bit)  IsFunc'
      'from ABSys_Org_TreeItem'#9#9'  '
      'where Ti_TR_Guid=dbo.Func_GetTreeGuid('#39'CustomObject'#39') '
      'union'
      
        'select TI_ParentGuid,TI_Code,TI_Code,TI_Name ,cast(0 as bit)  Is' +
        'Func'
      'from ABSys_Org_TreeItem'#9#9'  '
      'where Ti_TR_Guid=dbo.Func_GetTreeGuid('#39'CustomObjectType'#39') '
      ''
      '     ')
    SqlUpdateDatetime = 42199.700307060190000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    EditControlType = ctSingle
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ABSys_Org_TreeItem')
    IndexListDefs = <>
    LoadTables.Strings = (
      'ABSys_Org_TreeItem')
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 112
    Top = 264
  end
  object ABQuery1: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    AfterScroll = ABQuery1AfterScroll
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    SQL.Strings = (
      'select * from ABSys_Org_Key order by KE_Order'
      '')
    SqlUpdateDatetime = 42451.273317372690000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    EditControlType = ctSingle
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ABSys_Org_Key')
    IndexListDefs = <>
    LoadTables.Strings = (
      'ABSys_Org_Key')
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 27
    Top = 68
  end
  object ABQuery_TableDir: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    FilterOptions = [foCaseInsensitive]
    SQL.Strings = (
      'select *'
      'from '
      '('
      
        'select Ti_ParentGuid tempParentGuid,Ti_Guid tempGuid,ti_Code tem' +
        'pCode, Ti_Name tempName ,0 tempType,0 tempLevel,Tr_Order,Ti_Orde' +
        'r '
      
        'from ABSys_Org_Tree left join ABSys_Org_TreeItem on Ti_Tr_Guid=T' +
        'r_Guid'
      'where tr_code='#39'Table Dir'#39
      'union'
      
        'select Ta_Ti_Guid ,Ta_Guid ,ta_Name , Ta_Caption  ,1         ,0,' +
        '0       ,Ta_Order '
      'from ABSys_Org_table'
      ')  aa  '
      'order by tempLevel,Tr_Order,Ti_Order')
    SqlUpdateDatetime = 42090.502446261570000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    EditControlType = ctSingle
    UseTrans = False
    UpdateDatabase = False
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'absys_Org_Table')
    IndexListDefs = <>
    LoadTables.Strings = (
      'absys_Org_Table')
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 248
    Top = 184
  end
  object ABQuery1_5: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    MasterSource = ABDatasource1
    MasterFields = 'Ke_Guid'
    DetailFields = 'KF_Ke_Guid'
    SQL.Strings = (
      'select  *  '
      'from ABSys_Right_KeyField '
      'where KF_Ke_Guid=:Ke_Guid'
      '')
    SqlUpdateDatetime = 42199.689191608800000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    EditControlType = ctSingle
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ABSys_Right_KeyField')
    IndexListDefs = <>
    LoadTables.Strings = (
      'ABSys_Right_KeyField')
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 24
    Top = 383
    ParamData = <
      item
        Name = 'Ke_Guid'
        ParamType = ptInput
      end>
  end
  object ABDatasource1_5: TABDatasource
    AutoEdit = False
    DataSet = ABQuery1_5
    Left = 116
    Top = 392
  end
  object ABQuery1_4_2: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    SqlUpdateDatetime = 42198.489013275480000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    EditControlType = ctSingle
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    IndexListDefs = <>
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 496
    Top = 351
  end
  object ABDatasource1_4_2: TABDatasource
    AutoEdit = False
    DataSet = ABQuery1_4_2
    Left = 580
    Top = 360
  end
end
