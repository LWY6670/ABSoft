unit ABSys_Org_AboutU;
                                        
interface
                              
uses
  ABPubVarU,
  ABPubUserU,
  ABPubFuncU,
  ABPubConstU,
  ABPubDBU,
  ABPubPanelU,
  ABPubPassU,

  ABThirdQueryU,
  ABThirdConnU,
  ABThirdDBU,
  ABThirdCustomQueryU,
  ABThirdFuncU,

  ABFrameworkQueryU,
  ABFrameworkFuncU,
  ABFrameworkFuncFormU,
  ABFrameworkControlU,

  cxLookAndFeels,
  cxGraphics,
  cxImage,
  cxMemo,
  cxTextEdit,
  cxEdit,
  cxContainer,
  cxControls,
  cxButtons,
  cxLookAndFeelPainters,

  Windows,Messages,SysUtils,Variants,Classes,Graphics,Controls,Forms,DB,Dialogs,
  StdCtrls,Buttons,ExtCtrls,Menus,jpeg, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, ABFrameworkDictionaryQueryU, cxDBEdit;
                                                     
type
  TABSys_Org_AboutForm = class(TABFuncForm)
    SpeedButton1: TABcxButton;
    Label2: TLabel;
    Label3: TLabel;
    Label6: TLabel;
    Memo1: TABcxMemo;
    Label5: TLabel;
    Label7: TLabel;
    Label4: TLabel;
    Label1: TLabel;
    ABcxDBImage1: TABcxDBImage;
    ABDatasource1: TABDatasource;
    ABQuery1: TABQuery;
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ABSys_Org_AboutForm: TABSys_Org_AboutForm;

implementation

{$R *.dfm}
//框架注册代码
procedure ABRegister(var aFormClassName:string);stdcall;
begin
  aFormClassName  :=TABSys_Org_AboutForm.ClassName;
end;

exports
   ABRegister ;
//框架注册代码


procedure TABSys_Org_AboutForm.FormCreate(Sender: TObject);
var
  tempDataset:TDataSet;
  tempSubsequentClientCount:longint;
begin
  tempDataset:=ABGetConstSqlPubDataset('ABSys_Org_Parameter',[]);
  
  Label1.Caption:=ABGetFieldValue(tempDataset,['Pa_Name'],['SoftName'],['Pa_Value'],'');
  Label4.Caption:=ABGetFieldValue(tempDataset,['Pa_Name'],['VersionNo'],['Pa_Value'],'');
  Label2.Caption:=ABGetFieldValue(tempDataset,['Pa_Name'],['Developer'],['Pa_Value'],'');
  Label3.Caption:=ABGetFieldValue(tempDataset,['Pa_Name'],['DevelopDateTime'],['Pa_Value'],'');

  Memo1.Text:=ABTransactQL(ABGetFieldValue(tempDataset,['Pa_Name'],['CopyRight'],['Pa_Value'],''),[]);
  if Memo1.Lines.Count>9 then
  begin
    Memo1.Properties.ScrollBars:=ssVertical;
  end
  else
  begin
    Memo1.Properties.ScrollBars:=ssNone;
  end;

  Label6.Caption:=ABGetFieldValue(tempDataset,['Pa_Name'],['RegName'],['Pa_Value'],'');

  ABInitFormDataSet([],[],[],ABQuery1);

  if (ABCheckLicense(ABPublicSetPath+'License.key',ABGetFieldValue(tempDataset,['Pa_Name'],['RegName'],['Pa_Value'],''),now,tempSubsequentClientCount)) then
  begin
    Memo1.Lines.Add(' ');
    if ABPubUser.UseBeginDateTime>0 then
      Memo1.Lines.Add('用户开通时间:'+ABDateTimeToStr(ABPubUser.UseBeginDateTime));
    if ABPubUser.UseEndDateTime>0 then
      Memo1.Lines.Add('用户失效时间:'+ABDateTimeToStr(ABPubUser.UseEndDateTime));
    if ABPubUser.PassWordEndDateTime>0 then
      Memo1.Lines.Add('密码失效时间:'+ABDateTimeToStr(ABPubUser.PassWordEndDateTime));
    if ABPubUser.LicenseBeginDateTime>0 then
      Memo1.Lines.Add('授权开通时间:'+ABDateTimeToStr(ABPubUser.LicenseBeginDateTime));
    if ABPubUser.LicenseEndDateTime>0 then
      Memo1.Lines.Add('授权失效时间:'+ABDateTimeToStr(ABPubUser.LicenseEndDateTime));
  end;
end;

procedure TABSys_Org_AboutForm.SpeedButton1Click(Sender: TObject);
begin
  close;
end;

//框架注册代码
Initialization
  RegisterClass(TABSys_Org_AboutForm);
Finalization
  UnRegisterClass(TABSys_Org_AboutForm);

end.
