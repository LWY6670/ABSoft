object ABSys_Approved_MessageForm: TABSys_Approved_MessageForm
  Left = 173
  Top = 129
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  Anchors = [akLeft, akTop, akRight]
  Caption = #23457#25209#28040#24687#31649#29702
  ClientHeight = 550
  ClientWidth = 750
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object ABDBStatusBar1: TABdxDBStatusBar
    Left = 0
    Top = 531
    Width = 750
    Height = 19
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Width = 50
      end>
    PaintStyle = stpsUseLookAndFeel
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    StopUpdate = False
  end
  object ABcxPageControl1: TABcxPageControl
    Left = 0
    Top = 27
    Width = 750
    Height = 504
    Align = alClient
    TabOrder = 1
    Properties.ActivePage = cxTabSheet1
    Properties.CustomButtons.Buttons = <>
    LookAndFeel.Kind = lfFlat
    LookAndFeel.NativeStyle = False
    OnChange = ABcxPageControl1Change
    ActivePageIndex = 0
    ClientRectBottom = 503
    ClientRectLeft = 1
    ClientRectRight = 749
    ClientRectTop = 21
    object cxTabSheet1: TcxTabSheet
      Caption = #26410#23457#25209#28040#24687
      ImageIndex = 0
      object ABDBcxGrid1: TABcxGrid
        Left = 0
        Top = 0
        Width = 748
        Height = 444
        Align = alClient
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        LookAndFeel.NativeStyle = False
        object ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView
          PopupMenu.AutoHotkeys = maManual
          PopupMenu.CloseFootStr = False
          PopupMenu.LinkTableView = ABcxGridDBBandedTableView1
          PopupMenu.AutoApplyBestFit = True
          PopupMenu.AutoCreateAllItem = True
          OnDblClick = ABcxGridDBBandedTableView1DblClick
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = ABDatasource1
          DataController.Filter.Options = [fcoCaseInsensitive]
          DataController.Filter.AutoDataSetFilter = True
          DataController.Filter.TranslateBetween = True
          DataController.Filter.TranslateIn = True
          DataController.Filter.TranslateLike = True
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsBehavior.AlwaysShowEditor = True
          OptionsBehavior.FocusCellOnTab = True
          OptionsBehavior.GoToNextCellOnEnter = True
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.DataRowSizing = True
          OptionsData.Deleting = False
          OptionsData.Inserting = False
          OptionsSelection.CellSelect = False
          OptionsSelection.HideFocusRectOnExit = False
          OptionsSelection.MultiSelect = True
          OptionsView.CellAutoHeight = True
          OptionsView.GroupByBox = False
          OptionsView.BandHeaders = False
          Bands = <
            item
            end>
          ExtPopupMenu.AutoHotkeys = maManual
          ExtPopupMenu.CloseFootStr = False
          ExtPopupMenu.LinkTableView = ABcxGridDBBandedTableView1
          ExtPopupMenu.AutoApplyBestFit = True
          ExtPopupMenu.AutoCreateAllItem = True
        end
        object cxGridLevel1: TcxGridLevel
          GridView = ABcxGridDBBandedTableView1
        end
      end
      object Panel2: TPanel
        Left = 0
        Top = 444
        Width = 748
        Height = 38
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 1
        DesignSize = (
          748
          38)
        object Button1: TABcxButton
          Left = 665
          Top = 6
          Width = 75
          Height = 25
          Anchors = [akTop, akRight]
          Caption = #19981#21516#24847
          LookAndFeel.Kind = lfFlat
          TabOrder = 1
          OnClick = Button1Click
          ShowProgressBar = False
        end
        object Button2: TABcxButton
          Left = 585
          Top = 6
          Width = 75
          Height = 25
          Anchors = [akTop, akRight]
          Caption = #21516#24847
          Default = True
          LookAndFeel.Kind = lfFlat
          TabOrder = 0
          OnClick = Button2Click
          ShowProgressBar = False
        end
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = #24050#23457#25209#28040#24687
      ImageIndex = 1
      object ABDBcxGrid2: TABcxGrid
        Left = 0
        Top = 0
        Width = 748
        Height = 482
        Align = alClient
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        LookAndFeel.NativeStyle = False
        object ABcxGridDBBandedTableView2: TABcxGridDBBandedTableView
          PopupMenu.AutoHotkeys = maManual
          PopupMenu.CloseFootStr = False
          PopupMenu.LinkTableView = ABcxGridDBBandedTableView2
          PopupMenu.AutoApplyBestFit = True
          PopupMenu.AutoCreateAllItem = True
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = ABDatasource2
          DataController.Filter.Options = [fcoCaseInsensitive]
          DataController.Filter.AutoDataSetFilter = True
          DataController.Filter.TranslateBetween = True
          DataController.Filter.TranslateIn = True
          DataController.Filter.TranslateLike = True
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsBehavior.AlwaysShowEditor = True
          OptionsBehavior.FocusCellOnTab = True
          OptionsBehavior.GoToNextCellOnEnter = True
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.DataRowSizing = True
          OptionsData.Deleting = False
          OptionsData.Inserting = False
          OptionsSelection.CellSelect = False
          OptionsSelection.HideFocusRectOnExit = False
          OptionsSelection.MultiSelect = True
          OptionsView.CellAutoHeight = True
          OptionsView.GroupByBox = False
          OptionsView.BandHeaders = False
          Bands = <
            item
            end>
          ExtPopupMenu.AutoHotkeys = maManual
          ExtPopupMenu.CloseFootStr = False
          ExtPopupMenu.LinkTableView = ABcxGridDBBandedTableView2
          ExtPopupMenu.AutoApplyBestFit = True
          ExtPopupMenu.AutoCreateAllItem = True
        end
        object cxGridLevel2: TcxGridLevel
          GridView = ABcxGridDBBandedTableView2
        end
      end
    end
  end
  object ABDBNavigator2: TABDBNavigator
    Left = 0
    Top = 0
    Width = 750
    Height = 27
    Align = alTop
    BevelOuter = bvNone
    ShowCaption = False
    TabOrder = 2
    BigGlyph = False
    ImageLayout = blGlyphLeft
    DataSource = ABDatasource2
    VisibleButtons = [nbQuerySpacer, nbQuery, nbReport]
    ButtonRangeType = RtCustom
    BtnCustom1ImageIndex = -1
    BtnCustom2ImageIndex = -1
    BtnCustom3ImageIndex = -1
    BtnCustom4ImageIndex = -1
    BtnCustom5ImageIndex = -1
    BtnCustom6ImageIndex = -1
    BtnCustom7ImageIndex = -1
    BtnCustom8ImageIndex = -1
    BtnCustom9ImageIndex = -1
    BtnCustom10ImageIndex = -1
    BtnCustom1Caption = #26597#30475#21407#22987#21333#25454
    BtnCustom2Caption = #33258#23450#20041'2'
    BtnCustom3Caption = #33258#23450#20041'3'
    BtnCustom4Caption = #33258#23450#20041'4'
    BtnCustom5Caption = #33258#23450#20041'5'
    BtnCustom6Caption = #33258#23450#20041'6'
    BtnCustom7Caption = #33258#23450#20041'7'
    BtnCustom8Caption = #33258#23450#20041'8'
    BtnCustom9Caption = #33258#23450#20041'9'
    BtnCustom10Caption = #33258#23450#20041'10'
    BtnCustom1Kind = cxbkStandard
    BtnCustom2Kind = cxbkStandard
    BtnCustom3Kind = cxbkStandard
    BtnCustom4Kind = cxbkStandard
    BtnCustom5Kind = cxbkStandard
    BtnCustom6Kind = cxbkStandard
    BtnCustom7Kind = cxbkStandard
    BtnCustom8Kind = cxbkStandard
    BtnCustom9Kind = cxbkStandard
    BtnCustom10Kind = cxbkStandard
    ApprovedRollbackButton = nbCustom1
    ApprovedCommitButton = nbCustom1

  end
  object ABQuery1: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    FilterOptions = [foCaseInsensitive]
    SQL.Strings = (
      ' select *  '
      'from ABSys_Approved_Message '
      'where Me_State='#39'WaitFor'#39'  and ME_ApprovedOp_Guid='#39'[ABUser_Guid]'#39)
    SqlUpdateDatetime = 42230.868603159720000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ABSys_Approved_Message')
    IndexListDefs = <>
    LoadTables.Strings = (
      'ABSys_Approved_Message')
    
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 10
    Top = 190
  end
  object ABDatasource1: TABDatasource
    AutoEdit = False
    DataSet = ABQuery1
    Left = 51
    Top = 190
  end
  object ABQuery2: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    FilterOptions = [foCaseInsensitive]
    SQL.Strings = (
      ' select *  '
      'from ABSys_Approved_Message'
      
        'where Me_State in ('#39'Approve'#39' ,'#39'Dissent'#39')  and ME_ApprovedOp_Guid' +
        '='#39'[ABUser_Guid]'#39)
    SqlUpdateDatetime = 42230.868689606480000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ABSys_Approved_Message')
    IndexListDefs = <>
    LoadTables.Strings = (
      'ABSys_Approved_Message')
    
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 10
    Top = 225
  end
  object ABDatasource2: TABDatasource
    AutoEdit = False
    DataSet = ABQuery2
    Left = 51
    Top = 228
  end
end
