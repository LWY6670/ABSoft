{
框架操作传入SQL数据单元
}
unit ABSys_Approved_Message_ShowBillDataU;

interface
{$I ..\ABInclude.ini}


uses
  ABPubFormU,
  ABPubFuncU,
  ABPubConstU,
  ABPubUserU,
  ABPubDBU,
  ABPubPanelU,

  ABThirdQueryU,
  ABThirdCustomQueryU,

  ABFrameworkConstU,
  ABFrameworkControlU,
  ABFrameworkcxGridU,
  ABFrameworkQueryAllFieldComboxU,
  ABFrameworkDictionaryQueryU,
  ABFrameworkUserU,
  ABFrameworkFuncU,
  ABFrameworkDBPanelU,
  ABFrameworkQueryU,
  ABFrameworkDBNavigatorU,

  cxGridLevel,

  types,
  SysUtils,Variants,Classes,Controls,Forms,DB,StdCtrls,ExtCtrls,
  cxCustomData,
  cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxGrid, cxPC,
  cxSplitter, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxBarBuiltInMenu, Vcl.Menus, cxButtons,
  ABPubMultilingualDBNavigatorU, cxContainer, cxEdit, cxGroupBox, cxRadioGroup,
  cxStyles, cxFilter, cxData, cxDataStorage, cxNavigator, cxTL, cxMaskEdit,
  cxTLdxBarBuiltInMenu, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, ABThirdDBU, cxInplaceContainer, cxDBTL, cxTLData,
  cxClasses, cxGridCustomView, dxStatusBar;

type
  TABSys_Approved_Message_ShowBillDataForm = class(TABPubForm)
    Sheet1_Panel1: TPanel;
    Sheet1_Splitter1: TABcxSplitter;
    Sheet1_Grid1: TABcxGrid;
    Sheet1_TableView1: TABcxGridDBBandedTableView;
    Sheet1_Level1: TcxGridLevel;
    Sheet1_Panel2: TPanel;
    Sheet1_Main_Panel1: TABDBPanel;
    ABQuery1: TABQuery;
    ABDatasource1: TABDatasource;
    ABQuery1_1: TABQuery;
    ABDatasource1_1: TABDatasource;
    Sheet1_Detail_Grid1: TABcxGrid;
    Sheet1_Detail_TableView1: TABcxGridDBBandedTableView;
    Sheet1_Detail_Level1: TcxGridLevel;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

//显示单据
procedure ABShowBill(
                     aCaption:string;

                     aMainConnName:string;          //主SQL连接名
                     aMainSQL:string;               //主SQL

                     aDetailConnName:string;        //从SQL连接名
                     aDetailSQL:string;             //从SQL

                     aDetailMasterField:string;     //从SQL连接主字段
                     aDetailDetailField:string      //从SQL连接从字段
                     );

implementation

{$R *.dfm}

procedure ABShowBill(
                     aCaption:string;

                     aMainConnName:string;          //主SQL连接名
                     aMainSQL:string;               //主SQL

                     aDetailConnName:string;        //从SQL连接名
                     aDetailSQL:string;             //从SQL

                     aDetailMasterField:string;     //从SQL连接主字段
                     aDetailDetailField:string      //从SQL连接从字段
                     );
var
  tempForm:TABSys_Approved_Message_ShowBillDataForm;
begin
  tempForm := TABSys_Approved_Message_ShowBillDataForm.Create(nil);
  try
    if aCaption<>EmptyStr then
      tempForm.Caption:= aCaption;

    tempForm.ABQuery1.ConnName:=aMainConnName;
    tempForm.ABQuery1.SQL.Text:=aMainSQL;

    if aDetailSQL<>EmptyStr then
    begin
      tempForm.ABQuery1_1.ConnName:=aDetailConnName;
      tempForm.ABQuery1_1.SQL.Text:=aDetailSQL;
      tempForm.ABQuery1_1.MasterFields:=aDetailMasterField;
      tempForm.ABQuery1_1.DetailFields:=aDetailDetailField;
    end;

    tempForm.Sheet1_TableView1.ExtPopupMenu.SetupFileNameSuffix:=tempForm.Caption;
    tempForm.Sheet1_Detail_TableView1.ExtPopupMenu.SetupFileNameSuffix:='Detail_'+tempForm.Caption;
    tempForm.ShowModal;
  finally
    tempForm.Free;
  end;
end;

procedure TABSys_Approved_Message_ShowBillDataForm.FormShow(Sender: TObject);
begin
  ABInitFormDataSet([Sheet1_Main_Panel1],[Sheet1_TableView1],[],TABDictionaryQuery(ABDatasource1.DataSet),true,false,true);
  if ABQuery1_1.SQL.Text<>EmptyStr then
  begin
    ABInitFormDataSet([],[Sheet1_Detail_TableView1],[],TABDictionaryQuery(ABDatasource1_1.DataSet),true,false,true);
  end
  else
  begin
    Sheet1_Panel2.Visible:=False;
    Sheet1_Splitter1.Visible:=False;
  end;
end;

end.





