unit ABSys_Log_FieldChangeU;

interface

uses
  ABPubFuncU,
  ABPubPanelU,
  ABPubDBU,

  ABThirdQueryU,
  ABThirdConnU,
  ABThirdDBU,
  ABThirdCustomQueryU,

  ABFrameworkQuerySelectFieldPanelU,
  ABFrameworkcxGridU,
  ABFrameworkDictionaryQueryU,
  ABFrameworkDBNavigatorU,
  ABFrameworkFuncFormU,
  ABFrameworkControlU,
  ABFrameworkQueryU,
  ABFrameworkFuncU,

  cxDateUtils,
  cxLookAndFeels,
  cxMemo,
  cxTextEdit,
  cxContainer,
  cxLabel,
  cxCalendar,
  cxDropDownEdit,
  cxMaskEdit,
  cxButtons,
  cxLookAndFeelPainters,
  cxGridDBBandedTableView,
  cxGridBandedTableView,
  cxGrid,
  cxGridDBTableView,
  cxGridTableView,
  cxGridCustomTableView,
  cxGridCustomView,
  cxClasses,
  cxGridLevel,
  cxDBData,
  cxEdit,
  cxDataStorage,
  cxData,
  cxFilter,
  cxGraphics,
  cxCustomData,
  cxStyles,
  cxDBNavigator,
  cxNavigator,
  cxControls,
  cxPC,
  dxCore,
  dxStatusBar,

  Windows,Messages,SysUtils,Variants,Classes,Graphics,Controls,Forms,Dialogs,DB,
  DBClient,ExtCtrls,ComCtrls,StdCtrls,DBCtrls,Menus, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TABSys_Log_FieldChangeForm = class(TABFuncForm)
    ABQuery1: TABQuery;
    ABDatasource1: TABDatasource;
    ABDBStatusBar1: TABdxDBStatusBar;
    ABDBNavigator1: TABDBNavigator;
    ABDBcxGrid1: TABcxGrid;
    ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView;
    cxGridLevel1: TcxGridLevel;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ABSys_Log_FieldChangeForm: TABSys_Log_FieldChangeForm;

implementation

{$R *.dfm}

procedure ABRegister(var aFormClassName:string);stdcall;
begin
  aFormClassName  :=TABSys_Log_FieldChangeForm.ClassName;
end;

exports
   ABRegister ;

procedure TABSys_Log_FieldChangeForm.FormCreate(Sender: TObject);
begin
  ABInitFormDataSet([],[ABcxGridDBBandedTableView1],[],ABQuery1,false);
end;

Initialization
  RegisterClass(TABSys_Log_FieldChangeForm);

Finalization
  UnRegisterClass(TABSys_Log_FieldChangeForm);

end.
