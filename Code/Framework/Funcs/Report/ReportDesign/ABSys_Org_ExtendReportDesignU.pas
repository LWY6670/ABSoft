unit ABSys_Org_ExtendReportDesignU;
                               
interface      

uses
  ABPubLanguageU,
  ABPubSelectStrU,
  ABPubVarU,
  ABPubUserU,
  ABPubFuncU,
  ABPubConstU,
  ABPubDBU,
  ABPubPanelU,
  ABPubInPutStrU,
  ABPubPassU,
  ABPubMessageU,

  ABThirdQueryU,
  ABThirdConnU,
  ABThirdDBU,
  ABThirdCustomQueryU,
  ABThirdFuncU,

  ABFrameworkVarU,
  ABFrameworkcxGridU,
  ABFrameworkDBPanelU,
  ABFrameworkDictionaryQueryU,
  ABFrameworkFastReportU,
  ABFrameworkFuncFormU,
  ABFrameworkControlU,
  ABFrameworkQueryU,
  ABFrameworkFuncU,

  cxTLdxBarBuiltInMenu,
  cxLookAndFeels,
  cxDBPivotGrid,
  cxCustomPivotGrid,
  cxSplitter,
  cxListBox,
  cxTLData,
  cxDBTL,
  cxInplaceContainer,
  cxTL,
  cxCheckListBox,
  cxRadioGroup,
  cxCheckGroup,
  cxGroupBox,
  cxMemo,
  cxCalendar,
  cxDropDownEdit,
  cxMaskEdit,
  cxDBEdit,
  cxTextEdit,
  cxButtons,
  cxLookAndFeelPainters,
  cxGrid,
  cxGridDBBandedTableView,
  cxGridBandedTableView,
  cxGridTableView,
  cxGridCustomTableView,
  cxGridCustomView,
  cxClasses,
  cxGridLevel,
  cxDBData,
  cxDataStorage,
  cxData,
  cxFilter,
  cxGraphics,
  cxCustomData,
  cxStyles,
  cxControls,
  cxPC,
  cxLabel,
  cxEdit,
  cxContainer,
  dxStatusBar,
  dxdbtree,
  dxtree,

  Types,Windows,Messages,SysUtils,Variants,Classes,Graphics,Controls,Forms,
  ComCtrls,ExtCtrls,DB,DBClient,Menus,StdCtrls,Grids,DBGrids,TypInfo,Math,frxClass,
  frxDBSet, cxNavigator, dxBarBuiltInMenu, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TABSys_Org_ExtendReportDesignForm = class(TABFuncForm)
    Panel1: TPanel;
    ABDatasource1: TABDatasource;
    ABQuery1: TABQuery;
    Panel2: TPanel;
    Panel6: TPanel;
    Panel7: TPanel;
    Splitter4: TABcxSplitter;
    Panel811: TPanel;
    Splitter5: TABcxSplitter;
    ABDBcxGrid1: TABcxGrid;
    ABDBcxGrid1DBBandedTableView1: TABcxGridDBBandedTableView;
    ABDBcxGrid1Level1: TcxGridLevel;
    ABcxPageControl1: TABcxPageControl;
    ABQuery1_1: TABQuery;
    ABDatasource1_1: TABDatasource;
    cxTabSheet2: TcxTabSheet;
    ABQuery1_1_1: TABQuery;
    ABDatasource1_1_1: TABDatasource;
    ABDBcxGrid2: TABcxGrid;
    ABQuery2: TABQuery;
    ABDatasource2: TABDatasource;
    ABcxButton1: TABcxButton;
    ABcxButton2: TABcxButton;
    ABcxButton3: TABcxButton;
    ABcxButton4: TABcxButton;
    cxTabSheet1: TcxTabSheet;
    Splitter1: TABcxSplitter;
    Button3: TABcxButton;
    Panel3: TPanel;
    Panel9: TPanel;
    ListBox1: TABcxListBox;
    Panel10: TPanel;
    Splitter2: TABcxSplitter;
    Panel11: TPanel;
    Panel12: TPanel;
    ABDBcxGrid2Level1: TcxGridLevel;
    ABDBcxGrid2ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView;
    ABcxDBTreeView1: TABcxDBTreeView;
    ABcxDBTreeView1cxDBTreeListColumn1: TcxDBTreeListColumn;
    ABcxDBTreeView2: TABcxDBTreeView;
    cxDBTreeListColumn1: TcxDBTreeListColumn;
    ABcxDBTreeView1cxDBTreeListColumn2: TcxDBTreeListColumn;
    ABcxDBTreeView2cxDBTreeListColumn1: TcxDBTreeListColumn;
    ABcxButton5: TABcxButton;
    ABcxSplitter1: TABcxSplitter;
    ABDBStatusBar1: TABdxDBStatusBar;
    cxTabSheet3: TcxTabSheet;
    ABcxDBPivotGrid1: TABcxDBPivotGrid;
    PopupMenu2: TPopupMenu;
    MenuItem1: TMenuItem;
    N7: TMenuItem;
    Panel8: TPanel;
    ABcxPageControl2: TABcxPageControl;
    cxTabSheet4: TcxTabSheet;
    cxTabSheet5: TcxTabSheet;
    Memo1: TABcxMemo;
    ABDBPanel1: TABDBPanel;
    Memo2: TABcxMemo;
    procedure FormCreate(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure ABcxButton3Click(Sender: TObject);
    procedure ABcxButton4Click(Sender: TObject);
    procedure ABcxPageControl1Change(Sender: TObject);
    procedure ListBox1DblClick(Sender: TObject);
    procedure Memo1Exit(Sender: TObject);
    procedure ABcxButton1Click(Sender: TObject);
    procedure ABcxButton2Click(Sender: TObject);
    procedure ABcxDBTreeView2Click(Sender: TObject);
    procedure ABcxDBTreeView2DblClick(Sender: TObject);
    procedure ABQuery1_1AfterScroll(DataSet: TDataSet);
    procedure ABcxButton5Click(Sender: TObject);
    procedure ABQuery1AfterInsert(DataSet: TDataSet);
    procedure MenuItem1Click(Sender: TObject);
    procedure ABQuery1_1ABAfterApplyUpdates(aUpdateKind: TUpdateKind);
    procedure ABQuery1AfterScroll(DataSet: TDataSet);
  private
    FExtendReportTypeGuid:string;
    procedure UpdateState;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ABSys_Org_ExtendReportDesignForm: TABSys_Org_ExtendReportDesignForm;

implementation



{$R *.dfm}


procedure ABRegister(var aFormClassName:string);stdcall;
begin
  aFormClassName  :=TABSys_Org_ExtendReportDesignForm.ClassName;
end;

exports
   ABRegister;


procedure TABSys_Org_ExtendReportDesignForm.FormCreate(Sender: TObject);
begin
  FExtendReportTypeGuid:=ABGetConstSqlPubDataset('ABSys_Org_Tree',['Extend ReportType']).fieldbyname('Tr_Guid').asstring;
  ABInitFormDataSet([],[],[],ABQuery1);
  ABcxDBTreeView1.DragMode:=dmManual;
  ABInitFormDataSet([],[ABDBcxGrid1DBBandedTableView1],[],ABQuery1_1);
  ABInitFormDataSet([ABDBPanel1],[],[],ABQuery1_1_1);
  ABQuery1_1AfterScroll(ABQuery1_1);

  ABcxPageControl1.ActivePageIndex:=0;
  ABcxPageControl2.ActivePageIndex:=0;
  ABcxPageControl1Change(ABcxPageControl1);

  ABExecBeforeReportSQL;


  ABFillTreeItemByTr_Code('DBControl',['Ti_Varchar1'],ListBox1.Items);

end;

procedure TABSys_Org_ExtendReportDesignForm.Button3Click(Sender: TObject);
begin
  Close;
end;

procedure TABSys_Org_ExtendReportDesignForm.ABcxButton3Click(Sender: TObject);
var
  tempName:string;
begin
  tempName:=ABQuery1.FieldByName('Ti_Name').AsString+inttostr(ABQuery1_1.RecordCount+1);
  if (ABInPutStr('报表名称',tempName)) and
     (tempName<>EmptyStr) then
  begin
    ABQuery1_1.Append;
    ABQuery1_1.FieldByName('Er_Name').AsString:=tempName;
    ABQuery1_1.Post;

    ABQuery1_1_1.Append;
    ABQuery1_1_1.FieldByName('Ed_ParentGuid').AsString:='0';
    ABQuery1_1_1.FieldByName('ED_Name').AsString:='新分类';
    ABQuery1_1_1.Post;
  end;
end;

procedure TABSys_Org_ExtendReportDesignForm.ABcxButton4Click(Sender: TObject);
begin
  ABQuery1_1.Delete;
end;

procedure TABSys_Org_ExtendReportDesignForm.ABcxButton5Click(Sender: TObject);
begin
  ABQuery1_1.Edit;
end;

procedure TABSys_Org_ExtendReportDesignForm.ABcxDBTreeView2Click(
  Sender: TObject);
begin
  if ABDatasetIsEmpty(ABQuery1_1_1) then
  begin
    memo1.text:=EmptyStr;
    memo1.Hint:='';
    exit;
  end
  else if memo1.Hint<>ABQuery1_1_1.FindField('Ed_Guid').AsString then
  begin
    memo1.text:=
      ABDownToTextFromField('Main',
                   'ABSys_Org_ExtendReportDatasets','Ed_SQL',
                   'Ed_Guid='+ABQuotedStr(ABQuery1_1_1.FindField('Ed_Guid').AsString));
    memo1.Hint:=ABQuery1_1_1.FindField('Ed_Guid').AsString;

    ABCreateInputParams(memo1.text,
                        Panel8,
                        Panel8.owner,
                        False,

                        4,
                        False,

                        0,true,
                        0,False,

                        ABDBPanelParams_Leftspacing    ,
                        ABDBPanelParams_Topspacing     ,
                        ABDBPanelParams_Righttspacing  ,
                        ABDBPanelParams_Bottomspacing  ,
                        ABDBPanelParams_Xspacing       ,
                        ABDBPanelParams_Yspacing
                        );
    UpdateState;
  end;
end;

procedure TABSys_Org_ExtendReportDesignForm.ABcxDBTreeView2DblClick(
  Sender: TObject);
var
  tempStr2:string;
begin
  if ABcxDBTreeView2.SelectionCount<=0 then
    exit;

  tempStr2:=Memo1.text;
  //if ABSQLBuild(tempStr2,nil) then
  begin
    if Memo1.text<>tempStr2 then
    begin
      Memo1.text:=tempStr2;
      ABUptextToField('Main',
                     'ABSys_Org_ExtendReportDatasets','Ed_SQL',
                     'Ed_Guid='+ABQuotedStr(ABQuery1_1_1.FindField('Ed_Guid').AsString),
                     Memo1.text);
    end;
  end;
end;

procedure TABSys_Org_ExtendReportDesignForm.ListBox1DblClick(Sender: TObject);
var
  i:longint;
begin
  if (not (ABQuery1_1.Active)) or (ABDatasetIsEmpty(ABQuery1_1)) then
    exit;
    
  i:=Memo1.SelStart;
  if Memo1.SelText<>emptystr then
  begin
    Memo1.Text:=
      copy(Memo1.Text,1,Memo1.SelStart)+
      copy(Memo1.Text,Memo1.SelStart+Memo1.SelLength+1,length(Memo1.Text));
  end;
  Memo1.SelStart:=i;
  Memo1.Text:=
    copy(Memo1.Text,1,Memo1.SelStart)+
      ' ' +ListBox1.Items[ListBox1.ItemIndex]+' '+
      copy(Memo1.Text,Memo1.SelStart+1,length(Memo1.Text));
  Memo1.SelStart:=i+length(' ' +ListBox1.Items[ListBox1.ItemIndex]+' ');

  Memo1.EditModified:=true;
  if Memo1.CanFocus then
    Memo1.SetFocus;
end;

procedure TABSys_Org_ExtendReportDesignForm.Memo1Exit(Sender: TObject);
begin
  if ABDatasetIsEmpty(ABQuery1_1_1) then
  begin
    ABShow('未增加数据源分类,请检查.');
    abort;
  end;

  if Memo1.EditModified then
  begin
    ABUptextToField('Main',
                   'ABSys_Org_ExtendReportDatasets','Ed_SQL',
                   'Ed_Guid='+ABQuotedStr(ABQuery1_1_1.FindField('Ed_Guid').AsString),
                   Memo1.text);
  end;
end;

procedure TABSys_Org_ExtendReportDesignForm.MenuItem1Click(Sender: TObject);
begin
  Memo1.Text:=ABSaveControlPositionToSQL(Memo1.Text,Panel8,ABIIF(Sender=N7,true,false));

  ABUptextToField('Main',
                 'ABSys_Org_ExtendReportDatasets','Ed_SQL',
                 'Ed_Guid='+ABQuotedStr(ABQuery1_1_1.FindField('Ed_Guid').AsString),
                 Memo1.Text);
end;

procedure TABSys_Org_ExtendReportDesignForm.UpdateState;
begin
  ABcxButton1.Enabled:=
    (not ABDatasetIsEmpty(ABQuery1_1)) and (not ABDatasetIsEmpty(ABQuery1_1_1));
  ABcxButton2.Enabled:=
    (not ABDatasetIsEmpty(ABQuery1_1)) and (not ABDatasetIsEmpty(ABQuery1_1_1));

  ABcxButton3.Enabled:=
    (not ABDatasetIsEmpty(ABQuery1)) and (ABQuery1_1.Active);
  ABcxButton4.Enabled:=
    (not ABDatasetIsEmpty(ABQuery1)) and (ABQuery1_1.Active);

  Memo1.Enabled:=
    (not ABDatasetIsEmpty(ABQuery1_1)) and (not ABDatasetIsEmpty(ABQuery1_1_1));
end;

procedure TABSys_Org_ExtendReportDesignForm.ABcxPageControl1Change(
  Sender: TObject);
var
  tempConnName,
  tempSql1,tempSetupFileNameSuffix:string;
  tempBoolean:boolean;
  tempControl:TControl;
begin
  if (ABcxPageControl1.ActivePageIndex=1) or
     (ABcxPageControl1.ActivePageIndex=2) then
  begin
    perform(WM_NEXTDLGCTL,0,0);

    tempSetupFileNameSuffix:=
          ABQuery1.FindField('Ti_Name').AsString+'_'+
          ABQuery1_1.FindField('Er_Name').AsString+'_'+
          ABQuery1_1_1.FindField('Ed_Name').AsString;
    tempSql1:=ABReplaceInputParamsValue(tempBoolean,tempControl,Memo1.text,Panel8,nil,true);

    if (tempSql1<>EmptyStr) and
       (tempSql1<>ABQuotedStr(EmptyStr)) then
    begin
      tempConnName:=ABGetConnNameByConnGuid(ABQuery1_1_1.FindField('ED_ConnName').AsString);
      tempSql1:=ABExecSQLExecPart(tempConnName,tempSql1,[]);
      if ABcxPageControl1.ActivePageIndex=1 then
      begin
        ABDBcxGrid2ABcxGridDBBandedTableView1.ExtPopupMenu.SetupFileNameSuffix:=tempSetupFileNameSuffix;
        ABDBcxGrid2ABcxGridDBBandedTableView1.ClearItems;

        ABQuery2.Close;
        ABQuery2.ConnName:=tempConnName;
        ABQuery2.Sql.Text:=tempSql1;
        ABQuery2.Open;

        TABcxGridDBBandedTableView(ABDBcxGrid2ABcxGridDBBandedTableView1).CreateAllColumn;
      end
      else if ABcxPageControl1.ActivePageIndex=2 then
      begin
        ABcxDBPivotGrid1.ExtPopupMenu.SetupFileNameSuffix:=tempSetupFileNameSuffix;
        ABcxDBPivotGrid1.DeleteAllFields;
        ABQuery2.Close;
        ABQuery2.ConnName:=tempConnName;
        ABQuery2.Sql.Text:=tempSql1;
        ABQuery2.Open;
        ABcxDBPivotGrid1.ExtPopupMenu.load;
        ABcxDBPivotGrid1.ExpandAll;
      end;
    end;
  end;
end;

procedure TABSys_Org_ExtendReportDesignForm.ABQuery1AfterInsert(
  DataSet: TDataSet);
begin
  ABQuery1.FieldByName('Ti_Tr_Guid').AsString:= FExtendReportTypeGuid ;
end;

procedure TABSys_Org_ExtendReportDesignForm.ABQuery1AfterScroll(
  DataSet: TDataSet);
begin
  ABQuery1_1AfterScroll(ABQuery1_1);
end;

procedure TABSys_Org_ExtendReportDesignForm.ABQuery1_1ABAfterApplyUpdates(
  aUpdateKind: TUpdateKind);
var
  tempER_NameOldValue:string;
begin
  if (ABQuery1_1.State =dsEdit) and
     (not ABQuery1_1_1.IsEmpty)     then
  begin
    if not ABComparisonFieldOldNewValueSame(ABQuery1_1.FieldByName('ER_Name'))  then
    begin
      tempER_NameOldValue:=vartostr(ABGetFieldOldValue(ABQuery1_1.FieldByName('ER_Name')));
      //设计中的表格与交叉表
      ABDBcxGrid2ABcxGridDBBandedTableView1.ExtPopupMenu.ChangeFileNameBySetupFileNameSuffix(
          name+'_'+ABDBcxGrid2ABcxGridDBBandedTableView1.Name+
          ABQuery1.FindField('Ti_Name').AsString+'_'+
            tempER_NameOldValue+'_'+
            ABQuery1_1_1.FindField('Ed_Name').AsString,
          name+'_'+ABDBcxGrid2ABcxGridDBBandedTableView1.Name+
          ABQuery1.FindField('Ti_Name').AsString+'_'+
            ABQuery1_1.FindField('Er_Name').AsString+'_'+
            ABQuery1_1_1.FindField('Ed_Name').AsString
      );
      ABcxDBPivotGrid1.ExtPopupMenu.ChangeFileNameBySetupFileNameSuffix(
          name+'_'+ABcxDBPivotGrid1.Name+
          ABQuery1.FindField('Ti_Name').AsString+'_'+
            tempER_NameOldValue+'_'+
            ABQuery1_1_1.FindField('Ed_Name').AsString,

          name+'_'+ABcxDBPivotGrid1.Name+
          ABQuery1.FindField('Ti_Name').AsString+'_'+
            ABQuery1_1.FindField('Er_Name').AsString+'_'+
            ABQuery1_1_1.FindField('Ed_Name').AsString
      );


      //打印中的表格与交叉表
      ABDBcxGrid2ABcxGridDBBandedTableView1.ExtPopupMenu.ChangeFileNameBySetupFileNameSuffix(
          'ABSys_Org_ExtendReportPrintForm'+'_'+'ABcxGridDBBandedTableView1'+
          ABQuery1.FindField('Ti_Name').AsString+'_'+
            tempER_NameOldValue+'_'+
            ABQuery1_1_1.FindField('Ed_Name').AsString,
          'ABSys_Org_ExtendReportPrintForm'+'_'+'ABcxGridDBBandedTableView1'+
          ABQuery1.FindField('Ti_Name').AsString+'_'+
            ABQuery1_1.FindField('Er_Name').AsString+'_'+
            ABQuery1_1_1.FindField('Ed_Name').AsString
      );

      ABcxDBPivotGrid1.ExtPopupMenu.ChangeFileNameBySetupFileNameSuffix(
          'ABSys_Org_ExtendReportPrintForm'+'_'+'ABcxDBPivotGrid1'+
          ABQuery1.FindField('Ti_Name').AsString+'_'+
            tempER_NameOldValue+'_'+
            ABQuery1_1_1.FindField('Ed_Name').AsString,
          'ABSys_Org_ExtendReportPrintForm'+'_'+'ABcxDBPivotGrid1'+
          ABQuery1.FindField('Ti_Name').AsString+'_'+
            ABQuery1_1.FindField('Er_Name').AsString+'_'+
            ABQuery1_1_1.FindField('Ed_Name').AsString
      );
    end;
  end;

end;

procedure TABSys_Org_ExtendReportDesignForm.ABQuery1_1AfterScroll(
  DataSet: TDataSet);
begin
  if not DataSet.Active then
    exit;

  ABcxButton4.Enabled:= (ABPubUser.IsAdmin) or (not ABQuery1_1.FindField('Er_SysUse').AsBoolean);
  ABcxButton5.Enabled:=ABcxButton4.Enabled;
  ABcxButton1.Enabled:=ABcxButton4.Enabled;
  cxTabSheet1.Enabled:=ABcxButton4.Enabled;

  ABcxDBTreeView2Click(ABcxDBTreeView2);
  ABcxSplitter1.Left:=Panel811.Left+Panel811.Width+10;
end;

//设计报表
procedure TABSys_Org_ExtendReportDesignForm.ABcxButton1Click(Sender: TObject);
begin
  ABPubFrxReport.Init;
  ABPubFrxReport.DefaultPrint :=ABQuery1_1.FindField('ER_DefaultPrint').AsString;
  ABPubFrxReport.CreateOwner:= self;
  ABPubFrxReport.DataSource:= nil;
  ABPubFrxReport.InputParamParent:= Panel8;
  ABPubFrxReport.QuoteParamParent:= nil;
  ABPubFrxReport.ReportType:= rtExtend;
  ABPubFrxReport.DesignReport(ABQuery1_1.FindField('Er_Guid').AsString);
end;

//打印报表
procedure TABSys_Org_ExtendReportDesignForm.ABcxButton2Click(Sender: TObject);
begin
  ABPubFrxReport.Init;
  ABPubFrxReport.DefaultPrint :=ABQuery1_1.FindField('ER_DefaultPrint').AsString;
  ABPubFrxReport.Preview:=nil;
  ABPubFrxReport.CreateOwner:= self;
  ABPubFrxReport.DataSource:= nil;
  ABPubFrxReport.InputParamParent:= Panel8;
  ABPubFrxReport.QuoteParamParent:= nil;
  ABPubFrxReport.ReportType:= rtExtend;
  ABPubFrxReport.PreviewReport(ABQuery1_1.FindField('Er_Guid').AsString);
end;

Initialization
  RegisterClass(TABSys_Org_ExtendReportDesignForm);
Finalization
  UnRegisterClass(TABSys_Org_ExtendReportDesignForm);


end.







