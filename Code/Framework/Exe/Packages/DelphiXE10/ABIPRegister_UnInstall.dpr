program ABIPRegister_UnInstall;

uses
  WinSvc,
  Windows,
  SysUtils,
  Forms,
  ABPubserviceU,
  ABPubFuncU;

{$R *.res}
var
  tempServiceName,tempFileName:string;
begin
  Application.Initialize;

  tempServiceName:='ABSoftIPRegister';
  tempFileName:=ExtractFilePath(Application.EXEName)+'ABIPRegisterServiceP.exe';

  //如果服务正在运行则停止
  if ABServiceIsRunning(tempServiceName) then
  begin
    ABStopService(tempServiceName);
  end;
  //如果服务已安装则卸载
  if ABServiceIsInstall(tempServiceName) then
  begin
    ABUninstallService(tempServiceName);
  end;

  Application.Run;
end.

