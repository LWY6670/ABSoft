program ABIPRegisterServiceP;

uses
  SvcMgr,
  ABPubVarU,
  ABIPRegisterU in '..\..\ABRegisterIP\ABIPRegisterU.pas' {ABSoftIPRegister: TService},
  ABIPRegister_RegThreadU in '..\..\ABRegisterIP\ABIPRegister_RegThreadU.pas';

{$R *.RES}

begin
  Application.Initialize;
  Application.Title := ABAppName+' IP Register';
  Application.CreateForm(TABSoftIPRegister, ABSoftIPRegister);
  Application.Run;
end.
