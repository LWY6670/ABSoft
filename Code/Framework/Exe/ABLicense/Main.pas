unit Main;

interface

uses
  ShowRightU,

  ABpubInPutPassWordU,
  abpubmessageU,
  ABPubFormU,
  ABPubFuncU,
  ABPubPassU,
  ABPubUserU,
  ABPubVarU,
  abpubconstU,
  ABPubCheckTreeViewU,
  ABPubInPutStrU,
  ABPubSelectComboBoxU,
  ABPubMemoU,
  ABPubDogKeyU,
  ABPubMultilingualDBNavigatorU,

  ABpubDBU,
  ABThirdDBU,


  Math,
  Windows,Messages,SysUtils,Variants,Classes,Graphics,Controls,Forms,Dialogs,Menus,
  StdCtrls,ExtCtrls,DB,ADODB,Grids,DBGrids,DBCtrls,Mask,DateUtils,ADOInt,
  Buttons, ComCtrls, cxGraphics, cxControls, cxLookAndFeels,
  cxGridExportLink,
  cxLookAndFeelPainters, cxContainer, cxEdit, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxNavigator, cxDBData, cxCalendar, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, cxMemo, cxDBEdit, cxCalc, cxTextEdit, cxMaskEdit,
  cxDropDownEdit;


type
  TABRegisterForm = class(TABPubForm)
    DataSource1_1: TDataSource;
    ADOQuery1_1: TADOQuery;
    ADOConnection1: TADOConnection;
    DataSource1_2: TDataSource;
    ADOQuery1_2: TADOQuery;
    ADOConnection2: TADOConnection;
    ADOQuery3: TADOQuery;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    DataSource1_21: TDataSource;
    ADOQuery1_21: TADOQuery;
    ADOQuery1_1ID: TAutoIncField;
    ADOQuery1_1Name: TWideStringField;
    ADOQuery1_1BegDatetime: TDateTimeField;
    ADOQuery1_1EndDatetime: TDateTimeField;
    ADOQuery1_1ClientInfo: TWideMemoField;
    ADOQuery1_1CardCount: TIntegerField;
    ADOQuery1_1ControlCount: TIntegerField;
    ADOQuery1_1V100Count: TIntegerField;
    ADOQuery1_1V200Count: TIntegerField;
    ADOQuery1_1V300Count: TIntegerField;
    ADOQuery1_1ReaderCount: TIntegerField;
    ADOQuery1_1TerminalCount: TIntegerField;
    ADOQuery1_1FuncRight: TWideMemoField;
    ADOQuery1_2ID: TAutoIncField;
    ADOQuery1_2BegDatetime: TDateTimeField;
    ADOQuery1_2EndDatetime: TDateTimeField;
    ADOQuery1_2ClientInfo: TWideMemoField;
    ADOQuery1_2CardCount: TIntegerField;
    ADOQuery1_2ControlCount: TIntegerField;
    ADOQuery1_2V100Count: TIntegerField;
    ADOQuery1_2V200Count: TIntegerField;
    ADOQuery1_2V300Count: TIntegerField;
    ADOQuery1_2ReaderCount: TIntegerField;
    ADOQuery1_2TerminalCount: TIntegerField;
    ADOQuery1_2FuncRight: TWideMemoField;
    ADOQuery1_2State: TWideStringField;
    ADOQuery1_2RegName: TWideStringField;
    ADOQuery1_2RegKey: TWideStringField;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    PageControl2: TPageControl;
    TabSheet3: TTabSheet;
    Panel3: TPanel;
    Label2: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    SpeedButton7: TSpeedButton;
    cxDBDateEdit3: TcxDBDateEdit;
    cxDBDateEdit4: TcxDBDateEdit;
    cxDBTextEdit3: TcxDBTextEdit;
    Panel5: TPanel;
    Panel6: TPanel;
    Button1: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Button11: TButton;
    ABCheckTreeView1: TABCheckTreeView;
    TabSheet4: TTabSheet;
    Label1: TLabel;
    Splitter3: TSplitter;
    Panel1: TPanel;
    Splitter1: TSplitter;
    Panel8: TPanel;
    ABCheckTreeView2: TABCheckTreeView;
    Panel9: TPanel;
    Label20: TLabel;
    Button7: TButton;
    Button8: TButton;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1Name: TcxGridDBColumn;
    cxGrid1DBTableView1BegDatetime: TcxGridDBColumn;
    cxGrid1DBTableView1EndDatetime: TcxGridDBColumn;
    cxGrid1DBTableView1FuncRight: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    ADOQuery1_21ListID: TIntegerField;
    ADOQuery1_21Datetime: TDateTimeField;
    ADOQuery1_21Remark: TWideMemoField;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    cxGridDBTableView1Datetime: TcxGridDBColumn;
    cxGridDBTableView1Remark: TcxGridDBColumn;
    ADOQuery1_1SubsequentClientCount: TIntegerField;
    ADOQuery1_2SubsequentClientCount: TIntegerField;
    cxGrid3: TcxGrid;
    cxGridDBTableView2: TcxGridDBTableView;
    cxGridLevel2: TcxGridLevel;
    cxGridDBTableView2ID: TcxGridDBColumn;
    cxGridDBTableView2RegName: TcxGridDBColumn;
    cxGridDBTableView2BegDatetime: TcxGridDBColumn;
    cxGridDBTableView2EndDatetime: TcxGridDBColumn;
    cxGridDBTableView2SubsequentClientCount: TcxGridDBColumn;
    cxGridDBTableView2FuncRight: TcxGridDBColumn;
    ADOQuery1_2Count: TIntegerField;
    cxGridDBTableView2Count: TcxGridDBColumn;
    pm1: TPopupMenu;
    MenuItem1: TMenuItem;
    ADOQuery1_1Remark: TWideMemoField;
    ADOQuery1_2Remark: TWideMemoField;
    cxGrid1DBTableView1ClientInfo: TcxGridDBColumn;
    cxGrid1DBTableView1SubsequentClientCount: TcxGridDBColumn;
    cxGridDBTableView2RegKey: TcxGridDBColumn;
    cxGridDBTableView2ClientInfo: TcxGridDBColumn;
    cxGridDBTableView2Remark: TcxGridDBColumn;
    lbl1: TLabel;
    cxDBCalcEdit1: TcxDBCalcEdit;
    lbl2: TLabel;
    cxDBMemo1: TcxDBMemo;
    lbl3: TLabel;
    cxDBMemo2: TcxDBMemo;
    btn1: TSpeedButton;
    btn3: TSpeedButton;
    Panel2: TPanel;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    Label3: TLabel;
    cxDBCalcEdit9: TcxDBCalcEdit;
    Label4: TLabel;
    cxDBMemo3: TcxDBMemo;
    ADOQuery1_1Price: TFloatField;
    ADOQuery1_2Price: TFloatField;
    ADOQuery1_2CustomFunc: TWideMemoField;
    Panel4: TPanel;
    cxDBMemo4: TcxDBMemo;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    cxGridDBTableView2Column1: TcxGridDBColumn;
    cxGridDBTableView2Column2: TcxGridDBColumn;
    ADOQuery1_1CustomFunc: TWideMemoField;
    Panel7: TPanel;
    cxDBMemo5: TcxDBMemo;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Panel10: TPanel;
    Panel70: TPanel;
    Button2: TButton;
    procedure ADOQuery1_1AfterInsert(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure ABCheckTreeView1EndDrag(Sender, Target: TObject; X, Y: Integer);
    procedure Button11Click(Sender: TObject);
    procedure ABCheckTreeView2Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure SpeedButton7Click(Sender: TObject);
    procedure ADOQuery1_2BeforePost(DataSet: TDataSet);
    procedure ABCheckTreeView1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure btn3Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure cxGrid1DBTableView1FocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure cxGridDBTableView2FocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure Button2Click(Sender: TObject);
  private
    FStartGetFuncStr:boolean;
    FStartSetFuncStr:boolean;
    function GetFuncStr(aTreeView:TABCheckTreeView): string;
    procedure SetFuncStr(aTreeView:TABCheckTreeView;aFuncRight: string);
    procedure SaveFuncTreeView;
    procedure SaveFuncRight(aDataset: Tadoquery; aTreeView: TABCheckTreeView);
    procedure MakeLicense(aEditRegNameType: string);
    procedure WriteToLog(aName: string);
    procedure UnMakeLicense(aLicenseName: string);
    procedure AppendOrEditSoftDog(aAppend: Boolean; aBegDatetime,
      aEndDatetime: TDateTime; aSubsequentClientCount,aPrice: Integer; aClientInfo,
                          aFuncRight,aCustomFunc,aRemark: string);
    procedure UniteTemplate(var aBegDatetime, aEndDatetime: TDateTime;
      var aSubsequentClientCount,aPrice: Integer; var aClientInfo,
                          aFuncRight,aCustomFunc,aRemark: string);
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ABRegisterForm: TABRegisterForm;

implementation

{$R *.dfm}

procedure TABRegisterForm.FormCreate(Sender: TObject);
begin
  if ABCheckFileExists(ABSoftSetPath+'FuncTreeView.Data') then
  begin
    ABCheckTreeView1.LoadFromFile(ABSoftSetPath+'FuncTreeView.Data');
    ABCheckTreeView2.LoadFromFile(ABSoftSetPath+'FuncTreeView.Data');
  end;
  ADOConnection1.Close;
  ADOConnection1.ConnectionString:= 'Provider=Microsoft.Jet.OLEDB.4.0;Data Source='+ABSoftSetPath+'Data.mdb;Persist Security Info=False';
  ADOConnection1.open;

  ADOQuery1_1.close;
  ADOQuery1_1.Open;

  ADOQuery1_2.close;
  ADOQuery1_2.Open;

  ADOQuery1_21.close;
  ADOQuery1_21.Open;

  ADOQuery1_1.Recordset.Properties['Update Criteria'].Value:=adCriteriaKey;
  ADOQuery1_2.Recordset.Properties['Update Criteria'].Value:=adCriteriaKey;
  ADOQuery1_21.Recordset.Properties['Update Criteria'].Value:=adCriteriaKey;

  FStartGetFuncStr:=true;
  FStartSetFuncStr:=true;

  SetFuncStr(ABCheckTreeView2,cxGrid1DBTableView1.DataController.DataSet.FieldByName('FuncRight').AsString);
  SetFuncStr(ABCheckTreeView1,cxGridDBTableView2.DataController.DataSet.FieldByName('FuncRight').AsString);
end;

procedure TABRegisterForm.FormShow(Sender: TObject);
var
  tempSubsequentClientCount: longint;
begin
  if (not ABCheckLicense(ABPublicSetPath+'License.key','', now(), tempSubsequentClientCount,true))
      and (not ABInputPassWord) then
  begin
    Panel10.Visible:=false;
    Panel70.Visible:=true;
    Panel70.Align:=alClient;
  end;


  if cxGrid1.CanFocus then
    cxGrid1.SetFocus;
  if not cxGrid1DBTableView1.Focused then
    cxGrid1DBTableView1.Focused:=true;
  if cxGrid1DBTableView1.DataController.RecordCount>0 then
    cxGrid1DBTableView1.DataController.SelectRows(0,0);
end;

procedure TABRegisterForm.ADOQuery1_2BeforePost(DataSet: TDataSet);
begin
  ABSetFieldValue(ABDoRegisterKey(DataSet.FindField('RegName').AsString),DataSet.FindField('RegKey'));
end;

procedure TABRegisterForm.btn1Click(Sender: TObject);
var
  tempFieldName:string;
begin
  tempFieldName:=ABSelectFile('','','Key Files|*.Key');
  if ABCheckFileExists(tempFieldName)  then
  begin
    UnMakeLicense(tempFieldName);
  end;
end;

procedure TABRegisterForm.ADOQuery1_1AfterInsert(DataSet: TDataSet);
begin
  DataSet.FindField('BegDatetime').AsDateTime:= now;
  DataSet.FindField('EndDatetime').AsDateTime:= now+365*5;
  DataSet.FindField('SubsequentClientCount').AsInteger:= 0;

  DataSet.FindField('FuncRight').AsString:=ABFillStr('',500);
  if DataSet=ADOQuery1_1 then
  begin
    SetFuncStr(ABCheckTreeView2,DataSet.FindField('FuncRight').AsString);
  end
  else if DataSet=ADOQuery1_2 then
  begin
    SetFuncStr(ABCheckTreeView1,DataSet.FindField('FuncRight').AsString);
  end;
end;

function TABRegisterForm.GetFuncStr(aTreeView:TABCheckTreeView):string;
var
  i,j:LongInt;
  tempText:string;
begin
  Result:=ABFillStr('',500);
  for I := 0 to aTreeView.Items.Count-1 do
  begin
    if aTreeView.Checked[aTreeView.Items[i]] then
    begin
      tempText:=aTreeView.Items[i].Text;
      if (Pos('[',tempText)>0) and
         (Pos(']',tempText)>0)
          then
      begin
        j:=StrToIntDef(ABGetItemValue(tempText,'[','',']'),0);
        if (j>0) and (j<=500) then
        begin
          Result[j]:='1';
        end;
      end;
    end;
  end;
end;

procedure TABRegisterForm.MenuItem1Click(Sender: TObject);
var
  tempFileName:string;
begin
  tempFileName:=ABSaveFile('','', 'xls|*.xls');
  if tempFileName<>EmptyStr then
  begin
    ExportGridToExcel(tempFileName,cxGrid3,true,true)
  end;
end;

procedure TABRegisterForm.SetFuncStr(aTreeView:TABCheckTreeView;aFuncRight:string);
var
  i,j:LongInt;
  tempText:string;
begin
  if Length(aFuncRight)<500 then
    aFuncRight:=ABFillStr('',500);

  aTreeView.Items.BeginUpdate;
  try
    for I := 0 to aTreeView.Items.Count-1 do
    begin
      tempText:=aTreeView.Items[i].Text;
      if (Pos('[',tempText)>0) and
         (Pos(']',tempText)>0)
          then
      begin
        j:=StrToIntDef(ABGetItemValue(tempText,'[','',']'),0);
        if (j>0) and (j<=500) then
        begin
          aTreeView.Checked[aTreeView.Items[i]]:=(aFuncRight[j]='1');
        end;
      end
      else
        aTreeView.Checked[aTreeView.Items[i]]:=false;
    end;
  finally
    aTreeView.Items.EndUpdate;
  end;
end;

procedure TABRegisterForm.ABCheckTreeView1Click(Sender: TObject);
begin
  SaveFuncRight(ADOQuery1_2,ABCheckTreeView1);
end;

procedure TABRegisterForm.ABCheckTreeView2Click(Sender: TObject);
begin
  SaveFuncRight(ADOQuery1_1,ABCheckTreeView2);
end;

procedure TABRegisterForm.SaveFuncRight(aDataset:Tadoquery;aTreeView:TABCheckTreeView);
begin
  if not FStartGetFuncStr then
    exit;

  if aDataset.State in [dsBrowse] then
    aDataset.Edit;

  if aDataset.State in [dsEdit,dsInsert] then
    aDataset.FieldByName('FuncRight').AsString:=GetFuncStr(aTreeView);
end;

procedure TABRegisterForm.ABCheckTreeView1EndDrag(Sender, Target: TObject; X,
  Y: Integer);
begin
  SaveFuncTreeView;
end;

procedure TABRegisterForm.N1Click(Sender: TObject);
begin
  Panel6.Visible:=ABInputPassWord;

  if Panel6.Visible then
  begin
    ABCheckTreeView1.DragMode:= dmAutomatic;
  end
  else
  begin
    ABCheckTreeView1.DragMode:= dmManual;
  end;
end;

procedure TABRegisterForm.N2Click(Sender: TObject);
begin
  ABSetCheckTreeViewState(ABCheckTreeView1,coCheck);
end;

procedure TABRegisterForm.N3Click(Sender: TObject);
begin
  ABSetCheckTreeViewState(ABCheckTreeView1,coUnCheck);
end;

procedure TABRegisterForm.Button1Click(Sender: TObject);
var
  tempSQL,
  tempKey,
  tempParentKey,
  tempName:string;
begin
  if (ABCheckFileExists(ABSoftSetPath+'ABConn.udl')) and
     (ABShow('是否使用上次数据库连接?',[],['是','否'],2)=2) then
  begin
    ABDeleteFile(ABSoftSetPath+'ABConn.udl');
  end;

  ADOConnection2.Connected:=false;
  if ABOpenADOConnectionByUDL(ADOConnection2,'',True,false)  then
  begin
    tempSQL:= ADOQuery3.SQL.Text;
    if (ABShowEditMemo(tempSQL,false,'请输入功能菜单的查询SQL')) and
       (tempSQL<>EmptyStr) then
    begin
      ADOQuery3.Close;
      ADOQuery3.SQL.Text:=tempSQL;
      ADOQuery3.Open;

      tempKey :='tempGuid';
      tempParentKey :='tempParentGuid';
      tempName :='tempname';
      if (ABSelectComboBox(tempKey,ADOQuery3,'请选择主键字段')) and
         (tempKey<>EmptyStr) then
      begin
        if (ABSelectComboBox(tempParentKey,ADOQuery3,'请选择父级主键字段')) and
           (tempParentKey<>EmptyStr) then
        begin
          if (ABSelectComboBox(tempName,ADOQuery3,'请选择显示字段')) and
             (tempName<>EmptyStr) then
          begin
            ABDataSetsToTree
            (
             ABCheckTreeView1.Items,
             ADOQuery3,
             tempParentKey,
             tempKey,
             tempName
             );
            SaveFuncTreeView;
          end;
        end;
      end;
    end;
  end;
end;

procedure TABRegisterForm.Button2Click(Sender: TObject);
begin
  btn1Click(btn1);
end;

procedure TABRegisterForm.Button3Click(Sender: TObject);
var
  tempName:string;
begin
  if (ABInPutStr('名称',tempName)) and
     (tempName<>EmptyStr) then
  begin
    if (Assigned(ABCheckTreeView1.Selected)) then
    begin
      ABCheckTreeView1.Items.AddChild(ABCheckTreeView1.Selected,tempName);
    end
    else
    begin
      ABCheckTreeView1.Items.AddChild(nil,tempName);
    end;
    SaveFuncTreeView;
  end;
end;

procedure TABRegisterForm.Button11Click(Sender: TObject);
var
  tempName:string;
begin
  if (ABInPutStr('名称',tempName)) and
     (tempName<>EmptyStr) then
  begin
    if (Assigned(ABCheckTreeView1.Selected)) then
    begin
      ABCheckTreeView1.Selected.Text:=tempName;
    end;
    SaveFuncTreeView;
  end;
end;

procedure TABRegisterForm.Button4Click(Sender: TObject);
begin
  if (Assigned(ABCheckTreeView1.Selected)) then
  begin
    ABCheckTreeView1.Items.Delete(ABCheckTreeView1.Selected);
    SaveFuncTreeView;
  end;
end;

procedure TABRegisterForm.Button5Click(Sender: TObject);
begin
  if (Assigned(ABCheckTreeView1.Selected)) then
  begin
    ABCheckTreeView1.Selected.MoveTo(nil,naAdd);
    SaveFuncTreeView;
  end;
end;

procedure TABRegisterForm.SaveFuncTreeView;
begin
  ABCheckTreeView1.SaveToFile(ABSoftSetPath+'FuncTreeView.Data');
  ABCheckTreeView2.LoadFromFile(ABSoftSetPath+'FuncTreeView.Data');
end;

procedure TABRegisterForm.Button7Click(Sender: TObject);
begin
  ADOQuery1_1.Append;
end;

procedure TABRegisterForm.Button8Click(Sender: TObject);
begin
  ADOQuery1_1.Delete;
end;

procedure TABRegisterForm.cxGrid1DBTableView1FocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin
  if not FStartSetFuncStr then
    exit;

  SetFuncStr(ABCheckTreeView2,cxGrid1DBTableView1.DataController.DataSet.FieldByName('FuncRight').AsString);
end;

procedure TABRegisterForm.cxGridDBTableView2FocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin
  if not FStartSetFuncStr then
    exit;

  SetFuncStr(ABCheckTreeView1,cxGridDBTableView2.DataController.DataSet.FieldByName('FuncRight').AsString);
end;

procedure TABRegisterForm.SpeedButton3Click(Sender: TObject);
var
  tempBegDatetime,
  tempEndDatetime:TDateTime;
  tempSubsequentClientCount :LongInt;
  tempPrice:LongInt;

  tempClientInfo,
  tempRemark,
  tempCustomFunc,
  tempFuncRight:string;
begin
  UniteTemplate(
                tempBegDatetime,
                tempEndDatetime,
                tempSubsequentClientCount,
                tempPrice,

                tempClientInfo,
                tempFuncRight,
                tempCustomFunc,
                tempRemark
                );

  AppendOrEditSoftDog(false,
                      tempBegDatetime,
                      tempEndDatetime,
                      tempSubsequentClientCount,
                      tempPrice,

                      tempClientInfo,
                      tempFuncRight,
                      tempCustomFunc,
                      tempRemark
                      );
end;

procedure TABRegisterForm.SpeedButton2Click(Sender: TObject);
var
  tempBegDatetime,
  tempEndDatetime:TDateTime;
  tempSubsequentClientCount :LongInt;
  tempPrice:LongInt;

  tempClientInfo,
  tempRemark,
  tempCustomFunc,
  tempFuncRight:string;
begin
  if ADOQuery1_2.State in [dsEdit,dsInsert] then
    ADOQuery1_2.post;

  UniteTemplate(
                tempBegDatetime,
                tempEndDatetime,
                tempSubsequentClientCount,
                tempPrice,

                tempClientInfo,
                tempFuncRight,
                tempCustomFunc,
                tempRemark
                );

  AppendOrEditSoftDog(true,
                      tempBegDatetime,
                      tempEndDatetime,
                      tempSubsequentClientCount,
                      tempPrice,

                      tempClientInfo,
                      tempFuncRight,
                      tempCustomFunc,
                      tempRemark
                      );
end;

procedure TABRegisterForm.UniteTemplate(
                          var aBegDatetime,
                          aEndDatetime:TDateTime;
                          var aSubsequentClientCount,aPrice :LongInt;

                          var aClientInfo,
                          aFuncRight,aCustomFunc,aRemark:string);
var
  i,j:LongInt;
  tempStrings1,
  tempStrings2,
  tempStrings5,
  tempStrings6:TStrings;
begin
  aBegDatetime:=0;
  aEndDatetime:=0;
  aSubsequentClientCount:=0;

  aClientInfo:=emptystr;
  aFuncRight:=emptystr;
  aRemark :=emptystr;

  tempStrings1:=TStringList.Create;
  tempStrings2:=TStringList.Create;
  tempStrings5:=TStringList.Create;
  tempStrings6:=TStringList.Create;
  FStartSetFuncStr:=False;
  try
    for I := 0 to cxGrid1DBTableView1.DataController.GetSelectedCount- 1 do
    begin
      j:= cxGrid1DBTableView1.DataController.GetSelectedRowIndex(i);
      cxGrid1DBTableView1.DataController.DataSet.RecNo:=cxGrid1DBTableView1.DataController.GetRowInfo(j).RecordIndex+1;

      if (ADOQuery1_1.FindField('BegDatetime').AsDateTime>0)  then
      begin
        if (aBegDatetime=0) then
          aBegDatetime:=ADOQuery1_1.FindField('BegDatetime').AsDateTime
        else
          aBegDatetime:=min(aBegDatetime,ADOQuery1_1.FindField('BegDatetime').AsDateTime);
      end;

      if (ADOQuery1_1.FindField('EndDatetime').AsDateTime>0)  then
      begin
        if (aEndDatetime=0) then
          aEndDatetime:=ADOQuery1_1.FindField('EndDatetime').AsDateTime
        else
          aEndDatetime:=max(aEndDatetime,ADOQuery1_1.FindField('EndDatetime').AsDateTime);
      end;

      aSubsequentClientCount        :=max(aSubsequentClientCount,ADOQuery1_1.FindField('SubsequentClientCount').AsInteger) ;
      aPrice                        :=max(aPrice                  ,  ADOQuery1_1.FindField('Price').AsInteger);
      aRemark                       :=aRemark+ ADOQuery1_1.FindField('Remark').AsString;

      if Length(ADOQuery1_1.FindField('FuncRight').AsString)=500 then
      begin
        if aFuncRight=EmptyStr then
          aFuncRight:=ADOQuery1_1.FindField('FuncRight').AsString
        else
        begin
          for j := 1 to 500 do
          begin
            if (aFuncRight[j]='0') and (ADOQuery1_1.FindField('FuncRight').AsString[j]='0') then
              aFuncRight[j]:='0'
            else
              aFuncRight[j]:='1';
          end;
        end;
      end;

      if ADOQuery1_1.FindField('ClientInfo').AsString<>EmptyStr then
      begin
        tempStrings1.Text:=ADOQuery1_1.FindField('ClientInfo').AsString;
        for j := 0 to tempStrings1.Count-1 do
        begin
          if tempStrings2.IndexOf(tempStrings1.Strings[j])<0 then
          begin
            tempStrings2.Add(tempStrings1.Strings[j]);
          end;
        end;
      end;
      if ADOQuery1_1.FindField('CustomFunc').AsString<>EmptyStr then
      begin
        tempStrings5.Text:=ADOQuery1_1.FindField('CustomFunc').AsString;
        for j := 0 to tempStrings5.Count-1 do
        begin
          if tempStrings6.IndexOf(tempStrings5.Strings[j])<0 then
          begin
            tempStrings6.Add(tempStrings5.Strings[j]);
          end;
        end;
      end;
    end;
    aClientInfo:= tempStrings2.Text;
    aCustomFunc:= tempStrings6.Text;
  finally
    tempStrings1.Free;
    tempStrings2.Free;
    tempStrings5.Free;
    tempStrings6.Free;
    FStartSetFuncStr:=True;
    SetFuncStr(ABCheckTreeView2,cxGrid1DBTableView1.DataController.DataSet.FieldByName('FuncRight').AsString);
  end;
end;

procedure TABRegisterForm.AppendOrEditSoftDog(aAppend:Boolean;
                          aBegDatetime,
                          aEndDatetime:TDateTime;
                          aSubsequentClientCount,aPrice:LongInt;

                          aClientInfo,
                          aFuncRight,aCustomFunc,aRemark:string);
var
  i,j:LongInt;
  tempStrings1,
  tempStrings2:TStrings;
begin
  if ADOQuery1_2.State in [dsBrowse] then
  begin
    if aAppend then
      ADOQuery1_2.Append
    else
      ADOQuery1_2.Edit
  end;

  if ADOQuery1_2.State in [dsEdit,dsInsert] then
  begin
    tempStrings1:=TStringList.Create;
    tempStrings2:=TStringList.Create;
    try
      if aBegDatetime>0 then
        ADOQuery1_2.FindField('BegDatetime').AsDateTime := aBegDatetime
      else
        ADOQuery1_2.FindField('BegDatetime').Clear;

      if aEndDatetime>0 then
        ADOQuery1_2.FindField('EndDatetime').AsDateTime := aEndDatetime
      else
        ADOQuery1_2.FindField('EndDatetime').Clear;

      if ADOQuery1_2.State in [dsEdit] then
      begin
        ADOQuery1_2.FindField('SubsequentClientCount').AsInteger    := max(aSubsequentClientCount,ADOQuery1_2.FindField('SubsequentClientCount').AsInteger) ;
        ADOQuery1_2.FindField('Price').AsInteger                    := max(aPrice ,ADOQuery1_2.FindField('Price').AsInteger);
        ADOQuery1_2.FindField('Remark').AsString                    := aRemark+ADOQuery1_2.FindField('Remark').AsString;

        if Length(aFuncRight)=500 then
        begin
          for j := 1 to 500 do
          begin
            if (aFuncRight[j]='0') and (ADOQuery1_2.FindField('FuncRight').AsString[j]='0') then
              aFuncRight[j]:='0'
            else
              aFuncRight[j]:='1';
          end;

          ADOQuery1_2.FindField('FuncRight').AsString:=aFuncRight;
        end;

        if aClientInfo<>EmptyStr then
        begin
          tempStrings1.Text:=aClientInfo;
          tempStrings2.Text:=ADOQuery1_2.FindField('ClientInfo').AsString;
          for I := 0 to tempStrings1.Count-1 do
          begin
            if tempStrings2.IndexOf(tempStrings1.Strings[i])<0 then
            begin
              tempStrings2.Add(tempStrings1.Strings[i]);
            end;
          end;
          ADOQuery1_2.FindField('ClientInfo').AsString := tempStrings2.Text;
        end;

        if aCustomFunc<>EmptyStr then
        begin
          tempStrings1.Text:=aCustomFunc;
          tempStrings2.Text:=ADOQuery1_2.FindField('CustomFunc').AsString;
          for I := 0 to tempStrings1.Count-1 do
          begin
            if tempStrings2.IndexOf(tempStrings1.Strings[i])<0 then
            begin
              tempStrings2.Add(tempStrings1.Strings[i]);
            end;
          end;
          ADOQuery1_2.FindField('CustomFunc').AsString := tempStrings2.Text;
        end;
      end
      else
      begin
        ADOQuery1_2.FindField('SubsequentClientCount').AsInteger    := aSubsequentClientCount;
        ADOQuery1_2.FindField('Price').AsInteger                    := aPrice;
        ADOQuery1_2.FindField('ClientInfo').AsString    := aClientInfo  ;
        ADOQuery1_2.FindField('FuncRight').AsString     := aFuncRight;
        ADOQuery1_2.FindField('CustomFunc').AsString    := aCustomFunc  ;
        ADOQuery1_2.FindField('Remark').AsString     := aRemark;
      end;

      SetFuncStr(ABCheckTreeView1,cxGridDBTableView2.DataController.DataSet.FieldByName('FuncRight').AsString);
    finally
      tempStrings1.Free;
      tempStrings2.Free;
    end;
  end;
end;

procedure TABRegisterForm.SpeedButton7Click(Sender: TObject);
begin
  ABPostDataset(ADOQuery1_2);
  MakeLicense('NotCanEditRegName');
end;

procedure TABRegisterForm.btn3Click(Sender: TObject);
begin
  ABPostDataset(ADOQuery1_2);
  MakeLicense('CanEditRegName');
end;

procedure TABRegisterForm.WriteToLog(aName:string);
begin
  ADOQuery1_21.Insert;
  ADOQuery1_21.FieldByName('ListID').AsInteger:=ADOQuery1_2.FieldByName('ID').AsInteger;
  ADOQuery1_21.FieldByName('Datetime').AsDateTime:=Now;
  ADOQuery1_21.FieldByName('Remark').AsString:=
             aName+'[开始时间('+ABDateTimeToStr(ADOQuery1_2.FindField('BegDatetime').AsDateTime)+') '+
                     '结束时间('+ABDateTimeToStr(ADOQuery1_2.FindField('EndDatetime').AsDateTime)+') '+
                     '并发客户端数('+inttostr(ADOQuery1_2.FindField('SubsequentClientCount').AsInteger)+') '+
                     '授权客户端('+ADOQuery1_2.FindField('ClientInfo').AsString+') '+
                     '菜单权限('+ADOQuery1_2.FindField('FuncRight').AsString+') '+
                     '备注('+ADOQuery1_2.FindField('remark').AsString+') '+
                    ']';
  ADOQuery1_21.Post;
end;

procedure TABRegisterForm.MakeLicense(aEditRegNameType: string);
var
  tempStr1,tempIP,tempBegDatetime,tempEndDatetime:string;
  tempDatetime:TDateTime;
  i:longint;
  tempStrings:Tstrings;
begin
  tempStrings:=TStringList.Create;
  FStartSetFuncStr:=False;
  try
    if (ADOQuery1_2.FindField('RegName').AsString<>EmptyStr) then
    begin
      ABSetFieldValue(ABDoRegisterKey(ADOQuery1_2.FindField('RegName').AsString),ADOQuery1_2.FindField('RegKey'),true);

      //写入注册名称、注册码
      ABWriteIniInStrings('MainSetup','RegName',ADOQuery1_2.FindField('RegName').AsString,tempStrings);
      ABWriteIniInStrings('MainSetup','RegKey',ADOQuery1_2.FindField('RegKey').AsString,tempStrings);

      //写入开始时间、结束时间
      if (ADOQuery1_2.FindField('BegDatetime').IsNull) then
        tempDatetime:=ABStrToDateTime('2000-01-01 00:00:00')
      else
        tempDatetime:=ADOQuery1_2.FindField('BegDatetime').AsDateTime;
      ABWriteIniInStrings('MainSetup','BegDatetime',ABDateTimeToStr(tempDatetime),tempStrings);

      if (ADOQuery1_2.FindField('EndDatetime').IsNull) then
        tempDatetime:=ABStrToDateTime('2100-01-01 00:00:00')
      else
        tempDatetime:=ADOQuery1_2.FindField('EndDatetime').AsDateTime;
      ABWriteIniInStrings('MainSetup','EndDatetime',ABDateTimeToStr(tempDatetime),tempStrings);

      //写入并发客户端数
      ABWriteIniInStrings('MainSetup','SubsequentClientCount',inttostr(ADOQuery1_2.FindField('SubsequentClientCount').AsInteger),tempStrings);
      //写入详细的客户端信息
      if (cxDBMemo1.Text<>EmptyStr) then
      begin
        for I := 0 to cxDBMemo1.Lines.Count - 1 do
        begin
          tempIP:=ABGetLeftRightStr(cxDBMemo1.Lines[i]);
          tempStr1:=ABGetLeftRightStr(cxDBMemo1.Lines[i],axdRight);
          tempBegDatetime:=ABGetLeftRightStr(tempStr1);
          tempEndDatetime:=ABGetLeftRightStr(tempStr1,axdRight);
          if tempIP<>emptystr then
          begin
            //开始时间
            if tempBegDatetime=emptystr then
              tempDatetime:=ABStrToDateTime('2000-01-01 00:00:00')
            else
              tempDatetime:=ABStrToDateTime(tempBegDatetime);
            tempBegDatetime:=ABDateTimeToStr(tempDatetime);

            //结束时间
            if tempEndDatetime=emptystr then
              tempDatetime:=ABStrToDateTime('2100-01-01 00:00:00')
            else
              tempDatetime:=ABStrToDateTime(tempEndDatetime);
            tempEndDatetime:=ABDateTimeToStr(tempDatetime);

            ABWriteIniInStrings('ClientInfo',tempIP,tempBegDatetime+'='+tempEndDatetime,tempStrings);
          end;
        end;
      end
      else
      begin
        ABWriteIniInStrings('ClientInfo',EmptyStr,EmptyStr,tempStrings);
      end;

      //写入菜单权限
      ABWriteIniInStrings('MainSetup','FuncRight',ADOQuery1_2.FindField('FuncRight').AsString,tempStrings);

      //写入定制功能
      if (cxDBMemo2.Text<>EmptyStr) then
      begin
        for I := 0 to cxDBMemo2.Lines.Count - 1 do
        begin
          if (Trim(cxDBMemo2.Lines.Names[i])<>EmptyStr) and
             (Trim(cxDBMemo2.Lines.ValueFromIndex[i])<>EmptyStr) then
            ABWriteIniInStrings('CustomFunc',cxDBMemo2.Lines.Names[i],cxDBMemo2.Lines.ValueFromIndex[i],tempStrings);
        end;
      end
      else
      begin
        ABWriteIniInStrings('CustomFunc',EmptyStr,EmptyStr,tempStrings);
      end;

      //写入是否通用注册码标志
      ABWriteIniInStrings('MainSetup','EditRegNameType',aEditRegNameType,tempStrings);

      if ADOQuery1_2.State in [dsBrowse] then
        ADOQuery1_2.Edit;

      if ADOQuery1_2.State in [dsEdit,dsInsert] then
      begin
        ADOQuery1_2.Edit;
        ADOQuery1_2.FindField('Count').AsInteger := ADOQuery1_2.FindField('Count').AsInteger+1;
        ADOQuery1_2.post;
      end;

      tempStrings.Text:=ABDOPassWord(tempStrings.Text);
      tempStrings.SaveToFile(ABSoftSetPath+'License.key');

      WriteToLog(ABIIF(AnsiCompareText(aEditRegNameType,'NotCanEditRegName')=0,'生成专用客户授权','生成通用客户授权'));
      ABShow('授权文件[%s]生成成功',['License.key']);
    end
    else
      ABShow(ADOQuery1_2.FindField('RegName').DisplayLabel+('不能为空,请检查.'));

  finally
    FStartSetFuncStr:=true;
    tempStrings.Free;
  end;
end;

procedure TABRegisterForm.UnMakeLicense(aLicenseName:string);
var
  tempRegName,
  tempRegKey,
  tempBegDatetime,
  tempEndDatetime,
  tempSubsequentClientCount,
  tempFuncRight,
  tempStr,
  tempEditRegNameType:string;
  tempCustomFunc,
  tempClientInfo:tstrings;
  tempForm:TABShowRightForm;
  tempStrings:Tstrings;
begin
  tempStrings:=TStringList.Create;
  FStartSetFuncStr:=False;
  try
    if ABCheckFileExists(aLicenseName)  then
    begin
      tempStrings.LoadFromFile(aLicenseName);
      tempStrings.Text:=ABUnDOPassWord(tempStrings.Text);

      tempClientInfo := TStringList.Create;
      tempCustomFunc := TStringList.Create;
      try
        tempRegName                  :=ABReadIniInStrings('MainSetup','RegName'              ,tempStrings);
        tempRegKey                   :=ABReadIniInStrings('MainSetup','RegKey'               ,tempStrings);
        tempEditRegNameType          :=ABReadIniInStrings('MainSetup','EditRegNameType'      ,tempStrings);
        tempBegDatetime              :=ABReadIniInStrings('MainSetup','BegDatetime'          ,tempStrings);
        tempEndDatetime              :=ABReadIniInStrings('MainSetup','EndDatetime'          ,tempStrings);
        tempFuncRight                :=ABReadIniInStrings('MainSetup','FuncRight'            ,tempStrings);
        tempSubsequentClientCount    :=ABReadIniInStrings('MainSetup','SubsequentClientCount',tempStrings);
        ABReadIniInStrings('ClientInfo',tempClientInfo,tempStrings);
        ABReadIniInStrings('CustomFunc',tempCustomFunc,tempStrings);

        tempStr:='注册名称:'+tempRegName+ABEnterWrapStr+
                '注册码  :'+tempRegKey+ABEnterWrapStr+
                '注册类型:'+ABIIF(AnsiCompareText(tempEditRegNameType,'NotCanEditRegName')=0,'专用客户授权','通用客户授权')+ABEnterWrapStr+
                '开始使用:'+tempBegDatetime+ABEnterWrapStr+
                '结束使用:'+tempEndDatetime+ABEnterWrapStr+
                '并发客户端数:'+tempSubsequentClientCount+ABEnterWrapStr+
                '菜单权限:'+tempFuncRight+ABEnterWrapStr+
                '授权客户端:'+trim(tempClientInfo.Text)+ABEnterWrapStr+
                '定制功能:'+trim(tempCustomFunc.Text);
        tempForm:=TABShowRightForm.Create(nil);
        try
          if ADOQuery1_2.Locate('RegName',tempRegName,[]) then
          begin
            tempForm.Caption:='授权文件客户名称为%s';
          end                             
          else
          begin
            tempForm.Caption:='授权文件未注册';
          end;
          tempForm.mmo1.Text:='详细权限如下:'+ABEnterWrapStr+tempStr;

          ABCheckTreeView1.SaveToFile(ABSoftSetPath+'FuncTreeView.Data');
          tempForm.RzCheckTree3.LoadFromFile(ABSoftSetPath+'FuncTreeView.Data');
          SetFuncStr(tempForm.RzCheckTree3,tempFuncRight);

          tempForm.ShowModal;
        finally
          tempForm.Free;
        end;
      finally
        tempClientInfo.Free;
        tempCustomFunc.Free;
      end;
    end;
  finally
    FStartSetFuncStr:=true;
    tempStrings.Free;
  end;
end;



end.



