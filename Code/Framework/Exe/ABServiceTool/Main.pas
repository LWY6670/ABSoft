unit Main;

interface                                       

uses
  ABPubFuncU,
  ABPubFormU,
  ABPubConstU,
  ABPubServiceU,

  TypInfo,
  Windows,Messages,SysUtils,Variants,Classes,Graphics,Controls,Forms,Dialogs,
  StdCtrls,Buttons,ShellAPI,ComCtrls,Menus;

type
  TABServiceToolsForm = class(TABPubForm)
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Edit1: TEdit;
    Edit2: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    SpeedButton1: TSpeedButton;
    Edit3: TEdit;
    Label3: TLabel;
    StatusBar1: TStatusBar;
    Button5: TButton;
    procedure SpeedButton1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Edit1Exit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ABServiceToolsForm: TABServiceToolsForm;

implementation

{$R *.dfm}

procedure TABServiceToolsForm.Button1Click(Sender: TObject);
begin
  if ABCheckFileExists(Edit2.Text) then
  begin
    ABInstallService_Dos(Edit1.Text,Edit2.Text,false,SW_NORMAL);
    StatusBar1.Panels[0].Text:=GetEnumName(TypeInfo(TABServiceState), Ord(ABGetServiceStatus(Edit1.Text)));
  end;
end;


procedure TABServiceToolsForm.Button2Click(Sender: TObject);
begin
  if ABCheckFileExists(Edit2.Text) then
  begin
    if ABServiceIsRunning(Edit1.Text) then
    begin
      ABStopService(Edit1.Text);
    end;
    ABUninstallService(Edit1.Text);
    StatusBar1.Panels[0].Text:=GetEnumName(TypeInfo(TABServiceState), Ord(ABGetServiceStatus(Edit1.Text)));
  end;
end;

procedure TABServiceToolsForm.Button3Click(Sender: TObject);
begin
  if Edit1.Text <> EmptyStr then
  begin
    if not ABServiceIsInstall(Edit1.Text) then
    begin
      if ABCheckFileExists(Edit2.Text) then
      begin
        ABInstallService_Dos(Edit1.Text,Edit2.Text,false,SW_NORMAL);
      end;
    end;

    ABStartService(Edit1.Text);
    StatusBar1.Panels[0].Text:=GetEnumName(TypeInfo(TABServiceState), Ord(ABGetServiceStatus(Edit1.Text)));
  end;
end;

procedure TABServiceToolsForm.Button4Click(Sender: TObject);
begin
  if Edit1.Text <> EmptyStr then
  begin
    ABStopService(Edit1.Text);
    StatusBar1.Panels[0].Text:=GetEnumName(TypeInfo(TABServiceState), Ord(ABGetServiceStatus(Edit1.Text)));
  end;
end;

procedure TABServiceToolsForm.Button5Click(Sender: TObject);
begin
  StatusBar1.Panels[0].Text:=GetEnumName(TypeInfo(TABServiceState), Ord(ABGetServiceStatus(Edit1.Text)));
end;

procedure TABServiceToolsForm.Edit1Exit(Sender: TObject);
begin
  Button5Click(Edit1);
end;

procedure TABServiceToolsForm.FormCreate(Sender: TObject);
begin
  Button5Click(Edit1);
end;

procedure TABServiceToolsForm.SpeedButton1Click(Sender: TObject);
var
  tempStr:string;
begin
  tempStr:= ABSelectFile('', Edit2.Text, 'exe|*.exe');
  if tempStr<>EmptyStr then
  begin
    Edit2.Text:=tempStr;
  end;
end;


end.

