object MainForm: TMainForm
  Left = 0
  Top = 0
  Caption = 'ABSoft '#20013#38388#23618#26381#21153#22120
  ClientHeight = 500
  ClientWidth = 800
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 800
    Height = 481
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = #36830#25509#23458#25143#31471#20449#24687
      object StringGrid1: TStringGrid
        Left = 0
        Top = 0
        Width = 792
        Height = 416
        Align = alClient
        ColCount = 7
        DefaultColWidth = 30
        DefaultRowHeight = 21
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goDrawFocusSelected, goRowSizing, goColSizing]
        TabOrder = 0
      end
      object Panel1: TPanel
        Left = 0
        Top = 416
        Width = 792
        Height = 37
        Align = alBottom
        TabOrder = 1
        DesignSize = (
          792
          37)
        object Label5: TLabel
          Left = 332
          Top = 11
          Width = 53
          Height = 13
          AutoSize = False
          Caption = 'TCP'#31471#21475
          Transparent = True
        end
        object Label6: TLabel
          Left = 500
          Top = 11
          Width = 53
          Height = 13
          AutoSize = False
          Caption = 'HTTP'#31471#21475
          Transparent = True
        end
        object Button_ConnSetup: TButton
          Left = 665
          Top = 6
          Width = 115
          Height = 25
          Anchors = [akTop, akRight]
          Caption = #25968#25454#24211#36830#25509#35774#32622
          TabOrder = 0
          OnClick = Button_ConnSetupClick
        end
        object Button_Start: TButton
          Left = 8
          Top = 6
          Width = 75
          Height = 25
          Caption = #21551#21160
          TabOrder = 1
          OnClick = Button_StartClick
        end
        object Button_Stop: TButton
          Left = 88
          Top = 6
          Width = 75
          Height = 25
          Caption = #20572#27490
          Enabled = False
          TabOrder = 2
          OnClick = Button_StopClick
        end
        object Edit5: TEdit
          Tag = 91
          Left = 387
          Top = 6
          Width = 104
          Height = 21
          Anchors = [akLeft, akTop, akRight]
          ImeName = #20013#25991' ('#31616#20307') - '#24494#36719#25340#38899
          TabOrder = 3
          Text = '211'
        end
        object Edit6: TEdit
          Tag = 91
          Left = 555
          Top = 6
          Width = 104
          Height = 21
          Anchors = [akLeft, akTop, akRight]
          ImeName = #20013#25991' ('#31616#20307') - '#24494#36719#25340#38899
          TabOrder = 4
          Text = '8080'
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = #26085#24535#20449#24687
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Memo1: TMemo
        Left = 0
        Top = 0
        Width = 792
        Height = 412
        Align = alClient
        Lines.Strings = (
          'Memo1')
        ReadOnly = True
        ScrollBars = ssBoth
        TabOrder = 0
        OnKeyDown = Memo1KeyDown
      end
      object Panel2: TPanel
        Left = 0
        Top = 412
        Width = 792
        Height = 41
        Align = alBottom
        TabOrder = 1
        DesignSize = (
          792
          41)
        object Button2: TButton
          Left = 8
          Top = 6
          Width = 75
          Height = 25
          Caption = #28165#38500#26085#24535
          TabOrder = 0
          OnClick = Button2Click
        end
        object Button3: TButton
          Left = 88
          Top = 6
          Width = 75
          Height = 25
          Caption = #21478#23384#20026#26085#24535
          TabOrder = 1
          OnClick = Button3Click
        end
        object CheckBox2: TCheckBox
          Tag = 91
          Left = 703
          Top = 10
          Width = 80
          Height = 17
          Anchors = [akTop, akRight]
          Caption = #35760#35814#32454#26085#24535
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
        end
      end
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 481
    Width = 800
    Height = 19
    Panels = <
      item
        Text = #21551#21160#26102#38388
        Width = 55
      end
      item
        Width = 130
      end
      item
        Text = #24050#36816#34892#26102#38388
        Width = 70
      end
      item
        Width = 200
      end
      item
        Text = #24050#29992#20869#23384'(M)'
        Width = 80
      end
      item
        Width = 80
      end
      item
        Text = #24635#35745#20869#23384'(M)'
        Width = 80
      end
      item
        Width = 80
      end>
  end
  object Timer1: TTimer
    Enabled = False
    OnTimer = Timer1Timer
    Left = 352
    Top = 144
  end
  object SaveTextFileDialog1: TSaveTextFileDialog
    Left = 576
    Top = 128
  end
end
