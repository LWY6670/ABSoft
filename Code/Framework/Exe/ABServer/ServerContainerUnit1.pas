unit ServerContainerUnit1;

interface

uses
  ABPubVarU,
  ABPubFuncU,
  ABPubUserU,
  ABPubLogU,
  ABPubLocalParamsU,
  ABPubThreadU,
  ABPubPassU,

  SysUtils,Classes,Datasnap.DSTCPServerTransport,forms,Datasnap.DSHTTPCommon,
  Datasnap.DSHTTP,Datasnap.DSServer,Datasnap.DSCommonServer,Datasnap.DSAuth,
  DbxCompressionFilter,DBXCommon,DBCommonTypes,
  IPPeerServer;

type
  TServerContainer1 = class(TDataModule)
    DSServer1: TDSServer;
    DSTCPServerTransport1: TDSTCPServerTransport;
    DSAuthenticationManager1: TDSAuthenticationManager;
    DSServerClass1: TDSServerClass;
    procedure DSServerClass1GetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure DSAuthenticationManager1UserAuthorize(Sender: TObject;
      EventObject: TDSAuthorizeEventObject; var valid: Boolean);
    procedure DSAuthenticationManager1UserAuthenticate(Sender: TObject;
      const Protocol, Context, User, Password: string; var valid: Boolean;
      UserRoles: TStrings);
    procedure DSServer1Connect(DSConnectEventObject: TDSConnectEventObject);
    function DSServer1Trace(TraceInfo: TDBXTraceInfo): CBRType;
    procedure DSHTTPService1Trace(Sender: TObject; AContext: TDSHTTPContext;
      ARequest: TDSHTTPRequest; AResponse: TDSHTTPResponse);
    procedure DataModuleCreate(Sender: TObject);
    procedure DSServer1Disconnect(DSConnectEventObject: TDSConnectEventObject);
    procedure DSServer1Error(DSErrorEventObject: TDSErrorEventObject);
  private
    FClientIP:string;
    FClientPort:string;
    { Private declarations }
  public
    property ClientIP:string Read FClientIP write FClientIP;
    property ClientPort:string Read FClientPort write FClientPort;
  end;

var
  ServerContainer1: TServerContainer1;

implementation

uses ServerMethodsUnit1,Main;

{$R *.dfm}


procedure TServerContainer1.DSServerClass1GetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := ServerMethodsUnit1.TABServer;
end;

procedure TServerContainer1.DSAuthenticationManager1UserAuthenticate(
  Sender: TObject; const Protocol, Context, User, Password: string;
  var valid: Boolean; UserRoles: TStrings);
begin
  //验证来访者与密码
  if (AnsiCompareText(User,'test')=0) or
     ((AnsiCompareText(User,'admin')=0) and (ABPubUser.IsNoCheckAdminPasss(ABUnDOPassWord(Password))))  then
    valid := True
  else
    valid := False;

  MainForm.addLog('CheckUser('+User+'['+ABBoolToStr(valid)+']'+')');
end;

procedure TServerContainer1.DSAuthenticationManager1UserAuthorize(
  Sender: TObject; EventObject: TDSAuthorizeEventObject;
  var valid: Boolean);
begin
  { TODO : Authorize a user to execute a method.
    Use values from EventObject such as UserName, UserRoles, AuthorizedRoles and DeniedRoles.
    Use DSAuthenticationManager1.Roles to define Authorized and Denied roles
    for particular server methods. }
  valid := True;
end;

procedure TServerContainer1.DataModuleCreate(Sender: TObject);
begin
  DSServer1.AutoStart :=False;
end;

procedure TServerContainer1.DSServer1Connect(
  DSConnectEventObject: TDSConnectEventObject);
var
  tempDatetime:TDateTime;
  tempRemark:string;
begin
  if (Assigned(DSConnectEventObject.ChannelInfo)) and
     (AnsiCompareText(DSConnectEventObject.ConnectProperties['DSAuthenticationUser'],'test')<>0) then
  begin
    MainForm.addLog('DSServer1Connect ClientIP:'+DSConnectEventObject.ChannelInfo.ClientInfo.IpAddress+' ClientPort:'+DSConnectEventObject.ChannelInfo.ClientInfo.ClientPort);
    FClientIP:=DSConnectEventObject.ChannelInfo.ClientInfo.IpAddress;
    FClientPort:=DSConnectEventObject.ChannelInfo.ClientInfo.ClientPort;

    //增加列已连接的列表中
    tempDatetime:=Now;
    tempRemark:=DSConnectEventObject.ConnectProperties['App_Name'];
    if tempRemark=EmptyStr then
      tempRemark:='Info('+DSConnectEventObject.ChannelInfo.Info+')Id('+IntToStr(DSConnectEventObject.ChannelInfo.Id)+')';

    MainForm.AddClient(
              FClientIP,
              FClientPort,
              DSConnectEventObject.ChannelInfo.ClientInfo.Protocol   ,
              FormatDateTime('yyyy-mm-dd hh:nn:ss', tempDatetime),
              FormatDateTime('yyyy-mm-dd hh:nn:ss', tempDatetime),
              tempRemark);
  end;
end;

procedure TServerContainer1.DSServer1Disconnect(DSConnectEventObject: TDSConnectEventObject);
begin
  if (Assigned(DSConnectEventObject.ChannelInfo)) and
     (AnsiCompareText(DSConnectEventObject.ConnectProperties['DSAuthenticationUser'],'test')<>0) then
  begin
    MainForm.addLog('DSServer1Disconnect ClientIP:'+DSConnectEventObject.ChannelInfo.ClientInfo.IpAddress+' ClientPort:'+DSConnectEventObject.ChannelInfo.ClientInfo.ClientPort);

    MainForm.DeleteClient(FClientIP,FClientPort);
  end;
end;

procedure TServerContainer1.DSServer1Error(
  DSErrorEventObject: TDSErrorEventObject);
begin
  MainForm.addLog('DSServerError ' + DSErrorEventObject.Error.Message);
end;

function TServerContainer1.DSServer1Trace(TraceInfo: TDBXTraceInfo): CBRType;
begin
  if MainForm.CheckBox2.Checked then
    MainForm.addLog('Trace '+TraceInfo.CustomCategory+' '+TraceInfo.Message);

  Result := cbrUSEDEF; // take default action
end;

procedure TServerContainer1.DSHTTPService1Trace(Sender: TObject;
  AContext: TDSHTTPContext; ARequest: TDSHTTPRequest;
  AResponse: TDSHTTPResponse);
begin
  if MainForm.CheckBox2.Checked then
    MainForm.addLog('HTTP Trace ' + AContext.ToString+' ' + ARequest.Document+' ' + AResponse.ResponseText);
end;

end.

