{
导入数据窗体单元
}
unit ABThirdImportDataU;

interface
{$I ..\ABInclude.ini}

uses
  ABPubFormU,
  ABPubConstU,
  ABPubMessageU,
  ABPubDBU,
  ABPubFuncU,

  cxSSRes,
  cxClasses,
  cxSSheet,
  dxCore,

  TypInfo,
  Windows,SysUtils,Variants,Classes,Controls,Forms,StdCtrls,ExtCtrls,db,ComCtrls,

  ImgList, Menus, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  System.ImageList;

type
  TABImportDataForm = class(TABPubForm)
    Panel2: TPanel;
    cxSpreadSheet1: TcxSpreadSheet;
    pmSheetPopup: TPopupMenu;
    pmiCut: TMenuItem;
    pmiCopy: TMenuItem;
    pmiPaste: TMenuItem;
    N6: TMenuItem;
    actMerge1: TMenuItem;
    actSplitCells1: TMenuItem;
    N9: TMenuItem;
    pmiCols: TMenuItem;
    pmiColsHide: TMenuItem;
    pmiColsShow: TMenuItem;
    pmiRows: TMenuItem;
    pmiRowsHide: TMenuItem;
    pmiRowsShow: TMenuItem;
    Show1: TMenuItem;
    Hide1: TMenuItem;
    pmiFormatCells: TMenuItem;
    imgStandart: TImageList;
    N1: TMenuItem;
    Panel1: TPanel;
    Label1: TLabel;
    ProgressBarPanel: TPanel;
    ProgressBar1: TProgressBar;
    Label2: TLabel;
    Panel4: TPanel;
    Panel5: TPanel;
    Button2: TButton;
    Panel6: TPanel;
    Button1: TButton;
    Button3: TButton;
    pnl1: TPanel;
    btn1: TButton;
    btnABButton1: TButton;
    procedure Button3Click(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure pmiCutClick(Sender: TObject);
    procedure pmiCopyClick(Sender: TObject);
    procedure pmiPasteClick(Sender: TObject);
    procedure actMerge1Click(Sender: TObject);
    procedure actSplitCells1Click(Sender: TObject);
    procedure pmiColsHideClick(Sender: TObject);
    procedure pmiColsShowClick(Sender: TObject);
    procedure pmiRowsHideClick(Sender: TObject);
    procedure pmiRowsShowClick(Sender: TObject);
    procedure Show1Click(Sender: TObject);
    procedure Hide1Click(Sender: TObject);
    procedure pmiFormatCellsClick(Sender: TObject);
    procedure cxSpreadSheet1SheetPopupMenu(Sender: TObject; X, Y: Integer);
    procedure cxSpreadSheet1ContextPopup(Sender: TObject; MousePos: TPoint;
      var Handled: Boolean);
    procedure btnABButton1Click(Sender: TObject);
    procedure ABButton2Click(Sender: TObject);
    procedure cxSpreadSheet1Progress(Sender: TObject; APercent: Byte);
    procedure cxSpreadSheet1Editing(Sender: TcxSSBookSheet; const ACol,
      ARow: Integer; var CanEdit: Boolean);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    function getcelltext(acloumn, arow: Integer): string;
    function Setcelltext(acloumn, arow: Integer; atext: string): boolean;
    { Public declarations }
  end;

//得到由开窗输入的第一列数据组成的SQL条件
//aFieldCaption=字段的标签
//aSpaceSign=返回字段条件的分隔符
//aIsQuoted=返回字段条件是否要加首尾引号
//aResultAddBeginStr=返回字段条件附加的开始字串
//aResultAddEndStr=返回字段条件附加的结尾字串
function ABGetFieldMultiRowWhere(aFieldCaption:string;aSpaceSign:string=',';aIsQuoted:Boolean=true;aResultAddBeginStr:string=' in (';aResultAddEndStr:string=')'):string;

//得到由开窗输入的第一列数据组成的Tstrings
//aRemark=描述
function ABGetFieldMultiRow(aRemark:string;aColRount:LongInt=1;aSpace:string='';aAddConst:string=''):Tstrings;

//导入数据到数据集
//aInputDataset=导数据的目标数据集
//aKeyFieldName=目标数据集的主键字段名，多字段时用逗号分隔

//aReplaceFieldNameExpressions=字段替换列表,格式为:导入数据集字段名=导入数据对应的替换数据集字段=替换数据集对应导入数据集字段名，多组时用逗号分隔
//  如 Pe_Ca_Guid=Ca_Name=Ca_Guid
//aReplaceFieldDatasets=提供字段替换列表对应值的替换数据集数组

//aInputDataConstFieldNames=导入数据列对应的字段名，多字段时用逗号分隔
//  如有内容则代表的字段名与导入数据的列顺序要一致
//  如为空则表示首行是标题行，此时导入数据的列顺序可任意

//aCheckDataset=检测导入数据是否合法的数据集
//aCheckFieldNames=检测数据集的检测字段名(检测数据集和导入数据集中都存在检测字段)，多字段时用逗号分隔

//aCaption=开窗显示的标题
function ABInputDataToDataset(aInputDataset:TDataSet;
                              aKeyFieldNames:string;

                              aReplaceFieldNameExpressions:string;
                              aReplaceFieldDatasets:array of TDataSet;
                              aReplaceFieldDatasetAutoAppend:boolean=False;

                              aInputDataFieldCaptions:string='';
                              aInputDataConstFieldNames:string='';

                              aCheckDataset:TDataSet=nil;
                              aCheckFieldNames:string='';

                              aTitleCaption:string='';
                              aFormCaption:string=''):boolean;

implementation

{$R *.dfm}

var
  ABImportTableDataForm: TABImportDataForm;
  ABImportFieldMultiRowWhereForm: TABImportDataForm;

function ABGetFieldMultiRow(aRemark:string;aColRount:LongInt;aSpace:string;aAddConst:string):Tstrings;
var
  i,j:longint;
  tempStr:string;
begin
  Result:=TStringList.Create;
  if not Assigned(ABImportFieldMultiRowWhereForm) then
  begin
    ABImportFieldMultiRowWhereForm := TABImportDataForm.Create(nil);
    ABImportFieldMultiRowWhereForm.Panel5.Visible:=false;
  end;

  ABImportFieldMultiRowWhereForm.BringToFront;
  ABImportFieldMultiRowWhereForm.Label1.Caption:=aRemark;
  ABImportFieldMultiRowWhereForm.cxSpreadSheet1.Sheet.ClearAll;
  ABImportFieldMultiRowWhereForm.cxSpreadSheet1.Sheet.SelectCell(0,0);
  if ABImportFieldMultiRowWhereForm.ShowModal=mrok then
  begin
    for i := 0 to ABImportFieldMultiRowWhereForm.cxSpreadSheet1.Sheet.RowCount - 1 do
    begin
      if aColRount=1 then
      begin
        tempStr:=ABImportFieldMultiRowWhereForm.getcelltext(0,i)+aAddConst;
      end
      else
      begin
        tempStr:=EmptyStr;
        for j := 0 to aColRount-1 do
        begin
          ABAddstr(tempStr,ABImportFieldMultiRowWhereForm.getcelltext(j,i),aSpace);
        end;
        tempStr:=tempStr+aAddConst;
      end;

      if tempStr<>emptystr then
      begin
        Result.Add(tempStr);
      end
      else
      begin
        Break;
      end;
    end;
  end;
end;

function ABGetFieldMultiRowWhere(aFieldCaption:string;aSpaceSign:string;aIsQuoted:Boolean;aResultAddBeginStr:string;aResultAddEndStr:string):string;
var
  i:longint;
  tempStr:string;
  tempList: Tstrings;
begin
  tempList := ABGetFieldMultiRow('导入查询仅支持约1000行以下的数据查询,请确认第一列为需要查询的'+aFieldCaption);
  try
    if tempList.Count>0 then
    begin
      for i := 0 to tempList.Count - 1 do
      begin
        if aIsQuoted then
        begin
          tempStr:=QuotedStr(tempList[i]);
        end
        else
        begin
          tempStr:=tempList[i];
        end;

        ABAddstr(Result,tempStr,aSpaceSign);
      end;

      if Result<>EmptyStr then
      begin
        Result:= aResultAddBeginStr+Result+aResultAddEndStr;
      end;
    end;
  finally
    tempList.Free;
  end;
end;

procedure TABImportDataForm.Button1Click(Sender: TObject);
begin
  ModalResult:=mrOk;
end;

procedure TABImportDataForm.Button3Click(Sender: TObject);
begin
  if not Button2.Enabled then
  begin
    Button2.Enabled:=True;
  end
  else
    close;
end;

function TABImportDataForm.getcelltext(acloumn, arow: Integer): string;
var
  tempcxSSCellObject:TcxSSCellObject;
begin
  Result:=EmptyStr;
  tempcxSSCellObject:= cxSpreadSheet1.Sheet.GetCellObject(acloumn,arow);
  try
    Result:= trim(tempcxSSCellObject.DisplayText);
  finally
    tempcxSSCellObject.free;
  end;
end;

function TABImportDataForm.Setcelltext(acloumn, arow: Integer;atext:string):boolean;
var
  tempcxSSCellObject:TcxSSCellObject;
begin
  result:=false;
  if (acloumn>=0)  then
  begin
    tempcxSSCellObject:= cxSpreadSheet1.Sheet.GetCellObject(acloumn,arow);
    try
      tempcxSSCellObject.Text:=atext;
      result:=true;
    finally
      tempcxSSCellObject.free;
    end;
  end;
end;

procedure TABImportDataForm.btn1Click(Sender: TObject);
var
  i,j:LongInt;
  tempFileName:string;
  tempcxSSCellObject:TcxSSCellObject;
begin
  tempFileName:=ABSelectFile('','','97~2003 xls|*.xls');
  if tempFileName<>emptystr then
  begin
    Caption:= tempFileName;

    cxSpreadSheet1.BeginUpdate;
    cxSpreadSheet1.History.StartComplexAction(cxGetResourceString(@scxChangeCellsStyle));
    try
      cxSpreadSheet1.LoadFromFile(tempFileName);

      if Panel5.Visible then
      begin
        ProgressBarPanel.Visible:=True;
        ProgressBar1.Min:=0;
        ProgressBar1.Max:=100;
        ProgressBar1.Position:=0;
        try
          for I := 0 to cxSpreadSheet1.Sheet.RowCount -1 do
          begin
            Application.ProcessMessages;
            ProgressBar1.Position:=ProgressBar1.Position+1;
            Label2.Caption:=IntToStr(i+1)+'/'+inttostr(cxSpreadSheet1.Sheet.RowCount);
            for j := 0 to cxSpreadSheet1.Sheet.ColumnCount-1 do
            begin
              tempcxSSCellObject:=cxSpreadSheet1.Sheet.GetCellObject(j,i);
              try
                if (tempcxSSCellObject.Text<>emptystr) and (tempcxSSCellObject.Style.Format<>35) then
                  tempcxSSCellObject.Style.Format:=35;
              finally
                tempcxSSCellObject.free;
              end;
            end;
          end;
        finally
          ProgressBar1.Position:=0;
          ProgressBarPanel.Visible:=False;
        end;
      end;
    finally
      cxSpreadSheet1.History.StopComplexAction;
      cxSpreadSheet1.EndUpdate;
    end;
  end;
end;

procedure TABImportDataForm.cxSpreadSheet1ContextPopup(Sender: TObject;
  MousePos: TPoint; var Handled: Boolean);
begin
  if (MousePos.X = -1) and (MousePos.Y = -1) then
    MousePos := ClientToScreen(Point(0, 0))
  else
    MousePos := Mouse.CursorPos;
  pmSheetPopup.Popup(MousePos.X, MousePos.Y);
  Handled := True;
end;

procedure TABImportDataForm.cxSpreadSheet1Editing(Sender: TcxSSBookSheet;
  const ACol, ARow: Integer; var CanEdit: Boolean);
var
  i,j:LongInt;
  tempcxSSCellObject:TcxSSCellObject;
begin
  cxSpreadSheet1.BeginUpdate;
  cxSpreadSheet1.History.StartComplexAction(cxGetResourceString(@scxChangeCellsStyle));
  try
    for I := 0 to cxSpreadSheet1.Sheet.ColumnCount-1 do
    begin
      for j := 0 to cxSpreadSheet1.Sheet.RowCount-1 do
      begin
        tempcxSSCellObject:=cxSpreadSheet1.Sheet.GetCellObject(i,j);
        try
          if (tempcxSSCellObject.Text<>emptystr) and (tempcxSSCellObject.Style.Format<>35) then
            tempcxSSCellObject.Style.Format:=35;
        finally
          tempcxSSCellObject.free;
        end;
      end;
    end;
  finally
    cxSpreadSheet1.History.StopComplexAction;
    cxSpreadSheet1.EndUpdate;
  end;
end;

procedure TABImportDataForm.cxSpreadSheet1Progress(Sender: TObject;
  APercent: Byte);
begin
  Application.ProcessMessages;
end;

procedure TABImportDataForm.cxSpreadSheet1SheetPopupMenu(Sender: TObject; X,
  Y: Integer);
begin
  pmSheetPopup.Popup(X, Y);
end;

procedure TABImportDataForm.btnABButton1Click(Sender: TObject);
var
  tempFileName:string;
begin
  tempFileName:=ABSaveFile('','', 'xls|*.xls');
  if tempFileName<>EmptyStr then
  begin
    cxSpreadSheet1.SaveToFile(tempFileName);
  end;
end;

procedure TABImportDataForm.ABButton2Click(Sender: TObject);
begin
  ModalResult:=mrOk;
end;

procedure TABImportDataForm.pmiPasteClick(Sender: TObject);
begin
  cxSpreadSheet1.Sheet.Paste(cxSpreadSheet1.Sheet.SelectionRect.TopLeft);
end;

procedure TABImportDataForm.pmiCopyClick(Sender: TObject);
begin
  cxSpreadSheet1.Sheet.Copy(cxSpreadSheet1.Sheet.SelectionRect, False);
end;

procedure TABImportDataForm.pmiCutClick(Sender: TObject);
begin
  cxSpreadSheet1.Sheet.Copy(cxSpreadSheet1.Sheet.SelectionRect, True);
end;

procedure TABImportDataForm.pmiFormatCellsClick(Sender: TObject);
begin
  cxSpreadSheet1.Sheet.FormatCells(cxSpreadSheet1.Sheet.SelectionRect);
end;

procedure TABImportDataForm.pmiRowsHideClick(Sender: TObject);
begin
  cxSpreadSheet1.Sheet.SetVisibleState(cxSpreadSheet1.Sheet.SelectionRect, True, True, False);
end;

procedure TABImportDataForm.pmiRowsShowClick(Sender: TObject);
begin
  cxSpreadSheet1.Sheet.SetVisibleState(cxSpreadSheet1.Sheet.SelectionRect, True, True, True);
end;

procedure TABImportDataForm.pmiColsHideClick(Sender: TObject);
begin
  cxSpreadSheet1.Sheet.SetVisibleState(cxSpreadSheet1.Sheet.SelectionRect, True, False, False);
end;

procedure TABImportDataForm.pmiColsShowClick(Sender: TObject);
begin
  cxSpreadSheet1.Sheet.SetVisibleState(cxSpreadSheet1.Sheet.SelectionRect, True, False, True);
end;

procedure TABImportDataForm.actMerge1Click(Sender: TObject);
begin
  cxSpreadSheet1.Sheet.SetMergedState(cxSpreadSheet1.Sheet.SelectionRect, True);
end;

procedure TABImportDataForm.actSplitCells1Click(Sender: TObject);
begin
  cxSpreadSheet1.Sheet.SetMergedState(cxSpreadSheet1.Sheet.SelectionRect, False);
end;

procedure TABImportDataForm.Hide1Click(Sender: TObject);
begin
  cxSpreadSheet1.Sheet.SetVisibleState(cxSpreadSheet1.Sheet.SelectionRect, False, True, False);
end;

procedure TABImportDataForm.Show1Click(Sender: TObject);
begin
  cxSpreadSheet1.Sheet.SetVisibleState(cxSpreadSheet1.Sheet.SelectionRect, False, True, True);
end;

var
  FInputDataset:TDataSet;
  FKeyFieldNames:string;

  FReplaceFieldNameExpressions:string;
  FReplaceFieldDatasets:array of TDataSet;
  FReplaceFieldDatasetAutoAppend:boolean;

  FInputDataConstFieldNames:string;

  FCheckDataset:TDataSet;
  FCheckFieldNames:string;

  FTitleCaption:string;
  FFormCaption:string;

procedure TABImportDataForm.Button2Click(Sender: TObject);
var
  tempFieldNameAndCaptionStrings,
  tempFieldNameAndCloumnIndexStrings,

  tempKeyFieldNameStrings,
  tempReplaceFieldNameExpressionStrings,
  tempInputDataConstFieldNameStrings,
  tempCheckFieldNameStrings:TStrings;

  tempStateColumnIndex:longint;
  tempCellText,
  tempCheckFieldValues,
  tempKeyFieldValues:string;
  tempField:TField;

  i,j,
  tempBegRow,
  tempRowCount:LongInt;

  tempFind,
  tempInputed:Boolean;
  tempSumNotInCheckDataset,tempSumPostError:string;

  function LocateCurRol(aRow:LongInt;aCheckFieldNameStrings:TStrings;aCheckDataset:TDataSet;var aLocateFieldValues:string):boolean;
  var
    j:LongInt;
    tempStr,
    tempFindFieldNames: string;
    tempFindFieldValues: array of Variant;
  begin
    Result:=True;
    if (aCheckFieldNameStrings.Count>0) and
       (Assigned(aCheckDataset)) then
    begin
      aLocateFieldValues:=EmptyStr;
      tempFindFieldNames:=EmptyStr;
      SetLength(tempFindFieldValues,aCheckFieldNameStrings.Count);
      for j := 0 to aCheckFieldNameStrings.Count-1 do
      begin
        ABAddstr(tempFindFieldNames,aCheckFieldNameStrings[j],';');
        tempStr:=getcelltext(StrToInt(tempFieldNameAndCloumnIndexStrings.Values[aCheckFieldNameStrings[j]]),aRow);
        ABAddstr(aLocateFieldValues,tempStr,',');
        tempFindFieldValues[j]:=tempStr;
      end;
      Result:=aCheckDataset.Locate(tempFindFieldNames,VarArrayOf(tempFindFieldValues),[loCaseInsensitive]);
    end;
  end;
  procedure FillFieldStrings;
  var
    i:LongInt;
    tempCaption:string;
    tempFieldName:string;
  begin
    if tempInputDataConstFieldNameStrings.Count=0 then
    begin
      for I := 0 to ABImportTableDataForm.cxSpreadSheet1.Sheet.ColumnCount-1 do
      begin
        tempCaption:= ABImportTableDataForm.getcelltext(i,0);
        if tempCaption=EmptyStr then
          Break;

        tempFieldName:=ABGetFieldNamesByDisplayLabels(FInputDataset,tempCaption);
        if tempFieldName<>EmptyStr then
        begin
          tempFieldNameAndCaptionStrings.Add(tempFieldName+'='+tempCaption);
          tempFieldNameAndCloumnIndexStrings.Add(tempFieldName+'='+inttostr(i));
        end;
      end;
    end
    else
    begin
      for I := 0 to tempInputDataConstFieldNameStrings.Count-1 do
      begin
        tempFieldName:=Trim(tempInputDataConstFieldNameStrings[i]);
        tempCaption:=Trim(FInputDataset.FieldByName(tempFieldName).DisplayLabel);

        tempFieldNameAndCaptionStrings.Add(tempFieldName+'='+tempCaption);
        tempFieldNameAndCloumnIndexStrings.Add(tempFieldName+'='+inttostr(i));
      end;
    end;
  end;
  function KeyFieldInExcel:boolean;
  var
    i:LongInt;
  begin
    result:=True;
    if tempKeyFieldNameStrings.Count>0 then
    begin
      for I := 0 to tempKeyFieldNameStrings.Count-1 do
      begin
        if tempFieldNameAndCloumnIndexStrings.IndexOfName(tempKeyFieldNameStrings[i])<0 then
        begin
          result:=False;
          ABShow('主键字段[%s]在导入的数据中不存在,请检查.',[tempKeyFieldNameStrings[i]]);
          break;
        end;
      end;
    end;
  end;
  function AddStateColumn:LongInt;
  var
    i:LongInt;
    tempText:string;
  begin
    Result:=-1;
    cxSpreadSheet1.BeginUpdate;
    try
      for I := 0 to cxSpreadSheet1.Sheet.ColumnCount-1 do
      begin
        tempText:= ABImportTableDataForm.getcelltext(i,0);
        if (AnsiCompareText(tempText,'导入状态')=0) or
           ((tempInputDataConstFieldNameStrings.Count>0) and
            (ABCheckBeginStr(tempText, '状态-') )
             ) then
        begin
          Result:=i;
          Break;
        end
      end;

      if (Result=-1) then
      begin
        for I := 0 to cxSpreadSheet1.Sheet.ColumnCount-1 do
        begin
          tempText:= ABImportTableDataForm.getcelltext(i,0);
          if (tempText=EmptyStr) then
          begin
            Setcelltext(i,0,'导入状态');
            Result:=i;
            break;
          end;
        end;
      end;

      //增加一空列作为状态列
      if (Result=-1) and
         (cxSpreadSheet1.Sheet.ColumnCount<256) then
      begin
        try
          Setcelltext(cxSpreadSheet1.Sheet.ColumnCount,0,'导入状态');
          Result:=cxSpreadSheet1.Sheet.ColumnCount-1;
        except
        end;
      end;
    finally
      cxSpreadSheet1.EndUpdate;
    end;
  end;
  //根据替换公式和替换数据集取得当前单元格需要的字段对应值
  function GetCellReplaceFieldValue(aFieldName,aCellValue:string):string;
  var
    tempIndex:LongInt;
    tempLocalFieldname,
    tempGetValueFieldname:string;

    tempValue:Variant;
  begin
    result:=aCellValue ;
    if (tempReplaceFieldNameExpressionStrings.Count>0) then
    begin
      tempIndex:=tempReplaceFieldNameExpressionStrings.IndexOfName(aFieldName);
      if (tempIndex>=0) then
      begin
        tempLocalFieldName   :=Trim(ABGetLeftRightStr(tempReplaceFieldNameExpressionStrings.ValueFromIndex[tempIndex],axdLeft));
        tempGetValueFieldname:=Trim(ABGetLeftRightStr(tempReplaceFieldNameExpressionStrings.ValueFromIndex[tempIndex],axdright));

        if (Assigned(FReplaceFieldDatasets[tempIndex].FindField(tempLocalFieldName))) and
           (Assigned(FReplaceFieldDatasets[tempIndex].FindField(tempGetValueFieldname)))  then
        begin
          tempValue:=FReplaceFieldDatasets[tempIndex].Lookup(tempLocalFieldName,result,tempGetValueFieldname);
          if not ABVarIsNull(tempValue) then
          begin
            result:=vartostrdef(tempValue,'');
          end
          else
          begin
            if FReplaceFieldDatasetAutoAppend then
            begin
              try
                FReplaceFieldDatasets[tempIndex].Append;
                FReplaceFieldDatasets[tempIndex].FindField(tempLocalFieldName).AsString:=result;
                FReplaceFieldDatasets[tempIndex].Post;
                result:=FReplaceFieldDatasets[tempIndex].FindField(tempGetValueFieldname).AsString;
              except
                FReplaceFieldDatasets[tempIndex].Cancel;
                raise;
              end;
            end
            else
            begin

            end;
          end;
        end;
      end;
    end;
  end;
begin
  if (cxSpreadSheet1.Sheet.RowCount<=0) or
     (cxSpreadSheet1.Sheet.ColumnCount<=0) then
  begin
    ABShow('没有需导入的数据,请检查.');
    exit;
  end;

  Button2.Enabled:=False;
  btn1.Enabled:=False;
  btnABButton1.Enabled:=False;
  cxSpreadSheet1.ReadOnly:=true;
  tempKeyFieldNameStrings               :=TStringList.Create;
  tempFieldNameAndCaptionStrings        :=TStringList.Create;
  tempFieldNameAndCloumnIndexStrings    :=TStringList.Create;
  tempReplaceFieldNameExpressionStrings :=TStringList.Create;
  tempInputDataConstFieldNameStrings    :=TStringList.Create;
  tempCheckFieldNameStrings             :=TStringList.Create;
  try
    ABStrsToStrings(FKeyFieldNames              ,',',tempKeyFieldNameStrings              );
    ABStrsToStrings(FReplaceFieldNameExpressions,',',tempReplaceFieldNameExpressionStrings);
    ABStrsToStrings(FInputDataConstFieldNames   ,',',tempInputDataConstFieldNameStrings   );
    ABStrsToStrings(FCheckFieldNames            ,',',tempCheckFieldNameStrings            );

    if (Assigned(FInputDataset)) then
    begin
      if not FInputDataset.Active then
        FInputDataset.Open;
      if FInputDataset.State in [dsedit,dsInsert] then
        FInputDataset.Post;
    end;

    if (Assigned(FCheckDataset)) then
    begin
      if not FCheckDataset.Active then
        FCheckDataset.Open;
      if FCheckDataset.State in [dsedit,dsInsert] then
        FCheckDataset.Post;
    end;

    FillFieldStrings;

    if tempFieldNameAndCaptionStrings.Count<=0 then
    begin
      if tempInputDataConstFieldNameStrings.Count=0 then
        ABShow('请确认第一行为数据标题行.')
      else
        ABShow('请设置正确的导入字段名.');
    end
    else
    begin
      if KeyFieldInExcel then
      begin
        tempRowCount := 0;
        tempFind:=false;
        //确定行数,行中所有导入字段为空认为是行的结束
        while not tempFind do
        begin
          tempFind:=true;
          for I := 0 to tempFieldNameAndCloumnIndexStrings.Count-1 do
          begin
            tempCellText:=getcelltext(StrToInt(tempFieldNameAndCloumnIndexStrings.ValueFromIndex[i]),tempRowCount);
            if tempCellText<>EmptyStr then
            begin
              tempFind:=False;
              break;
            end;
          end;
          if tempFind then
            break;

          tempRowCount:=tempRowCount+ 1;
        end;

        if tempRowCount>0 then
        begin
          //在可能的情况下增加状态列
          tempStateColumnIndex:= AddStateColumn;

          ProgressBarPanel.Visible:=true;
          ProgressBar1.Min:=0;
          ProgressBar1.Max:=tempRowCount;
          ProgressBar1.Position:=0;
          FInputDataset.DisableControls;
          if (Assigned(FCheckDataset)) then
            FCheckDataset.DisableControls;
          try
            tempSumNotInCheckDataset :=EmptyStr;
            tempSumPostError  :=EmptyStr;
            tempBegRow:=1;
            if tempInputDataConstFieldNameStrings.Count>0 then
              tempBegRow:=0;

            tempInputed:=false;
            for I := tempBegRow to tempRowCount - 1 do
            begin
              if not tempInputed then
                tempInputed:=true;

              Application.ProcessMessages;
              Label2.Caption:=IntToStr(i+1)+'/'+inttostr(tempRowCount);
              ProgressBar1.Position:=i+1;
              if Button2.Enabled then
                exit;

              //判断检测字段是否在检测数据集中,不存在的在导入后将给出报告
              tempCheckFieldValues:=EmptyStr;
              if not LocateCurRol(i,tempCheckFieldNameStrings,FCheckDataset,tempCheckFieldValues) then
              begin
                ABAddstr(tempSumNotInCheckDataset,tempCheckFieldValues+ABEnterWrapStr,'');
                if tempStateColumnIndex>=0 then
                  Setcelltext(tempStateColumnIndex,i,'状态-检测数据集中不存在');
                Continue;
              end;

              tempKeyFieldValues:=EmptyStr;
              //EXCEL中有主键的情况时新增数据并写入主键内容
              if tempKeyFieldNameStrings.Count>0 then
              begin
                if LocateCurRol(i,tempKeyFieldNameStrings,FInputDataset,tempKeyFieldValues) then
                begin
                  FInputDataset.Edit;
                end
                else
                begin
                  FInputDataset.Append;
                end;
              end
              else
              //EXCEL中无主键的情况时新增数据
              begin
                FInputDataset.Append;
              end;

              //遍历所有列，插入到数据集中
              if FInputDataset.State in [dsedit,dsinsert] then
              begin
                for j := 0 to cxSpreadSheet1.Sheet.ColumnCount - 1 do
                begin
                  //状态列不导入
                  if tempStateColumnIndex=j then
                  begin
                    Continue;
                  end;

                  //取当前列值,无内容时则跳到下一列
                  tempCellText:=getcelltext(j,i);
                  if tempCellText=EmptyStr then
                  begin
                    Continue;
                  end;

                  //取得列对应的字段
                  //固定列方式时取得固定列对应的字段信息
                  if (j<tempInputDataConstFieldNameStrings.Count) then
                  begin
                    tempField:= FInputDataset.FindField(tempInputDataConstFieldNameStrings[j]);
                  end
                  else
                  //标题列方式时根据当前EXCEL首行的字段标签取出字段信息
                  begin
                    tempField:= ABGetFieldByDisplayLabel(FInputDataset,getcelltext(j,0));
                  end;

                  if Assigned(tempField) then
                  begin
                    tempCellText:=GetCellReplaceFieldValue(tempField.FieldName,tempCellText);
                    if tempCellText<>EmptyStr then
                    begin
                      if (tempField.DataType=ftBoolean)  then
                      begin
                        tempField.Value:=ABStrToBool(tempCellText);
                      end
                      else if (tempField.DataType=ftDate)  then
                      begin
                        tempField.Value:=ABStrToDate(tempCellText);
                      end
                      else if (tempField.DataType=ftDateTime) or
                              (tempField.DataType=ftTimeStamp)  then
                      begin
                        tempField.Value:=ABStrToDateTime(tempCellText);
                      end
                      else
                        tempField.AsString:=tempCellText;
                    end;
                  end;
                end;

                //一行结束时数据集的保存处理
                try
                  FInputDataset.Post;
                  Setcelltext(tempStateColumnIndex,i,'状态-导入成功');
                except
                  FInputDataset.Cancel;
                  ABAddstr(tempSumPostError,ABIIF(tempCheckFieldValues=emptystr,emptystr,'检测字段:['+tempCheckFieldValues+']')+
                                            ABIIF(tempKeyFieldValues=emptystr,emptystr,'主键字段:['+tempKeyFieldValues+']')+ABEnterWrapStr,'');

                  Setcelltext(tempStateColumnIndex,i,'状态-导入保存失败');
                  if (I<(tempRowCount-1)) and
                     (ABShow(ABIIF(tempCheckFieldValues<>emptystr,emptystr,'检测字段:['+tempCheckFieldValues+']')+ABEnterWrapStr+
                             ABIIF(tempKeyFieldValues<>emptystr,emptystr,'主键字段:['+tempKeyFieldValues+']')+ABEnterWrapStr+
                             '导入失败,继续导入还是退出?',[],['继续','退出'],1)=2) then
                  begin
                    break;
                  end;
                end;
              end;
            end;

            if (tempSumNotInCheckDataset=EmptyStr) and (tempSumPostError=EmptyStr) then
            begin
              if tempInputed then
              begin
                ABShow('全部导入成功.');
              end
              else
              begin
                ABShow('没有数据可以导入,请确认数据是否正确.');
              end;
            end
            else
            begin
              if trim(tempSumNotInCheckDataset)<>EmptyStr then
                ABShow('导入结束,没在检测数据集中找到的数据如下',tempSumNotInCheckDataset);

              if trim(tempSumPostError)<>EmptyStr then
              begin
                ABShow('导入结束,导入出错的数据如下',tempSumPostError);
              end;
            end;
          finally
            FInputDataset.EnableControls;
            if (Assigned(FCheckDataset)) then
              FCheckDataset.EnableControls;
            ProgressBar1.Position:=0;
            ProgressBarPanel.Visible:=false;
          end;
        end;
      end;
    end;
  finally
    tempKeyFieldNameStrings               .Free;
    tempFieldNameAndCaptionStrings        .Free;
    tempFieldNameAndCloumnIndexStrings    .Free;
    tempReplaceFieldNameExpressionStrings .Free;
    tempInputDataConstFieldNameStrings    .Free;
    tempCheckFieldNameStrings             .Free;
    Button2.Enabled:=True;
    btn1.Enabled:=True;
    btnABButton1.Enabled:=True;
    cxSpreadSheet1.ReadOnly:=False;
  end;
end;

function ABInputDataToDataset(aInputDataset:TDataSet;
                              aKeyFieldNames:string;

                              aReplaceFieldNameExpressions:string;
                              aReplaceFieldDatasets:array of TDataSet;
                              aReplaceFieldDatasetAutoAppend:boolean;

                              aInputDataFieldCaptions:string;
                              aInputDataConstFieldNames:string;

                              aCheckDataset:TDataSet;
                              aCheckFieldNames:string;

                              aTitleCaption:string;
                              aFormCaption:string):boolean;
var
  i:longint;
begin
  result:=false;
  FInputDataset                :=  aInputDataset                  ;
  FKeyFieldNames               :=  aKeyFieldNames                 ;

  FReplaceFieldNameExpressions :=  aReplaceFieldNameExpressions   ;
  SetLength(FReplaceFieldDatasets,High(aReplaceFieldDatasets)+1)  ;
  for I := Low(aReplaceFieldDatasets) to High(aReplaceFieldDatasets) do
  begin
    FReplaceFieldDatasets[i]   :=  aReplaceFieldDatasets[i]       ;
  end;
  FReplaceFieldDatasetAutoAppend:=aReplaceFieldDatasetAutoAppend;
  FInputDataConstFieldNames    :=  aInputDataConstFieldNames      ;

  FCheckDataset                :=  aCheckDataset                  ;
  FCheckFieldNames             :=  aCheckFieldNames               ;

  FTitleCaption                :=  aTitleCaption                  ;
  FFormCaption                 :=  aFormCaption                   ;

  if not Assigned(ABImportTableDataForm) then
  begin
    ABImportTableDataForm := TABImportDataForm.Create(nil);
    ABImportTableDataForm.Panel6.Visible:=False;
  end;

  if FFormCaption<>emptystr then
    ABImportTableDataForm.Caption:=FFormCaption
  else
    ABImportTableDataForm.Caption:='导入数据';

  if FTitleCaption<>emptystr then
    ABImportTableDataForm.Label1.Caption:=FTitleCaption
  else
    ABImportTableDataForm.Label1.Caption:=' '+'请确认第一行为数据标题';

  ABImportTableDataForm.cxSpreadSheet1.Sheet.ClearAll;
  if FInputDataConstFieldNames=emptystr then
  begin
    for I := 1 to ABGetSpaceStrCount(aInputDataFieldCaptions ,',') do
    begin
      ABImportTableDataForm.Setcelltext(i-1,0,ABGetSpaceStr(aInputDataFieldCaptions, i, ','));
    end;
  end;

  ABImportTableDataForm.cxSpreadSheet1.Sheet.SelectCell(0,0);
  if ABImportTableDataForm.ShowModal=mrok then
  begin

  end;
end;

procedure ABFinalization;
begin
  if Assigned(ABImportTableDataForm) then
    FreeAndNil(ABImportTableDataForm);
  if Assigned(ABImportFieldMultiRowWhereForm) then
    FreeAndNil(ABImportFieldMultiRowWhereForm);
end;

Initialization

Finalization
  ABFinalization;


end.


