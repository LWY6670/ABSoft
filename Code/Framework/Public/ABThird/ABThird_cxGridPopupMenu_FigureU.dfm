object ABcxGridPopupMenu_FigureForm: TABcxGridPopupMenu_FigureForm
  Left = 292
  Top = 251
  Caption = #22270#24418#20998#26512#37197#21046
  ClientHeight = 371
  ClientWidth = 613
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Notebook1: TNotebook
    Left = 0
    Top = 0
    Width = 613
    Height = 371
    Align = alClient
    Ctl3D = False
    PageIndex = 1
    ParentCtl3D = False
    TabOrder = 0
    object TPage
      Left = 0
      Top = 0
      Caption = '1'
      DesignSize = (
        613
        371)
      object GroupBox1: TGroupBox
        Left = 17
        Top = 16
        Width = 573
        Height = 90
        Anchors = [akLeft, akTop, akRight]
        TabOrder = 0
        DesignSize = (
          573
          90)
        object ABcxLabel1: TLabel
          Left = 17
          Top = 21
          Width = 26
          Height = 20
          AutoSize = False
          Caption = 'X'#36724':'
          Transparent = True
        end
        object ABcxLabel2: TLabel
          Left = 17
          Top = 47
          Width = 26
          Height = 20
          AutoSize = False
          Caption = 'Y'#36724':'
          Transparent = True
        end
        object ABcxComboBox1: TComboBox
          Left = 49
          Top = 20
          Width = 507
          Height = 21
          Anchors = [akLeft, akTop, akRight]
          TabOrder = 0
        end
        object ABcxCheckComboBox1: TcxCheckComboBox
          Left = 49
          Top = 47
          Anchors = [akLeft, akTop, akRight]
          Properties.ShowEmptyText = False
          Properties.DropDownSizeable = True
          Properties.EditValueFormat = cvfCaptions
          Properties.Items = <
            item
              Description = 'aa'
            end
            item
              Description = 'bb'
            end>
          TabOrder = 1
          Width = 507
        end
      end
      object ABcxButton1: TButton
        Left = 146
        Top = 120
        Width = 75
        Height = 25
        Caption = #21462#28040
        TabOrder = 1
        OnClick = ABcxButton1Click
      end
      object ABcxButton2: TButton
        Left = 56
        Top = 120
        Width = 75
        Height = 25
        Caption = #30830#35748
        TabOrder = 2
        OnClick = ABcxButton2Click
      end
    end
    object TPage
      Left = 0
      Top = 0
      Caption = '2'
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel1: TPanel
        Left = 0
        Top = 339
        Width = 613
        Height = 32
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 1
        DesignSize = (
          613
          32)
        object Label1: TLabel
          Left = 195
          Top = 10
          Width = 48
          Height = 13
          Caption = #26174#31034#27169#24335
        end
        object Label2: TLabel
          Left = 354
          Top = 10
          Width = 24
          Height = 13
          Caption = #35282#24230
        end
        object Button3: TButton
          Left = 536
          Top = 3
          Width = 71
          Height = 24
          Anchors = [akTop, akRight]
          Caption = #36864#20986
          TabOrder = 2
          OnClick = Button3Click
        end
        object RadioGroup1: TRadioGroup
          Left = 2
          Top = -1
          Width = 187
          Height = 30
          Columns = 4
          Ctl3D = True
          ItemIndex = 0
          Items.Strings = (
            #26609#24418
            #39292#24418
            #32447#24418
            #38754#31215)
          ParentCtl3D = False
          TabOrder = 0
          OnClick = RadioGroup1Click
        end
        object Button4: TButton
          Left = 462
          Top = 3
          Width = 71
          Height = 24
          Anchors = [akTop, akRight]
          Caption = #39640#32423
          TabOrder = 1
          OnClick = Button4Click
        end
        object ComboBox1: TComboBox
          Left = 249
          Top = 7
          Width = 101
          Height = 21
          TabOrder = 3
          OnChange = ComboBox1Change
        end
        object cxSpinEdit1: TSpinEdit
          Left = 386
          Top = 7
          Width = 55
          Height = 22
          MaxValue = 0
          MinValue = 0
          TabOrder = 4
          Value = 0
          OnChange = cxSpinEdit1PropertiesChange
        end
      end
      object Chart1: TChart
        Left = 0
        Top = 0
        Width = 613
        Height = 339
        BackWall.Brush.Style = bsClear
        Foot.Text.Strings = (
          '')
        Foot.Visible = False
        Gradient.EndColor = clGray
        Gradient.StartColor = clSilver
        Gradient.Visible = True
        Legend.Brush.Style = bsClear
        Title.Text.Strings = (
          'TChart')
        View3DOptions.Elevation = 315
        View3DOptions.Perspective = 0
        View3DOptions.Rotation = 360
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        DefaultCanvas = 'TGDIPlusCanvas'
        ColorPaletteIndex = 13
        object Series1: TBarSeries
          Marks.Style = smsLabelPercentTotal
          SeriesColor = clRed
          BarStyle = bsCilinder
          XValues.Name = 'X'
          XValues.Order = loNone
          YValues.Name = #38271#26465
          YValues.Order = loNone
          Left = 56
          Top = 248
        end
      end
    end
  end
end
