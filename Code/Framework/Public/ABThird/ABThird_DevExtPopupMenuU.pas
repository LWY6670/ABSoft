{
cxGrid右键菜单单元
Tag=19003 //TableView.Columns[i].Tag=19003 表示此列是计算列;
}
unit ABThird_DevExtPopupMenuU;

interface
{$I ..\ABInclude.ini}

uses
  ABPubScriptU,
  ABPubCharacterCodingU,
  ABPubVarU,
  ABPubMessageU,
  ABPubMemoU,
  ABPubUserU,
  ABPubConstU,
  ABPubSelectStrU,
  ABPubFuncU,
  ABPubSelectListBoxU,

  ABThirdCustomQueryU,
  ABThirdConnU,
  ABThirdFuncU,
  ABThird_cxGridPopupMenu_PrintU,
  ABThird_cxGridPopupMenu_AddColumU,
  ABThird_cxGridPopupMenu_ColorSetupU,
  ABThird_cxGridPopupMenu_FigureU,
  ABThirdcxGridAndcxTreeViewSearchU,

  dxCore,
  cxExportPivotGridLink,
  cxDBPivotGrid,
  cxEdit,
  cxTextEdit,
  cxGridDBDataDefinitions,
  cxGridExportLink,
  cxGridCustomView,
  cxStyles,
  cxGraphics,
  cxCustomData,
  cxgrid,
  cxGridDBBandedTableView,
  cxGridBandedTableView,
  cxGridCustomTableView,
  cxGridDBTableView,
  cxGridTableView,

  Windows,SysUtils,Variants,Classes,Graphics,Forms,Menus,TypInfo,Dialogs,Controls,
  db;

type
  //获取列显示时的事件,如计算字段,翻译CXGRID显示内容时会用到
  TColumnGetDataTextEvent = record
    ColumnName:string;
    SQL:string;
    ColumnType:string;
    EditValues:TStrings;
    Self:TcxGridColumn;
    FOldColumnGetDataText: TcxGridGetDataTextEvent;
  end;
  PColumnGetDataTextEvent = ^TColumnGetDataTextEvent;

  //序号重画的事件
  TIndicatorCellCustomDrawEvent = record
    TableViewName:string;
    FOldIndicatorCellCustomDraw : TcxGridIndicatorCellCustomDrawEvent;
    Self:TcxGridTableView ;
  end;
  PIndicatorCellCustomDrawEvent = ^TIndicatorCellCustomDrawEvent;

  //CELL单元格重画的事件,如依数据控制行列的颜色会用到
  TCellCustomDrawEvent = record
    TableViewName:string;
    FOldCellCustomDraw : TcxGridTableDataCellCustomDrawEvent;
    FOldStylesGetContentStyle:TcxGridGetCellStyleEvent;
    ColorCommandtext:string;
    Self:TcxGridTableView;
  end;
  PCellCustomDrawEvent = ^TCellCustomDrawEvent;

  TRefreshMenuItemVisibleEvent = procedure(aMenuItemNameList:TStrings) of object;
  //CXVIEW的右键菜单
  TABcxGridPopupMenu= class(TPopupMenu )
  private
    FOldSummary:TcxSummaryEvent;
    FOldGroupSummary:TcxSummaryEvent;

    //内部右键菜单列表
    FMenuItemsList: TStrings;
    //关联的表格控件
    FLinkTableView: TcxGridTableView;
    //计算列用的事件
    FColumnsOnGetDataText: TStringList;
    //序号显示所用的事件
    FIndicatorCellCustomDraw: TStringList;
    //颜色,计算等所用
    FCellCustomDraw: TStringList;
    //颜色所用的StyleRepository及列表
    FStyleRepository:TcxStyleRepository;
    FStyleList:TStringList;

    //颜色设置窗体
    FColorSetupForm:TForm;
    FCurSetColorStr:string;

    FAutoApplyBestFit: Boolean;
    FAutoCreateAllItem: Boolean;
    FSetupFileNameSuffix: string;
    FIsLoadViewProperty: boolean;
    FIsLoadViewStyleSheets: boolean;
    FIsLoadViewOtherSet: boolean;
    FIsLoadViewSetColor: boolean;
    FSetupFullFileName: string;
    FConnName: string;
    FCloseFootStr: Boolean;
    FOnRefreshMenuItemVisibleFalse: TRefreshMenuItemVisibleEvent;
    FOnRefreshMenuItemVisibleTrue: TRefreshMenuItemVisibleEvent;

    procedure PopupMenu_ShowGroup(Sender: TObject);
    procedure PopupMenu_AutoColumWidth(Sender: TObject);
    procedure PopupMenu_BestColumWidth(Sender: TObject);

    procedure PopupMenu_Order_Asc(Sender: TObject);
    procedure PopupMenu_Order_Desc(Sender: TObject);
    procedure PopupMenu_Order_DelAscORDesc(Sender: TObject);

    procedure PopupMenu_Agg_Sum(Sender: TObject);
    procedure PopupMenu_Agg_Count(Sender: TObject);
    procedure PopupMenu_Agg_Max(Sender: TObject);
    procedure PopupMenu_Agg_Min(Sender: TObject);
    procedure PopupMenu_Agg_Avg(Sender: TObject);
    procedure PopupMenu_Agg_Clear(Sender: TObject);

    procedure PopupMenu_Align_Left(Sender: TObject);
    procedure PopupMenu_Align_Right(Sender: TObject);
    procedure PopupMenu_Align_Center(Sender: TObject);

    procedure PopupMenu_Out_Excel(Sender: TObject);
    procedure PopupMenu_Out_XML(Sender: TObject);
    procedure PopupMenu_Out_Text(Sender: TObject);
    procedure PopupMenu_Out_Html(Sender: TObject);


    procedure PopupMenu_Find(Sender: TObject);
    procedure PopupMenu_CustomFilter(Sender: TObject);
    procedure PopupMenu_ColumnFilter(Sender: TObject);
    procedure PopupMenu_RowFilter(Sender: TObject);

    procedure PopupMenu_FilterBarShowMode_Always(Sender: TObject);
    procedure PopupMenu_FilterBarShowMode_Never(Sender: TObject);
    procedure PopupMenu_FilterBarShowMode_NonEmpty(Sender: TObject);

    procedure PopupMenu_ContentSelect_Cell(Sender: TObject);
    procedure PopupMenu_ContentSelect_RowSelect(Sender: TObject);
    procedure PopupMenu_ContentSelect_MultiRowSelect(Sender: TObject);
    procedure PopupMenu_ContentSelect_MultiRowCellSelect(Sender: TObject);

    procedure PopupMenu_Band_Show(Sender: TObject);
    procedure PopupMenu_Band_Add(Sender: TObject);
    procedure PopupMenu_Band_Del(Sender: TObject);
    procedure PopupMenu_Band_EditCaption(Sender: TObject);

    procedure PopupMenu_ConstBandPosition_Left(Sender: TObject);
    procedure PopupMenu_ConstBandPosition_Right(Sender: TObject);
    procedure PopupMenu_ConstBandPosition_None(Sender: TObject);

    procedure PopupMenu_CaleColum_Add(Sender: TObject);
    procedure PopupMenu_CaleColum_Del(Sender: TObject);
    procedure PopupMenu_CaleColum_Edit(Sender: TObject);

    procedure PopupMenu_Figure(Sender: TObject);
    procedure PopupMenu_ColorSetup(Sender: TObject);
    procedure PopupMenu_Print(Sender: TObject);

    procedure PopupMenu_Admin_SetupHaveField(Sender: TObject);
    procedure PopupMenu_Admin_SetupNoHaveField(Sender: TObject);
    procedure PopupMenu_Admin_SetupReadOnlyField(Sender: TObject);
    procedure PopupMenu_Admin_SetupEditField(Sender: TObject);
    procedure PopupMenu_Admin_ClearSetupField(Sender: TObject);

    procedure PopupMenu_Admin_ReCreateAllItems(Sender: TObject);
    procedure PopupMenu_Admin_TableViewCanEdit(Sender: TObject);

    procedure PopupMenu_Other_AutoRowNum(Sender: TObject);
    procedure PopupMenu_Other_FocusCellOnTab(Sender: TObject);
    procedure PopupMenu_Other_GoToNextCellOnEnter(Sender: TObject);
    procedure PopupMenu_Other_DirectionKeyMoveColumn(Sender: TObject);
    procedure PopupMenu_Other_ColumnHorzSizing(Sender: TObject);
    procedure PopupMenu_Other_DataRowSizing(Sender: TObject);
    procedure PopupMenu_Other_AutoRowHeight(Sender: TObject);
    procedure PopupMenu_Other_FullCollapse(Sender: TObject);
    procedure PopupMenu_Other_FullExpand(Sender: TObject);
    procedure PopupMenu_Other_ShowHintCloum(Sender: TObject);
    procedure PopupMenu_Other_ColumnsQuickCustomization(Sender: TObject);
    procedure PopupMenu_Other_ShowHeader(Sender: TObject);
    procedure PopupMenu_Other_EditHeaderCaption(Sender: TObject);
    procedure PopupMenu_Other_DisplayFormat(Sender: TObject);

    procedure PopupMenu_Other_ShowAggBar(Sender: TObject);
    procedure PopupMenu_Other_SumNumColumn(Sender: TObject);


    procedure PopupMenu_Setup_DoItems_DO(Sender: TObject);
    procedure PopupMenu_Setup_Clear(Sender: TObject);
    procedure PopupMenu_Setup_Default(Sender: TObject);
    procedure PopupMenu_Setup_Save(Sender: TObject);
    procedure PopupMenu_Setup_ToDatabase(Sender: TObject);

    procedure PopupMenu_Setup_SaveAndToDatabase(Sender: TObject);
  private
    procedure FreeStyleList;
    function GetStyleByColor(aColor: string): TcxStyle;

    procedure NewColumnsGetDataText(Sender: TcxCustomGridTableItem; ARecordIndex: Integer;
     var AText: String);

    procedure NewStylesGetContentStyle(
           Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
           AItem: TcxCustomGridTableItem;
          {$IF (CompilerVersion>22.0)}
          var
          {$ELSE}
          out
          {$IFEND}
            AStyle: TcxStyle
      );

    procedure NewSummary(
      ASender: TcxDataSummaryItems; Arguments: TcxSummaryEventArguments;
      var OutArguments: TcxSummaryEventOutArguments);
    procedure NewGroupSummary(ASender: TcxDataSummaryItems;
      Arguments: TcxSummaryEventArguments;
      var OutArguments: TcxSummaryEventOutArguments);

    procedure NewCustomDrawIndicatorCell(
      Sender: TcxGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxCustomGridIndicatorItemViewInfo; var ADone: Boolean);
    procedure Refresh;
    procedure SetLinkTableView(const Value: TcxGridTableView);
    function GetLinkGrid: TcxGrid;
    procedure SetIndicator(aActive: boolean);
    procedure SetColumnFoot(akind: TcxSummaryKind);
    procedure SetColumnAlign(aAlignment: TAlignment);
    procedure OutFile(aOutFileType: TABOutFileType);

    procedure FreeOldEVent_FCellCustomDraw;
    procedure FreeOldEVent_FColumnsOnGetDataText;
    procedure FreeOldEVent_FIndicatorCellCustomDraw;
    procedure CreatePopupMenu;
    procedure DestroyPopupMenu;

    function GetFilename(aTypeFlag:string): string;

    procedure OpreatorFile(aClear, aUpToDatabase, aDownFromDatabase,
      aSetToTempPath: boolean; aTempPathFileNameStrings: TStrings=nil);

    procedure PubColumnGetCellHint(
      Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
      ACellViewInfo: TcxGridTableDataCellViewInfo; const AMousePos: TPoint;
      var AHintText: TCaption; var AIsHintMultiLine: Boolean;
      var AHintTextRect: TRect);
    procedure LoadSetupFile;
    { Protected declarations }
  protected
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure DoPopup(Sender: TObject); override;
  public
    //关联的Grid
    property  LinkGrid: TcxGrid read GetLinkGrid;

    //通过FileStr来修改相关配制名称
    procedure ChangeFileNameBySetupFileNameSuffix(aOldFileStr,aNewFileStr:string);

    procedure FreeOldEVent;
    procedure CreateAllItems;
    procedure ClearAllItems;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  public
    //打开与关闭行号
    procedure OpenRowNum;
    procedure CloseRowNum;
    //加载与保存配制
    procedure Load;
    procedure Save;

    procedure LoadViewProperty;
    procedure LoadViewStyleSheets;
    procedure LoadViewSetColor(aInit:boolean=false);
    procedure LoadViewOtherSet;

    procedure SaveViewProperty;
    procedure SaveViewSetColor;
    procedure SaveViewStyleSheets;
    procedure SaveViewOtherSet;

    //是否加载过指定的项目
    property IsLoadViewProperty    :boolean read FIsLoadViewProperty    ;
    property IsLoadViewStyleSheets :boolean read FIsLoadViewStyleSheets ;
    property IsLoadViewSetColor    :boolean read FIsLoadViewSetColor    ;
    property IsLoadViewOtherSet    :boolean read FIsLoadViewOtherSet    ;
    { Public declarations }
  published
    //是否关闭SUM，Max，Min等待列的前置字符显示
    property  CloseFootStr: Boolean read FCloseFootStr write FCloseFootStr;

    //关联的表格控件
    property  LinkTableView: TcxGridTableView read FLinkTableView write SetLinkTableView;

    //加载时自动最佳列宽
    property  AutoApplyBestFit: Boolean read FAutoApplyBestFit write FAutoApplyBestFit;
    //加载时自动创建所有列
    property  AutoCreateAllItem: Boolean read FAutoCreateAllItem write FAutoCreateAllItem;

    //配制文件全名，当不为空时取得的配件文件名=SetupFullFileName+文件类型标识
    property  SetupFullFileName: string read FSetupFullFileName write FSetupFullFileName;
    //配制文件后缀名,当SetupFullFileName为空时取得的配件文件名=配制文件目录+所在窗体名+控件名+SetupFileNameSuffix+文件类型标识
    property  SetupFileNameSuffix: string read FSetupFileNameSuffix write FSetupFileNameSuffix;

    //刷新菜单项显示
    property OnRefreshMenuItemVisibleTrue:TRefreshMenuItemVisibleEvent read FOnRefreshMenuItemVisibleTrue    write FOnRefreshMenuItemVisibleTrue   ;
    //刷新菜单项不显示
    property OnRefreshMenuItemVisibleFalse:TRefreshMenuItemVisibleEvent read FOnRefreshMenuItemVisibleFalse    write FOnRefreshMenuItemVisibleFalse   ;
    { Published declarations }
  end;

  //cxDBPivotGrid 交叉表的右键菜单
  TABcxPivotGridPopupMenu= class(TPopupMenu )
  private
    //内部右键菜单列表
    FMenuItemsList: TStrings;

    FLinkcxDBPivotGrid: TcxDBPivotGrid;

    FSetupFileNameSuffix: string;
    FIsLoadViewProperty: boolean;
    FSetupFullFileName: string;
    FConnName: string;


    procedure OutFile(aOutFileType: TABOutFileType);
    function GetFilename(aTypeFlag:string): string;
    procedure SetLinkcxDBPivotGrid(const Value: TcxDBPivotGrid);


    procedure PopupMenu_Setup_ToDatabase(Sender: TObject);
    procedure PopupMenu_Setup_SaveAndToDatabase(Sender: TObject);
    procedure PopupMenu_Setup_Clear(Sender: TObject);
    procedure PopupMenu_Setup_Default(Sender: TObject);
    procedure PopupMenu_Setup_Save(Sender: TObject);
    procedure PopupMenu_Out_Excel(Sender: TObject);
    procedure PopupMenu_Out_XML(Sender: TObject);
    procedure PopupMenu_Out_Text(Sender: TObject);
    procedure PopupMenu_Out_Html(Sender: TObject);
    procedure PopupMenu_admin_OpenDesign(Sender: TObject);
    procedure PopupMenu_admin_CloseDesign(Sender: TObject);
  protected
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;

    procedure DoPopup(Sender: TObject); override;
    procedure CreatePopupMenu;
    procedure DestroyPopupMenu;

    procedure LoadViewProperty;
    procedure SaveViewProperty;
    { Protected declarations }
  public
    //通过FileStr来修改相关配制名称
    procedure ChangeFileNameBySetupFileNameSuffix(aOldFileStr,aNewFileStr:string);

    procedure OpreatorFile(aClear, aUpToDatabase, aDownFromDatabase,
      aSetToTempPath: boolean; aTempPathFileNameStrings: TStrings=nil);
    procedure Load;
    procedure Save;

    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    property IsLoadViewProperty    :boolean read FIsLoadViewProperty    ;

    { Public declarations }
  published
    property  LinkcxDBPivotGrid: TcxDBPivotGrid read FLinkcxDBPivotGrid write SetLinkcxDBPivotGrid;

    //配制文件全名，当不为空时取得的配件文件名=SetupFullFileName+文件类型标识
    property  SetupFullFileName: string read FSetupFullFileName write FSetupFullFileName;
    //配制文件后缀名,当SetupFullFileName为空时取得的配件文件名=配制文件目录+所在窗体名+控件名+SetupFileNameSuffix+文件类型标识
    property  SetupFileNameSuffix: string read FSetupFileNameSuffix write FSetupFileNameSuffix;
    { Published declarations }
  end;



implementation

var
  FLoadViewOtherSetStrings1,
  FLoadViewOtherSetStrings2 :TStrings;
  FSaveFileNames:array[0..4] of string=('_StyleSheets.ini','_ColorSet.ini','_Property.ini','_OtherSet.ini','_CaleColumnSQL.ini');

{ TABcxGridPopupMenu }

procedure TABcxGridPopupMenu.CloseRowNum;
begin
  SetIndicator(false);
end;

constructor TABcxGridPopupMenu.Create(AOwner: TComponent);
begin
  inherited;
  FConnName:='Main';
  FAutoApplyBestFit:=true;
  FAutoCreateAllItem:=true;
  FSetupFileNameSuffix:=emptystr;
  FSetupFullFileName:=emptystr;

  AutoHotkeys:=maManual;

  FStyleList:=TStringList.Create;
  FColumnsOnGetDataText:=TStringList.Create;
  FIndicatorCellCustomDraw:=TStringList.Create;
  FCellCustomDraw:=TStringList.Create;
  FMenuItemsList:=TStringList.Create;

  FStyleRepository:=TcxStyleRepository.Create(nil);

  if (AOwner is TcxGridTableView) then
    LinkTableView:=TcxGridTableView(AOwner);
end;

procedure TABcxGridPopupMenu.CreatePopupMenu;
begin
  if (Assigned(items)) and (items.Count<=0)  then
  begin
    ABCreateMenuItem(self,Items,'PopupMenu_ShowGroup'     ,'显示分组框',PopupMenu_ShowGroup     ,true);
    ABCreateMenuItem(self,Items,'PopupMenu_BestColumWidth','最佳列宽'  ,PopupMenu_BestColumWidth);
    ABCreateMenuItem(self,Items,'PopupMenu_AutoColumWidth','整页显示'  ,PopupMenu_AutoColumWidth,true);
    ABCreateMenuItem(self,Items,'','-');

    ABCreateMenuItem(self,Items,'PopupMenu_Order' ,'排序');
    ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Order_Asc'         ,'升序'    ,PopupMenu_Order_Asc         ,true,false,1,True);
    ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Order_Desc'        ,'降序'    ,PopupMenu_Order_Desc        ,true,false,1,True);
    ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Order_DelAscORDesc','清除排序',PopupMenu_Order_DelAscORDesc,true,false,1,True);

    ABCreateMenuItem(self,Items,'PopupMenu_Agg','数据汇总');
    ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Agg_Count'      ,'记录数'   ,PopupMenu_Agg_Count       ,true,false,2,True);
    ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Agg_Sum'        ,'合计'     ,PopupMenu_Agg_Sum         ,true,false,2,True);
    ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Agg_Max'        ,'最大值'   ,PopupMenu_Agg_Max         ,true,false,2,True);
    ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Agg_Min'        ,'最小值'   ,PopupMenu_Agg_Min         ,true,false,2,True);
    ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Agg_Avg'        ,'平均值'   ,PopupMenu_Agg_Avg         ,true,false,2,True);
    ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Agg_Clear'      ,'清除统计' ,PopupMenu_Agg_Clear       ,true,false,2,True);

    ABCreateMenuItem(self,Items,'PopupMenu_Align'      ,'数据对齐');
    ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Align_Left'  ,'左对齐'   ,PopupMenu_Align_Left   ,true,false,3,True);
    ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Align_Right' ,'右对齐'   ,PopupMenu_Align_Right  ,true,false,3,True);
    ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Align_Center','居中对齐' ,PopupMenu_Align_Center ,true,false,3,True);

    ABCreateMenuItem(self,Items,'PopupMenu_Out'        ,'数据输出');
    ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Out_Excel'   ,'Excel'    ,PopupMenu_Out_Excel);
    ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Out_XML'     ,'XML'      ,PopupMenu_Out_XML  );
    ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Out_Text'    ,'Text'     ,PopupMenu_Out_Text );
    ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Out_Html'    ,'Html'     ,PopupMenu_Out_Html );

    ABCreateMenuItem(self,Items,'PopupMenu_FindAndFilter' ,'数据查找与过滤');
    ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Find'   ,'全文查找'    ,PopupMenu_Find);
    ABCreateMenuItem(self,Items[Items.Count-1],'','-');
    ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_ColumnFilter'           ,'显示列过滤'      ,PopupMenu_ColumnFilter      ,true);
    ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_RowFilter'              ,'显示行过滤'      ,PopupMenu_RowFilter         ,true);
    ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_CustomFilter'           ,'自定义过滤'      ,PopupMenu_CustomFilter         );
    ABCreateMenuItem(self,Items[Items.Count-1],'','-');
    ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_FilterBarShowMode'   ,'过滤状态栏显示方式');
    ABCreateMenuItem(self,Items[Items.Count-1].Items[Items[Items.Count-1].Count-1],'PopupMenu_FilterBarShowMode_Always'   ,'总是显示'     ,PopupMenu_FilterBarShowMode_Always  ,true,false,4,True);
    ABCreateMenuItem(self,Items[Items.Count-1].Items[Items[Items.Count-1].Count-1],'PopupMenu_FilterBarShowMode_Never'    ,'总是不显示'     ,PopupMenu_FilterBarShowMode_Never   ,true,false,4,True);
    ABCreateMenuItem(self,Items[Items.Count-1].Items[Items[Items.Count-1].Count-1],'PopupMenu_FilterBarShowMode_NonEmpty' ,'有过滤时显示' ,PopupMenu_FilterBarShowMode_NonEmpty,true,false,4,True);

    ABCreateMenuItem(self,Items,'PopupMenu_ContentSelect','内容选择');
    ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_ContentSelect_Cell'               ,'可单元格选择'      ,PopupMenu_ContentSelect_Cell  ,true);
    ABCreateMenuItem(self,Items[Items.Count-1],'','-');
    ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_ContentSelect_RowSelect'          ,'单行选择'          ,PopupMenu_ContentSelect_RowSelect          ,true,false,5,True);
    ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_ContentSelect_MultiRowSelect'     ,'多行选择'          ,PopupMenu_ContentSelect_MultiRowSelect     ,true,false,5,True);
    ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_ContentSelect_MultiRowCellSelect' ,'多行单元格选择'    ,PopupMenu_ContentSelect_MultiRowCellSelect ,true,false,5,True);

    ABCreateMenuItem(self,Items,'','-');

    ABCreateMenuItem(self,Items,'PopupMenu_Band','表头');
    ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Band_Show'        ,'显示表头'     ,PopupMenu_Band_Show  ,true);
    ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Band_Add'         ,'增加表头'     ,PopupMenu_Band_Add);
    ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Band_Del'         ,'删除表头'     ,PopupMenu_Band_Del);
    ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Band_EditCaption' ,'修改表头'     ,PopupMenu_Band_EditCaption);
    ABCreateMenuItem(self,Items[Items.Count-1],'','-');
    ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_ConstBandPosition'   ,'固定表头位置');
    ABCreateMenuItem(self,Items[Items.Count-1].Items[Items[Items.Count-1].Count-1],'PopupMenu_ConstBandPosition_Left' ,'固定在左边'  ,PopupMenu_ConstBandPosition_Left ,true,false,6,True);
    ABCreateMenuItem(self,Items[Items.Count-1].Items[Items[Items.Count-1].Count-1],'PopupMenu_ConstBandPosition_Right','固定在右边'  ,PopupMenu_ConstBandPosition_Right,true,false,6,True);
    ABCreateMenuItem(self,Items[Items.Count-1].Items[Items[Items.Count-1].Count-1],'PopupMenu_ConstBandPosition_None' ,'清除固定'    ,PopupMenu_ConstBandPosition_None ,true,false,6,True);

    ABCreateMenuItem(self,Items,'PopupMenu_CaleColum','计算列');
    ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_CaleColum_Add'          ,'新增计算列'     ,PopupMenu_CaleColum_Add);
    ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_CaleColum_Del'          ,'删除计算列'     ,PopupMenu_CaleColum_Del);
    ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_CaleColum_Edit'         ,'修改计算列'     ,PopupMenu_CaleColum_Edit);

    ABCreateMenuItem(self,Items,'','-');

    ABCreateMenuItem(self,Items,'PopupMenu_Figure'            ,'图形分析'       ,PopupMenu_Figure);
    ABCreateMenuItem(self,Items,'PopupMenu_ColorSetup'        ,'配色设置'       ,PopupMenu_ColorSetup);
    ABCreateMenuItem(self,Items,'PopupMenu_Print'             ,'打印设置'       ,PopupMenu_Print);

    ABCreateMenuItem(self,Items,'','-');

    if ABPubUser.IsAdmin then
    begin
      ABCreateMenuItem(self,Items,'PopupMenu_Admin' ,'管理员配制');
      ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Admin_SetupHaveField'    ,'设置仅包含字段',PopupMenu_Admin_SetupHaveField);
      ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Admin_SetupNoHaveField'  ,'设置非包含字段',PopupMenu_Admin_SetupNoHaveField);
      ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Admin_SetupReadonlyField','设置只读字段'  ,PopupMenu_Admin_SetupReadonlyField);
      ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Admin_SetupEditField'    ,'设置只修改字段',PopupMenu_Admin_SetupEditField);
      ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Admin_ClearSetupField'    ,'清除所有设置',PopupMenu_Admin_ClearSetupField);

      ABCreateMenuItem(self,Items[Items.Count-1],'','-');
      ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Admin_TableViewCanEdit'  ,'表格能编辑'    ,PopupMenu_Admin_TableViewCanEdit,true);
      ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Admin_ReCreateAllItems'  ,'重建所有列'    ,PopupMenu_Admin_ReCreateAllItems);
    end;

    ABCreateMenuItem(self,Items,'PopupMenu_Other' ,'其它配制');
    ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Other_AutoRowNum'           ,'自动行号'         ,PopupMenu_Other_AutoRowNum,true);
    ABCreateMenuItem(self,Items[Items.Count-1],'','-');
    ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Other_FocusCellOnTab'        ,'Tab键移动单元格'  ,PopupMenu_Other_FocusCellOnTab        ,true);
    ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Other_GoToNextCellOnEnter'   ,'回车键移动单元格' ,PopupMenu_Other_GoToNextCellOnEnter   ,true);
    ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Other_DirectionKeyMoveColumn','方向键移动单元格' ,PopupMenu_Other_DirectionKeyMoveColumn,true);
    ABCreateMenuItem(self,Items[Items.Count-1],'','-');
    ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Other_ColumnHorzSizing','列宽可更改',PopupMenu_Other_ColumnHorzSizing,true);
    ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Other_DataRowSizing'   ,'行高可更改',PopupMenu_Other_DataRowSizing,true);
    ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Other_AutoRowHeight'   ,'自动行高'  ,PopupMenu_Other_AutoRowHeight,true);
    ABCreateMenuItem(self,Items[Items.Count-1],'','-');
    ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Other_FullExpand'  ,'展开分组',PopupMenu_Other_FullExpand);
    ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Other_FullCollapse','收缩分组',PopupMenu_Other_FullCollapse);
    ABCreateMenuItem(self,Items[Items.Count-1],'','-');
    ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Other_ShowHintCloum'            ,'选择/隐藏列'        ,PopupMenu_Other_ShowHintCloum);
    ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Other_ColumnsQuickCustomization','打开快速选择/隐藏列',PopupMenu_Other_ColumnsQuickCustomization,true);
    ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Other_ShowHeader'               ,'显示列标题栏'       ,PopupMenu_Other_ShowHeader,true);
    ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Other_EditHeaderCaption'        ,'修改列标题'         ,PopupMenu_Other_EditHeaderCaption);
    ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Other_DisplayFormat'            ,'修改列显示格式'     ,PopupMenu_Other_DisplayFormat);
    ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Other_SumNumColumn'             ,'汇总数值列'         ,PopupMenu_Other_SumNumColumn);
    ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Other_ShowAggBar'               ,'显示汇总栏'         ,PopupMenu_Other_ShowAggBar,true);
    ABCreateMenuItem(self,Items,'','-');

    ABCreateMenuItem(self,Items,'PopupMenu_Setup' ,'配制文件');
    ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Setup_Save'           ,'保存配置'    ,PopupMenu_Setup_Save);
    if ABPubUser.IsAdminOrSysuser then
    begin
      ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Setup_ToDatabase'     ,'上传服务器'  ,PopupMenu_Setup_ToDatabase);
      ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Setup_SaveAndToDatabase'     ,'保存后上传服务器',PopupMenu_Setup_SaveAndToDatabase);

      ABCreateMenuItem(self,Items[Items.Count-1],'','-');
      ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Setup_DoItems','批量操作');
      ABCreateMenuItem(self,Items[Items.Count-1].Items[Items[Items.Count-1].Count-1],'PopupMenu_Setup_DoItems_BestColumWidth' ,'最佳列宽'    ,nil,true,true);
      ABCreateMenuItem(self,Items[Items.Count-1].Items[Items[Items.Count-1].Count-1],'PopupMenu_Setup_DoItems_AutoColumWidth' ,'整页显示'    ,nil,true,true);
      ABCreateMenuItem(self,Items[Items.Count-1].Items[Items[Items.Count-1].Count-1],'PopupMenu_Setup_DoItems_SumNumColumn'   ,'汇总数值列'  ,nil,true,true);
      ABCreateMenuItem(self,Items[Items.Count-1].Items[Items[Items.Count-1].Count-1],'PopupMenu_Setup_DoItems_Save'           ,'保存配置'    ,nil,true,true);
      ABCreateMenuItem(self,Items[Items.Count-1].Items[Items[Items.Count-1].Count-1],'PopupMenu_Setup_DoItems_ToDatabase'     ,'上传服务器',nil,true,true);
      ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Setup_DoItems_DO','执行批量操作',PopupMenu_Setup_DoItems_DO);
      ABCreateMenuItem(self,Items[Items.Count-1],'','-');
      ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Setup_Clear','清除所有配制',PopupMenu_Setup_Clear);
    end;
    ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Down'      ,'恢复默认配制',PopupMenu_Setup_Default);

    ABGetMenuItems(self.Items,FMenuItemsList);
  end;
end;

procedure TABcxGridPopupMenu.SetLinkTableView(const Value: TcxGridTableView);
begin
  FLinkTableView := Value;
  if Assigned(FLinkTableView) then
    FLinkTableView.PopupMenu:=self;
end;

destructor TABcxGridPopupMenu.Destroy;
begin
  FreeOldEVent;
  FreeStyleList;

  FColumnsOnGetDataText.Free;
  FIndicatorCellCustomDraw.Free;
  FCellCustomDraw.Free;

  DestroyPopupMenu;

  FStyleRepository.Free;

  if Assigned(FColorSetupForm) then
    FColorSetupForm.free;

  FMenuItemsList.free;

  if (Assigned(FLinkTableView)) and
     (Assigned(FLinkTableView.DataController)) and
     (Assigned(FLinkTableView.DataController.Summary)) and
     (Assigned(FLinkTableView.DataController.Summary.FooterSummaryItems)) and
     (Assigned(FLinkTableView.DataController.Summary.FooterSummaryItems.OnSummary))  then
    FLinkTableView.DataController.Summary.FooterSummaryItems.OnSummary:=FOldSummary;

  if (Assigned(FLinkTableView)) and
     (Assigned(FLinkTableView.DataController)) and
     (Assigned(FLinkTableView.DataController.Summary)) and
     (Assigned(FLinkTableView.DataController.Summary.DefaultGroupSummaryItems)) and
     (Assigned(FLinkTableView.DataController.Summary.DefaultGroupSummaryItems.OnSummary))  then
    FLinkTableView.DataController.Summary.DefaultGroupSummaryItems.OnSummary:=FOldGroupSummary;

  inherited;
end;

procedure TABcxGridPopupMenu.FreeOldEVent;
begin
  FreeOldEVent_FColumnsOnGetDataText;
  FreeOldEVent_FIndicatorCellCustomDraw;
  FreeOldEVent_FCellCustomDraw;
end;

procedure TABcxGridPopupMenu.FreeOldEVent_FColumnsOnGetDataText;
var
  i:LongInt;
  tempColumnName:string;
begin
  if (Assigned(FLinkTableView)) and (Assigned(FColumnsOnGetDataText)) then
  begin
    for i := (FColumnsOnGetDataText.Count - 1) downto 0 do
    begin
      tempColumnName:=PColumnGetDataTextEvent(FColumnsOnGetDataText.Objects[i]).ColumnName;
      if (tempColumnName<>EmptyStr) and
         (Assigned(ABGetColumnByName(FLinkTableView,tempColumnName))) then
      begin
        PColumnGetDataTextEvent(FColumnsOnGetDataText.Objects[i]).Self.OnGetDataText:=
          PColumnGetDataTextEvent(FColumnsOnGetDataText.Objects[i]).FOldColumnGetDataText;
      end;
      if (Assigned(PColumnGetDataTextEvent(FColumnsOnGetDataText.Objects[i]).EditValues)) then
        PColumnGetDataTextEvent(FColumnsOnGetDataText.Objects[i]).EditValues.Free;

      Dispose(Pointer(FColumnsOnGetDataText.Objects[i]));
    end;
    FColumnsOnGetDataText.Clear;
  end;
end;

procedure TABcxGridPopupMenu.FreeStyleList;
var
  i:LongInt;
begin
  for i := (FStyleList.Count - 1) downto 0 do
  begin
    if Assigned(FStyleList.Objects[i]) then
    begin
      TcxStyle(FStyleList.Objects[i]).Free;
    end;
  end;
  FStyleList.Clear;
  FStyleList.Free;
end;

function TABcxGridPopupMenu.GetLinkGrid: TcxGrid;
begin
  result:= nil;
  if (Assigned(FLinkTableView)) and (Assigned(FLinkTableView.Control)) then
    result:= TcxGrid(FLinkTableView.Control);
end;

function TABcxGridPopupMenu.GetStyleByColor(aColor:string):TcxStyle;
var
  i:LongInt;
begin
  Result:=nil;
  i:=FStyleList.IndexOf(aColor);
  if i>=0 then
    Result:= TcxStyle(FStyleList.Objects[i]);

  if not Assigned(Result) then
  begin
    Result := TcxStyle.Create(Self);
    Result.TextColor :=StrToIntDef(aColor,0);
    FStyleList.AddObject(aColor,Result);
  end;
end;

procedure TABcxGridPopupMenu.FreeOldEVent_FIndicatorCellCustomDraw;
var
  i:LongInt;
begin
  if  (Assigned(FLinkTableView)) and (Assigned(FIndicatorCellCustomDraw)) then
  begin
    for i := (FIndicatorCellCustomDraw.Count - 1) downto 0 do
    begin
      PIndicatorCellCustomDrawEvent(FIndicatorCellCustomDraw.Objects[i]).Self.OnCustomDrawIndicatorCell:=
        PIndicatorCellCustomDrawEvent(FIndicatorCellCustomDraw.Objects[i]).FOldIndicatorCellCustomDraw;
      Dispose(Pointer(FIndicatorCellCustomDraw.Objects[i]));
    end;
    FIndicatorCellCustomDraw.Clear;
  end;
end;

procedure TABcxGridPopupMenu.FreeOldEVent_FCellCustomDraw;
var
  i:LongInt;
begin
  if (Assigned(FLinkTableView)) and  (Assigned(FCellCustomDraw)) then
  begin
    for i := (FCellCustomDraw.Count - 1) downto 0 do
    begin
      PCellCustomDrawEvent(FCellCustomDraw.Objects[i]).Self.OnCustomDrawCell:=
        PCellCustomDrawEvent(FCellCustomDraw.Objects[i]).FOldCellCustomDraw;

      if Assigned(PCellCustomDrawEvent(FCellCustomDraw.Objects[i]).Self.Styles) then
        PCellCustomDrawEvent(FCellCustomDraw.Objects[i]).Self.Styles.OnGetContentStyle:=
          PCellCustomDrawEvent(FCellCustomDraw.Objects[i]).FOldStylesGetContentStyle;

      Dispose(Pointer(FCellCustomDraw.Objects[i]));
    end;
    FCellCustomDraw.Clear;
  end;
end;

procedure TABcxGridPopupMenu.DestroyPopupMenu;
var
  i:longint;
begin
  for i := Items.Count-1 downto 0  do
  begin
    Items[0].Clear;
  end;
end;

procedure TABcxGridPopupMenu.DoPopup(Sender: TObject);
  procedure ABSetRadioItemMenuItemEnabledFalse;
  var
    i:longint;
  begin
    for I := 0 to FMenuItemsList.Count-1 do
    begin
      if (TMenuItem(FMenuItemsList.Objects[i]).RadioItem) then
      begin
        TMenuItem(FMenuItemsList.Objects[i]).Enabled:=False;
      end;
    end;
  end;

  procedure SetMenuItemChecked(aName:String;aChecked:Boolean);
  var
    tempMenuItem:TMenuItem;
  begin
    tempMenuItem:=ABGetMenuItem(FMenuItemsList,aName);
    if Assigned(tempMenuItem) then
    begin
      tempMenuItem.Checked:=aChecked;
      tempMenuItem.Enabled:=True;
    end;
  end;

  procedure SetMenuItemEnabled(aName:String;aEnabled:Boolean);
  var
    tempMenuItem:TMenuItem;
  begin
    tempMenuItem:=ABGetMenuItem(FMenuItemsList,aName);
    if Assigned(tempMenuItem) then
    begin
      tempMenuItem.Enabled:=aEnabled;
    end;
  end;

  procedure SetAllVisible(aVisible:boolean);
  var
    I: Integer;
  begin
    for I := 0 to FMenuItemsList.Count-1 do
    begin
      if TMenuItem(FMenuItemsList.Objects[i]).Visible<>aVisible  then
        TMenuItem(FMenuItemsList.Objects[i]).Visible:=aVisible;
    end;
  end;
  procedure RefreshMenuItemVisibleFalse;
  var
    tempVisibleMenuItems:TStrings;
    I: Integer;
  begin
    tempVisibleMenuItems:=TStringList.Create;
    try
      FOnRefreshMenuItemVisibleFalse(tempVisibleMenuItems);
      for I := 0 to FMenuItemsList.Count-1 do
      begin
        if tempVisibleMenuItems.IndexOf(FMenuItemsList.Strings[i])>=0 then
        begin
          if TMenuItem(FMenuItemsList.Objects[i]).Visible  then
            TMenuItem(FMenuItemsList.Objects[i]).Visible:=False;
        end;
      end;
    finally
      tempVisibleMenuItems.Free;
    end;
  end;
  procedure RefreshMenuItemVisibleTrue;
  var
    tempVisibleMenuItems:TStrings;
    I: Integer;
  begin
    tempVisibleMenuItems:=TStringList.Create;
    try
      FOnRefreshMenuItemVisibleTrue(tempVisibleMenuItems);
      for I := 0 to FMenuItemsList.Count-1 do
      begin
        if tempVisibleMenuItems.IndexOf(FMenuItemsList.Strings[i])>=0 then
        begin
          if not TMenuItem(FMenuItemsList.Objects[i]).Visible  then
            TMenuItem(FMenuItemsList.Objects[i]).Visible:=True;
        end;
      end;
    finally
      tempVisibleMenuItems.Free;
    end;
  end;
  procedure RefreshVisableItem;
  begin
    if (not Assigned(FOnRefreshMenuItemVisibleFalse)) and
       (not Assigned(FOnRefreshMenuItemVisibleTrue)) then
    begin
      SetAllVisible(True);
    end
    else
    begin
      if (Assigned(FOnRefreshMenuItemVisibleFalse)) and
         (Assigned(FOnRefreshMenuItemVisibleTrue)) then
      begin
        RefreshMenuItemVisibleFalse;
        RefreshMenuItemVisibleTrue;
      end
      else if (Assigned(FOnRefreshMenuItemVisibleFalse))  then
      begin
        SetAllVisible(True);
        RefreshMenuItemVisibleFalse;
      end
      else if (Assigned(FOnRefreshMenuItemVisibleTrue))  then
      begin
        SetAllVisible(False);
        RefreshMenuItemVisibleTrue;
      end;
    end;
  end;
var
  tempSummaryItem:TcxGridTableSummaryItem;
  tempField:Tfield;
  tempProperties:TcxCustomEditProperties;
begin
  inherited;
  if (not Assigned(FLinkTableView)) then exit;

  CreatePopupMenu;

  RefreshVisableItem;

  ABSetRadioItemMenuItemEnabledFalse;
  if (Assigned(FLinkTableView)) and (Assigned(FLinkTableView.Controller)) then
  begin
    SetMenuItemChecked('PopupMenu_ShowGroup',FLinkTableView.OptionsView.GroupByBox);
    SetMenuItemChecked('PopupMenu_AutoColumWidth',FLinkTableView.OptionsView.ColumnAutoWidth);

    SetMenuItemChecked('PopupMenu_ColumnFilter',FLinkTableView.OptionsCustomize.ColumnFiltering);
    SetMenuItemChecked('PopupMenu_RowFilter',FLinkTableView.FilterRow.Visible);
    SetMenuItemChecked('PopupMenu_FilterBarShowMode_Always'   ,FLinkTableView.FilterBox.Visible=fvAlways);
    SetMenuItemChecked('PopupMenu_FilterBarShowMode_Never'    ,FLinkTableView.FilterBox.Visible=fvNever);
    SetMenuItemChecked('PopupMenu_FilterBarShowMode_NonEmpty' ,FLinkTableView.FilterBox.Visible=fvNonEmpty);


    SetMenuItemChecked('PopupMenu_ContentSelect_Cell'               ,FLinkTableView.OptionsSelection.CellSelect);
    SetMenuItemChecked('PopupMenu_ContentSelect_RowSelect'          ,not FLinkTableView.OptionsSelection.MultiSelect);
    SetMenuItemChecked('PopupMenu_ContentSelect_MultiRowSelect'     ,FLinkTableView.OptionsSelection.MultiSelect and (not FLinkTableView.OptionsSelection.CellMultiSelect));
    SetMenuItemChecked('PopupMenu_ContentSelect_MultiRowCellSelect' ,FLinkTableView.OptionsSelection.CellMultiSelect);

    SetMenuItemChecked('PopupMenu_Admin_TableViewCanEdit'         , FLinkTableView.OptionsData.Editing);
    SetMenuItemChecked('PopupMenu_Other_AutoRowNum'               , Assigned(FLinkTableView.OnCustomDrawIndicatorCell));
    SetMenuItemChecked('PopupMenu_Other_FocusCellOnTab'           , FLinkTableView.OptionsBehavior.FocusCellOnTab);
    SetMenuItemChecked('PopupMenu_Other_GoToNextCellOnEnter'      , FLinkTableView.OptionsBehavior.GoToNextCellOnEnter);
    SetMenuItemChecked('PopupMenu_Other_DirectionKeyMoveColumn'   , FLinkTableView.OptionsBehavior.AlwaysShowEditor);
    SetMenuItemChecked('PopupMenu_Other_ColumnHorzSizing'         , FLinkTableView.OptionsCustomize.ColumnHorzSizing);
    SetMenuItemChecked('PopupMenu_Other_DataRowSizing'            , FLinkTableView.OptionsCustomize.DataRowSizing);
    SetMenuItemChecked('PopupMenu_Other_AutoRowHeight'            , (FLinkTableView.OptionsView.CellAutoHeight) and (FLinkTableView.OptionsView.DataRowHeight=0));
    SetMenuItemChecked('PopupMenu_Other_ColumnsQuickCustomization', FLinkTableView.OptionsCustomize.ColumnsQuickCustomization);
    SetMenuItemChecked('PopupMenu_Other_ShowHeader'               , FLinkTableView.OptionsView.Header);
    SetMenuItemChecked('PopupMenu_Other_ShowAggBar'               , FLinkTableView.OptionsView.Footer);

    if (Assigned(FLinkTableView.Controller.FocusedColumn)) then
    begin
      SetMenuItemChecked('PopupMenu_Order_Asc'         ,FLinkTableView.Controller.FocusedColumn.SortOrder=soascending );
      SetMenuItemChecked('PopupMenu_Order_Desc'        ,FLinkTableView.Controller.FocusedColumn.SortOrder=soDescending);
      SetMenuItemChecked('PopupMenu_Order_DelAscORDesc',FLinkTableView.Controller.FocusedColumn.SortOrder=soNone      );

      tempSummaryItem:=TcxGridTableSummaryItem(FLinkTableView.DataController.Summary.FooterSummaryItems.ItemOfItemLink(FLinkTableView.Controller.FocusedColumn));
      SetMenuItemChecked('PopupMenu_Agg_Count'      ,(Assigned(tempSummaryItem)) and (tempSummaryItem.Kind=skCount)                                       );
      SetMenuItemChecked('PopupMenu_Agg_Clear'      ,(not (Assigned(tempSummaryItem))) or ((Assigned(tempSummaryItem)) and (tempSummaryItem.Kind=skNone)) );
      tempField:=ABGetFocusedColumnField(FLinkTableView);
      if ((assigned(tempField)) and ((tempField.DataType in ABIntAndDecimalDataType))) or
         ((not assigned(tempField)) and
           (
            (AnsiCompareText(FLinkTableView.Controller.FocusedColumn.DataBinding.ValueType,'Float')=0) or
            (AnsiCompareText(FLinkTableView.Controller.FocusedColumn.DataBinding.ValueType,'Integer')=0) or
            (AnsiCompareText(FLinkTableView.Controller.FocusedColumn.DataBinding.ValueType,'DateTime')=0)
            )
            ) then
      begin
        SetMenuItemChecked('PopupMenu_Agg_Sum'        ,(Assigned(tempSummaryItem)) and (tempSummaryItem.Kind=skSum)                                         );
        SetMenuItemChecked('PopupMenu_Agg_Max'        ,(Assigned(tempSummaryItem)) and (tempSummaryItem.Kind=skMax)                                         );
        SetMenuItemChecked('PopupMenu_Agg_Min'        ,(Assigned(tempSummaryItem)) and (tempSummaryItem.Kind=skMin)                                         );
        SetMenuItemChecked('PopupMenu_Agg_Avg'        ,(Assigned(tempSummaryItem)) and (tempSummaryItem.Kind=skAverage)                                     );
      end;


      tempProperties:=FLinkTableView.Controller.FocusedColumn.Properties;
      if (Assigned(tempProperties)) then
      begin
        SetMenuItemChecked('PopupMenu_Align_Left'  ,FLinkTableView.Controller.FocusedColumn.Properties.Alignment.Horz=taLeftJustify);
        SetMenuItemChecked('PopupMenu_Align_Right' ,FLinkTableView.Controller.FocusedColumn.Properties.Alignment.Horz=taRightJustify);
        SetMenuItemChecked('PopupMenu_Align_Center',FLinkTableView.Controller.FocusedColumn.Properties.Alignment.Horz=taCenter);
      end;

      SetMenuItemEnabled('PopupMenu_Other_DisplayFormat',ABIsPublishProp(tempProperties,'DisplayFormat'));

      if (FLinkTableView is TcxGridDBBandedTableView)  then
      begin
        SetMenuItemChecked('PopupMenu_Band_Show'              ,TcxGridDBBandedTableView(FLinkTableView).OptionsView.BandHeaders);
        SetMenuItemChecked('PopupMenu_ConstBandPosition_Left' ,TcxGridDBBandedColumn(FLinkTableView.Controller.FocusedColumn).Position.Band.FixedKind =fkLeft);
        SetMenuItemChecked('PopupMenu_ConstBandPosition_Right',TcxGridDBBandedColumn(FLinkTableView.Controller.FocusedColumn).Position.Band.FixedKind=fkRight);
        SetMenuItemChecked('PopupMenu_ConstBandPosition_None' ,TcxGridDBBandedColumn(FLinkTableView.Controller.FocusedColumn).Position.Band.FixedKind=fkNone);
      end;
    end;
  end;
end;

procedure TABcxGridPopupMenu.SetIndicator(aActive: boolean);
  procedure DoIndicatorCellCustomDraw;
  var
    tempEvent:PIndicatorCellCustomDrawEvent;
  begin
    if FIndicatorCellCustomDraw.IndexOf(FLinkTableView.Name)<0 then
    begin
      New(tempEvent);
      tempEvent.Self:=FLinkTableView;
      tempEvent.FOldIndicatorCellCustomDraw:=FLinkTableView.OnCustomDrawIndicatorCell;
      FIndicatorCellCustomDraw.AddObject(FLinkTableView.Name,TObject(tempEvent));
      FLinkTableView.OnCustomDrawIndicatorCell:=NewCustomDrawIndicatorCell;
    end;
    FLinkTableView.OptionsView.Indicator:=false;
    FLinkTableView.OptionsView.Indicator:=true;

    ABSetAutoIDWidth(FLinkTableView);
  end;
begin
  if aActive then
  begin
    DoIndicatorCellCustomDraw;
  end
  else
  begin
    FLinkTableView.OptionsView.Indicator:=false;
    FreeOldEVent_FIndicatorCellCustomDraw;
  end;
end;

procedure TABcxGridPopupMenu.ClearAllItems;
begin
  if Assigned(FLinkTableView) then
  begin
    FreeOldEVent;
    FLinkTableView.BeginUpdate;
    try
      if  (FLinkTableView.ColumnCount>0) then
        FLinkTableView.ClearItems;
    finally
      FLinkTableView.EndUpdate;
    end;
  end;
end;

procedure TABcxGridPopupMenu.PubColumnGetCellHint(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  ACellViewInfo: TcxGridTableDataCellViewInfo; const AMousePos: TPoint;
  var AHintText: TCaption; var AIsHintMultiLine: Boolean;
  var AHintTextRect: TRect);
begin
  //
end;

procedure TABcxGridPopupMenu.CreateAllItems;
begin
  if Assigned(FLinkTableView) then
  begin
    FreeOldEVent;
    FLinkTableView.BeginUpdate;
    try
      if  (FLinkTableView.ColumnCount>0) then
        FLinkTableView.ClearItems;

      TcxGridDBDataController(FLinkTableView.DataController).CreateAllItems;
    finally
      FLinkTableView.EndUpdate;
    end;

    SetIndicator(FLinkTableView.OptionsView.Indicator);
  end;
end;

function TABcxGridPopupMenu.GetFilename(aTypeFlag:string):string;
begin
  if FSetupFullFileName<>EmptyStr then
  begin
    Result:=FSetupFullFileName+aTypeFlag;
  end
  else
  begin
    Result:=ABConfigPath+FLinkTableView.Owner.Name+'_'+FLinkTableView.Name+FSetupFileNameSuffix +aTypeFlag;
  end;
end;

procedure TABcxGridPopupMenu.LoadViewStyleSheets;
var
  tempStr1:string;
begin
  FIsLoadViewStyleSheets :=False;
  FLinkTableView.BeginUpdate;
  try
    tempStr1:=GetFilename('_StyleSheets.ini');
    FStyleRepository.ClearStyleSheets;
    FLinkTableView.Styles.StyleSheet:=nil;
    if ABCheckFileExists(tempStr1) then
    begin
      if (FLinkTableView is TcxGridBandedTableView) then
        LoadStyleSheetsFromIniFile(tempStr1, FStyleRepository,TcxGridBandedTableViewStyleSheet)
      else if (FLinkTableView is TcxGridDBTableView) then
        LoadStyleSheetsFromIniFile(tempStr1, FStyleRepository,TcxGridTableViewStyleSheet);

      if (FStyleRepository.StyleSheetCount>0) and (Assigned(FStyleRepository.StyleSheets[0])) then
      begin
        FLinkTableView.Styles.StyleSheet:= FStyleRepository.StyleSheets[0];
      end;
      FIsLoadViewStyleSheets :=true;
    end;
  finally
    FLinkTableView.EndUpdate;
  end;
end;

procedure TABcxGridPopupMenu.SaveViewStyleSheets;
var
  tempStr1:string;
  tempList:TList;
begin
  tempStr1:=GetFilename('_StyleSheets.ini');
  if ABCheckFileExists(tempStr1) then
  begin
    DeleteFile(tempStr1);
  end;

  if Assigned(FLinkTableView.Styles.StyleSheet) then
  begin
    tempList := TList.Create;
    try
      tempList.Add(FLinkTableView.Styles.StyleSheet );
      SaveStyleSheetsToIniFile(tempStr1, tempList);
    finally
      tempList.Free;
    end;
  end;
end;

procedure TABcxGridPopupMenu.LoadViewOtherSet;
  procedure  LoadColumnCaption;
  var
    i:LongInt;
    tempStr1:string;
    tempColumn:TcxGridColumn;
    tempDataset:TDataSet;
  begin
    tempStr1:=GetFilename('_OtherSet.ini');
    if (FLinkTableView is TcxGridDBBandedTableView) then
    begin
      tempDataset:=TcxGridDBBandedTableView(FLinkTableView).DataController.DataSet;
    end
    else if  (FLinkTableView is TcxGridDBTableView) then
    begin
      tempDataset:=TcxGridDBTableView(FLinkTableView).DataController.DataSet;
    end
    else
    begin
      exit;
    end;

    //加载标题
    if (Assigned(tempDataset)) and
       (tempDataset.Active) then
    begin
      ABReadInI('ColumnCaption',tempStr1,FLoadViewOtherSetStrings1);
      for i := 0 to FLinkTableView.ColumnCount-1 do
      begin
        tempColumn:= FLinkTableView.Columns[i];

        if (FLoadViewOtherSetStrings1.Values[IntToStr(i)]<>EmptyStr) and
           (AnsiCompareText(FLoadViewOtherSetStrings1.Values[IntToStr(i)],tempColumn.Caption)<>0) then
        begin
          tempColumn.Caption:=FLoadViewOtherSetStrings1.Values[IntToStr(i)];
        end;
      end;
    end;
  end;

var
  tempStr1,tempStr2,tempStr3,tempStr4,tempStr5:string;
  i:LongInt;
  tempColumn:TcxGridColumn;
  tempEvent: PColumnGetDataTextEvent;
  tempBoolean:boolean;

begin
  FIsLoadViewOtherSet    :=False;
  if not Assigned(FLinkTableView) then
    exit;

  tempStr1:=GetFilename('_OtherSet.ini');
  if (ABCheckFileExists(tempStr1)) then
  begin
    FLinkTableView.BeginUpdate;
    try
      //加载宽度
      ABReadInI('Other',tempStr1,FLoadViewOtherSetStrings2);

      if (Assigned(FLinkTableView.Control)) then
      begin
        if (Assigned(FLinkTableView.Control.Parent)) and
           (not (FLinkTableView.Control.Parent is TCustomForm)) then
        begin
          if  (Assigned(FLinkTableView.Control.Parent.Parent)) and
           (not (FLinkTableView.Control.Parent.Parent is TCustomForm))then
          begin
            if (FLinkTableView.Control.Parent.Parent.Align<>alClient)  then
            begin
              FLinkTableView.Control.Parent.Parent.Width:=
                StrToIntDef(FLoadViewOtherSetStrings2.Values['Parent.Parent.Width'],FLinkTableView.Control.Parent.Parent.Width);
              FLinkTableView.Control.Parent.Parent.Height:=
                StrToIntDef(FLoadViewOtherSetStrings2.Values['Parent.Parent.Height'],FLinkTableView.Control.Parent.Parent.Height);
            end;
          end;
          if (FLinkTableView.Control.Parent.Align<>alClient) then
          begin
            FLinkTableView.Control.Parent.Width:=
              StrToIntDef(FLoadViewOtherSetStrings2.Values['Parent.Width'],FLinkTableView.Control.Parent.Width);
            FLinkTableView.Control.Parent.Height:=
              StrToIntDef(FLoadViewOtherSetStrings2.Values['Parent.Height'],FLinkTableView.Control.Parent.Height);
          end;
        end;
        if (FLinkTableView.Control.Align<>alClient) then
        begin
          FLinkTableView.Control.Width:=
            StrToIntDef(FLoadViewOtherSetStrings2.Values['Width'],FLinkTableView.Control.Width);
          FLinkTableView.Control.Height:=
            StrToIntDef(FLoadViewOtherSetStrings2.Values['Height'],FLinkTableView.Control.Height);
        end;
      end;
      //加载显示格式
      tempBoolean:=false;
      for i := 0 to FLinkTableView.ColumnCount-1 do
      begin
        tempColumn:= FLinkTableView.Columns[i];
        if (Assigned(tempColumn.Properties)) then
        begin
          if (tempColumn.Properties is TcxCustomTextEditProperties) then
          begin
            if not tempBoolean then
            begin
              tempBoolean:=true;
              ABReadInI('DisplayFormat',tempStr1,FLoadViewOtherSetStrings1);
            end;

            tempStr2:=FLoadViewOtherSetStrings1.Values[IntToStr(i)];
            if tempStr2<>emptystr then
            begin
              TcxCustomTextEditProperties(tempColumn.Properties).DisplayFormat:=tempStr2;
            end;
          end;
        end;
      end;

      LoadColumnCaption;

      //加载计算列
      tempBoolean:=false;
      for i := 0 to FLinkTableView.ColumnCount-1 do
      begin
        tempColumn:= FLinkTableView.Columns[i];
        if (ABIsCaleColumn(tempColumn,false))  then
        begin
          if FColumnsOnGetDataText.IndexOf(FLinkTableView.Name+IntToStr(tempColumn.id))<0 then
          begin
            New(tempEvent);
            tempEvent.EditValues:=TStringList.Create;
            TStringList(tempEvent.EditValues).Sorted:=true;

            tempEvent.Self:=tempColumn;
            if not tempBoolean then
            begin
              tempBoolean:=true;
              ABReadInI('CaleColumn',tempStr1,FLoadViewOtherSetStrings1);
            end;

            tempStr2:=GetFilename('_CaleColumnSQL'+inttostr(i)+'.ini');
            if ABCheckFileExists(tempStr2) then
            begin
              tempStr3:=ABReadTxt(tempStr2);
            end
            else
            begin
              tempStr3:=EmptyStr;
            end;

            tempStr4:=FLoadViewOtherSetStrings1.Values[IntToStr(i)+'Caption'];
            tempStr5:=FLoadViewOtherSetStrings1.Values[IntToStr(i)+'ColumnType'];
            if tempStr5=EmptyStr then
              tempStr5:='String';

            if (tempStr3<>EmptyStr) and (tempStr4<>EmptyStr) then
            begin
              tempColumn.Tag:=19003;
              tempEvent.SQL:=trim(tempStr3);
              tempColumn.Caption:=tempStr4;
              tempColumn.Options.Editing:=False;
              tempColumn.DataBinding.ValueType:=tempStr5;
            end;

            if tempColumn.Name=EmptyStr then
              tempColumn.Name:=FLinkTableView.Name+'tempCreateCale'+ inttostr(tempColumn.ID);
            tempEvent.ColumnName:=tempColumn.Name;

            tempEvent.FOldColumnGetDataText:=tempColumn.OnGetDataText;
            tempColumn.OnGetDataText:=NewColumnsGetDataText;
            FColumnsOnGetDataText.AddObject(FLinkTableView.Name+IntToStr(tempColumn.id),TObject(tempEvent));
          end;
        end;
      end;

      //Band
      if (FLinkTableView is TcxGridDBBandedTableView) then
      begin
        TcxGridDBBandedTableView(FLinkTableView).OptionsView.BandHeaders:=
          StrToBoolDef(FLoadViewOtherSetStrings2.Values['BandIsView'],false);
        tempBoolean:=false;
        for i := 0 to TcxGridDBBandedTableView(FLinkTableView).Bands.Count-1 do
        begin
          if not tempBoolean then
          begin
            tempBoolean:=true;
            ABReadInI('Band',tempStr1,FLoadViewOtherSetStrings1);
          end;
          tempStr2:=FLoadViewOtherSetStrings1.Values[IntToStr(i)+'Caption'];
          tempStr3:=FLoadViewOtherSetStrings1.Values[IntToStr(i)+'Position'];

          TcxGridDBBandedTableView(FLinkTableView).Bands[i].Caption:=tempStr2;
          if tempStr3<>EmptyStr then
            TcxGridDBBandedTableView(FLinkTableView).Bands[i].Position.Band.FixedKind:=
              TcxGridBandFixedKind(GetEnumValue(Typeinfo(TcxGridBandFixedKind), tempStr3));
        end;

        //当显示表头时删除为空且没有列的表头
        if TcxGridDBBandedTableView(FLinkTableView).OptionsView.BandHeaders then
        begin
          for i := TcxGridDBBandedTableView(FLinkTableView).Bands.Count-1 downto 0 do
          begin
            if (trim(TcxGridDBBandedTableView(FLinkTableView).Bands.Items[i].Caption)=emptystr) and
               (TcxGridDBBandedTableView(FLinkTableView).Bands.Items[i].ColumnCount<=0) then
            begin
              TcxGridDBBandedTableView(FLinkTableView).Bands.Delete(i);
            end;
          end;
        end;
        TcxGridDBBandedTableView(FLinkTableView).OptionsView.BandHeaders :=StrToBoolDef(FLoadViewOtherSetStrings2.Values['BandHeaders']              ,false);
      end;


      FLinkTableView.OptionsSelection.CellSelect                  :=StrToBoolDef(FLoadViewOtherSetStrings2.Values['CellSelect']               ,false);
      FLinkTableView.OptionsSelection.MultiSelect                 :=StrToBoolDef(FLoadViewOtherSetStrings2.Values['MultiSelect']              ,false);
      FLinkTableView.OptionsSelection.CellMultiSelect             :=StrToBoolDef(FLoadViewOtherSetStrings2.Values['CellMultiSelect']          ,false);

      FLinkTableView.OptionsBehavior.FocusCellOnTab               :=StrToBoolDef(FLoadViewOtherSetStrings2.Values['FocusCellOnTab']           ,false);
      FLinkTableView.OptionsBehavior.GoToNextCellOnEnter          :=StrToBoolDef(FLoadViewOtherSetStrings2.Values['GoToNextCellOnEnter']      ,false);

      FLinkTableView.OptionsCustomize.ColumnsQuickCustomization   :=StrToBoolDef(FLoadViewOtherSetStrings2.Values['ColumnsQuickCustomization'],True);
      FLinkTableView.OptionsCustomize.ColumnHorzSizing            :=StrToBoolDef(FLoadViewOtherSetStrings2.Values['ColumnHorzSizing']         ,false);
      FLinkTableView.OptionsCustomize.DataRowSizing               :=StrToBoolDef(FLoadViewOtherSetStrings2.Values['DataRowSizing']            ,false);
      FLinkTableView.OptionsCustomize.ColumnFiltering             :=StrToBoolDef(FLoadViewOtherSetStrings2.Values['ColumnFiltering']          ,false);

      FLinkTableView.OptionsBehavior.AlwaysShowEditor             :=StrToBoolDef(FLoadViewOtherSetStrings2.Values['AlwaysShowEditor']         ,false);
      FLinkTableView.FilterRow.Visible                            :=StrToBoolDef(FLoadViewOtherSetStrings2.Values['FilterRow']                ,false);

      FLinkTableView.OptionsView.Header                           :=StrToBoolDef(FLoadViewOtherSetStrings2.Values['Header']                   ,True);
      FLinkTableView.OptionsView.ColumnAutoWidth                  :=StrToBoolDef(FLoadViewOtherSetStrings2.Values['ColumnAutoWidth']          ,false);

      FLinkTableView.OptionsView.DataRowHeight                    :=StrToIntDef (FLoadViewOtherSetStrings2.Values['DataRowHeight']            ,0);
      FLinkTableView.OptionsView.CellAutoHeight:=FLinkTableView.OptionsView.DataRowHeight=0;

      FLinkTableView.OptionsView.Indicator                        :=StrToBoolDef(FLoadViewOtherSetStrings2.Values['Indicator']                ,True);
      SetIndicator(FLinkTableView.OptionsView.Indicator);

      FLinkTableView.OptionsData.Editing:=StrToBoolDef(FLoadViewOtherSetStrings2.Values['TableViewCanEdit'],True);

      FLinkTableView.FilterBox.Visible:=
        TcxGridFilterVisible(GetEnumValue(Typeinfo(TcxGridFilterVisible),FLoadViewOtherSetStrings2.Values['FilterBox_Visible']));

      if  (FLinkTableView.DataController.Filter.FilterText<>EmptyStr) then
      begin
        if (FLinkTableView.FilterBox.Visible=fvNever)  then
          FLinkTableView.FilterBox.Visible:=fvNonEmpty;

        FLinkTableView.DataController.Filter.TranslateBetween:=true;
        FLinkTableView.DataController.Filter.TranslateIn:=true;
        FLinkTableView.DataController.Filter.TranslateLike:=true;
      end;

      FIsLoadViewOtherSet    :=true;
    finally
      FLinkTableView.EndUpdate;
    end;
  end;
end;

procedure TABcxGridPopupMenu.SaveViewOtherSet;
var
  tempStr1,tempStr2:string;
  i,j:LongInt;
  tempColumn:TcxGridColumn;
  tempEvent: PColumnGetDataTextEvent;
begin
  if not Assigned(FLinkTableView) then
    exit;

  tempStr1:=GetFilename('_OtherSet.ini');
  if ABCheckFileExists(tempStr1) then
  begin
    DeleteFile(tempStr1);
  end;

  ABWriteInI(['Other'],
             ['Width','Height'],
             [IntToStr(FLinkTableView.Control.Width),IntToStr(FLinkTableView.Control.Height)],tempStr1);
  if  (Assigned(FLinkTableView.Control.Parent)) then
  begin
    ABWriteInI(['Other'],
               ['Parent.Width','Parent.Height'],
               [IntToStr(FLinkTableView.Control.Parent.Width),IntToStr(FLinkTableView.Control.Parent.Height)],tempStr1);
    if  (Assigned(FLinkTableView.Control.Parent.Parent)) then
    begin
      ABWriteInI(['Other'],
                 ['Parent.Parent.Width','Parent.Parent.Height'],
                 [IntToStr(FLinkTableView.Control.Parent.Parent.Width),IntToStr(FLinkTableView.Control.Parent.Parent.Height)],tempStr1);
    end;
  end;

  //Band
  if (FLinkTableView is TcxGridDBBandedTableView) then
  begin
    ABWriteInI(['Other'],['BandIsView'],[ABBoolToStr(TcxGridDBBandedTableView(FLinkTableView).OptionsView.BandHeaders)
                                          ],tempStr1);
    for i := 0 to TcxGridDBBandedTableView(FLinkTableView).Bands.Count-1 do
    begin
      ABWriteInI(['Band'],[IntToStr(i)+'Caption'],[TcxGridDBBandedTableView(FLinkTableView).Bands[i].Caption],tempStr1);
      ABWriteInI(['Band'],[IntToStr(i)+'Position'],
      [GetEnumName(Typeinfo(TcxGridBandFixedKind),Ord(TcxGridDBBandedTableView(FLinkTableView).Bands[i].Position.Band.FixedKind))
       ],tempStr1);
    end;
  end;
  for i := 0 to FLinkTableView.ColumnCount-1 do
  begin
    tempColumn:= FLinkTableView.Columns[i];
    //保存显示格式
    if (Assigned(tempColumn.Properties)) then
    begin
      if (tempColumn.Properties is TcxCustomTextEditProperties) then
      begin
        tempStr2:=TcxCustomTextEditProperties(tempColumn.Properties).DisplayFormat;
        if tempStr2<>emptystr then
        begin
          ABWriteInI(['DisplayFormat'],[IntToStr(i)],[tempStr2],tempStr1);
        end;
      end;
    end;

    //保存标题
    if (tempColumn.Caption<>EmptyStr) and
       ((not Assigned(TcxGridDBBandedColumn(tempColumn).DataBinding)) or
        (not Assigned(TcxGridDBBandedColumn(tempColumn).DataBinding.Field)) or
        (AnsiCompareText(tempColumn.Caption,TcxGridDBBandedColumn(tempColumn).DataBinding.Field.DisplayLabel)<>0)
        ) then
    begin
      ABWriteInI(['ColumnCaption'],[IntToStr(i)],[tempColumn.Caption],tempStr1);
    end;

    //计算列
    if ABIsCaleColumn(tempColumn) then
    begin
      j := FColumnsOnGetDataText.IndexOf(FLinkTableView.Name+IntToStr(tempColumn.id));
      if (j >= 0) then
      begin
        tempEvent := PColumnGetDataTextEvent(FColumnsOnGetDataText.Objects[j]);

        tempstr2:=GetFilename('_CaleColumnSQL'+inttostr(i)+'.ini');
        if ABCheckFileExists(tempstr2) then
        begin
          DeleteFile(tempstr2);
        end;
        ABWriteTxt(tempstr2,tempEvent.SQL);

        ABWriteInI(['CaleColumn'],[IntToStr(i)+'ColumnType'],[tempColumn.DataBinding.ValueType],tempStr1);
        ABWriteInI(['CaleColumn'],[IntToStr(i)+'Caption'],[tempColumn.Caption],tempStr1);
      end;
    end;
  end;

  if (FLinkTableView is TcxGridDBBandedTableView) then
  begin
    ABWriteInI(['Other'],['BandHeaders']              ,[ABBoolToStr(TcxGridDBBandedTableView(FLinkTableView).OptionsView.BandHeaders)] ,tempStr1);
  end;

  ABWriteInI(['Other'],['CellSelect']               ,[ABBoolToStr(FLinkTableView.OptionsSelection.CellSelect)]               ,tempStr1);
  ABWriteInI(['Other'],['MultiSelect']              ,[ABBoolToStr(FLinkTableView.OptionsSelection.MultiSelect)]              ,tempStr1);
  ABWriteInI(['Other'],['CellMultiSelect']          ,[ABBoolToStr(FLinkTableView.OptionsSelection.CellMultiSelect)]          ,tempStr1);

  ABWriteInI(['Other'],['FocusCellOnTab']           ,[ABBoolToStr(FLinkTableView.OptionsBehavior.FocusCellOnTab)]            ,tempStr1);
  ABWriteInI(['Other'],['GoToNextCellOnEnter']      ,[ABBoolToStr(FLinkTableView.OptionsBehavior.GoToNextCellOnEnter)]       ,tempStr1);

  ABWriteInI(['Other'],['ColumnsQuickCustomization'],[ABBoolToStr(FLinkTableView.OptionsCustomize.ColumnsQuickCustomization)],tempStr1);
  ABWriteInI(['Other'],['ColumnHorzSizing']         ,[ABBoolToStr(FLinkTableView.OptionsCustomize.ColumnHorzSizing)]         ,tempStr1);
  ABWriteInI(['Other'],['DataRowSizing']            ,[ABBoolToStr(FLinkTableView.OptionsCustomize.DataRowSizing)]            ,tempStr1);
  ABWriteInI(['Other'],['ColumnFiltering']          ,[ABBoolToStr(FLinkTableView.OptionsCustomize.ColumnFiltering)]          ,tempStr1);

  ABWriteInI(['Other'],['AlwaysShowEditor']         ,[ABBoolToStr(FLinkTableView.OptionsBehavior.AlwaysShowEditor)]          ,tempStr1);
  ABWriteInI(['Other'],['FilterRow']                ,[ABBoolToStr(FLinkTableView.FilterRow.Visible)]                         ,tempStr1);
  ABWriteInI(['Other'],['Header']                   ,[ABBoolToStr(FLinkTableView.OptionsView.Header)]                     ,tempStr1);
  ABWriteInI(['Other'],['ColumnAutoWidth']          ,[ABBoolToStr(FLinkTableView.OptionsView.ColumnAutoWidth)]               ,tempStr1);

  ABWriteInI(['Other'],['DataRowHeight']            ,[IntToStr(FLinkTableView.OptionsView.DataRowHeight)]                    ,tempStr1);
  ABWriteInI(['Other'],['Indicator']                ,[ABBoolToStr(FLinkTableView.OptionsView.Indicator)]                     ,tempStr1);

  ABWriteInI(['Other'],['TableViewCanEdit']        ,[ABBoolToStr(FLinkTableView.OptionsData.Editing)]                                                 ,tempStr1);

  ABWriteInI(['Other'],['FilterBox_Visible']        ,[GetEnumName(TypeInfo(TcxGridFilterVisible), Ord(FLinkTableView.FilterBox.Visible))]  ,tempStr1);
end;

procedure TABcxGridPopupMenu.LoadViewProperty ;
var
  tempStr1:string;
begin
  FIsLoadViewProperty    :=False;
  tempStr1:=GetFilename('_Property.ini');
  if ABCheckFileExists(tempStr1) then
  begin
    FLinkTableView.BeginUpdate;
    try
      if (FAutoCreateAllItem) and (FLinkTableView.ColumnCount<=0) then
        CreateAllItems;

      //AChildrenCreating=TRUE时会将INI有的而FLinkTableView没有列创建出来
      //此处使用AChildrenCreating=TRUE是因为要自动创建计算列的关系
      //AChildrenDeleting=TRUE时会将INI没有的而FLinkTableView有列删除
      FLinkTableView.RestoreFromIniFile(tempStr1,True,false,[gsoUseFilter, gsoUseSummary]);
    finally
      FLinkTableView.EndUpdate;
    end;
    FIsLoadViewProperty    :=true;
  end
  else
  begin
    FLinkTableView.BeginUpdate;
    try
      if (FAutoCreateAllItem) and (FLinkTableView.ColumnCount<=0) then
      begin
        CreateAllItems;
      end;
    finally
      FLinkTableView.EndUpdate;
    end;

    FLinkTableView.BeginUpdate;
    try
      if (FAutoApplyBestFit) and
         (FLinkTableView.DataController.RecordCount>100) and
         (FLinkTableView.ColumnCount>0) then
      begin
        FLinkTableView.ApplyBestFit;
      end;
    finally
      FLinkTableView.EndUpdate;
    end;
  end;
end;

procedure TABcxGridPopupMenu.SaveViewProperty;
var
  tempStr1:string;
begin
  tempStr1:=GetFilename('_Property.ini');
  if ABCheckFileExists(tempStr1) then
  begin
    DeleteFile(tempStr1);
  end;

  FLinkTableView.StoreToIniFile(tempStr1,True,[gsoUseFilter, gsoUseSummary] );
end;

procedure TABcxGridPopupMenu.LoadViewSetColor(aInit:boolean) ;
var
  tempStr1:string;
  tempEvent:PCellCustomDrawEvent;
  i:longint;
begin
  FIsLoadViewSetColor    :=False;
  if aInit then
  begin
    tempStr1:=GetFilename('_ColorSet.ini');
    if ABCheckFileExists(tempStr1) then
    begin
      FCurSetColorStr:=trim(ABReadTxt(tempStr1));
      FIsLoadViewSetColor:=true;
    end
  end;

  if FCurSetColorStr<>emptystr then
  begin
    i:=FCellCustomDraw.IndexOf(FLinkTableView.Name);
    if i<0 then
    begin
      New(tempEvent);
      tempEvent.Self:=FLinkTableView;
      tempEvent.FOldCellCustomDraw:=FLinkTableView.OnCustomDrawCell;
      //FLinkTableView.OnCustomDrawCell:=NewCustomDrawCell;

      tempEvent.FOldStylesGetContentStyle:=FLinkTableView.Styles.OnGetContentStyle;
      FLinkTableView.Styles.OnGetContentStyle:=NewStylesGetContentStyle;

      FCellCustomDraw.AddObject(FLinkTableView.Name,TObject(tempEvent));

      i:=FCellCustomDraw.Count-1;
    end;

    PCellCustomDrawEvent(FCellCustomDraw.Objects[i]).ColorCommandtext:=FCurSetColorStr;
    FLinkTableView.OptionsSelection.InvertSelect:=False;
  end
  else
  begin
    FLinkTableView.OptionsSelection.InvertSelect:=true;
  end;
end;

procedure TABcxGridPopupMenu.SaveViewSetColor;
var
  tempStr1:string;
  i:longint;
begin
  i:=FCellCustomDraw.IndexOf(FLinkTableView.Name);
  if i>=0 then
  begin
    tempStr1:=GetFilename('_ColorSet.ini');
    if ABCheckFileExists(tempStr1) then
    begin
      DeleteFile(tempStr1);
    end;
    ABWriteTxt(tempStr1,PCellCustomDrawEvent(FCellCustomDraw.Objects[i]).ColorCommandtext);
  end;
end;

procedure TABcxGridPopupMenu.OpenRowNum;
begin
  SetIndicator(True);
end;

procedure TABcxGridPopupMenu.OutFile(aOutFileType:TABOutFileType);
var
  temSavedialog1:TSaveDialog;
begin
  temSavedialog1:=TSaveDialog.Create(self);
  if (owner is TcxGridTableView) then
    temSavedialog1.FileName:=Tcontrol(TcxGrid(TcxGridTableView(owner).Control).Owner).name+'_'+FLinkTableView.name+'_'+ABDateToStr(Now)
  else
    temSavedialog1.FileName:=TCustomForm(owner).name+'_'+FLinkTableView.name+'_'+ABDateToStr(Now);

  try
    if aOutFileType =otExcel then
      temSavedialog1.Filter:='*.xls|*.xls'
    else if aOutFileType =otXML then
      temSavedialog1.Filter:='*.xml|*.xml'
    else if aOutFileType =otText then
      temSavedialog1.Filter:='*.txt|*.txt'
    else if aOutFileType =otHtml then
      temSavedialog1.Filter:='*.Html|*.Html';

    if temSavedialog1.Execute then
    begin
      if aOutFileType =otExcel then
        ExportGridToExcel(temSavedialog1.FileName,Tcxgrid(FLinkTableView.Control),true,true)
      else if aOutFileType =otXML then
        ExportGridToXML(temSavedialog1.FileName,Tcxgrid(FLinkTableView.Control),true,true)
      else if aOutFileType =otText then
        ExportGridToText(temSavedialog1.FileName,Tcxgrid(FLinkTableView.Control),true,true)
      else if aOutFileType =otHtml then
        ExportGridToHTML(temSavedialog1.FileName,Tcxgrid(FLinkTableView.Control),true,true);

      FLinkTableView.DataController.Groups.FullCollapse
    end;
  finally
    temSavedialog1.free;
  end;
end;

procedure TABcxGridPopupMenu.Refresh;
var
  tempEvent1:PColumnGetDataTextEvent;
  tempEvent2:PCellCustomDrawEvent ;
  tempColumn:TcxGridColumn;
  i,j:longint;
begin

  //计算列
  for i := 0 to FLinkTableView.ColumnCount-1 do
  begin
    tempColumn:= FLinkTableView.Columns[i];
    j := FColumnsOnGetDataText.IndexOf(FLinkTableView.Name+IntToStr(tempColumn.id));
    if (j >= 0) then
    begin
      tempEvent1 := PColumnGetDataTextEvent(FColumnsOnGetDataText.Objects[j]);
      if tempEvent1.SQL<>EmptyStr then
      begin
        tempEvent1.SQL:=tempEvent1.SQL;
      end;
      if (Assigned(tempColumn.Summary)) then
      begin
        if   (tempColumn.Summary.FooterFormat<>EmptyStr) then
          tempColumn.Summary.FooterFormat:=(tempColumn.Summary.FooterFormat);
        if   (tempColumn.Summary.GroupFooterFormat<>EmptyStr) then
          tempColumn.Summary.GroupFooterFormat:=(tempColumn.Summary.GroupFooterFormat);
        if   (tempColumn.Summary.GroupFormat<>EmptyStr) then
          tempColumn.Summary.GroupFormat:=(tempColumn.Summary.GroupFormat);
      end;
    end;
  end;

  j:=FCellCustomDraw.IndexOf(FLinkTableView.Name);
  if j>=0 then
  begin
    tempEvent2 := PCellCustomDrawEvent(FCellCustomDraw.Objects[j]);
    if tempEvent2.ColorCommandtext<>EmptyStr then
    begin
      tempEvent2.ColorCommandtext:=(tempEvent2.ColorCommandtext);
    end;
  end;

  FLinkTableView.DataController.Refresh;
end;

procedure TABcxGridPopupMenu.SetColumnFoot(akind: TcxSummaryKind);
var
  tempColumn:TcxGridColumn;
  tempstr1,tempstr2,tempFooterFormat,tempGroupFooterFormat,tempGroupFormat:string;
begin
  if not FLinkTableView.OptionsView.Footer then
  begin
    FLinkTableView.OptionsView.Footer:= true;
    FLinkTableView.OptionsView.GroupFooters:=gfVisibleWhenExpanded ;
  end;

  tempColumn:=TcxGridColumn(FLinkTableView.Controller.FocusedColumn);
  if Assigned(tempColumn) then
  begin
    tempstr2:=EmptyStr;
    if (Assigned(tempColumn.Properties)) then
    begin
      if (tempColumn.Properties is TcxCustomTextEditProperties) then
      begin
        tempstr2:=TcxCustomTextEditProperties(tempColumn.Properties).DisplayFormat;
      end;
    end;
    if tempstr2=EmptyStr then
    begin
      tempstr2:='#0.00';
    end;

    FLinkTableView.DataController.Summary.BeginUpdate;
    try
      case akind of
        skCount:
        begin
          tempstr1:=ABIIF(FCloseFootStr,tempstr2,'记录数:#0条');
          tempFooterFormat     :=tempstr1;
          tempGroupFooterFormat:=tempstr1;
          tempGroupFormat      :=tempstr1;
        end;
        skSum:
        begin
          tempstr1:=ABIIF(FCloseFootStr,tempstr2,'总计:'+tempstr2);
          tempFooterFormat      :=tempstr1;
          tempstr1:=('小计:'+tempstr2);
          tempGroupFooterFormat :=tempstr1;
          tempGroupFormat      :=tempstr1;
        end;
        skMax:
        begin
          tempstr1:=ABIIF(FCloseFootStr,tempstr2,'最大值:'+tempstr2);
          tempFooterFormat     :=tempstr1;
          tempGroupFooterFormat:=tempstr1;
          tempGroupFormat      :=tempstr1;
        end;
        skMin:
        begin
          tempstr1:=ABIIF(FCloseFootStr,tempstr2,'最小值:#'+tempstr2);
          tempFooterFormat     :=tempstr1;
          tempGroupFooterFormat:=tempstr1;
          tempGroupFormat      :=tempstr1;
        end;
        skAverage:
        begin
          tempstr1:=ABIIF(FCloseFootStr,tempstr2,'平均值:#'+tempstr2);
          tempFooterFormat     :=tempstr1;
          tempGroupFooterFormat:=tempstr1;
          tempGroupFormat      :=tempstr1;
        end;
      else ;
      end;

      tempColumn.Summary.GroupKind:=akind;       //分组后的主记录中
      if tempGroupFormat<>'' then
      begin
        if akind=skCount then
          tempColumn.Summary.GroupFormat:=tempGroupFormat
        else
          tempColumn.Summary.GroupFormat:=tempColumn.Caption+' '+tempGroupFormat;
      end;

      tempColumn.Summary.GroupFooterKind:=akind; //分组后的各主记录状态栏
      tempColumn.Summary.GroupFooterFormat:=tempGroupFooterFormat;

      tempColumn.Summary.FooterKind:=akind;      //最下方的状态栏
      tempColumn.Summary.FooterFormat :=tempFooterFormat;

      //tempColumn.Summary.SortByGroupFooterSummary:=true;
      tempColumn.Summary.SortByGroupSummary:=true;
    finally
      FLinkTableView.DataController.Summary.EndUpdate;
    end;
  end;
end;

procedure TABcxGridPopupMenu.SetColumnAlign(aAlignment: TAlignment);
var
  tempColumn:TcxGridColumn;
begin
  tempColumn:=TcxGridColumn(FLinkTableView.Controller.FocusedColumn);
  if (Assigned(tempColumn)) and (Assigned(tempColumn.Properties)) then
  begin
    tempColumn.Properties.Alignment.Horz := aAlignment;
    tempColumn.HeaderAlignmentHorz:=aAlignment;
  end;
end;

procedure TABcxGridPopupMenu.NewColumnsGetDataText(
  Sender: TcxCustomGridTableItem; ARecordIndex: Integer;
  var AText: String);
var
  i:longint;
  tempEvent: PColumnGetDataTextEvent;
  tempStr1,tempStr4,tempValue:string;
begin
  i := FColumnsOnGetDataText.IndexOf(FLinkTableView.Name+IntToStr(Sender.id));
  if (i >= 0) then
  begin
    tempEvent := PColumnGetDataTextEvent(FColumnsOnGetDataText.Objects[i]);

    if ABIsCaleColumn(Sender) then
    begin
      tempStr1:=EmptyStr;
      if Trim(tempEvent.SQL)=EmptyStr then
      begin
        tempValue :=tempEvent.EditValues.Values[IntToStr(ARecordIndex)];
        if tempValue<>emptystr then
          tempStr1:= tempValue;
      end
      else
      begin
        tempStr4:=ABReplaceColumnCaption(tempEvent.SQL,
                                         TcxGridTableView(Sender.GridView),
                                         ARecordIndex,
                                         false);
        if ABPos('ABGetFirstPY',tempStr4)>0 then
        begin
          tempStr1:=trim(ABStringReplace(tempStr4,'ABGetFirstPY',EmptyStr));
          tempStr1:=ABGetFirstPy(AnsiString(tempStr1));
        end
        else if ABPos('ABGetAllPY',tempStr4)>0 then
        begin
          tempStr1:=trim(ABStringReplace(tempStr4,'ABGetAllPY',EmptyStr));
          tempStr1:=ABGetAllPY(tempStr1);
        end
        else if ABPos('ABGetFirstWB',tempStr4)>0 then
        begin
          tempStr1:=trim(ABStringReplace(tempStr4,'ABGetFirstWB',EmptyStr));
          tempStr1:=ABGetFirstWB(AnsiString(tempStr1));
        end
        else if ABPos('ABGetAllWB',tempStr4)>0 then
        begin
          tempStr1:=trim(ABStringReplace(tempStr4,'ABGetAllWB',EmptyStr));
          tempStr1:=ABGetAllWB(tempStr1);
        end
        else
          tempStr1:=ABExecScript(tempStr4,False,false);
      end;

      if AnsiCompareText(AText,tempStr1)<>0 then
      begin
        AText:= tempStr1;
      end;

      if Assigned(tempEvent.FOldColumnGetDataText) then
        tempEvent.FOldColumnGetDataText(Sender,ARecordIndex,AText);
    end;
  end;
end;

procedure TABcxGridPopupMenu.NewCustomDrawIndicatorCell(
  Sender: TcxGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxCustomGridIndicatorItemViewInfo; var ADone: Boolean);
var
  i:longint;
  tempEvent: PIndicatorCellCustomDrawEvent;
begin
  i := FIndicatorCellCustomDraw.IndexOf(FLinkTableView.Name );
  if (i >= 0) then
  begin
    tempEvent := PIndicatorCellCustomDrawEvent(FIndicatorCellCustomDraw.Objects[i]);
    ABSetAutoIDValue(AViewInfo,ACanvas,ADone);

    if Assigned(tempEvent.FOldIndicatorCellCustomDraw) then
      tempEvent.FOldIndicatorCellCustomDraw(Sender,ACanvas,AViewInfo,ADone);
  end;
end;

procedure TABcxGridPopupMenu.NewStylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem;
  {$IF (CompilerVersion>22.0)}
  var
  {$ELSE}
  out
  {$IFEND}
   AStyle: TcxStyle
  );
var
  tempstr1,tempstr2,tempstr3:String;

  tempOnlyDoItem: TcxCustomGridTableItem;
  i,j:longint;
  tempEvent: PCellCustomDrawEvent;
  tempColor:TColor;
  tempStyl:TcxStyle;
begin
  if (not Assigned(AItem)) or  ( not AItem.Visible) then
     Exit;

  i := FCellCustomDraw.IndexOf(FLinkTableView.Name );
  if (i >= 0) then
  begin
    tempEvent := PCellCustomDrawEvent(FCellCustomDraw.Objects[i]);
    if trim(tempEvent.ColorCommandtext)<>EmptyStr then
    begin
      for I := 1 to  ABGetSpaceStrCount(tempEvent.ColorCommandtext,'[end]') do
      begin
        tempstr2:=trim(ABGetSpaceStr(tempEvent.ColorCommandtext,i,'[end]'));
        tempOnlyDoItem:=nil;
        j:=ABPos(ColumnColorFlag,tempstr2) ;
        if j>0 then
        begin
          tempOnlyDoItem:=AItem;
          Delete(tempstr2,j,Length(ColumnColorFlag));
        end
        else
        begin
        end;

        if tempstr2<>EmptyStr then
        begin
          if (not Assigned(tempOnlyDoItem )) or
             (ABPOS(':'+tempOnlyDoItem.Caption,tempstr2)>0) then
          begin
            tempstr1:=
                ABReplaceColumnCaption(tempstr2,
                                       TcxGridTableView(Sender),
                                       ARecord.RecordIndex);
            if tempstr1<>EmptyStr then
            begin
              tempstr3:=ABExecScript(tempstr1,False,false);
              tempColor:= StrToIntDef(tempstr3,-1);
              if (tempColor<>-1) then
              begin
                tempStyl:=GetStyleByColor(IntToStr(tempColor));
                if AStyle<>tempStyl then
                  AStyle := tempStyl;
              end;
            end;

          end;
        end;
      end;
    end;

    if Assigned(tempEvent.FOldStylesGetContentStyle) then
      tempEvent.FOldStylesGetContentStyle(Sender,ARecord,AItem,AStyle);
  end;

end;

procedure TABcxGridPopupMenu.NewGroupSummary(
  ASender: TcxDataSummaryItems; Arguments: TcxSummaryEventArguments;
  var OutArguments: TcxSummaryEventOutArguments);
var
  tempColumn:TcxGridColumn;
begin
  if (Arguments.SummaryItem.Format<>EmptyStr) and
     (assigned(Arguments.SummaryItem.Field))and
     (assigned(Arguments.SummaryItem.Field.Item)) then
  begin
    tempColumn:=TcxGridColumn(Arguments.SummaryItem.Field.Item);
    if (Assigned(tempColumn)) and (ABIsCaleColumn(tempColumn)) then
    begin
      if AnsiCompareText(tempColumn.DataBinding.ValueType,'String')=0 then
      begin
        OutArguments.Value:=ABGetColumnValue(tempColumn,Arguments.RecordIndex);
      end
      else if (AnsiCompareText(tempColumn.DataBinding.ValueType,'Float')=0) or
              (AnsiCompareText(tempColumn.DataBinding.ValueType,'Integer')=0) or
              (AnsiCompareText(tempColumn.DataBinding.ValueType,'DateTime')=0) then
      begin
        OutArguments.Value:=StrToFloatDef(ABGetColumnValue(tempColumn,Arguments.RecordIndex),0);
      end;
    end;

    if Assigned(FOldGroupSummary) then
      FOldGroupSummary(ASender,Arguments,OutArguments);
  end;
end;

procedure TABcxGridPopupMenu.NewSummary(ASender: TcxDataSummaryItems;
  Arguments: TcxSummaryEventArguments;
  var OutArguments: TcxSummaryEventOutArguments);
var
  tempColumn:TcxGridColumn;
begin
  if Arguments.SummaryItem.Format<>EmptyStr then
  begin
    tempColumn:=TcxGridColumn(Arguments.SummaryItem.Field.Item);
    if (Assigned(tempColumn)) and (ABIsCaleColumn(tempColumn)) then
    begin
      if AnsiCompareText(tempColumn.DataBinding.ValueType,'String')=0 then
      begin
        OutArguments.Value:=ABGetColumnValue(tempColumn,Arguments.RecordIndex);
      end
      else if (AnsiCompareText(tempColumn.DataBinding.ValueType,'Float')=0) or
              (AnsiCompareText(tempColumn.DataBinding.ValueType,'Integer')=0) or
              (AnsiCompareText(tempColumn.DataBinding.ValueType,'DateTime')=0) then
      begin
        OutArguments.Value:=StrToFloatDef(ABGetColumnValue(tempColumn,Arguments.RecordIndex),0);
      end;
    end;

    if Assigned(FOldSummary) then
      FOldSummary(ASender,Arguments,OutArguments);
  end;
end;

procedure TABcxGridPopupMenu.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited;
  if (Operation = opRemove) then
  begin
    if (FLinkTableView =AComponent)  then
      FLinkTableView := nil;
  end;
end;

procedure TABcxGridPopupMenu.Load;
var
  tempStr1:string;
begin
  if Assigned(FLinkTableView) then
  begin
    FLinkTableView.BeginUpdate;
    try
      //加载前先清除上一次打开数据集的Summary内容
      FLinkTableView.DataController.Summary.DefaultGroupSummaryItems.Clear;
      FLinkTableView.DataController.Summary.SummaryGroups.Clear;
      FLinkTableView.DataController.Summary.FooterSummaryItems.Clear;


      //清除上一次打开数据集的附加表头内容
      if (FLinkTableView is TcxGridDBBandedTableView) then
      begin
        TcxGridDBBandedTableView(FLinkTableView).OptionsView.BandHeaders:=False;
        while TcxGridDBBandedTableView(FLinkTableView).Bands.Count>1 do
        begin
          TcxGridDBBandedTableView(FLinkTableView).Bands.Delete(1);
        end;

        //TcxGridDBBandedTableView(FLinkTableView).Bands.Clear;
        {
        for I := 1 to TcxGridDBBandedTableView(FLinkTableView).Bands.Count - 1 do
        begin
          TcxGridDBBandedTableView(FLinkTableView).Bands.Delete(i);
          //TcxGridDBBandedTableView(FLinkTableView).Bands[i].Caption:=emptystr;
          //TcxGridDBBandedTableView(FLinkTableView).Bands[i].Visible:=False;
        end;
        }
      end;
    finally
      FLinkTableView.EndUpdate;
    end;

    LoadViewProperty;
    LoadViewStyleSheets;

    FLinkTableView.BeginUpdate;
    try
      LoadViewSetColor(true);
      ABSetColumnPropertiesClass(FLinkTableView);
      LoadViewOtherSet;

      //ABDelNotCaleAndNotLinkFieldColumns放到LoadViewOtherSet后是因为LoadViewOtherSet中会对计算字段进行标识,如果
      //放在LoadViewProperty中, ABDelNotCaleAndNotLinkFieldColumns就会误将计算字段删除
      tempStr1:=GetFilename('_Property.ini');
      if ABCheckFileExists(tempStr1) then
      begin
        //删除 Load中因RestoreFromIniFile而多创建的列（计算列除外）
        ABDelNotCaleAndNotLinkFieldColumns(FLinkTableView);
      end;

      {
      for i := FLinkTableView.columncount-1 downto  0 do
      begin
        FLinkTableView.Columns[i].OnGetCellHint:=PubColumnGetCellHint;
      end;
      }
      //汇总事件重定义，以做计算字段的汇总
      if (Assigned(FLinkTableView)) and
         (Assigned(FLinkTableView.DataController)) and
         (Assigned(FLinkTableView.DataController.Summary)) and
         (Assigned(FLinkTableView.DataController.Summary.FooterSummaryItems))  then
      begin
        FOldSummary:=FLinkTableView.DataController.Summary.FooterSummaryItems.OnSummary;
        FLinkTableView.DataController.Summary.FooterSummaryItems.OnSummary:=NewSummary;
      end;

      if (Assigned(FLinkTableView)) and
         (Assigned(FLinkTableView.DataController)) and
         (Assigned(FLinkTableView.DataController.Summary)) and
         (Assigned(FLinkTableView.DataController.Summary.DefaultGroupSummaryItems)) then
      begin
        FOldGroupSummary:=FLinkTableView.DataController.Summary.DefaultGroupSummaryItems.OnSummary;
        FLinkTableView.DataController.Summary.DefaultGroupSummaryItems.OnSummary:=NewGroupSummary;
      end;

      Refresh;
    finally
      FLinkTableView.EndUpdate;
    end;
  end;
end;

procedure TABcxGridPopupMenu.Save;
begin
  if Assigned(FLinkTableView) then
  begin
    FLinkTableView.Focused:=true;
    SaveViewProperty;
    SaveViewStyleSheets;
    SaveViewSetColor;
    SaveViewOtherSet;
  end;
end;

procedure TABcxGridPopupMenu.ChangeFileNameBySetupFileNameSuffix(aOldFileStr,aNewFileStr:string);
var
  I:LongInt;
  tempOldFileName,
  tempNewFileName:string;
begin
  for I := Low(FSaveFileNames) to high(FSaveFileNames) do
  begin
    tempOldFileName:=ABConfigPath+aOldFileStr +FSaveFileNames[i];
    if ABCheckFileExists(tempOldFileName) then
    begin
      tempNewFileName:=ABConfigPath+aNewFileStr +FSaveFileNames[i];
      RenameFile(tempOldFileName,tempNewFileName);
    end;
  end;
end;

procedure TABcxGridPopupMenu.OpreatorFile(aClear:boolean;aUpToDatabase:Boolean;aDownFromDatabase:boolean;
                                          aSetToTempPath:boolean;aTempPathFileNameStrings:TStrings);
var
  i,j:LongInt;
  tempFileName,tempOnlyFileName:string;
  procedure DoItem;
  begin
    if aClear then
    begin
      if ABCheckFileExists(tempFileName) then
      begin
        DeleteFile(tempFileName);
      end;
    end
    else if aUpToDatabase then
    begin
      if (ABCheckFileExists(tempFileName)) and (FConnName<>emptystr) then
      begin
        ABExecSQL(FConnName,
                        'exec Proc_SetPublicPkgNewVersion '+QuotedStr(tempOnlyFileName)+',''ABSoft_Set\ABClientP\Config'' ' );
        ABUpFileToField(FConnName,'ABSys_Org_Function','FU_Pkg',
                         ' where FU_FileName='+QuotedStr(tempOnlyFileName),
                         tempFileName);

        if not ABCheckFileExists(ABConfigPath+tempOnlyFileName) then
        begin
          ABCopyFile(tempFileName,ABConfigPath+tempOnlyFileName);
        end;
      end;
    end
    else if aDownFromDatabase then
    begin
      if  (FConnName<>emptystr) then
        ABDownToFileFromField(FConnName,'ABSys_Org_Function','FU_Pkg',
                      ' where FU_FileName='+QuotedStr(tempOnlyFileName),
                      tempFileName);
    end
    else if aSetToTempPath then
    begin
      if ABCheckFileExists(tempFileName) then
      begin
        ABCopyFile(tempFileName,ABTempPath+tempOnlyFileName);
        aTempPathFileNameStrings.add(ABTempPath+tempOnlyFileName);
      end;
    end;
  end;
begin
  if (Assigned(FLinkTableView)) and
     (Assigned(FLinkTableView.Owner)) then
  begin
    for I := Low(FSaveFileNames) to high(FSaveFileNames) do
    begin
      if AnsiCompareText(FSaveFileNames[i],'_CaleColumnSQL.ini')=0 then
      begin
        j:=0;
        while True do
        begin
          tempFileName:=GetFilename('_CaleColumnSQL'+inttostr(j)+'.ini');
          tempOnlyFileName:=ABGetFilename(tempFileName,false);
          if ((aClear) or (aUpToDatabase)  or (aSetToTempPath)) and
             (not ABCheckFileExists(tempFileName)) then
          begin
            Break;
          end;

          DoItem;

          if (aDownFromDatabase) and
             (not ABCheckFileExists(tempFileName)) then
          begin
            Break;
          end;

          j:=j+1;
        end;
      end
      else
      begin
        tempFileName:=GetFilename(FSaveFileNames[i]);
        tempOnlyFileName:=ABGetFilename(tempFileName,false);
        DoItem;
      end;
    end;
  end;
end;


procedure TABcxGridPopupMenu.PopupMenu_Admin_ReCreateAllItems(Sender: TObject);
begin
  CreateAllItems;
end;

procedure TABcxGridPopupMenu.LoadSetupFile;
var
  tempAutoCreateAllItem:boolean;
begin
  if Assigned(FLinkTableView) then
  begin
    if not ABExecNoParamPublishProp(FLinkTableView,'CreateAllColumn') then
    begin
      ClearAllItems;
      tempAutoCreateAllItem:=AutoCreateAllItem;
      AutoCreateAllItem:=True;
      try
        Load;
      finally
        AutoCreateAllItem:=tempAutoCreateAllItem;
      end;
    end;
  end;
end;

procedure TABcxGridPopupMenu.PopupMenu_Admin_SetupHaveField(Sender: TObject);
begin
  ABExecNoParamPublishProp(FLinkTableView,'SetupHaveField');
end;

procedure TABcxGridPopupMenu.PopupMenu_Admin_SetupNoHaveField(Sender: TObject);
begin
  ABExecNoParamPublishProp(FLinkTableView,'SetupNoHaveField');
end;

procedure TABcxGridPopupMenu.PopupMenu_Admin_SetupReadOnlyField(Sender: TObject);
begin
  ABExecNoParamPublishProp(FLinkTableView,'SetupReadOnlyField');
end;

procedure TABcxGridPopupMenu.PopupMenu_Admin_SetupEditField(Sender: TObject);
begin
  ABExecNoParamPublishProp(FLinkTableView,'SetupEditField');
end;

procedure TABcxGridPopupMenu.PopupMenu_Admin_ClearSetupField(Sender: TObject);
begin
  ABExecNoParamPublishProp(FLinkTableView,'ClearSetupField');
end;

procedure TABcxGridPopupMenu.PopupMenu_Admin_TableViewCanEdit(Sender: TObject);
begin
  FLinkTableView.OptionsData.Editing:=TMenuItem(Sender).Checked;
end;

procedure TABcxGridPopupMenu.PopupMenu_Agg_Count(Sender: TObject);
begin
  ABSelectMenuItemOfGroup(TMenuItem(Sender));
  SetColumnFoot(skCount);
end;

procedure TABcxGridPopupMenu.PopupMenu_Agg_Sum(Sender: TObject);
begin
  ABSelectMenuItemOfGroup(TMenuItem(Sender));
  SetColumnFoot(skSum);
end;

procedure TABcxGridPopupMenu.PopupMenu_Agg_Max(Sender: TObject);
begin
  ABSelectMenuItemOfGroup(TMenuItem(Sender));
  SetColumnFoot(skMax);
end;

procedure TABcxGridPopupMenu.PopupMenu_Agg_Min(Sender: TObject);
begin
  ABSelectMenuItemOfGroup(TMenuItem(Sender));
  SetColumnFoot(skMin);
end;

procedure TABcxGridPopupMenu.PopupMenu_Agg_Avg(Sender: TObject);
begin
  ABSelectMenuItemOfGroup(TMenuItem(Sender));
  SetColumnFoot(skAverage);
end;

procedure TABcxGridPopupMenu.PopupMenu_Agg_Clear(Sender: TObject);
begin
  ABSelectMenuItemOfGroup(TMenuItem(Sender));
  SetColumnFoot(skNone);
end;

procedure TABcxGridPopupMenu.PopupMenu_Align_Center(Sender: TObject);
begin
  ABSelectMenuItemOfGroup(TMenuItem(Sender));
  SetColumnAlign(taCenter);
end;

procedure TABcxGridPopupMenu.PopupMenu_Align_Left(Sender: TObject);
begin
  ABSelectMenuItemOfGroup(TMenuItem(Sender));
  SetColumnAlign(taLeftJustify);
end;

procedure TABcxGridPopupMenu.PopupMenu_Align_Right(Sender: TObject);
begin
  ABSelectMenuItemOfGroup(TMenuItem(Sender));
  SetColumnAlign(taRightJustify);
end;

procedure TABcxGridPopupMenu.PopupMenu_AutoColumWidth(Sender: TObject);
begin
  FLinkTableView.OptionsView.ColumnAutoWidth:=TMenuItem(Sender).Checked;
end;

procedure TABcxGridPopupMenu.PopupMenu_Band_Add(Sender: TObject);
var
  tempstr1,tempstr2:string;
  i,j:LongInt;
  tempHave:Boolean;
begin
  if (FLinkTableView is TcxGridDBBandedTableView) then
  begin
    if InputQuery('增加表头','表头名称',tempstr1) then
    begin
      if tempstr1<>emptystr then
      begin
        tempstr1:=ABStringReplace(tempstr1,';',',');
        for I := 1 to ABGetSpaceStrCount(tempstr1,',') do
        begin
          tempstr2:=ABGetSpaceStr(tempstr1,i,',');
          tempHave:=false;
          for j := 0 to TcxGridDBBandedTableView(FLinkTableView).Bands.Count-1 do
          begin
            if AnsiCompareText(TcxGridDBBandedTableView(FLinkTableView).Bands.Items[j].Caption,tempstr2)=0 then
            begin
              tempHave:=true;
              break;
            end;
          end;

          if not tempHave then
          begin
            TcxGridDBBandedTableView(FLinkTableView).Bands.Add;
            TcxGridDBBandedTableView(FLinkTableView).Bands[TcxGridDBBandedTableView(FLinkTableView).Bands.Count-1].Caption:=tempstr2;
          end;
        end;
      end;
    end;
  end;
end;

procedure TABcxGridPopupMenu.PopupMenu_Band_Del(Sender: TObject);
var
  i:longint;
  tempForm:TABSelectListBoxForm;
begin
  if (FLinkTableView is TcxGridDBBandedTableView) then
  begin
    tempForm := TABSelectListBoxForm.Create(nil);
    try
      tempForm.Caption:=('删除表头');
      tempForm.ListBox1.Clear;
      for i := 0 to TcxGridDBBandedTableView(FLinkTableView).Bands.Count-1 do
      begin
        tempForm.ListBox1.Items.Add(TcxGridDBBandedTableView(FLinkTableView).Bands[i].Caption);
      end;

      if (tempForm.ShowModal=mrok) and (tempForm.ListBox1.ItemIndex>=0) then
      begin
        if TcxGridDBBandedTableView(FLinkTableView).Bands[tempForm.ListBox1.ItemIndex].ColumnCount<=0 then
          TcxGridDBBandedTableView(FLinkTableView).Bands.Delete(tempForm.ListBox1.ItemIndex)
        else
          ABShow('此表头下还包含列,只有不包含列的表头才可删除,请先移除列.');
      end;
    finally
      tempForm.Free;
    end;
  end;
end;

procedure TABcxGridPopupMenu.PopupMenu_Band_EditCaption(Sender: TObject);
var
  i:longint;
  tempForm:TABMemoForm;
begin
  if (FLinkTableView is TcxGridDBBandedTableView) then
  begin
    tempForm := TABMemoForm.Create(nil);
    try
      tempForm.Caption:=('修改表头');
      tempForm.Memo1.Clear;
      for i := 0 to TcxGridDBBandedTableView(FLinkTableView).Bands.Count-1 do
      begin
        tempForm.Memo1.Lines.Add((Format('第%d表头=', [i]))+
                                 TcxGridDBBandedTableView(FLinkTableView).Bands[i].Caption);
      end;

      if tempForm.ShowModal=mrok then
      begin
        for i := 0 to TcxGridDBBandedTableView(FLinkTableView).Bands.Count-1 do
        begin
          TcxGridDBBandedTableView(FLinkTableView).Bands[i].Caption:=
            ABGetItemValue(tempForm.Memo1.Lines[i],
              (Format('第%d表头=', [i])),'=') ;
        end;
      end;
    finally
      tempForm.Free;
    end;
  end;
end;

procedure TABcxGridPopupMenu.PopupMenu_Band_Show(Sender: TObject);
begin
  if (FLinkTableView is TcxGridDBBandedTableView) then
    TcxGridDBBandedTableView(FLinkTableView).OptionsView.BandHeaders:=TMenuItem(Sender).Checked;
end;

procedure TABcxGridPopupMenu.PopupMenu_BestColumWidth(Sender: TObject);
begin
  FLinkTableView.BeginUpdate;
  try
    if (FLinkTableView.GroupedColumnCount>0) and
       (FLinkTableView.ColumnCount>0) then
    begin
      FLinkTableView.Columns[0].GroupIndex:=0;

      if FLinkTableView.DataController.FocusedRowIndex>=0  then
        FLinkTableView.ViewData.Rows[FLinkTableView.DataController.FocusedRowIndex].Expand(True);

      FLinkTableView.ApplyBestFit;
      FLinkTableView.Columns[0].GroupIndex:=-1;
    end
    else
    begin
      FLinkTableView.ApplyBestFit;
    end;
  finally
    FLinkTableView.EndUpdate;
  end;
end;

procedure TABcxGridPopupMenu.PopupMenu_CaleColum_Add(Sender: TObject);
var
  i,j:longint;
  tempForm:TABcxGridPopupMenu_AddColumForm;
  tempColumn:TcxGridColumn;
  tempEvent:PColumnGetDataTextEvent;
begin
  tempForm := TABcxGridPopupMenu_AddColumForm.Create(nil);
  try
    j:=-1;
    if (FLinkTableView.Controller.FocusedColumn is TcxGridDBBandedColumn) then
      j:=  TcxGridDBBandedColumn(FLinkTableView.Controller.FocusedColumn).Position.BandIndex;

    tempForm.Caption:=('增加计算列');
    tempForm.ListBox1.Clear;
    for i := 0 to FLinkTableView.ColumnCount-1 do
    begin
      if ((FLinkTableView.Columns[i] is  TcxGridDBBandedColumn) and
          (Assigned(TcxGridDBBandedColumn(FLinkTableView.Columns[i]).DataBinding.Field))) or
         ((FLinkTableView.Columns[i] is TcxGridDBColumn) and
          (Assigned(TcxGridDBColumn(FLinkTableView.Columns[i]).DataBinding.Field)))
           then
      begin
        tempForm.ListBox1.Items.Add(FLinkTableView.Columns[i].Caption);
      end;
    end;

    if tempForm.ShowModal=mrok then
    begin
      tempColumn:=FLinkTableView.CreateColumn;
      tempColumn.Tag:=19003;
      tempColumn.DataBinding.ValueType:=  tempForm.ComboBox1.Text ;
      tempColumn.Options.Editing:=false;
      tempColumn.Caption:=tempForm.Edit1.Text;

      //if AnsiCompareText(tempColumn.DataBinding.ValueType,'Boolean')=0 then
        //tempColumn.PropertiesClass:=tcx;

      if (j<>-1) and
         (tempColumn is TcxGridDBBandedColumn) then
      begin
        TcxGridDBBandedColumn(tempColumn).Position.BandIndex:=j;
      end;

      if FColumnsOnGetDataText.IndexOf(FLinkTableView.Name+IntToStr(tempColumn.id))<0 then
      begin
        New(tempEvent);
        tempEvent.EditValues:=TStringList.Create;
        TStringList(tempEvent.EditValues).Sorted:=true;

        tempColumn.Name:=FLinkTableView.Name+'tempCreateCale'+ inttostr(tempColumn.ID);
        tempEvent.ColumnName:=tempColumn.Name;

        tempEvent.Self:=tempColumn;
        tempEvent.ColumnType :=tempColumn.DataBinding.ValueType;
        tempEvent.FOldColumnGetDataText:=tempColumn.OnGetDataText;
        tempEvent.SQL:=trim(tempForm.Memo1.Text);
        FColumnsOnGetDataText.AddObject(FLinkTableView.Name+IntToStr(tempColumn.id),TObject(tempEvent));
        tempColumn.OnGetDataText:=NewColumnsGetDataText;
      end;

      if (FLinkTableView is TcxGridDBBandedTableView) then
        TcxGridDBBandedTableView(FLinkTableView).DataController.DataSet.Last
      else if (FLinkTableView is TcxGridDBTableView) then
        TcxGridDBTableView(FLinkTableView).DataController.DataSet.Last;
    end;
  finally
    tempForm.Free;
  end;
end;

procedure TABcxGridPopupMenu.PopupMenu_CaleColum_Del(Sender: TObject);
var
  i,j:longint;
  tempForm:TABSelectListBoxForm;
  tempGridTableItem:TcxCustomGridTableItem;
begin
  tempForm := TABSelectListBoxForm.Create(nil);
  try
    tempForm.Caption:=('删除计算列');
    tempForm.ListBox1.Clear;
    for i := 0 to FLinkTableView.ColumnCount-1 do
    begin
      if ((FLinkTableView.Columns[i] is TcxGridDBBandedColumn) and
          (not Assigned(TcxGridDBBandedColumn(FLinkTableView.Columns[i]).DataBinding.Field))) or
         ((FLinkTableView.Columns[i] is TcxGridDBColumn) and
          (not Assigned(TcxGridDBColumn(FLinkTableView.Columns[i]).DataBinding.Field)))
           then
      begin
        tempForm.ListBox1.Items.Add(FLinkTableView.Columns[i].caption+'='+inttostr(i));
      end;
    end;

    if (tempForm.ShowModal=mrok) and (tempForm.ListBox1.ItemIndex>=0) then
    begin
      j:=strtoint(ABGetItemValue(tempForm.ListBox1.Items[tempForm.ListBox1.ItemIndex],'='));
      tempGridTableItem:= FLinkTableView.FindItemByID(j);
      if Assigned(tempGridTableItem) then
      begin
        j:=FColumnsOnGetDataText.IndexOf(FLinkTableView.Name+IntToStr(tempGridTableItem.id));
        if j>=0 then
        begin
          if (Assigned(PColumnGetDataTextEvent(FColumnsOnGetDataText.Objects[j]).EditValues)) then
            PColumnGetDataTextEvent(FColumnsOnGetDataText.Objects[j]).EditValues.Free;

          Dispose(Pointer(FColumnsOnGetDataText.Objects[j]));
          FColumnsOnGetDataText.Delete(j);
        end;
        tempGridTableItem.Free;
      end;
    end;
  finally
    tempForm.Free;
  end;
end;

procedure TABcxGridPopupMenu.PopupMenu_CaleColum_Edit(Sender: TObject);
var
  i,j:longint;
  tempForm:TABcxGridPopupMenu_AddColumForm;
  tempColumn:TcxGridColumn;
  tempEvent: PColumnGetDataTextEvent;
begin
  tempForm := TABcxGridPopupMenu_AddColumForm.Create(nil);
  try
    tempColumn:=FLinkTableView.Controller.FocusedColumn;
    j := FColumnsOnGetDataText.IndexOf(FLinkTableView.Name+IntToStr(tempColumn.id));
    if (j >= 0) then
    begin
      tempEvent := PColumnGetDataTextEvent(FColumnsOnGetDataText.Objects[j]);
      tempForm.Memo1.Text:=tempEvent.SQL;
      tempForm.Edit1.Text:=tempColumn.Caption;
      if tempColumn.DataBinding.ValueType<>EmptyStr then
        tempForm.ComboBox1.Text:=tempColumn.DataBinding.ValueType;
      tempForm.Caption:=('修改计算列');
      tempForm.ListBox1.Clear;
      for i := 0 to FLinkTableView.ColumnCount-1 do
      begin
        if ((FLinkTableView.Columns[i] is TcxGridDBBandedColumn) and
            (Assigned(TcxGridDBBandedColumn(FLinkTableView.Columns[i]).DataBinding.Field))) or
           ((FLinkTableView.Columns[i] is TcxGridDBColumn) and
            (Assigned(TcxGridDBColumn(FLinkTableView.Columns[i]).DataBinding.Field)))
             then
        begin
          tempForm.ListBox1.Items.Add(FLinkTableView.Columns[i].caption);
        end;
      end;

      if tempForm.ShowModal=mrok then
      begin
        tempColumn.DataBinding.ValueType:=tempForm.ComboBox1.Text;
        tempColumn.Options.Editing:=false;
        tempColumn.Caption:=tempForm.Edit1.Text;
        tempColumn.Name:=FLinkTableView.Name+'tempCreateCale'+ inttostr(tempColumn.ID);

        tempEvent.ColumnType:= tempColumn.DataBinding.ValueType ;
        tempEvent.SQL:= trim(tempForm.Memo1.Text) ;
        tempEvent.ColumnName:=tempColumn.Name;

        if (FLinkTableView is TcxGridDBBandedTableView) then
          TcxGridDBBandedTableView(FLinkTableView).DataController.DataSet.Last
        else if (FLinkTableView is TcxGridDBTableView) then
          TcxGridDBTableView(FLinkTableView).DataController.DataSet.Last;
      end;
    end;
  finally
    tempForm.Free;
  end;
end;

procedure TABcxGridPopupMenu.PopupMenu_ColorSetup(Sender: TObject);
begin
  if not Assigned(FColorSetupForm) then
    FColorSetupForm := TABcxGridPopupMenu_ColorSetupForm.Create(nil);

  TABcxGridPopupMenu_ColorSetupForm(FColorSetupForm).LinkTableView:=FLinkTableView;
  TABcxGridPopupMenu_ColorSetupForm(FColorSetupForm).cxListBox1.Items.Text:=FCurSetColorStr;

  if (TABcxGridPopupMenu_ColorSetupForm(FColorSetupForm).ShowModal=mrOk) then
  begin
    FCurSetColorStr:=trim(TABcxGridPopupMenu_ColorSetupForm(FColorSetupForm).cxListBox1.Items.Text);
    LoadViewSetColor(false);
    Refresh;
  end;
end;

procedure TABcxGridPopupMenu.PopupMenu_ColumnFilter(Sender: TObject);
begin
  FLinkTableView.OptionsCustomize.ColumnFiltering:=TMenuItem(Sender).Checked;
end;

procedure TABcxGridPopupMenu.PopupMenu_ConstBandPosition_Left(Sender: TObject);
begin
  ABSelectMenuItemOfGroup(TMenuItem(Sender));
  if TMenuItem(Sender).Checked then
    TcxGridDBBandedColumn(FLinkTableView.Controller.FocusedColumn).Position.Band.FixedKind:=fkLeft
end;

procedure TABcxGridPopupMenu.PopupMenu_ConstBandPosition_None(Sender: TObject);
begin
  ABSelectMenuItemOfGroup(TMenuItem(Sender));
  if TMenuItem(Sender).Checked then
    TcxGridDBBandedColumn(FLinkTableView.Controller.FocusedColumn).Position.Band.FixedKind:=fkNone
end;

procedure TABcxGridPopupMenu.PopupMenu_ConstBandPosition_Right(Sender: TObject);
begin
  ABSelectMenuItemOfGroup(TMenuItem(Sender));
  if TMenuItem(Sender).Checked then
    TcxGridDBBandedColumn(FLinkTableView.Controller.FocusedColumn).Position.Band.FixedKind:=fkRight
end;

procedure TABcxGridPopupMenu.PopupMenu_ContentSelect_RowSelect(Sender: TObject);
begin
  ABSelectMenuItemOfGroup(TMenuItem(Sender));
  if TMenuItem(Sender).Checked then
  begin
    FLinkTableView.OptionsSelection.CellMultiSelect:=false;
    FLinkTableView.OptionsSelection.MultiSelect:=false;
  end;
end;

procedure TABcxGridPopupMenu.PopupMenu_ContentSelect_MultiRowSelect(Sender: TObject);
begin
  ABSelectMenuItemOfGroup(TMenuItem(Sender));
  if TMenuItem(Sender).Checked then
  begin
    FLinkTableView.OptionsSelection.CellMultiSelect:=false;
    FLinkTableView.OptionsSelection.MultiSelect:=true;
  end;
end;

procedure TABcxGridPopupMenu.PopupMenu_ContentSelect_Cell(Sender: TObject);
begin
  if TMenuItem(Sender).Checked then
    FLinkTableView.OptionsSelection.CellMultiSelect:=false;

  FLinkTableView.OptionsSelection.CellSelect:=TMenuItem(Sender).Checked;
end;

procedure TABcxGridPopupMenu.PopupMenu_ContentSelect_MultiRowCellSelect(Sender: TObject);
begin
  ABSelectMenuItemOfGroup(TMenuItem(Sender));
  if TMenuItem(Sender).Checked then
  begin
    FLinkTableView.OptionsSelection.CellMultiSelect:=true;
  end;
end;

procedure TABcxGridPopupMenu.PopupMenu_CustomFilter(Sender: TObject);
begin
  FLinkTableView.Filtering.RunCustomizeDialog(nil);
end;

procedure TABcxGridPopupMenu.PopupMenu_Figure(Sender: TObject);
begin
  ABcxGridPopupMenu_Figure(FLinkTableView);
end;

procedure TABcxGridPopupMenu.PopupMenu_FilterBarShowMode_Always(Sender: TObject);
begin
  ABSelectMenuItemOfGroup(TMenuItem(Sender));
  if TMenuItem(Sender).Checked then
  begin
    FLinkTableView.FilterBox.Visible:=fvAlways;
  end;
end;

procedure TABcxGridPopupMenu.PopupMenu_FilterBarShowMode_Never(Sender: TObject);
begin
  ABSelectMenuItemOfGroup(TMenuItem(Sender));
  if TMenuItem(Sender).Checked then
  begin
    FLinkTableView.FilterBox.Visible:=fvNever;
  end;
end;

procedure TABcxGridPopupMenu.PopupMenu_FilterBarShowMode_NonEmpty(Sender: TObject);
begin
  ABSelectMenuItemOfGroup(TMenuItem(Sender));
  if TMenuItem(Sender).Checked then
  begin
    FLinkTableView.FilterBox.Visible:=fvNonEmpty;
  end;
end;

procedure TABcxGridPopupMenu.PopupMenu_Find(Sender: TObject);
begin
  ABcxDataControllerAllLocal(LinkGrid);
end;

procedure TABcxGridPopupMenu.PopupMenu_Order_Asc(Sender: TObject);
var
  tempColumn:TcxGridColumn;
begin
  ABSelectMenuItemOfGroup(TMenuItem(Sender));
  tempColumn:=TcxGridColumn(FLinkTableView.Controller.FocusedColumn);
  if Assigned(tempColumn) then
  begin
    tempColumn.SortOrder:=soascending;
  end;
end;

procedure TABcxGridPopupMenu.PopupMenu_Order_Desc(Sender: TObject);
var
  tempColumn:TcxGridColumn;
begin
  ABSelectMenuItemOfGroup(TMenuItem(Sender));
  tempColumn:=TcxGridColumn(FLinkTableView.Controller.FocusedColumn);
  if Assigned(tempColumn) then
  begin
    tempColumn.SortOrder:=soDescending;
  end;
end;

procedure TABcxGridPopupMenu.PopupMenu_Order_DelAscORDesc(Sender: TObject);
var
  tempColumn:TcxGridColumn;
begin
  ABSelectMenuItemOfGroup(TMenuItem(Sender));
  tempColumn:=TcxGridColumn(FLinkTableView.Controller.FocusedColumn);
  if Assigned(tempColumn) then
  begin
    tempColumn.SortOrder:=soNone;
  end;
end;


procedure TABcxGridPopupMenu.PopupMenu_Other_AutoRowHeight(Sender: TObject);
begin
  if TMenuItem(Sender).Checked then
  begin
    FLinkTableView.OptionsView.CellAutoHeight:=true;
    FLinkTableView.OptionsView.DataRowHeight:=0;
  end
  else
  begin
    FLinkTableView.OptionsView.CellAutoHeight:=false;
    FLinkTableView.OptionsView.DataRowHeight:=18;
  end;
end;

procedure TABcxGridPopupMenu.PopupMenu_Other_AutoRowNum(Sender: TObject);
begin
  SetIndicator(TMenuItem(Sender).Checked);
end;

procedure TABcxGridPopupMenu.PopupMenu_Other_ColumnHorzSizing(Sender: TObject);
begin
  FLinkTableView.OptionsCustomize.ColumnHorzSizing:=TMenuItem(Sender).Checked;
end;

procedure TABcxGridPopupMenu.PopupMenu_Other_ColumnsQuickCustomization(
  Sender: TObject);
begin
  FLinkTableView.OptionsCustomize.ColumnsQuickCustomization:=TMenuItem(Sender).Checked;
end;


procedure TABcxGridPopupMenu.PopupMenu_Other_DataRowSizing(Sender: TObject);
begin
  FLinkTableView.OptionsCustomize.DataRowSizing:=TMenuItem(Sender).Checked;
end;

procedure TABcxGridPopupMenu.PopupMenu_Other_DirectionKeyMoveColumn(
  Sender: TObject);
begin
  FLinkTableView.OptionsBehavior.AlwaysShowEditor:=TMenuItem(Sender).Checked;
end;

procedure TABcxGridPopupMenu.PopupMenu_Other_DisplayFormat(Sender: TObject);
var
  tempstr:string;
  tempProperties:TcxCustomEditProperties;
begin
  if (Assigned(FLinkTableView.Controller.FocusedColumn)) then
  begin
    tempProperties:=FLinkTableView.Controller.FocusedColumn.Properties;
    if (Assigned(tempProperties)) and
       (tempProperties is TcxCustomTextEditProperties) then
    begin
      tempstr:=TcxCustomTextEditProperties(tempProperties).DisplayFormat;
      if InputQuery('设置显示格式','显示格式',tempstr) then
      begin
        try
          TcxCustomTextEditProperties(tempProperties).DisplayFormat:=tempstr;
        except
          ABShow('格式设置出错['+tempstr+']');
        end;
      end;
    end;
  end;
end;

procedure TABcxGridPopupMenu.PopupMenu_Other_EditHeaderCaption(Sender: TObject);
var
  i:longint;
  tempForm:TABMemoForm;
begin
  tempForm := TABMemoForm.Create(nil);
  try
    tempForm.Caption:=('修改标题栏');
    tempForm.Memo1.Clear;
    for i := 0 to FLinkTableView.ColumnCount-1 do
    begin
      tempForm.Memo1.Lines.Add(FLinkTableView.Columns[i].Caption);
    end;

    if tempForm.ShowModal=mrok then
    begin
      for i := 0 to FLinkTableView.ColumnCount-1 do
      begin
        if (tempForm.Memo1.Lines[i]<>EmptyStr) and
           (AnsiCompareText(FLinkTableView.Columns[i].Caption,tempForm.Memo1.Lines[i])<>0) then
          FLinkTableView.Columns[i].Caption:=tempForm.Memo1.Lines[i];
      end;
    end;
  finally
    tempForm.Free;
  end;
end;

procedure TABcxGridPopupMenu.PopupMenu_Other_FocusCellOnTab(Sender: TObject);
begin
  FLinkTableView.OptionsBehavior.FocusCellOnTab:=TMenuItem(Sender).Checked;
end;

procedure TABcxGridPopupMenu.PopupMenu_Other_FullCollapse(Sender: TObject);
begin
  if (Assigned(FLinkTableView.DataController.Groups)) then
  begin
    //FLinkTableView.ViewData.Expand(False);
    if (FLinkTableView is TcxGridDBBandedTableView) then
      TcxGridDBBandedTableView(FLinkTableView).DataController.Groups.FullCollapse
    else if (FLinkTableView is TcxGridDBTableView) then
      TcxGriddbTableView(FLinkTableView).DataController.Groups.FullCollapse;
  end;
end;

procedure TABcxGridPopupMenu.PopupMenu_Other_FullExpand(Sender: TObject);
begin
  if (Assigned(FLinkTableView.DataController.Groups)) then
  begin
    //FLinkTableView.ViewData.Expand(true);

    if (FLinkTableView is TcxGridDBBandedTableView) then
      TcxGridDBBandedTableView(FLinkTableView).DataController.Groups.FullExpand
    else if (FLinkTableView is TcxGridDBTableView) then
      TcxGriddbTableView(FLinkTableView).DataController.Groups.FullExpand;

  end;
end;

procedure TABcxGridPopupMenu.PopupMenu_Other_GoToNextCellOnEnter(
  Sender: TObject);
begin
  FLinkTableView.OptionsBehavior.GoToNextCellOnEnter:=TMenuItem(Sender).Checked;
end;

procedure TABcxGridPopupMenu.PopupMenu_Other_ShowAggBar(Sender: TObject);
begin
  FLinkTableView.OptionsView.Footer:=TMenuItem(Sender).Checked;
  if FLinkTableView.OptionsView.Footer then
    FLinkTableView.OptionsView.GroupFooters:=gfVisibleWhenExpanded
  else
    FLinkTableView.OptionsView.GroupFooters:=gfInvisible;
end;

procedure TABcxGridPopupMenu.PopupMenu_Other_ShowHeader(Sender: TObject);
begin
  FLinkTableView.OptionsView.Header:=TMenuItem(Sender).Checked;
end;

procedure TABcxGridPopupMenu.PopupMenu_Other_ShowHintCloum(Sender: TObject);
begin
  FLinkTableView.Controller.Customization:=not FLinkTableView.Controller.Customization;
end;

procedure TABcxGridPopupMenu.PopupMenu_Other_SumNumColumn(Sender: TObject);
var
  i:LongInt;
  tempStr1:string;
begin
  for I := 0 to FLinkTableView.ColumnCount - 1 do
  begin
    FLinkTableView.Columns[i].Focused:=true;
    tempStr1:= FLinkTableView.Controller.FocusedColumn.DataBinding.ValueType;
    if  (ABPos(','+tempStr1+',',','+'Smallint,Integer,Float,Currency,BCD,Word,Largeint,FMTBcd,'+',')>0)
           then
    begin
      SetColumnFoot(skSum)
    end;
  end;
end;

procedure TABcxGridPopupMenu.PopupMenu_Out_Excel(Sender: TObject);
begin
  OutFile(otExcel);
end;

procedure TABcxGridPopupMenu.PopupMenu_Out_Html(Sender: TObject);
begin
  OutFile(otHtml);
end;

procedure TABcxGridPopupMenu.PopupMenu_Out_Text(Sender: TObject);
begin
  OutFile(otText);
end;

procedure TABcxGridPopupMenu.PopupMenu_Out_Xml(Sender: TObject);
begin
  OutFile(otXML);
end;

procedure TABcxGridPopupMenu.PopupMenu_Print(Sender: TObject);
var
  PrintForm: TABcxGridPopupMenu_PrintForm;
begin
  PrintForm:= TABcxGridPopupMenu_PrintForm.Create(nil);
  try
    PrintForm.SetLink(LinkGrid);
    PrintForm.ShowModal;
  finally
    PrintForm.Free;
  end;
end;

procedure TABcxGridPopupMenu.PopupMenu_RowFilter(Sender: TObject);
begin
  FLinkTableView.FilterRow.Visible:=TMenuItem(Sender).Checked;
end;

procedure TABcxGridPopupMenu.PopupMenu_Setup_Clear(Sender: TObject);
begin
  OpreatorFile(True,False,False,false);
  LoadSetupFile;
end;

procedure TABcxGridPopupMenu.PopupMenu_Setup_Save(Sender: TObject);
begin
  if (Assigned(FLinkTableView)) then
  begin
    Save;
  end;
end;

procedure TABcxGridPopupMenu.PopupMenu_Setup_ToDatabase(Sender: TObject);
begin
  OpreatorFile(False,True,False,false);
end;

procedure TABcxGridPopupMenu.PopupMenu_Setup_SaveAndToDatabase(Sender: TObject);
begin
  Save;
  OpreatorFile(False,True,False,false);
end;

procedure TABcxGridPopupMenu.PopupMenu_Setup_DoItems_DO(Sender: TObject);
var
  tempMenuItem,
  tempMenuItem1:TMenuItem;
begin
  tempMenuItem:=ABGetMenuItem(FMenuItemsList,'PopupMenu_Setup_DoItems_BestColumWidth');
  if (Assigned(tempMenuItem)) and (tempMenuItem.Checked) then
  begin
    PopupMenu_BestColumWidth(ABGetMenuItem(FMenuItemsList,'PopupMenu_BestColumWidth'));
  end;
  tempMenuItem:=ABGetMenuItem(FMenuItemsList,'PopupMenu_Setup_DoItems_AutoColumWidth');
  if (Assigned(tempMenuItem)) and (tempMenuItem.Checked) then
  begin
    tempMenuItem1:=ABGetMenuItem(FMenuItemsList,'PopupMenu_AutoColumWidth');
    if (Assigned(tempMenuItem1)) then
    begin
      tempMenuItem1.Checked:=true;
      PopupMenu_AutoColumWidth(tempMenuItem1);
    end;
  end;
  tempMenuItem:=ABGetMenuItem(FMenuItemsList,'PopupMenu_Setup_DoItems_SumNumColumn');
  if (Assigned(tempMenuItem)) and (tempMenuItem.Checked) then
  begin
    PopupMenu_Other_SumNumColumn(ABGetMenuItem(FMenuItemsList,'PopupMenu_Other_SumNumColumn'));
  end;
  tempMenuItem:=ABGetMenuItem(FMenuItemsList,'PopupMenu_Setup_DoItems_Save');
  if (Assigned(tempMenuItem)) and (tempMenuItem.Checked) then
  begin
    PopupMenu_Setup_Save(ABGetMenuItem(FMenuItemsList,'PopupMenu_Setup_Save'));
  end;
  tempMenuItem:=ABGetMenuItem(FMenuItemsList,'PopupMenu_Setup_DoItems_ToDatabase');
  if (Assigned(tempMenuItem)) and (tempMenuItem.Checked) then
  begin
    PopupMenu_Setup_ToDatabase(ABGetMenuItem(FMenuItemsList,'PopupMenu_Setup_ToDatabase'));
  end;
end;

procedure TABcxGridPopupMenu.PopupMenu_Setup_Default(Sender: TObject);
begin
  OpreatorFile(False,False,True,false);
  LoadSetupFile;
end;

procedure TABcxGridPopupMenu.PopupMenu_ShowGroup(Sender: TObject);
begin
  FLinkTableView.OptionsView.GroupByBox:=TMenuItem(Sender).Checked;
end;


{ TABcxPivotGridPopupMenu }

constructor TABcxPivotGridPopupMenu.Create(AOwner: TComponent);
begin
  inherited;
  AutoHotkeys:=maManual;

  FMenuItemsList:=TStringList.Create;
  FConnName:='Main';
  FIsLoadViewProperty:=false;

  if (AOwner is TcxDBPivotGrid) then
    LinkcxDBPivotGrid:=TcxDBPivotGrid(AOwner);
end;

destructor TABcxPivotGridPopupMenu.Destroy;
begin
  FMenuItemsList.free;

  inherited;
end;

procedure TABcxPivotGridPopupMenu.DestroyPopupMenu;
var
  i:longint;
begin
  for i := Items.Count-1 downto 0  do
  begin
    Items[0].Clear;
  end;
end;

procedure TABcxPivotGridPopupMenu.DoPopup(Sender: TObject);
begin
  inherited;
  CreatePopupMenu;
end;

function TABcxPivotGridPopupMenu.GetFilename(aTypeFlag: string): string;
begin
  if (not Assigned(FLinkcxDBPivotGrid)) then exit;

  if FSetupFullFileName<>EmptyStr then
  begin
    Result:=FSetupFullFileName+aTypeFlag;
  end
  else
  begin
    Result:=ABConfigPath+FLinkcxDBPivotGrid.Owner.Name+'_'+FLinkcxDBPivotGrid.Name+FSetupFileNameSuffix +aTypeFlag;
  end;
end;

procedure TABcxPivotGridPopupMenu.Load;
begin
  LoadViewProperty;
end;

procedure TABcxPivotGridPopupMenu.LoadViewProperty;
var
  tempStr1:string;
begin
  if (not Assigned(FLinkcxDBPivotGrid)) then exit;

  tempStr1:=GetFilename('_Property.ini');
  if ABCheckFileExists(tempStr1) then
  begin
    if  (FLinkcxDBPivotGrid.FieldCount<=0) then
      FLinkcxDBPivotGrid.CreateAllFields;
    FLinkcxDBPivotGrid.BeginUpdate;
    try
      FLinkcxDBPivotGrid.RestoreFromIniFile(tempStr1);
      PopupMenu_admin_CloseDesign(nil);
    finally
      FLinkcxDBPivotGrid.EndUpdate;
    end;
  end
  else
  begin
    if  (FLinkcxDBPivotGrid.FieldCount<=0) then
      FLinkcxDBPivotGrid.CreateAllFields;
  end;
  FIsLoadViewProperty    :=true;
end;

procedure TABcxPivotGridPopupMenu.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited;
  if (Operation = opRemove) then
  begin
    if (FLinkcxDBPivotGrid =AComponent)  then
      FLinkcxDBPivotGrid := nil;
  end;
end;

procedure TABcxPivotGridPopupMenu.OutFile(aOutFileType:TABOutFileType);
var
  temSavedialog1:TSaveDialog;
begin
  if (not Assigned(FLinkcxDBPivotGrid)) then exit;

  temSavedialog1:=TSaveDialog.Create(self);
  try
    temSavedialog1.FileName:=TCustomForm(owner).name+'_'+FLinkcxDBPivotGrid.name+'_'+ABDateToStr(Now);
    if aOutFileType =otExcel then
      temSavedialog1.Filter:='*.xls|*.xls'
    else if aOutFileType =otXML then
      temSavedialog1.Filter:='*.xml|*.xml'
    else if aOutFileType =otText then
      temSavedialog1.Filter:='*.txt|*.txt'
    else if aOutFileType =otHtml then
      temSavedialog1.Filter:='*.Html|*.Html';

    if temSavedialog1.Execute then
    begin
      if aOutFileType =otExcel then
        cxExportPivotGridToExcel(temSavedialog1.FileName,FLinkcxDBPivotGrid)
      else if aOutFileType =otXML then
        cxExportPivotGridToXML(temSavedialog1.FileName,FLinkcxDBPivotGrid)
      else if aOutFileType =otText then
        cxExportPivotGridToText(temSavedialog1.FileName,FLinkcxDBPivotGrid)
      else if aOutFileType =otHtml then
        cxExportPivotGridToHTML(temSavedialog1.FileName,FLinkcxDBPivotGrid);

      FLinkcxDBPivotGrid.DataController.Groups.FullCollapse
    end;
  finally
    temSavedialog1.free;
  end;
end;

procedure TABcxPivotGridPopupMenu.Save;
begin
  SaveViewProperty;
end;

procedure TABcxPivotGridPopupMenu.SaveViewProperty;
var
  tempStr1:string;
begin
  if (not Assigned(FLinkcxDBPivotGrid)) then exit;

  tempStr1:=GetFilename('_Property.ini');
  if ABCheckFileExists(tempStr1) then
  begin
    DeleteFile(tempStr1);
  end;
  FLinkcxDBPivotGrid.StoreToIniFile(tempStr1,True);
end;

procedure TABcxPivotGridPopupMenu.SetLinkcxDBPivotGrid(
  const Value: TcxDBPivotGrid);
begin
  FLinkcxDBPivotGrid := Value;
  if Assigned(FLinkcxDBPivotGrid) then
    FLinkcxDBPivotGrid.PopupMenu:=self;
end;

procedure TABcxPivotGridPopupMenu.ChangeFileNameBySetupFileNameSuffix(aOldFileStr,aNewFileStr:string);
var
  I:LongInt;
  tempOldFileName,
  tempNewFileName:string;
begin
  for I := Low(FSaveFileNames) to high(FSaveFileNames) do
  begin
    tempOldFileName:=ABConfigPath+aOldFileStr +FSaveFileNames[i];
    if ABCheckFileExists(tempOldFileName) then
    begin
      tempNewFileName:=ABConfigPath+aNewFileStr +FSaveFileNames[i];
      RenameFile(tempOldFileName,tempNewFileName);
    end;
  end;
end;

procedure TABcxPivotGridPopupMenu.OpreatorFile(aClear:boolean;aUpToDatabase:Boolean;aDownFromDatabase:boolean;
                                          aSetToTempPath:boolean;aTempPathFileNameStrings:TStrings);
var
  tempFileName,tempOnlyFileName:string;
  i:LongInt;
  procedure DoItem;
  begin
    if aClear then
    begin
      if ABCheckFileExists(tempFileName) then
      begin
        DeleteFile(tempFileName);
      end;
    end
    else if aUpToDatabase then
    begin
      if (ABCheckFileExists(tempFileName)) and (FConnName<>emptystr) then
      begin
        ABExecSQL(FConnName,
                        'exec Proc_SetPublicPkgNewVersion  '+QuotedStr(tempOnlyFileName)+',''ABSoft_Set\ABClientP\Config'' ' );
        ABUpFileToField(FConnName,'ABSys_Org_Function','FU_Pkg',
                         ' where FU_FileName='+QuotedStr(tempOnlyFileName),
                         tempFileName);

        if not ABCheckFileExists(ABConfigPath+tempOnlyFileName) then
        begin
          ABCopyFile(tempFileName,ABConfigPath+tempOnlyFileName);
        end;
      end;
    end
    else if aDownFromDatabase then
    begin
      if (FConnName<>emptystr)  then
        ABDownToFileFromField(FConnName,'ABSys_Org_Function','FU_Pkg',
                      ' where FU_FileName='+QuotedStr(tempOnlyFileName),
                      tempFileName);
    end
    else if aSetToTempPath then
    begin
      if ABCheckFileExists(tempFileName) then
      begin
        ABCopyFile(tempFileName,ABTempPath+tempOnlyFileName);
        aTempPathFileNameStrings.add(ABTempPath+tempOnlyFileName);
      end;
    end;
  end;
begin
  if (Assigned(FLinkcxDBPivotGrid))  then
  begin
    for I := Low(FSaveFileNames) to high(FSaveFileNames) do
    begin
      if AnsiCompareText(FSaveFileNames[i],'_CaleColumnSQL.ini')<>0 then
      begin
        tempFileName:=GetFilename(FSaveFileNames[i]);
        tempOnlyFileName:=ABGetFilename(tempFileName,false);

        DoItem;
      end;
    end;
  end;
end;


procedure TABcxPivotGridPopupMenu.CreatePopupMenu;
begin
  if (Assigned(items)) and (items.Count<=0)  then
  begin
    ABCreateMenuItem(self,Items,'PopupMenu_Out'        ,'数据输出');
    ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Out_Excel'   ,'Excel'    ,PopupMenu_Out_Excel);
    ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Out_XML'     ,'XML'      ,PopupMenu_Out_XML  );
    ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Out_Text'    ,'Text'     ,PopupMenu_Out_Text );
    ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Out_Html'    ,'Html'     ,PopupMenu_Out_Html );

    if ABPubUser.IsAdmin then
    begin
      ABCreateMenuItem(self,Items,'','-');
      ABCreateMenuItem(self,Items,'PopupMenu_Admin' ,'管理员配制');
      ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_admin_OpenDesign'    ,'打开设计',PopupMenu_admin_OpenDesign);
      ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_admin_CloseDesign'  ,'关闭设计',PopupMenu_admin_CloseDesign);
    end;
    ABCreateMenuItem(self,Items,'','-');
    ABCreateMenuItem(self,Items,'PopupMenu_Setup' ,'配制文件');
    ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Setup_Save'           ,'保存配置'    ,PopupMenu_Setup_Save);
    if ABPubUser.IsAdminOrSysuser then
    begin
      ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Setup_ToDatabase'     ,'上传服务器'  ,PopupMenu_Setup_ToDatabase);
      ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Setup_SaveAndToDatabase'     ,'保存后上传服务器',PopupMenu_Setup_SaveAndToDatabase);
      ABCreateMenuItem(self,Items[Items.Count-1],'','-');
      ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Setup_Clear','清除所有配制',PopupMenu_Setup_Clear);
    end;
    ABCreateMenuItem(self,Items[Items.Count-1],'PopupMenu_Down'      ,'恢复默认配制',PopupMenu_Setup_Default);

    ABGetMenuItems(self.Items,FMenuItemsList);
  end;
end;

procedure TABcxPivotGridPopupMenu.PopupMenu_Setup_ToDatabase(Sender: TObject);
begin
  OpreatorFile(False,True,False,false);
end;

procedure TABcxPivotGridPopupMenu.PopupMenu_Setup_SaveAndToDatabase(Sender: TObject);
begin
  PopupMenu_admin_CloseDesign(nil);
  Save;
  OpreatorFile(False,True,False,false);
end;

procedure TABcxPivotGridPopupMenu.PopupMenu_Setup_Default(Sender: TObject);
begin
  OpreatorFile(False,False,True,false);
  Load;
end;

procedure TABcxPivotGridPopupMenu.PopupMenu_Setup_Clear(Sender: TObject);
begin
  OpreatorFile(True,false,False,false);
  FLinkcxDBPivotGrid.DeleteAllFields;
  Load;
end;

procedure TABcxPivotGridPopupMenu.PopupMenu_Setup_Save(Sender: TObject);
begin
  PopupMenu_admin_CloseDesign(nil);
  Save;
end;

procedure TABcxPivotGridPopupMenu.PopupMenu_Out_Excel(Sender: TObject);
begin
  OutFile(otExcel);
end;

procedure TABcxPivotGridPopupMenu.PopupMenu_Out_Html(Sender: TObject);
begin
  OutFile(otHtml);
end;

procedure TABcxPivotGridPopupMenu.PopupMenu_Out_Text(Sender: TObject);
begin
  OutFile(otText);
end;

procedure TABcxPivotGridPopupMenu.PopupMenu_Out_Xml(Sender: TObject);
begin
  OutFile(otXML);
end;

procedure TABcxPivotGridPopupMenu.PopupMenu_admin_CloseDesign(Sender: TObject);
begin
  if (not Assigned(FLinkcxDBPivotGrid)) then exit;
  FLinkcxDBPivotGrid.OptionsView.ColumnFields:=false;
  FLinkcxDBPivotGrid.OptionsView.DataFields:=false;
  FLinkcxDBPivotGrid.OptionsView.FilterFields:=false;
  FLinkcxDBPivotGrid.OptionsView.RowFields:=True;
end;

procedure TABcxPivotGridPopupMenu.PopupMenu_admin_OpenDesign(Sender: TObject);
begin
  if (not Assigned(FLinkcxDBPivotGrid)) then exit;
  FLinkcxDBPivotGrid.OptionsView.ColumnFields:=True;
  FLinkcxDBPivotGrid.OptionsView.DataFields:=True;
  FLinkcxDBPivotGrid.OptionsView.FilterFields:=True;
  FLinkcxDBPivotGrid.OptionsView.RowFields:=True;
end;


procedure ABFinalization;
begin
  FLoadViewOtherSetStrings1.Free;
  FLoadViewOtherSetStrings2.Free;
end;

procedure ABInitialization;
begin
  FLoadViewOtherSetStrings1 := TStringList.Create;
  FLoadViewOtherSetStrings2 := TStringList.Create;
end;

Initialization
  ABInitialization;

Finalization
  ABFinalization;


end.





