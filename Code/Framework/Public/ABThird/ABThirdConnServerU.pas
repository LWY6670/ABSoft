{
服务器连接单元
}

unit ABThirdConnServerU;

interface
{$I ..\ABInclude.ini}

uses
  ABPubFormU,
  ABPubVarU,
  ABPubConstU,
  ABPubServiceU,
  ABPubPassU,
  ABPubDBU,
  ABPubFuncU,
  ABPubMessageU,
  ABPubThreadU,
  ABPubInPutPassWordU,
  ABPubLocalParamsU,

  ABThirdDBU,
  ABThirdFormU,

  ABThirdServerMethodsU,

  typinfo,
  FireDAC.UI.Intf, FireDAC.VCLUI.Wait, FireDAC.Comp.UI,
  FireDAC.Stan.Def,
  FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  FireDAC.Phys.MSSQLDef, FireDAC.Phys, FireDAC.Phys.ODBCBase, FireDAC.Phys.MSSQL,
  FireDAC.Stan.StorageBin,
  FireDAC.Stan.StorageXML, FireDAC.Stan.StorageJSON,

  DBXJSON,JSON,
  forms,Windows,SysUtils,DB,ExtCtrls,Controls,
  StdCtrls,Buttons,Classes, Data.DBXDataSnap, IPPeerClient, Data.DBXCommon,
  Datasnap.DBClient, Datasnap.DSConnect, Data.SqlExpr, FireDAC.Stan.Pool,
  Vcl.Menus;



type
  TABConnServerForm = class(TABThirdForm)
    Label1: TLabel;
    Label2: TLabel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Edit1: TEdit;
    ListBox1: TListBox;
    Timer1: TTimer;
    SQLConnection1: TSQLConnection;
    DSProviderConnection1: TDSProviderConnection;
    FDStanStorageJSONLink1: TFDStanStorageJSONLink;
    FDStanStorageXMLLink1: TFDStanStorageXMLLink;
    FDStanStorageBinLink1: TFDStanStorageBinLink;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure N1Click(Sender: TObject);
  private
    FOkClose: Boolean;
    function CheckConn(aShowMsg: boolean): boolean;
    { Private declarations }
  public
    property OkClose:Boolean read FOkClose  write FOkClose;
    { Public declarations }
  end;

{
//回调函数
procedure TMainForm.Button8Click(Sender: TObject);
var
  MyCallback: TCallbackParam;
begin
  MainForm.ProgressBar1.Max:=5;
  MyCallback := TCallbackParam.Create;
  ClientModule1.ABServerClient.LongTimeRunFunc(MyCallback);
  MainForm.ProgressBar1.Position:=0;
end;
}
  TCallbackParam = class(TDBXCallback)
    function Execute(const Arg: TJSONValue): TJSONValue; override;
  end;

//设置连接
//aShow=True表示显示连接框
function ABCreateConns_inside(aShow:boolean):Boolean;

//得到服务器的函数列表类
function ABGetServerClient:TABServerClient;
//服务器流转到内存流
function ABGetServerStreamToMemoryStream(aStream: TStream):TMemoryStream;

implementation
{$R *.dfm}
var
  FSQLConnection1: TSQLConnection;
  FDSProviderConnection1: TDSProviderConnection;
  FServerClient: TABServerClient;

  FConnStrings:TStrings;
  FFireDACConnection: TFireDACConnection;

procedure ABFreeConns;
begin
  SetLength(ABConns,0);
end;

procedure SetConnectionParams(aSQLConnection: TSQLConnection;aHostName,aPort,aUserName,aPassword:string);
begin
  aSQLConnection.Params.Values['HostName'] :=aHostName;
  aSQLConnection.Params.Values['Port']     :=aPort;

  aSQLConnection.Params.Values['DSAuthenticationUser']     :=aUserName;
  aSQLConnection.Params.Values['DSAuthenticationPassword'] :=aPassword;
end;

function ABCreateConns_inside(aShow:boolean):Boolean;
var
  tempForm: TABConnServerForm;
  tempNotSetupFile:boolean;
  //加载数据库连接
  function LoadConnLists:boolean;
    function LoadConnList(aConnName,aHost,aDatabase,aUserName,aPassWord:string;aManual,aIsMain:boolean;aDatabaseType:TABDatabaseType):boolean;
    var
      i:longint;
    begin
      SetLength(ABConns,high(ABConns)+1+1);
      i:=high(ABConns);
      ABConns[i].Conninfo.ConnName     :=aConnName;
      ABConns[i].Conninfo.Host         :=aHost;
      ABConns[i].Conninfo.Database     :=aDatabase;
      ABConns[i].Conninfo.IsMain       :=aIsMain;
      ABConns[i].Conninfo.Manual       :=aManual;
      ABConns[i].Conninfo.UserName     :=aUserName;
      ABConns[i].Conninfo.PassWord     :=aPassWord;
      ABConns[i].Conninfo.DatabaseType :=dtSQLServer;
      ABConns[i].Conn                  :=FFireDACConnection;
      result:=true;
    end;
  var
    i:LongInt;
    tempStrings:TStrings;
    tempMasterKeyPass:string;
  begin
    result:=True;
    ABFreeConns;

    tempMasterKeyPass:=ABDOPassWord('MasterKey');
    FDSProviderConnection1.Connected:=false;
    FSQLConnection1.Connected:=false;
    SetConnectionParams(FSQLConnection1,
                        ABGetLeftRightStr(tempForm.Edit1.Text,axdLeft,':'),
                        ABGetLeftRightStr(tempForm.Edit1.Text,axdright,':',''),
                        ABReadInI('DSServer','DSAuthenticationUser',ABConnFileName,'admin','admin'),
                        ABReadInI('DSServer','DSAuthenticationPassword',ABConnFileName,tempMasterKeyPass,tempMasterKeyPass)
                        );
    try
      FSQLConnection1.Connected:=true;
      FDSProviderConnection1.Connected:=true;
      ABFreeAndNil(FServerClient);
      FServerClient := TABServerClient.Create(FSQLConnection1.DBXConnection);

      if (FDSProviderConnection1.Connected) then
      begin
        tempStrings:=TStringList.Create;
        try
          tempStrings.Text:=FServerClient.GetConnStrings;
          for I := 0 to tempStrings.Count-1 do
          begin
            LoadConnList(
                        ABGetItemValue(tempStrings[i],'[ConnName]','','['),
                        ABGetItemValue(tempStrings[i],'[Host]','','['),
                        ABGetItemValue(tempStrings[i],'[Database]','','['),
                        ABGetItemValue(tempStrings[i],'[UserName]','','['),
                        ABGetItemValue(tempStrings[i],'[Password]','','['),

                        abstrtobool(ABGetItemValue(tempStrings[i],'[Manual]','','[')),
                        abstrtobool(ABGetItemValue(tempStrings[i],'[IsMain]','','[')),
                        TABDatabaseType(GetEnumValue(TypeInfo(TABDatabaseType),ABGetItemValue(tempStrings[i],'[DatabaseType]','','[')))
                       );
          end;
        finally
          tempStrings.Free;
        end;
      end;
    except
      raise;
    end;
  end;
begin
  tempNotSetupFile:=(not ABCheckFileExists(ABConnFileName));
  Application.CreateForm(TABConnServerForm,tempForm);
  tempForm.CloseAction:=caHide;
  try
    if (tempNotSetupFile) or (aShow) then
    begin
      result:=(tempForm.ShowModal=mrOk);
    end
    else
    begin
      result:=tempForm.CheckConn(false);
      if not result then
      begin
        if (ABInDelphi) or
           (ABInputPassWord('服务器连接失败,请输入密码设置连接')) then
        begin
          result:=(tempForm.ShowModal=mrOk);
        end;
      end;
    end;

    if result then
      result:=LoadConnLists;
  finally
    tempForm.Free;
    tempForm:=nil;
  end;
end;


{ TCallbackParam }

function TCallbackParam.Execute(const Arg: TJSONValue): TJSONValue;
var
  Data: TJSONValue;
begin
  //得到服务器传过来的值TJSONNumber(Arg).AsInt;

  Data := TJSONValue(Arg.Clone);
  Result := Data
end;

function ABGetServerStreamToMemoryStream(aStream: TStream):TMemoryStream;
const
  BufSize = $F000;
var
  Buffer: TBytes;
  ReadCount: Integer;
begin
  result:=TMemoryStream.Create;
  aStream.Position:=0;
  result.Position:=0;
  //DataSnap默认的缓存大小是32k 所以如果流的大小超过这个大小就会被自动分成多个包
  //当传递大量数据时获取到的大小是-1，所以如果还是按照一般的方法来读取流的数据就会有问题了
  //大小未知则一直读取到没有数据为止
  if aStream.Size = -1 then
  begin
    SetLength(Buffer, BufSize);
    repeat
      ReadCount := aStream.Read(Buffer[0], BufSize);
        if ReadCount > 0 then
          result.WriteBuffer(Buffer[0], ReadCount);
            if ReadCount < BufSize then
              break;
    until ReadCount < BufSize;
  end
  //大小已知则直接复制数据
  else
  begin
    result.CopyFrom(aStream, 0);
  end;
  result.Position:=0;
end;

function ABGetServerClient:TABServerClient;
begin
  Result:=FServerClient;
end;


function TABConnServerForm.CheckConn(aShowMsg: boolean): boolean;
begin
  result:=false;
  if Edit1.Text<>EmptyStr then
  begin
    DSProviderConnection1.Connected:=false;
    SQLConnection1.Connected:=false;
    DSProviderConnection1.ServerClassName:=ABReadInI('DSServer','Name',ABConnFileName,'DSServer1','DSServer1');
    SQLConnection1.DriverName:='DataSnap';
    SQLConnection1.LoginPrompt:= False ;
    SetConnectionParams(SQLConnection1,
                        ABGetLeftRightStr(Edit1.Text,axdLeft,':'),
                        ABGetLeftRightStr(Edit1.Text,axdright,':',''),
                        'test',
                        '');
    try
      SQLConnection1.Connected:=true;
      DSProviderConnection1.Connected:=true;
      result:=True;

      if ListBox1.Items.IndexOf(Edit1.Text)<0 then
        ListBox1.Items.Add(Edit1.Text);

      if aShowMsg then
        ABShow('连接成功.');

      DSProviderConnection1.Connected:=false;
      SQLConnection1.Connected:=false;
    except
      ABShow('连接失败.');
    end;
  end;
end;

procedure TABConnServerForm.N1Click(Sender: TObject);
begin
  ABSetLocalParamsValue('LoginType','ltCS_Two');
  Close;
end;

procedure TABConnServerForm.BitBtn1Click(Sender: TObject);
begin
  CheckConn(True);
end;

procedure TABConnServerForm.BitBtn2Click(Sender: TObject);
begin
  if CheckConn(False) then
  begin
    if FOkClose then
      close
    else
      ModalResult:=mrOk;
  end;
end;

procedure ABFinalization;
begin
  ABFreeConns;
  FFireDACConnection.Free;

  ABFreeAndNil(FServerClient);
  FSQLConnection1.Free;
  FDSProviderConnection1.Free;
  FConnStrings.Free;
end;

procedure ABInitialization;
begin
  FFireDACConnection:=TFireDACConnection.Create(nil);
  FFireDACConnection.DriverName:='MSSQL';

  FServerClient:=nil;
  FConnStrings:=TStringList.Create;

  FSQLConnection1:= TSQLConnection.Create(nil);
  FSQLConnection1.DriverName:='DataSnap';
  FSQLConnection1.LoginPrompt:= False ;

  FDSProviderConnection1:= TDSProviderConnection.Create(nil);
  FDSProviderConnection1.ServerClassName:=ABReadInI('DSServer','Name',ABConnFileName,'DSServer1','DSServer1');
  FDSProviderConnection1.SQLConnection:=FSQLConnection1;
end;

Initialization
  ABInitialization;
Finalization
  ABFinalization;


end.


