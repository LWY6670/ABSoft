object ABConnServerForm: TABConnServerForm
  Left = 438
  Top = 273
  BorderIcons = [biSystemMenu]
  Caption = #26381#21153#22120#36830#25509#31649#29702
  ClientHeight = 233
  ClientWidth = 566
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PopupMenu = PopupMenu1
  Position = poDesktopCenter
  DesignSize = (
    566
    233)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 10
    Top = 13
    Width = 103
    Height = 13
    AutoSize = False
    Caption = #24403#21069#26381#21153#22120#36830#25509':'
    Transparent = True
  end
  object Label2: TLabel
    Left = 10
    Top = 37
    Width = 103
    Height = 132
    AutoSize = False
    Caption = #21487#29992#26381#21153#22120#36830#25509':'
    Transparent = True
    Layout = tlCenter
  end
  object BitBtn1: TBitBtn
    Left = 383
    Top = 187
    Width = 75
    Height = 25
    Anchors = [akTop, akRight]
    Caption = #27979#35797
    Default = True
    TabOrder = 0
    OnClick = BitBtn1Click
  end
  object BitBtn2: TBitBtn
    Left = 464
    Top = 187
    Width = 75
    Height = 25
    Anchors = [akTop, akRight]
    Caption = #30830#35748
    Default = True
    Glyph.Data = {
      DE010000424DDE01000000000000760000002800000024000000120000000100
      0400000000006801000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333333333333333330000333333333333333333333333F33333333333
      00003333344333333333333333388F3333333333000033334224333333333333
      338338F3333333330000333422224333333333333833338F3333333300003342
      222224333333333383333338F3333333000034222A22224333333338F338F333
      8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
      33333338F83338F338F33333000033A33333A222433333338333338F338F3333
      0000333333333A222433333333333338F338F33300003333333333A222433333
      333333338F338F33000033333333333A222433333333333338F338F300003333
      33333333A222433333333333338F338F00003333333333333A22433333333333
      3338F38F000033333333333333A223333333333333338F830000333333333333
      333A333333333333333338330000333333333333333333333333333333333333
      0000}
    NumGlyphs = 2
    TabOrder = 1
    OnClick = BitBtn2Click
  end
  object Edit1: TEdit
    Tag = 91
    Left = 113
    Top = 10
    Width = 430
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 2
    Text = 'localhost:211'
  end
  object ListBox1: TListBox
    Tag = 910
    Left = 113
    Top = 37
    Width = 430
    Height = 132
    Anchors = [akLeft, akTop, akRight]
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    ItemHeight = 14
    ParentFont = False
    TabOrder = 3
  end
  object Timer1: TTimer
    Enabled = False
    Left = 72
    Top = 160
  end
  object SQLConnection1: TSQLConnection
    DriverName = 'DataSnap'
    LoginPrompt = False
    Params.Strings = (
      'DriverUnit=Data.DBXDataSnap'
      'HostName=localhost'
      'Port=211'
      'CommunicationProtocol=tcp/ip'
      'DatasnapContext=datasnap/'
      
        'DriverAssemblyLoader=Borland.Data.TDBXClientDriverLoader,Borland' +
        '.Data.DbxClientDriver,Version=21.0.0.0,Culture=neutral,PublicKey' +
        'Token=91d62ebb5b0d1b1b'
      'Filters={}')
    Left = 264
    Top = 176
    UniqueId = '{1CE795BA-8342-42F7-B0BC-E5FAF5C6C606}'
  end
  object DSProviderConnection1: TDSProviderConnection
    ServerClassName = 'DSServer1'
    SQLConnection = SQLConnection1
    Left = 144
    Top = 176
  end
  object FDStanStorageJSONLink1: TFDStanStorageJSONLink
    Left = 296
    Top = 104
  end
  object FDStanStorageXMLLink1: TFDStanStorageXMLLink
    Left = 416
    Top = 64
  end
  object FDStanStorageBinLink1: TFDStanStorageBinLink
    Left = 296
    Top = 32
  end
  object PopupMenu1: TPopupMenu
    Left = 128
    Top = 8
    object N1: TMenuItem
      Caption = #37325#21551#21160#20999#25442#21040#20004#23618
      OnClick = N1Click
    end
  end
end
