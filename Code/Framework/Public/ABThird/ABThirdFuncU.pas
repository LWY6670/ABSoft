{
共用函数单元

  遍历CXGRID选择行的方法有两个
 主从结构要用主TableView的活动TableView做
 FMultiSelectTableView.MasterGridView.ViewData.Rows[MasterGridView.FocusedRowIndex].AsMasterDataRow.ActiveDetailGridView

 //不需要对数据集进行DisableControls;EnableControls;操作,运行过程中当前记录不会移动,用于
 //不需要设置相应字段值的情况
 1. for I := 0 to Sheet1_TableView1.Controller.SelectedRowCount- 1 do
    begin
      Sheet1_TableView1.Controller.SelectedRecords[i].Values[tempCa_AccountIndex];

    end;

 //运行过程中当前记录会移动
 //如不对数据集进行DisableControls;EnableControls;操作,则在移动记录时会花费不少时间,
 //大数据量时DisableControls;EnableControls;会很慢
 2. for I := 0 to Sheet1_TableView1.DataController.GetSelectedCount- 1 do
    begin
      j:= aDataController.GetSelectedRowIndex(i);
      aDataController.DataSet.RecNo:=aDataController.GetRowInfo(j).RecordIndex+1;

    end;

非关联数据的tableView中TextEdit列值修改时事件顺序
procedure TAccessForm.cxGridBandedColumn4PropertiesChange(Sender: TObject);
procedure TAccessForm.cxGridBandedColumn4PropertiesValidate(Sender: TObject;
procedure TAccessForm.cxGridBandedColumn4PropertiesEditValueChanged(
非关联数据的tableView中TCheck列值修改时事件顺序
procedure TAccessForm.cxGridBandedColumn4PropertiesChange(Sender: TObject);
procedure TAccessForm.cxGridBandedColumn4PropertiesEditValueChanged(
procedure TAccessForm.cxGridBandedColumn4PropertiesValidate(Sender: TObject;
}
unit ABThirdFuncU;

interface
{$I ..\ABInclude.ini}

uses
  ABPubFormU,
  ABPubUserU,
  ABPubVarU,
  ABPubManualProgressBar_ThreadU,
  ABPubCharacterCodingU,
  ABPubConstU,
  ABPubFuncU,
  ABPubDBU,
  ABPubLanguageU,
  ABPubScriptU,
  ABPubCheckTreeViewU,
  ABPubPassU,

  ABThirdDBU,
  ABThirdConnU,
  ABThirdCustomQueryU,

  cxEditRepositoryItems,
  cxExtEditRepositoryItems,
  cxTL,
  cxDBTL,
  cxShellComboBox,cxDBExtLookupComboBox,cxHyperLinkEdit,cxBlobEdit,cxButtonEdit,
  cxProgressBar,cxTrackBar,cxImageComboBox,
  cxFontNameComboBox,
  cxLookAndFeelPainters,
  cxLabel,
  cxDropDownEdit,
  cxCheckListBox,
  cxTimeEdit,
  cxCalendar,
  cxCalc,
  cxCheckBox,
  cxSpinEdit,
  cxTextEdit,
  cxGraphics,
  cxGridExportLink,
  cxGrid,
  cxCustomData,
  cxDBData,
  cxMaskEdit,
  cxRichEdit,
  cxRadioGroup,
  cxImage,
  cxMemo,
  dxStatusBar,
  dxBar,
  cxGridDBBandedTableView,
  cxGridDBDataDefinitions,
  cxGridCustomTableView,
  cxGridDBTableView,
  cxGridTableView,
  cxCurrencyEdit,
  cxMRUEdit,
  cxListBox,
  cxColorComboBox,
  cxCheckComboBox,
  cxCheckGroup,
  cxDBLookupComboBox,
  cxGroupBox,
  cxListView,
  cxEdit,
  dxCore,
  dxdbtrel,
  unitResFile,
  unitResourceVersionInfo,
  NativeXml,

  dxNavBar,
  dxNavBarCollns,

  ComCtrls,types,TypInfo,Classes,Dialogs,Variants,Windows,ExtCtrls,Graphics,Messages,
  SysUtils,DB,Controls,StdCtrls,forms;


//得到表格列关联的数据源
function ABGetColumnDataSource(aTableItem: TcxCustomGridTableItem):TDataSource;
//得到表格列关联的字段
function ABGetColumnField(aTableItem: TcxCustomGridTableItem):TField;overload;

//在TreeList中查找字串
//aDownUpType=0从开始查找，aDownUpType=1向下查找，aDownUpType=-1向上查找
function ABSearchIncxTreeList(aTreeList: TcxDBTreeList; aText: string; aDownUpType: LongInt=0):boolean;
//在TableView中查找字串
//aDownUpType=0从开始查找，aDownUpType=1向下查找，aDownUpType=-1向上查找
function ABSearchIncxTableView(aTableView: TcxGridTableView; aText: string; aDownUpType: LongInt=0): boolean;
//得到表格某一序号列关联的字段
function ABGetColumnField(aTableView: TcxCustomGridTableView;aIndex:LongInt):TField;overload;
//得到表格焦点列关联的字段
function ABGetFocusedColumnField(aTableView: TcxCustomGridTableView):TField;
//通过表格列名称得到列
function ABGetColumnByName(aTableView:TcxGridTableView;aName:string):TcxCustomGridTableItem;
//通过表格列标题得到列
function ABGetColumnByCaption(aTableView:TcxGridTableView;aCaption:string):TcxCustomGridTableItem;
//通过表格关联的字段名得到列
function ABGetColumnByFieldName(aTableView:TcxCustomGridTableView;aFieldName:string):TcxCustomGridTableItem;

//通过表格关联的字段名得到列的属性值
function ABGetColumnProperty(aTableView:TcxCustomGridTableView;aFieldName:string;aProperty:string):Variant;
//通过表格关联的字段名设置列的属性值
procedure ABSetColumnProperty(aTableView:TcxCustomGridTableView;aFieldName:string;aProperty:string;aValue:Variant);
//通过表格记录号得到TableItem列的值
function ABGetColumnValue(aItem:TcxCustomGridTableItem;aRecord:LongInt;aQuotedStr:boolean=true):Variant;
//判断表格列是否是计算列
function ABIsCaleColumn(aItem:TcxCustomGridTableItem;aCheckTag:Boolean=true):Boolean;
//判断表格列是否有关联字段
function ABIsLinkField(aItem:TcxCustomGridTableItem):Boolean;


//得到表格的DataController
function ABGetTableViewDataController(aTableView: TcxCustomGridTableView):TcxGridDBDataController;
//得到表格的数据源
function ABGetTableViewDataSource(aTableView: TcxCustomGridTableView):TDataSource;
//取得表格排序的字段名
//aSpaceFlag=各字段间的分隔符
//aAddOrderBY=是否增加OrderBY
//aOrderByStr=OrderBY的标志串
//aDescStr=降序标志串
function ABGetTableViewOrder(aTableView:TcxCustomGridTableView;aSpaceFlag:string=',';aAddOrderBY:boolean=True;aOrderByStr:string='order by';aDescStr:string='Desc'):string;
//设置表格排序
procedure ABSetTableViewOrder(aTableView:TcxCustomGridTableView;aOrderFieldNames:array of string;aAscOrDescs:array of TcxDataSortOrder);

//删除表格中非计算列且非字段关联列
procedure ABDelNotCaleAndNotLinkFieldColumns(aView:TcxGridTableView);
//用表格记录号中的列值替换aCommText中的列标题
function ABReplaceColumnCaption(aCommText:string;
                                aTableView: TcxGridTableView;
                                aRecordIndex:LongInt;
                                aQuotedStr:boolean=true):string;


//根据表格中列关联字段的类型设置列的PropertiesClass类别
procedure ABSetColumnPropertiesClass(aView:TcxGridTableView);
//创建表格中关联字段的列
procedure ABCreateColumns(aView:TcxGridTableView;aFieldNames:string='');

//根据记录条数设置表格的记录序号的宽度
procedure ABSetAutoIDWidth(aTableView: TcxCustomGridTableView);
//根据记录条数设置表格的记录序号的值
procedure ABSetAutoIDValue(var thisViewInfo: TcxCustomGridIndicatorItemViewInfo; var thisCanvas:TcxCanvas; var thisDone: Boolean);

//批量修改表格中选择记录字段的值
//aMultiSelectType=选择方式
//aFieldNames=设置的字段名数组
//aValuesOrFieldNamesOrFieldNameOperations=值数组,可以是固定的值或需取值字段名，也可以是取值字段名的+-*/连结的四则运算表达式(此时表达式中字段名格式要是[:字段名])
//aValuesIsCale=值数组是否是四则运算表达式
procedure ABSetFieldValue_MultiSelect(
                          aDataController:TcxCustomDataController;
                          aMultiSelectType:TABMultiSelectType;

                          aFieldNames:array of string;
                          aValuesOrFieldNamesOrFieldNameOperations:array of Variant;
                          aValuesIsCale:array of Boolean;
                          aFastMode:boolean=False;
                          aKeyFieldName:string='';
                          aAddSQL:string=''
                           );overload;
procedure ABSetFieldValue_MultiSelect(
                          aDataController:TcxCustomDataController;
                          aObject,
                          aSetCurObject,aSetSelectObject,aSetCurToEndObject,aSetAllObject:TObject;

                          aFieldNames:array of string;
                          aValuesOrFieldNamesOrFieldNameOperations:array of Variant;
                          aValuesIsCale:array of Boolean;
                          aFastMode:boolean=False;
                          aKeyFieldName:string='';
                          aAddSQL:string=''
                           );overload;

//批量修改表格中选择记录指定字段的值是其它字段的首拼或全拼
//aMultiSelectType=选择方式
//aFirstPYFromFieldNames=设置首拼的字段名数组
//aFirstPYToFieldNames=首拼的取值字段数组
//aAllPYFromFieldNames=设置全拼的字段名数组
//aAllPYToFieldNames=全拼的取值字段数组
procedure ABSetFieldValue_MultiSelect(
                          aDataController:TcxCustomDataController;
                          aMultiSelectType:TABMultiSelectType;

                          aFirstPYFromFieldNames:array of string;
                          aFirstPYToFieldNames:array of string;
                          aAllPYFromFieldNames:array of string;
                          aAllPYToFieldNames:array of string;
                          aFastMode:boolean=False;
                          aKeyFieldName:string='';
                          aAddSQL:string=''
                           );overload;
procedure ABSetFieldValue_MultiSelect(
                          aDataController:TcxCustomDataController;

                          aObject,
                          aSetCurObject,aSetSelectObject,aSetCurToEndObject,aSetAllObject:TObject;

                          aFirstPYFromFieldNames:array of string;
                          aFirstPYToFieldNames:array of string;
                          aAllPYFromFieldNames:array of string;
                          aAllPYToFieldNames:array of string;
                          aFastMode:boolean=False;
                          aKeyFieldName:string='';
                          aAddSQL:string=''
                           );overload;

//转换表格中选择记录为插入或修改数据的SQL
//aMultiSelectType=选择方式
//aTableName=SQL中的表名
//aKeyFieldnames=SQL中的表的主键名
//aUpdateFieldnames=出现在SQL中更新部分的字段
//aInsertFieldnames=出现在SQL中插入部分的字段
//aReplaceFieldStrings=字段替换列表(参考ABGetSQLByDataset函数对应参数)
//aReplaceFieldDatasets=提供字段替换列表对应值的数据集数组(参考ABGetSQLByDataset函数对应参数)
function ABGetSQL_MultiSelect(
                           aDataController:TcxCustomDataController ;
                           aMultiSelectType:TABMultiSelectType;
                           aTableName:string;
                           aKeyFieldnames:array of string;
                           aUpdateFieldnames:array of string;
                           aInsertFieldnames:array of string;

                           aReplaceFieldStrings:TStrings;
                           aReplaceFieldDatasets:array of TDataSet

                           ):string;
//得到表格中选择记录的字段值
//aMultiSelectType=选择方式
//aFieldName=返回值的字段名
function ABGetFieldValue_MultiSelect(
                          aDataController:TcxCustomDataController;
                          aMultiSelectType:TABMultiSelectType;
                          aFieldName:string;
                          aSpaceFlag:string=','
                           ):string;



//得到CX系列控件的属性值(可不停止扩充，现只实现部分cx系列DataField,Color,ReadOnly)
function ABGetCortrolPropertyValue(aControl:TObject;aPropertyName:string):Variant;
//设置CX系列控件的属性值(可不停止扩充，现只实现部分cx系列DataField,Color,ReadOnly)
procedure ABSetCortrolPropertyValue(aControl:TObject;aPropertyName:string;aValue:Variant);

//设置表格的选择(无选择时默认选择当前记录)
procedure ABSetTableViewSelect(aView:TcxGridTableView);overload;
//根据多选类型设置表格的选择
procedure ABSetTableViewSelect(aView:TcxGridTableView;aMultiSelectType:TABMultiSelectType);overload;
//设置DataController的选择(无选择时默认选择当前记录)
procedure ABSetcxDBDataControllerSelect(aDataController:TcxCustomDataController);

//设置CheckListBox的操作
//aType=0全选择,1全不选择,2反向选择,
procedure ABSetcxListBoxSelect(aCheckListBox: TcxCheckListBox; aType: LongInt);
//保存CheckListBox的选择到默认的文件中
procedure ABSavecxListBoxSelect(aCheckListBox: TcxCheckListBox);
//从默认的文件中加载CheckListBox的选择
procedure ABLoadcxListBoxSelect(aCheckListBox: TcxCheckListBox);

//查找XML文档的结点
function ABFindXMLNode( aFindNode: TXMLNode;
                        aFindNodeName,aFindAttrName,aFindAttrNameValue: String;
                        aFindParentNodeName,aFindParentAttrName,aFindParentAttrNameValue: String
                        ): TXMLNode;

//设置XML文档的结点
function ABSetXMLNodeValue( aFindNode: TXMLNode;
                            aFindNodeName,aFindAttrName,aFindAttrNameValue: String;
                            aFindParentNodeName,aFindParentAttrName,aFindParentAttrNameValue: String;
                            aNodeValue:string): TXMLNode;

//读取资源中的版本号
function ABReadResVer(const FileName: string; var Major, Minor, Release, Build: Word): Boolean;
//修改资源中的版本号
procedure ABModifyResVer(const FileName: string; Major, Minor, Release, Build: Word);
//创建空的资源文件(根据传入的项目文件判断，当资源文件不存在时创建空的res资源文件)
function ABCreateEmptyRes(aFileName: string): boolean;

//得到ELPHI项目信息(包括dproj、dpr、dpk)
function ABGetDelphiProjectinfo(aFileName: string;aSetType:TABBatchSetDelphiProjectType):TStringDynArray;
//设置DELPHI项目信息(包括dproj、dpr、dpk)
procedure ABSetDelphiProjectinfo(aFileName: string;aSetType:TABBatchSetDelphiProjectType;aValue:array of string);
//批量设置DELPHI项目信息(包括dproj、dpr、dpk)
procedure ABSetDelphiProjectinfos(aString: TStrings;aSetType:TABBatchSetDelphiProjectType;aValue:array of string;aProgressBar:TProgressBar=nil);
//得到项目信息的字串列表(包括dproj、dpr、dpk)
procedure ABGetDelphiProjectinfoToStrings(aFileName: string;aStrings:TStrings);

//为TdxNavBarGroup增加新项
//aSetFirst=是否新增到组的第一项
//aMaxGroupLinkCount=组的最多项的数量(0表示没有限制)
procedure ABAddNavBarGroupItem(aNavBar: TdxNavBar;
                         aGroup:TdxNavBarGroup;
                         aCaption,aHint: string;
                         aEvent:TNotifyEvent=nil;
                         aSetFirst:Boolean=false;
                         aMaxGroupLinkCount:LongInt=0);
//拷贝TRadioGroup的项到TcxRadioGroup
procedure ABCopyRadioGroupItems(aFromRadioGroup: TRadioGroup;aToRadioGroup: TcxRadioGroup;aClearToRadioGroupOld:Boolean=true);
//清除dxBarItemLinks中的项
procedure ABCleardxBarItems(aItemLinks:TdxBarItemLinks;aFreeItem:Boolean=true);
//设置dxBarItems中的项可见性
//aSyncClient=是否同步子项的aVisible
procedure ABSetdxBarItemsVisible(aItemLinks:TdxBarItemLinks;aVisible:TdxBarItemVisible;aSyncClient:boolean=true);

//根据TCustomListBox的行内容字符数目设置水平流动条的流动宽度
//aBeginCharNum=起始的字符数量，为0时取所有行中最长行的字符数量
procedure ABRefreshcxListBoxWidth(aListBox: TcxCheckListBox; aBeginCharNum: LongInt = 0);
//多个cxGrid的当前TableView合并到一个EXCEL文件的多个Sheet
//调用示例  ABExportCxgridsToExcel([cxGrid1,cxGrid2],['cxGrid1','cxGrid2']);
procedure ABExportCxgridsToExcel(acxGrids: array of TcxGrid; aSheetNames: array of string);

//翻译dxStatusBar中项的内容
procedure ABLanguageSetComponentsText(adxStatusBar: TdxStatusBar);
//CheckComboBox中编辑值转到列表中复选
procedure ABCheckComboBoxEditValueToStates(
  acxCheckComboBoxProperties: TcxCheckComboBoxProperties; const AValue: Variant; var ACheckStates: TcxCheckStates);
//CheckComboBox中列表中复选转到编辑值
procedure ABCheckComboBoxStatesToEditValue(
  acxCheckComboBoxProperties: TcxCheckComboBoxProperties; const ACheckStates: TcxCheckStates; out AValue: Variant);


implementation


type
  TcxDataControllerGroupsProtected = class (TcxDataControllerGroups);

type
  PFindTextInfo = ^TFindTextInfo;
  TFindTextInfo = record
    AForward:Boolean;
    ClounmIndex,
    FindClounmIndex:LongInt;
    Text: string;
  end;


procedure ABCheckComboBoxEditValueToStates(
  acxCheckComboBoxProperties: TcxCheckComboBoxProperties; const AValue: Variant; var ACheckStates: TcxCheckStates);
var
  I: Integer;
  tempDelimiter,
  tempVarlueStr:string;
begin
  tempVarlueStr:=VarToStrDef(AValue,'');
  tempDelimiter:= acxCheckComboBoxProperties.Delimiter;
  for I := Low(ACheckStates) to High(ACheckStates) do
  begin
    if ABPos(tempDelimiter+acxCheckComboBoxProperties.Items[i].Description+tempDelimiter,
             tempDelimiter+tempVarlueStr+tempDelimiter)>0 then
      ACheckStates[i]:=cbsChecked
    else
      ACheckStates[i]:=cbsUnChecked;
  end;
end;

procedure ABCheckComboBoxStatesToEditValue(
  acxCheckComboBoxProperties: TcxCheckComboBoxProperties; const ACheckStates: TcxCheckStates; out AValue: Variant);
var
  I: Integer;
  tempStrSum1:string;
begin
  tempStrSum1:=EmptyStr;
  for I := Low(ACheckStates) to High(ACheckStates) do
  begin
    if (ACheckStates[i]=cbsChecked) then
    begin
      if (acxCheckComboBoxProperties.Items[i].Description<>EmptyStr) then
      begin
        ABAddstr(tempStrSum1,
                 acxCheckComboBoxProperties.Items[i].Description,
                 acxCheckComboBoxProperties.Delimiter);
      end;
    end;
  end;
  AValue:=tempStrSum1;
end;

function FindTextFilter(ANode: TcxTreeListNode; AData: Pointer): Boolean;
var
  i:LongInt;
begin
  Result := False;
  if PFindTextInfo(AData).AForward then
  begin
    for I := PFindTextInfo(AData).ClounmIndex to ANode.ValueCount-1 do
    begin
      Result := ABLike(ANode.Texts[i], PFindTextInfo(AData).Text);
      if Result then
      begin
        PFindTextInfo(AData).FindClounmIndex:=i;
        Break;
      end;
    end;
    PFindTextInfo(AData).ClounmIndex:=0
  end
  else
  begin
    for I := PFindTextInfo(AData).ClounmIndex downto 0 do
    begin
      Result := ABLike(ANode.Texts[i], PFindTextInfo(AData).Text);
      if Result then
      begin
        PFindTextInfo(AData).FindClounmIndex:=i;
        Break;
      end;
    end;
    PFindTextInfo(AData).ClounmIndex:=ANode.ValueCount-1
  end;
end;

function ABSearchIncxTreeList(aTreeList: TcxDBTreeList; aText: string; aDownUpType: LongInt):boolean;
var
  tempNode:TcxTreeListNode;
  AFindInfo: TFindTextInfo;
  tempAForward:Boolean;
begin
  result:=false;
  tempNode:=nil;
  tempAForward:=false;
  AFindInfo.Text := aText;
  if aDownUpType=0 then
  begin
    tempNode:= aTreeList.Root;
    tempAForward:=true;
    AFindInfo.ClounmIndex:=0;
  end
  else  if aDownUpType=-1 then
  begin
    tempNode:= aTreeList.FocusedNode;
    tempAForward:=False;
    if Assigned(tempNode) then
    begin
      AFindInfo.ClounmIndex:=aTreeList.FocusedColumn.ItemIndex;
      if aTreeList.FocusedColumn.IsLeft then
      begin
        AFindInfo.ClounmIndex:=aTreeList.ColumnCount-1;
        tempNode:= tempNode.GetPrev;
      end
      else
      begin
        AFindInfo.ClounmIndex:=aTreeList.FocusedColumn.ItemIndex-1;
      end;
    end;
  end
  else  if aDownUpType=1 then
  begin
    tempNode:= aTreeList.FocusedNode;
    tempAForward:=true;
    if Assigned(tempNode) then
    begin
      if aTreeList.FocusedColumn.IsRight then
      begin
        AFindInfo.ClounmIndex:=0;
        tempNode:= tempNode.GetNext;
      end
      else
      begin
        AFindInfo.ClounmIndex:=aTreeList.FocusedColumn.ItemIndex+1;
      end;
    end;
  end;
  AFindInfo.AForward := tempAForward;

  tempNode := aTreeList.Find(
                    @AFindInfo,                   //查找描述结构
                    tempNode,                     //开始查找的结点
                    False,
                    tempAForward,                 //True=向下，False=向前
                    FindTextFilter,               //查找函数
                    False
                    );


  if Assigned(tempNode) then
  begin
    tempNode.Selected:=true;
    aTreeList.Columns[AFindInfo.FindClounmIndex].Focused:=true;
    result:=True;
  end;
end;

function ABSearchIncxTableView(aTableView: TcxGridTableView; aText: string; aDownUpType: LongInt ): boolean;
  //这里可以按你的选项处理
  function Compare(const ARecIndex, AColIndex: integer): boolean;
  begin
    Result :=ABLike(aTableView.DataController.DisplayTexts[ARecIndex, aTableView.VisibleColumns[AColIndex].Index], AText);
  end;
var
  tempColIndex_Begin,tempColIndex_End: integer;
  tempRowIndex_Begin,tempRowIndex_End: integer;
  tempRecIndex,
  tempGroupsIndex: integer;
  tempGroupsCount: integer;
  ii,i, j: integer;
  tempCurIndex: integer;
begin
{
  for I := 0 to ABcxGrid1ABcxGridDBBandedTableView1.VisibleColumnCount-1 do
  begin
    if ABcxGrid1ABcxGridDBBandedTableView1.DataController.Search.Locate(i,Edit1.Text) then
    begin
      break;
    end;
  end;
  ABcxGrid1ABcxGridDBBandedTableView1.DataController.Search.Locat方式不支持开头的通配符，如*aa之类
  }

  Result := false;
  tempColIndex_Begin:=0;
  tempColIndex_End:=0;
  tempRowIndex_Begin:=0;
  tempRowIndex_End:=0;
  tempGroupsIndex:=0;
  aTableView.DataController.ClearSelection;
  if aTableView.DataController.Groups.GroupingItemCount = 0 then
  begin
    if aDownUpType=-1 then
    begin
      tempColIndex_End:=aTableView.Controller.FocusedColumnIndex;
      tempRowIndex_End:=aTableView.Controller.FocusedRowIndex;
      if aTableView.Controller.FocusedColumn.IsFirst then
      begin
        tempColIndex_End := aTableView.VisibleColumnCount - 1;
        Dec(tempRowIndex_End);
      end
      else
      begin
        Dec(tempColIndex_End)
      end;
    end
    else if aDownUpType=0 then
    begin
      tempColIndex_Begin:=0;
      tempRowIndex_Begin:=0;
    end
    else if aDownUpType=1 then
    begin
      tempColIndex_Begin:=aTableView.Controller.FocusedColumnIndex;
      tempRowIndex_Begin:=aTableView.Controller.FocusedRowIndex;
      if aTableView.Controller.FocusedColumn.IsLast then
      begin
        tempColIndex_Begin := 0;
        Inc(tempRowIndex_Begin);
      end
      else
      begin
        Inc(tempColIndex_Begin)
      end;
    end;

    if aDownUpType=-1 then
    begin
      for i := tempRowIndex_End downto 0 do
      begin
        for j := tempColIndex_End downto 0 do
        begin
          if Compare(aTableView.ViewData.Rows[i].RecordIndex, j) then
          begin
            aTableView.Controller.FocusedRowIndex := i;
            aTableView.Controller.FocusedColumnIndex := j;
            Result :=True ;
            Break;
          end;
        end;

        if Result then
          Break;
        tempColIndex_End := aTableView.VisibleColumnCount - 1;
      end;
    end
    else
    begin
      for i := tempRowIndex_Begin to aTableView.ViewData.RowCount - 1 do
      begin
        for j := tempColIndex_Begin to aTableView.VisibleColumnCount - 1 do
        begin
          if Compare(aTableView.ViewData.Rows[i].RecordIndex, j) then
          begin
            aTableView.Controller.FocusedRowIndex := i;
            aTableView.Controller.FocusedColumnIndex := j;
            Result :=True ;
            Break;
          end;
        end;

        if Result then
          Break;

        tempColIndex_Begin := 0;
      end;
    end;
  end
  else
  begin
    if aDownUpType=-1 then
    begin
      tempColIndex_End:=aTableView.Controller.FocusedColumnIndex;
      tempRowIndex_End:=aTableView.Controller.FocusedRowIndex;
      if aTableView.Controller.FocusedColumn.IsFirst then
      begin
        tempColIndex_End := aTableView.VisibleColumnCount - 1;
        Dec(tempRowIndex_End);
      end
      else
      begin
        Dec(tempColIndex_End)
      end;
      tempGroupsIndex := aTableView.DataController.Groups.DataGroupIndexByRowIndex[tempRowIndex_End];
    end
    else if aDownUpType=0 then
    begin
      tempColIndex_Begin:=0;
      tempRowIndex_Begin:=0;
      tempGroupsIndex := aTableView.DataController.Groups.DataGroupIndexByRowIndex[tempRowIndex_Begin];
    end
    else if aDownUpType=1 then
    begin
      tempColIndex_Begin:=aTableView.Controller.FocusedColumnIndex;
      tempRowIndex_Begin:=aTableView.Controller.FocusedRowIndex;
      if aTableView.Controller.FocusedColumn.IsLast then
      begin
        tempColIndex_Begin := 0;
        Inc(tempRowIndex_Begin);
      end
      else
      begin
        Inc(tempColIndex_Begin)
      end;
      tempGroupsIndex := aTableView.DataController.Groups.DataGroupIndexByRowIndex[tempRowIndex_Begin];
    end;
    tempGroupsCount := TcxDataControllerGroupsProtected(aTableView.DataController.Groups).DataGroups.Count;
    if aDownUpType=-1 then
    begin
      for ii := tempGroupsIndex downto 0 do
      begin
        for i := aTableView.DataController.Groups.ChildCount[ii] - 1 downto 0 do
        begin
          tempRecIndex := aTableView.DataController.Groups.ChildRecordIndex[ii, i];
          if tempRecIndex = -1 then
            Continue;

          tempCurIndex := aTableView.DataController.GetRowIndexByRecordIndex(tempRecIndex, false);
          //tempCurIndex > tempRowIndex_End决定只向上查找
          if (tempCurIndex > -1) and (tempCurIndex > tempRowIndex_End) then
            Continue;

          for j := tempColIndex_End downto 0  do
          begin
            if Compare(tempRecIndex, j) then
            begin
              aTableView.Controller.FocusedRowIndex    := aTableView.DataController.GetRowIndexByRecordIndex(tempRecIndex, true);
              aTableView.Controller.FocusedColumnIndex := j;
              Result :=True ;
              Break;
            end;
          end;

          tempColIndex_End :=  aTableView.VisibleColumnCount - 1;
          if Result then
            Break;
        end;
        if Result then
          Break;
      end;
    end
    else
    begin
      for ii := tempGroupsIndex to tempGroupsCount-1 do
      begin
        for i := 0 to aTableView.DataController.Groups.ChildCount[ii] - 1 do
        begin
          tempRecIndex := aTableView.DataController.Groups.ChildRecordIndex[ii, i];
          if tempRecIndex = -1 then
            Continue;

          tempCurIndex := aTableView.DataController.GetRowIndexByRecordIndex(tempRecIndex, false);
          //tempCurIndex < tempRowIndex_Begin决定只向下查找
          if (tempCurIndex > -1) and (tempCurIndex < tempRowIndex_Begin) then
            Continue;

          for j := tempColIndex_Begin to aTableView.VisibleColumnCount - 1 do
          begin
            if Compare(tempRecIndex, j) then
            begin
              aTableView.Controller.FocusedRowIndex    := aTableView.DataController.GetRowIndexByRecordIndex(tempRecIndex, true);
              aTableView.Controller.FocusedColumnIndex := j;
              Result :=True ;
              Break;
            end;
          end;

          tempColIndex_Begin := 0;
          if Result then
            Break;
        end;
        if Result then
          Break;
      end;
    end;
  end;

  if Result then
  begin
    ABSetTableViewSelect(aTableView);
  end;
end;

procedure ABAddNavBarGroupItem(aNavBar: TdxNavBar;
                         aGroup:TdxNavBarGroup;
                         aCaption,aHint: string;
                         aEvent:TNotifyEvent;
                         aSetFirst:Boolean;
                         aMaxGroupLinkCount:LongInt);
var
  i:LongInt;
  tempItem:TdxNavBarItem;
  procedure AddGroupItemLink(aGroup:TdxNavBarGroup;aItem:TdxNavBarItem);
  var
    tempLink:TdxNavBarItemLink;
  begin
    tempLink:=aGroup.FindLink(aItem);
    if not Assigned(tempLink) then
    begin
      tempLink:=aGroup.CreateLink(aItem);
    end;
    if aSetFirst then
      tempLink.Index:=0;
  end;
begin
  //检测是否存在旧的item
  tempItem:=nil;
  for I := 0 to aNavBar.Items.Count - 1 do
  begin
    if (AnsiCompareText(aNavBar.Items.Items[i].Caption,aCaption)=0) and
       (AnsiCompareText(aNavBar.Items.Items[i].Hint,aHint)=0)  then
    begin
      tempItem:=aNavBar.Items.Items[i];
      Break;
    end;
  end;

  //不存在则增加新的
  if not Assigned(tempItem) then
  begin
    tempItem:=TdxNavBarItem(aNavBar.Items.Insert(0));
    tempItem.OnClick:=aEvent;
    tempItem.Caption:=aCaption;
    tempItem.Hint   :=aHint;
  end;

  //增加到组的连接
  AddGroupItemLink(aGroup,tempItem);

  //检测组中连接的数量是否超过最大值,如超过则删除最后一个
  if (aMaxGroupLinkCount>0) and (aGroup.LinkCount>aMaxGroupLinkCount) then
  begin
    aNavBar.Items.Remove(aGroup.Links[aGroup.LinkCount-1].Item);
  end;
end;

function ABGetDelphiVersionByDprojVersion(adprojVersion:double):TABDelphiVersion;
begin
  if ABCompareDouble(adprojVersion,13.4,2)=0 then
  begin
    result:=dvDXE2;
  end
  else if ABCompareDouble(adprojVersion,14.3,2)=0 then
  begin
    result:=dvDXE3;
  end
  else
  begin
    result:=dvD2009;
  end;
end;

function ABSetXMLNodeValue( aFindNode: TXMLNode;
                            aFindNodeName,aFindAttrName,aFindAttrNameValue: String;
                            aFindParentNodeName,aFindParentAttrName,aFindParentAttrNameValue: String;
                            aNodeValue:string): TXMLNode;
var
  tempNode: TXMLNode;
begin
  Result := ABFindXMLNode( aFindNode,
                           aFindNodeName,aFindAttrName,aFindAttrNameValue,
                           aFindParentNodeName,aFindParentAttrName,aFindParentAttrNameValue);
  if Assigned(Result) then
  begin
    if Assigned(Result.Parent) then
      Result.Parent.WriteString(aFindNodeName,aNodeValue);
  end
  else
  begin
    Result := ABFindXMLNode(
                   aFindNode,
                   aFindParentNodeName,aFindParentAttrName,aFindParentAttrNameValue,
                   '','','');
    if (Assigned(Result))  then
    begin
      if Assigned(Result.NodeByName(aFindNodeName)) then
        Result.WriteString(aFindNodeName,aNodeValue)
      else
      begin
        tempNode:=Result.NodeNewAtIndex(Result.NodeCount,aFindNodeName);
        tempNode.Value:=aNodeValue;
       end;
    end;
  end;
end;

function ABFindXMLNode( aFindNode: TXMLNode;
                        aFindNodeName,aFindAttrName,aFindAttrNameValue: String;
                        aFindParentNodeName,aFindParentAttrName,aFindParentAttrNameValue: String
                        ): TXMLNode;
var
  I: Integer;
begin
  Result := nil;
  if (aFindNode.Name = aFindNodeName) and
     ((aFindAttrName=emptystr) or (aFindNode.HasAttribute(aFindAttrName))) and
     ((aFindAttrNameValue=emptystr) or (aFindNode.AttributeByName[aFindAttrName].Value = aFindAttrNameValue)) and
     ((aFindParentNodeName=emptystr) or (Assigned(aFindNode.Parent)) and (aFindNode.Parent.Name=aFindParentNodeName)) and
     ((aFindParentAttrName=emptystr) or (aFindNode.Parent.HasAttribute(aFindParentAttrName))) and
     ((aFindParentAttrNameValue=emptystr) or (aFindNode.Parent.AttributeByName[aFindParentAttrName].Value = aFindParentAttrNameValue))
     then
  begin
    Result := aFindNode;
    Exit;
  end;

  if aFindNode.NodeCount>0 then
  begin
    for I := 0 to aFindNode.NodeCount - 1 do
    begin
      Result := ABFindXMLNode(aFindNode.Nodes[I],
                              aFindNodeName,aFindAttrName,aFindAttrNameValue,
                              aFindParentNodeName,aFindParentAttrName,aFindParentAttrNameValue);

      if Assigned(Result) then
        Break;
    end;
  end;
end;

function ABCreateEmptyRes(aFileName: string): boolean;
var
  Res: TResModule;
  tempExt: string;
  tempResFileName: string;
begin
  result:=false;
  tempExt:= ABGetFileExt(aFileName);
  tempResFileName:=ChangeFileExt(aFileName, '.res');
  if (
      (AnsiCompareText(tempExt,'dpk')=0) or
      (AnsiCompareText(tempExt,'dpr')=0)
     ) and
     (not ABCheckFileExists(tempResFileName))
      then
  begin
    Res := TResModule.Create;
    try
      Res.SaveToFile(tempResFileName);
      result:=true;
    finally
      Res.Free;
    end;
  end;
end;

procedure ABModifyResVer(const FileName: string; Major, Minor, Release, Build: Word);
var
  Res: TResModule;
  I: Integer;
  ResVer: TVersionInfoResourceDetails;
  VerData: TULargeInteger;
begin
  ResVer := nil;

  Res := TResModule.Create;
  try
    Res.LoadFromFile(FileName);

    for I := 0 to Res.ResourceCount - 1 do
    begin
      if Res.ResourceDetails[I] is TVersionInfoResourceDetails then
        ResVer := Res.ResourceDetails[I] as TVersionInfoResourceDetails;
    end;

    if ResVer <> nil then
    begin
      VerData.HighPart := MakeLong(Minor, Major);
      VerData.LowPart := MakeLong(Build, Release);
      ResVer.FileVersion := VerData;

      Res.SaveToFile(FileName);
    end;
  finally
    Res.Free;
  end;
end;

function ABReadResVer(const FileName: string; var Major, Minor, Release, Build: Word): Boolean;
var
  Res: TResModule;
  I: Integer;
  ResVer: TVersionInfoResourceDetails;
begin
  Result := False;
  ResVer := nil;

  Res := TResModule.Create;
  try
    try
      Res.LoadFromFile(FileName);

      for I := 0 to Res.ResourceCount - 1 do
      begin
        if Res.ResourceDetails[I] is TVersionInfoResourceDetails then
          ResVer := Res.ResourceDetails[I] as TVersionInfoResourceDetails;
      end;

      if ResVer <> nil then
      begin
        Major := HiWord(ResVer.FileVersion.HighPart);
        Minor := ResVer.FileVersion.HighPart and $0000FFFF;
        Release := HiWord(ResVer.FileVersion.LowPart);
        Build := ResVer.FileVersion.LowPart and $0000FFFF;

        Result := True;
      end;
    except
      Result := False;
    end;
  finally
    Res.Free;
  end;
end;

procedure ABGetDelphiProjectinfoToStrings(aFileName: string;aStrings:TStrings);
var
  tempArray:TStringDynArray;
begin
  aStrings.Clear;
  aStrings.Add('bstIncludeVerInfo='+ABGetDelphiProjectinfo(aFileName,bstIncludeVerInfo)[0]);
  aStrings.Add('bstIncludeVerInfo='+ABGetDelphiProjectinfo(aFileName,bstBuildIncVersion)[0]);
  tempArray:=ABGetDelphiProjectinfo(aFileName,bstVersion);
  aStrings.Add('bstVersion0='+tempArray[0]);
  aStrings.Add('bstVersion1='+tempArray[1]);
  aStrings.Add('bstVersion2='+tempArray[2]);
  aStrings.Add('bstVersion3='+tempArray[3]);
  aStrings.Add('bstBplPath='+ABGetDelphiProjectinfo(aFileName,bstBplPath)[0]);
  aStrings.Add('bstBplPath='+ABGetDelphiProjectinfo(aFileName,bstDcpPath)[0]);
  aStrings.Add('bstExePath='+ABGetDelphiProjectinfo(aFileName,bstExePath)[0]);
  aStrings.Add('bstBuildWithPackage='+ABGetDelphiProjectinfo(aFileName,bstBuildWithPackage)[0]);
  aStrings.Add('bstBuildWithPackageList='+ABGetDelphiProjectinfo(aFileName,bstBuildWithPackageList)[0]);
end;

function ABGetDelphiProjectinfo(aFileName: string;aSetType:TABBatchSetDelphiProjectType):TStringDynArray;
var
  tempFileExt :string;
  procedure ABGetDelphidproj;
  var
    tempXmlDoc: TnativeXml;
    tempRootNode, tempNode: TxmlNode;
    tempSourceFileExt:string;
    tempDelphiVersion_Double:Double;
    tempDelphiVersion:TABDelphiVersion;
    procedure DodvD2009;
    begin
      case aSetType of
        bstIncludeVerInfo:
        begin
          tempNode:=ABFindXMLNode(tempRootNode,
                            'VersionInfo','Name','IncludeVerInfo',
                            'VersionInfo','','');
        end;
        bstBuildIncVersion:
        begin
          tempNode:=ABFindXMLNode(tempRootNode,
                            'VersionInfo','Name','AutoIncBuild',
                            'VersionInfo','','');
        end;
        bstVersion:
        begin
          result[0]:='0';
          result[1]:='0';
          result[2]:='0';
          result[3]:='0';
          tempNode:=ABFindXMLNode(tempRootNode,
                            'VersionInfo','Name','MajorVer',
                            'VersionInfo','','');
          if Assigned(tempNode) then
            result[0]:=tempNode.Value;
          tempNode:=ABFindXMLNode(tempRootNode,
                            'VersionInfo','Name','MinorVer',
                            'VersionInfo','','');
          if Assigned(tempNode) then
            result[1]:=tempNode.Value;
          tempNode:=ABFindXMLNode(tempRootNode,
                            'VersionInfo','Name','Release',
                            'VersionInfo','','');
          if Assigned(tempNode) then
            result[2]:=tempNode.Value;
          tempNode:=ABFindXMLNode(tempRootNode,
                            'VersionInfo','Name','Build',
                            'VersionInfo','','');
          if Assigned(tempNode) then
            result[3]:=tempNode.Value;

          tempNode:=nil;
        end;
        bstDcpPath:
        begin
          if AnsiCompareText(tempSourceFileExt,'dpk')=0 then
          begin
            tempNode:=ABFindXMLNode(tempRootNode,
                              'DCC_DcpOutput','','',
                              'PropertyGroup','Condition','''$(Base)''!=''''');
          end;
        end;
        bstBplPath:
        begin
          if AnsiCompareText(tempSourceFileExt,'dpk')=0 then
          begin
            tempNode:=ABFindXMLNode(tempRootNode,
                              'DCC_BplOutput','','',
                              'PropertyGroup','Condition','''$(Base)''!=''''');
          end;
        end;
        bstExePath:
        begin
          if AnsiCompareText(tempSourceFileExt,'dpr')=0 then
          begin
            tempNode:=ABFindXMLNode(tempRootNode,
                              'DCC_ExeOutput','','',
                              'PropertyGroup','Condition','''$(Base)''!=''''');
          end;
        end;
        bstBuildWithPackage:
        begin
          if AnsiCompareText(tempSourceFileExt,'dpr')=0 then
          begin
            tempNode:=ABFindXMLNode(tempRootNode,
                              'UsePackages','','',
                              'PropertyGroup','Condition','''$(Base)''!=''''');
          end;
        end;
        bstBuildWithPackageList:
        begin
          if AnsiCompareText(tempSourceFileExt,'dpr')=0 then
          begin
            tempNode:=ABFindXMLNode(tempRootNode,
                              'DCC_UsePackage','','',
                              'PropertyGroup','Condition','''$(Base)''!=''''');
          end;
        end;
      end;

      if Assigned(tempNode) then
        result[0]:=tempNode.Value;
    end;
    procedure DodvDXE2;
    begin
      case aSetType of
        bstIncludeVerInfo:
        begin
          tempNode:=ABFindXMLNode(tempRootNode,
                            'VerInfo_IncludeVerInfo','','',
                            'PropertyGroup','Condition','''$(Base)''!=''''');
        end;
        bstBuildIncVersion:
        begin
  //            tempNode:=ABFindXMLNode(tempRootNode,
  //                              'VerInfo_AutoIncVersion','','',
  //                              'PropertyGroup','Condition','''$(Base)''!=''''');
        end;
        bstVersion:
        begin
          result[0]:='0';
          result[1]:='0';
          result[2]:='0';
          result[3]:='0';
          tempNode:=ABFindXMLNode(tempRootNode,
                            'VerInfo_MajorVer','','',
                            'PropertyGroup','Condition','''$(Base)''!=''''');
          if Assigned(tempNode) then
            result[0]:=tempNode.Value;
          tempNode:=ABFindXMLNode(tempRootNode,
                            'VerInfo_MinorVer','','',
                            'PropertyGroup','Condition','''$(Base)''!=''''');
          if Assigned(tempNode) then
            result[1]:=tempNode.Value;
          tempNode:=ABFindXMLNode(tempRootNode,
                            'VerInfo_Release','','',
                            'PropertyGroup','Condition','''$(Base)''!=''''');
          if Assigned(tempNode) then
            result[2]:=tempNode.Value;
          tempNode:=ABFindXMLNode(tempRootNode,
                            'VerInfo_Build','','',
                            'PropertyGroup','Condition','''$(Base)''!=''''');
          if Assigned(tempNode) then
            result[3]:=tempNode.Value;

          tempNode:=nil;
        end;
        bstDcpPath:
        begin
          if AnsiCompareText(tempSourceFileExt,'dpk')=0 then
          begin
            tempNode:=ABFindXMLNode(tempRootNode,
                              'DCC_DcpOutput','','',
                              'PropertyGroup','Condition','''$(Base)''!=''''');
          end;
        end;
        bstBplPath:
        begin
          if AnsiCompareText(tempSourceFileExt,'dpk')=0 then
          begin
            tempNode:=ABFindXMLNode(tempRootNode,
                              'DCC_BplOutput','','',
                              'PropertyGroup','Condition','''$(Base)''!=''''');
          end;
        end;
        bstExePath:
        begin
          if AnsiCompareText(tempSourceFileExt,'dpr')=0 then
          begin
            tempNode:=ABFindXMLNode(tempRootNode,
                              'DCC_ExeOutput','','',
                              'PropertyGroup','Condition','''$(Base)''!=''''');
          end;
        end;
        bstBuildWithPackage:
        begin
          if AnsiCompareText(tempSourceFileExt,'dpr')=0 then
          begin
            tempNode:=ABFindXMLNode(tempRootNode,
                              'UsePackages','','',
                              'PropertyGroup','Condition','''$(Base)''!=''''');
          end;
        end;
        bstBuildWithPackageList:
        begin
          if AnsiCompareText(tempSourceFileExt,'dpr')=0 then
          begin
            tempNode:=ABFindXMLNode(tempRootNode,
                              'DCC_UsePackage','','',
                              'PropertyGroup','Condition','''$(Base)''!=''''');
          end;
        end;
      end;

      if Assigned(tempNode) then
        result[0]:=tempNode.Value;
    end;
    procedure DodvDXE3;
    begin
      case aSetType of
        bstIncludeVerInfo:
        begin
          tempNode:=ABFindXMLNode(tempRootNode,
                            'VerInfo_IncludeVerInfo','','',
                            'PropertyGroup','Condition','''$(Base)''!=''''');
        end;
        bstBuildIncVersion:
        begin
          tempNode:=ABFindXMLNode(tempRootNode,
                            'VerInfo_AutoIncVersion','','',
                            'PropertyGroup','Condition','''$(Base)''!=''''');
        end;
        bstVersion:
        begin
          result[0]:='0';
          result[1]:='0';
          result[2]:='0';
          result[3]:='0';
          tempNode:=ABFindXMLNode(tempRootNode,
                            'VerInfo_MajorVer','','',
                            'PropertyGroup','Condition','''$(Base)''!=''''');
          if Assigned(tempNode) then
            result[0]:=tempNode.Value;
          tempNode:=ABFindXMLNode(tempRootNode,
                            'VerInfo_MinorVer','','',
                            'PropertyGroup','Condition','''$(Base)''!=''''');
          if Assigned(tempNode) then
            result[1]:=tempNode.Value;
          tempNode:=ABFindXMLNode(tempRootNode,
                            'VerInfo_Release','','',
                            'PropertyGroup','Condition','''$(Base)''!=''''');
          if Assigned(tempNode) then
            result[2]:=tempNode.Value;
          tempNode:=ABFindXMLNode(tempRootNode,
                            'VerInfo_Build','','',
                            'PropertyGroup','Condition','''$(Base)''!=''''');
          if Assigned(tempNode) then
            result[3]:=tempNode.Value;

          tempNode:=nil;
        end;
        bstDcpPath:
        begin
          if AnsiCompareText(tempSourceFileExt,'dpk')=0 then
          begin
            tempNode:=ABFindXMLNode(tempRootNode,
                              'DCC_DcpOutput','','',
                              'PropertyGroup','Condition','''$(Base)''!=''''');
          end;
        end;
        bstBplPath:
        begin
          if AnsiCompareText(tempSourceFileExt,'dpk')=0 then
          begin
            tempNode:=ABFindXMLNode(tempRootNode,
                              'DCC_BplOutput','','',
                              'PropertyGroup','Condition','''$(Base)''!=''''');
          end;
        end;
        bstExePath:
        begin
          if AnsiCompareText(tempSourceFileExt,'dpr')=0 then
          begin
            tempNode:=ABFindXMLNode(tempRootNode,
                              'DCC_ExeOutput','','',
                              'PropertyGroup','Condition','''$(Base)''!=''''');
          end;
        end;
        bstBuildWithPackage:
        begin
          if AnsiCompareText(tempSourceFileExt,'dpr')=0 then
          begin
            tempNode:=ABFindXMLNode(tempRootNode,
                              'UsePackages','','',
                              'PropertyGroup','Condition','''$(Base)''!=''''');
          end;
        end;
        bstBuildWithPackageList:
        begin
          if AnsiCompareText(tempSourceFileExt,'dpr')=0 then
          begin
            tempNode:=ABFindXMLNode(tempRootNode,
                              'DCC_UsePackage','','',
                              'PropertyGroup','Condition','''$(Base)''!=''''');
          end;
        end;
      end;

      if Assigned(tempNode) then
        result[0]:=tempNode.Value;
    end;
  begin
    //修改文件内容
    tempXmlDoc := TnativeXml.Create(nil);
    try
      tempXmlDoc.LoadFromFile(aFileName);
      tempRootNode := tempXmlDoc.Root;
      tempSourceFileExt:=EmptyStr;
      tempNode := ABFindXMLNode(tempRootNode, 'Source','Name','MainSource',
                                              'Source','','');
      if tempNode <> nil then
      begin
        tempSourceFileExt:=ABGetFileExt(tempNode.Value);
      end
      else
      begin
        exit;
      end;
      tempNode := ABFindXMLNode(tempRootNode,'ProjectVersion','','',
                                             'PropertyGroup','','');
      if tempNode <> nil then
      begin
        tempDelphiVersion_Double:=StrToFloat(tempNode.Value);
        tempDelphiVersion:=ABGetDelphiVersionBydprojVersion(tempDelphiVersion_Double);
      end
      else
      begin
        exit;
      end;

      tempNode:=nil;
      case tempDelphiVersion of
        dvD2009:
        begin
          DodvD2009;
        end;
        dvDXE2:
        begin
          DodvDXE2;
        end;
        dvDXE3:
        begin
          DodvDXE3;
        end;
      end;
    finally
      tempXmlDoc.Free;
    end;
  end;
begin
  setlength(result,4);
  if ABCheckFileExists(aFileName) then
  begin
    tempFileExt:=ABGetFileExt(aFileName);
    //D2009~DXE3的项目文件
    if AnsiCompareText(tempFileExt, 'dproj')=0 then
    begin
      ABGetDelphidproj;
    end
    //D7的项目文件
    else if
            (
             (AnsiCompareText(tempFileExt, 'dpr')=0) or
             (AnsiCompareText(tempFileExt, 'dpk')=0)
             ) and
            (ABCheckFileExists(ChangeFileExt(aFileName, '.dof'))) and
            (
             (AnsiCompareText(tempFileExt, 'dpr')=0) or
             (aSetType<>bstIncludeVerInfo) and (aSetType<>bstBuildWithPackage)  and (aSetType<>bstBuildWithPackageList)
             )  then
    begin
      aFileName:=ChangeFileExt(aFileName, '.dof');
      if ABCheckFileExists(aFileName) then
        ABGetDelphidof(aFileName,aSetType,result);
    end;
  end;
end;

procedure ABSetDelphidproj(aFileName: string;aSetType:TABBatchSetDelphiProjectType;aValue:array of string);
var
  tempXmlDoc: TnativeXml;
  tempRootNode, tempNode: TXmlNode;
  tempSourceFileExt:string;
  tempDelphiVersion_Double:Double;
  tempStrings:TStrings;
  tempDelphiVersion: TABDelphiVersion;

  procedure DodvD2009;
  begin
    case aSetType of
      bstIncludeVerInfo:
      begin
        ABSetXMLNodeValue(tempRootNode,
                          'VersionInfo','Name','IncludeVerInfo',
                          'VersionInfo','','',ABBoolToStr(ABStrToBool(aValue[0])));
      end;
      bstBuildIncVersion:
      begin
        ABSetXMLNodeValue(tempRootNode,
                          'VersionInfo','Name','AutoIncBuild',
                          'VersionInfo','','',ABBoolToStr(ABStrToBool(aValue[0])));
      end;
      bstVersion:
      begin
        ABSetXMLNodeValue(tempRootNode,
                          'VersionInfo','Name','MajorVer',
                          'VersionInfo','','',aValue[0]);
        ABSetXMLNodeValue(tempRootNode,
                          'VersionInfo','Name','MinorVer',
                          'VersionInfo','','',aValue[1]);
        ABSetXMLNodeValue(tempRootNode,
                          'VersionInfo','Name','Release',
                          'VersionInfo','','',aValue[2]);
        ABSetXMLNodeValue(tempRootNode,
                          'VersionInfo','Name','Build',
                          'VersionInfo','','',aValue[3]);

        ABSetXMLNodeValue(tempRootNode,
                          'VersionInfoKeys','Name','FileVersion',
                          'VersionInfoKeys','','',Format('%s.%s.%s.%s', [aValue[0], aValue[1], aValue[2], aValue[3]]));
      end;
      bstDcpPath:
      begin
        if AnsiCompareText(tempSourceFileExt,'dpk')=0 then
        begin
          ABSetXMLNodeValue(tempRootNode,
                            'DCC_DcpOutput','','',
                            'PropertyGroup','Condition','''$(Base)''!=''''',aValue[0]);
        end;
      end;
      bstBplPath:
      begin
        if AnsiCompareText(tempSourceFileExt,'dpk')=0 then
        begin
          ABSetXMLNodeValue(tempRootNode,
                            'DCC_BplOutput','','',
                            'PropertyGroup','Condition','''$(Base)''!=''''',aValue[0]);
        end;
      end;
      bstExePath:
      begin
        if AnsiCompareText(tempSourceFileExt,'dpr')=0 then
        begin
          ABSetXMLNodeValue(tempRootNode,
                            'DCC_ExeOutput','','',
                            'PropertyGroup','Condition','''$(Base)''!=''''',aValue[0]);
        end;
      end;
      bstBuildWithPackage:
      begin
        if AnsiCompareText(tempSourceFileExt,'dpr')=0 then
        begin
          ABSetXMLNodeValue(tempRootNode,
                            'UsePackages','','',
                            'PropertyGroup','Condition','''$(Base)''!=''''',ABBoolToStr(ABStrToBool(aValue[0])));
        end;
      end;
      bstBuildWithPackageList:
      begin
        if AnsiCompareText(tempSourceFileExt,'dpr')=0 then
        begin
          ABSetXMLNodeValue(tempRootNode,
                            'DCC_UsePackage','','',
                            'PropertyGroup','Condition','''$(Base)''!=''''',aValue[0]);
        end;
      end;
    end;
  end;
  procedure DodvDXE2;
  begin
    case aSetType of
      bstIncludeVerInfo:
      begin
        ABSetXMLNodeValue(tempRootNode,
                          'VerInfo_IncludeVerInfo','','',
                          'PropertyGroup','Condition','''$(Base)''!=''''',ABBoolToStr(ABStrToBool(aValue[0])));
      end;
      bstBuildIncVersion:
      begin
//            ABSetXMLNodeValue(tempRootNode,
//                              'VerInfo_AutoIncVersion','','',
//                              'PropertyGroup','Condition','''$(Base)''!=''''',ABBoolToStr(ABStrToBool(aValue[0])));
      end;
      bstVersion:
      begin
        ABSetXMLNodeValue(tempRootNode,
                          'VerInfo_MajorVer','','',
                          'PropertyGroup','Condition','''$(Base)''!=''''',aValue[0]);
        ABSetXMLNodeValue(tempRootNode,
                          'VerInfo_MinorVer','','',
                          'PropertyGroup','Condition','''$(Base)''!=''''',aValue[1]);
        ABSetXMLNodeValue(tempRootNode,
                          'VerInfo_Release','','',
                          'PropertyGroup','Condition','''$(Base)''!=''''',aValue[2]);
        ABSetXMLNodeValue(tempRootNode,
                          'VerInfo_Build','','',
                          'PropertyGroup','Condition','''$(Base)''!=''''',aValue[3]);

        ABSetXMLNodeValue(tempRootNode,
                          'VerInfo_Keys','','',
                          'PropertyGroup','Condition','''$(Base)''!=''''',
                          Format('CompanyName=;FileDescription=;FileVersion=%s.%s.%s.%s;'+
                                'InternalName=;LegalCopyright=;LegalTrademarks=;OriginalFilename=;'+
                                'ProductName=;ProductVersion=1.0.0.0;Comments=', [aValue[0], aValue[1], aValue[2], aValue[3]])
                                );
      end;
      bstDcpPath:
      begin
        if AnsiCompareText(tempSourceFileExt,'dpk')=0 then
        begin
          ABSetXMLNodeValue(tempRootNode,
                            'DCC_DcpOutput','','',
                            'PropertyGroup','Condition','''$(Base)''!=''''',aValue[0]);
        end;
      end;
      bstBplPath:
      begin
        if AnsiCompareText(tempSourceFileExt,'dpk')=0 then
        begin
          ABSetXMLNodeValue(tempRootNode,
                            'DCC_BplOutput','','',
                            'PropertyGroup','Condition','''$(Base)''!=''''',aValue[0]);
        end;
      end;
      bstExePath:
      begin
        if AnsiCompareText(tempSourceFileExt,'dpr')=0 then
        begin
          ABSetXMLNodeValue(tempRootNode,
                            'DCC_ExeOutput','','',
                            'PropertyGroup','Condition','''$(Base)''!=''''',aValue[0]);
        end;
      end;
      bstBuildWithPackage:
      begin
        if AnsiCompareText(tempSourceFileExt,'dpr')=0 then
        begin
          ABSetXMLNodeValue(tempRootNode,
                            'UsePackages','','',
                            'PropertyGroup','Condition','''$(Base)''!=''''',ABBoolToStr(ABStrToBool(aValue[0])));
        end;
      end;
      bstBuildWithPackageList:
      begin
        if AnsiCompareText(tempSourceFileExt,'dpr')=0 then
        begin
          ABSetXMLNodeValue(tempRootNode,
                            'DCC_UsePackage','','',
                            'PropertyGroup','Condition','''$(Base)''!=''''',aValue[0]);
        end;
      end;
    end;
  end;
  procedure DodvDXE3;
  begin
    case aSetType of
      bstIncludeVerInfo:
      begin
        ABSetXMLNodeValue(tempRootNode,
                          'VerInfo_IncludeVerInfo','','',
                          'PropertyGroup','Condition','''$(Base)''!=''''',ABBoolToStr(ABStrToBool(aValue[0])));
      end;
      bstBuildIncVersion:
      begin
        ABSetXMLNodeValue(tempRootNode,
                          'VerInfo_AutoIncVersion','','',
                          'PropertyGroup','Condition','''$(Base)''!=''''',ABBoolToStr(ABStrToBool(aValue[0])));
      end;
      bstVersion:
      begin
        ABSetXMLNodeValue(tempRootNode,
                          'VerInfo_MajorVer','','',
                          'PropertyGroup','Condition','''$(Base)''!=''''',aValue[0]);
        ABSetXMLNodeValue(tempRootNode,
                          'VerInfo_MinorVer','','',
                          'PropertyGroup','Condition','''$(Base)''!=''''',aValue[1]);
        ABSetXMLNodeValue(tempRootNode,
                          'VerInfo_Release','','',
                          'PropertyGroup','Condition','''$(Base)''!=''''',aValue[2]);
        ABSetXMLNodeValue(tempRootNode,
                          'VerInfo_Build','','',
                          'PropertyGroup','Condition','''$(Base)''!=''''',aValue[3]);

        ABSetXMLNodeValue(tempRootNode,
                          'VerInfo_Keys','','',
                          'PropertyGroup','Condition','''$(Base)''!=''''',
                          Format('CompanyName=;FileDescription=;FileVersion=%s.%s.%s.%s;'+
                                'InternalName=;LegalCopyright=;LegalTrademarks=;OriginalFilename=;'+
                                'ProductName=;ProductVersion=1.0.0.0;Comments=', [aValue[0], aValue[1], aValue[2], aValue[3]])
                                );
      end;
      bstDcpPath:
      begin
        if AnsiCompareText(tempSourceFileExt,'dpk')=0 then
        begin
          ABSetXMLNodeValue(tempRootNode,
                            'DCC_DcpOutput','','',
                            'PropertyGroup','Condition','''$(Base)''!=''''',aValue[0]);
        end;
      end;
      bstBplPath:
      begin
        if AnsiCompareText(tempSourceFileExt,'dpk')=0 then
        begin
          ABSetXMLNodeValue(tempRootNode,
                            'DCC_BplOutput','','',
                            'PropertyGroup','Condition','''$(Base)''!=''''',aValue[0]);
        end;
      end;
      bstExePath:
      begin
        if AnsiCompareText(tempSourceFileExt,'dpr')=0 then
        begin
          ABSetXMLNodeValue(tempRootNode,
                            'DCC_ExeOutput','','',
                            'PropertyGroup','Condition','''$(Base)''!=''''',aValue[0]);
        end;
      end;
      bstBuildWithPackage:
      begin
        if AnsiCompareText(tempSourceFileExt,'dpr')=0 then
        begin
          ABSetXMLNodeValue(tempRootNode,
                            'UsePackages','','',
                            'PropertyGroup','Condition','''$(Base)''!=''''',ABBoolToStr(ABStrToBool(aValue[0])));
        end;
      end;
      bstBuildWithPackageList:
      begin
        if AnsiCompareText(tempSourceFileExt,'dpr')=0 then
        begin
          ABSetXMLNodeValue(tempRootNode,
                            'DCC_UsePackage','','',
                            'PropertyGroup','Condition','''$(Base)''!=''''',aValue[0]);
        end;
      end;
    end;
  end;
begin
  tempStrings:=TStringList.Create;
  //修改文件内容
  tempXmlDoc := TNativeXml.Create(nil);
  try
    //tempXmlDoc.XmlFormat := xfReadable;
    tempXmlDoc.LoadFromFile(aFileName);

    tempRootNode := tempXmlDoc.Root;
    tempSourceFileExt:=EmptyStr;
    tempNode := ABFindXMLNode(tempRootNode, 'Source','Name','MainSource',
                                            'Source','','');
    if tempNode <> nil then
    begin
      tempSourceFileExt:=ABGetFileExt(tempNode.Value);
    end
    else
    begin
      exit;
    end;
    tempNode := ABFindXMLNode(tempRootNode,'ProjectVersion','','',
                                           'PropertyGroup','','');
    if tempNode <> nil then
    begin
      tempDelphiVersion_Double:=StrToFloat(tempNode.Value);
      tempDelphiVersion:=ABGetDelphiVersionBydprojVersion(tempDelphiVersion_Double);
    end
    else
    begin
      exit;
    end;

    case tempDelphiVersion of
      dvD2009:
      begin
        DodvD2009;
      end;
      dvDXE2:
      begin
        DodvDXE2;
      end;
      dvDXE3:
      begin
        DodvDXE3;
      end;
    end;

    tempXmlDoc.SaveToFile(aFileName);

    if (tempDelphiVersion<>dvD2007) and
       (tempDelphiVersion<>dvD2009) and
       (tempDelphiVersion<>dvD2010) then
    begin
      tempStrings.LoadFromFile(aFileName);
      ABWriteTxt(aFileName,tempStrings.Text,tfUtf8);
    end;
  finally
    tempStrings.Free;
    tempXmlDoc.Free;
  end;
end;

procedure ABSetDelphiProjectinfos(aString: TStrings;aSetType:TABBatchSetDelphiProjectType;aValue:array of string;aProgressBar:TProgressBar=nil);
var
  i:LongInt;
  tempProgressVisable:Boolean;
begin
  tempProgressVisable:=false;
  if Assigned(aProgressBar) then
  begin
    tempProgressVisable := aProgressBar.Visible;

    if not tempProgressVisable then
      aProgressBar.Visible := True;
    aProgressBar.Min := 0;
    aProgressBar.Max := aString.Count;
  end;
  try
    for i:=0 to aString.Count-1 do
    begin
      if Assigned(aProgressBar) then
        aProgressBar.Position := aProgressBar.Position + 1;
      Application.ProcessMessages;

      ABSetDelphiProjectinfo(aString[i],aSetType,aValue);
    end;
  finally
    if Assigned(aProgressBar) then
    begin
      if not tempProgressVisable then
        aProgressBar.Visible := false;
      aProgressBar.Position := 0;
    end;
  end;
end;

procedure ABSetDelphiProjectinfo(aFileName: string;aSetType:TABBatchSetDelphiProjectType;aValue:array of string);
var
  tempFileExt :string;
begin
  if ABCheckFileExists(aFileName) then
  begin
    tempFileExt:=ABGetFileExt(aFileName);
    //D2009~DXE3的项目文件
    if AnsiCompareText(tempFileExt, 'dproj')=0 then
    begin
      ABSetDelphidproj(aFileName,aSetType,aValue);
    end
    //D7的项目文件
    else if
            (
             (AnsiCompareText(tempFileExt, 'dpr')=0) or
             (AnsiCompareText(tempFileExt, 'dpk')=0)
             ) and
            (ABCheckFileExists(ChangeFileExt(aFileName, '.dof'))) and
            (
             (AnsiCompareText(tempFileExt, 'dpr')=0) or
             (aSetType<>bstIncludeVerInfo) and (aSetType<>bstBuildWithPackage)  and (aSetType<>bstBuildWithPackageList)
             )  and
            (
             (AnsiCompareText(tempFileExt, 'dpr')=0) and (aSetType<>bstDcpPath) and (aSetType<>bstBplPath) or
             (AnsiCompareText(tempFileExt, 'dpk')=0) and (aSetType<>bstExePath)
             ) then
    begin
      aFileName:=ChangeFileExt(aFileName, '.dof');
      if ABCheckFileExists(aFileName) then
        ABSetDelphidof(aFileName,aSetType,aValue);
    end
    else
    begin
      exit;
    end;

    if  aSetType=bstVersion then
    begin
      aFileName:=ChangeFileExt(aFileName, '.res');
      if ABCheckFileExists(aFileName) then
        ABModifyResVer(aFileName,
                     StrToInt(aValue[0]),StrToInt(aValue[1]),StrToInt(aValue[2]),StrToInt(aValue[3]));
    end;
  end;
end;

function ABGetCortrolPropertyValue(aControl:TObject;aPropertyName:string):Variant;
begin
  Result:=EmptyStr;
  if AnsiCompareText(aPropertyName,'DataField')=0 then
  begin
    if ABIsPublishProp(aControl,'DataBinding.DataField') then
       Result:=ABGetPropValue(aControl,'DataBinding.DataField')
    else if ABIsPublishProp(aControl,'DataField') then
       Result:=ABGetPropValue(aControl,'DataField');
  end
  else if AnsiCompareText(aPropertyName,'Color')=0 then
  begin
    if ABIsPublishProp(aControl,'Style.TextColor') then
       Result:=ABGetPropValue(aControl,'Style.TextColor')
    else if ABIsPublishProp(aControl,'Color') then
       Result:=ABGetPropValue(aControl,'Color');
  end
  else if AnsiCompareText(aPropertyName,'ReadOnly')=0 then
  begin
    if ABIsPublishProp(aControl,'Properties.ReadOnly') then
       Result:=ABGetPropValue(aControl,'Properties.ReadOnly')
    else if ABIsPublishProp(aControl,'ReadOnly') then
       Result:=ABGetPropValue(aControl,'ReadOnly');
  end
end;

procedure ABSetCortrolPropertyValue(aControl:TObject;aPropertyName:string;aValue:Variant);
begin
  if AnsiCompareText(aPropertyName,'DataField')=0 then
  begin
    if ABIsPublishProp(aControl,'DataBinding.DataField') then
       ABSetPropValue(aControl,'DataBinding.DataField',aValue)
    else if ABIsPublishProp(aControl,'DataField') then
       ABSetPropValue(aControl,'DataField',aValue);
  end
  else if AnsiCompareText(aPropertyName,'Color')=0 then
  begin
    if (aControl is TcxLabel) or (aControl is TLabel)  then
    begin
      if ABIsPublishProp(aControl,'Style.TextColor') then
        ABSetPropValue(aControl,'Style.TextColor',aValue)
      else if ABIsPublishProp(aControl,'Color') then
        ABSetPropValue(aControl,'Color',aValue);
    end
    else
    begin
      if ABIsPublishProp(aControl,'Style.Color') then
        ABSetPropValue(aControl,'Style.Color',aValue)
      else if ABIsPublishProp(aControl,'Color') then
        ABSetPropValue(aControl,'Color',aValue);
    end;
  end
  else if AnsiCompareText(aPropertyName,'ReadOnly')=0 then
  begin
    if ABIsPublishProp(aControl,'Properties.ReadOnly') then
       ABSetPropValue(aControl,'Properties.ReadOnly',aValue)
    else if ABIsPublishProp(aControl,'ReadOnly') then
       ABSetPropValue(aControl,'ReadOnly',aValue);
  end
end;

procedure ABSavecxListBoxSelect(aCheckListBox: TcxCheckListBox);
var
  i: LongInt;
  tempPubFileName: string;
begin
  if (not Assigned(aCheckListBox.Owner)) then
    exit;
  if (aCheckListBox.Owner.Name=EmptyStr) or (aCheckListBox.Name=EmptyStr) then
    exit;

  tempPubFileName := ABDefaultValuePath+'PubDefaultValue.ini';
  for i := 0 to aCheckListBox.Count - 1 do
  begin
    ABWriteInI(aCheckListBox.Owner.Name+'_'+aCheckListBox.Name + '_Selects',
               aCheckListBox.Items[i].Text,ABBoolToStr(aCheckListBox.Items[i].Checked),tempPubFileName);
  end;
end;

procedure ABLoadcxListBoxSelect(aCheckListBox: TcxCheckListBox);
var
  i: LongInt;
  tempPubFileName: string;
  tempStrings:TStrings;
begin
  tempPubFileName := ABDefaultValuePath+'PubDefaultValue.ini';
  if not ABCheckFileExists(tempPubFileName) then
    exit;
  if (not Assigned(aCheckListBox.Owner)) then
    exit;
  if (aCheckListBox.Owner.Name=EmptyStr) or (aCheckListBox.Name=EmptyStr) then
    exit;

  tempStrings := TStringList.Create;
  try
    ABReadInI(aCheckListBox.Owner.Name+'_'+aCheckListBox.Name + '_Selects',tempPubFileName,tempStrings);
    if tempStrings.Count>0 then
    begin
      for i := 0 to aCheckListBox.Count - 1 do
      begin
        if tempStrings.IndexOfName(aCheckListBox.Items[i].Text)>=0 then
        begin
          aCheckListBox.Items[i].Checked:= ABStrToBool(tempStrings.Values[aCheckListBox.Items[i].Text]);
        end;
      end;
    end;
  finally
    tempStrings.Free;
  end;
end;

procedure ABSetcxListBoxSelect(aCheckListBox: TcxCheckListBox; aType: LongInt);
var
  i: LongInt;
begin
  for i := 0 to aCheckListBox.Count - 1 do
  begin
    case aType of
      0:
      begin
        if not aCheckListBox.Items[i].Checked then
          aCheckListBox.Items[i].Checked := true;
      end;
      1:
      begin
        if aCheckListBox.Items[i].Checked then
          aCheckListBox.Items[i].Checked := false;
      end;
      2:
      begin
        aCheckListBox.Items[i].Checked := not aCheckListBox.Items[i].Checked;
      end;
    end;
  end;
end;

procedure ABRefreshcxListBoxWidth(aListBox: TcxCheckListBox; aBeginCharNum: LongInt);
var
  i: LongInt;
begin
  if aBeginCharNum = 0 then
  begin
    for i := 0 to aListBox.Count - 1 do
    begin
      if aBeginCharNum < Length(aListBox.Items[i].Text) then
        aBeginCharNum := Length(aListBox.Items[i].Text);
    end;
  end;
  SendMessage(aListBox.Handle, LB_SETHORIZONTALEXTENT, aBeginCharNum * 9, 0);
end;


procedure ABLanguageSetComponentsText(adxStatusBar: TdxStatusBar);
var
  I: Integer;
begin
  for I := 0 to adxStatusBar.Panels.Count - 1 do
  begin
    if AnsiCompareText(adxStatusBar.Panels[i].PanelStyleClassName,'TdxStatusBarTextPanelStyle')=0 then
    begin
      adxStatusBar.Panels[i].Text:=ABLanguage.GetCodeStr(adxStatusBar.Panels[i].Text);
    end;
  end;
end;

procedure ABSetAutoIDValue(var thisViewInfo: TcxCustomGridIndicatorItemViewInfo; var thisCanvas:TcxCanvas; var thisDone: Boolean);
var
  iRowNum: integer;
  rtCell, rtCell1: TRect;
begin
  if (thisViewInfo is TcxGridIndicatorRowItemViewInfo) then
  begin
    iRowNum := TcxGridIndicatorRowItemViewInfo(thisViewInfo).GridRecord.Index + 1;

    rtCell := thisViewInfo.Bounds;
    rtCell1 := thisViewInfo.Bounds;
    rtCell1.Left := rtCell1.Left + 2;
    rtCell1.Top := rtCell1.Top + 3;

    thisCanvas.FillRect(rtCell);
    thisCanvas.DrawComplexFrame(rtCell, clWhite, cl3DDkShadow, cxBordersAll, 1);
    thisCanvas.Font.Color := clBlack;
    thisCanvas.Brush.Style := bsClear;
    thisCanvas.DrawTexT(inttostr(iRowNum), rtCell1, cxDontClip, true);

    thisDone := true;
  end;
end;

procedure ABSetTableViewSelect(aView:TcxGridTableView);
var
  tempcxGrid:TcxGrid;
begin
  if (not Assigned(aView)) then
    exit;

  tempcxGrid:=TcxGrid(aView.Control);
  if (not Assigned(tempcxGrid)) then
    exit;

  if (Assigned(tempcxGrid.Owner)) and
     (tempcxGrid.Owner is TABPubForm) and
     (not TABPubForm(tempcxGrid.Owner).FormCreateOk) then
    exit;

//  if tempcxGrid.CanFocus then
//    tempcxGrid.SetFocus;
  if not aView.Focused then
    aView.Focused:=true;

  ABSetcxDBDataControllerSelect(ABGetTableViewDataController(aView));
end;

procedure ABSetcxDBDataControllerSelect(aDataController:TcxCustomDataController);
var
  tempDataset:Tdataset;
begin
  if (not Assigned(aDataController)) then
    exit;

  if aDataController is TcxDBTreeListDataController then
  begin
    tempDataset:=TcxDBTreeListDataController(aDataController).DataSet;
  end
  else if aDataController is TcxDBDataController then
  begin
    tempDataset:=TcxDBDataController(aDataController).DataSet;
  end
  else
  begin
    exit;
  end;

  if (not Assigned(tempDataset)) then
    exit;

  if (aDataController.GetSelectedCount<=0) and
     (aDataController.rowcount>0) then
  begin
    aDataController.BeginUpdate;
    try
      if aDataController.FocusedRecordIndex=-1 then
      begin
        aDataController.SelectRows(0,0);
      end
      else
      begin
        aDataController.SelectRows(aDataController.FocusedRowIndex,
                                   aDataController.FocusedRowIndex);
      end;
    finally
      aDataController.EndUpdate;
    end;
  end;
end;

procedure ABSetTableViewSelect(aView:TcxGridTableView;aMultiSelectType:TABMultiSelectType);
var
  tempDataController:TcxDBDataController;
  tempcxGrid:TcxGrid;
begin
  if (not Assigned(aView)) then
    exit;

  tempcxGrid:=TcxGrid(aView.Control);
  if (not Assigned(tempcxGrid)) then
    exit;

  tempDataController:=ABGetTableViewDataController(aView);
  if (not Assigned(tempDataController)) and
     (not Assigned(tempDataController.DataSet)) then
    exit;

  if tempcxGrid.CanFocus then
    tempcxGrid.SetFocus;
  if not aView.Focused then
    aView.Focused:=true;

  tempDataController.BeginUpdate;
  try
    case aMultiSelectType of
      mtCur:
      begin
        tempDataController.ClearSelection;
        tempDataController.SelectRows(tempDataController.FocusedRowIndex,
                                      tempDataController.FocusedRowIndex);
      end;
      mtSelect:
      begin
        if tempDataController.GetSelectedCount<=0 then
        begin
          if tempDataController.FocusedRowIndex=-1 then
          begin
            tempDataController.SelectRows(0,0);
          end
          else
          begin
            tempDataController.SelectRows(tempDataController.FocusedRowIndex,
                                          tempDataController.FocusedRowIndex);
          end;
        end;
      end;
      mtCurToEnd:
      begin
        tempDataController.ClearSelection;
        tempDataController.SelectRows(tempDataController.FocusedRowIndex,
                                      tempDataController.RowCount-1);
      end;
      mtAll:
      begin
        tempDataController.SelectAll;
      end;
    end;
  finally
    tempDataController.EndUpdate;
  end;
end;

procedure ABSetColumnPropertiesClass(aView:TcxGridTableView);
var
  i:LongInt;
  tempField:Tfield;
begin
  for I := 0 to aView.ColumnCount - 1 do
  begin
    tempField:=ABGetColumnField(aView,i);
    if (not Assigned(aView.Columns[i].Properties)) and (Assigned(tempField)) then
    begin
      case tempField.DataType of
        ftString, ftWideString,ftFixedChar,{$IF (CompilerVersion>=18.5)} ftFixedWideChar, {$IFEND} ftGuid:
          aView.Columns[i].PropertiesClass := TcxTextEditProperties;
        ftMemo, ftFmtMemo{$IF (CompilerVersion>=18.5)} ,ftWideMemo {$IFEND}:
          aView.Columns[i].PropertiesClass := TcxMemoProperties;
        ftSmallint, ftInteger, ftWord,{$IF (CompilerVersion>=20.0)} ftLongWord,ftShortint, {$IFEND}ftLargeint, ftAutoInc:
          aView.Columns[i].PropertiesClass := TcxSpinEditProperties;
        ftBoolean:
          aView.Columns[i].PropertiesClass := TcxCheckBoxProperties;
        ftFloat{$IF (CompilerVersion>=20.0)} ,DB.ftExtended {$IFEND}:
          aView.Columns[i].PropertiesClass := TcxCalcEditProperties;
        ftCurrency, ftBCD,ftFMTBcd:
          aView.Columns[i].PropertiesClass := TcxCalcEditProperties;
        ftDate:
          aView.Columns[i].PropertiesClass := TcxDateEditProperties;
        ftDateTime,ftTimeStamp :
          begin
            aView.Columns[i].PropertiesClass := TcxDateEditProperties;
            TcxDateEditProperties(aView.Columns[i].Properties).Kind:= ckDateTime;
          end;
        ftTime:
          aView.Columns[i].PropertiesClass := TcxTimeEditProperties;
        ftBlob:
        begin
          aView.Columns[i].PropertiesClass := TcxBlobEditProperties;
          TcxBlobEditProperties(aView.Columns[i].Properties).BlobEditKind := bekPict;
          TcxBlobEditProperties(aView.Columns[i].Properties).PictureGraphicClassName:='TJPEGImage';
        end;
      else
        aView.Columns[i].PropertiesClass := TcxTextEditProperties;
      end;
    end;
  end;
end;

procedure ABCreateColumns(aView:TcxGridTableView;aFieldNames:string);
var
  tempDBDataController:TcxGridDBDataController;
  tempView:TcxGridTableView;
  tempColumn:TcxGridColumn;
  i:longint;
begin
  tempDBDataController:=ABGetTableViewDataController(aView);
  if (Assigned(tempDBDataController)) and
     (Assigned(tempDBDataController.DataSet)) and
     (tempDBDataController.DataSet.Active) then
  begin
    tempView:=aView;
    if tempView.columncount>0 then
      tempView.ClearItems;

    for i := 0 to tempDBDataController.DataSet.FieldCount-1 do
    begin
      if (aFieldNames=EmptyStr) or (ABPos(','+tempDBDataController.DataSet.Fields[i].FieldName+',',','+aFieldNames+',')> 0) then
      begin
        tempColumn:=tempView.CreateColumn;
        if tempView.Name<>EmptyStr then
          tempColumn.Name:=tempView.Name+ABStrToName(tempDBDataController.DataSet.Fields[i].FieldName);
        TcxGridItemDBDataBinding(tempColumn.DataBinding).FieldName:=tempDBDataController.DataSet.Fields[i].FieldName;
      end;
    end;
  end;
end;

procedure ABSetdxBarItemsVisible(aItemLinks:TdxBarItemLinks;aVisible:TdxBarItemVisible;aSyncClient:boolean);
var
  i:LongInt;
begin
  for I := aItemLinks.Count - 1 downto 0 do
  begin
    if (aSyncClient) and (aItemLinks[i].Item is TdxBarSubItem)  then
      ABSetdxBarItemsVisible(TdxBarSubItem(aItemLinks[i].Item).ItemLinks,aVisible);

    if Assigned(aItemLinks[i].Item) then
      aItemLinks[i].Item.Visible:=aVisible;
  end;
end;

procedure ABCleardxBarItems(aItemLinks:TdxBarItemLinks;aFreeItem:Boolean);
var
  i:LongInt;
begin
  if aFreeItem then
  begin
    for I := aItemLinks.Count - 1 downto 0 do
    begin
      if aItemLinks[i].Item is TdxBarSubItem then
        ABCleardxBarItems(TdxBarSubItem(aItemLinks[i].Item).ItemLinks,aFreeItem);

      if Assigned(aItemLinks[i].Item) then
        aItemLinks[i].Item.Free;
    end;
    aItemLinks.clear;
  end
  else
  begin
    aItemLinks.clear;
  end;
end;

procedure ABCopyRadioGroupItems(aFromRadioGroup: TRadioGroup;aToRadioGroup: TcxRadioGroup;aClearToRadioGroupOld:Boolean);
var
  i:LongInt;
begin
  aFromRadioGroup.Items.BeginUpdate;
  aToRadioGroup.Properties.Items.BeginUpdate;
  try
    if aClearToRadioGroupOld then
      aToRadioGroup.Properties.Items.Clear;

    aToRadioGroup.Height:=aFromRadioGroup.Height;

    for I := 0 to aFromRadioGroup.Items.Count-1 do
    begin
      aToRadioGroup.Properties.Items.Add;
      aToRadioGroup.Properties.Items[aToRadioGroup.Properties.Items.Count-1].Caption:=aFromRadioGroup.Items[i];
    end;
    aToRadioGroup.Properties.Columns:=aFromRadioGroup.Columns;

    if (aToRadioGroup.Properties.Items.count-1>=aFromRadioGroup.ItemIndex) and (aFromRadioGroup.ItemIndex>=0) then
      aToRadioGroup.ItemIndex:=aFromRadioGroup.ItemIndex;
  finally
    aFromRadioGroup.Items.EndUpdate;
    aToRadioGroup.Properties.Items.EndUpdate;
  end;
end;

procedure ABExportCxgridsToExcel(acxGrids: array of TcxGrid; aSheetNames: array of string);
var
  temSavedialog1: TSaveDialog;
  i: LongInt;
  tempFileName: array of string;
  tempErrMsg:string;
begin
  temSavedialog1 := TSaveDialog.Create(nil);
  try
    temSavedialog1.Filter := '*.xls|*.xls';
    if temSavedialog1.Execute then
    begin
      SetLength(tempFileName,High(acxGrids)+1);
      for i := Low(acxGrids) to High(acxGrids) do
      begin
        tempFileName[i]:= extractfilepath(temSavedialog1.FileName)+ extractfilename(ChangeFileExt(temSavedialog1.FileName, ''))+'_'+inttostr(i + 1)+'.xls';
        ExportGridToExcel(tempFileName[i], acxGrids[i], true, true)
      end;
      ABMergerExcel_OLE(tempFileName,[],temSavedialog1.FileName,aSheetNames,tempErrMsg);
    end;
  finally
    temSavedialog1.free;
  end;
end;

function ABGetSQL_MultiSelect(
                           aDataController:TcxCustomDataController;
                           aMultiSelectType:TABMultiSelectType;
                           aTableName:string;
                           aKeyFieldnames:array of string;
                           aUpdateFieldnames:array of string;
                           aInsertFieldnames:array of string;

                           aReplaceFieldStrings:TStrings;
                           aReplaceFieldDatasets:array of TDataSet
                           ):string;
var
  j: Integer;
  tempOldBeforeScroll:TDataSetNotifyEvent;
  tempOldAfterScroll:TDataSetNotifyEvent;
  tempCurProgress: Integer;
  tempDataset: TDataset;

  procedure DoSelect;
  var
    I: Integer;
  begin
    ABPubManualProgressBar_ThreadU.ABRunProgressBar(tempCurProgress,aDataController.GetSelectedCount);
    aDataController.BeginUpdate;
    try
      for I := 0 to aDataController.GetSelectedCount- 1 do
      begin
        tempCurProgress:=tempCurProgress+1;

        j:= aDataController.GetSelectedRowIndex(i);
        aDataController.FocusedRecordIndex:=aDataController.GetRowInfo(j).RecordIndex;

        abaddstr(result,ABGetSQLByDataset( tempDataset,
                                           aTableName,
                                           aKeyFieldnames,
                                           aUpdateFieldnames,
                                           aInsertFieldnames,

                                           aReplaceFieldStrings,
                                           aReplaceFieldDatasets
                                          ),
             ABEnterWrapStr);
      end;
    finally
      aDataController.EndUpdate;
      ABPubManualProgressBar_ThreadU.ABStopProgressBar;
    end;
  end;
begin
  result:= emptystr;
  if (not Assigned(aDataController)) then
    exit;

  tempDataset:=nil;
  if aDataController is TcxDBTreeListDataController then
  begin
    tempDataset:=TcxDBTreeListDataController(aDataController).DataSet;
  end
  else if aDataController is TcxDBDataController then
  begin
    tempDataset:=TcxDBDataController(aDataController).DataSet;
  end
  else
  begin
    exit;
  end;

  if (tempDataset.IsEmpty) then
    exit;

  if (aDataController.GetSelectedCount=1) and (aMultiSelectType=mtSelect) then
    aMultiSelectType:= mtCur;

  ABBackAndStopDatasetEvent(tempDataset,tempOldBeforeScroll,tempOldAfterScroll);
  try
    case aMultiSelectType of
      mtCur:
      begin
        abaddstr(result,ABGetSQLByDataset( tempDataset,
                                           aTableName,
                                           aKeyFieldnames,
                                           aUpdateFieldnames,
                                           aInsertFieldnames,

                                           aReplaceFieldStrings,
                                           aReplaceFieldDatasets
                                          ),
                 ABEnterWrapStr);
      end;
      mtSelect:
      begin
        DoSelect;
      end;
      mtCurToEnd:
      begin
        aDataController.ClearSelection;
        aDataController.SelectRows(aDataController.FocusedRowIndex,aDataController.RowCount-1);
        DoSelect;
      end;
      mtAll:
      begin
        ABPubManualProgressBar_ThreadU.ABRunProgressBar(tempCurProgress,tempDataset.RecordCount);
        tempDataset.DisableControls;
        try
          tempDataset.First;
          while not tempDataset.EOF do
          begin
            tempCurProgress:=tempCurProgress+1;

            abaddstr(result,ABGetSQLByDataset( tempDataset,
                                               aTableName,
                                               aKeyFieldnames,
                                               aUpdateFieldnames,
                                               aInsertFieldnames,

                                               aReplaceFieldStrings,
                                               aReplaceFieldDatasets
                                              ),
                 ABEnterWrapStr);

            tempDataset.Next;
          end;
        finally
          tempDataset.EnableControls;
          ABPubManualProgressBar_ThreadU.ABStopProgressBar;
        end;
      end;
    end;
  finally
    ABUnBackDatasetEvent(tempDataset,tempOldBeforeScroll,tempOldAfterScroll);
  end;
end;

function ABGetFieldValue_MultiSelect(
                          aDataController:TcxCustomDataController;
                          aMultiSelectType:TABMultiSelectType;
                          aFieldName:string;
                          aSpaceFlag:string
                           ):string;
var
  j: Integer;
  tempCurProgress: Integer;
  tempOldBeforeScroll:TDataSetNotifyEvent;
  tempOldAfterScroll:TDataSetNotifyEvent;
  tempDataset: TDataset;
  function GetFieldValue_Cur:string;
  var
    tempField:Tfield;
  begin
    result:=EmptyStr;
    tempField:=tempDataset.FindField(aFieldName);
    if Assigned(tempField) then
    begin
      result:= tempField.AsString;
      if (tempField.DataType in ABStrAndMemoDataType) then
      begin
        result:=QuotedStr(result);
      end;
    end;
  end;
  procedure DoSelect;
  var
    I: Integer;
  begin
    ABPubManualProgressBar_ThreadU.ABRunProgressBar(tempCurProgress,aDataController.GetSelectedCount);
    aDataController.BeginUpdate;
    if tempDataset is TFireDACQuery then
      TFireDACQuery(tempDataset).BeginUpdate;
    try
      for I := 0 to aDataController.GetSelectedCount- 1 do
      begin
        tempCurProgress:=tempCurProgress+1;

        j:= aDataController.GetSelectedRowIndex(i);
        aDataController.FocusedRecordIndex:=aDataController.GetRowInfo(j).RecordIndex;

        ABAddstr(result,GetFieldValue_Cur,aSpaceFlag);
      end;
    finally
      if tempDataset is TFireDACQuery then
        TFireDACQuery(tempDataset).EndUpdate;
      aDataController.EndUpdate;
      ABPubManualProgressBar_ThreadU.ABStopProgressBar;
    end;
  end;
begin
  result:= emptystr;
  if (not Assigned(aDataController)) then
    exit;

  tempDataset:=nil;
  if aDataController is TcxDBTreeListDataController then
  begin
    tempDataset:=TcxDBTreeListDataController(aDataController).DataSet;
  end
  else if aDataController is TcxDBDataController then
  begin
    tempDataset:=TcxDBDataController(aDataController).DataSet;
  end
  else
  begin
    exit;
  end;

  if (tempDataset.IsEmpty) then
    exit;

  if (aDataController.GetSelectedCount=1) and (aMultiSelectType=mtSelect) then
    aMultiSelectType:= mtCur;

  ABBackAndStopDatasetEvent(tempDataset,tempOldBeforeScroll,tempOldAfterScroll);
  try
    case aMultiSelectType of
      mtCur:
      begin
        result:= GetFieldValue_Cur;
      end;
      mtSelect:
      begin
        DoSelect;
      end;
      mtCurToEnd:
      begin
        aDataController.ClearSelection;
        aDataController.SelectRows(aDataController.FocusedRowIndex,aDataController.RowCount-1);
        DoSelect;
      end;
      mtAll:
      begin
        ABPubManualProgressBar_ThreadU.ABRunProgressBar(tempCurProgress,tempDataset.RecordCount);
        tempDataset.DisableControls;
        try
          tempDataset.First;
          while not tempDataset.EOF do
          begin
            tempCurProgress:=tempCurProgress+1;

            ABAddstr(result,GetFieldValue_Cur);

            tempDataset.Next;
          end;
        finally
          tempDataset.EnableControls;
          ABPubManualProgressBar_ThreadU.ABStopProgressBar;
        end;
      end;
    end;
  finally
    ABUnBackDatasetEvent(tempDataset,tempOldBeforeScroll,tempOldAfterScroll);
  end;
end;

procedure Do_ABSetFieldValue_CurRow(
                          aDataSet:TDataSet;

                          aFieldNames:array of string;
                          aValuesOrFieldNamesOrFieldNameOperations:array of Variant;
                          aValuesIsCale:array of Boolean;

                          aFirstPYFromFieldNames:array of string;
                          aFirstPYToFieldNames:array of string;
                          aAllPYFromFieldNames:array of string;
                          aAllPYToFieldNames:array of string;
                          aOutKeyFieldName:string;
                          aOutStrings:Tstrings
                           );
var
  I: Integer;
  tempEdit,tempDO:boolean;
  tempField:TField;

  tempCurFieldValues,
  tempFieldName: string;
  tempFieldValue: Variant;
  tempFieldIsCale,
  tempDoDataset,
  tempError:Boolean;
  procedure CaleFieldValue;
  var
    tempResult:string;
  begin
    tempFieldValue:=ABTransactQL(tempFieldValue,[aDataSet]);
    tempResult:=ABExpressionEval(tempFieldValue,tempError);
    if tempError then
    begin
      tempResult:=ABExecScript(tempFieldValue)
    end;
    tempFieldValue:=tempResult;
  end;
begin
  tempDO:=false;
  tempEdit:=false;
  tempDoDataset:=(aOutKeyFieldName=emptystr);

  if (tempDoDataset) then
  begin
    if (aDataSet.State in [dsBrowse]) then
    begin
      tempEdit:=true;
      aDataSet.Edit;
    end;
  end
  else
  begin
    tempCurFieldValues:=aDataSet.FindField(aOutKeyFieldName).AsString;
  end;

  for I:= Low(aFieldNames) to High(aFieldNames) do
  begin
    tempFieldName:=aFieldNames[i];
    tempFieldValue:=aValuesOrFieldNamesOrFieldNameOperations[i];
    if i<=High(aValuesIsCale) then
      tempFieldIsCale:= aValuesIsCale[i]
    else
      tempFieldIsCale:=false;

    if tempFieldIsCale then
      CaleFieldValue;

    tempField:=aDataSet.FindField(tempFieldValue);
    if Assigned(tempField) then
    begin
      tempFieldValue:=tempField.Value;
    end;
    if tempDoDataset then
    begin
      aDataSet.FindField(tempFieldName).Value:=tempFieldValue;
    end
    else
    begin
      tempCurFieldValues:=tempCurFieldValues+char(9)+VarToStrDef(tempFieldValue,emptystr);
    end;

    if not tempDO then
      tempDO:=true;
  end;
  for I:= Low(aFirstPYFromFieldNames) to High(aFirstPYFromFieldNames) do
  begin
    tempFieldValue:=ABGetFirstPy(AnsiString(aDataSet.FindField(aFirstPYFromFieldNames[i]).AsString));
    if tempDoDataset then
    begin
      aDataSet.FindField(aFirstPYToFieldNames[i]).AsString:=tempFieldValue;
    end
    else
    begin
      tempCurFieldValues:=tempCurFieldValues+char(9)+tempFieldValue;
    end;
    if not tempDO then
      tempDO:=true;
  end;
  for I:= Low(aAllPYFromFieldNames) to High(aAllPYFromFieldNames) do
  begin
    tempFieldValue:=ABGetAllPy(aDataSet.FindField(aAllPYFromFieldNames[i]).AsString);
    if tempDoDataset then
    begin
      aDataSet.FindField(aAllPYToFieldNames[i]).AsString:=tempFieldValue;
    end
    else
    begin
      tempCurFieldValues:=tempCurFieldValues+char(9)+tempFieldValue;
    end;
    if not tempDO then
      tempDO:=true;
  end;

  if (tempDoDataset) then
  begin
    if tempEdit then
    begin
      if tempDO then
        aDataSet.Post
      else
        aDataSet.Cancel;
    end;
  end
  else
  begin
    aOutStrings.Add(tempCurFieldValues);
  end;
end;

procedure Do_ABSetFieldValue_AllRow(
                          aDataSet:TDataSet;

                          aFieldNames:array of string;
                          aValuesOrFieldNamesOrFieldNameOperations:array of Variant;
                          aValuesIsCale:array of Boolean;

                          aFirstPYFromFieldNames:array of string;
                          aFirstPYToFieldNames:array of string;
                          aAllPYFromFieldNames:array of string;
                          aAllPYToFieldNames:array of string;
                          aOutKeyFieldName:string;
                          aOutStrings:Tstrings
                           );
var
  tempCurProgress: Integer;
begin
  ABPubManualProgressBar_ThreadU.ABRunProgressBar(tempCurProgress,aDataSet.RecordCount);
  aDataSet.DisableControls;
  try
    aDataSet.FIRST;
    while not aDataSet.EOF do
    begin
      tempCurProgress:=tempCurProgress+1;

      Do_ABSetFieldValue_CurRow(
                          aDataSet,
                          aFieldNames,
                          aValuesOrFieldNamesOrFieldNameOperations,
                          aValuesIsCale,

                          aFirstPYFromFieldNames,
                          aFirstPYToFieldNames,
                          aAllPYFromFieldNames,
                          aAllPYToFieldNames,

                          aOutKeyFieldName,
                          aOutStrings
                          );
      aDataSet.Next;
    end;
  finally
    aDataSet.EnableControls;
    ABPubManualProgressBar_ThreadU.ABStopProgressBar;
  end;
end;

procedure Do_ABSetFieldValue_MultiSelect(
                          aDataController:TcxCustomDataController;
                          aMultiSelectType:TABMultiSelectType;

                          aFieldNames:array of string;
                          aValuesOrFieldNamesOrFieldNameOperations:array of Variant;
                          aValuesIsCale:array of Boolean;

                          aFirstPYFromFieldNames:array of string;
                          aFirstPYToFieldNames:array of string;
                          aAllPYFromFieldNames:array of string;
                          aAllPYToFieldNames:array of string;

                          aFastMode:boolean;
                          aKeyFieldName:string;
                          aAddSQL:string
                           );
var
  ii:longint;
  tempCurProgress: Integer;
  tempDataset: TDataset;

  tempIntArray:array of LongInt;
  tempIntArrayCount:LongInt;
  tempOldBeforeScroll:TDataSetNotifyEvent;
  tempOldAfterScroll:TDataSetNotifyEvent;

  tempList: TStrings;
  tempName:string;
  tempFieldConst:string;
  tempVarTableName:string;
  tempFileName:string;
  tempConnName:string;

  function GetFieldConst(aField:TField):string;
  begin
    result:=emptystr;
    if aField.DataType  in ABIntDataType then
      result:='temp'+aField.FieldName+' int '
    else if aField.DataType  in ABDecimalDataType then
      result:='temp'+aField.FieldName+' decimal(18, 4) '
    else
      result:='temp'+aField.FieldName+' varchar(100) ';
  end;
  procedure Doselect;
  var
    I,j: Integer;
  begin
    aFastMode:= aDataController.GetSelectedCount>100;
    tempIntArrayCount:=0;
    SetLength(tempIntArray,tempIntArrayCount);
    ABPubManualProgressBar_ThreadU.ABRunProgressBar(tempCurProgress,aDataController.GetSelectedCount);
    aDataController.BeginUpdate;
    try
      for I := 0 to aDataController.GetSelectedCount- 1 do
      begin
        tempCurProgress:=tempCurProgress+1;
        j:= aDataController.GetSelectedRowIndex(i);
        j:=aDataController.GetRowInfo(j).RecordIndex;
        if j+1<=tempDataset.RecordCount then
        begin
          tempIntArrayCount:=tempIntArrayCount+1;
          SetLength(tempIntArray,tempIntArrayCount);
          tempIntArray[tempIntArrayCount-1]:=j;
        end;
      end;
    finally
      aDataController.EndUpdate;
      ABPubManualProgressBar_ThreadU.ABStopProgressBar;
    end;

    ABIntArraySort(tempIntArray);
    ABPubManualProgressBar_ThreadU.ABRunProgressBar(tempCurProgress,high(tempIntArray)+1);
    tempDataset.DisableControls;
    try
      for I :=  high(tempIntArray) downto Low(tempIntArray) do
      begin
        tempCurProgress:=tempCurProgress+1;

        tempDataset.RecNo:=tempIntArray[i]+1;

        DO_ABSetFieldValue_CurRow(
                            tempDataset,
                            aFieldNames,
                            aValuesOrFieldNamesOrFieldNameOperations,
                            aValuesIsCale,

                            aFirstPYFromFieldNames,
                            aFirstPYToFieldNames,
                            aAllPYFromFieldNames,
                            aAllPYToFieldNames,

                            ABIIF(aFastMode,aKeyFieldName,emptystr),
                            tempList
                            );
      end;
    finally
      tempDataset.EnableControls;
      ABPubManualProgressBar_ThreadU.ABStopProgressBar;
    end;
  end;
begin
  if (not Assigned(aDataController)) then
    exit;

  tempDataset:=nil;
  if aDataController is TcxDBTreeListDataController then
  begin
    tempDataset:=TcxDBTreeListDataController(aDataController).DataSet;
  end
  else if aDataController is TcxDBDataController then
  begin
    tempDataset:=TcxDBDataController(aDataController).DataSet;
  end
  else
  begin
    exit;
  end;

  if (tempDataset.IsEmpty) then
    exit;

  ABSetcxDBDataControllerSelect(aDataController);

  if (aDataController.GetSelectedCount=1) and (aMultiSelectType=mtSelect) then
    aMultiSelectType:= mtCur;


  if tempDataset is TFireDACQuery then
    TFireDACQuery(tempDataset).BeginUpdate;
  ABBackAndStopDatasetEvent(tempDataset,tempOldBeforeScroll,tempOldAfterScroll);
  tempList:= TStringList.Create;
  try
    case aMultiSelectType of
      mtCur:
      begin
        aFastMode:= False;
        DO_ABSetFieldValue_CurRow(
                            tempDataset,
                            aFieldNames,
                            aValuesOrFieldNamesOrFieldNameOperations,
                            aValuesIsCale,

                            aFirstPYFromFieldNames,
                            aFirstPYToFieldNames,
                            aAllPYFromFieldNames,
                            aAllPYToFieldNames ,

                            '',
                            nil
                            );
      end;
      mtSelect:
      begin
        Doselect;
      end;
      mtCurToEnd:
      begin
        aDataController.ClearSelection;
        aDataController.SelectRows(aDataController.FocusedRowIndex,aDataController.RowCount-1);

        Doselect;
      end;
      mtAll:
      begin
        aFastMode:= (tempDataset.RecordCount>100);
        DO_ABSetFieldValue_AllRow(
                            tempDataset,
                            aFieldNames,
                            aValuesOrFieldNamesOrFieldNameOperations,
                            aValuesIsCale,

                            aFirstPYFromFieldNames,
                            aFirstPYToFieldNames,
                            aAllPYFromFieldNames,
                            aAllPYToFieldNames,

                            ABIIF(aFastMode,aKeyFieldName,emptystr),
                            tempList
                            );
      end;
    end;

    if aFastMode then
    begin
      tempConnName:=ABGetPropValue(tempDataset,'ConnName');
      if tempConnName<>emptystr then
      begin
        //为了加快效率，运用先加入临时表，再对临时表操作的方法
        tempName :='SetFieldValue';
        tempVarTableName:='##temp'+tempName+'_'+ABStrToName(ABPubUser.Code);
        tempFileName:=ABTempPath+'temp'+tempName+'.txt';
        tempList.SaveToFile(tempFileName);

        tempFieldConst:=emptystr;
        for ii := low(aFieldNames) to high(aFieldNames) do
        begin
          ABAddstr(tempFieldConst,GetFieldConst(tempDataset.FindField(aFieldNames[ii])));
        end;
        for ii := low(aFirstPYToFieldNames) to high(aFirstPYToFieldNames) do
        begin
          ABAddstr(tempFieldConst,GetFieldConst(tempDataset.FindField(aFirstPYToFieldNames[ii])));
        end;
        for ii := low(aAllPYToFieldNames) to high(aAllPYToFieldNames) do
        begin
          ABAddstr(tempFieldConst,GetFieldConst(tempDataset.FindField(aAllPYToFieldNames[ii])));
        end;
        try
          ABExecSQL(tempConnName,
                    //导入数据到临时表
                    ' set nocount on '+ABEnterWrapStr+
                    ' if exists (select * from tempdb.dbo.sysobjects where id = object_id(N''tempdb.[dbo].['+tempVarTableName+']''))  '+ABEnterWrapStr+
                    '   drop table '+tempVarTableName+'                                                                               '+ABEnterWrapStr+
                    ' create table  '+tempVarTableName+'(tempGuid varchar(100),'+tempFieldConst+')                             '+ABEnterWrapStr,
                    []);
          ABExecSQL(tempConnName,
                    ' execute master..xp_cmdshell ''bcp "'+tempVarTableName+'" in "'+tempFileName+'" '+
                    '   -S"'+ABGetConnInfoByConnName(tempConnName).Conninfo.Host+'" '+
                      ABIIF(ABGetConnInfoByConnName(tempConnName).Conninfo.UserName<>emptystr,
                            '   -U"'+ABGetConnInfoByConnName(tempConnName).Conninfo.UserName+'" '+
                            '   -P"'+ABUnDOPassWord(ABGetConnInfoByConnName(tempConnName).Conninfo.Password)+'" ',
                            ' -T ')+
                    '   -c -a65535 '+
                    '   -h"TABLOCK'' '+ABEnterWrapStr+

                    //将临时表不存在计划明细中的数据导入
                    aAddSQL+
                    ' set nocount off                                                                                                                                       ',
                    []);
        finally
          ABReFreshQuery(tempDataset,aKeyFieldName);
        end;
      end;
    end;
  finally
    ABUnBackDatasetEvent(tempDataset,tempOldBeforeScroll,tempOldAfterScroll);
    if tempDataset is TFireDACQuery then
      TFireDACQuery(tempDataset).EndUpdate;
    tempList.Free;
  end;
end;

//批量修改TcxGridTableView中选择记录字段的值
procedure ABSetFieldValue_MultiSelect(
                          aDataController:TcxCustomDataController;
                          aMultiSelectType:TABMultiSelectType;

                          aFieldNames:array of string;
                          //字段值可以是指定字段名的字段值，也可以是固定的值，也可以是字段名与+-*/进行连结后的字段组合值
                          aValuesOrFieldNamesOrFieldNameOperations:array of Variant;
                          aValuesIsCale:array of Boolean;
                          aFastMode:boolean;
                          aKeyFieldName:string;
                          aAddSQL:string
                           );
begin
  Do_ABSetFieldValue_MultiSelect(
                          aDataController,
                          aMultiSelectType,
                          aFieldNames,
                          aValuesOrFieldNamesOrFieldNameOperations,
                          aValuesIsCale,

                          [],
                          [],
                          [],
                          [],

                          aFastMode,
                          aKeyFieldName,
                          aAddSQL
                          );
end;

procedure ABSetFieldValue_MultiSelect(
                          aDataController:TcxCustomDataController;
                          aObject,
                          aSetCurObject,aSetSelectObject,aSetCurToEndObject,aSetAllObject:TObject;

                          aFieldNames:array of string;
                          aValuesOrFieldNamesOrFieldNameOperations:array of Variant;
                          aValuesIsCale:array of Boolean;
                          aFastMode:boolean;
                          aKeyFieldName:string;
                          aAddSQL:string
                           );
begin
  if aObject=aSetCurObject then
  begin
    ABSetFieldValue_MultiSelect(aDataController,
                                mtCur,
                                aFieldNames,aValuesOrFieldNamesOrFieldNameOperations,aValuesIsCale,

                                aFastMode,
                                aKeyFieldName,
                                aAddSQL
                                );
  end
  else if aObject=aSetSelectObject then
  begin
    ABSetFieldValue_MultiSelect(aDataController,
                                mtSelect,
                                aFieldNames,aValuesOrFieldNamesOrFieldNameOperations,aValuesIsCale,

                                aFastMode,
                                aKeyFieldName,
                                aAddSQL
                                );
  end
  else if aObject=aSetCurToEndObject then
  begin
    ABSetFieldValue_MultiSelect(aDataController,
                                mtCurToEnd,
                                aFieldNames,aValuesOrFieldNamesOrFieldNameOperations,aValuesIsCale,

                                aFastMode,
                                aKeyFieldName,
                                aAddSQL
                                );
  end
  else if aObject=aSetAllObject then
  begin
    ABSetFieldValue_MultiSelect(aDataController,
                                mtAll,
                                aFieldNames,aValuesOrFieldNamesOrFieldNameOperations,aValuesIsCale,

                                aFastMode,
                                aKeyFieldName,
                                aAddSQL
                                );
  end;
end;

procedure ABSetFieldValue_MultiSelect(
                          aDataController:TcxCustomDataController;
                          aMultiSelectType:TABMultiSelectType;

                          aFirstPYFromFieldNames:array of string;
                          aFirstPYToFieldNames:array of string;
                          aAllPYFromFieldNames:array of string;
                          aAllPYToFieldNames:array of string;
                          aFastMode:boolean;
                          aKeyFieldName:string;
                          aAddSQL:string
                           );
begin
  Do_ABSetFieldValue_MultiSelect(
                          aDataController,
                          aMultiSelectType,
                          [],
                          [],
                          [],

                          aFirstPYFromFieldNames,
                          aFirstPYToFieldNames,
                          aAllPYFromFieldNames,
                          aAllPYToFieldNames,

                          aFastMode,
                          aKeyFieldName,
                          aAddSQL
                          );

end;

procedure ABSetFieldValue_MultiSelect(
                          aDataController:TcxCustomDataController;

                          aObject,
                          aSetCurObject,aSetSelectObject,aSetCurToEndObject,aSetAllObject:TObject;

                          aFirstPYFromFieldNames:array of string;
                          aFirstPYToFieldNames:array of string;
                          aAllPYFromFieldNames:array of string;
                          aAllPYToFieldNames:array of string;
                          aFastMode:boolean;
                          aKeyFieldName:string;
                          aAddSQL:string
                           );
begin
  if aObject=aSetCurObject then
  begin
    ABSetFieldValue_MultiSelect(aDataController,
                                mtCur,
                                aFirstPYFromFieldNames,aFirstPYToFieldNames,aAllPYFromFieldNames,aAllPYToFieldNames,

                                aFastMode,
                                aKeyFieldName,
                                aAddSQL
                                );
  end
  else if aObject=aSetSelectObject then
  begin
    ABSetFieldValue_MultiSelect(aDataController,
                                mtSelect,
                                aFirstPYFromFieldNames,aFirstPYToFieldNames,aAllPYFromFieldNames,aAllPYToFieldNames,

                                aFastMode,
                                aKeyFieldName,
                                aAddSQL
                                );
  end
  else if aObject=aSetCurToEndObject then
  begin
    ABSetFieldValue_MultiSelect(aDataController,
                                mtCurToEnd,
                                aFirstPYFromFieldNames,aFirstPYToFieldNames,aAllPYFromFieldNames,aAllPYToFieldNames,

                                aFastMode,
                                aKeyFieldName,
                                aAddSQL
                                );
  end
  else if aObject=aSetAllObject then
  begin
    ABSetFieldValue_MultiSelect(aDataController,
                                mtAll,
                                aFirstPYFromFieldNames,aFirstPYToFieldNames,aAllPYFromFieldNames,aAllPYToFieldNames,

                                aFastMode,
                                aKeyFieldName,
                                aAddSQL
                                );
  end;
end;

procedure ABDelNotCaleAndNotLinkFieldColumns(aView:TcxGridTableView);
var
  i:longint;
begin
  if (Assigned(aView))  then
  begin
    for i := aView.columncount-1 downto  0 do
    begin
      if (not ABIsLinkField(aView.Columns[i])) and
         (not ABIsCaleColumn(aView.Columns[i])) then
      begin
        aView.Columns[i].Free;
      end;
    end;
  end;
end;

procedure ABSetAutoIDWidth(aTableView: TcxCustomGridTableView);
begin
  if (aTableView.DataController.RecordCount<>0) and
     (TcxGridTableOptionsView(aTableView.OptionsView).Indicator) and
     (TcxGridTableOptionsView(aTableView.OptionsView).IndicatorWidth<>5+Length(IntToStr(aTableView.DataController.RecordCount))*7) then
    TcxGridTableOptionsView(aTableView.OptionsView).IndicatorWidth:=5+Length(IntToStr(aTableView.DataController.RecordCount))*7;
end;

function ABGetColumnField(aTableItem: TcxCustomGridTableItem):TField;
begin
  Result:=nil;
  if aTableItem is TcxGridDBColumn then
    Result:=TcxGridDBColumn(aTableItem).DataBinding.Field
  else if aTableItem is TcxGridDBBandedColumn then
    Result:=TcxGridDBBandedColumn(aTableItem).DataBinding.Field;
end;

function ABGetTableViewDataSource(aTableView: TcxCustomGridTableView):TDataSource;
begin
  Result:=nil;
  if aTableView is TcxGridDBBandedTableView then
    Result:=TcxGridDBBandedTableView(aTableView).DataController.DataSource
  else if aTableView is TcxGridDBTableView then
    Result:=TcxGridDBTableView(aTableView).DataController.DataSource;
end;

function ABGetColumnDataSource(aTableItem: TcxCustomGridTableItem):TDataSource;
begin
  Result:=nil;
  if aTableItem is TcxGridDBColumn then
    Result:=TcxGridDBColumn(aTableItem).DataBinding.DataController.DataSource
  else if aTableItem is TcxGridDBBandedColumn then
    Result:=TcxGridDBBandedColumn(aTableItem).DataBinding.DataController.DataSource;
end;

function ABGetTableViewDataController(aTableView: TcxCustomGridTableView):TcxGridDBDataController;
begin
  Result:=nil;
  if aTableView is TcxGridDBBandedTableView then
    Result:=TcxGridDBBandedTableView(aTableView).DataController
  else if aTableView is TcxGridDBTableView then
    Result:=TcxGridDBTableView(aTableView).DataController;
end;

function ABGetFocusedColumnField(aTableView: TcxCustomGridTableView):TField;
begin
  Result:=nil;
  if (aTableView is TcxGridDBBandedTableView) and
     (Assigned(TcxGridDBBandedColumn(TcxGridDBBandedTableView(aTableView).Controller.FocusedColumn).DataBinding)) and
     (Assigned(TcxGridDBBandedColumn(TcxGridDBBandedTableView(aTableView).Controller.FocusedColumn).DataBinding.Field))  then
    Result:=TcxGridDBBandedColumn(TcxGridDBBandedTableView(aTableView).Controller.FocusedColumn).DataBinding.Field
  else if (aTableView is TcxGridDBTableView) and
          (Assigned(TcxGridDBColumn(TcxGridDBTableView(aTableView).Controller.FocusedColumn).DataBinding)) and
          (Assigned(TcxGridDBColumn(TcxGridDBTableView(aTableView).Controller.FocusedColumn).DataBinding.Field)) then
    Result:=TcxGridDBColumn(TcxGridDBTableView(aTableView).Controller.FocusedColumn).DataBinding.Field;
end;

function ABGetColumnField(aTableView: TcxCustomGridTableView;aIndex:LongInt):TField;
begin
  Result:=nil;
  if aTableView is TcxGridDBBandedTableView then
    Result:=TcxGridDBBandedTableView(aTableView).Columns[aIndex].DataBinding.Field
  else if aTableView is TcxGridDBTableView then
    Result:=TcxGridDBTableView(aTableView).Columns[aIndex].DataBinding.Field;
end;

function ABGetColumnValue(aItem:TcxCustomGridTableItem;aRecord:LongInt;aQuotedStr:boolean):Variant;
var
  tempIsStr:Boolean;
begin
  if ABIsCaleColumn(aItem) then
  begin
    Result:=aItem.GridView.DataController.GetDisplayText(aRecord,aItem.Index);
  end
  else
    Result:=vartostrdef(aItem.GridView.DataController.Values[aRecord,aItem.Index],'');

  tempIsStr:=(ABPos(','+aItem.DataBinding.ValueType+',',','+'Memo,FmtMemo,WideMemo,String,Guid,FixedChar,WideString,FixedWideChar,'+',')>0);
  if (ABVarIsNull(Result)) then
  begin
    if tempIsStr then
      Result:=''
    else
      Result:=0;
  end;        

  if (aQuotedStr) and (tempIsStr) then
    Result:=QuotedStr(Result);
end;

function ABIsCaleColumn(aItem:TcxCustomGridTableItem;aCheckTag:Boolean):Boolean;
begin
  Result:=false;
  if Assigned(aItem) then
    Result:=(not ABIsLinkField(aItem)) and
            ((not aCheckTag) or (aItem.tag=19003));
end;

function ABIsLinkField(aItem:TcxCustomGridTableItem):Boolean;
var
  tempField:TField;
begin
  Result:=false;
  if Assigned(aItem) then
  begin
    tempField:=ABGetColumnField(aItem);
    if Assigned(tempField) then
    begin
      Result:= true;
    end;
  end;                                                                         
end;

function ABReplaceColumnCaption(aCommText:string;
                                aTableView: TcxGridTableView;
                                aRecordIndex:LongInt;
                                aQuotedStr:boolean):string;
var
  i:longint;
  tempStr1,tempStr2,tempStr3 :string;
  tempColumn:TcxCustomGridTableItem;
  tempDo:Boolean;
begin
  tempDo:=false;
  tempStr1:=aCommText;
  if  ((aTableView is TcxGridDBBandedTableView) or
      (aTableView is TcxGridDBTableView)) then
  begin
    for i := 0 to aTableView.ColumnCount-1 do
    begin
      tempStr2:=aTableView.Columns[i].caption;
      if (ABPOS(':'+tempStr2,tempStr1)>0) then
      begin
        tempColumn:=ABGetColumnByCaption(aTableView,tempStr2);
        if Assigned(tempColumn ) then
        begin
          tempDo:=True;
          tempStr3:=ABGetColumnValue(tempColumn,aRecordIndex,aQuotedStr);
          tempStr1:=ABStringReplace(tempStr1,':'+tempStr2,tempStr3);
        end;
        if ABPos(':',tempStr1)<=0 then break;
      end;
    end;
  end;

  if tempDo then
    Result:=tempStr1
  else
    Result:=EmptyStr;
end;

function ABGetColumnByCaption(aTableView:TcxGridTableView;aCaption:string):TcxCustomGridTableItem;
var
  i:LongInt;
begin
  result:=nil;
  for I := 0 to aTableView.ColumnCount - 1 do
  begin
    if AnsiCompareText(aTableView.Columns[i].Caption,aCaption)=0 then
      result:=aTableView.Columns[i];
  end;
end;

function ABGetColumnByName(aTableView:TcxGridTableView;aName:string):TcxCustomGridTableItem;
var
  i:LongInt;
begin
  result:=nil;
  for I := 0 to aTableView.ColumnCount - 1 do
  begin
    if AnsiCompareText(aTableView.Columns[i].Name,aName)=0 then
      result:=aTableView.Columns[i];
  end;
end;

function ABGetTableViewOrder(aTableView:TcxCustomGridTableView;aSpaceFlag:string;aAddOrderBY:boolean;aOrderByStr:string;aDescStr:string):string;
var
   i: integer;
begin
  result:='';
  for i := 0 to aTableView.SortedItemCount - 1 do
  begin
    if Assigned(ABGetColumnField(aTableView,aTableView.SortedItems[i].Index)) then
    begin
      if (aTableView.SortedItems[i].SortOrder =soAscending) then
      begin
        result := result +aSpaceFlag+ABGetColumnField(aTableView,aTableView.SortedItems[i].Index).FieldName;
      end
      else if (aTableView.SortedItems[i].SortOrder =soDescending) then
      begin
        result := result +aSpaceFlag+ ABGetColumnField(aTableView,aTableView.SortedItems[i].Index).FieldName+' '+aDescStr+' ';
      end;
    end;
  end;
  if result<>emptystr then
  begin
    result := copy(result,Length(aSpaceFlag)+1,maxint);

    if aAddOrderBY then
      result := ' '+aOrderByStr+' '+result;
  end;
end;

procedure ABSetTableViewOrder(aTableView:TcxCustomGridTableView;aOrderFieldNames:array of string;aAscOrDescs:array of TcxDataSortOrder);
var
   i,j: integer;
   tempField:TField;
begin
  for i := 0 to aTableView.ItemCount - 1 do
  begin
    tempField:=ABGetColumnField(aTableView,aTableView.Items[i].Index);
    if (Assigned(tempField)) then
    begin
      j:=ABStrInArray(tempField.FieldName,aOrderFieldNames);
      if (j>=0) then
      begin
        aTableView.Items[i].SortOrder:= aAscOrDescs[j];
      end
      else
      begin
        aTableView.Items[i].SortOrder:= soNone;
      end;
    end;
  end;
end;


function ABGetColumnByFieldName(aTableView:TcxCustomGridTableView;aFieldName:string):TcxCustomGridTableItem;
begin
  Result:=nil;
  if aTableView is TcxGridDBBandedTableView then
    Result:=TcxGridDBBandedTableView(aTableView).GetColumnByFieldName(aFieldName)
  else if aTableView is TcxGridDBTableView then
    Result:=TcxGridDBTableView(aTableView).GetColumnByFieldName(aFieldName);
end;

function ABGetColumnProperty(aTableView:TcxCustomGridTableView;aFieldName:string;aProperty:string):Variant;
var
  tempTableItem:TcxCustomGridTableItem;
begin
  Result:=null;
  tempTableItem:=ABGetColumnByFieldName(aTableView,aFieldName);
  if (Assigned(tempTableItem)) and (ABIsPublishProp(tempTableItem,aProperty)) then
  begin
    Result:=ABGetPropValue(tempTableItem,aProperty);
  end;
end;

procedure ABSetColumnProperty(aTableView:TcxCustomGridTableView;aFieldName:string;aProperty:string;aValue:Variant);
var
  tempTableItem:TcxCustomGridTableItem;
begin
  tempTableItem:=ABGetColumnByFieldName(aTableView,aFieldName);
  if (Assigned(tempTableItem)) and (ABIsPublishProp(tempTableItem,aProperty)) then
  begin
    ABSetPropValue(tempTableItem,aProperty,aValue);
  end;
end;




end.










