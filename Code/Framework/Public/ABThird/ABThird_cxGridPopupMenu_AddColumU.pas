{
cxGrid右键增加计算列单元
}
unit ABThird_cxGridPopupMenu_AddColumU;

interface
{$I ..\ABInclude.ini}

uses
  ABPubFormU,
  ABPubVarU,

  ABPubScriptU,
  ABPubMessageU,
  ABPubFuncU,

  Windows,SysUtils,Variants,Classes,Controls,Forms,StdCtrls,ExtCtrls,ShellAPI,
  Buttons;

type
  TABcxGridPopupMenu_AddColumForm = class(TABPubForm)
    Panel1: TPanel;
    Button1: TButton;
    Button2: TButton;
    Panel2: TPanel;
    Memo1: TMemo;
    Panel3: TPanel;
    ListBox1: TListBox;
    Label1: TLabel;
    Edit1: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    ComboBox1: TComboBox;
    Label4: TLabel;
    cxButton7: TButton;
    Panel4: TPanel;
    speedButton1: TSpeedButton;
    speedButton2: TSpeedButton;
    speedButton3: TSpeedButton;
    speedButton4: TSpeedButton;
    SpeedButton5: TSpeedButton;
    SpeedButton6: TSpeedButton;
    SpeedButton7: TSpeedButton;
    SpeedButton8: TSpeedButton;
    cxButton1: TButton;
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure ListBox1DblClick(Sender: TObject);
    procedure speedButton1Click(Sender: TObject);
    procedure cxButton7Click(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;


implementation

{$R *.dfm}

procedure TABcxGridPopupMenu_AddColumForm.Button2Click(Sender: TObject);
begin                                                    
  if Edit1.Text=EmptyStr  then
  begin
    ABShow('列名不能为空,请检查.');
  end
  else if Memo1.Text=EmptyStr  then
  begin
    ABShow('列的计算公式不能为空,请检查.');
  end
  else
    ModalResult:=mrOk;
end;

procedure TABcxGridPopupMenu_AddColumForm.cxButton1Click(Sender: TObject);
var
  tempStr1:string;
begin
  tempStr1:= ABExecScript(Memo1.Text);
  if tempStr1<>'-1' then
  begin
    ABShow(('测试成功,返回值为')+':['+tempStr1+']');
  end;
end;

procedure TABcxGridPopupMenu_AddColumForm.cxButton7Click(Sender: TObject);
begin
  if ABCheckFileExists(ABPublicSetPath+'ABJavaScript.chm') then
    ShellExecute(0,nil,PChar(ABPublicSetPath+'ABJavaScript.chm'),nil,nil,sw_shownormal)
end;

procedure TABcxGridPopupMenu_AddColumForm.Button1Click(Sender: TObject);
begin
  ModalResult:=mrNo;

end;

procedure TABcxGridPopupMenu_AddColumForm.ListBox1DblClick(Sender: TObject);
var
  i:longint;
begin
  i:=Memo1.SelStart;
  if Memo1.SelText<>emptystr then
    Memo1.Text:=copy(Memo1.Text,1,Memo1.SelStart)+copy(Memo1.Text,Memo1.SelStart+Memo1.SelLength+1,length(Memo1.Text));
  Memo1.SelStart:=i;
  Memo1.Text:=copy(Memo1.Text,1,Memo1.SelStart)+' :' +ListBox1.Items[ListBox1.ItemIndex]+' '+copy(Memo1.Text,Memo1.SelStart+1,length(Memo1.Text));
  Memo1.SelStart:=length(Memo1.Text);
end;

procedure TABcxGridPopupMenu_AddColumForm.speedButton1Click(Sender: TObject);
var
  i:longint;
begin
  i:=Memo1.SelStart;
  if Memo1.SelText<>emptystr then
    Memo1.Text:=copy(Memo1.Text,1,Memo1.SelStart)+copy(Memo1.Text,Memo1.SelStart+Memo1.SelLength+1,length(Memo1.Text));
  Memo1.SelStart:=i;

  Memo1.Text:=copy(Memo1.Text,1,Memo1.SelStart)+TspeedButton(sender).Caption+' '+copy(Memo1.Text,Memo1.SelStart+1,length(Memo1.Text));
  Memo1.SelStart:=length(Memo1.Text);
end;

procedure TABcxGridPopupMenu_AddColumForm.SpeedButton6Click(Sender: TObject);
var
  i:longint;
begin
  i:=Memo1.SelStart;
  if Memo1.SelText<>emptystr then
    Memo1.Text:=copy(Memo1.Text,1,Memo1.SelStart)+copy(Memo1.Text,Memo1.SelStart+Memo1.SelLength+1,length(Memo1.Text));
  Memo1.SelStart:=i;

  Memo1.Text:=copy(Memo1.Text,1,Memo1.SelStart)+TSpeedButton(Sender).Hint+' '+copy(Memo1.Text,Memo1.SelStart+1,length(Memo1.Text));
  Memo1.SelStart:=length(Memo1.Text);
end;


end.
