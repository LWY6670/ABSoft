{
数据库连接单元
用UNI连接时SQLServerUniProvider 单元引用不能删除,因为此单元Initialization部分做了初始化的事情
删除会导致连接不成功,连接不成功时Application.Terminate 会错误地调用一些单元的 Initialization  ,造成导常,所以连接不成功时,用 Halt 强制结束程序
}

unit ABThirdConnDatabaseU;

interface
{$I ..\ABInclude.ini}

uses
  ABPubFormU,
  ABPubVarU,
  ABPubConstU,
  ABPubServiceU,
  ABPubPassU,
  ABPubDBU,
  ABPubFuncU,
  ABPubMessageU,
  ABPubThreadU,
  ABPubInPutPassWordU,
  ABPubLocalParamsU,
  ABPubMultilingualDBNavigatorU,

  ABThirdDBU,
  ABThirdFormU,

  FireDAC.Stan.StorageBin,
  FireDAC.Stan.StorageXML, FireDAC.Stan.StorageJSON,
  FireDAC.UI.Intf, FireDAC.VCLUI.Wait, FireDAC.Comp.UI,
  FireDAC.Stan.Def,
  FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  FireDAC.Phys.MSSQLDef, FireDAC.Phys, FireDAC.Phys.ODBCBase, FireDAC.Phys.MSSQL,

  TypInfo,
  Data.Win.ADODB,
  forms,Windows,SysUtils,DB,ExtCtrls,Controls,
  Messages,
  StdCtrls,Buttons,Classes, Vcl.Menus, FireDAC.Stan.Pool, Vcl.Grids,
  Vcl.DBGrids, Vcl.DBCtrls;

type
  TABConnDatabaseForm = class(TABThirdForm)
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    BitBtn1: TBitBtn;
    Edit3: TEdit;
    Edit4: TEdit;
    BitBtn2: TBitBtn;
    ComboBox1: TComboBox;
    ComboBox2: TComboBox;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    SpeedButton1: TSpeedButton;
    Panel2: TPanel;
    DataSource1: TDataSource;
    FireDACQuery1: TFireDACQuery;
    FireDACConnection1: TFireDACConnection;
    Panel3: TPanel;
    ABMultilingualDBNavigator1: TABMultilingualDBNavigator;
    Panel4: TPanel;
    Button1: TButton;
    DBGrid1: TDBGrid;
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);

    procedure PasswordOnFieldGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure PasswordOnFieldSetText(Sender: TField; const Text: string);
    procedure FireDACQuery1AfterInsert(DataSet: TDataSet);
    procedure FireDACQuery1Beforedelete(DataSet: TDataSet);
  private
    FOkClose: Boolean;
    function CheckConn(aShowMsg: boolean): boolean;
    { Private declarations }
  public
    property OkClose:Boolean read FOkClose  write FOkClose;
    procedure WmSysCommand(var msg: TMessage); message WM_SYSCOMMAND;
    { Public declarations }
  end;

//设置连接
//aShow=True表示显示连接框
function ABCreateConns_inside(aShow:boolean):Boolean;


implementation
{$R *.dfm}

procedure ABFreeConns;
var
  i:longint;
begin
  for I := Low(ABConns) to High(ABConns) do
  begin
    if (Assigned(ABConns[i].Conn)) and
       (not ABConns[i].Conninfo.Manual) then
    begin
      ABConns[i].Conn.Free;
    end;
  end;
  SetLength(ABConns,0);
end;

function ABCreateConns_inside(aShow:boolean):Boolean;
  //加载数据库连接
  function LoadConnLists:boolean;
    function LoadConnList(aConnName,aHost,aDatabase,aUserName,aPassWord:string;aManual,aIsMain:boolean;aDatabaseType:TABDatabaseType):boolean;
    var
      i:longint;
    begin
      SetLength(ABConns,high(ABConns)+1+1);
      i:=high(ABConns);
      ABConns[i].Conninfo.ConnName     :=aConnName;
      ABConns[i].Conninfo.Host         :=aHost;
      ABConns[i].Conninfo.Database     :=aDatabase;
      ABConns[i].Conninfo.IsMain       :=aIsMain;
      ABConns[i].Conninfo.Manual       :=aManual;
      ABConns[i].Conninfo.UserName     :=aUserName;
      ABConns[i].Conninfo.PassWord     :=aPassWord;
      ABConns[i].Conninfo.DatabaseType :=dtSQLServer;
      ABConns[i].Conn                  :=TFireDACConnection.Create(nil);

      result:=ABOpenConnection(
                   ABConns[i].Conn,
                   aDatabaseType,
                   ABConns[i].Conninfo.Host,
                   ABConns[i].Conninfo.Database,
                   ABConns[i].Conninfo.UserName,
                   ABConns[i].Conninfo.PassWord,
                   True,false
                       );
    end;
  var
    tempString:TStrings;
    tempDataset:TFireDACQuery;
  begin
    ABFreeConns;

    tempString:=TStringList.Create;
    tempDataset:=TFireDACQuery.Create(nil);
    try
      ABReadInI('ConnDatabase',ABConnFileName,tempString);
      result:=LoadConnList('Main',tempString.Values['Host'],tempString.Values['Database'],tempString.Values['UserName'],ABUnDoPassword(tempString.Values['PassWord']),false,true,dtSQLServer);
      tempDataset.Connection:= TFireDACConnection(ABConns[0].Conn);

      tempDataset.close;
      tempDataset.SQL.Text:='select object_ID(''ABSys_Org_ConnList'')';
      tempDataset.Open;
      if not tempDataset.Fields[0].IsNull  then
      begin
        tempDataset.close;
        tempDataset.SQL.Text:='select * from  ABSys_Org_ConnList where isnull(CL_NoUse,0)=0 and Cl_Name<>''Main''';
        tempDataset.Open;
        tempDataset.First;
        while not tempDataset.Eof do
        begin
          result:=LoadConnList(tempDataset.FindField('CL_Name').AsString,
                               tempDataset.FindField('CL_Server').AsString,
                               tempDataset.FindField('CL_DatabaseName').AsString,
                               tempDataset.FindField('CL_UserName').AsString,
                               ABUnDoPassword(tempDataset.FindField('CL_Password').AsString),
                               False,false,dtSQLServer
                               );
          if not result then
          begin
            break;
          end;
          tempDataset.Next;
        end;
      end;
    finally
      tempDataset.Free;
      tempString.Free;
    end;
  end;
var
  tempForm: TABConnDatabaseForm;
  tempNotSetupFile:boolean;
  tempConnection: TFireDACConnection;
begin
  tempNotSetupFile:=(not ABCheckFileExists(ABConnFileName));
  Application.CreateForm(TABConnDatabaseForm,tempForm);
  tempForm.CloseAction:=caHide;
  tempConnection := TFireDACConnection.Create(nil);
  try
    if (tempNotSetupFile) or (aShow) then
    begin
      result:=(tempForm.ShowModal=mrOk);
    end
    else
    begin
      result:=ABOpenConnection( tempConnection, dtSQLServer,
                                tempForm.ComboBox1.Text,
                                tempForm.ComboBox2.Text,
                                tempForm.Edit3.Text,
                                tempForm.Edit4.Text,
                                True,false
                                );
      if not result then
      begin
        if (ABInDelphi) or
           (ABInputPassWord('数据库连接失败,请输入密码设置连接')) then
        begin
          result:=(tempForm.ShowModal=mrOk);
        end;
      end;
    end;

    if result then
      LoadConnLists;
  finally
    tempForm.Free;
    tempForm:=nil;
    tempConnection.Free;
  end;
end;

procedure TABConnDatabaseForm.PasswordOnFieldGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  if AnsiCompareText(Sender.FieldName,'CL_PassWord')=0 then
  begin
    Text:=copy('*****************************************',1,length(ABUnDoPassword(Sender.AsString)));
  end;
end;

procedure TABConnDatabaseForm.PasswordOnFieldSetText(
  Sender: TField; const Text: string);
begin
  if AnsiCompareText(Sender.FieldName,'CL_PassWord')=0 then
  begin
    Sender.AsString :=ABDoPassword(Text);
  end;
end;

procedure TABConnDatabaseForm.SpeedButton1Click(Sender: TObject);
var
  I: Integer;
begin
  if not Panel2.Visible then
  begin
    if not  FireDACConnection1.Connected then
      CheckConn(false);

    if not  FireDACConnection1.Connected then
    begin
      ABShow('请先测试主数据连接再设置多数据库连接.');
      exit;
    end;

    if not FireDACQuery1.Active then
    begin
      FireDACQuery1.Connection:=FireDACConnection1;
      FireDACQuery1.UpdateOptions.KeyFields:='CL_Guid';
      FireDACQuery1.UpdateOptions.UpdateMode:=upWhereKeyOnly;
      FireDACQuery1.close;
      FireDACQuery1.SQL.Text:='select * from  ABSys_Org_ConnList where  Cl_Name<>''Main''';
      FireDACQuery1.Open;
      DBGrid1.Columns[0].Width :=65;
      DBGrid1.Columns[1].Width :=80;
      DBGrid1.Columns[2].Width :=80;
      DBGrid1.Columns[3].Width :=55;
      DBGrid1.Columns[4].Width :=65;
      DBGrid1.Columns[5].Width :=65;

      FireDACQuery1.FieldByName('CL_PassWord').OnGetText:=PasswordOnFieldGetText;
      FireDACQuery1.FieldByName('CL_PassWord').OnSetText:=PasswordOnFieldSetText;
      //Resync解决打开窗体后看到的密码长度太长问题
      FireDACQuery1.Resync([]);
    end;
  end;

  Panel2.Visible:=not Panel2.Visible;
end;

procedure TABConnDatabaseForm.SpeedButton2Click(Sender: TObject);
begin
  ABGetSqlServerList(ComboBox1.Items,false);
end;

procedure TABConnDatabaseForm.SpeedButton3Click(Sender: TObject);
begin
  if BitBtn1.CanFocus then
    BitBtn1.SetFocus;

  CheckConn(false);
end;

procedure TABConnDatabaseForm.WmSysCommand(var msg: TMessage);
begin
  inherited;//必须加，否则模态窗关闭最大化还原将不能用
  if msg.WParam = SC_MINIMIZE then
  begin
    WindowState := wsMinimized;
    Show;
  end
end;

procedure TABConnDatabaseForm.BitBtn1Click(Sender: TObject);
begin
  CheckConn(True);
end;

function TABConnDatabaseForm.CheckConn(aShowMsg: boolean): boolean;
var
  tempQuery: TFireDACQuery;
begin
  result:=false;
  if Trim(ComboBox1.Text)=EmptyStr  then
  begin
    ComboBox1.Text:='.';
  end;

  tempQuery := TFireDACQuery.Create(nil);
  try
    if ABOpenConnection(FireDACConnection1, dtSQLServer,
              ComboBox1.Text,
              ComboBox2.Text,
              Edit3.Text,
              Edit4.Text,
              True,
              aShowMsg
              ) then
    begin
      tempQuery.Connection := FireDACConnection1;
      tempQuery.Close;
      tempQuery.SQL.Text:='select name from master.dbo.sysdatabases  ';
      tempQuery.Open;
      ABDatasetToStrings(tempQuery,['Name'],ComboBox2.Items);
      ABStrsToStrings(ComboBox1.Text,'',ComboBox1.Items,True,False);
      result:=true;
    end;
  finally
    tempQuery.Free;
  end;
end;

procedure TABConnDatabaseForm.BitBtn2Click(Sender: TObject);
begin
  if Trim(ComboBox2.Text)=EmptyStr  then
  begin
    ABShow('数据库不能为空,请选择.');
    exit;
  end;

  if CheckConn(false) then
  begin
    ABWriteInI(['ConnDatabase'],
               ['Host','Database','UserName','PassWord'],
               [ComboBox1.Text,ComboBox2.Text,Edit3.Text,ABDOPassWord(Edit4.Text)],
               ABConnFileName);

    if FOkClose then
      close
    else
      ModalResult:=mrOk;
  end;
end;

procedure TABConnDatabaseForm.Button1Click(Sender: TObject);
var
  tempConnection: TFireDACConnection;
begin
  Datasource1.DataSet.First;
  tempConnection := TFireDACConnection.Create(nil);
  try
    while not Datasource1.DataSet.Eof do
    begin
      if (Datasource1.DataSet.FieldByName('CL_Server').IsNull) or
         (Datasource1.DataSet.FieldByName('CL_Server').AsString=EmptyStr)
           then
      begin
        Datasource1.DataSet.Edit;
        Datasource1.DataSet.FieldByName('CL_Server').AsString:='.';
        Datasource1.DataSet.Post;
      end;


      if not Datasource1.DataSet.FindField('CL_NoUse').AsBoolean then
      begin
        if not ABOpenConnection(tempConnection, dtSQLServer,
                  Datasource1.DataSet.FindField('CL_Server').AsString,
                  Datasource1.DataSet.FindField('CL_DatabaseName').AsString,
                  Datasource1.DataSet.FindField('CL_UserName').AsString,
                  ABUnDoPassword(Datasource1.DataSet.FindField('CL_Password').AsString),
                  true
                  ) then
        begin
          break;
        end;
      end;
      
      Datasource1.DataSet.Next;
    end;
  finally
    tempConnection.Free;
  end;
end;

procedure TABConnDatabaseForm.FireDACQuery1AfterInsert(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('CL_Guid').AsString:=ABGetGuid;
  DataSet.FieldByName('PubID').AsString:=ABGetGuid;
end;

procedure TABConnDatabaseForm.FireDACQuery1Beforedelete(DataSet: TDataSet);
begin
  if ABShow('删除数据连接同时也会删除连接的所有数据字典信息,确认删除吗?',[],['是','否'],1)=1 then
  begin
    FireDACConnection1.ExecSQL('delete ABSys_Org_Table where Ta_CL_Guid='+QuotedStr(FireDACQuery1.FieldByName('CL_Guid').AsString));
  end
  else
  begin
    abort;
  end;
end;

procedure TABConnDatabaseForm.FormCreate(Sender: TObject);
  procedure LoadSetup;
  var
    tempString:TStrings;
  begin
    tempString:=TStringList.Create;
    try
      ABReadInI('ConnDatabase',ABConnFileName,tempString);
      ComboBox1.Text:=tempString.Values['Host'];
      ComboBox2.Text:=tempString.Values['Database'];
      Edit3.Text:=tempString.Values['UserName'];
      Edit4.Text:=ABUnDOPassWord(tempString.Values['PassWord']);
    finally
      tempString.Free;
    end;
  end;
var
  tempHandle:THandle;
begin
  FireDACQuery1.CachedUpdates:=False;

  if ABCheckFileExists(ABConnFileName) then
  begin
    tempHandle:=ABCreateMutex('TABConnDatabaseForm.Setup.ini');
    if tempHandle>0  then
    begin
      if WaitForSingleObject(tempHandle, INFINITE) = WAIT_OBJECT_0 then
      begin
        LoadSetup;

        ReleaseMutex(tempHandle);
        ABCloseMutex('TABConnDatabaseForm.Setup.ini');
      end;
    end
    else
    begin
      LoadSetup;
    end;
  end;
end;

procedure TABConnDatabaseForm.N1Click(Sender: TObject);
begin
  ABSetLocalParamsValue('LoginType','ltCS_Three');
  Close;
end;

procedure ABFinalization;
begin
  ABFreeConns;
end;

procedure ABInitialization;
begin
end;

Initialization
  ABInitialization;
Finalization
  ABFinalization;

end.
































































