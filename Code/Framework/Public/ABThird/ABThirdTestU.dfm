object ABTestForm: TABTestForm
  Left = 376
  Top = 135
  Caption = #20013#21326#20849#21644#22269
  ClientHeight = 521
  ClientWidth = 931
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl3: TPageControl
    Left = 281
    Top = 30
    Width = 650
    Height = 491
    ActivePage = TabSheet5
    Align = alClient
    TabOrder = 0
    object TabSheet5: TTabSheet
      Caption = #25511#20214
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 321
        Height = 443
        Align = alLeft
        TabOrder = 0
        object GroupBox23: TGroupBox
          Left = 1
          Top = 1
          Width = 319
          Height = 224
          Align = alTop
          Caption = 'ABThirdQueryU'
          TabOrder = 0
          object Button17: TButton
            Left = 2
            Top = 197
            Width = 315
            Height = 25
            Align = alBottom
            Caption = #25171#24320
            TabOrder = 0
            OnClick = Button17Click
          end
          object cxGrid1: TcxGrid
            Left = 2
            Top = 15
            Width = 315
            Height = 84
            Align = alTop
            TabOrder = 1
            object cxGrid1DBBandedTableView1: TcxGridDBBandedTableView
              PopupMenu = ABcxGridPopupMenu1
              Navigator.Buttons.CustomButtons = <>
              DataController.DataSource = DataSource1
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              OptionsCustomize.ColumnsQuickCustomization = True
              OptionsView.GroupByBox = False
              OptionsView.Indicator = True
              OptionsView.BandHeaders = False
              OnCustomDrawIndicatorCell = cxGrid1DBBandedTableView1CustomDrawIndicatorCell
              Bands = <
                item
                end>
              object cxGrid1DBBandedTableView1TA_TI_Guid: TcxGridDBBandedColumn
                DataBinding.FieldName = 'TA_TI_Guid'
                Width = 122
                Position.BandIndex = 0
                Position.ColIndex = 0
                Position.RowIndex = 0
              end
              object cxGrid1DBBandedTableView1TA_CL_Guid: TcxGridDBBandedColumn
                DataBinding.FieldName = 'TA_CL_Guid'
                Width = 111
                Position.BandIndex = 0
                Position.ColIndex = 1
                Position.RowIndex = 0
              end
              object cxGrid1DBBandedTableView1TA_Guid: TcxGridDBBandedColumn
                DataBinding.FieldName = 'TA_Guid'
                Width = 122
                Position.BandIndex = 0
                Position.ColIndex = 2
                Position.RowIndex = 0
              end
              object cxGrid1DBBandedTableView1TA_Type: TcxGridDBBandedColumn
                DataBinding.FieldName = 'TA_Type'
                Width = 48
                Position.BandIndex = 0
                Position.ColIndex = 3
                Position.RowIndex = 0
              end
              object cxGrid1DBBandedTableView1TA_ID: TcxGridDBBandedColumn
                DataBinding.FieldName = 'TA_ID'
                Width = 66
                Position.BandIndex = 0
                Position.ColIndex = 4
                Position.RowIndex = 0
              end
              object cxGrid1DBBandedTableView1TA_Name: TcxGridDBBandedColumn
                DataBinding.FieldName = 'TA_Name'
                Width = 118
                Position.BandIndex = 0
                Position.ColIndex = 5
                Position.RowIndex = 0
              end
              object cxGrid1DBBandedTableView1TA_Caption: TcxGridDBBandedColumn
                DataBinding.FieldName = 'TA_Caption'
                Width = 113
                Position.BandIndex = 0
                Position.ColIndex = 6
                Position.RowIndex = 0
              end
              object cxGrid1DBBandedTableView1TA_IsTail: TcxGridDBBandedColumn
                DataBinding.FieldName = 'TA_IsTail'
                Position.BandIndex = 0
                Position.ColIndex = 7
                Position.RowIndex = 0
              end
              object cxGrid1DBBandedTableView1TA_MaxRecord: TcxGridDBBandedColumn
                DataBinding.FieldName = 'TA_MaxRecord'
                Position.BandIndex = 0
                Position.ColIndex = 8
                Position.RowIndex = 0
              end
              object cxGrid1DBBandedTableView1TA_CanInsert: TcxGridDBBandedColumn
                DataBinding.FieldName = 'TA_CanInsert'
                Position.BandIndex = 0
                Position.ColIndex = 9
                Position.RowIndex = 0
              end
              object cxGrid1DBBandedTableView1TA_CanDelete: TcxGridDBBandedColumn
                DataBinding.FieldName = 'TA_CanDelete'
                Position.BandIndex = 0
                Position.ColIndex = 10
                Position.RowIndex = 0
              end
              object cxGrid1DBBandedTableView1TA_CanEdit: TcxGridDBBandedColumn
                DataBinding.FieldName = 'TA_CanEdit'
                Position.BandIndex = 0
                Position.ColIndex = 11
                Position.RowIndex = 0
              end
              object cxGrid1DBBandedTableView1TA_CanPrint: TcxGridDBBandedColumn
                DataBinding.FieldName = 'TA_CanPrint'
                Position.BandIndex = 0
                Position.ColIndex = 12
                Position.RowIndex = 0
              end
              object cxGrid1DBBandedTableView1TA_SetInsertCommand: TcxGridDBBandedColumn
                DataBinding.FieldName = 'TA_SetInsertCommand'
                Width = 116
                Position.BandIndex = 0
                Position.ColIndex = 13
                Position.RowIndex = 0
              end
              object cxGrid1DBBandedTableView1TA_SetEditCommand: TcxGridDBBandedColumn
                DataBinding.FieldName = 'TA_SetEditCommand'
                Width = 105
                Position.BandIndex = 0
                Position.ColIndex = 14
                Position.RowIndex = 0
              end
              object cxGrid1DBBandedTableView1TA_SetPostCommand: TcxGridDBBandedColumn
                DataBinding.FieldName = 'TA_SetPostCommand'
                Width = 108
                Position.BandIndex = 0
                Position.ColIndex = 15
                Position.RowIndex = 0
              end
              object cxGrid1DBBandedTableView1TA_SetDeleteCommand: TcxGridDBBandedColumn
                DataBinding.FieldName = 'TA_SetDeleteCommand'
                Width = 118
                Position.BandIndex = 0
                Position.ColIndex = 16
                Position.RowIndex = 0
              end
              object cxGrid1DBBandedTableView1TA_Order: TcxGridDBBandedColumn
                DataBinding.FieldName = 'TA_Order'
                Position.BandIndex = 0
                Position.ColIndex = 17
                Position.RowIndex = 0
              end
              object cxGrid1DBBandedTableView1PubID: TcxGridDBBandedColumn
                DataBinding.FieldName = 'PubID'
                Width = 97
                Position.BandIndex = 0
                Position.ColIndex = 18
                Position.RowIndex = 0
              end
              object cxGrid1DBBandedTableView1a: TcxGridDBBandedColumn
                DataBinding.FieldName = 'a'
                Width = 81
                Position.BandIndex = 0
                Position.ColIndex = 19
                Position.RowIndex = 0
              end
              object cxGrid1DBBandedTableView1b: TcxGridDBBandedColumn
                DataBinding.FieldName = 'b'
                Width = 70
                Position.BandIndex = 0
                Position.ColIndex = 20
                Position.RowIndex = 0
              end
              object cxGrid1DBBandedTableView1c: TcxGridDBBandedColumn
                DataBinding.FieldName = 'c'
                Width = 81
                Position.BandIndex = 0
                Position.ColIndex = 21
                Position.RowIndex = 0
              end
            end
            object cxGrid1Level1: TcxGridLevel
              GridView = cxGrid1DBBandedTableView1
            end
          end
          object cxGrid2: TcxGrid
            Left = 2
            Top = 99
            Width = 315
            Height = 98
            Align = alClient
            TabOrder = 2
            object cxGridDBBandedTableView1: TcxGridDBBandedTableView
              Navigator.Buttons.CustomButtons = <>
              DataController.DataSource = DataSource1
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              OptionsCustomize.ColumnsQuickCustomization = True
              OptionsView.GroupByBox = False
              OptionsView.Indicator = True
              OptionsView.BandHeaders = False
              OnCustomDrawIndicatorCell = cxGrid1DBBandedTableView1CustomDrawIndicatorCell
              Bands = <
                item
                end>
              object cxGridDBBandedColumn1: TcxGridDBBandedColumn
                DataBinding.FieldName = 'TA_TI_Guid'
                Width = 122
                Position.BandIndex = 0
                Position.ColIndex = 0
                Position.RowIndex = 0
              end
              object cxGridDBBandedColumn2: TcxGridDBBandedColumn
                DataBinding.FieldName = 'TA_CL_Guid'
                Width = 111
                Position.BandIndex = 0
                Position.ColIndex = 1
                Position.RowIndex = 0
              end
              object cxGridDBBandedColumn3: TcxGridDBBandedColumn
                DataBinding.FieldName = 'TA_Guid'
                Width = 122
                Position.BandIndex = 0
                Position.ColIndex = 2
                Position.RowIndex = 0
              end
              object cxGridDBBandedColumn4: TcxGridDBBandedColumn
                DataBinding.FieldName = 'TA_Type'
                Width = 48
                Position.BandIndex = 0
                Position.ColIndex = 3
                Position.RowIndex = 0
              end
              object cxGridDBBandedColumn5: TcxGridDBBandedColumn
                DataBinding.FieldName = 'TA_ID'
                Width = 66
                Position.BandIndex = 0
                Position.ColIndex = 4
                Position.RowIndex = 0
              end
              object cxGridDBBandedColumn6: TcxGridDBBandedColumn
                DataBinding.FieldName = 'TA_Name'
                Width = 118
                Position.BandIndex = 0
                Position.ColIndex = 5
                Position.RowIndex = 0
              end
              object cxGridDBBandedColumn7: TcxGridDBBandedColumn
                DataBinding.FieldName = 'TA_Caption'
                Width = 113
                Position.BandIndex = 0
                Position.ColIndex = 6
                Position.RowIndex = 0
              end
              object cxGridDBBandedColumn8: TcxGridDBBandedColumn
                DataBinding.FieldName = 'TA_IsTail'
                Position.BandIndex = 0
                Position.ColIndex = 7
                Position.RowIndex = 0
              end
              object cxGridDBBandedColumn9: TcxGridDBBandedColumn
                DataBinding.FieldName = 'TA_MaxRecord'
                Position.BandIndex = 0
                Position.ColIndex = 8
                Position.RowIndex = 0
              end
              object cxGridDBBandedColumn10: TcxGridDBBandedColumn
                DataBinding.FieldName = 'TA_CanInsert'
                Position.BandIndex = 0
                Position.ColIndex = 9
                Position.RowIndex = 0
              end
              object cxGridDBBandedColumn11: TcxGridDBBandedColumn
                DataBinding.FieldName = 'TA_CanDelete'
                Position.BandIndex = 0
                Position.ColIndex = 10
                Position.RowIndex = 0
              end
              object cxGridDBBandedColumn12: TcxGridDBBandedColumn
                DataBinding.FieldName = 'TA_CanEdit'
                Position.BandIndex = 0
                Position.ColIndex = 11
                Position.RowIndex = 0
              end
              object cxGridDBBandedColumn13: TcxGridDBBandedColumn
                DataBinding.FieldName = 'TA_CanPrint'
                Position.BandIndex = 0
                Position.ColIndex = 12
                Position.RowIndex = 0
              end
              object cxGridDBBandedColumn14: TcxGridDBBandedColumn
                DataBinding.FieldName = 'TA_SetInsertCommand'
                Width = 116
                Position.BandIndex = 0
                Position.ColIndex = 13
                Position.RowIndex = 0
              end
              object cxGridDBBandedColumn15: TcxGridDBBandedColumn
                DataBinding.FieldName = 'TA_SetEditCommand'
                Width = 105
                Position.BandIndex = 0
                Position.ColIndex = 14
                Position.RowIndex = 0
              end
              object cxGridDBBandedColumn16: TcxGridDBBandedColumn
                DataBinding.FieldName = 'TA_SetPostCommand'
                Width = 108
                Position.BandIndex = 0
                Position.ColIndex = 15
                Position.RowIndex = 0
              end
              object cxGridDBBandedColumn17: TcxGridDBBandedColumn
                DataBinding.FieldName = 'TA_SetDeleteCommand'
                Width = 118
                Position.BandIndex = 0
                Position.ColIndex = 16
                Position.RowIndex = 0
              end
              object cxGridDBBandedColumn18: TcxGridDBBandedColumn
                DataBinding.FieldName = 'TA_Order'
                Position.BandIndex = 0
                Position.ColIndex = 17
                Position.RowIndex = 0
              end
              object cxGridDBBandedColumn19: TcxGridDBBandedColumn
                DataBinding.FieldName = 'PubID'
                Width = 97
                Position.BandIndex = 0
                Position.ColIndex = 18
                Position.RowIndex = 0
              end
              object cxGridDBBandedColumn20: TcxGridDBBandedColumn
                DataBinding.FieldName = 'a'
                Width = 81
                Position.BandIndex = 0
                Position.ColIndex = 19
                Position.RowIndex = 0
              end
              object cxGridDBBandedColumn21: TcxGridDBBandedColumn
                DataBinding.FieldName = 'b'
                Width = 70
                Position.BandIndex = 0
                Position.ColIndex = 20
                Position.RowIndex = 0
              end
              object cxGridDBBandedColumn22: TcxGridDBBandedColumn
                DataBinding.FieldName = 'c'
                Width = 81
                Position.BandIndex = 0
                Position.ColIndex = 21
                Position.RowIndex = 0
              end
            end
            object cxGridLevel1: TcxGridLevel
              GridView = cxGridDBBandedTableView1
            end
          end
        end
        object dxNavBar1: TdxNavBar
          Left = 1
          Top = 246
          Width = 319
          Height = 59
          Align = alTop
          ActiveGroupIndex = 0
          TabOrder = 1
          View = 0
          object dxNavBar1Group1: TdxNavBarGroup
            Caption = 'dxNavBar1Group1'
            SelectedLinkIndex = -1
            TopVisibleLinkIndex = 0
            Links = <>
          end
        end
        object RadioGroup1: TRadioGroup
          Left = 1
          Top = 305
          Width = 319
          Height = 40
          Align = alTop
          Caption = 'RadioGroup1'
          Columns = 2
          Items.Strings = (
            'test1'
            #27979#35797'2')
          TabOrder = 2
        end
        object cxRadioGroup1: TcxRadioGroup
          Left = 1
          Top = 345
          Align = alTop
          Caption = 'cxRadioGroup1'
          ParentBackground = False
          ParentColor = False
          Properties.Items = <>
          Style.Color = clBtnFace
          TabOrder = 3
          Height = 48
          Width = 319
        end
        object cxCheckListBox1: TcxCheckListBox
          Left = 2
          Top = 392
          Width = 63
          Height = 47
          Items = <
            item
              Text = 'a'
            end
            item
              Text = 'bbbbbaaaaaa'
            end
            item
              Text = 'c'
            end>
          TabOrder = 4
        end
        object cxTextEdit1: TcxTextEdit
          Left = 64
          Top = 392
          TabOrder = 5
          Text = 'cxTextEdit1'
          Width = 121
        end
        object cxDBTextEdit1: TcxDBTextEdit
          Left = 64
          Top = 416
          TabOrder = 6
          Width = 121
        end
        object cxComboBox1: TcxComboBox
          Left = 1
          Top = 225
          Align = alTop
          Properties.Items.Strings = (
            '1'
            '2'
            '3'
            '4'
            '5'
            '6'
            '7'
            '8'
            '9'
            '0'
            '1'
            '2'
            '3'
            '4'
            '5'
            '6'
            '7'
            '8'
            '9'
            '0'
            '1'
            '2'
            '3'
            '4'
            '5'
            '6'
            '7'
            '8'
            '9'
            '0')
          TabOrder = 7
          Text = '4'
          Width = 319
        end
        object cxCheckComboBox1: TcxCheckComboBox
          Left = 191
          Top = 399
          Properties.Items = <
            item
              Description = 'aa'
              ShortDescription = 'a'
            end
            item
              Description = 'bb'
              ShortDescription = 'b'
            end>
          Properties.OnEditValueToStates = cxCheckComboBox1PropertiesEditValueToStates
          Properties.OnStatesToEditValue = cxCheckComboBox1PropertiesStatesToEditValue
          TabOrder = 8
          Width = 121
        end
      end
      object dxStatusBar1: TdxStatusBar
        Left = 0
        Top = 443
        Width = 642
        Height = 20
        Panels = <
          item
            PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
            Text = #20013#22269#20154
          end
          item
            PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
          end
          item
            PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
            Text = #24037#26234#33021
          end>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
      end
      object cxDBPivotGrid1: TcxDBPivotGrid
        Left = 324
        Top = 3
        Width = 315
        Height = 434
        DataSource = DataSource1
        Groups = <>
        PopupMenu = ABcxPivotGridPopupMenu1
        TabOrder = 2
      end
      object BitBtn1: TBitBtn
        Left = 208
        Top = 3
        Width = 75
        Height = 25
        Caption = 'BitBtn1'
        TabOrder = 3
      end
    end
    object TabSheet1: TTabSheet
      Caption = #21333#20803
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 161
        Height = 463
        Align = alLeft
        TabOrder = 0
        object GroupBox38: TGroupBox
          Left = 1
          Top = 260
          Width = 159
          Height = 37
          Align = alTop
          Caption = 'ABThirdQueryU'
          TabOrder = 0
          object Button57: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            Caption = 'ABThirdQueryU'
            TabOrder = 0
            OnClick = Button57Click
          end
        end
        object GroupBox1: TGroupBox
          Left = 1
          Top = 223
          Width = 159
          Height = 37
          Align = alTop
          Caption = 'ABThirdCustomQueryU'
          TabOrder = 1
          object Button1: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            Caption = 'ABThirdCustomQueryU'
            TabOrder = 0
            OnClick = Button1Click
          end
        end
        object GroupBox2: TGroupBox
          Left = 1
          Top = 186
          Width = 159
          Height = 37
          Align = alTop
          Caption = 'ABThirdCacheDatasetU'
          TabOrder = 2
          object Button2: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            Caption = 'ABThirdCacheDatasetU'
            TabOrder = 0
            OnClick = Button2Click
          end
        end
        object GroupBox3: TGroupBox
          Left = 1
          Top = 149
          Width = 159
          Height = 37
          Align = alTop
          Caption = 'ABThirdImportDataU'
          TabOrder = 3
          object Button3: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            Caption = 'ABThirdImportDataU'
            TabOrder = 0
            OnClick = Button3Click
          end
        end
        object GroupBox4: TGroupBox
          Left = 1
          Top = 112
          Width = 159
          Height = 37
          Align = alTop
          Caption = 'ABThirdFuncU'
          TabOrder = 4
          object Button4: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            Caption = 'ABThirdFuncU'
            TabOrder = 0
            OnClick = Button4Click
          end
        end
        object GroupBox5: TGroupBox
          Left = 1
          Top = 75
          Width = 159
          Height = 37
          Align = alTop
          Caption = 'ABThirdConnU'
          TabOrder = 5
          object Button5: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            Caption = 'ABThirdConnU'
            TabOrder = 0
            OnClick = Button5Click
          end
        end
        object GroupBox6: TGroupBox
          Left = 1
          Top = 1
          Width = 159
          Height = 37
          Align = alTop
          Caption = 'ABThirdDBU'
          TabOrder = 6
          object Button7: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            Caption = 'ABThirdDBU'
            TabOrder = 0
            OnClick = Button7Click
          end
        end
        object GroupBox7: TGroupBox
          Left = 1
          Top = 38
          Width = 159
          Height = 37
          Align = alTop
          Caption = 'ABThirdConnDatabaseU'
          TabOrder = 7
          object Button8: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            Caption = 'ABThirdConnDatabaseU'
            TabOrder = 0
            OnClick = Button8Click
          end
        end
      end
      object Panel2: TPanel
        Left = 161
        Top = 0
        Width = 161
        Height = 463
        Align = alLeft
        TabOrder = 1
        object GroupBox8: TGroupBox
          Left = 1
          Top = 260
          Width = 159
          Height = 37
          Align = alTop
          Caption = 'GroupBox8'
          TabOrder = 0
          object Button9: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            TabOrder = 0
          end
        end
        object GroupBox9: TGroupBox
          Left = 1
          Top = 223
          Width = 159
          Height = 37
          Align = alTop
          Caption = 'GroupBox8'
          TabOrder = 1
          object Button10: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            TabOrder = 0
          end
        end
        object GroupBox10: TGroupBox
          Left = 1
          Top = 186
          Width = 159
          Height = 37
          Align = alTop
          Caption = 'GroupBox8'
          TabOrder = 2
          object Button11: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            TabOrder = 0
          end
        end
        object GroupBox11: TGroupBox
          Left = 1
          Top = 149
          Width = 159
          Height = 37
          Align = alTop
          Caption = 'GroupBox8'
          TabOrder = 3
          object Button12: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            TabOrder = 0
          end
        end
        object GroupBox12: TGroupBox
          Left = 1
          Top = 112
          Width = 159
          Height = 37
          Align = alTop
          Caption = 'GroupBox8'
          TabOrder = 4
          object Button13: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            TabOrder = 0
          end
        end
        object GroupBox13: TGroupBox
          Left = 1
          Top = 75
          Width = 159
          Height = 37
          Align = alTop
          Caption = 'GroupBox8'
          TabOrder = 5
          object Button14: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            TabOrder = 0
          end
        end
        object GroupBox14: TGroupBox
          Left = 1
          Top = 38
          Width = 159
          Height = 37
          Align = alTop
          Caption = 'ABThirdControlU'
          TabOrder = 6
          object Button15: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            Caption = 'ABThirdControlU'
            TabOrder = 0
            OnClick = Button15Click
          end
        end
        object GroupBox15: TGroupBox
          Left = 1
          Top = 1
          Width = 159
          Height = 37
          Align = alTop
          Caption = 'ABThird_DevExtPopupMenuU'
          TabOrder = 7
          object Button16: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            Caption = 'ABThird_DevExtPopupMenuU'
            TabOrder = 0
            OnClick = Button16Click
          end
        end
      end
    end
  end
  object Panel8: TPanel
    Left = 0
    Top = 30
    Width = 281
    Height = 491
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 1
    object Memo4: TMemo
      Left = 0
      Top = 353
      Width = 281
      Height = 83
      Align = alBottom
      Lines.Strings = (
        'Memo3')
      ScrollBars = ssBoth
      TabOrder = 0
      OnKeyDown = Memo3KeyDown
    end
    object Memo3: TMemo
      Left = 0
      Top = 0
      Width = 281
      Height = 353
      Align = alClient
      Lines.Strings = (
        'Memo3')
      ScrollBars = ssBoth
      TabOrder = 1
      OnKeyDown = Memo3KeyDown
    end
    object ProgressBar2: TProgressBar
      Left = 0
      Top = 436
      Width = 281
      Height = 17
      Align = alBottom
      TabOrder = 2
    end
    object Panel10: TPanel
      Left = 0
      Top = 453
      Width = 281
      Height = 38
      Align = alBottom
      TabOrder = 3
      object Button52: TButton
        Left = 111
        Top = 6
        Width = 156
        Height = 25
        Caption = #35843#29992#25152#26377#20989#25968
        TabOrder = 0
        OnClick = Button52Click
      end
      object Button6: TButton
        Left = 0
        Top = 6
        Width = 105
        Height = 25
        Caption = #28165#38500#26085#24535
        TabOrder = 1
        OnClick = Button6Click
      end
    end
  end
  object DataSource1: TDataSource
    DataSet = ABThirdQuery1
    Left = 76
    Top = 48
  end
  object ABThirdQuery1: TABThirdQuery
    ActiveStoredUsage = []
    ReadOnly = False
    SQL.Strings = (
      'select * from ABSys_Org_Table')
    BeforeDeleteAsk = True
    FieldCaptions = 
      'CL_PC='#26426#22120#21517#31216',CL_SPID='#36827#31243#26631#24535',CL_OP_Name='#25805#20316#20154#21592',CL_WindowsType='#31995#32479#31867#22411',CL_B' +
      'egDatetime='#24320#22987#26102#38388',CL_HeartbeatDatetime='#26368#36817#30340#24515#36339
    MaxRecordCount = 0
    EditControlType = ctSingle
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ABSys_Org_Table')
    IndexListDefs = <>
    Left = 80
    Top = 96
  end
  object ABcxGridPopupMenu1: TABcxGridPopupMenu
    AutoHotkeys = maManual
    CloseFootStr = False
    LinkTableView = cxGrid1DBBandedTableView1
    AutoApplyBestFit = True
    AutoCreateAllItem = True
    Left = 144
    Top = 192
  end
  object aaaa: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Times New Roman'
    Font.Style = []
    Categories.Strings = (
      'Custom')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    LookAndFeel.Kind = lfUltraFlat
    LookAndFeel.NativeStyle = False
    PopupMenuLinks = <>
    UseSystemFont = False
    Left = 144
    Top = 152
    DockControlHeights = (
      0
      0
      30
      0)
    object inheritedBar: TdxBar
      AllowClose = False
      AllowCustomizing = False
      AllowQuickCustomizing = False
      Caption = 'DataNav'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 0
      DockingStyle = dsTop
      FloatLeft = 559
      FloatTop = 157
      FloatClientWidth = 52
      FloatClientHeight = 562
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Times New Roman'
      Font.Style = []
      ItemLinks = <
        item
          Visible = True
          ItemName = 'btnCustom1'
        end
        item
          Visible = True
          ItemName = 'btnCustom2'
        end
        item
          Visible = True
          ItemName = 'btnCustom3'
        end
        item
          Visible = True
          ItemName = 'btnCustom4'
        end
        item
          Visible = True
          ItemName = 'btnCustom5'
        end>
      NotDocking = [dsNone, dsLeft, dsTop, dsRight, dsBottom]
      OldName = 'DataNav'
      OneOnRow = True
      Row = 0
      UseOwnFont = True
      Visible = True
      WholeRow = True
    end
    object btnCustom1: TdxBarLargeButton
      Caption = #33258#23450#20041'1'
      Category = 0
      Hint = #33258#23450#20041'1'
      Visible = ivAlways
      LargeImageIndex = 14
      AutoGrayScale = False
      GlyphLayout = glLeft
    end
    object btnCustom2: TdxBarLargeButton
      Caption = #33258#23450#20041'2'
      Category = 0
      Hint = #33258#23450#20041'2'
      Visible = ivAlways
      LargeImageIndex = 14
      AutoGrayScale = False
      GlyphLayout = glLeft
    end
    object btnCustom3: TdxBarLargeButton
      Caption = #33258#23450#20041'3'
      Category = 0
      Hint = #33258#23450#20041'3'
      Visible = ivAlways
      LargeImageIndex = 14
      AutoGrayScale = False
      GlyphLayout = glLeft
    end
    object btnCustom4: TdxBarLargeButton
      Caption = #33258#23450#20041'4'
      Category = 0
      Hint = #33258#23450#20041'4'
      Visible = ivAlways
      LargeImageIndex = 14
      AutoGrayScale = False
      GlyphLayout = glLeft
    end
    object btnCustom5: TdxBarLargeButton
      Caption = #33258#23450#20041'5'
      Category = 0
      Hint = #33258#23450#20041'5'
      Visible = ivAlways
      LargeImageIndex = 14
      AutoGrayScale = False
      GlyphLayout = glLeft
    end
  end
  object ABcxPivotGridPopupMenu1: TABcxPivotGridPopupMenu
    AutoHotkeys = maManual
    LinkcxDBPivotGrid = cxDBPivotGrid1
    Left = 144
    Top = 248
  end
  object ABThirdQuery2: TABThirdQuery
    ActiveStoredUsage = []
    ReadOnly = False
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    EditControlType = ctSingle
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    IndexListDefs = <>
    Left = 184
    Top = 96
  end
  object FireDACConnection1: TFireDACConnection
    ResourceOptions.AssignedValues = [rvAutoReconnect]
    ResourceOptions.AutoReconnect = True
    Left = 717
    Top = 326
  end
  object FireDACQuery1: TFireDACQuery
    Left = 829
    Top = 230
  end
  object FDQuery1: TFDQuery
    Left = 749
    Top = 150
  end
  object FDMemTable1: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    Left = 821
    Top = 318
  end
  object FDMoniRemoteClientLink1: TFDMoniRemoteClientLink
    Left = 845
    Top = 142
  end
end
