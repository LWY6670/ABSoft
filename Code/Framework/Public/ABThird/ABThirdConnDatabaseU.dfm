object ABConnDatabaseForm: TABConnDatabaseForm
  Left = 438
  Top = 273
  AutoSize = True
  BorderStyle = bsDialog
  Caption = #25968#25454#24211#36830#25509
  ClientHeight = 230
  ClientWidth = 664
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PopupMenu = PopupMenu1
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 210
    Height = 230
    Align = alLeft
    PopupMenu = PopupMenu1
    TabOrder = 0
    DesignSize = (
      210
      230)
    object Label1: TLabel
      Left = 11
      Top = 28
      Width = 44
      Height = 13
      AutoSize = False
      Caption = #26381#21153#22120'   '
      Transparent = True
    end
    object Label2: TLabel
      Left = 11
      Top = 60
      Width = 44
      Height = 13
      AutoSize = False
      Caption = #25968#25454#24211'   '
      Transparent = True
    end
    object Label3: TLabel
      Left = 11
      Top = 92
      Width = 44
      Height = 13
      AutoSize = False
      Caption = #29992#25143#21517'   '
      Transparent = True
    end
    object Label4: TLabel
      Left = 11
      Top = 125
      Width = 44
      Height = 13
      AutoSize = False
      Caption = #23494#30721'   '
      Transparent = True
    end
    object SpeedButton2: TSpeedButton
      Left = 184
      Top = 25
      Width = 20
      Height = 21
      Anchors = [akTop, akRight]
      Caption = '...'
      Flat = True
      OnClick = SpeedButton2Click
    end
    object SpeedButton3: TSpeedButton
      Left = 184
      Top = 57
      Width = 20
      Height = 21
      Anchors = [akTop, akRight]
      Caption = '...'
      Flat = True
      OnClick = SpeedButton3Click
    end
    object SpeedButton1: TSpeedButton
      Left = 1
      Top = 199
      Width = 208
      Height = 25
      Caption = #22810#25968#25454#24211#36830#25509
      Flat = True
      OnClick = SpeedButton1Click
    end
    object BitBtn1: TBitBtn
      Left = 28
      Top = 166
      Width = 75
      Height = 25
      Caption = #27979#35797
      Default = True
      TabOrder = 4
      OnClick = BitBtn1Click
    end
    object Edit3: TEdit
      Left = 58
      Top = 90
      Width = 127
      Height = 21
      Hint = #19981#25903#25345#23494#30721#20026#31354#30340#29992#25143#21517#30331#24405
      Anchors = [akLeft, akTop, akRight]
      ImeName = #20013#25991' ('#31616#20307') - '#24494#36719#25340#38899
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
    end
    object Edit4: TEdit
      Left = 58
      Top = 122
      Width = 127
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      ImeName = #20013#25991' ('#31616#20307') - '#24494#36719#25340#38899
      PasswordChar = '*'
      TabOrder = 3
    end
    object BitBtn2: TBitBtn
      Left = 109
      Top = 166
      Width = 75
      Height = 25
      Caption = #30830#35748
      Default = True
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333330000333333333333333333333333F33333333333
        00003333344333333333333333388F3333333333000033334224333333333333
        338338F3333333330000333422224333333333333833338F3333333300003342
        222224333333333383333338F3333333000034222A22224333333338F338F333
        8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
        33333338F83338F338F33333000033A33333A222433333338333338F338F3333
        0000333333333A222433333333333338F338F33300003333333333A222433333
        333333338F338F33000033333333333A222433333333333338F338F300003333
        33333333A222433333333333338F338F00003333333333333A22433333333333
        3338F38F000033333333333333A223333333333333338F830000333333333333
        333A333333333333333338330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
      TabOrder = 5
      OnClick = BitBtn2Click
    end
    object ComboBox1: TComboBox
      Tag = 910
      Left = 58
      Top = 25
      Width = 127
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      ImeName = #20013#25991' ('#31616#20307') - '#24494#36719#25340#38899
      TabOrder = 0
    end
    object ComboBox2: TComboBox
      Tag = 910
      Left = 58
      Top = 57
      Width = 127
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      ImeName = #20013#25991' ('#31616#20307') - '#24494#36719#25340#38899
      TabOrder = 1
    end
  end
  object Panel2: TPanel
    Left = 210
    Top = 0
    Width = 454
    Height = 230
    TabOrder = 1
    Visible = False
    object Panel3: TPanel
      Left = 1
      Top = 203
      Width = 452
      Height = 26
      Align = alBottom
      TabOrder = 0
      object ABMultilingualDBNavigator1: TABMultilingualDBNavigator
        Left = 1
        Top = 1
        Width = 339
        Height = 24
        DataSource = DataSource1
        VisibleButtons = [nbInsert, nbDelete, nbEdit, nbPost, nbCancel]
        Align = alClient
        TabOrder = 0
      end
      object Panel4: TPanel
        Left = 340
        Top = 1
        Width = 111
        Height = 24
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object Button1: TButton
          Left = 3
          Top = 0
          Width = 107
          Height = 25
          Caption = #27979#35797#25152#26377#36830#25509
          TabOrder = 0
          OnClick = Button1Click
        end
      end
    end
    object DBGrid1: TDBGrid
      Left = 1
      Top = 1
      Width = 452
      Height = 202
      Align = alClient
      DataSource = DataSource1
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'CL_Name'
          Title.Caption = #36830#25509#21517#31216
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CL_Server'
          PickList.Strings = (
            '.')
          Title.Caption = #26381#21153#22120
          Width = 83
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CL_DatabaseName'
          Title.Caption = #25968#25454#24211
          Width = 77
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CL_UserName'
          PickList.Strings = (
            'sa')
          Title.Caption = #29992#25143#21517
          Width = 53
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CL_Password'
          Title.Caption = #23494#30721
          Width = 66
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CL_NoUse'
          PickList.Strings = (
            'True'
            'False')
          Title.Caption = #26242#20572
          Width = 66
          Visible = True
        end>
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 128
    Top = 8
    object N1: TMenuItem
      Caption = #37325#21551#21160#20999#25442#21040#19977#23618
      OnClick = N1Click
    end
  end
  object DataSource1: TDataSource
    AutoEdit = False
    DataSet = FireDACQuery1
    Left = 365
    Top = 120
  end
  object FireDACQuery1: TFireDACQuery
    Beforedelete = FireDACQuery1Beforedelete
    AfterInsert = FireDACQuery1AfterInsert
    Left = 469
    Top = 120
  end
  object FireDACConnection1: TFireDACConnection
    Params.Strings = (
      'LoginTimeout=33'
      'DriverID=MSSQL'
      'OSAuthent=Yes'
      'Server=11'
      'Database=Buga')
    ResourceOptions.AssignedValues = [rvAutoReconnect]
    ResourceOptions.AutoReconnect = True
    LoginPrompt = False
    Left = 421
    Top = 64
  end
end
