{
框架数据集单元
}
unit ABFrameworkQueryU;

interface
{$I ..\ABInclude.ini}

uses
  ABPubFuncU,
  ABpubconstU,
  ABPubDBU,
  ABPubScriptU,
  ABPubMessageU,
  ABPubLocalParamsU,
  ABPubLogU,
  ABPubFormU,

  ABThirdCustomQueryU,
  ABThirdQueryU,
  ABThirdDBU,
  ABThirdConnU,
  ABThirdFuncU,

  ABFrameworkVarU,
  ABFrameworkControlU,
  ABFrameworkFuncU,
  ABFrameworkDictionaryQueryU,
  ABFrameworkConstU,
  ABFrameworkcxGridU,
  ABFrameworkDBPanelU,
  ABFrameworkQueryFormU,
  ABFrameworkDBPanelFormU,
  ABFrameworkDBNavigatorU,
  ABFrameworkQuerySelectFieldPanelU,

  math,
  Types,StdConvs,Graphics,cxGridLevel,

  SysUtils,Variants,Classes,Controls,Forms,DB,StdCtrls,ExtCtrls,
  cxCustomData,
  cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxGrid, cxPC,
  cxSplitter, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxBarBuiltInMenu;


type
  //默认不能自动进入修改，需按修改按钮才能修改数据
  TABDatasource            = class(TDatasource           )
  private
    { Private declarations }
  protected
    { Protected declarations }
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    { Public declarations }
  published
    { Published declarations }
  end;

  //框架桌面数据集的子类
  //1.关联表格与面板的显示控件
  //2.加载与保存框架表中设置的SQL
  TABQuery = class(TABDictionaryQuery)
  private
    //面板方式显示数据的窗体
    FDBPanelForm:TForm;
    //关联的GRID多选更新相关
    FMultiSelectFieldNames:array of string;
    FMultiSelectFieldValues:array of Variant;
    FMultiSelectValuesIsCale:array of Boolean;
    FMultiSelectTableView: TABcxGridDBBandedTableView;
    //查询窗体
    FQueryForm: TABQueryForm;
  private
    FStopMultiSelectUpdate:Boolean;
    FOnAfterShowDBPanel: TNotifyEvent;
    FOnBeforeShowDBPanel: TNotifyEvent;
    { Private declarations }
  protected
    procedure DoBeforeCommitTrans;override;

    procedure Loaded; override;
    procedure DoBeforePost; override;
    procedure DoBeforeInsert; override;

    procedure DoAfterOpen;   override;
    procedure DoAfterCancel; override;
    procedure DoAfterEdit;  override;
    procedure DoAfterInsert; override;
    procedure DoAfterPost;   override;
    procedure AfterFieldChange(aField: TField; var aDo: Boolean);override;
    { Protected declarations }
    { Public declarations }
  public
    procedure CreateFieldExtInfos;override;
    procedure FreeFieldExtInfos;override;
    //停用关联的GRID多选时的更新
    property StopMultiSelectUpdate:Boolean  read FStopMultiSelectUpdate  write FStopMultiSelectUpdate;

    //开窗查询数据
    procedure OpenQuery;override;
    //以面板方式显示当前记录的数据
    //aCanEdit=显示的数据是否能编辑
    //aActivePageCaption=当前页面标题
    procedure ShowDBPanel(aCanEdit:boolean;
                          aActivePageCaption:string='';
                          aEnabledFieldNames:string='';
                          aVisibleFieldNames:string=''
                          );override;


    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    //以面板方式显示当前记录事件
    property OnBeforeShowDBPanel: TNotifyEvent read FOnBeforeShowDBPanel write FOnBeforeShowDBPanel;
    property OnAfterShowDBPanel: TNotifyEvent read FOnAfterShowDBPanel write FOnAfterShowDBPanel;
    { Published declarations }
  end;

type
  TTWODatasourceArray= array of array of TDatasource;
  TTWODBPanelArray=array of array of TABDBPanel;
  TTWOTableViewArray= array of array of TABcxGridDBBandedTableView;
  TTWOboolean= array of array of boolean;
  TTWOString= array of array of String;

  TThreeDBPanelArray= array of array of array of TABDBPanel;
  TThreeTableViewArray= array of array of array of TABcxGridDBBandedTableView;



//字段对有定义取值SQL的影响
procedure ABFieldForGetValueField(aFieldDef:PABFieldDef;aCheckDataset: TDataSet;aIsNotChangeField:boolean=false;aOnlySetOnNull:boolean=false);
//处理字段的编辑对其它引用到此字段的字段取值的影响,aFieldDef为其它字段SQL中引用到的字段名
procedure ABFieldChangeForCiteField(aFieldDef:PABFieldDef);
//关联替换字段的处理
procedure ABFieldChangeForRelatingField(aFieldDef:PABFieldDef;aFromDataset:TDataSet;aOnlySetOnNull:boolean=false);

//设置DataSet关联控件控制，为了提高效率,只在数据集的AfterEdit,AfterInsert,AfterCancel,AfterPost中调用
procedure ABSetDataSetControlOnSateChange(aDataSet:TDataSet;
                              aDBPanel:Boolean=False;aDBGrid:Boolean=False;
                              aInit:boolean=false);

//设置DataSet动态控制（指根据其它字段的值决定）
//为了提高效率,只在数据集的AfterEdit AfterFieldChange中调用
procedure ABSetDataSetCiteControl(aDataSet:TDataSet;aFieldDef:PABFieldDef=nil;
                                  aDBPanel:Boolean=False;aDBGrid:Boolean=False);

//设置字段的颜色控制
procedure ABSetFieldReadOnlyAndColor(aFieldDef: PABFieldDef;
                           aReadOnly: boolean;
                           aDBPanel:Boolean=False;aDBGrid:Boolean=False;aCancel:boolean=false);

//功能中窗体数据集初始化
//aPageControl=窗体页控件
//aDBPanels=关联的面板输入控件
//aTableViews=关联的面板表格控件
//aDataSet=数据集
//aGetData=打开后是否取得数据
//aLoadAutoSQL=是否加载框架表中的SQL
//aSetActive=是否将数据集打开
procedure ABInitFormDataSet(  aDBPanels: array of TABDBPanel;
                              aTableViews:array of TABcxGridDBBandedTableView;
                              aQueryPanels:array of TABQuerySelectFieldPanel;
                              aDataSet: TABDictionaryQuery;
                              aGetData:Boolean=true;
                              aLoadAutoSQL:boolean=true;
                              aSetActive:boolean=true
                              );
//初始化模板窗体
//aPageControl=窗体页控件
procedure ABInitTemplateForm(aPageControl:TABcxPageControl);

//功能中多页窗体数据集初始化
//aPageControl=窗体页控件
//aNavigator=窗体主数据工具栏控件
//aDBStatusBar=窗体主数据状态栏控件

//aMainDatasources=主表数据源数组
//aMainDBPanels=主表数据源面板数组
//aMainTableViews=主表数据源表格数组
//aMainGetData=主表数据源默认是否取出数据
//aMainLoadAutoSQL=主表数据源是否取设置的SQL
//aMainSetActive=主表数据源是否打开数据集

//aDetailDatasources=从表数据源数组
//aDetailDBPanels=从表数据源面板数组
//aDetailTableViews=从表数据源表格数组
//aDetailGetData=从表数据源默认是否取出数据
//aDetailLoadAutoSQL=从表数据源是否取设置的SQL
//aDetailSetActive=从表数据源是否打开数据集

//aChangePageControlRefreshMainData=在切换页面时是否刷新主数据源数据
procedure ABInitFormPageControl(
                                aPageControl:TcxPageControl;

                                aNavigator:TABDBNavigator;
                                aDBStatusBar:TABdxDBStatusBar;

                                aMainDatasources:array of TDatasource;
                                aMainDBPanels: TTWODBPanelArray;
                                aMainTableViews:TTWOTableViewArray;

                                aMainGetData:array of Boolean;
                                aMainLoadAutoSQL:array of boolean;
                                aMainSetActive:array of boolean;

                                aDetailDatasources:TTWODatasourceArray;
                                aDetailDBPanels:TThreeDBPanelArray;
                                aDetailTableViews:TThreeTableViewArray;

                                aDetailGetData:TTWOboolean;
                                aDetailLoadAutoSQL:TTWOboolean;
                                aDetailSetActive:TTWOboolean;

                                aChangePageControlRefreshMainData:array of  boolean
                                );

//设置关联到数据集的子数据集连接控件的编辑状态
procedure ABSetDetailDatasetEditState(aDataSet:TABThirdQuery;aCanEdit:Boolean);


implementation

procedure ABSetDetailDatasetEditState(aDataSet:TABThirdQuery;aCanEdit:Boolean);
var
  i:LongInt;
  tempList1:TList;
  tempButtons: TABDBNavigatorButtonSet;
begin
  if (Assigned(aDataSet)) and
     (Assigned(aDataSet.Owner)) and
     (aDataSet.Owner is TForm) then
  begin
    if (aDataSet.IsMainControl) then
    begin
      tempList1 := TList.Create;
      try
        //取得设置的数据集列表
        ABGetDetailDatasetList(aDataSet,tempList1,False);
        if tempList1.Count>0  then
        begin
          for I := 0 to aDataSet.Owner.ComponentCount - 1 do
          begin
            if (aDataSet.Owner.Components[i] is  TABDBNavigator) and
               (Assigned(TABDBNavigator(aDataSet.Owner.Components[i]).DataSource)) and
               (Assigned(TABDBNavigator(aDataSet.Owner.Components[i]).DataSource.DataSet)) and
               (tempList1.IndexOf(TABDBNavigator(aDataSet.Owner.Components[i]).DataSource.DataSet)>=0) then
            begin
              ABPostDataset(TABDBNavigator(aDataSet.Owner.Components[i]).DataSource.DataSet);
              TABDBNavigator(aDataSet.Owner.Components[i]).DataSource.AutoEdit:= aCanEdit;

              tempButtons:=
                [
                nbFirstRecord           ,
                nbPreviousRecord        ,
                nbNextRecord            ,
                nbLastRecord            ,

                nbInsertSpacer          ,
                nbEdit                  ,

                nbPostSpacer            ,
                nbPost                  ,
                nbCancel                ,

                nbQuerySpacer           ,
                nbQuery                 ,
                nbReport               ,

                nbCustomSpacer         ,
                nbCustom1            ,
                nbCustom2            ,
                nbCustom3            ,
                nbCustom4            ,
                nbCustom5            ,
                nbCustom6            ,
                nbCustom7            ,
                nbCustom8            ,
                nbCustom9            ,
                nbCustom10           ,

                nbExitSpacer         ,
                nbExit               ,
                nbNull               ];

              if aCanEdit then
              begin
                tempButtons:=tempButtons+
                              [
                              nbInsert                ,
                              nbCopy                  ,
                              nbDelete                ];

              end;

              TABDBNavigator(aDataSet.Owner.Components[i]).SetBtnEnabled(tempButtons);
            end;
          end;
        end;
      finally
        tempList1.Free;
      end;

    end;
  end;
end;

procedure ABInitTemplateForm(aPageControl:TABcxPageControl);
var
  tempDataset_Func:TDataSet;
  tempDataset_FuncMainTemplateSetup:TDataSet;
  tempDataset_FuncMainTemplatePanelSetup:TDataSet;
  tempDataset_FuncDetailTemplateSetup:TDataSet;

  tempDataSet:TABQuery;
  tempDatasource:TABDataSource;
  tempGrid: TABcxGrid;
  tempSplitter: TABcxSplitter;
  tempPanel:TPanel;

  tempMainDatasource:TABDataSource;
  tempDetailDataSet:TABQuery;
  tempMainGrid: TABcxGrid;

  tempClientPanel:TPanel;
  tempMainDBPanelPageControl: TABcxPageControl;
  tempDetailPageControl: TABcxPageControl;
  tempNavigator: TABDBNavigator;

  procedure DoCreateTableView(
                               aPageControl:TABcxPageControl;
                               aCaption:string;
                               aNameFlag:string;

                               aConnName,
                               aSQL:string;
                               aInitTableName:string;
                               aUpdateTableName:string;
                               aDatasetOperateTypes:TABDatasetOperateTypes
                                );
  var
    tempTabSheet : TcxTabSheet;
    tempTableView: TABcxGridDBBandedTableView;
    tempLevel: TcxGridLevel;
    tempOnChange: TNotifyEvent;
  begin
    tempDataSet:=TABQuery.Create(aPageControl.Owner);
    tempDataSet.Name:='ABQuery'+aNameFlag;
    tempDataSet.MultiLevelCaption:=aCaption;
    if (not (dtDelete in aDatasetOperateTypes)) and
       (not (dtAppend in aDatasetOperateTypes)) and
       (not (dtEdit in aDatasetOperateTypes)) then
    begin
      tempDataSet.UpdateOptions.ReadOnly:=true;
    end;
    tempDataSet.ConnName:=aConnName;
    tempDataSet.Sql.Text:=aSQL;
    if aInitTableName<>EmptyStr then
      ABStrsToStrings(aInitTableName,',',tempDataSet.LoadTables,True,false);
    if aUpdateTableName<>EmptyStr then
      ABStrsToStrings(aUpdateTableName,',',tempDataSet.UpdateTables,True,false);

    tempDatasource:=TABDataSource.Create(aPageControl.Owner);
    tempDatasource.DataSet:=tempDataSet;
    tempDatasource.Name:='ABDataSource'+aNameFlag;

    tempOnChange:=aPageControl.OnChange;
    aPageControl.OnChange:=nil;
    try
      tempTabSheet := TcxTabSheet.Create(aPageControl.Owner);
      tempTabSheet.Name:='ABTabSheet'+aNameFlag;
      tempTabSheet.PageControl:= aPageControl;
      tempTabSheet.Caption :=aCaption ;
    finally
      aPageControl.OnChange:=tempOnChange;
    end;

    tempNavigator:= TABDBNavigator.Create(aPageControl.Owner);
    tempNavigator.VisibleButtons := [nbInsert, nbCopy,nbDelete, nbEdit, nbPostSpacer, nbPost, nbCancel];
    //  tempNavigator.VisibleButtons:=tempNavigator.VisibleButtons+[nbQuerySpacer,nbQuery,nbReport];
    tempNavigator.Name:='ABNavigator'+aNameFlag;
    tempNavigator.Parent         := tempTabSheet ;
    tempNavigator.Align          := alTop ;
    tempNavigator.TabOrder       := 0 ;
    tempNavigator.DataSource     := tempDatasource;

    tempPanel:= TPanel.Create(aPageControl.Owner);
    tempPanel.Name:='ParentPanel'+aNameFlag;
    tempPanel.Parent         := tempTabSheet ;
    tempPanel.Align          := alClient ;
    tempPanel.BevelOuter     := bvNone ;
    tempPanel.Caption        := EmptyStr ;

    tempGrid       := TABcxGrid.Create(aPageControl.Owner);
    tempGrid.Name:='ABGrid'+aNameFlag;
    tempGrid.Parent:= tempPanel ;
    tempGrid.Align          := alClient ;
    tempGrid.TabOrder:=1;

    tempTableView     := TABcxGridDBBandedTableView(tempGrid.CreateView(TABcxGridDBBandedTableView));
    tempTableView.Name:='ABTableView'+aNameFlag;
    tempLevel         := tempGrid.Levels.Add;
    tempLevel.Name    :='ABLevel'+aNameFlag;
    tempLevel.GridView:=tempTableView;
    tempTableView.OptionsView.BandHeaders:=False;
    tempTableView.OptionsView.GroupByBox:=False;
    tempTableView.OptionsView.Indicator:=true;
    tempTableView.ExtPopupMenu.SetupFileNameSuffix:=TForm(aPageControl.Owner).Caption;
    tempTableView.Bands.Add;
    tempTableView.DataController.DataSource   :=  tempDatasource;
    if (dtAppend in aDatasetOperateTypes) then
    begin
      tempNavigator.VisibleButtons :=tempNavigator.VisibleButtons+ [nbInsertSpacer,nbInsert,nbCopy];
      tempTableView.OptionsData.Appending :=true;
      tempTableView.OptionsData.Inserting :=true;
    end
    else
    begin
      tempNavigator.VisibleButtons :=tempNavigator.VisibleButtons- [nbInsertSpacer,nbInsert,nbCopy];
      tempTableView.OptionsData.Appending :=false;
      tempTableView.OptionsData.Inserting :=false;
    end;
    if (dtEdit in aDatasetOperateTypes) then
    begin
      tempNavigator.VisibleButtons :=tempNavigator.VisibleButtons+ [nbEdit];
      tempTableView.OptionsData.Editing :=true;
    end
    else
    begin
      tempNavigator.VisibleButtons :=tempNavigator.VisibleButtons- [nbEdit];
      tempTableView.OptionsData.Editing :=false;
    end;
    if (dtDelete in aDatasetOperateTypes) then
    begin
      tempNavigator.VisibleButtons :=tempNavigator.VisibleButtons+ [nbDelete];
      tempTableView.OptionsData.Deleting :=true;
    end
    else
    begin
      tempNavigator.VisibleButtons :=tempNavigator.VisibleButtons- [nbDelete];
      tempTableView.OptionsData.Deleting :=false;
    end;
    if ( dtAppend in aDatasetOperateTypes) or
       ( dtEdit in aDatasetOperateTypes)  then
      tempNavigator.VisibleButtons:=tempNavigator.VisibleButtons+[nbPostSpacer,nbPost,nbCancel]
    else if not (dtDelete in aDatasetOperateTypes) then
    begin
      tempNavigator.VisibleButtons:=tempNavigator.VisibleButtons-[nbPostSpacer,nbPost,nbCancel];
    end;

    tempNavigator.Visible :=not (tempNavigator.VisibleButtons=[]);
    if (dtselect in aDatasetOperateTypes) then
    begin
      tempTableView.OptionsSelection.CellSelect  :=not (dtselect in aDatasetOperateTypes);
      tempTableView.OptionsSelection.MultiSelect :=(dtselect in aDatasetOperateTypes);
    end;
  end;

  procedure DoCreateDBPanel(
                               aPageControl:TABcxPageControl;
                               aCaption:string;
                               aNameFlag:string;
                               aDatasource:TABDataSource
                                );
  var
    tempTabSheet : TcxTabSheet;
    tempOnChange: TNotifyEvent;
    tempABDBPanel:TABDBPanel;
  begin
    tempOnChange:=aPageControl.OnChange;
    aPageControl.OnChange:=nil;
    try
      tempTabSheet := TcxTabSheet.Create(aPageControl.Owner);
      tempTabSheet.Name:='ABMainDBPanelTabSheet'+aNameFlag;
      tempTabSheet.PageControl:= aPageControl;
      tempTabSheet.Caption :=aCaption ;

      tempABDBPanel:= TABDBPanel.Create(aPageControl.Owner);
      tempABDBPanel.Parent         := tempTabSheet ;
      tempABDBPanel.Name           :='ABMainDBPanel'+aNameFlag;
      tempABDBPanel.Align          := alClient ;
      tempABDBPanel.BevelOuter     := bvNone ;
      tempABDBPanel.Caption        := EmptyStr ;
      tempABDBPanel.DataSource     := aDatasource ;
    finally
      aPageControl.OnChange:=tempOnChange;
    end;
  end;
begin
  if aPageControl.Owner is TABPubForm then
  begin
    tempDataset_Func:=ABGetConstSqlPubDataset('ABSys_Org_Function',[TABPubForm(aPageControl.Owner).FileName]);
    if (Assigned(tempDataset_Func)) then
    begin
      //主表
      tempDataset_FuncMainTemplateSetup:=ABGetConstSqlPubDataset('ABSys_Org_FuncMainTemplateSetup',[tempDataset_Func.FieldByName('FU_Guid').AsString]);
      if (Assigned(tempDataset_FuncMainTemplateSetup)) then
      begin
        tempDataset_FuncMainTemplateSetup.First;
        while not tempDataset_FuncMainTemplateSetup.Eof do
        begin
          tempDataset_FuncMainTemplatePanelSetup:=ABGetConstSqlPubDataset('ABSys_Org_FuncMainTemplatePanelSetup',[tempDataset_FuncMainTemplateSetup.FieldByName('FM_Guid').AsString]);
          tempDataset_FuncDetailTemplateSetup:=ABGetConstSqlPubDataset('ABSys_Org_FuncDetailTemplateSetup',[tempDataset_FuncMainTemplateSetup.FieldByName('FM_Guid').AsString]);
          DoCreateTableView(
                            aPageControl,
                            tempDataset_FuncMainTemplateSetup.FieldByName('FM_Caption').AsString,
                            inttostr(tempDataset_FuncMainTemplateSetup.RecNo),

                            ABGetConnNameByConnGuid(tempDataset_FuncMainTemplateSetup.FieldByName('FM_ConnName').AsString),
                            tempDataset_FuncMainTemplateSetup.FieldByName('FM_SQL').AsString,
                            tempDataset_FuncMainTemplateSetup.FieldByName('FM_InitTables').AsString,
                            tempDataset_FuncMainTemplateSetup.FieldByName('FM_UpdateTables').AsString,

                            [dtAppend,dtEdit,dtDelete]
                            );

          if tempDataset_FuncMainTemplateSetup.RecordCount=1 then
          begin
            tempNavigator.VisibleButtons :=tempNavigator.VisibleButtons+ [nbExitSpacer,nbExit];
          end;
          tempMainDatasource:=tempDatasource;
          tempMainGrid:=tempGrid;

          tempClientPanel :=tempPanel;
          tempMainDBPanelPageControl:=nil;
          tempDetailPageControl:=nil;

          if (Assigned(tempDataset_FuncMainTemplatePanelSetup)) and (not tempDataset_FuncMainTemplatePanelSetup.IsEmpty) or
             (Assigned(tempDataset_FuncDetailTemplateSetup)) and (not tempDataset_FuncDetailTemplateSetup.IsEmpty) then
          begin
            if (Assigned(tempDataset_FuncMainTemplatePanelSetup)) and (not tempDataset_FuncMainTemplatePanelSetup.IsEmpty) and
               (Assigned(tempDataset_FuncDetailTemplateSetup)) and (not tempDataset_FuncDetailTemplateSetup.IsEmpty) then
            begin
              tempMainGrid.Align:=alLeft;

              tempPanel:= TPanel.Create(aPageControl.Owner);
              tempPanel.Name:='DBPanelAndDetail'+inttostr(tempDataset_FuncMainTemplateSetup.RecNo);
              tempPanel.Parent         := tempClientPanel ;
              tempPanel.BevelOuter     := bvNone ;
              tempPanel.Caption        := EmptyStr ;
              tempPanel.Align          := alClient ;

              tempSplitter   := TABcxSplitter.Create(aPageControl.Owner);
              tempSplitter.Parent:= tempClientPanel ;
              tempSplitter.Name:='ABSplitter'+inttostr(tempDataset_FuncMainTemplateSetup.RecNo);
              tempSplitter.HotZoneClassName := 'TcxMediaPlayer8Style' ;
              tempSplitter.Control:= tempMainGrid ;
              tempSplitter.AlignSplitter := salLeft;
              tempSplitter.Left:=tempMainGrid.Left+tempMainGrid.Width;

              tempMainDBPanelPageControl:= TABcxPageControl.Create(aPageControl.Owner);
              tempMainDBPanelPageControl.Parent:= tempPanel ;
              tempMainDBPanelPageControl.Name:='ABDBPanelPageControl'+inttostr(tempDataset_FuncMainTemplateSetup.RecNo);
              tempMainDBPanelPageControl.Align:=alTop;

              tempSplitter:= TABcxSplitter.Create(aPageControl.Owner);
              tempSplitter.Parent:= tempPanel ;
              tempSplitter.Control:= tempMainDBPanelPageControl ;
              tempSplitter.Name:='ABSplitterDBPanel'+inttostr(tempDataset_FuncMainTemplateSetup.RecNo);
              tempSplitter.HotZoneClassName := 'TcxMediaPlayer8Style' ;
              tempSplitter.AlignSplitter := salTop ;
              tempSplitter.Top:=tempMainDBPanelPageControl.Top+tempMainDBPanelPageControl.Width;

              tempDetailPageControl:= TABcxPageControl.Create(aPageControl.Owner);
              tempDetailPageControl.Parent:= tempPanel ;
              tempDetailPageControl.Name:='ABDetailPageControl'+inttostr(tempDataset_FuncMainTemplateSetup.RecNo);
              tempDetailPageControl.Align:=alClient;
            end
            else
            begin
              tempMainGrid.Align:=alClient;

              tempSplitter   := TABcxSplitter.Create(aPageControl.Owner);
              tempSplitter.Parent:= tempClientPanel ;
              tempSplitter.Name:='ABSplitter'+inttostr(tempDataset_FuncMainTemplateSetup.RecNo);
              tempSplitter.HotZoneClassName := 'TcxMediaPlayer8Style' ;
              tempSplitter.AlignSplitter := salTop;

              if (Assigned(tempDataset_FuncMainTemplatePanelSetup)) and (not tempDataset_FuncMainTemplatePanelSetup.IsEmpty) then
              begin
                tempMainDBPanelPageControl:= TABcxPageControl.Create(aPageControl.Owner);
                tempMainDBPanelPageControl.Parent:= tempClientPanel ;
                tempMainDBPanelPageControl.Name:='ABDBPanelPageControl'+inttostr(tempDataset_FuncMainTemplateSetup.RecNo);
                tempMainDBPanelPageControl.Align:=alTop;
                tempSplitter.Control:= tempMainDBPanelPageControl ;
                tempSplitter.Top:=tempMainDBPanelPageControl.Top+tempMainDBPanelPageControl.Width;
              end
              else if (Assigned(tempDataset_FuncDetailTemplateSetup)) and (not tempDataset_FuncDetailTemplateSetup.IsEmpty) then
              begin
                tempDetailPageControl:= TABcxPageControl.Create(aPageControl.Owner);
                tempDetailPageControl.Parent:= tempClientPanel ;
                tempDetailPageControl.Name:='ABDetailPageControl'+inttostr(tempDataset_FuncMainTemplateSetup.RecNo);
                tempDetailPageControl.Align:=alTop;
                tempSplitter.Control:= tempDetailPageControl ;
                tempSplitter.Top:=tempDetailPageControl.Top+tempDetailPageControl.Width;
              end;
            end;
          end
          else
          begin
            tempMainGrid.Align:=alClient;
          end;
          //主表面板
          if (Assigned(tempDataset_FuncMainTemplatePanelSetup)) and
             (Assigned(tempMainDBPanelPageControl))  then
          begin
            tempDataset_FuncMainTemplatePanelSetup.First;
            while not tempDataset_FuncMainTemplatePanelSetup.Eof do
            begin
              DoCreateDBPanel(
                                tempMainDBPanelPageControl,
                                tempDataset_FuncMainTemplatePanelSetup.FieldByName('FP_Caption').AsString,
                                inttostr(tempDataset_FuncMainTemplateSetup.RecNo)+'_'+inttostr(tempDataset_FuncMainTemplatePanelSetup.RecNo),
                                tempMainDatasource
                                );

              tempDataset_FuncMainTemplatePanelSetup.Next;
            end;
          end;

          //从表
          if (Assigned(tempDataset_FuncDetailTemplateSetup)) and
             (Assigned(tempDetailPageControl))  then
          begin
            tempDataset_FuncDetailTemplateSetup.First;
            while not tempDataset_FuncDetailTemplateSetup.Eof do
            begin
              DoCreateTableView(
                                tempDetailPageControl,
                                tempDataset_FuncDetailTemplateSetup.FieldByName('FD_Caption').AsString,
                                inttostr(tempDataset_FuncMainTemplateSetup.RecNo)+'_'+inttostr(tempDataset_FuncDetailTemplateSetup.RecNo),

                                ABGetConnNameByConnGuid(tempDataset_FuncDetailTemplateSetup.FieldByName('FD_ConnName').AsString),
                                tempDataset_FuncDetailTemplateSetup.FieldByName('FD_SQL').AsString,
                                tempDataset_FuncDetailTemplateSetup.FieldByName('FD_InitTables').AsString,
                                tempDataset_FuncDetailTemplateSetup.FieldByName('FD_UpdateTables').AsString,

                                [dtAppend,dtEdit,dtDelete]
                                );

              tempDetailDataSet:=tempDataSet;

              tempDetailDataSet.DataSource:=tempMainDataSource;
              tempDetailDataSet.MasterFields:=tempDataset_FuncDetailTemplateSetup.FieldByName('FD_MasterFields').AsString;
              tempDetailDataSet.DetailFields:=tempDataset_FuncDetailTemplateSetup.FieldByName('FD_DetailFields').AsString;

              tempDataset_FuncDetailTemplateSetup.Next;
            end;
          end;
          if (Assigned(tempDetailPageControl)) then
          begin
            tempDetailPageControl.HideTabs:=tempDetailPageControl.PageCount<=1;
            if tempDetailPageControl.PageCount>0 then
              tempDetailPageControl.ActivePageIndex:=0;
          end;
          if (Assigned(tempMainDBPanelPageControl)) then
          begin
            tempMainDBPanelPageControl.HideTabs:=tempMainDBPanelPageControl.PageCount<=1;
            if tempMainDBPanelPageControl.PageCount>0 then
              tempMainDBPanelPageControl.ActivePageIndex:=0;
          end;

          tempDataset_FuncMainTemplateSetup.Next;
        end;
        if (Assigned(aPageControl)) then
        begin
          aPageControl.HideTabs:=aPageControl.PageCount<=1;
          if aPageControl.PageCount>0 then
            aPageControl.ActivePageIndex:=0;
        end;
      end;
    end;
  end;
end;

procedure ABInitFormPageControl(
                                aPageControl:TcxPageControl;

                                aNavigator:TABDBNavigator;
                                aDBStatusBar:TABdxDBStatusBar;

                                aMainDatasources:array of TDatasource;
                                aMainDBPanels: TTWODBPanelArray;
                                aMainTableViews:TTWOTableViewArray;

                                aMainGetData:array of Boolean;
                                aMainLoadAutoSQL:array of boolean;
                                aMainSetActive:array of boolean;

                                aDetailDatasources:TTWODatasourceArray;
                                aDetailDBPanels:TThreeDBPanelArray;
                                aDetailTableViews:TThreeTableViewArray;

                                aDetailGetData:TTWOboolean;
                                aDetailLoadAutoSQL:TTWOboolean;
                                aDetailSetActive:TTWOboolean;

                                aChangePageControlRefreshMainData:array of  boolean
                                );
var
  tempIndex,j: Integer;
begin
  tempIndex:=aPageControl.ActivePageIndex;
  if (aPageControl.ActivePageIndex<=High(aMainDatasources)-low(aMainDatasources)) then
  begin
    if (Assigned(aNavigator)) and (aNavigator.Visible) then
      ABSetObjectPropValue(aNavigator,'DataSource',aMainDatasources[tempIndex]);
    if (Assigned(aDBStatusBar)) and (aDBStatusBar.Visible) then
      ABSetObjectPropValue(aDBStatusBar,'DataSource',aMainDatasources[tempIndex]);
  end;

  if not TABQuery(aMainDatasources[tempIndex].DataSet).Active then
  begin
    ABInitFormDataSet(aMainDBPanels[tempIndex],aMainTableViews[tempIndex],[],TABQuery(aMainDatasources[tempIndex].DataSet),
                      aMainGetData[tempIndex],aMainLoadAutoSQL[tempIndex],aMainSetActive[tempIndex]);

    if  tempIndex<= High(aDetailDatasources)-Low(aDetailDatasources) then
    begin
      for j := Low(aDetailDatasources[tempIndex]) to High(aDetailDatasources[tempIndex]) do
      begin
        if  (tempIndex<= High(aDetailDBPanels)-Low(aDetailDBPanels)) and
            (j<= High(aDetailDBPanels[tempIndex])-Low(aDetailDBPanels[tempIndex])) and
            (tempIndex<= High(aDetailTableViews)-Low(aDetailTableViews)) and
            (j<= High(aDetailTableViews[tempIndex])-Low(aDetailTableViews[tempIndex])) and
            (tempIndex<= High(aDetailDatasources)-Low(aDetailDatasources)) and
            (j<= High(aDetailDatasources[tempIndex])-Low(aDetailDatasources[tempIndex])) and
            (tempIndex<= High(aDetailGetData)-Low(aDetailGetData)) and
            (j<= High(aDetailGetData[tempIndex])-Low(aDetailGetData[tempIndex])) and
            (tempIndex<= High(aDetailLoadAutoSQL)-Low(aDetailLoadAutoSQL)) and
            (j<= High(aDetailLoadAutoSQL[tempIndex])-Low(aDetailLoadAutoSQL[tempIndex])) and
            (tempIndex<= High(aDetailSetActive)-Low(aDetailSetActive)) and
            (j<= High(aDetailSetActive[tempIndex])-Low(aDetailSetActive[tempIndex])) then
        begin
          ABInitFormDataSet(aDetailDBPanels[tempIndex][j],aDetailTableViews[tempIndex][j],[],TABQuery(aDetailDatasources[tempIndex][j].DataSet),
                            aDetailGetData[tempIndex][j],aDetailLoadAutoSQL[tempIndex][j],aDetailSetActive[tempIndex][j]);
        end;
      end;
    end;
  end
  else if aChangePageControlRefreshMainData[tempIndex] then
  begin
    ABReFreshQuery(TABQuery(aMainDatasources[tempIndex].DataSet),[]);
  end;
end;

procedure ABSetDataSetControlOnSateChange(aDataSet:TDataSet;
                              aDBPanel:Boolean;aDBGrid:Boolean;
                              aInit:boolean);
var
  i:LongInt;
  tempFieldDef:PABFieldDef;
  tempReadOnly:Boolean;
  procedure SetFieldUniqueNotNullColor(aFieldDef: PABFieldDef;aDBPanel:Boolean=False;aDBGrid:Boolean=False);
  var
    tempABDBWinControl:TWinControl;
    tempABDBLabel:TWinControl;
  begin
    if (aFieldDef.Fi_IsInputPanelView)  or
       (aFieldDef.Fi_IsGridView) then
    begin
      if (Assigned(aFieldDef.PMetaDef)) and (not aFieldDef.PMetaDef.Fi_CanNull) or
         (not aFieldDef.Fi_CanNull) then
      begin
        if (aFieldDef.Fi_IsInputPanelView) and (aDBPanel) then
        begin
          tempABDBLabel:=aFieldDef.Fi_Label;
          tempABDBWinControl:=aFieldDef.Fi_Control;
          if (Assigned(tempABDBLabel))  then
          begin
            if (ABQueryParams_ColorInLableOrControl=1)  or
               (ABQueryParams_ColorInLableOrControl>2)   then
            begin
              ABSetCortrolPropertyValue(tempABDBLabel,'Color',ABQueryParams_NotNullColor_Label);
            end;
          end;

          if (Assigned(tempABDBWinControl)) then
          begin
            if (ABQueryParams_ColorInLableOrControl>=2) then
            begin
              ABSetCortrolPropertyValue(tempABDBWinControl,'Color',ABQueryParams_NotNullColor_Control);
            end;
          end;
        end;

        if (aFieldDef.Fi_IsGridView) and (aDBGrid) then
        begin
          if (Assigned(aFieldDef.Fi_GridColumn)) then
          begin
            //设置列的非空字段颜色
          end;
        end;
      end;
    end;
  end;
begin
  if (not Assigned(aDataSet)) then
    exit;

  if (aDataSet.ControlsDisabled) then
    exit;

  if (aDataSet is TFireDACQuery) and
     (TFireDACQuery(aDataSet).BatchMode) then
    exit;

  for I := 0 to aDataSet.FieldCount - 1 do
  begin
    tempFieldDef:=ABFieldDefByFieldName(aDataSet,aDataSet.Fields[i].FieldName);
    if (not Assigned(tempFieldDef)) then
      Continue;

    //设置只读与非空字段
    tempReadOnly:= ((not tempFieldDef.Fi_CanEditInInsert) and (aDataSet.State in [dsInsert])) or
                   ((not tempFieldDef.Fi_CanEditInEdit) and (aDataSet.State in [dsEdit])) or
                   (not tempFieldDef.Fi_CanEditInEdit) and (not tempFieldDef.Fi_CanEditInInsert) ;
    ABSetFieldReadOnlyAndColor( tempFieldDef,tempReadOnly,
                                (aDBPanel) and (not tempFieldDef.Fi_LockInputPanelView),
                                (aDBGrid) and (not tempFieldDef.Fi_LockGridView),
                                 aInit);

    if (aDataSet.State in [dsEdit,dsInsert]) then
      SetFieldUniqueNotNullColor( tempFieldDef,
                                 (aDBPanel) and (not tempFieldDef.Fi_LockInputPanelView),
                                 (aDBGrid) and (not tempFieldDef.Fi_LockGridView));
  end;
end;

procedure ABSetDataSetCiteControl(aDataSet:TDataSet;
                                   aFieldDef:PABFieldDef;
                                   aDBPanel:Boolean;aDBGrid:Boolean);
var
  i:LongInt;
  tempFieldDef:PABFieldDef;
  tempReadOnly:Boolean;
  tempJavaCommand:string;
begin
  if (not Assigned(aDataSet)) then
    exit;

  if (aDataSet.ControlsDisabled) then
    exit;

  if (aDataSet is TFireDACQuery) and
     (TFireDACQuery(aDataSet).BatchMode)then
    exit;

  for I := 0 to aDataSet.FieldCount - 1 do
  begin
    tempFieldDef:=ABFieldDefByFieldName(aDataSet,aDataSet.Fields[i].FieldName);
    if not Assigned(tempFieldDef) then
      Continue;

    tempJavaCommand:= tempFieldDef.Fi_SetReadOnlyCommand;
    if (tempJavaCommand<>EmptyStr) then
    begin
      if  ((aFieldDef=nil) or
          (abpos(':'+aFieldDef.Fi_Name,tempJavaCommand)>0)
          ) then
      begin
        tempJavaCommand:= ABTransactQL(tempJavaCommand,[aDataSet]);
        tempReadOnly:=ABStrToBool(ABExecScript(tempJavaCommand));

        ABSetFieldReadOnlyAndColor(tempFieldDef,tempReadOnly,
                                   (aDBPanel) and (not tempFieldDef.Fi_LockInputPanelView),
                                   (aDBGrid) and (not tempFieldDef.Fi_LockGridView)
                                    );
      end;
    end;
  end;
end;

procedure ABSetFieldReadOnlyAndColor(aFieldDef: PABFieldDef;
                                     aReadOnly: boolean;aDBPanel:Boolean=False;aDBGrid:Boolean=False;aCancel:boolean=false);
var
  tempABDBWinControl:TWinControl;
  tempABDBLabel:TWinControl;
  tempColor_Label,tempColor_Control:LongInt;
begin
  if (not Assigned(aFieldDef)) then
    exit;
  if (not Assigned(aFieldDef.Field)) then
    exit;
  if (not Assigned(aFieldDef.Field.DataSet)) then
    exit;

  if (aFieldDef.Field.DataSet.ControlsDisabled) then
    exit;

  if (aFieldDef.Field.DataSet is TFireDACQuery) and
     (TFireDACQuery(aFieldDef.Field.DataSet).BatchMode) then
    exit;

  if (aFieldDef.Fi_IsInputPanelView)  or
     (aFieldDef.Fi_IsGridView) then
  begin
    if (aCancel) or (not aReadOnly) then
    begin
      tempColor_Label:=clWindowText;
      tempColor_Control:=clWindow;
      aReadOnly:=false;
    end
    else
    begin
      tempColor_Label:=ABQueryParams_ReadOnlyColor_Label;
      tempColor_Control:=ABQueryParams_ReadOnlyColor_Control;
    end;
    //设置只读的字段颜色
    if (aFieldDef.Fi_IsInputPanelView) and (aDBPanel) then
    begin
      tempABDBLabel:=aFieldDef.Fi_Label;
      tempABDBWinControl:=aFieldDef.Fi_Control;
      if (Assigned(tempABDBLabel))  then
      begin
        if (ABQueryParams_ColorInLableOrControl=1)  or
           (ABQueryParams_ColorInLableOrControl>2)   then
        begin
          ABSetCortrolPropertyValue(tempABDBLabel,'Color',tempColor_Label);
        end;
      end;
      if (Assigned(tempABDBWinControl)) then
      begin
        if (ABQueryParams_ColorInLableOrControl>=2)   then
        begin
          ABSetCortrolPropertyValue(tempABDBWinControl,'Color',tempColor_Control);
        end;
        ABSetCortrolPropertyValue(tempABDBWinControl,'ReadOnly',aReadOnly);
      end;
    end;

    if (aFieldDef.Fi_IsGridView) and (aDBGrid) then
    begin
      if  (Assigned(aFieldDef.Fi_GridColumn)) then
      begin
        //aFieldDef.Fi_GridColumn.Properties.ReadOnly:=aReadOnly;
        aFieldDef.Fi_GridColumn.Options.Editing:=not aReadOnly;
      end;
    end;
  end;
end;

procedure ABFieldChangeForRelatingField(aFieldDef:PABFieldDef;aFromDataset:TDataSet;aOnlySetOnNull:boolean=false);
var
  tempStr1,tempStr2:string;
  i:longint;
  tempABOnFieldValidate: TFieldNotifyEvent;
  tempABOnFieldChange: TFieldNotifyEvent;
  tempField:Tfield;
begin
  for i := 1 to ABGetSpaceStrCount(aFieldDef.Fi_RelatingFromFields,',') do
  begin
    tempStr1:=ABGetSpaceStr(aFieldDef.Fi_RelatingFromFields,i,',');
    tempStr2:=ABGetSpaceStr(aFieldDef.Fi_RelatingToFields,i,',');
    if (not aOnlySetOnNull) or
       (
        (aFieldDef.field.DataSet.FindField(tempStr2).IsNull) or
        (aFieldDef.field.DataSet.FindField(tempStr2).AsString=EmptyStr)
        ) then
    begin
      tempField:=aFieldDef.field.DataSet.FindField(tempStr2);
      tempABOnFieldChange:=tempField.OnChange;
      tempABOnFieldValidate:=tempField.OnValidate;
      tempField.OnChange:=nil;
      tempField.OnValidate:=nil;
      try
        ABSetFieldValue(aFromDataset.FindField(tempStr1).Value,
                        tempField);
      finally
        tempField.OnChange:=tempABOnFieldChange;
        tempField.OnValidate:=tempABOnFieldValidate;
      end;
    end;
  end;
end;

procedure ABFieldForGetValueField(aFieldDef:PABFieldDef;aCheckDataset: TDataSet;aIsNotChangeField:boolean;aOnlySetOnNull:boolean);
var
  tempDataSet: TDataSet;
  tempABOnFieldValidate: TFieldNotifyEvent;
  tempABOnFieldChange: TFieldNotifyEvent;
begin
  if (aFieldDef.Field.IsNull) or (aFieldDef.Field.AsString=EmptyStr) or (not aOnlySetOnNull)  then
  begin
    tempABOnFieldChange:=aFieldDef.Field.OnChange;
    tempABOnFieldValidate:=aFieldDef.Field.OnValidate;
    aFieldDef.Field.OnChange:=nil;
    aFieldDef.Field.OnValidate:=nil;
    try
      if ABIsSelectSQL(aFieldDef.Fi_GetValueSQL) then
      begin
        tempDataSet := ABGetDataset(TABThirdReadDataQuery(aFieldDef.Field.DataSet).ConnName, aFieldDef.Fi_GetValueSQL,
                                    [], aCheckDataset);
        try
          if tempDataSet.FieldCount=1 then
          begin
            if aIsNotChangeField then
            begin
              if tempDataSet.Fields[0].IsNull then
                ABSetFieldValue(null,aFieldDef.Field)
              else
                ABSetFieldValue(tempDataSet.Fields[0].Value,aFieldDef.Field);
            end;
          end
          else if tempDataSet.FieldCount>1 then
          begin
            if (aFieldDef.Fi_RelatingFromFields<>EmptyStr) and
               (aFieldDef.Fi_RelatingToFields<>EmptyStr)  then
            begin
              ABFieldChangeForRelatingField(aFieldDef,tempDataSet)
            end
          end;
        finally
          tempDataSet.Free;
        end;
      end;
    finally
      aFieldDef.Field.OnChange:=tempABOnFieldChange;
      aFieldDef.Field.OnValidate:=tempABOnFieldValidate;
    end;
  end;
end;

procedure ABFieldChangeForCiteField(aFieldDef:PABFieldDef);
var
  i,j:LongInt;
  tempFieldDef:PABFieldDef;
  tempstr,tempstr1,tempstr2,tempFieldName,tempFieldValue:string;

  tempOnGetText: TFieldGetTextEvent;
  tempOnSetText: TFieldSetTextEvent;
  tempOnValidate: TFieldNotifyEvent;
  tempOnChange: TFieldNotifyEvent;
begin
  for I := 0 to aFieldDef.field.DataSet.FieldCount - 1 do
  begin
    tempFieldDef:=ABFieldDefByFieldName(aFieldDef.field.DataSet,aFieldDef.field.DataSet.Fields[i].FieldName);
    if not Assigned(tempFieldDef) then  Continue;

    if (tempFieldDef.Field.FieldKind<>fkCalculated) and
       (tempFieldDef.Fi_GetValueSQL<>EmptyStr) and
       (abpos(':'+aFieldDef.Fi_Name,tempFieldDef.Fi_GetValueSQL)>0) then
    begin
      ABFieldForGetValueField(tempFieldDef,aFieldDef.field.DataSet,aFieldDef.Field<>aFieldDef.field.DataSet.Fields[i]);
    end;

    if (aFieldDef.Field<>aFieldDef.field.DataSet.Fields[i]) then
    begin
      if (tempFieldDef.IsDown)  then
      begin
        //下拉数据集包含当前字段的引用时的处理
        if (Assigned(ABGetParam(tempFieldDef.PDownDef.Fi_DataSet,aFieldDef.Field.FieldName))) then
        begin
          ABReFreshQuery(tempFieldDef.PDownDef.Fi_DataSet,[]);
          if tempFieldDef.PDownDef.Fi_DataSet.FieldCount=1 then
          begin
            ABSetFieldValue(tempFieldDef.PDownDef.Fi_DataSet.Fields[0].Value,tempFieldDef.Field);
          end
          else
          begin
            if (tempFieldDef.PDownDef.Fi_CheckInDown) and
               (not tempFieldDef.Field.IsNull) and
               (tempFieldDef.Field.asstring<>emptystr) then
            begin
              if not tempFieldDef.PDownDef.Fi_DataSet.Locate(
                                                        tempFieldDef.PDownDef.Fi_SaveField,
                                                        tempFieldDef.Field.AsString,
                                                        []) then
              begin
                ABBackAndStopFieldEvent(tempFieldDef.Field,tempOnGetText,tempOnSetText,tempOnValidate,tempOnChange);
                try
                  tempFieldDef.Field.Clear;
                finally
                  ABUnBackFieldEvent(tempFieldDef.Field,tempOnGetText,tempOnSetText,tempOnValidate,tempOnChange);
                end;
              end;
            end;
          end;
        end;

        //过滤条件包含当前字段的引用时的处理
        if abpos(':'+aFieldDef.Field.FieldName,tempFieldDef.PDownDef.Fi_Filter)>0 then
        begin
          if (tempFieldDef.PDownDef.Fi_CheckInDown) and
             (not tempFieldDef.Field.IsNull) and
             (tempFieldDef.Field.asstring<>emptystr) then
          begin
            tempstr1:=tempFieldDef.PDownDef.Fi_SaveField;
            tempstr2:=tempFieldDef.Field.AsString;
            for j := 1 to ABGetSpaceStrCount(tempFieldDef.PDownDef.Fi_Filter, ' and ') do
            begin
              tempstr := ABGetSpaceStr(tempFieldDef.PDownDef.Fi_Filter, j, ' and ');
              if ABPos(':' ,tempstr)>0 then
              begin
                tempstr:=ABTransactQL(tempstr,[aFieldDef.Field.DataSet]);

                tempFieldName:= ABGetLeftRightStr(tempstr,axdLeft);
                if tempFieldName<>EmptyStr then
                begin
                  tempstr1:=tempFieldName+';'+tempstr1;
                  tempFieldValue:= ABGetLeftRightStr(tempstr,axdRight);
                  tempstr2:=tempFieldValue+';'+tempstr2;
                end;
              end;
            end;
            if not tempFieldDef.PDownDef.Fi_DataSet.Locate(
                                                      tempstr1,
                                                      ABStrToStringArray(tempstr2,';'),
                                                      []) then
            begin
              ABBackAndStopFieldEvent(tempFieldDef.Field,tempOnGetText,tempOnSetText,tempOnValidate,tempOnChange);
              try
                tempFieldDef.Field.Clear;
              finally
                ABUnBackFieldEvent(tempFieldDef.Field,tempOnGetText,tempOnSetText,tempOnValidate,tempOnChange);
              end;
            end;
          end;
        end;
      end;
    end;
  end;
end;

procedure ABInitFormDataSet( aDBPanels: array of TABDBPanel;
                      aTableViews:array of TABcxGridDBBandedTableView;
                      aQueryPanels:array of TABQuerySelectFieldPanel;
                      aDataSet: TABDictionaryQuery;
                      aGetData:Boolean;
                      aLoadAutoSQL:boolean;
                      aSetActive:boolean
                      );
var
  i: Integer;

  tempOldBeforeScroll:TDataSetNotifyEvent;
  tempOldAfterScroll:TDataSetNotifyEvent;
  tempDataset:TDataSet;
begin
  if (ABLocalParams.Debug) then
    ABWriteLog('Begin procedure '+aDataSet.Owner.Name+' '+aDataSet.Name+'.ABInitFormDataSet');

  ABBackAndStopDatasetEvent(aDataSet,tempOldBeforeScroll,tempOldAfterScroll);
  try
    if (aLoadAutoSQL) then
    begin
      if (aDataSet.Name<>EmptyStr) and
         (Assigned(aDataSet.Owner)) and
         ((aDataSet.Owner is TABPubForm)) and
         (aDataSet.Owner.Name<>EmptyStr) and
         (not TABPubForm(aDataSet.Owner).IsTemplate) then
      begin
        tempDataset:=ABGetConstSqlPubDataset('ABSys_Org_FuncSQL',[aDataSet.Owner.Name],TABInsideQuery);
        if (Assigned(tempDataset))   then
        begin
          if  (tempDataset.Locate('FS_Name',aDataSet.Name,[])) then
          begin
            //从数据库中取新的SQL更新到数据集SQL中
            if  (tempDataset.FieldByName('FS_Sql').AsString<>EmptyStr) and
                (
                  (TABDictionaryQuery(aDataSet).Sql.Text=EmptyStr) or
                  (TABDictionaryQuery(aDataSet).SqlUpdateDatetime=0) or
                  (ABDateTimeToStr(tempDataset.FieldByName('FS_UpdateDatetime').AsDateTime)>ABDateTimeToStr(TABDictionaryQuery(aDataSet).SqlUpdateDatetime))
                  ) then
            begin
              TABDictionaryQuery(aDataSet).ConnName:=ABGetConnNameByConnGuid(tempDataset.FieldByName('FS_ConnName').AsString);
              TABDictionaryQuery(aDataSet).Sql.Text:=tempDataset.FieldByName('FS_Sql').AsString;
              TABDictionaryQuery(aDataSet).SqlUpdateDatetime:=tempDataset.FieldByName('FS_UpdateDatetime').AsDateTime;
            end
            //将数据集SQL更新到数据库中
            else  if  (TABDictionaryQuery(aDataSet).Sql.Text<>EmptyStr) and
                      (
                        (tempDataset.IsEmpty) or
                        (tempDataset.FieldByName('FS_Sql').IsNull) or
                        (tempDataset.FieldByName('FS_Sql').AsString=EmptyStr) or
                        (ABDateTimeToStr(tempDataset.FieldByName('FS_UpdateDatetime').AsDateTime)<ABDateTimeToStr(TABDictionaryQuery(aDataSet).SqlUpdateDatetime))
                        ) then
            begin
              if TABDictionaryQuery(aDataSet).FuncGuid<>emptystr then
                ABSetFieldValue(tempDataset,
                                ['FS_FU_Guid','FS_FormName','FS_Name'],
                                [TABDictionaryQuery(aDataSet).FuncGuid,aDataSet.Owner.Name,aDataSet.name],
                                ['FS_ConnName','FS_Sql','FS_UpdateDatetime'],
                                [ABGetConnGuidByConnName(TABQuery(aDataSet).ConnName),TABQuery(aDataSet).Sql.Text,TABQuery(aDataSet).SqlUpdateDatetime]
                                  );
            end;
          end
          else
          begin
            //将数据集SQL更新到数据库中
            if TABDictionaryQuery(aDataSet).FuncGuid<>emptystr then
              if TABDictionaryQuery(aDataSet).Sql.Text<>EmptyStr then
                ABSetFieldValue(tempDataset,
                                ['FS_FU_Guid','FS_FormName','FS_Name'],
                                [TABDictionaryQuery(aDataSet).FuncGuid,aDataSet.Owner.Name,aDataSet.name],
                                ['FS_ConnName','FS_Sql','FS_UpdateDatetime'],
                                [ABGetConnGuidByConnName(TABQuery(aDataSet).ConnName),TABQuery(aDataSet).Sql.Text,TABQuery(aDataSet).SqlUpdateDatetime]
                                  );
          end;
        end;
      end;
    end;
    
    if aSetActive then
    begin
      ABCloseDataset(aDataSet);

      if not aGetData then
        TABThirdReadDataQuery(aDataSet).AddWhereofNext:='1=2';

      if (ABLocalParams.Debug) then
      begin
        ABWriteLog('Begin '+aDataSet.Owner.Name+' '+aDataSet.Name+'.ABReFreshQuery');
        ABWriteLog(aDataSet.Owner.Name+' '+aDataSet.Name+'.SQL.Text '+TABQuery(aDataSet).Sql.Text);
      end;

      ABReFreshQuery(aDataSet,[]);

      if not aGetData then
        TABThirdReadDataQuery(aDataSet).AddWhereofNext:=EmptyStr;

      if (ABLocalParams.Debug) then
        ABWriteLog('End   '+aDataSet.Owner.Name+' '+aDataSet.Name+'.ABReFreshQuery');
    end;

    if aDataSet.Active then
    begin
      aDataSet.InitActiveOK:=true;
      for I := Low(aDBPanels) to high(aDBPanels) do
      begin
        if (Assigned(aDBPanels[i])) and
           (Assigned(aDBPanels[i].Parent)) and
           (Assigned(aDBPanels[i].Parent.Parent)) and
           (aDBPanels[i].Parent.Parent is TcxPageControl) and
           (TcxPageControl(aDBPanels[i].Parent.Parent).Height>0) and
           (aDBPanels[i].AutoHeight) then
        begin
          TcxPageControl(aDBPanels[i].Parent.Parent).Height:=0;
        end;
      end;

      for I := Low(aDBPanels) to high(aDBPanels) do
      begin
        if Assigned(aDBPanels[i]) then
        begin
          if (ABLocalParams.Debug) then
            ABWriteLog('Begin '+aDataSet.Owner.Name+' '+aDBPanels[i].Name+'.RefreshControls');

          aDBPanels[i].RefreshControls(1,1,false);

          if (ABLocalParams.Debug) then
            ABWriteLog('End   '+aDataSet.Owner.Name+' '+aDBPanels[i].Name+'.RefreshControls');
        end;
      end;

      for I := Low(aTableViews) to high(aTableViews) do
      begin
        if Assigned(aTableViews[i]) then
        begin
          if (aTableViews[i] is TABcxGridDBBandedTableView) then
          begin
            if (ABLocalParams.Debug) then
              ABWriteLog('Begin '+aDataSet.Owner.Name+' '+aTableViews[i].Name+'.CreateAllColumn');

            TABcxGridDBBandedTableView(aTableViews[i]).CreateAllColumn;

            if (ABLocalParams.Debug) then
              ABWriteLog('End   '+aDataSet.Owner.Name+' '+aTableViews[i].Name+'.CreateAllColumn');
          end;
        end;
      end;

      for I := Low(aQueryPanels) to high(aQueryPanels) do
      begin
        if Assigned(aQueryPanels[i]) then
        begin
          if (aQueryPanels[i] is TABQuerySelectFieldPanel) then
          begin
            if (ABLocalParams.Debug) then
              ABWriteLog('Begin '+aDataSet.Owner.Name+' '+aTableViews[i].Name+'.ReFreshControl');

            TABQuerySelectFieldPanel(aQueryPanels[i]).ReFreshControl;

            if (ABLocalParams.Debug) then
              ABWriteLog('End   '+aDataSet.Owner.Name+' '+aTableViews[i].Name+'.ReFreshControl');
          end;
        end;
      end;

      aDataSet.CreateLinkOk:=true;
    end;
  finally
    ABUnBackDatasetEvent(aDataSet,tempOldBeforeScroll,tempOldAfterScroll);
    if aDataSet.Active then
    begin
      if Assigned(aDataSet.AfterScroll) then aDataSet.AfterScroll(aDataSet);
    end;
  end;
  if (ABLocalParams.Debug) then
    ABWriteLog('End   procedure '+aDataSet.Owner.Name+' '+aDataSet.Name+'.ABInitFormDataSet');
end;

{ TABDatasource }

constructor TABDatasource.Create(AOwner: TComponent);
begin
  inherited;
end;

destructor TABDatasource.Destroy;
begin

  inherited;
end;

{ TABQuery }

procedure TABQuery.ShowDBPanel(aCanEdit: boolean;
  aActivePageCaption,
  aEnabledFieldNames,
  aVisibleFieldNames:string);
var
  tempResult:LongInt;
begin
  if (active)   then
  begin
    if Assigned(OnBeforeShowDBPanel) then
      OnBeforeShowDBPanel(self);
    try
      if not Assigned(FDBPanelForm) then
      begin
        FDBPanelForm:= TABDBPanelForm.Create(self);
      end;

      TABDBPanelForm(FDBPanelForm).SetActivePageView(aActivePageCaption);
      TABDBPanelForm(FDBPanelForm).EnabledFieldNames:=aEnabledFieldNames;
      TABDBPanelForm(FDBPanelForm).VisibleFieldNames:=aVisibleFieldNames;

      if (aCanEdit) and
         (ABCheckDataSetCanEdit(self)) and
         (not (State in [dsEdit,dsInsert])) then
      begin
        if (not IsEmpty) or
           (CanInsert) then
          Edit;
      end;

      tempResult:=FDBPanelForm.ShowModal;
      case tempResult of
        mrNo,mrCancel:
        begin
          if (State in [dsEdit,dsInsert]) then
            Cancel;
        end;
      end;
    finally
      if Assigned(OnAfterShowDBPanel) then
       OnAfterShowDBPanel(self);
    end;
  end;
end;

constructor TABQuery.Create(AOwner: TComponent);
begin
  inherited;
  FQueryForm:=nil;
end;

procedure TABQuery.DoBeforeCommitTrans;
begin
  inherited;
end;

procedure TABQuery.DoBeforeInsert;
var
  tempDataSet: TDataset;
begin
  inherited;

  if (Assigned(DataSource)) and
     (Assigned(DataSource.DataSet)) then
  begin
    tempDataSet := TDataset(DataSource.DataSet);

    if (not ParentIsMainControl)  then
    begin
      if (tempDataSet.State in [dsEdit, dsInsert]) then
      begin
        try
          tempDataSet.Post;
        except
          abshow('请先保存主表数据,再增加从表数据.');
          Abort;
        end;
      end;
    end;

    if ABDatasetIsEmpty(tempDataSet) then
    begin
      abshow('请先增加主表数据,再增加从表数据.');
      Abort;
    end;
  end;
end;

destructor TABQuery.Destroy;
begin
  if Assigned(FDBPanelForm) then
    FreeAndNil(FDBPanelForm);
  if Assigned(FQueryForm) then
    FreeAndNil(FQueryForm);

  inherited;
end;

procedure TABQuery.Loaded;
var
  tempSQL:string;
  tempDataset:TDataSet;
begin
  inherited;

  //使得在设计界面打开时总是关闭数据集
  if Active then
    close;

  if (csDesigning in ComponentState)  then
  begin
    if (Name<>EmptyStr) and
       (Assigned(Owner)) and
       ((Owner is TABPubForm)) and
       (Owner.Name<>EmptyStr) and
       (not TABPubForm(Owner).IsTemplate) then
    begin
      //属性加载完成后取最数据库中可能新的SQL
      //数据集显示时显示最新的SQL
      //当重编译框架包时再显示模块中的数据集则会在此过程中报错，先注释一下
      if ABIsConn('Main') then
      begin
        tempDataset:=ABGetConstSqlPubDataset('ABSys_Org_FuncSQL',[Owner.Name],TABInsideQuery);
        if (Assigned(tempDataset)) and
           (tempDataset.Locate('FS_Name',Name,[])) then
        begin
          if  (Sql.Text=EmptyStr) or
              (SqlUpdateDatetime=0) or
              (ABDateTimeToStr(tempDataset.FieldByName('FS_UpdateDatetime').AsDateTime)>ABDateTimeToStr(SqlUpdateDatetime)) then
          begin
            tempSql:= VarToStrDef(tempDataset.FieldByName('FS_Sql').AsString,'');

            if tempSQL<>EmptyStr then
            begin
              ConnName:=ABGetConnNameByConnGuid(tempDataset.FieldByName('FS_ConnName').AsString);
              Sql.Text:=tempSQL;
              SqlUpdateDatetime:=tempDataset.FieldByName('FS_UpdateDatetime').AsDateTime;
            end;
          end;
        end;
      end;
    end;
  end;
end;

procedure TABQuery.OpenQuery;
var
  tempWhere:string;
begin
  if ABQueryFormQuery(tempWhere,self,FQueryForm) then
  begin
    RefreshQuery(tempWhere,[]);
  end;
end;

procedure TABQuery.DoAfterCancel;
begin
  inherited;
  ABSetDataSetControlOnSateChange(self,True,True,true);
  ABSetDetailDatasetEditState(self,False);
end;

procedure TABQuery.DoAfterEdit;
begin
  inherited;
  ABSetDataSetControlOnSateChange(self,True,True);

  ABSetDataSetCiteControl(self,nil,true,true);

  ABSetDetailDatasetEditState(self,True);
end;

procedure TABQuery.DoAfterInsert;
begin
  inherited;

  ABSetDataSetControlOnSateChange(self,True,True);

  ABSetDetailDatasetEditState(self,True);
end;

procedure TABQuery.DoAfterOpen;
begin
  inherited;

  ABSetDetailDatasetEditState(self,False);
end;

procedure TABQuery.DoAfterPost;
  procedure UpdateMultiSelect;
  begin
    if FStopMultiSelectUpdate then
      exit;

    FStopMultiSelectUpdate := true;
    try
      if (Assigned(FMultiSelectTableView)) and
         (FMultiSelectTableView.DataController.GetSelectedCount>1) and
         (High(FMultiSelectFieldNames)>=0) then
      begin
        ABSetFieldValue_MultiSelect(FMultiSelectTableView.DataController,
                                    mtSelect,
                                    FMultiSelectFieldNames,
                                    FMultiSelectFieldValues,
                                    FMultiSelectValuesIsCale
                                    );

      end;
    finally
      FStopMultiSelectUpdate := false;
    end;
  end;
var
  tempLastOperatorType: TABDatasetOperateType;
begin
  tempLastOperatorType:=LastOperatorType;
  inherited;
  ABSetDataSetControlOnSateChange(self,True,True,True);

  if (tempLastOperatorType=dtedit) then
  begin
    UpdateMultiSelect;
  end;

  ABSetDetailDatasetEditState(self,False);
end;

procedure TABQuery.DoBeforePost;
  procedure InitMultiSelect;
  var
    ii,i, j: LongInt;
    tempFieldDef:PABFieldDef;
  begin
    if FStopMultiSelectUpdate then
      exit;

    j:=0;
    FMultiSelectTableView:=nil;
    setlength(FMultiSelectFieldNames, j);
    setlength(FMultiSelectFieldValues, j);
    setlength(FMultiSelectValuesIsCale, j);
    try
      for ii := 0 to LinkDBTableViewList.Count-1 do
      begin
        if (Assigned(LinkDBTableViewList[ii])) and
           (TObject(LinkDBTableViewList[ii]) is TABcxGridDBBandedTableView)  then
        begin
          FMultiSelectTableView:=TABcxGridDBBandedTableView(LinkDBTableViewList[ii]);
          //如果关联的是从TableView则取主TableView的活动TableView
          if FMultiSelectTableView.DataController.MasterKeyFieldNames<>EmptyStr then
            FMultiSelectTableView:=
              TABcxGridDBBandedTableView(TABcxGridDBBandedTableView(FMultiSelectTableView.MasterGridView).
                ViewData.Rows[TABcxGridDBBandedTableView(FMultiSelectTableView.MasterGridView).
                                DataController.FocusedRowIndex].AsMasterDataRow.ActiveDetailGridView);

          if FMultiSelectTableView.DataController.GetSelectedCount>1 then
          begin
            for I := 0 to FieldCount-1 do
            begin
              tempFieldDef:=FieldDefByFieldName(Fields[i].FieldName);
              if (Assigned(tempFieldDef)) and
                 (tempFieldDef.Fi_Edited) and
                 (tempFieldDef.Fi_BatchEdit)  then
              begin
                j:=j+1;
                setlength(FMultiSelectFieldNames, j);
                setlength(FMultiSelectFieldValues, j);
                setlength(FMultiSelectValuesIsCale, j);
                FMultiSelectFieldNames[j-1]:=Fields[i].FieldName;
                FMultiSelectFieldValues[j-1]:=tempFieldDef.Field.Value;
                FMultiSelectValuesIsCale[j-1]:=false;
              end;
            end;
          end;
        end;
        //暂时只处理一个关联
        Break;
      end;
    finally
      FStopMultiSelectUpdate := false;
    end;
  end;
begin
  if (State =dsedit) then
  begin
    InitMultiSelect;
  end;
  inherited;
end;

procedure TABQuery.AfterFieldChange(aField: TField;var aDo:Boolean);
var
  tempFieldDef:PABFieldDef;
begin
  inherited;
  tempFieldDef:=FieldDefByFieldName(aField.FieldName);
  if Assigned(tempFieldDef) then
  begin
    if aDo then
    begin
      if (tempFieldDef.IsDown) and
         (tempFieldDef.Fi_RelatingFromFields<>EmptyStr) and
         (tempFieldDef.Fi_RelatingToFields<>EmptyStr)  and
         (tempFieldDef.PDownDef.Fi_DataSet.Locate(tempFieldDef.PDownDef.Fi_SaveField,tempFieldDef.Field.AsString,[])) then
      begin
        ABFieldChangeForRelatingField(tempFieldDef,tempFieldDef.PDownDef.Fi_DataSet);
      end;
      ABFieldChangeForCiteField(tempFieldDef);

      ABSetDataSetCiteControl(self,tempFieldDef,true,true);
    end;
  end;
end;

procedure TABQuery.CreateFieldExtInfos;
var
  i:LongInt;
  tempFieldDef:PABFieldDef;
begin
  inherited;

  for I := 0 to DefList.Count - 1 do
  begin
    tempFieldDef:=PABFieldDef(DefList.Objects[i]);
    if (Assigned(tempFieldDef)) AND
       (tempFieldDef.IsDown) then
    begin
      ABAppendDownListAddItem(tempFieldDef);
    end;
  end;
end;

procedure TABQuery.FreeFieldExtInfos;
var
  i:LongInt;
  tempFieldDef:PABFieldDef;
begin
  for I := 0 to DefList.Count - 1 do
  begin
    tempFieldDef:=PABFieldDef(DefList.Objects[i]);
    if Assigned(tempFieldDef) then
    begin
      if tempFieldDef.IsDown then
      begin
        ABDelDownListAddItem(tempFieldDef);

        ABFreePubDownEdit(tempFieldDef.Flag);
      end;
    end;
  end;

  inherited;
end;



end.



