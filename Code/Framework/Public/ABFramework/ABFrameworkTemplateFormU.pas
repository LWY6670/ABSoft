unit ABFrameworkTemplateFormU;

interface

uses
  ABPubFuncU,
  ABPubPanelU,
  ABPubDBU,

  ABThirdQueryU,
  ABThirdConnU,
  ABThirdDBU,
  ABThirdCustomQueryU,

  ABFrameworkDBPanelU,
  ABFrameworkQuerySelectFieldPanelU,
  ABFrameworkcxGridU,
  ABFrameworkDictionaryQueryU,
  ABFrameworkDBNavigatorU,
  ABFrameworkFuncFormU,
  ABFrameworkControlU,
  ABFrameworkQueryU,
  ABFrameworkFuncU,

  cxDateUtils,
  cxLookAndFeels,
  cxTextEdit,
  cxContainer,
  cxLabel,
  cxCalendar,
  cxDropDownEdit,
  cxMaskEdit,
  cxButtons,
  cxLookAndFeelPainters,
  cxGridDBBandedTableView,
  cxGridBandedTableView,
  cxGrid,
  cxGridDBTableView,
  cxGridTableView,
  cxGridCustomTableView,
  cxGridCustomView,
  cxClasses,
  cxGridLevel,
  cxDBData,
  cxEdit,
  cxDataStorage,
  cxData,
  cxFilter,
  cxGraphics,
  cxCustomData,
  cxStyles,
  cxDBNavigator,
  cxNavigator,
  cxControls,
  cxPC,
  dxCore,
  dxStatusBar,

  Windows,Messages,SysUtils,Variants,Classes,Graphics,Controls,Forms,Dialogs,DB,
  DBClient,ExtCtrls,ComCtrls,StdCtrls,DBCtrls,Menus,
  ABFrameworkQueryAllFieldComboxU, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, cxSplitter, dxBarBuiltInMenu ;

type
  TABTemplateForm = class(TABFuncForm)
    ABcxPageControl1: TABcxPageControl;
    ABDBNavigatorTop: TABDBNavigator;
    ABDBStatusBar1: TABdxDBStatusBar;
    procedure FormCreate(Sender: TObject);
    procedure ABcxPageControl1Change(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ABTemplateForm: TABTemplateForm;

implementation

{$R *.dfm}


procedure TABTemplateForm.ABcxPageControl1Change(Sender: TObject);
var
  tempMainDatasources:array of TDatasource;
  tempMainDBPanels: TTWODBPanelArray;
  tempMainTableViews:TTWOTableViewArray;

  tempMainGetData:array of Boolean;
  tempMainLoadAutoSQL:array of boolean;
  tempMainSetActive:array of boolean;

  tempDetailDatasources:TTWODatasourceArray;
  tempDetailDBPanels:TThreeDBPanelArray;
  tempDetailTableViews:TThreeTableViewArray;

  tempDetailGetData:TTWOboolean;
  tempDetailLoadAutoSQL:TTWOboolean;
  tempDetailSetActive:TTWOboolean;

  tempChangePageControlRefreshMainData:array of  boolean;

  i,j:LongInt;
  tempDataSource:TDataSource;
  tempPageControl: TABcxPageControl;
  tempABDBPanel:TABDBPanel;
  tempTableView:TABcxGridDBBandedTableView;
begin
  SetLength(tempMainDatasources,ABcxPageControl1.PageCount);
  SetLength(tempMainDBPanels   ,ABcxPageControl1.PageCount);
  SetLength(tempMainTableViews ,ABcxPageControl1.PageCount);
  SetLength(tempMainGetData    ,ABcxPageControl1.PageCount);
  SetLength(tempMainLoadAutoSQL,ABcxPageControl1.PageCount);
  SetLength(tempMainSetActive  ,ABcxPageControl1.PageCount);

  SetLength(tempDetailDatasources,ABcxPageControl1.PageCount);
  SetLength(tempDetailDBPanels   ,ABcxPageControl1.PageCount);
  SetLength(tempDetailTableViews ,ABcxPageControl1.PageCount);
  SetLength(tempDetailGetData    ,ABcxPageControl1.PageCount);
  SetLength(tempDetailLoadAutoSQL,ABcxPageControl1.PageCount);
  SetLength(tempDetailSetActive  ,ABcxPageControl1.PageCount);

  for I := 0 to ABcxPageControl1.PageCount-1 do
  begin
    tempMainGetData[i]:=true;
    tempMainLoadAutoSQL[i]:=False;
    tempMainSetActive[i]:=true;

    tempDataSource:=TDataSource(FindComponent('ABDataSource'+inttostr(i+1)));
    if Assigned(tempDataSource) then
    begin
      tempMainDatasources[i]:=tempDataSource;
    end;

    tempPageControl:=TABcxPageControl(FindComponent('ABDBPanelPageControl'+inttostr(i+1)));
    if Assigned(tempPageControl) then
    begin
      SetLength(tempMainDBPanels[i],tempPageControl.PageCount);
      for j := 0 to tempPageControl.PageCount-1 do
      begin
        tempABDBPanel:=TABDBPanel(FindComponent('ABMainDBPanel'+inttostr(i+1)+'_'+inttostr(j+1)));
        if Assigned(tempABDBPanel) then
        begin
          tempMainDBPanels[i][j]:=tempABDBPanel;
        end;
      end;
    end;
    tempTableView:=TABcxGridDBBandedTableView(FindComponent('ABTableView'+inttostr(i+1)));
    if Assigned(tempTableView) then
    begin
      SetLength(tempMainTableViews[i],1);
      tempMainTableViews[i][0]:=tempTableView;
    end;
    
    tempPageControl:=TABcxPageControl(FindComponent('ABDetailPageControl'+inttostr(i+1)));
    if Assigned(tempPageControl) then
    begin
      SetLength(tempDetailDatasources[i],tempPageControl.PageCount);
      SetLength(tempDetailDBPanels   [i],tempPageControl.PageCount);
      SetLength(tempDetailTableViews [i],tempPageControl.PageCount);
      SetLength(tempDetailGetData    [i],tempPageControl.PageCount);
      SetLength(tempDetailLoadAutoSQL[i],tempPageControl.PageCount);
      SetLength(tempDetailSetActive  [i],tempPageControl.PageCount);
      for j := 0 to tempPageControl.PageCount-1 do
      begin
        SetLength(tempDetailDBPanels   [i][j],0);
        tempDetailGetData[i][j]:=true;
        tempDetailLoadAutoSQL[i][j]:=False;
        tempDetailSetActive[i][j]:=true;

        tempDataSource:=TDataSource(FindComponent('ABDataSource'+inttostr(i+1)+'_'+inttostr(j+1)));
        if Assigned(tempDataSource) then
        begin                    
          tempDetailDatasources[i][j]:=tempDataSource;
        end;

        tempTableView:=TABcxGridDBBandedTableView(FindComponent('ABTableView'+inttostr(i+1)+'_'+inttostr(j+1)));
        if Assigned(tempTableView) then
        begin
          SetLength(tempDetailTableViews[i][j],1);
          tempDetailTableViews[i][j][0]:=tempTableView;
        end;
      end;
    end;
  end;

  ABInitFormPageControl(
                        ABcxPageControl1,

                        ABDBNavigatorTop,
                        ABDBStatusBar1,

                        tempMainDatasources,
                        tempMainDBPanels,
                        tempMainTableViews,
                        tempMainGetData,
                        tempMainLoadAutoSQL,
                        tempMainSetActive,

                        tempDetailDatasources,
                        tempDetailDBPanels,
                        tempDetailTableViews,
                        tempDetailGetData,
                        tempDetailLoadAutoSQL,
                        tempDetailSetActive,

                        tempChangePageControlRefreshMainData
                        );          
end;
                    
procedure TABTemplateForm.FormCreate(Sender: TObject);
begin
  ABInitTemplateForm(ABcxPageControl1);
  ABDBNavigatorTop.Visible:=ABcxPageControl1.PageCount>1;
  ABcxPageControl1Change(ABcxPageControl1);
end;

Initialization
  RegisterClass(TABTemplateForm);

Finalization
  UnRegisterClass(TABTemplateForm);

end.
