{
框架下拉项单元
}
unit ABFrameworkTreeItemEditU;

interface
{$I ..\ABInclude.ini}

uses
  ABPubMultilingualDBNavigatorU,
  ABPubFuncU,
  ABPubUserU,
  ABPubDBU,
  ABPubDBMainDetailPopupMenuU,
  ABPubFormU,

  ABThird_DevExtPopupMenuU,
  ABThirdFuncU,
  ABThirdDBU,

  ABFrameworkConstU,
  ABFrameworkFuncU,
  ABFrameworkDictionaryQueryU,

  cxButtons,
  cxLabel,
  cxPC,
  cxGrid,
  cxGridDBBandedTableView,
  cxGridCustomTableView,
  cxGridLevel,
  cxEditRepositoryItems,
  cxEdit,

  forms,DB,Classes,Controls,SysUtils,Variants,ExtCtrls,DBCtrls,
  Menus,

  StdCtrls, ComCtrls, dxdbtree, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, cxControls, cxContainer, dxBarBuiltInMenu, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator, cxDBData, dxtree,
  cxClasses, cxGridCustomView, cxGridTableView, cxGridBandedTableView;


type
  TABDownItemForm = class(TABPubForm)
    DataSource1: TDataSource;
    pnl1: TPanel;
    btn2: TcxButton;
    Sheet1_PageControl2: TcxPageControl;
    Sheet1_Detail_Sheet1: TcxTabSheet;
    cxTabSheet1: TcxTabSheet;
    Sheet1_Grid1: TcxGrid;
    Sheet1_TableView1: TcxGridDBBandedTableView;
    Sheet1_Level1: TcxGridLevel;
    EditRepository: TcxEditRepository;
    ABcxLabel1: TcxLabel;
    ABMultilingualDBNavigator1: TABMultilingualDBNavigator;
    dxDBTreeView1: TdxDBTreeView;
    ABDBMainDetailPopupMenu1: TABDBMainDetailPopupMenu;
    ABcxGridPopupMenu1: TABcxGridPopupMenu;
    procedure btn2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ABDBNavigator1AfterBtnEditClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FQuery1BeforePost(DataSet: TDataSet);
    procedure FQuery1AfterInsert(DataSet: TDataSet);
  private
    FQuery1: TABDictionaryQuery;
    FEdit_Ti_Guid:string;
    { Private declarations }
  public
    Dl_Code:string;
    { Public declarations }
  end;

//显示下拉列项
//aValue=选择的返回字段值
//aDl_Code=下拉项编号
//aRetField=返回字段
function  ABShowDownListItem(aDl_Code: string;aRetField:string;var aValue:string): Boolean ;

//设置不同下拉列项时显示不同字段列的控制
procedure ABViewDownItemByMain(aDl_Code:string;
                               aDetailDataSet:TDataSet;
                               aDefaultTi_Varchar1Visibled,
                               aDefaultTi_Varchar2Visibled,
                               aDefaultTi_bit1Visibled,
                               aDefaultTi_bit2Visibled:boolean;

                               aEditRepository: TcxEditRepository;
                               aTableView:TcxGridDBBandedTableView);


implementation

{$R *.dfm}

procedure ABViewDownItemByMain(aDl_Code:string;aDetailDataSet:TDataSet;
                                 aDefaultTi_Varchar1Visibled,aDefaultTi_Varchar2Visibled,
                                 aDefaultTi_bit1Visibled,aDefaultTi_bit2Visibled:boolean;
                                 aEditRepository: TcxEditRepository;
                                 aTableView:TcxGridDBBandedTableView);
var
  tempTi_Varchar1_EditRepository,tempTi_Varchar2_EditRepository:TcxEditRepositoryItem;
  tempTi_bit1,tempTi_bit2,tempTi_Varchar1,tempTi_Varchar2:string;
  tempTi_bit1Visibled,tempTi_bit2Visibled,tempTi_Varchar1Visibled,tempTi_Varchar2Visibled:boolean;
begin
  if not aDetailDataSet.Active then
    exit;

  tempTi_Varchar1_EditRepository:=nil;
  tempTi_Varchar2_EditRepository:=nil;
  tempTi_Varchar1:=TABDictionaryQuery(aDetailDataSet).FieldDefByFieldName('Ti_Varchar1').Fi_Caption;
  tempTi_Varchar2:=TABDictionaryQuery(aDetailDataSet).FieldDefByFieldName('Ti_Varchar2').Fi_Caption;
  tempTi_bit1    :=TABDictionaryQuery(aDetailDataSet).FieldDefByFieldName('Ti_bit1').Fi_Caption;
  tempTi_bit2    :=TABDictionaryQuery(aDetailDataSet).FieldDefByFieldName('Ti_bit1').Fi_Caption;

  tempTi_Varchar1Visibled:=aDefaultTi_Varchar1Visibled;
  tempTi_Varchar2Visibled:=aDefaultTi_Varchar2Visibled;
  tempTi_bit1Visibled:=aDefaultTi_bit1Visibled;
  tempTi_bit2Visibled:=aDefaultTi_bit2Visibled;

  if (AnsiCompareText(aDl_Code,'Function Dir')=0)  then
  begin
    tempTi_bit1:='是否显示';
    tempTi_bit1Visibled:=true;
    tempTi_bit2:='前面加分组条';
    tempTi_bit2Visibled:=true;
  end
  else if AnsiCompareText(aDl_Code,'CustomObject')=0 then
  begin
    tempTi_bit1:='是否使用';
    tempTi_bit1Visibled:=true;
    tempTi_Varchar1:='类型';
    tempTi_Varchar1Visibled:=true;
    tempTi_Varchar1_EditRepository:=aEditRepository.ItemByName('EditRepositoryItem_Ti_Varchar1_'+aDl_Code);
    if not Assigned(tempTi_Varchar1_EditRepository) then
    begin
      tempTi_Varchar1_EditRepository:=aEditRepository.CreateItem(TcxEditRepositoryComboBoxItem);
      ABFillTreeItemByTr_Code('CustomObjectType',['Ti_Code'],TcxEditRepositoryComboBoxItem(tempTi_Varchar1_EditRepository).Properties.Items);
    end;
  end
  else if AnsiCompareText(aDl_Code,'Enterprise Dir')=0 then
  begin
    tempTi_Varchar1:='类型';
    tempTi_Varchar1Visibled:=true;
  end;

  aTableView.GetColumnByFieldName('Ti_Varchar1').PropertiesClass:=nil;
  aTableView.GetColumnByFieldName('Ti_Varchar2').PropertiesClass:=nil;
  aTableView.GetColumnByFieldName('Ti_Varchar1').RepositoryItem:=tempTi_Varchar1_EditRepository;
  aTableView.GetColumnByFieldName('Ti_Varchar2').RepositoryItem:=tempTi_Varchar2_EditRepository;

  if AnsiCompareText(tempTi_Varchar1,aDetailDataSet.FieldByName('Ti_Varchar1').DisplayLabel)<>0 then
  begin
    aDetailDataSet.FieldByName('Ti_Varchar1').DisplayLabel:=tempTi_Varchar1;
  end;
  if AnsiCompareText(tempTi_Varchar2,aDetailDataSet.FieldByName('Ti_Varchar2').DisplayLabel)<>0 then
  begin
    aDetailDataSet.FieldByName('Ti_Varchar2').DisplayLabel:=tempTi_Varchar2;
  end;
  if AnsiCompareText(tempTi_bit1,aDetailDataSet.FieldByName('Ti_bit1').DisplayLabel)<>0 then
  begin
    aDetailDataSet.FieldByName('Ti_bit1').DisplayLabel:=tempTi_bit1;
  end;
  if AnsiCompareText(tempTi_bit2,aDetailDataSet.FieldByName('Ti_bit2').DisplayLabel)<>0 then
  begin
    aDetailDataSet.FieldByName('Ti_bit2').DisplayLabel:=tempTi_bit2;
  end;

  if tempTi_bit1Visibled<>ABGetColumnProperty(aTableView,'Ti_bit1','Visible') then
  begin
    ABSetColumnProperty(aTableView,'Ti_bit1','Visible',tempTi_bit1Visibled);
  end;
  if tempTi_bit2Visibled<>ABGetColumnProperty(aTableView,'Ti_bit2','Visible') then
  begin
    ABSetColumnProperty(aTableView,'Ti_bit2','Visible',tempTi_bit2Visibled);
  end;
  if tempTi_Varchar1Visibled<>ABGetColumnProperty(aTableView,'Ti_Varchar1','Visible') then
  begin
    ABSetColumnProperty(aTableView,'Ti_Varchar1','Visible',tempTi_Varchar1Visibled);
  end;
  if tempTi_Varchar2Visibled<>ABGetColumnProperty(aTableView,'Ti_Varchar2','Visible')then
  begin
    ABSetColumnProperty(aTableView,'Ti_Varchar2','Visible',tempTi_Varchar2Visibled);
  end;
end;

function ABShowDownListItem(aDl_Code: string;aRetField:string;var aValue:string): Boolean ;
var
  tempForm:TABDownItemForm ;
  i:LongInt;
  tempTableItem:TcxGridDBBandedColumn;
begin
  Result:=false;
  tempForm := TABDownItemForm.Create(nil);
  try
    if (ABPubUser.IsAdminOrSysuser)  then
    begin
      tempForm.ABMultilingualDBNavigator1.VisibleButtons:=
        tempForm.ABMultilingualDBNavigator1.VisibleButtons+
         [nbInsert,nbDelete,nbEdit,nbPost,nbCancel];
    end
    else
    begin
      tempForm.ABMultilingualDBNavigator1.Visible:=false;
    end;

    tempForm.ABcxLabel1.Caption:=ABGetConstSqlPubDataset('ABSys_Org_Tree',[aDl_Code]).fieldbyname('Tr_Remark').asstring;

    tempForm.Dl_Code:= aDl_Code;
    tempForm.DataSource1.DataSet.Close;
    ABSetParamValue(tempForm.DataSource1.DataSet,'Ti_Tr_Guid',
                    ABGetConstSqlPubDataset('ABSys_Org_Tree',[aDl_Code]).fieldbyname('Tr_Guid').asstring);
    tempForm.DataSource1.DataSet.Open;

    ABSetDatasetFilter(tempForm.DataSource1.DataSet,aRetField+'<>'+QuotedStr(ABcxLookupComboBoxAddNewItemStr));
    tempForm.ABcxGridPopupMenu1.SetupFileNameSuffix:=aDl_Code;
    tempForm.ABcxGridPopupMenu1.Load;
    for I := 0 to TABDictionaryQuery(tempForm.DataSource1.DataSet).FieldDefs.Count-1 do
    begin
      if not TABDictionaryQuery(tempForm.DataSource1.DataSet).FieldDefItems[i].Fi_IsGridView then
      begin
        tempTableItem:=TcxGridDBBandedColumn(ABGetColumnByFieldName(tempForm.Sheet1_TableView1,TABDictionaryQuery(tempForm.DataSource1.DataSet).FieldDefItems[i].Fi_Name));
        if (Assigned(tempTableItem)) then
        begin
          tempTableItem.VisibleForCustomization:=false;
          tempTableItem.Visible:=false;
        end;
      end;
    end;

    if tempForm.ShowModal=mrOk then
    begin
      aValue:= tempForm.DataSource1.DataSet.FindField(aRetField).AsString;
      Result:=True;
    end;
  finally
    tempForm.Free;
  end;
end;

procedure TABDownItemForm.ABDBNavigator1AfterBtnEditClick(
  Sender: TObject);
begin
  FEdit_Ti_Guid:=DataSource1.DataSet.FindField('Ti_Guid').AsString;
end;

procedure TABDownItemForm.FQuery1AfterInsert(DataSet: TDataSet);
begin
  DataSource1.DataSet.FindField('Ti_Tr_Guid').AsString:=ABGetConstSqlPubDataset('ABSys_Org_Tree',[Dl_Code]).fieldbyname('Tr_Guid').asstring;
  DataSource1.DataSet.FindField('Ti_Guid').AsString:=ABGetGuid;
end;

procedure TABDownItemForm.btn2Click(Sender: TObject);
begin
  ABPostDataset(DataSource1.DataSet);
  ModalResult:=mrOk;
end;

procedure TABDownItemForm.FormCreate(Sender: TObject);
begin
  FQuery1:=TABDictionaryQuery.Create(self);
  FQuery1.BeforePost:= FQuery1BeforePost;
  FQuery1.AfterInsert:= FQuery1AfterInsert;
  FQuery1.Sql.Text:='select * from ABSys_Org_TreeItem where  Ti_Tr_Guid=:Ti_Tr_Guid';
  DataSource1.DataSet:=FQuery1;
  ABDBMainDetailPopupMenu1.DataSource:=DataSource1;
end;

procedure TABDownItemForm.FormDestroy(Sender: TObject);
begin
  FQuery1.free;
end;

procedure TABDownItemForm.FormShow(Sender: TObject);
begin
  Sheet1_PageControl2.ActivePageIndex:=0;
  ABViewDownItemByMain(Dl_Code,DataSource1.DataSet,
                         ABGetColumnProperty(Sheet1_TableView1,'Ti_Varchar1','Visible'),
                         ABGetColumnProperty(Sheet1_TableView1,'Ti_Varchar2','Visible'),
                         ABGetColumnProperty(Sheet1_TableView1,'Ti_bit1'    ,'Visible'),
                         ABGetColumnProperty(Sheet1_TableView1,'Ti_bit2'    ,'Visible'),
                         EditRepository,
                         Sheet1_TableView1);

end;

procedure TABDownItemForm.FQuery1BeforePost(DataSet: TDataSet);
  //下拉列项管理的附加更新,如发卡器只能有一种卡在使用状态
  procedure ABDownItemOtherControl(aDl_Code:string;aEdit_Ti_Guid:string;
                                aDataSet:TDataSet);
  begin
    if (DataSet.State in [dsedit,dsinsert]) then
    begin            
      if (
          (AnsiCompareText(aDl_Code,'Card Type')=0) or
          (AnsiCompareText(aDl_Code,'Wiegand')=0)
          )and
         (AnsiCompareText(aEdit_Ti_Guid,aDataSet.FindField('Ti_Guid').AsString)=0) and
         (ABFieldDefByFieldName(aDataSet,'Ti_Bit1').Fi_Edited) and
         (aDataSet.FieldByName('Ti_Bit1').AsBoolean) then
      begin
        aDataSet.First;
        while not aDataSet.Eof do
        begin
          if (AnsiCompareText(aEdit_Ti_Guid,aDataSet.FindField('Ti_Guid').AsString)<>0) and
             (aDataSet.FieldByName('Ti_Bit1').AsBoolean) then
          begin
            aDataSet.Edit;
            aDataSet.FieldByName('Ti_Bit1').AsBoolean:=false;
            aDataSet.Post;
          end;
          aDataSet.Next;
        end;
      end;
    end;
  end;
begin
  ABDownItemOtherControl(Dl_Code,FEdit_Ti_Guid,DataSet);
end;

end.



