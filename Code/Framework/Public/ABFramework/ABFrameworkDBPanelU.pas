{
框架字典数据集输入Panel单元，同一个字段只可存在于一个输入面板上，不支持不同面板上显示与编辑同一个字段控件
}
unit ABFrameworkDBPanelU;

interface
{$I ..\ABInclude.ini}

uses
  ABPubPanelU,
  ABPubManualProgressBar_ThreadU,
  ABPubMessageU,
  ABPubSelectStrU,
  ABPubOrderStrU,
  ABPubDefaultValueU,
  abPubCOnstU,
  abpubFuncU,
  abpubUserU,
  abpubDBU,
  ABPubShowEditDatasetU,

  ABThirdDBU,
  ABThirdFuncU,
  ABThirdConnU,
  ABThirdCustomQueryU,

  ABFrameworkControlU,
  ABFrameworkDictionaryQueryU,
  ABFrameworkFuncU,
  ABFrameworkConstU,
  ABFrameworkVarU,
  ABFrameworkcxGridU,

  cxLabel,
  cxContainer,
  cxRadioGroup,
  cxCheckBox,
  cxEdit,

  SysUtils,Variants,Classes,Graphics,Forms,Menus,Controls,DB,
  ExtCtrls,math;

type
  TABDBPanel=class;
  TABDBPanelDataLink=class(TDataLink)
  private
    FlinkPanel:TABDBPanel;
    { Private declarations }
  protected
    procedure ActiveChanged; override;
    { Protected declarations }
  public
    property linkPanel : TABDBPanel read FlinkPanel write FlinkPanel ;
    { Public declarations }
  published
    { Published declarations }
  end;

  TABDBPanel = class(TABCustomPanel)
  private
    FFuncControlPropertyDataset:Tdataset;
  private
    FAutoCreate: Boolean;
    FDataLink : TABDBPanelDataLink;

    FAutoHeight: Boolean;
    FNoHaveField: string;
    FHaveField: string;
    FReadOnlyField: string;
    FPublicSizePosition: Boolean;
    FEditField: string;
    FReadOnly: Boolean;

    FAutoWidth: Boolean;
    FOnAfterFreeControls: TNotifyEvent;
    FOnAfterSaveControls: TNotifyEvent;
    FOnAfterCreateControls: TNotifyEvent;
    FGetExpProperty: Boolean;
    FAddAnchors_akBottom: Boolean;
    FAddAnchors_akRight: Boolean;
    FAutoCaleHeight: longint;
    FAutoCaleWidth: longint;
    FExceptionControlNames: Tstrings;

    function GetDataSource : TDataSource;
    procedure SetDataSource(Value : TDataSource);
    function SetFi_SetOrder: Boolean;

    function GetDBPanelPropertyValue(aPropertyName: string): string;
    procedure SetDBPanelPropertyValue(aPropertyName, aValue: string);
    procedure ReFresh_Fi_SetOrder;

    procedure SetPublicSizePosition(const Value: Boolean);

    procedure GetExpProperty;
    function GetFuncControlPropertyDataset: Tdataset;
    function GetExceptionControlNames: TStrings;
    procedure SetExceptionControlNames(const Value: TStrings);
  protected
    procedure Notification(AComponent: TComponent;Operation: TOperation); override;
  public
    //右键菜单的事件
    procedure PopupMenu_Plan(Sender: TObject);

    //刷新控件
    procedure RefreshControls(aPositionType:LongInt;        //位置类型,1:字段在字典位置,2:字段在数据集位置,3:手工设置字段位置
                              aSizeType:LongInt;            //尺寸类型,1:字段在字典尺寸,2:字段默认尺寸
                              aAutoSuit:Boolean;            //创建后是否自适应边界（包含最左边控件宽度和最下边高度适应）
                              aIsCreate:boolean=true;        //是否重新创建控件
                              aMinWidth:longint=20;          //面板的最小宽度
                              aMinHeight:longint=20);        //面板的最小高度

    //设置控件的位置和大小
    procedure SetControlsPosition(aPositionType:LongInt;
                                  aSizeType:LongInt;
                                  aAutoSuit:Boolean;
                                  aMinWidth,
                                  aMinHeight:longint);
    //释放控件
    procedure FreeControls;
    //创建控件
    procedure CreateControls;

    //连接控件和字段的控件对象
    procedure LinkFieldDefLabelControls(aOnlyBeforeCreate:boolean=true);

    //保存所有控件位置和大小
    procedure SaveControls;overload;
    //保存当前焦点控件位置和大小
    procedure SaveControls(aControl: TControl;aFieldName: string);overload;

    //字段控件位置和大小是否保存在共用的地方(共用的地方指数据字典表中，不同地方指控件属性表中)
    property PublicSizePosition  : Boolean read FPublicSizePosition   write SetPublicSizePosition;

    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    //拷贝设置字段
    procedure CopySetup;
    //清除设置字段
    procedure ClearSetupField;
    //开窗设置包含字段
    procedure SetupHaveField;
    //开窗设置包含字段(隐藏已在其它面板显示的字段)
    procedure SetupHaveField_HideViewInotherPanel;
    //开窗设置不包含字段
    procedure SetupNoHaveField;
    //开窗设置只读字段
    procedure SetupReadOnlyField;
    //开窗设置可编辑字段
    procedure SetupEditField;

    //设置包含字段
    procedure SetEditField(aValue: string);
    //设置不包含字段
    procedure SetHaveField(aValue: string);
    //设置只读字段
    procedure SetNoHaveField(aValue: string);
    //设置可编辑字段
    procedure SetReadOnlyField(aValue: string);

  published
    property ReadOnly : Boolean read FReadOnly write FReadOnly;

    property DataSource : TDataSource read GetDataSource write SetDataSource;

    //例外的控件名列表，表示这些控件在面板中不做面板的内部操作，仅受设计期设置限制
    property ExceptionControlNames : Tstrings read GetExceptionControlNames write SetExceptionControlNames;

    //自动设置面板最右控件的右宽度扩展
    property AddAnchors_akRight : Boolean read FAddAnchors_akRight write FAddAnchors_akRight ;
    //自动设置面板最下控件的下高度扩展
    property AddAnchors_akBottom : Boolean read FAddAnchors_akBottom write FAddAnchors_akBottom ;

    //面板计算的高与宽，创建面板控件时计算出的最佳面板的宽度与高度
    property AutoCaleWidth  : longint read FAutoCaleWidth ;
    property AutoCaleHeight  : longint read FAutoCaleHeight ;

    //是否要将面板的高与宽设置为面板计算的高与宽
    //自动设置面板高为面板计算的高
    property AutoHeight : Boolean read FAutoHeight write FAutoHeight ;
    //自动设置面板宽为面板计算的宽
    property AutoWidth : Boolean read FAutoWidth write FAutoWidth ;

    //创建控件后触发事件
    property OnAfterCreateControls: TNotifyEvent read FOnAfterCreateControls write FOnAfterCreateControls;
    //释放控件后触发事件
    property OnAfterFreeControls: TNotifyEvent read FOnAfterFreeControls write FOnAfterFreeControls;

    //保存控件后触发事件
    property OnAfterSaveControls: TNotifyEvent read FOnAfterSaveControls write FOnAfterSaveControls;
  end;

implementation


{ TABDBPanelDataLink }

procedure TABDBPanelDataLink.ActiveChanged;
begin
  inherited;
  if (Assigned(DataSet)) then
  begin
    FlinkPanel.LinkFieldDefLabelControls;
    //有待改进
    //在激活连接时创建字段的控件会存在旧控件在PAS单元中没有删除,新控件在PAS单元中没有创建的问题,
    //从而不能正确保存
    //FlinkPanel.RefreshControls;
  end;
end;

{ TABDBPanel }

constructor TABDBPanel.Create(AOwner: TComponent);
var
  tempMenuItem:TMenuItem;
begin
  inherited Create(AOwner);

  FExceptionControlNames:= TStringList.Create;
  FAddAnchors_akRight:=true;
  FAddAnchors_akBottom:=true;
  FAutoCreate:=false;
  AutoSize:=false;
  FGetExpProperty:=false;

  FDataLink := TABDBPanelDataLink.Create;
  FDataLink.linkPanel:=self;

  AutoHeight:=true;
  AutoWidth:=true;
  Height:=200;
  //admin才能设计 TABDBPanel中的控件位置与大小
  if ABPubUser.IsAdmin then
  begin
    PopupMenu := TPopupMenu.Create(self);

    tempMenuItem:=TMenuItem.Create(self);
    tempMenuItem.Caption:=ABDBPanelPopupMenuStrs[0];
    PopupMenu.Items.Add(tempMenuItem);

    tempMenuItem:=TMenuItem.Create(self);
    tempMenuItem.OnClick:=PopupMenu_Plan;
    tempMenuItem.Name:='PopupMenu_RefreshControls_InitPositionAndInitSize_AutoSize';
    tempMenuItem.Caption:=ABDBPanelPopupMenuStrs[1];
    PopupMenu.Items.Add(tempMenuItem);

    tempMenuItem:=TMenuItem.Create(self);
    tempMenuItem.OnClick:=PopupMenu_Plan;
    tempMenuItem.Name:='PopupMenu_RefreshControls_InitPositionAndDatabaseSize_AutoSize';
    tempMenuItem.Caption:=ABDBPanelPopupMenuStrs[2];
    PopupMenu.Items.Add(tempMenuItem);

    tempMenuItem:=TMenuItem.Create(self);
    tempMenuItem.OnClick:=PopupMenu_Plan;
    tempMenuItem.Name:='PopupMenu_RefreshControls_SetPositionAndInitSize_AutoSize';
    tempMenuItem.Caption:=ABDBPanelPopupMenuStrs[3];
    PopupMenu.Items.Add(tempMenuItem);

    tempMenuItem:=TMenuItem.Create(self);
    tempMenuItem.OnClick:=PopupMenu_Plan;
    tempMenuItem.Name:='PopupMenu_RefreshControls_SetPositionAndDatabaseSize_AutoSize';
    tempMenuItem.Caption:=ABDBPanelPopupMenuStrs[4];
    PopupMenu.Items.Add(tempMenuItem);

    tempMenuItem:=TMenuItem.Create(self);
    tempMenuItem.OnClick:=PopupMenu_Plan;
    tempMenuItem.Name:='PopupMenu_RefreshControls_DatabasePositionAndInitSize_AutoSize';
    tempMenuItem.Caption:=ABDBPanelPopupMenuStrs[5];
    PopupMenu.Items.Add(tempMenuItem);

    tempMenuItem:=TMenuItem.Create(self);
    tempMenuItem.OnClick:=PopupMenu_Plan;
    tempMenuItem.Name:='PopupMenu_RefreshControls_DatabasePositionAndDatabaseSize_AutoSize';
    tempMenuItem.Caption:=ABDBPanelPopupMenuStrs[6];
    PopupMenu.Items.Add(tempMenuItem);

    tempMenuItem:=TMenuItem.Create(self);
    tempMenuItem.OnClick:=PopupMenu_Plan;
    tempMenuItem.Name:='PopupMenu_RefreshControls_DatabasePositionAndDatabaseSize';
    tempMenuItem.Caption:=ABDBPanelPopupMenuStrs[7];
    PopupMenu.Items.Add(tempMenuItem);

    tempMenuItem:=TMenuItem.Create(self);
    tempMenuItem.OnClick:=PopupMenu_Plan;
    tempMenuItem.Name:='PopupMenu_FreeControls';
    tempMenuItem.Caption:=ABDBPanelPopupMenuStrs[8];
    PopupMenu.Items.Add(tempMenuItem);

    tempMenuItem:=TMenuItem.Create(self);
    tempMenuItem.Caption:=ABDBPanelPopupMenuStrs[9];
    PopupMenu.Items.Add(tempMenuItem);

    tempMenuItem:=TMenuItem.Create(self);
    tempMenuItem.OnClick:=PopupMenu_Plan;
    tempMenuItem.Name:='PopupMenu_SaveControlsToField';
    tempMenuItem.Caption:=ABDBPanelPopupMenuStrs[10];
    tempMenuItem.Default:=true;
    PopupMenu.Items.Add(tempMenuItem);

    tempMenuItem:=TMenuItem.Create(self);
    tempMenuItem.OnClick:=PopupMenu_Plan;
    tempMenuItem.Name:='PopupMenu_SaveControlsToSingle';
    tempMenuItem.Caption:=ABDBPanelPopupMenuStrs[11];
    PopupMenu.Items.Add(tempMenuItem);


    tempMenuItem:=TMenuItem.Create(self);
    tempMenuItem.Caption:=ABDBPanelPopupMenuStrs[12];
    PopupMenu.Items.Add(tempMenuItem);

    tempMenuItem:=TMenuItem.Create(self);
    tempMenuItem.OnClick:=PopupMenu_Plan;
    tempMenuItem.Name:='PopupMenu_SetupHaveField';
    tempMenuItem.Caption:=ABDBPanelPopupMenuStrs[13];
    PopupMenu.Items.Add(tempMenuItem);

    tempMenuItem:=TMenuItem.Create(self);
    tempMenuItem.OnClick:=PopupMenu_Plan;
    tempMenuItem.Name:='PopupMenu_SetupHaveField_HideViewInotherPanel';
    tempMenuItem.Caption:=ABDBPanelPopupMenuStrs[14];
    PopupMenu.Items.Add(tempMenuItem);

    tempMenuItem:=TMenuItem.Create(self);
    tempMenuItem.OnClick:=PopupMenu_Plan;
    tempMenuItem.Name:='PopupMenu_SetupNoHaveField';
    tempMenuItem.Caption:=ABDBPanelPopupMenuStrs[15];
    PopupMenu.Items.Add(tempMenuItem);

    tempMenuItem:=TMenuItem.Create(self);
    tempMenuItem.OnClick:=PopupMenu_Plan;
    tempMenuItem.Name:='PopupMenu_SetupReadOnlyField';
    tempMenuItem.Caption:=ABDBPanelPopupMenuStrs[16];
    PopupMenu.Items.Add(tempMenuItem);

    tempMenuItem:=TMenuItem.Create(self);
    tempMenuItem.OnClick:=PopupMenu_Plan;
    tempMenuItem.Name:='PopupMenu_SetupEditField';
    tempMenuItem.Caption:=ABDBPanelPopupMenuStrs[17];
    PopupMenu.Items.Add(tempMenuItem);

    tempMenuItem:=TMenuItem.Create(self);
    tempMenuItem.OnClick:=PopupMenu_Plan;
    tempMenuItem.Name:='PopupMenu_CopySetup';
    tempMenuItem.Caption:=ABDBPanelPopupMenuStrs[18];
    PopupMenu.Items.Add(tempMenuItem);

    tempMenuItem:=TMenuItem.Create(self);
    tempMenuItem.OnClick:=PopupMenu_Plan;
    tempMenuItem.Name:='PopupMenu_ClearSetupField';
    tempMenuItem.Caption:=ABDBPanelPopupMenuStrs[19];
    PopupMenu.Items.Add(tempMenuItem);
  end;
end;

destructor TABDBPanel.Destroy;
var
  i:longint;
begin
  FreeControls;
  if (assigned(PopupMenu)) then
  begin
    for i := PopupMenu.Items.Count-1 downto 0  do
    begin
      PopupMenu.Items[i].Clear;
    end;
    PopupMenu.Free;
  end;
  FDataLink.Free;
  FExceptionControlNames.Free;
  inherited Destroy;
end;

function TABDBPanel.GetDataSource : TDataSource;
begin
  Result := FDataLink.DataSource;
end;

procedure TABDBPanel.SetDataSource(Value : TDataSource);
begin
  FDataLink.DataSource := Value;
end;

function TABDBPanel.GetExceptionControlNames: TStrings;
begin
  Result:=FExceptionControlNames;
end;

procedure TABDBPanel.SetExceptionControlNames(const Value: TStrings);
begin
  FExceptionControlNames.Assign(Value);
end;

procedure TABDBPanel.FreeControls;
var
  i:longint;
  tempFieldDef:PABFieldDef;
  tempControl:TControl;
begin
  inherited;
  if (Assigned(FDataLink.DataSet)) and
     (FDataLink.DataSet.Active)  then
  begin
    for i := 0 to FDataLink.DataSet.FieldCount-1 do
    begin
      tempFieldDef:=ABFieldDefByFieldName(FDataLink.DataSet,FDataLink.DataSet.Fields[i].FieldName);
      if (Assigned(tempFieldDef))  then
      begin
        if (Assigned(tempFieldDef.Fi_Label)) and
           (tempFieldDef.Fi_Label.Parent =self) and
           ((FExceptionControlNames.count<=0) or (FExceptionControlNames.indexof(tempFieldDef.Fi_Label.name)<0)) then
        begin
          tempFieldDef.Fi_Label.Free;
          tempFieldDef.Fi_Label:=nil;
        end;
        if (Assigned(tempFieldDef.Fi_Control)) and
           (tempFieldDef.Fi_Control.Parent =self) and
           ((FExceptionControlNames.count<=0) or (FExceptionControlNames.indexof(tempFieldDef.Fi_Control.name)<0)) then
        begin
          tempFieldDef.Fi_Control.Free;
          tempFieldDef.Fi_Control:=nil;
        end;
      end;
    end;

    for i := ControlCount-1 downto 0 do
    begin
      if ((Assigned(Controls[i]))) and
         ((FExceptionControlNames.count<=0) or (FExceptionControlNames.indexof(Controls[i].name)<0)) then
      begin
        tempControl:=Controls[i];
        tempControl.Free;
      end;
      FAutoCreate:=false;
    end;
    FAutoCreate:=false;
  end;

  if Assigned(FOnAfterFreeControls) then
    FOnAfterFreeControls(self);
end;

procedure TABDBPanel.CreateControls;
var
  tempABDBWinControl:TWinControl;
  tempLabel:TcxLabel;

  tempField:Tfield;
  tempFieldDef:PABFieldDef;

  i:longint;
  tempABBlobFieldDef:TBlobFieldDef;
  tempParentForm:TCustomForm;
  tempDataSet:TDataset;
begin
  tempParentForm:=GetParentForm(self);
  if Assigned(tempParentForm) then
  begin
    tempDataSet:=FDataLink.DataSet;
    for i := 0 to FDataLink.DataSet.FieldCount-1 do
    begin
      tempField:=FDataLink.DataSet.Fields[i];
      tempFieldDef:=ABFieldDefByFieldName(FDataLink.DataSet,tempField.FieldName);
      if Assigned(tempFieldDef) then
      begin
        if (tempFieldDef.Fi_IsInputPanelView)  and
           ((FNoHaveField=EmptyStr)   or (ABPos(','+tempField.FieldName+',',','+FNoHaveField  +',')<=0)) and
           ((FHaveField=EmptyStr) or (ABPos(','+tempField.FieldName+',',','+FHaveField+',')> 0))
            then
        begin
          //判断是否存在已创建的控件,如存在则用已有的控件
          tempABDBWinControl:=TWinControl(FindChildControl(Name+'_'+tempFieldDef.Fi_Name+'_Control'));
          tempLabel:=TcxLabel(FindChildControl(Name+'_'+tempFieldDef.Fi_Name+'_Label'));

          if not Assigned(tempABDBWinControl) then
          begin
            tempABDBWinControl:=TWinControl(ABCreateCloumnAndControl(DataSource,tempField,tempParentForm));
          end;

          ABReFreshCloumnAndControl(DataSource,tempField,tempABDBWinControl);
          if (tempField.IsBlob) then
          begin
            tempABBlobFieldDef:=TABDictionaryQuery(tempDataSet).BlobFieldDefs.FindFieldName(tempField.FieldName);
            if Assigned(tempABBlobFieldDef) then
            begin
              ABSetObjectPropValue(tempABDBWinControl,'DataBinding.DataSource',tempABBlobFieldDef.Datasource);
              ABSetPropValue(tempABDBWinControl,'DataBinding.DataField',tempField.FieldName);
            end;
          end;

          tempABDBWinControl.Hint:=tempFieldDef.Fi_Hint;
          tempABDBWinControl.ShowHint:=true;
          tempABDBWinControl.Name:= Name+'_'+tempFieldDef.Fi_Name+'_Control';
          if (tempABDBWinControl is TcxCustomCheckBox) or
             (tempABDBWinControl is TcxCustomRadioGroup) then
          begin
            ABSetPropValue(tempABDBWinControl,'Caption',EmptyStr);
          end;
          if (FDataLink.DataSet.Fields[i].FieldKind =fkCalculated) or
             ((Assigned(tempFieldDef.PMetaDef)) and (tempFieldDef.PMetaDef.Fi_IsAutoAdd)) then
            tempABDBWinControl.Enabled:=false;

          if not Assigned(tempLabel) then
            tempLabel:=TABDBFieldCaption.Create(tempParentForm);

          tempLabel.FocusControl:=tempABDBWinControl;
          tempLabel.Hint:=tempFieldDef.Fi_Hint;

          tempLabel.ShowHint:=true;
          tempLabel.Name:= Name+'_'+tempFieldDef.Fi_Name+'_Label';

          TABDBFieldCaption(tempLabel).DataSource:=DataSource;
          TABDBFieldCaption(tempLabel).DataField:=tempField.FieldName;
          tempLabel.Caption:=tempFieldDef.Fi_Caption;
          tempLabel.AutoSize:=false;
          tempLabel.Properties.WordWrap:=true;

          if ((FReadOnlyField=EmptyStr) or (ABPos(','+tempFieldDef.Fi_Name+',',','+FReadOnlyField  +',')<=0)) and
             ((FEditField=EmptyStr)   or (ABPos(','+tempFieldDef.Fi_Name+',',','+FEditField+',')> 0)) then
          begin
          end
          else
          begin
            tempABDBWinControl.Enabled:=false;
            tempFieldDef.Fi_LockInputPanelView :=true;
          end;

          tempLabel.Parent:=Self;
          tempABDBWinControl.Parent:= Self;

          tempFieldDef.Fi_Label:=tempLabel;
          tempFieldDef.Fi_Control:=tempABDBWinControl;
        end;
      end;
    end;

    FAutoCreate:=true;
  end;
  if Assigned(FOnAfterCreateControls) then
    FOnAfterCreateControls(self);
end;

procedure TABDBPanel.ReFresh_Fi_SetOrder;
var
  i,j,k,tempOrder:longint;
  tempFieldName:string;
  tempField:TField;
  tempFieldDef:PABFieldDef;
begin
  for i := 0 to FDataLink.DataSet.FieldCount-1 do
  begin
    tempFieldDef:=ABFieldDefByFieldName(FDataLink.DataSet,FDataLink.DataSet.Fields[i].FieldName);
    if (Assigned(tempFieldDef)) and
       (tempFieldDef.Fi_IsInputPanelView)  and
       ((FNoHaveField=EmptyStr) or (ABPos(','+tempFieldDef.Fi_Name+',',','+FNoHaveField  +',')<=0)) and
       ((FHaveField=EmptyStr) or (ABPos(','+tempFieldDef.Fi_Name+',',','+FHaveField+',')> 0)) then
    begin
      tempFieldDef.Fi_SetOrder:=-1;
    end;
  end;

  i:=0;
  tempOrder:=0;
  While i<=Height+20 do
  Begin
    j:=0;
    While j<=width+20 do
    Begin
      for k := 0 to ControlCount - 1 do
      begin
        if ((Assigned(Controls[k]))) and
           (Controls[k].Top>=i) and (Controls[k].Top<i+20) and
           (Controls[k].left>=j) and (Controls[k].left<j+20) and
           ((FExceptionControlNames.count<=0) or (FExceptionControlNames.indexof(Controls[k].name)<0)) and
           (not (Controls[k] is TcxLabel))  then
        begin
          tempFieldName:=ABGetCortrolPropertyValue(Controls[k],'DataField');
          if (tempFieldName<>EmptyStr) then
          begin
            tempField:=FDataLink.DataSet.FindField(tempFieldName);
            if (Assigned(tempField)) then
            begin
              tempFieldDef:=ABFieldDefByFieldName(FDataLink.DataSet,tempFieldName);
              if (Assigned(tempFieldDef))  then
              begin
                tempFieldDef.Fi_SetOrder:=tempOrder;
                Inc(tempOrder,1);
                //同一个20平方内的有多个控件时将只考虑第一个
                Break;
              end;
            end;
          end;
        end;
      end;
      inc(j,20);
    end;
    inc(i,20);
  end;
end;

function TABDBPanel.GetFuncControlPropertyDataset:Tdataset;
begin
  if not Assigned(FFuncControlPropertyDataset) then
    FFuncControlPropertyDataset:= ABGetConstSqlPubDataset('ABSys_Org_FuncControlProperty',
                             [TABDictionaryQuery(DataSource.DataSet).FuncGuid,
                              GetParentForm(self).Name],TABInsideQuery);

  result:=FFuncControlPropertyDataset;
end;

procedure TABDBPanel.SetControlsPosition(aPositionType:LongInt;aSizeType:LongInt;aAutoSuit:Boolean;aMinWidth,aMinHeight:longint);
var
  i:longint;
  tempABDBWinControl,
  tempPriWinControl:TWinControl;
  tempLabel:TcxLabel;

  templeft,
  tempTop,
  tempMaxHeight,
  tempSelfWidth:LongInt;
  tempFieldDef:PABFieldDef;

  tempLableleft,tempLableTop,tempLableWidth,tempLableHeight:longint;
  tempControlleft,tempControlTop,tempControlWidth,tempControlHeight:longint;
  tempParentForm:TCustomForm;

  tempFieldPosition:boolean;
  tempHeight:longint;
  tempWidth:longint;
begin
  tempMaxHeight   :=0;
  tempPriWinControl :=nil;

  templeft:=ABDBPanelParams_Leftspacing;
  tempTop :=ABDBPanelParams_Topspacing;

  tempSelfWidth:=Width-ABDBPanelParams_Leftspacing-ABDBPanelParams_Righttspacing;
  tempFieldPosition:=False;
  for i := 0 to FDataLink.DataSet.FieldCount-1 do
  begin
    if aPositionType=3 then
      tempFieldDef:=TABDictionaryQuery(FDataLink.DataSet).FieldDefBySetOrder(i)
    else
      tempFieldDef:=ABFieldDefByFieldName(FDataLink.DataSet,FDataLink.DataSet.Fields[i].FieldName);

    if (Assigned(tempFieldDef)) and
       (Assigned(tempFieldDef.Fi_Label)) and
       (tempFieldDef.Fi_Label.Parent =self)  then
    begin
      tempLabel:=TcxLabel(tempFieldDef.Fi_Label);
      tempABDBWinControl:=tempFieldDef.Fi_Control;
      //是否用数据字典中字段的大小
      if aSizeType=1 then
      begin
        if FPublicSizePosition then
        begin
          tempLableWidth:=tempFieldDef.Fi_LabelWidth;
          tempLableHeight:=tempFieldDef.Fi_LabelHeight;
          tempControlWidth:=tempFieldDef.Fi_ControlWidth;
          tempControlHeight:=tempFieldDef.Fi_ControlHeight;
        end
        else
        begin
          tempParentForm:=GetParentForm(self);
          if (Assigned(tempParentForm)) and (tempParentForm.Name<>EmptyStr) then
          begin
            tempLableWidth   := ABGetFieldValue(GetFuncControlPropertyDataset,
                                                          ['FP_ControlName','FP_PropertyName'],
                                                          [tempLabel.Name,'Fi_LabelWidth'   ],
                                                          ['FP_PropertyValue'],0);
            tempLableHeight   := ABGetFieldValue(GetFuncControlPropertyDataset,
                                                          ['FP_ControlName','FP_PropertyName'],
                                                          [tempLabel.Name,'Fi_LabelHeight'   ],
                                                          ['FP_PropertyValue'],0);
            tempControlWidth   := ABGetFieldValue(GetFuncControlPropertyDataset,
                                                          ['FP_ControlName','FP_PropertyName'],
                                                          [tempABDBWinControl.Name,'Fi_ControlWidth'   ],
                                                          ['FP_PropertyValue'],0);
            tempControlHeight   := ABGetFieldValue(GetFuncControlPropertyDataset,
                                                          ['FP_ControlName','FP_PropertyName'],
                                                          [tempABDBWinControl.Name,'Fi_ControlHeight'   ],
                                                          ['FP_PropertyValue'],0);
          end;
        end;
      end
      else
      begin
        tempLableWidth:=0;
        tempLableHeight:=0;
        tempControlWidth:=0;
        tempControlHeight:=0;
      end;

      tempFieldDef.Fi_Label.Parent:=nil;
      tempFieldDef.Fi_Control.Parent:=nil;
      try
        ABSetControlSize(tempLableWidth,tempLableHeight,tempControlWidth,tempControlHeight,
                         TcxLabel(tempFieldDef.Fi_Label),tempFieldDef.Fi_Control,8,false,True);

        tempFieldPosition:=False;
        //是否用数据字典中字段的位置
        if (aPositionType=1)then
        begin
          tempFieldPosition:=true;
          tempLableLeft   :=0;
          tempLableTop    :=0;
          tempControlLeft :=0;
          tempControlTop  :=0;
          if FPublicSizePosition then
          begin
            tempLableleft:=tempFieldDef.Fi_LabelLeft;
            tempLableTop:=tempFieldDef.Fi_LabelTop;
            tempControlleft:=tempFieldDef.Fi_ControlLeft;
            tempControlTop:=tempFieldDef.Fi_ControlTop;
          end
          else
          begin
            tempParentForm:=GetParentForm(self);
            if (Assigned(tempParentForm))  and (tempParentForm.Name<>EmptyStr)then
            begin
              tempLableLeft   := ABVartoInt64Def(GetFuncControlPropertyDataset.Lookup('FP_ControlName;FP_PropertyName',
                                                  VarArrayOf([tempLabel.Name,     'Fi_LabelLeft'   ]),
                                                 'FP_PropertyValue'),0);
              tempLableTop    := ABVartoInt64Def(GetFuncControlPropertyDataset.Lookup('FP_ControlName;FP_PropertyName',
                                                  VarArrayOf([tempLabel.Name,     'Fi_LabelTop'    ]),
                                                 'FP_PropertyValue'),0);
              tempControlLeft := ABVartoInt64Def(GetFuncControlPropertyDataset.Lookup('FP_ControlName;FP_PropertyName',
                                                  VarArrayOf([tempABDBWinControl.Name,'Fi_ControlLeft' ]),
                                                 'FP_PropertyValue'),0);
              tempControlTop  := ABVartoInt64Def(GetFuncControlPropertyDataset.Lookup('FP_ControlName;FP_PropertyName',
                                                  VarArrayOf([tempABDBWinControl.Name,'Fi_ControlTop'  ]),
                                                 'FP_PropertyValue'),0);
            end;
          end;

          if (tempLableleft=0) and
             (tempLableTop=0) and
             (tempControlleft=0) and
             (tempControlTop=0) then
          begin
            tempFieldPosition:=False;
          end
          else
          begin
            tempLabel.Left:=tempLableleft;
            tempLabel.Top:=tempLableTop;
            tempABDBWinControl.Left:=tempControlleft;
            tempABDBWinControl.Top:=tempControlTop;
            tempPriWinControl:=tempABDBWinControl;
          end;
        end;

        if not tempFieldPosition then
        begin
          //设置位置
          tempLabel.Left:=tempLeft;
          tempLabel.Top :=tempTop;
          tempLeft:=tempLeft+tempLabel.Width+1;
          tempABDBWinControl.Left:=tempLeft;
          tempABDBWinControl.Top:=tempTop;

          //控件所放位置是否超出最大宽度时换行
          if tempABDBWinControl.Left+tempABDBWinControl.Width-ABTrunc(tempABDBWinControl.Width/5)>tempSelfWidth then
          begin
            //本次控件换行时调整上一个控件的宽度
            if (Assigned(tempPriWinControl)) then
            begin
              tempPriWinControl.Width:=tempSelfWidth-tempPriWinControl.Left-ABDBPanelParams_Righttspacing;
            end;

            //换行时实始当前左边与上边的值
            templeft:=ABDBPanelParams_Leftspacing;
            tempTop:=tempTop+tempMaxHeight+ABDBPanelParams_Yspacing;
            //重新设置控件位置
            tempLabel.Left:=tempLeft;
            tempLabel.Top :=tempTop;
            tempLeft:=tempLabel.Left+tempLabel.Width+1;
            tempABDBWinControl.Left:=tempLeft;
            tempABDBWinControl.Top:=tempTop;

            //设置下一控件的左边
            tempLeft:=tempLeft+tempABDBWinControl.Width+ABDBPanelParams_Xspacing;
            tempMaxHeight:=tempLabel.Height;
          end
          else
          begin
            //设置下一控件的左边
            tempLeft:=tempLeft+tempABDBWinControl.Width+ABDBPanelParams_Xspacing;
          end;
          tempMaxHeight:=Max(tempMaxHeight,Max(tempLabel.Height,tempABDBWinControl.Height));
          tempPriWinControl:= tempABDBWinControl;
        end;
      finally
        tempFieldDef.Fi_Label.Parent:=self;
        tempFieldDef.Fi_Control.Parent:=self;
      end;
    end;
  end;

  //最后一个控件调整宽度
  if (not tempFieldPosition)then
  begin
    if (Assigned(tempPriWinControl)) then
    begin
      tempPriWinControl.Width:=tempSelfWidth-tempPriWinControl.Left-ABDBPanelParams_Righttspacing;
    end;
  end;

  ABGetParentControlWidthAndHeight(
                                  self,
                                  tempWidth,
                                  tempHeight
                                    );
  FAutoCaleWidth :=tempWidth+ABDBPanelParams_Righttspacing ;
  FAutoCaleHeight:= tempHeight+ABDBPanelParams_Bottomspacing;
  ABSetParentControlSize( self,
                          FAutoCaleWidth,
                          FAutoCaleHeight,
                          aMinWidth,FAutoWidth,aMinHeight,FAutoHeight
                            );

  ABSetRightControlAndLastControl(self,
                                  tempWidth,
                                  tempHeight,

                                  ABDBPanelParams_Righttspacing,ABDBPanelParams_Bottomspacing,
                                  aAutoSuit,AddAnchors_akRight,AddAnchors_akBottom
                                  );
end;

procedure TABDBPanel.LinkFieldDefLabelControls(aOnlyBeforeCreate:boolean);
var
  tempFieldDef:PABFieldDef;
  tempFieldName:string;
  tempField:TField;
  i:longint;
begin
  if (aOnlyBeforeCreate) and
     (FAutoCreate) then
    exit;

  for i := ControlCount-1 downto 0 do
  begin
    tempFieldName:=EmptyStr;
    if ((Assigned(Controls[i]))) and
       ((FExceptionControlNames.count<=0) or (FExceptionControlNames.indexof(Controls[i].name)<0)) then
    begin
      if (Controls[i] is TABDBFieldCaption)    then
      begin
        tempFieldName:=trim(TABDBFieldCaption(Controls[i]).DataField);
        if (tempFieldName<>EmptyStr) then
        begin
          tempField:=FDataLink.DataSet.FindField(tempFieldName);
          if (Assigned(tempField)) then
          begin
            tempFieldDef:=ABFieldDefByFieldName(FDataLink.DataSet,tempField.FieldName);
            if (Assigned(tempFieldDef))  then
            begin
              tempFieldDef.Fi_Label:=TWinControl(Controls[i]);
            end;
          end;
        end;
      end
      else
      begin
        tempFieldName:=ABGetCortrolPropertyValue(Controls[i],'DataField');
        if (tempFieldName<>EmptyStr) then
        begin
          tempField:=FDataLink.DataSet.FindField(tempFieldName);
          if (Assigned(tempField)) then
          begin
            tempFieldDef:=ABFieldDefByFieldName(FDataLink.DataSet,tempFieldName);
            if (Assigned(tempFieldDef))  then
            begin
              tempFieldDef.Fi_Control:=TWinControl(Controls[i]);
            end;
          end;
        end;
      end;
    end;
  end;
end;

procedure TABDBPanel.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited;
  if (Operation = opRemove) then
  begin
    if (Assigned(FDataLink)) and (FDataLink.DataSource =AComponent)   then
      FDataLink.DataSource:=nil;
  end;
end;

procedure TABDBPanel.RefreshControls( aPositionType:LongInt;
                                      aSizeType:LongInt;
                                      aAutoSuit:Boolean;
                                      aIsCreate:boolean;
                                      aMinWidth:longint;
                                      aMinHeight:longint
                                      );
var
  tempStr1:string;
begin
  if (Assigned(FDataLink.DataSet)) and
     (FDataLink.DataSet.FieldCount>0) then
  begin
    FDataLink.DataSet.DisableControls;
    try
      GetExpProperty;
      if aPositionType=3 then
      begin
        ReFresh_Fi_SetOrder;
        if not SetFi_SetOrder then
          exit;
      end;

      tempStr1:=GetDBPanelPropertyValue('PublicSizePosition');
      if tempStr1=EmptyStr then
      begin
        FPublicSizePosition:=true;
      end
      else
      begin
        FPublicSizePosition:=ABStrToBool(tempStr1);
      end;

      if aIsCreate then
      begin
        FreeControls;
        CreateControls;
        SetControlsPosition(aPositionType,aSizeType,aAutoSuit,aMinWidth,aMinHeight);
      end
      else
      begin
        //仅关联控件与字段框架信息的控件变量
        CreateControls;
      end;

      if FDataLink.DataSet is TABDictionaryQuery then
      begin
        if (TABDictionaryQuery(FDataLink.DataSet).LinkDBPanelList.IndexOf(self)<0) then
        begin
          TABDictionaryQuery(FDataLink.DataSet).LinkDBPanelList.Add(self);
        end;
        TABDictionaryQuery(FDataLink.DataSet).LinkBlobFieldDatasets;
      end;
    finally
      FDataLink.DataSet.EnableControls;
    end;
  end;
end;

function TABDBPanel.SetFi_SetOrder:Boolean;
var
  tempForm:TABOrderStrForm;
  tempFieldDef:PABFieldDef;
  ii,i:LongInt;
begin
  Result:=false;
  if (Assigned(FDataLink.DataSet)) and
     (FDataLink.DataSet.FieldCount>0) then
  begin
    tempForm:=TABOrderStrForm.Create(nil);
    try
      for ii := 0 to FDataLink.DataSet.FieldCount-1 do
      begin
        for i := 0 to FDataLink.DataSet.FieldCount-1 do
        begin
          tempFieldDef:=ABFieldDefByFieldName(FDataLink.DataSet,FDataLink.DataSet.Fields[i].FieldName);
          if (Assigned(tempFieldDef)) and
             (tempFieldDef.Fi_IsInputPanelView)  and
             ((FNoHaveField=EmptyStr) or (ABPos(','+tempFieldDef.Fi_Name+',',','+FNoHaveField  +',')<=0)) and
             ((FHaveField=EmptyStr) or (ABPos(','+tempFieldDef.Fi_Name+',',','+FHaveField+',')> 0)) then
          begin
            if tempFieldDef.Fi_SetOrder=-1 then
            begin
              if tempForm.ABItemList1.Frame.ListBox1.Items.IndexOf(tempFieldDef.Fi_Caption)<0 then
                tempForm.ABItemList1.Frame.ListBox1.Items.Insert(0,tempFieldDef.Fi_Caption);
            end
            else if tempFieldDef.Fi_SetOrder=ii then
            begin
              tempForm.ABItemList1.Frame.ListBox1.Items.Add(tempFieldDef.Fi_Caption);
              break;
            end;
          end;
        end;
      end;

      if tempForm.ShowModal=mrOK then
      begin
        for i := 0 to FDataLink.DataSet.FieldCount-1 do
        begin
          tempFieldDef:=ABFieldDefByFieldName(FDataLink.DataSet,FDataLink.DataSet.Fields[i].FieldName);
          if Assigned(tempFieldDef) then
            tempFieldDef.Fi_SetOrder:=-1;
        end;

        for I := 0 to tempForm.ABItemList1.Frame.ListBox1.Items.Count - 1 do
        begin
          tempFieldDef:=ABFieldDefByFieldName(FDataLink.DataSet,ABGetFieldNamesByDisplayLabels(FDataLink.DataSet,tempForm.ABItemList1.Frame.ListBox1.Items[i]));
          if (Assigned(tempFieldDef))  then
          begin
            tempFieldDef.Fi_SetOrder:=i;
          end;
        end;
        Result:=true;
      end;
    finally
      tempForm.Free;
    end;
  end;
end;

procedure TABDBPanel.SetupHaveField;
var
  tempStr1:string;
begin
  tempStr1:=FHaveField;
  if (ABSelectFieldNames(tempStr1,FDataLink.DataSet)) and
     (tempStr1<>FHaveField) then
  begin
    SetHaveField(tempStr1);
  end;
end;

procedure TABDBPanel.SetupHaveField_HideViewInotherPanel;
var
  tempStr1,
  tempNoHaveFIeldNames:string;
  I: Integer;
  tempFieldDef:PABFieldDef;
begin
  tempNoHaveFIeldNames:=emptystr;
  for i := 0 to FDataLink.DataSet.FieldCount-1 do
  begin
    tempFieldDef:=ABFieldDefByFieldName(FDataLink.DataSet,FDataLink.DataSet.Fields[i].FieldName);
    if (Assigned(tempFieldDef))  and
       (Assigned(tempFieldDef.Fi_Control))  and
       (tempFieldDef.Fi_Control.parent<>self) then
    begin
      ABAddstr(tempNoHaveFIeldNames,tempFieldDef.Fi_Name);
    end;
  end;

  tempStr1:=FHaveField;
  if (ABSelectFieldNames(tempStr1,FDataLink.DataSet,tempNoHaveFIeldNames)) and
     (tempStr1<>FHaveField) then
  begin
    SetHaveField(tempStr1);
  end;
end;

procedure TABDBPanel.SetupNoHaveField;
var
  tempStr1:string;
begin
  tempStr1:=FNoHaveField;
  if (ABSelectFieldNames(tempStr1,FDataLink.DataSet))  and
     (tempStr1<>FNoHaveField) then
  begin
    SetNoHaveField(tempStr1);
  end;
end;

procedure TABDBPanel.SetupReadOnlyField;
var
  tempStr1:string;
begin
  tempStr1:=FReadOnlyField;
  if (ABSelectFieldNames(tempStr1,FDataLink.DataSet)) and
     (tempStr1<>FReadOnlyField) then
  begin
    SetReadOnlyField(tempStr1);
  end;
end;

procedure TABDBPanel.SetupEditField;
var
  tempStr1:string;
begin
  tempStr1:=FEditField;
  if (ABSelectFieldNames(tempStr1,FDataLink.DataSet)) and
     (tempStr1<>FEditField) then
  begin
    SetEditField(tempStr1);
  end;
end;

procedure TABDBPanel.SetHaveField(aValue:string);
begin
  SetDBPanelPropertyValue('HaveField'    ,aValue);
  FHaveField := aValue;
end;

procedure TABDBPanel.SetNoHaveField(aValue:string);
begin
  SetDBPanelPropertyValue('NoHaveField'    ,aValue);
  FNoHaveField := aValue;
end;

procedure TABDBPanel.SetReadOnlyField(aValue:string);
begin
  SetDBPanelPropertyValue('ReadOnlyField'    ,aValue);
  FReadOnlyField := aValue;
end;

procedure TABDBPanel.SetEditField(aValue:string);
begin
  SetDBPanelPropertyValue('EditField',aValue);
  FEditField := aValue;
end;

procedure TABDBPanel.ClearSetupField;
begin
  SetHaveField(EmptyStr);
  SetNoHaveField(EmptyStr);
  SetReadOnlyField(EmptyStr);
  SetEditField(EmptyStr);
end;

procedure TABDBPanel.CopySetup;
var
  tempStr1:string;
  tempDataset,tempDataset1:TDataset;
begin
  tempDataset:=ABGetDataset('',
    ' SELECT FU_Name,FU_Guid,FP_FormName,FP_controlname                      '+
    ' FROM  ABSys_Org_Function,ABSys_Org_FuncControlProperty         '+
    ' where Fu_Guid =FP_FU_Guid and ISNULL(FP_controlname,'''')<>''''    '+
    ' group by FU_Name,FU_Guid,FP_FormName,FP_controlname            ',
                                    [],nil,True,TABFieldCaptionQuery);
  try
    tempDataset.FieldByName('FU_Guid').Visible:=false;
    if not tempDataset.IsEmpty then
    begin
      tempStr1:=ABShowEditDataset(tempDataset,false,False,false,'FU_Guid,FP_FormName,FP_controlname','请选择复制设置的来源模块控件',true,200);
      if tempStr1<>EmptyStr then
      begin
        tempDataset1:=ABGetDataset( '',
                                    ' SELECT FP_PropertyName,FP_PropertyValue                      '+
                                    ' FROM  ABSys_Org_FuncControlProperty                          '+
                                    ' where FP_FU_Guid='+QuotedStr(ABGetSpaceStr(tempStr1, 1, ','))+' and '+
                                    '       FP_FormName='+QuotedStr(ABGetSpaceStr(tempStr1, 2, ','))+' and '+
                                    '       FP_controlname='+QuotedStr(ABGetSpaceStr(tempStr1, 3, ',')),
                                    [],nil,True,TABFieldCaptionQuery);
        try
          if not tempDataset1.IsEmpty then
          begin
            if tempDataset1.Locate('FP_PropertyName','HaveField',[]) then
              SetHaveField(tempDataset1.FieldByName('FP_PropertyValue').AsString);
            if tempDataset1.Locate('FP_PropertyName','NoHaveField',[]) then
              SetNoHaveField(tempDataset1.FieldByName('FP_PropertyValue').AsString);
            if tempDataset1.Locate('FP_PropertyName','ReadOnlyField',[]) then
              SetReadOnlyField(tempDataset1.FieldByName('FP_PropertyValue').AsString);
            if tempDataset1.Locate('FP_PropertyName','EditField',[]) then
              SetEditField(tempDataset1.FieldByName('FP_PropertyValue').AsString);
          end;
        finally
          tempDataset1.Free;
        end;
      end;
    end;
  finally
    tempDataset.Free;
  end;
end;

function TABDBPanel.GetDBPanelPropertyValue(aPropertyName: string):string;
var
  tempParentForm:TCustomForm;
begin
  Result:=EmptyStr;
  tempParentForm:=GetParentForm(self);
  if (Assigned(tempParentForm))  and (tempParentForm.Name<>EmptyStr) then
  begin
    Result:=  ABGetFieldValue(GetFuncControlPropertyDataset,
                              ['FP_ControlName','FP_PropertyName'],
                              [Name,aPropertyName],
                              ['FP_PropertyValue'],'');
  end;
end;

procedure TABDBPanel.GetExpProperty;
begin
  if (not FGetExpProperty) and
     (Assigned(DataSource)) and
     (Assigned(DataSource.DataSet)) and
     (DataSource.DataSet.Active) then
  begin
    FHaveField:=GetDBPanelPropertyValue('HaveField');
    FNoHaveField:=GetDBPanelPropertyValue('NoHaveField');
    FReadOnlyField:=GetDBPanelPropertyValue('ReadOnlyField');
    FEditField:=GetDBPanelPropertyValue('EditField');
    FGetExpProperty:=true;
  end;
end;

procedure TABDBPanel.SetDBPanelPropertyValue(aPropertyName: string;aValue:string);
var
  tempParentForm:TCustomForm;
begin
  tempParentForm:=GetParentForm(self);
  if (Name<>EmptyStr) and
     (Assigned(tempParentForm)) and
     (tempParentForm.Name<>EmptyStr) and
     (TABDictionaryQuery(DataSource.DataSet).FuncGuid<>EmptyStr) then
  begin
    ABSetFieldValue(
                    GetFuncControlPropertyDataset,
                    ['FP_FU_Guid','FP_FormName','FP_ControlName','FP_PropertyName'],
                    [TABDictionaryQuery(DataSource.DataSet).FuncGuid,tempParentForm.Name,Name,aPropertyName],
                    ['FP_PropertyValue'],
                    [aValue]
                      );
  end;
end;


procedure TABDBPanel.SetPublicSizePosition(const Value: Boolean);
var
  tempboolean:Boolean;
begin
  tempboolean:=value;
  if (Assigned(DataSource)) and
     (Assigned(DataSource.DataSet)) then
  begin
    if (tempboolean) then
    begin
      if TABDictionaryQuery(DataSource.DataSet).LoadTables.Count>1  then
      begin
        if ABShow('多表模式时确定全局保存吗?',[],['是','否'],1)=2 then
          tempboolean:=false;
      end;
    end
    else
    begin
      if TABDictionaryQuery(DataSource.DataSet).LoadTables.Count=1  then
      begin
        if ABShow('单表模式时确定单独保存吗?',[],['是','否'],1)=2 then
          tempboolean:=false;
      end;
    end;
  end;

  SetDBPanelPropertyValue('PublicSizePosition',ABIIF(tempboolean,EmptyStr,ABBoolToStr(tempboolean)));
  FPublicSizePosition := tempboolean;
end;

procedure TABDBPanel.SaveControls(aControl:TControl;aFieldName:string);
var
  tempField:TField;
  tempFieldDef:PABFieldDef;
  tempParentForm:TCustomForm;
begin
  if (Assigned(aControl)) and
     (aFieldName<>EmptyStr) and
     (aControl.Parent =self) then
  begin
    tempParentForm:=GetParentForm(self);
    tempField:=FDataLink.DataSet.FindField(aFieldName);
    tempFieldDef:=ABFieldDefByFieldName(FDataLink.DataSet,tempField.FieldName);
    if (Assigned(tempFieldDef))  then
    begin
      if (aControl is TABDBFieldCaption)  then
      begin
        tempFieldDef.Fi_LabelLeft  :=aControl.Left;
        tempFieldDef.Fi_LabelTop   :=aControl.Top;
        tempFieldDef.Fi_LabelWidth :=aControl.Width;
        tempFieldDef.Fi_LabelHeight:=aControl.Height;

        if FPublicSizePosition then
        begin
          ABExecSQL('Main',
           ' update ABSys_Org_Field '+
           ' set Fi_LabelLeft  ='+inttostr(aControl.Left)+ ','+
           '     Fi_LabelTop   ='+inttostr(aControl.Top) + ','+
           '     Fi_LabelWidth ='+inttostr(aControl.Width) + ','+
           '     Fi_LabelHeight ='+inttostr(aControl.Height) +
           ' where Fi_Guid='+QuotedStr(tempFieldDef.Fi_Guid));
        end
        else
        begin
          if (Assigned(tempParentForm)) and
             (tempParentForm.Name<>EmptyStr)  and
             (TABDictionaryQuery(DataSource.DataSet).FuncGuid<>EmptyStr) then
          begin
            ABSetFieldValue(GetFuncControlPropertyDataset,['FP_FU_Guid','FP_FormName','FP_ControlName','FP_PropertyName'],[TABDictionaryQuery(DataSource.DataSet).FuncGuid,tempParentForm.Name,aControl.Name,'Fi_LabelLeft'  ],['FP_PropertyValue'],[aControl.Left  ]);
            ABSetFieldValue(GetFuncControlPropertyDataset,['FP_FU_Guid','FP_FormName','FP_ControlName','FP_PropertyName'],[TABDictionaryQuery(DataSource.DataSet).FuncGuid,tempParentForm.Name,aControl.Name,'Fi_LabelTop'   ],['FP_PropertyValue'],[aControl.Top   ]);
            ABSetFieldValue(GetFuncControlPropertyDataset,['FP_FU_Guid','FP_FormName','FP_ControlName','FP_PropertyName'],[TABDictionaryQuery(DataSource.DataSet).FuncGuid,tempParentForm.Name,aControl.Name,'Fi_LabelWidth' ],['FP_PropertyValue'],[aControl.Width ]);
            ABSetFieldValue(GetFuncControlPropertyDataset,['FP_FU_Guid','FP_FormName','FP_ControlName','FP_PropertyName'],[TABDictionaryQuery(DataSource.DataSet).FuncGuid,tempParentForm.Name,aControl.Name,'Fi_LabelHeight'],['FP_PropertyValue'],[aControl.Height]);
          end;
        end;
      end
      else
      begin
        tempFieldDef.Fi_ControlLeft  :=aControl.Left;
        tempFieldDef.Fi_ControlTop   :=aControl.Top;
        tempFieldDef.Fi_ControlWidth :=aControl.Width;
        tempFieldDef.Fi_ControlHeight:=aControl.Height;

        if FPublicSizePosition then
        begin
          ABExecSQL('Main',
           ' update ABSys_Org_Field '+
           ' set Fi_ControlLeft  ='+inttostr(aControl.Left)+ ','+
           '     Fi_ControlTop   ='+inttostr(aControl.Top) + ','+
           '     Fi_ControlWidth ='+inttostr(aControl.Width) + ','+
           '     Fi_ControlHeight ='+inttostr(aControl.Height) +
           ' where Fi_Guid='+QuotedStr(tempFieldDef.Fi_Guid));
        end
        else
        begin
          if (Assigned(tempParentForm)) and
             (tempParentForm.Name<>EmptyStr)  and
             (TABDictionaryQuery(DataSource.DataSet).FuncGuid<>EmptyStr) then
          begin
            ABSetFieldValue(GetFuncControlPropertyDataset,['FP_FU_Guid','FP_FormName','FP_ControlName','FP_PropertyName'],[TABDictionaryQuery(DataSource.DataSet).FuncGuid,tempParentForm.Name,aControl.Name,'Fi_ControlLeft'  ],['FP_PropertyValue'],[aControl.Left  ]);
            ABSetFieldValue(GetFuncControlPropertyDataset,['FP_FU_Guid','FP_FormName','FP_ControlName','FP_PropertyName'],[TABDictionaryQuery(DataSource.DataSet).FuncGuid,tempParentForm.Name,aControl.Name,'Fi_ControlTop'   ],['FP_PropertyValue'],[aControl.Top   ]);
            ABSetFieldValue(GetFuncControlPropertyDataset,['FP_FU_Guid','FP_FormName','FP_ControlName','FP_PropertyName'],[TABDictionaryQuery(DataSource.DataSet).FuncGuid,tempParentForm.Name,aControl.Name,'Fi_ControlWidth' ],['FP_PropertyValue'],[aControl.Width ]);
            ABSetFieldValue(GetFuncControlPropertyDataset,['FP_FU_Guid','FP_FormName','FP_ControlName','FP_PropertyName'],[TABDictionaryQuery(DataSource.DataSet).FuncGuid,tempParentForm.Name,aControl.Name,'Fi_ControlHeight'],['FP_PropertyValue'],[aControl.Height]);
          end;
        end;
      end;
    end;
  end;
end;

procedure TABDBPanel.SaveControls;
var
  tempCurProgress: Integer;
  i:longint;
  tempFieldDef:PABFieldDef;
  tempUpdateTableNames:Tstrings;
begin
  if (Assigned(DataSource)) then
  begin
    LinkFieldDefLabelControls;
    ABPubManualProgressBar_ThreadU.ABRunProgressBar(tempCurProgress,FDataLink.DataSet.FieldCount);
    tempUpdateTableNames:=TStringList.Create;
    try
      for i := 0 to FDataLink.DataSet.FieldCount-1 do
      begin
        tempCurProgress:=tempCurProgress+1;

        tempFieldDef:=ABFieldDefByFieldName(FDataLink.DataSet,FDataLink.DataSet.Fields[i].FieldName);
        if (Assigned(tempFieldDef))  and
           (tempFieldDef.Fi_IsInputPanelView)  and
           ((FNoHaveField=EmptyStr)   or (ABPos(','+tempFieldDef.Field.FieldName+',',','+FNoHaveField  +',')<=0)) and
           ((FHaveField=EmptyStr) or (ABPos(','+tempFieldDef.Field.FieldName+',',','+FHaveField+',')> 0))
            then
        begin
          SaveControls(tempFieldDef.Fi_Label,tempFieldDef.Fi_Name);
          SaveControls(tempFieldDef.Fi_Control,tempFieldDef.Fi_Name);
          if tempUpdateTableNames.IndexOf(tempFieldDef.Fi_Ta_Name)<0 then
            tempUpdateTableNames.Add(tempFieldDef.Fi_Ta_Name);
        end;
      end;

      for I := 0 to tempUpdateTableNames.Count-1 do
      begin
        ABReFreshQuery(ABGetConstSqlPubDataset('ABSys_Org_Field',[ABGetConnInfoByConnName(TABDictionaryQuery(FDataLink.DataSet).ConnName).Conninfo.Database,tempUpdateTableNames[i]]),
                       []);
      end;
    finally
      ABPubManualProgressBar_ThreadU.ABStopProgressBar;
      tempUpdateTableNames.Free;
    end;
  end;

  if Assigned(FOnAfterSaveControls) then
    FOnAfterSaveControls(self);
end;

procedure TABDBPanel.PopupMenu_Plan(Sender: TObject);
begin
  if TControl(sender).Name =      'PopupMenu_RefreshControls_InitPositionAndInitSize_AutoSize' then
  begin
    RefreshControls(2,2,true);
  end
  else if TControl(sender).Name = 'PopupMenu_RefreshControls_InitPositionAndDatabaseSize_AutoSize' then
  begin
    RefreshControls(2,1,true);
  end
  else if TControl(sender).Name = 'PopupMenu_RefreshControls_SetPositionAndInitSize_AutoSize' then
  begin
    RefreshControls(3,2,true);
  end
  else if TControl(sender).Name = 'PopupMenu_RefreshControls_SetPositionAndDatabaseSize_AutoSize' then
  begin
    RefreshControls(3,1,true);
  end
  else if TControl(sender).Name = 'PopupMenu_RefreshControls_DatabasePositionAndInitSize_AutoSize' then
  begin
    RefreshControls(1,2,true);
  end
  else if TControl(sender).Name = 'PopupMenu_RefreshControls_DatabasePositionAndDatabaseSize_AutoSize' then
  begin
    RefreshControls(1,1,true);
  end
  else if TControl(sender).Name = 'PopupMenu_RefreshControls_DatabasePositionAndDatabaseSize' then
  begin
    RefreshControls(1,1,False);
  end
  else if TControl(sender).Name = 'PopupMenu_SaveControlsToField' then
  begin
    PublicSizePosition:=true;
    SaveControls;
  end
  else if TControl(sender).Name = 'PopupMenu_SaveControlsToSingle' then
  begin
    PublicSizePosition:=false;
    SaveControls;
  end
  else if TControl(sender).Name = 'PopupMenu_FreeControls' then
  begin
    FreeControls;
  end
  else if TControl(sender).Name = 'PopupMenu_SetupHaveField' then
  begin
    SetupHaveField;
  end
  else if TControl(sender).Name = 'PopupMenu_SetupHaveField_HideViewInotherPanel' then
  begin
    SetupHaveField_HideViewInotherPanel;
  end
  else if TControl(sender).Name = 'PopupMenu_SetupNoHaveField' then
  begin
    SetupNoHaveField;
  end
  else if TControl(sender).Name = 'PopupMenu_SetupReadOnlyField' then
  begin
    SetupReadOnlyField;
  end
  else if TControl(sender).Name = 'PopupMenu_SetupEditField' then
  begin
    SetupEditField;
  end
  else if TControl(sender).Name = 'PopupMenu_CopySetup' then
  begin
    CopySetup;
  end
  else if TControl(sender).Name = 'PopupMenu_ClearSetupField' then
  begin
    ClearSetupField;
  end;
end;


end.

