{
框架控件属性设计单元
}
unit ABFrameworkObjectInspectorU;

interface
{$I ..\ABInclude.ini}

uses
  ABPubFuncU,
  ABPubDesignU,
  ABPubDesignAlignU,
  ABPubSelectCheckComboBoxU,
  ABPubDBU,
  ABPubFormU,

  ABThirdFormU,

  ABFrameworkFuncU,
  ABFrameworkDictionaryQueryU,

  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, Math,
  Dialogs, ComCtrls, ExtCtrls,ActiveX,Types,TypInfo, ImgList, ToolWin,

  DB,
  cxOI, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  cxEdit, cxInplaceContainer, cxVGrid, Vcl.StdCtrls;


type
  TAlignSizeStyle = (
    asAlignLeft, asAlignRight,asAlignTop, asAlignBottom,
    asAlignHCenter,asAlignVCenter,
    asSpaceEquH, asSpaceEquHX, asSpaceIncH, asSpaceDecH, asSpaceRemoveH,
    asSpaceEquV, asSpaceEquVY, asSpaceIncV, asSpaceDecV, asSpaceRemoveV,
    asIncWidth, asDecWidth , asMakeMaxWidth,asMakeMinWidth,asMakeSameWidth,
    asIncHeight, asDecHeight,asMakeMaxHeight,asMakeMinHeight, asMakeSameHeight,
    asMakeSameSize,
    asParentHCenter, asParentVCenter,asSelect);


type
  TABObjectInspectorForm = class(TABPubForm)
    CheckBox1: TCheckBox;
    Panel1: TPanel;
    cxRTTIInspector1: TcxRTTIInspector;
    procedure cxRTTIInspector1EditValueChanged(Sender: TObject;
      ARowProperties: TcxCustomEditorRowProperties);
    procedure cxRTTIInspector1FilterProperty(Sender: TObject;
      const PropertyName: string; var Accept: Boolean);
  protected
    procedure DoClose(var Action: TCloseAction); override;
    { Private declarations }
  public
    { Public declarations }
  end;

//显示对象的属性窗体
procedure ABShowObjectInspector(aForm:TCustomForm);
//关闭对象的属性窗体
procedure ABCloseObjectInspector;
//设置需设计的对象
procedure ABSetObjectInspectorObject(aInspectedObject:TControl);
//刷新对象的属性值
procedure ABRefreshObjectInspectorProperty;
//刷新对象的属性窗体的位置
procedure ABRefreshObjectInspectorPosition(aForm:TCustomForm);
//取得对象的属性编辑窗体
function ABGetObjectInspectorForm:TABObjectInspectorForm;

implementation
{$R *.dfm}
var
  FObjectInspectorForm: TABObjectInspectorForm;

function ABGetObjectInspectorForm:TABObjectInspectorForm;
begin
  if not Assigned(FObjectInspectorForm) then
    FObjectInspectorForm := TABObjectInspectorForm.Create(nil);

  Result:= FObjectInspectorForm;
end;

procedure ABShowObjectInspector(aForm:TCustomForm);
begin
  if not ABGetObjectInspectorForm.Showing then
    ABGetObjectInspectorForm.Show;

  ABRefreshObjectInspectorPosition(aForm);
end;

procedure ABCloseObjectInspector;
begin
  if ABGetObjectInspectorForm.Showing then
    ABGetObjectInspectorForm.Hide;
end;

procedure ABSetObjectInspectorObject(aInspectedObject:TControl);
begin
  if ABGetObjectInspectorForm.Showing then
    ABGetObjectInspectorForm.cxRTTIInspector1.InspectedObject:=aInspectedObject;
end;

procedure ABRefreshObjectInspectorProperty;
begin
  if ABGetObjectInspectorForm.Showing then
    ABGetObjectInspectorForm.cxRTTIInspector1.RefreshInspectedProperties;
end;

procedure ABRefreshObjectInspectorPosition(aForm:TCustomForm);
begin
  if ABGetObjectInspectorForm.Showing then
  begin
    if ABGetObjectInspectorForm.Height<aForm.Height then
      ABGetObjectInspectorForm.Height:=aForm.Height;

    ABGetObjectInspectorForm.left:=aForm.ClientOrigin.x+aForm.Width;
    ABGetObjectInspectorForm.top:= aForm.ClientOrigin.y;
    if Assigned(Application.MainForm) then
    begin
      ABGetObjectInspectorForm.top:= ABGetObjectInspectorForm.top-(Application.MainForm.Height-Application.MainForm.ClientHeight);
    end;
  end;
end;

{ TABObjectInspectorForm }

procedure TABObjectInspectorForm.cxRTTIInspector1EditValueChanged(Sender: TObject; ARowProperties: TcxCustomEditorRowProperties);
  procedure GetPropertyLongNameAndValue(var aPropertyName,aPropertyValue:string;aRow:TcxCustomRow);
  begin
    aPropertyName:=TcxEditorRow(aRow).Properties.Caption;
    aPropertyValue:=TcxEditorRow(aRow).Properties.Value;
    while Assigned(aRow.Parent) do
    begin
      aRow:=aRow.Parent;
      aPropertyName:=TcxEditorRow(aRow).Properties.Caption+'.'+aPropertyName;
    end;
  end;
var
  tempFuncControlPropertyDataset:TDataset;
  tempForm:TCustomForm;
  tempComponent:TComponent;

  tempAutoLoad,
  tempFuGuid,
  tempPropertyValue,
  tempPropertyName:string;

  tempRow:TcxCustomRow;
  i:LongInt;
begin
  if (Assigned(ABPubDesignerHook)) and
     (Assigned(ABPubDesignerHook.Form)) and
     (ABPubDesignerHook.Form is TABThirdForm) and
     (ABGetObjectInspectorForm.Showing)  then
  begin
    tempForm:=ABPubDesignerHook.Form;
    tempFuGuid:=ABGetFuncFieldValueByFuncFileName(ABGetBPLFileName(tempForm),'Fu_Guid');
    if (tempFuGuid<>EmptyStr) then
    begin
      tempComponent:=TComponent(ABGetObjectInspectorForm.cxRTTIInspector1.InspectedObject);
      tempFuncControlPropertyDataset:= ABGetConstSqlPubDataset('ABSys_Org_FuncControlProperty',
                                       [tempFuGuid,
                                       tempForm.Name],TABInsideQuery);
      if (Assigned(tempFuncControlPropertyDataset))  then
      begin
        tempRow:= ABGetObjectInspectorForm.cxRTTIInspector1.FocusedRow;
        if (Assigned(tempRow)) then
        begin
          //集合属性的单独处理
          if (Assigned(tempRow.Parent)) then
          begin
            GetPropertyLongNameAndValue(tempPropertyName,tempPropertyValue,tempRow.Parent);
            if not (ABPropIsType(tempComponent,tempPropertyName,tkSet)) then
            begin
              GetPropertyLongNameAndValue(tempPropertyName,tempPropertyValue,tempRow);
            end;
          end
          else
          begin
            tempPropertyName:=TcxEditorRow(tempRow).Properties.Caption;
            tempPropertyValue:=TcxEditorRow(tempRow).Properties.Value;
          end;

          //当涉及到多个控件时修改多个控件的属性值
          for I := 0 to ABPubDesignerHook.ControlCount - 1 do
          begin
            if ABPubDesignerHook.Controls[i]<>tempComponent then
            begin
              tempComponent:=ABPubDesignerHook.Controls[i];
              ABSetPropValue(tempComponent,tempPropertyName,tempPropertyValue);
            end;
            if CheckBox1.Checked then
            begin
              tempAutoLoad:=  ABGetFieldValue(tempFuncControlPropertyDataset,
                                      ['FP_ControlName','FP_PropertyName'],
                                      [tempComponent.Name,tempPropertyName],
                                      ['FP_LoadKey'],'');
              if ABSelectCheckComboBox(tempAutoLoad,
                                       ABGetConstSqlPubDataset('ABSys_Org_Key',['']),
                                       'Ke_Name','Ke_Name','Ke_Guid','启用控件角色(空=所有)',tempComponent.Name) then
              begin
                ABSetFieldValue(tempFuncControlPropertyDataset,
                                ['FP_FU_Guid','FP_FormName','FP_ControlName','FP_PropertyName'],
                                [tempFuGuid,tempForm.name,tempComponent.Name,tempPropertyName],
                                ['FP_PropertyValue','FP_AutoLoad','FP_LoadKey'],
                                [tempPropertyValue,true,tempAutoLoad]
                                  );
              end;
            end;
          end;
        end;
      end;
    end;
    ABPubDesignerHook.ShowGrabHandle(True);
  end;
end;

procedure TABObjectInspectorForm.cxRTTIInspector1FilterProperty(Sender: TObject;
  const PropertyName: string; var Accept: Boolean);
begin
  if (Assigned(ABPubDesignerHook)) and
     (Assigned(ABPubDesignerHook.Form)) and
     (ABPubDesignerHook.Form is TABThirdForm) and
     (ABGetObjectInspectorForm.Showing) and
     (ABPubDesignerHook.ControlCount>1) then
  begin
    if (AnsiCompareText(PropertyName,'Tag')<>0) and
       (AnsiCompareText(PropertyName,'left')<>0) and
       (AnsiCompareText(PropertyName,'top')<>0) and
       (AnsiCompareText(PropertyName,'width')<>0) and
       (AnsiCompareText(PropertyName,'Height')<>0) and
       (AnsiCompareText(PropertyName,'Enabled')<>0) and
       (AnsiCompareText(PropertyName,'Visible')<>0) and
       (AnsiCompareText(PropertyName,'anchors')<>0) and
       (AnsiCompareText(PropertyName,'akleft')<>0) and
       (AnsiCompareText(PropertyName,'aktop')<>0) and
       (AnsiCompareText(PropertyName,'akright')<>0) and
       (AnsiCompareText(PropertyName,'akbottom')<>0) and
       (AnsiCompareText(PropertyName,'align')<>0) and
       (AnsiCompareText(PropertyName,'tag')<>0)
        then
      Accept:=false;
  end;
end;

procedure TABObjectInspectorForm.DoClose(var Action: TCloseAction);
begin
  Action:=caHide;
  inherited;
end;

procedure ABFinalization;
begin
  if Assigned(FObjectInspectorForm) then
    FObjectInspectorForm.Free;
end;


Initialization

Finalization
  ABFinalization;



end.





