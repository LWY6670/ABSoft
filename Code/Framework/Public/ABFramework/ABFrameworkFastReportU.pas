{
框架报表单元
使用方式
  ABPubFrxReport.Init;
  ABPubFrxReport.CreateOwner:= self;
  ABPubFrxReport.InputParamParent:= Panel8;
  ABPubFrxReport.ReportType:= rtExtend;
  ABPubFrxReport.DesignReport/PrintReport/PreviewReport;

//frxDesgn 单元的引用不能少，否则会出现打开不了报表设计窗体的问题
//dxSkinController1有皮肤打开时,FastReport不能看到设计界面,关闭皮肤就正常了
//所以 dxSkinGetControllerClassForWindow 方法中
// 排除AControl.Owner.ClassName='TfrxDesignerForm',以便dxSkinController1有皮肤打开时,也能看到FastReport的设计界面

//系统参数修改图标时要重新启动系统才能看到新的图标
//第一次加载报表时看不到参数是因为参数是在加载报表时加入的,要重新加载一下报表才行
//必须引用单元frxDesgn，否则不能显示报表设计界面
}
unit ABFrameworkFastReportU;

interface
{$I ..\ABInclude.ini}

uses
  ABPubConstU,
  ABPubVarU,
  ABPubFuncU,
  ABPubUserU,
  ABPubDBU,
  ABPubLocalParamsU,

  ABThirdFuncU,
  ABThirdConnU,
  ABThirdQueryU,
  ABThirdCustomQueryU,

  ABFrameworkVarU,
  ABFrameworkFuncU,
  ABFrameworkControlU,

  printers,
  cxLabel,
  SysUtils,Variants,Classes,Graphics,Controls,ComCtrls,DB,TypInfo,

  frxDesgn,frxClass,
  frxVariables,frxDBSet,frxExportXML,frxExportXLS,frxExportImage,
  frxExportText,frxExportRTF,frxExportPDF,frxExportODF,frxExportMail,frxExportHTML,
  frxExportCSV;

type
  TABReportType=(rtExtend,rtFunc);

  TABFrxReport            = class(TfrxReport)
  private
    FOldBeforeScroll:TDataSetNotifyEvent;
    FOldAfterScroll:TDataSetNotifyEvent;
    FAddDataSourceDatasets:array of TDataset;
    FExtVariable:Tstrings;
    FDataSource: TDataSource;
    FReportType: TABReportType;
    FInputParamParent: TWinControl;
    FQuoteParamParent: TWinControl;
    FCreateOwner: TComponent;
    FDefaultPrint: string;

    function UserFunction(const MethodName: string; var Params: Variant): Variant;
    procedure LoadImageVariable;
    procedure CreateQueryVariable;
    procedure CreateSysVariable;
    procedure CreateUserFunc;
    function SetFromTableAndField(var aTableName, aFormatFieldName,aGuidFieldName: string): Boolean;

    //根据当前的DataSource层次初始化TfrxReport的数据集
    procedure InitfrxReportDataSets(aPrintRange:TABPrintRange);
    procedure CreateExtVariable;
    { Private declarations }
  protected
    procedure Notification(AComponent: TComponent;Operation: TOperation); override;
  public
    FExtendDatasets:array of TDataset;
    FExtendDataSources:array of TDataSource;
    //增加定制的报表
    function AddCustomReport(aFu_Guid, aGroupName, aName: string): string;
    //创建扩展报表的数据集与数据源
    function CreateRes(aGuid: string): boolean;
    //释放扩展报表的数据集与数据源
    procedure FreeCreateRes;

    procedure LoadFromStream(Stream: TStream); override;
    procedure Clear; override;
    procedure DefineProperties(Filer: TFiler); override;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  public
    //调用报表功能前进行的初始化
    procedure Init;

    //设计报表
    procedure DesignReport(aGuid:string;aPrintRange:TABPrintRange=prAll);
    //打印报表
    procedure PrintReport(aGuid:string;aPrintRange:TABPrintRange=prAll;aShowDialog:Boolean=true);
    //预览报表
    procedure PreviewReport(aGuid:string;aPrintRange:TABPrintRange=prAll);

    //创建报表第一级SQL输入参数的控件
    procedure CreateExtendReportInputControl(aGuid:string;aInputControlParent:TWinControl);

    //增加扩展的数据集
    procedure ADDExtDatasets(aExtDatasets:array of TDataset);

    //增加扩展的变量
    procedure ADDExtVariable(aVarName,aVarValue:string);

    { Public declarations }
  published
    //报表查询过程中创建对象的所有者,一般为窗体对象
    property CreateOwner:TComponent read FCreateOwner  write FCreateOwner;
    //报表类型（属于扩展报表或者功能模块下的报表）
    property ReportType:TABReportType read FReportType  write FReportType;
    //报表的数据源
    property DataSource:TDataSource read FDataSource  write FDataSource;

    //报表的私有输入参数容器,打开报表时在此容器内创建控件，查询报表时根据创建的控件创建报表参数
    property InputParamParent:TWinControl read FInputParamParent  write FInputParamParent;
    //报表的引用输入参数容器,打开报表时不在此容器内创建控件，查询报表时根据引用的控件创建报表参数
    property QuoteParamParent:TWinControl read FQuoteParamParent  write FQuoteParamParent;

    //打印时默认的打印机
    property DefaultPrint: string read FDefaultPrint write FDefaultPrint ;
    { Published declarations }
  end;                                              

var
  ABPubFrxReport:TABFrxReport;

implementation

var
  FPubReportFileName :string;

  frxBMPExport1: TfrxBMPExport;
  frxCSVExport1: TfrxCSVExport;
  frxGIFExport1: TfrxGIFExport;
  frxHTMLExport1: TfrxHTMLExport;
  frxJPEGExport1: TfrxJPEGExport;
  frxMailExport1: TfrxMailExport;
  frxODSExport1: TfrxODSExport;
  frxODTExport1: TfrxODTExport;
  frxPDFExport1: TfrxPDFExport;
  frxRTFExport1: TfrxRTFExport;
  frxSimpleTextExport1: TfrxSimpleTextExport;
  frxTIFFExport1: TfrxTIFFExport;
  //frxTXTExport1: TfrxTXTExport;
  frxXLSExport1: TfrxXLSExport;
  frxXMLExport1: TfrxXMLExport;


{ TABFrxReport }

procedure TABFrxReport.InitfrxReportDataSets(aPrintRange:TABPrintRange);
  function DoCreatefrxDBDataset(aDataSet:TDataSet):TfrxDBDataset;
  var
    i:LongInt;
  begin
    Result:=nil;
    if aDataSet.Active then
    begin
      Result:=TfrxDBDataset(frxFindDataSet(nil,aDataSet.Name,FCreateOwner));
      if not Assigned(Result) then
      begin
        Result:=TfrxDBDataset.Create(FCreateOwner);
        DataSets.Add(Result);
        Result.DataSet :=aDataSet;
        Result.username :=aDataSet.Name;

        Result.FieldAliases.Clear;
        for I := 0 to aDataSet.FieldCount-1 do
        begin
          Result.FieldAliases.Add(aDataSet.Fields[i].FieldName+'='+aDataSet.Fields[i].DisplayLabel);
        end;
      end;
    end;
  end;

  procedure InitDataSet(aDataSet:TDataSet;aDetailOraParent:longint;aTop:boolean);
  var
    i:LongInt;
    tempList:TList;
    tempfrxDBDataset:TfrxDBDataset;
  begin
    if not Assigned(aDataSet)  then
      exit;

    tempfrxDBDataset:=DoCreatefrxDBDataset(aDataSet);

    if aTop then
    begin
      if Assigned(tempfrxDBDataset) then
      begin
        if (aPrintRange=prAll) then
        begin
          tempfrxDBDataset.RangeBegin:=rbFirst;
          tempfrxDBDataset.RangeEnd:=reLast;
        end
        else if (aPrintRange=prCur) then
        begin
          tempfrxDBDataset.RangeBegin:=rbCurrent;
          tempfrxDBDataset.RangeEnd:=reCurrent;
        end;
      end;
    end;

    if aDetailOraParent=1 then
    begin
      tempList := TList.Create;
      try
        aDataSet.GetDetailDataSets(tempList);
        for I := 0 to tempList.Count - 1 do
        begin
          InitDataSet(TDataSet(tempList.Items[i]),aDetailOraParent,false);
        end;
      finally
        tempList.Free;
      end;
    end
    else if aDetailOraParent=2 then
    begin
      if (Assigned(aDataSet.DataSource)) and
         (Assigned(aDataSet.DataSource.DataSet)) then
        InitDataSet(aDataSet.DataSource.DataSet,aDetailOraParent,false);
    end;
  end;
var
  i:LongInt;
begin
  if not Assigned(FDataSource)  then
    exit;

  InitDataSet(FDataSource.DataSet,1,true);
  if (Assigned(FDataSource.DataSet)) and
     (Assigned(FDataSource.DataSet.DataSource)) and
     (Assigned(FDataSource.DataSet.DataSource.DataSet)) then
    InitDataSet(FDataSource.DataSet.DataSource.DataSet,2,False);

  for I := Low(FAddDataSourceDatasets) to High(FAddDataSourceDatasets) do
  begin
    DoCreatefrxDBDataset(FAddDataSourceDatasets[i]);
  end;
end;

procedure TABFrxReport.Init;
begin
  FreeCreateRes;

  FDefaultPrint:=emptystr;
  Preview:=nil;
  FCreateOwner:= nil;
  FDataSource:= nil;
  FInputParamParent:= nil;
  FQuoteParamParent:= nil;

end;

procedure TABFrxReport.ADDExtDatasets(aExtDatasets:array of TDataset);
var
  i:LongInt;
begin
  SetLength(FAddDataSourceDatasets,High(aExtDatasets)+1);
  for I := Low(aExtDatasets) to High(aExtDatasets) do
  begin
    FAddDataSourceDatasets[i]:= aExtDatasets[i];
  end;
end;

procedure TABFrxReport.FreeCreateRes;
var
  i:LongInt;
begin
  FDataSource:=nil;

  for I := Low(FExtendDataSources) to High(FExtendDataSources) do
  begin
    if Assigned(FExtendDataSources[i]) then
      FreeAndNil(FExtendDataSources[i]);
  end;

  for I := Low(FExtendDatasets) to High(FExtendDatasets) do
  begin
    if Assigned(FExtendDatasets[i]) then
      FreeAndNil(FExtendDatasets[i]);
  end;
  SetLength(FExtendDatasets,0);
  SetLength(FExtendDataSources,0);

  SetLength(FAddDataSourceDatasets,0);

  for I := DataSets.Count-1 downto 0 do
  begin
    DataSets[i].Free;
  end;
  DataSets.Clear;

  FExtVariable.Clear;
end;

procedure TABFrxReport.CreateQueryVariable;
var
  temParamCount:longint;
  procedure DoCreateQueryVariable(aWinControl:TWinControl);
  var
    tempControl:TWinControl;
    i:longint;
    tempValue:Variant;
  begin
    for i :=  0 to aWinControl.ControlCount-1 do
    begin
      tempControl:=TWinControl(aWinControl.Controls[i]);
      if (Assigned(tempControl))  and
         (not (tempControl is TcxLabel)) then
      begin
        tempValue:=ABGetControlValue(tempControl);
        temParamCount:=temParamCount+1;
        Variables.AddVariable('ABQueryVariable',tempControl.ClassName+'_'+inttostr(temParamCount),QuotedStr(VarToStrDef(tempValue,'')));
      end;
    end;
  end;
begin
  if Not (FReportType=rtExtend) then
    exit;

  //增加变量组,注意前面是空格
  Variables.DeleteCategory('ABQueryVariable');
  Variables.Add.Name:=' ABQueryVariable';
  temParamCount:=0;
  if Assigned(FQuoteParamParent) then
    DoCreateQueryVariable(FQuoteParamParent);
  if Assigned(FInputParamParent) then
    DoCreateQueryVariable(FInputParamParent);
end;

procedure TABFrxReport.CreateSysVariable;
var
  tempName:string;
  tempValue:string;
  tempDataset:TDataSet;
begin
  //增加变量组,注意前面是空格
  Variables.DeleteCategory('ABSysVariable');
  Variables.Add.Name:=' ABSysVariable';

  tempDataset:=ABGetConstSqlPubDataset('ABSys_Org_Parameter',[]);
  tempDataset.First;
  while not tempDataset.Eof do
  begin
    tempName := tempDataset.FindField('Pa_Name').AsString;
    tempValue := tempDataset.FindField('Pa_Value').AsString;
    //增加变量项目,这里不需要空格'
    tempValue:=QuotedStr(tempValue);
    Variables.AddVariable('ABSysVariable','ABParams_'+tempName,tempValue);

    tempDataset.Next;
  end;

  Variables.AddVariable('ABSysVariable','ABUser_Guid',QuotedStr(ABPubUser.Guid));
  Variables.AddVariable('ABSysVariable','ABUser_Code',QuotedStr(ABPubUser.Code));
  Variables.AddVariable('ABSysVariable','ABUser_Name',QuotedStr(ABPubUser.Name));
  Variables.AddVariable('ABSysVariable','ABUser_PassWord',QuotedStr(ABPubUser.PassWord));
  Variables.AddVariable('ABSysVariable','ABUser_EncryptPassWord',QuotedStr(ABPubUser.EncryptPassWord));
  Variables.AddVariable('ABSysVariable','ABUser_FuncRight',QuotedStr(ABPubUser.FuncRight));
  Variables.AddVariable('ABSysVariable','ABUser_CustomFunc',QuotedStr(ABPubUser.CustomFunc));

  Variables.AddVariable('ABSysVariable','ABUser_UseBeginDateTime',QuotedStr(ABDateTimeToStr(ABPubUser.UseBeginDateTime)));
  Variables.AddVariable('ABSysVariable','ABUser_UseEndDateTime',QuotedStr(ABDateTimeToStr(ABPubUser.UseEndDateTime)));
  Variables.AddVariable('ABSysVariable','ABUser_LicenseBeginDateTime',QuotedStr(ABDateTimeToStr(ABPubUser.LicenseBeginDateTime)));
  Variables.AddVariable('ABSysVariable','ABUser_LicenseEndDateTime',QuotedStr(ABDateTimeToStr(ABPubUser.LicenseEndDateTime)));
  Variables.AddVariable('ABSysVariable','ABUser_PassWordEndDateTime',QuotedStr(ABDateTimeToStr(ABPubUser.PassWordEndDateTime)));

  Variables.AddVariable('ABSysVariable','ABUser_LoginDateTime',QuotedStr(ABDateTimeToStr(ABPubUser.LoginDateTime)));
  Variables.AddVariable('ABSysVariable','ABUser_LoginOutDateTime',QuotedStr(ABDateTimeToStr(ABPubUser.LoginOutDateTime)));
  Variables.AddVariable('ABSysVariable','ABUser_SysLanguage',QuotedStr(ABGetSysLanguageStr(ABGetSysLanguage)));
  Variables.AddVariable('ABSysVariable','ABUser_SysVersion',QuotedStr(ABGetSysVersion));
  Variables.AddVariable('ABSysVariable','ABUser_HostName',QuotedStr(ABPubUser.HostName));
  Variables.AddVariable('ABSysVariable','ABUser_HostIP',QuotedStr(ABPubUser.HostIP));
  Variables.AddVariable('ABSysVariable','ABUser_MacAddress',QuotedStr(ABPubUser.MacAddress));
  Variables.AddVariable('ABSysVariable','ABUser_BroadIP',QuotedStr(ABPubUser.BroadIP));
  Variables.AddVariable('ABSysVariable','ABUser_LoginType',QuotedStr(GetEnumName(TypeInfo(TABLoginType), Ord(ABLocalParams.LoginType))));
  Variables.AddVariable('ABSysVariable','ABUser_VarGuid',QuotedStr(ABPubUser.VarGuid));
  Variables.AddVariable('ABSysVariable','ABUser_VarDatetime',QuotedStr(ABDateTimeToStr(ABPubUser.VarDatetime)));
end;

procedure TABFrxReport.CreateExtVariable;
var
  I: Integer;
begin
  //增加变量组,注意前面是空格
  Variables.DeleteCategory('ABExtVariable');
  Variables.Add.Name:=' ABExtVariable';
  for I := 0 to FExtVariable.count-1 do
  begin
    Variables.AddVariable('ABExtVariable',FExtVariable.Names[i],FExtVariable.ValueFromIndex[i]);
  end;
end;

procedure TABFrxReport.LoadFromStream(Stream: TStream);
var
  I:Integer;
  tempfrxComponent:TfrxComponent;
begin
  inherited;
  Variables.BeginUpdate;
  try
    CreateSysVariable;
    LoadImageVariable;
    CreateQueryVariable;
    CreateExtVariable;
  finally
    Variables.EndUpdate;
  end;

  //因为例如导出到PDF等可能会出现汉字的乱码,所以此处将所有的显示修改成宋体以使得导出的汉字不是乱码
  for I:=0 to AllObjects.Count-1 do
  begin
    tempfrxComponent :=AllObjects.Items[i];
    if (tempfrxComponent is TFrxMemoView) and
       (AnsiCompareText(TFrxMemoView(tempfrxComponent).Font.Name,'宋体')<>0) then
    begin
      TFrxMemoView(tempfrxComponent).Font.Name:='宋体';
    end;
  end;
end;

procedure TABFrxReport.LoadImageVariable;
var
  i:LongInt;
  tempComponent:TfrxComponent;
  tempDataset:TDataSet;
  tempMemoryStream:TMemoryStream;
begin
  tempMemoryStream:=TMemoryStream.Create;
  try
    tempDataset:=ABGetConstSqlPubDataset('ABSys_Org_Parameter',[]);
    while not tempDataset.Eof do
    begin
      if (not tempDataset.FindField('Pa_ExpandValue').IsNull)  then
      begin
        for I := 0 to 10 do
        begin
          if i=0 then
            tempComponent:= FindObject('ABParams_'+tempDataset.FindField('Pa_Name').asstring)
          else
            tempComponent:= FindObject('ABParams_'+tempDataset.FindField('Pa_Name').asstring+inttostr(i));

          if Assigned(tempComponent) then
          begin
            if tempComponent is TfrxPictureView then
            begin
              TBlobField(tempDataset.FindField('Pa_ExpandValue')).SaveToStream(tempMemoryStream);
              TfrxPictureView(tempComponent).Stretched:=true;
              TfrxPictureView(tempComponent).LoadPictureFromStream(tempMemoryStream);
            end;
          end;
        end;
      end;

      tempDataset.Next;
    end;
  finally
    tempMemoryStream.Free;
  end;
end;

function TABFrxReport.CreateRes(aGuid:string):boolean;
var
  tempDataset:TDataset;
  tempIndex:LongInt;
  tempTreeView:TTreeview;
  procedure DoCreateRes(aTreeNode:TTreeNode;aMasterSource:TDataSource);
  var
    tempSql1:string;
    tempBoolean:boolean;
    tempControl:TControl;
    tempMainSource:TDataSource;

    i:LongInt;
  begin
    if (Assigned(aTreeNode)) then
    begin
      ABSetDataSetRecno(tempDataset,LongInt(aTreeNode.Data));
      tempSql1:=ABDownTotextFromField(
                       'Main',
                       'ABSys_Org_ExtendReportDatasets','Ed_SQL',
                       'Ed_Guid='+QuotedStr(tempDataset.FindField('Ed_Guid').AsString)
                       );
      tempSql1:=ABReplaceInputParamsValue(tempBoolean,tempControl,tempSql1,FInputParamParent,FQuoteParamParent,true);
      if (tempSql1<>EmptyStr) and
         (tempSql1<>QuotedStr(EmptyStr)) then
      begin
        FExtendDatasets[tempIndex]:=TABThirdReadDataQuery.Create(nil);
        FExtendDatasets[tempIndex].Name:='tempCreateDatasets_'+inttostr(tempIndex);
        TABThirdReadDataQuery(FExtendDatasets[tempIndex]).ConnName:=ABGetConnNameByConnGuid(tempDataset.FindField('ED_ConnName').AsString);
        TABThirdReadDataQuery(FExtendDatasets[tempIndex]).Sql.Text:=ABExecSQLExecPart(TABThirdReadDataQuery(FExtendDatasets[tempIndex]).ConnName,tempSql1,[]);
        TABThirdReadDataQuery(FExtendDatasets[tempIndex]).MasterSource:=aMasterSource;
        TABThirdReadDataQuery(FExtendDatasets[tempIndex]).MasterFields:=ABStringReplace(tempDataset.FindField('Ed_MainFields').AsString,',',';');
        TABThirdReadDataQuery(FExtendDatasets[tempIndex]).DetailFields:=ABStringReplace(tempDataset.FindField('Ed_DetailFields').AsString,',',';');
        FExtendDatasets[tempIndex].Open;

        tempMainSource:=TDataSource.Create(nil);
        FExtendDataSources[tempIndex]:=tempMainSource;
        FExtendDataSources[tempIndex].AutoEdit:=False;
        FExtendDataSources[tempIndex].Name:='tempCreateDataSources_'+inttostr(tempIndex);
        FExtendDataSources[tempIndex].DataSet:=FExtendDatasets[tempIndex];
        tempIndex:=tempIndex+1;

        for I := 0 to aTreeNode.Count - 1 do
        begin
          DoCreateRes(aTreeNode.Item[i],tempMainSource);
        end;
      end;
    end;
  end;
begin
  result:=false;
  tempDataset:=ABGetConstSqlPubDataset('ABSys_Org_ExtendReportDatasets',[aGuid]);
  if not ABDatasetIsEmpty(tempDataset) then
  begin
    tempTreeView:=TTreeview.Create(FCreateOwner);
    tempTreeView.Visible:=false;
    tempTreeView.Parent:=TWinControl(FCreateOwner);
    tempTreeView.Visible:=false;
    try
      SetLength(FExtendDatasets,tempDataset.RecordCount);
      SetLength(FExtendDataSources,tempDataset.RecordCount);
      ABDataSetsToTree
      (
       tempTreeView.Items,
       tempDataset,
       'Ed_ParentGuid',
       'Ed_Guid',
       'Ed_Name'
       );
      if (Assigned(tempTreeView.TopItem)) then
      begin
        tempIndex:=0;
        DoCreateRes(tempTreeView.TopItem,nil);
      end;
      result:=True;
    finally
      tempTreeView.Free;
    end;
  end;
end;

function TABFrxReport.SetFromTableAndField(var aTableName,aFormatFieldName,aGuidFieldName:string):Boolean;
begin
  result:=true;
  if FReportType=rtExtend then
  begin
    aTableName:= 'ABSys_Org_ExtendReport';
    aFormatFieldName:= 'Er_Format';
    aGuidFieldName:= 'Er_Guid';
  end
  else if FReportType=rtFunc then
  begin
    aTableName:= 'ABSys_Org_FuncReport';
    aFormatFieldName:= 'Fr_Format';
    aGuidFieldName:= 'Fr_Guid';
  end
  else
  begin
    result:=False;
  end;
end;

procedure TABFrxReport.DesignReport(aGuid:string;aPrintRange:TABPrintRange);
var
  tempLastChange:TDateTime;
  tempTableName,
  tempFileFieldName,
  tempGuidFieldName:string;
  tempPubReportFileName :string;
begin
  if aGuid=EmptyStr then
    exit;

  SetFromTableAndField(tempTableName,tempFileFieldName,tempGuidFieldName);
  if FReportType=rtExtend then
  begin
    if not Assigned(FDataSource) then
    begin
      if not CreateRes(aGuid) then
        exit;

      FDataSource:=FExtendDataSources[0];
    end;
  end;

  tempPubReportFileName:=FPubReportFileName;
  ABDownToFileFromField('Main',tempTableName,tempFileFieldName,
                 'where '+tempGuidFieldName+'='+QuotedStr(aGuid),
                 tempPubReportFileName);

  if ( not ABCheckFileExists(tempPubReportFileName)) and
     ( ABCheckFileExists(ABDefaultUserPath+'DefaultReport.fr3'))
      then
  begin
    tempPubReportFileName:=ABDefaultUserPath+'DefaultReport.fr3';
  end;

  InitfrxReportDataSets(aPrintRange);

  LoadFromFile(tempPubReportFileName);
  FileName:=tempPubReportFileName;
  PrintOptions.Printer:=ABGetDefaultPrinterName(DefaultPrint);
  tempLastChange:= ReportOptions.LastChange;

  ABBackAndStopDatasetEvent(FDataSource.DataSet,FOldBeforeScroll,FOldAfterScroll);
  try
    inherited DesignReport;
  finally
    ABUnBackDatasetEvent(FDataSource.DataSet,FOldBeforeScroll,FOldAfterScroll);
  end;

  if ReportOptions.LastChange<>tempLastChange then
  begin
    ABUpFileToField('Main',tempTableName,tempFileFieldName,
                 'where '+tempGuidFieldName+'='+QuotedStr(aGuid),
                 FileName);
  end;
end;

function TABFrxReport.AddCustomReport(aFu_Guid,aGroupName,aName:string):string;
var
  tempGuid:string;
begin
  result:= ABGetSQLValue( 'AIC',
                     ' select FR_Guid'+
                     ' from ABSys_Org_FuncReport '+
                     ' where FR_FU_Guid='+QuotedStr(aFu_Guid)+' and '+
                     '       FR_GroupName='+QuotedStr(aGroupName)+' and '+
                     '       FR_Name='+QuotedStr(aName),
                     [],'');
  if result=emptystr then
  begin
    tempGuid:=ABGetGuid;
    ABExecSQL('AIC',
              ' insert into ABSys_Org_FuncReport(FR_FU_Guid,FR_Guid,FR_GroupName,FR_Name,PubID) '+
              ' values('+QuotedStr(aFu_Guid)+','+
              '        '+QuotedStr(tempGuid)+','+
              '        '+QuotedStr(aGroupName)+','+
              '        '+QuotedStr(aName)+','+
              '        '+QuotedStr(aFu_Guid)+','+
              '         )',
              []);
    result:=tempGuid;
  end;
end;

procedure TABFrxReport.PreviewReport(aGuid:string;aPrintRange:TABPrintRange);
var
  tempTableName,
  tempFileFieldName,
  tempGuidFieldName:string;
begin
  if aGuid=EmptyStr then
    exit;

  SetFromTableAndField(tempTableName,tempFileFieldName,tempGuidFieldName);
  if FReportType=rtExtend then
  begin
    if not Assigned(FDataSource) then
    begin
      if not CreateRes(aGuid) then
        exit;

      FDataSource:=FExtendDataSources[0];
    end;
  end;

  ABDownToFileFromField('Main',tempTableName,tempFileFieldName,
                 'where '+tempGuidFieldName+'='+QuotedStr(aGuid),
                 FPubReportFileName);
  InitfrxReportDataSets(aPrintRange);
  LoadFromFile(FPubReportFileName);
  FileName:=FPubReportFileName;
  PrintOptions.Printer:=ABGetDefaultPrinterName(DefaultPrint);
  ABBackAndStopDatasetEvent(FDataSource.DataSet,FOldBeforeScroll,FOldAfterScroll);
  try
    ShowReport;
  finally
    ABUnBackDatasetEvent(FDataSource.DataSet,FOldBeforeScroll,FOldAfterScroll);
  end;
end;

procedure TABFrxReport.PrintReport(aGuid:string;aPrintRange:TABPrintRange;aShowDialog:Boolean);
var
  tempTableName,
  tempFileFieldName,
  tempGuidFieldName:string;
begin
  if aGuid=EmptyStr then
    exit;

  SetFromTableAndField(tempTableName,tempFileFieldName,tempGuidFieldName);
  if FReportType=rtExtend then
  begin
    if not Assigned(FDataSource) then
    begin
      if not CreateRes(aGuid) then
        exit;
      FDataSource:=FExtendDataSources[0];
    end;
  end;

  ABDownToFileFromField('Main',tempTableName,tempFileFieldName,
                 'where '+tempGuidFieldName+'='+QuotedStr(aGuid),
                 FPubReportFileName);
  InitfrxReportDataSets(aPrintRange);
  LoadFromFile(FPubReportFileName);
  FileName:=FPubReportFileName;
  PrintOptions.Printer:=ABGetDefaultPrinterName(DefaultPrint);

  PrintOptions.ShowDialog:=aShowDialog;
  ABBackAndStopDatasetEvent(FDataSource.DataSet,FOldBeforeScroll,FOldAfterScroll);
  try
    PrepareReport;
    Print;
  finally
    ABUnBackDatasetEvent(FDataSource.DataSet,FOldBeforeScroll,FOldAfterScroll);
  end;
end;

procedure TABFrxReport.ADDExtVariable(aVarName, aVarValue: string);
var
  i:LongInt;
begin
  i:=FExtVariable.IndexOfName(aVarName);
  if i>=0 then
  begin
    FExtVariable.ValueFromIndex[i]:=aVarValue;
  end
  else
  begin
    FExtVariable.Add(aVarName+'='+aVarValue);
  end;
end;

procedure TABFrxReport.Clear;
begin
  inherited;
end;

constructor TABFrxReport.Create(AOwner: TComponent);
begin
  inherited;
  OnUserFunction:=UserFunction;
  CreateUserFunc;
  FExtVariable:=TstringList.Create;
end;

procedure TABFrxReport.CreateExtendReportInputControl(aGuid: string;aInputControlParent:TWinControl);
var
  tempDataset:TDataSet;
  tempStr1:string;
begin
  tempDataset:=ABGetConstSqlPubDataset('ABSys_Org_ExtendReportDatasets',[aGuid]);
  tempStr1:=  ABDownToTextFromField('Main',
               'ABSys_Org_ExtendReportDatasets','Ed_SQL',
               'isnull(ED_ParentGuid,''0'')=''0'' and Ed_Guid='+ABQuotedStr(tempDataset.FindField('Ed_Guid').AsString));

  ABCreateInputParams(tempStr1,
                      aInputControlParent,
                      aInputControlParent.owner,
                      False,

                      4,
                      False,

                      0,true,
                      0,False,

                      ABDBPanelParams_Leftspacing    ,
                      ABDBPanelParams_Topspacing     ,
                      ABDBPanelParams_Righttspacing  ,
                      ABDBPanelParams_Bottomspacing  ,
                      ABDBPanelParams_Xspacing       ,
                      ABDBPanelParams_Yspacing);

end;

procedure TABFrxReport.CreateUserFunc;
begin
  AddFunction('function GetSQLValue(aConnName:string;aSql: string): String;','ABSysFunc',('取得SQL的值'));
end;

function TABFrxReport.UserFunction(const MethodName: string;
  var Params: Variant): Variant;
begin
  if UpperCase(MethodName) = UpperCase('ABGetSQLValue') then
    Result := ABGetSQLValue(Params[0],Params[1],[],'');
end;

procedure TABFrxReport.DefineProperties(Filer: TFiler);
begin
  inherited;
  if (ABPubFrxReport=self) and (FileName=emptystr) then
  begin
    FileName:=FPubReportFileName;
  end;
end;

destructor TABFrxReport.Destroy;
begin
  FreeCreateRes;
  FExtVariable.free;
  inherited;
end;

procedure TABFrxReport.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited;
  if (Operation = opRemove) then
  begin
    if (FDataSource =AComponent)   then
      FDataSource:=nil
    else if (FInputParamParent =AComponent)   then
      FInputParamParent:=nil
    else if (FQuoteParamParent =AComponent)   then
      FQuoteParamParent:=nil
    else if (FCreateOwner =AComponent)   then
      FCreateOwner:=nil;
  end;
end;

procedure ABCreatefrxExport;
begin
  if not Assigned(frxBMPExport1)  then
  begin
    frxBMPExport1:= TfrxBMPExport.Create(nil);
    frxCSVExport1:= TfrxCSVExport.Create(nil);
    frxGIFExport1:= TfrxGIFExport.Create(nil);
    frxHTMLExport1:= TfrxHTMLExport.Create(nil);
    frxJPEGExport1:= TfrxJPEGExport.Create(nil);
    frxMailExport1:= TfrxMailExport.Create(nil);
    frxODSExport1:= TfrxODSExport.Create(nil);
    frxODTExport1:= TfrxODTExport.Create(nil);
    frxPDFExport1:= TfrxPDFExport.Create(nil);
    frxRTFExport1:= TfrxRTFExport.Create(nil);
    frxSimpleTextExport1:= TfrxSimpleTextExport.Create(nil);
    frxTIFFExport1:= TfrxTIFFExport.Create(nil);
    //frxTXTExport1:= TfrxTXTExport.Create(nil);
    frxXLSExport1:= TfrxXLSExport.Create(nil);
    frxXMLExport1:= TfrxXMLExport.Create(nil);
  end;
end;

procedure ABFreefrxExport;
begin
  if Assigned(frxBMPExport1) then
  begin
    FreeAndNil(frxBMPExport1       );
    FreeAndNil(frxCSVExport1       );
    FreeAndNil(frxGIFExport1       );
    FreeAndNil(frxHTMLExport1      );
    FreeAndNil(frxJPEGExport1      );
    FreeAndNil(frxMailExport1      );
    FreeAndNil(frxODSExport1       );
    FreeAndNil(frxODTExport1       );
    FreeAndNil(frxPDFExport1       );
    FreeAndNil(frxRTFExport1       );
    FreeAndNil(frxSimpleTextExport1);
    FreeAndNil(frxTIFFExport1      );
    //FreeAndNil(frxTXTExport1       );
    FreeAndNil(frxXLSExport1       );
    FreeAndNil(frxXMLExport1       );
  end;
end;

procedure ABFinalization;
begin
  ABFreefrxExport;
  try
    ABPubFrxReport.free;
  except
    //ABPubFrxReport.free会报错,应是FrxReport有BUG,先隐藏掉
  end;
  UnRegisterClass(TABFrxReport);
end;

procedure ABInitialization;
begin
  FPubReportFileName:=ABDefaultUserPath+'DefaultReport.fr3';

  RegisterClass(TABFrxReport);
  ABPubFrxReport:=TABFrxReport.Create(nil);
  ABPubFrxReport.Name:='ABPubFrxReport';

  ABCreatefrxExport;
end;

Initialization
  ABInitialization;

Finalization
  ABFinalization;

end.







