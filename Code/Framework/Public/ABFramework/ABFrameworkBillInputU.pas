{
框架单据导入单元
}
unit ABFrameworkBillInputU;

interface
{$I ..\ABInclude.ini}


uses
  ABPubFormU,
  ABPubFuncU,
  ABPubConstU,
  ABPubUserU,
  ABPubDBU,
  ABPubPanelU,

  ABThirdQueryU,
  ABThirdDBU,
  ABThirdConnU,
  ABThirdCustomQueryU,
  ABThirdFuncU,

  ABFrameworkConstU,
  ABFrameworkControlU,
  ABFrameworkcxGridU,
  ABFrameworkQueryAllFieldComboxU,
  ABFrameworkDictionaryQueryU,
  ABFrameworkUserU,
  ABFrameworkFuncU,
  ABFrameworkDBPanelU,
  ABFrameworkQueryU,
  ABFrameworkDBNavigatorU,

  cxGridLevel,

  SysUtils,Variants,Classes,Controls,Forms,DB,StdCtrls,ExtCtrls,
  cxCustomData,
  cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxGrid, cxPC,
  cxSplitter, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxBarBuiltInMenu, Vcl.Menus, cxButtons,
  ABPubMultilingualDBNavigatorU, cxContainer, cxEdit, cxStyles, cxFilter,
  cxData, cxDataStorage, cxNavigator, cxClasses, cxGridCustomView, cxGroupBox,
  cxRadioGroup, Vcl.ComCtrls, dxCore, cxDateUtils, cxTextEdit, cxMaskEdit,
  cxDropDownEdit, cxCalendar, cxCheckBox, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TABFrameworkBillInputForm = class(TABPubForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Button1: TABcxButton;
    Button2: TABcxButton;
    ABcxRadioGroup1: TABcxRadioGroup;
    Panel4: TPanel;
    Label5: TLabel;
    Label6: TLabel;
    PriBillName: TLabel;
    Button3: TButton;
    ABDBNavigator1: TABDBNavigator;
    Splitter1: TABcxSplitter;
    ABDBcxGrid1: TABcxGrid;
    ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView;
    cxGridLevel1: TcxGridLevel;
    ABcxGrid1: TABcxGrid;
    ABcxGridDBBandedTableView2: TABcxGridDBBandedTableView;
    cxGridLevel2: TcxGridLevel;
    ABcxDateTimeEdit1: TABcxDateTimeEdit;
    ABcxDateTimeEdit2: TABcxDateTimeEdit;
    ABcxTextEdit1: TABcxTextEdit;
    ABQuery1: TABQuery;
    ABDatasource1: TABDatasource;
    ABQuery2: TABQuery;
    ABDatasource2: TABDatasource;
    procedure Button3Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    FBi_MainDatetimeFieldName ,
    FBi_MainCodeFieldName:string;
    procedure SetAddWhereofNext;
    { Private declarations }
  public
    { Public declarations }
  end;

  TABBillInputButton=class(TABcxButton)
  private
    FFillDetailDataSet: TDataset;
    FDetailFieldNames: string;
    FFillMainDataSet: TDataset;
    FMainFieldNames: string;
    FCode: string;
    { Private declarations }
  protected
    { Protected declarations }
  public
    procedure Click; override;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  public
  published
    property Code:string read FCode  write FCode;
    property MainDataSet:TDataset read FFillMainDataSet  write FFillMainDataSet;
    property MainFieldNames:string read FMainFieldNames  write FMainFieldNames;

    property DetailDataSet:TDataset read FFillDetailDataSet  write FFillDetailDataSet;
    property DetailFieldNames:string read FDetailFieldNames  write FDetailFieldNames;
    { Public declarations }
  end;


implementation

{$R *.dfm}

{ TABInputData }

constructor TABBillInputButton.Create(AOwner: TComponent);
begin
  inherited;

end;

destructor TABBillInputButton.Destroy;
begin

  inherited;
end;

procedure TABBillInputButton.Click;
var
  tempForm: TABFrameworkBillInputForm;
  tempDataset:Tdataset;
  tempBi_DetailSQLFillFieldNames,
  tempBi_MainSQLFillFieldNames:string;

  i,j: longint;

  tempFromFieldName,
  tempToFieldName:string;
  procedure DoFillDetail(aDetailSQLFillFieldNames:string;aAllRecord:Boolean);
    procedure DoFillDetail_Cur;
    var
      i:LongInt;
      tempValue:Variant;
    begin
      FFillDetailDataSet.Append;
      for i := 1 to ABGetSpaceStrCount(aDetailSQLFillFieldNames, ',') do
      begin
        tempFromFieldName := ABGetSpaceStr(aDetailSQLFillFieldNames, i, ',');
        tempToFieldName := ABGetSpaceStr(FDetailFieldNames, i, ',');
        if (Assigned(FFillDetailDataSet.FindField(tempToFieldName))) then
        begin
          if (Assigned(tempForm.ABQuery2.FindField(tempFromFieldName)))  then
            tempValue:=tempForm.ABQuery2.FindField(tempFromFieldName).Value
          else
            tempValue:=tempFromFieldName;

          ABSetFieldValue(tempValue,
                          FFillDetailDataSet.FindField(tempToFieldName));
        end;
      end;
      FFillDetailDataSet.Post;
    end;
  var
    ii,jj:LongInt;
  begin
    if (Assigned(FFillDetailDataSet)) then
    begin
      FFillDetailDataSet.DisableControls;
      try
        if aAllRecord then
        begin
          tempForm.ABQuery2.First;
          while not tempForm.ABQuery2.Eof do
          begin
            DoFillDetail_Cur;

            tempForm.ABQuery2.Next;
          end;
        end
        else
        begin
          for ii := 0 to tempForm.ABcxGridDBBandedTableView2.DataController.GetSelectedCount- 1 do
          begin
            jj:= tempForm.ABcxGridDBBandedTableView2.DataController.GetSelectedRowIndex(ii);
            tempForm.ABcxGridDBBandedTableView2.DataController.DataSet.RecNo:=tempForm.ABcxGridDBBandedTableView2.DataController.GetRowInfo(jj).RecordIndex+1;

            DoFillDetail_Cur;
          end;
        end;
      finally
        FFillDetailDataSet.EnableControls;
      end;
    end;
  end;
  procedure DoFillMail(aMainSQLFillFieldNames:string);
  var
    i:LongInt;
    tempValue:Variant;
  begin
    if (Assigned(FFillMainDataSet)) then
    begin
      FFillMainDataSet.DisableControls;
      try
        FFillMainDataSet.Append;
        for i := 1 to ABGetSpaceStrCount(aMainSQLFillFieldNames, ',') do
        begin
          tempFromFieldName := ABGetSpaceStr(aMainSQLFillFieldNames, i, ',');
          tempToFieldName := ABGetSpaceStr(FMainFieldNames, i, ',');
          if (Assigned(FFillMainDataSet.FindField(tempToFieldName))) then
          begin
            if (Assigned(tempForm.ABQuery1.FindField(tempFromFieldName)))  then
              tempValue:=tempForm.ABQuery1.FindField(tempFromFieldName).Value
            else
              tempValue:=tempFromFieldName;

            ABSetFieldValue(tempValue,FFillMainDataSet.FindField(tempToFieldName));
          end;
        end;
        FFillMainDataSet.Post;
      finally
        FFillMainDataSet.EnableControls;
      end;
    end;
  end;
begin
  inherited;
  tempDataset:= ABGetConstSqlPubDataset('ABSys_Org_BillInput',[FCode]);
  if (Assigned(tempDataset)) and
     (not ABDatasetIsEmpty(tempDataset)) then
  begin
    tempForm:= TABFrameworkBillInputForm.Create(nil);
    tempForm.Name:='BillInputForm_'+tempDataset.FieldByName('Bi_Name').AsString;
    try
      tempForm.ABcxRadioGroup1.Visible:=(Assigned(FFillMainDataSet));

      tempForm.ABQuery1.Close;
      tempForm.ABQuery1.ConnName:=ABGetConnNameByConnGuid(tempDataset.FieldByName('Bi_MainConnName').AsString);
      tempForm.ABQuery1.SQL.Text:=tempDataset.FieldByName('Bi_MainSQL').AsString;
      if tempDataset.FieldByName('Bi_MainInitTable').AsString<>EmptyStr then
        tempForm.ABQuery1.LoadTables.Text:=tempDataset.FieldByName('Bi_MainInitTable').AsString;

      tempForm.ABQuery2.Close;
      tempForm.ABQuery2.ConnName:=ABGetConnNameByConnGuid(tempDataset.FieldByName('Bi_DetailConnName').AsString);
      tempForm.ABQuery2.SQL.Text:=tempDataset.FieldByName('Bi_DetailSQL').AsString;
      if tempDataset.FieldByName('Bi_DetailInitTable').AsString<>EmptyStr then
        tempForm.ABQuery2.LoadTables.Text:=tempDataset.FieldByName('Bi_DetailInitTable').AsString;
      tempForm.ABQuery2.MasterFields:=tempDataset.FieldByName('Bi_DetailSQLMasterFields').AsString;
      tempForm.ABQuery2.DetailFields:=tempDataset.FieldByName('Bi_DetailSQLDetailFields').AsString;

      if not tempDataset.FieldByName('Bi_MainInitBeginDatetime').IsNull then
        tempForm.ABcxDateTimeEdit1.Date:=tempDataset.FieldByName('Bi_MainInitBeginDatetime').AsDateTime
      else
        tempForm.ABcxDateTimeEdit1.Date:= ABGetServerDate-7;

      if not tempDataset.FieldByName('Bi_MainInitEndDatetime').IsNull then
        tempForm.ABcxDateTimeEdit2.Date:=tempDataset.FieldByName('Bi_MainInitEndDatetime').AsDateTime
      else
        tempForm.ABcxDateTimeEdit2.Date:= ABStrToDateTime(ABDateToStr(ABGetServerDate)+' 23:59:59');

      tempForm.FBi_MainDatetimeFieldName:=tempDataset.FieldByName('Bi_MainDatetimeFieldName').AsString;
      tempForm.FBi_MainCodeFieldName:=tempDataset.FieldByName('Bi_MainCodeFieldName').AsString;
      tempForm.SetAddWhereofNext;
      ABInitFormDataSet([],[tempForm.ABcxGridDBBandedTableView1],[],tempForm.ABQuery1);
      ABInitFormDataSet([],[tempForm.ABcxGridDBBandedTableView2],[],tempForm.ABQuery2);

      if tempForm.ShowModal=mrOk then
      begin
        if (Assigned(FFillMainDataSet)) then
        begin
          ABPostDataset(FFillMainDataSet);
        end;

        tempBi_MainSQLFillFieldNames:=tempDataset.FieldByName('Bi_MainSQLFillFieldNames').AsString;
        tempBi_DetailSQLFillFieldNames:=tempDataset.FieldByName('Bi_DetailSQLFillFieldNames').AsString;
        ABSetTableViewSelect(tempForm.ABcxGridDBBandedTableView1);
        if (tempForm.ABcxRadioGroup1.Visible) and
           (tempForm.ABcxRadioGroup1.ItemIndex=0) then
        begin
          for I := 0 to tempForm.ABcxGridDBBandedTableView1.DataController.GetSelectedCount- 1 do
          begin
            j:= tempForm.ABcxGridDBBandedTableView1.DataController.GetSelectedRowIndex(i);
            tempForm.ABcxGridDBBandedTableView1.DataController.DataSet.RecNo:=tempForm.ABcxGridDBBandedTableView1.DataController.GetRowInfo(j).RecordIndex+1;

            DoFillMail(tempBi_MainSQLFillFieldNames);
            DoFillDetail(tempBi_DetailSQLFillFieldNames,
                          (tempForm.ABcxGridDBBandedTableView1.DataController.GetSelectedCount>1) or
                          (tempForm.ABcxGridDBBandedTableView2.DataController.GetSelectedCount=0)
                          );
          end;
        end
        else if (tempForm.ABcxRadioGroup1.Visible) and
                (tempForm.ABcxRadioGroup1.ItemIndex=1) then
        begin
          for I := 0 to tempForm.ABcxGridDBBandedTableView1.DataController.GetSelectedCount- 1 do
          begin
            j:= tempForm.ABcxGridDBBandedTableView1.DataController.GetSelectedRowIndex(i);
            tempForm.ABcxGridDBBandedTableView1.DataController.DataSet.RecNo:=tempForm.ABcxGridDBBandedTableView1.DataController.GetRowInfo(j).RecordIndex+1;
            if i=0 then
              DoFillMail(tempBi_MainSQLFillFieldNames);

            DoFillDetail(tempBi_DetailSQLFillFieldNames,
                          (tempForm.ABcxGridDBBandedTableView1.DataController.GetSelectedCount>1) or
                          (tempForm.ABcxGridDBBandedTableView2.DataController.GetSelectedCount=0)
                          );
          end;
        end
        else
        begin
          for I := 0 to tempForm.ABcxGridDBBandedTableView1.DataController.GetSelectedCount- 1 do
          begin
            j:= tempForm.ABcxGridDBBandedTableView1.DataController.GetSelectedRowIndex(i);
            tempForm.ABcxGridDBBandedTableView1.DataController.DataSet.RecNo:=tempForm.ABcxGridDBBandedTableView1.DataController.GetRowInfo(j).RecordIndex+1;

            DoFillDetail( tempBi_DetailSQLFillFieldNames,
                          (tempForm.ABcxGridDBBandedTableView1.DataController.GetSelectedCount>1) or
                          (tempForm.ABcxGridDBBandedTableView2.DataController.GetSelectedCount=0)
                          );
          end;
        end;
      end;
    finally
      tempForm.Free;
    end;
  end;
end;

procedure TABFrameworkBillInputForm.SetAddWhereofNext;
var
  tempAddWhereofNext:string;
begin
  tempAddWhereofNext:=EmptyStr;
  if FBi_MainDatetimeFieldName<>EmptyStr then
  begin
    if ABcxDateTimeEdit1.Date>0 then
      ABAddstr( tempAddWhereofNext,
                FBi_MainDatetimeFieldName+'>='+QuotedStr(ABDateTimeToStr(ABcxDateTimeEdit1.Date)),
                ' and '
                );
    if ABcxDateTimeEdit2.Date>0 then
      ABAddstr( tempAddWhereofNext,
                FBi_MainDatetimeFieldName+'<='+QuotedStr(ABDateTimeToStr(ABcxDateTimeEdit2.Date)),
                ' and '
                );
  end;
  if (FBi_MainCodeFieldName<>EmptyStr) and
     (ABcxTextEdit1.Text<>EmptyStr) then
    ABAddstr( tempAddWhereofNext,
              FBi_MainCodeFieldName+'='+QuotedStr(ABcxTextEdit1.Text),
              ' and '
              );
  ABQuery1.AddWhereofNext:=tempAddWhereofNext;
end;

procedure TABFrameworkBillInputForm.Button1Click(Sender: TObject);
begin
  ModalResult:=mrCancel;
end;

procedure TABFrameworkBillInputForm.Button2Click(Sender: TObject);
begin
  ModalResult:=mrOk;
end;

procedure TABFrameworkBillInputForm.Button3Click(Sender: TObject);
begin
  SetAddWhereofNext;
  ABReFreshQuery(ABQuery1,[]);
end;

end.


