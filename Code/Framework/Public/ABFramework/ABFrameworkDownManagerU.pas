{
框架下拉项管理单元
}
unit ABFrameworkDownManagerU;

interface
{$I ..\ABInclude.ini}


uses
  abPubCOnstU,
  ABPubFuncU,
  ABPubFormU,
  ABPubVarU,

  ABThirdCacheDatasetU,
  ABThird_DevExtPopupMenuU,
  ABThirdFuncU,

  ABFrameworkconstU,
  ABFrameworkFuncU,

  cxGridCustomView,

  cxPC,
  cxButtons,
  cxGridLevel,
  cxGridDBBandedTableView,
  cxGridTableView,
  cxGridCustomTableView,
  cxGrid,
  cxTextEdit,
  cxMaskEdit,
  cxButtonEdit,
  cxImageComboBox,
  cxSpinEdit,
  cxCalc,
  cxHyperLinkEdit,
  cxTimeEdit,
  cxCurrencyEdit,
  cxBlobEdit,
  cxMRUEdit,
  cxDropDownEdit,
  cxDBLookupEdit,
  cxLabel,
  cxProgressBar,
  cxTrackBar,
  cxColorComboBox,
  cxFontNameComboBox,
  cxCheckGroup,
  cxRichEdit,
  cxShellComboBox,
  cxDBExtLookupComboBox,
  cxDBEdit,
  cxDBLabel,
  cxDBProgressBar,
  cxDBTrackBar,
  cxDBColorComboBox,
  cxDBFontNameComboBox,
  cxDBCheckComboBox,
  cxDBCheckGroup,
  cxDBRichEdit,
  cxDBShellComboBox,
  cxDBTL,
  cxDBData,
  cxEdit,

  SysUtils,Classes,Controls,Forms,DB,StdCtrls,ExtCtrls,ComCtrls,

  Menus, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxBarBuiltInMenu, cxContainer, cxStyles, cxClasses;

type
  TABDownManagerForm = class(TABPubForm)
    Splitter1: TSplitter;
    PageControl2: TPageControl;
    ListBox1: TListBox;
    ABcxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    Panel1: TPanel;
    Panel2: TPanel;
    ABcxLabel1: TcxLabel;
    ABcxTextEdit1: TcxTextEdit;
    ABcxButton1: TcxButton;
    ABcxGridPopupMenu1: TABcxGridPopupMenu;
    Sheet1_Grid1: TcxGrid;
    Sheet1_Level1: TcxGridLevel;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ListBox1Click(Sender: TObject);
    procedure ABcxButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;



implementation
{$R *.dfm}

procedure TABDownManagerForm.FormCreate(Sender: TObject);
begin
  ListBox1.Items.Clear;
end;

procedure TABDownManagerForm.FormShow(Sender: TObject);
begin
  if ABcxPageControl1.ActivePageIndex>0 then
    ABcxPageControl1.ActivePageIndex:=0;
end;

procedure TABDownManagerForm.ListBox1Click(Sender: TObject);
begin
  if ListBox1.ItemIndex>=0 then
  begin
    Sheet1_Level1.GridView:=   Sheet1_Grid1.Views[ListBox1.ItemIndex];

    ABcxTextEdit1.Text:= TcxGridDBBandedTableView(Sheet1_Level1.GridView).DataController.KeyFieldNames;
  end;
end;

procedure TABDownManagerForm.ABcxButton1Click(Sender: TObject);
begin
  TcxGridDBBandedTableView(Sheet1_Level1.GridView).DataController.KeyFieldNames:=ABcxTextEdit1.Text;
end;


end.
