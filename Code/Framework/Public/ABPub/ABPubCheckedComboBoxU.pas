{
复选下拉框控件单元

主要有以下属性，可实现下拉、显示、保存值对应不同的值
Text=选择项目的显示串
DownText=选择项目的下拉串
SaveText=选择项目的保存串

ViewItems=项目的显示列表
Items=项目的下拉列表
SaveItems=选择项目的保存列表
如
ViewItems=001、002
Items=张三、李四
SaveItems=111、222

下拉显示的是张三、李四
全选择后显示的是001、002
全选择后SaveText得到的却是111、222
}
unit ABPubCheckedComboBoxU;

interface
{$I ..\ABInclude.ini}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls,StdCtrls;

type
  TABCBQuoteStyle = (qsNone, qsSingle, qsDouble);
  TABCheckedComboBox = class(TCustomComboBox)
  private
    { Private declarations }
    FListInstance: Pointer;
    FDefListProc: Pointer;
    FListHandle: HWnd;
    FQuoteStyle: TABCBQuoteStyle;
    FColorNotFocus: TColor;
    FCheckedCount: integer;
    FTextAsHint: boolean;
    FOnCheckClick: TNotifyEvent;
    FViewItems: TStrings;
    FViewSeparatorStr: string;
    FSaveItems: TStrings;
    FSaveSeparatorStr: string;
    FDownSeparatorStr: string;
    procedure CNDrawItem(var Message: TWMDrawItem); message CN_DRAWITEM;
    procedure CMEnter(var Message: TCMEnter); message CM_ENTER;
    procedure CMExit(var Message: TCMExit); message CM_EXIT;
    procedure ListWndProc(var Message: TMessage);
    procedure SetColorNotFocus(value: TColor);
    procedure SetViewItems(const Value: TStrings);
    procedure SetSaveItems(const Value: TStrings);
    function GetSaveText: string;
    function GetDownText: string;
  protected
    { Protected declarations }
    //暂时存放显示的字串
    m_strText: string;
    m_bTextUpdated: boolean;
    procedure WndProc(var Message: TMessage); override;
    procedure RecalcText;
    function GetText: string;
    function GetCheckedCount: integer;
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure SetCheck(nIndex: integer; checked: boolean);
    function AddChecked(value: string; checked: boolean): integer;
    function IsChecked(nIndex: integer): boolean;
    procedure CheckAll(checked: boolean);
    property Text: string read GetText;
    property DownText: string read GetDownText;
    property SaveText: string read GetSaveText;
    property CheckedCount: integer read GetCheckedCount;
  published
    { Published declarations }
    property ViewItems: TStrings read FViewItems write SetViewItems;
    property SaveItems: TStrings read FSaveItems write SetSaveItems;
    property ViewSeparatorStr: string read FViewSeparatorStr write FViewSeparatorStr;
    property SaveSeparatorStr: string read FSaveSeparatorStr write FSaveSeparatorStr;
    property DownSeparatorStr: string read FDownSeparatorStr write FDownSeparatorStr;

    property Style;
    property Anchors;
    property BiDiMode;
    property Color;
    property ColorNotFocus: TColor read FColorNotFocus write SetColorNotFocus;
    property Constraints;
    property Ctl3D;
    property DragCursor;
    property DragKind;
    property DragMode;
    property DropDownCount;
    property Enabled;
    property Font;
    property ImeMode;
    property ImeName;
    property ItemHeight;
    property Items;
    property MaxLength;
    property ParentBiDiMode;
    property ParentColor;
    property ParentCtl3D;
    property ParentFont;
    property ParentShowHint;
    property PopupMenu;
    property QuoteStyle: TABCBQuoteStyle read FQuoteStyle write FQuoteStyle default qsNone;
    property ShowHint;
    property ShowTextAsHint: Boolean read FTextAsHint write FTextAsHint default true;
    property Sorted;
    property TabOrder;
    property TabStop;
    property Visible;
    property OnChange;
    property OnCheckClick: TNotifyEvent read FOnCheckClick write FOnCheckClick;
    property OnClick;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnDropDown;
    property OnEndDock;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnStartDock;
    property OnStartDrag;
  end;

implementation

var
  FCheckWidth, FCheckHeight: Integer;
  { TABCheckedComboBox }

procedure GetCheckSize;
var
  tempBitmap:TBitmap;
begin
  tempBitmap:=TBitmap.Create;
  try
    tempBitmap.Handle := LoadBitmap(0, PChar(32759));
    FCheckWidth := tempBitmap.Width div 4;
    FCheckHeight := tempBitmap.Height div 3;
  finally
    tempBitmap.Free;
  end;
end;

procedure TABCheckedComboBox.SetCheck(nIndex: integer; checked: boolean);
begin
  if (nIndex > -1) and (nIndex < Items.count) then
  begin
    Items.Objects[nIndex] := TObject(checked);
    m_bTextUpdated := FALSE;
    Invalidate;
    if Assigned(FOnCheckClick) then
      OnCheckClick(self)
  end;
end;

function TABCheckedComboBox.AddChecked(value: string; checked: boolean): integer;
begin
  result := Items.AddObject(value, TObject(checked));
  if result >= 0 then
  begin
    m_bTextUpdated := FALSE;
    Invalidate;
  end;
end;

function TABCheckedComboBox.IsChecked(nIndex: integer): boolean;
begin
  result := false;
  if (nIndex > -1) and (nIndex < Items.count) then
    result := Items.Objects[nIndex] = TObject(TRUE)
end;

procedure TABCheckedComboBox.CheckAll(checked: boolean);
var
  i: integer;
begin
  for i := 0 to Items.count - 1 do
    Items.Objects[i] := TObject(checked);
end;

function GetFormatedText(kind: TABCBQuoteStyle; str: string): string;
var
  s: string;
begin
  result := str;
  if length(str) > 0 then
  begin
    s := str;
    case kind of
      qsSingle: result :=
        '''' +
          StringReplace(S, ',', ''',''', [rfReplaceAll]) +
          '''';
      qsDouble: result :=
        '"' +
          StringReplace(S, ',', '","', [rfReplaceAll]) +
          '"';
    end;
  end;
end;

function TABCheckedComboBox.GetText: string;
begin
  RecalcText;
  if FQuoteStyle = qsNone then
    result := m_strText
  else
    result := GetFormatedText(FQuoteStyle, m_strText);
end;

function TABCheckedComboBox.GetCheckedCount: integer;
begin
  RecalcText;
  result := FCheckedCount;
end;

function TABCheckedComboBox.GetDownText: string;
var
  i: integer;
  tempItem: string;
begin
  Result:=EmptyStr;
  for i := 0 to items.Count - 1 do
  begin
    if IsChecked(i) then
    begin
      tempItem := items[i];
      if (tempItem <> '') then
      begin
        if Result=EmptyStr then
        begin
          Result:=tempItem;
        end
        else
        begin
          Result:=Result+FDownSeparatorStr+tempItem;
        end;
      end;
    end;
  end;
end;

function TABCheckedComboBox.GetSaveText: string;
var
  i: integer;
  tempItem: string;
begin
  Result:=EmptyStr;
  for i := 0 to items.Count - 1 do
  begin
    if IsChecked(i) then
    begin
      if i < FViewItems.Count then
        tempItem := FSaveItems[i]
      else
        tempItem := items[i];

      if (tempItem <> '') then
      begin
        if Result=EmptyStr then
        begin
          Result:=tempItem;
        end
        else
        begin
          Result:=Result+FSaveSeparatorStr+tempItem;
        end;
      end;
    end;
  end;
end;

procedure TABCheckedComboBox.RecalcText;
var
  nCount, i: integer;
  strItem,
    strText: string;
begin
  if (not m_bTextUpdated) then
  begin
    FCheckedCount := 0;
    nCount := items.count;
    strText := '';
    for i := 0 to nCount - 1 do
      if IsChecked(i) then
      begin
        inc(FCheckedCount);
        if i < FViewItems.Count then
          strItem := FViewItems[i]
        else
          strItem := items[i];
        if (strText <> '') then
          strText := strText + FViewSeparatorStr;
        strText := strText + strItem;
      end;
    // Set the text
    m_strText := strText;
    if FTextAsHint then
      Hint := m_strText;
    m_bTextUpdated := TRUE;
  end;
end;

procedure TABCheckedComboBox.SetColorNotFocus(value: TColor);
begin
  if FColorNotFocus <> Value then
    FColorNotFocus := Value;
  Invalidate
end;

procedure TABCheckedComboBox.SetSaveItems(const Value: TStrings);
begin
  FSaveItems.Assign(Value)
end;

procedure TABCheckedComboBox.SetViewItems(const Value: TStrings);
begin
  FViewItems.Assign(Value)
end;

procedure TABCheckedComboBox.CMEnter(var Message: TCMEnter);
begin
  Self.Color := clWhite;
  if Assigned(OnEnter) then
    OnEnter(Self);
end;

procedure TABCheckedComboBox.CMExit(var Message: TCMExit);
begin
  Self.Color := FColorNotFocus;
  if Assigned(OnExit) then
    OnExit(Self);
end;

procedure TABCheckedComboBox.CNDrawItem(var Message: TWMDrawItem);
var
  State: TOwnerDrawState;
  rcBitmap, rcText: Trect;
  nCheck: integer; // 0 - No check, 1 - Empty check, 2 - Checked
  nState: integer;
  strText: string;
  ItId: Integer;
  dc: HDC;
begin
  State := TOwnerDrawState(LongRec( Message.DrawItemStruct.itemState).Lo);
  dc := Message.DrawItemStruct.hDC;
  rcBitmap := Message.DrawItemStruct.rcItem;
  rcText := Message.DrawItemStruct.rcItem;
  ItId := Message.DrawItemStruct.itemID;
  // Check if we are drawing the static portion of the combobox
  if (itID < 0) then
  begin
    RecalcText();
    strText := m_strText;
    nCheck := 0;
  end
  else
  begin
    strtext := Items[ItId];
    rcBitmap.Left := 2;
    rcBitmap.Top := rcText.Top + (rcText.Bottom - rcText.Top - FCheckWidth) div 2;
    rcBitmap.Right := rcBitmap.Left + FCheckWidth;
    rcBitmap.Bottom := rcBitmap.Top + FCheckHeight;

    rcText.left := rcBitmap.right;
    nCheck := 1;
    if IsChecked(ItId) then
      inc(nCheck);
  end;
  if (nCheck > 0) then
  begin
    SetBkColor(dc, GetSysColor(COLOR_WINDOW));
    SetTextColor(dc, GetSysColor(COLOR_WINDOWTEXT));
    nState := DFCS_BUTTONCHECK;
    if (nCheck > 1) then
      nState := nState or DFCS_CHECKED;
    DrawFrameControl(dc, rcBitmap, DFC_BUTTON, nState);
  end;
  if (odSelected in State) then
  begin
    SetBkColor(dc, $0091622F);
    SetTextColor(dc, GetSysColor(COLOR_HIGHLIGHTTEXT));
  end
  else
  begin
    if (nCheck = 0) then
    begin
      SetTextColor(dc, ColorToRGB(Font.Color));
      SetBkColor(dc, ColorToRGB(FColorNotFocus));
    end
    else
    begin
      SetTextColor(dc, ColorToRGB(Font.Color));
      SetBkColor(dc, ColorToRGB(Brush.Color));
    end;
  end;
  if itID >= 0 then
    strText := ' ' + strtext;
  ExtTextOut(dc, 0, 0, ETO_OPAQUE, @rcText, nil, 0, nil);
  DrawText(dc, pchar(strText), Length(strText), rcText, DT_SINGLELINE or DT_VCENTER or DT_END_ELLIPSIS);
  if odFocused in State then
    DrawFocusRect(dc, rcText);
end;

//DefWindowProc

procedure TABCheckedComboBox.ListWndProc(var Message: TMessage);
var
  nItemHeight, nTopIndex, nIndex: Integer;
  rcItem, rcClient: TRect;
  pt: TPoint;
begin
  case Message.Msg of
    LB_GETCURSEL: // this is for to not draw the selected in the text area
      begin
        Message.result := -1;
        exit;
      end;
    WM_CHAR: // pressing space toggles the checked
      begin
        if (TWMKey(Message).CharCode = VK_SPACE) then
        begin
          // Get the current selection
          nIndex := CallWindowProcA(FDefListProc, FListHandle, LB_GETCURSEL, Message.wParam, Message.lParam);
          SendMessage(FListHandle, LB_GETITEMRECT, nIndex, LongInt(@rcItem));
          InvalidateRect(FListHandle, @rcItem, FALSE);
          SetCheck(nIndex, not IsChecked(nIndex));
          SendMessage(WM_COMMAND, handle, CBN_SELCHANGE, handle);
          Message.result := 0;
          exit;
        end
      end;
    WM_LBUTTONDOWN:
      begin
        Windows.GetClientRect(FListHandle, rcClient);
        pt.x := TWMMouse(Message).XPos; //LOWORD(Message.lParam);
        pt.y := TWMMouse(Message).YPos; //HIWORD(Message.lParam);
        if (PtInRect(rcClient, pt)) then
        begin
          nItemHeight := SendMessage(FListHandle, LB_GETITEMHEIGHT, 0, 0);
          nTopIndex := SendMessage(FListHandle, LB_GETTOPINDEX, 0, 0);
          // Compute which index to check/uncheck
          nIndex := trunc(nTopIndex + pt.y / nItemHeight);
          SendMessage(FListHandle, LB_GETITEMRECT, nIndex, LongInt(@rcItem));
          if (PtInRect(rcItem, pt)) then
          begin
            InvalidateRect(FListHandle, @rcItem, FALSE);
            SetCheck(nIndex, not IsChecked(nIndex));
            SendMessage(WM_COMMAND, handle, CBN_SELCHANGE, handle);
          end
        end
      end;
    WM_LBUTTONUP:
      begin
        Message.result := 0;
        exit;
      end;
  end;
  ComboWndProc(Message, FListHandle, FDefListProc);
end;

constructor TABCheckedComboBox.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FViewSeparatorStr := ',';
  FSaveSeparatorStr := ',';
  FDownSeparatorStr := ',';
  FViewItems := TStringList.Create;
  FSaveItems := TStringList.Create;
  ShowHint := true;
  FTextAsHint := true;
  ParentShowHint := False;
  FListHandle := 0;
  FQuoteStyle := qsNone;
  FColorNotFocus := clWindow;
  Style := csOwnerDrawVariable;
  m_bTextUpdated := FALSE;
  FListInstance := MakeObjectInstance(ListWndProc);
end;

destructor TABCheckedComboBox.Destroy;
begin
  FViewItems.Free;
  FSaveItems.Free;
  FreeObjectInstance(FListInstance);
  inherited Destroy;
end;

procedure TABCheckedComboBox.WndProc(var Message: TMessage);
var
  lWnd: HWND;
begin
  if message.Msg = WM_CTLCOLORLISTBOX then
  begin
    // If the listbox hasn't been subclassed yet, do so...
    if (FListHandle = 0) then
    begin
      lwnd := message.LParam;
      if (lWnd <> 0) and (lWnd <> FDropHandle) then
      begin
        // Save the listbox handle
        FListHandle := lWnd;
        FDefListProc := Pointer(GetWindowLong(FListHandle, GWL_WNDPROC));
        SetWindowLong(FListHandle, GWL_WNDPROC, Longint(FListInstance));
      end;
    end;
  end;
  inherited;
end;

initialization
  GetCheckSize;

end.

