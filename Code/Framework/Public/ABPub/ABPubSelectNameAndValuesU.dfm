object ABSelectNameAndValuesForm: TABSelectNameAndValuesForm
  Left = 210
  Top = 124
  Caption = #26469#28304#19982#30446#26631#23545#24212#36873#25321
  ClientHeight = 498
  ClientWidth = 698
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 698
    Height = 498
    Align = alClient
    BevelOuter = bvNone
    Caption = 'Panel2'
    TabOrder = 0
    ExplicitWidth = 700
    ExplicitHeight = 500
    object Panel1: TPanel
      Left = 0
      Top = 462
      Width = 698
      Height = 36
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitTop = 464
      ExplicitWidth = 700
      DesignSize = (
        698
        36)
      object Label1: TLabel
        Left = 8
        Top = 11
        Width = 80
        Height = 16
        AutoSize = False
        Caption = #26469#28304#21015
        Transparent = True
      end
      object Label2: TLabel
        Left = 256
        Top = 11
        Width = 80
        Height = 16
        AutoSize = False
        Caption = #30446#30340#21015
        Transparent = True
      end
      object SpeedButton1: TButton
        Left = 503
        Top = 6
        Width = 60
        Height = 25
        Anchors = [akTop, akRight]
        Caption = #26032#22686
        TabOrder = 2
        OnClick = SpeedButton1Click
        ExplicitLeft = 505
      end
      object ComboBox1: TComboBox
        Left = 91
        Top = 8
        Width = 159
        Height = 21
        ImeName = #24555#20048#20116#31508
        TabOrder = 0
      end
      object ComboBox2: TComboBox
        Left = 337
        Top = 8
        Width = 157
        Height = 21
        Anchors = [akLeft, akTop, akRight]
        ImeName = #24555#20048#20116#31508
        TabOrder = 1
        ExplicitWidth = 159
      end
      object Button1: TButton
        Left = 630
        Top = 6
        Width = 60
        Height = 25
        Anchors = [akTop, akRight]
        Caption = #30830#23450
        TabOrder = 3
        OnClick = Button1Click
        ExplicitLeft = 632
      end
      object Button2: TButton
        Left = 564
        Top = 6
        Width = 60
        Height = 25
        Anchors = [akTop, akRight]
        Caption = #21024#38500
        TabOrder = 4
        OnClick = Button2Click
        ExplicitLeft = 566
      end
    end
    object ListBox1: TListBox
      Left = 0
      Top = 0
      Width = 698
      Height = 462
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ItemHeight = 14
      ParentFont = False
      TabOrder = 1
      ExplicitWidth = 700
      ExplicitHeight = 464
    end
  end
end
