{
增强TdownButton控件单元(支持下拉菜单)
}
unit ABPubDownButtonU;

interface
{$I ..\ABInclude.ini}

uses
  abpubconstU,
  ABPubFuncU,

  Classes,stdctrls,Controls,Menus,Types,Windows;

type
  TABDownButton = class(TButton)
  private
    FChickDown:Boolean;
  {$IF (CompilerVersion<=18.5)}
    FDropDownMenu: TPopupMenu;
  {$IFEND}
  protected        
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState;
      X, Y: Integer); override;
  public
    procedure Click; override;
  Published
  {$IF (CompilerVersion<=18.5)}
    property DropDownMenu:TPopupMenu read FDropDownMenu write FDropDownMenu;
  {$IFEND}                        
  end;


implementation

{ TABDownButton }

procedure TABDownButton.Click;
begin
  if (FChickDown) and
     (ABIsPublishProp(self,'DropDownMenu')) and
     (Assigned(ABGetObjectPropValue(self,'DropDownMenu'))) then
  begin
    TPopupMenu(ABGetObjectPropValue(self,'DropDownMenu')).Popup(Mouse.CursorPos.X, Mouse.CursorPos.Y);
  end
  else
  begin
    inherited;
  end;
end;


procedure TABDownButton.MouseDown(Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
var
  FMouseDownScreenPoint,
  FMouseDownControlPoint:TPoint;
begin
  inherited;
  FChickDown:=false;
  if ssLeft in Shift then
  begin
    //x,y为屏幕坐标
    FMouseDownScreenPoint:=Point(x,y);
    //ControlPoint为当前Button控件的坐标
    FMouseDownControlPoint:=Self.ClientToScreen(FMouseDownScreenPoint);

    //可以此处根据FMouseDownControlPoint指定在某些位置时显示下拉菜单
    FChickDown:=True;
  end;
end;


end.


