object ABOrderStrForm: TABOrderStrForm
  Left = 747
  Top = 266
  Caption = #25490#24207
  ClientHeight = 400
  ClientWidth = 500
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 359
    Width = 500
    Height = 41
    Align = alBottom
    TabOrder = 1
    DesignSize = (
      500
      41)
    object btn1: TButton
      Left = 416
      Top = 6
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #21462#28040
      TabOrder = 0
      OnClick = btn1Click
    end
    object btn2: TButton
      Left = 335
      Top = 6
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #30830#35748
      TabOrder = 1
      OnClick = btn2Click
    end
  end
  object ABItemList1: TABItemList
    Left = 0
    Top = 0
    Width = 500
    Height = 359
    Align = alClient
    ShowCaption = False
    TabOrder = 0
    MoveVisible = True
  end
end
