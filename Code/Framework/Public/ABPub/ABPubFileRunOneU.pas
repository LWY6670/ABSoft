{
使程序只运行一次（直接引用一下此单元就能使得程序具有只运行一次的能力）
}
unit ABPubFileRunOneU;

interface
{$I ..\ABInclude.ini}

uses
  ABPubVarU,
  ABPubFuncU,

  Forms,Windows;

implementation
var
  FAppFullNamehMutex: THandle;

function ABSetRunOne(aName:string):THandle;
var
  tempName:string;
  tempMutex: THandle;
begin
  Result:=0;
  if not ABInDelphi then
  begin
    tempName:=ABStrToName(ABAppFullName);
    tempMutex :=OpenMutex(MUTEX_ALL_ACCESS, False,PChar(tempName));
    if tempMutex> 0 then
    begin
      CloseHandle(tempMutex);
      MessageBox(0,PChar('程序['+ABAppFullName+']已经运行'),'提示',MB_ICONINFORMATION);
      Application.Terminate;
    end
    else
    begin
      Result := CreateMutex(nil, False, PChar(tempName));
    end;
  end;
end;

procedure ABInitialization;
begin
  FAppFullNamehMutex:=ABSetRunOne(ABAppFullName);
end;

procedure ABFinalization;
begin
  CloseHandle(FAppFullNamehMutex);
end;

Initialization
  ABInitialization;

Finalization
  ABFinalization ;

end.
