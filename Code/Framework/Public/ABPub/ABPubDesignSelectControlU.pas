{
提供运行期选择窗体中控件
}
unit ABPubDesignSelectControlU;

interface
{$I ..\ABInclude.ini}

uses
  SysUtils, Classes, Controls, Forms, StdCtrls;

type
  TABPubDesignSelectControlForm = class(TForm)
    gbFilter: TGroupBox;
    rbCurrForm: TRadioButton;
    rbSpecControl: TRadioButton;
    cbbFilterControl: TComboBox;
    gbByName: TGroupBox;
    gbComponentList: TGroupBox;
    lbSource: TListBox;
    btnAdd: TButton;
    btnAddAll: TButton;
    btnDelete: TButton;
    btnDeleteAll: TButton;
    btnSelAll: TButton;
    btnSelNone: TButton;
    btnSelInvert: TButton;
    Label1: TLabel;
    lbDest: TListBox;
    Label2: TLabel;
    btnOK: TButton;
    btnCancel: TButton;
    edtByName: TEdit;
    cbByName: TCheckBox;
    cbByClass: TCheckBox;
    cbbByClass: TComboBox;
    Label4: TLabel;
    cbbSourceOrderStyle: TComboBox;
    cbbSourceOrderDir: TComboBox;
    cbSubClass: TCheckBox;
    cbIncludeChildren: TCheckBox;
    btnMoveToTop: TButton;
    btnMoveToBottom: TButton;
    btnMoveUp: TButton;
    btnMoveDown: TButton;
    procedure DoUpdateSourceOrder(Sender: TObject);
    procedure DoUpdateListControls(Sender: TObject);
    procedure actAddExecute(Sender: TObject);
    procedure actAddAllExecute(Sender: TObject);
    procedure actDeleteExecute(Sender: TObject);
    procedure actDeleteAllExecute(Sender: TObject);
    procedure actSelAllExecute(Sender: TObject);
    procedure actSelNoneExecute(Sender: TObject);
    procedure actSelInvertExecute(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure actMoveToTopExecute(Sender: TObject);
    procedure actMoveToBottomExecute(Sender: TObject);
    procedure actMoveUpExecute(Sender: TObject);
    procedure actMoveDownExecute(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    FControls:TStrings;
    procedure BeginUpdateList;
    procedure EndUpdateList;
    { Private declarations }
  protected
  public
    procedure UpdateSourceOrders;
    procedure RefreshState;
    { Public declarations }
  end;

//选择控件
function ABPubDesignSelectControl(aForm:TCustomForm;aSelectControls:TStrings):boolean;

var
  ABPubDesignSelectControlForm: TABPubDesignSelectControlForm;

implementation

{$R *.dfm}
type
  TSortStyle = (ssByName, ssByClass);
  TSortDir = (sdUp, sdDown);

var
  SortStyle: TSortStyle;
  SortDir: TSortDir;

function ABPubDesignSelectControl(aForm:TCustomForm;aSelectControls:TStrings):boolean;
var
  tempForm: TABPubDesignSelectControlForm;
  tempIndex,I:Integer;
begin
  result:=false;
  tempForm:=TABPubDesignSelectControlForm.Create(nil);
  try
    for I := 0 to aForm.ComponentCount-1 do
    begin
      if aForm.Components[i] is TControl then
      begin
        if (aForm.Components[i] is TWinControl) and
           (TWinControl(aForm.Components[i]).ControlCount>0) and
           (tempForm.cbbFilterControl.Items.IndexOf(TWinControl(aForm.Components[i]).Name)<0) then
        begin
          tempForm.cbbFilterControl.Items.AddObject(aForm.Components[i].Name,aForm.Components[i])
        end;

        if (tempForm.cbbByClass.Items.IndexOf(aForm.Components[i].ClassName)<0) then
        begin
          tempForm.cbbByClass.Items.Add(aForm.Components[i].ClassName)
        end;

        tempForm.FControls.AddObject(aForm.Components[i].Name,aForm.Components[i]);
      end;
    end;

    if (aSelectControls.Count>0) then
    begin
      if aSelectControls.Objects[aSelectControls.Count-1] is TWinControl then
      begin
        tempIndex:= tempForm.cbbFilterControl.Items.IndexOf(TWinControl(aSelectControls.Objects[aSelectControls.Count-1]).name);
        if tempIndex>=0 then
        begin
          tempForm.rbCurrForm.Checked:=False;
          tempForm.rbSpecControl.Checked:=true;
          tempForm.cbbFilterControl.ItemIndex:=tempIndex;
        end;
      end;
      aSelectControls.Clear;
    end;

    if tempForm.ShowModal=mrOk then
    begin
      for I := 0 to tempForm.lbDest.Count-1 do
      begin
        tempIndex:=tempForm.FControls.IndexOf(tempForm.lbDest.Items[i]);
        if tempIndex>0 then
        begin
          aSelectControls.AddObject(tempForm.FControls[tempIndex],tempForm.FControls.Objects[tempIndex])
        end;
      end;
      result:=true;
    end;
  finally
    tempForm.Free;
  end;
end;

procedure TABPubDesignSelectControlForm.btnCancelClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TABPubDesignSelectControlForm.btnOKClick(Sender: TObject);
begin
  ModalResult := mrOk;
end;

// 增加选择
procedure TABPubDesignSelectControlForm.actAddExecute(Sender: TObject);
var
  i: Integer;
begin
  BeginUpdateList;
  try
    for i := 0 to lbSource.Items.Count - 1 do
      if lbSource.Selected[i] then
        lbDest.Items.AddObject(lbSource.Items[i], lbSource.Items.Objects[i]);
    for i := lbSource.Items.Count - 1 downto 0 do
      if lbSource.Selected[i] then
        lbSource.Items.Delete(i);
  finally
    EndUpdateList;
  end;
end;

// 增加全部选择
procedure TABPubDesignSelectControlForm.actAddAllExecute(Sender: TObject);
begin
  BeginUpdateList;
  try
    lbDest.Items.AddStrings(lbSource.Items);
    lbSource.Items.Clear;
  finally
    EndUpdateList;
  end;
end;

// 删除选择
procedure TABPubDesignSelectControlForm.actDeleteExecute(Sender: TObject);
var
  i: Integer;
begin
  BeginUpdateList;
  try
    for i := 0 to lbDest.Items.Count - 1 do // 只有当前过滤列表中有的才加入到左边
      if lbDest.Selected[i] and (lbSource.Items.IndexOf(lbDest.Items[i]) < 0) then
        lbSource.Items.AddObject(lbDest.Items[i],lbDest.Items.Objects[i]);
    for i := lbDest.Items.Count - 1 downto 0 do
      if lbDest.Selected[i] then
        lbDest.Items.Delete(i);
  finally
    UpdateSourceOrders;
    EndUpdateList;
  end;
end;

// 删除全部选择
procedure TABPubDesignSelectControlForm.actDeleteAllExecute(Sender: TObject);
var
  i: Integer;
begin
  BeginUpdateList;
  try
    for i := 0 to lbDest.Items.Count - 1 do // 只有当前过滤列表中有的才加入到左边
      if lbSource.Items.IndexOf(lbDest.Items[i])< 0 then
        lbSource.Items.AddObject(lbDest.Items[i],lbDest.Items.Objects[i]);
    lbDest.Items.Clear;
  finally
    UpdateSourceOrders;
    EndUpdateList;
  end;
end;

// 选择全部
procedure TABPubDesignSelectControlForm.actSelAllExecute(Sender: TObject);
var
  i: Integer;
begin
  for i := 0 to lbSource.Items.Count - 1 do
    lbSource.Selected[i] := True;
end;

// 取消选择
procedure TABPubDesignSelectControlForm.actSelNoneExecute(Sender: TObject);
var
  i: Integer;
begin
  for i := 0 to lbSource.Items.Count - 1 do
    lbSource.Selected[i] := False;
end;

// 反转选择
procedure TABPubDesignSelectControlForm.actSelInvertExecute(Sender: TObject);
var
  i: Integer;
begin
  for i := 0 to lbSource.Items.Count - 1 do
    lbSource.Selected[i] := not lbSource.Selected[i];
end;

// 移动到顶部
procedure TABPubDesignSelectControlForm.actMoveToTopExecute(Sender: TObject);
var
  i, j: Integer;
begin
  BeginUpdateList;
  try
    j := 0;
    for i := 0 to lbDest.Items.Count - 1 do
      if lbDest.Selected[i] then
      begin
        lbDest.Items.Move(i, j);
        lbDest.Selected[j] := True;
        Inc(j);
      end;
  finally
    EndUpdateList;
  end;
end;

// 移动到底部
procedure TABPubDesignSelectControlForm.actMoveToBottomExecute(Sender: TObject);
var
  i, j: Integer;
begin
  BeginUpdateList;
  try
    j := lbDest.Items.Count - 1;
    for i := lbDest.Items.Count - 1 downto 0 do
      if lbDest.Selected[i] then
      begin
        lbDest.Items.Move(i, j);
        lbDest.Selected[j] := True;
        Dec(j);
      end;
  finally
    EndUpdateList;
  end;
end;

// 上移一格
procedure TABPubDesignSelectControlForm.actMoveUpExecute(Sender: TObject);
var
  i: Integer;
begin
  BeginUpdateList;
  try
    for i := 1 to lbDest.Items.Count - 1 do
      if lbDest.Selected[i] and not lbDest.Selected[i - 1] then
      begin
        lbDest.Items.Move(i, i - 1);
        lbDest.Selected[i - 1] := True;
      end;
  finally
    EndUpdateList;
  end;
end;

// 下移一格
procedure TABPubDesignSelectControlForm.actMoveDownExecute(Sender: TObject);
var
  i: Integer;
begin
  BeginUpdateList;
  try
    for i := lbDest.Items.Count - 2 downto 0 do
      if lbDest.Selected[i] and not lbDest.Selected[i + 1] then
      begin
        lbDest.Items.Move(i, i + 1);
        lbDest.Selected[i + 1] := True;
      end;
  finally
    EndUpdateList;
  end;
end;

// 字符串列表排序过程
function DoSortProc(List: TStringList; Index1, Index2: Integer): Integer;
var
  Comp1, Comp2: TComponent;
begin
  Comp1 := TComponent(List.Objects[Index1]);
  Comp2 := TComponent(List.Objects[Index2]);
  if SortStyle = ssByName then         // 按名称排序
    Result := AnsiCompareText(Comp1.Name, Comp2.Name)
  else
  begin
    Result := AnsiCompareText(Comp1.ClassName, Comp2.ClassName);
    if Result = 0 then                 // 如果同类再按名称排序
      Result := AnsiCompareText(Comp1.Name, Comp2.Name);
  end;
  if SortDir = sdDown then             // 反向排序
    Result := -Result;
end;


// 对列表框进行排序
procedure DoSortListBox(ListBox: TCustomListBox);
var
  SelStrs: TStrings;
  OrderStrs: TStrings;
  i: Integer;
begin
  SelStrs := nil;
  OrderStrs := nil;
  try
    SelStrs := TStringList.Create;
    OrderStrs := TStringList.Create;
    for i := 0 to ListBox.Items.Count - 1 do // 保存选择的条目
      if ListBox.Selected[i] then
        SelStrs.Add(ListBox.Items[i]);       // ListBox.Items 是 ListBoxStrings 类型
    OrderStrs.Assign(ListBox.Items);         // 不能直接排序，通过 TStringList 来进行
    TStringList(OrderStrs).CustomSort(DoSortProc);
    ListBox.Items.Assign(OrderStrs);
    for i := 0 to ListBox.Items.Count - 1 do // 恢复选择的条目
      ListBox.Selected[i] := SelStrs.IndexOf(ListBox.Items[i]) >= 0;
  finally
    if SelStrs <> nil then SelStrs.Free;
    if OrderStrs <> nil then OrderStrs.Free;
  end;
end;

// 重新对源列表排序
procedure TABPubDesignSelectControlForm.DoUpdateSourceOrder(Sender: TObject);
begin
  if cbbSourceOrderStyle.ItemIndex <= 0 then
    DoUpdateListControls(nil)
  else
    UpdateSourceOrders;
end;


procedure TABPubDesignSelectControlForm.FormCreate(Sender: TObject);
begin
  FControls:=TStringList.Create;
end;

procedure TABPubDesignSelectControlForm.FormDestroy(Sender: TObject);
begin
  FControls.Free;
end;

procedure TABPubDesignSelectControlForm.FormShow(Sender: TObject);
begin
  DoUpdateListControls(nil);
end;

// 对源列表重新进行排序
procedure TABPubDesignSelectControlForm.UpdateSourceOrders;
begin
  RefreshState;

  case cbbSourceOrderStyle.ItemIndex of
    1: SortStyle := ssByName;
    2: SortStyle := ssByClass;
  else
    Exit;
  end;
  if cbbSourceOrderDir.ItemIndex = 1 then
    SortDir := sdDown
  else
    SortDir := sdUp;
  DoSortListBox(lbSource);
end;


procedure TABPubDesignSelectControlForm.DoUpdateListControls(Sender: TObject);
var
  i: Integer;
  SelStrs: TStrings;
  WinControl: TWinControl;

  // 名称是否匹配
  function MatchName(const AName: string): Boolean;
  begin
    Result := not cbByName.Checked or (edtByName.Text = '') or
      (AnsiPos(UpperCase(edtByName.Text), UpperCase(AName)) > 0);
  end;

  // 类型是否匹配
  function MatchClass(AObject: TObject): Boolean;
  begin
    Result := not cbByClass.Checked or (cbbByClass.Text = '') or
      AObject.ClassNameIs(cbbByClass.Text) or
      (cbSubClass.Checked and AObject.InheritsFrom(
      TClass(cbbByClass.Items.Objects[cbbByClass.ItemIndex])));
  end;

  // 增加一项条目
  procedure AddItem(AComponent: TComponent; IncludeChildren: Boolean = False);
  var
    i: Integer;
  begin
    // 判断是否匹配
    if (AComponent.Name <> '') and MatchName(AComponent.Name) and MatchClass(AComponent)
       and (lbSource.Items.IndexOf(AComponent.Name) < 0) then
    begin
      lbSource.Items.AddObject(AComponent.Name,AComponent);
    end;
    // 递归增加子控件
    if IncludeChildren and (AComponent is TWinControl) then
    begin
      for i := 0 to TWinControl(AComponent).ControlCount - 1 do
        AddItem(TWinControl(AComponent).Controls[i], True);
    end;
  end;
begin
  if Sender = rbCurrForm then
  begin
    rbSpecControl.Checked:=false;
  end
  else  if Sender = rbSpecControl then
  begin
    rbCurrForm.Checked:=false;
  end;

  RefreshState;
  BeginUpdateList;
  try
    SelStrs := TStringList.Create;
    try
      for i := 0 to lbSource.Items.Count - 1 do // 保存当前已选择的列表
        if lbSource.Selected[i] then
          SelStrs.Add(lbSource.Items[i]);


      lbSource.Clear;
      if rbCurrForm.Checked then       // 窗体上所有组件
      begin
        for i := 0 to FControls.Count - 1 do
        begin
          if (TControl(FControls.Objects[i]).Name <> '') and
              MatchName(TControl(FControls.Objects[i]).Name) and
              MatchClass(TControl(FControls.Objects[i])) and
              (lbSource.Items.IndexOf(TControl(FControls.Objects[i]).Name) < 0) then
            lbSource.Items.AddObject(FControls[i],FControls.Objects[i]);
        end;
      end
      else if rbSpecControl.Checked then // 指定控件的子控件
      begin
        if cbbFilterControl.ItemIndex >= 0 then
        begin
          WinControl := TWinControl(cbbFilterControl.Items.Objects[cbbFilterControl.ItemIndex]);
            for i := 0 to WinControl.ControlCount - 1 do
              AddItem(WinControl.Controls[i], cbIncludeChildren.Checked);
        end;
      end;
      for i := 0 to lbSource.Items.Count - 1 do // 恢复保存的选择列表
        lbSource.Selected[i] := SelStrs.IndexOf(lbSource.Items[i]) >= 0;
    finally
      SelStrs.Free;
    end;
  finally
    UpdateSourceOrders;
    EndUpdateList;
  end;
end;

// 开始更新列表
procedure TABPubDesignSelectControlForm.BeginUpdateList;
begin
  lbSource.Items.BeginUpdate;
  lbDest.Items.BeginUpdate;
end;

// 结束更新列表
procedure TABPubDesignSelectControlForm.EndUpdateList;
begin
  lbSource.Items.EndUpdate;
  lbDest.Items.EndUpdate;
end;

procedure TABPubDesignSelectControlForm.RefreshState;
begin
  cbbFilterControl.Enabled:= rbSpecControl.Checked;
  cbIncludeChildren.Enabled:= rbSpecControl.Checked;

  edtByName.Enabled:= cbByName.Checked;
  cbbByClass.Enabled:= cbByClass.Checked;
  cbSubClass.Enabled:= cbByClass.Checked;

  cbbSourceOrderDir.Enabled:= cbbSourceOrderStyle.ItemIndex>0;
end;

end.
