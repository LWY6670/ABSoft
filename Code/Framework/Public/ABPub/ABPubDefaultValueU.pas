{
控件属性自动记忆单元
此单元处理窗体中控件属性的自动保存加载，窗体正常关闭时会记录下控件属性，窗体再次显示时会自动加载这些属性，
如某一窗体上的一个Edit1设置了自动记忆，在Edit1中输入字符“abc123”,然后关闭窗体，
再打开窗体，则字符“abc123”就自动显示在Edit1中了。,
控件是否自动记忆属性通过控件的Tag首位为9来设置，格式如下

Tag中首位为9且个位为1
此控件自动保存属性到共用的默认值文件，如保存文本框内容、时间框时间、PageControl的初始页面等
一般为单行的文本属性或单值属性
如91

 Tag中首位为9且十位为1
此控件自动保存属性到单独的默认值文件，如保存备注框的内容
一般为多行的文本属性
如910

Tag中首位为9且百位为1
此控件自动保存大小与位置到共用的默认值文件文件，如保存窗体最后位置
如 9100

以上的标志位也可多位同时为1，如Tag中首位为9且个位十位都为1
此控件自动保存属性到共用的默认值文件和到单独的默认值文件
如911
}
unit ABPubDefaultValueU;

interface
{$I ..\ABInclude.ini}

uses
  ABPubVarU,
  ABPubFuncU,
  ABPubConstU,

  SysUtils,Classes,Controls,Forms,ComCtrls;

//保存所有者下所有子组件的属性
//aOwner=所有者
procedure ABSaveAutoSavePropertys(aOwner: TComponent);overload;
//加载所有者下所有子组件的属性
//aOwner=所有者
procedure ABLoadAutoSavePropertys(aOwner: TComponent);overload;

//保存控件数组中所有组件的属性
//aComponentArray=控件数组
procedure ABSaveAutoSavePropertys(aComponentArray:array of TComponent);overload;
//加载控件数组中所有组件的属性
//aComponentArray=控件数组
procedure ABLoadAutoSavePropertys(aComponentArray:array of TComponent);overload;

//保存组件的属性
//aComponent=需保存属性的组件
procedure ABSaveAutoSaveProperty(aComponent: TComponent);overload;
//加载组件的属性
//aComponent=需保存属性的组件
procedure ABLoadAutoSaveProperty(aComponent: TComponent);overload;

//保存组件的扩展属性，当需要单独对某个属性进行自动保存时使用,将不再受Tag值限制
//aComponent=需保存属性的组件
//aExtOneLineProperty=扩展的单行属性，
//aExtMultiLineProperty=扩展的多行属性，
procedure ABSaveAutoSaveProperty(aComponent: TComponent;
                                 aExtOneLineProperty:array of string;
                                 aExtMultiLineProperty:array of string);overload;
//加载组件的扩展属性，当需要单独对某个属性进行自动加载时使用,将不再受Tag值限制
//aComponent=需保存属性的组件
//aExtOneLineProperty=扩展的单行属性，
//aExtMultiLineProperty=扩展的多行属性，
procedure ABLoadAutoSaveProperty(aComponent: TComponent;
                                 aExtOneLineProperty:array of string;
                                 aExtMultiLineProperty:array of string);overload;

//判断Tag值是否包含指定的操作类型
//aOperatorType=检测的操作类型
//aTag=检测的tag值
function ABCheckOperatorInTag(aOperatorType:TABTagType;
                              aTag:LongInt): boolean;


var
  //自动保存的属性名(保存到共用默认值文件),可在后续程序中进行扩展
  ABAutoSave91PropertyNames,
  //自动保存的属性名(保存到单个默认值文件),可在后续程序中进行扩展
  ABAutoSave910PropertyNames:string;

implementation


var
  ABAutoLoadStrings:TStrings;     //自动保存属性共用的TStrings
  ABPositionSize:array[1..4] of string=('left','top','width','height');

function ABCheckOperatorInTag(aOperatorType:TABTagType;aTag:LongInt): boolean;
var
  tempTagstrLength: LongInt;
  tempTagstr: string;
begin
  result:=False;
  tempTagstr:=inttostr(aTag);
  tempTagstrLength:=Length(tempTagstr);
  if (tempTagstrLength>0) and
     (tempTagstr[1]='9') then
  begin
    case aOperatorType of
      ttPubAutoSave_91:
      begin
        result:=tempTagstr[tempTagstrLength]='1';
      end;
      ttSingleAutoSave_910:
      begin
        if tempTagstrLength-1>0 then
          result:=tempTagstr[tempTagstrLength-1]='1';
      end;
      ttAutoPositionSize_9100:
      begin
        if tempTagstrLength-2>0 then
          result:=tempTagstr[tempTagstrLength-2]='1';
      end;
      ttNotCheckDataSetOnCloseForm_91000:
      begin
        if tempTagstrLength-4>0 then
          result:=tempTagstr[tempTagstrLength-4]='1';
      end;
      tt_910000:
      begin
        if tempTagstrLength-5>0 then
          result:=tempTagstr[tempTagstrLength-5]='1';
      end;
      tt_9100000:
      begin
        if tempTagstrLength-6>0 then
          result:=tempTagstr[tempTagstrLength-6]='1';
      end;
      tt_91000000:
      begin
        if tempTagstrLength-7>0 then
          result:=tempTagstr[tempTagstrLength-7]='1';
      end;
    end;
  end;
end;

procedure ABSaveAutoSaveProperty(aComponent: TComponent;aExtOneLineProperty:array of string;aExtMultiLineProperty:array of string);
var
  j: LongInt;
  tempPropertyValue: string;
  tempStrings: TStrings;
  tempFileName: string;
begin
  tempFileName := ABDefaultValuePath+'PubDefaultValue.ini';

  for j := Low(aExtOneLineProperty) to High(aExtOneLineProperty) do
  begin
    if ABIsPublishProp(aComponent, aExtOneLineProperty[j]) then
    begin
      tempPropertyValue := ABGetPropValue(aComponent, aExtOneLineProperty[j]);
      ABWriteInI(aComponent.Owner.Name, aComponent.Name + '.' +
        aExtOneLineProperty[j], tempPropertyValue, tempFileName);
    end
    else
    begin
      if (aComponent is TPageControl) and (AnsiCompareText('ActivePageIndex',aExtOneLineProperty[j])=0) then
      begin
        tempPropertyValue := IntToStr(TPageControl(aComponent).ActivePageIndex);
        ABWriteInI(aComponent.Owner.Name, aComponent.Name + '.' +
          aExtOneLineProperty[j], tempPropertyValue, tempFileName);
      end;
    end;
  end;


  for j := Low(aExtMultiLineProperty) to High(aExtMultiLineProperty) do
  begin
    if ABIsPublishProp(aComponent, aExtMultiLineProperty[j]) then
    begin
      tempFileName := ABDefaultValuePath+'SingleDefaultValue_' +
        aComponent.Owner.Name + '_' + aComponent.Name + '_' +
        aExtMultiLineProperty[j] + '.txt';

      tempStrings := ABGetStringsProperty(aComponent, aExtMultiLineProperty[j]);
      if Assigned(tempStrings) then
        tempStrings.SaveToFile(tempFileName)
      else
      begin
        tempPropertyValue := ABGetPropValue(aComponent,aExtMultiLineProperty[j]);
        ABWriteTxt(tempFileName, tempPropertyValue);
      end;
    end;
  end;
end;

procedure ABLoadAutoSaveProperty(aComponent: TComponent;aExtOneLineProperty:array of string;aExtMultiLineProperty:array of string);
var
  j,k,
  tempIndex: LongInt;
  tempPropertyName, tempPropertyValue,
  tempSelfFileName: string;
  tempStrings: TStrings;
begin
  if (aComponent.Tag>0) then
  begin
    if inttostr(aComponent.Tag)[1]='9' then
    begin
      //首位为9表示是与自动保存属性相关
      //Tag中个位为1
      //此控件自动保存属性到共用的默认值文件
      //此可推广到保存窗体最后位置,PageControl的初始页面等应用中
      //一般为单行的文本属性或单值属性
      //如91
      if (ABAutoLoadStrings.Count>0) then
      begin
        if ABCheckOperatorInTag(ttPubAutoSave_91,aComponent.Tag) then
        begin
          for j := 1 to ABGetSpaceStrCount(ABAutoSave91PropertyNames, ',') do
          begin
            tempPropertyName := ABGetSpaceStr(ABAutoSave91PropertyNames, j,',');
            tempIndex:= ABAutoLoadStrings.IndexOfName(aComponent.Name + '.' + tempPropertyName);
            if tempIndex>=0 then
            begin
              tempPropertyValue :=ABAutoLoadStrings.ValueFromIndex[tempIndex];
              if ABIsPublishProp(aComponent, tempPropertyName) then
              begin
                if AnsiCompareText(tempPropertyName, 'Value') = 0 then
                begin
                  ABSetPropValue(aComponent, tempPropertyName,StrToIntDef(tempPropertyValue, 0));
                end
                else
                  ABSetPropValue(aComponent, tempPropertyName,tempPropertyValue);
              end
              else
              begin
                if (aComponent is TPageControl) and (AnsiCompareText('ActivePageIndex',tempPropertyName)=0) then
                begin
                  TPageControl(aComponent).ActivePageIndex:=StrToInt(tempPropertyValue);
                end;
              end;
            end;
          end;
        end;
        //Tag中百位都为1
        //此控件自动保存大小与位置到共用的默认值文件文件
        //如9100
        if ABCheckOperatorInTag(ttAutoPositionSize_9100,aComponent.Tag) then
        begin
          for k := 1 to 4 do
          begin
            tempIndex:= ABAutoLoadStrings.IndexOfName(aComponent.Name + '.' + ABPositionSize[k]);
            if tempIndex>=0 then
            begin
              tempPropertyValue :=ABAutoLoadStrings.ValueFromIndex[tempIndex];
              if ABIsPublishProp(aComponent, ABPositionSize[k]) then
              begin
                ABSetPropValue(aComponent, ABPositionSize[k],StrToIntDef(tempPropertyValue,0));
              end
            end;
          end;
        end;
      end;
      // Tag中十位为1
      //此控件自动保存属性到单独的默认值文件
      //一般为多行的文本属性
      //如910
      if ABCheckOperatorInTag(ttSingleAutoSave_910,aComponent.Tag) then
      begin
        for j := 1 to ABGetSpaceStrCount(ABAutoSave910PropertyNames, ',') do
        begin
          tempPropertyName := ABGetSpaceStr(ABAutoSave910PropertyNames, j, ',');
          if ABIsPublishProp(aComponent, tempPropertyName) then
          begin
            tempSelfFileName := ABDefaultValuePath+'SingleDefaultValue_' +
              aComponent.Owner.Name + '_' + aComponent.Name + '_' +
              tempPropertyName + '.txt';

            if ABCheckFileExists(tempSelfFileName) then
            begin
              tempStrings := ABGetStringsProperty(aComponent,tempPropertyName);
              if Assigned(tempStrings) then
                tempStrings.LoadFromFile(tempSelfFileName)
              else
              begin
                tempPropertyValue := ABReadTxt(tempSelfFileName);
                ABSetPropValue(aComponent, tempPropertyName, tempPropertyValue);
              end;
            end;
          end;
        end;
      end;
    end;
  end;
end;

procedure ABSaveAutoSaveProperty(aComponent: TComponent);
var
  j,k: LongInt;
  tempPropertyName, tempPropertyValue,
  tempSelfFileName: string;
  tempStrings: TStrings;
  tempPubFileName: string;
begin
  tempPubFileName := ABDefaultValuePath+'PubDefaultValue.ini';

  if (aComponent.Tag>0) then
  begin
    if inttostr(aComponent.Tag)[1]='9' then
    begin
      //首位为9表示是与自动保存属性相关
      //Tag中个位为1
      //此控件自动保存属性到共用的默认值文件
      //此可推广到保存窗体最后位置,PageControl的初始页面等应用中
      //一般为单行的文本属性或单值属性
      //如91
      if ABCheckOperatorInTag(ttPubAutoSave_91,aComponent.Tag) then
      begin
        for j := 1 to ABGetSpaceStrCount(ABAutoSave91PropertyNames, ',') do
        begin
          tempPropertyName := ABGetSpaceStr(ABAutoSave91PropertyNames, j, ',');
          if ABIsPublishProp(aComponent, tempPropertyName) then
          begin
            tempPropertyValue := ABGetPropValue(aComponent, tempPropertyName);
            ABWriteInI(aComponent.Owner.Name, aComponent.Name + '.' +
              tempPropertyName, tempPropertyValue, tempPubFileName);
          end
          else
          begin
            if (aComponent is TPageControl) and (AnsiCompareText('ActivePageIndex',tempPropertyName)=0) then
            begin
              tempPropertyValue := IntToStr(TPageControl(aComponent).ActivePageIndex);
              ABWriteInI(aComponent.Owner.Name, aComponent.Name + '.' +
                tempPropertyName, tempPropertyValue, tempPubFileName);
            end;
          end;
        end;
      end;

      //Tag中百位都为1
      //此控件自动保存大小与位置到共用的默认值文件文件
      //如9100
      if ABCheckOperatorInTag(ttAutoPositionSize_9100,aComponent.Tag) then
      begin
        for k := 1 to 4 do
        begin
          tempPropertyName :=ABPositionSize[k];
          if ABIsPublishProp(aComponent, tempPropertyName) then
          begin
            tempPropertyValue := ABGetPropValue(aComponent, tempPropertyName);
            ABWriteInI(aComponent.Owner.Name, aComponent.Name + '.' +
              tempPropertyName, tempPropertyValue, tempPubFileName);
          end;
        end;
      end;

      // Tag中十位为1
      //此控件自动保存属性到单独的默认值文件
      //一般为多行的文本属性
      //如910
      if ABCheckOperatorInTag(ttSingleAutoSave_910,aComponent.Tag) then
      begin
        for j := 1 to ABGetSpaceStrCount(ABAutoSave910PropertyNames, ',') do
        begin
          tempPropertyName := ABGetSpaceStr(ABAutoSave910PropertyNames, j, ',');
          if ABIsPublishProp(aComponent, tempPropertyName) then
          begin
            tempSelfFileName := ABDefaultValuePath+'SingleDefaultValue_' +
              aComponent.Owner.Name + '_' + aComponent.Name + '_' +
              tempPropertyName + '.txt';

            tempStrings := ABGetStringsProperty(aComponent, tempPropertyName);
            if Assigned(tempStrings) then
              tempStrings.SaveToFile(tempSelfFileName)
            else
            begin
              tempPropertyValue := ABGetPropValue(aComponent,tempPropertyName);
              ABWriteTxt(tempSelfFileName, tempPropertyValue);
            end;
          end;
        end;
      end;
    end;
  end;
end;

procedure ABLoadAutoSaveProperty(aComponent: TComponent);
var
  j,k,
  tempIndex: LongInt;
  tempPropertyName, tempPropertyValue,
  tempSelfFileName: string;
  tempStrings: TStrings;
begin
  if (aComponent.Tag>0) then
  begin
    if inttostr(aComponent.Tag)[1]='9' then
    begin
      //首位为9表示是与自动保存属性相关
      //Tag中个位为1
      //此控件自动保存属性到共用的默认值文件
      //此可推广到保存窗体最后位置,PageControl的初始页面等应用中
      //一般为单行的文本属性或单值属性
      //如91
      if (ABAutoLoadStrings.Count>0) then
      begin
        if ABCheckOperatorInTag(ttPubAutoSave_91,aComponent.Tag) then
        begin
          for j := 1 to ABGetSpaceStrCount(ABAutoSave91PropertyNames, ',') do
          begin
            tempPropertyName := ABGetSpaceStr(ABAutoSave91PropertyNames, j,',');
            tempIndex:= ABAutoLoadStrings.IndexOfName(aComponent.Name + '.' + tempPropertyName);
            if tempIndex>=0 then
            begin
              tempPropertyValue :=ABAutoLoadStrings.ValueFromIndex[tempIndex];
              if ABIsPublishProp(aComponent, tempPropertyName) then
              begin
                if AnsiCompareText(tempPropertyName, 'Value') = 0 then
                begin
                  ABSetPropValue(aComponent, tempPropertyName,StrToIntDef(tempPropertyValue, 0));
                end
                else
                  ABSetPropValue(aComponent, tempPropertyName,tempPropertyValue);
              end
              else
              begin
                if (aComponent is TPageControl) and (AnsiCompareText('ActivePageIndex',tempPropertyName)=0) then
                begin
                  TPageControl(aComponent).ActivePageIndex:=StrToInt(tempPropertyValue);
                end;
              end;
            end;
          end;
        end;
        //Tag中百位都为1
        //此控件自动保存大小与位置到共用的默认值文件文件
        //如9100
        if ABCheckOperatorInTag(ttAutoPositionSize_9100,aComponent.Tag) then
        begin
          for k := 1 to 4 do
          begin
            tempIndex:= ABAutoLoadStrings.IndexOfName(aComponent.Name + '.' + ABPositionSize[k]);
            if tempIndex>=0 then
            begin
              tempPropertyValue :=ABAutoLoadStrings.ValueFromIndex[tempIndex];
              if ABIsPublishProp(aComponent, ABPositionSize[k]) then
              begin
                ABSetPropValue(aComponent, ABPositionSize[k],StrToIntDef(tempPropertyValue,0));
              end
            end;
          end;
        end;
      end;
      // Tag中十位为1
      //此控件自动保存属性到单独的默认值文件
      //一般为多行的文本属性
      //如910
      if ABCheckOperatorInTag(ttSingleAutoSave_910,aComponent.Tag) then
      begin
        for j := 1 to ABGetSpaceStrCount(ABAutoSave910PropertyNames, ',') do
        begin
          tempPropertyName := ABGetSpaceStr(ABAutoSave910PropertyNames, j, ',');
          if ABIsPublishProp(aComponent, tempPropertyName) then
          begin
            tempSelfFileName := ABDefaultValuePath+'SingleDefaultValue_' +
              aComponent.Owner.Name + '_' + aComponent.Name + '_' +
              tempPropertyName + '.txt';

            if ABCheckFileExists(tempSelfFileName) then
            begin
              tempStrings := ABGetStringsProperty(aComponent,tempPropertyName);
              if Assigned(tempStrings) then
                tempStrings.LoadFromFile(tempSelfFileName)
              else
              begin
                tempPropertyValue := ABReadTxt(tempSelfFileName);
                ABSetPropValue(aComponent, tempPropertyName, tempPropertyValue);
              end;
            end;
          end;
        end;
      end;
    end;
  end;
end;

procedure ABLoadAutoSavePropertys(aOwner: TComponent);
var
  i: LongInt;
begin
  if (aOwner is TCustomForm) or (aOwner is TCustomFrame)  then
    ABReadInI(aOwner.Name,ABDefaultValuePath+'PubDefaultValue.ini',ABAutoLoadStrings)
  else
    ABReadInI(ABGetParentForm(TControl(aOwner)).Name,ABDefaultValuePath+'PubDefaultValue.ini',ABAutoLoadStrings);

  ABLoadAutoSaveProperty(aOwner);
  for I := 0 to aOwner.ComponentCount - 1 do
  begin
    ABLoadAutoSaveProperty(aOwner.Components[i]);
  end;
end;

procedure ABSaveAutoSavePropertys(aOwner: TComponent);
var
  i: LongInt;
begin
  ABSaveAutoSaveProperty(aOwner);
  for I := 0 to aOwner.ComponentCount - 1 do
  begin
    ABSaveAutoSaveProperty(aOwner.Components[i]);
  end;
end;

procedure ABSaveAutoSavePropertys(aComponentArray:array of TComponent);
var
  i: LongInt;
begin
  for I := Low(aComponentArray) to High(aComponentArray) do
  begin
    ABSaveAutoSaveProperty(aComponentArray[i]);
  end;
end;

procedure ABLoadAutoSavePropertys(aComponentArray:array of TComponent);
var
  i: LongInt;
begin
  if High(aComponentArray)>=0 then
  begin
    if (aComponentArray[0].Owner is TCustomForm) or (aComponentArray[0].Owner is TCustomFrame)  then
      ABReadInI(aComponentArray[0].Owner.Name,ABDefaultValuePath+'PubDefaultValue.ini',ABAutoLoadStrings)
    else
      ABReadInI(ABGetParentForm(TControl(aComponentArray[0].Owner)).Name,ABDefaultValuePath+'PubDefaultValue.ini',ABAutoLoadStrings);

    for I := Low(aComponentArray) to High(aComponentArray) do
    begin
      ABLoadAutoSaveProperty(aComponentArray[i]);
    end;
  end;
end;

procedure ABInitialization;
begin
  //自动保存多行属性的控件属性串
  //对于一些属性的设置需要其它属性的支持，如ABcxComboBox设置ItemIndex或text时要先在properties.items中设置值
  //则properties.items属性要在调用LoadAutoSavePropert;之前进行设置，
  //如在constructor Create(AOwner: TComponent);前设置
  ABAutoSave91PropertyNames := 'ItemIndex,checked,text,value,down,ActivePageIndex,Date,Time,Datetime,Color,ColorValue,EditValue,HotKey,ShowText';
  ABAutoSave910PropertyNames := 'Properties.Items.text,Items.text,lines.text,strings.text';

  ABAutoLoadStrings:=TStringList.Create;
end;

procedure ABFinalization;
begin
  ABAutoLoadStrings.Free;
end;

Initialization
  ABInitialization;

Finalization
  ABFinalization;


end.
