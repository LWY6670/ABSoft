object ABSelectCheckTreeViewForm: TABSelectCheckTreeViewForm
  Left = 0
  Top = 0
  Caption = #22810#36873
  ClientHeight = 500
  ClientWidth = 700
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 459
    Width = 700
    Height = 41
    Align = alBottom
    TabOrder = 0
    DesignSize = (
      700
      41)
    object btn1: TButton
      Left = 616
      Top = 6
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #21462#28040
      TabOrder = 3
      OnClick = btn1Click
    end
    object btn2: TButton
      Left = 535
      Top = 6
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #30830#35748
      TabOrder = 4
      OnClick = btn2Click
    end
    object ABcxButton1: TButton
      Left = 4
      Top = 6
      Width = 75
      Height = 25
      Caption = #20840#36873
      TabOrder = 0
      OnClick = ABcxButton1Click
    end
    object ABcxButton2: TButton
      Left = 85
      Top = 6
      Width = 75
      Height = 25
      Caption = #20840#19981#36873
      TabOrder = 1
      OnClick = ABcxButton2Click
    end
    object ABcxButton3: TButton
      Left = 166
      Top = 6
      Width = 75
      Height = 25
      Caption = #21453#21521#36873#25321
      TabOrder = 2
      OnClick = ABcxButton3Click
    end
  end
  object RzCheckTree3: TABCheckTreeView
    Left = 0
    Top = 0
    Width = 700
    Height = 459
    MoveAfirm = False
    Align = alClient
    Ctl3D = False
    Flatness = cfAlwaysFlat
    GrayedIsChecked = False
    HideSelection = False
    Indent = 19
    ParentCtl3D = False
    SortType = stText
    TabOrder = 1
  end
end
