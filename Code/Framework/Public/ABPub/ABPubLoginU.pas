{
����Ա��¼��Ԫ
}
unit ABPubLoginU;

interface
{$I ..\ABInclude.ini}


uses
  ABPubFormU,

  SysUtils,Classes,Controls,Forms,StdCtrls,ExtCtrls, jpeg;

type
  TABPubLoginForm = class(TABPubForm)
    Image1: TImage;
    chkAutoLogin: TCheckBox;
    btnOkLogin: TButton;
    btnCancelLogin: TButton;
    leUser: TEdit;
    lePassword: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    procedure lePasswordKeyPress(Sender: TObject; var Key: Char);
    procedure leUserKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure chkAutoLoginKeyPress(Sender: TObject; var Key: Char);
    procedure Label3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;


implementation

{$R *.dfm}                         

procedure TABPubLoginForm.leUserKeyPress(Sender: TObject; var Key: Char);
begin
  if (key= #13)  then
    if lePassword.CanFocus then
      lePassword.SetFocus;
end;

procedure TABPubLoginForm.chkAutoLoginKeyPress(Sender: TObject; var Key: Char);
begin
  if (key= #13) then
  begin
    ModalResult:=mrOk
  end;
end;

procedure TABPubLoginForm.FormShow(Sender: TObject);
begin
  if not leUser.Enabled then
  begin
    if lePassword.CanFocus then
      lePassword.SetFocus;
  end
  else if leUser.CanFocus then
  begin
    if leUser.CanFocus then
      leUser.SetFocus;
  end;
end;

procedure TABPubLoginForm.lePasswordKeyPress(Sender: TObject; var Key: Char);
begin
  if (key= #13) then
  begin
    if (leUser.Text<>EmptyStr) then
      ModalResult:=mrOk
    else
    begin
      if leUser.CanFocus then
        leUser.SetFocus;
    end;
  end;
end;


procedure TABPubLoginForm.Label3Click(Sender: TObject);
begin
  chkAutoLogin.Checked:=not chkAutoLogin.Checked;
end;

end.
