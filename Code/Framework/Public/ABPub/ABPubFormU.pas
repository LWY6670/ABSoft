{
共用基类窗体单元(其下的窗体都具有多语文功能)

窗体关闭时默认采用的是  FCloseAction:=caHide;
  关闭动作时为隐藏窗体,可SHOWMODEL后再处理数据,最后释放窗体,
  show时的关闭并未释放,要手动再释放

当退出窗体时如果有数据处于编辑或增加状态,则进行提示(TDataSet.Tag为91000表示此TDataSet不进行提示)
}
unit ABPubFormU;


interface
{$I ..\ABInclude.ini}

uses
  ABPubLanguageU,
  ABPubLocalParamsU,
  ABPubLogU,
  ABPubFuncU,
  ABPubConstU,
  ABPubMessageU,
  ABPubDefaultValueU,
  ABPubDesignU,

  Windows,SysUtils,Variants,Classes,Messages,Controls,Forms,DB;

const
  //系统菜单中开始设计子菜单消息
  ABPubFormBeginDesign_Msg=WM_USER+120;
  //系统菜单中结束设计子菜单消息
  ABPubFormEndDesign_Msg=WM_USER+130;

type
  //不能在基类窗体中写事件操作如FormCloseQuery等，
  //在非传统继承下(如ABSOFT采用的)子类的事件函数会覆盖父類的FormCloseQuery

  TABFormClass = class of TABPubForm;
  TABPubForm = class(TForm)
  private
    FAddCaption,
    FBegShowCaption:string;

    FCloseIsCheckQuery: Boolean;
    FCloseAction: TCloseAction;
    FLoadAutoSavePropertyIng: Boolean;
    FPubStrings: Tstrings;
    FCloseQueryIng: Boolean;
    FFormCreateOk: Boolean;
    FFileName: string;
    FIsTemplate: Boolean;

    procedure SetAddCaption(const Value: string);
    procedure SetPubStrings(const Value: TStrings);
    { Private declarations }
  protected
    procedure DoCreate ; override;
    procedure DoDestroy; override;
    procedure DoShow; override;
    procedure DoClose(var Action: TCloseAction); override;

    procedure CreateParams(var Params: TCreateParams); override;
    procedure DoHide; override;
    procedure Loaded; override;
    procedure ReadState(Reader: TReader); override;
     { Private declarations }
  public
    property PubStrings:TStrings read FPubStrings write SetPubStrings;
    function PubFunction:Boolean;  virtual;

    function CloseQuery:Boolean; override;
    constructor Create(AOwner: TComponent);override;
    constructor CreateAndSetName(AOwner: TComponent;aIsTemplate:boolean;aFileName:string);
    destructor Destroy; override;

    property AddCaption:string read FAddCaption write SetAddCaption  ;
    procedure LoadAutoSavePropert;

    property CloseQueryIng:Boolean read FCloseQueryIng ;
    property LoadAutoSavePropertyIng:Boolean read FLoadAutoSavePropertyIng;
    property CloseIsCheckQuery:Boolean read FCloseIsCheckQuery write FCloseIsCheckQuery  ;
    property CloseAction:TCloseAction read FCloseAction write FCloseAction;
    property FormCreateOk:Boolean read FFormCreateOk write FFormCreateOk  ;

    property IsTemplate:Boolean read FIsTemplate write FIsTemplate  ;
    property FileName:string read FFileName write FFileName  ;
    { Public declarations }
  published
  end;

//当退出窗体时如果有数据处于编辑或增加状态,则进行提示
procedure ABBeforeCloseForm(aOwner: TComponent; aDataSets: array of TDataSet; var CanClose: Boolean);

var
  //是否关闭窗体的事情
  ABCloseFormEvent:boolean;
implementation

{$R *.dfm}



{$IF (CompilerVersion<20.0)}   //200=DELPHI 2009
type
  TABFuncReadWideString = function: WideString of Object;
  TABFuncReadString = function: String of Object;

  TABReader=class(TReader)
  private
  protected
    function NewReadWideString: WideString;
    function NewReadString: string;
  public
    constructor Create(Stream: TStream; BufSize: Integer);
    destructor Destroy; override;
  end;

  TABResourceStream=class(TResourceStream)
  public
    function ReadComponent(Instance: TComponent): TComponent;
  end;

function ABInternalReadComponentRes(const ResName: string; HInst: THandle; var Instance: TComponent): Boolean;
var
  HRsrc: THandle;
  tempResourceStream:TABResourceStream;
begin                   { avoid possible EResNotFound exception }
  if HInst = 0 then HInst := HInstance;
  HRsrc := FindResource(HInst, PChar(ResName), RT_RCDATA);
  Result := HRsrc <> 0;
  if not Result then Exit;
  tempResourceStream:=TABResourceStream.Create(HInst, ResName, RT_RCDATA);
  try
    Instance := tempResourceStream.ReadComponent(Instance);
  finally
    tempResourceStream.Free;
  end;
  Result := True;
end;

function ABInitInheritedComponent(Instance: TComponent; RootAncestor: TClass): Boolean;

  function InitComponent(ClassType: TClass): Boolean;
  begin
    Result := False;
    if (ClassType = TComponent) or (ClassType = RootAncestor) then Exit;
    Result := InitComponent(ClassType.ClassParent);
    Result := ABInternalReadComponentRes(ClassType.ClassName, FindResourceHInstance(
      FindClassHInstance(ClassType)), Instance) or Result;
  end;

var
  LocalizeLoading: Boolean;
begin
  GlobalNameSpace.BeginWrite;  // hold lock across all ancestor loads (performance)
  try
    LocalizeLoading := (Instance.ComponentState * [csInline, csLoading]) = [];
    if LocalizeLoading then BeginGlobalLoading;  // push new loadlist onto stack
    try
      Result := InitComponent(Instance.ClassType);
      if LocalizeLoading then NotifyGlobalLoading;  // call Loaded
    finally
      if LocalizeLoading then EndGlobalLoading;  // pop loadlist off stack
    end;
  finally
    GlobalNameSpace.EndWrite;
  end;
end;

{ TABResourceStream }

function TABResourceStream.ReadComponent(Instance: TComponent): TComponent;
var
  Reader: TABReader;
begin
  Reader := TABReader.Create(Self, 4096);
  try

    Result := Reader.ReadRootComponent(Instance);
  finally
    Reader.Free;
  end;
end;

{ TABReader }

constructor TABReader.Create(Stream: TStream; BufSize: Integer);
var
  OldFunc1,NewFunc1:TABFuncReadWideString;
  POldFunc1,PNewFunc1:Pointer;

  OldFunc2,NewFunc2:TABFuncReadString;
  POldFunc2,PNewFunc2:Pointer;

begin
  inherited Create(Stream,BufSize);


  OldFunc1:=ReadWideString;
  POldFunc1:=@OldFunc1;

  NewFunc1:=NewReadWideString;
  PNewFunc1:=@NewFunc1;

  ABHook_Func(POldFunc1,PNewFunc1);

  OldFunc2:=ReadString;
  POldFunc2:=@OldFunc2;
  NewFunc2:=NewReadString;
  PNewFunc2:=@NewFunc2;
  ABHook_Func(POldFunc2,PNewFunc2);
end;

destructor TABReader.Destroy;
begin

  inherited;
end;

function TABReader.NewReadString: string;
var
  L: Integer;
begin
  if NextValue in [vaWString, vaUTF8String] then
  begin
    Result := ReadWideString;
  end
  else
  begin
    L := 0;
    case ReadValue of
      vaString:
        Read(L, SizeOf(Byte));
      vaLString:
        Read(L, SizeOf(Integer));
    else
      raise EReadError.CreateRes(@SInvalidPropertyValue);
    end;
    SetLength(Result, L);
    Read(Pointer(Result)^, L);
  end;
end;

function TABReader.NewReadWideString: WideString;
  //D2009以前，因为汉字保存的是Unicode,所以在简繁处理时要先用此函数转换一下才能再进行后续的处理
  function UnicodeToStrAfterNewReadWideString(  aStr: WideString): string;
  var
    nLen: integer;
  begin
    Result := aStr;
    if Result <> '' then
    begin
      nLen := WideCharToMultiByte(936, 624, @aStr[1], -1, nil, 0, nil, nil);
      SetLength(Result, nLen - 1);
      if nLen > 1 then
        WideCharToMultiByte(936, 624, @aStr[1], -1, @Result[1], nLen - 1, nil,
          nil);
    end;
  end;
var
  L: Integer;
  Temp: UTF8String;
  tempStr:AnsiString;
begin
  if NextValue in [vaString, vaLString] then
    Result := ReadString
  else
  begin
    L := 0;
    case ReadValue of
      vaWString:
        begin
          Read(L, SizeOf(Integer));
          SetLength(Result, L);
          Read(Pointer(Result)^, L * 2);
        end;
      vaUTF8String:
        begin
          Read(L, SizeOf(Integer));
          SetLength(Temp, L);
          Read(Pointer(Temp)^, L);
          Result := Utf8Decode(Temp);
        end;
    else
      raise EReadError.CreateRes(@SInvalidPropertyValue);
    end;
  end;

  //以下处理是为简繁体做的，经过以下处理，可认为简体显示的是GB简体，繁体显示的BIG5繁体
  //begin-------------------
  tempStr:=UnicodeToStrAfterNewReadWideString(Result);
  if ABHaveChinese(tempStr) then
  begin
    //简体中文环境编译
    if ABLocalParams.SourceCodeChineseType=ctGBChs then
    begin
      if (ABGetSysLanguage=wlChinaTw) then
      begin
        result :=ABGBToBig5(tempStr);
      end;
    end
    //繁体中文环境编译
    else if ABLocalParams.SourceCodeChineseType=ctBIG5 then
    begin
      if (ABGetSysLanguage=wlChina) then
      begin
        result :=ABBig5ToGB(tempStr);
      end;
    end
  end;
  //end-------------------

  //D2009以前，如果用ShowMessage因为参数会先转化为ansistring，所以显示出来的会是乱码，而用 MessageBoxExW则不会
  //MessageBoxExW(0,Pwidechar(Result),'',MB_Ok,0);
  //ShowMessage(result);
end;
{$IFEND}

procedure ABBeforeCloseForm(aOwner: TComponent; aDataSets: array of TDataSet; var
  CanClose: Boolean);
var
  i, j: LongInt;
  tempNoSaveList: TList;
  procedure  AddtopNoSaveList( aDataSet:TDataSet);
  begin
    if tempNoSaveList.IndexOf(aDataSet)<0 then
    begin
      tempNoSaveList.Add(aDataSet);
    end;
  end;
  procedure  SetList( aDataSet:TDataSet);
  begin
    if (aDataSet.Tag <> 19001) then
    begin
      if (aDataSet is TDataSet) and
         (aDataSet.State in [dsedit, dsinsert]) then
      begin
        if (Assigned(aDataSet.DataSource)) and
           (Assigned(aDataSet.DataSource.DataSet))
            then
        begin
          if (Assigned(aDataSet.DataSource.DataSet.DataSource)) and
             (Assigned(aDataSet.DataSource.DataSet.DataSource.DataSet))
              then
          begin
            AddtopNoSaveList(aDataSet.DataSource.DataSet.DataSource.DataSet);
          end;

          AddtopNoSaveList(aDataSet.DataSource.DataSet);
        end;

        AddtopNoSaveList(aDataSet);
      end;
    end;
  end;
begin
  if (ABLocalParams.Debug) then
    ABWriteLog('Begin procedure ABBeforeCloseForm('+aOwner.Name+')');

  CanClose := true;
  tempNoSaveList := TList.Create;
  try
    if (Assigned(aOwner)) and
       (High(aDataSets) <= 0)  then
    begin
      for I := 0 to aOwner.ComponentCount - 1 do
      begin
        if (aOwner.Components[i] is TDataSet) then
          SetList(TDataSet(aOwner.Components[i]));
      end;
    end
    else if (not Assigned(aOwner)) and
            (High(aDataSets) > 0)  then
    begin
      for i := Low(aDataSets) to high(aDataSets) do
      begin
        SetList(aDataSets[i]);
      end;
    end;

    if tempNoSaveList.Count > 0 then
    begin
      j := ABshow('是否要保存未保存的数据',[],['是','否','取消'],1);
      case j of
        1:
        begin
          try
            for i := 0 to tempNoSaveList.Count - 1  do
            begin
              if TDataSet(tempNoSaveList[i]).State in [dsEdit,dsInsert] then
                TDataSet(tempNoSaveList[i]).Post;
            end;
            CanClose := true;
          except
            CanClose := false;
            raise;
          end;
        end;
        2:
        begin
          try
            for i := 0 to  tempNoSaveList.Count - 1  do
            begin
              if TDataSet(tempNoSaveList[i]).State in [dsEdit,dsInsert] then
                TDataSet(tempNoSaveList[i]).Cancel;
            end;
            CanClose := true;
          except
            CanClose := false;
            raise;
          end;
        end;
        3:
        begin
          CanClose := false;
        end;
      end;
    end;
  finally
    tempNoSaveList.Free;
  end;

  if (ABLocalParams.Debug) then
    ABWriteLog('End   procedure ABBeforeCloseForm('+aOwner.Name+')');
end;


{ TABPubForm }
procedure TABPubForm.SetAddCaption(const Value: string);
begin
  FAddCaption := Value;
  if FAddCaption<>emptystr then
    caption:=FBegShowCaption+' '+FAddCaption
  else
    caption:=FBegShowCaption;
end;

procedure TABPubForm.SetPubStrings(const Value: TStrings);
begin
  FPubStrings.Assign(Value);
end;

function TABPubForm.PubFunction: Boolean;
begin
  Result:=false;
end;

procedure TABPubForm.LoadAutoSavePropert;
begin
  if (ABLocalParams.Debug) then
    ABWriteLog('Begin procedure '+Name+'.LoadAutoSavePropert');

  if not FLoadAutoSavePropertyIng then
  begin
    FLoadAutoSavePropertyIng:=true ;
    try
      ABLoadAutoSavePropertys(self);
    finally
      FLoadAutoSavePropertyIng:=false;
    end;
  end;

  if (ABLocalParams.Debug) then
    ABWriteLog('End   procedure '+Name+'.LoadAutoSavePropert');
end;

//创建窗体的第1步
constructor TABPubForm.Create(AOwner: TComponent);
begin
  if (ABLocalParams.Debug) then
    ABWriteLog('Begin procedure '+Name+'.Create');

  FPubStrings:= TStringList.Create;
  FCloseIsCheckQuery:=True;
  FCloseAction:=caHide;

  {$IF (CompilerVersion<20.0)}   //200=DELPHI 2009
    //inherited;
    //重写共用窗体的Create,用来解决D2009之前简体编译到繁体环境下显示不了的问题
    GlobalNameSpace.BeginWrite;
    try
      CreateNew(AOwner);
      if (ClassType <> TForm) and not (csDesigning in ComponentState) then
      begin
        Include(FFormState, fsCreating);
        try
          if not ABInitInheritedComponent(Self, TForm) then
            raise EResNotFound.CreateFmt(SResNotFound, [ClassName]);
        finally
          Exclude(FFormState, fsCreating);
        end;
        if OldCreateOrder then DoCreate;
      end;
    finally
      GlobalNameSpace.EndWrite;
    end;
  {$ELSE}
    inherited Create(AOwner);
  {$IFEND}

  if (ABLocalParams.Debug) then
    ABWriteLog('End   procedure '+Name+'.Create');
end;

constructor TABPubForm.CreateAndSetName(AOwner: TComponent;aIsTemplate:boolean;aFileName:string);
begin
  FIsTemplate:=aIsTemplate;
  FFileName:=aFileName;
  Create(AOwner);
end;

//创建窗体的第2步,由第一步的inherited而来
procedure TABPubForm.ReadState(Reader: TReader);
begin
  if (ABLocalParams.Debug) then
    ABWriteLog('Begin procedure '+Name+'.ReadState');

  inherited;

  if (ABLocalParams.Debug) then
    ABWriteLog('End   procedure '+Name+'.ReadState');
end;

//创建窗体的第3步,,由第2步的inherited而来
procedure TABPubForm.CreateParams(var Params: TCreateParams);
begin
  if (ABLocalParams.Debug) then
    ABWriteLog('Begin procedure '+Name+'.CreateParams');

  inherited;

  if (ABLocalParams.Debug) then
    ABWriteLog('End   procedure '+Name+'.CreateParams');
end;

//创建窗体的第4步,由第一步的inherited而来,执行后回到 第一步的inherited后
procedure TABPubForm.Loaded;
begin
  if (ABLocalParams.Debug) then
    ABWriteLog('Begin procedure '+Name+'.Loaded');

  if FIsTemplate then
  begin
    name:='TemplateForm_'+abstrtoname(FFileName);
  end;
  inherited;

  if (ABLocalParams.Debug) then
    ABWriteLog('End   procedure '+Name+'.Loaded');
end;

//创建窗体的第5步，由第一步完成后而来  //创建窗体的第6步为窗体的事件FormCreate，由DoCreate的inherited而来，
//FormCreate在非传统继承下(如ABSOFT采用的)子类的事件函数会覆盖父類的FormCreate
procedure TABPubForm.DoCreate;
var
  tempOnShow: TNotifyEvent;
  tempOnCreate: TNotifyEvent;
begin
  if (ABLocalParams.Debug) then
    ABWriteLog('Begin procedure '+Name+'.DoCreate');

  LoadAutoSavePropert;

  ABLanguage.SetComponentsText(self);

  if ABCloseFormEvent then
  begin
    tempOnShow:=OnShow;
    tempOnCreate:=OnCreate;
    OnShow:=nil;
    OnCreate:=nil;
    try
      inherited;
    finally
      OnShow:=tempOnShow;
      OnCreate:=tempOnCreate;
    end;
  end
  else
  begin
    inherited;
  end;

  FFormCreateOk:=true;

  if (ABLocalParams.Debug) then
    ABWriteLog('End   procedure '+Name+'.DoCreate');
end;

//显示窗体的第1步  //显示窗体的第2步 为窗体的事件 FormShow，由DoShow的inherited而来，
//FormShow在非传统继承下(如ABSOFT采用的)子类的事件函数会覆盖父類的FormShow
procedure TABPubForm.DoShow;
begin
  if (ABLocalParams.Debug) then
    ABWriteLog('Begin procedure '+Name+'.DoShow');

  inherited;
  FBegShowCaption:=caption;

  if (ABLocalParams.Debug) then
    ABWriteLog('End   procedure '+Name+'.DoShow');
end;

//关闭窗体的第1步  //关闭窗体的第2步 为窗体的事件FormCloseQuery，由CloseQuery的inherited而来，
//FormCloseQuery在非传统继承下(如ABSOFT采用的)子类的事件函数会覆盖父類的FormCloseQuery
function TABPubForm.CloseQuery: Boolean;
begin
  if (ABLocalParams.Debug) then
    ABWriteLog('Begin procedure '+Name+'.CloseQuery');

  Result:=True;
  FCloseQueryIng:=true;
  try
    if FAddCaption<>emptystr then
    begin
      Result:=false;
    end;

    if (Assigned(ABPubDesignerHook)) and
       (Assigned(ABPubDesignerHook.Form)) and
       (ABPubDesignerHook.Form=self)  then
    begin
      ABShow('请先结束设计模式再关闭窗体.');
      Result:=false;
    end;

    if Result then
    begin
      if FCloseIsCheckQuery then
        ABBeforeCloseForm(Self,[],Result);
    end;

    if Result then
    begin
      Result:=inherited CloseQuery;
    end;
  finally
    FCloseQueryIng:=false;
  end;

  if (ABLocalParams.Debug) then
    ABWriteLog('End   procedure '+Name+'.CloseQuery');
end;

//关闭窗体的第3步  //关闭窗体的第4步 为窗体的事件FormClose，由DoClose的inherited而来，
//FormClose在非传统继承下(如ABSOFT采用的)子类的事件函数会覆盖父類的FormClose
procedure TABPubForm.DoClose(var Action: TCloseAction);
begin
  if (ABLocalParams.Debug) then
    ABWriteLog('Begin procedure '+Name+'.DoClose');

  Action:=FCloseAction;
  inherited;

  if (ABLocalParams.Debug) then
    ABWriteLog('End   procedure '+Name+'.DoClose');
end;

//释放时第一步
destructor TABPubForm.Destroy;
begin
  if (ABLocalParams.Debug) then
    ABWriteLog('Begin procedure '+Name+'.Destroy');

  FPubStrings.Free;
  inherited;

  if (ABLocalParams.Debug) then
    ABWriteLog('End   procedure '+Name+'.Destroy');
end;

//释放窗体的第2步  //释放窗体的第3步 为窗体的事件 FormDestroy，由DoDestroy的inherited而来，
//FormDestroy在非传统继承下(如ABSOFT采用的)子类的事件函数会覆盖父類的FormDestroy
procedure TABPubForm.DoDestroy;
begin
  if (ABLocalParams.Debug) then
    ABWriteLog('Begin procedure '+Name+'.DoDestroy');

  ABSaveAutoSavePropertys(self);

  inherited;

  if (ABLocalParams.Debug) then
    ABWriteLog('End   procedure '+Name+'.DoDestroy');
end;

procedure TABPubForm.DoHide;
begin
  if (ABLocalParams.Debug) then
    ABWriteLog('Begin procedure '+Name+'.DoHide');

  inherited;

  if (ABLocalParams.Debug) then
    ABWriteLog('End   procedure '+Name+'.DoHide');
end;


end.

