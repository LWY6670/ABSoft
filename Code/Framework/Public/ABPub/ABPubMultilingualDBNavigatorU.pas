{
增强的TDBNavigator组件单元(支持中文标题和公布颜色属性)
}
unit ABPubMultilingualDBNavigatorU;

interface
{$I ..\ABInclude.ini}
uses
  Classes,DBCtrls,Graphics;

type
  //中文化且可设置Color的TDBNavigator
  TABMultilingualDBNavigator = class(TDBNavigator)
  private
  protected
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  Published
    property Color default clBtnFace;
  end;

implementation

{ TABMultilingualDBNavigator }

constructor TABMultilingualDBNavigator.Create(AOwner: TComponent);
begin
  inherited;
  Buttons[nbFirst]  .Caption:=('首条');
  Buttons[nbPrior]  .Caption:=('上条');
  Buttons[nbNext]   .Caption:=('下条');
  Buttons[nbLast]   .Caption:=('未条');
  Buttons[nbInsert] .Caption:=('增加');
  Buttons[nbDelete] .Caption:=('删除');
  Buttons[nbEdit]   .Caption:=('编辑');
  Buttons[nbPost]   .Caption:=('保存');
  Buttons[nbCancel] .Caption:=('取消');
  Buttons[nbRefresh].Caption:=('刷新');
  //Color:=$00CDD5C8;
end;

destructor TABMultilingualDBNavigator.Destroy;
begin

  inherited;
end;


end.

