object ABInPutPassWordForm: TABInPutPassWordForm
  Left = 390
  Top = 283
  BorderStyle = bsDialog
  Caption = #36755#20837#31649#29702#23494#30721
  ClientHeight = 100
  ClientWidth = 300
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 19
    Top = 27
    Width = 30
    Height = 13
    Caption = #23494#30721'  '
    Transparent = True
  end
  object Button1: TButton
    Left = 59
    Top = 61
    Width = 75
    Height = 25
    Caption = #30830#35748
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 153
    Top = 61
    Width = 75
    Height = 25
    Caption = #21462#28040
    TabOrder = 1
    OnClick = Button2Click
  end
  object leNewPassword: TEdit
    Left = 59
    Top = 26
    Width = 195
    Height = 21
    ImeName = #24555#20048#20116#31508
    PasswordChar = '*'
    TabOrder = 2
    OnKeyPress = leNewPasswordKeyPress
  end
end
