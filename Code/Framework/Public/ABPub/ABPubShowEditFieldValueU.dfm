object ABShowEditFieldValueForm: TABShowEditFieldValueForm
  Left = 446
  Top = 227
  Caption = #32534#36753#23383#27573#20540
  ClientHeight = 350
  ClientWidth = 500
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  DesignSize = (
    500
    350)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 17
    Top = 22
    Width = 60
    Height = 13
    AutoSize = False
    Caption = #23383#27573#21517#31216
    Transparent = True
  end
  object Label2: TLabel
    Left = 17
    Top = 47
    Width = 60
    Height = 13
    AutoSize = False
    Caption = #23383#27573#20540
    Transparent = True
  end
  object ComboBox1: TComboBox
    Left = 77
    Top = 19
    Width = 405
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    Ctl3D = True
    ImeName = #24555#20048#20116#31508
    ParentCtl3D = False
    TabOrder = 0
    OnChange = ComboBox1Change
  end
  object DBMemo1: TDBMemo
    Left = 77
    Top = 47
    Width = 405
    Height = 251
    Anchors = [akLeft, akTop, akRight, akBottom]
    DataSource = DataSource1
    ImeName = #24555#20048#20116#31508
    TabOrder = 1
  end
  object pnl1: TPanel
    Left = 0
    Top = 309
    Width = 500
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    DesignSize = (
      500
      41)
    object btn1: TButton
      Left = 416
      Top = 6
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #21462#28040
      TabOrder = 0
      OnClick = btn1Click
    end
    object Button2: TButton
      Left = 335
      Top = 6
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #30830#35748
      TabOrder = 1
      OnClick = Button2Click
    end
  end
  object DataSource1: TDataSource
    Left = 18
    Top = 92
  end
end
