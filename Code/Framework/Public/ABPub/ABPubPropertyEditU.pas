{
共用的属性编辑器单元
}
unit ABPubPropertyEditU;

interface
{$I ..\ABInclude.ini}

uses
  ABPubOrderStrU,
  ABPubSelectStrU,
  abpubFuncU,

  Classes,DB,DesignIntf,DesignEditors;

type
  //字段下拉列框的属性编辑器
  TABFieldComboxProperty = class(TStringProperty)
  public
    function  GetAttributes: TPropertyAttributes; override;
    procedure GetValues(Proc: TGetStrProc); override;
  end;

  //字段多选的属性编辑器
  TABFieldSelectsProperty = class(TStringProperty)
  public
    function GetAttributes:TPropertyAttributes;override;
    procedure Edit;override;
  end;

  //字段排序的属性编辑器
  TABFieldOrderProperty = class(TStringProperty)
  public
    function GetAttributes:TPropertyAttributes;override;
    procedure Edit;override;
  end;

  //主数据集字段下拉列框的属性编辑器
  TABMainDatasetFieldComboxProperty = class(TStringProperty)
  public
    function  GetAttributes: TPropertyAttributes; override;
    procedure GetValues(Proc: TGetStrProc); override;
  end;

  //主数据集字段多选的属性编辑器
  TABMainDatasetFieldSelectsProperty = class(TStringProperty)
  public
    function GetAttributes:TPropertyAttributes;override;
    procedure Edit;override;
  end;


implementation



{ TABFieldComboxProperty }

function TABFieldComboxProperty.GetAttributes: TPropertyAttributes;
begin
  Result := [paValueList, paSortList];
end;

procedure TABFieldComboxProperty.GetValues(Proc: TGetStrProc);
var
  I: Integer;
  tempDataSet:TDataSet;
begin
  tempDataSet:=ABGetDataSetProperty(GetComponent(0));
  if (Assigned(tempDataSet)) and (tempDataSet.FieldDefs.Count>0) then
  begin
    for I := 0 to tempDataSet.FieldDefs.Count - 1 do
      Proc(tempDataSet.FieldDefs.Items[I].Name);
  end;
end;

{ TABFieldSelectsProperty }

function TABFieldSelectsProperty.GetAttributes: TPropertyAttributes;
begin
  result:=[paDialog,paMultiselect];
end;

procedure TABFieldSelectsProperty.Edit;
var
  tempStr1:string;
  tempDataSet:TDataSet;
begin
  tempDataSet:=ABGetDataSetProperty(GetComponent(0));
  if (Assigned(tempDataSet)) and (tempDataSet.FieldDefs.Count>0) then
  begin
    tempStr1:=value;
    ABSelectFieldNames(tempStr1,tempDataSet);
    if tempStr1<>value then
      value:= tempStr1;
  end;
end;

{ TABFieldOrderProperty }

procedure TABFieldOrderProperty.Edit;
var
  tempStr1:string;
  tempDataSet:TDataSet;
begin
  tempDataSet:=ABGetDataSetProperty(GetComponent(0));
  if (Assigned(tempDataSet)) and (tempDataSet.FieldDefs.Count>0) then
  begin
    tempStr1:=value;
    ABOrderFieldNames(tempStr1,tempDataSet);
    if tempStr1<>value then
      value:= tempStr1;
  end;
end;

function TABFieldOrderProperty.GetAttributes: TPropertyAttributes;
begin
  result:=[paDialog,paMultiselect];
end;

{ TABMainDatasetFieldSelectsProperty }

function TABMainDatasetFieldSelectsProperty.GetAttributes: TPropertyAttributes;
begin
  result:=[paDialog,paMultiselect];
end;

procedure TABMainDatasetFieldSelectsProperty.Edit;
var
  tempStr1:string;
  tempDataSet:TDataSet;
begin
  tempDataSet:=ABGetDataSetProperty(GetComponent(0));
  if (Assigned(tempDataSet)) and
     (Assigned(tempDataSet.DataSource))  and
     (Assigned(tempDataSet.DataSource.DataSet)) and
     (tempDataSet.DataSource.DataSet.FieldDefs.Count>0) then
  begin
    tempStr1:=value;
    ABSelectFieldNames(tempStr1,tempDataSet.DataSource.DataSet);
    if tempStr1<>value then
      value:= tempStr1;
  end;
end;

{ TABMainDatasetFieldComboxProperty }

function TABMainDatasetFieldComboxProperty.GetAttributes: TPropertyAttributes;
begin
  Result := [paValueList, paSortList];
end;

procedure TABMainDatasetFieldComboxProperty.GetValues(Proc: TGetStrProc);
var
  I: Integer;
  tempDataSet:TDataSet;
begin
  tempDataSet:=ABGetDataSetProperty(GetComponent(0));

  if (Assigned(tempDataSet)) and
     (Assigned(tempDataSet.DataSource))  and
     (Assigned(tempDataSet.DataSource.DataSet)) and
     (tempDataSet.DataSource.DataSet.FieldDefs.Count>0) then
  begin
    for I := 0 to tempDataSet.DataSource.DataSet.FieldDefs.Count - 1 do
      Proc(tempDataSet.DataSource.DataSet.FieldDefs.Items[I].Name);
  end;
end;


end.
