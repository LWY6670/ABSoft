{
脚本解析单元
}
unit ABPubScriptU;

interface
{$I ..\ABInclude.ini}
uses
  abPubMessageU,
  ABPubConstU,
  ABPubFuncU,


  SysUtils,Variants,System.Win.comobj;

//执行Javascript脚本
function ABExecScript (aCommand: string;aShowResultStr:Boolean=false;aReoprtRaise:Boolean=True;aScriptType:TABScriptType=stJava):string;
//执行批量的Javascript脚本，由ABSubStrBegin 与ABSubStrEnd分隔
function ABExecScripts(aCommand: string;aShowResultStr:Boolean=false;aReoprtRaise:Boolean=True;aScriptType:TABScriptType=stJava):Boolean;

implementation
var
  ABJavascript: Variant;                      //共用的Javascript支持对象
  ABVBscript: Variant;                        //共用的VBscript支持对象
  ABScriptCommands:array of TABNodeData;

function ABExecScript(aCommand: string;aShowResultStr:Boolean;aReoprtRaise:Boolean;aScriptType:TABScriptType): string;
var
  I: Integer;
  tempFind:Boolean;
begin
  result := emptystr;
  //因为DELPHI IDE环境下运行script.eval非常慢,所以IDE环境下直接返回空
  //if (ABInDelphi) then
  //  exit;

  aCommand:=Trim(aCommand);
  if (aCommand = emptystr) or
     (aCommand=QuotedStr(EmptyStr)) then
  begin
    exit;
  end;

  //创建criptOLE对象
  case aScriptType of
    stJava:
    begin
      if ABVarIsNull(ABJavascript) then
      begin
        ABJavascript := createoleobject('scriptcontrol');
        ABJavascript.language := 'Javascript';
      end;
    end;
    stVB:
    begin
      if ABVarIsNull(ABVBscript) then
      begin
        ABVBscript := createoleobject('scriptcontrol');
        ABVBscript.language := 'VBscript';
      end;
    end
    else
    begin
      exit;
    end;
  end;

  tempFind:=False;
  for I := low(ABScriptCommands) to High(ABScriptCommands) do
  begin
    if (AnsiCompareText(aCommand,ABScriptCommands[i].Name)=0) then
    begin
      Result := ABScriptCommands[i].Value;
      tempFind:=true;
      Break;
    end;
  end;

  if not tempFind then
  begin
    try
      //执行Script
      case aScriptType of
        stJava:
        begin
          Result := ABJavascript.eval(aCommand);
        end;
        stVB:
        begin
          Result := ABVBscript.eval(aCommand);
        end;
      end;

      SetLength(ABScriptCommands,high(ABScriptCommands)+1+1);
      i:=high(ABScriptCommands);
      ABScriptCommands[i].Name:=aCommand;
      ABScriptCommands[i].Value:=Result;
    except
      Result := '-1';
      if aReoprtRaise then
        raise;
    end;
  end;

  if (aShowResultStr) and (Result<>EmptyStr) then
  begin
    ABShow(Result);
  end;
end;

function ABExecScripts(aCommand: string;aShowResultStr:Boolean;aReoprtRaise:Boolean;aScriptType:TABScriptType):Boolean;
var
  tempSubItemStr,tempSubStr:string;
  tempPosBeginIndex,
  tempSubStrEndIndex,tempSubStrBegIndex: Integer;
begin
  Result := True;

  //因为DELPHI IDE环境下运行script.eval非常慢,所以IDE环境下直接返回空
  //if (ABInDelphi) then
  //  exit;

  aCommand:=Trim(aCommand);
  if (aCommand = emptystr) or
     (aCommand=QuotedStr(EmptyStr)) then
  begin
    exit;
  end;

  tempPosBeginIndex:=1;
  tempSubStrBegIndex:=abpos(ABSubStrBegin,aCommand);
  while tempSubStrBegIndex>0 do
  begin
    tempSubStrEndIndex:=ABPos(ABSubStrEnd,aCommand,tempPosBeginIndex);
    if tempSubStrEndIndex<=0 then
      break;

    tempSubItemStr:=Copy(aCommand,tempSubStrBegIndex,tempSubStrEndIndex-tempSubStrBegIndex+length(ABSubStrEnd));

    tempSubStr:=tempSubItemStr;
    tempSubStr:=     ABStringReplace(tempSubStr,ABSubStrBegin,EmptyStr);
    tempSubStr:=Trim(ABStringReplace(tempSubStr,ABSubStrEnd  ,EmptyStr));


    tempSubStr:=Copy(aCommand,
                          tempSubStrBegIndex+length(ABSubStrBegin),
                          tempSubStrEndIndex-tempSubStrBegIndex-length(ABSubStrBegin));
    if (ABExecScript(tempSubStr,aShowResultStr,aReoprtRaise,aScriptType)<>EmptyStr) then
    begin
      result:=false;
      break;
    end;

    aCommand:=ABStringReplace(aCommand,tempSubItemStr,'');
    tempPosBeginIndex:=tempSubStrEndIndex+length(ABSubStrEnd)-length(tempSubItemStr);
    tempSubStrBegIndex:=abpos(ABSubStrBegin,aCommand,tempPosBeginIndex);
  end;

  if (result) and
     (aCommand<>EmptyStr) and
     (ABExecScript(aCommand,aShowResultStr,aReoprtRaise,aScriptType)<>EmptyStr) then
  begin
    result:=false;
  end;
end;



end.
