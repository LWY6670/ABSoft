{
具有数据感知能力的标签单元
}
unit ABPubDBLabelsU;

interface
{$I ..\ABInclude.ini}
uses
  ABPubFuncU,
  ABPubDBU,

  Classes,DB,Controls,StdCtrls,SysUtils;

type
  TABDBLabels         = class;
  TABDBLabelsDataLink =class(TDataLink)
  private
    owner:TABDBLabels;
    { Private declarations }
  protected
    procedure EditingChanged; override;
    procedure ActiveChanged; override;
    procedure RecordChanged(Field: TField); override;
    procedure DataSetChanged; override;
    procedure DataSetScrolled(Distance: Integer);override;
    { Protected declarations }
  public
    { Public declarations }
  published
    { Published declarations }
  end;

  TABDBLabels         = class(TLabel )
  private
    FDataLink:TABDBLabelsDataLink;
    FDataFields: string;

    procedure SetDataSource(const Value: TDataSource);
    procedure Refresh;
    function GetDataSource: TDataSource;
    procedure SetDataFields(const Value: string);
    { Private declarations }
    { Private declarations }
  protected
    procedure Notification(AComponent: TComponent;Operation:    TOperation);override;
    { Protected declarations }
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    { Public declarations }
  published
    //关联的数据源
    property DataSource: TDataSource read GetDataSource write SetDataSource;
    //关联的数据字段，多个字段时用逗号分隔
    property DataFields: string read FDataFields write SetDataFields;
    { Published declarations }
  end;

implementation

{ TABDBLabels }

constructor TABDBLabels.Create(AOwner: TComponent);
begin
  inherited;
  Transparent:=true;

  Anchors := Anchors+[akRight];
  Alignment := taRightJustify;
  AutoSize:=false;

  FDataLink := TABDBLabelsDataLink.Create;
  FDataLink.owner:=self;
end;

destructor TABDBLabels.Destroy;
begin
  FDataLink.Free;
  FDataLink:=nil;
  inherited;
end;

function TABDBLabels.GetDataSource: TDataSource;
begin
  Result:=FDataLink.DataSource;
end;

procedure TABDBLabels.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited ;
  if (Operation = opRemove) then
  begin
    if (Assigned(FDataLink)) and (FDataLink.DataSource =AComponent)   then
      FDataLink.DataSource:=nil;
  end;
end;

procedure TABDBLabels.Refresh;
var
  i:LongInt;
  tempStr1,tempStrSum:string;
  tempField1:TField;
begin
  if (DataFields=EmptyStr) or
     (not Assigned(DataSource)) or
     (not Assigned(DataSource.DataSet)) or
     ((not DataSource.DataSet.Active)) or
     ((ABDatasetIsEmpty(DataSource.DataSet)))
     then
  begin
    Caption:=EmptyStr;
    exit;
  end;

  tempStrSum:=EmptyStr;
  for I := 1 to ABGetSpaceStrCount(DataFields, ',') do
  begin
    tempStr1 := ABGetSpaceStr(DataFields, i, ',');
    if tempStr1<>EmptyStr then
    begin
      tempField1:= DataSource.DataSet.FindField(tempStr1);
      if Assigned(tempField1) then
      begin
        if tempField1.DisplayText<>EmptyStr then
          ABAddstr(tempStrSum,tempField1.DisplayLabel+':'+tempField1.DisplayText,'  ');
      end;
    end;
  end;

  if AnsiCompareText(tempStrSum,Caption)<>0 then
    Caption:=tempStrSum;
end;

procedure TABDBLabels.SetDataFields(const Value: string);
begin
  FDataFields:=ABStringReplace(Value,';',',');
  Refresh;
end;

procedure TABDBLabels.SetDataSource(const Value: TDataSource);
begin
  if not (FDataLink.DataSourceFixed and (csLoading in ComponentState)) then
  begin
    FDataLink.DataSource := Value;
  end;
  if Value <> nil then
  begin
    Value.FreeNotification(Self);
  end;

  if Assigned(Value) then
    Refresh;
end;

{ TABDBLabelsDataLink }

procedure TABDBLabelsDataLink.ActiveChanged;
begin
  inherited;
  TABDBLabels(owner).Refresh;
end;

procedure TABDBLabelsDataLink.DataSetChanged;
begin
  inherited;
  TABDBLabels(owner).Refresh;
end;

procedure TABDBLabelsDataLink.DataSetScrolled(Distance: Integer);
begin
  inherited;

end;

procedure TABDBLabelsDataLink.EditingChanged;
begin
  inherited;
  TABDBLabels(owner).Refresh;
end;

procedure TABDBLabelsDataLink.RecordChanged(Field: TField);
begin
  inherited;
  TABDBLabels(owner).Refresh;
end;

end.
