object ABPubEditPassWordForm: TABPubEditPassWordForm
  Left = 390
  Top = 283
  BorderStyle = bsDialog
  Caption = #20462#25913#23494#30721
  ClientHeight = 150
  ClientWidth = 300
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 27
    Top = 11
    Width = 76
    Height = 13
    AutoSize = False
    Caption = #26087#23494#30721'  '
    Transparent = True
  end
  object Label2: TLabel
    Left = 27
    Top = 37
    Width = 76
    Height = 13
    AutoSize = False
    Caption = #26032#23494#30721'  '
    Transparent = True
  end
  object Label3: TLabel
    Left = 27
    Top = 64
    Width = 76
    Height = 13
    AutoSize = False
    Caption = #26032#23494#30721#39564#35777'   '
    Transparent = True
  end
  object Button1: TButton
    Left = 62
    Top = 106
    Width = 75
    Height = 25
    Caption = #20462#25913
    TabOrder = 3
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 156
    Top = 106
    Width = 75
    Height = 25
    Caption = #21462#28040
    TabOrder = 4
    OnClick = Button2Click
  end
  object leOldPassword: TEdit
    Left = 102
    Top = 8
    Width = 180
    Height = 21
    PasswordChar = '*'
    TabOrder = 0
    OnKeyPress = leOldPasswordKeyPress
  end
  object leNewPassword: TEdit
    Left = 102
    Top = 35
    Width = 180
    Height = 21
    PasswordChar = '*'
    TabOrder = 1
    OnKeyPress = leNewPasswordKeyPress
  end
  object leAffirmNewPassword: TEdit
    Left = 102
    Top = 62
    Width = 180
    Height = 21
    PasswordChar = '*'
    TabOrder = 2
    OnKeyPress = leAffirmNewPasswordKeyPress
  end
end
