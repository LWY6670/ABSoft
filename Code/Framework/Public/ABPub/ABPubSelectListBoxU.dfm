object ABSelectListBoxForm: TABSelectListBoxForm
  Left = 389
  Top = 204
  Caption = 'ListBox'#32534#36753#31383#20307
  ClientHeight = 400
  ClientWidth = 500
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object ListBox1: TListBox
    Left = 0
    Top = 0
    Width = 500
    Height = 359
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    ImeName = #24555#20048#20116#31508
    ItemHeight = 14
    ParentFont = False
    TabOrder = 0
  end
  object Panel1: TPanel
    Left = 0
    Top = 359
    Width = 500
    Height = 41
    Align = alBottom
    TabOrder = 1
    DesignSize = (
      500
      41)
    object Button1: TButton
      Left = 419
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #21462#28040
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 339
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #30830#23450
      TabOrder = 1
      OnClick = Button2Click
    end
  end
end
