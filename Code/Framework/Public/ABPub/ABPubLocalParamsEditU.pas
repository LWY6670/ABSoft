{
本地参数管理单元
}
unit ABPubLocalParamsEditU;
                             
interface
{$I ..\ABInclude.ini}

uses
  ABPubLocalParamsU,
  abpubConstU,
  abpubVarU,
  ABPubFuncU,
  ABPubRegisterFileU,
  ABPubSelectNameAndValuesU,
  ABPubLanguageEditU,

  printers,
  ShellAPI,
  Windows,
  SysUtils,Classes,Controls,Forms,StdCtrls,ExtCtrls,TypInfo,Spin, Vcl.Buttons;

type
  TABPubLocalParamsEditForm = class(TForm)
    Memo1: TMemo;
    GroupBox1: TGroupBox;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    SpinEdit1: TSpinEdit;
    Label4: TLabel;
    SpinEdit2: TSpinEdit;
    Label5: TLabel;
    RadioGroup2: TRadioGroup;
    CheckBox4: TCheckBox;
    RadioGroup1: TRadioGroup;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure SpinEdit1Change(Sender: TObject);
    procedure SpinEdit2Change(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure CheckBox2Click(Sender: TObject);
    procedure RadioGroup2Click(Sender: TObject);
    procedure CheckBox4Click(Sender: TObject);
    procedure RadioGroup1Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
  private
    FFormCreate:boolean;
    { Private declarations }
  public                            
    { Public declarations }
  end;

//本地参数编辑
procedure ABLocalParamsEdit;

implementation

{$R *.dfm}                           

procedure ABLocalParamsEdit;
var
  tempLocalEditForm: TABPubLocalParamsEditForm;
begin
  tempLocalEditForm := TABPubLocalParamsEditForm.Create(nil);
  try
    tempLocalEditForm.ShowModal;
  finally
    tempLocalEditForm.Free;
  end;
end;

procedure TABPubLocalParamsEditForm.FormCreate(Sender: TObject);
begin
  if ABCheckFileExists(ABLocalParamsFile) then
  begin
    Memo1.Lines.LoadFromFile(ABLocalParamsFile);
  end;

  CheckBox4.Checked :=ABLocalParams.ShowStartForm;
  SpinEdit1.Value   :=ABLocalParams.SockLocalPort;
  SpinEdit2.Value   :=ABLocalParams.SockToPort;
  CheckBox1.Checked :=ABLocalParams.Debug;
  CheckBox2.Checked :=ABLocalParams.LanguageEnabled;

  RadioGroup2.ItemIndex:=ord(ABLocalParams.BackType);
  RadioGroup1.ItemIndex:=ord(ABLocalParams.LoginType);

  FFormCreate:=True;
end;

procedure TABPubLocalParamsEditForm.SpeedButton1Click(Sender: TObject);
var
  tempStr:string;
  tempPubStrings:TStrings;
  i:LongInt;
begin
  tempPubStrings:=TStringList.Create;
  try
    for i := 0 to  ABRegisterFile.Flist.Count - 1 do
    begin
      tempPubStrings.Add(PABPackageList(ABRegisterFile.Flist.Objects[i]).Form.Caption);
    end;

    tempStr:=ABStringsToStrs(ABPubFuncPrintNames);
    if ABSelectNameAndValues(tempPubStrings,printers.printer.printers,
                             tempStr) then
    begin
      ABStrsToStrings(tempStr,',',ABPubFuncPrintNames);
    end;
  finally
    tempPubStrings.Free;
  end;
end;

procedure TABPubLocalParamsEditForm.SpeedButton2Click(Sender: TObject);
begin
  ShellExecute(0, 'open', PChar(ABLogFile), nil, nil, sw_shownormal);
end;

procedure TABPubLocalParamsEditForm.SpeedButton3Click(Sender: TObject);
begin
  ABLanguageEdit;
end;

procedure TABPubLocalParamsEditForm.SpinEdit1Change(Sender: TObject);
begin
  if not FFormCreate then  exit;
  
  ABSetLocalParamsValue('SockLocalPort',IntToStr(SpinEdit1.Value));
  Memo1.Lines.LoadFromFile(ABLocalParamsFile);
end;

procedure TABPubLocalParamsEditForm.SpinEdit2Change(Sender: TObject);
begin
  if not FFormCreate then  exit;

  ABSetLocalParamsValue('SockToPort',IntToStr(SpinEdit2.Value));
  Memo1.Lines.LoadFromFile(ABLocalParamsFile);
end;

procedure TABPubLocalParamsEditForm.CheckBox1Click(Sender: TObject);
begin
  if not FFormCreate then  exit;

  ABSetLocalParamsValue('Debug',ABBoolToStr(TCheckBox(Sender).Checked));
  Memo1.Lines.LoadFromFile(ABLocalParamsFile);
end;

procedure TABPubLocalParamsEditForm.CheckBox2Click(Sender: TObject);
begin
  if not FFormCreate then  exit;

  ABSetLocalParamsValue('LanguageEnabled',ABBoolToStr(TCheckBox(Sender).Checked));
  Memo1.Lines.LoadFromFile(ABLocalParamsFile);
end;

procedure TABPubLocalParamsEditForm.CheckBox4Click(Sender: TObject);
begin
  if not FFormCreate then  exit;

  ABSetLocalParamsValue('ShowStartForm',ABBoolToStr(TCheckBox(Sender).Checked));
  Memo1.Lines.LoadFromFile(ABLocalParamsFile);
end;

procedure TABPubLocalParamsEditForm.RadioGroup1Click(Sender: TObject);
begin
  if not FFormCreate then  exit;

  ABSetLocalParamsValue('LoginType',GetEnumName(TypeInfo(TABLoginType), RadioGroup1.ItemIndex));
  Memo1.Lines.LoadFromFile(ABLocalParamsFile);
end;

procedure TABPubLocalParamsEditForm.RadioGroup2Click(Sender: TObject);
begin
  if not FFormCreate then  exit;

  ABSetLocalParamsValue('BackType',GetEnumName(TypeInfo(TABBackType), RadioGroup2.ItemIndex));
  Memo1.Lines.LoadFromFile(ABLocalParamsFile);
end;


end.
