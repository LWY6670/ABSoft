{
常量及自定义类型的定义单元
}
unit ABPubConstU;

interface
{$I ..\ABInclude.ini}

uses
  DB,Windows;


const
  //回车
  ABEnterStr = #$D;
  //换行
  ABWrapStr = #$A;
  //回车+换行
  ABEnterWrapStr = #$D#$A;
  //字串中包含多个块时用于标识开始结束的标记
  ABSubStrBegin = '[Begin]';
  ABSubStrEnd = '[End]';

  //SQL包含执行块时用于标识开始结束的标记
  ABExceSQLBegin = '[ExceSQLBegin]';
  ABExceSQLEnd = '[ExceSQLEnd]';

  //客户端程序的名称
  ABSoftName = 'ABSoft';

type
  //客户端连接类型：两层、三层、BS)
  TABLoginType = (ltCS_Two, ltCS_Three, ltBS);

  //报表打印输出方式:预览方式打印、选择打印机方式打印、直接打印
  TABPrintOutType=(ptPreview,ptSelectPrint,ptDirect);
  //打印数据的范围：当前记录、所有数据
  TABPrintRange=(prCur,prAll);

  //DELPHI版本相关
  TABDelphiVersion = (dvnull,
                      dvD7,dvD2007,dvD2009,dvD2010,dvDXE1,dvDXE2,dvDXE3,dvDXE4,dvDXE5,dvDXE6,dvDXE7,dvDXE8,dvDXE10);
  //dvD5, dvD6, dvD8,dvD2005, dvD2006, //此行的版本暂不支持,这些版本未测试
  //DELPHI批量设置工程的项目类型
  TABBatchSetDelphiProjectType=(bstnull,bstIncludeVerInfo,bstBuildIncVersion,bstVersion,bstDcpPath,
                                bstBplPath,bstExePath,bstBuildWithPackage,bstBuildWithPackageList);
  //数据操作类型
  TABDatasetOperateType = ( dtClose,
                            dtBrowse,

                            dtSelect,
                            dtAppend,
                            dtEdit,
                            dtDelete);
  TABDatasetOperateTypes =set of TABDatasetOperateType;
  //数据集增加类型
  TABDatasetAddType = (daAppend,daInsert);
  //操作数据集的事件
  TABDatasetOperatorNotifyEvent = procedure(aType: TABDatasetOperateType;
                                            aDatetime:TDatetime;
                                            aError:string) of object;
  //数据表格可多选择时操作的类型
  //mtCur 只操作当前记录
  //mtSelect 操作选择的记录
  //mtCurToEnd 操作当前记录到最后
  //mtAll 操作所有记录
  TABMultiSelectType=(mtCur,mtSelect,mtCurToEnd,mtAll);
  //输出的文件种类
  TABOutFileType=(otExcel,otXML,otText,otHtml);
  //数据库类型
  TABDatabaseType=(dtNullDatabase,dtSQLServer,dtOracle,dtAccess,dtDB2,dtMysql);
  //字段值求和类型：数值型字段按数值相加、字符型字段按字符连接、按字段类型来求和
  TABFieldValueSumType = (stNumber,stString,stAuto);

  //客户端背景类型
  TABBackType=(btNull,btPicture,btMail,btFlow,btCustom);
  //文本编码类型
  TABTxtFileType=(tfAnsi,tfUnicode,tfUnicodeBigEndian,tfUtf8);
  //tag类型
  TABTagType=(ttPubAutoSave_91,ttSingleAutoSave_910,ttAutoPositionSize_9100,
              ttNotCheckDataSetOnCloseForm_91000,tt_910000,tt_9100000,tt_91000000);

  //X方向位置
  TABXDirection = (axdLeft, axdRight);
  //Y方向位置
  TABYDirection = (axdTop, axdBottom);
  //XY方向位置
  TABDirection = (dTop,DBottom, dLeft, dRight);

  //Socket类型
  TABSocketType = (stUDP,stTCPServer,stTCPClient);
  //Socket模型  选择模型、异步选择模型、事件选择模型、重叠IO事件通知模型、
  //            重叠IO完成例程模型、完成端口模型
  TABSocketModel = (smSelect,smWSAAsyncSelect,smWSAEventSelect,smOverlappedIO_Event,
                    smOverlappedIO_CompletionRoutine,smIOCP);
  //Socket状态
  TABSocketState = (ssClosed,ssOpen);

  //文件时间类型 创建时间，修改时间，访问时间
  TABFileTimeType = (fttCreation, fttLastAccess, fttLastWrite);
  //文件操作类型
  TABFileOperateType = (ftNull,ftInsert,ftEdit,ftDelete);
  //文件目录类型
  TABDirType = (dtDir, dtFile);
  TABDirTypes = set of TABDirType;

  //注册表中设置随系统启动自动运行的类型 每次启动都运行，仅在第一次系统启动运行
  TABAutoRunType=(atRun,atRunOnce);
  //注册表结点的类型
  TABRegKeyValueType = (rtString,rtInteger);

  //字符类型:中文,英文字母,数字,其它
  TABCharType = (ctChina, ctEnglish, ctFig, ctOther);
  //中文字符集类型:GB中的简体,GB中的繁体,BIG5
  TABChineseType = (ctNull, ctGBChs, ctGBCht, ctBIG5);


  //进度条状态
  TABProgressState=(psStop,psRun);
  //树结点操作类型
  TABCheckTreeViewOperator=(coCheck,coUnCheck,coReverse);


  //服务程序状态：无状态、服务已停止、正在启动服务、正在停止服务、服务正在运行、
  //              正在继续运行服务、正在暂停服务、服务已暂停、不能打开服务控制台、
  //              不能打开服务、取状态出错
  TABServiceState=(stNull,stStopped,stStartPending,stStopPending,
                   stRunning,stContinuePending,stPausePending,stPaused,
                   stNotOpenServiceControl,stNotOpenService,stGetStateError);


  //联网类型
  TABIntetnetType = (itModem, itLAN, itProxy, itModemBusy, itOther);
  //WINDOWS操作系统语言类型
  TABSysLanguageType = (wlAuto,wlChina,wlChinaTw,wlJapanese,wlKorean,wlEnglish);
  //Bitmap灰化处理类型
  TABGrayBitmapType = (gbtMax,gbtAvg,gbtWeightedAvg);
  //主从结点的类型
  TABMainDetailType = (mdtMain, mdtDetail);
  //Script类型
  TABScriptType = (stJava, stVB);
  //扩展的事件类型
  TABNotifyEvent = procedure(Sender: TObject;var aContinue:Boolean) of object;

  //数据库连接参数
  PABConnParams = ^TABConnParams;
  TABConnParams = record
    ConnName: string;
    IsMain:boolean;
    Manual:boolean;    //手工增加的，不负责释放

    DatabaseType:TABDatabaseType;
    Host: string;
    Database:string;
    UserName: string; //如果 UserName为空且为dtSQLServer 则为windows 身份验证
    Password: string;
  end;

  //数据库连接结构
  PABConninfo = ^TABConninfo;
  TABConninfo = record
    Conninfo: TABConnParams;
    Conn: TCustomConnection;
  end;

  //服务程序信息
  TABServiceInfo=Record
    HscManager :THandle;
    Service: THandle;

    aServiceName :PChar; //服务名
    aDisplayName :PChar; //服务的显示名
    DesireAccess :DWORD; //访问权限  SERVICE_ALL_ACCESS
    ServiceType :Dword; //服务的类别
    StartType :Dword; //服务的启动方式
    ErrorControl :Dword; //服务错误控制方式  SERVICE_ERROR_IGNORE
    BinaryPathName :PChar; //服务执行文件的具体路径
    LoadOrderGroup :PChar;
    TagId :PDword;
    Dependencies :PChar;  //依赖：有多个依赖的服务时，中间以空格隔开}
    ServerStartName :PChar;
    Password :PChar;
  end;

  //查询的字段名和字段值比较类型
  TABQueryCompareType = (
                  qtEqual                       ,//等于
                  qtNotEqual                    ,//不等于
                  qtLess                        ,//小于
                  qtLessEqual                   ,//小于或等于
                  qtGreater                     ,//大于
                  qtGreaterEqual                ,//大于或等于
                  qtLike                        ,//包含
                  qtNotLike                     ,//不包含
                  qtLeftLike                    ,//左包含
                  qtNotLeftLike                 ,//左不包含
                  qtRightLike                   ,//右包含
                  qtNotRightLike                ,//右不包含
                  qtBetween                     ,//从***到***
                  qtNotBetween                  ,//不从***到***
                  qtInList                      ,//在列表中
                  qtNotInList                   ,//不在列表中
                  qtIsNull                      ,//为空
                  qtIsNotNull                    //不为空
                  );
  TABQueryCompareTypes = set of TABQueryCompareType;
  //扩展的数据别名
  TABAnsiCharDynArray= array of AnsiChar;
  TABWideCharDynArray= array of WideChar;
  TAB4ByteDynArray = array[0..4] of Byte;

  //主从节点结构
  PABNodeData = ^TABNodeData;
  TABNodeData = record
    ParentKey,
    Key,

    Type_,
    Name,
    Value: string;

    Value_Exs:array of string;
    Remark: string;
  end;

  //DELPHI版本常量
  //{$IF (CompilerVersion>22.0)}

  {$IFDEF VER150}      // Delphi 7.0
    TDataSetClass = class of Tdataset;
    const ABDelphiVersion=dvD7;
  {$ENDIF}

  {$IFDEF VER185}      // Delphi 11.0 (Tiburon) Delphi2007
    const ABDelphiVersion=dvD2007;
  {$ENDIF}

  {$IFDEF VER200}      // Delphi 12.0 (Tiburon) Delphi2009
    const ABDelphiVersion=dvD2009;
  {$ENDIF}

  {$IFDEF VER210}      // Delphi 14.0
    const ABDelphiVersion=dvD2010;
  {$ENDIF}

  {$IFDEF VER220}      // Delphi 15.0 Delphi XE1
    const ABDelphiVersion=dvDXE1;
  {$ENDIF}

  {$IFDEF VER230}      // Delphi 16.0 Delphi XE2
    const ABDelphiVersion=dvDXE2;
  {$ENDIF}

  {$IFDEF VER240}      // Delphi 17.0 Delphi XE3
    const ABDelphiVersion=dvDXE3;
  {$ENDIF}

  {$IFDEF VER250}      // Delphi 18.0 Delphi XE4
    const ABDelphiVersion=dvDXE4;
  {$ENDIF}

  {$IFDEF VER260}      // Delphi 19.0 Delphi XE5
    const ABDelphiVersion=dvDXE5;
  {$ENDIF}

  {$IFDEF VER270}      // Delphi 20.0 Delphi XE6
    const ABDelphiVersion=dvDXE6;
  {$ENDIF}

  {$IFDEF VER280}      // Delphi 21.0 Delphi XE7
    const ABDelphiVersion=dvDXE7;
  {$ENDIF}

  {$IFDEF VER290}      // Delphi 22.0 Delphi XE8
    const ABDelphiVersion=dvDXE8;
  {$ENDIF}

  {$IFDEF VER300}      // Delphi 23.0 Delphi XE10
    const ABDelphiVersion=dvDXE10;
  {$ENDIF}


implementation



end.
