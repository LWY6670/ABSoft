{
Socket通讯属性框架单元
}
unit ABPubSockFrameU;

interface
{$I ..\ABInclude.ini}

uses
  abpubfuncU,
  ABPubConstU,
  ABPubSockU,

  ABPubFrameU,

  SysUtils,TypInfo,Types, Classes, Controls, Forms, ExtCtrls, StdCtrls,
  ComCtrls, Spin, Menus, ABPubDownButtonU;

type
  TABSockFrame = class(TABPubFrame)
    Splitter1: TSplitter;
    Panel1: TPanel;
    GroupBox9: TGroupBox;
    Memo1: TMemo;
    Panel4: TPanel;
    Panel7: TPanel;
    Button1: TButton;
    Button2: TButton;
    Button5: TButton;
    Button3: TButton;
    StatusBar1: TStatusBar;
    GroupBox11: TGroupBox;
    Memo2: TMemo;
    Edit2: TEdit;
    ProgressBar1: TProgressBar;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel5: TPanel;
    Label2: TLabel;
    ComboBox1: TComboBox;
    Label6: TLabel;
    Label7: TLabel;
    Edit1: TEdit;
    SpinEdit2: TSpinEdit;
    Panel6: TPanel;
    Label4: TLabel;
    Edit3: TEdit;
    Label5: TLabel;
    SpinEdit1: TSpinEdit;
    Label8: TLabel;
    ComboBox2: TComboBox;
    ABSocket1: TABSocket;
    Label3: TLabel;
    SpinEdit3: TSpinEdit;
    SpinEdit4: TSpinEdit;
    Label1: TLabel;
    Button4: TABDownButton;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    Button6: TABDownButton;
    PopupMenu2: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    Button7: TButton;
    procedure Button5Click(Sender: TObject);
    procedure Memo1KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure ABSocket1Error(Sender: TObject; Error: Integer; Msg: string);
    procedure ABSocket1Event(Sender: TObject; aSocket: Cardinal;aEvent: Integer; aEventDesc: string);
    procedure ComboBox1Change(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure ABSocket1Data(Sender: TObject; aSocket: Cardinal; aFromIP,
      aFromPort: string; aSocketPackage: TSocketPackage; aBuff: PByte;
      aFileName: string);
    procedure ABSocket1Close(Sender: TObject; aSocket: Cardinal);
  private
    { Private declarations }
  public
    procedure AppendLog(aStr: string);
    procedure SendSocketObject(aSocketObject: TSocketObject; Sender: TObject);

    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    { Public declarations }
  end;

  TABSockUI = class(TGroupBox)
  private
    FFrame: TABSockFrame;
    FOnBegReadData: TNotifyEvent;
    FOnEndReadData: TNotifyEvent;
    FOnSendByteArrayClick: TNotifyEvent;
    FOnOpenClick: TNotifyEvent;
    FOnSendTextClick: TNotifyEvent;
    FOnCloseClick: TNotifyEvent;
    FOnSendStreamClick: TNotifyEvent;
  protected
    procedure Paint; override;
    procedure Resize; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    property Frame: TABSockFrame read FFrame write FFrame;
    property OnBegReadData: TNotifyEvent read FOnBegReadData write FOnBegReadData;
    property OnEndReadData: TNotifyEvent read FOnEndReadData write FOnEndReadData;

    property OnOpenClick: TNotifyEvent read FOnOpenClick write FOnOpenClick;
    property OnCloseClick: TNotifyEvent read FOnCloseClick write FOnCloseClick;
    property OnSendTextClick: TNotifyEvent read FOnSendTextClick write FOnSendTextClick;
    property OnSendByteArrayClick: TNotifyEvent read FOnSendByteArrayClick write FOnSendByteArrayClick;
    property OnSendStreamClick: TNotifyEvent read FOnSendStreamClick write FOnSendStreamClick;
  end;

implementation
{$R *.dfm}

procedure TABSockFrame.ComboBox1Change(Sender: TObject);
begin
  if ComboBox1.ItemIndex = 0 then
  begin
    ABSetControlVisible([Panel5,Panel3,Panel6], true);
    SpinEdit3.MaxValue:=65507;
  end
  else if ComboBox1.ItemIndex = 1 then
  begin
    ABSetControlVisible([Panel3,Panel6], false);
    ABSetControlVisible([Panel5], true);
    SpinEdit3.MaxValue:=MaxInt;
  end
  else if ComboBox1.ItemIndex = 2 then
  begin
    ABSetControlVisible([Panel5,Panel6], false);
    ABSetControlVisible([Panel3], true);
    SpinEdit3.MaxValue:=MaxInt;
  end;
  SpinEdit3.Value:=1024;  //  1024 65507 MaxInt

  Panel2.Top:=0;
  Panel5.Top:=100;
  Panel3.Top:=200;
  Panel6.Top:=300;

  Panel4.Height:=ABIIF(Panel2.Visible,Panel2.Height,0)+ABIIF(Panel5.Visible,Panel5.Height,0)+
                 ABIIF(Panel3.Visible,Panel3.Height,0)+ABIIF(Panel6.Visible,Panel6.Height,0);
end;

procedure TABSockFrame.Button1Click(Sender: TObject);
var
  tempOk: Boolean;
begin
  ABSocket1.Close;
  ABSocket1.BuffSize := SpinEdit3.Value;
  ABSocket1.LocalPort := SpinEdit1.Value;
  ABSocket1.MaxError := SpinEdit4.Value;
  ABSocket1.GroupBroadcastIP := Edit3.Text;
  ABSocket1.SocketType :=TABSocketType(ComboBox1.ItemIndex);
  ABSocket1.SocketModel :=TABSocketModel(ComboBox2.ItemIndex);
  ABSocket1.ToHost := Edit1.Text;
  ABSocket1.ToPort := SpinEdit2.Value;
  if Assigned(TABSockUI(Owner).FOnOpenClick) then
  begin
    TABSockUI(Owner).FOnOpenClick(Sender);
  end
  else
  begin
    ABSocket1.Open;
  end;

  if ComboBox1.ItemIndex = 0 then
  begin
    tempOk := (ABSocket1.SocketState = ssOpen);
    if tempOk then
    begin
      ABSetControlEnabled([Panel5,Panel6], false);
      StatusBar1.SimpleText := '监听已开启';
    end;
  end
  else if ComboBox1.ItemIndex = 1 then
  begin
    tempOk := (ABSocket1.SocketState = ssOpen);
    if tempOk then
    begin
      ABSetControlEnabled([Panel5], false);
      StatusBar1.SimpleText := '监听已开启';
    end;
  end
  else //if ComboBox1.ItemIndex = 2 then
  begin
    tempOk := (ABSocket1.SocketState = ssOpen);
    if tempOk then
    begin
      ABSetControlEnabled([Panel3], false);
      StatusBar1.SimpleText := '连接已开启';
    end;
  end;

  if tempOk then
  begin
    ABSetControlEnabled([Button1, Panel2], false);
    ABSetControlEnabled([Button2, Button3, Button4,Button6,Button7], true);
  end;
end;

procedure TABSockFrame.Button2Click(Sender: TObject);
var
  tempOk: Boolean;
begin
  if Assigned(TABSockUI(Owner).FOnCloseClick) then
  begin
    TABSockUI(Owner).FOnCloseClick(Sender);
  end;

  tempOk:=false;
  ABSocket1.Close;
  if ComboBox1.ItemIndex = 0 then
  begin
    tempOk := (ABSocket1.SocketState = ssClosed);
  end
  else if ComboBox1.ItemIndex = 1 then
  begin
    tempOk := (ABSocket1.SocketState = ssClosed);
  end
  else if ComboBox1.ItemIndex = 2 then
  begin
    tempOk := (ABSocket1.SocketState = ssClosed);
  end;

  if tempOk then
  begin
    StatusBar1.SimpleText := '连接或监听已关闭';
    ABSetControlEnabled([Button1,Panel2,Panel3,Panel5,Panel6], true);
    ABSetControlEnabled([Button2, Button3, Button4,Button6,Button7], false);
  end;
end;

procedure TABSockFrame.Button3Click(Sender: TObject);
begin
  if Assigned(TABSockUI(Owner).FOnSendTextClick) then
  begin
    TABSockUI(Owner).FOnSendTextClick(Sender);
  end
  else
  begin
    SendSocketObject(soText,Sender);
  end;
end;

procedure TABSockFrame.Button7Click(Sender: TObject);
begin
  if Assigned(TABSockUI(Owner).FOnSendByteArrayClick) then
  begin
    TABSockUI(Owner).FOnSendByteArrayClick(Sender);
  end
  else
  begin
    SendSocketObject(soByteArray,Sender);
  end;
end;

procedure TABSockFrame.MenuItem1Click(Sender: TObject);
begin
  if Assigned(TABSockUI(Owner).FOnSendStreamClick) then
  begin
    TABSockUI(Owner).FOnSendStreamClick(Sender);
  end
  else
  begin
    SendSocketObject(soStream,Sender);
  end;
end;

procedure TABSockFrame.MenuItem2Click(Sender: TObject);
begin
  if Assigned(TABSockUI(Owner).FOnSendStreamClick) then
  begin
    TABSockUI(Owner).FOnSendStreamClick(Sender);
  end
  else
  begin
    SendSocketObject(soStream,Sender);
  end;
end;

procedure TABSockFrame.N1Click(Sender: TObject);
begin
  if Assigned(TABSockUI(Owner).FOnSendStreamClick) then
  begin
    TABSockUI(Owner).FOnSendStreamClick(Sender);
  end
  else
  begin
    SendSocketObject(soFile,Sender);
  end;
end;

procedure TABSockFrame.N2Click(Sender: TObject);
begin
  if Assigned(TABSockUI(Owner).FOnSendStreamClick) then
  begin
    TABSockUI(Owner).FOnSendStreamClick(Sender);
  end
  else
  begin
    SendSocketObject(soFile,Sender);
  end;
end;

procedure TABSockFrame.SendSocketObject(aSocketObject: TSocketObject;Sender: TObject);
var
  I,j,tempcount: Integer;
  tempFileName: string;
  tempFileStream:TStream;
  tempIsFileStream:Boolean;
  tempProgressBar:TProgressBar;
begin
  tempFileStream:=nil;
  tempIsFileStream:=false;
  tempProgressBar:=ProgressBar1;
  if (aSocketObject=soFile) or
     (aSocketObject=soStream) or
     (aSocketObject=soByteArray)
     then
  begin
    tempcount:=1;
    tempFileName := ABSelectFile;
    if tempFileName = EmptyStr then
    begin
      exit;
    end;

    if (aSocketObject=soFile)  then
      tempIsFileStream:=ABIIF(Sender=N1,True,false)
    else if (aSocketObject=soStream) then
    begin
      tempIsFileStream:=ABIIF(Sender=MenuItem1,True,false);
      if tempIsFileStream then
      begin
        tempFileStream:=TFileStream.Create(tempFileName,fmOpenRead);
      end
      else
      begin
        tempFileStream:=TMemoryStream.Create;
        TMemoryStream(tempFileStream).LoadFromFile(tempFileName);
      end;
    end
    else if (aSocketObject=soByteArray) then
    begin
      tempFileStream:=TMemoryStream.Create;
      TMemoryStream(tempFileStream).LoadFromFile(tempFileName);
    end
  end
  else
  begin
    tempcount:=StrToIntDef(Edit2.Text, 1);
    if tempcount<=0  then
      tempcount:=1;
    if tempcount>1 then
    begin
      ProgressBar1.Min:=0;
      ProgressBar1.Max:=tempcount;
      ProgressBar1.Position:=0;
      tempProgressBar:=nil;
    end;
  end;

  if Assigned(TABSockUI(Owner).FOnBegReadData) then
    TABSockUI(Owner).FOnBegReadData(Owner);
  Button3.Enabled:=False;
  Button4.Enabled:=False;
  Button6.Enabled:=False;
  Button7.Enabled:=False;
  try
    AppendLog('Begin:'+ABDateTimeToStr(now));
    for I := 1 to tempcount do
    begin
      if ABSocket1.SocketState=ssClosed then
        Break;

      if tempcount>1 then
        ProgressBar1.Position:=i;

      if ComboBox1.ItemIndex = 0 then
      begin
        case aSocketObject of
          soText:
          begin
            ABSocket1.SendText(ansistring(Memo1.Text),Edit1.Text, SpinEdit2.Value,tempProgressBar);
          end;
          soFile:
          begin
            ABSocket1.SendFile(tempFileName,Edit1.Text, SpinEdit2.Value,tempProgressBar,tempIsFileStream);
          end;
          soStream:
          begin
            ABSocket1.SendStream(tempFileStream,Edit1.Text, SpinEdit2.Value,tempProgressBar);
          end;
          soByteArray:
          begin
            ABSocket1.SendByteArray(TMemoryStream(tempFileStream).Memory,tempFileStream.Size,Edit1.Text, SpinEdit2.Value,tempProgressBar);
          end;
        end;
      end
      else if ComboBox1.ItemIndex = 1 then
      begin
        for j := 0 to ABSocket1.ClientLists.Count - 1 do
        begin
          case aSocketObject of
            soText:
            begin
              ABSocket1.SendText(ABSocket1.ClientLists.Sockets[j],ansistring(Memo1.Text),tempProgressBar);
            end;
            soFile:
            begin
              ABSocket1.SendFile(ABSocket1.ClientLists.Sockets[j],tempFileName,tempProgressBar,tempIsFileStream);
            end;
            soStream:
            begin
              ABSocket1.SendStream(ABSocket1.ClientLists.Sockets[j],tempFileStream,tempProgressBar);
            end;
            soByteArray:
            begin
              ABSocket1.SendByteArray(ABSocket1.ClientLists.Sockets[j],TMemoryStream(tempFileStream).Memory,tempFileStream.Size,tempProgressBar);
            end;
          end;
        end;
      end
      else if ComboBox1.ItemIndex = 2 then
      begin
        case aSocketObject of
          soText:
          begin
            ABSocket1.SendText(ansistring(Memo1.Text),tempProgressBar);
          end;
          soFile:
          begin
            ABSocket1.SendFile(tempFileName,tempProgressBar,tempIsFileStream);
          end;
          soStream:
          begin
            ABSocket1.SendStream(tempFileStream,tempProgressBar);
          end;
          soByteArray:
          begin
            ABSocket1.SendByteArray(TMemoryStream(tempFileStream).Memory,tempFileStream.Size,tempProgressBar);
          end;
        end;
      end;
    end;
    AppendLog('End  :'+ABDateTimeToStr(now));
  finally
    if tempcount>1 then
      ProgressBar1.Position:=0;

    if Assigned(tempFileStream) then
      tempFileStream.Free;

    if Assigned(TABSockUI(Owner).FOnEndReadData) then
      TABSockUI(Owner).FOnEndReadData(Owner);
    Button3.Enabled:=true;
    Button4.Enabled:=true;
    Button6.Enabled:=true;
    Button7.Enabled:=true;
  end;
end;

procedure TABSockFrame.ABSocket1Close(Sender: TObject; aSocket: Cardinal);
begin
  Button2Click(Button2);
end;

procedure TABSockFrame.ABSocket1Data(Sender: TObject; aSocket: Cardinal;
  aFromIP, aFromPort: string; aSocketPackage: TSocketPackage; aBuff: PByte;
  aFileName: string);
var
  tempFromIPandPort: string;
begin
  tempFromIPandPort :='[' +
                      'FromIP=' +aFromIP+','+
                      'FromPort=' +aFromPort+','+
                      'Type='+GetEnumName(Typeinfo(Tsocketobject),ord(aSocketPackage._Type))+','+
                      'Number='+inttostr(aSocketPackage._Number)+','+
                      'DataLength='+inttostr(aSocketPackage._DataLength)+
                      ']'+ABEnterWrapStr;

  case aSocketPackage._Type of
    soText:
    begin
      AppendLog(tempFromIPandPort +string(ansistring(pansichar(aBuff))));
    end;
    //数据流和字节流暂时用处理文件的方式来处理，接收时保存在文件中
    soFile,soStream,soByteArray:
    begin
      AppendLog(tempFromIPandPort +aFileName);
    end;
    soTextACK,soFileACK,soStreamACK,soByteArrayACK:
    begin
      AppendLog(tempFromIPandPort +'ACK');
    end;
  end;
end;

procedure TABSockFrame.ABSocket1Error(Sender: TObject; Error: Integer;
  Msg: string);
begin
  AppendLog('['+ABDateTimeToStr(now,23)+']=Error=[' + inttostr(Error) + ':' + Msg+']');
end;

procedure TABSockFrame.ABSocket1Event(Sender: TObject; aSocket: Cardinal;
  aEvent: Integer; aEventDesc: string);
begin
//  AppendLog('['+ABDateTimeToStr(now,23)+']=Event=[' + inttostr(aEvent) + ':' + aEventDesc+']');
end;

procedure TABSockFrame.AppendLog(aStr: string);
begin
  Memo2.Lines.Add(aStr);
end;

procedure TABSockFrame.Button5Click(Sender: TObject);
begin
  Memo2.clear;
end;

constructor TABSockFrame.Create(AOwner: TComponent);
begin
  inherited;
end;

destructor TABSockFrame.Destroy;
begin
  inherited;
end;

procedure TABSockFrame.Memo1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Shift = [ssCtrl]) then
  begin
    if (Key = 65) then
    begin
      TMemo(Sender).SelectAll;
    end;
  end;
end;

{ TABItemList }

constructor TABSockUI.Create(AOwner: TComponent);
begin
  inherited;
  FFrame := TABSockFrame.Create(self);
  FFrame.Parent := Self;
  FFrame.Show;
end;

destructor TABSockUI.Destroy;
begin
  FFrame.Free;
  inherited;
end;

procedure TABSockUI.Resize;
begin
  inherited;
end;

procedure TABSockUI.Paint;
begin
  inherited;
end;

end.

