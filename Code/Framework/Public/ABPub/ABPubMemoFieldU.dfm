object ABMemoFieldForm: TABMemoFieldForm
  Left = 389
  Top = 204
  Caption = 'Memo'#23383#27573#32534#36753#31383#20307
  ClientHeight = 400
  ClientWidth = 500
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object DBMemo1: TDBMemo
    Left = 0
    Top = 0
    Width = 500
    Height = 359
    Align = alClient
    DataSource = DataSource1
    ImeName = #24555#20048#20116#31508
    ScrollBars = ssBoth
    TabOrder = 0
  end
  object pnl1: TPanel
    Left = 0
    Top = 359
    Width = 500
    Height = 41
    Align = alBottom
    TabOrder = 1
    DesignSize = (
      500
      41)
    object btn1: TButton
      Left = 416
      Top = 6
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #21462#28040
      TabOrder = 0
      OnClick = btn1Click
    end
    object Button2: TButton
      Left = 335
      Top = 6
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #30830#35748
      TabOrder = 1
      OnClick = Button2Click
    end
  end
  object DataSource1: TDataSource
    Left = 18
    Top = 92
  end
end
