{
共用的函数与过程单元
}
unit ABPubFuncU;

interface
{$I ..\ABInclude.ini}

uses
  ABPubConstU,
  ABPubVarU,

  printers,
  zlib,
  midaslib,
  Windows, Classes, SysUtils, DB, Menus, Math, Variants,Types,
  Controls, Forms, ComCtrls,Graphics,CheckLst,
  StdCtrls,System.Win.Registry,DateUtils,stdconvs,Messages,Dialogs,FileCtrl,
  ShellAPI, System.Win.ComObj,IniFiles, ShlObj,ActiveX,Winsock2,Tlhelp32,
  StrUtils, TypInfo,Vcl.Imaging.jpeg, mmsystem, Imm,wininet,psAPI;


//*******************日期、时间操作*********************************
//日期时间分解到日期和时间
procedure ABDateTimeToDateAndTime(aDateTime:TDateTime;var  aDate:TDate;var aTime: TTime);
//日期和时间合并到日期时间
function ABDateAndTimeToDatetime( aDate:TDate;aTime: TTime): TDateTime;

//日期时间转成字串
//aDateTime=需转成字串的日期时间
//aLong=需转成字串的长度,默认aLong=19返回日期+时分秒, aLong=23时返回日期+时分秒毫秒
//aDateSpaceSign=日期分隔符
//aTimeSpaceSign=时间分隔符
function ABDateTimeToStr( aDateTime: TDateTime; aLong: LongInt = 19;
                          aDateSpaceSign: string = '-'; aTimeSpaceSign: string = ':'): string;
//日期转成字串
function ABDateToStr(aDateTime: TDateTime; aDateSpaceSign: string = '-'): string;
//时间转成字串 ,默认aLong=8返回时分秒, aLong=12时返回时分秒毫秒
function ABTimeToStr(aDateTime: TDateTime; aLong: LongInt = 8; aTimeSpaceSign:string = ':'): string;

//字串转成日期时间，如果aStr中日期时间格式不对则异常
function ABStrToDateTime( aStr: string;
                          aDateSpaceSign: string = '-'; aTimeSpaceSign: string = ':'): TDateTime;
//字串转成日期时间(当转换失败时返回aDefDateTime)
function ABStrToDateTimeDef(aStr: string;aDefDateTime:TDateTime;
                            aDateSpaceSign: string = '-'; aTimeSpaceSign: string = ':'): TDateTime;
//字串转成日期
function ABStrToDate(aStr: string; aDateSpaceSign: string = '-'): TDate;
//字串转成时间
function ABStrToTime(aStr: string;aDateSpaceSign: string='-' ; aTimeSpaceSign:string=':' ): TTime;

//取得日期中月份的第一天的日期
function ABGetMonthFirstDate(aDate: TDate): TDate;
//取得日期中月份的最后一天的日期
function ABGetMonthEndDate(aDate: TDate): TDate;
//取得日期中上月的第一天的日期
function ABGetLastMonthFirstDate(aDate: TDate): TDate;
//取得日期中上月的最后一天的日期
function ABGetLastMonthEndDate(aDate: TDate): TDate;
//取得日期中本季的第一天的日期
function ABGetSeasonFirstDate(aDate: TDate): TDate;
//取得日期中本季的最后一天的日期
function ABGetSeasonEndDate(aDate: TDate): TDate;
//取得日期中上季的第一天的日期
function ABGetLastSeasonFirstDate(aDate: TDate): TDate;
//取得日期中上季的最后一天的日期
function ABGetLastSeasonEndDate(aDate: TDate): TDate;
//取得日期中本年的第一天的日期
function ABGetYearFirstDate(aDate: TDate): TDate;
//取得日期中本年的最后一天的日期
function ABGetYearEndDate(aDate: TDate): TDate;
//取得日期中上年的第一天的日期
function ABGetLastYearFirstDate(aDate: TDate): TDate;
//取得日期中上年的最后一天的日期
function ABGetLastYearEndDate(aDate: TDate): TDate;
//取得日期中上年同期天
function ABGetLastYearSameDate(aDate: TDate): TDate;
//取得日期是一年的第几周
function ABWeekOfYear(aData: TDate): Integer;

//判断日期年是否为闰年
function ABIsLeapYear(aYear: Integer): Boolean;
//多个日期取较大的日期
function ABMaxDateTime(const aDateTimes: array of TDateTime): TDateTime;
//多个日期取较小的日期
function ABMinDateTime(const aDateTimes: array of TDateTime): TDateTime;

//两个日期时间相差的年,月,周,天,小时,分,秒,毫秒；(返回包含小数)，aResUnit返回类型如下
//  tuMilliSeconds: TConvType;
//  tuSeconds: TConvType;
//  tuMinutes: TConvType;
//  tuHours: TConvType;
//  tuDays: TConvType;
//  tuWeeks: TConvType;
//  tuMonths: TConvType;
//  tuYears: TConvType;
function ABGetDateTimeSpan_Float(aBeg,
                                 aEnd: TDateTime;
                                 aResUnit: Word): Double;
//两个日期时间相差的年,月,周,天,小时,分,秒,毫秒；(四会五入后返回整数) ,aResUnit返回类型如下
//  tuMilliSeconds: TConvType;
//  tuSeconds: TConvType;
//  tuMinutes: TConvType;
//  tuHours: TConvType;
//  tuDays: TConvType;
//  tuWeeks: TConvType;
//  tuMonths: TConvType;
//  tuYears: TConvType;
function ABGetDateTimeSpan_Int(aBeg,
                               aEnd: TDateTime;
                               aResUnit: Word): LongInt;
// 返回当前系统的GMT/UTC时间，即零时区时间
//协调世界时(Universal Time Coordinated)，过去曾用格林威治平均时(GMT)来表示.
//北京时间比UTC时间早8小时，以1999年1月1日0000UTC为例，UTC时间是零点，北京时间为1999年1月1日早上8点整。
function ABGetGMT: TDateTime; stdcall;
//转换本地时间为GMT/UTC时间
function ABLocaleToGMT(const aDateTime: TDateTime): TDateTime; stdcall;
//转换GMT/UTC时间为本地时间
function ABGMTToLocale(const aDateTime: TDateTime): TDateTime; stdcall;

//********************数学函数操作****************************
//二进制字符转十进制
function ABBinToDec(aValue: string): string;
//十进制转化二进制
function ABDecTobin(aValue: int64): string;

//十六进制转化成二进制
function ABHexToBin(aHex: string): string;
//二进制转化成十六进制
function ABBinToHex(aBin: string): string;

//取得与ORD函数的差量,为数字时取与'0'的差量,为字符时取与'A'的差量
function ABTransChar(const aStr: string): Integer;

//判断aCh是否是空白字符
function ABIsSpace(const aStr: string): Boolean;
//判断aCh是否是十六进制数字字符
function ABIsXDigit(const aStr: string): Boolean;
//判断一个字符串是否是整数
function ABIsInteger(const aStr: string): Boolean;
function ABIsInt64(const aStr: string): Boolean;
//判断字符串是否是浮点数
function ABIsFloat(const aStr: string): Boolean;

//输出长度为aLength的随机字符串(26个字母)
function ABRandomStr(aLength: Longint): string;overload;
//输出值在aBegNum,aEndNum之间的随机数字符串
function ABRandomStr(aBegNum,aEndNum: Integer): string;overload;

//输出长度为aLength的随机汉字
function ABRandomChinese(aLength: Longint): string;

//输出最大值为aMaxNum的随机数
function ABRandomNum(aMaxNum: Integer): integer;overload;
//输出值在aBegNum,aEndNum之间的随机数
function ABRandomNum(aBegNum,aEndNum: Integer): integer;overload;

//布尔转字符串
function ABBoolToStr(aBool: Boolean): string;
function ABBoolToDefStr(aBool: Boolean;aTrueValue:string;aFalseValue:string): string;
//布尔转Int
function ABBoolToInt(aBool: Boolean): LongInt;
function ABBoolToDefInt(aBool: Boolean;aTrueValue:LongInt;aFalseValue:LongInt): LongInt;
//字符串转布尔
function ABStrToBool(aValue: string): Boolean;
function ABDefStrToBool(aValue: string;aTrueValueArray:array of string): Boolean;
//Int转布尔
function ABIntToBool(aValue: LongInt): Boolean;
function ABDefIntToBool(aValue: LongInt;aTrueValueArray:array of LongInt): Boolean;

//判断整数aValue是否在aMin和aMax之间
function ABInBound(aValue: Integer;
                   aMin,
                   aMax: Integer): Boolean;
//检测aValue，限制输出在aMin..aMax之间
function ABTrimInt(aValue,
                   aMin,
                   aMax: Integer): Integer;
//交换两个数
procedure ABSwap(var aA, aB: Variant);

//合并左上角坐标ax, ay和宽度aWidth、高度aHeight为aRect
procedure ABEnRect(var aRect: TRect;
                   ax, ay, aWidth, aHeight: Integer);
//分解aRect为左上角坐标ax, ay和宽度aWidth、高度aHeight
procedure ABDeRect(aRect: TRect; var ax, ay, aWidth, aHeight: Integer);
//比较aRect1, aRect2是否相等
function ABRectEqu(aRect1, aRect2: TRect): Boolean;

//合并acx, acy为aSize
procedure ABEnSize(var aSize: TSize;acx, acy: Integer);
//分解aSize为acx, acy
procedure ABDeSize(aSize: TSize; var acx, acy: Integer);
//比较aSize1, aSize2是否相等
function ABSizeEqu(aSize1, aSize2: TSize): Boolean;

//向上取整: （取得大于等于aX的最小的整数）如：ceil(-123.55)=-123， ceil(123.15)=124
function ABCeil(aX: Extended): Integer;
//向下取整:（取得小于等于aX的最大的整数）如：floor(-123.55)=-124，floor(123.55)=123
function ABFloor(aX: Extended): Integer;
//四舍五入后取整
function ABTrunc(aNum: double): Integer;
//指定小数位数四舍五入
function ABRound(aNum: double; aPosition: integer=0): double;
//指定小数位数四舍五入，返回字串，aFillZero=True时为不足的小数位置填充0
function ABRoundStr(aNum: double; aPosition: integer;aFillZero:Boolean=false): string;
// 交换高低字节
function ABxhl(const aByte: Byte): Byte; overload;
// 交换高低字节
function ABxhl(const aWord: Word): Word; overload;
// 交换高低字节
function ABxhl(const aDWord: DWord): Dword; overload;

//比较两个浮点数(返回值=1为aNum1>aNum2,返回值=0为相等,返回值=-1为aNum1<aNum2
function ABCompareDouble(aNum1,aNum2: double; aPosition: integer = 8): longint;
//正整数数组排序
procedure ABIntArraySort(var aIntArray:array of LongInt);
//求最大公约数
function ABGetGreatestCommonDivisor(ax,ay: LongInt): LongInt;
//求最小公倍数
function ABGetLeaseCommonMultiple(ax,ay: LongInt): LongInt;

//Var转换成Int64
function ABVartoInt64Def(aValue:Variant;aDefault:Int64=0):Int64;
//表达式是否是数字表达式
function ABIsNumExpression(aExpression: string): Boolean;
//四则运算表达式求值,如果执行异常则aError=True
function ABExpressionEval(aExpression: string; var aError: Boolean): Variant;

//*******************注册表操作*********************************
//aRootKey指定当前操作的注册表主键
//共有HKEY_CLASSES_ROOT、HKEY_CURRENT_USER、HKEY_LOCAL_MACHINE、HKEY_USERS、HKEY_CURRENT_CONFIG五种取值
//检测注册表路径是否存在
function ABFindRegPath(const aPath: string;aRootKey: HKEY =HKEY_LOCAL_MACHINE): Boolean;
//创建注册表路径
function ABCreateRegPath(const aPath: string;aRootKey: HKEY =HKEY_LOCAL_MACHINE): Boolean;
//删除注册表路径
function ABDeleteRegPath(const aPath: string;aRootKey: HKEY =HKEY_LOCAL_MACHINE): Boolean;

//检测注册表键值是否存在
function ABFindRegKey(aPath: string; aKeyName: string;aRootKey: HKEY =HKEY_LOCAL_MACHINE): Boolean;
//读注册表键值
function ABReadRegKey(aPath: string; aKeyName: string;aRootKey: HKEY =HKEY_LOCAL_MACHINE): string;
//写注册表键值
function ABWriteRegKey( aPath: string; aKeyName, aKeyValue: string;
                        aValueType: TABRegKeyValueType = rtString; aRootKey: HKEY =HKEY_LOCAL_MACHINE):boolean;
//删除注册表键值
function ABDeleteRegKey( aPath: string; aKeyName: string;aRootKey: HKEY =HKEY_LOCAL_MACHINE):boolean;
//设置随系统启动的程序
//aAutoRunType=atRun 每次系统启动时都启动,aAutoRunType=atRunOnce 仅下次系统启动时启动
//aAutoRunName=自启动名称，
//aAutoRunFileName=自启动的文件名
//aParams=自启动的参数，如'hide'表示隐藏方式运行
procedure ABAddAutoRun(aAutoRunType: TABAutoRunType;aAutoRunName, aAutoRunFileName: string;aParams:string='');
//删除随系统启动的程序
procedure ABDelAutoRun(aAutoRunType: TABAutoRunType;aAutoRunName: string);

//*******************数组操作*********************************
//数组复制
procedure ABCopyStringArray  (aSArrays:array of string  ; aDArrays:TStringDynArray  );
procedure ABCopyIntegerArray (aSArrays:array of Integer ; aDArrays:TIntegerDynArray );
procedure ABCopyByteArray    (aSArrays:array of Byte    ; aDArrays:TByteDynArray    );
procedure ABCopyDoubleArray  (aSArrays:array of Double  ; aDArrays:TDoubleDynArray  );
procedure ABCopyBooleanArray (aSArrays:array of Boolean ; aDArrays:TBooleanDynArray );
//数组转成字串
function ABStringArrayToStr  (aArrays:array of string  ; aQuoted: Boolean = false;aSpaceSign: string = ','): string;
function ABAnsiCharArrayToStr(aArrays:array of AnsiChar; aQuoted: Boolean = false;aSpaceSign: string = ','): string;
function ABWideCharArrayToStr(aArrays:array of WideChar; aQuoted: Boolean = false;aSpaceSign: string = ','): string;
function ABIntegerArrayToStr (aArrays:array of Integer ; aQuoted: Boolean = false;aSpaceSign: string = ','): string;
function ABByteArrayToStr    (aArrays:array of Byte    ; aQuoted: Boolean = false;aSpaceSign: string = ','): string;
function ABDoubleArrayToStr  (aArrays:array of Double  ; aQuoted: Boolean = false;aSpaceSign: string = ','): string;
function ABBooleanArrayToStr (aArrays:array of Boolean ; aQuoted: Boolean = false;aSpaceSign: string = ','): string;
function ABVariantArrayToStr (aArrays:array of Variant ; aQuoted: Boolean = false;aSpaceSign: string = ','): string;overload;
function ABVariantArrayToStr (aArrays:Variant ; aQuoted: Boolean = false;aSpaceSign: string = ','): string;overload;
//字串转成数组
function ABStrToStringArray   (aStr: string;aSpaceSign: string=','; aSetCount:LongInt=0; aDefaultValue:string  =''):TStringDynArray;
function ABStrToAnsiCharArray (aStr: string;aSpaceSign: string=','; aSetCount:LongInt=0; aDefaultValue:AnsiChar=#0):TABAnsiCharDynArray;
function ABStrToWideCharArray (aStr: string;aSpaceSign: string=','; aSetCount:LongInt=0; aDefaultValue:WideChar=#0):TABWideCharDynArray;
function ABStrToIntegerArray  (aStr: string;aSpaceSign: string=','; aSetCount:LongInt=0; aDefaultValue:Integer =0 ):TIntegerDynArray;
function ABStrToByteArray     (aStr: string;aSpaceSign: string=','; aSetCount:LongInt=0; aDefaultValue:Byte    =0 ):TByteDynArray;
function ABStrToDoubleArray   (aStr: string;aSpaceSign: string=','; aSetCount:LongInt=0; aDefaultValue:Double  =0 ):TDoubleDynArray;
function ABStrToBooleanArray  (aStr: string;aSpaceSign: string=','; aSetCount:LongInt=0; aDefaultValue:Boolean =false ):TBooleanDynArray;
//在数组中删除另一数据的项目
function ABDeleteInArrayItem(aArrays:array of string;aDeleteArrays:array of string):TStringDynArray;

//变量是否等于变量数组其中的某一个元素(不支持匹配选项，一般用作数值比较)
function ABVariantInArray(aValue: Variant; aVariants: array of Variant): Boolean;
//字串是否匹配字串数组中的某一个元素
function ABStrInArray(aStr: string; aStrArray: array of string; aOptions:TLocateOptions= [loCaseInsensitive]): LongInt;
//由分隔符分隔的字串所有子项是否匹配字串数组中的某一个元素
function ABStrsInArray(aStrs: string; aSpaceSign:string; aStrArray: array of string; aOptions:TLocateOptions = [loCaseInsensitive]): Boolean;
//字串数组中的某一个元素是否匹配字串
function ABArrayInStr(aStrArray: array of string; aStr: string; Options:TLocateOptions = [loCaseInsensitive]): Boolean;

//*******************目录,文件操作相关*********************************
//将Byte数组生成文件
procedure ABByteArrayWriteFile(aByteDynArray: TByteDynArray;aCount:LongInt;aFileName : string );
//将文件生成Byte数组
function ABFileToByteArray(const aFileName:string ):TByteDynArray;
//将Byte数组加到文件尾部
procedure ABByteArrayAppendFile(aByteDynArray: TByteDynArray;aCount:LongInt;aFileName : string );
//得到文件或目录的盘符
//aPathAndFileName=包含盘符的文件或目录名称
//aDeleteMaoHao=是否删除盘符中的冒号
function ABGetDriverName(aPathAndFileName:string;aDeleteMaoHao:boolean=false): string;
//取文件的版本号(适用于EXE，BPL，DLL等)
function ABGetFileVersion(aFileName: string): string;
//取较大的版本号
function ABGetMaxVersion(const aVersion1, aVersion2: string): string;
//取较小的版本号
function ABGetMinVersion(const aVersion1, aVersion2: string): string;

//取DOS短文件名
function ABGetShortFileName(const aFileName: string): string;
//取DOS短路径名
function ABGetShortPath(const aPath: string): string;

//返回输入文件的路径,有\结尾
function ABGetFilePath(aFileName: string): string;
//返回输入文件的目录,无\结尾
function ABGetFileDir(aFileName: string): string;
//返回输入目录或路径的目录
function ABGetDir(aDirOrPath: string): string;overload;
//返回..\开始的绝对目录 (aBasePath=..所在的目录或路径
function ABGetDir(aBaseDirOrPath,aDianDianDirOrPath: string): string;overload;
//返回输入目录或路径的路径
function ABGetPath(aDirOrPath: string): string;overload;
//返回..\开始的绝对路径 (aBasePath=..所在的目录或路径
function ABGetPath(aBaseDirOrPath,aDianDianDirOrPath: string): string;overload;

//取得上级路径
function ABGetParentPath(aDirOrPath: string): string;
//取得上级目录
function ABGetParentDir(aDirOrPath: string): string;
//取得最后一级目录名称
function ABGetLastDirName(aDirOrPath: string): string;

//得到当前目录
function ABGetCurrentDir: string;
//设置当前目录
procedure ABSetCurrentDir(aDirOrPath: string);
//返回包含绝对文件名的完整文件名字符串(当前目录路径+aFileName)
function ABGetFileFullName(aFileName: string): string;
//仅返回文件名,不返回路径,aIsDelExt=True则返回的文件名不包含扩展名
function ABGetFilename(aFileName: string; aIsDelExt: Boolean = true): string;
//返回文件的扩展名,aIsDelDian=True则返回的扩展名不包含点
function ABGetFileExt(aFileName: string; aIsDelDian: Boolean = true): string;
//返回URL下载地址的文件名(包含扩展名)
function ABGetURLFileName(aURL: string): string;

//选择目录对话框
function ABSelectDirectory(aDirOrPath: string= ''; aTitleCaption: string = ''): string;
//选择文件对话框,aMultiFiles=多选时的多文件名列表
function ABSelectFile(aDefaultDir: string = ''; aDefaultFile: string = '';aFilter: string = '';
                      aMultiSelect:Boolean=false;aMultiFiles:Tstrings=nil;aOwner:TComponent=nil): string;
//保存文件对话框
function ABSaveFile(aDefaultDirOrPath: string = ''; aDefaultFile: string = '';aFilter: string = '';aOwner:TComponent=nil): string;
//得到保存对话框的文件名（没加扩展名时会自动加扩展名）
function ABGetSaveDialogFileName(aSaveDialog:TSaveDialog): string;

//检测目录是否存在
function ABCheckDirExists(aDirOrPath: string): boolean;
//检查文件是否存在
function ABCheckFileExists(aFileName: string): boolean;
//取一目录下目录数(包含隐藏目录)
//aNameFilters=查询目录时的筛选,支援?與*，可设置多组过滤数组
//aFindLevel=查询的目录层次，=-1不限层查找，否则只查找指定层的目录文件
function ABGetDirCount(aDirOrPath: string;aNameFilters: array of string;aFindLevel: longint = -1): Integer;
//取一目录下的文件数(包含隐藏目录)
//aNameFilters=查询文件时的筛选,支援?與*，可设置多组过滤数组
//aFindLevel=查询的目录层次，=-1不限层查找，否则只查找指定层的目录文件
function ABGetFileCount(aDirOrPath: string;aNameFilters: array of string;aFindLevel: longint=-1;aNameFilterOnlyExt:boolean=False): Integer;
//取一目录下的文件夹列表
//aNoResultDirOrPath=在返回的aDirList列表中是否以传入的aDirOrPath开头
procedure ABGetDirList( aDirList: TStrings;
                        aDirOrPath: string; aNameFilters: array of string;aFindLevel: longint = -1;
                        aProgressBar: TProgressBar = nil;
                        aResultDirOrPath: boolean = True);
//取一目录下的文件列表
//aNameFilterOnlyExt=表示名称过虑是否仅针对FindFirst结果的扩展名
procedure ABGetFileList(aFileList: TStrings;
                        aDirOrPath: string; aNameFilters: array of string;aFindLevel:longint = -1;
                        aNameFilterOnlyExt:boolean=False;
                        aProgressBar: TProgressBar = nil;
                        aResultDirOrPath: boolean = True);
//创建目录
procedure ABCreateDir(aDirOrPath: string);
//创建目录并置空
procedure ABCreateEmptystrDir(aDirOrPath: string);
//删除目录,aIsDelDir=true 删除目录中文件后也删除此目录
procedure ABDeltree(aDirOrPath: string;aIsDelSelf:boolean=true);
//修改目录名称
function ABEditDir(aOldDirOrPath, aNewDirOrPath: string): Boolean;
//拷贝目录
procedure ABCopyDir(aSDirOrPath, aDDirOrPath: string;aFilter:string='*.*';aBeforeCopyDelDDirOrPath:boolean=false;aProgressBar:TProgressBar=nil);
//移动目录
function ABMoveDir(const aSDirOrPath, aDDirOrPath: string;aBeforeMoveDelDDirOrPath:boolean=false): Boolean;

//删除文件(只读等文件属性的文件也可以删除)
function ABDeleteFile(const aFileName: string): Boolean;overload;
function ABDeleteFile(aFileNameStrings: TStrings): Boolean;overload;
//删除文件到回收站
function ABDeleteToRecycleBin(aFilename: string): Boolean;
//不支持进度条方式拷贝文件
function ABCopyFile(aSFileName, aTFileName: string): Boolean; overload;
//支持进度条方式拷贝文件
function ABCopyFile(aSFileName, aTFileName: string; aProgressBar: TProgressBar): Boolean;  overload;
//移动文件
function ABMoveFile(aSFileName, aTFileName: string): Boolean;
//移动aDirOrPath下的文件到aDirOrPath的子目录aSubDirOrPathk中
function ABMoveFileToSubPath(aDirOrPath,aFileName,aSubDirOrPath: string): Boolean;

//取文件时间
function ABGetFileDateTime(aFileName: string; var aCreationTime, aLastWriteTime,aLastAccessTime: TDateTime): Boolean; overload;
function ABGetFileDateTime(aFileName: string; aType: TABFileTimeType): TDateTime;overload;
//设置文件时间
function ABSetFileDateTime(aFileName: string; aCreationTime, aLastWriteTime,aLastAccessTime: TDateTime): Boolean; overload;
function ABSetFileDateTime(aFileName: string; aType: TABFileTimeType; aDateTime:TDateTime): Boolean; overload;

//文件时间转本地时间
function ABFileTimeToDateTime(aFileTime: TFileTime): TDateTime;
//本地时间转文件时间
function ABDateTimeToFileTime(aDateTime: TDateTime): TFileTime;

//读取或设置文件的拍照及其它时间，读取时返回拍照时间
Function ABReadOrEditFileAllDatetime(aFileName:string;aSetDatetime:TDateTime):String;

//打开文件属性窗口
procedure ABShowEditFileProperties(const aFName: string);
//取得文件属性 faReadOnly or faHidden
function ABGetFileAttr(aFileName: string): LongInt;
//设置文件属性 faReadOnly or faHidden
//aArrt=faReadOnly、faHidden、faReadOnly or faHidden
//aType=ftInsert,ftEdit,ftDelete 表示设置的种类是增加、修改、删除属性
procedure ABSetFileAttr(aFileName: string; aArrt: LongInt; aType:TABFileOperateType);
//取文件大小
function ABGetFileSize(aFileName: string): int64;
//取文件大小字串(**GB/MB/KB/B)
//aFileSize=文件的大小
//aDivBCount=aFileSize与字节的关系，如aFileSize传入的是字节单位则aDivBCount=1
function ABGetFileSizeStr(aFileSize: Int64;aDivBCount:LongInt=1): string;

//返回一文本文件的行数
function ABGetTxtline(const aFileName: string): integer;
//文件是否是文本文件
function ABIsTextFile(aFileName:string):boolean;
//返回文本文件编码类型，sText=读出的文件内容
function ABGetTextFileType(const aFileName: string): TABTxtFileType;overload;
function ABGetTextFileType(const aFileName: string; var sText:string): TABTxtFileType;overload;

//兼容多种格式的文件文件读首行 tfAnsi,tfUnicode,tfUnicodeBigEndian,tfUtf8
function ABReadTxtFirstRow(aFilename: string): string;
//兼容多种格式的文件文件读 tfAnsi,tfUnicode,tfUnicodeBigEndian,tfUtf8
function ABReadTxt(aFilename: string): string;
//兼容多种格式的文件文件写
function ABWriteTxt( aFileName: string;  aStr: string;aSaveFileType: TABTxtFileType=tfAnsi; aAppend: boolean = false):boolean;
//将TXT文件中第aBeginPosition个位置起旧串aOldStr替换成新串aNewStr
function ABTxtReplace(aFileName: string; aOldStr, aNewStr: string;aBeginPosition:longint=1): boolean;overload;
//将TXT文件中第aBeginPosition个位置起长度为aLength的部分替换成新串aNewStr
function ABTxtReplace(aFileName: string; aBeginPosition, aLength: Integer; aNewStr:string): boolean;overload;

//DELPHI XE1下的TIniFile仅支持ansi、Unicode格式文件,所以所有的INI文件操作也仅支持这两种格式
//INI文件读
//aEmptystrResultValue=项不存在或值为空时返回的值
//aEmptystrWriteValue=项不存在或值为空时写入文件的值
function ABReadInI(aGroup, aName, aFilename: string;aEmptystrResultValue:string='';aEmptystrWriteValue:string=''): string;overload;
//批量读取多组下项的方法，后继aGroup一样时可只写一个
function ABReadInI(aGroups: array of string; aNames: array of string; aFilename:string;aSpaceSign: string = ','): string;overload;
//批量读取组下所有项到aStrings中
procedure ABReadInI(aGroup, aFilename: string; aStrings: TStrings); overload;
//INI文件写
procedure ABWriteInI(aGroup: string; aName: string;aValue: string; aFilename: string);overload;
//批量写多组下项的方法，后继aGroup一样时可只写一个,aName、aValues要一一对应
procedure ABWriteInI(aGroups: array of string; aNames: array of string;aValues: array of string; aFilename: string); overload;
//批量写aStrings下所有项到某一组中
//aDeleteOldGroup=是否删除组中旧的内容
procedure ABWriteInI(aGroup, aFilename: string; aStrings: TStrings;aDeleteOldGroup:Boolean=true); overload;
//检测INI某项是否存在
function ABCheckInI(aGroup, aName, aFilename: string):Boolean;
//删除组下某项
procedure ABDeleteIni(aGroup, aName, aFilename: string); overload;
//删除组下所有项
procedure ABDeleteIni(aGroup, aFilename: string); overload;

//读TStrings中组aGroup下的aName的值
//aEmptystrResultValue=项不存在或值为空时返回的值
//aEmptystrWriteValue=项不存在或值为空时写入的值
function  ABReadIniInStrings(aGroup, aName: string;aStrings: TStrings; aEmptystrResultValue:string='';aEmptystrWriteValue:string=''): string;overload;
//批量读取TStrings组下所有项到aOutStrings中
procedure ABReadIniInStrings(aGroup: string;aStrings: TStrings; aOutStrings: TStrings); overload;
//写TStrings中组aGroup下的aName的值
procedure ABWriteIniInStrings(aGroup, aName,aValue: string;aStrings: TStrings);

//写TStrings中aName的值
procedure ABWriteItemInStrings(aName,aValue: string;aStrings: TStrings);

//读字串中组aGroup下的aName的值
//aEmptystrResultValue=项不存在或值为空时返回的值
//aEmptystrWriteValue=项不存在或值为空时写入的值
function ABReadIniInText(aGroup, aName: string;var aText: string; aEmptystrResultValue:string='';aEmptystrWriteValue:string=''): string;
//读字串中aName的值
function ABReadItemInTxt(aName: string;aText: string):string;
//写字串中组aGroup下的aName的值
procedure ABWriteIniInText( aGroup, aName,aValue: string;var aText: string);

//创建ACCESS文件,系统需要安装MADAC
procedure ABCreateMDB(aDBName: string);
//判断文件是否正在使用
function ABFileInUse(aFName: string): Boolean;
//打开文件
function ABOpenWith(const aFileName: string): Integer;

//将来源文件指定SHEET页合并到新EXCEL文件（OLE方式速度较慢）
//aSFileNames=需合并的多个EXCEL文件
//aSSheetNames=需合并的EXCEL文件SHEET页名称，
//如[]表示合并所有文件的所有SHEET页，
//  ['sheet1,sheet2']表示合并第一个文件的sheet1,sheet2两页及其它文件的所有页
//aDFileName=合并后新文件的名称
//aDSheetNames=合并后新页面的名称（默认为来源文件名+来源文件的SHEET页名称，当与已有页面名称重复时则不设置名称）
//aErrMsg=失败时返回错讯
//aAppend=是否将来源文件的SHEET页追加到新文件中
//aDelSFile=合并完成后是否删除来源文件
//aOpenNew=合并完成后是否打开新文件
function ABMergerExcel_OLE(aSFileNames: array of string;aSSheetNames: array of string;
                       aDFileName: string;aDSheetNames: array of string;
                       var aErrMsg:string;
                       aAppend:Boolean=false;
                       aDelSFile:Boolean=false;aOpenNew:Boolean=False): boolean;

//*******************图像操作*********************************


//操作图片亮度,实质是每个象素点的R、G、B分量进行调节
//aNumber>0时增加亮度,aNumber<0时减少亮度，
procedure ABOperateBitmapBright(aBitmap: TBitmap; aNumber: LongInt=10);
//操作图片饱和度,实质是每个象素点的R、G、B分量亮的更亮，暗的更暗
//aCheckValue用来判断是亮和暗的分界值
//aNumber>0时增加饱和度,aNumber<0时减少饱和度
procedure ABOperateBitmapSaturation(aBitmap: TBitmap; aNumber: LongInt=10;aCheckValue:LongInt=128);
//操作图片对比度,实质是每个象素点的R、G、B分量全亮的更亮，全暗的更暗
//aCheckValue用来判断是亮和暗的分界值
//aNumber>0时增加对比度,aNumber<0时减少对比度，
procedure ABOperateBitmapContrast(aBitmap: TBitmap;aNumber: LongInt=10;aCheckValue:LongInt=128);

//图像灰度化就是使色彩的三种颜色分量R、G、B的值相同，由于颜色值的取值范围是[0，255]，
//所以灰度的级别只有256种，即灰度图象仅能表现256种灰度颜色，常用有3种处理方法：
//最大值法(Maximum)：R=G=B=Max(R,G,B)，这种方法处理后灰度图象的亮度会偏高。
//平均值法(Average)：R=G=B=(R+G+B)/3，这种方法处理后灰度图象的亮度较柔和。
//加权平均值法(Weighted Average)： 经实际经验和理论推导证明，采用R=G=B=0.30*R+0.59*G+0.11*B可以得到最合理的灰度图象
//二值化图像值只有两种(255,255,255)和(0,0,0)而灰度图图像像素的值有256种(0,0,0)、(1,1,1)...(255,255,255)
//二值化图片
procedure ABBlackWhiteBitmap(aSBitmap,aDBitmap:TBitmap;aType:TABGrayBitmapType=gbtWeightedAvg;aExtValue: integer=128);
//灰化图片,灰度图就是每个像素RGB值相等的"彩色图".
procedure ABGrayBitmap(aSBitmap,aDBitmap:TBitmap;aType:TABGrayBitmapType=gbtWeightedAvg;aExtValue: integer=0);

//图片转化为01字串
function ABBitmapToString(aBitmap:TBitmap;aCheckMoreStr:string='0';aCheckLessStr:string='1';aType:TABGrayBitmapType=gbtAvg;aCheckValue: integer=128):string;

//二值化ImageList中所有图片到文件目录中文件
procedure ABBlackWhiteImageList(aOutPath:string;aImageList:TImageList;aType:TABGrayBitmapType=gbtWeightedAvg;aExtValue: integer=128);
//灰化ImageList中所有图片到文件目录中文件
procedure ABGrayImageList(aOutPath:string;aImageList:TImageList;aType:TABGrayBitmapType=gbtWeightedAvg;aExtValue: integer=0);

//JPG文件与BMP文件互转
//aScale= 生成的Jpg文件和原文件相比的压缩比[1..100] (数字越大生的文件越大)
procedure ABJpgFileToBmpFile(aSFileName, aDFileName:string);
procedure ABBmpFileToJpgFile(aSFileName, aDFileName:string; aScale:TJPEGQualityRange=90);
//BMP文件与ico文件互转
procedure ABIcoFileToBmpFile(aSFileName, aDFileName:string; aClarity:boolean=true);
procedure ABBmpFileToIcoFile(aSFileName, aDFileName:string; aClarity:boolean=true);
procedure ABIconToBitmap(aIcon: TIcon; aBitmap: TBitmap; aClarity:boolean=True);
procedure  ABBitmapToIcon(aBitmap:TBitmap;aIcon:TIcon;aClarity:boolean=True);

//取得与文件相关的图标,成功则返回True
function ABGetFileIcon(aFileName: string; var aIcon: TIcon): Boolean;
//按指定宽和高缩放TGraphic的尺寸
procedure ABChangPictureSize(aInputGraphic:TGraphic;aOutPicture:TPicture; aNewWidth: Integer ; aNewHeight: Integer  );overload;
//按指定倍数缩放TGraphic的尺寸
procedure ABChangPictureSize(aInputGraphic:TGraphic;aOutPicture:TPicture; aTimes: double  );overload;
//按指定倍数缩放TGraphic的尺寸
procedure ABChangPictureSize(aSFileName,aDFileName:string; aTimes: double  );overload;
//按指定宽和高缩放TGraphic的尺寸
procedure ABChangPictureSize(aSFileName,aDFileName:string;  aNewWidth: Integer ; aNewHeight: Integer  );overload;


//*******************集合、枚举操作*********************************
//(不支持集合或枚举内修改列值，如Taaaaaaaaaaa = ( a000,a001=2,a002))

//得到集合变量的元索个数
//var aSetType: TMySet;
//aSetType=[st1,st2];
//ABGetSetCount(aSetType,sizeof(TMySet));
function ABGetSetCount(const aSetType;aSize: Integer): Integer;overload;
//得到集合类型的元索个数
//ABGetSetCount(TypeInfo(TSetDemo));
function ABGetSetCount(aTypeInfo: PTypeInfo): Integer;overload;

//集合转字集串
//aBrackets=是否加中括号
//var aSetType: TMySet;
//aSetType=[st1,st2];
//Edit1.Text := ABSetToStr(TypeInfo(TMySet),aSetType, True);
function ABSetToStr(aTypeInfo: PTypeInfo; const aValue; aBrackets: Boolean = True): string;
//字集串转集合
//ABStrToSet(TypeInfo(TMySet), '[st1,st2]', s);
procedure ABStrToSet(aTypeInfo: PTypeInfo; const aValue: string; out aResult);

//得到枚举类型的元索个数
//type aSetType= (a1,a2,a3);
//ABGetSetCount(TypeInfo(aSetType));
function ABGetEnumCount(aTypeInfo: PTypeInfo): Integer;
//枚举名转值
function ABEnumNameToValue(aTypeInfo: PTypeInfo; aName: string): LongInt;
//枚举值转名
function ABEnumValueToName(aTypeInfo: PTypeInfo; aValue: LongInt): string;

//******************网络操作**************
//从FTP地址信息中取得FTP用户名、密码、主机、端口、目录
//aFtpAddress 格式必须是类似ftp://rec:ooo@192.168.76.11/20050418/abcdef.vox,
procedure ABGetFtpInfo(aFtpAddress: string;
                      var aUserName,aPassWord,aHost,aPort,aDir:string);
//取得FTP的路径(不以'/'结尾的补上'/')
function ABGetFtppath(aDirOrPath: string): string;
//取得Html文件中的文本
function ABGetHtmlFileTxt(aHtmlFilename: string): string;
// 取本机的广播地址
function ABGetBroadIP:string;
//检查是否连上internet
function ABIsOnInternet: boolean;
//得到连网的类型
function ABGetInternetKind: TABIntetnetType;

//******************字符串操作**************
//取得Caption的宽度
function ABGetCaptionWidth(aFont:TFont;aCaption: string): longint;overload;
function ABGetCaptionWidth(aFont:TFont;aCaptionLen: longint): longint;overload;
//检测字串是否以检测串开头
function ABCheckBeginStr(aStr: string; aCheckStr: string): boolean;
//检测字串是否以检测串结尾
function ABCheckEndStr(aStr: string; aCheckStr: string): boolean;
//将子串aSubStr增加到累计串aSumStr中，以aSpaceSign为分隔符，以aDirection决定增加在左边还是右边
procedure ABAddstr(var aSumStr: string; aSubStr: string; aSpaceSign: string=',';aDirection: TABXDirection = axdRight);
//功能与ABAddstr类似，函数返回增加后的累计串
function ABAddstr_Ex(aSumStr: string; aSubStr: string; aSpaceSign: string=',';aDirection: TABXDirection = axdRight): string;

//获得源串aStr中以分隔符aSpaceSign分隔的子串的个数
//如(';aa;',';')=3;(';aa',';')=2;('aa;',';')=2; ('aa',';')=1;('aa;bb;cc',';')=3
function ABGetSpaceStrCount(aStr: string; aSpaceSign: string): longint;
//获得源串aStr中返回第aIndex-1和aIndex个分隔符aSpaceSign间的子串(下标从1开始)
//如(';aa;',1,';')='';(';aa;',2,';')='aa';(';aa;',3,';')=''
//  ('aa;bb;cc',1,';')='aa';('aa;bb;cc',2,';')='bb';('aa;bb;cc',3,';')='cc'
function ABGetSpaceStr(aStr: string; aIndex: longint; aSpaceSign: string): string;
//获得源串aStr中返回子串aSubStr是第几个分隔符aSpaceSign分隔的(下标从1开始)
//如(';aa;','',';')=1;(';aa;','aa',';')=2;('aa;bb;cc','aa',';')=1;('aa;bb;cc','bb',';')=2;('aa;bb;cc','cc',';')=3
function ABGetSpaceIndex(aStr, aSubStr: string; aSpaceSign: string):longint;

//将源串aStr的第aBeginPosition个位置起旧串aOldStr替换成新串aNewStr
function ABStringReplace(aStr: string; aOldStr, aNewStr: string;aBeginPosition:longint=1): string;overload;
//将源串aStr的第aBeginPosition个位置起长度为aLength的部分替换成新串aNewStr
function ABStringReplace(aStr: string; aBeginPosition, aLength: Integer; aNewStr:string): string; overload;
//将源串aStr的第aBeginPosition个位置起aOldStrs数组中包含的字串替换成新串aNewStr
function ABStringReplace(aStr: string; aOldStrs:array of string;aNewStr:string;aBeginPosition:longint=1): string; overload;

//不分大小写的POS
function ABPos(aSubStr,aStr: String; aBeginPosition: LongInt = 1): Integer;
//不分大小写返回子串aSubstr在源串aStr中最后一次出现的位置,而不是第一次出现的位置
function ABRightPos(const aSubstr, aStr: string): Integer;
//不分大小写且支援?與*的Like字符串，aLikeToStr 中支援?與*
function ABLike(aLikeFromStr,aLikeToStr: string): Boolean; overload;
//扩展ABLike,只要数组aStrArray中有一项ABLike返回True则就返回True
function ABLike(aLikeFromStr: string; aLikeToStrArray: array of string): Boolean; overload;

//取得源串aStr中aItemName名称子串后的内容
//aStr=源串
//aItemName=名称子串
//aDelStrs=返回值中要删除的字符
//aEndStr =返回值的结束字符，扫描到此字符时认为结束，返回aItemName后到aEndStr间除了aDelStrs的字串
//aCheckUppLowFlag=扫描时是否区别大小写
//aFind=是否找到
function ABGetItemValue(aStr: string;
                        aItemName: string;
                        aDelStrs: string;
                        aEndStr: string;
                        aCheckUppLowFlag: boolean;
                        var aFind: boolean
                        ): string;overload;
function ABGetItemValue(aStr: string;
                        aItemName: string;
                        aDelStrs: string = '';
                        aEndStr: string = ' ';
                        aCheckUppLowFlag: boolean = false
                        ): string;overload;
//取得aStrings中aItemName名称子串后的内容
function ABGetItemValue(aStrings: Tstrings;
                        aItemName: string;
                        aDelStrs: string;
                        aEndStr: string;
                        aCheckUppLowFlag: boolean;
                        var aFind: boolean
                        ): string;overload;
function ABGetItemValue(aStrings: Tstrings;
                        aItemName: string;
                        aDelStrs: string = '';
                        aEndStr: string = ' ';
                        aCheckUppLowFlag: boolean = false
                        ): string;overload;

//设置源串aStr中aItemName名称子串后的内容
//aStr=源串,返回设置后的串
//aItemName=名称子串
//aItemValue=名称子串
//aEndStr =返回值的结束字符，扫描到此字符时认为结束，返回aItemName后到aEndStr间除了aDelStrs的字串
//aCheckUppLowFlag=扫描时是否区别大小写
function ABSetItemValue(var aStr: string;
                        aItemName: string;
                        aItemValue: string;
                        aEndStr: string = ' ';
                        aCheckUppLowFlag: boolean = false
                        ): boolean;

//删除源串aStr中aItemName名称子串后的内容
function ABDelItemValue(var aStr: string;
                        aItemName: string;
                        aDelStrs: string = '';
                        aEndStr: string = ' ';
                        aCheckUppLowFlag: boolean = false
                        ): boolean;

//返回字符串左边的字符
function ABLeftStr(aStr: string; aLeng: Integer): string;
//返回字符串右边的字符 Examples: StrRight('ABCEDFG',3); 返回:'DFG'
function ABRightStr(aStr: string; aLeng: Integer): string;

//得到GUID
function ABGetGuid: string;
//判断一个字符串是否是合法的GUID
function ABStrIsGUID(const aStr: string): Boolean;

//转换为引号括括起来的串
//如'a'bcd='''a''bcd'
function ABQuotedStr(aStr:string):string;
//去掉引号括括起来的串中的所有引号
//如'''a''bcd'='a'bcd
function ABUnQuotedStr(aStr:string):string;
//仅去掉引号括括起来的串中的首尾引号
//如'''a''bcd'=''a''bcd
function ABUnQuotedFirstlastStr(aStr:string):string;

//检测字串aStr是否以aEndSign结束，如不是则在字串aStr尾部补上aEndSign串
function ABFillEndSign(aStr: string; aEndSign: string = ','): string;
//将字串aStr以aAddStr填充到aSetLength长度，aXDirection决定填充在左边还是右边
function ABFillStr(aStr: string;
                    aSetLength: longint;
                    aAddStr: string = '0';
                    aXDirection: TABXDirection = axdLeft): string;

//取得源串aStr中指定串的左或右部分
//在源串aStr中以分隔符aSpaceSign决定左右子串，再以aReturnDirection决定取左串还是右串返回
//aSpaceSign不存在且取axdLeft时返回aStr，否则返回空串
//aDelStrs=返回值中要删除的字符
//aCheckUppLowFlag=是否区别大小写
function ABGetLeftRightStr( aStr: string; aReturnDirection: TABXDirection = axdLeft;
                            aSpaceSign: string = '='; aDelStrs: string = '';
                            aCheckUppLowFlag: boolean = false):string;
//判断字串是否为空
function ABIsEmptyStr(aStr: string): Boolean;
//取得字符串表示的性别标志, aSexFlag=[]时，默认为 'man' 'woman' 'NoKnow';
//如aStr=True、man、男的、T时返回的是aSexFlag的第一个值，如aStr=False、woman、女的、F时返回的是aSexFlag的第二个值，否则是第三个值
function ABGetSexFlag(aStr: string;aSexFlags:array of string): string;
//比较两个字串,如全为数值则按数值方式比较 ,否则按字符方式比较
function ABCompareNumberOrText(aStr1, aStr2: string): Integer;
//将字串分隔整理，如可将一段文字按分隔符aSpaceSign,按行长度aRowLength进行整理
//整理后得到行最长为aRowLength，以aSpaceSign分隔，固定以aBegStr开始的文本
//如 ('Windows, Classes, SysUtils, DB, Menus, Math, Variants,Types,Controls, Forms, ComCtrls,Graphics;',50,'  ')
//则会返回
//  Windows, Classes, SysUtils, DB, Menus, Math,
//  Variants,Types,Controls, Forms, ComCtrls,Graphics;
function ABCompartTrimStr(aStr: string;aSpaceSign: string=',';aRowLength:longint=80;aBegStr:string=''): string;
//将字串aStr指定每SpLen位以分隔符aSpaceSign进行分隔,如 ('123456789',4,'.')=1.2345.6789
function ABSpaceStr(aStr: string; aSpaceLen: Integer = 3; aSpaceSign: string = ','):string;
//返回字符串的反转(倒置)
function ABReverseString(aStr: string): string;
//复制字符串aCount次
function ABCopyStr(aStr: string; aCount: LongInt; aSpaceSign: string = ''): string;
//取字串aStr开头的空格数量
function ABGetStrBeginSpaceCount(aStr:string): longint;
//取得子串aSubStr在源串aStr中第aCount次出现的位置
function ABGetSubStrPosition(aSubStr,aStr:string;aCount:Integer):Integer;
//删除字段aStr首部的0并返回
function ABDeleteBegin0(aStr:string):string;

//在aStrings中依次查找以数组aFindStrs中字串开头的行，在数组aFindStrs中字串都找到时返回最后一个字串所在的行号(下标从0开始)
//aTrimStringLine=取aStrings行中数据时是否要去掉首尾空格
//aTrimFindStr=取aFindStrs中字串是否要去掉首尾空格
//如('object Panel2: TPanel
//        Left = 0
//        Width = 798
//        Height = 41
//        Align = alBottom
//        TabOrder = 5
//        object Label26: TLabel ',['object Panel2','TabOrder'])= 5
function ABGetIndexByBeginStr(aStrings: TStrings;
                              aFindStrs:array of string;
                              aTrimStringLine:Boolean=True;
                              aTrimFindStr:Boolean=True
                              ): longint;
//序号字串转换为标志串输出
//如('1,2,3,4,5,8,9,10',0,11)='011111001110'
function ABNumberStrToFlag(aNumberStr:string;
                          aBeginNumber:LongInt;aEndNumber:LongInt;
                          aHaveFlag:string='1';aNoHaveFlag:string='0';
                          aSpaceSign: string = ','
                          ):string;

//将分隔符分隔的字串各项加入到字串列表中
//aStrs=由分隔符分隔的字串
//aSpaceSign=分隔符
//aStrings=字串列表
//aNoFillSame=字串列中在已存在时是否还要加入
//aIsDelOld=加入前是否删除字串列表中已有的内容
procedure ABStrsToStrings(aStrs: string;
                          aSpaceSign: string;
                          aStrings: Tstrings;
                          aNoFillSame: boolean = true;
                          aIsDelOld: boolean = true);
//将分隔符分隔的字串各项名称部分加入到字串列表中
procedure ABNameofStrsToStrings(aStrs: string;
                                aSpaceSign: string;
                                aStrings: Tstrings;
                                aNoFillSame: boolean = true;
                                aIsDelOld: boolean = true);
//将分隔符分隔的字串各项值部分加入到字串列表中
procedure ABValueofStrsToStrings( aStrs: string;
                                  aSpaceSign: string;
                                  aStrings: Tstrings;
                                  aNoFillSame: boolean = true;
                                  aIsDelOld: boolean = true);

//将字串列表各项合并成分隔符分隔的字串并返回
//aStrings=字串列表
//aSpaceSign=分隔符
function ABStringsToStrs(aStrings: Tstrings; aSpaceSign: string = ','): string;
//将字串列表各项名称部分合并成分隔符分隔的字串并返回
function ABNameOfStringsToStrs(aStrings: Tstrings; aSpaceSign: string = ','): string;
//将字串列表各项值部分合并成分隔符分隔的字串并返回
function ABValueOfStringsToStrs(aStrings: Tstrings; aSpaceSign: string = ','): string;

//将源字串列表的项加到目标字串列表中
//aSStrings=源字串列表
//aDStrings=目标字串列表
//aNoFillSame=目标字串列中在已存在时是否还要加入
//aIsDelOld=加入前是否删除目标字串列表中已有的内容
procedure ABStringsToStrings( aSStrings: TStrings;
                              aDStrings: TStrings;
                              aNoFillSame: boolean = true;
                              aIsDelOld: boolean = true);

//在源字串列表中删除既存在源字串列表又存在目标字串列表中的项
procedure ABDeleteInStringsItem(aSStrings, aDStrings: Tstrings);
//在字串中删除另一字串的项目
function ABDeleteInStrItem(aStrs: string;aDeleteStrs: string): string;

//在以整个字串形式的列表中新增一项
function ABInsertInStrs(aStrs: string;aIndex:longint; aStr: string): string;
//修改以整个字串形式的列表中某一项
function ABEditInStrs(aStrs: string;aIndex:longint; aNewStr: string): string;
//删除以整个字串形式的列表中某一项
function ABDeleteInStrs(aStrs: string; aIndex:longint): string;

//*******************DELPHI操作************
//取DELPHI的注册表目录串
function ABGetDelphiRegPath(aDelphiVersion: TABDelphiVersion): string;
//取DELPHI的注册表包目录串
function ABGetDelphiLibraryRegPath(aDelphiVersion: TABDelphiVersion): string;

//取DELPHI的安装目录
function ABGetDelphiSetupPath(aDelphiVersion: TABDelphiVersion): string;
//取DELPHI的包查找目录
function ABGetDelphiSeachPath(aDelphiVersion: TABDelphiVersion): string;
//取DELPHI的bpl默认程序目录
function ABGetDelphiBplPath(aDelphiVersion: TABDelphiVersion): string;
//取DELPHI的dcp默认程序目录
function ABGetDelphiDcpPath(aDelphiVersion: TABDelphiVersion): string;

//取DELPHI的主EXE应用程序全名
function ABGetDelphiAppLongFileName(aDelphiVersion: TABDelphiVersion): string;
//取DELPHI的dcc32.exe文件全名
function ABGetDelphiDccLongFileName(aDelphiVersion: TABDelphiVersion): string;
//取DELPHI的dcc32.cfg文件全名
function ABGetDelphiCfgLongFileName(aDelphiVersion: TABDelphiVersion): string;
//取DELPHI的dof扩展名
function ABGetDelphiDofExt(aDelphiVersion:TABDelphiVersion):string;
//通过组文件取得DELPHI的版本 dvD2009~dvDXE3的DELPHI组文件扩展名都是'groupproj',都认为是dvD2009
function ABGetDelphiVersion(aGroupFileName:string):TABDelphiVersion;

//加载工程组文件中的项目文件到Strings中
function ABLoadGroupItemToStrings(aFileName: string;aStrings: TStrings):boolean;
//另存Strings中项目文件为BPG组文件
procedure ABSaveToD7Group(aStrings: TStrings; aFileName: string = '');

//取得dof中的信息
procedure ABGetDelphidof(aFileName: string;aSetType:TABBatchSetDelphiProjectType;var aValues:array of string);
//设置dof中的信息
procedure ABSetDelphidof(aFileName: string;aSetType:TABBatchSetDelphiProjectType;aValues:array of string);

//创建DelphiCFG文件
//如果aCfgFileName=...dcc32.cfg,则按DELPHI编译器所有到的总配制文件格式写,否则按项目自己的cfg格式写
procedure ABCreateDelphiCfgFile(
                      aDelphiVersion: TABDelphiVersion;
                      aCfgFileName: string;
                      aDcpOutPath,
                      aBPLOutPath,
                      aExeOutPath:string;

                      aLibraryPath: Tstrings;
                      aAddDefaultSearchPath: Boolean;
                      aShortFileNameModel: Boolean = false);

//批量编译DELPHI项目
//在调试时因为一些DELPHI的包正在使用,所以常会报一些包找不到的错,脱离或关闭DELPHI运行则不会提示
//aCompilerFileName=编译器文件名
//aFileNameStrings=需要编译的文件全名列表
//aLogStrings=输出的日志
//aFailureFileNameStrings=失败的文件全名列表
//aCfgFileName=cfg文件名,为空时表示使用项目自己的cfg文件
//aDcpOutPath=Dcp输出目录
//aBPLOutPath=BPL输出目录
//aExeOutPath=Exe输出目录
//aLibraryPath=包路径
//aDcpOutPath=Dcp输出目录
//aAddDefaultSearchPath=是否增加DELPHI默认的查找路径
//aShortFileNameModel=是否短文件名模式
//aAfterCompilerNotifyEvent=编译后的事件
//aProgressBar=编译进度条
function ABBuildProjects(aDelphiVersion:TABDelphiVersion;
                         aCompilerFileName: string;
                         aFileNameStrings:TStrings;
                         aLogStrings:TStrings;
                         aFailureFileNameStrings:TStrings;

                         aCfgFileName,
                         aDcpOutPath,
                         aBPLOutPath,
                         aExeOutPath:string;
                         aLibraryPath: Tstrings;
                         aAddDefaultSearchPath: Boolean;
                         aShortFileNameModel: Boolean = false;

                         aAfterCompilerNotifyEvent:TABNotifyEvent=nil;
                         aProgressBar:TProgressBar=nil):boolean;
//单个编译DELPHI项目(生成新的cfg文件编译)
//aFileName=需要编译的文件名
//aOutMsg=编译输出的消息
function ABBuildProject (aDelphiVersion:TABDelphiVersion;
                         aCompilerFileName: string;
                         aFileName: string;
                         var aOutMsg:string;

                         aCfgFileName,
                         aDcpOutPath,
                         aBPLOutPath,
                         aExeOutPath:string;
                         aLibraryPath: Tstrings;
                         aAddDefaultSearchPath: Boolean;
                         aShortFileNameModel: Boolean = false;
                         aAfterCompilerNotifyEvent:TABNotifyEvent=nil
                         ):boolean;overload;
//单个编译DELPHI项目(根据默认cfg文件编译)
//aCompilerFileName=编译器文件名
//aFileName=需要编译的文件名
//aOutMsg=编译输出的消息
function ABBuildProject(aCompilerFileName: string; aFileName: string;var aOutMsg:string): boolean;overload;

//*******************对象属性操作*********************************
//取对象的一般属性值,如果是多级属性则支持如示例的格式 object.fornt.name
function ABGetPropValue(aSelf: TObject; aPropName: string; aEmptyStr: boolean =true): Variant;
//取对象的对象属性值,如果是多级属性则支持如示例的格式 object.fornt.name
function ABGetObjectPropValue(aSelf: TObject; aPropName: string): TObject;
//设置对象的一般属性值,如果是多级属性则支持如示例的格式 object.fornt.name
procedure ABSetPropValue(aSelf: TObject; aPropName: string; aValue: Variant);
//设置对象的对象属性值,如果是多级属性则支持如示例的格式 object.fornt.name
procedure ABSetObjectPropValue(aSelf: TObject; aPropName: string; aValue:  TObject);
//对象的属性是否是指定类型 ,如果是多级属性则支持如示例的格式 object.fornt.name
function ABPropIsType(aSelf: TObject; aPropName: string; aTypeKind: TTypeKind): Boolean;

//取对象的方法,如果是多级属性则支持如示例的格式 object.fornt.name
function ABGetMethodProp(aSelf: TObject; aPropName: string): TMethod;
//设置对象的方法,如果是多级属性则支持如示例的格式 object.fornt.name
procedure ABSetMethodProp(aSelf: TObject; aPropName: string; aValue: TMethod);
//执行没有参数的公布过程
function ABExecNoParamPublishProp(aObject:TObject;aProcName: string):boolean;

//判断是否Publish属性
function ABIsPublishProp(aSelf: TObject; aPropName: string): Boolean;
//将对象的属性全放在AStrings中
procedure ABGetClassProperties(aSelf: TObject; aStrings: TStrings);
//取对象的tStrings属性值,如果是多级属性则支持如示例的格式 object.fornt.name
function ABGetStringsProperty(aSelf: TObject; aPropName: string): TStrings;
//取对象的数据集属性值,如果是多级属性则支持如示例的格式 object.fornt.name
//aCheckAllProperty=检测的数据集属性值为nil时是否再去检测下一个属性直到最后一个
function ABGetDataSetProperty(aSelf: TPersistent; aCheckAllProperty: Boolean =  false): TDataSet;
//检测是否是多行属性名
function ABIsMoreLineProperty(aPropertyName: string): Boolean;

//组件的属性序列化到DFM形式的文本
function ABComponentToDFMProperty(aComponent: TComponent): string;
//DFM形式的组件属性反序列化到组件
function ABDFMPropertyToComponent(aDFMString: string;aComponent:TComponent): TComponent;
//删除DFM文件中控件
function ABDFMControlDelete(aStrings:TStrings;aSpaceStr:string;aBegStr:string;aEndStr:string='end'):boolean;
//拷贝DFM文件中控件
function ABDFMControlCopy(aStrings:TStrings;aSpaceStr:string;aBegStr:string;aEndStr:string='end'):string;

//*******************组件、控件,窗体等对象操作*********************************
//检测对象的所有者是否来源于检测的所有者，支持多层
function ABOwnerCheck(aSender: TControl; aCheckOnwer: TComponent): Boolean;
//检测对象的父容器是否来源于检测的父容器，支持多层
function ABParentCheck(aSender: TControl; aCheckParent: TComponent): Boolean;
//取得对象所有Owner及组件名称
//格式为aComponent.Owner.Owner.Name +' '+aComponent.Owner.Name+' '+aComponent.Name
function ABGetOwnerLongName(aComponent: TComponent;aSpaceFlag:string='.'):string;
//检测对象是否从指定的类型名继承  /
//aObject=检测的对象
//aClassNames=类型名，如果检测的对象是从此类型继承则函数返回True,多个类型名间用逗号分隔
function ABObjectInheritFrom(aObject: TObject; aClassNames: string):boolean;
//得到控件的父对象,直到类型名称为指定的类型或从指定的类型继承时返回,多个类型名间用逗号分隔
function ABGetParent(aControl: TControl; aClassNames: string='Tobject'):TObject;
//得到者件所有者对象,到类型名称为指定的类型或从指定的类型继承时返回,多个类型名间用逗号分隔
function ABGetOwner(aComponent: TComponent; aClassNames: string='Tobject'): TComponent;
//得到控件所在的窗体
function ABGetParentForm(aControl: TControl): TForm;
//获得鼠标针指下的窗体
function ABGetMouseForm: TForm;
//获得鼠标针指下的控件
function ABGetMouseControl: TWinControl;
//获得指定坐标下的窗体
function ABGetCoordinateForm(aX, aY: integer): TForm;
//移动鼠标到控件中心
procedure ABMoveMouseToControl(aWinControl: TControl);
//根据aIsError决定是否要显示aErrorStr字串的提示框并设置焦点在aSender上
function ABWinControlShowError(aSender: TWinControl;aIsError:Boolean;aErrorStr: string): boolean;
//设置组件的Enabled
procedure ABSetControlEnabled(aComponents:array of TComponent;aEnabled:Boolean);overload;
procedure ABSetControlEnabled(aComponent:TComponent;aEnabled:Boolean);overload;
//设置组件的Visible
procedure ABSetControlVisible(aComponents:array of TComponent;aVisible:Boolean);overload;
procedure ABSetControlVisible(aComponent:TComponent;aVisible:Boolean);overload;

//得到备注控件的行号(如Memo或RichEdit)
function ABGetMemoRow(aControl: TControl): longint; //integer;
//得到备注控件当前光标的行和列信息(如Memo或RichEdit)
function ABGetMemoCursor(aEdit: TCustomEdit): TPoint;

//得到父控件下类型名为指定的类型或从指定的类型继承的第一个子控件,多个类型名间用逗号分隔
function ABGetFirstClientControlByClassName(aParent: TWinControl; aClassNames:string='Tobject'): TControl;
//返回类型所在库文件全名(可以是DLL，也可以是BPL或者EXE的名称）,aClass要继承于TPersistent
function ABGetClassFileName(aClass: TClass): String;
//返回类型所在单元名,aClass要继承于TPersistent
function ABGetClassUnitName(aClass: TClass): String;

//返回类型的遂层父亲类型及其所在单元或库文件名列表,aClass要继承于TPersistent
//aStrings=输出的列表，格式为:类型名=单元名=库文件名
//aHaveClassName=输出的列表中是否包含类型名
//aHaveUnitName=输出的列表中是否包含单元名
//aHaveFileName=输出的列表中是否包含库文件名
procedure ABGetParentClassLists(aClass: TClass;
                                aStrings: TStrings;
                                aHaveClassName:Boolean=true;
                                aHaveUnitName:Boolean=true;
                                aHaveFileName:Boolean=true;
                                aSpaceFlag:string='=');

//TProgressBar操作
//设置进度条开始
//aMax=进最大值
//aProcessMessages=设置后是否进行ProcessMessages
//aSetVisibleTrue=是否设置进度条Visible=True
procedure ABSetProgressBarBegin(aProgressBar: TProgressBar;aMax:longint;aProcessMessages:boolean=False;aSetVisibleTrue:boolean=False);
//设置进度条下一个步进
//aStep=步进数量
//aProcessMessages=设置后是否进行ProcessMessages
function ABSetProgressBarNext(aProgressBar: TProgressBar;aStep:LongInt=1;aSleepNum:LongInt=0;aProcessMessages:boolean=True):longint;
//设置进度条结束
//aProcessMessages=设置后是否进行ProcessMessages
//aSetVisibleFalse=是否设置进度条Visible=False
procedure ABSetProgressBarEnd(aProgressBar: TProgressBar;aProcessMessages:boolean=False;aSetVisibleFalse:boolean=False);

//ListBox列表操作(包含TListBox、TCheckListBox)
//从源ListBox的aIndex项移动到目标ListBox
//aSListBox=源ListBox
//aIndex=源ListBox要移动项的索引
//aEndSign=源ListBox要移动项的结束标志，移动时只将移动项此标志前的字符移到目标ListBox
//aDListBox=目标ListBox
//aSListBoxNextSelect=源ListBox是否在移动后选择下一个
procedure ABListBoxItemMoveToListBox(aSListBox:TCustomListBox;
                                    aIndex: LongInt;
                                    aEndSign: string;
                                    aDListBox: TCustomListBox;
                                    aSListBoxNextSelect:boolean=true);
//ListBox索引项上移
//aListBox=要上移的ListBox
//aIndex=ListBox要上移项的索引
//aStep=ListBox上移的数量
procedure ABListBoxUp(aListBox: TCustomListBox; aIndex: LongInt; aStep: LongInt = 1);
//ListBox索引项下移
//aListBox=要下移的ListBox
//aIndex=ListBox要下移项的索引
//aStep=ListBox下移的数量
procedure ABListBoxDown(aListBox: TCustomListBox; aIndex: LongInt; aStep: LongInt =   1);

//将ListBox中所有选择项复制到字串列表中 (TListBox类型时Select操作、TCheckListBox类型时Checked操作)
//aNoFillSame=碰已存在于列表中的是否不重复填充
//aIsDelOld=在填充之前是否删除列表中已有的内容
procedure ABListBoxSelectsCopyStrings(aListBox: TCustomListBox;
                               aStrings:TStrings;
                               aNoFillSame: boolean = true;
                               aIsDelOld: boolean = true);
//将ListBox中某一位置到最后的所有项复制到字串列表中
//aBegIndex=开始复制的位置
//aCount=复制的数量
//aCount=碰已存在于列表中的是否不重复填充
//aIsDelOld=在填充之前是否删除列表中已有的内容
procedure ABListBoxCopyStrings(aListBox: TCustomListBox;
                               aBegIndex,
                               aCount:longint;
                               aStrings:TStrings;
                               aNoFillSame: boolean = true;
                               aIsDelOld: boolean = true);
//设置ListBox的宽度
//aWidth=需设置的宽度，如果=0则根据ListBox项中最长内容也设置宽度
procedure ABSetListBoxWidth(aListBox: TCustomListBox; aWidth: LongInt = 0);

//设置ListBox的选择类型 (TListBox类型时进行Select操作、TCheckListBox类型时进行Checked操作)
//aType=选择类型 aType=0全选择,  aType=1全不选择, aType=2反向选择,
procedure ABSetListBoxSelect(aListBox: TCustomListBox; aType: LongInt);
//保存ListBox的选择状态到本地配制文件中(TListBox类型时进行Select操作、TCheckListBox类型时进行Checked操作)
procedure ABSaveListBoxSelect(aListBox: TCustomListBox);
//从本地配制文件中读取ListBox的选择状态(TListBox类型时进行Select操作、TCheckListBox类型时进行Checked操作)
procedure ABLoadListBoxSelect(aListBox: TCustomListBox);

//在TreeView中查找是指定字串的结点
//aTreeNodes=查找的TreeView控件的结点集合
//aFindText=查找结点的文本
//aFindLevels=查找结点的层,=[]表示查找所有层
function ABFindTree(aTreeNodes: TTreeNodes;
                     aFindText:string;
                     aFindLevels: array of Variant
                     ):TTreeNode;overload;
//在树结点的所有子结点中查找是指定字串的结点
//aTreeNode=查找的父结点
//aFindText=查找结点的文本
//aFindLevels=查找结点的层,=[]表示查找所有层
function ABFindTree(aTreeNode: TTreeNode;
                     aFindText:string;
                     aFindLevels: array of Variant
                     ):TTreeNode;overload;
//在TreeView中查找是指定Data的结点
//aTreeNodes=查找的TreeView控件的结点集合
//aFindData=查找结点的Data
//aFindLevels=查找结点的层,=[]表示查找所有层
function ABFindTree(aTreeNodes: TTreeNodes;
                     aFindData:Pointer;
                     aFindLevels: array of Variant
                     ):TTreeNode;overload;
//在树结点的所有子结点中查找是指定Data的结点
//aTreeNode=查找的父结点
//aFindText=查找结点的Data
//aFindLevels=查找结点的层,=[]表示查找所有层
function ABFindTree(aTreeNode: TTreeNode;
                     aFindData:Pointer;
                     aFindLevels: array of Variant
                     ):TTreeNode;overload;


//取得所有菜单项到列表TList中
procedure ABGetMenuItems(aMenu: TMenuItem;aStrings: TStrings);
//根据名称从列表TList中取得菜单项
Function ABGetMenuItem(aStrings: TStrings;aName:String):TMenuItem;
//清除菜单的所有菜单项
procedure ABClareMenuItems(aMenu: TMenu);
//对组中的菜单项在点击时做必选处理
procedure ABSelectMenuItemOfGroup(aMenuItem: TMenuItem);
//创建菜单
function ABCreateMenuItem(aOwner:TComponent;
                          aItems: TMenuItem;

                          aName:string;
                          aCaption:string='';

                          aOnClick:TNotifyEvent=nil;
                          aAutoCheck:boolean=false;
                          aChecked:boolean=false;
                          aGroupIndex:LongInt=0;
                          aRadioItem:boolean=false
                          ):TMenuItem;


//根据控件的位置对你控件中的子控件进行Order重设置
//aParent=设置的容器控件
//aSetClient=容器父控件中的子控件如果是容器控件，是否设置子容器控件中的子控件
procedure ABAutoTabOrder(const aParent  : TWinControl;const aSetClient: Boolean = True);

//检测值两个变体变量是否相同
function ABComparisonValueSame(aValue1,
                               aValue2:Variant): Boolean;
//*******************操作系统操作*********************************

//在指定位置创建指定文件的快捷方式
//快捷方式实质也是一种文件，删除快捷方式可用删除文件的方法来做
//aType= 快捷方式位置类型(ShlObj 中定义下列常量)
//  CSIDL_COMMON_programs     开始菜单
//  CSIDL_DESKTOP             所有用户桌面
//  CSIDL_DESKTOPDIRECTORY    当前用户桌面目录
//  CSIDL_FONTS               字体目录
//  CSIDL_NETHOOD             网络邻居
//  CSIDL_PERSONAL            我的文档目录
//  CSIDL_PROGRAMS            开始菜单程序目录
//  CSIDL_RECENT              存放用户最近访问文件快捷方式的目录
//  CSIDL_SENDTO              "发送到"目录
//  CSIDL_STARTMENU           开始菜单目录
//  CSIDL_STARTUP             开始菜单启动项目录
//  CSIDL_FAVORITES           收藏夹目录
// aSFileName=快捷方式的源文件名
// aShortCutName=快捷方式名称
// aShortCutSubPath=快捷方式位置的子路径，如开始菜单程序目录下的某某公司某某系统
procedure ABCreateShortCut(aType:LongInt;aSFileName:string;aShortCutName:string;aShortCutSubPath: string='');
//得到快捷方式位置类型对应的路径
function ABGetShortCutTypePath(aType:LongInt): string;
//得到快捷方式的全文件名
function ABGetShortCutLLinkFileName(const aShortCutFileName: string): string;

//取Windows系统目录
function ABGetWindowsPath: string;
//取Windows临时文件目录
function ABGetWindowsTempPath: string;
//返回Windows临时文件目录中指定文件扩展名的临时文件名
function ABGetWindowsTempFileName(aExt: string): string;overload;
//返回指定文件目录中指定文件扩展名的临时文件名
function ABGetWindowsTempFileName(aPath,aExt: string): string;overload;

//返回最近一次WSA异步调用的错误信息字符串
function ABGetLastWsaErrorStr: string;
//返回最近一次API调用的错误信息字符串
function ABGetLastErrorString: string;

//得到系统环境变量列表
function ABGetEnvironmentList:TStrings;
//替换字串中的系统变量为实际系统变量的值
//aStr=包含系统变量的字串
//aBegFlag=字串中系统变量的开始标志
//aEndFlag=字串中系统变量的结束标志
function ABReplaceEnvironment(aStr:string;aBegFlag:string='$(';aEndFlag:string=')'):String;

//当前环境是否是64位Windows系统
function ABIsWin64: Boolean;
//当前环境是否是虚拟机
function ABIsVMwarePresent: LongBool;

//获取Windows当前登录名的用户名
function ABGetWindowsUserName: string;
//获取Windows注册的单位及用户名
function ABGetRegistryOrgAndUserName(var aOrg,aUserName: string): boolean;

//获取操作系统版本号
function ABGetSysVersion: string;
//获取操作系统的语言类型
function ABGetSysLanguage: TABSysLanguageType;
//获取操作系统的语言类型字串
function ABGetSysLanguageStr(aLanguage: TABSysLanguageType): string;

//获取盘符的序列号
//该号既可指软磁盘要得, 如:A:盘和B:盘的, 又可以指硬盘的逻辑盘, 如: C:, D:...的,是高级格式化时随机产生的, 是可以修改的
//aDiskChar=不带冒号的盘符
function ABGetDiskVolumeSerialNumber(aDiskChar: Char): string;
//获取硬盘序列号,该号是出厂时生产厂家为区别产品而设置的, 是唯一的, 是只读的
//aResLeng=将序列号中多少个字符进行返回
function ABGetDiskSerialNumber(aResLeng: LongInt = 256): string;
//检查盘符准备是否就绪
//aDiskChar=不带冒号的盘符
function ABDiskVolumeIsOK(aDiskChar: Char): Boolean;
//获取当前机器CPU的速率（MHz）
function ABCPUSpeed: Double;
//获取CPU的ID
//aIndex=第几个CPU，ABGetCpuID(1)表示第一个CPU的ID
function ABGetCpuID(aIndex: LongInt): string;
//获取计算机的物理内存(KB)
function ABGetTotalMemory: Dword;
//获取计算机可用的物理内存(KB)
function ABGetCanUseMemory: Dword;

//查找指定标题且指类名的窗体并替换标题
//aFindCaption=查找的窗体标题
//aReplaceCaption=替换的标题
//aFindClassName=查找的窗体类名如不为空则查找窗体标题时也要满足类型名等于aFindClassName
//aPartFindReplace=查找替换时是否部分查找且部分替换
//aReplaceClientControl=是否替换容器类窗体中的控件标题，默认不替换容器内部的窗体标题
//aReplaceAll=是否替换所有符合条件的窗体标题，默认时替换一个后就退出
procedure ABReplaceControlCaption(var aFindFindHWnd: HWnd;
                                   aFindCaption:string;
                                   aFindClassName:string='';

                                   aReplace:boolean=false;
                                   aReplaceCaption:string='';

                                   aPartFindReplace:boolean=false;
                                   aReplaceClientControl:Boolean=false;
                                   aReplaceAll:Boolean=false);

//返回指定句柄窗口的类名
function ABGetControlClassName(aHandle: HWND): string;
//返回指定句柄窗口的标题
function ABGetControlCaption(aHandle: HWND): string;
//设置指定句柄窗口最上方显示
procedure ABSetControlOnTop(aHandle: HWND; aOnTop: Boolean);
//设置指定句柄程序是否出现在任务栏
//aHandle=设置出现在任务栏的程序句柄，如Application.Handle
procedure ABSetControlTaskBarVisible(aHandle: HWND; aVisible: Boolean);
//设置指定句柄窗体动画效果
// aHandle=想要显示动画窗体的句柄
// dwTime= 动画时间数,单位毫秒
// dwFlags=显示方式,取值如下定义
//  1.AW_HOR_POSITIVE = $00000001; // 从左向右开屏 ,当使用AW_CENTER标志时，该标志将被忽略。
//  2.AW_HOR_NEGATIVE = $00000002; // 从右向左开屏 ,当使用AW_CENTER标志时，该标志将被忽略。
//  3.AW_VER_POSITIVE = $00000004; // 从上向下开屏 ,当使用AW_CENTER标志时，该标志将被忽略。
//  4.AW_VER_NEGATIVE = $00000008; // 从下向上开屏 ,当使用AW_CENTER标志时，该标志将被忽略。
//  5,AW_CENTER = $00000010; // 从中心向四周扩展,若使用了AW_HIDE标志，则使窗口向内重叠；若未使用AW_HIDE标志，则使窗口向外扩展。
//  6.AW_HIDE = $00010000; // 关闭时候与前面的定义组合使用,如AW_HIDE or AW_CENTER
//  7.AW_ACTIVATE = $00020000; // 与1-5组合,开屏使用 ,在使用了AW_HIDE标志后不要使用这个标志。
//  8.AW_SLIDE = $00040000; // 与1-5 + 6/7 组合,产生滑行效果,缺省则为滚动动画  ,当使用AW_CENTER标志时，该标志将被忽略。
//  9.AW_BLEND = $00080000; // Win2000下使用,淡入淡出效果,只有当hWnd为顶层窗口的时候才可以使用此标志。
procedure ABAnimateWindow(aHandle: HWND;aTime: DWORD=500; aFlags: DWORD=AW_BLEND);

//设置任务栏是否可见
procedure ABSetTaskBarVisible(aVisible: Boolean);
//设置桌面是否可见
procedure ABSetDesktopVisible(aVisible: Boolean);

//打开输入法
procedure ABOpenIME(aImeName: string);
//关闭输入法
procedure ABCloseIME;

//关闭计算机
procedure ABShutDownComputer;
//重启计算机
procedure ABRebootComputer;
//注销当前用户
procedure ABLOGOFF;

//重置、恢复对象函数或纯函数地址
//使用方法
//var
//  a1,a2:TAB4ByteDynArray;
//  //重置对象函数地址
//  a1:=ABHook_Func(GetMethodProp(Button2, 'OnClick').Code,GetMethodProp(Button3, 'OnClick').Code);
//  或a1:=ABHook_Func(@TForm47.Button2Click,@TForm47.Button3Click);
//  //重置纯函数地址
//  a2:=ABHook_Func(@msg1,@msg2);
//  .....
//  //恢复对象函数原始地址
//  ABUnHook_Func(GetMethodProp(Button2, 'OnClick').Code,a1);
//  或ABUnHook_Func(@TForm47.Button2Click,a1);
//  //恢复纯函数原始地址
//  ABUnHook_Func(@msg1,a2);

//重置函数的地址
//aOldAddress=旧的对象函数或纯函数地址
//aNewAddress=新的对象函数或纯函数地址
//返回值为原始地址，返回值在由ABUnHook_Func恢复时使用
function ABHook_Func(aOldAddress, aNewAddress: Pointer): TAB4ByteDynArray;
//恢复由ABHook_Func重置过的函数原始地址
//aCurAddress=函数当前的地址
//aBackAddress=由ABHook_Func返回的原始地址
procedure ABUnHook_Func(aCurAddress: Pointer; aBackAddress: TAB4ByteDynArray);

//系统热键是指全局的系统组合按键
//使用前先注册热键
//FShowHideHotKey_ID:integer;
//FShowHideHotKey_ID:=ABRegisterHotKey(handle,'MyF9HotKey',HotKey1.HotKey);
//注册热键后在窗体的WMHotKey中就可以检测是否按下了所注册的热键
//procedure WMHotKey(var Msg : TWMHotKey); message WM_HOTKEY;
//procedure TMainForm.WMHotKey(var Msg: TWMHotKey);
//begin
//  if (Msg.HotKey=FShowHideHotKey_ID) then
//  begin
//  end;
//end;
//使用后取消热键
//ABUnRegisterHotKey(handle,'MyF9HotKey');
//注册热键
function ABRegisterHotKey(ahandle:Hwnd;aAtomName:string;aShortCut:TShortCut):longint;
//取消热键
function ABUnRegisterHotKey(ahandle:Hwnd;aAtomName:string):longint;

//运行一个文件并等待其结束
//aFileName=文件名
//aShowType=显示类型，如 SW_Hide=运行时隐藏,SW_SHOWNORMAL=运行时显示
//aParams=运行参数
function ABWaitAppEnd(aFileName: string;
                      aShowType: Integer=SW_SHOWNORMAL;
                      aParams: string=''): Boolean;

//获取Windows启动模式
function ABWindowsBootMode: string;

//返回当前键盘特殊键的状态
//aCheckKeys=检测的键，如[VK_NUMLOCK,VK_CAPITAL,VK_INSERT]表示数字键、大写键、插入键
function ABGetKeyBoardStatus(aCheckKeys:array of longint):TBooleanDynArray;
//取当前机器名称,IP,MAC
procedure ABGetHostInfo(var aHostName,aHostIP,aMacAddress:string);

//通过文件名查找指定的进程，然后返回进程ID
function ABFindAProcess(aFilename:string):DWORD;
//通过文件名查找指定的进程占用的内存(KB)
function ABGetProcessMemorySize(aFilename: string): LongInt;

//动态设置分辨率,如果宽与高是系统不支持的则设置无效
function ABDynamicResolution(aWidth, aHeight: WORD): Boolean;

//使鼠标变忙和恢复正常
procedure ABDoBusy(aBusy: Boolean = true);

//取服务器机器时间同步到本机
//aServerHostName=服务器的机器名
function ABSynLocalDatetime(aServerHostName: string): boolean;
//取得开机后系统流逝的毫秒数,毫秒级的精度,连续开机过了49.71天后又归0
function ABTicker: DWORD; register;

//获得计算机的COM口列表
procedure ABGetComs(aCOMS: TStrings);
//非阻塞式Sleep,时间未到时会不停调用Application.ProcessMessages
procedure ABSleep(aMilliSeconds: Cardinal);

//操作成功的提示音
procedure ABOKBeep;

//根据Access文件名创建ODBC系统DNS连接
procedure ABCreateOdbc(const aAccessFilePath, aODBCName: string);

//判断变体是否为空（VarIsEmpty or VarIsNull or VarIsClear ）
//aVariant支持数组的判断，当数组元素全为空时函数才会返回True
function ABVarIsNull(aVariant: Variant): Boolean;
//变体为空时返回传入值，否则返回自己
function ABIsNull(aVar: Variant;aValue: Variant): Variant;
//判断指针是否为空
function ABPointerIsNull(aPointer: Pointer): Boolean;
//释放并置空一个对象指针
procedure ABFreeAndNil(var aObj);

//播放声音文件
function ABPlay(const aFileName: string): Boolean;

//if语句的简写
function ABIIF(aTest: Boolean; aTrueValue, aFalseValue: Variant): Variant;
//取IP转换的数字唯一值
//127.0.0.1=127*1+0*10+0*100+1*1000
function ABGetIPFlag(aHostIP: string;aAddInt:LongInt=0): longint;
//取得系统默认的打印机名称
Function ABGetDefaultPrinterName : String;overload;
//根据传入的打印机或功能名取得默认的打印机
Function ABGetDefaultPrinterName(aPrintNameOrFuncName:string):string;overload;

//判断字符是否是汉字
function ABIsChinese(const aChar: Char): Boolean;
//判断字串中是否有汉字
function ABHaveChinese(const aStr: String): Boolean;
//将字串转换为可用的名称，如:和/是不能出现在控件名称中的，此函数将这些不能出现在名称中的字符替换为下划线
function ABStrToName(const aStr: string): string;
//统计文本文件中英文单词出现的次数
function ABReportFileWordCount(aFileName:String;aOutStrings: TStrings;aAppendWordList:Boolean=true;aAppendWordCount:Boolean=true):LongInt;

//*******************流操作*********************************
//流到变体
function ABStreamToVariant(aStream: TStream): Variant;
//变体到流
procedure ABVariantToStream(aVariant: Variant; aStream: TStream);
//压缩文件   源文件,压缩方式clFastest, clDefault, clMax
procedure ABCompressFile(aSfilename, aDfilename: string; const CompressionLevel: TCompressionLevel = zlib.clDefault);
//解压文件  源文件,解压后的文件
procedure ABUnCompressFile(aSfilename, aDfilename: string);

//压缩流   源流,压缩方式clFastest, clDefault, clMax
procedure ABCompressStream(aStream: TMemoryStream; CompressionLevel: TCompressionLevel = zlib.clDefault);
//解压流   源流,解压后的流
procedure ABUnCompressStream(aStream: TMemoryStream);


implementation

var
  FTrueStrs:array[0..10] of string=('True','T','Yes','1','man','男的','男性','男','雄的','雄性','雄');
  FFalseStrs:array[0..10] of string=('False','F','No','0','woman','女的','女性','女','雌的','雌性','雌');
  FEnvironmentStrings: TStrings;        //系统环境变量列表

function ABReportFileWordCount(aFileName:String;aOutStrings: TStrings;aAppendWordList:Boolean;aAppendWordCount:Boolean):LongInt;
var
  tempCurLine, tempCurWord: string;
  tempFile: TStringList;
  tempWordList: TStringList;
  tempOutStrings: TStringList;
  tempCurWordIndex,i, j: Integer;
begin
  Result:=0;

  if not ABCheckFileExists(aFileName) then
    exit;

  tempWordList := TStringList.Create;
  tempFile := TStringList.Create;
  tempOutStrings := TStringList.Create;
  if Assigned(aOutStrings) then
  begin
    aOutStrings.BeginUpdate;
    aOutStrings.Clear;
  end;
  //Sorted:=true 表示单词列表是有序的,关键地方，不会排序则需要32秒，排序800毫秒
  TStringList(tempWordList).Sorted:=true;
  try
    //经试验，对于三十M的文本文件，TStringList也能很快的加载,与文件行读差别不大，所以统一使用TStringList加载
    tempFile.LoadFromFile(aFileName);
    //一行一行的读取
    for i := 0 to tempFile.Count - 1 do
    begin
      tempCurLine := LowerCase(tempFile.Strings[i]) +' ' ;
      tempCurWord := '';
      for j := 1 to Length(tempCurLine) do
      begin
        if (tempCurLine[j] >= 'a') and
           (tempCurLine[j] <= 'z') then
          tempCurWord := tempCurWord + tempCurLine[j]
        else
        begin
          //一个单词一个单词的统计
          if tempCurWord <> '' then
          begin
            //原以为IndexOf为是瓶颈，但看了TStringList代码，发现在排序时已使用了二分法查找
            tempCurWordIndex := tempWordList.IndexOf(tempCurWord);
            if tempCurWordIndex >= 0 then
            begin
              tempWordList.Objects[tempCurWordIndex] :=Pointer(LongInt(tempWordList.Objects[tempCurWordIndex])+1);
            end
            else
            begin
              //在排序时，AddObject会先二分查找插入位置
              //插入时仅Move一次节点后面的内存块，而不是一个节点一个节点的移，所以不会影响效率
              tempWordList.AddObject(tempCurWord,TObject(Pointer(1)));
            end;
            tempCurWord := '';
          end;
        end;
      end;
    end;

    if Assigned(aOutStrings) then
    begin
      if aAppendWordList then
      begin
        for i := 0 to tempWordList.Count - 1 do
        begin
          tempOutStrings.Add(tempWordList[i]+'='+inttostr(LongInt(tempWordList.Objects[i])));
        end;
      end;
      if aAppendWordCount then
        tempOutStrings.Add('Word Count='+inttostr(tempWordList.Count));

      aOutStrings.AddStrings(tempOutStrings);
    end;

    result:=tempWordList.Count;
  finally
    if Assigned(aOutStrings) then
      aOutStrings.EndUpdate;
    tempOutStrings.Free;
    tempWordList.Free;
    tempFile.Free;
  end;
end;

function ABStrToName(const aStr: string): string;
var
  tempWideChar: Char;
  i, j: LongInt;
begin
  Result := aStr;
  if ABHaveChinese(aStr) then
  begin
    Result := EmptyStr;
    i := 1;
    j := 1;
    while i <= Length(aStr) do
    begin
      tempWideChar := aStr[j];
      if ABHaveChinese(tempWideChar) then
      begin
        Result := Result + IntToStr(Ord(tempWideChar));
        i := i + 1;
      end
      else
      begin
        if tempWideChar <> '' then
          Result := Result + tempWideChar;
        i := i + 1;
      end;
      j := j + 1;
    end;
  end;

  result := StringReplace(result, ' ', '' ,[rfReplaceAll, rfIgnoreCase]);
  result := StringReplace(result, '=', '_',[rfReplaceAll, rfIgnoreCase]);
  result := StringReplace(result, '.', '_',[rfReplaceAll, rfIgnoreCase]);
  result := StringReplace(result, ',', '_',[rfReplaceAll, rfIgnoreCase]);
  result := StringReplace(result, '/', '_',[rfReplaceAll, rfIgnoreCase]);
  result := StringReplace(result, '\', '_',[rfReplaceAll, rfIgnoreCase]);
  result := StringReplace(result, ':', '_',[rfReplaceAll, rfIgnoreCase]);
  result := StringReplace(result, '-', '_',[rfReplaceAll, rfIgnoreCase]);
  result := StringReplace(result, '{', '_',[rfReplaceAll, rfIgnoreCase]);
  result := StringReplace(result, '}', '_',[rfReplaceAll, rfIgnoreCase]);
end;

//判断WideChar字符是否是汉字
function ABIsChinese(const aChar: Char): Boolean;
begin
  //双字节时汉字的 UniCode 编码范围是: $4E00..$9FA5
  //单字节时汉字的 ASCII 码>=128
  Result:=true;
  if SizeOf(aChar)=2 then
  begin
    if (ord(aChar)<19968) or (ord(aChar)>40869) then
      Result:=false;
  end
  else if SizeOf(aChar)=1 then
  begin
    if ord(aChar) < 128 then
      Result:=false;
  end;
end;

//判断是否有汉字
function ABHaveChinese(const aStr: String): Boolean;
var
  i:LongInt;
begin
  Result:=false;
  for I := 1 to Length(aStr) do
  begin
    if ABIsChinese(aStr[i]) then
    begin
      Result:=true;
      break;
    end;
  end;
end;

procedure ABSetProgressBarBegin(aProgressBar: TProgressBar;aMax:longint;aProcessMessages:boolean;aSetVisibleTrue:boolean);
begin
  if Assigned(aProgressBar) then
  begin
    aProgressBar.Min:=0;
    aProgressBar.Max:=aMax;
    aProgressBar.Position:=0;

    if aSetVisibleTrue then
      aProgressBar.Visible :=true;
    if aProcessMessages then
      application.ProcessMessages;
  end;
end;

function ABSetProgressBarNext(aProgressBar: TProgressBar;aStep:LongInt;aSleepNum:LongInt;aProcessMessages:boolean):longint;
begin
  Result:=0;
  if Assigned(aProgressBar) then
  begin
    if aProgressBar.Position<aProgressBar.Max then
      aProgressBar.Position := aProgressBar.Position+aStep;

    if aSleepNum>0 then
      Sleep(aSleepNum);

    Result:=aProgressBar.Position;
    if aProcessMessages then
      application.ProcessMessages;
  end;
end;

procedure ABSetProgressBarEnd(aProgressBar: TProgressBar;aProcessMessages:boolean;aSetVisibleFalse:boolean);
begin
  if Assigned(aProgressBar) then
  begin
    aProgressBar.Position:=0;
    if aSetVisibleFalse then
      aProgressBar.Visible :=False;
    if aProcessMessages then
      application.ProcessMessages;
  end;
end;

procedure ABVariantToStream(aVariant: Variant; aStream: TStream);
var
  p: Pointer;
begin
  p := VarArrayLock(aVariant);
  try
    aStream.Position:=0;
    aStream.Write(p^, VarArrayHighBound(aVariant, 1) + 1);
  finally
    VarArrayUnlock(aVariant);
  end;
end;

function ABStreamToVariant(aStream: TStream): Variant;
var
  p: Pointer;
begin
  Result := VarArrayCreate([0, aStream.Size - 1], varByte);
  p := VarArrayLock(Result);
  try
    aStream.Position := 0;
    aStream.Read(p^, aStream.Size);
  finally
    VarArrayUnlock(Result);
  end;
end;

procedure ABCompressStream(aStream: TMemoryStream; CompressionLevel: TCompressionLevel);
var
  SourceStream: TCompressionStream;
  DestStream: TMemoryStream;
  Count: int64; //注意,此处修改了原来是int
begin
  aStream.Position := 0;
  //获得流的原始尺寸
  Count := aStream.Size;
  DestStream := TMemoryStream.Create;
  SourceStream := TCompressionStream.Create(CompressionLevel, DestStream);
  try
     //SourceStream中保存着原始的流
    aStream.SaveToStream(SourceStream);
     //将原始流进行压缩, DestStream中保存着压缩后的流
    SourceStream.Free;
    aStream.Clear;
     //写入原始图像的尺寸
    aStream.WriteBuffer(Count, SizeOf(Count));
     //写入经过压缩的流
    aStream.CopyFrom(DestStream, 0);
    aStream.Position := 0;
  finally
    DestStream.Free;
  end;
end;

procedure ABUnCompressStream(aStream: TMemoryStream);
var
  SourceStream: TDecompressionStream;
  DestStream: TMemoryStream;
  Buffer: PChar;
  Count: int64;
begin
  aStream.Position := 0;
  //从被压缩的图像流中读出原始的尺寸
  aStream.ReadBuffer(Count, SizeOf(Count));
  //根据尺寸大小为将要读入的原始流分配内存块
  GetMem(Buffer, Count);
  DestStream := TMemoryStream.Create;
  SourceStream := TDecompressionStream.Create(aStream);
  try
    //将被压缩的流解压缩,然后存入 Buffer内存块中
    SourceStream.ReadBuffer(Buffer^, Count);
    //将原始流保存至 DestStream流中
    DestStream.WriteBuffer(Buffer^, Count);
    DestStream.Position := 0; //复位流指针
    aStream.LoadFromStream(DestStream);
    aStream.Position := 0;
  finally
    FreeMem(Buffer);
    DestStream.Free;
  end;
end;

procedure ABCompressFile(aSfilename, aDfilename: string; const CompressionLevel: TCompressionLevel);
var
  tempStream: TMemoryStream;
begin
  tempStream := TMemoryStream.Create;
  try
    tempStream.LoadFromFile(aSfilename);
    ABCompressStream(tempStream, CompressionLevel);
    tempStream.SaveToFile(aDfilename);
  finally
    tempStream.Free;
  end;
end;

procedure ABUnCompressFile(aSfilename, aDfilename: string);
var
  tempStream: TMemoryStream;
begin
  tempStream := TMemoryStream.Create;
  try
    tempStream.LoadFromFile(aSfilename);
    ABUnCompressStream(tempStream);
    tempStream.SaveToFile(aDfilename);
  finally
    tempStream.Free;
  end;
end;

procedure ABByteArrayAppendFile(aByteDynArray: TByteDynArray;aCount:LongInt;aFileName : string );
var
  tempFile: FIle of Byte;
begin
  AssignFile( tempFile, aFileName );
  if ABCheckFileExists(aFileName) then
  begin
    Reset(tempFile);
    Seek(tempFile,FileSize(tempFile));
  end
  else
  begin
    Rewrite(tempFile);
  end;

  try
    BlockWrite(tempFile, (@aByteDynArray[0])^, aCount );
  finally
    CloseFile( tempFile);
  end;
end;

procedure ABByteArrayWriteFile(aByteDynArray: TByteDynArray;aCount:LongInt;aFileName : string );
var
  tempFile: FIle of Byte;
begin
  //将一个外部文件的文件名与一个File 类型的变量关联，对File 类型的变量进行初始化
  AssignFile( tempFile, aFileName );
  //覆盖方式创建文件(无类型文件时第二个参数指定一次读出写入的字节大小,后面BlockWrite中的aCount是指写入多少次)
  Rewrite(tempFile);
  try
    //向文件中写入aCount数量的字节
    BlockWrite(tempFile,(@aByteDynArray[0])^, aCount);
  finally
    //关闭文件
    CloseFile( tempFile );
  end;
end;

function ABFileToByteArray(const aFileName:string ):TByteDynArray;
const
  BLOCK_SIZE=1024;
var
  BytesRead,BytesToWrite,Count:integer;
  F:File of Byte;
  ptemp:Pointer;
begin
  AssignFile( F, aFileName );
  Reset(F);
  try
    Count := FileSize( F );
    SetLength(Result, Count );
    ptemp := @Result[0];
    BytesRead := BLOCK_SIZE;
    while (BytesRead = BLOCK_SIZE ) do
    begin
      BytesToWrite := Min(Count, BLOCK_SIZE);
      BlockRead(F, ptemp^, BytesToWrite , BytesRead );
      ptemp := Pointer(LongInt(ptemp) + BLOCK_SIZE);
      Count := Count-BytesRead;
    end;
  finally
    CloseFile( F );
  end;
end;

function ABIsEmptyStr(aStr: string): Boolean;
begin
  Result := Trim(aStr) = EmptyStr;
end;

function ABVarIsNull(aVariant: Variant): Boolean;
var
  i:LongInt;
begin
  Result := true;
  if (VarIsArray(aVariant)) then
  begin
    for i := VarArrayLowBound(aVariant,1) to VarArrayHighBound(aVariant,1) do
    begin
      Result := ABVarIsNull(aVariant[i]);
      if not Result then
      begin
        Result := false;
        Break;
      end;
    end;
  end
  else
  begin
    Result := (VarIsEmpty(aVariant)) or
              (VarIsNull(aVariant)) or
              (VarIsClear(aVariant));
  end;
end;

function ABPointerIsNull(aPointer: Pointer): Boolean;
begin
  Result := (not Assigned(aPointer)) or (aPointer = nil);
end;

procedure ABFreeAndNil(var aObj);
begin
  if Assigned(Pointer(aObj)) then
    FreeAndNil(aObj);
end;

function ABDateTimeToStr(aDateTime: TDateTime; aLong: LongInt; aDateSpaceSign:string; aTimeSpaceSign: string): string;
begin
  Result := Copy(FormatDateTime(
    'YYYY' + aDateSpaceSign + 'MM' + aDateSpaceSign + 'DD'+ABIIF(aDateSpaceSign=emptystr,'',' ')+'HH' + aTimeSpaceSign +
    'NN' + aTimeSpaceSign + 'SS'+aTimeSpaceSign+'ZZZ',
    aDateTime), 1, aLong);
end;

function ABDateToStr(aDateTime: TDateTime; aDateSpaceSign: string): string;
begin
  Result := ABDateTimeToStr(aDateTime, 10, aDateSpaceSign);
end;

function ABStrToDateTimeDef(aStr: string;aDefDateTime:TDateTime;
                             aDateSpaceSign: string ; aTimeSpaceSign: string ): TDateTime;
begin
  try
    Result:= ABStrToDateTime(aStr,aDateSpaceSign,aTimeSpaceSign);
  except
    Result:= aDefDateTime;
  end;
end;

function ABStrToDateTime( aStr: string;
                          aDateSpaceSign: string; aTimeSpaceSign: string ): TDateTime;
var
  AYear, AMonth, ADay, AHour, AMinute, ASecond,AMilliSecond: Word;
  i:longint;
begin
  AYear:=0;
  AMonth:=0;
  ADay:=0;
  AHour:=0;
  AMinute:=0;
  ASecond:=0;
  AMilliSecond:=0;

  aStr:=Trim(aStr);
  if aStr<>emptystr then
  begin
    i:=ABPos(aDateSpaceSign,aStr);
    if i>0 then
    begin
      AYear:=StrToInt(Copy(aStr,1,i-1));
      aStr:=Copy(aStr,i+length(aDateSpaceSign),maxint);

      i:=ABPos(aDateSpaceSign,aStr);
      if i>0 then
      begin
        AMonth:=StrToInt(Copy(aStr,1,i-1));
        aStr:=Copy(aStr,i+length(aDateSpaceSign),maxint);

        i:=ABPos(' ',aStr);
        if i>0 then
        begin
          ADay:=StrToInt(Copy(aStr,1,i-1));
          aStr:=Copy(aStr,i+length(' '),maxint);
        end
        else
        begin
          ADay:=StrToInt(aStr);
          aStr:=EmptyStr;
        end;
      end;
    end
    else
    begin
      AYear :=StrToIntDef(Copy(aStr,1,4),AYear);
      AMonth:=StrToIntDef(Copy(aStr,5,2),AMonth);
      ADay  :=StrToIntDef(Copy(aStr,7,2),ADay);
    end;

    if aStr<>emptystr then
    begin
      i:=ABPos(aTimeSpaceSign,aStr);
      if i>0 then
      begin
        AHour:=StrToInt(Copy(aStr,1,i-1));
        aStr:=Copy(aStr,i+length(aTimeSpaceSign),maxint);

        i:=ABPos(aTimeSpaceSign,aStr);
        if i>0 then
        begin
          AMinute:=StrToInt(Copy(aStr,1,i-1));
          aStr:=Copy(aStr,i+length(aTimeSpaceSign),maxint);

          i:=ABPos(aTimeSpaceSign,aStr);
          if i<=0 then
          begin
            i:=ABPos(' ',aStr);
            if i<=0 then
            begin
              i:=ABPos('.',aStr);
            end;
          end;

          if i>0 then
          begin
            ASecond:=StrToInt(Copy(aStr,1,i-1));
            aStr:=Copy(aStr,i+length(aTimeSpaceSign),maxint);
            AMilliSecond:=StrToIntDef(aStr,0);
          end
          else
          begin
            ASecond:=StrToInt(aStr);
          end;
        end
        else
        begin
          AMinute:=StrToInt(aStr);
        end;
      end
      else
      begin
        if pos(' ',aStr)>0 then
        begin
          AHour        :=StrToIntDef(Copy(aStr,10,2),AHour);
          AMinute      :=StrToIntDef(Copy(aStr,12,2),AMinute);
          ASecond      :=StrToIntDef(Copy(aStr,14,2),ASecond);
          AMilliSecond :=StrToIntDef(Copy(aStr,17,3),AMilliSecond);
        end
        else
        begin
          AHour        :=StrToIntDef(Copy(aStr,9,2),AHour);
          AMinute      :=StrToIntDef(Copy(aStr,11,2),AMinute);
          ASecond      :=StrToIntDef(Copy(aStr,13,2),ASecond);
          AMilliSecond :=StrToIntDef(Copy(aStr,15,3),AMilliSecond);
        end;
      end;
    end;
  end;

  result:= EncodeDateTime(AYear, AMonth, ADay, AHour, AMinute, ASecond,AMilliSecond)
end;

function ABStrToDate(aStr: string; aDateSpaceSign: string ): TDate;
begin
  result:= DateOf(ABStrToDateTime(aStr,aDateSpaceSign,':'));
end;

function ABStrToTime(aStr: string;aDateSpaceSign: string ; aTimeSpaceSign:string ): TTime;
begin
  if abpos(aDateSpaceSign,aStr)<=0 then
    aStr:='2001-01-01'+' '+trim(aStr);

  result:= TimeOf(ABStrToDateTime(aStr,aDateSpaceSign,aTimeSpaceSign));
end;

function ABTimeToStr(aDateTime: TDateTime; aLong: LongInt; aTimeSpaceSign:
  string): string;
begin
  Result := copy(ABDateTimeToStr(aDateTime, 23, '-', aTimeSpaceSign), 12,aLong);
end;

procedure ABDateTimeToDateAndTime(aDateTime:TDateTime;var  aDate:TDate;var aTime: TTime);
var
  AYear, AMonth, ADay, AHour, AMinute, ASecond,AMilliSecond: Word;
begin
  DecodeDateTime(aDateTime, AYear, AMonth, ADay, AHour, AMinute, ASecond,AMilliSecond);
  aDate:= EncodeDate(AYear, AMonth, ADay);
  aTime:= EncodeTime(AHour, AMinute, ASecond,AMilliSecond);
end;

function ABDateAndTimeToDatetime( aDate:TDate;aTime: TTime): TDateTime;
var
  AYear, AMonth, ADay, AHour, AMinute, ASecond,AMilliSecond: Word;
begin
  DecodeDate(ADate, AYear, AMonth, ADay);
  DecodeTime(aTime, AHour, AMinute, ASecond,AMilliSecond);

  result:= EncodeDateTime(AYear, AMonth, ADay, AHour, AMinute, ASecond,AMilliSecond)
end;

function ABGetMonthFirstDate(ADate: TDate): TDate;
var
  AFirstDay: string;
begin
  AFirstDay := FormatDateTime('yyyy-mm-dd', ADate);
  Delete(AFirstDay, 9, 2);
  Insert('01', AFirstDay, 9);
  Result := ABStrToDate(AFirstDay);
end;

function ABGetMonthEndDate(ADate: TDate): TDate;
var
  AFirstDay, ANextFirstDay: string;
begin
  AFirstDay := FormatDateTime('yyyy-mm-dd', ABGetMonthFirstDate(ADate));
  if StrToInt(copy(AFirstDay, 6, 2)) < 12 then
  begin
    //取当月第一天的日期
    ANextFirstDay := IntToStr(StrToInt(Copy(AFirstDay, 6, 2)) + 1);
    //取下月第一天的日期
    Delete(AFirstDay, 6, 2); //删除字串中的月份
    Insert(ANextFirstDay, AFirstDay, 6); //插入当月的月份到字串中
    Result := ABStrToDate(AFirstDay) - 1; //取得当月最后一天的日期
  end
  else
  begin
    Result := ABStrToDate(copy(AFirstDay, 1, 4) + '-12-31');
  end;
end;

function ABGetLastMonthFirstDate(ADate: TDate): TDate;
var
  AYear, AMonth, ADay: Word;
begin
  DecodeDate(ADate, AYear, AMonth, ADay);
  if AMonth = 1 then
  begin
    AYear := AYear - 1;
    AMonth := 12
  end
  else
  begin
    AMonth := AMonth - 1;
  end;
  ADate := EncodeDate(AYear, AMonth, ADay);
  Result := ABGetMonthFirstDate(ADate);
end;

function ABGetLastMonthEndDate(ADate: TDate): TDate;
var
  AYear, AMonth, ADay: Word;
begin
  DecodeDate(ADate, AYear, AMonth, ADay);
  if AMonth = 1 then
  begin
    AYear := AYear - 1;
    AMonth := 12
  end
  else
  begin
    AMonth := AMonth - 1;
  end;
  ADate := EncodeDate(AYear, AMonth, ADay);
  Result := ABGetMonthEndDate(ADate);
end;

function ABGetSeasonFirstDate(ADate: TDate): TDate;
var
  AYear, AMonth, ADay: Word;
begin
  DecodeDate(ADate, AYear, AMonth, ADay);
  if AMonth <= 3 then
    AMonth := 1
  else if AMonth <= 6 then
    AMonth := 4
  else if AMonth <= 9 then
    AMonth := 7
  else if AMonth <= 12 then
    AMonth := 10;
  ADate := EncodeDate(AYear, AMonth, 1);

  Result := ADate;
end;

function ABGetSeasonEndDate(ADate: TDate): TDate;
var
  AYear, AMonth, ADay: Word;
begin
  DecodeDate(ADate, AYear, AMonth, ADay);
  if AMonth <= 3 then
    AMonth := 3
  else if AMonth <= 6 then
    AMonth := 6
  else if AMonth <= 9 then
    AMonth := 9
  else if AMonth <= 12 then
    AMonth := 12;
  ADate := EncodeDate(AYear, AMonth, ADay);

  Result := ABGetMonthEndDate(ADate);
end;

function ABGetLastSeasonFirstDate(ADate: TDate): TDate;
var
  AYear, AMonth, ADay: Word;
begin
  DecodeDate(ADate, AYear, AMonth, ADay);
  if AMonth <= 3 then
  begin
    AMonth := 10;
    AYear := AYear - 1;
  end
  else if AMonth <= 6 then
    AMonth := 1
  else if AMonth <= 9 then
    AMonth := 4
  else if AMonth <= 12 then
    AMonth := 7;
  ADate := EncodeDate(AYear, AMonth, 1);

  Result := ABGetMonthFirstDate(ADate);
end;

function ABGetLastSeasonEndDate(ADate: TDate): TDate;
var
  AYear, AMonth, ADay: Word;
begin
  DecodeDate(ADate, AYear, AMonth, ADay);
  if AMonth <= 3 then
  begin
    AMonth := 12;
    AYear := AYear - 1;
  end
  else if AMonth <= 6 then
    AMonth := 3
  else if AMonth <= 9 then
    AMonth := 6
  else if AMonth <= 12 then
    AMonth := 9;
  ADate := EncodeDate(AYear, AMonth, ADay);

  Result := ABGetMonthEndDate(ADate);
end;

function ABGetYearFirstDate(ADate: TDate): TDate;
var
  AYear, AMonth, ADay: Word;
begin
  DecodeDate(ADate, AYear, AMonth, ADay);
  ADate := EncodeDate(AYear, 1, 1);
  Result := ADate;
end;

function ABGetYearEndDate(ADate: TDate): TDate;
var
  AYear, AMonth, ADay: Word;
begin
  DecodeDate(ADate, AYear, AMonth, ADay);
  ADate := EncodeDate(AYear, 12, 31);
  Result := ADate;
end;

function ABGetLastYearFirstDate(ADate: TDate): TDate;
var
  AYear, AMonth, ADay: Word;
begin
  DecodeDate(ADate, AYear, AMonth, ADay);
  ADate := EncodeDate(AYear - 1, 1, 1);
  Result := ADate;
end;

function ABGetLastYearEndDate(ADate: TDate): TDate;
var
  AYear, AMonth, ADay: Word;
begin
  DecodeDate(ADate, AYear, AMonth, ADay);
  ADate := EncodeDate(AYear - 1, 12, 31);
  Result := ADate;
end;

function ABGetLastYearSameDate(ADate: TDate): TDate;
var
  AYear, AMonth, ADay: Word;
begin
  DecodeDate(ADate, AYear, AMonth, ADay);
  ADate := EncodeDate(AYear - 1, AMonth, ADay);
  Result := ADate;
end;

function ABIsLeapYear(aYear: Integer): Boolean;
begin
  Result := (aYear mod 4 = 0) and ((aYear mod 100 <> 0) or (aYear mod 400 = 0));
end;

function ABMaxDateTime(const aDateTimes: array of TDateTime): TDateTime;
var
  I: Cardinal;
begin
  Result := aDateTimes[0];
  for I := 1 to High(aDateTimes) do
    if aDateTimes[I] > Result then
      Result := aDateTimes[I];
end;

function ABMinDateTime(const aDateTimes: array of TDateTime): TDateTime;
var
  I: Cardinal;
begin
  Result := aDateTimes[0];
  for I := 1 to High(aDateTimes) do
    if aDateTimes[I] < Result then
      Result := aDateTimes[I];
end;

function ABGetDateTimeSpan_Float(aBeg, aEnd: TDateTime; aResUnit: Word): Double;
begin
  if aResUnit = tuYears then
    result := YearSpan(aBeg, aEnd)
  else if aResUnit = tuMonths then
    result := MonthSpan(aBeg, aEnd)
  else if aResUnit = tuWeeks then
    result := WeekSpan(aBeg, aEnd)
  else if aResUnit = tuDays then
    result := DaySpan(aBeg, aEnd)
  else if aResUnit = tuHours then
    result := HourSpan(aBeg, aEnd)
  else if aResUnit = tuMinutes then
    result := MinuteSpan(aBeg, aEnd)
  else if aResUnit = tuSeconds then
    result := SecondSpan(aBeg, aEnd)
  else if aResUnit = tuMilliSeconds then
    result := MilliSecondSpan(aBeg, aEnd)
  else
    Result := 0;
end;

function ABWeekOfYear(aData: TDate): Integer;
var
  Year, Month, Day , Week: Word;
  FirstDate:TDate;
begin
  DecodeDate( aData, Year, Month, Day );
  FirstDate:=StrToDate(IntToStr(Year)+'-1-1');
  Week:=DayOfWeek(FirstDate)-1;
  Result:=((Round(aData-FirstDate)-1+week) div 7)+1;
end;

function ABGetDateTimeSpan_Int(aBeg, aEnd: TDateTime; aResUnit: Word): LongInt;
begin
  if aResUnit = tuYears then
    result :=ABTrunc(YearSpan(aBeg, aEnd))
  else if aResUnit = tuMonths then
    result := ABTrunc(MonthSpan(aBeg, aEnd))
  else if aResUnit = tuWeeks then
    result := ABTrunc(WeekSpan(aBeg, aEnd))
  else if aResUnit = tuDays then
    result := ABTrunc(DaySpan(aBeg, aEnd))
  else if aResUnit = tuHours then
    result := ABTrunc(HourSpan(aBeg, aEnd))
  else if aResUnit = tuMinutes then
    result := ABTrunc(MinuteSpan(aBeg, aEnd))
  else if aResUnit = tuSeconds then
    result := ABTrunc(SecondSpan(aBeg, aEnd))
  else if aResUnit = tuMilliSeconds then
    result := ABTrunc(MilliSecondSpan(aBeg, aEnd))
  else
    Result := 0;
end;

function ABGetGMT: TDateTime;
var
  TimeRec: TSystemTime;
begin
  GetSystemTime(TimeRec);

  Result := SystemTimeToDateTime(TimeRec);
end;

const
  MinsPerDay = 24 * 60;

function ABGetGMTBias: Integer;
var
  info: TTimeZoneInformation;
  Mode: DWord;
begin
  Mode := GetTimeZoneInformation(info);
  Result := info.Bias;
  case Mode of
    TIME_ZONE_ID_INVALID: RaiseLastOSError;
    TIME_ZONE_ID_STANDARD: Result := Result + info.StandardBias;
    TIME_ZONE_ID_DAYLIGHT: Result := Result + info.DaylightBias;
  end;
end;

function ABLocaleToGMT(const aDateTime: TDateTime): TDateTime;
begin
  Result := aDateTime + (ABGetGMTBias / MinsPerDay);
end;

function ABGMTToLocale(const aDateTime: TDateTime): TDateTime;
begin
  Result := aDateTime - (ABGetGMTBias / MinsPerDay);
end;

function ABBinToDec(aValue: string): string; //int64;
var
  str: string;
  Int:int64 ;
  i: Integer;
begin
  Str := UpperCase(aValue);
  Int := 0;
  for i := 1 to Length(str) do
    Int := Int * 2 + ORD(str[i]) - 48;
  Result := IntToStr(Int);
end;

function ABDecTobin(aValue: int64): string;
//十进制转二进制 函数一
  function mod_num(n1, n2: int64): int64; //取余数
  begin
    result := n1 - n1 div n2 * n2
  end;
  //十进制转二进制 函数二
  function reverse(s: string): string; //取反串
  var
    i, num: Integer;
    st: string;
  begin
    num := Length(s);
    st := EmptyStr;
    for i := num downto 1 do
    begin
      st := st + s[i];
    end;
    Result := st;
  end;
var
  ST: string;
  N: int64;
begin
  ST := EmptyStr;
  n := aValue;
  while n >= 2 do
  begin
    st := st + IntToStr(mod_num(n, 2));
    n := n div 2;
  end;
  st := st + IntToStr(n);
  Result := reverse(st);
end;

function ABHexToBin(aHex: string): string;
var
  I: Integer;
begin
  Result:=emptystr;
  for I := 1 to Length(aHex) do
  begin
    case aHex[I] of
     '0': Result := Result+'0000';
     '1': Result := Result+'0001';
     '2': Result := Result+'0010';
     '3': Result := Result+'0011';
     '4': Result := Result+'0100';
     '5': Result := Result+'0101';
     '6': Result := Result+'0110';
     '7': Result := Result+'0111';
     '8': Result := Result+'1000';
     '9': Result := Result+'1001';
     'A': Result := Result+'1010';
     'B': Result := Result+'1011';
     'C': Result := Result+'1100';
     'D': Result := Result+'1101';
     'E': Result := Result+'1110';
     'F': Result := Result+'1111';
    end;
  end;
end;

function ABBinToHex(aBin: String): String;
var
  I:Integer;
  Sum:Integer;
begin
  Result:=emptystr;
  if Length(aBin) mod 4 <>0 then
    for I:=1 to 4-Length(aBin) mod 4 do
      aBin:='0'+aBin;

  Sum:=0;
  for I:=1 to Length(aBin) do
  begin
    Sum:=Sum*2+StrToInt(aBin[I]);
    if I mod 4=0 then
    begin
      case Sum of
        0..9:Result:=Result+IntToStr(Sum);
        10..15:Result:=Result+Chr(65+Sum-10);//65是A的ASCII
      end;
      Sum:=0;
    end;
  end;
end;

function ABTransChar(const aStr: string): Integer;
begin
  if (aStr[1]>='0') and (aStr[1]<='9') then
    Result := Ord(aStr[1]) - Ord('0')
  else
    Result :=Ord(aStr[1]) - Ord('A');
end;

function ABTrimInt(aValue, aMin, aMax: Integer): Integer;
begin
  if aValue > aMax then
    Result := aMax
  else if aValue < aMin then
    Result := aMin
  else
    Result := aValue;
end;

procedure ABEnRect(var aRect: TRect; ax, ay, aWidth, aHeight: Integer);
begin
  aRect.Left  :=    ax      ;
  aRect.Top   :=    ay      ;
  aRect.Right :=    aRect.Left+aWidth ;
  aRect.Bottom:=    aRect.Top+aHeight ;
end;

procedure ABDeRect(aRect: TRect; var ax, ay, aWidth, aHeight: Integer);
begin
  ax      := aRect.Left             ;
  ay      := aRect.Top              ;
  aWidth  := aRect.Right - aRect.Left;
  aHeight := aRect.Bottom - aRect.Top;
end;

function ABRectEqu(aRect1, aRect2: TRect): Boolean;
begin
  Result := (aRect1.Left = aRect2.Left) and (aRect1.Top = aRect2.Top) and
            (aRect1.Right = aRect2.Right) and (aRect1.Bottom = aRect2.Bottom);
end;

procedure ABEnSize(var aSize: TSize;acx, acy: Integer);
begin
  aSize.cx   :=    acx      ;
  aSize.cy   :=    acy      ;
end;

procedure ABDeSize(aSize: TSize; var acx, acy: Integer);
begin
  acx      := aSize.cx             ;
  acy      := aSize.cy             ;
end;

function ABSizeEqu(aSize1, aSize2: TSize): Boolean;
begin
  Result := (aSize1.cx = aSize2.cx) and (aSize1.cy = aSize2.cy);
end;

function ABInBound(aValue: Integer; aMin, aMax: Integer): Boolean;
begin
  Result := (aValue >= aMin) and (aValue <= aMax);
end;

procedure ABSwap(var aA, aB: Variant);
var
  Tmp: Variant;
begin
  Tmp := aA;
  aA := aB;
  aB := Tmp;
end;

function ABIsNumExpression(aExpression: string): Boolean;
var
   i:integer;
begin
  aExpression := trim(aExpression);
  for i:=1 to length(aExpression) do
  begin
    if (not CharInSet(aExpression[i],['0'..'9','+','-','*','/','(',')','.',' ']))  then
    begin
      result:=false;
      exit;
    end;
  end;
  result:=true;
end;

function ABExpressionEval(aExpression: string; var aError: Boolean): Variant;
var
  i: integer;
  c: char;
  ctemp: char;
  strnew: string;
  Operators: array[0..99] of char;
  Operands: array[0..99] of double;
  Operand: string;
  top: integer;
  x, y, v: double;
begin
  result := EmptyStr;
  aExpression := trim(aExpression);
  //判断STRORD只有数字或运算符号
  if not ABIsNumExpression(aExpression) then
  begin
    aError:=True;
    exit;
  end;

  v:=0;
  aError:=false;
  try
    //----------------------------
    strnew := '';
    top := -1;
    for i := 1 to length(aExpression) do
    begin
      c := aExpression[i];
      case c of
        ' ': ;
        '+', '-':
          begin
            while (top >= 0) do
            begin
              ctemp := Operators[top];
              top := top - 1;
              if ctemp = '(' then
              begin
                top := top + 1;
                Operators[top] := ctemp;
                break; //跳出这个while循环
              end
              else
                strnew := strnew + ctemp;
            end;
            top := top + 1;
            Operators[top] := c;
            strnew := strnew + ' ';
          end;
        '*', '/':
          begin
            while (top >= 0) do
            begin
              ctemp := Operators[top];
              top := top - 1;
              if ctemp = '(' then
              begin
                top := top + 1;
                Operators[top] := ctemp;
                break; //跳出这个while循环
              end
              else
              begin
                if (ctemp = '+') or (ctemp = '-') then
                begin
                  top := top + 1;
                  Operators[top] := ctemp;
                  break; //跳出这个while循环
                end
                else
                  strnew := strnew + ctemp;
              end;
            end;
            top := top + 1;
            Operators[top] := c;
            strnew := strnew + ' ';
          end;
        '(':
          begin
            top := top + 1;
            Operators[top] := c;
            strnew := strnew + ' ';
          end;
        ')':
          begin
            while (top >= 0) do
            begin
              ctemp := Operators[top];
              top := top - 1;
              if ctemp = '(' then
              begin
                break; //跳出这个while循环
              end
              else
                strnew := strnew + ctemp;
            end;
            strnew := strnew + ' ';
          end;
      else
        strnew := strnew + c;
      end;
    end; //for end
    //////////操作符出栈
    while (top >= 0) do
    begin
      strnew := strnew + Operators[top];
      top := top - 1;
    end;
    //////////
    top := -1;
    Operand := '';
    for i := 1 to length(strnew) do
    begin
      c := strnew[i];
      if ((c >= '0') and (c <= '9')) or (c = '.') then
      begin
        Operand := Operand + c;
      end;
      if ((c = ' ') or (i = length(strnew))) and (Operand <> '') then
      begin
        top := top + 1;
        Operands[Top] := strtofloat(Operand);
        Operand := '';
      end;
      if (c = '+') or (c = '-') or (c = '*') or (c = '/') then
      begin
        if (Operand <> '') then
        begin
          top := top + 1;
          Operands[Top] := strtofloat(Operand);
          Operand := '';
        end;
        y := Operands[Top]; //pop 双目运算符的第二操作数 (后进先出)注意操作数顺序对除法的影响
        top := top - 1;
        x := Operands[Top]; //pop 双目运算符的第一操作数
        top := top - 1;
        case c of
          '+': v := x + y;
          '-': v := x - y;
          '*': v := x * y;
          '/':
            begin
              if y <> 0 then
              begin
                v := x / y; // 第一操作数 / 第二操作数 注意操作数顺序对除法的影响
              end
              else
              begin
                Result := 'Dividend By Zero';
                exit;
              end;
            end;
        else
          v := 0;
        end; //end case
        top := top + 1;
        Operands[Top] := v; //push 中间结果再次入栈
      end;
    end; //end for

    if Top>=0 then
      v := Operands[Top] //pop 最终结果
    else
      aError:=true;
    // showmessage('计算结果为：    '+floattostr(v));
  //////////计算后缀表达式：结束

    result := floattostr(v);
  except
    aError:=true;
  end;
end;

function ABCeil(aX: Extended): Integer;
begin
  Result := Ceil(aX);
end;

function ABFloor(aX: Extended): Integer;
begin
  Result := Floor(aX);
end;

function ABTrunc(aNum: double): Integer;
begin
  Result:=Trunc(ABRound(aNum,0));
end;

function ABCompareDouble(aNum1,aNum2: double; aPosition: integer ): longint;
var
  tempPosition:LongInt;
  tempPower,
  tempInt1,tempInt2:LongInt;
  tempStr1,tempStr2:string;
begin
  tempPower:=1;
  tempPosition:=Abs(aPosition);
  while tempPosition>0 do
  begin
    tempPower:=tempPower*10;
    tempPosition:=tempPosition-1;
  end;

  //正向取小数位
  if aPosition>=0 then
  begin
    tempStr1:=FormatFloat('#.'+ StringOfChar('0',aPosition),StrToFloat(FloatToStr(aNum1)));
    tempStr2:=FormatFloat('#.'+ StringOfChar('0',aPosition),StrToFloat(FloatToStr(aNum2)));
    if StrToFloat(tempStr1)>StrToFloat(tempStr2) then
      result:=1
    else if StrToFloat(tempStr1)=StrToFloat(tempStr2) then
      result:=0
    else
      result:=-1;
  end
  //反向取小数位
  else
  begin
    tempInt1:=ABTrunc(aNum1 / tempPower);
    tempInt2:=ABTrunc(aNum2 / tempPower);
    if tempInt1>tempInt2 then
      result:=1
    else if tempInt1=tempInt2 then
      result:=0
    else
      result:=-1;
  end;
end;

function ABRound(aNum: double; aPosition: integer): double;
var
  LFactor: Double;
begin
  LFactor := IntPower(10, aPosition);
  if aNum < 0 then
    Result := (aNum * LFactor) - 0.5
  else
    Result := (aNum * LFactor) + 0.5;

  Result := Trunc(Result) / LFactor;
end;

function ABxhl(const aByte: Byte): Byte;
asm
  rol al, 4   /// 循环左移4位即可
end;

function ABxhl(const aWord: word): word;
asm
  // xchg ah,al
  rol ax, 8   /// 循环左移8位即可
end;

function ABxhl(const aDWord: DWord): Dword;
asm
  rol eax, 16    /// 循环左移16位即可
end;

function ABRoundStr(aNum: double; aPosition: integer;aFillZero:Boolean): string;
var
  i,j:longint;
begin
  result:=FloatToStr(ABRound(aNum,aPosition));
  if aPosition>0 then
  begin
    if aFillZero then
    begin
      i:=pos('.',result);
      if i<=0 then
      begin
        result:=result+'.';
        i:=Length(result)+1;
      end;

      j:=aPosition;
      while j>0 do
      begin
        result:=result+'0';
        j:=j-1;
      end;
      result:=copy(result,1,i)+copy(result,i+1,aPosition);
    end;
  end
end;

function ABRandomStr(aLength: Longint): string;
var
  X: Longint;
begin
  Randomize;
  if aLength <= 0 then
    exit;
  SetLength(Result, aLength);
  for X := 1 to aLength do
    Result[X] := Chr(Random(26) + 65);
end;

function ABIsSpace(const aStr: string): Boolean;
begin
  Result :=(ABIsEmptyStr(aStr)) or (CharInSet(aStr[1] , [#32, #8, #13, #10]));
end;

function ABIsXDigit(const aStr: string): Boolean;
begin
  Result := CharInSet(aStr[1],['0'..'9', 'a'..'f', 'A'..'F']);
end;

function ABIsInteger(const aStr: string): Boolean;
var
  iValue: Integer;
begin
  Result := TryStrToInt(aStr, iValue);
end;

function ABIsInt64(const aStr: string): Boolean;
var
  iValue: Int64;
begin
  Result := TryStrToInt64(aStr, iValue);
end;

function ABIsFloat(const aStr: string): Boolean;
var
  dValue: Double;
begin
  Result := TryStrToFloat(aStr, dValue);
end;

function ABRandomNum(aBegNum,aEndNum: Integer): integer;
begin
  Result:=aBegNum+ABRandomNum(aEndNum-aBegNum);
end;

function ABRandomStr(aBegNum,aEndNum: Integer): string;
begin
  Result:=IntToStr(ABRandomNum(aBegNum,aEndNum));
end;

function ABRandomChinese(aLength: Longint): string;
var
  i: Integer;
begin
  Result:=EmptyStr;
  for I := 1 to aLength do
  begin
    Result:=Result+WideChar(20000 + ABRandomNum(10000));
  end;
end;

function ABRandomNum(aMaxNum: Integer): integer;
begin
  Result := 0;
  if aMaxNum = 0 then
    Exit;

  aMaxNum := Abs(aMaxNum);
  Randomize;
  Result := ABTrunc(abs(Random(ABTicker) mod (aMaxNum+1)));
end;

function ABBoolToStr(aBool: Boolean): string;
begin
  Result :=ABBoolToDefStr(aBool,'True','False');
end;

function ABBoolToDefStr(aBool: Boolean;aTrueValue:string;aFalseValue:string): string;
begin
  if aBool then
    Result := aTrueValue
  else
    Result := aFalseValue;
end;

function ABBoolToInt(aBool: Boolean): LongInt;
begin
  Result :=ABBoolToDefInt(aBool,1,0);
end;

function ABBoolToDefInt(aBool: Boolean;aTrueValue:LongInt;aFalseValue:LongInt): LongInt;
begin
  if aBool then
    Result := aTrueValue
  else
    Result := aFalseValue;
end;

function ABStrToBool(aValue: string): Boolean;
begin
  Result :=ABDefStrToBool(aValue,FTrueStrs);
end;

function ABDefStrToBool(aValue: string;aTrueValueArray:array of string): Boolean;
var
  i:LongInt;
begin
  result:= False;
  for I := Low(aTrueValueArray) to High(aTrueValueArray) do
  begin
    result:= AnsiCompareText(aValue,aTrueValueArray[i]) = 0;
    if result then
      Break;
  end;
end;

function ABIntToBool(aValue: LongInt): Boolean;
begin
  Result :=ABDefIntToBool(aValue,[1]);
end;

function ABDefIntToBool(aValue: LongInt;aTrueValueArray:array of LongInt): Boolean;
var
  i:LongInt;
begin
  result:= False;
  for I := Low(aTrueValueArray) to High(aTrueValueArray) do
  begin
    result:=(aValue=aTrueValueArray[i]);
    if result then
      Break;
  end;
end;

function ABGetSexFlag(aStr: string;aSexFlags:array of string): string;
var
  tempmanFlag,
  tempwomanFlag,
  tempNoKnowFlag:string;
  i:LongInt;
begin
  result :=EmptyStr;

  if (High(aSexFlags)-Low(aSexFlags))>=0 then
    tempmanFlag:=aSexFlags[0]
  else
    tempmanFlag := 'man';

  if (High(aSexFlags)-Low(aSexFlags))>=1 then
    tempwomanFlag:=aSexFlags[1]
  else
    tempwomanFlag := 'woman';

  if (High(aSexFlags)-Low(aSexFlags))>=2 then
    tempNoKnowFlag:=aSexFlags[2]
  else
    tempNoKnowFlag := 'NoKnow';

  if result=EmptyStr then
  begin
    for I := Low(FTrueStrs) to High(FTrueStrs) do
    begin
      if AnsiCompareText(aStr,FTrueStrs[i]) = 0 then
      begin
        result :=tempmanFlag;
        Break;
      end;
    end;
  end;

  if result=EmptyStr then
  begin
    for I := Low(FFalseStrs) to High(FFalseStrs) do
    begin
      if AnsiCompareText(aStr,FFalseStrs[i]) = 0 then
      begin
        result :=tempwomanFlag;
        Break;
      end;
    end;
  end;

  if result=EmptyStr then
  begin
    result := tempNoKnowFlag;
  end;
end;

function ABGetGreatestCommonDivisor(ax,ay: LongInt): LongInt;
var
  n: LongInt;
begin
  n := ay;
  if ax < ay then
  begin
    ay := ax;
    ax := n;
  end;

  while n > 0 do
  begin
    n := ax mod ay;
    ax := ay;
    if n > 0 then ay := n;
  end;
  Result := ay;
end;

function ABGetLeaseCommonMultiple(ax,ay: LongInt): LongInt;
var
  m,n: LongInt;
begin
  m := ax * ay;
  n := ay;
  if ax < ay then
  begin
    ay := ax;
    ax := n;
  end;

  while n > 0 do
  begin
    n := ax mod ay;
    ax := ay;
    if n > 0 then ay := n;
  end;

  Result := m div ay;
end;

procedure ABIntArraySort(var aIntArray:array of LongInt);
  procedure IntSort(var aIntArray:array of LongInt; low:Integer=0; high:Integer=-1; k:Cardinal=$80000000; c:Cardinal=1);
  var
    i,j,x: Integer;
  begin
    if high = -1 then high := Length(aIntArray) -1;
    i := low;
    j := high;
    while (i < j) do
    begin
      while (aIntArray[j] and k <> 0) and (i < j) do Dec(j);
      while (aIntArray[i] and k = 0) and (i < j) do Inc(i);
      if i < j then
      begin
        x := aIntArray[j];
        aIntArray[j] := aIntArray[i];
        aIntArray[i] := x;
      end else begin
        if aIntArray[j] and k <> 0 then Dec(i) else Inc(j);
        Break;
      end;
    end;
    if k > c then
    begin
      if low < i then IntSort(aIntArray, low, i, k div 2);
      if j < high then IntSort(aIntArray, j, high, k div 2);
    end;
  end;
begin
  IntSort(aIntArray);
end;

function ABVartoInt64Def(aValue:Variant;aDefault:Int64):Int64;
begin
  result:= StrToInt64Def(VarToStrDef(aValue,IntToStr(aDefault)),aDefault);
end;

function ABIIF(aTest: Boolean; aTrueValue, aFalseValue: Variant): Variant;
begin
  if aTest then
    Result := aTrueValue
  else
    Result := aFalseValue;
end;

function ABIsNull(aVar: Variant;aValue: Variant): Variant;
begin
  if (ABVarIsNull(aVar)) or
     (VarIsStr(aVar)) and (VarToStrDef(aVar,emptystr)=emptystr) then
    Result := aValue
  else
    Result := aVar;
end;

function ABGetIPFlag(aHostIP: string;aAddInt:LongInt): longint;
var
  tempPathings:TStrings;
  I: Integer;
begin
  result:=0;
  tempPathings := TStringList.Create;
  try
    ABStrsToStrings(aHostIP, '.',tempPathings);
    for I := 0 to tempPathings.Count - 1 do
    begin
      result:=result+StrToInt(tempPathings[i])*ABTrunc(power(10,i));
    end;
    result:=result+aAddInt;
  finally
    tempPathings.Free;
  end;
end;

function ABRegisterHotKey(ahandle:Hwnd;
                          aAtomName:string;
                          aShortCut:TShortCut):longint;
  function ByteReserve(aValue: Word):Word;
  var
    buf:Byte;
    m: Integer;
  begin
    Result := 0;
    for m:=0 to 7 do
    begin
      buf := Byte((aValue shr m) and $01);
      Result := Result or (buf shl (7-m));
    end;
  end;
var
  tempkey:word;
  tempshift:TShiftState;
  Shirt:Cardinal;
begin
  result:=0;
  if aShortCut>0 then
  begin
    //增加一个原子
    result:=GlobalAddAtom(Pchar(aAtomName));
    if result<>0 then
    begin
      ShortCutToKey(aShortCut, tempkey, tempshift);
      Shirt := ByteReserve(Word((aShortCut and $F000) shr 8));
      RegisterHotKey(ahandle,result,Shirt,Cardinal(Chr(tempkey)));
    end;
  end;
end;

function ABUnRegisterHotKey(ahandle:Hwnd;
                          aAtomName:string):longint;
begin
  result:=GlobalFindAtom(Pchar(aAtomName));
  if result<>0 then
  begin
    if UnRegisterHotKey(ahandle,result) then
    begin
      GlobalDeleteAtom(result);
    end;
  end;
end;

procedure ABAddAutoRun(aAutoRunType: TABAutoRunType;aAutoRunName, aAutoRunFileName: string;aParams:string);
var
  Regf: TRegistry;
  tempValue:string;
begin
  if ABCheckFileExists(aAutoRunFileName) then
  begin
    Regf := TRegistry.Create;
    Regf.RootKey := HKEY_LOCAL_MACHINE;
    try
      if aAutoRunType=atRun then
      begin
        RegF.OpenKey('SOFTWARE\Microsoft\Windows\CurrentVersion\Run', true);
      end
      else
      begin
        RegF.OpenKey('SOFTWARE\Microsoft\Windows\CurrentVersion\RunOnce', true);
      end;
      tempValue:=RegF.ReadString(aAutoRunName);

      if AnsiCompareText(tempValue,aAutoRunFileName+abiif(aParams='','',' '+aParams))=0 then
      begin

      end
      else
      begin
        RegF.WriteString(aAutoRunName, '"'+aAutoRunFileName+'"'+abiif(aParams='','',' '+aParams));
      end;
    finally
      RegF.CloseKey;
      RegF.Free;
    end;
  end;
end;

procedure ABDelAutoRun(aAutoRunType: TABAutoRunType;aAutoRunName: string);
var
  Regf: TRegistry;
begin
  Regf := TRegistry.Create;
  Regf.RootKey := HKEY_LOCAL_MACHINE;
  try
    if aAutoRunType=atRun then
    begin
      RegF.OpenKey('SOFTWARE\Microsoft\Windows\CurrentVersion\Run', true);
    end
    else
    begin
      RegF.OpenKey('SOFTWARE\Microsoft\Windows\CurrentVersion\RunOnce', true);
    end;
    RegF.DeleteValue(aAutoRunName);
  finally
    RegF.CloseKey;
    RegF.Free;
  end;
end;

function ABFindRegPath(const aPath: string;aRootKey: HKEY): Boolean;
var
  rRegObject: TRegistry;
begin
  result:=false;
  rRegObject := TRegistry.Create;
  try
    rRegObject.RootKey := aRootKey;
    if rRegObject.OpenKey(aPath, False) then
    begin
      Result := True;
    end;
    rRegObject.CloseKey;
  finally
    rRegObject.Free;
  end;
end;

function ABCreateRegPath(const aPath: string;aRootKey: HKEY): Boolean;
var
  rRegObject: TRegistry;
begin
  result:=false;
  rRegObject := TRegistry.Create;
  try
    rRegObject.RootKey := aRootKey;
    if rRegObject.OpenKey(aPath, True) then
    begin
      Result := True;
    end;
    rRegObject.CloseKey;
  finally
    rRegObject.Free;
  end;
end;

function ABDeleteRegPath(const aPath: string;aRootKey: HKEY): Boolean;
var
  rRegObject: TRegistry;
begin
  result:=false;
  rRegObject := TRegistry.Create;
  try
    rRegObject.RootKey := aRootKey;
    if rRegObject.DeleteKey(aPath) then
    begin
      Result := True;
    end;
    rRegObject.CloseKey;
  finally
    rRegObject.Free;
  end;
end;

function ABFindRegKey(aPath: string; aKeyName: string;aRootKey: HKEY): Boolean;
var
  rRegObject: TRegistry;
  Info: TRegDataInfo;
begin
  result:=false;
  rRegObject := TRegistry.Create;
  try
    rRegObject.RootKey := aRootKey;
    if rRegObject.OpenKey(aPath, False) then
    begin
      Result := rRegObject.GetDataInfo(aKeyName, Info) and (Info.DataSize > 0);
    end;
    rRegObject.CloseKey;
  finally
    rRegObject.Free;
  end;
end;

function ABReadRegKey(aPath: string; aKeyName: string; aRootKey: HKEY): string;
var
  rRegObject: TRegistry;
begin
  rRegObject := TRegistry.Create;
  try
    rRegObject.RootKey := aRootKey;
    if rRegObject.OpenKey(aPath, False) then
    begin
      Result := rRegObject.GetDataAsString(aKeyName);
    end
    else
      Result := '';
    rRegObject.CloseKey;
  finally
    rRegObject.Free;
  end;
end;

function ABDeleteRegKey( aPath: string; aKeyName: string;aRootKey: HKEY):boolean;
var
  rRegObject: TRegistry;
begin
  result:=false;
  rRegObject := TRegistry.Create;
  try
    rRegObject.RootKey := aRootKey;
    if rRegObject.OpenKey(aPath, false) then
    begin
      rRegObject.DeleteValue(aKeyName);
      result:=True;
    end;
    rRegObject.CloseKey;
  finally
    rRegObject.Free;
  end;
end;

function ABWriteRegKey(aPath: string; aKeyName, aKeyValue: string;
                      aValueType: TABRegKeyValueType; aRootKey: HKEY):boolean;
var
  rRegObject: TRegistry;
begin
  result:=false;
  rRegObject := TRegistry.Create;
  try
    rRegObject.RootKey := aRootKey;
    if rRegObject.OpenKey(aPath, True) then
    begin
      case aValueType of
        rtString: rRegObject.WriteString(aKeyName, aKeyValue);
        rtInteger: rRegObject.WriteInteger(aKeyName, StrToInt(aKeyValue));
      end;
      result:=True;
    end;
    rRegObject.CloseKey;
  finally
    rRegObject.Free;
  end;
end;

procedure ABCopyStringArray  (aSArrays:array of string  ; aDArrays:TStringDynArray  );
var
  i: LongInt;
begin
  SetLength(aDArrays,High(aSArrays)-Low(aSArrays)+1);

  for I := Low(aSArrays) to High(aSArrays) do
  begin
    aDArrays[i]:= aSArrays[i];
  end;
end;

procedure ABCopyIntegerArray (aSArrays:array of Integer ; aDArrays:TIntegerDynArray );
var
  i: LongInt;
begin
  SetLength(aDArrays,High(aSArrays)-Low(aSArrays)+1);

  for I := Low(aSArrays) to High(aSArrays) do
  begin
    aDArrays[i]:= aSArrays[i];
  end;
end;
procedure ABCopyByteArray    (aSArrays:array of Byte    ; aDArrays:TByteDynArray    );
var
  i: LongInt;
begin
  SetLength(aDArrays,High(aSArrays)-Low(aSArrays)+1);

  for I := Low(aSArrays) to High(aSArrays) do
  begin
    aDArrays[i]:= aSArrays[i];
  end;
end;
procedure ABCopyDoubleArray  (aSArrays:array of Double  ; aDArrays:TDoubleDynArray  );
var
  i: LongInt;
begin
  SetLength(aDArrays,High(aSArrays)-Low(aSArrays)+1);

  for I := Low(aSArrays) to High(aSArrays) do
  begin
    aDArrays[i]:= aSArrays[i];
  end;
end;
procedure ABCopyBooleanArray (aSArrays:array of Boolean ; aDArrays:TBooleanDynArray );
var
  i: LongInt;
begin
  SetLength(aDArrays,High(aSArrays)-Low(aSArrays)+1);

  for I := Low(aSArrays) to High(aSArrays) do
  begin
    aDArrays[i]:= aSArrays[i];
  end;
end;

function ABStringArrayToStr(aArrays:array of string; aQuoted: Boolean; aSpaceSign:string): string;
var
  tempstr, tempstr1: string;
  i: LongInt;
begin
  Result := EmptyStr;
  for I := Low(aArrays) to High(aArrays) do
  begin
    if aQuoted then
      tempstr := QuotedStr(aArrays[i])
    else
      tempstr := aArrays[i];

    if tempstr1 = EmptyStr then
      tempstr1 := tempstr
    else
      tempstr1 := tempstr1 + aSpaceSign + tempstr;
  end;
  Result := tempstr1;
end;

function ABAnsiCharArrayToStr(aArrays:array of AnsiChar; aQuoted: Boolean ;aSpaceSign: string ): string;
var
  tempstr, tempstr1: string;
  i: LongInt;
begin
  Result := EmptyStr;
  for I := Low(aArrays) to High(aArrays) do
  begin
    if aQuoted then
      tempstr := QuotedStr(String(AnsiString(aArrays[i])))
    else
      tempstr := String(AnsiString(aArrays[i]));

    if tempstr1 = EmptyStr then
      tempstr1 := tempstr
    else
      tempstr1 := tempstr1 + aSpaceSign + tempstr;
  end;
  Result := tempstr1;
end;

function ABWideCharArrayToStr(aArrays:array of WideChar; aQuoted: Boolean ;aSpaceSign: string ): string;
var
  tempstr, tempstr1: string;
  i: LongInt;
begin
  Result := EmptyStr;
  for I := Low(aArrays) to High(aArrays) do
  begin
    if aQuoted then
      tempstr := QuotedStr(aArrays[i])
    else
      tempstr := aArrays[i];

    if tempstr1 = EmptyStr then
      tempstr1 := tempstr
    else
      tempstr1 := tempstr1 + aSpaceSign + tempstr;
  end;
  Result := tempstr1;
end;

function ABIntegerArrayToStr(aArrays:array of Integer; aQuoted: Boolean ;aSpaceSign: string ): string;
var
  tempstr, tempstr1: string;
  i: LongInt;
begin
  Result := EmptyStr;
  for I := Low(aArrays) to High(aArrays) do
  begin
    if aQuoted then
      tempstr := QuotedStr(inttostr(aArrays[i]))
    else
      tempstr := inttostr(aArrays[i]);

    if tempstr1 = EmptyStr then
      tempstr1 := tempstr
    else
      tempstr1 := tempstr1 + aSpaceSign + tempstr;
  end;
  Result := tempstr1;
end;

function ABByteArrayToStr(aArrays:array of Byte; aQuoted: Boolean ;aSpaceSign: string ): string;
var
  tempstr, tempstr1: string;
  i: LongInt;
begin
  Result := EmptyStr;
  for I := Low(aArrays) to High(aArrays) do
  begin
    if aQuoted then
      tempstr := QuotedStr(inttostr(aArrays[i]))
    else
      tempstr := inttostr(aArrays[i]);

    if tempstr1 = EmptyStr then
      tempstr1 := tempstr
    else
      tempstr1 := tempstr1 + aSpaceSign + tempstr;
  end;
  Result := tempstr1;
end;

function ABDoubleArrayToStr(aArrays:array of Double; aQuoted: Boolean;aSpaceSign: string): string;
var
  tempstr, tempstr1: string;
  i: LongInt;
begin
  Result := EmptyStr;
  for I := Low(aArrays) to High(aArrays) do
  begin
    if aQuoted then
      tempstr := QuotedStr(FloatToStr(aArrays[i]))
    else
      tempstr := FloatToStr(aArrays[i]);

    if tempstr1 = EmptyStr then
      tempstr1 := tempstr
    else
      tempstr1 := tempstr1 + aSpaceSign + tempstr;
  end;
  Result := tempstr1;
end;

function ABBooleanArrayToStr(aArrays:array of Boolean    ; aQuoted: Boolean;aSpaceSign: string): string;
var
  tempstr, tempstr1: string;
  i: LongInt;
begin
  Result := EmptyStr;
  for I := Low(aArrays) to High(aArrays) do
  begin
    if aQuoted then
      tempstr := QuotedStr(ABBoolToStr(aArrays[i]))
    else
      tempstr := ABBoolToStr(aArrays[i]);

    if tempstr1 = EmptyStr then
      tempstr1 := tempstr
    else
      tempstr1 := tempstr1 + aSpaceSign + tempstr;
  end;
  Result := tempstr1;
end;

function ABVariantArrayToStr (aArrays:array of Variant ; aQuoted: Boolean ;aSpaceSign: string ): string;
var
  tempstr, tempstr1: string;
  i: LongInt;
begin
  Result := EmptyStr;
  for I := Low(aArrays) to High(aArrays) do
  begin
    if aQuoted then
      tempstr := QuotedStr(VarToStr(aArrays[i]))
    else
      tempstr := VarToStr(aArrays[i]);

    if tempstr1 = EmptyStr then
      tempstr1 := tempstr
    else
      tempstr1 := tempstr1 + aSpaceSign + tempstr;
  end;
  Result := tempstr1;
end;

function ABVariantArrayToStr (aArrays:Variant ; aQuoted: Boolean = false;aSpaceSign: string = ','): string;
var
  tempstr, tempstr1: string;
  i: LongInt;
begin
  Result := EmptyStr;
  if (VarIsArray(aArrays)) then
  begin
    for i := VarArrayLowBound(aArrays,1) to VarArrayHighBound(aArrays,1) do
    begin
      if aQuoted then
        tempstr := QuotedStr(VarToStr(aArrays[i]))
      else
        tempstr := VarToStr(aArrays[i]);

      if tempstr1 = EmptyStr then
        tempstr1 := tempstr
      else
        tempstr1 := tempstr1 + aSpaceSign + tempstr;
    end;
    Result := tempstr1;
  end;
end;

function ABDeleteInStrItem(aStrs: string;aDeleteStrs: string): string;
var
  i: LongInt;
  tempArrays:TStringDynArray;
  tempDeleteArrays:TStringDynArray;
  tempResult:TStringDynArray;
begin
  result:=EmptyStr;
  tempArrays:=ABStrToStringArray(aStrs);
  tempDeleteArrays:=ABStrToStringArray(aDeleteStrs);
  tempResult:=ABDeleteInArrayItem(tempArrays,tempDeleteArrays);

  for I := Low(tempResult) to High(tempResult) do
  begin
    ABAddstr(result,tempResult[i]);
  end;
end;

function ABDeleteInArrayItem(aArrays:array of string;aDeleteArrays:array of string):TStringDynArray;
var
  i,tempCount: LongInt;
begin
  tempCount:=0;
  for I := Low(aArrays) to High(aArrays) do
  begin
    if abstrinarray(aArrays[i],aDeleteArrays)<0 then
    begin
      tempCount:=tempCount+1;
      setlength(result, tempCount);
      result[tempCount-1]:=aArrays[i];
    end;
  end;
end;

function ABStrToBooleanArray  (aStr: string;aSpaceSign: string; aSetCount:LongInt; aDefaultValue:Boolean):TBooleanDynArray;
var
  i, tempStrItemCount,tempResultItemCount: LongInt;
  tempValue:Boolean;
begin
  tempStrItemCount := ABGetSpaceStrCount(aStr, aSpaceSign);
  tempResultItemCount:=max(tempStrItemCount,aSetCount);
  setlength(result, tempResultItemCount);
  for I := 1 to tempResultItemCount do
  begin
    tempValue:=aDefaultValue;
    if i<=tempStrItemCount then
      tempValue:= ABStrToBool(ABGetSpaceStr(aStr, i, aSpaceSign));

    result[i - 1] := tempValue;
  end;
end;

function ABStrToStringArray  (aStr: string;aSpaceSign: string; aSetCount:LongInt; aDefaultValue:string  ):TStringDynArray;
var
  i, tempStrItemCount,tempResultItemCount: LongInt;
  tempValue:string;
begin
  tempStrItemCount := ABGetSpaceStrCount(aStr, aSpaceSign);
  tempResultItemCount:=max(tempStrItemCount,aSetCount);
  setlength(result, tempResultItemCount);
  for I := 1 to tempResultItemCount do
  begin
    tempValue:=aDefaultValue;
    if i<=tempStrItemCount then
      tempValue:= ABGetSpaceStr(aStr, i, aSpaceSign);

    result[i - 1] := tempValue;
  end;
end;

function ABStrToAnsiCharArray(aStr: string;aSpaceSign: string; aSetCount:LongInt; aDefaultValue:AnsiChar):TABAnsiCharDynArray;
var
  i, tempStrItemCount,tempResultItemCount: LongInt;
  tempStr:string;
  tempValue:AnsiChar;
begin
  tempStrItemCount := ABGetSpaceStrCount(aStr, aSpaceSign);
  tempResultItemCount:=max(tempStrItemCount,aSetCount);
  setlength(result, tempResultItemCount);
  for I := 1 to tempResultItemCount do
  begin
    tempValue:=aDefaultValue;
    if i<=tempStrItemCount then
    begin
      tempStr:= ABGetSpaceStr(aStr, i, aSpaceSign);
      if (tempStr<>EmptyStr) then
        tempValue:=AnsiChar(tempStr[1])
    end;

    result[i - 1] := tempValue;
  end;
end;

function ABStrToWideCharArray(aStr: string;aSpaceSign: string; aSetCount:LongInt; aDefaultValue:WideChar):TABWideCharDynArray;
var
  i, tempStrItemCount,tempResultItemCount: LongInt;
  tempStr:string;
  tempValue:WideChar;
begin
  tempStrItemCount := ABGetSpaceStrCount(aStr, aSpaceSign);
  tempResultItemCount:=max(tempStrItemCount,aSetCount);
  setlength(result, tempResultItemCount);
  for I := 1 to tempResultItemCount do
  begin
    tempValue:=aDefaultValue;
    if i<=tempStrItemCount then
    begin
      tempStr:= ABGetSpaceStr(aStr, i, aSpaceSign);
      if (tempStr<>EmptyStr) then
        tempValue:=WideChar(tempStr[1])
    end;

    result[i - 1] := tempValue;
  end;
end;

function ABStrToIntegerArray (aStr: string;aSpaceSign: string; aSetCount:LongInt; aDefaultValue:Integer ):TIntegerDynArray;
var
  i, tempStrItemCount,tempResultItemCount: LongInt;
  tempStr:string;
  tempValue:Integer;
begin
  tempStrItemCount := ABGetSpaceStrCount(aStr, aSpaceSign);
  tempResultItemCount:=max(tempStrItemCount,aSetCount);
  setlength(result, tempResultItemCount);
  for I := 1 to tempResultItemCount do
  begin
    tempValue:=aDefaultValue;
    if i<=tempStrItemCount then
    begin
      tempStr:= ABGetSpaceStr(aStr, i, aSpaceSign);
      if (tempStr<>EmptyStr) then
        tempValue:=StrToInt(tempStr)
    end;

    result[i - 1] := tempValue;
  end;
end;

function ABStrToByteArray    (aStr: string;aSpaceSign: string; aSetCount:LongInt; aDefaultValue:Byte    ):TByteDynArray;
var
  i, tempStrItemCount,tempResultItemCount: LongInt;
  tempStr:string;
  tempValue:Byte;
begin
  tempStrItemCount := ABGetSpaceStrCount(aStr, aSpaceSign);
  tempResultItemCount:=max(tempStrItemCount,aSetCount);
  setlength(result, tempResultItemCount);
  for I := 1 to tempResultItemCount do
  begin
    tempValue:=aDefaultValue;
    if i<=tempStrItemCount then
    begin
      tempStr:= ABGetSpaceStr(aStr, i, aSpaceSign);
      if (tempStr<>EmptyStr) then
        tempValue:=StrToInt(tempStr)
    end;

    result[i - 1] := tempValue;
  end;
end;

function ABStrToDoubleArray  (aStr: string;aSpaceSign: string; aSetCount:LongInt; aDefaultValue:Double):TDoubleDynArray;
var
  i, tempStrItemCount,tempResultItemCount: LongInt;
  tempStr:string;
  tempValue:Double;
begin
  tempStrItemCount := ABGetSpaceStrCount(aStr, aSpaceSign);
  tempResultItemCount:=max(tempStrItemCount,aSetCount);
  setlength(result, tempResultItemCount);
  for I := 1 to tempResultItemCount do
  begin
    tempValue:=aDefaultValue;
    if i<=tempStrItemCount then
    begin
      tempStr:= ABGetSpaceStr(aStr, i, aSpaceSign);
      if (tempStr<>EmptyStr) then
        tempValue:=StrToFloat(tempStr)
    end;

    result[i - 1] := tempValue;
  end;
end;

function ABVariantInArray(aValue: Variant; aVariants: array of Variant): Boolean;
var
  i: LongInt;
begin
  Result := False;
  for I := Low(aVariants) to High(aVariants) do
  begin
    if (aValue = aVariants[i]) then
    begin
      Result := true;
      Break;
    end;
  end;
end;

function ABStrInArray(aStr: string; aStrArray: array of string; aOptions:TLocateOptions): LongInt;
var
  i: LongInt;
begin
  Result := -1;
  for I := Low(aStrArray) to High(aStrArray) do
  begin
    if (aStr = aStrArray[i]) or
      ((aOptions = [loPartialKey, loCaseInsensitive]) and (ABPos(aStr, aStrArray[i])> 0)) or
      (([loPartialKey] = aOptions) and (Pos(aStr, aStrArray[i]) > 0)) or
      (([loCaseInsensitive] = aOptions) and (AnsiCompareText(aStr, aStrArray[i]) = 0)) then
    begin
      Result := i;
      Break;
    end;
  end;
end;

function ABArrayInStr(aStrArray: array of string; aStr: string; Options:
  TLocateOptions): Boolean;
var
  i: LongInt;
begin
  Result := False;
  for I := Low(aStrArray) to High(aStrArray) do
  begin
    if (aStrArray[i]=aStr) or
      ((Options = [loPartialKey, loCaseInsensitive]) and (ABPos(aStrArray[i], aStr)> 0)) or
      (([loPartialKey] = Options) and (Pos(aStrArray[i], aStr) > 0)) or
      (([loCaseInsensitive] = Options) and (AnsiCompareText(aStr, aStrArray[i]) = 0)) then
    begin
      Result := true;
      Break;
    end;
  end;
end;

function ABStrsInArray(aStrs: string; aSpaceSign:string; aStrArray: array of string; aOptions:TLocateOptions ): Boolean;
var
  i, j: LongInt;
  tempstr: string;
begin
  Result := true;

  j := ABGetSpaceStrCount(aStrs, aSpaceSign);
  for I := 1 to j do
  begin
    tempstr := ABGetSpaceStr(aStrs, i, aSpaceSign);
    if ABStrInArray(tempstr, aStrArray,aOptions)<0 then
    begin
      Result := False;
      Break;
    end;
  end;
end;

function ABIsMoreLineProperty(aPropertyName:string):Boolean;
begin
  result:=(abpos(','+aPropertyName+',',','+'Items.Text,lines.Text,properties.Items.Text'+',')>0);
end;

function ABGetParent(aControl: TControl; aClassNames: string): TObject;
var
  tempControl: TControl;
begin
  tempControl := aControl.Parent;
  while (Assigned(tempControl)) and
        (not ABObjectInheritFrom(tempControl,aClassNames)) do
  begin
    tempControl := tempControl.Parent;
  end;
  result := tempControl;
end;

function ABObjectInheritFrom(aObject: TObject; aClassNames: string):boolean;
var
  tempClass: TClass;
begin
  result:=false;
  if Assigned(aObject) then
  begin
    tempClass := aObject.ClassType;

    while True  do
    begin
      if (ABPos(','+tempClass.ClassName + ',', ','+aClassNames+',')> 0) then
      begin
        result := true;
        break;
      end
      else
      begin
        tempClass := tempClass.ClassParent;
        if not Assigned(tempClass) then
          break;
      end;
    end;
  end;
end;

function ABGetOwner(aComponent: TComponent; aClassNames: string):TComponent;
var
  tempComponent: TComponent;
begin
  tempComponent := aComponent.Owner;
  while (Assigned(tempComponent)) and
        (not ABObjectInheritFrom(tempComponent,aClassNames)) do
  begin
    tempComponent := tempComponent.Owner;
  end;
  result := tempComponent;
end;

function ABOwnerCheck(aSender: TControl; aCheckOnwer: TComponent): Boolean;
var
  W: TComponent;
begin
  Result := False;
  W := aSender.Owner;
  while W <> nil do
  begin
    if W = aCheckOnwer then
    begin
      Result := True;
      Exit;
    end;
    W := W.Owner;
  end;
end;

function ABParentCheck(aSender: TControl; aCheckParent: TComponent): Boolean;
var
  W: TControl;
begin
  Result := False;
  W := aSender.Parent;
  while W <> nil do
  begin
    if W = aCheckParent then
    begin
      Result := True;
      Exit;
    end;
    W := W.Parent;
  end;
end;

function ABGetOwnerLongName(aComponent: TComponent;aSpaceFlag:string):string;
begin
  Result:=aComponent.Name;
  while (Assigned(aComponent.Owner))  do
  begin
    aComponent:=aComponent.Owner;
    if (aComponent is TApplication) then
      break;

    result:=aComponent.Name+aSpaceFlag+result;
  end;
end;

function ABGetParentForm(aControl: TControl): TForm;
begin
  Result := TForm(GetParentForm(aControl));
end;

function ABGetMouseControl: TWinControl;
var
  CurSorPos: TPoint;
begin
  GetCursorPos(CurSorPos);
  Result := FindVCLWindow(CurSorPos);
end;

function ABGetMouseForm: Tform;
var
  CurSorPos: TPoint;
begin
  GetCursorPos(CurSorPos);
  Result :=ABGetCoordinateForm(CurSorPos.X,CurSorPos.Y)
end;

function ABGetCoordinateForm(aX, aY: integer): Tform;
var
  P: TPoint;
  W: TWinControl;
begin
  P.X := aX;
  P.Y := aY;
  W := FindVCLWindow(P); //得到鼠标指针下的VCL可视组件
  if Assigned(W) then
  begin
    while Assigned(w.Parent) do
      //当W的上级Parent不为空时就继续往上找
      w := w.Parent;
    Result := Tform(W); //最后返回窗体的名称Name
  end
  else
  begin
    Result := nil;
  end;
end;

procedure ABMoveMouseToControl(AWinControl: TControl);
var
  rtControl: TRect;
begin
  rtControl := AWinControl.BoundsRect;
  MapWindowPoints(AWinControl.Parent.Handle, 0, rtControl, 2);
  SetCursorPos(rtControl.Left + (rtControl.Right - rtControl.Left) div 2,
    rtControl.Top + (rtControl.Bottom - rtControl.Top) div 2);
end;

function ABComponentToDFMProperty(aComponent: TComponent): string;
var
  BinStream: TMemoryStream;
  StrStream: TStringStream;
  s: string;
begin
  BinStream := TMemoryStream.Create;
  try
    StrStream := TStringStream.Create(s);
    try
      BinStream.WriteComponent(aComponent);
      BinStream.Seek(0, soFromBeginning);
      ObjectBinaryToText(BinStream, StrStream);
      StrStream.Seek(0, soFromBeginning);

      Result := StrStream.DataString;
    finally
      StrStream.Free;
    end;
  finally
    BinStream.Free
  end;
end;

function ABDFMPropertyToComponent(aDFMString: string;aComponent: TComponent): TComponent;
var
  StrStream: TStringStream;
  BinStream: TMemoryStream;
begin
  StrStream := TStringStream.Create(aDFMString);
  try
    BinStream := TMemoryStream.Create;
    try
      ObjectTextToBinary(StrStream, BinStream);
      BinStream.Seek(0, soFromBeginning);

      Result := BinStream.ReadComponent(aComponent);
    finally
      BinStream.Free;
    end;
  finally
    StrStream.Free;
  end;
end;

function ABDFMControlDelete(aStrings:TStrings;aSpaceStr:string;aBegStr:string;aEndStr:string):boolean;
var
  i:LongInt;
begin
  result:=false;
  i:=aStrings.IndexOf(aSpaceStr+aBegStr);
  while i>=0 do
  begin
    aStrings.Delete(i);
    if (i<=aStrings.Count-1) and
       (AnsiCompareText(aStrings[i],aSpaceStr+aEndStr)=0) then
    begin
      aStrings.Delete(i);
      result:=True;
      Break;
    end;
  end;
end;

function ABDFMControlCopy(aStrings:TStrings;aSpaceStr:string;aBegStr:string;aEndStr:string):string;
var
  i,j:LongInt;
  tempStrings:TStrings;
begin
  result:=EmptyStr;
  tempStrings:=TStringList.Create;
  try
    i:=aStrings.IndexOf(aSpaceStr+aBegStr);
    if (i>=0) then
    begin
      for j := i to aStrings.Count-1 do
      begin
        tempStrings.Add(copy(aStrings[j],length(aSpaceStr)+1,maxint));
        if AnsiCompareText(aStrings[j],aSpaceStr+aEndStr)=0 then
          Break;
      end;

      result:=Trim(tempStrings.Text);
    end;
  finally
    tempStrings.Free;
  end;
end;

function ABWinControlShowError(aSender: TWinControl;aIsError:Boolean;aErrorStr: string): boolean;
begin
  result := False;
  if aIsError then
  begin
    if aErrorStr<>EmptyStr then
      ShowMessage(aErrorStr);

    if Assigned(aSender) then
    begin
      if aSender.CanFocus then
        aSender.SetFocus;
    end;
    result := true;
  end;
end;

procedure ABSetControlEnabled(aComponents:array of TComponent;aEnabled:Boolean);
var
  I: longint;
begin
  for I := Low(aComponents) to High(aComponents) do
  begin
    ABSetControlEnabled(aComponents[i],aEnabled);
  end;
end;

procedure ABSetControlEnabled(aComponent:TComponent;aEnabled:Boolean);
var
  I: longint;
begin
  if aComponent is TwinControl then
  begin
    TwinControl(aComponent).Enabled:=aEnabled;
    for I := 0 to TwinControl(aComponent).ControlCount-1 do
    begin
      ABSetControlEnabled(TwinControl(aComponent).Controls[i],aEnabled);
    end;
  end
  else if aComponent is TPopupMenu then
  begin
    for i :=0 to TPopupMenu(aComponent).Items.Count - 1 do
    begin
      ABSetControlEnabled(TPopupMenu(aComponent).Items[i],aEnabled);
    end;
  end
  else if aComponent is TMenuItem then
  begin
    TMenuItem(aComponent).Enabled:=aEnabled;
    for i :=0 to TMenuItem(aComponent).Count - 1 do
    begin
      ABSetControlEnabled(TMenuItem(aComponent).Items[i],aEnabled);
    end;
  end;
end;

procedure ABSetControlVisible(aComponents:array of TComponent;aVisible:Boolean);
var
  I: longint;
begin
  for I := Low(aComponents) to High(aComponents) do
  begin
    ABSetControlVisible(aComponents[i],aVisible);
  end;
end;

procedure ABSetControlVisible(aComponent:TComponent;aVisible:Boolean);
var
  I: longint;
begin
  if aComponent is TwinControl then
  begin
    TwinControl(aComponent).Visible:=aVisible;
    for I := 0 to TwinControl(aComponent).ControlCount-1 do
    begin
      ABSetControlVisible(TwinControl(aComponent).Controls[i],aVisible);
    end;
  end
  else if aComponent is TPopupMenu then
  begin
    for i :=0 to TPopupMenu(aComponent).Items.Count - 1 do
    begin
      ABSetControlVisible(TPopupMenu(aComponent).Items[i],aVisible);
    end;
  end
  else if aComponent is TMenuItem then
  begin
    TMenuItem(aComponent).Visible:=aVisible;
    for i :=0 to TMenuItem(aComponent).Count - 1 do
    begin
      ABSetControlVisible(TMenuItem(aComponent).Items[i],aVisible);
    end;
  end;
end;

function ABGetMemoCursor(aEdit: TCustomEdit): TPoint;
begin
  Result.y := SendMessage(aEdit.Handle, EM_LINEFROMCHAR,aEdit.SelStart, 0) + 1;
  Result.x := aEdit.SelStart - SendMessage(aEdit.Handle, EM_LINEINDEX,Result.y - 1, 0) + 1;
end;

function ABGetMemoRow(aControl: TControl): longint; //integer;
begin
  result := aControl.Perform(em_LineFromChar, 0, 0) + 1;
end;

function ABGetFileVersion(aFileName: string): string;
var
  InfoSize, Wnd: DWORD;
  VerBuf: Pointer;
  VerInfo: ^VS_FIXEDFILEINFO;
begin
  Result := EmptyStr;
  InfoSize := GetFileVersionInfoSize(PChar(aFileName), Wnd);
  if InfoSize <> 0 then
  begin
    GetMem(VerBuf, InfoSize);
    try
      if GetFileVersionInfo(PChar(aFileName), Wnd, InfoSize, VerBuf) then
      begin
        VerInfo := nil;
        VerQueryValue(VerBuf, '\', Pointer(VerInfo), Wnd);
        if Assigned(VerInfo) then
          Result := Format('%d.%d.%d.%d', [VerInfo^.dwFileVersionMS shr 16,
            VerInfo^.dwFileVersionMS and $0000FFFF,
              VerInfo^.dwFileVersionLS shr 16,
              VerInfo^.dwFileVersionLS and $0000FFFF]);
      end;
    finally
      FreeMem(VerBuf, InfoSize);
    end;
  end;
end;

function ABGetShortFileName(const aFileName: string): string;
var
  X: Integer;
  FileHandle:integer;
begin
  if (not ABCheckBeginStr(aFileName,'$')) and
     (not ABCheckFileExists(aFileName)) then
  begin
    if not ABCheckDirExists(ABGetFiledir(aFileName)) then
      ABCreateDir(ABGetFiledir(aFileName));

    FileHandle:=0;
    try
      FileHandle:=FileCreate(aFileName);
    finally
      fileclose(FileHandle);
    end;
  end;

  SetLength(Result, MAX_PATH);
  X := GetShortPathName(PChar(aFileName), PChar(Result), MAX_PATH);
  SetLength(Result, X);
end;

function ABGetShortPath(const aPath: string): string;
var
  X: Integer;
begin
  if (not ABCheckBeginStr(aPath,'$')) and
     (not ABCheckDirExists(ABGetdir(aPath))) then
    ABCreateDir(ABGetdir(aPath));

  SetLength(Result, MAX_PATH);
  X := GetShortPathName(PChar(aPath), PChar(Result), MAX_PATH);
  SetLength(Result, X);
end;

function ABSaveFile(aDefaultDirOrPath: string; aDefaultFile: string; aFilter: string;aOwner:TComponent):
  string;
var
  SaveDialog1: TSaveDialog;
begin
  Result := EmptyStr;
  SaveDialog1 := TSaveDialog.Create(aOwner);
  try
    if ABCheckDirExists(aDefaultDirOrPath) then
      SaveDialog1.InitialDir := aDefaultDirOrPath;

    if aDefaultFile<>EmptyStr then
      SaveDialog1.FileName := aDefaultFile;

    SaveDialog1.Filter := aFilter;
    if (SaveDialog1.Execute) then
    begin
      result := ABGetSaveDialogFileName(SaveDialog1);
    end;
  finally
    SaveDialog1.Free;
  end;
end;

function ABGetSaveDialogFileName(aSaveDialog:TSaveDialog): string;
begin
  result := aSaveDialog.FileName;
  if (result<>emptystr) and (ABGetFileExt(result)=emptystr) then
  begin
    result:=result+ABStringReplace(ABGetSpaceStr(aSaveDialog.Filter,2*aSaveDialog.FilterIndex,'|'),'*','');
  end;
end;

function ABCheckDirExists(aDirOrPath: string): boolean;
begin
  if trim(aDirOrPath)=EmptyStr then
    result := false
  else
    result := SysUtils.DirectoryExists(aDirOrPath);
end;

function ABGetFileFullName(aFileName: string): string;
begin
  result := expandfilename(aFileName);
end;

function ABGetFiledir(aFileName: string): string;
begin
  result := extractfiledir(aFileName);
end;

function ABGetFileExt(aFileName: string; aIsDelDian: boolean): string;
begin
  if aIsDelDian then
    result := Copy(extractfileext(aFileName), 2, maxint)
  else
    result := extractfileext(aFileName)
end;

function ABGetFilename(aFileName: string; aIsDelExt: boolean): string;
begin
  Result := aFileName;
  if Result = EmptyStr then
    exit;

  if aIsDelExt then
    Result := ChangeFileExt(Result, '');
  result := extractfilename(Result);
end;

function ABGetFilepath(aFileName: string): string;
begin
  result := extractfilepath(aFileName);
end;

function ABGetURLFileName(aURL: string): string;
var
  i: integer;
  s: string;
begin
  s := aURL;
  i := Pos('/', s);
  while i <> 0 do //去掉"/"前面的内容剩下的就是文件名了
  begin
    Delete(s, 1, i);
    i := Pos('/', s);
  end;
  Result := s;
end;

function ABCheckFileExists(aFileName: string): boolean;
begin
  if trim(aFileName)=EmptyStr then
    result := false
  else
    result := FileExists(aFileName);
end;

//LastDelimiter 即从右到左扫描S,返回第一个在Delimiters
function ABGetParentDir(aDirOrPath: string): string;
var
  i: LongInt;
begin
  aDirOrPath := ABGetdir(aDirOrPath);
  Result :=aDirOrPath;

  i := LastDelimiter('\', aDirOrPath);
  if i > 0 then
  begin
    Result := Copy(aDirOrPath, 1, i - 1);
  end;
end;

function ABGetParentPath(aDirOrPath: string): string;
var
  i: LongInt;
begin
  aDirOrPath := ABGetdir(aDirOrPath);

  i := LastDelimiter('\', aDirOrPath);
  if i > 0 then
  begin
    Result := Copy(aDirOrPath, 1, i);
  end
  else
    Result := aDirOrPath+'\';
end;

function ABGetLastDirName(aDirOrPath: string): string;
var
  i: LongInt;
begin
  aDirOrPath := ABGetdir(aDirOrPath);

  i := LastDelimiter('\', aDirOrPath);
  if i > 0 then
  begin
    Result := Copy(aDirOrPath, i+1,length(aDirOrPath));
  end;
end;

procedure ABCreateDir(aDirOrPath: string);
begin
  SysUtils.ForceDirectories(aDirOrPath);
end;

procedure ABCreateEmptystrDir(aDirOrPath: string);
begin
  if not DirectoryExists(aDirOrPath) then
    ABCreateDir(aDirOrPath);
  ABDeltree(aDirOrPath,false);
end;

function ABGetdir(aDirOrPath: string): string;
begin
  aDirOrPath := Trim(aDirOrPath);
  if Copy(aDirOrPath, Length(aDirOrPath), 1) = '\' then
  begin
    aDirOrPath := Copy(aDirOrPath, 1, Length(aDirOrPath) - 1);
  end;
  Result := aDirOrPath;
end;

function ABGetpath(aDirOrPath: string): string;
begin
  aDirOrPath := Trim(aDirOrPath);
  if (aDirOrPath<>EmptyStr) and
     (Copy(aDirOrPath, Length(aDirOrPath), 1) <> '\') then
  begin
    aDirOrPath := aDirOrPath + '\';
  end;
  Result := aDirOrPath;
end;

function ABGetDir(aBaseDirOrPath,aDianDianDirOrPath: string): string;
begin
  aBaseDirOrPath:=ABGetpath(aBaseDirOrPath);
  aDianDianDirOrPath:=ABGetDir(aDianDianDirOrPath);
  while copy(aDianDianDirOrPath, 1, 2) = '..' do
  begin
    aBaseDirOrPath := ABGetParentPath(aBaseDirOrPath);
    aDianDianDirOrPath:=copy(aDianDianDirOrPath, 4,MaxInt)
  end;
  Result :=  aBaseDirOrPath+ aDianDianDirOrPath;
end;

function ABGetpath(aBaseDirOrPath,aDianDianDirOrPath: string): string;
begin
  aBaseDirOrPath:=ABGetpath(aBaseDirOrPath);
  aDianDianDirOrPath:=trim(aDianDianDirOrPath);
  while copy(aDianDianDirOrPath, 1, 2) = '..' do
  begin
    aBaseDirOrPath := ABGetParentPath(aBaseDirOrPath);
    aDianDianDirOrPath:=copy(aDianDianDirOrPath, 4,MaxInt)
  end;
  Result :=  ABGetFilePath(aBaseDirOrPath+ aDianDianDirOrPath);
end;

function ABSelectDirectory(aDirOrPath: string; aTitleCaption: string): string;
begin
  Result :=EmptyStr;
  if aTitleCaption = EmptyStr then
    aTitleCaption := '选择文件夹';

  if not ABCheckDirExists(aDirOrPath) then
    aDirOrPath:=emptystr;

  IF SelectDirectory(aTitleCaption, '', aDirOrPath) then
    Result := aDirOrPath
end;

function ABSelectFile(aDefaultDir: string; aDefaultFile: string; aFilter:
  string;aMultiSelect:Boolean;aMultiFiles:Tstrings;aOwner:TComponent): string;
var
  OpenDialog1: TOpenDialog;
begin
  Result := EmptyStr;
  OpenDialog1 := TOpenDialog.Create(aOwner);
  try
    if aMultiSelect then
      OpenDialog1.Options:=OpenDialog1.Options+[ofAllowMultiSelect]
    else
      OpenDialog1.Options:=OpenDialog1.Options-[ofAllowMultiSelect];

    if ABCheckDirExists(aDefaultDir) then
      OpenDialog1.InitialDir := aDefaultDir;

    if ABCheckFileExists(aDefaultFile) then
      OpenDialog1.FileName := aDefaultFile;

    OpenDialog1.Filter := aFilter;

    if (OpenDialog1.Execute) then
    begin
      result := OpenDialog1.FileName;
      if aMultiSelect then
        aMultiFiles.Assign(OpenDialog1.Files);
    end;
  finally
    OpenDialog1.Free;
  end;
end;

function ABMoveDir(const aSDirOrPath, aDDirOrPath: string;aBeforeMoveDelDDirOrPath:boolean): Boolean;
begin
  Result := false;
  if (not ABCheckDirExists(aSDirOrPath)) then
    Exit;

  if aBeforeMoveDelDDirOrPath then
    ABDeltree(aDDirOrPath);

  ABCopyDir(aSDirOrPath,aDDirOrPath,'*.*',aBeforeMoveDelDDirOrPath);
  ABDeltree(aSDirOrPath);
  Result :=true;
end;

function ABEditDir(aOldDirOrPath, aNewDirOrPath: string): Boolean;
begin
  Result := false;
  if (ABCheckDirExists(aNewDirOrPath)) then
    Exit;

  RenameFile(PChar(aOldDirOrPath),PChar(aNewDirOrPath));
end;

function ABMoveFile(aSFileName, aTFileName: string): Boolean;
begin
  Result := false;
  if (not ABCheckFileExists(aSFileName)) then
    Exit;

  ABDeleteFile(aTFileName);
  Result :=MoveFile(PChar(aSFileName),PChar(aTFileName));
end;

function ABGetFileAttr(aFileName: string): LongInt;
begin
  Result := -1;
  if ABCheckFileExists(aFileName) then
    Result := FileGetAttr(aFileName);
end;

procedure ABSetFileAttr(aFileName: string; aArrt: LongInt; aType:TABFileOperateType);
var
  OldAttr: Integer;
begin
  case aType of
    ftInsert:
    begin
      OldAttr := FileGetAttr(aFileName);
      FileSetAttr(aFileName, OldAttr or aArrt);
    end;
    ftDelete:
    begin
      OldAttr := FileGetAttr(aFileName);
      FileSetAttr(aFileName, OldAttr and (not aArrt));
    end;
    ftEdit:
    begin
      FileSetAttr(aFileName, aArrt or faArchive);
    end;
  end;
end;

procedure ABShowEditFileProperties(const aFName: string);
var
  SEI: SHELLEXECUTEINFO;
begin
  SEI.cbSize := SizeOf(SEI);
  SEI.fMask := SEE_MASK_NOCLOSEPROCESS or SEE_MASK_INVOKEIDLIST or
    SEE_MASK_FLAG_NO_UI;
  SEI.Wnd := Application.Handle;
  SEI.lpVerb := 'properties';
  SEI.lpFile := PChar(aFName);
  SEI.lpParameters := nil;
  SEI.lpDirectory := nil;
  SEI.nShow := 0;
  SEI.hInstApp := 0;
  SEI.lpIDList := nil;
  ShellExecuteEx(@SEI);
end;

function ABFileInUse(aFName: string): Boolean;
var
  HFileRes: HFILE;
begin
  Result := False;
  if not ABCheckFileExists(aFName) then
    Exit;

  try
    HFileRes := CreateFile(PChar(aFName), GENERIC_READ or GENERIC_WRITE, 0,
      nil, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
    Result := (HFileRes = INVALID_HANDLE_VALUE);
    if not Result then
      CloseHandle(HFileRes);
  except
  end;
end;

function ABGetFileSize(aFileName: string): int64;
var
  SearchRec: TSearchRec;
begin
  if FindFirst(ExpandFileName(aFileName), faAnyFile, SearchRec) = 0 then
    Result := SearchRec.Size
  else
    Result := -1;
end;

procedure ABSetCurrentDir(aDirOrPath: string);
begin
  SetCurrentDir(aDirOrPath);
end;

function ABGetCurrentDir: string;
begin
  result:= GetCurrentDir;
end;

function ABGetFileSizeStr(aFileSize: Int64;aDivBCount:LongInt): string;
begin
  aFileSize:=aFileSize*aDivBCount;

  if aFileSize >= 1024 * 1024 * 1024 then //超过G
    Result := Format('%0.1f', [aFileSize / (1024 * 1024 * 1024)]) + ' GB'
  else if aFileSize >= 1024 * 1024 then //超过M
    Result := Format('%0.1f', [aFileSize / (1024 * 1024)]) + ' MB'
  else if aFileSize >= 1024 then //超过K
    Result := Format('%0.1f', [aFileSize / 1024]) + ' KB'
  else
    Result := IntToStr(aFileSize) + ' B';
end;

function ABSetFileDateTime(aFileName: string; aCreationTime, aLastWriteTime,aLastAccessTime:TDateTime): Boolean;
var
  FileHandle: Integer;
  tempCreationTime, tempLastAccessTime, tempLastWriteTime:TFileTime;
begin
  FileHandle := FileOpen(aFileName, fmOpenWrite or fmShareDenyNone);
  if FileHandle > 0 then
  begin
    tempCreationTime:=ABDateTimeToFileTime(aCreationTime);
    tempLastWriteTime:=ABDateTimeToFileTime(aLastWriteTime);
    tempLastAccessTime:=ABDateTimeToFileTime(aLastAccessTime);

    SetFileTime(FileHandle, @tempCreationTime, @tempLastAccessTime, @tempLastWriteTime);

    FileClose(FileHandle);
    Result := True;
  end
  else
    Result := False;
end;

Function ABReadOrEditFileAllDatetime(aFileName:string;aSetDatetime:TDateTime):String;
Const
  Numbers9 : Set of Char = ['0'..'9'];
  Numbers1 : Set of Char = ['0'..'1'];
  Numbers3 : Set of Char = ['0'..'3'];
  Numbers5 : Set of Char = ['0'..'5'];
  MaxItems = $9FF;
var
  MemStream: TMemoryStream;
  tempSetDatetime: ansiString;

  s      : String;
  i,j    : Integer;
  a      : Array of ansiChar;
Begin
  Result:=EmptyStr;
  MemStream := TMemoryStream.Create;
  try
    MemStream.LoadFromFile(aFileName);
    SetLength(a,MemStream.Size);
    MemStream.Read(a[0],MemStream.Size);

    s := a[6] + a[7] + a[8] + a[9];
    if ABStrInArray(ansistring(ANSIUppercase(s)),['JFIF','EXIF'])<0 then
      Exit;

    if S = '' then exit;
    For I := 10 to MaxItems - 15 do
    Begin
      If a[i] = ':' then
      Begin
        s := '';
        For j := 0 to 18 do
          s := s + a[i + j - 4];

        If(s[01] in Numbers9) and  (s[02] in Numbers9) and
          (s[03] in Numbers9) and  (s[04] in Numbers9) and
          (s[05] = ':')       and
          (s[06] in Numbers1) and  (s[07] in Numbers9) and
          (s[08] = ':')       and
          (s[09] in Numbers3) and  (s[10] in Numbers9) and
          (s[11] = ' ')       and
          (s[12] in Numbers5) and  (s[13] in Numbers9) and
          (s[14] = ':')       and
          (s[15] in Numbers5) and  (s[16] in Numbers9) and
          (s[17] = ':')       and
          (s[18] in Numbers5) and  (s[19] in Numbers9) then
        begin
          If s<> '0000:00:00 00:00:00' then
          begin
            Result := s;
          end;
          Break;
        End;
      End;
    End;

    if (Result<>EmptyStr) and
       (aSetDatetime>0) then
    begin
      tempSetDatetime:=ABDateTimeToStr(aSetDatetime,23,':');
      for I := 0 to MemStream.Size-1 do
      begin
        if (ansistring(a[i+0 ])=Result[1 ]) and
           (ansistring(a[i+1 ])=Result[2 ]) and
           (ansistring(a[i+2 ])=Result[3 ]) and
           (ansistring(a[i+3 ])=Result[4 ]) and
           (ansistring(a[i+4 ])=Result[5 ]) and
           (ansistring(a[i+5 ])=Result[6 ]) and
           (ansistring(a[i+6 ])=Result[7 ]) and
           (ansistring(a[i+7 ])=Result[8 ]) and
           (ansistring(a[i+8 ])=Result[9 ]) and
           (ansistring(a[i+9 ])=Result[10]) and
           (ansistring(a[i+10])=Result[11]) and
           (ansistring(a[i+11])=Result[12]) and
           (ansistring(a[i+12])=Result[13]) and
           (ansistring(a[i+13])=Result[14]) and
           (ansistring(a[i+14])=Result[15]) and
           (ansistring(a[i+15])=Result[16]) and
           (ansistring(a[i+16])=Result[17]) and
           (ansistring(a[i+17])=Result[18]) and
           (ansistring(a[i+18])=Result[19]) then
        begin
          a[i+0 ]:=tempSetDatetime[1 ] ;
          a[i+1 ]:=tempSetDatetime[2 ] ;
          a[i+2 ]:=tempSetDatetime[3 ] ;
          a[i+3 ]:=tempSetDatetime[4 ] ;
          a[i+4 ]:=tempSetDatetime[5 ] ;
          a[i+5 ]:=tempSetDatetime[6 ] ;
          a[i+6 ]:=tempSetDatetime[7 ] ;
          a[i+7 ]:=tempSetDatetime[8 ] ;
          a[i+8 ]:=tempSetDatetime[9 ] ;
          a[i+9 ]:=tempSetDatetime[10] ;
          a[i+10]:=tempSetDatetime[11] ;
          a[i+11]:=tempSetDatetime[12] ;
          a[i+12]:=tempSetDatetime[13] ;
          a[i+13]:=tempSetDatetime[14] ;
          a[i+14]:=tempSetDatetime[15] ;
          a[i+15]:=tempSetDatetime[16] ;
          a[i+16]:=tempSetDatetime[17] ;
          a[i+17]:=tempSetDatetime[18] ;
          a[i+18]:=tempSetDatetime[19] ;
        end;
      end;
      MemStream.Position := 0;
      MemStream.Write(a[0],MemStream.Size);
      MemStream.SaveToFile(aFileName);
    end;
  finally
    MemStream.Free;
  end;
End;

function ABSetFileDateTime(aFileName: string; aType: TABFileTimeType; aDateTime:TDateTime): Boolean;
var
  Handle: THandle;
  LocalFileTime, FileTime: TFileTime;
  DosDateTime: Integer;
  I: TABFileTimeType;
  FileTimes: array[TABFileTimeType] of Pointer;
begin
  Result := False;
  DosDateTime := DateTimeToFileDate(aDateTime);
  Handle := FileOpen(aFileName, fmOpenWrite or fmShareDenyNone);
  if Handle <> INVALID_HANDLE_VALUE then
  try
    for I := fttCreation to fttLastWrite do
      FileTimes[I] := nil;

    DosDateTimeToFileTime(LongRec(DosDateTime).Hi, LongRec(DosDateTime).Lo,LocalFileTime);
    LocalFileTimeToFileTime(LocalFileTime, FileTime);
    FileTimes[aType] := @FileTime;
    SetFileTime(Handle, FileTimes[fttCreation], FileTimes[fttLastAccess],FileTimes[fttLastWrite]);

    Result := true;
  finally
    FileClose(Handle);
  end;
end;

function ABGetFileDateTime(aFileName: string; var aCreationTime,aLastWriteTime,aLastAccessTime:TDateTime): Boolean;
var
  FileHandle: Integer;
  tempCreationTime, tempLastAccessTime, tempLastWriteTime:TFileTime;
begin
  FileHandle := FileOpen(aFileName, fmOpenRead or fmShareDenyNone);
  if FileHandle > 0 then
  begin
    GetFileTime(FileHandle, @tempCreationTime, @tempLastAccessTime, @tempLastWriteTime);

    aCreationTime:=ABFileTimeToDateTime(tempCreationTime);
    aLastWriteTime:=ABFileTimeToDateTime(tempLastWriteTime);
    aLastAccessTime:=ABFileTimeToDateTime(tempLastAccessTime);
    FileClose(FileHandle);

    Result := True;
  end
  else
    Result := False;
end;

function ABGetFileDateTime(aFileName: string; aType: TABFileTimeType): TDateTime;
var
  a1, a2, a3: TDateTime;
begin
  result:=0;
  ABGetFileDateTime(aFileName,  a1,a2, a3);
  if aType = fttCreation then
    result := a1
  else if aType =fttLastWrite  then
    result := a2
  else if aType =fttLastAccess  then
    result := a3;
end;

function ABGetFileIcon(aFileName: string; var aIcon: TIcon): Boolean;
var
  SHFileInfo: TSHFileInfo;
  h: HWND;
begin
  h := SHGetFileInfo(PChar(aFileName),
                      0,
                      SHFileInfo,
                      SizeOf(SHFileInfo),
                      SHGFI_ICON or SHGFI_SYSICONINDEX);
  aIcon.Handle := SHFileInfo.hIcon;
  Result := (h <> 0);
end;

function ABFileTimeToDateTime(aFileTime: TFileTime): TDateTime;
var
  STime: TSystemTime;
begin
  FileTimeToLocalFileTime(aFileTime, aFileTime);
  FileTimeToSystemTime(aFileTime, STime);
  Result := SystemTimeToDateTime(STime);
end;

function ABDateTimeToFileTime(aDateTime: TDateTime): TFileTime;
var
  FTime: TFileTime;
  STime: TSystemTime;
begin
  DateTimeToSystemTime(aDateTime,STime);
  SystemTimeToFileTime(STime, FTime);
  LocalFileTimeToFileTime(FTime, FTime);
  Result := FTime;
end;

function ABCopyFile(aSFileName, aTFileName: string): Boolean;
begin
  Result := CopyFile(PChar(aSFileName), PChar(aTFileName), false);
end;

function ABCopyFile(aSFileName, aTFileName: string; aProgressBar: TProgressBar): Boolean;
var
  FromF, ToF: file;
  NumRead, NumWritten: Integer;
  Buf: array[1..65535] of ansiChar;
begin
  //关联文件
  AssignFile(FromF, aSFileName);
  Reset(FromF, 1);

  AssignFile(ToF, aTFileName);
  Rewrite(ToF, 1);
  ABSetProgressBarBegin(aProgressBar,Ceil(FileSize(FromF) / (High(Buf)+1)));
  try
    repeat
      ABSetProgressBarNext(aProgressBar);

      //从源文件读出写到目标文件中
      BlockRead(FromF, Buf, SizeOf(Buf), NumRead);
      BlockWrite(ToF, Buf, NumRead, NumWritten);
    until (NumRead = 0) or (NumWritten <> NumRead);
    Result := (NumRead = 0);
  finally
    ABSetProgressBarEnd(aProgressBar);
    CloseFile(FromF);
    CloseFile(ToF);
  end;
end;

procedure ABCopyDir(aSDirOrPath, aDDirOrPath: string;aFilter:string;aBeforeCopyDelDDirOrPath:boolean;aProgressBar:TProgressBar);
var DirInfo: TSearchRec;
    DosError: Integer;
begin
  if not (SysUtils.DirectoryExists(aSDirOrPath)) then
    Exit;

  aSDirOrPath:=abgetpath(aSDirOrPath);
  aDDirOrPath:=abgetpath(aDDirOrPath);
  ABSetProgressBarNext(aProgressBar);

  if aBeforeCopyDelDDirOrPath then
    ABDeltree(aDDirOrPath);

  if (not SysUtils.DirectoryExists(aDDirOrPath)) then
      SysUtils.ForceDirectories(aDDirOrPath);

  Application.ProcessMessages;
  DosError := FindFirst(aSDirOrPath+aFilter, faHidden or faSysFile or faDirectory, DirInfo);
  while DosError=0 do
  begin
    ABSetProgressBarNext(aProgressBar);

    if ((DirInfo.Attr and FaDirectory)=faDirectory) and (DirInfo.Name<>'.') and (DirInfo.Name<>'..') then
      ABCopyDir(aSDirOrPath + DirInfo.Name, aDDirOrPath + DirInfo.Name,aFilter,aBeforeCopyDelDDirOrPath,aProgressBar);

    if ((DirInfo.Attr and FaDirectory)<>FaDirectory)  then
      CopyFile(PChar(aSDirOrPath + DirInfo.Name), PChar(aDDirOrPath + DirInfo.Name), false);
    DosError := FindNext(DirInfo);
  end;
  FindClose(DirInfo);
end;

function ABMoveFileToSubPath(aDirOrPath,aFileName,aSubDirOrPath: string): Boolean;
begin
  Result :=false;
  aDirOrPath:= ABGetpath(aDirOrPath);
  aSubDirOrPath:=ABGetpath(aSubDirOrPath);
  if ABCheckFileExists(aDirOrPath + aFileName) then
  begin
    if not SysUtils.DirectoryExists(aDirOrPath + aSubDirOrPath) then
      MkDir(aDirOrPath + aSubDirOrPath);

    if ABCheckFileExists(aDirOrPath + aFileName) then
    begin
      DeleteFile(aDirOrPath +aSubDirOrPath + aFileName);
    end;

    Result :=ABMoveFile(aDirOrPath + aFileName, aDirOrPath +aSubDirOrPath + aFileName);
  end;
end;

procedure ABDeltree(aDirOrPath: string;aIsDelSelf:boolean);
var
  sr: TSearchRec;
  fr: Integer;
begin
  if not SysUtils.DirectoryExists(aDirOrPath) then
  begin
    Exit;
  end;
  aDirOrPath:=ABGetpath(aDirOrPath);
  fr := FindFirst(aDirOrPath + '*.*', faAnyFile, sr);
  try
    while fr = 0 do
    begin
      if (sr.Name <> '.') and (sr.Name <> '..') then
      begin
        // 对于特殊文件(只读,系统,隐藏)则先用FileSetAttr函数修改属性.修改文件属性
        FileSetAttr(sr.Name,0);

        if sr.Attr and faDirectory = faDirectory then
          ABDeltree( aDirOrPath+ sr.Name)
        else
          DeleteFile(aDirOrPath + sr.Name);
      end;
      fr := FindNext(sr);
    end;
  finally
    FindClose(sr);
  end;

  if aIsDelSelf then
    RemoveDir(aDirOrPath);
end;

function ABGetDirCount(aDirOrPath: string;aNameFilters: array of string;aFindLevel: longint): Integer;
var
  sr: TSearchRec;
  fr: Integer;
  tempBoolean: Boolean;
begin
  Result := 0;
  if aFindLevel = 0 then
    exit;
  aFindLevel := aFindLevel - 1;

  aDirOrPath := ABGetpath(aDirOrPath);
  tempBoolean := (high(aNameFilters) = -1);

  fr := FindFirst(aDirOrPath + '*.*', faAnyFile, sr);
  while fr = 0 do
  begin
    if ((sr.Attr and faDirectory) = faDirectory) then
    begin
      if (sr.Name <> '.') and (sr.Name <> '..') then
      begin
        if (tempBoolean) or (ABLike(sr.Name,aNameFilters)) then
          Inc(Result);

        Result := Result + ABGetDirCount(aDirOrPath + sr.Name,aNameFilters, aFindLevel);
      end;
    end;

    fr := FindNext(sr);
  end;
  FindClose(sr);
end;

procedure ABGetDirList( aDirList: TStrings;
                        aDirOrPath: string; aNameFilters: array of string;aFindLevel: longint;
                        aProgressBar: TProgressBar;
                        aResultDirOrPath: boolean);
var
  DSearchRec: TSearchRec;
  FindResult: shortint;
  tempBoolean: Boolean;
begin
  if aFindLevel = 0 then
    exit;
  if aDirOrPath = EmptyStr then
    exit;

  aFindLevel := aFindLevel - 1;
  aDirOrPath := ABGetpath(aDirOrPath);

  try
    tempBoolean := (high(aNameFilters) = -1) ;
    FindResult := FindFirst(aDirOrPath + '*.*', faAnyFile, DSearchRec);
    while FindResult = 0 do
    begin
      ABSetProgressBarNext(aProgressBar);

      if ((DSearchRec.Attr and faDirectory) = faDirectory) then
      begin
        if (DSearchRec.Name<> '.') and (DSearchRec.Name <> '..') then
        begin
          if (tempBoolean) or (ABLike(DSearchRec.Name,aNameFilters)) then
          begin
            if not aResultDirOrPath then
              aDirList.Add(DSearchRec.Name)
            else
              aDirList.Add(aDirOrPath + DSearchRec.Name);
          end;

          ABGetDirList( aDirList,aDirOrPath + DSearchRec.Name, aNameFilters, aFindLevel, aProgressBar,aResultDirOrPath);
        end;
      end;
      FindResult := FindNext(DSearchRec);
    end;
  finally
    FindClose(DSearchRec);
  end;
end;

function ABGetFileCount(aDirOrPath: string;aNameFilters: array of string;aFindLevel: longint;aNameFilterOnlyExt:boolean): Integer;
var
  sr: TSearchRec;
  fr: Integer;
  tempBoolean: Boolean;
begin
  Result := 0;
  if aFindLevel = 0 then
    exit;
  aFindLevel := aFindLevel - 1;

  aDirOrPath := ABGetpath(aDirOrPath);
  tempBoolean := (high(aNameFilters) = -1);

  fr := FindFirst(aDirOrPath + '*.*', faAnyFile, sr);
  while fr = 0 do
  begin
    if ((sr.Attr and faDirectory) = faDirectory) then
    begin
      if (sr.Name <> '.') and (sr.Name <> '..') then
      begin
        Result := Result + ABGetFileCount(aDirOrPath + sr.Name,aNameFilters, aFindLevel);
      end
    end
    else
    begin
      if (tempBoolean) or
         ((not aNameFilterOnlyExt) and (ABLike(sr.Name,aNameFilters))) or
         ((aNameFilterOnlyExt) and (ABLike(ABGetFileExt(sr.Name),aNameFilters)))
          then
        Inc(Result);
    end;

    fr := FindNext(sr);
  end;
  FindClose(sr);
end;

procedure ABGetFileList(aFileList: TStrings;
                        aDirOrPath: string;aNameFilters: array of string;aFindLevel: longint;
                        aNameFilterOnlyExt:boolean;
                        aProgressBar: TProgressBar;
                        aResultDirOrPath: boolean);
var
  DSearchRec: TSearchRec;
  FindResult: shortint;
  tempBoolean: Boolean;
begin
  if aFindLevel = 0 then
    exit;
  if aDirOrPath = EmptyStr then
    exit;

  aFindLevel := aFindLevel - 1;
  aDirOrPath := ABGetpath(aDirOrPath);
  try
    tempBoolean := (high(aNameFilters) = -1) ;
    FindResult := FindFirst(aDirOrPath + '*.*', faAnyFile, DSearchRec);
    while FindResult = 0 do
    begin
      ABSetProgressBarNext(aProgressBar);

      if ((DSearchRec.Attr and faDirectory) = faDirectory) then
      begin
        if (DSearchRec.Name <> '.') and (DSearchRec.Name <> '..') then
        begin
          ABGetFileList(aFileList,aDirOrPath + DSearchRec.Name, aNameFilters,aFindLevel,aNameFilterOnlyExt,aProgressBar,  aResultDirOrPath);
        end
      end
      else
      begin
        if (tempBoolean) or
           ((not aNameFilterOnlyExt) and (ABLike(DSearchRec.Name,aNameFilters))) or
           ((aNameFilterOnlyExt) and (ABLike(ABGetFileExt(DSearchRec.Name),aNameFilters)))
            then
        begin
          if not aResultDirOrPath then
            aFileList.Add(DSearchRec.Name)
          else
            aFileList.Add(aDirOrPath + DSearchRec.Name);
        end;
      end;
      FindResult := FindNext(DSearchRec);
    end;
  finally
    FindClose(DSearchRec);
  end;
end;

function ABOpenWith(const aFileName: string): Integer;
begin
  Result := ShellExecute(Application.Handle, 'open', 'rundll32.exe', PChar('shell32.dll,OpenAs_RunDLL ' + aFileName), '', SW_SHOW);
end;

function ABIsTextFile(aFileName:string):boolean;
var
  Fs:TFileStream;
  i,size:integer;
  IsTextFile:boolean;
  ByteData:Byte;
begin
  if FileExists(aFileName) then
  begin
    Fs:=TFileStream.Create(aFileName,fmOpenRead);
    IsTextFile:=true;
    i:=0;
    size:=Fs.Size;
    While (i<size) and IsTextFile do
    begin
      Fs.Read(ByteData,1);
      IsTextFile:=ByteData<>0;
      inc(i)
    end;
    Fs.Free;
    Result:=IsTextFile
  end
  else
    Result:=false
end;

function ABMergerExcel_OLE(aSFileNames: array of string;aSSheetNames: array of string;
                       aDFileName: string;aDSheetNames: array of string;
                       var aErrMsg:string;
                       aAppend:Boolean;
                       aDelSFile:Boolean;aOpenNew:Boolean): boolean;
var
  i, j,k: Integer;
  tempApp: Variant;
  tempExceBookS: OleVariant;
  tempExceBookD: OleVariant;
  tempDelInitSheet:boolean;

  tempDSheetNameStrings:TStrings;
  tempSSheetNames_Item,
  tempDSheetNames_Item,
  tempDSheetNames_Cur:string;
begin
  Result := false;

  tempDelInitSheet:=false;
  //如果新文件已存在则根据需要删除新文件
  if (not aAppend) and
     (ABCheckFileExists(aDFileName)) then
    DeleteFile(aDFileName);
  try
    tempDSheetNameStrings:=TStringList.Create;
    tempApp := CreateOleObject('Excel.Application');
    tempApp.DisplayAlerts := False;
    try
      if (ABCheckFileExists(aDFileName)) then
      begin
        tempExceBookD := tempApp.WorkBooks.Open(aDFileName);
      end
      else
      begin
        tempExceBookD := tempApp.WorkBooks.Add;
        tempDelInitSheet:=True;
      end;
      //将已有的SHEET名称放到列表中
      for k :=  1 to tempExceBookD.Sheets.Count do
      begin
        tempDSheetNameStrings.add(tempExceBookD.Sheets[k].name);
      end;

      for i := low(aSFileNames) to High(aSFileNames) do
      begin
        tempSSheetNames_Item:=EmptyStr;
        tempDSheetNames_Item:=EmptyStr;
        if i<=High(aSSheetNames) then
          tempSSheetNames_Item:=abstringreplace(aSSheetNames[i],';',',');
        if i<=High(aDSheetNames) then
          tempDSheetNames_Item:=abstringreplace(aDSheetNames[i],';',',');

        tempExceBookS := tempApp.WorkBooks.Open(aSFileNames[i]);
        //骤档骤Sheet复制到模版档中
        for j := 1 to tempExceBookS.Sheets.Count do
        begin
          if (tempSSheetNames_Item=EmptyStr) or
             (abpos(','+tempExceBookS.Sheets[j].Name+',',','+tempSSheetNames_Item+',')>0) then
          begin
            tempExceBookS.Sheets[j].Copy(After := tempExceBookD.Sheets[tempExceBookD.Sheets.Count]);
            //因为一个EXCEL至少要一个SHEET页面，而一个EXCEL创建后会自动有三个SHEET页面，
            //所以在合并第一个页面后再去删除新文件中这些无用的SHEET
            if tempDelInitSheet then
            begin
              tempDelInitSheet:=false;
              for k := tempExceBookD.Sheets.Count-1 downto 1 do
              begin
                tempDSheetNameStrings.Delete(tempDSheetNameStrings.IndexOf(tempExceBookD.Sheets[k].name));
                tempExceBookD.Sheets[k].delete;
              end;
            end;
          end;

          //设置名称
          tempDSheetNames_Cur:=EmptyStr;
          if tempDSheetNames_Item=EmptyStr then
            tempDSheetNames_Cur:= ABGetFilename(aSFileNames[i])+' '+tempExceBookS.Sheets[j].name
          else
            tempDSheetNames_Cur:= ABGetSpaceStr(tempDSheetNames_Item,j,',');

          if (tempDSheetNames_Cur<>EmptyStr) and
             (tempDSheetNameStrings.IndexOf(tempDSheetNames_Cur)<0)  then
          begin
            tempExceBookD.Sheets[tempExceBookD.Sheets.Count].name:=tempDSheetNames_Cur;
            tempDSheetNameStrings.Add(tempDSheetNames_Cur);
          end;
        end;
        tempExceBookS.Close;
      end;
      //将生成的Model档另存为
      tempExceBookD.SaveAs(aDFileName);

      //合并完成后是否删除来源文件
      if aDelSFile then
      begin
        for i := low(aSFileNames) to High(aSFileNames) do
        begin
          DeleteFile(aSFileNames[i]);
        end;
      end;
      Result := true;
    finally
      tempDSheetNameStrings.Free;
      tempApp.DisplayAlerts := true;
      if (Result) and (aOpenNew) then
      begin
        tempApp.Visible := true;
      end
      else
      begin
        tempExceBookD.Close;
        tempApp.Quit;
      end;
    end;
  except
    on E: Exception do
    begin
      aErrMsg := E.Message;
      raise;
    end;
  end;
end;

function ABDeleteFile(aFileNameStrings: TStrings): Boolean;
var
  i:LongInt;
begin
	Result:=false;
  for I := 0 to aFileNameStrings.Count-1 do
  begin
    Result:=ABDeleteFile(aFileNameStrings[i]);
    if not Result then
      Break;
  end;
end;

function ABDeleteFile(const aFileName: string): Boolean;
begin
	Result:=false;
	if not ABCheckFileExists(aFileName) then
    exit;

	FileSetAttr(aFileName, (not SysUtils.faReadOnly) and (not SysUtils.faHidden) and (not SysUtils.faSysFile));
	Result:=DeleteFile(aFileName);
end;

function ABDeleteToRecycleBin(aFilename: string): Boolean;
var
  SHF: TSHFileOpStruct;
begin
  FillChar(SHF, sizeof(SHF), 0);
  SHF.Wnd:= Application.Handle;
  SHF.wFunc:= FO_Delete;
  SHF.pFrom:= PChar (aFilename+#0#0);
  SHF.pTo := PChar (#0#0);
  SHF.fFlags:=
    FOF_Silent or
    FOF_AllowUndo or
    FOF_NoConfirmation;
  result:= SHFileOperation (SHF) = 0;
end;


procedure ABCreateMDB(aDBName: string);
var
  CreateAccess: OleVariant;
begin
  if not ABCheckFileExists(aDBName) then
  begin
    CreateAccess := CreateOleObject('ADOX.Catalog');
    CreateAccess.Create('Provider=Microsoft.Jet.OLEDB.4.0;Data Source=' +aDBName);
  end;
end;

function ABGetMaxVersionIndex(const aVersion1, aVersion2: string): LongInt;
var
  i, j, k: longint;
begin
  result := 0;
  if (aVersion1 = EmptyStr) and (aVersion2 <> EmptyStr) then
    result := 2
  else if (aVersion2 = EmptyStr) and (aVersion1 <> EmptyStr) then
    result := 1
  else
  begin
    for i := 1 to Min(ABGetSpaceStrCount(aVersion1, '.'),ABGetSpaceStrCount(aVersion2, '.')) do
    begin
      j := StrToIntDef(ABGetSpaceStr(aVersion1, i, '.'), 0);
      k := StrToIntDef(ABGetSpaceStr(aVersion2, i, '.'), 0);
      if j > k then
      begin
        result := 1;
        break;
      end
      else if j < k then
      begin
        result := 2;
        break;
      end;
    end;
  end;
end;

function ABGetMinVersion(const aVersion1, aVersion2: string): string;
var
  i: longint;
begin
  result := EmptyStr;
  i:=ABGetMaxVersionIndex(aVersion1,aVersion2);
  if i=1 then
    result := aVersion2
  else if i=2 then
    result := aVersion1;
end;

function ABGetMaxVersion(const aVersion1, aVersion2: string): string;
var
  i: longint;
begin
  result := EmptyStr;
  i:=ABGetMaxVersionIndex(aVersion1,aVersion2);
  if i=1 then
    result := aVersion1
  else if i=2 then
    result := aVersion2;
end;

function ABGetTextFileTypeAndText(const aFileName: string;aReadText:boolean;aReadFirstRow:Boolean; var sText:string): TABTxtFileType;
  function WordLoHiExchange(w:Word):Word;register;
  asm
    XCHG AL, AH
  end;
const
  TextFormatFlag:array[tfAnsi..tfUtf8] of word=($0000,$FFFE,$FEFF,$EFBB);
var
  w:Word;
  tempFileStream:TFileStream;
  tempAnsiText:AnsiString;
  tempUnicodeText:WideString;
  tempUTF8Text:UTF8String;
  tempBeginLength,
  tempEnterWrapIndex,
  tempReadTxtLength:LongInt;
  tempTxtFileType: TABTxtFileType;

  function GetEnterWrapIndex(aStep:LongInt=1;aEnterFlag:longint=$D;aWrapFlag:longint=$A):LongInt;
  var
    tempReadb:byte;
    tempReadw:Word;
    function DoRead:LongInt;
    begin
      Result:=0;
      case aStep of
        1:
        begin
          tempFileStream.Read(tempReadb,aStep);
          Result:=tempReadb;
        end;
        2:
        begin
          tempFileStream.Read(tempReadw,aStep);
          tempReadw:=WordLoHiExchange(tempReadw);
          Result:=tempReadw;
        end;
      end;
    end;
  begin
    tempFileStream.Position:=tempBeginLength;
    Result:=-1;
    repeat
      if DoRead=aEnterFlag then
      begin
        if DoRead=aWrapFlag then
        begin
          Result:=tempFileStream.Position-2*aStep;
          Break;
        end;
      end;
    until (tempFileStream.Position>=tempFileStream.Size-aStep);
  end;
  procedure DoReadAllText;
  var
    i:LongInt;
  begin
    tempFileStream.Position:=tempBeginLength;
    tempReadTxtLength:=0;
    case tempTxtFileType of
      tfAnsi:
      begin
        if (aReadFirstRow) and
           (tempFileStream.Size-tempBeginLength>=2) then
        begin
          tempEnterWrapIndex:=GetEnterWrapIndex;
          tempFileStream.Position:=tempBeginLength;
          if tempEnterWrapIndex>0 then
          begin
            tempReadTxtLength:=tempEnterWrapIndex-tempBeginLength;
          end
          else
          begin
            tempReadTxtLength:=tempFileStream.Size-tempBeginLength;
          end
        end
        else
        begin
          tempReadTxtLength:=tempFileStream.Size-tempBeginLength;
        end;
        SetLength(tempAnsiText,tempReadTxtLength);
        tempFileStream.Read(tempAnsiText[1],tempReadTxtLength);
        sText := string(tempAnsiText);
      end;
      tfUnicode:
      begin
        if (aReadFirstRow) and
           (tempFileStream.Size-tempBeginLength>=2) then
        begin
          tempEnterWrapIndex:=GetEnterWrapIndex(2,$D00,$A00);
          tempFileStream.Position:=tempBeginLength;
          if tempEnterWrapIndex>0 then
          begin
            tempReadTxtLength:=tempEnterWrapIndex-tempBeginLength;
          end
          else
          begin
            tempReadTxtLength:=tempFileStream.Size-tempBeginLength;
          end
        end
        else
        begin
          tempReadTxtLength:=tempFileStream.Size-tempBeginLength;
        end;
        SetLength(tempUnicodeText,tempReadTxtLength div 2);
        tempFileStream.Read(tempUnicodeText[1],tempReadTxtLength);
        sText := tempUnicodeText;
      end;
      tfUnicodeBigEndian:
      begin
        if (aReadFirstRow) and
           (tempFileStream.Size-tempBeginLength>=2) then
        begin
          tempEnterWrapIndex:=GetEnterWrapIndex(2,$D,$A);
          tempFileStream.Position:=tempBeginLength;
          if tempEnterWrapIndex>0 then
          begin
            tempReadTxtLength:=tempEnterWrapIndex-tempBeginLength;
          end
          else
          begin
            tempReadTxtLength:=tempFileStream.Size-tempBeginLength;
          end
        end
        else
        begin
          tempReadTxtLength:=tempFileStream.Size-tempBeginLength;
        end;
        SetLength(tempUnicodeText,tempReadTxtLength div 2);
        tempFileStream.Read(tempUnicodeText[1],tempReadTxtLength);

        //将取出的字节前后倒置
        for I := 0 to ((tempFileStream.Size-tempBeginLength) div 2)-1 do
        begin
          tempUnicodeText[i+1]:=chr(htons(ord(tempUnicodeText[i+1])));
        end;
        sText := tempUnicodeText;
      end;
      tfUtf8:
      begin
        if (aReadFirstRow) and
           (tempFileStream.Size-tempBeginLength>=2) then
        begin
          tempEnterWrapIndex:=GetEnterWrapIndex;
          tempFileStream.Position:=tempBeginLength;
          if tempEnterWrapIndex>0 then
          begin
            tempReadTxtLength:=tempEnterWrapIndex-tempBeginLength;
          end
          else
          begin
            tempReadTxtLength:=tempFileStream.Size-tempBeginLength;
          end
        end
        else
        begin
          tempReadTxtLength:=tempFileStream.Size-tempBeginLength;
        end;
        SetLength(tempUTF8Text,tempReadTxtLength);
        tempFileStream.Read(tempUTF8Text[1],tempReadTxtLength);
        sText := UTF8ToString(tempUTF8Text);
      end;
    end;
  end;
begin
  result:=tfANSI;
  if not ABCheckFileExists(aFileName) then
    exit;

  tempTxtFileType:=tfANSI;
  tempFileStream:=TFileStream.Create(aFileName,fmOpenRead or fmShareDenyNone);
  try
    tempFileStream.Read(w,2);
    w:=WordLoHiExchange(w);//因为是以Word数据类型读取，故高低字节互换
    if w = TextFormatFlag[tfUnicode] then
    begin
      tempBeginLength:=2;
      tempTxtFileType:= tfUnicode;
    end
    else if w = TextFormatFlag[tfUnicodeBigEndian] then
    begin
      tempBeginLength:=2;
      tempTxtFileType:= tfUnicodeBigEndian;
    end
    else if w = TextFormatFlag[tfUtf8] then
    begin
      //这里要注意一下，UFT-8必须要跳过三个字节。
      tempBeginLength:=3;
      tempTxtFileType:=tfUtf8;
    end
    else
    begin
      tempBeginLength:=0;
      tempTxtFileType:=tfANSI;
    end;

    if aReadText then
    begin
      DoReadAllText;
    end;
    result:=tempTxtFileType;
  finally
    tempFileStream.Free;
  end;
end;

function ABGetTextFileType(const aFileName: string; var sText:string): TABTxtFileType;
begin
  result:= ABGetTextFileTypeAndText(aFileName,True,false,sText);
end;

function ABGetTextFileType(const aFileName: string): TABTxtFileType;
var
  sText:string;
begin
  result:= ABGetTextFileTypeAndText(aFileName,False,false,sText);
end;

function ABReadTxtFirstRow(aFilename: string): string;
begin
  ABGetTextFileTypeAndText(aFileName,True,True,result);
end;

function ABReadTxt(aFilename: string): string;
begin
  ABGetTextFileTypeAndText(aFileName,True,false,result);
end;

function ABWriteTxt( aFileName: string;  aStr: string;aSaveFileType: TABTxtFileType; aAppend: boolean ):boolean;
var
  MemStream: TMemoryStream;
  HeaderStr: Ansistring;
  i:LongInt;
  tempAnsiText:AnsiString;
  tempUnicodeText:WideString;
  tempUTF8Text:UTF8String;
begin
  Result:=false;
  if aFileName = emptystr then
    exit;

  MemStream := TMemoryStream.Create;
  try
    if aAppend then
    begin
      MemStream.LoadFromFile(aFileName);
      MemStream.Position:=MemStream.Size;
      aStr:=#$D#$A+aStr;
    end;

    case aSaveFileType of
      tfAnsi:
      begin
        tempAnsiText :=AnsiString(aStr);
        MemStream.Write(tempAnsiText[1], Length(tempAnsiText));
      end;
      tfUnicode:
      begin
        if MemStream.Position<=0 then
        begin
          HeaderStr := #$FF#$FE;
          MemStream.Write(HeaderStr[1], 2);
        end;

        tempUnicodeText := WideString(aStr);
        MemStream.Write(tempUnicodeText[1], Length(tempUnicodeText) * 2);
      end;
      tfUnicodeBigEndian:
      begin
        if MemStream.Position<=0 then
        begin
          HeaderStr := #$FE#$FF;
          MemStream.Write(HeaderStr[1], 2);
        end;

        tempUnicodeText := WideString(aStr);
        //将保存的字节前后倒置
        for I := 0 to Length(tempUnicodeText)-1 do
        begin
          tempUnicodeText[i+1]:=chr(htons(ord(tempUnicodeText[i+1])));
        end;

        MemStream.Write(tempUnicodeText[1], Length(tempUnicodeText) * 2);
      end;
      tfUtf8:
      begin
        if MemStream.Position<=0 then
        begin
          HeaderStr := #$EF#$BB#$BF;
          MemStream.Write(HeaderStr[1], 3);
        end;
        tempUTF8Text := AnsiToUtf8(aStr);
        MemStream.Write(tempUTF8Text[1], Length(tempUTF8Text));
      end;
    end;
    MemStream.Position := 0;
    MemStream.SaveToFile(aFileName);
  finally
    MemStream.Free;
  end;
end;

function ABCheckInI(aGroup, aName, aFilename: string):Boolean;
var
  tempInIFile: TIniFile;
  tempFlag:string;
begin
  result := false;
  if aFilename = EmptyStr then
    exit;
  if not ABCheckFileExists(aFilename) then
    exit;

  tempFlag:='-`0123456789-=zxcvbnm,./';
  tempInIFile := TIniFile.Create(aFilename);
  try
    result := tempInIFile.ReadString(aGroup, aName,tempFlag)<>tempFlag;
  finally
    tempInIFile.free;
  end;
end;

function ABReadInI(aGroup, aName, aFilename: string;aEmptystrResultValue:string;aEmptystrWriteValue:string): string;
var
  tempInIFile: TIniFile;
begin
  result := EmptyStr;
  if aFilename = EmptyStr then
    exit;

  tempInIFile := TIniFile.Create(aFilename);
  try
    if ABCheckFileExists(aFilename) then
    begin
      result := tempInIFile.ReadString(aGroup, aName,EmptyStr);
    end;

    if (result= EmptyStr) then
    begin
      tempInIFile.WriteString(aGroup, aName, aEmptystrWriteValue);
      result := aEmptystrResultValue;
    end;
  finally
    tempInIFile.Free;
  end;
end;

procedure ABReadInI(aGroup, aFilename: string; aStrings: TStrings);
var
  tempInIFile: TIniFile;
begin
  if aFilename = EmptyStr then
    exit;

  aStrings.Clear;
  aStrings.BeginUpdate;
  tempInIFile := TIniFile.Create(aFilename);
  try
    if ABCheckFileExists(aFilename) then
    begin
      tempInIFile.ReadSectionValues(aGroup, aStrings);
    end;
  finally
    tempInIFile.Free;
    aStrings.EndUpdate;
  end;
end;

procedure ABWriteInI(aGroup, aFilename: string; aStrings: TStrings;aDeleteOldGroup:Boolean);
var
  tempInIFile: TIniFile;
  i: LongInt;
begin
  if aFilename = EmptyStr then
    exit;

  tempInIFile := TIniFile.Create(aFilename);
  try
    if aDeleteOldGroup then
      tempInIFile.EraseSection(aGroup);
    for i := 0 to aStrings.Count-1 do
    begin
      tempInIFile.WriteString(aGroup,aStrings.Names[i], aStrings.ValueFromIndex[i]);
    end;
  finally
    tempInIFile.Free;
  end;
end;

function ABGetTxtline(const aFileName: string): integer;
var
  TxtFile: TStringList;
begin
  TxtFile := TStringList.Create;
  try
    TxtFile.LoadFromFile(aFileName);
    result:=TxtFile.Count;
  finally
    TxtFile.Free;
  end;
end;

function ABGetHtmlFileTxt(aHtmlFilename: string): string;
var
  Mystring: TStrings;
  s, lineS: string;
  line, Llen, i, j: integer;
  rloop: boolean;
begin
  rloop := False;
  Mystring := TStringlist.Create;
  s := EmptyStr;
  Mystring.LoadFromFile(ahtmlfilename);
  line := Mystring.Count;
  try
    for i := 0 to line - 1 do
    begin
      lineS := Mystring[i];
      Llen := length(lineS);
      j := 1;
      while (j <= Llen) and (lineS[j] = ' ') do
      begin
        j := j + 1;
        s := s + ' ';
      end;
      while j <= Llen do
      begin
        if lineS[j] = '<' then
          rloop := True;
        if lineS[j] = '>' then
        begin
          rloop := False;
          j := j + 1;
          continue;
        end;
        if rloop then
        begin
          j := j + 1;
          continue;
        end
        else
          s := s + lineS[j];
        j := j + 1;
      end;
      s := s + ABEnterWrapStr;
    end;
  finally
    Mystring.Free;
  end;
  result := s;
end;

procedure ABDeleteIni(aGroup, aFilename: string);
var
  tempInIFile: TIniFile;
begin
  if aFilename = EmptyStr then
    exit;

  tempInIFile := TIniFile.Create(aFilename);
  try
    if ABCheckFileExists(aFilename) then
    begin
      tempInIFile.EraseSection(aGroup);
    end;
  finally
    tempInIFile.Free;
  end;
end;

procedure ABDeleteIni(aGroup, aName, aFilename: string);
var
  tempInIFile: TIniFile;
begin
  if aFilename = EmptyStr then
    exit;

  tempInIFile := TIniFile.Create(aFilename);
  try
    if ABCheckFileExists(aFilename) then
    begin
      tempInIFile.DeleteKey(aGroup, aName);
    end;
  finally
    tempInIFile.Free;
  end;
end;

function ABReadInI(aGroups: array of string; aNames: array of string; aFilename:
  string; aSpaceSign: string): string;
var
  tempInIFile: TIniFile;
  i: LongInt;
  tempStr, tempStr1: string;
begin
  result := EmptyStr;
  if aFilename = EmptyStr then
    exit;

  tempStr := ',';
  tempStr1 := EmptyStr;
  if ABCheckFileExists(aFilename) then
  begin
    tempInIFile := TIniFile.Create(aFilename);
    try
      for i := 0 to High(aNames) do
      begin
        if i<=High(aGroups) then
          tempStr := tempInIFile.ReadString(aGroups[i], aNames[i], '')
        else
          tempStr := tempInIFile.ReadString(aGroups[High(aGroups)], aNames[i], '');

        if tempStr1 = EmptyStr then
          tempStr1 := tempStr
        else
          tempStr1 := tempStr1 + aSpaceSign + tempStr;
      end;
      result := tempStr1;
    finally
      tempInIFile.Free;
    end;
  end;
end;

procedure ABWriteInI(aGroups: array of string; aNames: array of string;aValues: array of string; aFilename: string);
var
  tempInIFile: TIniFile;
  i: LongInt;
  j: LongInt;
begin
  if aFilename = EmptyStr then
    exit;

  j := Min(High(aNames), High(aValues));
  tempInIFile := TIniFile.Create(aFilename);
  try
    for i := 0 to j do
    begin
      if i<=High(aGroups) then
        tempInIFile.WriteString(aGroups[i], aNames[i], aValues[i])
      else
        tempInIFile.WriteString(aGroups[High(aGroups)], aNames[i], aValues[i]);
    end;
  finally
    tempInIFile.Free;
  end;
end;

procedure ABWriteInI(aGroup: string; aName: string; aValue: string; aFilename:string);
var
  tempInIFile: TIniFile;
begin
  if (aFilename = EmptyStr ) or (aGroup = EmptyStr) or (aName = EmptyStr)  then
    exit;

  tempInIFile := TIniFile.Create(aFilename);
  try
    tempInIFile.WriteString(aGroup, aName, aValue);
  finally
    tempInIFile.Free;
  end;
end;

function ABCompareNumberOrText(aStr1, aStr2: string): Integer;
var
  tempDouble1, tempDouble2: Double;
begin
  if (TryStrToFloat(aStr1, tempDouble1)) and
     (TryStrToFloat(aStr2, tempDouble2)) then
  begin
    result :=ABCompareDouble(tempDouble1,tempDouble2);
  end
  else
  begin
    result := AnsiCompareText(aStr1, aStr2);
  end;
end;

function ABCompartTrimStr(aStr: string;aSpaceSign: string;aRowLength:longint;aBegStr:string): string;
var
  i: LongInt;
  tempItem,
  tempRowStr:string;
begin
  result:=emptystr;
  tempRowStr:=emptystr;
  for I := 1 to ABGetSpaceStrCount(aStr, aSpaceSign) do
  begin
    tempItem:=ABGetSpaceStr(aStr, i, aSpaceSign);
    if length(tempRowStr+aSpaceSign+tempItem)>aRowLength then
    begin
      ABAddstr(result,aBegStr+tempRowStr+aSpaceSign,ABEnterWrapStr);
      tempRowStr:=emptystr;
      ABAddstr(tempRowStr,tempItem,aSpaceSign);
    end
    else
    begin
      ABAddstr(tempRowStr,tempItem,aSpaceSign);
    end;
  end;

  ABAddstr(result,aBegStr+tempRowStr,ABEnterWrapStr);
end;

function ABGetCaptionWidth(aFont:TFont;aCaption: string): longint;
begin
  result:=ABGetCaptionWidth(aFont,Length(AnsiString(aCaption)));
end;

function ABGetCaptionWidth(aFont:TFont;aCaptionLen: longint): longint;
begin
  //字体大小会影响得到的宽度
  //此处所对应的宽度是默认字体=8，对于其它设置的宽度此处没有处理，以后补上

  result:=aCaptionLen*6+8;
end;

function ABAddstr_Ex(aSumStr: string; aSubStr: string; aSpaceSign: string;aDirection: TABXDirection): string;
begin
  Result:= aSumStr;
  ABAddstr(Result,aSubStr,aSpaceSign,aDirection)
end;

function ABCheckBeginStr(aStr: string; aCheckStr: string): boolean;
begin
  result:=(AnsiCompareText(Copy(aStr,1,Length(aCheckStr)),aCheckStr)=0);
end;

function ABCheckEndStr(aStr: string; aCheckStr: string): boolean;
begin
  result:=(AnsiCompareText(Copy(aStr,Length(aStr)-Length(aCheckStr)+1,Length(aCheckStr)),aCheckStr)=0);
end;

procedure ABAddstr(var aSumStr: string; aSubStr: string; aSpaceSign: string;aDirection: TABXDirection);
begin
  if aSubStr<>EmptyStr then
  begin
    if aSumStr=EmptyStr then
      aSumStr:= aSubStr
    else
    begin
      if aDirection=axdLeft then
        aSumStr:=aSubStr+aSpaceSign+ aSumStr
      else
        aSumStr:=aSumStr+aSpaceSign+ aSubStr;
    end;
  end;
end;

function ABGetSpaceStrCount(aStr: string; aSpaceSign: string): longint;
var
  tempSourceStrLength,
  tempSpaceSignLength,
  tempPos,
  tempCount: longint;
begin
  tempCount:=0;

  if aStr<>emptystr then
  begin
    aStr := ABStringReplace(aStr, ABEnterWrapStr, ' ');
    if aSpaceSign = emptystr then
    begin
      tempCount := length(aStr);
    end
    else
    begin
      //发现子串的个数
      tempCount := 0;
      //当前源串扫描的位置
      tempPos := 1;
      //源串长度
      tempSourceStrLength := length(aStr);
      //分隔符长度
      tempSpaceSignLength := length(aSpaceSign);

      while (tempPos <= tempSourceStrLength)  do
      begin
        if AnsiCompareText(copy(aStr, tempPos, tempSpaceSignLength), aSpaceSign)<> 0 then
        begin
          //未扫描到分隔符则位置下移继续扫描
          tempPos := tempPos + 1;
        end
        else
        //扫描到分隔符
        begin
          tempCount := tempCount + 1;
          tempPos := tempPos + tempSpaceSignLength;
        end;
      end;
      //子串的个数再+1
      tempCount := tempCount + 1;
    end;
  end;

  result := tempCount;
end;

function ABGetSpaceStr(aStr: string; aIndex: longint; aSpaceSign:string): string;
var
  i, j: longint;
begin
  result :=emptystr;
  if aStr = emptystr then
    exit;

  if aSpaceSign = emptystr then
  begin
    result := aStr[aIndex];
  end
  else
  begin
    aStr := ABStringReplace(aStr, ABEnterWrapStr, ' ');
    for i := 1 to aIndex do
    begin
      j := ABPos(aSpaceSign, aStr);
      if j <= 0 then
      begin
        if i = aIndex then
          result := aStr;
        break;
      end
      else
      begin
        if i = aIndex then
        begin
          if j - 1>0 then
            result := (copy(aStr, 1, j - 1));

          break;
        end;
        delete(aStr, 1, j + Length(aSpaceSign) - 1);
      end;
    end;
  end;
end;

function ABGetSpaceIndex(aStr, aSubStr: string; aSpaceSign: string):
  longint;
var
  i: longint;
begin
  result := 0;
  aStr := ABStringReplace(aStr, ABEnterWrapStr, ' ');
  aSubStr := ABStringReplace(aSubStr, ABEnterWrapStr, ' ');
  for i := 1 to ABGetSpaceStrCount(aStr, aSpaceSign) + 1 do
  begin
    if AnsiCompareText(aSubStr, ABGetSpaceStr(aStr, i, aSpaceSign))= 0 then
    begin
      result := i;
      break;
    end;
  end;
end;

function ABGetGuid: string;
var
  TimeE: Int64; //频率
begin
  //CreateGUID(tempGuid);
  //result := GUIDToString(tempGuid);
  result:=FormatDateTime('YYYYMMDDHHNNSSZZZZZZZ',now)+copy(floattostr(TimeE),1,15)+ABRandomStr(0,99999);
end;

function ABStrIsGUID(const aStr: string): Boolean;
begin
  try
    StringToGUID(aStr);
    Result := True;
  except
    Result := False;
  end;
end;

function ABTxtReplace(aFileName: string; aOldStr, aNewStr: string;aBeginPosition:longint): boolean;
var
 tempStr:string;
begin
  Result:=False;
  if ABCheckFileExists(aFileName) then
  begin
    tempStr:=ABReadTxt(aFileName);
    if aBeginPosition>1 then
    begin
      tempStr :=copy(tempStr,1,aBeginPosition-1)+ StringReplace(copy(tempStr,aBeginPosition,length(tempStr)), aOldStr, aNewStr, [rfReplaceAll, rfIgnoreCase]);
    end
    else
    begin
      tempStr := StringReplace(tempStr, aOldStr, aNewStr, [rfReplaceAll, rfIgnoreCase]);
    end;
    ABWriteTxt(aFileName,tempStr);
    Result:=True;
  end;
end;

function ABTxtReplace(aFileName: string; aBeginPosition, aLength: Integer; aNewStr:string): boolean;
var
 tempStr:string;
begin
  Result:=False;
  if ABCheckFileExists(aFileName) then
  begin
    tempStr:=ABReadTxt(aFileName);
    tempStr := Copy(tempStr, 1, aBeginPosition - 1) + aNewStr + Copy(tempStr, aBeginPosition + aLength, Length(tempStr));
    ABWriteTxt(aFileName,tempStr);
    Result:=True;
  end;
end;

function ABStringReplace(aStr: string; aBeginPosition, aLength: Integer; aNewStr:string): string;
begin
  Result := Copy(aStr, 1, aBeginPosition - 1) + aNewStr + Copy(aStr, aBeginPosition + aLength, Length(aStr));
end;

function ABStringReplace(aStr: string; aOldStr, aNewStr: string;aBeginPosition:longint): string;
begin
  if aBeginPosition>1 then
  begin
    result :=copy(aStr,1,aBeginPosition-1)+ StringReplace(copy(aStr,aBeginPosition,length(aStr)), aOldStr, aNewStr, [rfReplaceAll, rfIgnoreCase]);
  end
  else
  begin
    result := StringReplace(aStr, aOldStr, aNewStr, [rfReplaceAll, rfIgnoreCase]);
  end;
end;

function ABStringReplace(aStr: string; aOldStrs:array of string;aNewStr:string;aBeginPosition:longint): string;
  function DoItem(aStr: string): string;
  var
    I: Integer;
  begin
    Result:= aStr;
    for I := Low(aOldStrs) to High(aOldStrs) do
    begin
      Result:= StringReplace(Result, aOldStrs[i], aNewStr, [rfReplaceAll, rfIgnoreCase]);
    end;
  end;
begin
  if aBeginPosition>1 then
  begin
    result :=copy(aStr,1,aBeginPosition-1)+DoItem(copy(aStr,aBeginPosition,length(aStr)));
  end
  else
  begin
    result := DoItem(aStr);
  end;
end;

function ABPos(aSubStr, aStr: String; aBeginPosition: LongInt): Integer;
begin
  aStr := UpperCase(aStr);
  aSubStr := UpperCase(aSubStr);
  if aBeginPosition > 1 then
  begin
    aStr := Copy(aStr, aBeginPosition, Length(aStr));
    Result := Pos(aSubStr, aStr);
    if Result > 0 then
      Result := aBeginPosition + Result - 1;
  end
  else
  begin
    Result := Pos(aSubStr, aStr);
  end
end;

function ABGetSubStrPosition(aSubStr,aStr:string;aCount:Integer):Integer;
var
  nCount,nIndex:Integer;
begin
  nCount:=0;
  while aCount>0 do
  begin
    nIndex:=ABPos(aSubStr,aStr);
    if nIndex<=0 then
      break;
    aStr:=Copy(aStr,nIndex+1,Length(aStr)-nIndex);
    nCount:=nCount+nIndex;
    aCount:=aCount-1;
  end;
  Result:=nCount;
end;

function ABRightPos(const aSubstr, aStr: string): Integer;
var
  i,j,len: Integer;
  PCharS,PCharSub:PChar;
begin
  PCharS:=PChar(UpperCase(aStr)); //将字符串转化为PChar格式(考虑兼容性)
  PCharSub:=PChar(UpperCase(aSubstr));
  Result:=0;
  len:=length(aSubstr);
  for i:=0 to length(aStr)-1 do
  begin
    for j:=0 to len-1 do
    begin
      if PCharS[i+j]<>PCharSub[j] then break;
    end;
    if j=len then Result:=i+1;
  end;
end;

function ABLike(aLikeFromStr,aLikeToStr: string): Boolean;
var
  StringPtr, PatternPtr: PChar;
  StringRes, PatternRes: PChar;
begin
  Result := False;
  PatternPtr := PChar(UpperCase(aLikeToStr));
  StringPtr := PChar(UpperCase(aLikeFromStr));
  StringRes := nil;
  PatternRes := nil;
  repeat
    repeat // ohne vorangegangenes "*"
      case PatternPtr^ of
        #0 : begin
               Result := StringPtr^ = #0;
               if Result or (StringRes = nil) or (PatternRes = nil) then Exit;
               StringPtr := StringRes;
               PatternPtr := PatternRes;
               Break;
             end;
        '*': begin
               Inc(PatternPtr);
               PatternRes := PatternPtr;
               Break;
             end;
        '?': begin
               if StringPtr^ = #0 then Exit;
               Inc(StringPtr);
               Inc(PatternPtr);
             end;
        else begin
               if StringPtr^ = #0 then Exit;
               if StringPtr^ <> PatternPtr^ then
               begin
                 if (StringRes = nil) or (PatternRes = nil) then Exit;
                 StringPtr := StringRes;
                 PatternPtr := PatternRes;
                 Break;
               end else
               begin
                 Inc(StringPtr);
                 Inc(PatternPtr);
               end;
             end;
      end;
    until False;

    repeat // mit vorangegangenem "*"
      case PatternPtr^ of
        #0 : begin
               Result := True;
               Exit;
             end;
        '*': begin
               Inc(PatternPtr);
               PatternRes := PatternPtr;
             end;
        '?': begin
               if StringPtr^ = #0 then Exit;
               Inc(StringPtr);
               Inc(PatternPtr);
             end;
        else begin
               repeat
                 if StringPtr^ = #0 then Exit;
                 if StringPtr^ = PatternPtr^ then Break;
                 Inc(StringPtr);
               until False;
               Inc(StringPtr);
               StringRes := StringPtr;
               Inc(PatternPtr);
               Break;
             end;
      end;
    until False;
  until False;
end;

function ABLike(aLikeFromStr: string; aLikeToStrArray: array of string): Boolean;
var
  i: LongInt;
begin
  Result := False;
  aLikeFromStr:=UpperCase(aLikeFromStr);
  for I := Low(aLikeToStrArray) to High(aLikeToStrArray) do
  begin
    if ABLike(aLikeFromStr,UpperCase(aLikeToStrArray[i])) then
    begin
      Result := true;
      Break;
    end;
  end;
end;

procedure ABReadIniInStrings(aGroup: string;aStrings: TStrings; aOutStrings: TStrings);
var
  i, j:LongInt;
  tempStr1: string;
begin
  aOutStrings.Clear;
  if (aGroup <> EmptyStr) then
  begin
    for I := 0 to aStrings.Count - 1 do
    begin
      if AnsiCompareText(Trim(aStrings[i]), '[' + aGroup + ']') = 0 then
      begin
        aOutStrings.Clear;
        for j := i + 1 to aStrings.Count - 1 do
        begin
          tempStr1 := Trim(aStrings[j]);
          if (ABPos('[', tempStr1) > 0) and (ABPos(']', tempStr1) > 0) then
          begin
            break;
          end
          else
          begin
            aOutStrings.Add(tempStr1);
          end;
        end;
        Break;
      end;
    end;
  end;
end;

function ABReadIniInStrings(aGroup, aName: string;aStrings: TStrings; aEmptystrResultValue:string;aEmptystrWriteValue:string): string;
var
  i, j,
  tempFindGroupIndex,tempFindNameIndex: LongInt;
  tempStr1: string;
begin
  result := EmptyStr;
  tempFindGroupIndex:=-1;
  tempFindNameIndex:=-1;
  if (aGroup <> EmptyStr) and
     (aName <> EmptyStr) then
  begin
    for I := 0 to aStrings.Count - 1 do
    begin
      if AnsiCompareText(Trim(aStrings[i]), '[' + aGroup + ']') = 0 then
      begin
        tempFindGroupIndex:=i;
        for j := i + 1 to aStrings.Count - 1 do
        begin
          tempStr1 := Trim(aStrings[j]);
          if (ABPos('[', tempStr1) > 0) and (ABPos(']', tempStr1) > 0) then
          begin
            break;
          end
          else
          begin
            if ABCheckBeginStr(tempStr1,  aName)   then
            begin
              tempFindNameIndex:=j;
              Result := ABGetLeftRightStr(tempStr1,axdRight);
              break;
            end;
          end;
        end;
        Break;
      end;
    end;
  end;

  if (result= EmptyStr) then
  begin
    if tempFindNameIndex>0 then
    begin
      if AnsiCompareText(aStrings.ValueFromIndex[tempFindNameIndex],aEmptystrWriteValue)<>0 then
        aStrings.ValueFromIndex[tempFindNameIndex]:=aEmptystrWriteValue;
    end
    else if tempFindGroupIndex>=0 then
    begin
      if tempFindGroupIndex=aStrings.Count-1 then
        aStrings.Append(aName+'='+aEmptystrWriteValue)
      else
        aStrings.Insert(tempFindGroupIndex+1,aName+'='+aEmptystrWriteValue);
    end
    else
    begin
      aStrings.Append('[' + aGroup + ']');
      aStrings.Append(aName+'='+aEmptystrWriteValue);
    end;

    result := aEmptystrResultValue;
  end;
end;

procedure ABWriteItemInStrings(aName,aValue: string;aStrings: TStrings);
begin
  if aStrings.IndexOfName(aName)<0 then
    aStrings.Add(aName+'='+aValue)
  else
    aStrings.Values[aName]:=aValue;
end;

procedure ABWriteIniInStrings(aGroup, aName,aValue: string; aStrings: TStrings);
var
  i, j,
  tempFindGroupIndex,tempFindNameIndex: LongInt;
  tempStr1: string;
begin
  tempFindGroupIndex:=-1;
  tempFindNameIndex:=-1;
  if (aGroup <> EmptyStr) and
     (aName <> EmptyStr) then
  begin
    for I := 0 to aStrings.Count - 1 do
    begin
      if AnsiCompareText(Trim(aStrings[i]), '[' + aGroup + ']') = 0 then
      begin
        tempFindGroupIndex:=i;
        for j := i + 1 to aStrings.Count - 1 do
        begin
          tempStr1 := Trim(aStrings[j]);
          if (ABPos('[', tempStr1) > 0) and (ABPos(']', tempStr1) > 0) then
          begin
            break;
          end
          else
          begin
            if ABCheckBeginStr(tempStr1,   aName)  then
            begin
              tempFindNameIndex:=j;
              aStrings.ValueFromIndex[tempFindNameIndex]:=aValue;
              break;
            end;
          end;
        end;
        Break;
      end;
    end;
  end;

  if tempFindNameIndex<0 then
  begin
    if tempFindGroupIndex>=0 then
    begin
      if (aName <> EmptyStr) then
      begin
        if tempFindGroupIndex=aStrings.Count-1 then
          aStrings.Append(aName+'='+aValue)
        else
          aStrings.Insert(tempFindGroupIndex+1,aName+'='+aValue);
      end;
    end
    else
    begin
      aStrings.Append('[' + aGroup + ']');
      if (aName <> EmptyStr) then
        aStrings.Append(aName+'='+aValue);
    end;
  end;
end;

function ABReadIniInText(aGroup, aName: string;var aText: string; aEmptystrResultValue:string;aEmptystrWriteValue:string): string;
var
  tempStrings: TStrings;
begin
  result := EmptyStr;
  if (aGroup = EmptyStr) then
    exit;

  tempStrings := TStringList.Create;
  try
    tempStrings.Text := aText;
    result := ABReadIniInStrings(aGroup,aName,tempStrings,aEmptystrResultValue,aEmptystrWriteValue);
    aText:=Trim(tempStrings.Text);
  finally
    tempStrings.Free;
  end;
end;

function ABReadItemInTxt(aName: string;aText: string):string;
var
  tempStrings: TStrings;
  i:LongInt;
begin
  result := EmptyStr;
  tempStrings := TStringList.Create;
  try
    tempStrings.Text := aText;
    i := tempStrings.IndexOfName(aName);
    if i>=0 then
    begin
      result:=tempStrings.ValueFromIndex[i];
    end;
  finally
    tempStrings.Free;
  end;
end;

procedure ABWriteIniInText( aGroup, aName,aValue: string;var aText: string);
var
  tempStrings: TStrings;
begin
  if (aText = EmptyStr) or (aGroup = EmptyStr) then
    exit;

  tempStrings := TStringList.Create;
  try
    tempStrings.Text := aText;
    ABWriteIniInStrings(aGroup,aName,aValue,tempStrings);
    aText :=Trim(tempStrings.Text);
  finally
    tempStrings.Free;
  end;
end;

function ABSpaceStr(aStr: string; aSpaceLen: Integer; aSpaceSign: string):string;
var
  i, j: Integer;
begin
  Result := EmptyStr;
  j := 0;
  for i := Length(aStr) downto 1 do
  begin
    Result := aStr[i] + Result;
    Inc(j);
    if ((j mod aSpaceLen) = 0) and (i <> 1) then
      Result := aSpaceSign + Result;
  end;
end;

function ABLeftStr(aStr: string; aLeng: Integer): string;
begin
  Result := Copy(aStr, 1, aLeng)
end;

function ABRightStr(aStr: string; aLeng: Integer): string;
var
  tempBegin:longint;
begin
  tempBegin:=Length(aStr) - aLeng + 1;
  if tempBegin>0 then
    Result := Copy(aStr,tempBegin, aLeng)
  else
    Result := Copy(aStr,1, aLeng);
end;

function ABReverseString(aStr: string): string;
begin
  Result := ReverseString(aStr);
end;

function ABCopyStr(aStr: string; aCount: LongInt; aSpaceSign: string): string;
var
  i: LongInt;
begin
  Result := EmptyStr;
  for I := 1 to aCount do
  begin
    ABAddstr(Result,aStr,aSpaceSign);
  end;
end;

function ABGetIndexByBeginStr(aStrings: TStrings;
                              aFindStrs:array of string;
                              aTrimStringLine:Boolean;
                              aTrimFindStr:Boolean
                              ): longint;
var
  i,j,tempNextBegin,tempFindCount:LongInt;
  tempStr1,tempStr2:string;
begin
  result:=-1;
  tempNextBegin:=0;
  tempFindCount:=0;
  for i := Low(aFindStrs) to High(aFindStrs) do
  begin
    if aTrimFindStr then
      tempStr1:=Trim(aFindStrs[i])
    else
      tempStr1:=aFindStrs[i];

    for J := tempNextBegin to aStrings.Count-1 do
    begin
      if aTrimStringLine then
        tempStr2:=Trim(aStrings[j])
      else
        tempStr2:=aStrings[j];

      if ABCheckBeginStr(tempStr2,tempStr1) then
      begin
        tempNextBegin:=j+1;
        tempFindCount:=tempFindCount+1;
        if tempFindCount=(High(aFindStrs)-Low(aFindStrs)+1) then
        begin
          result:=j;
        end;
        Break;
      end;
    end;
  end;
end;

function ABGetStrBeginSpaceCount(aStr:string): longint;
var
  i:LongInt;
begin
  result:=0;
  for i := 1 to Length(aStr) do
  begin
    if aStr[i]<>' ' then
    begin
      Break;
    end
    else
    begin
      result:=result+1;
    end;
  end;
end;

function ABDelItemValue(var aStr: string;
                        aItemName: string;
                        aDelStrs: string ;
                        aEndStr: string ;
                        aCheckUppLowFlag: boolean
                        ): boolean;
var
  i, j: longint;
begin
  result:=false;
  if not aCheckUppLowFlag then
    i := ABPos(aItemName, aStr)
  else
    i := Pos(aItemName, aStr);

  if i > 0 then
  begin
    result:=true;
    j := ABPos(aEndStr,aStr,i+length(aItemName));
    if j <= 0 then
      j := length(aStr);

    aStr := copy(aStr, 1,i + length(aItemName)-1)+ copy(aStr, j ,length(aStr));

    if (aStr <> EmptyStr) and (aDelStrs <> EmptyStr) then
    begin
      for i := 1 to ABGetSpaceStrCount(aDelStrs, ',') do
        aStr := ABStringReplace(aStr, ABGetSpaceStr(aDelStrs, i,','), '');
    end;
  end;
end;

function ABSetItemValue(var aStr: string;
                        aItemName: string;
                        aItemValue: string;
                        aEndStr: string;
                        aCheckUppLowFlag: boolean
                        ): boolean;
var
  i, j: longint;
  temOld: string;
begin
  result:=false;
  if not aCheckUppLowFlag then
    i := ABPos(aItemName, aStr)
  else
    i := Pos(aItemName, aStr);

  if i > 0 then
  begin
    j := ABPos(aEndStr, copy(aStr, i + length(aItemName), length(aStr)));

    if j >0 then
    begin
      temOld:= copy(aStr, i + length(aItemName), j - 1);
      result:=(temOld<>aItemValue);
      if result then
        aStr:= copy(aStr, 1,i + length(aItemName)-1) +
                aItemValue+
                copy(aStr,j+(i + length(aItemName)-1),length(aStr));

    end
    else
    begin
      temOld:= copy(aStr, i + length(aItemName), length(aStr));
      result:=(temOld<>aItemValue);
      if result then
        aStr:= copy(aStr, 1,i + length(aItemName)-1) +
                aItemValue;
    end;
  end;
end;

function ABGetItemValue(aStr: string;
                        aItemName: string;
                        aDelStrs: string;
                        aEndStr: string;
                        aCheckUppLowFlag: boolean;
                        var aFind: boolean
                        ): string;
var
  i, j: longint;
begin
  result := EmptyStr;
  aFind  :=false;
  if not aCheckUppLowFlag then
    i := ABPos(aItemName, aStr)
  else
    i := Pos(aItemName, aStr);

  if i > 0 then
  begin
    j := ABPos(aEndStr, copy(aStr, i + length(aItemName), length(aStr)));
    if j = 0 then
      j := length(copy(aStr, i + length(aItemName), length(aStr))) + 1;

    result := copy(aStr, i + length(aItemName), j - 1);

    if (result <> EmptyStr) and (aDelStrs <> EmptyStr) then
    begin
      for i := 1 to ABGetSpaceStrCount(aDelStrs, ',') do
        result := ABStringReplace(result, ABGetSpaceStr(aDelStrs, i,','), '');
    end;
    aFind  :=true;
  end;
end;

function ABGetItemValue(aStr: string;
                        aItemName: string;
                        aDelStrs: string;
                        aEndStr: string;
                        aCheckUppLowFlag: boolean
                        ): string;
var
  tempFind:boolean;
begin
  result := ABGetItemValue(aStr,
                        aItemName,
                        aDelStrs,
                        aEndStr,
                        aCheckUppLowFlag,
                        tempFind
                        );
end;

function ABQuotedStr(aStr:string):string;
begin
  Result := QuotedStr(aStr);
end;

function ABUnQuotedStr( aStr:string):string;
begin
  Result:=ABStringReplace(aStr,QuotedStr(''''),'''');
end;

function ABUnQuotedFirstlastStr(aStr:string):string;
begin
  if Copy(aStr,1,1)='''' then
  begin
    aStr:=Copy(aStr,2,Length(aStr));
  end;
  if Copy(aStr,Length(aStr),1)='''' then
  begin
    aStr:=Copy(aStr,1,Length(aStr)-1);
  end;

  Result:=aStr;
end;

function ABGetLeftRightStr(aStr: string; aReturnDirection: TABXDirection;
                           aSpaceSign: string; aDelStrs: string;
                           aCheckUppLowFlag: boolean): string;
var
  i: longint;
  temstr1: string;
begin
  if aReturnDirection=axdLeft then
    result := aStr
  else
    result := EmptyStr;

  if aSpaceSign <> EmptyStr then
  begin
    if not aCheckUppLowFlag then
      i := ABPos(aSpaceSign, aStr)
    else
      i := Pos(aSpaceSign, aStr);

    if i > 0 then
    begin
      if aReturnDirection = axdLeft then
        temstr1 := copy(aStr, 1, i - 1)
      else
        temstr1 := copy(aStr, i + Length(aSpaceSign), length(aStr));

      if (temstr1 <> EmptyStr) and (aDelStrs <> EmptyStr) then
      begin
        for i := 1 to ABGetSpaceStrCount(aDelStrs, ',') do
          temstr1 := ABStringReplace(temstr1, ABGetSpaceStr(aDelStrs, i,','), '');
      end;
      result := temstr1;
    end
  end;
  result := trim(result);
end;

function ABGetItemValue(aStrings: Tstrings;
                        aItemName: string;
                        aDelStrs: string;
                        aEndStr: string;
                        aCheckUppLowFlag: boolean;
                        var aFind: boolean
                        ): string;
var
  i: longint;
  tempStr: string;
begin
  result := EmptyStr;
  for i := 0 to aStrings.Count - 1 do
  begin
    tempStr := ABStringReplace(aStrings[i], #9, '');
    result := ABGetItemValue(tempStr, aItemName, aDelStrs, aEndStr,aCheckUppLowFlag, aFind);
    if aFind then
      break;
  end;
end;

function ABGetItemValue(aStrings: Tstrings;
                        aItemName: string;
                        aDelStrs: string;
                        aEndStr: string;
                        aCheckUppLowFlag: boolean
                        ): string;
var
  temFind: boolean;
begin
  result := ABGetItemValue(aStrings,
                        aItemName,
                        aDelStrs,
                        aEndStr,
                        aCheckUppLowFlag,
                        temFind
                        );
end;

function ABFillEndSign(aStr: string; aEndSign: string): string;
begin
  result := aStr;
  if (aStr <> EmptyStr) and
     (Copy(aStr, Length(aStr)-Length(aEndSign)+1, Length(aEndSign)) <> aEndSign) then
    result := aStr + aEndSign;
end;

function ABFillStr(aStr: string; aSetLength: longint; aAddStr: string;
  aXDirection: TABXDirection): string;
var
  temstr1: string;
begin
  if length(aStr) >= aSetLength then
  begin
    result := copy(aStr, 1, aSetLength);
  end
  else
  begin
    temstr1 := EmptyStr;
    while length(temstr1 + aStr+aAddStr)<=aSetLength do
    begin
      temstr1 := temstr1 + aAddStr;
    end;

    if length(temstr1)>aSetLength then
      result := copy(temstr1, 1, aSetLength);

    if axdLeft = aXDirection then
      result := temstr1 + aStr
    else if axdright = aXDirection then
      result := aStr + temstr1;
  end;
end;

function ABGetSetCount(const aSetType;aSize: Integer): Integer;
  function GetBit1Count(aValue: Byte): Integer;
  var
    I: Integer;
  begin
    Result := 0;
    for I := 7 downto 0 do
      if aValue and (1 shl I) <> 0 then
        Inc(Result);
  end;
var
  P: PByteArray;
  I: Integer;
begin
  Result := 0;
  P := @aSetType;
  for I := 0 to aSize - 1 do
    Inc(Result, GetBit1Count(P[I]));
end;

function ABGetSetCount(aTypeInfo: PTypeInfo): Integer;
var
  ElemTypeInfo: PTypeInfo;
  ElemTypeData: PTypeData;
begin
  ElemTypeInfo := GetTypeData(aTypeInfo).CompType^;
  ElemTypeData := GetTypeData(ElemTypeInfo);
  Result:=ElemTypeData.MaxValue-ElemTypeData.MinValue+1;
end;

procedure ABStrToSet(aTypeInfo: PTypeInfo; const aValue: string; out aResult);
  function NextWord(var P: PChar): string;
  var I: Integer;
  begin
    while CharInSet(P^,[',', ' ', '[', ']']) do Inc(P);
    I := 0; while CharInSet(P[I],['A'..'Z', 'a'..'z', '0'..'9', '_']) do Inc(I);
    SetString(Result, P, I); Inc(P, I);
  end;
type TMaxSet = set of Byte;
var
  ElemTypeInfo: PTypeInfo; ElemTypeData: PTypeData; P: PChar;
  EnumName: string; I, ElemValue: Integer;
begin
  ElemTypeInfo := GetTypeData(aTypeInfo)^.CompType^;
  ElemTypeData := GetTypeData(ElemTypeInfo);
  for I := ElemTypeData^.MinValue to ElemTypeData^.MaxValue do Exclude(TMaxSet(aResult), I);
  P := PChar(aValue);
  repeat
    EnumName := NextWord(P);
    if EnumName = '' then Break;
    ElemValue := GetEnumValue(ElemTypeInfo, EnumName);
    if ElemValue < 0 then
      raise EConvertError.CreateFmt('值"%s"不正常吧!', [EnumName]);
    Include(TMaxSet(aResult), ElemValue);
  until False;
end;

function ABSetToStr(aTypeInfo: PTypeInfo; const aValue; aBrackets: Boolean = True): string;
type
  ByteSet = set of 0..7;
var
  ByteValue: ^ByteSet;
  I, M: Integer;
  ElemTypeInfo: PTypeInfo;
  ElemTypeData: PTypeData;
begin
  Result := '';
  ElemTypeInfo := GetTypeData(aTypeInfo)^.CompType^;
  ElemTypeData := GetTypeData(ElemTypeInfo);
  ByteValue := @aValue;
  for I := ElemTypeData^.MinValue to ElemTypeData^.MaxValue do
  begin
    M := I mod 8;
    if M in ByteValue^ then
    begin
      if Result <> '' then
        Result := Result + ',';
      Result := Result + GetEnumName(ElemTypeInfo, I);
    end;
    if M = 7 then Inc(ByteValue)
  end;
  if aBrackets then
    Result := '[' + Result + ']';
end;

function ABEnumNameToValue(aTypeInfo: PTypeInfo; aName: string): LongInt;
begin
  result:= GetEnumValue(aTypeInfo,aName);
end;

function ABEnumValueToName(aTypeInfo: PTypeInfo; aValue: LongInt): string;
begin
  result:=GetEnumName(aTypeInfo,Ord(aValue));
end;

function ABGetEnumCount(aTypeInfo: PTypeInfo): Integer;
var
  ElemTypeData: PTypeData;
begin
  ElemTypeData := GetTypeData(aTypeInfo);
  Result:=ElemTypeData.MaxValue-ElemTypeData.MinValue+1;
end;

function ABDeleteBegin0(aStr:string):string;
begin
  while copy(aStr,1,1)='0' do
  begin
    aStr:=copy(aStr,2,length(aStr));
  end;
  Result:=aStr;
end;

function ABNumberStrToFlag(aNumberStr:string;
                          aBeginNumber:LongInt;aEndNumber:LongInt;
                          aHaveFlag:string;aNoHaveFlag:string;
                          aSpaceSign: string
                          ):string;
var
  I: Integer;
begin
  result:=emptystr;
  aNumberStr:=aSpaceSign+aNumberStr+aSpaceSign;
  for I := aBeginNumber to aEndNumber do
  begin
    if ABPos(aSpaceSign+inttostr(i)+aSpaceSign,aNumberStr)>0 then
    begin
      result:=result+aHaveFlag;
    end
    else
      result:=result+aNoHaveFlag;
  end;
end;

function ABGetFirstClientControlByClassName(aParent: TWinControl; aClassNames:string): TControl;
var
  I: Integer;
begin
  Result:=nil;
  for I := 0 to aParent.ControlCount-1 do
  begin
    if (ABObjectInheritFrom(aParent.Controls[i],aClassNames)) then
    begin
      Result:=aParent.Controls[i];
      break;
    end;
  end;
end;

function ABPropIsType(aSelf: TObject; aPropName: string; aTypeKind: TTypeKind): Boolean;
var
  i, j: LongInt;
  tempstr: string;
begin
  result := False;
  if Assigned(aSelf) then
  begin
    j := ABGetSpaceStrCount(aPropName, '.');
    for I := 1 to j do
    begin
      tempstr := ABGetSpaceStr(aPropName, i, '.');
      if IsPublishedProp(aSelf, tempstr) then
      begin
        if i = j then
        begin
          result :=(PropIsType(aSelf, tempstr,aTypeKind));
        end
        else
        begin
          aSelf := GetObjectProp(aSelf, tempstr);
        end;
      end
      else
      begin
        result := null;
        Break;
      end
    end;
  end;
end;

function ABGetPropValue(aSelf: TObject; aPropName: string; aEmptyStr: boolean):
  Variant;
var
  i, j: LongInt;
  tempstr: string;
begin
  result := null;
  if Assigned(aSelf) then
  begin
    j := ABGetSpaceStrCount(aPropName, '.');
    for I := 1 to j do
    begin
      tempstr := ABGetSpaceStr(aPropName, i, '.');
      if IsPublishedProp(aSelf, tempstr) then
      begin
        if i = j then
        begin
          if not (PropType(aSelf, tempstr) in [tkClass, tkMethod]) then
            result := GetPropValue(aSelf, tempstr)
        end
        else
        begin
          aSelf := GetObjectProp(aSelf, tempstr);
          if Assigned(aSelf) then
          begin
            if (aSelf is TStrings) then
            begin
              result := TStrings(aSelf).Text;
              break;
            end;
          end
          else
            break;
        end;
      end
      else
      begin
        result := null;
        Break;
      end
    end;

    if (aEmptyStr) and (ABVarIsNull(result)) then
      result := EmptyStr;
  end;
end;

function ABGetObjectPropValue(aSelf: TObject; aPropName: string): TObject;
var
  i, j: LongInt;
  tempstr: string;
begin
  result := nil;
  if not Assigned(aSelf) then
    exit;

  j := ABGetSpaceStrCount(aPropName, '.');
  for I := 1 to j do
  begin
    tempstr := ABGetSpaceStr(aPropName, i, '.');
    if IsPublishedProp(aSelf, tempstr) then
    begin
      aSelf := GetObjectProp(aSelf, tempstr);
      result := aSelf;
    end
    else
    begin
      result := nil;
      Break;
    end
  end;
end;

procedure ABSetPropValue(aSelf: TObject; aPropName: string; aValue: Variant);
var
  i, j: LongInt;
  tempstr: string;
begin
  if not Assigned(aSelf) then
    exit;

  j := ABGetSpaceStrCount(aPropName, '.');
  for I := 1 to j do
  begin
    tempstr := ABGetSpaceStr(aPropName, i, '.');
    if (IsPublishedProp(aSelf, tempstr)) then
    begin
      if (i = j) then
      begin
        if not (PropType(aSelf, tempstr) in [tkClass, tkMethod]) then
          SetPropValue(aSelf, tempstr, aValue)
      end
      else
      begin
        aSelf := GetObjectProp(aSelf, tempstr);
        if Assigned(aSelf) then
        begin
          if (aSelf is TStrings) then
          begin
            TStrings(aSelf).Text := aValue;
            break;
          end;
        end
        else
          break;
      end;
    end
    else
      Break;
  end;
end;

procedure ABSetObjectPropValue(aSelf: TObject; aPropName: string; aValue:
  TObject);
var
  i, j: LongInt;
  tempstr: string;
begin
  if not Assigned(aSelf) then
    exit;

  j := ABGetSpaceStrCount(aPropName, '.');
  for I := 1 to j do
  begin
    tempstr := ABGetSpaceStr(aPropName, i, '.');
    if (IsPublishedProp(aSelf, tempstr)) then
    begin
      if (i = j) then
        SetObjectProp(aSelf, tempstr, aValue)
      else
        aSelf := GetObjectProp(aSelf, tempstr);

      if not Assigned(aself) then
        break;
    end
    else
      Break;
  end;
end;

function ABIsPublishProp(aSelf: TObject; aPropName: string): Boolean;
var
  i, j: LongInt;
  tempstr: string;
begin
  Result := false;
  if not Assigned(aSelf) then
    exit;

  j := ABGetSpaceStrCount(aPropName, '.');
  for I := 1 to j do
  begin
    tempstr := ABGetSpaceStr(aPropName, i, '.');
    if i = j then
      Result := IsPublishedProp(aSelf, tempstr)
    else if (IsPublishedProp(aSelf, tempstr)) then
    begin
      aSelf := GetObjectProp(aSelf, tempstr);
      if Assigned(aSelf) then
      begin
        if (aSelf is  TStrings) then
        begin
          Result := True;
          Break;
        end;
      end
      else
        break;
    end
    else
      Break;
  end;
end;

function ABGetStringsProperty(aSelf: TObject; aPropName: string): TStrings;
var
  i, j: LongInt;
  tempstr: string;
begin
  Result := nil;
  if not Assigned(aSelf) then
    exit;

  j := ABGetSpaceStrCount(aPropName, '.');
  for I := 1 to j do
  begin
    tempstr := ABGetSpaceStr(aPropName, i, '.');
    if (IsPublishedProp(aSelf, tempstr)) then
    begin
      aSelf := GetObjectProp(aSelf, tempstr);
      if Assigned(aSelf) then
      begin
        if (aSelf is  TStrings) then
        begin
          Result := TStrings(aSelf);
          break;
        end;
      end
      else
        break;
    end
    else
      Break;
  end;
end;

function ABGetMethodProp(aSelf: TObject; aPropName:string): TMethod;
var
  i, j: LongInt;
  tempstr: string;
begin
  if not Assigned(aSelf) then
    exit;

  j := ABGetSpaceStrCount(aPropName, '.');
  for I := 1 to j do
  begin
    tempstr := ABGetSpaceStr(aPropName, i, '.');
    if i = j then
      result := GetMethodProp(aSelf, tempstr)
    else if IsPublishedProp(aSelf, tempstr) then
      aSelf := GetObjectProp(aSelf, tempstr)
    else
      Break;

    if not Assigned(aSelf) then
      exit;

  end;
end;

function ABExecNoParamPublishProp(aObject:TObject;aProcName: string):boolean;
type
  TMyProc = procedure of object;
var
  theProc:TMyProc;
begin
  result:=false;
  if Assigned(aObject) then
  begin
    TMethod(theProc).Code:=aObject.MethodAddress(aProcName);
    if Assigned(TMethod(theProc).Code) then
    begin
      TMethod(theProc).Data:=aObject;
      theProc;
      result:=True;
    end;
  end;
end;

procedure ABSetMethodProp(aSelf: TObject; aPropName: string; aValue: TMethod);
var
  i, j: LongInt;
  tempstr: string;
begin
  if not Assigned(aSelf) then
    exit;

  j := ABGetSpaceStrCount(aPropName, '.');
  for I := 1 to j do
  begin
    tempstr := ABGetSpaceStr(aPropName, i, '.');
    if i = j then
      SetMethodProp(aSelf, tempstr, aValue)
    else if IsPublishedProp(aSelf, tempstr) then
      aSelf := GetObjectProp(aSelf, tempstr)
    else
      Break;
    if not Assigned(aSelf) then
      exit;
  end;
end;

function ABGetDataSetProperty(aSelf: TPersistent; aCheckAllProperty: Boolean):TDataSet;
begin
  result := nil;
  if ABIsPublishProp(aSelf, 'DataSet') then
  begin
    result := TDataSet(ABGetObjectPropValue(aSelf, 'DataSet'));
    if (Assigned(result)) or (not aCheckAllProperty) then
      exit;
  end;
  if (ABIsPublishProp(aSelf, 'ABDataSet')) then
  begin
    result := TDataSet(ABGetObjectPropValue(aSelf, 'ABDataSet'));
    if (Assigned(result)) or (not aCheckAllProperty) then
      exit;
  end;
  if (ABIsPublishProp(aSelf, 'DataSource')) then
  begin
    result := TDataSource(ABGetObjectPropValue(aSelf, 'DataSource')).DataSet;
    if (Assigned(result)) or (not aCheckAllProperty) then
      exit;
  end;
  if (ABIsPublishProp(aSelf, 'ABDataSource')) then
  begin
    result := TDataSource(ABGetObjectPropValue(aSelf, 'ABDataSource')).DataSet;
    if (Assigned(result)) or (not aCheckAllProperty) then
      exit;
  end;

  if (ABIsPublishProp(aSelf, 'DataController.DataSource')) then
  begin
    result := TDataSource(ABGetObjectPropValue(aSelf,'DataController.DataSource')).DataSet;
    if (Assigned(result)) or (not aCheckAllProperty) then
      exit;
  end;
  if (ABIsPublishProp(aSelf, 'DataController.DataSet')) then
  begin
    result := TDataSet(ABGetObjectPropValue(aSelf, 'DataController.DataSet'));
    if (Assigned(result)) or (not aCheckAllProperty) then
      exit;
  end;

  if (ABIsPublishProp(aSelf, 'Databinding.DataSource')) then
  begin
    result := TDataSet(ABGetObjectPropValue(aSelf, 'Databinding.DataSource'));
    if (Assigned(result)) or (not aCheckAllProperty) then
      exit;
  end;

  if (aSelf is TDataSet) then
  begin
    result := TDataSet(aSelf);
    if (Assigned(result)) or (not aCheckAllProperty) then
      exit;
  end;
  if (aSelf is TDataSource) then
  begin
    result := TDataSource(aSelf).DataSet;
    if (Assigned(result)) or (not aCheckAllProperty) then
      exit;
  end;
end;

function ABGetClassFileName(aClass: TClass): String;
var
  szfilename:array[0..MAX_PATH] of Char;
begin
  FillChar(szfilename,SizeOf(szfilename),#0);
  GetModuleFileName(FindClassHInstance(aClass),szfilename,MAX_PATH);
  result:=szfilename;
end;

function ABGetClassUnitName(aClass: TClass): String;
var
  PTD: PTypeData;
begin
  Result:=EmptyStr;
  if not AClass.InheritsFrom(TPersistent) then
    Exit;

  PTD := GetTypeData(AClass.ClassInfo);
  result:=PTD.UnitName;
end;

procedure ABGetParentClassLists(aClass: TClass;
                                aStrings: TStrings;
                                aHaveClassName:Boolean;
                                aHaveUnitName:Boolean;
                                aHaveFileName:Boolean;
                                aSpaceFlag:string);
var
  PTD: PTypeData;
  function DoItem:string;
  begin
    result:=EmptyStr;
    if aHaveClassName then
      ABAddstr(result,AClass.ClassName,aSpaceFlag);
    if aHaveUnitName then
      ABAddstr(result,PTD.UnitName,aSpaceFlag);
    if aHaveFileName then
      ABAddstr(result,ABGetClassFileName(aClass),aSpaceFlag);
  end;
begin
  aStrings.Clear;
  if not AClass.InheritsFrom(TPersistent) then
    Exit;

  PTD := GetTypeData(AClass.ClassInfo);
  aStrings.Add(DoItem);
  repeat
    PTD := GetTypeData(AClass.ClassInfo);
    AClass := AClass.ClassParent;

    aStrings.Add(DoItem);
  until not AClass.InheritsFrom(TPersistent);
end;

procedure ABGetClassProperties(aSelf: TObject; AStrings: TStrings);
var
  PropList: PPropList;
  ClassTypeInfo: PTypeInfo;
  ClassTypeData: PTypeData;
  i: integer;
  NumProps: Integer;
begin
  if not Assigned(aSelf) then
    exit;

  ClassTypeInfo := aSelf.ClassType.ClassInfo;
  ClassTypeData := GetTypeData(ClassTypeInfo);
  if ClassTypeData.PropCount <> 0 then
  begin
    GetMem(PropList, SizeOf(PPropInfo) * ClassTypeData.PropCount);
    try
      GetPropInfos(aSelf.ClassInfo, PropList);
      for i := 0 to ClassTypeData.PropCount - 1 do
        if not (PropList[i]^.PropType^.Kind = tkMethod) then
          AStrings.Add(Format('%s:  %s', [PropList[i]^.Name,
            PropList[i]^.PropType^.Name]));
      NumProps := GetPropList(aSelf.ClassInfo, [tkMethod], PropList);
      if NumProps <> 0 then
      begin
        AStrings.Add('');
        AStrings.Add('      EVENTS      ================  ');
        AStrings.Add('');
      end;
      //  Fill  the  AStrings  with  the  events.
      for i := 0 to NumProps - 1 do
        AStrings.Add(Format('%s:  %s', [PropList[i]^.Name,
          PropList[i]^.PropType^.Name]));

    finally
      FreeMem(PropList, SizeOf(PPropInfo) * ClassTypeData.PropCount);
    end;
  end;
end;

procedure DoStrsToStrings(aStrs: string;
                          aSpaceSign: string;
                          aType:longint;
                          aStrings: Tstrings;
                          aNoFillSame: boolean;
                          aIsDelOld: boolean);
var
  i: LongInt;
  tempstr: string;
  procedure DoItem(aStr: string);
  begin
    if (aStr <> EmptyStr)then
    begin
      if aNoFillSame then
      begin
        if aStrings.IndexOf(aStr) < 0 then
        begin
          aStrings.Add(aStr);
        end;
      end
      else
      begin
        aStrings.Add(aStr);
      end;
    end;
  end;
begin
  aStrings.BeginUpdate;
  try
    if aIsDelOld then
      aStrings.Clear;

    if aSpaceSign<>EmptyStr then
    begin
      for I := 1 to ABGetSpaceStrCount(aStrs, aSpaceSign) do
      begin
        case aType of
          1:
          begin
            tempstr := ABGetSpaceStr(aStrs, i, aSpaceSign);
          end;
          2:
          begin
            tempstr := ABGetLeftRightStr(ABGetSpaceStr(aStrs, i, aSpaceSign));
          end;
          3:
          begin
            tempstr := ABGetLeftRightStr(ABGetSpaceStr(aStrs, i, aSpaceSign),axdRight);
          end;
        end;

        DoItem(tempstr);
      end;
    end
    else
    begin
      DoItem(aStrs);
    end;
  finally
    aStrings.EndUpdate;
  end;
end;

procedure ABStrsToStrings(aStrs: string;
                          aSpaceSign: string;
                          aStrings: Tstrings;
                          aNoFillSame: boolean;
                          aIsDelOld: boolean);
begin
  DoStrsToStrings(aStrs,aSpaceSign,1,aStrings,aNoFillSame,aIsDelOld);
end;

procedure ABNameofStrsToStrings(aStrs: string;
                          aSpaceSign: string;
                          aStrings: Tstrings;
                          aNoFillSame: boolean;
                          aIsDelOld: boolean);
begin
  DoStrsToStrings(aStrs,aSpaceSign,2,aStrings,aNoFillSame,aIsDelOld);
end;

procedure ABValueofStrsToStrings(aStrs: string;
                          aSpaceSign: string;
                          aStrings: Tstrings;
                          aNoFillSame: boolean;
                          aIsDelOld: boolean);
begin
  DoStrsToStrings(aStrs,aSpaceSign,3,aStrings,aNoFillSame,aIsDelOld);
end;

function DoStringsToStrs(aStrings: Tstrings;aType:LongInt; aSpaceSign: string): string;
var
  i: LongInt;
  tempstr: string;
begin
  result := emptystr;
  for I := 0 to aStrings.count - 1 do
  begin
    case aType of
      1:
      begin
        tempstr := aStrings[i];
      end;
      2:
      begin
        tempstr := aStrings.Names[i];
      end;
      3:
      begin
        tempstr := aStrings.ValueFromIndex[i];
      end;
    end;

    if tempstr <> EmptyStr then
    begin
      ABAddstr(Result,tempstr,aSpaceSign);
    end;
  end;
end;

function ABStringsToStrs(aStrings: Tstrings; aSpaceSign: string): string;
begin
  result := DoStringsToStrs(aStrings,1,aSpaceSign);
end;

function ABNameOfStringsToStrs(aStrings: Tstrings; aSpaceSign: string): string;
begin
  result := DoStringsToStrs(aStrings,2,aSpaceSign);
end;

function ABValueOfStringsToStrs(aStrings: Tstrings; aSpaceSign: string): string;
begin
  result := DoStringsToStrs(aStrings,3,aSpaceSign);
end;

procedure ABStringsToStrings( aSStrings: TStrings;
                              aDStrings: TStrings;
                              aNoFillSame: boolean;
                              aIsDelOld: boolean
                            );
var
  i: longint;
  tempStr1: string;
begin
  aSStrings.BeginUpdate;
  aDStrings.BeginUpdate;
  try
    if aIsDelOld then
      aDStrings.Clear;

    for I := 0 to aSStrings.Count - 1 do
    begin
      tempStr1 := aSStrings.Strings[i];
      if aNoFillSame then
      begin
        if aDStrings.IndexOf(tempStr1) < 0 then
        begin
          aDStrings.Add(tempStr1);
        end;
      end
      else
      begin
        aDStrings.Add(tempStr1);
      end;
    end;
  finally
    aSStrings.EndUpdate;
    aDStrings.EndUpdate;
  end;
end;

procedure ABListBoxItemMoveToListBox(aSListBox:TCustomListBox; aIndex: LongInt;aEndSign: string; aDListBox: TCustomListBox;aSListBoxNextSelect:boolean);
var
  tempStr1:string;
begin
  if aIndex >= 0 then
  begin
    aSListBox.Items.BeginUpdate;
    aDListBox.Items.BeginUpdate;
    try
      tempStr1:=aSListBox.Items[aIndex];
      if aEndSign<>EmptyStr then
        ABGetLeftRightStr(tempStr1,axdLeft, aEndSign);

      aDListBox.Items.Add(tempStr1);
      aSListBox.Items.Delete(aIndex);
      if aSListBoxNextSelect then
      begin
        if aIndex > aSListBox.Count - 1 then
          aIndex := aSListBox.Count - 1;

        if aIndex>=0 then
        begin
          if (aSListBox.MultiSelect) then
          begin
            if (aSListBox.SelCount = 0) then
            begin
              aSListBox.Selected[aIndex] := true;
            end;
          end
          else
            aSListBox.ItemIndex := aIndex;
        end;
      end;
    finally
      aSListBox.Items.EndUpdate;
      aDListBox.Items.EndUpdate;
    end;
  end;
end;

procedure ABListBoxUp(aListBox: TCustomListBox; aIndex: LongInt; aStep: LongInt);
var
  i: LongInt;
begin
  if (aIndex >= 0) and
     (aIndex <= aListBox.Count - 1) then
  begin
    i := aIndex - aStep;
    if i < 0 then
      i := 0;
    if aIndex <> i then
    begin
      aListBox.Items.Move(aIndex, i);
      if aListBox.MultiSelect then
        aListBox.Selected[i] := true
      else
        aListBox.ItemIndex := i;
    end;
  end;
end;

procedure ABListBoxDown(aListBox: TCustomListBox; aIndex: LongInt; aStep: LongInt);
var
  i: LongInt;
begin
  if (aIndex >= 0) and (aIndex + aStep >= 0) and
    (aIndex <= aListBox.Count - 1) then
  begin
    i := aIndex + aStep;
    if i > aListBox.Count - 1 then
      i := aListBox.Count - 1;

    if aIndex <> i then
    begin
      aListBox.Items.Move(aIndex, i);
      if aListBox.MultiSelect then
        aListBox.Selected[i] := true
      else
        aListBox.ItemIndex := i;
    end;
  end;
end;

procedure ABListBoxSelectsCopyStrings(aListBox: TCustomListBox;
                               aStrings:TStrings;
                               aNoFillSame: boolean;
                               aIsDelOld: boolean );
var
  i: LongInt;
begin
  if aIsDelOld then
  begin
    aStrings.Clear;
  end;
  for I := 0 to aListBox.Count-1 do
  begin
    if (aListBox is TListBox) and (aListBox.Selected[i]) or
       (aListBox is TCheckListBox) and (TCheckListBox(aListBox).Checked[i])
         then
    begin
      if aNoFillSame then
      begin
        if aStrings.IndexOf(aListBox.Items[i]) < 0 then
        begin
          aStrings.Add(aListBox.Items[i]);
        end;
      end
      else
      begin
        aStrings.Add(aListBox.Items[i]);
      end;
    end;
  end;
end;

procedure ABListBoxCopyStrings(aListBox: TCustomListBox;aBegIndex,aCount:longint; aStrings:TStrings;
                               aNoFillSame: boolean;
                               aIsDelOld: boolean );
var
  i,tempCount: LongInt;
begin
  if aIsDelOld then
  begin
    aStrings.Clear;
  end;

  tempCount:=min(aBegIndex+aCount-1,aListBox.Count-1);
  for I := aBegIndex to tempCount do
  begin
    if aNoFillSame then
    begin
      if aStrings.IndexOf(aListBox.Items[i]) < 0 then
      begin
        aStrings.Add(aListBox.Items[i]);
      end;
    end
    else
    begin
      aStrings.Add(aListBox.Items[i]);
    end;
  end;
end;

procedure ABSaveListBoxSelect(aListBox: TCustomListBox);
var
  i: LongInt;
  tempPubFileName,
  tempStr1: string;
begin
  if (not Assigned(aListBox.Owner)) then
    exit;
  if (aListBox.Owner.Name=EmptyStr) or (aListBox.Name=EmptyStr) then
    exit;

  tempPubFileName := ABDefaultValuePath+'PubDefaultValue.ini';
  for i := 0 to aListBox.Count - 1 do
  begin
    if aListBox is TCheckListBox then
    begin
      tempStr1:=ABBoolToStr(TCheckListBox(aListBox).Checked[i]);
    end
    else if aListBox is TListBox then
    begin
      tempStr1:=ABBoolToStr(TListBox(aListBox).Selected[i]);
    end
    else
    begin
      Continue
    end;
    ABWriteInI(aListBox.Owner.Name+'_'+aListBox.Name + '_Selects',
               aListBox.Items[i],
               tempStr1,
               tempPubFileName);
  end;
end;

procedure ABLoadListBoxSelect(aListBox: TCustomListBox);
var
  i: LongInt;
  tempPubFileName: string;
  tempBoolean1: Boolean;
  tempStrings:TStrings;
begin
  tempPubFileName := ABDefaultValuePath+'PubDefaultValue.ini';
  if not ABCheckFileExists(tempPubFileName) then
    exit;
  if (not Assigned(aListBox.Owner)) then
    exit;
  if (aListBox.Owner.Name=EmptyStr) or (aListBox.Name=EmptyStr) then
    exit;

  tempStrings := TStringList.Create;
  try
    ABReadInI(aListBox.Owner.Name+'_'+aListBox.Name + '_Selects',tempPubFileName,tempStrings);
    if tempStrings.Count>0 then
    begin
      for i := 0 to aListBox.Count - 1 do
      begin
        if tempStrings.IndexOfName(aListBox.Items[i])>=0 then
        begin
          tempBoolean1:=ABStrToBool(tempStrings.Values[aListBox.Items[i]]);
          if aListBox is TCheckListBox then
          begin
            TCheckListBox(aListBox).Checked[i]:=tempBoolean1 ;
          end
          else if aListBox is TListBox then
          begin
            TListBox(aListBox).Selected[i]:=tempBoolean1 ;
          end;
        end;
      end;
    end;
  finally
    tempStrings.Free;
  end;
end;

procedure ABSetListBoxSelect(aListBox: TCustomListBox; aType: LongInt);
var
  i: LongInt;
begin
  for i := 0 to aListBox.Count - 1 do
  begin
    case aType of
      0:
      begin
        if aListBox is TCheckListBox then
        begin
          if not TCheckListBox(aListBox).Checked[i] then
            TCheckListBox(aListBox).Checked[i] := true;
        end
        else if aListBox is TListBox then
        begin
          if not TListBox(aListBox).Selected[i] then
            TListBox(aListBox).Selected[i] := true;
        end;
      end;
      1:
      begin
        if aListBox is TCheckListBox then
        begin
          if TCheckListBox(aListBox).Checked[i] then
            TCheckListBox(aListBox).Checked[i] := false;
        end
        else if aListBox is TListBox then
        begin
          if TListBox(aListBox).Selected[i] then
            TListBox(aListBox).Selected[i] := false;
        end;
      end;
      2:
      begin
        if aListBox is TCheckListBox then
        begin
          TCheckListBox(aListBox).Checked[i] := not TCheckListBox(aListBox).Checked[i];
        end
        else if aListBox is TListBox then
        begin
          TListBox(aListBox).Selected[i] := not TListBox(aListBox).Selected[i];
        end;
      end;
    end;
  end;
end;

procedure ABSetListBoxWidth(aListBox: TCustomListBox; aWidth: LongInt);
var
  i: LongInt;
begin
  if aWidth = 0 then
  begin
    for i := 0 to aListBox.Count - 1 do
    begin
      if aWidth < Length(aListBox.Items[i]) then
        aWidth := Length(aListBox.Items[i]);
    end;
  end;
  SendMessage(aListBox.Handle, LB_SETHORIZONTALEXTENT, aWidth * 8, 0);
end;

procedure ABDeleteInStringsItem(aSStrings, aDStrings: Tstrings);
var
  i, j: LongInt;
begin
  aSStrings.BeginUpdate;
  aDStrings.BeginUpdate;
  try
    for I := 0 to aDStrings.count - 1 do
    begin
      j := aSStrings.IndexOf(aDStrings[i]);
      if j >= 0 then
      begin
        aSStrings.Delete(j);
      end;
    end;
  finally
    aSStrings.EndUpdate;
    aDStrings.EndUpdate;
  end;
end;

function ABInsertInStrs(aStrs: string;aIndex:longint; aStr: string): string;
var
  tempStrings:TStrings;
begin
  tempStrings := TStringList.Create;
  try
    tempStrings.Text:=aStrs;
    tempStrings.Insert(aIndex,aStr);
    result:=tempStrings.Text;
  finally
    tempStrings.Free;
  end;
end;

function ABEditInStrs(aStrs: string;aIndex:longint; aNewStr: string): string;
var
  tempStrings:TStrings;
begin
  tempStrings := TStringList.Create;
  try
    tempStrings.Text:=aStrs;
    tempStrings[aIndex]:=aNewStr;
    result:=tempStrings.Text;
  finally
    tempStrings.Free;
  end;
end;

function ABDeleteInStrs(aStrs: string; aIndex:longint): string;
var
  tempStrings:TStrings;
begin
  tempStrings := TStringList.Create;
  try
    tempStrings.Text:=aStrs;
    tempStrings.Delete(aIndex);
    result:=tempStrings.Text;
  finally
    tempStrings.Free;
  end;
end;

function ABGetDelphiRegPath(aDelphiVersion: TABDelphiVersion): string;
begin
  result := EmptyStr;
  case aDelphiVersion of
    dvD7:
      begin
        result := 'SOFTWARE\Borland\Delphi\7.0';
      end;
    dvD2007:
      begin
        result := 'SOFTWARE\CodeGear\BDS\5.0';
      end;
    dvD2009:
      begin
        result := 'SOFTWARE\CodeGear\BDS\6.0';
      end;
    dvD2010:
      begin
        result := 'SOFTWARE\CodeGear\BDS\7.0';
      end;
    dvDXE1:
      begin
        result := 'SOFTWARE\Embarcadero\BDS\8.0';
      end;
    dvDXE2:
      begin
        result := 'SOFTWARE\Embarcadero\BDS\9.0';
      end;
    dvDXE3:
      begin
        result := 'SOFTWARE\Embarcadero\BDS\10.0';
      end;
    dvDXE4:
      begin
        result := 'SOFTWARE\Embarcadero\BDS\12.0';
      end;
    dvDXE5:
      begin
        result := 'SOFTWARE\Embarcadero\BDS\13.0';
      end;
    dvDXE6:
      begin
        result := 'SOFTWARE\Embarcadero\BDS\14.0';
      end;
    dvDXE7:
      begin
        result := 'SOFTWARE\Embarcadero\BDS\15.0';
      end;
    dvDXE8:
      begin
        result := 'SOFTWARE\Embarcadero\BDS\16.0';
      end;
    dvDXE10:
      begin
        result := 'SOFTWARE\Embarcadero\BDS\17.0';
      end;
  end;
end;

function ABGetDelphiLibraryRegPath(aDelphiVersion: TABDelphiVersion): string;
begin
  case aDelphiVersion of
    dvD7:
      begin
        result := '\Library';
      end;
    dvD2007:
      begin
        result := '\Library';
      end;
    dvD2009:
      begin
        result := '\Library';
      end;
    dvD2010:
      begin
        result := '\Library';
      end;
    dvDXE1:
      begin
        result := '\Library';
      end;
    dvDXE2:
      begin
        result := '\Library';
      end;
    dvDXE3:
      begin
        result := '\Library';
      end;
    dvDXE4:
      begin
        result := '\Library';
      end;
    dvDXE5:
      begin
        result := '\Library';
      end;
    dvDXE6:
      begin
        result := '\Library';
      end;
    dvDXE7:
      begin
        result := '\Library\Win32';
      end;
    dvDXE8:
      begin
        result := '\Library\Win32';
      end;
    dvDXE10:
      begin
        result := '\Library\Win32';
      end;
  end;
  result := ABGetDelphiRegPath(aDelphiVersion)+result;
end;

function ABGetDelphiSeachPath(aDelphiVersion: TABDelphiVersion): string;
begin
  result := ABReplaceEnvironment(ABReadRegKey(ABGetDelphiLibraryRegPath(aDelphiVersion), 'Search Path',HKEY_CURRENT_USER));
end;

function ABGetDelphiSetupPath(aDelphiVersion: TABDelphiVersion): string;
begin
  result := ABReplaceEnvironment(ABReadRegKey(ABGetDelphiRegPath(aDelphiVersion), 'RootDir',HKEY_CURRENT_USER));
end;

function ABGetDelphiAppLongFileName(aDelphiVersion: TABDelphiVersion): string;
begin
  result := ABReplaceEnvironment(ABReadRegKey(ABGetDelphiRegPath(aDelphiVersion), 'App',HKEY_CURRENT_USER));
end;

function ABGetDelphiBplPath(aDelphiVersion: TABDelphiVersion): string;
begin
  result := ABReplaceEnvironment(ABReadRegKey(ABGetDelphiLibraryRegPath(aDelphiVersion), 'Package DPL Output'));
end;

function ABGetDelphiDcpPath(aDelphiVersion: TABDelphiVersion): string;
begin
  result := ABReplaceEnvironment(ABReadRegKey(ABGetDelphiLibraryRegPath(aDelphiVersion), 'Package DCP Output'));
end;

function ABGetDelphiDccLongFileName(aDelphiVersion: TABDelphiVersion): string;
begin
  result := ABReplaceEnvironment(ABReadRegKey(ABGetDelphiRegPath(aDelphiVersion), 'RootDir')+'bin\dcc32.exe');
  if not ABCheckFileExists(result) then
    result:=EmptyStr;
end;

function ABGetDelphiCfgLongFileName(aDelphiVersion: TABDelphiVersion): string;
begin
  result := ABReplaceEnvironment(ABReadRegKey(ABGetDelphiRegPath(aDelphiVersion), 'RootDir')+'bin\dcc32.cfg');
  if not ABCheckFileExists(result) then
    result:=EmptyStr;
end;

function ABGetDelphiDofExt(aDelphiVersion:TABDelphiVersion):string;
begin
  result:=EmptyStr;
  case aDelphiVersion of
    dvD7,dvD2007:
    begin
      result:='dof';
    end;
    dvD2009,dvD2010,dvDXE1,dvDXE2,dvDXE3,dvDXE4,dvDXE5,dvDXE6,dvDXE7,dvDXE8,dvDXE10:
    begin
      result:='dproj';
    end;
  end;
end;

function ABGetDelphiVersion(aGroupFileName:string):TABDelphiVersion;
var
  tempFileExt:string;
begin
  Result:=dvnull;
  tempFileExt:=ABGetFileExt(aGroupFileName);
  if AnsiCompareText(tempFileExt,'bpg')=0 then
  begin
    Result:=dvD7;
  end
  {
  else if AnsiCompareText(tempFileExt,'bpg')=0 then
  begin
    Result:=dvD2007;
  end
  }
  else if AnsiCompareText(tempFileExt,'groupproj')=0 then
  begin
    Result:=dvD2009;
    {
  end
  else if AnsiCompareText(tempFileExt,'groupproj')=0 then
  begin
    Result:=dvD2010;
  end
  else if AnsiCompareText(tempFileExt,'groupproj')=0 then
  begin
    Result:=dvDXE;
  end
  else if AnsiCompareText(tempFileExt,'groupproj')=0 then
  begin
    Result:=dvDXE2;
  end
  else if AnsiCompareText(tempFileExt,'groupproj')=0 then
  begin
    Result:=dvDXE3;
    }
  end;
end;

function ABReplaceEnvironment(aStr:string;aBegFlag:string;aEndFlag:string):String;
var
  tempEnvironmentName,
  tempEnvironmentValue:string;
  tempFindPosition,
  tempBeginFindPosition,
  tempEnvironmentindex: Integer;
  tempStrings:TStrings;
  I: Integer;
begin
  result :=aStr;
  tempStrings:=ABGetEnvironmentList;
  if (aBegFlag=EmptyStr) and
     (aEndFlag=EmptyStr) then
  begin
    for I := 0 to tempStrings.Count-1 do
    begin
      result := ABStringReplace(result,tempStrings.Names[i], tempStrings.ValueFromIndex[i]);
    end;
  end
  else
  begin
    tempBeginFindPosition:=1;
    tempFindPosition:=ABPos(aBegFlag,result,tempBeginFindPosition);
    while tempFindPosition>0 do
    begin
      tempEnvironmentName := ABGetItemValue(result, aBegFlag, '', aEndFlag);
      tempEnvironmentindex:=tempStrings.IndexOfName(tempEnvironmentName);
      if tempEnvironmentindex>=0 then
      begin
        tempEnvironmentValue:=tempStrings.ValueFromIndex[tempEnvironmentindex];
        result := ABStringReplace(result, aBegFlag+tempEnvironmentName+aEndFlag, tempEnvironmentValue);
        tempBeginFindPosition:= tempFindPosition+length(tempEnvironmentValue);
      end
      else
      begin
        tempBeginFindPosition:= tempFindPosition+length(aBegFlag+tempEnvironmentName+aEndFlag);
      end;

      tempFindPosition:=ABPos(aBegFlag,result,tempBeginFindPosition);
    end;
  end;
end;

function ABGetLastErrorString: string;
begin
  Result := SysErrorMessage(GetLastError);
end;

function ABWaitAppEnd(aFileName: string;aShowType: Integer;aParams: string): Boolean;
var
  ShellExInfo: TShellExecuteInfo;
begin
  if AnsiCompareText(ABGetFileExt(aFileName),'bat')=0 then
    SetCurrentDir(extractfilepath(aFileName));

  FillChar(ShellExInfo, SizeOf(ShellExInfo), 0);
  ShellExInfo.cbSize := SizeOf(ShellExInfo);
  ShellExInfo.fMask := see_Mask_NoCloseProcess;
  ShellExInfo.Wnd := Application.Handle;
  ShellExInfo.lpFile := PChar(aFileName);
  ShellExInfo.lpParameters := PChar(aParams);
  ShellExInfo.nShow := aShowType;
  Result := ShellExecuteEx(@ShellExInfo);
  if Result then
  begin
    while WaitForSingleObject(ShellExInfo.HProcess, 100) = WAIT_TIMEOUT do
    begin
      Application.ProcessMessages;
      if Application.Terminated then
        Break;
    end;
  end;
end;

function ABGetLastWsaErrorStr: string;
begin
  Result := SysErrorMessage(WSAGetLastError);
end;

function ABGetEnvironmentList:TStrings;
var
  EnvPt, TmpPt: PChar;
  I: Integer;
  tempStrings:TStrings;
begin
  if not Assigned(FEnvironmentStrings) then
  begin
    FEnvironmentStrings:=TStringList.Create;
    if ABCheckFileExists(ABGetFilepath(ABGetDelphiAppLongFileName(ABDelphiVersion))+'rsvars.bat') then
    begin
      tempStrings:=TStringList.Create;
      try
        FEnvironmentStrings.LoadFromFile(ABGetFilepath(ABGetDelphiAppLongFileName(ABDelphiVersion))+'rsvars.bat');
        for I := 0 to FEnvironmentStrings.Count-1 do
        begin
          if Trim(FEnvironmentStrings[i])<>EmptyStr then
          begin
            FEnvironmentStrings[i]:=trim(ABStringReplace(FEnvironmentStrings[i],'@SET',''));
            if AnsiCompareText(FEnvironmentStrings.Names[i],'BDS')=0 then
            begin
              if tempStrings.IndexOf('BDSLIB='+FEnvironmentStrings.ValueFromIndex[i]+'\Lib')<=0 then
                tempStrings.Add('BDSLIB='+FEnvironmentStrings.ValueFromIndex[i]+'\Lib');
              if tempStrings.IndexOf('Platform=win32')<=0 then
                tempStrings.Add('Platform=win32');
            end;
          end;
        end;
        FEnvironmentStrings.AddStrings(tempStrings);
      finally
        tempStrings.Free;
      end;
    end;

    EnvPt := GetEnvironmentStrings();
    try
      TmpPt := EnvPt;
      while (TmpPt^ <> #0) do
      begin
        if (TmpPt^ <> '=') then
        begin
          if FEnvironmentStrings.IndexOf(TmpPt)<=0 then
            FEnvironmentStrings.Add(TmpPt);
          Inc(TmpPt, Length(FEnvironmentStrings[FEnvironmentStrings.Count - 1]) + 1);
        end
        else
        begin
          while (TmpPt^ <> #0) do Inc(TmpPt);
          Inc(TmpPt); // 下一字符串
        end;
      end;
      TStringList(FEnvironmentStrings).Sorted:=true;
    finally
      FreeEnvironmentStrings(EnvPt);
    end;
  end;
  result:=FEnvironmentStrings;
end;

procedure ABCreateDelphiCfgFile(aDelphiVersion: TABDelphiVersion;
                      aCfgFileName: string;

                      aDcpOutPath,
                      aBPLOutPath,
                      aExeOutPath:string;

                      aLibraryPath: Tstrings;
                      aAddDefaultSearchPath: Boolean;
                      aShortFileNameModel: Boolean);
var
  i: LongInt;
  tempStr1,
  tempDefaultSearchPath,
  tempItemSum: string;
  tempFileStrings: TStrings;
begin
  if aShortFileNameModel then
  begin
    aDcpOutPath := ABGetShortPath(aDcpOutPath);
    aBPLOutPath := ABGetShortPath(aBPLOutPath);
    aExeOutPath := ABGetShortPath(aExeOutPath);
  end;

  tempItemSum := '"' + ABReplaceEnvironment('$(BDSLIB)\$(Platform)\release')+ '"';
  tempFileStrings := TStringList.Create;
  try
    if Assigned(aLibraryPath) then
    begin
      for I := 0 to aLibraryPath.Count-1 do
      begin
        tempStr1 := ABReplaceEnvironment(aLibraryPath[i]);
        if aShortFileNameModel then
          tempStr1 := ABGetShortPath(tempStr1);

        if tempStr1 <> EmptyStr then
        begin
          if ABPos('"' + tempStr1 + '";',tempItemSum+';')<=0 then
          begin
            ABAddstr(tempItemSum,'"' + tempStr1 + '"',';');
          end;
        end;
      end;
    end;

    if aAddDefaultSearchPath then
    begin
      tempDefaultSearchPath := ABReplaceEnvironment(ABReadRegKey(ABGetDelphiRegPath(aDelphiVersion) + '\Library',
        'Search Path',   HKEY_CURRENT_USER));

      for I := 1 to ABGetSpaceStrCount(tempDefaultSearchPath, ';') do
      begin
        tempStr1 := ABReplaceEnvironment(ABGetSpaceStr(tempDefaultSearchPath, i, ';'));
        if aShortFileNameModel then
        begin
          tempStr1 := ABGetShortPath(tempStr1);
        end;

        if tempStr1 <> EmptyStr then
        begin
          if ABPos('"' + tempStr1 + '";',tempItemSum+';')<=0 then
            ABAddstr(tempItemSum,'"' + tempStr1 + '"',';');
        end;
      end;
    end;

    //dcc32.cfg 全局文件
    if AnsiCompareText(ABGetFilename(aCfgFileName),'dcc32')=0 then
    begin
      tempFileStrings.Add(trim('-AWinTypes=Windows;WinProcs=Windows;DbiTypes=BDE;DbiProcs=BDE;DbiErrs=BDE'));
    end
    else
    //Dpk文件.cfg 项目文件
    begin
      tempFileStrings.Add(trim('-$A8                                                                       '));
      tempFileStrings.Add(trim('-$B-                                                                       '));
      tempFileStrings.Add(trim('-$C+                                                                       '));
      tempFileStrings.Add(trim('-$D+                                                                       '));
      tempFileStrings.Add(trim('-$E-                                                                       '));
      tempFileStrings.Add(trim('-$F-                                                                       '));
      tempFileStrings.Add(trim('-$G+                                                                       '));
      tempFileStrings.Add(trim('-$H+                                                                       '));
      tempFileStrings.Add(trim('-$I+                                                                       '));
      tempFileStrings.Add(trim('-$J-                                                                       '));
      tempFileStrings.Add(trim('-$K-                                                                       '));
      tempFileStrings.Add(trim('-$L+                                                                       '));
      tempFileStrings.Add(trim('-$M-                                                                       '));
      tempFileStrings.Add(trim('-$N+                                                                       '));
      tempFileStrings.Add(trim('-$O+                                                                       '));
      tempFileStrings.Add(trim('-$P+                                                                       '));
      tempFileStrings.Add(trim('-$Q-                                                                       '));
      tempFileStrings.Add(trim('-$R-                                                                       '));
      tempFileStrings.Add(trim('-$S-                                                                       '));
      tempFileStrings.Add(trim('-$T-                                                                       '));
      tempFileStrings.Add(trim('-$U-                                                                       '));
      tempFileStrings.Add(trim('-$V+                                                                       '));
      tempFileStrings.Add(trim('-$W-                                                                       '));
      tempFileStrings.Add(trim('-$X+                                                                       '));
      tempFileStrings.Add(trim('-$YD                                                                       '));
      tempFileStrings.Add(trim('-$Z1                                                                       '));
      tempFileStrings.Add(trim('-GD                                                                        '));
      tempFileStrings.Add(trim('-cg                                                                        '));
      tempFileStrings.Add(trim('--drc                                                                      '));
      tempFileStrings.Add(trim('-AWinTypes=Windows;WinProcs=Windows;DbiTypes=BDE;DbiProcs=BDE;DbiErrs=BDE; '));
      tempFileStrings.Add(trim('-H+                                                                        '));
      tempFileStrings.Add(trim('-W+                                                                        '));
      tempFileStrings.Add(trim('-M                                                                         '));
      tempFileStrings.Add(trim('-$M16384,1048576                                                           '));
      tempFileStrings.Add(trim('-K$00400000                                                                '));
    end;
    tempFileStrings.Add('-i' + tempItemSum); //包文件
    tempFileStrings.Add('-U' + tempItemSum); //单元文件
    tempFileStrings.Add('-R' + tempItemSum); //资源文件

    if aBPLOutPath <> EmptyStr then
      tempFileStrings.Add('-LE' + '"' + aBPLOutPath + '"');
    if aDcPOutPath <> EmptyStr then
      tempFileStrings.Add('-LN' + '"' + aDcPOutPath + '"');
    if aExeOutPath <> EmptyStr then
      tempFileStrings.Add('-E' + '"' + aExeOutPath + '"');

    //dcc32.cfg 全局文件
    if AnsiCompareText(ABGetFilename(aCfgFileName),'dcc32')=0 then
    begin
      tempFileStrings.Add(trim('-AWinTypes=Windows;WinProcs=Windows;DbiTypes=BDE;DbiProcs=BDE;DbiErrs=BDE'));
    end
    else
    //Dpk文件.cfg 项目文件
    begin
      tempFileStrings.Add(trim('-Z                                                                         '));
      tempFileStrings.Add(trim('-w-UNSAFE_TYPE                                                             '));
      tempFileStrings.Add(trim('-w-UNSAFE_CODE                                                             '));
      tempFileStrings.Add(trim('-w-UNSAFE_CAST                                                             '));
    end;

    tempFileStrings.SaveToFile(aCfgFileName);
  finally
    tempFileStrings.free;
  end;
end;

function ABBuildProject(aCompilerFileName: string; aFileName: string;var aOutMsg:string): boolean;
var
  tempCompilerDir: string;
  tempCompilerFileName: string;
  tempCompilerName: string;

  tempCompilerProject: string;
  tempCompilerProjectExt: string;
  tempCompilerBatFileName: string;
  tempCompilerLogFileName: string;
  tempCompilerLog: string;
  i: LongInt;
begin
  Application.ProcessMessages;
  aOutMsg:=EmptyStr;

  //设置编译的文件名与路径
  tempCompilerName:=ABGetFilename(aCompilerFileName);
  tempCompilerProjectExt:= ABGetFileExt(aFileName);
  tempCompilerProject  := ABGetFilename(aFileName, false);
  tempCompilerDir     := ABGetFiledir(aCompilerFileName);
  tempCompilerFileName := ABGetFilename(aCompilerFileName,false);
  tempCompilerBatFileName:=tempCompilerDir + '\tempCompiler.bat';
  tempCompilerLogFileName:=tempCompilerDir+'\tempCompilerLog.txt';

  {
  //一定要设置为编译器所在目录,否则生成的文件到不了cfg中设置的路径,而还是会在工程所在目录
  if (AnsiCompareText(GetCurrentDir,tempCompilerDir)<>0) then
  begin
    SetCurrentDir(tempCompilerDir);
  end;
  }
  //生成编译命令的批处理文件
  ABWriteTxt(tempCompilerBatFileName,
             'del '+'"'+tempCompilerLogFileName+'"'+ABEnterWrapStr+
             ABGetDriverName(aFileName)+ABEnterWrapStr+
             'cd '+ABGetFiledir(aFileName)+ABEnterWrapStr+
             tempCompilerFileName+' -B -Q' +
             ' "'+aFileName +'"'+ ' >> ' +
             ' "'+tempCompilerLogFileName+'"'
             );

  //执行编译的批处理文件
  ABWaitAppEnd(tempCompilerBatFileName, SW_Hide);

  //取得编译日志
  tempCompilerLog := ABReadTxt(tempCompilerLogFileName);
  if (tempCompilerLog <> EmptyStr) then
  begin
    i := ABPos('Fatal:', tempCompilerLog);
    if i<=0 then
      i := ABPos('Error:', tempCompilerLog);

    if (i>0) then
    begin
      result  :=false;
      aOutMsg := tempCompilerProject+'  ' +'编译失败'+ABEnterWrapStr+
                 copy(tempCompilerLog, i, MaxInt);
    end
    else
    begin
      result :=true;
    end;
  end
  else
  begin
    result :=true;
  end;
end;

procedure ABAddPathToLibraryPath( aFileName:String;aLibraryPath:TStrings);
var
  tempStr1:string;
begin
  tempStr1:=ABGetFilepath(aFileName);
  if (ABCheckDirExists(tempStr1)) and
     (aLibraryPath.IndexOf(tempStr1)<0) then
    aLibraryPath.Add(tempStr1);
end;

function ABGetFileNameByCompilerFileName( aFileName:String;aCompilerFileName:String):string;
begin
  result:=aFileName;
  if (AnsiCompareText(ABGetFileExt(aFileName),'dproj')=0) and
     (ABPos('MsBuild',aCompilerFileName)<=0) then
  begin
    if ABCheckFileExists(ChangeFileExt(aFileName, '.dpk')) then
      result:=ChangeFileExt(aFileName, '.dpk')
    else  if ABCheckFileExists(ChangeFileExt(aFileName, '.dpr')) then
      result:=ChangeFileExt(aFileName, '.dpr');
  end;
end;

function ABBuildProjects(aDelphiVersion:TABDelphiVersion;
                         aCompilerFileName: string;

                         aFileNameStrings:TStrings;
                         aLogStrings:TStrings;
                         aFailureFileNameStrings:TStrings;

                         aCfgFileName,
                         aDcpOutPath,
                         aBPLOutPath,
                         aExeOutPath:string;
                         aLibraryPath: Tstrings;
                         aAddDefaultSearchPath: Boolean;
                         aShortFileNameModel: Boolean;

                         aAfterCompilerNotifyEvent:TABNotifyEvent;
                         aProgressBar:TProgressBar):boolean;
var
  i:LongInt;
  tempCurOK:boolean;
  tempLibraryPath: Tstrings;
  tempOutMsg:string;
begin
  result:=true;

  ABSetProgressBarBegin(aProgressBar,aFileNameStrings.Count);
  aLogStrings.Clear;
  aFailureFileNameStrings.Clear;
  tempLibraryPath:=TStringList.Create;
  try
    for I := 0 to aFileNameStrings.count-1 do
    begin
      ABSetProgressBarNext(aProgressBar);
      tempLibraryPath.Assign(tempLibraryPath);

      tempCurOK:=ABBuildProject (aDelphiVersion,
                         aCompilerFileName,
                         aFileNameStrings[i],
                         tempOutMsg,

                         aCfgFileName,
                         aDcpOutPath,
                         aBPLOutPath,
                         aExeOutPath,
                         tempLibraryPath,
                         aAddDefaultSearchPath,
                         aShortFileNameModel,
                         aAfterCompilerNotifyEvent);
      if not tempCurOK then
      begin
        Result := false;
        aFailureFileNameStrings.Add(aFileNameStrings[i]);
        aLogStrings.Add('第'+IntToStr(i+1)+'个工程['+ABGetFilename(aFileNameStrings[i])+']编译失败:'+ABEnterWrapStr+tempOutMsg);
      end
      else
      begin
        aLogStrings.Add('第'+IntToStr(i+1)+'个工程['+ABGetFilename(aFileNameStrings[i])+']编译成功.');
      end;
    end;
  finally
    tempLibraryPath.Free;
    ABSetProgressBarEnd(aProgressBar);
  end;
end;

function ABBuildProject (aDelphiVersion:TABDelphiVersion;
                         aCompilerFileName: string;
                         aFileName: string;
                         var aOutMsg:string;

                         aCfgFileName,
                         aDcpOutPath,
                         aBPLOutPath,
                         aExeOutPath:string;
                         aLibraryPath: Tstrings;
                         aAddDefaultSearchPath: Boolean;
                         aShortFileNameModel: Boolean;
                         aAfterCompilerNotifyEvent:TABNotifyEvent):boolean;
var
  j:LongInt;

  tempObjectCfgFileName,
  tempFileBasePATH,
  tempFileStr,
  tempItem,
  tempItems,
  tempFileExt:string;
begin
  result:=true;

  //尽可能的取得包路径
  if Assigned(aLibraryPath) then
  begin
    //通过编译器名来返回等待编译的文件名
    //dproj用MsBuild编译，dpk、dpr用dcc32编译
    aFileName:= ABGetFileNameByCompilerFileName(aFileName,aCompilerFileName);

    tempFileBasePATH:=ABGetFilepath(aFileName);
    ABAddPathToLibraryPath(tempFileBasePATH,aLibraryPath);
    tempFileExt:=ABGetFileExt(aFileName);
    if (AnsiCompareText(tempFileExt, 'dpr')=0) then
    begin
      tempFileStr:= ABGetItemValue(ABReadTxt(aFileName),'uses','','begin');
      for j := 1 to ABGetSpaceStrCount(tempFileStr, ',') do
      begin
        tempItems := ABGetSpaceStr(tempFileStr, j, ',');
        if tempItems <> EmptyStr then
        begin
          tempItem:= ABGetItemValue(tempItems,' in ''','','''');
          if tempItem <> EmptyStr then
          begin
            ABAddPathToLibraryPath(ABGetpath(tempFileBasePATH,tempItem),aLibraryPath);
          end;
        end;
      end;
    end
    else if (AnsiCompareText(tempFileExt, 'dpk')=0) then
    begin
      tempFileStr:= ABGetItemValue(ABReadTxt(aFileName),'contains','','end.');
      for j := 1 to ABGetSpaceStrCount(tempFileStr, ';') do
      begin
        tempItems := ABGetSpaceStr(tempFileStr, j, ';');
        if tempItems <> EmptyStr then
        begin
          tempItem:= ABGetItemValue(tempItems,' in ''','','''');
          if tempItem <> EmptyStr then
          begin
            ABAddPathToLibraryPath(ABGetpath(tempFileBasePATH,tempItem),aLibraryPath);
          end;
        end;
      end;
    end;
  end;

  //原cfg文件改名
  tempObjectCfgFileName:=aCfgFileName;
  if tempObjectCfgFileName=EmptyStr then
    tempObjectCfgFileName:=ChangeFileExt(aFileName,'.cfg');

  if (ABCheckFileExists(tempObjectCfgFileName)) and
     (not ABCheckFileExists(tempObjectCfgFileName+'.back')) then
    ABCopyFile(tempObjectCfgFileName,tempObjectCfgFileName+'.back');

  try
    //创建新的cfg文件
    ABCreateDelphiCfgFile(aDelphiVersion,
                tempObjectCfgFileName,

                aDcpOutPath,
                aBPLOutPath,
                aExeOutPath,

                aLibraryPath,
                aAddDefaultSearchPath,
                aShortFileNameModel);

    //编译文件
    if not ABBuildProject(aCompilerFileName,aFileName,aOutMsg) then
    begin
      Result := false;
    end;

    if Assigned(aAfterCompilerNotifyEvent) then
    begin
      aAfterCompilerNotifyEvent(nil,Result);
    end;
  finally
    //原cfg文件恢复原名
    if (ABCheckFileExists(tempObjectCfgFileName+'.back')) then
    begin
      ABCopyFile(tempObjectCfgFileName,tempObjectCfgFileName+'.PriUse');
      ABCopyFile(tempObjectCfgFileName+'.back',tempObjectCfgFileName);
      DeleteFile(tempObjectCfgFileName+'.back');
    end;
  end;
end;

procedure ABSaveToD7Group(aStrings: TStrings; aFileName: string);
var
  tempSaveAsPath,
  tempSamePath,
  tempCurItemName,
  tempCurLineItemNameList,
  tempItemNameList,
  tempItemInfoList,
  tempFileAllName: string;
  i,tempMaxLineLength: LongInt;
  tempList: TStrings;
begin
  if aFileName = EmptyStr then
  begin
    aFileName := ABSaveFile('', '', 'bpg|*.bpg');
  end;

  if aFileName <> EmptyStr then
  begin
    tempMaxLineLength:=79;
    tempSaveAsPath := ABGetFilePath(aFileName);
    tempList := TStringList.Create;
    try
      tempCurLineItemNameList := 'PROJECTS = ';
      for I := 0 to aStrings.Count - 1 do
      begin
        if ABCheckFileExists(ChangeFileExt(aStrings[i], '.dpk')) then
        begin
          tempCurItemName:=ABGetFilename(aStrings[i])+'.bpl';
        end
        else if ABCheckFileExists(ChangeFileExt(aStrings[i], '.dpr')) then
        begin
          tempCurItemName:=ABGetFilename(aStrings[i])+'.exe';
        end
        else
        begin
          tempCurItemName:=aStrings[i];
        end;

        //换行处理
        if (i = aStrings.Count - 1) and (Length(tempCurLineItemNameList + tempCurItemName)>tempMaxLineLength) and
                                        (Length(tempCurItemName)<=tempMaxLineLength) or
           (i <> aStrings.Count - 1) and (Length(tempCurLineItemNameList + tempCurItemName+ ' '+'\')>tempMaxLineLength) then
        begin
          tempItemNameList:=tempItemNameList +tempCurLineItemNameList+'\'+ABEnterWrapStr;
          tempCurLineItemNameList :='  '+tempCurItemName;
        end
        else
        begin
          tempCurLineItemNameList := tempCurLineItemNameList + tempCurItemName;
        end;
        if (i <> aStrings.Count - 1) then
        begin
          tempCurLineItemNameList := tempCurLineItemNameList + ' ';
        end;

        //相同盘符下的文件相对路径的处理
        if AnsiCompareText(Copy(tempSaveAsPath,1,2),Copy(aStrings[i],1,2))=0 then
        begin
          tempFileAllName:=EmptyStr;
          tempSamePath:=tempSaveAsPath;
          while not ABCheckBeginStr(aStrings[i],tempSamePath) do
          begin
            tempFileAllName:=tempFileAllName+'..\';
            tempSamePath:=ABGetParentPath(tempSamePath);
          end;
          tempFileAllName:=tempFileAllName+ABStringReplace(aStrings[i], tempSamePath, '');
        end
        else
        begin
          tempFileAllName:=aStrings[i];
        end;

        tempItemInfoList := tempItemInfoList + tempCurItemName+': ' +
          tempFileAllName + ABEnterWrapStr +
          '  $(DCC)' + ABEnterWrapStr +
          '' + ABEnterWrapStr;
      end;

      if tempCurLineItemNameList<>EmptyStr then
      begin
        tempItemNameList:=tempItemNameList +tempCurLineItemNameList+ABEnterWrapStr;
      end;

      tempList.Text :=
        '#------------------------------------------------------------------------------' + ABEnterWrapStr
        +
        'VERSION = BWS.01' + ABEnterWrapStr
        +
        '#------------------------------------------------------------------------------' + ABEnterWrapStr
        +
        '!ifndef ROOT' + ABEnterWrapStr
        +
        'ROOT = $(MAKEDIR)\..' + ABEnterWrapStr
        +
        '!endif' + ABEnterWrapStr
        +
        '#------------------------------------------------------------------------------' + ABEnterWrapStr
        +
        'MAKE = $(ROOT)\bin\make.exe -$(MAKEFLAGS) -f$**' + ABEnterWrapStr
        +
        'DCC = $(ROOT)\bin\dcc32.exe $**' + ABEnterWrapStr
        +
        'BRCC = $(ROOT)\bin\brcc32.exe $**' + ABEnterWrapStr
        +
        '#------------------------------------------------------------------------------' + ABEnterWrapStr
        +
        tempItemNameList +
        '#------------------------------------------------------------------------------' + ABEnterWrapStr
        +
        'default: $(PROJECTS)' + ABEnterWrapStr
        +
        '#------------------------------------------------------------------------------' + ABEnterWrapStr
        + ABEnterWrapStr+
        tempItemInfoList;
      tempList.SaveToFile(aFileName);
    finally
      tempList.Free;
    end;
  end;
end;

procedure ABSetDelphidof(aFileName: string;aSetType:TABBatchSetDelphiProjectType;aValues:array of string);
var
  tempStrings:TStrings;
  tempIndex:LongInt;
  tempItemName,
  tempItemValue:string;
begin
  tempItemName       :=EmptyStr;
  tempItemValue      :=EmptyStr;
  tempItemValue      :=aValues[0];
  case aSetType of
    bstIncludeVerInfo:
    begin
      tempItemName       :='IncludeVerInfo';
    end;
    bstBuildIncVersion:
    begin
      tempItemName       :='AutoIncBuild';
    end;
    bstVersion:
    begin
      tempItemName       :='FileVersion';
    end;
    bstDcpPath:
    begin
      tempItemName       :='PackageDCPOutputDir';
    end;
    bstBplPath:
    begin
      tempItemName       :='PackageDLLOutputDir';
    end;
    bstExePath:
    begin
      tempItemName       :='OutputDir';
    end;
    bstBuildWithPackage:
    begin
      tempItemName       :='UsePackages';
    end;
    bstBuildWithPackageList:
    begin
      tempItemName       :='Packages';
    end;
  end;
  //修改文件内容
  if (tempItemName<>EmptyStr) then
  begin
    tempStrings:=TStringList.Create;
    try
      tempStrings.LoadFromFile(aFileName);
      if  aSetType=bstVersion then
      begin
        tempStrings.Values['MajorVer']:=aValues[0];
        tempStrings.Values['MinorVer']:=aValues[1];
        tempStrings.Values['Release'] :=aValues[2];
        tempStrings.Values['Build']   :=aValues[3];
      end
      else
      begin
        tempIndex:=tempStrings.IndexOfName(tempItemName);
        if tempIndex>=0 then
        begin
          if tempItemValue<>EmptyStr then
            tempStrings.ValueFromIndex[tempIndex]:=tempItemValue
          else
            tempStrings.Strings[tempIndex]:=tempItemName+tempStrings.NameValueSeparator+tempItemValue;
        end;
      end;
      tempStrings.SaveToFile(aFileName);
    finally
      tempStrings.Free;
    end;
  end;
end;

procedure ABGetDelphidof(aFileName: string;aSetType:TABBatchSetDelphiProjectType;var aValues:array of string);
var
  tempStrings:TStrings;
  tempIndex:LongInt;
  tempItemName:string;
begin
  tempItemName       :=EmptyStr;
  case aSetType of
    bstIncludeVerInfo:
    begin
      tempItemName       :='IncludeVerInfo';
    end;
    bstBuildIncVersion:
    begin
      tempItemName       :='AutoIncBuild';
    end;
    bstVersion:
    begin
      tempItemName       :='FileVersion';
    end;
    bstDcpPath:
    begin
      tempItemName       :='PackageDCPOutputDir';
    end;
    bstBplPath:
    begin
      tempItemName       :='PackageDLLOutputDir';
    end;
    bstExePath:
    begin
      tempItemName       :='OutputDir';
    end;
    bstBuildWithPackage:
    begin
      tempItemName       :='UsePackages';
    end;
    bstBuildWithPackageList:
    begin
      tempItemName       :='Packages';
    end;
  end;
  //修改文件内容
  if (tempItemName<>EmptyStr) then
  begin
    tempStrings:=TStringList.Create;
    try
      tempStrings.LoadFromFile(aFileName);
      if aSetType= bstVersion  then
      begin
        aValues[0]:=tempStrings.Values['MajorVer'];
        aValues[1]:=tempStrings.Values['MinorVer'];
        aValues[2]:=tempStrings.Values['Release'] ;
        aValues[3]:=tempStrings.Values['Build']   ;
      end
      else
      begin
        tempIndex:=tempStrings.IndexOfName(tempItemName);
        if tempIndex>=0 then
        begin
          aValues[0]:=tempStrings.ValueFromIndex[tempIndex];
          aValues[1]:=EmptyStr ;
          aValues[2]:=EmptyStr ;
          aValues[3]:=EmptyStr ;
        end;
      end;
    finally
      tempStrings.Free;
    end;
  end;
end;

function ABLoadGroupItemToStrings(aFileName: string;aStrings: TStrings):boolean;
var
  i: LongInt;
  tempDelphiVersion:TABDelphiVersion;

  tempPath,                //组文件的路径
  tempFileText,            //组文件内容
  tempItemListText,        //组文件中项目列表
  tempCurItemFileName: String;        //组文件中项目列表当前项目文件名
  tempCurItemFileAllName: String;     //组文件中项目列表当前项目文件全名

  tempItemCount:LongInt;   //项目列表中项目的数量

  tempItemListBeg,             //项目列表开始
  tempItemListEnd,             //项目列表结束
  tempItemListDelList,         //项目列表中删除的内容
  tempItemListSpace,           //项目列表中的分隔符

  tempItemInfoBeg,             //项目详细信息开始 (其中的[:Item]表示引用项目列表中的当前项名称)
  tempItemInfoEnd,             //项目详细信息结束
  tempItemInfoDelList:String;  //项目详细信息中删除的内容
begin
  Result:=false;
  tempDelphiVersion:=ABGetDelphiVersion(aFileName);

  case tempDelphiVersion of
    dvD7,dvD2007:
    begin
      tempFileText:= ABReadTxt(aFileName);
      tempItemListBeg := 'PROJECTS = ';
      tempItemListEnd := '#';
      tempItemListSpace:=' ';

      tempItemInfoBeg := '[:Item]:';
      tempItemInfoEnd := '$(DCC)';
      Result:=true;
    end;
    dvD2009:
    begin
      {$IF (CompilerVersion>=20.0)}
      tempFileText := UTF8ToString(AnsiString(ABReadTxt(aFileName)));
      tempItemListBeg := '<Target Name="Build">';
      tempItemListEnd := '</Target>';
      tempItemListDelList:='<CallTarget Targets=","/>';
      tempItemListSpace:=';';

      tempItemInfoBeg := '<Target Name="[:Item]">';
      tempItemInfoEnd := '</Target>';
      tempItemInfoDelList:='<MSBuild Projects=","/>';
      Result:=true;
      {$IFEND}
    end;
  end;

  if Result then
  begin
    tempPath:=ABGetFilepath(aFileName);
    tempItemListText := trim(ABGetItemValue(tempFileText, tempItemListBeg, tempItemListDelList, tempItemListEnd));
    tempItemListText := trim(ABStringReplace(tempItemListText, '\', ''));
    tempItemCount := ABGetSpaceStrCount(tempItemListText,tempItemListSpace);
    for I := 1 to tempItemCount do
    begin
      //取得当前项目名称
      tempCurItemFileName := trim(ABGetSpaceStr(tempItemListText, i,tempItemListSpace));
      if Trim(tempCurItemFileName) = EmptyStr then
        Continue;

      //取得当前项目的文件全名
      tempCurItemFileAllName := trim(ABGetItemValue(tempFileText,
                                                    ABStringReplace(tempItemInfoBeg, '[:Item]', tempCurItemFileName),
                                                    tempItemInfoDelList,
                                                    ABStringReplace(tempItemInfoEnd, '[:Item]', tempCurItemFileName)
                                                     )
                                      );
      if (tempCurItemFileAllName <> EmptyStr) then
      begin
        tempCurItemFileAllName :=  ABGetpath(tempPath,tempCurItemFileAllName);
        if (aStrings.IndexOf(tempCurItemFileAllName) < 0) then
        begin
          aStrings.Add(tempCurItemFileAllName);
        end;
      end;
    end;
  end;
end;

function ABBitmapToString(aBitmap:TBitmap;aCheckMoreStr:string;aCheckLessStr:string;aType:TABGrayBitmapType;aCheckValue: integer):string;
var
  tempPointRGB: PRGBTriple;
  x, y: Integer;
  tempValue: Integer;
begin
  Result:=EmptyStr;
  aBitmap.PixelFormat   :=   pf24bit;
  for   y   :=   0   to   aBitmap.Height   -   1   do
  begin
    tempPointRGB   :=   aBitmap.ScanLine[y];
    for   x   :=   0   to   aBitmap.Width   -   1   do
    begin
      tempValue:=0;
      case aType of
        gbtMax:
        begin
         tempValue := max(max(tempPointRGB.rgbtRed,tempPointRGB.rgbtGreen) , tempPointRGB.rgbtBlue);
        end;
        gbtAvg:
        begin
         tempValue := (tempPointRGB.rgbtRed + tempPointRGB.rgbtGreen + tempPointRGB.rgbtBlue) div 3;
        end;
        gbtWeightedAvg:
        begin
          //此处用整形，而不用浮点是为了搞高效率 gray := Round(0.299 * P[3 * X + 2] + 0.587 * P[3 * X + 1] + 0.11 * P[3 * X]);
          tempValue := (30 * tempPointRGB.rgbtRed + 59 * tempPointRGB.rgbtGreen + 11 * tempPointRGB.rgbtBlue) div 100;
        end;
      end;
      if tempValue<=aCheckValue then
      begin
        ABAddstr(Result,aCheckLessStr,'');
      end
      else
      begin
        ABAddstr(Result,aCheckMoreStr,'');
      end;
      Inc(tempPointRGB);
    end;
    ABAddstr(Result,ABEnterWrapStr,'');
  end;
end;

procedure ABBlackWhiteOrGrayBitmap(aSBitmap,aDBitmap:TBitmap;aExtValue: integer;aOnlyBlackWhite:boolean;aType:TABGrayBitmapType);
var
  tempPointRGB: PRGBTriple;
  x, y: Integer;
  tempValue: Integer;
begin
  if aSBitmap<>aDBitmap then
    aDBitmap.Assign(aSBitmap);

  aDBitmap.PixelFormat   :=   pf24bit;
  for   y   :=   0   to   aDBitmap.Height   -   1   do
  begin
    tempPointRGB   :=   aDBitmap.ScanLine[y];
    for   x   :=   0   to   aDBitmap.Width   -   1   do
    begin
      tempValue:=0;
      case aType of
        gbtMax:
        begin
         tempValue := max(max(tempPointRGB.rgbtRed,tempPointRGB.rgbtGreen) , tempPointRGB.rgbtBlue);
        end;
        gbtAvg:
        begin
         tempValue := (tempPointRGB.rgbtRed + tempPointRGB.rgbtGreen + tempPointRGB.rgbtBlue) div 3;
        end;
        gbtWeightedAvg:
        begin
          //此处用整形，而不用浮点是为了搞高效率 gray := Round(0.299 * P[3 * X + 2] + 0.587 * P[3 * X + 1] + 0.11 * P[3 * X]);
          tempValue := (30 * tempPointRGB.rgbtRed + 59 * tempPointRGB.rgbtGreen + 11 * tempPointRGB.rgbtBlue) div 100;
        end;
      end;
      //二值化
      if aOnlyBlackWhite then
      begin
        if tempValue<=aExtValue then
        begin
          tempValue:=0;
        end
        else
        begin
          tempValue:=255;
        end;
      end
      else
      //灰化
      begin
        if aExtValue<>0 then
        begin
          tempValue:=tempValue+aExtValue;
          if tempValue>255 then
            tempValue:=255
          else if tempValue<0 then
            tempValue:=0;
        end;
      end;
      tempPointRGB.rgbtRed := tempValue;
      tempPointRGB.rgbtGreen := tempValue;
      tempPointRGB.rgbtBlue := tempValue;
      Inc(tempPointRGB);
    end;
  end;
end;

procedure ABBlackWhiteBitmap(aSBitmap,aDBitmap:TBitmap;aType:TABGrayBitmapType;aExtValue: integer);
begin
  ABBlackWhiteOrGrayBitmap(aSBitmap,aDBitmap,aExtValue,true,aType);
end;

procedure ABGrayBitmap(aSBitmap,aDBitmap:TBitmap;aType:TABGrayBitmapType;aExtValue: integer);
begin
  ABBlackWhiteOrGrayBitmap(aSBitmap,aDBitmap,aExtValue,False,aType);
end;

procedure ABOperateBitmapBright(aBitmap: TBitmap; aNumber: LongInt=10);
var
  p: PByteArray;
  x, y: Integer;
begin
  //24位真彩色
  aBitmap.PixelFormat := pf24Bit;
  for y := 0 to aBitmap.Height - 1 do
  begin
    p := aBitmap.scanline[y];
    for x := 0 to aBitmap.Width - 1 do
    begin
      p[x * 3]     := Min(Max(p[x * 3]     + aNumber,0),255);
      p[x * 3 + 1] := Min(Max(p[x * 3+ 1]  + aNumber,0),255);
      p[x * 3 + 2] := Min(Max(p[x * 3+ 2]  + aNumber,0),255);
    end;
  end;
  aBitmap.Assign(aBitmap);
end;

procedure ABOperateBitmapContrast(aBitmap: TBitmap;aNumber: LongInt=10;aCheckValue:LongInt=128);
var
  p: PByteArray;
  x, y: Integer;
begin
  //24位真彩色
  aBitmap.PixelFormat := pf24Bit;
  for y := 0 to aBitmap.Height - 1 do
  begin
    p := aBitmap.scanline[y];
    for x := 0 to aBitmap.Width - 1 do
    begin
      if (p[x * 3]     > aCheckValue) and
         (p[x * 3 + 1] > aCheckValue) and
         (p[x * 3 + 2] > aCheckValue) then
      begin
        p[x * 3]     := Min(Max(p[x * 3]     + aNumber,0),255);
        p[x * 3 + 1] := Min(Max(p[x * 3+ 1]  + aNumber,0),255);
        p[x * 3 + 2] := Min(Max(p[x * 3+ 2]  + aNumber,0),255);
      end;
      if (p[x *3]     < aCheckValue) and
         (p[x *3 + 1] < aCheckValue) and
         (p[x *3 + 2] < aCheckValue) then
      begin
        p[x * 3]     := Min(Max(p[x * 3]     - aNumber,0),255);
        p[x * 3 + 1] := Min(Max(p[x * 3+ 1]  - aNumber,0),255);
        p[x * 3 + 2] := Min(Max(p[x * 3+ 2]  - aNumber,0),255);
      end;
    end;
  end;
  aBitmap.Assign(aBitmap);
end;

procedure ABOperateBitmapSaturation(aBitmap: TBitmap; aNumber: LongInt=10;aCheckValue:LongInt=128);
var
  p: PByteArray;
  x, y: Integer;
begin
  //24位真彩色
  aBitmap.PixelFormat := pf24Bit;
  for y := 0 to aBitmap.Height - 1 do
  begin
    p := aBitmap.scanline[y];
    for x := 0 to aBitmap.Width - 1 do
    begin
      if (p[x * 3]     > aCheckValue)then
      begin
        p[x * 3]     := Min(Max(p[x * 3]     + aNumber,0),255);
      end
      else
      begin
        p[x * 3]     := Min(Max(p[x * 3]     - aNumber,0),255);
      end;

      if (p[x * 3+1]     > aCheckValue)then
      begin
        p[x * 3+1]     := Min(Max(p[x * 3+1]     + aNumber,0),255);
      end
      else
      begin
        p[x * 3+1]     := Min(Max(p[x * 3+1]     - aNumber,0),255);
      end;

      if (p[x * 3+2]     > aCheckValue)then
      begin
        p[x * 3+2]     := Min(Max(p[x * 3+2]     + aNumber,0),255);
      end
      else
      begin
        p[x * 3+2]     := Min(Max(p[x * 3+2]     - aNumber,0),255);
      end;
    end;
  end;
  aBitmap.Assign(aBitmap);
end;

procedure ABGrayImageList(aOutPath:string;aImageList:TImageList;aType:TABGrayBitmapType;aExtValue: integer);
var
  tempSBitmap:   TBitmap;
  tempDBitmap:   TBitmap;
  i:LongInt;
begin
  tempSBitmap   :=   TBitmap.Create;
  tempDBitmap   :=   TBitmap.Create;
  try
    for I := 0 to aImageList.Count-1 do
    begin
      aImageList.GetBitmap(i,tempSBitmap);
      ABGrayBitmap(tempSBitmap,tempDBitmap,aType,aExtValue);
      tempDBitmap.SaveToFile(aOutPath+inttostr(i)+'.bmp');
    end;
  finally
    tempSBitmap.Free;
    tempDBitmap.Free;
  end;
end;

procedure ABBlackWhiteImageList(aOutPath:string;aImageList:TImageList;aType:TABGrayBitmapType;aExtValue: integer);
var
  tempSBitmap:   TBitmap;
  tempDBitmap:   TBitmap;
  i:LongInt;
begin
  tempSBitmap   :=   TBitmap.Create;
  tempDBitmap   :=   TBitmap.Create;
  try
    for I := 0 to aImageList.Count-1 do
    begin
      aImageList.GetBitmap(i,tempSBitmap);
      ABBlackWhiteBitmap(tempSBitmap,tempDBitmap,aType,aExtValue);
      tempDBitmap.SaveToFile(aOutPath+inttostr(i)+'.bmp');
    end;
  finally
    tempSBitmap.Free;
    tempDBitmap.Free;
  end;
end;

procedure ABJpgFileToBmpFile(aSFileName, aDFileName:string);
var
  tempJpeg: TJpegImage;
  tempBitmap: Tbitmap;
begin
  tempbitmap:=tbitmap.Create;
  tempJpeg:= TJpegImage.Create;
  try
    tempJpeg.LoadFromFile(aSFileName);
    tempbitmap.Assign(tempJpeg);
    tempbitmap.SaveToFile(aDFileName);
  finally
    tempbitmap.free;
    tempJpeg.Free;
  end;
end;

procedure ABBmpFileToJpgFile(aSFileName, aDFileName:string; ascale:TJPEGQualityRange);
var
  tempBitmap: Tbitmap;
  tempJpeg: TJpegImage;
begin
  tempBitmap:= tbitmap.Create;
  tempJpeg:= TJpegImage.Create;
  try
    tempBitmap.LoadFromFile(aSFileName);
    tempJpeg.Assign(tempBitmap);
    if ascale<>tempJpeg.CompressionQuality then
    begin
      tempJpeg.CompressionQuality:=ascale;
    end;
    tempJpeg.SaveToFile(aDFileName);
  finally
    tempBitmap.free;
    tempJpeg.Free;
  end;
end;

procedure ABIconToBitmap(aIcon: TIcon; aBitmap: TBitmap; aClarity:boolean);
begin
  aBitmap.Width:=aIcon.Width;
  aBitmap.Height:=aIcon.Height;

  //aBitmap.Canvas.Font.Color := aBgColor; //字体颜色
  aBitmap.Canvas.Brush.Color := clBlack; //底色
  if aClarity then
    aBitmap.Canvas.Brush.Style := bsClear;//透明

  aBitmap.Canvas.FillRect(Rect(0, 0, aBitmap.Width, aBitmap.Height));
  aBitmap.Canvas.Draw(0, 0, aIcon);
end;

procedure  ABBitmapToIcon(aBitmap:TBitmap;aIcon:TIcon;aClarity:boolean);
var
  vBitmapMask:   TBitmap;
  vBitmapColor:   TBitmap;
  vIconInfo:   TIconInfo;
begin
  vBitmapMask   :=   TBitmap.Create;
  vBitmapColor   :=   TBitmap.Create;
  try
    vBitmapMask.Width   :=   aBitmap.Width;
    vBitmapMask.Height   :=   aBitmap.Height;
    vBitmapMask.Canvas.Brush.Color   :=   clBlack; //底色
    if aClarity then
      vBitmapMask.Canvas.Brush.Style   :=   bsClear;         //透明

    vBitmapMask.Canvas.Rectangle(0,   0,   aBitmap.Width,   aBitmap.Height);

    vBitmapColor.Width   :=   aBitmap.Width;
    vBitmapColor.Height   :=   aBitmap.Height;
    StretchBlt(vBitmapColor.Canvas.Handle,   0,   0,   aBitmap.Width   ,   aBitmap.Height,
               aBitmap.Canvas.Handle     ,   0,   0,   aBitmap.Width,   aBitmap.Height,   SRCCOPY);

    vIconInfo.fIcon      :=   True;
    vIconInfo.xHotspot   :=   0;
    vIconInfo.yHotspot   :=   0;
    vIconInfo.hbmMask    :=   vBitmapMask.Handle;
    vIconInfo.hbmColor   :=   vBitmapColor.Handle;
    aIcon.Handle         :=   CreateIconIndirect(vIconInfo);
  finally
    vBitmapMask.Free;
    vBitmapColor.Free;
  end;
end;
procedure ABIcoFileToBmpFile(aSFileName, aDFileName:string;aClarity:boolean);
var
  tempBitmap:tbitmap;
  tempico:ticon;
begin
  tempBitmap:=tbitmap.Create;
  tempico:=ticon.Create;
  try
    tempico.LoadFromFile(aSFileName);
    ABIconToBitmap(tempico,tempBitmap,aClarity);
    tempBitmap.SaveToFile(aDFileName);
  finally
    tempBitmap.Free;
    tempico.Free;
  end;
end;

procedure ABBmpFileToIcoFile(aSFileName, aDFileName:string; aClarity:boolean);
var
  tempBitmap:tbitmap;
  tempico:ticon;
begin
  tempBitmap:=tbitmap.Create;
  tempico:=ticon.Create;
  try
    tempBitmap.LoadFromFile(aSFileName);
    ABBitmapToIcon(tempBitmap,tempico,aClarity);
    tempico.SaveToFile(aDFileName);
  finally
    tempBitmap.Free;
    tempico.Free;
  end;
end;

procedure DoChangPictureSize(aSFileName,aDFileName:string; aTimes: double; aNewWidth: Integer ; aNewHeight: Integer  );
var
  bmp, SourceBmp, bmp2: TBitmap;
  jpg, SourceJpg: TJPEGImage;
begin
  if UpperCase(ExtractFileExt(aSFileName)) = '.JPG' then
  begin
    bmp := TBitmap.Create;
    SourceJpg := TJPEGImage.Create;
    Jpg := TJPEGImage.Create;
    try
      SourceJpg.LoadFromFile(aSFileName);
      if aTimes>0 then
      begin
        aNewWidth:=ABTrunc(SourceJpg.Width*aTimes);
        aNewHeight:=ABTrunc(SourceJpg.Height*aTimes);
      end;

      bmp.Width := aNewWidth;
      bmp.Height := aNewHeight;
      bmp.PixelFormat := pf32bit;
      bmp.Canvas.StretchDraw(Rect(0, 0, bmp.Width, bmp.Height), SourceJpg);
      jpg.Assign(bmp);
      jpg.SaveToFile(aDFileName);
    finally
      bmp.Free;
      jpg.Free;
      SourceJpg.Free;
    end;
  end
  else if UpperCase(ExtractFileExt(aSFileName)) = '.BMP' then
  begin
    bmp := TBitmap.Create;
    bmp2 := TBitmap.Create;
    SourceBmp := TBitmap.Create;
    try
      SourceBmp.LoadFromFile(aSFileName);
      if aTimes>0 then
      begin
        aNewWidth:=ABTrunc(SourceBmp.Width*aTimes);
        aNewHeight:=ABTrunc(SourceBmp.Height*aTimes);
      end;

      bmp.Width := aNewWidth;
      bmp.Height := aNewHeight;
      bmp.PixelFormat := pf32bit;
      bmp.Canvas.StretchDraw(Rect(0, 0, bmp.Width, bmp.Height), SourceBmp);
      bmp2.Assign(bmp);
      bmp2.SaveToFile(aDFileName);
    finally
      bmp.Free;
      bmp2.Free;
      SourceBmp.Free;
    end;
  end;
end;

procedure ABChangPictureSize(aSFileName,aDFileName:string;  aNewWidth: Integer ; aNewHeight: Integer  );
begin
  DoChangPictureSize(aSFileName,aDFileName,0,aNewWidth,aNewHeight);
end;

procedure ABChangPictureSize(aInputGraphic:TGraphic;aOutPicture:TPicture; aNewWidth: Integer ; aNewHeight: Integer  );
var
  bmp: TBitmap;
  jpg: TJPEGImage;
begin
  bmp := TBitmap.Create;
  Jpg := TJPEGImage.Create;
  try
    bmp.Width := aNewWidth;
    bmp.Height := aNewHeight;
    bmp.PixelFormat := pf32bit;
    bmp.Canvas.StretchDraw(bmp.Canvas.ClipRect, aInputGraphic);
    jpg.Assign(bmp);
    aOutPicture.Assign(jpg);
  finally
    bmp.Free;
    jpg.Free;
  end;
end;

procedure ABChangPictureSize(aSFileName,aDFileName:string; aTimes: double  );
begin
  DoChangPictureSize(aSFileName,aDFileName,aTimes,0,0);
end;

procedure ABChangPictureSize(aInputGraphic:TGraphic;aOutPicture:TPicture; aTimes: double  );
begin
  ABChangPictureSize(aInputGraphic,aOutPicture,ABTrunc(aInputGraphic.Width*aTimes), ABTrunc(aInputGraphic.Height*aTimes));
end;

procedure ABCreateShortCut(aType:LongInt;aSFileName:string;aShortCutName:string;aShortCutSubPath: string);
var
  tmpObject: IUnknown;
  tmpSLink: IShellLink;
  tmpPFile: IPersistFile;
  LinkFilename,
  tempPath: string;
begin
  tmpObject := CreateComObject(CLSID_ShellLink);
  //创建建立快捷方式的外壳扩展
  tmpSLink := tmpObject as IShellLink; //取得接口
  tmpPFile := tmpObject as IPersistFile; //用来储存*.lnk文件的接口
  tmpSLink.SetPath(pChar(aSFileName)); //设定notepad.exe所在路径
  tmpSLink.SetWorkingDirectory(pChar(ExtractFilePath(aSFileName)));

  aShortCutName :=  aShortCutName + '.lnk';
  tempPath:=ABGetpath(ABGetShortCutTypePath(aType)+ aShortCutSubPath);

  if not SysUtils.DirectoryExists(tempPath) then
    SysUtils.ForceDirectories(tempPath);

  LinkFilename :=  tempPath+ aShortCutName;
  tmpPFile.Save(PWideChar(LinkFilename), FALSE); //保存*.lnk文件
end;

function ABGetShortCutTypePath(aType:LongInt): string;
var
  pidl: PItemIDList;
  FavPath: array[0..MAX_PATH] of char;
begin
  SHGetSpecialFolderLocation(0, aType, pidl);
  SHGetPathFromIDList(pidl, favpath);
  result:=ABGetpath(FavPath);
end;

function ABGetShortCutLLinkFileName(const aShortCutFileName: string): string;
var
  _SLink: IShellLink;
  _PFile: IPersistFile;
  findData: TWin32FindData;
  tmpStr: array[0..MAX_PATH - 1] of Char;
const
  SUCCESS_FINDED = 1;
  ERR_FILENAME = 2;
  ERR_UNKNOW = 3;
begin
  Result := EmptyStr;
  if (AnsiCompareText(ExtractFileExt(aShortCutFileName), '.LNK') <> 0) or (not
    (ABCheckFileExists(aShortCutFileName))) then
  begin
    exit;
  end;

  FillChar(tmpStr, MAX_PATH, #0);
  try
    OleCheck(CoCreateInstance(CLSID_ShellLink, nil, CLSCTX_INPROC_SERVER,
      IShellLink, _SLink));

    _PFile := _SLink as IPersistFile;

    OleCheck(_PFile.Load(PWideChar(aShortCutFileName), STGM_READ));
    OleCheck(_SLink.Resolve(0, SLR_ANY_MATCH or SLR_NO_UI));
    OleCheck(_SLink.GetPath(tmpStr, MAX_PATH, findData, SLGP_UNCPRIORITY));

    Result := tmpStr;
  except
    raise;
  end;
end;

procedure ABGetComs(aCOMS: Tstrings);
var
  temreg: TRegistry;
  i: longint;
begin
  temreg := nil;
  try
    temreg := TRegistry.Create;
    temreg.RootKey := HKEY_LOCAL_MACHINE;
    temreg.OpenKey('hardware\devicemap\serialcomm', false);
    temreg.GetValueNames(aCOMS);
    for i := 0 to aCOMS.Count - 1 do
    begin
      aCOMS.Strings[i] := temreg.ReadString(aCOMS.Strings[i])
    end;
  finally
    temreg.CloseKey;
    temreg.free;
  end;
end;

function ABComparisonValueSame(aValue1,aValue2:Variant): Boolean;
var
  i:LongInt;
  tempBoolean1,
  tempBoolean2:boolean;
begin
  result :=False;
  tempBoolean1:=abvarisnull(aValue1);
  tempBoolean2:=abvarisnull(aValue2);

  if (tempBoolean1) and (tempBoolean2) then
  begin
    result := true;
  end
  else if (tempBoolean1) or (tempBoolean2) then
  begin
    result := false;
  end
  else
  begin
    if (VarIsArray(aValue1)) or
       (VarIsArray(aValue2)) then
    begin
      if (abvarisnull(aValue1) and
         (not abvarisnull(aValue2))) then
      begin
        if VarIsArray(aValue2) then
        begin
          result :=true;
          for i := VarArrayLowBound(aValue2,1) to VarArrayHighBound(aValue2,1) do
          begin
            if not abvarisnull(aValue2[i]) then
            begin
              result:=False;
              Break;
            end;
          end;
        end
        else
        begin
          result :=(VarToStrDef(aValue2,'')='');
        end;
      end
      else if (not abvarisnull(aValue1) and
              (abvarisnull(aValue2))) then
      begin
        if VarIsArray(aValue1) then
        begin
          result :=true;
          for i := VarArrayLowBound(aValue1,1) to VarArrayHighBound(aValue1,1) do
          begin
            if not abvarisnull(aValue1[i]) then
            begin
              result:=False;
              Break;
            end;
          end;
        end
        else
        begin
          result :=(VarToStrDef(aValue1,'')='');
        end;
      end
      else if (not abvarisnull(aValue1) and
              (not abvarisnull(aValue2))) then
      begin
        if (VarIsArray(aValue1)) and (VarIsArray(aValue2)) then
        begin
          if (VarArrayLowBound(aValue1,1)=VarArrayLowBound(aValue2,1)) and
             (VarArrayHighBound(aValue1,1)=VarArrayHighBound(aValue2,1)) then
          begin
            result:=true;
            for i := VarArrayLowBound(aValue1,1) to VarArrayHighBound(aValue1,1) do
            begin
              if (abvarisnull(aValue1[i])) and (abvarisnull(aValue2[i])) then
              begin

              end
              else if aValue1[i]<>aValue2[i] then
              begin
                result:=False;
                Break;
              end;
            end;
          end
          else
          begin
            result:=False;
          end;
        end
        else
        begin
          result :=(aValue1 = aValue2);
        end;
      end;
    end
    else
    begin
      if (aValue1 = aValue2) then
      begin
        result := true;
      end;
    end;
  end;
end;

procedure ABGetMenuItems(aMenu: TMenuItem;aStrings: TStrings);
  procedure GetMenuItem(aParentMenuItem: TMenuItem);
  var
    i: longint;
  begin
    for i := aParentMenuItem.Count - 1 downto 0 do
    begin
      if aParentMenuItem.Items[i].Name<>EmptyStr then
        aStrings.AddObject(aParentMenuItem.Items[i].Name,aParentMenuItem.Items[i]);
      GetMenuItem(aParentMenuItem.Items[i]);
    end;
  end;
var
  i: longint;
begin
  aStrings.Clear;
  for i := aMenu.Count - 1 downto 0 do
  begin
    if aMenu.Items[i].Name<>EmptyStr then
      aStrings.AddObject(aMenu.Items[i].Name,aMenu.Items[i]);
    GetMenuItem(aMenu.Items[i]);
  end;
end;

Function ABGetMenuItem(aStrings: TStrings;aName:String):TMenuItem;
var
  i:longint;
begin
  result:=nil;
  i:=aStrings.IndexOf(aName);
  if i>=0 then
  begin
    result:=TMenuItem(aStrings.Objects[i]);
  end;
end;

procedure ABClareMenuItems(aMenu: TMenu);
  procedure ABClareMenuItem(aParentMenuItem: TMenuItem);
  var
    i: longint;
  begin
    for i := aParentMenuItem.Count - 1 downto 0 do
    begin
      ABClareMenuItem(aParentMenuItem.Items[i]);
      aParentMenuItem.Items[i].Free;
    end;
  end;
var
  i: longint;
begin
  for i := aMenu.Items.Count - 1 downto 0 do
  begin
    ABClareMenuItem(aMenu.Items[i]);
    aMenu.Items.Items[i].Free;
  end;
end;

function ABCreateMenuItem(aOwner:TComponent;
                        aItems: TMenuItem;

                        aName:string;
                        aCaption:string;

                        aOnClick:TNotifyEvent;
                        aAutoCheck:boolean;
                        aChecked:boolean;
                        aGroupIndex:LongInt;
                        aRadioItem:boolean
                        ):TMenuItem;
begin
  Result:=TMenuItem.Create(aOwner);
  if aName<>EmptyStr then
    Result.Name:=aName;
  if aCaption<>EmptyStr then
    Result.Caption:=aCaption;
  if aGroupIndex>0 then
    Result.GroupIndex:=aGroupIndex;
  if aRadioItem then
    Result.RadioItem:=aRadioItem;
  if aAutoCheck then
    Result.AutoCheck:=aAutoCheck;
  if aChecked then
    Result.Checked:=aChecked;

  if Assigned(aOnClick) then
    Result.OnClick:=aOnClick;

  aItems.Add(Result);
end;

procedure ABSelectMenuItemOfGroup(aMenuItem: TMenuItem);
begin
  if (Assigned(aMenuItem)) and
     (aMenuItem.GroupIndex>0) and
     (aMenuItem.RadioItem) and
     (aMenuItem.AutoCheck) and
     (not aMenuItem.Checked) then
  begin
    aMenuItem.Checked:=true;
  end;
end;

function ABFindTree(aTreeNode: TTreeNode;
                     aFindText:string;
                     aFindLevels: array of Variant
                     ):TTreeNode;
var
  i:longint;
  tempFindLevelsCount:LongInt;
begin
  result:=nil;
  tempFindLevelsCount:=High(aFindLevels);
  if ((tempFindLevelsCount=-1) or (ABVariantInArray(aTreeNode.Level,aFindLevels))) and
     (AnsiCompareText(aTreeNode.Text,aFindText)=0)  then
  begin
    result:=aTreeNode;
  end
  else
  begin
    for I := 0 to aTreeNode.Count - 1 do
    begin
      result:=ABFindTree(aTreeNode.Item[i],aFindText,aFindLevels);
      if Assigned(result) then
      begin
        Break;
      end;
    end;
  end;
end;

function ABFindTree(aTreeNodes: TTreeNodes;
                     aFindText:string;
                     aFindLevels: array of Variant
                     ):TTreeNode;
var
  i:longint;
  tempFindLevelsCount:LongInt;
begin
  result:=nil;
  tempFindLevelsCount:=High(aFindLevels);
  for I := 0 to aTreeNodes.Count - 1 do
  begin
    if ((tempFindLevelsCount=-1) or (ABVariantInArray(aTreeNodes[i].Level,aFindLevels))) and
       (AnsiCompareText(aTreeNodes[i].Text,aFindText)=0) then
    begin
      result:=aTreeNodes[i];
      break;
    end;
  end;
end;

function ABFindTree( aTreeNodes: TTreeNodes;
                     aFindData:Pointer;
                     aFindLevels: array of Variant
                     ):TTreeNode;
var
  i:longint;
  tempFindLevelsCount:LongInt;
begin
  result:=nil;
  tempFindLevelsCount:=High(aFindLevels);
  for I := 0 to aTreeNodes.Count - 1 do
  begin
    if ((tempFindLevelsCount=-1) or (ABVariantInArray(aTreeNodes[i].Level,aFindLevels))) and
       (aTreeNodes[i].Data=aFindData) then
    begin
      result:=aTreeNodes[i];
      break;
    end;
  end;
end;

function ABFindTree(aTreeNode: TTreeNode;
                     aFindData:Pointer;
                     aFindLevels: array of Variant
                     ):TTreeNode;
var
  i:longint;
  tempFindLevelsCount:LongInt;
begin
  result:=nil;
  tempFindLevelsCount:=High(aFindLevels);
  if ((tempFindLevelsCount=-1) or (ABVariantInArray(aTreeNode.Level,aFindLevels))) and
     (aTreeNode.Data=aFindData)  then
  begin
    result:=aTreeNode;
  end
  else
  begin
    for I := 0 to aTreeNode.Count - 1 do
    begin
      result:=ABFindTree(aTreeNode.Item[i],aFindData,aFindLevels);
      if Assigned(result) then
      begin
        Break;
      end;
    end;
  end;
end;

procedure ABSleep(aMilliSeconds: Cardinal);
var
  t: Cardinal;
begin
  t :=  GetTickCount() + aMilliSeconds;
  while(GetTickCount() < t)do begin
    Application.ProcessMessages;
  end;
end;

procedure ABOKBeep;
begin
  sysutils.Beep;
end;

function ABPlay(const aFileName: string): Boolean;
  function PlayWav(const aFileName: string): Boolean;
  var
    i:longint;
  begin
    i:=10;
    repeat
      Result := sndPlaySoundW(PWideChar(aFileName), SND_ASYNC);

      if not Result then
        absleep(100);

      i:=i-1;
    until (i<0) or (Result);
  end;
begin
  result:=False;
  if AnsiCompareText(ABGetFileExt(aFileName),'wav')=0 then
  begin
    result:=PlayWav(aFileName);
  end
  else if AnsiCompareText(ABGetFileExt(aFileName),'avi')=0 then
  begin

  end;
end;

function ABIsVMwarePresent(): LongBool;
begin
  Result := False;
 {$IFDEF CPU386}
  try
    asm
            mov     eax, 564D5868h
            mov     ebx, 00000000h
            mov     ecx, 0000000Ah
            mov     edx, 00005658h
            in      eax, dx
            cmp     ebx, 564D5868h
            jne     @@exit
            mov     Result, True
    @@exit:
    end;
  except
    Result := False;
  end;
{$ENDIF}
end;

function ABIsWin64: Boolean;
var
  Kernel32Handle: THandle;
  IsWow64Process: function(Handle: Windows.THandle; var Res: Windows.BOOL): Windows.BOOL; stdcall;
  GetNativeSystemInfo: procedure(var lpSystemInfo: TSystemInfo); stdcall;
  isWoW64: Bool;
  SystemInfo: TSystemInfo;
const
  PROCESSOR_ARCHITECTURE_AMD64 = 9;
  PROCESSOR_ARCHITECTURE_IA64 = 6;
begin
  Kernel32Handle := GetModuleHandle('KERNEL32.DLL');
  if Kernel32Handle = 0 then
    Kernel32Handle := LoadLibrary('KERNEL32.DLL');
  if Kernel32Handle <> 0 then
  begin
    IsWOW64Process := GetProcAddress(Kernel32Handle,'IsWow64Process');
    GetNativeSystemInfo := GetProcAddress(Kernel32Handle,'GetNativeSystemInfo');
    if Assigned(IsWow64Process) then
    begin
      IsWow64Process(GetCurrentProcess,isWoW64);
      Result := isWoW64 and Assigned(GetNativeSystemInfo);
      if Result then
      begin
        GetNativeSystemInfo(SystemInfo);
        Result := (SystemInfo.wProcessorArchitecture = PROCESSOR_ARCHITECTURE_AMD64) or
                  (SystemInfo.wProcessorArchitecture = PROCESSOR_ARCHITECTURE_IA64);
      end;
    end
    else Result := False;
  end
  else Result := False;
end;

function ABGetWindowsPath: string;
var
  Buf: array[0..MAX_PATH] of Char;
begin
  GetWindowsDirectory(Buf, MAX_PATH);
  Result := ABGetpath(Buf);
end;

function ABGetWindowsTempPath: string;
var
  Buf: array[0..MAX_PATH] of Char;
begin
  GettempPath(MAX_PATH, Buf);
  Result := ABGetFilepath(Buf);
end;

function ABGetWindowsTempFileName(aExt: string): string;
begin
  Result := ABGetWindowsTempFileName(ABGetWindowsTempPath,AExt)
end;

function ABGetWindowsTempFileName(aPath,aExt: string): string;
begin
  repeat
    Result := APath + '~' + IntToStr(GetTickCount)+'.'+ AExt;
    Sleep(1);
  until not FileExists(Result);
end;

function ABGetWindowsUserName: string;
const
  cnMaxUserNameLen = 254;
var
  sUserName: string;
  dwUserNameLen: Dword;
begin
  dwUserNameLen := cnMaxUserNameLen - 1;
  SetLength(sUserName, cnMaxUserNameLen);
  GetUserName(PChar(sUserName), dwUserNameLen);
  SetLength(sUserName, dwUserNameLen);
  Result := sUserName;
end;

function ABGetRegistryOrgAndUserName(var aOrg,aUserName: string): boolean;
var
  Myreg: Tregistry;
  RegString: string;
begin
  Result:=false;
  MyReg := Tregistry.Create;
  try
    MyReg.RootKey := HKEY_LOCAL_MACHINE;
    if (Win32Platform = VER_PLATFORM_WIN32_NT) then
      RegString := 'Software\Microsoft\Windows NT\CurrentVersion'
    else
      RegString := 'Software\Microsoft\Windows\CurrentVersion';

    if MyReg.openkey(RegString, False) then
    begin
      aOrg := MyReg.readstring('RegisteredOrganization');
      aUserName := MyReg.readstring('RegisteredOwner')  ;

      Result:=true;
    end;
  finally
    MyReg.CloseKey;
    MyReg.Free;
  end;
end;

function ABGetSysVersion: string;
var
  AWin32Version: Extended;
  os:string;
begin
  os:='Windows ';
  AWin32Version := StrtoFloat(format('%d.%d' ,[Win32MajorVersion, Win32MinorVersion]));
  if Win32Platform=VER_PLATFORM_WIN32s then
    Result := os + '32'
  else if Win32Platform=VER_PLATFORM_WIN32_WINDOWS then
  begin
    if AWin32Version=4.0 then
      Result := os + '95'
    else if AWin32Version=4.1 then
      Result := os + '98'
    else if AWin32Version=4.9 then
      Result := os + 'Me'
    else
      Result := os + '9x'
  end
  else if Win32Platform = VER_PLATFORM_WIN32_NT then
  begin
    if AWin32Version=3.51 then
      Result := os + 'NT 3.51'
    else if AWin32Version=4.0 then
      Result := os + 'NT 4.0'
    else if AWin32Version=5.0 then
      Result := os + '2000'
    else if AWin32Version=5.1 then
      Result := os + 'XP'
    else if AWin32Version=5.2 then
      Result := os + '2003'
    else if AWin32Version=6.0 then
      Result := os + 'Vista'
    else if AWin32Version=6.1 then
      Result := os + '7'
    else
      Result := os ;
  end
  else
    Result := os + '??';
end;

function ABWindowsBootMode: string;
begin
  case (GetSystemMetrics(SM_CLEANBOOT)) of
    0: Result := ('正常模式启动');
    1: Result := ('安全模式启动');
    2: Result := ('安全模式启动,但附带网络功能');
  else
    Result := ('错误:系统启动有问题.');
  end;
end;

function ABGetDriverName(aPathAndFileName:string;aDeleteMaoHao:boolean): string;
begin
  result := emptystr;
  if pos(':',aPathAndFileName)>0 then
  begin
    if aDeleteMaoHao then
      result:=copy(aPathAndFileName,1,pos(':',aPathAndFileName)-1)
    else
      result:=copy(aPathAndFileName,1,pos(':',aPathAndFileName));
  end;
end;

function ABGetDiskVolumeSerialNumber(aDiskChar: Char): string;
var
  SerialNum: dword;
  a, b: dword;
  Buffer: array[0..255] of char;
begin
  result := '';
  if GetVolumeInformation(PChar(aDiskChar + ':\'), Buffer, SizeOf(Buffer), @SerialNum, a, b, nil, 0) then
    Result := IntToStr(SerialNum);
end;

function ABDiskVolumeIsOK(aDiskChar: Char): Boolean;
var
  Oem: CARDINAL;
  Dw1, Dw2: DWORD;
begin
  Oem := SetErrorMode(SEM_FAILCRITICALERRORS);
  Result := GetVolumeInformation(PChar(aDiskChar + ':\'), nil, 0, nil, Dw1, Dw2, nil, 0);
  SetErrorMode(Oem);
end;

function ABCPUSpeed: Double;
const
  DelayTime = 500;
var
  TimerHi, TimerLo: DWORD;
  PriorityClass, Priority: Integer;
begin
  PriorityClass := GetPriorityClass(GetCurrentProcess);
  Priority := GetThreadPriority(GetCurrentThread);
  SetPriorityClass(GetCurrentProcess, REALTIME_PRIORITY_CLASS);
  SetThreadPriority(GetCurrentThread, THREAD_PRIORITY_TIME_CRITICAL);
  Sleep(10);
  asm
dw 310Fh
mov TimerLo, eax
mov TimerHi, edx
  end;
  Sleep(DelayTime);
  asm
dw 310Fh
sub eax, TimerLo
sbb edx, TimerHi
mov TimerLo, eax
mov TimerHi, edx
  end;
  SetThreadPriority(GetCurrentThread, Priority);
  SetPriorityClass(GetCurrentProcess, PriorityClass);
  Result := TimerLo / (1000.0 * DelayTime);
end;

function ABGetDiskSerialNumber(aResLeng: LongInt): string;
type
  TSrbIoControl = packed record
    HeaderLength: ULONG;
    Signature: array[0..7] of Char;
    Timeout: ULONG;
    ControlCode: ULONG;
    ReturnCode: ULONG;
    Length: ULONG;
  end;
  SRB_IO_CONTROL = TSrbIoControl;
  PSrbIoControl = ^TSrbIoControl;

  TIDERegs = packed record
    bFeaturesReg: Byte; // Used for specifying SMART "commands".
    bSectorCountReg: Byte; // IDE sector count register
    bSectorNumberReg: Byte; // IDE sector number register
    bCylLowReg: Byte; // IDE low order cylinder value
    bCylHighReg: Byte; // IDE high order cylinder value
    bDriveHeadReg: Byte; // IDE drive/head register
    bCommandReg: Byte; // Actual IDE command.
    bReserved: Byte; // reserved. Must be zero.
  end;
  IDEREGS = TIDERegs;
  PIDERegs = ^TIDERegs;

  TSendCmdInParams = packed record
    cBufferSize: DWORD;
    irDriveRegs: TIDERegs;
    bDriveNumber: Byte;
    bReserved: array[0..2] of Byte;
    dwReserved: array[0..3] of DWORD;
    bBuffer: array[0..0] of Byte;
  end;
  SENDCMDINPARAMS = TSendCmdInParams;
  PSendCmdInParams = ^TSendCmdInParams;

  TIdSector = packed record
    wGenConfig: Word;
    wNumCyls: Word;
    wReserved: Word;
    wNumHeads: Word;
    wBytesPerTrack: Word;
    wBytesPerSector: Word;
    wSectorsPerTrack: Word;
    wVendorUnique: array[0..2] of Word;
    sSerialNumber: array[0..19] of Char;
    wBufferType: Word;
    wBufferSize: Word;
    wECCSize: Word;
    sFirmwareRev: array[0..7] of Char;
    sModelNumber: array[0..39] of Char;
    wMoreVendorUnique: Word;
    wDoubleWordIO: Word;
    wCapabilities: Word;
    wReserved1: Word;
    wPIOTiming: Word;
    wDMATiming: Word;
    wBS: Word;
    wNumCurrentCyls: Word;
    wNumCurrentHeads: Word;
    wNumCurrentSectorsPerTrack: Word;
    ulCurrentSectorCapacity: ULONG;
    wMultSectorStuff: Word;
    ulTotalAddressableSectors: ULONG;
    wSingleWordDMA: Word;
    wMultiWordDMA: Word;
    bReserved: array[0..127] of Byte;
  end;
  PIdSector = ^TIdSector;

const
  IDE_ID_FUNCTION = $EC;
  IDENTIFY_BUFFER_SIZE = 512;
  DFP_RECEIVE_DRIVE_DATA = $0007C088;
  IOCTL_SCSI_MINIPORT = $0004D008;
  IOCTL_SCSI_MINIPORT_IDENTIFY = $001B0501;
  DataSize = sizeof(TSendCmdInParams) - 1 + IDENTIFY_BUFFER_SIZE;
  BufferSize = SizeOf(SRB_IO_CONTROL) + DataSize;
  W9xBufferSize = IDENTIFY_BUFFER_SIZE + 16;
var
  hDevice: THandle;
  cbBytesReturned: DWORD;
  pInData: PSendCmdInParams;
  pOutData: Pointer; // PSendCmdOutParams
  Buffer: array[0..BufferSize - 1] of Byte;
  srbControl: TSrbIoControl absolute Buffer;
  tempPIdSector:PIdSector;

  procedure ChangeByteOrder(var Data; Size: Integer);
  var
    ptr: PChar;
    i: Integer;
    c: Char;
  begin
    ptr := @Data;
    for i := 0 to (Size shr 1) - 1 do
    begin
      c := ptr^;
      ptr^ := (ptr + 1)^;
      (ptr + 1)^ := c;
      Inc(ptr, 2);
    end;
  end;

begin
  Result := '';
  FillChar(Buffer, BufferSize, #0);
  if Win32Platform = VER_PLATFORM_WIN32_NT then
  begin // Windows NT, Windows 2000
    // Get SCSI port handle
    hDevice := CreateFile('\\.\Scsi0:',
      GENERIC_READ or GENERIC_WRITE,
      FILE_SHARE_READ or FILE_SHARE_WRITE,
      nil, OPEN_EXISTING, 0, 0);
    if hDevice = INVALID_HANDLE_VALUE then
      Exit;
    try
      srbControl.HeaderLength := SizeOf(SRB_IO_CONTROL);
      System.Move('SCSIDISK', srbControl.Signature, 8);
      srbControl.Timeout := 2;
      srbControl.Length := DataSize;
      srbControl.ControlCode := IOCTL_SCSI_MINIPORT_IDENTIFY;
      pInData := PSendCmdInParams(PChar(@Buffer)
        + SizeOf(SRB_IO_CONTROL));
      pOutData := pInData;
      pInData.cBufferSize := IDENTIFY_BUFFER_SIZE;
      pInData.bDriveNumber := 0;
      pInData.irDriveRegs.bFeaturesReg := 0;
      pInData.irDriveRegs.bSectorCountReg := 1;
      pInData.irDriveRegs.bSectorNumberReg := 1;
      pInData.irDriveRegs.bCylLowReg := 0;
      pInData.irDriveRegs.bCylHighReg := 0;
      pInData.irDriveRegs.bDriveHeadReg := $A0;
      pInData.irDriveRegs.bCommandReg := IDE_ID_FUNCTION;
      if not DeviceIoControl(hDevice, IOCTL_SCSI_MINIPORT,
        @Buffer, BufferSize, @Buffer, BufferSize,
        cbBytesReturned, nil) then
        Exit;
    finally
      CloseHandle(hDevice);
    end;
  end
  else
  begin // Windows 95 OSR2, Windows 98
    hDevice := CreateFile('\\.\SMARTVSD', 0, 0, nil,
      CREATE_NEW, 0, 0);
    if hDevice = INVALID_HANDLE_VALUE then
      Exit;
    try
      pInData := PSendCmdInParams(@Buffer);
      pOutData := @pInData^.bBuffer;
      pInData.cBufferSize := IDENTIFY_BUFFER_SIZE;
      pInData.bDriveNumber := 0;
      pInData.irDriveRegs.bFeaturesReg := 0;
      pInData.irDriveRegs.bSectorCountReg := 1;
      pInData.irDriveRegs.bSectorNumberReg := 1;
      pInData.irDriveRegs.bCylLowReg := 0;
      pInData.irDriveRegs.bCylHighReg := 0;
      pInData.irDriveRegs.bDriveHeadReg := $A0;
      pInData.irDriveRegs.bCommandReg := IDE_ID_FUNCTION;
      if not DeviceIoControl(hDevice, DFP_RECEIVE_DRIVE_DATA,
        pInData, SizeOf(TSendCmdInParams) - 1, pOutData,
        W9xBufferSize, cbBytesReturned, nil) then
        Exit;
    finally
      CloseHandle(hDevice);
    end;
  end;
  tempPIdSector:=PIdSector(PChar(pOutData) + 16);
  ChangeByteOrder(tempPIdSector.sSerialNumber, SizeOf(tempPIdSector.sSerialNumber));
  SetString(Result, tempPIdSector.sSerialNumber, SizeOf(tempPIdSector.sSerialNumber));
  Result := Copy(result, 1, aResLeng);
end;

function ABGetTotalMemory: Dword;
var
  memStatus: TMemoryStatus;
begin
  memStatus.dwLength := sizeOf(memStatus);
  GlobalMemoryStatus(memStatus);
  Result := memStatus.dwTotalPhys div 1024;
end;

function ABGetCanUseMemory: Dword;
var
  memStatus: TMemoryStatus;
begin
  memStatus.dwLength := sizeOf(memStatus);
  GlobalMemoryStatus(memStatus);
  Result := memStatus.dwAvailPhys div 1024;
end;

function ABGetProcessMemorySize(aFilename: string): LongInt;
var
  pProcess:THandle;
  hProcSnap:THandle;
  PPMCSize:Cardinal;
  pe32:TProcessEntry32;
  PPMC:PPROCESS_MEMORY_COUNTERS;
begin
  result:=0;
  aFilename:=ABGetFilename(aFilename,false);
  PPMCSize := SizeOf(PROCESS_MEMORY_COUNTERS);
  GetMem(PPMC, PPMCSize);
  PPMC^.cb:= PPMCSize;

  hProcSnap:=CreateToolHelp32SnapShot(TH32CS_SNAPALL,  0);
  if  hProcSnap=INVALID_HANDLE_VALUE  then
    Exit;
  pe32.dwSize:=SizeOf(ProcessEntry32);
  if  Process32First(hProcSnap,pe32)=True then
  begin
    while Process32Next(hProcSnap,pe32)=True  do
    begin
      if  uppercase(pe32.szExeFile)=uppercase(aFilename)  then
      begin
        pProcess:=OpenProcess(PROCESS_ALL_ACCESS,FALSE, pe32.th32ProcessID);

        if (GetProcessMemoryInfo(pProcess,PPMC,PPMCSize)) then
        begin
          result:=PPMC^.WorkingSetSize div 1024;
          FreeMem(PPMC);
        end
        else
        begin
          FreeMem(PPMC);
        end;
        CloseHandle(pProcess);
      end;
    end;
  end;
  CloseHandle(hProcSnap);
end;

// 从Winsock 2.0导入函数WSAIOCtl
function WSAIoctl(s: TSocket; cmd: DWORD; lpInBuffer: PCHAR; dwInBufferLen:
  DWORD;
  lpOutBuffer: PCHAR; dwOutBufferLen: DWORD;
  lpdwOutBytesReturned: LPDWORD;
  lpOverLapped: POINTER;
  lpOverLappedRoutine: POINTER): Integer; stdcall; external 'WS2_32.DLL';

const
  SIO_GET_INTERFACE_LIST = $4004747F;
  IFF_UP = $00000001;
  IFF_BROADCAST = $00000002;
  IFF_LOOPBACK = $00000004;
  IFF_POINTTOPOINT = $00000008;
  IFF_MULTICAST = $00000010;

type
  sockaddr_gen = packed record
    AddressIn: sockaddr_in;
    filler: packed array[0..7] of AnsiChar;
  end;

  INTERFACE_INFO = packed record
    iiFlags: u_long;                    // Interface flags
    iiAddress: sockaddr_gen;            // Interface address
    iiBroadcastAddress: sockaddr_gen;   // Broadcast address
    iiNetmask: sockaddr_gen;            // Network mask
  end;

function ABGetBroadIP:string;
var
  tempWSAData: TWSAData;
  s: TSocket;
  NumInterfaces: Integer;
  BytesReturned, SetFlags: u_long;
  pAddr, pMask, pCast: TInAddr;
  PtrA: pointer;
  Buffer: array[0..20] of INTERFACE_INFO;
  i: Integer;
begin
  result := EmptyStr;
  if WSAStartup($0202, tempWSAData)=0 then
  begin
    try
      s := Socket(AF_INET, SOCK_STREAM, 0); // Open a socket
      if (s <> INVALID_SOCKET) then
      begin
        try                                   // Call WSAIoCtl
          PtrA := @bytesReturned;
          if (WSAIoCtl(s, SIO_GET_INTERFACE_LIST, nil, 0, @Buffer, 1024, PtrA, nil,
            nil) <> SOCKET_ERROR) then
          begin                               // If ok, find out how
            // many interfaces exist
            NumInterfaces := BytesReturned div SizeOf(INTERFACE_INFO);
            for i := 0 to NumInterfaces - 1 do // For every interface
            begin
              SetFlags := Buffer[i].iiFlags;
              if (SetFlags and IFF_BROADCAST = IFF_BROADCAST) and not
                (SetFlags and IFF_LOOPBACK = IFF_LOOPBACK) then
              begin
                pAddr := Buffer[i].iiAddress.AddressIn.sin_addr;
                pMask := Buffer[i].iiNetmask.AddressIn.sin_addr;
                pCast.S_addr := pAddr.S_addr or not pMask.S_addr;
                result := string(inet_ntoa(pCast));
              end;
            end;
          end;
        finally
          CloseSocket(s);
        end;
      end;
    finally
      WSACleanup;
    end;
  end;
end;

function ABGetMacAddress(aHostName: string): string;
type
  TNetTransportEnum = function(pszServer: PWideChar;
    Level: DWORD;
    var pbBuffer: pointer;
    PrefMaxLen: LongInt;
    var EntriesRead: DWORD;
    var TotalEntries: DWORD;
    var ResumeHandle: DWORD): DWORD; stdcall;

  TNetApiBufferFree = function(Buffer: pointer): DWORD; stdcall;

  PTransportInfo = ^TTransportInfo;
  TTransportInfo = record
    quality_of_service: DWORD;
    number_of_vcs: DWORD;
    transport_name: PWChar;
    transport_address: PWChar;
    wan_ish: boolean;

  end;
var
  E, ResumeHandle,
    EntriesRead,
    TotalEntries: DWORD;
  FLibHandle: THandle;
  sMachineName,
    sMacAddr,
    Retvar: string;
  pBuffer: pointer;
  pInfo: PTransportInfo;
  FNetTransportEnum: TNetTransportEnum;
  FNetApiBufferFree: TNetApiBufferFree;
  pszServer: array[0..128] of WideChar;
  i, ii, iIdx: integer;
begin
  sMachineName := aHostName;
  Retvar := '00-00-00-00-00-00';

  // Add leading \\ if missing
  if (sMachineName <> EmptyStr) and (length(sMachineName) >= 2) then
  begin
    if copy(sMachineName, 1, 2) <> '\\' then
      sMachineName := '\\' + sMachineName
  end;

  // Setup and load from DLL
  pBuffer := nil;
  ResumeHandle := 0;
  FLibHandle := LoadLibrary('NETAPI32.DLL');

  // Execute the external function
  if FLibHandle <> 0 then
  begin
    @FNetTransportEnum := GetProcAddress(FLibHandle, 'NetWkstaTransportEnum');
    @FNetApiBufferFree := GetProcAddress(FLibHandle, 'NetApiBufferFree');
    E := FNetTransportEnum(StringToWideChar(sMachineName, pszServer, 129), 0,
      pBuffer, -1, EntriesRead, TotalEntries, Resumehandle);

    if E = 0 then
    begin
      pInfo := pBuffer;

      // Enumerate all protocols - look for TCPIP
      for i := 1 to EntriesRead do
      begin
        if ABPos('TCPIP', pInfo^.transport_name) <> 0 then
        begin
          // Got It - now format result 'xx-xx-xx-xx-xx-xx'
          iIdx := 1;
          sMacAddr := pInfo^.transport_address;

          for ii := 1 to 12 do
          begin
            Retvar[iIdx] := sMacAddr[ii];
            inc(iIdx);
            if iIdx in [3, 6, 9, 12, 15] then
              inc(iIdx);
          end;
        end;

        inc(pInfo);
      end;
      if Assigned(pBuffer) then
        FNetApiBufferFree(pBuffer);
    end;

    try
      FreeLibrary(FLibHandle);
    except
      // 错误处理
      raise;
    end;
  end;
  result := Retvar;
end;


procedure ABGetHostInfo(var aHostName,aHostIP,aMacAddress:string);
  function GetHostName:String;
  var
    ComputerName: array[0..MAX_COMPUTERNAME_LENGTH+1] of char;
    Size: Cardinal;
  begin
    result:='';
    Size := MAX_COMPUTERNAME_LENGTH+1;
    GetComputerName(ComputerName, Size);
    Result:=StrPas(ComputerName);
  end;
var
   HostEnt: PHostEnt;
  tempWSAData: TWSAData;
begin
  if WSAStartup($0202, tempWSAData)=0 then
  begin
    try
      aHostName:=GetHostName;
      HostEnt :=gethostbyname(PAnsiChar(AnsiString(aHostName)));
        aHostIP := Format('%d.%d.%d.%d',
                         [Byte(HostEnt.h_addr^[0]),
                          Byte(HostEnt.h_addr^[1]),
                          Byte(HostEnt.h_addr^[2]),
                          Byte(HostEnt.h_addr^[3])
                          ]
                          );
      aMacAddress:=ABGetMacAddress('');
    finally
      WSACleanup;
    end;
  end;
end;

function ABGetKeyBoardStatus(aCheckKeys:array of longint):TBooleanDynArray;
var
  KeyStates:TKeyboardState;
  I: Integer;
begin
  GetKeyboardState(KeyStates);
  SetLength(Result,High(aCheckKeys)+1);
  for I := Low(aCheckKeys) to High(aCheckKeys) do
  begin
    Result[i]:=Odd(KeyStates[aCheckKeys[i]]);
  end;
end;

procedure ABOpenIME(aImeName: string);
var
  i: integer;
  MyHKL: hkl;
begin
  if aImeName <> '' then
  begin
    if Screen.Imes.Count <> 0 then
    begin
      i := Screen.Imes.IndexOf(aImeName);
      if i >= 0 then
      begin
        MyHKL := hkl(Screen.Imes.Objects[i]);
        ActivateKeyboardLayout(MyHKL, KLF_ACTIVATE);
      end;
    end;
  end;
end;

//关闭输入法

procedure ABCloseIME;
var
  MyHKL: hkl;
begin
  MyHKL := GetKeyboardLayout(0);
  if ImmIsIme(MyHKL) then
    ImmSimulateHotKey(Application.Handle, IME_CHOTKEY_IME_NONIME_TOGGLE);
end;

function ABHook_Func(aOldAddress, aNewAddress: Pointer): TAB4ByteDynArray;
var
  dwFlag: DWORD;
  NewCode: array[0..4] of Byte;
begin
  //改变本进程内虚拟地址页的保护属性
  VirtualProtect(aOldAddress, 5, PAGE_EXECUTE_READWRITE, dwFlag);

  Move(aOldAddress^, Result, 5);
  NewCode[0] := $E9;
  PDWORD(@NewCode[1])^ := DWORD(aNewAddress) - DWORD(aOldAddress) - 5;

  Move(NewCode, aOldAddress^, 5);
  VirtualProtect(aOldAddress, 5, dwFlag, dwFlag);
end;

procedure ABUnHook_Func(aCurAddress: Pointer; aBackAddress: TAB4ByteDynArray);
var
  dwFlag: DWORD;
begin
  VirtualProtect(aCurAddress, 5, PAGE_EXECUTE_READWRITE, dwFlag);
  Move(aBackAddress, aCurAddress^, 5);
  VirtualProtect(aCurAddress, 5, dwFlag, dwFlag);
end;

function ABGetControlClassName(aHandle: HWND): string;
begin
  SetLength(Result, 255);
  GetClassName(aHandle, PChar(Result), Length(Result));
end;

function ABGetControlCaption(aHandle: HWND): string;
begin
  SetLength(Result, GetWindowTextLength(aHandle) + 1);
  GetWindowText(aHandle, PChar(Result), Length(Result));
  Result := PChar(Result);
end;


var
  FFindCaption,
  FFindClassName,
  FReplaceCaption: string;

  FReplace,
  FPartFindReplace,
  FReplaceAll,
  FReplaceClientControl,
  FFindReplaceEnd: Boolean;
  FFindHWnd: HWnd;


function DoEnumWindowsProc(aHandle: HWnd; aLPARAM: lParam): boolean; stdcall;
  function DoFindReplace(aHandle: HWnd):boolean;
  var
    tempCaptionChars: array[0..254] of char;
    tempClassNameChars: array[0..254] of char;
    tempCaption,
    tempClassName,
    tempStr1: string;
  begin
    result:=false;
    if (GetWindowText(aHandle, tempCaptionChars, 254)<>0) and
       (GetClassName(aHandle, tempClassNameChars, 254)<>0) then
    begin
      tempCaption   := tempCaptionChars;
      tempClassName := tempClassNameChars;

      if ((FFindClassName=EmptyStr) or
          (AnsiCompareText(FFindClassName, tempClassName) = 0) or
          (FPartFindReplace) and (ABPos(FFindClassName,tempClassName)>0)
          ) and
         ((FFindCaption=EmptyStr) or
          (AnsiCompareText(FFindCaption, tempCaption) = 0) or
          (FPartFindReplace) and (ABPos(FFindCaption,tempCaption)>0)
          )
           then
      begin
        if FReplace then
        begin
          //修改标题
          if FPartFindReplace then
          begin
            tempStr1:=ABStringReplace(tempCaption,FFindCaption,FReplaceCaption);
          end
          else
          begin
            tempStr1:=FReplaceCaption;
          end;
          SendMessage(aHandle, WM_SETTEXT, 0, Integer(tempStr1));
        end;

        if not FReplaceAll then
        begin
          FFindReplaceEnd := True;
        end;

        FFindHWnd:=aHandle;
        result:=True;
      end;
    end;
  end;
  function DoEnumClentWindowsProc(aHandle: HWnd; aLPARAM: lParam): Boolean; stdcall;
  begin
    Result := true;
    if DoFindReplace(aHandle) then
    begin
      Result := not FFindReplaceEnd;
    end;
  end;
begin
  Result := true;
  FFindHWnd:=0;
  if DoFindReplace(aHandle) then
  begin
    Result := not FFindReplaceEnd;
  end;

  if (FReplaceClientControl) and
     (not FFindReplaceEnd) then
  begin
    Enumchildwindows(aHandle,@DoEnumClentWindowsProc,0)
  end;
end;

procedure ABReplaceControlCaption(var aFindFindHWnd: HWnd;
                                   aFindCaption:string;
                                   aFindClassName:string;

                                   aReplace:boolean;
                                   aReplaceCaption:string;

                                   aPartFindReplace:boolean;
                                   aReplaceClientControl:Boolean;
                                   aReplaceAll:Boolean);
begin
  FFindCaption:=aFindCaption;
  FFindClassName:=aFindClassName;
  FReplaceCaption:=aReplaceCaption;

  FPartFindReplace:=aPartFindReplace;
  FReplaceAll:=aReplaceAll;
  FReplaceClientControl:=aReplaceClientControl;

  FFindReplaceEnd:=false;
  FReplace:=aReplace;
  FFindHWnd:=0;
  EnumWindows(@DoEnumWindowsProc, 0);
  aFindFindHWnd:= FFindHWnd;
end;

function ABTicker: DWORD; register;
begin
  Result:= GetTickCount;
end;

procedure ABCreateOdbc(const aAccessFilePath, aODBCName: string);
var
  registertemp: TRegistry;
  bData: array[0..0] of byte;
  dll32_path: string;
begin
  setlength(dll32_path, 255);
  if GetSystemDirectory(PChar(dll32_path), 255) <> 0 then
  begin
    setlength(dll32_path, strlen(pchar(dll32_path)));
    dll32_path := dll32_path + '\odbcjt32.dll';
  end;

  registertemp := TRegistry.Create;
  try
    registertemp.RootKey := HKEY_LOCAL_MACHINE;
    if registertemp.OpenKey('Software\ODBC\ODBC.INI\' + aODBCName + '\Engines\Jet', false) then
    begin
    end
    else
    begin
      //设置根键值为HKEY_LOCAL_MACHINE
      //找到Software\ODBC\ODBC.INI\ODBC Data Sources
      if registertemp.OpenKey('Software\ODBC\ODBC.INI\ODBC Data Sources', True) then
      begin //注册一个DSN名称
        registertemp.WriteString(aODBCName, 'Microsoft Access Driver (*.mdb)');
        registertemp.CloseKey;
        //找到或创建Software\ODBC\ODBC.INI\aODBCName,写入DSN配置信息
        if registertemp.OpenKey('Software\ODBC\ODBC.INI\' + aODBCName, True) then
        begin
          registertemp.WriteString('DBQ', aAccessFilePath);
          //数据库目录,连接您的数据库
          registertemp.WriteString('Description', '软件公有数据源 '); //数据源描述
          registertemp.WriteString('Driver', dll32_path); //驱动程序DLL文件
          registertemp.WriteInteger('DriverId', 25); //驱动程序标识
          registertemp.WriteString('FIL', 'Ms Access;'); //Filter依据
          registertemp.WriteInteger('SafeTransaction', 0); //支持的事务操作数目
          registertemp.WriteString('UID', EmptyStr); //用户名称
          bData[0] := 0;
          registertemp.WriteBinaryData('Exclusive', bData, 1); //非独占方式
          registertemp.WriteBinaryData('ReadOnly', bData, 1); //非只读方式

          registertemp.CloseKey;
          //找到或创建Software\ODBC\ODBC.INI\aODBCName\Engines\Jet
          //写入DSN数据库引擎配置信息
          if registertemp.OpenKey('Software\ODBC\ODBC.INI\' + aODBCName + '\Engines\Jet', True) then
          begin
            registertemp.WriteString('ImplicitCommitSync', 'Yes');
            registertemp.WriteInteger('MaxBufferSize', 5120); //缓冲区大小
            registertemp.WriteInteger('PageTimeout', 10); //页超时
            registertemp.WriteInteger('Threads', 3); //支持的线程数目
            registertemp.WriteString('UserCommitSync', 'Yes');
            registertemp.CloseKey;
          end;
        end;
      end;
    end;
  finally
    registertemp.Free;
  end;
end;

function ABGetCpuID(aIndex: longint): string;
  function GetCnCPUID(): string;
  const
    CPUINFO = '%.8x-%.8x-%.8x-%.8x';
  var
    iEax: Integer;
    iEbx: Integer;
    iEcx: Integer;
    iEdx: Integer;
  begin
    asm
      push ebx
      push ecx
      push edx
      mov  eax, 1
      DW $A20F//cpuid
      mov  iEax, eax
      mov  iEbx, ebx
      mov  iEcx, ecx
      mov  iEdx, edx
      pop edx
      pop ecx
      pop ebx
    end;
    Result := Format(CPUINFO, [iEax, iEbx, iEcx, iEdx]);
  end;

  procedure SetCPU(h: THandle;CpuNo: longint);
    //CpuNo:决定了获得第几个cpu内核的第几个序列号.
  var
    {$IF (CompilerVersion>=23.0)}      //230 =EX2
      ProcessAffinity: NativeuInt;
      _SystemAffinity: NativeuInt;
    {$ELSE}
      ProcessAffinity: Cardinal;
      _SystemAffinity: Cardinal;
    {$IFEND}
  begin
    GetProcessAffinityMask(h, ProcessAffinity, _SystemAffinity);
    ProcessAffinity := CpuNo; //this sets the process to only run on CPU 0
    //for CPU 1 only use 2 and for CPUs 1 & 2 use 3
    SetProcessAffinityMask(h, ProcessAffinity)
  end;
begin
  SetCPU(GetCurrentProcess, aIndex);
  Result := GetCnCPUID;
end;

function ABDynamicResolution(aWidth, aHeight: WORD): Boolean;
var
  lpDevMode: TDeviceMode;
begin
  Result := EnumDisplaySettings(nil, 0, lpDevMode);
  if Result then
  begin
    lpDevMode.dmFields := DM_PELSWIDTH or DM_PELSHEIGHT;
    lpDevMode.dmPelsWidth := aWidth;
    lpDevMode.dmPelsHeight := aHeight;
    Result := ChangeDisplaySettings(lpDevMode, 0) = DISP_CHANGE_SUCCESSFUL;
  end;
end;

procedure ABSetControlOnTop(aHandle: HWND; aOnTop: Boolean);
const
  csOnTop: array[Boolean] of HWND = (HWND_NOTOPMOST, HWND_TOPMOST);
begin
  SetWindowPos(aHandle, csOnTop[aOnTop], 0, 0, 0, 0, SWP_NOMOVE or SWP_NOSIZE);
end;

procedure ABSetControlTaskBarVisible(aHandle: HWND; aVisible: Boolean);
var
  WndLong: Integer;
begin
  WndLong := 0;
  ShowWindow(aHandle, SW_HIDE);
  if not aVisible then
    SetWindowLong(aHandle, GWL_EXSTYLE, WndLong or WS_EX_TOOLWINDOW and not WS_EX_APPWINDOW or WS_EX_TOPMOST)
  else
    SetWindowLong(aHandle, GWL_EXSTYLE, WndLong);

  ShowWindow(aHandle, SW_SHOW);
end;

procedure ABSetTaskBarVisible(aVisible: Boolean);
const
  csWndShowFlag: array[Boolean] of DWORD = (SW_HIDE, SW_RESTORE);
var
  wndHandle: THandle;
begin
  wndHandle := FindWindow('Shell_TrayWnd', nil);
  ShowWindow(wndHandle, csWndShowFlag[aVisible]);
end;

procedure ABSetDesktopVisible(aVisible: Boolean);
const
  csWndShowFlag: array[Boolean] of DWORD = (SW_HIDE, SW_RESTORE);
var
  hDesktop: THandle;
begin
  hDesktop := FindWindow('Progman', nil);
  ShowWindow(hDesktop, csWndShowFlag[aVisible]);
end;

procedure ABDoBusy(aBusy: boolean);
begin
  if aBusy then
  begin
    Screen.Cursor := crHourGlass;
  end
  else
  begin
    Screen.Cursor := crDefault;
  end;
end;

function ABSynLocalDatetime(aServerHostName: string): boolean;
begin
	Result:=false;
  try
    WinExec(PAnsiChar(AnsiString('net time \\'+trim(aServerHostName)+' /set /yes')), sw_hide);
  except
		exit;
  end;
	Result:=true;
end;

//取得控制权
procedure ABGetPrivilege;
var
  NewState:       TTokenPrivileges;
  lpLuid:         Int64;
  ReturnLength:   DWord;
  {$IF (CompilerVersion>=26.0)}
  ToKenHandle:    NativeUInt;
  {$ELSE}
  ToKenHandle:    Cardinal;
  {$IFEND}
begin
  OpenProcessToken(GetCurrentProcess,
                   TOKEN_ADJUST_PRIVILEGES
                   OR TOKEN_ALL_ACCESS
                   OR STANDARD_RIGHTS_REQUIRED
                   OR TOKEN_QUERY,ToKenHandle);
  LookupPrivilegeValue(nil,'SeShutdownPrivilege',lpLuid);
  NewState.PrivilegeCount:=1;
  NewState.Privileges[0].Luid:=lpLuid;
  NewState.Privileges[0].Attributes:=SE_PRIVILEGE_ENABLED;
  ReturnLength:=0;
  AdjustTokenPrivileges(ToKenHandle,False,NewState,0,nil,ReturnLength);
end;

procedure ABRebootComputer;
begin
  ABGetPrivilege;
  ExitWindowsEx(EWX_REBOOT, 0);
end;

procedure ABSHUTDOWNComputer;
begin
  ABGetPrivilege;
  ExitWindowsEx(EWX_SHUTDOWN OR EWX_POWEROFF, 0);
end;

procedure ABLOGOFF;
begin
  ExitWindowsEx(EWX_LOGOFF, 0);
end;

function ABFindAProcess(aFilename:string):DWORD;
var
  lppe:TProcessEntry32;
  SsHandle:Thandle;
  FoundAProc:boolean;
begin
  result:=0;
  AFilename:=ABGetFilename(aFilename,false);

  SsHandle   := CreateToolHelp32SnapShot(TH32CS_SNAPALL,0);
  lppe.dwSize := Sizeof(lppe); //初始化
  FoundAProc := Process32First(Sshandle,lppe);
  //枚举Process,然后判断是否是所要查找的Process
  while FoundAProc do
  begin
    if AnsiStricomp(PChar(ExtractFilename(lppe.szExefile)),PChar(AFilename))=0 then
    begin
      result:=lppe.th32ProcessID;
      break;
    end;
    FoundAProc :=Process32Next(SsHandle,lppe);
  end;
//  if not FoundAProc then showmessage(SysErrorMessage(GetLastError));
  CloseHandle(SsHandle);
end;

procedure ABAutoTabOrder(const aParent  : TWinControl;const aSetClient: Boolean);
var
  i : Integer;
  j : Integer;
  b : Boolean;
  c : TWinControl;
  lst : TList;
begin
  lst :=  TList.Create;
  try
    for i := 0 to aParent.ControlCount - 1 do
      if aParent.Controls[i] is TWinControl then
      begin
        c :=  TWinControl(aParent.Controls[i]);
        b :=  False;
        for j := 0 to lst.Count - 1 do
        begin
          if (c.Top < TWinControl(lst[j]).Top) or
              ((c.Top = TWinControl(lst[j]).Top) and (c.Left < TWinControl(lst[j]).Left)) then
          begin
            lst.Insert(j, c);
            b :=  True;
          end;
        end;
        if not b then
          lst.Add(c);
      end;
    for i := 0 to lst.Count - 1 do
    begin
      TWinControl(lst[i]).TabOrder  :=  i;
      if aSetClient then
        ABAutoTabOrder(TWinControl(lst[i]), aSetClient);
    end;
  finally
    lst.Free;
  end;
end;

//function AnimateWindow(hWnd: HWND; dwTime: DWORD; dwFlags: DWORD): BOOL; stdcall;
//   for i:=0 to ControlCount-1 do
//    Controls[i].Refresh;
//  BringToFront;
//  Visible:=true;
procedure ABAnimateWindow(aHandle: HWND;aTime: DWORD; aFlags: DWORD);
begin
{
var
  pOSVersionInfo: OSVersionInfo;

  pOSVersionInfo.dwOSVersionInfoSize := sizeof(OSVersionInfo);
  GetVersionEx(pOSVersionInfo);
  //判断是否是WIN2000及以上
  if pOSVersionInfo.dwPlatformId = VER_PLATFORM_WIN32_NT then
  begin
  end;
  }
  AnimateWindow(aHandle, aTime, aFlags);
end;

function ABGetSysLanguageStr(aLanguage: TABSysLanguageType): string;
begin
  Result := Copy(GetEnumName(TypeInfo(TABSysLanguageType), Ord(aLanguage)), 3,MaxInt);
end;

function ABGetSysLanguage: TABSysLanguageType;
begin
  result := wlAuto;
  case GetSystemDefaultLangID of
    $0404: result := (wlChinaTw); //'Chinese (Taiwan)';
    $0804: result := (wlChina); //'Chinese (PRC)';
    $0C04: result := (wlChinaTw); //'Chinese (Hong Kong)';
    $0411: result := (wlJapanese); //'Japanese';
    $0412: result := (wlKorean); //'Korean';
    $0409: result := (wlEnglish); //'English (United States)';
  end;
end;

function ABGetInternetKind: TABIntetnetType;
var
  flags: dword;
begin
  Result := itOther;
  if InternetGetConnectedState(@flags, 0) then
  begin
    if (flags and INTERNET_CONNECTION_MODEM) = INTERNET_CONNECTION_MODEM then
    begin
      Result := itModem;
    end;
    if (flags and INTERNET_CONNECTION_LAN) = INTERNET_CONNECTION_LAN then
    begin
      Result := itLAN;
    end;
    if (flags and INTERNET_CONNECTION_PROXY) = INTERNET_CONNECTION_PROXY then
    begin
      Result := itProxy;
    end;
    if (flags and INTERNET_CONNECTION_MODEM_BUSY) =
      INTERNET_CONNECTION_MODEM_BUSY then
    begin
      Result := itModemBusy;
    end
  end;
end;

function ABIsOnInternet: boolean;
begin
  if InternetCheckConnection('http://www.yahoo.com/',1,0) then
    result := true
  else
  begin
    if InternetCheckConnection('http://www.badu.com/',1,0) then
      result := true
    else
    begin
      if InternetCheckConnection('',1,0) then
        result := true
      else
      begin
        if InternetGetConnectedState(nil, 0) then
          result := true
        else
          result := false;
      end;
    end;
  end;
end;

function ABGetFtppath(aDirOrPath: string): string;
begin
  aDirOrPath := Trim(aDirOrPath);
  if Copy(aDirOrPath, Length(aDirOrPath), 1) <> '/' then
  begin
    aDirOrPath := aDirOrPath + '/';
  end;
  Result := aDirOrPath;
end;

procedure ABGetFtpInfo(aFtpAddress: string;
                      var aUserName,aPassWord,aHost,aPort,aDir:string);
var
  i, j: integer;
  s, tmp: string;
begin
  s := aFtpAddress;
  //去掉ftp头
  if Pos('ftp://', LowerCase(s)) <> 0 then
    Delete(s, 1, 6);

  i := Pos('@', s);
  if i <> 0 then //地址含用户名,也可能含密码
  begin
    tmp := Copy(s, 1, i - 1);
    s := copy(s, i + 1, Length(s));
    j := Pos(':', tmp);
    //包含密码
    if j <> 0 then
    begin
      aUserName := Copy(tmp, 1, j - 1); //得到用户名
      aPassWord := Copy(tmp, j + 1, i - j - 1); //得到密码
    end
    else
    begin
      aUserName := tmp;
      aPassWord := EmptyStr;
    end;
  end
  else
  //匿名用户
  begin
    aUserName := 'anonymous';
    aPassWord := 'IEUser@';
  end;

  i := Pos(':', s);
  j := Pos('/', s);
  //主机
  aHost:= Copy(s, 1, j - 1);
  //含端口
  if i <> 0 then
    aPort := Copy(s, i + 1, j - i - 1)
  else
    aPort := '21'; //默认21端口

  aDir := '/';
  if j > 0 then
  begin
    aDir :=aDir+ Copy(s, j + 1, Length(s));
  end;
end;

Function ABGetDefaultPrinterName : String;
var
  iSize : Integer;
  sIniFile, sSection, sKeyName : PChar;
begin
  iSize := 256;
  sIniFile := 'win.ini';
  sSection := 'windows';
  sKeyName := 'device';
  SetLength(Result,iSize);
  GetPrivateProfileString(sSection,sKeyName,nil,PChar(Result),iSize,sIniFile);
  Result := Copy(Result, 0, Pos(',',Result)-1);
end;

Function ABGetDefaultPrinterName(aPrintNameOrFuncName:string):string;
begin
  if aPrintNameOrFuncName=EmptyStr then
  begin
    if printers.printer.printers.Indexof(aPrintNameOrFuncName)>=0  then
      Result := aPrintNameOrFuncName
    else if (ABPubFuncPrintNames.IndexOfName(aPrintNameOrFuncName)>=0) and
            (printers.printer.printers.Indexof(ABPubFuncPrintNames.Values[aPrintNameOrFuncName])>=0) then
      Result := ABPubFuncPrintNames.Values[aPrintNameOrFuncName];
  end
  else
  begin
    Result :=ABGetDefaultPrinterName;
  end;
end;

procedure ABInitialization;
begin
  FEnvironmentStrings:= nil;
end;


procedure ABFinalization;
begin
  if Assigned(FEnvironmentStrings) then
    FEnvironmentStrings.Free;
end;

Initialization
  ABInitialization;

Finalization
  ABFinalization;


end.


