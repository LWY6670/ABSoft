{
增强文件列表控件单元(支持按时间排序)
}
unit ABPubFileListU;

interface
{$I ..\ABInclude.ini}

uses
  ABPubconstU,
  ABPubFuncU,

  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, FileCtrl;

type
  TABFileListBox = class(TFileListBox)
  private
  protected        
    procedure ReadFileNames;override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  Published
  end;


implementation

{ TABFileListBox }

constructor TABFileListBox.Create(AOwner: TComponent);
 begin
   inherited;
   Sorted := False; //禁止它自动排序
 end;

procedure TABFileListBox.ReadFileNames;
 var
   AttrIndex: TFileAttr;
   I: Integer;
   FileExt: string;
   MaskPtr: PChar;
   Ptr: PChar;
   AttrWord: Word;
   FileInfo: TSearchRec;
   SaveCursor: TCursor;
   Glyph: TBitmap;
   DateList :TStringList; //时间列表
 const
    Attributes: array[TFileAttr] of Word = (faReadOnly, faHidden, faSysFile,
    faVolumeID, faDirectory, faArchive, 0);

  procedure QuickSort(L, R: Integer); //快速排序
   var
     I, J: Integer;
     P: Integer;
   begin
     I := L;
     J := R;
     P := StrToInt(DateList[(L + R) shr 1]);
     repeat
       while StrToInt(DateList[I]) < P  do Inc(I);
       while StrToInt(DateList[J]) > P  do Dec(J);
       if I <= J then
         begin
           Items.Exchange(I, J);
           DateList.Exchange(I, J);
           Inc(I);
           Dec(J);
         end;
     until I > J;
      if L < J then
         QuickSort(L, J);
      if I < R then
         QuickSort(I, R);
   end;

begin
   DateList := TStringList.Create;
   AttrWord := DDL_READWRITE;
   if HandleAllocated then
   begin
     for AttrIndex := ftReadOnly to ftArchive do
       if AttrIndex in FileType then
         AttrWord := AttrWord or Attributes[AttrIndex];
     ChDir(FDirectory);
     Clear;
     I := 0;
     SaveCursor := Screen.Cursor;
     Items.BeginUpdate;
     try
       MaskPtr := PChar(FMask);
       while MaskPtr <> nil do
       begin
         Ptr := StrScan (MaskPtr, ';');
         if Ptr <> nil then
           Ptr^ := #0;
         if FindFirst(MaskPtr, AttrWord, FileInfo) = 0 then
         begin
           repeat
             if (ftNormal in FileType) or (FileInfo.Attr and AttrWord <> 0) then
               if FileInfo.Attr and faDirectory <> 0 then
               begin
                 I := Items.Add(Format('[%s]',[FileInfo.Name]));
                 if ShowGlyphs then
                   Items.Objects[I] := DirBMP;
               end
               else
               begin
                 FileExt := AnsiLowerCase(ExtractFileExt(FileInfo.Name));
                 Glyph := UnknownBMP;
                 if (FileExt = '.exe') or (FileExt = '.com') or
                   (FileExt = '.bat') or (FileExt = '.pif') then
                   Glyph := ExeBMP;
                I := Items.AddObject(FileInfo.Name, Glyph);
                DateList.Append(IntToStr(FileInfo.Time));
               end;
             if I = 100 then
               Screen.Cursor := crHourGlass;
           until FindNext(FileInfo) <> 0;
           FindClose(FileInfo);
         end;
         if Ptr <> nil then
         begin
           Ptr^ := ';';
           Inc (Ptr);
         end;
         MaskPtr := Ptr;
       end;
       if Items.Count>0 then
         QuickSort(0,Items.Count - 1 );
     finally
       Items.EndUpdate;
       Screen.Cursor := SaveCursor;
       DateList.Free;
     end;
     Change;
   end;
 end;

destructor TABFileListBox.Destroy;
begin

  inherited;
end;

end.


