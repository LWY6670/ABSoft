{
共用基类TFrame单元(其下的TFrame都具有多语文功能)
}
unit ABPubFrameU;
                             
interface
{$I ..\ABInclude.ini}

uses
  ABPubLanguageU,

  Classes,Controls,Forms;

type
  TABPubFrame = class(TFrame)
  private
    { Private declarations }
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    { Public declarations }
  Published
  end;

implementation
{$R *.dfm}

constructor TABPubFrame.Create(AOwner: TComponent);
begin
  inherited;
  ABLanguage.SetComponentsText(self);
end;

destructor TABPubFrame.Destroy;
begin
  inherited;
end;


end.


