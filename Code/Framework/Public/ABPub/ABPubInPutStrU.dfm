object ABInPutStrForm: TABInPutStrForm
  Left = 390
  Top = 283
  BorderStyle = bsDialog
  Caption = #36755#20837
  ClientHeight = 150
  ClientWidth = 300
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 300
    Height = 50
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object Label_Remark: TLabel
      Left = 0
      Top = 0
      Width = 300
      Height = 50
      Align = alClient
      Alignment = taCenter
      AutoSize = False
      Caption = #36755#20837
      Transparent = True
      Layout = tlCenter
      WordWrap = True
      ExplicitWidth = 291
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 50
    Width = 300
    Height = 100
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      300
      100)
    object Label_Input: TLabel
      Left = 16
      Top = 23
      Width = 71
      Height = 13
      AutoSize = False
      Caption = #36755#20837
      Transparent = True
    end
    object Button1: TButton
      Left = 67
      Top = 56
      Width = 75
      Height = 25
      Caption = #30830#35748
      Default = True
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 161
      Top = 56
      Width = 75
      Height = 25
      Cancel = True
      Caption = #21462#28040
      TabOrder = 1
      OnClick = Button2Click
    end
    object Text_Input: TEdit
      Left = 88
      Top = 20
      Width = 193
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      ImeName = #24555#20048#20116#31508
      TabOrder = 2
      OnKeyPress = Text_InputKeyPress
    end
  end
end
