unit ABManagePassWordTestU;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TABManagePassWordTestForm = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Button1: TButton;
    Edit1: TEdit;
    Edit2: TEdit;
    Button2: TButton;
    Button3: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ABManagePassWordTestForm: TABManagePassWordTestForm;

  function CheckPassword(aWord,aPassWord: pansichar):longint;stdcall;external 'ABManagePassWordD.Dll';
  function DOPassWord(aPassWord: pansichar):pansichar;stdcall;external 'ABManagePassWordD.Dll';
  function UnDOPassWord(aPassWord: pansichar):pansichar;stdcall;external 'ABManagePassWordD.Dll';

implementation

{$R *.dfm}

procedure TABManagePassWordTestForm.Button1Click(Sender: TObject);
begin
  if CheckPassword(pansichar(ansistring(Edit1.Text)),pansichar(ansistring(Edit2.Text)))=0 then
  begin
    ShowMessage('�ɹ�');
  end
  else
  begin
    ShowMessage('ʧ��');
  end;
end;

procedure TABManagePassWordTestForm.Button2Click(Sender: TObject);
begin
  Edit2.Text:=DOPassWord(pansichar(ansistring(Edit1.Text)));
end;

procedure TABManagePassWordTestForm.Button3Click(Sender: TObject);
begin
  Edit1.Text:=UnDOPassWord(pansichar(ansistring(Edit2.Text)));

end;

end.
