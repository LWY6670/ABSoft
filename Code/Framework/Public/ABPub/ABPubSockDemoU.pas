{
Socket通讯示例单元
}
unit ABPubSockDemoU;

interface
{$I ..\ABInclude.ini}

uses


  ABPubFormU,



  ABPubUserU,

  Classes, Controls, Forms, StdCtrls, ExtCtrls, Spin,
  ABPubSockFrameU;

type
  TABPubSockDemoForm = class(TABPubForm)
    Panel1: TPanel;
    ABSockUI1: TABSockUI;
    ABSockUI2: TABSockUI;
    Panel2: TPanel;
    ABSockUI3: TABSockUI;
    ABSockUI4: TABSockUI;
    procedure FormCreate(Sender: TObject);
    procedure ABSockUI1BegReadData(Sender: TObject);
    procedure ABSockUI1EndReadData(Sender: TObject);
  private
    { Private declarations }
  protected
  public
    { Public declarations }
  end;

procedure ABShowSockDemoForm;

implementation
{$R *.dfm}

procedure ABShowSockDemoForm;
var
  tempForm: TABPubSockDemoForm;
begin
  tempForm := TABPubSockDemoForm.Create(nil);
  try
    tempForm.ShowModal;
  finally
    tempForm.Free;
  end;
end;

procedure TABPubSockDemoForm.ABSockUI1BegReadData(Sender: TObject);
begin
  ABSockUI1.Frame.Memo2.Lines.BeginUpdate;
  ABSockUI2.Frame.Memo2.Lines.BeginUpdate;
  ABSockUI3.Frame.Memo2.Lines.BeginUpdate;
  ABSockUI4.Frame.Memo2.Lines.BeginUpdate;
end;

procedure TABPubSockDemoForm.ABSockUI1EndReadData(Sender: TObject);
begin
  ABSockUI1.Frame.Memo2.Lines.EndUpdate;
  ABSockUI2.Frame.Memo2.Lines.EndUpdate;
  ABSockUI3.Frame.Memo2.Lines.EndUpdate;
  ABSockUI4.Frame.Memo2.Lines.EndUpdate;
end;

procedure TABPubSockDemoForm.FormCreate(Sender: TObject);
begin
  ABSockUI1.Frame.ComboBox1.ItemIndex := 0; //Socket 类型(udp tcpServer tcpClient)
  ABSockUI1.Frame.ComboBox2.ItemIndex := 4; //Socket 模型(0~5)
  ABSockUI1.Frame.SpinEdit1.Value := 6000;
  ABSockUI1.Frame.Edit1.Text := ABPubUser.HostIP;
  ABSockUI1.Frame.SpinEdit2.Value := 6001;
  ABSockUI1.Frame.ComboBox1Change(ABSockUI1.Frame.ComboBox1);

  ABSockUI2.Frame.ComboBox1.ItemIndex := 0;
  ABSockUI2.Frame.ComboBox2.ItemIndex := ABSockUI1.Frame.ComboBox2.ItemIndex;
  ABSockUI2.Frame.SpinEdit1.Value := 6001;
  ABSockUI2.Frame.Edit1.Text := ABPubUser.HostIP;
  ABSockUI2.Frame.SpinEdit2.Value := 6000;
  ABSockUI2.Frame.ComboBox1Change(ABSockUI1.Frame.ComboBox1);

  ABSockUI3.Frame.ComboBox1.ItemIndex := 1;
  ABSockUI3.Frame.ComboBox2.ItemIndex := ABSockUI1.Frame.ComboBox2.ItemIndex;
  ABSockUI3.Frame.SpinEdit1.Value := 7000;
  ABSockUI3.Frame.Edit1.Text := ABPubUser.HostIP;
  ABSockUI3.Frame.SpinEdit2.Value := 7001;
  ABSockUI3.Frame.ComboBox1Change(ABSockUI1.Frame.ComboBox1);

  ABSockUI4.Frame.ComboBox1.ItemIndex := 2;
  ABSockUI4.Frame.ComboBox2.ItemIndex := ABSockUI1.Frame.ComboBox2.ItemIndex;
  ABSockUI4.Frame.SpinEdit1.Value := 7001;
  ABSockUI4.Frame.Edit1.Text := ABPubUser.HostIP;
  ABSockUI4.Frame.SpinEdit2.Value := 7000;
  ABSockUI4.Frame.ComboBox1Change(ABSockUI1.Frame.ComboBox1);
end;

end.

