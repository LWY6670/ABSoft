{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressBars components                                   }
{                                                                    }
{           Copyright (c) 1998-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSBARS AND ALL ACCOMPANYING VCL  }
{   CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY.                  }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRibbonCustomizationFormHelper;
 
{$I cxVer.inc}

interface

uses
{$IFDEF DELPHI16}
  System.UITypes,
{$ENDIF}
   Windows, SysUtils, Classes, Types, Controls, Graphics, Dialogs, Math, IniFiles, dxCore, cxClasses, cxGeometry, cxGraphics,
   cxTL, dxBar, dxRibbon, cxDropDownEdit, cxLookAndFeelPainters, dxBarStrs;

const
  dxrcfContextLevel = 0;
  dxrcfTabLevel = 1;
  dxrcfGroupLevel = 2;
  dxrcfItemLevel = 3;

  dxrcfCommandsCommandsNotInTheRibbon = 0;
  dxrcfCommandsAllCommands = 1;
  dxrcfCommandsAllTabs = 2;
  dxrcfCommandsMainTabs = 3;
  dxrcfCommandsToolTabs = 4;
  dxrcfCommandsCustomTabsAndGroups = 5;

  dxrcfRibbonAllTabs = 0;
  dxrcfRibbonMainTabs = 1;
  dxrcfRibbonToolTabs = 2;

type
  { TdxRibbonCustomizationFormItemNodeData }

  TdxRibbonCustomizationFormItemNodeData = class
  private
    FIsNewItemLinkNeeded: Boolean;
    FItem: TdxBarItem;
    FItemLink: TdxBarItemLink;
  public
    constructor Create(AItem: TdxBarItem; AItemLink: TdxBarItemLink); reintroduce;
    function Clone: TdxRibbonCustomizationFormItemNodeData;
    procedure DrawImage(ACanvas: TcxCanvas; var ARect: TRect);
    function IsBeginGroup: Boolean;
    function IsSubItem: Boolean;

    property IsNewItemLinkNeeded: Boolean read FIsNewItemLinkNeeded write FIsNewItemLinkNeeded;
    property Item: TdxBarItem read FItem;
    property ItemLink: TdxBarItemLink read FItemLink write FItemLink;
  end;

  { TdxRibbonCustomizationFormMergedTabInfo }

  TdxRibbonCustomizationFormMergedTabInfo = class
  private
    FMergedTabs: TcxObjectList;
    FOwnerTab: TdxRibbonTab;
  public
    constructor Create(AOwnerTab: TdxRibbonTab); virtual;
    destructor Destroy; override;

    procedure StoreMergedState;
    procedure RestoreMergedState;

    property MergedTabs: TcxObjectList read FMergedTabs;
    property OwnerTab: TdxRibbonTab read FOwnerTab;
  end;

  { TdxRibbonCustomizationFormMergedTabsList }

  TdxRibbonCustomizationFormMergedTabsList = class(TcxObjectList)
  private
    FTabs: TdxRibbonTabCollection;

    function GetItem(AIndex: Integer): TdxRibbonCustomizationFormMergedTabInfo;
  protected
    function AddTabInfo(ATab: TdxRibbonTab): TdxRibbonCustomizationFormMergedTabInfo;

    property Items[Index: Integer]: TdxRibbonCustomizationFormMergedTabInfo read GetItem; default;
    property Tabs: TdxRibbonTabCollection read FTabs;
  public
    constructor Create(ATabs: TdxRibbonTabCollection); virtual;

    procedure StoreMergedState;
    procedure RestoreMergedState;
  end;

  { TdxCustomRibbonCustomizationFormHelper }

  TdxCustomRibbonCustomizationFormHelper = class
  private
    FCommandsComboBox: TcxComboBox;
    FCommandsTreeList: TcxTreeList;
    FLockCount: Integer;
    FMergedTabsList: TdxRibbonCustomizationFormMergedTabsList;
    FMovedNode: TcxTreeListNode;
    FMovedNodeLevel: Integer;
    FRibbon: TdxCustomRibbon;
    FRibbonComboBox: TcxComboBox;
    FRibbonTreeList: TcxTreeList;

    function GetBarManagerBaseIniSection: string;
    function GetBarManagerMainIniSection: string;
    function GetFocusedNode: TcxTreeListNode;
    function GetItemLinkContent(AItem: TdxBarItem; ABar: TdxBar; ASource: TCustomIniFile; AItemLinkSection: string): TdxBarItemLink;
    function GetItemNodeCaption(AItem: TdxBarItem; AItemLink: TdxBarItemLink): string;
    function GetSavedBarIndex(ABar: TdxBar; ASource: TCustomIniFile): Integer;
  protected
    procedure AfterNodeMoved(const AConvertNodeData: Boolean); virtual; abstract;
    procedure ConvertItemNodeToNew(ANode: TcxTreeListNode);
    procedure CopyNode(ASourceNode, ATargetNode: TcxTreeListNode; ACopyData: Boolean = True);
    procedure DoMoveNode(ASourceNode, ATargetNode: TcxTreeListNode; const AInsertBeforeTargetNode: Boolean;
      const ATargetNodeIsParent: Boolean = False); virtual; abstract;
    function GetBarIniSection(ABar: TdxBar; const AIndex: Integer): string;
    function GetItemData(AItemNode: TcxTreeListNode): TdxRibbonCustomizationFormItemNodeData;
    function GetItemNodeEnabled(AParentNode: TcxTreeListNode): Boolean;
    function GetItemNodeVisible(AItem: TdxBarItem): Boolean; virtual;
    function GetNodeFor(AData: TPersistent; AParentNode: TcxTreeListNode): TcxTreeListNode;
    function GetTargetNodeForMovingFocusedNodeDown: TcxTreeListNode; virtual; abstract;
    function GetTargetNodeForMovingFocusedNodeUp: TcxTreeListNode; virtual; abstract;
    procedure LoadBarContentFormIni(ABar: TdxBar; ATargetNode: TcxTreeListNode; ATargetTreeList: TcxTreeList; ASource: TCustomIniFile);
    procedure PopulateAllItems(ASource: TdxBarManager; const AOnlyMissingItems: Boolean = False);
    procedure PopulateCommandsComboBoxContent; virtual; abstract;
    procedure PopulateItemNodeContent(ATargetNode: TcxTreeListNode; AItem: TdxBarItem; AItemLink: TdxBarItemLink;
      const ANeedBeginGroupNode: Boolean = False); virtual;
    procedure PopulateRibbonComboBoxContent; virtual; abstract;
    procedure SetFocusedNode(ANode: TcxTreeListNode; const AExpandNodeParents: Boolean = False); virtual;
    procedure LockContentUpdating;
    procedure UnlockContentUpdating;

    function IsAboveNodeCenter(ANode: TcxTreeListNode; const ACoordinate: Integer): Boolean;
    function IsRibbonContainsItem(AItem: TdxBarItem): Boolean;
    function IsSameElement(ASourceNode, ATargetNode: TcxTreeListNode): Boolean; overload;
    function IsSameElement(ANode: TcxTreeListNode; AData: TPersistent): Boolean; overload;
    function IsSourceNodeValid(ASourceNode: TcxTreeListNode): Boolean; virtual;
    function IsTargetNodeValid(ASourceNode, ATargetNode: TcxTreeListNode): Boolean; virtual;
                          
    property BarManagerBaseIniSection: string read GetBarManagerBaseIniSection;
    property BarManagerMainIniSection: string read GetBarManagerMainIniSection;
    property CommandsComboBox: TcxComboBox read FCommandsComboBox;
    property CommandsTreeList: TcxTreeList read FCommandsTreeList;
    property FocusedNode: TcxTreeListNode read GetFocusedNode;
    property MergedTabsList: TdxRibbonCustomizationFormMergedTabsList read FMergedTabsList;
    property Ribbon: TdxCustomRibbon read FRibbon;
    property RibbonComboBox: TcxComboBox read FRibbonComboBox;
    property RibbonTreeList: TcxTreeList read FRibbonTreeList;
  public
    constructor Create(ARibbon: TdxCustomRibbon; ACommandsTreeList, ARibbonTreeList: TcxTreeList;
      ACommandsComboBox, ARibbonComboBox: TcxComboBox); reintroduce; virtual;
    destructor Destroy; override;
    procedure Initialize;

    procedure BeginApplyChanges; virtual; abstract;
    procedure CreateAddedElements; virtual; abstract;
    procedure DeleteRemovedElements; virtual; abstract;
    procedure EndApplyChanges; virtual; abstract;
    procedure ReorderElements; virtual; abstract;
    procedure SynchronizeElementCaptions; virtual; abstract;

    procedure AddNode(ANode: TcxTreeListNode);
    procedure CheckFocusedNode;
    procedure DropMovedNodeTo(ATargetNode: TcxTreeListNode; const ADropPointY: Integer); virtual; abstract;
    procedure FreeNodeData(ANode: TcxTreeListNode);
    function GetFirstVisibleTab: TdxRibbonTab;
    function GetNodeLevel(ANode: TcxTreeListNode): Integer; virtual; abstract;
    procedure InitializeMovedNode(ANode: TcxTreeListNode);
    procedure MoveFocusedNodeDown; virtual; abstract;
    procedure MoveFocusedNodeUp; virtual; abstract;
    procedure PopulateCommandsTreeListContent; virtual; abstract;
    procedure PopulateRibbonTreeListContent(const AIsReseting: Boolean = False); virtual; abstract;
    procedure ReleaseMovedNode;
    procedure RemoveNode(ANode: TcxTreeListNode); virtual; abstract;
    procedure RenameNode(ANode: TcxTreeListNode; ANewCaption: string); virtual;

    function DrawItemNode(Sender: TcxCustomTreeList; ACanvas: TcxCanvas; AViewInfo: TcxTreeListEditCellViewInfo;
      ALookAndFeelPainter: TcxCustomLookAndFeelPainter): Boolean;

    function CanMoveFocusedNodeDown: Boolean; virtual;
    function CanMoveFocusedNodeUp: Boolean; virtual;
    function CanMoveNodeTo(ASourceNode, ATargetNode: TcxTreeListNode): Boolean; virtual; abstract;
    function CanRemoveFocusedNode: Boolean; virtual;
    function CanRenameFocusedNode: Boolean; virtual;
    function CanUpdateContent: Boolean;

    property MovedNode: TcxTreeListNode read FMovedNode;
  end;

  { TdxRibbonCustomizationFormHelper }

  TdxRibbonCustomizationFormHelper = class(TdxCustomRibbonCustomizationFormHelper)
  private
    function GetContextCaption(AContext: TdxRibbonContext): string;
  protected
    function AddNewNodeToFocusedNode(const ATargetLevel: Integer; ACaption: string): TcxTreeListNode;
    procedure AfterNodeMoved(const AConvertNodeData: Boolean); override;
    procedure ConvertGroupNodeToNew(ANode: TcxTreeListNode);
    procedure ConvertTabNodeToNew(ANode: TcxTreeListNode);
    function CreateContextNode(AContext: TdxRibbonContext; ATargetTreeList: TcxTreeList; const AIsReseting: Boolean = False): TcxTreeListNode;
    function CreateTabNode(ATab: TdxRibbonTab; AContextNode: TcxTreeListNode; ATargetTreeList: TcxTreeList; const AIsReseting: Boolean = False): TcxTreeListNode;
    procedure DoMoveNode(ASourceNode, ATargetNode: TcxTreeListNode; const AInsertBeforeTargetNode: Boolean;
      const ATargetNodeIsParent: Boolean = False); override;
    function GetContextNode(ASourceNode: TcxTreeListNode): TcxTreeListNode;
    function GetItemNodeVisible(AItem: TdxBarItem): Boolean; override;
    function GetTargetNodeForMovingFocusedNodeDown: TcxTreeListNode; override;
    function GetTargetNodeForMovingFocusedNodeUp: TcxTreeListNode; override;
    function LoadExistingGroupNodeContentFormIni(AToolBar: TdxBar; AParentTabNode: TcxTreeListNode;
      ATargetTreeList: TcxTreeList; ASource: TCustomIniFile): TcxTreeListNode;
    function LoadMissingGroupNodeContentFormIni(AToolBar: TdxBar; AParentTabNode: TcxTreeListNode;
      ATargetTreeList: TcxTreeList; ASource: TCustomIniFile): TcxTreeListNode;
    procedure LoadTabNodeContentFormIni(ANode: TcxTreeListNode; ATargetTreeList: TcxTreeList; ASource: TCustomIniFile);
    procedure PopulateCommandsComboBoxContent; override;
    procedure PopulateCustomTabsAndGroups;
    procedure PopulateGroupNodeContent(ANode: TcxTreeListNode; ATargetTreeList: TcxTreeList);
    procedure PopulateRibbonComboBoxContent; override;
    procedure PopulateTabNodeContent(ANode: TcxTreeListNode; ATargetTreeList: TcxTreeList);
    procedure RestoreTabNodeExpanding(ANode: TcxTreeListNode; AStorage: TList);
    procedure SetFocusedNode(ANode: TcxTreeListNode; const AExpandNodeParents: Boolean = False); override;
    procedure StoreTabNodeExpanding(ANode: TcxTreeListNode; AStorage: TList);
    procedure SynchronizeMatchingGroupNodesWith(AGroupNode: TcxTreeListNode);

    procedure CheckAndAddContextNodeToRibbon(ANode: TcxTreeListNode);
    procedure CheckAndAddGroupNodeToRibbon(ANode: TcxTreeListNode);
    procedure CheckAndAddItemNodeToRibbon(ANode: TcxTreeListNode);
    procedure CheckAndAddTabNodeToRibbon(ANode: TcxTreeListNode);
    procedure CheckAndDeleteContextsFromRibbon;
    procedure CheckAndDeleteGroupsFromRibbon(AGroups: TdxRibbonTabGroups; ATabNode: TcxTreeListNode);
    procedure CheckAndDeleteItemLinksFromRibbon(AItemLinks: TdxBarItemLinks; AGroupNode: TcxTreeListNode);
    procedure CheckAndDeleteTabsFromRibbon(AContext: TdxRibbonContext);

    function IsGroupCustomizingAllowed(ANode: TcxTreeListNode): Boolean;
    function IsGroupHiddenInTab(AGroupNode, ATabNode: TcxTreeListNode): Boolean;
    function IsParentTabTheSame(ASourceGroupNode, ATargetNode: TcxTreeListNode): Boolean;
    function IsRibbonGroupNodeWithSharedToolBar(ANode: TcxTreeListNode): Boolean;
    function IsSourceNodeValid(ASourceNode: TcxTreeListNode): Boolean; override;
    function IsTargetNodeValid(ASourceNode, ATargetNode: TcxTreeListNode): Boolean; override;
  public
    procedure BeginApplyChanges; override;
    procedure CreateAddedElements; override;
    procedure DeleteRemovedElements; override;
    procedure EndApplyChanges; override;
    procedure ReorderElements; override;
    procedure SynchronizeElementCaptions; override;

    procedure AddNewContext;
    procedure AddNewGroup;
    procedure AddNewTab;
    procedure DropMovedNodeTo(ATargetNode: TcxTreeListNode; const ADropPointY: Integer); override;
    procedure ExpandAllContexts;
    function GetNodeLevel(ANode: TcxTreeListNode): Integer; override;
    function IsTabsInCommands: Boolean;
    procedure MoveFocusedNodeDown; override;
    procedure MoveFocusedNodeUp; override;
    procedure PopulateCommandsTreeListContent; override;
    procedure PopulateRibbonTreeListContent(const AIsReseting: Boolean = False); override;
    procedure RemoveNode(ANode: TcxTreeListNode); override;
    procedure RenameNode(ANode: TcxTreeListNode; ANewCaption: string); override;
    procedure ResetTabNode(ANode: TcxTreeListNode);
    procedure UpdateContextsVisibility;

    function CanMoveFocusedNodeDown: Boolean; override;
    function CanMoveFocusedNodeUp: Boolean; override;
    function CanMoveNodeTo(ASourceNode, ATargetNode: TcxTreeListNode): Boolean; override;
    function CanRemoveFocusedNode: Boolean; override;
    function CanResetFocusedNode: Boolean;

    function DrawContextNode(Sender: TcxCustomTreeList; ACanvas: TcxCanvas; AViewInfo: TcxTreeListEditCellViewInfo;
      ALookAndFeelPainter: TcxCustomLookAndFeelPainter): Boolean;
  end;

  { TdxRibbonQATCustomizationFormHelper }

  TdxRibbonQATCustomizationFormHelper = class(TdxCustomRibbonCustomizationFormHelper)
  private
    function GetContextPrefix(AContext: TdxRibbonContext): string;
  protected
    procedure AfterNodeMoved(const AConvertNodeData: Boolean); override;
    procedure DoMoveNode(ASourceNode, ATargetNode: TcxTreeListNode; const AInsertBeforeTargetNode: Boolean;
      const ATargetNodeIsParent: Boolean = False); override;
    function GetTargetNodeForMovingFocusedNodeDown: TcxTreeListNode; override;
    function GetTargetNodeForMovingFocusedNodeUp: TcxTreeListNode; override;
    procedure PopulateBeginGroupNodeContent(ATargetNode: TcxTreeListNode);
    procedure PopulateContextTabs(AContext: TdxRibbonContext; AItems: TStrings);
    procedure PopulateItemNodeContent(ATargetNode: TcxTreeListNode; AItem: TdxBarItem; AItemLink: TdxBarItemLink;
      const ANeedBeginGroupNode: Boolean = False); override;
    procedure PopulateTabItems(ATab: TdxRibbonTab);

    function IsSourceNodeValid(ASourceNode: TcxTreeListNode): Boolean; override;
    function IsTargetNodeValid(ASourceNode, ATargetNode: TcxTreeListNode): Boolean; override;
  public
    procedure BeginApplyChanges; override;
    procedure CreateAddedElements; override;
    procedure DeleteRemovedElements; override;
    procedure EndApplyChanges; override;
    procedure ReorderElements; override;
    procedure SynchronizeElementCaptions; override;

    procedure ChangeQATPosition(const AShowBelowRibbon: Boolean);
    procedure DropMovedNodeTo(ATargetNode: TcxTreeListNode; const ADropPointY: Integer); override;
    function GetNodeLevel(ANode: TcxTreeListNode): Integer; override;
    function IsQATBelowRibbon: Boolean;
    procedure MoveFocusedNodeDown; override;
    procedure MoveFocusedNodeUp; override;
    procedure PopulateCommandsComboBoxContent; override;
    procedure PopulateCommandsTreeListContent; override;
    procedure PopulateRibbonComboBoxContent; override;
    procedure PopulateRibbonTreeListContent(const AIsReseting: Boolean = False); override;
    procedure RemoveNode(ANode: TcxTreeListNode); override;

    function CanMoveNodeTo(ASourceNode, ATargetNode: TcxTreeListNode): Boolean; override;
    function CanRemoveFocusedNode: Boolean; override;
    function CanRenameFocusedNode: Boolean; override;
  end;

implementation

type
  TdxBarAccess = class(TdxBar);
  TdxBarItemLinkAccess = class(TdxBarItemLink);
  TdxBarManagerAccess = class(TdxBarManager);
  TdxCustomRibbonAccess = class(TdxCustomRibbon);
  TdxRibbonTabAccess = class(TdxRibbonTab);
  TdxRibbonTabCollectionAccess = class(TdxRibbonTabCollection);
  TdxRibbonTabGroupAccess = class(TdxRibbonTabGroup);
  TdxRibbonTabGroupsAccess = class(TdxRibbonTabGroups);
  TcxTreeListNodeAccess = class(TcxTreeListNode);

{ TdxRibbonCustomizationFormItemNodeData }

constructor TdxRibbonCustomizationFormItemNodeData.Create(AItem: TdxBarItem; AItemLink: TdxBarItemLink);
begin
  inherited Create;
  FIsNewItemLinkNeeded := False;
  FItem := AItem;
  FItemLink := AItemLink;
end;

function TdxRibbonCustomizationFormItemNodeData.Clone: TdxRibbonCustomizationFormItemNodeData;
begin
  Result := TdxRibbonCustomizationFormItemNodeData.Create(Item, ItemLink);
end;

procedure TdxRibbonCustomizationFormItemNodeData.DrawImage(ACanvas: TcxCanvas; var ARect: TRect);
var
  AImageWidth: Integer;
  AGlyph: TBitmap;
begin
  if Item <> nil then
  begin
    AImageWidth := cxRectHeight(ARect);
    if Item is TdxRibbonQuickAccessGroupButton then
      AGlyph := TdxRibbonQuickAccessGroupButton(Item).Toolbar.Glyph
    else
      AGlyph := Item.Glyph;
    cxDrawImage(ACanvas, cxRectSetWidth(ARect, AImageWidth), AGlyph, Item.BarManager.Images, Item.ImageIndex, ifmFit);
    Inc(ARect.Left, AImageWidth);
  end;
end;

function TdxRibbonCustomizationFormItemNodeData.IsBeginGroup: Boolean;
begin
  Result := Item = nil;
end;

function TdxRibbonCustomizationFormItemNodeData.IsSubItem: Boolean;
begin
  Result := Item is TCustomdxBarSubItem;
end;

{ TdxRibbonCustomizationFormMergedTabInfo }

constructor TdxRibbonCustomizationFormMergedTabInfo.Create(AOwnerTab: TdxRibbonTab);
begin
  inherited Create;
  FOwnerTab := TdxRibbonTabAccess(AOwnerTab);
  FMergedTabs := TcxObjectList.Create(False);
end;

destructor TdxRibbonCustomizationFormMergedTabInfo.Destroy;
begin
  FreeAndNil(FMergedTabs);
  inherited Destroy;
end;

procedure TdxRibbonCustomizationFormMergedTabInfo.StoreMergedState;
begin
  MergedTabs.Assign(TdxRibbonTabAccess(OwnerTab).MergeData.Children);
  OwnerTab.Unmerge;
end;

procedure TdxRibbonCustomizationFormMergedTabInfo.RestoreMergedState;
var
  I: Integer;
begin
  for I := 0 to MergedTabs.Count - 1 do
    OwnerTab.Merge(TdxRibbonTab(MergedTabs[I]));
  MergedTabs.Clear;
end;

{ TdxRibbonCustomizationFormMergedTabsList }

constructor TdxRibbonCustomizationFormMergedTabsList.Create(ATabs: TdxRibbonTabCollection);
begin
  inherited Create;
  FTabs := ATabs;
end;

procedure TdxRibbonCustomizationFormMergedTabsList.StoreMergedState;
var
  ATab: TdxRibbonTabAccess;
  I: Integer;
begin
  for I := 0 to Tabs.Count - 1 do
  begin
    ATab := TdxRibbonTabAccess(Tabs[I]);
    if ATab.IsMerged and not ATab.MergeData.CreatedByMerging then
      AddTabInfo(ATab).StoreMergedState;
  end;
end;

procedure TdxRibbonCustomizationFormMergedTabsList.RestoreMergedState;
var
  ATabInfo: TdxRibbonCustomizationFormMergedTabInfo;
  I: Integer;
begin
  for I := 0 to Count - 1 do
  begin
    ATabInfo := Items[I];
    if Tabs.IndexOf(ATabInfo.OwnerTab) >= 0 then
      ATabInfo.RestoreMergedState;
  end;
  Clear;
end;

function TdxRibbonCustomizationFormMergedTabsList.AddTabInfo(ATab: TdxRibbonTab): TdxRibbonCustomizationFormMergedTabInfo;
var
  AIndex: Integer;
begin
  AIndex := Add(TdxRibbonCustomizationFormMergedTabInfo.Create(ATab));
  Result := Items[AIndex];
end;

function TdxRibbonCustomizationFormMergedTabsList.GetItem(AIndex: Integer): TdxRibbonCustomizationFormMergedTabInfo;
begin
  Result := TdxRibbonCustomizationFormMergedTabInfo(inherited Items[AIndex]);
end;

{ TdxCustomRibbonCustomizationFormHelper }

constructor TdxCustomRibbonCustomizationFormHelper.Create(ARibbon: TdxCustomRibbon;
  ACommandsTreeList, ARibbonTreeList: TcxTreeList; ACommandsComboBox, ARibbonComboBox: TcxComboBox);
begin
  inherited Create;
  FRibbon := ARibbon;
  FCommandsComboBox := ACommandsComboBox;
  FCommandsTreeList := ACommandsTreeList;
  FRibbonComboBox := ARibbonComboBox;
  FRibbonTreeList := ARibbonTreeList;
  FMergedTabsList := TdxRibbonCustomizationFormMergedTabsList.Create(Ribbon.Tabs);
  MergedTabsList.StoreMergedState;
end;

destructor TdxCustomRibbonCustomizationFormHelper.Destroy;
begin
  CommandsTreeList.Root.DeleteChildren;
  RibbonTreeList.Root.DeleteChildren;
  MergedTabsList.RestoreMergedState;
  FreeAndNil(FMergedTabsList);
  inherited Destroy;
end;

procedure TdxCustomRibbonCustomizationFormHelper.Initialize;
begin
  PopulateCommandsComboBoxContent;
  PopulateRibbonComboBoxContent;   
  PopulateCommandsTreeListContent;
  PopulateRibbonTreeListContent;
end;

procedure TdxCustomRibbonCustomizationFormHelper.AddNode(ANode: TcxTreeListNode);
var
  ANextFocusedNode: TcxTreeListNode;
begin
  InitializeMovedNode(ANode);
  DropMovedNodeTo(FocusedNode, RibbonTreeList.Height);
  ReleaseMovedNode;
  ANextFocusedNode := ANode;
  repeat
    ANextFocusedNode := ANextFocusedNode.getNextSibling;
  until (ANextFocusedNode = nil) or ANextFocusedNode.Visible;
  if ANextFocusedNode <> nil then
    ANextFocusedNode.Focused := True;
end;

procedure TdxCustomRibbonCustomizationFormHelper.CheckFocusedNode;
begin
  if (RibbonTreeList.FocusedNode = nil) and (RibbonTreeList.Count > 0) then
    RibbonTreeList.FocusedNode := FocusedNode;
end;

procedure TdxCustomRibbonCustomizationFormHelper.FreeNodeData(ANode: TcxTreeListNode);
begin
  if TObject(ANode.Data) is TdxRibbonCustomizationFormItemNodeData then
  begin
    TObject(ANode.Data).Free;
    ANode.Data := nil;
  end;
end;

function TdxCustomRibbonCustomizationFormHelper.GetFirstVisibleTab: TdxRibbonTab;
var
  I: Integer;
begin
  Result := nil;
  for I := 0 to Ribbon.TabCount - 1 do
    if TdxRibbonTabAccess(Ribbon.Tabs[I]).IsVisible then
    begin
      Result := Ribbon.Tabs[I];
      Break;
    end;
end;

procedure TdxCustomRibbonCustomizationFormHelper.InitializeMovedNode(ANode: TcxTreeListNode);
begin
  FMovedNode := ANode;
  FMovedNodeLevel := Min(GetNodeLevel(ANode), dxrcfItemLevel);
  LockContentUpdating;
end;

procedure TdxCustomRibbonCustomizationFormHelper.ReleaseMovedNode;
begin
  FMovedNode := nil;
  FMovedNodeLevel := -1;
  UnlockContentUpdating;
end;

procedure TdxCustomRibbonCustomizationFormHelper.RenameNode(ANode: TcxTreeListNode; ANewCaption: string);
begin
  ANode.Texts[0] := ANewCaption;
end;

function TdxCustomRibbonCustomizationFormHelper.DrawItemNode(Sender: TcxCustomTreeList; ACanvas: TcxCanvas;
  AViewInfo: TcxTreeListEditCellViewInfo; ALookAndFeelPainter: TcxCustomLookAndFeelPainter): Boolean;
var
  ARect: TRect;
begin
  ARect := AViewInfo.BoundsRect;
  GetItemData(AViewInfo.Node).DrawImage(ACanvas, ARect);
  ARect := cxRectInflate(ARect, -cxHeaderTextOffset);
  ARect := cxRectCenterVertically(ARect, cxTextHeight(ACanvas.Handle));
  cxDrawText(ACanvas, AViewInfo.Node.Texts[0], ARect, DT_LEFT);
  Result := True;
end;

function TdxCustomRibbonCustomizationFormHelper.CanMoveFocusedNodeDown: Boolean;
begin
  Result := IsSourceNodeValid(FocusedNode) and IsTargetNodeValid(FocusedNode, GetTargetNodeForMovingFocusedNodeDown);
end;

function TdxCustomRibbonCustomizationFormHelper.CanMoveFocusedNodeUp: Boolean;
begin
  Result := IsSourceNodeValid(FocusedNode) and IsTargetNodeValid(FocusedNode, GetTargetNodeForMovingFocusedNodeUp);
end;

function TdxCustomRibbonCustomizationFormHelper.CanRemoveFocusedNode: Boolean;
begin
  Result := FocusedNode <> nil;
end;

function TdxCustomRibbonCustomizationFormHelper.CanRenameFocusedNode: Boolean;
begin
  Result := (FocusedNode <> nil) and FocusedNode.Enabled and ((GetNodeLevel(FocusedNode) <> dxrcfContextLevel) or
    not SameText(FocusedNode.Texts[0], cxGetResourceString(@sdxRibbonCustomizationFormMainTabs)));
end;

function TdxCustomRibbonCustomizationFormHelper.CanUpdateContent: Boolean;
begin
  Result := FLockCount = 0;
end;

procedure TdxCustomRibbonCustomizationFormHelper.ConvertItemNodeToNew(ANode: TcxTreeListNode);
begin
  GetItemData(ANode).IsNewItemLinkNeeded := True;
  ANode.Enabled := True;
end;

procedure TdxCustomRibbonCustomizationFormHelper.CopyNode(ASourceNode, ATargetNode: TcxTreeListNode; ACopyData: Boolean = True);
var
  I: Integer;
  ASourceNodeLevel: Integer;
  ASourceNodeData: TdxRibbonCustomizationFormItemNodeData;
begin
  LockContentUpdating;
  try
    ATargetNode.Assign(ASourceNode);
    ATargetNode.Enabled := ASourceNode.Enabled and ((GetNodeLevel(ASourceNode.Parent) < dxrcfItemLevel) or
      not ((GetItemData(ASourceNode.Parent) <> nil) and GetItemData(ASourceNode.Parent).IsSubItem));
    ASourceNodeLevel := GetNodeLevel(ASourceNode);
    ASourceNodeData := GetItemData(ASourceNode);
    if ACopyData then
      if ASourceNodeLevel >= dxrcfItemLevel then
        ATargetNode.Data := ASourceNodeData.Clone
      else
        ATargetNode.Data := ASourceNode.Data;
    if (ASourceNodeLevel < dxrcfItemLevel) or ASourceNodeData.IsSubItem then
      for I := 0 to ASourceNode.Count - 1 do
        CopyNode(ASourceNode.Items[I], ATargetNode.AddChild);
  finally
    UnlockContentUpdating;
  end;
end;

function TdxCustomRibbonCustomizationFormHelper.GetBarIniSection(ABar: TdxBar; const AIndex: Integer): string;
begin
  Result := TdxBarAccess(ABar).GetIniSection(BarManagerBaseIniSection, AIndex);
end;

function TdxCustomRibbonCustomizationFormHelper.GetItemData(AItemNode: TcxTreeListNode): TdxRibbonCustomizationFormItemNodeData;
begin
  if GetNodeLevel(AItemNode) >= dxrcfItemLevel then
    Result := TdxRibbonCustomizationFormItemNodeData(AItemNode.Data)
  else
    Result := nil;
end;

function TdxCustomRibbonCustomizationFormHelper.GetItemNodeEnabled(AParentNode: TcxTreeListNode): Boolean;
begin
  Result := AParentNode.Enabled and ((AParentNode.TreeList = CommandsTreeList) or (GetItemData(AParentNode) = nil) or
    not GetItemData(AParentNode).IsSubItem);
end;

function TdxCustomRibbonCustomizationFormHelper.GetItemNodeVisible(AItem: TdxBarItem): Boolean;
begin
  Result := AItem.VisibleForCustomization and not (AItem is TdxBarSeparator);
end;

function TdxCustomRibbonCustomizationFormHelper.GetNodeFor(AData: TPersistent; AParentNode: TcxTreeListNode): TcxTreeListNode;
begin
  Result := AParentNode;
  if Result <> nil then
  begin
    Result := AParentNode.getFirstChild;
    while (Result <> nil) and not IsSameElement(Result, AData) do
      Result := Result.getNextSibling;
  end;
end;

procedure TdxCustomRibbonCustomizationFormHelper.LoadBarContentFormIni(ABar: TdxBar; ATargetNode: TcxTreeListNode;
  ATargetTreeList: TcxTreeList; ASource: TCustomIniFile);
var
  ABarSection, AItemLinkSection: string;
  AItem: TdxBarItem;
  AItemLink: TdxBarItemLink;
  AItemNode: TcxTreeListNode;
  I: Integer;
begin
  ABarSection := GetBarIniSection(ABar, GetSavedBarIndex(ABar, ASource));
  for I := 0 to ASource.ReadInteger(ABarSection, 'ItemLinkCount', 0) - 1 do
  begin
    AItemLinkSection := TdxBarItemLinkAccess.GetIniSection(ABarSection, I, skIni);
    AItem := Ribbon.BarManager.GetItemByName(ASource.ReadString(AItemLinkSection, 'ItemName', ''));
    if AItem <> nil then
    begin
      AItemNode := ATargetTreeList.AddChild(ATargetNode);
      AItemLink := GetItemLinkContent(AItem, ABar, ASource, AItemLinkSection);
      PopulateItemNodeContent(AItemNode, AItem, AItemLink, ASource.ReadBool(AItemLinkSection, 'BeginGroup', False));
    end;
  end;
end;

procedure TdxCustomRibbonCustomizationFormHelper.PopulateAllItems(ASource: TdxBarManager;
  const AOnlyMissingItems: Boolean = False);
var
  I: Integer;
begin
  CommandsTreeList.OptionsView.ShowRoot := True;
  for I := 0 to ASource.ItemCount - 1 do
    if not AOnlyMissingItems or not IsRibbonContainsItem(ASource.Items[I]) then
      PopulateItemNodeContent(CommandsTreeList.Add, ASource.Items[I], nil);
end;

procedure TdxCustomRibbonCustomizationFormHelper.PopulateItemNodeContent(ATargetNode: TcxTreeListNode;
  AItem: TdxBarItem; AItemLink: TdxBarItemLink; const ANeedBeginGroupNode: Boolean = False);
var
  ASubItem: TCustomdxBarSubItem;
  ASubItemLink: TdxBarItemLink;
  I: Integer;
begin
  ATargetNode.Data := TdxRibbonCustomizationFormItemNodeData.Create(AItem, AItemLink);
  ATargetNode.Texts[0] := GetItemNodeCaption(AItem, AItemLink);
  ATargetNode.Enabled := GetItemNodeEnabled(ATargetNode.Parent);
  ATargetNode.Visible := GetItemNodeVisible(AItem);
  if GetItemData(ATargetNode).IsSubItem then
  begin
    ASubItem := TCustomdxBarSubItem(AItem);
    for I := 0 to ASubItem.ItemLinks.Count - 1 do
    begin
      ASubItemLink := ASubItem.ItemLinks[I];
      PopulateItemNodeContent(ATargetNode.AddChild, ASubItemLink.Item, ASubItemLink, ASubItemLink.BeginGroup);
    end;
  end;
end;

procedure TdxCustomRibbonCustomizationFormHelper.SetFocusedNode(ANode: TcxTreeListNode; const AExpandNodeParents: Boolean = False);
begin
  ANode.Focused := True;
end;

procedure TdxCustomRibbonCustomizationFormHelper.LockContentUpdating;
begin
  Inc(FLockCount);
end;

procedure TdxCustomRibbonCustomizationFormHelper.UnlockContentUpdating;
begin
  Dec(FLockCount);
end;

function TdxCustomRibbonCustomizationFormHelper.IsAboveNodeCenter(ANode: TcxTreeListNode; const ACoordinate: Integer): Boolean;
var
  AViewData: TcxTreeListNodeViewData;
begin
  AViewData := TcxTreeListNodeAccess(ANode).ViewData;
  Result := (AViewData <> nil) and (ACoordinate < cxRectCenter(AViewData.GetRealBounds).Y);
end;

function TdxCustomRibbonCustomizationFormHelper.IsRibbonContainsItem(AItem: TdxBarItem): Boolean;
var
  ATabIndex, AGroupIndex: Integer;
  ATab: TdxRibbonTab;
  AToolBar: TdxBar;
begin
  Result := False;
  ATabIndex := 0;
  while not Result and (ATabIndex < Ribbon.TabCount) do
  begin
    ATab := Ribbon.Tabs[ATabIndex];
    AGroupIndex := 0;
    while not Result and (AGroupIndex < ATab.Groups.Count) do
    begin
      AToolBar := ATab.Groups[AGroupIndex].ToolBar;
      Result := (AToolBar <> nil) and AToolBar.ItemLinks.HasItem(AItem);
      Inc(AGroupIndex);
    end;
    Inc(ATabIndex);
  end;
end;

function TdxCustomRibbonCustomizationFormHelper.IsSameElement(ASourceNode, ATargetNode: TcxTreeListNode): Boolean;
begin
  Result := (ASourceNode.Data <> nil) and (ATargetNode.Data <> nil);
  case Min(GetNodeLevel(ATargetNode), dxrcfItemLevel) of
    dxrcfContextLevel, dxrcfTabLevel:
      Result := ASourceNode.Data = ATargetNode.Data;
    dxrcfGroupLevel:
      Result := Result and (TdxRibbonTabGroup(ASourceNode.Data).ToolBar = TdxRibbonTabGroup(ATargetNode.Data).ToolBar);
    dxrcfItemLevel:
      Result := Result and (GetItemData(ASourceNode).Item = GetItemData(ATargetNode).Item);
  end;
end;

function TdxCustomRibbonCustomizationFormHelper.IsSameElement(ANode: TcxTreeListNode; AData: TPersistent): Boolean;
begin
  Result := (ANode.Data <> nil) and (AData <> nil);
  case Min(GetNodeLevel(ANode), dxrcfItemLevel) of
    dxrcfContextLevel, dxrcfTabLevel:
      Result := TPersistent(ANode.Data) = AData;
    dxrcfGroupLevel:
      Result := Result and (TdxRibbonTabGroup(ANode.Data).ToolBar = TdxRibbonTabGroup(AData).ToolBar);
    dxrcfItemLevel:
      Result := Result and (GetItemData(ANode).Item = TdxBarItem(AData));
  end;
end;

function TdxCustomRibbonCustomizationFormHelper.IsSourceNodeValid(ASourceNode: TcxTreeListNode): Boolean;
begin
  Result := ASourceNode <> nil;
end;

function TdxCustomRibbonCustomizationFormHelper.IsTargetNodeValid(ASourceNode, ATargetNode: TcxTreeListNode): Boolean;
begin
  Result := (ATargetNode <> nil) and ATargetNode.Visible;
end;

function TdxCustomRibbonCustomizationFormHelper.GetBarManagerBaseIniSection: string;
begin
  Result := TdxBarManagerAccess(Ribbon.BarManager).GetBaseIniSection;
end;

function TdxCustomRibbonCustomizationFormHelper.GetBarManagerMainIniSection: string;
begin
  Result := TdxBarManagerAccess(Ribbon.BarManager).GetBarManagerSection(BarManagerBaseIniSection, skIni);
end;

function TdxCustomRibbonCustomizationFormHelper.GetFocusedNode: TcxTreeListNode;
begin
  Result := RibbonTreeList.FocusedNode;
  if (Result = nil) and (RibbonTreeList.SelectionCount > 0) then
    Result := RibbonTreeList.Selections[0];
end;

function TdxCustomRibbonCustomizationFormHelper.GetItemLinkContent(AItem: TdxBarItem; ABar: TdxBar;
  ASource: TCustomIniFile; AItemLinkSection: string): TdxBarItemLink;
var
  AStream: TMemoryStream;
  AViewLevels: TdxBarItemViewLevels;
  AUserCaption: string;
  AUserPaintStyle, AUserWidth: Integer;
begin
  Result := ABar.ItemLinks.FindByItem(AItem);
  if (Result = nil) and (AItem.LinkCount > 0) then
  begin
    Result := AItem.Links[0];
    AUserCaption := ASource.ReadString(AItemLinkSection, 'UserCaption', '');
    if AUserCaption <> '' then
      Result.UserCaption := AUserCaption;
    AUserPaintStyle := ASource.ReadInteger(AItemLinkSection, 'UserPaintStyle', 0);
    if AUserPaintStyle <> 0 then
      Result.UserPaintStyle := TdxBarPaintStyle(AUserPaintStyle);
    AUserWidth := ASource.ReadInteger(AItemLinkSection, 'UserWidth', 100);
    if AUserWidth <> 100 then
      Result.UserWidth := AUserWidth;
    Result.ButtonGroup := TdxBarButtonGroupPosition(ASource.ReadInteger(AItemLinkSection, 'ButtonGroup', Integer(bgpNone)));
    Result.Position := TdxBarItemPosition(ASource.ReadInteger(AItemLinkSection, 'Position', Integer(dxBarItemDefaultPosition)));
    AStream := TMemoryStream.Create;
    try
      if ASource.ReadBinaryStream(AItemLinkSection, 'ViewLevels', AStream) <> 0 then
      begin
        AStream.Read(AViewLevels, SizeOf(TdxBarItemViewLevels));
        Result.ViewLevels := AViewLevels;
      end;
    finally
      AStream.Free;
    end;
  end;
end;

function TdxCustomRibbonCustomizationFormHelper.GetItemNodeCaption(AItem: TdxBarItem; AItemLink: TdxBarItemLink): string;
begin
  if AItemLink <> nil then
    Result := AItemLink.Caption
  else
    Result := AItem.Caption;
end;

function TdxCustomRibbonCustomizationFormHelper.GetSavedBarIndex(ABar: TdxBar; ASource: TCustomIniFile): Integer;
var
  I: Integer;
begin
  Result := -1;
  for I := 0 to ASource.ReadInteger(BarManagerMainIniSection, 'BarCount', 0) - 1 do
    if ASource.ReadString(GetBarIniSection(ABar, I), 'Caption', '') = ABar.Caption then
    begin
      Result := I;
      Break;
    end;
end;

{ TdxRibbonCustomizationFormHelper }

procedure TdxRibbonCustomizationFormHelper.BeginApplyChanges;
begin
  CommandsComboBox.ItemIndex := dxrcfCommandsAllTabs;
  Ribbon.BeginUpdate;
end;

procedure TdxRibbonCustomizationFormHelper.CreateAddedElements;
var
  ANode: TcxTreeListNode;
  I: Integer;
begin
  for I := 1 to RibbonTreeList.AbsoluteCount - 1 do
  begin
    ANode := RibbonTreeList.AbsoluteItems[I];
    case GetNodeLevel(ANode) of
      dxrcfContextLevel:
        CheckAndAddContextNodeToRibbon(ANode);
      dxrcfTabLevel:
        CheckAndAddTabNodeToRibbon(ANode);
      dxrcfGroupLevel:
        CheckAndAddGroupNodeToRibbon(ANode);
      dxrcfItemLevel:
        CheckAndAddItemNodeToRibbon(ANode);
    end;
  end;
end;

procedure TdxRibbonCustomizationFormHelper.DeleteRemovedElements;
begin
  CheckAndDeleteContextsFromRibbon;
end;

procedure TdxRibbonCustomizationFormHelper.EndApplyChanges;
begin
  TdxCustomRibbonAccess(Ribbon).RecalculateBars;
  if Ribbon.ActiveTab <> nil then
    TdxRibbonTabAccess(Ribbon.ActiveTab).Activate;
  Ribbon.EndUpdate;
end;

procedure TdxRibbonCustomizationFormHelper.ReorderElements;
var
  ANode: TcxTreeListNode;
  I: Integer;
begin
  for I := 1 to RibbonTreeList.AbsoluteCount - 1 do
  begin
    ANode := RibbonTreeList.AbsoluteItems[I];
    case GetNodeLevel(ANode) of
      dxrcfContextLevel:
        TdxRibbonContext(ANode.Data).Index := ANode.Index - 1;
      dxrcfTabLevel:
        begin
          TdxRibbonTab(ANode.Data).Visible := ANode.Checked;
          TdxRibbonTab(ANode.Data).Index := ANode.Index;
        end;
      dxrcfGroupLevel:
        TdxRibbonTabGroup(ANode.Data).Index := ANode.Index;
      dxrcfItemLevel:
        GetItemData(ANode).ItemLink.Index := ANode.Index;
    end;
  end;
end;

procedure TdxRibbonCustomizationFormHelper.SynchronizeElementCaptions;
var
  ANode: TcxTreeListNode;
  I: Integer;
begin
  for I := 1 to RibbonTreeList.AbsoluteCount - 1 do
  begin
    ANode := RibbonTreeList.AbsoluteItems[I];
    case GetNodeLevel(ANode) of
      dxrcfContextLevel:
        TdxRibbonContext(ANode.Data).Caption := ANode.Texts[0];
      dxrcfTabLevel:
        TdxRibbonTab(ANode.Data).Caption := ANode.Texts[0];
      dxrcfGroupLevel:
        TdxRibbonTabGroup(ANode.Data).Caption := ANode.Texts[0];
      dxrcfItemLevel:
        GetItemData(ANode).ItemLink.UserCaption := ANode.Texts[0];
    end;
  end;
end;

procedure TdxRibbonCustomizationFormHelper.AddNewContext;   
var
  ANewContextNode: TcxTreeListNode;
begin
  LockContentUpdating;
  try
    ANewContextNode := RibbonTreeList.Add;
    ANewContextNode.Texts[0] := cxGetResourceString(@sdxRibbonCustomizationFormNewContext);
    ANewContextNode.Focused := True;
    ANewContextNode.CheckGroupType := ncgCheckGroup;
    AddNewTab;
    ANewContextNode.Expand(True);
  finally
    UnlockContentUpdating;
  end;
end;

procedure TdxRibbonCustomizationFormHelper.AddNewGroup;
begin
  LockContentUpdating;
  try
    AddNewNodeToFocusedNode(dxrcfGroupLevel, cxGetResourceString(@sdxRibbonCustomizationFormNewGroup));
  finally
    UnlockContentUpdating;
  end;
end;

procedure TdxRibbonCustomizationFormHelper.AddNewTab;
var
  ANewTabNode: TcxTreeListNode;
begin
  LockContentUpdating;
  try
    ANewTabNode := AddNewNodeToFocusedNode(dxrcfTabLevel, cxGetResourceString(@sdxRibbonCustomizationFormNewTab));
    AddNewGroup;
    ANewTabNode.Expand(True);
  finally
    UnlockContentUpdating;
  end;
end;

procedure TdxRibbonCustomizationFormHelper.DropMovedNodeTo(ATargetNode: TcxTreeListNode; const ADropPointY: Integer);
begin
  if CanMoveNodeTo(MovedNode, ATargetNode) then
  begin
    while GetNodeLevel(ATargetNode) > FMovedNodeLevel do
      ATargetNode := ATargetNode.Parent;
    if GetNodeLevel(ATargetNode) = FMovedNodeLevel then
      DoMoveNode(MovedNode, ATargetNode, IsAboveNodeCenter(ATargetNode, ADropPointY))
    else
      DoMoveNode(MovedNode, ATargetNode, False, True);
  end;
end;

procedure TdxRibbonCustomizationFormHelper.ExpandAllContexts;
var
  I: Integer;
begin
  for I := 0 to RibbonTreeList.Root.Count - 1 do
    RibbonTreeList.Root.Items[I].Expand(False);
  if IsTabsInCommands then
    for I := 0 to CommandsTreeList.Root.Count - 1 do
      CommandsTreeList.Root.Items[I].Expand(False);
end;

function TdxRibbonCustomizationFormHelper.GetNodeLevel(ANode: TcxTreeListNode): Integer;
begin
  if ANode = nil then
    Result := -1
  else
    if ANode.TreeList = RibbonTreeList then
      Result := ANode.Level
    else
      if CommandsComboBox.ItemIndex in [dxrcfCommandsCommandsNotInTheRibbon, dxrcfCommandsAllCommands] then
        Result := dxrcfItemLevel
      else
        if CommandsComboBox.ItemIndex <> dxrcfCommandsCustomTabsAndGroups then
          Result := ANode.Level
        else
          Result := ANode.Level + 1;
end;

function TdxRibbonCustomizationFormHelper.IsTabsInCommands: Boolean;
begin
  Result := CommandsComboBox.ItemIndex in [dxrcfCommandsAllTabs, dxrcfCommandsMainTabs, dxrcfCommandsToolTabs];
end;

procedure TdxRibbonCustomizationFormHelper.MoveFocusedNodeDown;
var
  ASourceNode, ATargetNode: TcxTreeListNode;
begin
  ASourceNode := FocusedNode;
  ATargetNode := GetTargetNodeForMovingFocusedNodeDown;
  if GetNodeLevel(ATargetNode) = GetNodeLevel(ASourceNode) then
    DoMoveNode(ASourceNode, ATargetNode, False)
  else
    if ATargetNode.Count > 0 then
      DoMoveNode(ASourceNode, ATargetNode.getFirstChild, True)
    else
      DoMoveNode(ASourceNode, ATargetNode, False, True);
end;

procedure TdxRibbonCustomizationFormHelper.MoveFocusedNodeUp;
var
  ASourceNode, ATargetNode: TcxTreeListNode;
begin
  ASourceNode := FocusedNode;
  ATargetNode := GetTargetNodeForMovingFocusedNodeUp;
  if GetNodeLevel(ATargetNode) = GetNodeLevel(ASourceNode) then
    DoMoveNode(ASourceNode, ATargetNode, ATargetNode = ASourceNode.getPrevSibling)
  else
    if ATargetNode.Count > 0 then
      DoMoveNode(ASourceNode, ATargetNode.GetLastChild, False)
    else
      DoMoveNode(ASourceNode, ATargetNode, True, True);
end;

procedure TdxRibbonCustomizationFormHelper.PopulateCommandsTreeListContent;
var
  I: Integer;
begin
  LockContentUpdating;
  CommandsTreeList.BeginUpdate;
  try
    CommandsTreeList.Clear;
    CommandsTreeList.OptionsView.ShowRoot := False;
    CommandsTreeList.OptionsBehavior.IncSearch := CommandsComboBox.ItemIndex in [dxrcfCommandsCommandsNotInTheRibbon, dxrcfCommandsAllCommands];
    case CommandsComboBox.ItemIndex of
      dxrcfCommandsCommandsNotInTheRibbon:
        PopulateAllItems(Ribbon.BarManager, True);
      dxrcfCommandsAllCommands:
        PopulateAllItems(Ribbon.BarManager);
      dxrcfCommandsAllTabs:
        begin
          CreateContextNode(nil, CommandsTreeList);
          for I := 0 to Ribbon.Contexts.Count - 1 do
            CreateContextNode(Ribbon.Contexts[I], CommandsTreeList);
        end;
      dxrcfCommandsMainTabs:
        CreateContextNode(nil, CommandsTreeList);
      dxrcfCommandsToolTabs:
        for I := 0 to Ribbon.Contexts.Count - 1 do
          CreateContextNode(Ribbon.Contexts[I], CommandsTreeList);
      dxrcfCommandsCustomTabsAndGroups:
        PopulateCustomTabsAndGroups;
    end;
  finally
    CommandsTreeList.EndUpdate;
    UnlockContentUpdating;
  end;
end;

procedure TdxRibbonCustomizationFormHelper.PopulateRibbonTreeListContent(const AIsReseting: Boolean = False);
var
  I: Integer;
begin
  LockContentUpdating;
  RibbonTreeList.BeginUpdate;
  try
    RibbonTreeList.Clear;
    CreateContextNode(nil, RibbonTreeList, AIsReseting);
    for I := 0 to Ribbon.Contexts.Count - 1 do
      CreateContextNode(Ribbon.Contexts[I], RibbonTreeList, AIsReseting);
    UpdateContextsVisibility;
  finally
    RibbonTreeList.EndUpdate;
    UnlockContentUpdating;
  end;
end;

procedure TdxRibbonCustomizationFormHelper.RemoveNode(ANode: TcxTreeListNode);
var
  AGroupNode: TcxTreeListNode;
begin
  ANode.DeleteChildren;
  if GetNodeLevel(ANode) = dxrcfItemLevel then
    AGroupNode := ANode.Parent
  else
    AGroupNode := nil;
  ANode.Delete;
  if AGroupNode <> nil then
    SynchronizeMatchingGroupNodesWith(AGroupNode);
end;

procedure TdxRibbonCustomizationFormHelper.RenameNode(ANode: TcxTreeListNode; ANewCaption: string);
begin
  inherited RenameNode(ANode, ANewCaption);
  case GetNodeLevel(ANode) of
    dxrcfGroupLevel:
      SynchronizeMatchingGroupNodesWith(ANode);
    dxrcfItemLevel:
      SynchronizeMatchingGroupNodesWith(ANode.Parent);
  end;
end;

procedure TdxRibbonCustomizationFormHelper.ResetTabNode(ANode: TcxTreeListNode);
var
  AExpandedNodesList: TList;
begin
  LockContentUpdating;
  AExpandedNodesList := TList.Create;
  StoreTabNodeExpanding(ANode, AExpandedNodesList);
  try
    LoadTabNodeContentFormIni(ANode, RibbonTreeList, TdxBarManagerAccess(Ribbon.BarManager).SavedState);
    ANode.Expand(False);
    SetFocusedNode(ANode);
  finally
    RestoreTabNodeExpanding(ANode, AExpandedNodesList);
    AExpandedNodesList.Free;
    UnlockContentUpdating;
  end;
end;

procedure TdxRibbonCustomizationFormHelper.UpdateContextsVisibility;
var
  I: Integer;
begin
  if CanUpdateContent and (RibbonTreeList.Root.Count > 0) then
  begin
    RibbonTreeList.Root.Items[0].Visible := RibbonComboBox.ItemIndex in [dxrcfRibbonAllTabs, dxrcfRibbonMainTabs];
    for I := 1 to RibbonTreeList.Root.Count - 1 do
      RibbonTreeList.Root.Items[I].Visible := RibbonComboBox.ItemIndex in [dxrcfRibbonAllTabs, dxrcfRibbonToolTabs];
    RibbonTreeList.SelectionList.Clear;
    RibbonTreeList.FocusedNode := nil;
    ExpandAllContexts;
  end;
end;

function TdxRibbonCustomizationFormHelper.CanMoveFocusedNodeDown: Boolean;
begin
  Result := inherited CanMoveFocusedNodeDown and ((RibbonComboBox.ItemIndex <> dxrcfRibbonMainTabs) or
    (GetContextNode(GetTargetNodeForMovingFocusedNodeDown) = RibbonTreeList.Root.Items[0]));
end;

function TdxRibbonCustomizationFormHelper.CanMoveFocusedNodeUp: Boolean;
begin
  Result := inherited CanMoveFocusedNodeUp and ((RibbonComboBox.ItemIndex <> dxrcfRibbonToolTabs) or
    (GetContextNode(GetTargetNodeForMovingFocusedNodeUp) <> RibbonTreeList.Root.Items[0]));
end;

function TdxRibbonCustomizationFormHelper.CanMoveNodeTo(ASourceNode, ATargetNode: TcxTreeListNode): Boolean;
var
  AActualParentNode: TcxTreeListNode;
  ASourceNodeLevel, I: Integer;
begin
  Result := IsSourceNodeValid(ASourceNode) and IsTargetNodeValid(ASourceNode, ATargetNode) and
    (not IsRibbonGroupNodeWithSharedToolBar(ASourceNode) or IsParentTabTheSame(ASourceNode, ATargetNode));
  if Result then
  begin
    ASourceNodeLevel := Min(GetNodeLevel(ASourceNode), dxrcfItemLevel);
    AActualParentNode := ATargetNode;
    while (GetNodeLevel(AActualParentNode) > ASourceNodeLevel - 1) do
      AActualParentNode := AActualParentNode.Parent;
    if ((ASourceNode.TreeList = RibbonTreeList) or (ASourceNodeLevel = dxrcfItemLevel)) and
      (AActualParentNode <> ASourceNode.Parent) then
      for I := 0 to AActualParentNode.Count - 1 do
        if IsSameElement(ASourceNode, AActualParentNode.Items[I]) then
        begin
          Result := False;
          Break;
        end;
  end;
end;

function TdxRibbonCustomizationFormHelper.CanRemoveFocusedNode: Boolean;
begin
  Result := inherited CanRemoveFocusedNode;
  if Result then
    case Min(GetNodeLevel(FocusedNode), dxrcfItemLevel) of
      dxrcfContextLevel:
        Result := (FocusedNode.Data = nil) and (FocusedNode.Index <> 0);
      dxrcfTabLevel:
        Result := (FocusedNode.Data = nil) or not TdxRibbonTab(FocusedNode.Data).IsPredefined;
      dxrcfGroupLevel:
        Result := (FocusedNode.Data = nil) or not TdxRibbonTabGroup(FocusedNode.Data).IsToolBarShared;
      dxrcfItemLevel:
        Result := FocusedNode.Enabled;
    end;
end;

function TdxRibbonCustomizationFormHelper.CanResetFocusedNode: Boolean;
var
  AFocusedNode: TcxTreeListNode;
begin
  AFocusedNode := FocusedNode;
  Result := (AFocusedNode <> nil) and (GetNodeLevel(AFocusedNode) > dxrcfContextLevel);
  if Result then
  begin
    while GetNodeLevel(AFocusedNode) > dxrcfTabLevel do
      AFocusedNode := AFocusedNode.Parent;
    Result := (AFocusedNode.Data <> nil) and TdxRibbonTab(AFocusedNode.Data).IsPredefined;
  end;
end;

function TdxRibbonCustomizationFormHelper.DrawContextNode(Sender: TcxCustomTreeList; ACanvas: TcxCanvas;
  AViewInfo: TcxTreeListEditCellViewInfo; ALookAndFeelPainter: TcxCustomLookAndFeelPainter): Boolean;
var
  ATextRect: TRect;
begin
  ALookAndFeelPainter.DrawGalleryGroupHeader(ACanvas, AViewInfo.BoundsRect);
  ACanvas.Font.Style := ACanvas.Font.Style + [fsBold];
  ACanvas.Font.Color := ALookAndFeelPainter.GetGalleryGroupTextColor;
  ATextRect := cxRectCenterVertically(AViewInfo.BoundsRect, cxTextHeight(ACanvas.Handle));
  ATextRect := cxRectInflate(ATextRect, -cxHeaderTextOffset, 0);
  cxDrawText(ACanvas, AViewInfo.Node.Texts[0], ATextRect, DT_LEFT);
  Result := True;
end;

function TdxRibbonCustomizationFormHelper.AddNewNodeToFocusedNode(const ATargetLevel: Integer; ACaption: string): TcxTreeListNode;
var
  AParentNode: TcxTreeListNode;
  ANodeIndex: Integer;
begin
  ANodeIndex := -1;
  AParentNode := FocusedNode;
  while GetNodeLevel(AParentNode) >= ATargetLevel do
  begin
    ANodeIndex := AParentNode.Index;
    AParentNode := AParentNode.Parent;
  end;
  if (ANodeIndex >= 0) and (ANodeIndex < AParentNode.Count - 1) then
    Result := RibbonTreeList.Insert(AParentNode.Items[ANodeIndex + 1])
  else
    Result := RibbonTreeList.AddChild(AParentNode);
  Result.Texts[0] := ACaption;
  Result.Checked := (GetNodeLevel(Result) <> dxrcfGroupLevel) or AParentNode.Checked;
  Result.MakeVisible;
  SetFocusedNode(Result);
end;

procedure TdxRibbonCustomizationFormHelper.AfterNodeMoved(const AConvertNodeData: Boolean);
var
  AFocusedNode: TcxTreeListNode;
begin
  AFocusedNode := FocusedNode;
  case GetNodeLevel(AFocusedNode) of
    dxrcfTabLevel:
      if AConvertNodeData then
        ConvertTabNodeToNew(AFocusedNode);
    dxrcfGroupLevel:
      begin
        AFocusedNode.Checked := AFocusedNode.Parent.Checked;
        if AConvertNodeData then
          ConvertGroupNodeToNew(AFocusedNode);
      end;
    dxrcfItemLevel:
      begin
        if AConvertNodeData then
          ConvertItemNodeToNew(AFocusedNode);
        SynchronizeMatchingGroupNodesWith(AFocusedNode.Parent);
      end;
  end;
end;

procedure TdxRibbonCustomizationFormHelper.ConvertGroupNodeToNew(ANode: TcxTreeListNode);
var
  I: Integer;
begin
  for I := 0 to ANode.Count - 1 do
    ConvertItemNodeToNew(ANode.Items[I]);
  ANode.Data := nil;
  ANode.Texts[0] := ANode.Texts[0] + cxGetResourceString(@sdxRibbonCustomizationFormCustomElementSuffix);
  ANode.Enabled := True;
end;

procedure TdxRibbonCustomizationFormHelper.ConvertTabNodeToNew(ANode: TcxTreeListNode);
var
  I: Integer;
begin
  for I := 0 to ANode.Count - 1 do
    ConvertGroupNodeToNew(ANode.Items[I]);
  ANode.Data := nil;
  ANode.Texts[0] := ANode.Texts[0] + cxGetResourceString(@sdxRibbonCustomizationFormCustomElementSuffix);
end;

function TdxRibbonCustomizationFormHelper.CreateContextNode(AContext: TdxRibbonContext; ATargetTreeList: TcxTreeList;
  const AIsReseting: Boolean = False): TcxTreeListNode;
var
  I: Integer;
begin
  Result := ATargetTreeList.Add(nil, AContext);
  Result.Texts[0] := GetContextCaption(AContext);
  for I := 0 to Ribbon.TabCount - 1 do
    if (Ribbon.Tabs[I].Context = AContext) and (((ATargetTreeList = RibbonTreeList) and not AIsReseting) or
      Ribbon.Tabs[I].IsPredefined) then
      CreateTabNode(Ribbon.Tabs[I], Result, ATargetTreeList, AIsReseting);
  ATargetTreeList.AbsoluteItems[Result.AbsoluteIndex].Expand(False);
  Result.CheckGroupType := ncgCheckGroup;
end;

function TdxRibbonCustomizationFormHelper.CreateTabNode(ATab: TdxRibbonTab; AContextNode: TcxTreeListNode;
  ATargetTreeList: TcxTreeList; const AIsReseting: Boolean = False): TcxTreeListNode;
begin
  Result := ATargetTreeList.AddChild(AContextNode, ATab);
  if (ATargetTreeList = CommandsTreeList) or AIsReseting then
    LoadTabNodeContentFormIni(Result, ATargetTreeList, TdxBarManagerAccess(Ribbon.BarManager).SavedState)
  else
    PopulateTabNodeContent(Result, ATargetTreeList);
  Result.Expanded := ATab.Active;
end;

procedure TdxRibbonCustomizationFormHelper.DoMoveNode(ASourceNode, ATargetNode: TcxTreeListNode;
  const AInsertBeforeTargetNode: Boolean; const ATargetNodeIsParent: Boolean = False);
var
  ANewNode: TcxTreeListNode;
  AGroupWasHidden: Boolean;
begin
  if ATargetNodeIsParent then
    ANewNode := ATargetNode.AddChild
  else
    if AInsertBeforeTargetNode then
      ANewNode := ATargetNode.Parent.InsertChild(ATargetNode)
    else
      if ATargetNode.getNextSibling = nil then
        ANewNode := ATargetNode.Parent.AddChild
      else
        ANewNode := ATargetNode.Parent.InsertChild(ATargetNode.getNextSibling);
  AGroupWasHidden := IsGroupHiddenInTab(ASourceNode, ANewNode.Parent);
  CopyNode(ASourceNode, ANewNode);
  SetFocusedNode(ANewNode, True);
  AfterNodeMoved((ASourceNode.TreeList <> RibbonTreeList) and not AGroupWasHidden);
  if ASourceNode.TreeList = RibbonTreeList then
    RemoveNode(ASourceNode);
end;

function TdxRibbonCustomizationFormHelper.GetContextNode(ASourceNode: TcxTreeListNode): TcxTreeListNode;
begin
  Result := ASourceNode;
  while GetNodeLevel(Result) > dxrcfContextLevel do
    Result := Result.Parent;
end;

function TdxRibbonCustomizationFormHelper.GetItemNodeVisible(AItem: TdxBarItem): Boolean;
begin
  Result := inherited GetItemNodeVisible(AItem) and not (AItem is TdxRibbonQuickAccessGroupButton);
end;

function TdxRibbonCustomizationFormHelper.GetTargetNodeForMovingFocusedNodeDown: TcxTreeListNode;
var
  ALevel: Integer;
begin
  Result := FocusedNode;
  if Result <> nil then
  begin
    ALevel := GetNodeLevel(Result);
    repeat
      Result := Result.GetNext;
      while (Result <> nil) and (GetNodeLevel(Result) <> ALevel) and (GetNodeLevel(Result) <> (ALevel - 1)) do
        Result := Result.GetNext;
    until (Result = nil) or CanMoveNodeTo(FocusedNode, Result);
  end;
end;

function TdxRibbonCustomizationFormHelper.GetTargetNodeForMovingFocusedNodeUp: TcxTreeListNode;
var
  ALevel: Integer;
begin
  Result := FocusedNode;
  if Result <> nil then
  begin
    ALevel := GetNodeLevel(Result);
    repeat
      Result := Result.GetPrev;
      while (Result <> nil) and (GetNodeLevel(Result) <> ALevel) and ((GetNodeLevel(Result) <> (ALevel - 1)) or
        (Result = FocusedNode.Parent)) do
        Result := Result.GetPrev;
    until (Result = nil) or CanMoveNodeTo(FocusedNode, Result);
  end;
end;

function TdxRibbonCustomizationFormHelper.LoadExistingGroupNodeContentFormIni(AToolBar: TdxBar;
  AParentTabNode: TcxTreeListNode; ATargetTreeList: TcxTreeList; ASource: TCustomIniFile): TcxTreeListNode;
var
  AGroups: TdxRibbonTabGroups;
begin
  AGroups := TdxRibbonTab(AParentTabNode.Data).Groups;
  Result := ATargetTreeList.AddChild(AParentTabNode, AGroups.FindByToolBar(AToolBar));
  Result.Enabled := IsGroupCustomizingAllowed(Result);
  LoadBarContentFormIni(AToolBar, Result, ATargetTreeList, ASource);
end;

function TdxRibbonCustomizationFormHelper.LoadMissingGroupNodeContentFormIni(AToolBar: TdxBar;
  AParentTabNode: TcxTreeListNode; ATargetTreeList: TcxTreeList; ASource: TCustomIniFile): TcxTreeListNode;
var
  AGroups: TdxRibbonTabGroups;
  ATempGroup: TdxRibbonTabGroup;
begin
  AGroups := TdxRibbonTab(AParentTabNode.Data).Groups;
  AGroups.BeginUpdate;
  try
    ATempGroup := AGroups.Add;
    try
      ATempGroup.ToolBar := AToolBar;
      Result := ATargetTreeList.AddChild(AParentTabNode, ATempGroup);
      LoadBarContentFormIni(AToolBar, Result, ATargetTreeList, ASource);
      Result.Data := nil;
      Result.Enabled := True;
    finally
      AGroups.Delete(ATempGroup.Index);
    end;
  finally
    AGroups.EndUpdate;
  end;
end;

procedure TdxRibbonCustomizationFormHelper.LoadTabNodeContentFormIni(ANode: TcxTreeListNode;
  ATargetTreeList: TcxTreeList; ASource: TCustomIniFile);
var
  ATab: TdxRibbonTab;
  AToolBar: TdxBar;
  AToolBarVisible: Boolean;
  AGroupNode: TcxTreeListNode;
  ATabSection, AGroupSection: string;
  I: Integer;
begin
  ATab := TdxRibbonTab(ANode.Data);
  ATabSection := TdxRibbonTabCollectionAccess(Ribbon.Tabs).GetIniSection(ATab.Name, ASource);
  ANode.Texts[0] := ASource.ReadString(ATabSection, 'Caption', ATab.Caption);
  ANode.DeleteChildren;

  for I := 0 to ASource.ReadInteger(ATabSection, 'GroupCount', 0) - 1 do
  begin
    AGroupSection := TdxRibbonTabGroupsAccess(ATab.Groups).GetIniSection(I, ASource);
    AToolBar := Ribbon.BarManager.BarByComponentName(ASource.ReadString(AGroupSection, 'ToolBar', ''));
    AToolBarVisible := ASource.ReadBool(AGroupSection, 'ToolBarVisible', True);
    if (AToolBar <> nil) and ((ATargetTreeList = CommandsTreeList) or AToolBarVisible) then
    begin
      if ATab.Groups.FindByToolBar(AToolBar) <> nil then
        AGroupNode := LoadExistingGroupNodeContentFormIni(AToolBar, ANode, ATargetTreeList, ASource)
      else
        AGroupNode := LoadMissingGroupNodeContentFormIni(AToolBar, ANode, ATargetTreeList, ASource);
      AGroupNode.Texts[0] := ASource.ReadString(AGroupSection, 'Caption', AToolBar.Caption);
    end;
  end;
  
  for I := 0 to ANode.Count - 1 do
    SynchronizeMatchingGroupNodesWith(ANode.Items[I]);
  ANode.Checked := ASource.ReadBool(ATabSection, 'Visible', False);
end;

procedure TdxRibbonCustomizationFormHelper.PopulateCommandsComboBoxContent;
var
  AItems: TStrings;
begin
  AItems := CommandsComboBox.Properties.Items;
  LockContentUpdating;
  AItems.BeginUpdate;
  try
    AItems.Clear;
    AItems.Add(cxGetResourceString(@sdxRibbonCustomizationFormCommandsNotInTheRibbon));
    AItems.Add(cxGetResourceString(@sdxRibbonCustomizationFormAllCommands));
    AItems.Add(cxGetResourceString(@sdxRibbonCustomizationFormAllTabs));
    AItems.Add(cxGetResourceString(@sdxRibbonCustomizationFormMainTabs));
    AItems.Add(cxGetResourceString(@sdxRibbonCustomizationFormToolTabs));
    AItems.Add(cxGetResourceString(@sdxRibbonCustomizationFormCustomTabsAndGroups));
    CommandsComboBox.ItemIndex := dxrcfCommandsAllTabs;
  finally
    AItems.EndUpdate;
    UnlockContentUpdating;
  end;
end;

procedure TdxRibbonCustomizationFormHelper.PopulateCustomTabsAndGroups;
var
  ACustomGroupsNode: TcxTreeListNode;
  ATab: TdxRibbonTab; 
  AGroup: TdxRibbonTabGroup;
  I, J: Integer;
begin
  CommandsTreeList.OptionsView.ShowRoot := True;
  ACustomGroupsNode := CommandsTreeList.Add;
  ACustomGroupsNode.Texts[0] := cxGetResourceString(@sdxRibbonCustomizationFormCustomGroups);
  for I := 0 to Ribbon.TabCount - 1 do
  begin
    ATab := Ribbon.Tabs[I];
    if not ATab.IsPredefined then
      CreateTabNode(ATab, nil, CommandsTreeList).Expand(False);
    for J := 0 to ATab.Groups.Count - 1 do
    begin
      AGroup := ATab.Groups[J];
      if (AGroup.ToolBar <> nil) and not AGroup.ToolBar.IsPredefined then
        PopulateGroupNodeContent(CommandsTreeList.AddChild(ACustomGroupsNode, AGroup), CommandsTreeList);
    end;
  end;
  ACustomGroupsNode.MoveTo(CommandsTreeList.Root.Items[CommandsTreeList.Root.Count - 1], tlamAdd);
  ACustomGroupsNode.Expand(False);
end;

procedure TdxRibbonCustomizationFormHelper.PopulateGroupNodeContent(ANode: TcxTreeListNode; ATargetTreeList: TcxTreeList);
var
  AItemLinks: TdxBarItemLinks;
  AGroup: TdxRibbonTabGroup;
  I: Integer;
begin
  AGroup := TdxRibbonTabGroup(ANode.Data);
  ANode.Texts[0] := AGroup.Caption;
  ANode.Enabled := IsGroupCustomizingAllowed(ANode);
  AItemLinks := AGroup.ToolBar.ItemLinks;
  for I := 0 to AItemLinks.Count - 1 do
    PopulateItemNodeContent(ATargetTreeList.AddChild(ANode), AItemLinks[I].Item, AItemLinks[I]);
end;

procedure TdxRibbonCustomizationFormHelper.PopulateRibbonComboBoxContent;
var
  AItems: TStrings;
begin
  AItems := RibbonComboBox.Properties.Items;
  LockContentUpdating;
  AItems.BeginUpdate;
  try
    AItems.Clear;
    AItems.Add(cxGetResourceString(@sdxRibbonCustomizationFormAllTabs));
    AItems.Add(cxGetResourceString(@sdxRibbonCustomizationFormMainTabs));
    AItems.Add(cxGetResourceString(@sdxRibbonCustomizationFormToolTabs));
    RibbonComboBox.ItemIndex := dxrcfRibbonAllTabs;
  finally
    AItems.EndUpdate;
    UnlockContentUpdating;
  end;
end;

procedure TdxRibbonCustomizationFormHelper.PopulateTabNodeContent(ANode: TcxTreeListNode; ATargetTreeList: TcxTreeList);
var
  ATab: TdxRibbonTab;
  AGroup: TdxRibbonTabGroup;
  I: Integer;
begin
  ATab := TdxRibbonTab(ANode.Data);
  ANode.Texts[0] := ATab.Caption;
  for I := 0 to ATab.Groups.Count - 1 do
  begin
    AGroup := ATab.Groups[I];
    if (AGroup.ToolBar = nil) or ((ATargetTreeList = RibbonTreeList) and not TdxRibbonTabGroupAccess(AGroup).IsActuallyVisible) then
      Continue;
    PopulateGroupNodeContent(ATargetTreeList.AddChild(ANode, AGroup), ATargetTreeList);
  end;
  ANode.Checked := ATab.Visible;
end;

procedure TdxRibbonCustomizationFormHelper.RestoreTabNodeExpanding(ANode: TcxTreeListNode; AStorage: TList);
var
  AGroupNode: TcxTreeListNode;
  I: Integer;
begin
  for I := 0 to ANode.Count - 1 do
  begin
    AGroupNode := ANode.Items[I];
    if AStorage.IndexOf(AGroupNode.Data) <> -1 then
      AGroupNode.Expand(False);
  end;
  AStorage.Clear;
end;

procedure TdxRibbonCustomizationFormHelper.SetFocusedNode(ANode: TcxTreeListNode; const AExpandNodeParents: Boolean = False);
var
  AParentNode: TcxTreeListNode;
begin
  inherited SetFocusedNode(ANode, AExpandNodeParents);
  if AExpandNodeParents then
  begin
    AParentNode := ANode;
    while GetNodeLevel(AParentNode) > dxrcfContextLevel do
    begin
      AParentNode := AParentNode.Parent;
      AParentNode.Expand(False);
    end;
  end;
end;

procedure TdxRibbonCustomizationFormHelper.StoreTabNodeExpanding(ANode: TcxTreeListNode; AStorage: TList);
var
  AGroupNode: TcxTreeListNode;
  I: Integer;
begin
  for I := 0 to ANode.Count - 1 do
  begin
    AGroupNode := ANode.Items[I];
    if (AGroupNode.Data <> nil) and AGroupNode.Expanded then
      AStorage.Add(AGroupNode.Data);
  end;
end;

procedure TdxRibbonCustomizationFormHelper.SynchronizeMatchingGroupNodesWith(AGroupNode: TcxTreeListNode);
var
  AFocusedNode, ANode: TcxTreeListNode;
  AIsGroupExpanded: Boolean;
begin
  AFocusedNode := FocusedNode;
  RibbonTreeList.BeginUpdate;
  try
    ANode := RibbonTreeList.Root.getFirstChild;
    while (ANode <> nil) and (ANode.GetNext <> nil) do
    begin
      ANode := ANode.GetNext;
      if (GetNodeLevel(ANode) = dxrcfGroupLevel) and IsSameElement(ANode, AGroupNode) and (ANode <> AGroupNode) then
      begin
        AIsGroupExpanded := ANode.Expanded;
        ANode.DeleteChildren;
        CopyNode(AGroupNode, ANode, False);
        ANode.Expanded := AIsGroupExpanded;
      end;
    end;
  finally
    RibbonTreeList.EndUpdate;
  end;
  if AFocusedNode <> nil then
    AFocusedNode.Focused := True;
end;

procedure TdxRibbonCustomizationFormHelper.CheckAndAddContextNodeToRibbon(ANode: TcxTreeListNode);
var
  AContext: TdxRibbonContext;
begin
  if ANode.Data = nil then
  begin
    AContext := Ribbon.Contexts.Add;
    AContext.Visible := True;
    ANode.Data := AContext;
  end;
end;

procedure TdxRibbonCustomizationFormHelper.CheckAndAddGroupNodeToRibbon(ANode: TcxTreeListNode);
var
  AGroup: TdxRibbonTabGroup;
  ATab: TdxRibbonTab;
begin
  ATab := TdxRibbonTab(ANode.Parent.Data);
  AGroup := TdxRibbonTabGroup(ANode.Data);
  if AGroup = nil then
  begin
    AGroup := ATab.Groups.Add;
    AGroup.ToolBar := Ribbon.BarManager.AddToolBar;
    ANode.Data := AGroup;
  end
  else
    TdxRibbonTabGroupAccess(AGroup).MoveToTab(ATab);
  AGroup.ToolBar.Visible := True;
end;

procedure TdxRibbonCustomizationFormHelper.CheckAndAddItemNodeToRibbon(ANode: TcxTreeListNode);
var
  AItemLinks: TdxBarItemLinks;
  ANodeData: TdxRibbonCustomizationFormItemNodeData;
  ANewItemLink: TdxBarItemLink;
begin
  AItemLinks := TdxRibbonTabGroup(ANode.Parent.Data).ToolBar.ItemLinks;
  ANodeData := GetItemData(ANode);
  if ANodeData.IsNewItemLinkNeeded or not AItemLinks.HasItem(ANodeData.Item) then
  begin
    ANewItemLink := AItemLinks.Add(ANodeData.Item);
    if ANodeData.ItemLink <> nil then
      ANewItemLink.Assign(ANodeData.ItemLink);
    ANodeData.ItemLink := ANewItemLink;
    SynchronizeMatchingGroupNodesWith(ANode.Parent);
  end;
end;

procedure TdxRibbonCustomizationFormHelper.CheckAndAddTabNodeToRibbon(ANode: TcxTreeListNode);
var
  AContext: TdxRibbonContext;
  ATab: TdxRibbonTab;
begin
  AContext := TdxRibbonContext(ANode.Parent.Data);
  ATab := TdxRibbonTab(ANode.Data);
  if ATab = nil then
    ATab := Ribbon.Tabs.Add;
  ATab.Context := AContext;
  ATab.Visible := ANode.Checked;
  ANode.Data := ATab;
end;

procedure TdxRibbonCustomizationFormHelper.CheckAndDeleteContextsFromRibbon;
var
  AContexts: TdxRibbonContexts;
  I: Integer;
begin
  AContexts := Ribbon.Contexts;
  AContexts.BeginUpdate;
  try
    for I := AContexts.Count - 1 downto 0 do
    begin
      CheckAndDeleteTabsFromRibbon(AContexts[I]);
      if GetNodeFor(AContexts[I], RibbonTreeList.Root) = nil then
        AContexts.Delete(I);
    end;
    CheckAndDeleteTabsFromRibbon(nil);
  finally
    AContexts.EndUpdate;
  end;
end;

procedure TdxRibbonCustomizationFormHelper.CheckAndDeleteGroupsFromRibbon(AGroups: TdxRibbonTabGroups; ATabNode: TcxTreeListNode);
var
  AGroupNode: TcxTreeListNode;
  AGroup: TdxRibbonTabGroup;
  I: Integer;
begin
  AGroups.BeginUpdate;
  try
    for I := AGroups.Count - 1 downto 0 do
    begin
      AGroup := AGroups[I];
      if AGroup.ToolBar = nil then
        Continue;
      AGroupNode := GetNodeFor(AGroup, ATabNode);
      if AGroupNode <> nil then
        CheckAndDeleteItemLinksFromRibbon(AGroup.ToolBar.ItemLinks, AGroupNode)
      else
        if AGroup.ToolBar.IsPredefined then
          AGroup.ToolBar.Visible := False
        else
          Ribbon.BarManager.DeleteToolBar(AGroup.ToolBar, False);
    end;
  finally
    AGroups.EndUpdate;
  end;
end;

procedure TdxRibbonCustomizationFormHelper.CheckAndDeleteItemLinksFromRibbon(AItemLinks: TdxBarItemLinks; AGroupNode: TcxTreeListNode);
var
  I: Integer;
begin
  AItemLinks.BeginUpdate;
  try
    for I := AItemLinks.Count - 1 downto 0 do
      if GetNodeFor(AItemLinks[I].Item, AGroupNode) = nil then
        AItemLinks.Delete(I);
  finally
    AItemLinks.EndUpdate;
  end;
end;

procedure TdxRibbonCustomizationFormHelper.CheckAndDeleteTabsFromRibbon(AContext: TdxRibbonContext);
var
  ATabs: TdxRibbonTabCollection;
  AContextNode, ATabNode: TcxTreeListNode;
  ATab: TdxRibbonTab;
  I: Integer;
begin
  ATabs := Ribbon.Tabs;
  AContextNode := GetNodeFor(AContext, RibbonTreeList.Root);
  ATabs.BeginUpdate;
  try
    for I := ATabs.Count - 1 downto 0 do
    begin
      ATab := ATabs[I];
      if ATab.Context = AContext then
      begin
        ATabNode := GetNodeFor(ATab, AContextNode);
        CheckAndDeleteGroupsFromRibbon(ATab.Groups, ATabNode);
        if ATabNode = nil then
          ATabs.Delete(I);
      end;
    end;
  finally
    ATabs.EndUpdate;
  end;
end;

function TdxRibbonCustomizationFormHelper.IsGroupCustomizingAllowed(ANode: TcxTreeListNode): Boolean;
begin
  Result := (ANode <> nil) and ((ANode.Data = nil) or (TdxRibbonTabGroup(ANode.Data).ToolBar = nil) or
    TdxRibbonTabGroup(ANode.Data).ToolBar.AllowCustomizing);
end;

function TdxRibbonCustomizationFormHelper.IsGroupHiddenInTab(AGroupNode, ATabNode: TcxTreeListNode): Boolean;
var
  AGroup: TdxRibbonTabGroup;
  I: Integer;
begin
  Result := (GetNodeLevel(AGroupNode) = dxrcfGroupLevel) and (GetNodeLevel(ATabNode) = dxrcfTabLevel) and
    (AGroupNode.Data <> nil) and (ATabNode.Data <> nil);
  if Result then
  begin
    AGroup := TdxRibbonTabGroup(AGroupNode.Data);
    Result := (AGroup.ToolBar <> nil) and TdxRibbonTab(ATabNode.Data).Groups.ContainsToolBar(AGroup.ToolBar);
    if Result then
      for I := 0 to ATabNode.Count - 1 do
        if IsSameElement(ATabNode.Items[I], AGroup) then
        begin
          Result := False;
          Break;
        end;
  end;
end;

function TdxRibbonCustomizationFormHelper.IsParentTabTheSame(ASourceGroupNode, ATargetNode: TcxTreeListNode): Boolean;
var
  ATargetTabNode: TcxTreeListNode;
begin
  ATargetTabNode := ATargetNode;
  while GetNodeLevel(ATargetTabNode) <> dxrcfTabLevel do
    ATargetTabNode := ATargetTabNode.Parent;
  Result := ASourceGroupNode.Parent = ATargetTabNode;
end;

function TdxRibbonCustomizationFormHelper.IsRibbonGroupNodeWithSharedToolBar(ANode: TcxTreeListNode): Boolean;
begin
  Result := (ANode.TreeList = RibbonTreeList) and (GetNodeLevel(ANode) = dxrcfGroupLevel) and (ANode.Data <> nil) and
    TdxRibbonTabGroup(ANode.Data).IsToolBarShared;
end;

function TdxRibbonCustomizationFormHelper.IsSourceNodeValid(ASourceNode: TcxTreeListNode): Boolean;
begin
  Result := inherited IsSourceNodeValid(ASourceNode) and (GetNodeLevel(ASourceNode) <> dxrcfContextLevel) and
    ((GetNodeLevel(ASourceNode) < dxrcfItemLevel) or (ASourceNode.TreeList <> RibbonTreeList) or ASourceNode.Enabled) and
    not ((ASourceNode.TreeList = CommandsTreeList) and (CommandsComboBox.ItemIndex = dxrcfCommandsCustomTabsAndGroups) and
    (ASourceNode.Data = nil));
end;

function TdxRibbonCustomizationFormHelper.IsTargetNodeValid(ASourceNode, ATargetNode: TcxTreeListNode): Boolean;
begin
  Result := inherited IsTargetNodeValid(ASourceNode, ATargetNode) and
    (GetNodeLevel(ATargetNode) >= Min(GetNodeLevel(ASourceNode), dxrcfItemLevel) - 1) and
    (ATargetNode.Enabled or (GetNodeLevel(ASourceNode) <= dxrcfGroupLevel));
end;

function TdxRibbonCustomizationFormHelper.GetContextCaption(AContext: TdxRibbonContext): string;
begin
  if AContext <> nil then
    Result := AContext.Caption
  else
    Result := cxGetResourceString(@sdxRibbonCustomizationFormMainTabs);
end;

{ TdxRibbonQATCustomizationFormHelper }

procedure TdxRibbonQATCustomizationFormHelper.BeginApplyChanges;
begin
  Ribbon.BarManager.BeginUpdate;
end;

procedure TdxRibbonQATCustomizationFormHelper.CreateAddedElements;
var
  ANodeData: TdxRibbonCustomizationFormItemNodeData;
  AItemLinks: TdxBarItemLinks;
  ANewItemLink: TdxBarItemLink;
  I: Integer;
begin
  AItemLinks := Ribbon.QuickAccessToolbar.Toolbar.ItemLinks;
  for I := 0 to RibbonTreeList.Root.Count - 1 do
  begin
    ANodeData := GetItemData(RibbonTreeList.Root.Items[I]);
    if not ANodeData.IsBeginGroup and (ANodeData.IsNewItemLinkNeeded or not AItemLinks.HasItem(ANodeData.Item)) then
    begin
      ANewItemLink := AItemLinks.Add(ANodeData.Item);
      if ANodeData.ItemLink <> nil then
        ANewItemLink.Assign(ANodeData.ItemLink);
      ANodeData.ItemLink := ANewItemLink;
    end;
  end;
end;

procedure TdxRibbonQATCustomizationFormHelper.DeleteRemovedElements;
var
  AItemLinks: TdxBarItemLinks;
  I: Integer;
begin
  AItemLinks := Ribbon.QuickAccessToolbar.Toolbar.ItemLinks;
  AItemLinks.BeginUpdate;
  try
    for I := AItemLinks.Count - 1 downto 0 do
      if GetNodeFor(AItemLinks[I].Item, RibbonTreeList.Root) = nil then
        AItemLinks.Delete(I);
  finally
    AItemLinks.EndUpdate;
  end;
end;

procedure TdxRibbonQATCustomizationFormHelper.EndApplyChanges;
begin
  Ribbon.BarManager.EndUpdate;
end;

procedure TdxRibbonQATCustomizationFormHelper.ReorderElements;
var
  ANodeData: TdxRibbonCustomizationFormItemNodeData;
  AGroupCount, I: Integer;
begin
  AGroupCount := 0;
  for I := 0 to RibbonTreeList.Root.Count - 1 do
  begin
    ANodeData := GetItemData(RibbonTreeList.Root.Items[I]);
    if ANodeData.IsBeginGroup then
      Inc(AGroupCount)
    else
    begin
      ANodeData.ItemLink.BeginGroup := (I > 0) and GetItemData(RibbonTreeList.Root.Items[I - 1]).IsBeginGroup;
      ANodeData.ItemLink.Index := I - AGroupCount;
    end;
  end;
end;

procedure TdxRibbonQATCustomizationFormHelper.SynchronizeElementCaptions;
begin
  //do nothing
end;

procedure TdxRibbonQATCustomizationFormHelper.ChangeQATPosition(const AShowBelowRibbon: Boolean);
const
  QATPositionMap: array [Boolean] of TdxQuickAccessToolbarPosition = (qtpAboveRibbon, qtpBelowRibbon);
begin
  Ribbon.QuickAccessToolbar.Position := QATPositionMap[AShowBelowRibbon];
end;

procedure TdxRibbonQATCustomizationFormHelper.DropMovedNodeTo(ATargetNode: TcxTreeListNode; const ADropPointY: Integer);
begin
  if CanMoveNodeTo(MovedNode, ATargetNode) then
    if IsTargetNodeValid(MovedNode, ATargetNode) then
      DoMoveNode(MovedNode, ATargetNode, IsAboveNodeCenter(ATargetNode, ADropPointY))
    else
      DoMoveNode(MovedNode, ATargetNode, False, True);
end;

function TdxRibbonQATCustomizationFormHelper.GetNodeLevel(ANode: TcxTreeListNode): Integer;
begin
  Result := dxrcfItemLevel;
end;

function TdxRibbonQATCustomizationFormHelper.IsQATBelowRibbon: Boolean;
begin
  Result := Ribbon.QuickAccessToolbar.Position = qtpBelowRibbon;
end;

procedure TdxRibbonQATCustomizationFormHelper.MoveFocusedNodeDown;
begin
  DoMoveNode(FocusedNode, GetTargetNodeForMovingFocusedNodeDown, False);
end;

procedure TdxRibbonQATCustomizationFormHelper.MoveFocusedNodeUp;
begin
  DoMoveNode(FocusedNode, GetTargetNodeForMovingFocusedNodeUp, True);
end;

procedure TdxRibbonQATCustomizationFormHelper.PopulateCommandsComboBoxContent;
var
  AItems: TStrings;
  I: Integer;
begin
  AItems := CommandsComboBox.Properties.Items;
  LockContentUpdating;
  AItems.BeginUpdate;
  try
    AItems.Clear;
    AItems.Add(cxGetResourceString(@sdxRibbonCustomizationFormCommandsNotInTheRibbon));
    AItems.Add(cxGetResourceString(@sdxRibbonCustomizationFormAllCommands));
    PopulateContextTabs(nil, AItems);
    for I := 0 to Ribbon.Contexts.Count - 1 do
      PopulateContextTabs(Ribbon.Contexts[I], AItems);
    CommandsComboBox.ItemIndex := dxrcfCommandsAllCommands;
  finally
    AItems.EndUpdate;
    UnlockContentUpdating;
  end;
end;

procedure TdxRibbonQATCustomizationFormHelper.PopulateCommandsTreeListContent;
begin
  LockContentUpdating;
  CommandsTreeList.BeginUpdate;
  try
    CommandsTreeList.Clear;
    CommandsTreeList.OptionsBehavior.IncSearch := True;
    PopulateBeginGroupNodeContent(CommandsTreeList.Add);
    case CommandsComboBox.ItemIndex of
      dxrcfCommandsCommandsNotInTheRibbon:
        PopulateAllItems(Ribbon.BarManager, True);
      dxrcfCommandsAllCommands:
        PopulateAllItems(Ribbon.BarManager);
      else
        PopulateTabItems(TdxRibbonTab(CommandsComboBox.ItemObject));
    end;
  finally
    CommandsTreeList.EndUpdate;
    UnlockContentUpdating;
  end;
end;

procedure TdxRibbonQATCustomizationFormHelper.PopulateRibbonComboBoxContent;
begin
  //do nothing
end;

procedure TdxRibbonQATCustomizationFormHelper.PopulateRibbonTreeListContent(const AIsReseting: Boolean = False);
var
  AItemLinks: TdxBarItemLinks;
  I: Integer;
begin
  LockContentUpdating;
  RibbonTreeList.BeginUpdate;
  try                      
    RibbonTreeList.Clear;
    RibbonTreeList.OptionsView.ShowRoot := True;
    if AIsReseting then
      LoadBarContentFormIni(Ribbon.QuickAccessToolbar.Toolbar, RibbonTreeList.Root, RibbonTreeList,
        TdxBarManagerAccess(Ribbon.BarManager).SavedState)
    else
    begin
      AItemLinks := Ribbon.QuickAccessToolbar.Toolbar.ItemLinks;
      for I := 0 to AItemLinks.Count - 1 do
        PopulateItemNodeContent(RibbonTreeList.Add, AItemLinks[I].Item, AItemLinks[I], AItemLinks[I].BeginGroup);
    end;
  finally
    RibbonTreeList.EndUpdate;
    UnlockContentUpdating;
  end;
end;

procedure TdxRibbonQATCustomizationFormHelper.RemoveNode(ANode: TcxTreeListNode);
begin
  ANode.Delete;
end;

function TdxRibbonQATCustomizationFormHelper.CanMoveNodeTo(ASourceNode, ATargetNode: TcxTreeListNode): Boolean;
var
  I: Integer;
begin
  Result := IsSourceNodeValid(ASourceNode) and ((ATargetNode = nil) or IsTargetNodeValid(ASourceNode, ATargetNode));
  if Result and (ASourceNode.TreeList = CommandsTreeList) and not GetItemData(ASourceNode).IsBeginGroup then
    for I := 0 to RibbonTreeList.Root.Count - 1 do
      if IsSameElement(ASourceNode, RibbonTreeList.Root.Items[I]) then
      begin
        Result := False;
        Break;
      end;
end;

function TdxRibbonQATCustomizationFormHelper.CanRemoveFocusedNode: Boolean;
begin
  Result := inherited CanRemoveFocusedNode and FocusedNode.Enabled;
end;

function TdxRibbonQATCustomizationFormHelper.CanRenameFocusedNode: Boolean;
begin
  Result := False;
end;

procedure TdxRibbonQATCustomizationFormHelper.AfterNodeMoved(const AConvertNodeData: Boolean);
begin
  if AConvertNodeData then
    ConvertItemNodeToNew(FocusedNode);
end;

procedure TdxRibbonQATCustomizationFormHelper.DoMoveNode(ASourceNode, ATargetNode: TcxTreeListNode;
  const AInsertBeforeTargetNode: Boolean; const ATargetNodeIsParent: Boolean = False);
var
  ANewNode: TcxTreeListNode;
begin
  if (ATargetNode = nil) or AInsertBeforeTargetNode then
    ANewNode := RibbonTreeList.Insert(ATargetNode)
  else
    ANewNode := RibbonTreeList.Insert(ATargetNode.getNextSibling);
  CopyNode(ASourceNode, ANewNode);
  SetFocusedNode(ANewNode);
  AfterNodeMoved(ASourceNode.TreeList <> RibbonTreeList);
  if ASourceNode.TreeList = RibbonTreeList then
    RemoveNode(ASourceNode);
end;

function TdxRibbonQATCustomizationFormHelper.GetTargetNodeForMovingFocusedNodeDown: TcxTreeListNode;
begin
  Result := FocusedNode.getNextSibling;
end;

function TdxRibbonQATCustomizationFormHelper.GetTargetNodeForMovingFocusedNodeUp: TcxTreeListNode;
begin
  Result := FocusedNode.getPrevSibling;
end;

procedure TdxRibbonQATCustomizationFormHelper.PopulateBeginGroupNodeContent(ATargetNode: TcxTreeListNode);
begin
  ATargetNode.Data := TdxRibbonCustomizationFormItemNodeData.Create(nil, nil);
  ATargetNode.Texts[0] := cxGetResourceString(@sdxRibbonCustomizationFormBeginGroup);
  ATargetNode.Enabled := GetItemNodeEnabled(ATargetNode.Parent);
end;

procedure TdxRibbonQATCustomizationFormHelper.PopulateContextTabs(AContext: TdxRibbonContext; AItems: TStrings);
var
  AContextPrefix, ATabSuffix: string;
  I: Integer;
begin
  AContextPrefix := GetContextPrefix(AContext);
  ATabSuffix := cxGetResourceString(@sdxRibbonCustomizationFormTabSuffix);
  for I := 0 to Ribbon.TabCount - 1 do
    if Ribbon.Tabs[I].Context = AContext then
      AItems.AddObject(AContextPrefix + Ribbon.Tabs[I].Caption + ATabSuffix, Ribbon.Tabs[I]);
end;

procedure TdxRibbonQATCustomizationFormHelper.PopulateItemNodeContent(ATargetNode: TcxTreeListNode; AItem: TdxBarItem;
  AItemLink: TdxBarItemLink; const ANeedBeginGroupNode: Boolean = False);
begin
  inherited PopulateItemNodeContent(ATargetNode, AItem, AItemLink, ANeedBeginGroupNode);
  if (ATargetNode.TreeList = RibbonTreeList) and ANeedBeginGroupNode then
    PopulateBeginGroupNodeContent(ATargetNode.Parent.InsertChild(ATargetNode));
end;

procedure TdxRibbonQATCustomizationFormHelper.PopulateTabItems(ATab: TdxRibbonTab);
var
  AToolBar: TdxBar;
  I, J: Integer;
begin
  CommandsTreeList.OptionsView.ShowRoot := True;
  for I := 0 to ATab.Groups.Count - 1 do
  begin
    AToolBar := ATab.Groups[I].ToolBar;
    if AToolBar <> nil then
      for J := 0 to AToolBar.ItemLinks.Count - 1 do
        PopulateItemNodeContent(CommandsTreeList.Add, AToolBar.ItemLinks[J].Item, AToolBar.ItemLinks[J]);
  end;
end;

function TdxRibbonQATCustomizationFormHelper.IsSourceNodeValid(ASourceNode: TcxTreeListNode): Boolean;
begin
  Result := inherited IsSourceNodeValid(ASourceNode) and ((ASourceNode.TreeList <> RibbonTreeList) or ASourceNode.Enabled);
end;

function TdxRibbonQATCustomizationFormHelper.IsTargetNodeValid(ASourceNode, ATargetNode: TcxTreeListNode): Boolean;
begin
  Result := inherited IsTargetNodeValid(ASourceNode, ATargetNode) and ATargetNode.Enabled;
end;

function TdxRibbonQATCustomizationFormHelper.GetContextPrefix(AContext: TdxRibbonContext): string;
begin
  if AContext <> nil then
    Result := AContext.Caption + cxGetResourceString(@sdxRibbonCustomizationFormDelimiterContextTab)
  else
    Result := '';
end;

end.
