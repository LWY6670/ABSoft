{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressBars string table constants                       }
{                                                                    }
{           Copyright (c) 1998-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSBARS AND ALL ACCOMPANYING VCL  }
{   CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY.                  }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxBarStrs;

{$I cxVer.inc}

interface

resourcestring
  dxSBAR_LOOKUPDIALOGCAPTION = '选择值';
  dxSBAR_LOOKUPDIALOGOK = '确定';
  dxSBAR_LOOKUPDIALOGCANCEL = '取消';

  dxSBAR_DIALOGOK = '确定';
  dxSBAR_DIALOGCANCEL = '取消';
  dxSBAR_COLOR_STR_0 = '黑色';
  dxSBAR_COLOR_STR_1 = '褐色';
  dxSBAR_COLOR_STR_2 = '绿色';
  dxSBAR_COLOR_STR_3 = '黄绿';
  dxSBAR_COLOR_STR_4 = '深蓝';
  dxSBAR_COLOR_STR_5 = '紫色';
  dxSBAR_COLOR_STR_6 = '青色';
  dxSBAR_COLOR_STR_7 = '灰色';
  dxSBAR_COLOR_STR_8 = '银色';
  dxSBAR_COLOR_STR_9 = '红色';
  dxSBAR_COLOR_STR_10 = '浅绿色';
  dxSBAR_COLOR_STR_11 = '黄色';
  dxSBAR_COLOR_STR_12 = '蓝色';
  dxSBAR_COLOR_STR_13 = '紫红色';
  dxSBAR_COLOR_STR_14 = '浅绿色';
  dxSBAR_COLOR_STR_15 = '白色';
  dxSBAR_COLORAUTOTEXT = '(自动)';
  dxSBAR_COLORCUSTOMTEXT = '(自定义)';
  dxSBAR_DATETODAY = '今天';
  dxSBAR_DATECLEAR = '清除';
  dxSBAR_DATEDIALOGCAPTION = '选择日期';
  dxSBAR_TREEVIEWDIALOGCAPTION = '选择项目';
  dxSBAR_IMAGEDIALOGCAPTION = '选择项目';
  dxSBAR_IMAGEINDEX = '图片索引';
  dxSBAR_IMAGETEXT = '文本';
  dxSBAR_PLACEFORCONTROL = '位置在 ';
  dxSBAR_CANTASSIGNCONTROL = '不能分配多个TdxBarControlContainerItem控件';
  dxSBAR_CXEDITVALUEDIALOGCAPTION = '输入值';

  dxSBAR_WANTTORESETTOOLBAR = '是否重置工具栏''%s''？';
  dxSBAR_WANTTORESETUSAGEDATA = '本操作将删除此应用程序中使用过的命令，将默认的命令还原到菜单和工具栏中。它不会撤消任何显式自定义。确定要继续吗？';
  dxSBAR_BARMANAGERMORETHANONE  = '一个控件只能包含一个TdxBarManager';
  dxSBAR_BARMANAGERBADOWNER = 'TdxBarManager的所有者应具有 TWinControl';
  dxSBAR_NOBARMANAGERS = '没有有效的TdxBarManagers';
  dxSBAR_WANTTODELETETOOLBAR = '是否删除''%s''工具栏？';
  dxSBAR_WANTTODELETETOOLBARS = '是否删除选择的工具栏？';
  dxSBAR_WANTTODELETECATEGORY = '是否删除''%s''分类？';
  dxSBAR_WANTTOCLEARCOMMANDS = '是否删除''%s''分类中的所有命令？';
  dxSBAR_RECURSIVEMENUS = '不能创建循环的菜单';
  dxSBAR_COMMANDNAMECANNOTBEBLANK = '命令名称不能为空。请输入一个名称。';
  dxSBAR_TOOLBAREXISTS = '工具栏''%s''已存在。输入其他名称。';
  dxSBAR_RECURSIVEGROUPS = '不能创建循环的分组';
  dxSBAR_WANTTODELETECOMPLEXITEM = '已选定对象其中含有多个链接的项目。请确认是否删除这些链接？';
  dxSBAR_CANTPLACEQUICKACCESSGROUPBUTTON = '您只能将TdxRibbonQuickAccessGroupButton放置在TdxRibbonQuickAccessToolbar上';
  dxSBAR_QUICKACCESSGROUPBUTTONTOOLBARNOTDOCKEDINRIBBON = '快速存取组按钮的工具条不能停靠在 Ribbon';
  dxSBAR_QUICKACCESSALREADYHASGROUPBUTTON = '快速访问工具条已经包含相同的工具条的分组按钮';
  dxSBAR_CANTPLACESEPARATOR = '分隔符不能置于指定工具栏';
  dxSBAR_CANTPLACERIBBONGALLERY = 'TdxRibbonGalleryItem只能放置到一个子菜单或功能区控件中';
  dxSBAR_CANTPLACESKINCHOOSERGALLERY = 'TdxSkinChooserGalleryItem只能放置到一个子菜单或功能区控件中';
  
  dxSBAR_CANTMERGEBARMANAGER = '您不能对指定的工具栏管理器进行合并操作';
  dxSBAR_CANTMERGETOOLBAR = '您不能对指定的工具条进行合并操作';
  dxSBAR_CANTMERGEWITHMERGEDTOOLBAR = '您不能用已被合并的工具条来合并其他工具条';
  dxSBAR_CANTUNMERGETOOLBAR = '不能分离指定的工具栏';
  dxSBAR_ONEOFTOOLBARSALREADYMERGED = '指定工具栏管理器已经合并';
  dxSBAR_ONEOFTOOLBARSHASMERGEDTOOLBARS = '指定工具栏管理器已经合并';
  dxSBAR_TOOLBARHASMERGEDTOOLBARS = '工具栏''%s''已合并到工具栏中';
  dxSBAR_TOOLBARSALREADYMERGED = '工具栏''%s''已经合并在工具栏''%s''中了';
  dxSBAR_TOOLBARSARENOTMERGED = '工具栏''%s''没有与工具栏''%s''合并';
  
  dxSBAR_RIBBONCANTMERGE = '不能合并指定的功能区';
  dxSBAR_RIBBONCANTMERGETAB = '不能合并指定的功能区页';
  dxSBAR_RIBBONCANTMERGEWITHOUTBARMANAGER = '不能合并功能区和指定非栏管理器';
  dxSBAR_RIBBONCANTUNMERGE = '不能拆分指定的功能区';
  dxSBAR_RIBBONCANTUNMERGETAB = '不能拆分指定的功能区页';
  dxSBAR_RIBBONONEOFTABGROUPSALREADYMERGED = '一个指定的功能区页中的功能区页组已经合并';
  dxSBAR_RIBBONSARENOTMERGED = '功能区''%s''没有与功能区''%s''合并';
  dxSBAR_RIBBONTABSARENOTMERGED = '功能区页''%s''没有与功能区页''%s''合并';
  
  dxSBAR_RIBBON_MINIMIZERIBBON = '收缩功能区';
  dxSBAR_RIBBON_PINRIBBON = '固定功能区';
  dxSBAR_RIBBON_RESTORERIBBON = '展开功能区';
  dxSBAR_RIBBONFORM_CLOSE = '关闭';
  dxSBAR_RIBBONFORM_DISPLAYOPTIONS = 'Ribbon Display Options';

  dxSBAR_RIBBONFORM_HELP = '帮助';
  dxSBAR_RIBBONFORM_MAXIMIZE = '最大化';
  dxSBAR_RIBBONFORM_MINIMIZE = '最小化';
  dxSBAR_RIBBONFORM_RESTOREDOWN = '向下还原';
  dxSBAR_RIBBONFORM_RESTOREUP = '向上还原';
  dxSBAR_RIBBONDISPLAYOPTIONS_AUTOHIDERIBBON_CAPTION = 'Auto-hide Ribbon';
  dxSBAR_RIBBONDISPLAYOPTIONS_AUTOHIDERIBBON_DESCRIPTION = 'Hide the Ribbon. Click at the top of the application to show it.';
  dxSBAR_RIBBONDISPLAYOPTIONS_SHOWTABS_CAPTION = 'Show Tabs';
  dxSBAR_RIBBONDISPLAYOPTIONS_SHOWTABS_DESCRIPTION = 'Show Ribbon tabs only. Click a tab to show the commands.';
  dxSBAR_RIBBONDISPLAYOPTIONS_SHOWTABSANDCOMMANDS_CAPTION = 'Show Tabs and Commands';
  dxSBAR_RIBBONDISPLAYOPTIONS_SHOWTABSANDCOMMANDS_DESCRIPTION = 'Show Ribbon tabs and commands all the time.';

  dxSBAR_DEFAULTCATEGORYNAME = '默认';
  // begin DesignTime section
  dxSBAR_NEWBUTTONCAPTION = '新建按钮';
  dxSBAR_NEWITEMCAPTION = '新建项';
  dxSBAR_NEWRIBBONGALLERYITEMCAPTION = '新建分类';
  dxSBAR_NEWSEPARATORCAPTION = '新建分隔符';
  dxSBAR_NEWSUBITEMCAPTION = '新建子项目';

  dxSBAR_CP_ADDSUBITEM = '添加子项目(&S)';
  dxSBAR_CP_ADDBUTTON = '添加按钮(&B)';
  dxSBAR_CP_ADDLARGEBUTTON = '添加大图标(&A)';
  dxSBAR_CP_ADDSEPARATOR = '添加分隔符(&S)';
  dxSBAR_CP_ADDDXITEM = '添加项目(&I)';
  dxSBAR_CP_ADDCXITEM = '添加输入框(&C)';
  dxSBAR_CP_ADDGROUPBUTTON = '添加分组按钮(&U)';
  dxSBAR_CP_DELETEITEM = '删除项目';
  dxSBAR_CP_DELETELINK = '删除链接';
  // end DesignTime section

  dxSBAR_CP_RESET = '重置(&R)';
  dxSBAR_CP_DELETE = '删除(&D)';
  dxSBAR_CP_NAME = '名称(&N):';
  dxSBAR_CP_CAPTION = '标题(&C):'; // is the same as dxSBAR_CP_NAME (at design time)
  dxSBAR_CP_BUTTONPAINTSTYLEMENU = '按钮风格(&S)';
  dxSBAR_CP_DEFAULTSTYLE = '默认风格(&U)';
  dxSBAR_CP_TEXTONLYALWAYS = '仅文本(总是)(&T)';
  dxSBAR_CP_TEXTONLYINMENUS = '仅文本(菜单中)(&O)';
  dxSBAR_CP_IMAGEANDTEXT = '图像和文本(&A)';
  dxSBAR_CP_BEGINAGROUP = '开始分组(&G)';
  dxSBAR_CP_VISIBLE = '可见(&V)';
  dxSBAR_CP_MOSTRECENTLYUSED = '最近使用过的(&M)';
  // begin DesignTime section
  dxSBAR_CP_DISTRIBUTED = '分布式(&T)';
  dxSBAR_CP_POSITIONMENU = '位置(&P)';
  dxSBAR_CP_VIEWLEVELSMENU = '查看层(&L)';
  dxSBAR_CP_ALLVIEWLEVELS = '所有';
  dxSBAR_CP_SINGLEVIEWLEVELITEMSUFFIX = ' 唯一';
  dxSBAR_CP_BUTTONGROUPMENU = '按钮分组(&R)';
  dxSBAR_CP_BUTTONGROUP = '分组';
  dxSBAR_CP_BUTTONUNGROUP = '取消分组';
  // end DesignTime section

  dxSBAR_ADDEX = '添加...';
  dxSBAR_RENAMEEX = '重命名...';
  dxSBAR_DELETE = '删除';
  dxSBAR_CLEAR = '清除';
  dxSBAR_VISIBLE = '可见';
  dxSBAR_OK = '确定';
  dxSBAR_CANCEL = '取消';
  dxSBAR_SUBMENUEDITOR = '子菜单编辑器...';
  dxSBAR_SUBMENUEDITORCAPTION = 'ExpressBars子菜单编辑器';
  dxSBAR_INSERTEX = '插入...';

  dxSBAR_MOVEUP = '上移';
  dxSBAR_MOVEDOWN = '下移';
  dxSBAR_POPUPMENUEDITOR = '弹出菜单编辑器...';
  dxSBAR_TABSHEET1 = ' 工具条 ';
  dxSBAR_TABSHEET2 = ' 命令 ';
  dxSBAR_TABSHEET3 = ' 选项 ';
  dxSBAR_TOOLBARS = '工具栏(&A):';
  dxSBAR_TNEW = '新建(&N)...';
  dxSBAR_TRENAME = '重命名(&E)...';
  dxSBAR_TDELETE = '删除(&D)';
  dxSBAR_TRESET = '重置(&R)...';
  dxSBAR_CLOSE = '关闭';
  dxSBAR_CAPTION = '自定义';
  dxSBAR_CATEGORIES = '分类(&G):';
  dxSBAR_COMMANDS = '命令(&D):';
  dxSBAR_DESCRIPTION = '说明  ';

  dxSBAR_MDIMINIMIZE = '最小化窗口';
  dxSBAR_MDIRESTORE = '还原窗口';
  dxSBAR_MDICLOSE = '关闭窗口';
  dxSBAR_CUSTOMIZE = '自定义(&C)...';
  dxSBAR_ADDREMOVEBUTTONS = '添加或删除按钮(&A)';
  dxSBAR_MOREBUTTONS = '更多按钮';
  dxSBAR_RESETTOOLBAR = '重置工具栏(&R)';
  dxSBAR_EXPAND = '展开(Ctrl+Down)';
  dxSBAR_DRAGTOMAKEMENUFLOAT = '拖动鼠标，使这个菜单浮动';
  dxSBAR_MORECOMMANDS = '更多命令(&M)...';
  dxSBAR_SHOWBELOWRIBBON = '显示在功能区下方快速访问工具栏(&S)';
  dxSBAR_SHOWABOVERIBBON = '显示快速访问工具栏上方功能区(&S)';
  dxSBAR_MINIMIZERIBBON = '最小化功能区(&N)';
  dxSBAR_CUSTOMIZERIBBON = 'Customize the &Ribbon...';
  dxSBAR_CUSTOMIZERIBBONQAT = '&Customize Quick Access Toolbar...'; 
  dxSBAR_ADDTOQAT = '添加到快速访问工具栏(&A)';
  dxSBAR_ADDTOQATITEMNAME = '%s 添加到快速访问工具条(&A)';
  dxSBAR_REMOVEFROMQAT = '从快速访问工具栏中删除(&R)';
  dxSBAR_CUSTOMIZEQAT = '自定义快速访问工具条';
  dxSBAR_ADDGALLERYNAME = '图库';
  dxSBAR_SHOWALLGALLERYGROUPS = '显示所有分组';
  dxSBAR_HIDEALLGALLERYGROUPS = '隐藏全部组';
  dxSBAR_CLEARGALLERYFILTER = '清空过滤';
  dxSBAR_GALLERYEMPTYFILTERCAPTION = '<空>';
  dxSBAR_PIN = '固定这个项到列表';
  dxSBAR_UNPIN = '从列表中取消项的固定';

  dxSBAR_TOOLBARNEWNAME  = '自定义 ';
  dxSBAR_CATEGORYADD  = '添加类别';
  dxSBAR_CATEGORYINSERT  = '插入类别';
  dxSBAR_CATEGORYRENAME  = '重命名类别';
  dxSBAR_TOOLBARADD  = '添加工具栏';
  dxSBAR_TOOLBARRENAME  = '重命名工具栏';
  dxSBAR_CATEGORYNAME  = '类别名称(&C):';
  dxSBAR_TOOLBARNAME  = '工具条名称(&T):';
  dxSBAR_CUSTOMIZINGFORM = '自定义窗口...';

  dxSBAR_MODIFY = '...修改';
  dxSBAR_PERSMENUSANDTOOLBARS = '个性化菜单和工具栏  ';
  dxSBAR_MENUSSHOWRECENTITEMS = '菜单优先显示最近使用过的命令(&N)';
  dxSBAR_SHOWFULLMENUSAFTERDELAY = '短延迟后显示全部菜单(&U)';
  dxSBAR_RESETUSAGEDATA = '重置我使用的数据(&R)';

  dxSBAR_OTHEROPTIONS = '其他  ';
  dxSBAR_LARGEICONS = '大图标(&L)';
  dxSBAR_HINTOPT1 = '在工具栏显示工具提示(&T)';
  dxSBAR_HINTOPT2 = '在工具提示中显示快捷键(&H)';
  dxSBAR_MENUANIMATIONS = '菜单动画(&M):';
  dxSBAR_MENUANIM1 = '(无)';
  dxSBAR_MENUANIM2 = '随机';
  dxSBAR_MENUANIM3 = '折叠';
  dxSBAR_MENUANIM4 = '滑动';
  dxSBAR_MENUANIM5 = '淡入';
  
  dxSBAR_CANTFINDBARMANAGERFORSTATUSBAR = '没有找到状态栏的栏管理器';

  dxSBAR_BUTTONDEFAULTACTIONDESCRIPTION = '按下';

  SBlob = '(二进制对象)';
  
  dxSBAR_APPMENUOUTSIDERIBBON = '应用程序菜单不能显示在功能区外';
  dxSBAR_EXTRAPANEHEADER = 'Recent Documents';
  dxSBAR_GDIPLUSNEEDED = '%s 需要安装 微软 GDI+ 连接库';
  dxSBAR_RIBBONMORETHANONE  = '窗体中只能有一个%s实例';
  dxSBAR_RIBBONBADOWNER = '%s 应将 TCustomForm 做为它的拥有者';
  dxSBAR_RIBBONBADPARENT = '%s 应将 TCustomForm 做为它的父组件';
  dxSBAR_RIBBONADDTAB = '添加选项卡';
  dxSBAR_RIBBONDELETETAB = '删除选项卡';
  dxSBAR_RIBBONADDEMPTYGROUP = '添加空分组';
  dxSBAR_RIBBONADDGROUPWITHTOOLBAR = '添加组和工具栏';
  dxSBAR_RIBBONDELETEGROUP = '删除分组';

  dxSBAR_ACCESSIBILITY_RIBBONNAME = '功能区';
  dxSBAR_ACCESSIBILITY_RIBBONTABCOLLECTIONNAME = '功能区选项页';

  sdxRibbonCustomizationFormAddErrorMsg = 'Commands need to be added to custom groups. ' +
    'To create a group, pick a tab in the list, then click New Group.';
  sdxRibbonCustomizationFormAllCommands = 'All Commands';
  sdxRibbonCustomizationFormAllTabs = 'All Tabs';
  sdxRibbonCustomizationFormBeginGroup = '<Separator>';
  sdxRibbonCustomizationFormCommandsNotInTheRibbon = 'Commands Not in the Ribbon';
  sdxRibbonCustomizationFormCustomElementSuffix = ' (Custom)';
  sdxRibbonCustomizationFormCustomGroups = 'Custom Groups';
  sdxRibbonCustomizationFormCustomTabsAndGroups = 'Custom Tabs and Groups';
  sdxRibbonCustomizationFormDelimiterContextTab = ' | ';
  sdxRibbonCustomizationFormDisplayName = 'Display name';
  sdxRibbonCustomizationFormMainTabs = 'Main Tabs';
  sdxRibbonCustomizationFormNewContext = 'New Context';
  sdxRibbonCustomizationFormNewGroup = 'New Group';
  sdxRibbonCustomizationFormNewTab = 'New Tab';
  sdxRibbonCustomizationFormRename = 'Rename';
  sdxRibbonCustomizationFormTabSuffix = ' Tab';
  sdxRibbonCustomizationFormToolTabs = 'Tool Tabs';
  //Captions
  sdxRibbonCustomizationFormCaptionAdd = '&Add';
  sdxRibbonCustomizationFormCaptionAddNewContext = 'Add New &Context';
  sdxRibbonCustomizationFormCaptionAddNewGroup = 'Add New &Group';
  sdxRibbonCustomizationFormCaptionAddNewTab = 'Add New &Tab';
  sdxRibbonCustomizationFormCaptionCancel = '&Cancel';
  sdxRibbonCustomizationFormCaptionCommandsSource = 'C&hoose commands from:';
  sdxRibbonCustomizationFormCaptionMoveDown = '向下';
  sdxRibbonCustomizationFormCaptionMoveUp = '向上';
  sdxRibbonCustomizationFormCaptionNewElement = '增加';
  sdxRibbonCustomizationFormCaptionOK = '确定';
  sdxRibbonCustomizationFormCaptionQuickAccessToolbar = 'Customize &Quick Access Toolbar:';
  sdxRibbonCustomizationFormCaptionQuickAccessToolbarShowBelowRibbon = 'Show Quick Access Toolbar below the Ribbon';
  sdxRibbonCustomizationFormCaptionQuickAccessToolbarTitle = 'Quick Access Toolbar Customization';
  sdxRibbonCustomizationFormCaptionRemove = '&Remove';
  sdxRibbonCustomizationFormCaptionRename = 'Rena&me...';
  sdxRibbonCustomizationFormCaptionReset = 'R&eset';
  sdxRibbonCustomizationFormCaptionResetAllCustomizations = 'Reset a&ll customizations';
  sdxRibbonCustomizationFormCaptionResetOnlySelectedTab = 'Reset only &selected tab';
  sdxRibbonCustomizationFormCaptionResetSelectedTab = 'Reset Ta&b';
  sdxRibbonCustomizationFormCaptionRibbonTitle = 'Ribbon Customization';
  sdxRibbonCustomizationFormCaptionRibbonSource = 'Customize the Ri&bbon:';
  sdxRibbonCustomizationFormCaptionShowTab = '&Show Tab';

implementation

uses
  dxCore;

procedure AddBarsResourceStringNames(AProduct: TdxProductResourceStrings);

  procedure InternalAdd(const AResourceStringName: string; AAddress: Pointer);
  begin
    AProduct.Add(AResourceStringName, AAddress);
  end;

begin
  InternalAdd('dxSBAR_LOOKUPDIALOGCAPTION', @dxSBAR_LOOKUPDIALOGCAPTION);
  InternalAdd('dxSBAR_LOOKUPDIALOGOK', @dxSBAR_LOOKUPDIALOGOK);
  InternalAdd('dxSBAR_LOOKUPDIALOGCANCEL', @dxSBAR_LOOKUPDIALOGCANCEL);
  InternalAdd('dxSBAR_DIALOGOK', @dxSBAR_DIALOGOK);
  InternalAdd('dxSBAR_DIALOGCANCEL', @dxSBAR_DIALOGCANCEL);
  InternalAdd('dxSBAR_COLOR_STR_0', @dxSBAR_COLOR_STR_0);
  InternalAdd('dxSBAR_COLOR_STR_1', @dxSBAR_COLOR_STR_1);
  InternalAdd('dxSBAR_COLOR_STR_2', @dxSBAR_COLOR_STR_2);
  InternalAdd('dxSBAR_COLOR_STR_3', @dxSBAR_COLOR_STR_3);
  InternalAdd('dxSBAR_COLOR_STR_4', @dxSBAR_COLOR_STR_4);
  InternalAdd('dxSBAR_COLOR_STR_5', @dxSBAR_COLOR_STR_5);
  InternalAdd('dxSBAR_COLOR_STR_6', @dxSBAR_COLOR_STR_6);
  InternalAdd('dxSBAR_COLOR_STR_7', @dxSBAR_COLOR_STR_7);
  InternalAdd('dxSBAR_COLOR_STR_8', @dxSBAR_COLOR_STR_8);
  InternalAdd('dxSBAR_COLOR_STR_9', @dxSBAR_COLOR_STR_9);
  InternalAdd('dxSBAR_COLOR_STR_10', @dxSBAR_COLOR_STR_10);
  InternalAdd('dxSBAR_COLOR_STR_11', @dxSBAR_COLOR_STR_11);
  InternalAdd('dxSBAR_COLOR_STR_12', @dxSBAR_COLOR_STR_12);
  InternalAdd('dxSBAR_COLOR_STR_13', @dxSBAR_COLOR_STR_13);
  InternalAdd('dxSBAR_COLOR_STR_14', @dxSBAR_COLOR_STR_14);
  InternalAdd('dxSBAR_COLOR_STR_15', @dxSBAR_COLOR_STR_15);
  InternalAdd('dxSBAR_COLORAUTOTEXT', @dxSBAR_COLORAUTOTEXT);
  InternalAdd('dxSBAR_COLORCUSTOMTEXT', @dxSBAR_COLORCUSTOMTEXT);
  InternalAdd('dxSBAR_DATETODAY', @dxSBAR_DATETODAY);
  InternalAdd('dxSBAR_DATECLEAR', @dxSBAR_DATECLEAR);
  InternalAdd('dxSBAR_DATEDIALOGCAPTION', @dxSBAR_DATEDIALOGCAPTION);
  InternalAdd('dxSBAR_TREEVIEWDIALOGCAPTION', @dxSBAR_TREEVIEWDIALOGCAPTION);
  InternalAdd('dxSBAR_IMAGEDIALOGCAPTION', @dxSBAR_IMAGEDIALOGCAPTION);
  InternalAdd('dxSBAR_IMAGEINDEX', @dxSBAR_IMAGEINDEX);
  InternalAdd('dxSBAR_IMAGETEXT', @dxSBAR_IMAGETEXT);
  InternalAdd('dxSBAR_PLACEFORCONTROL', @dxSBAR_PLACEFORCONTROL);
  InternalAdd('dxSBAR_CANTASSIGNCONTROL', @dxSBAR_CANTASSIGNCONTROL);
  InternalAdd('dxSBAR_CXEDITVALUEDIALOGCAPTION', @dxSBAR_CXEDITVALUEDIALOGCAPTION);
  InternalAdd('dxSBAR_WANTTORESETTOOLBAR', @dxSBAR_WANTTORESETTOOLBAR);
  InternalAdd('dxSBAR_WANTTORESETUSAGEDATA', @dxSBAR_WANTTORESETUSAGEDATA);
  InternalAdd('dxSBAR_BARMANAGERMORETHANONE', @dxSBAR_BARMANAGERMORETHANONE);
  InternalAdd('dxSBAR_BARMANAGERBADOWNER', @dxSBAR_BARMANAGERBADOWNER);
  InternalAdd('dxSBAR_NOBARMANAGERS', @dxSBAR_NOBARMANAGERS);
  InternalAdd('dxSBAR_WANTTODELETETOOLBAR', @dxSBAR_WANTTODELETETOOLBAR);
  InternalAdd('dxSBAR_WANTTODELETETOOLBARS', @dxSBAR_WANTTODELETETOOLBARS);
  InternalAdd('dxSBAR_WANTTODELETECATEGORY', @dxSBAR_WANTTODELETECATEGORY);
  InternalAdd('dxSBAR_WANTTOCLEARCOMMANDS', @dxSBAR_WANTTOCLEARCOMMANDS);
  InternalAdd('dxSBAR_RECURSIVEMENUS', @dxSBAR_RECURSIVEMENUS);
  InternalAdd('dxSBAR_COMMANDNAMECANNOTBEBLANK', @dxSBAR_COMMANDNAMECANNOTBEBLANK);
  InternalAdd('dxSBAR_TOOLBAREXISTS', @dxSBAR_TOOLBAREXISTS);
  InternalAdd('dxSBAR_RECURSIVEGROUPS', @dxSBAR_RECURSIVEGROUPS);
  InternalAdd('dxSBAR_WANTTODELETECOMPLEXITEM', @dxSBAR_WANTTODELETECOMPLEXITEM);
  InternalAdd('dxSBAR_CANTPLACEQUICKACCESSGROUPBUTTON', @dxSBAR_CANTPLACEQUICKACCESSGROUPBUTTON);
  InternalAdd('dxSBAR_QUICKACCESSGROUPBUTTONTOOLBARNOTDOCKEDINRIBBON', @dxSBAR_QUICKACCESSGROUPBUTTONTOOLBARNOTDOCKEDINRIBBON);
  InternalAdd('dxSBAR_QUICKACCESSALREADYHASGROUPBUTTON', @dxSBAR_QUICKACCESSALREADYHASGROUPBUTTON);
  InternalAdd('dxSBAR_CANTPLACESEPARATOR', @dxSBAR_CANTPLACESEPARATOR);
  InternalAdd('dxSBAR_CANTPLACERIBBONGALLERY', @dxSBAR_CANTPLACERIBBONGALLERY);
  InternalAdd('dxSBAR_CANTPLACESKINCHOOSERGALLERY', @dxSBAR_CANTPLACESKINCHOOSERGALLERY);
  InternalAdd('dxSBAR_CANTMERGEBARMANAGER', @dxSBAR_CANTMERGEBARMANAGER);
  InternalAdd('dxSBAR_CANTMERGETOOLBAR', @dxSBAR_CANTMERGETOOLBAR);
  InternalAdd('dxSBAR_CANTMERGEWITHMERGEDTOOLBAR', @dxSBAR_CANTMERGEWITHMERGEDTOOLBAR);
  InternalAdd('dxSBAR_CANTUNMERGETOOLBAR', @dxSBAR_CANTUNMERGETOOLBAR);
  InternalAdd('dxSBAR_ONEOFTOOLBARSALREADYMERGED', @dxSBAR_ONEOFTOOLBARSALREADYMERGED);
  InternalAdd('dxSBAR_ONEOFTOOLBARSHASMERGEDTOOLBARS', @dxSBAR_ONEOFTOOLBARSHASMERGEDTOOLBARS);
  InternalAdd('dxSBAR_TOOLBARHASMERGEDTOOLBARS', @dxSBAR_TOOLBARHASMERGEDTOOLBARS);
  InternalAdd('dxSBAR_TOOLBARSALREADYMERGED', @dxSBAR_TOOLBARSALREADYMERGED);
  InternalAdd('dxSBAR_TOOLBARSARENOTMERGED', @dxSBAR_TOOLBARSARENOTMERGED);

  InternalAdd('dxSBAR_RIBBONCANTMERGE', @dxSBAR_RIBBONCANTMERGE);
  InternalAdd('dxSBAR_RIBBONCANTMERGETAB', @dxSBAR_RIBBONCANTMERGETAB);
  InternalAdd('dxSBAR_RIBBONCANTMERGEWITHOUTBARMANAGER', @dxSBAR_RIBBONCANTMERGEWITHOUTBARMANAGER);
  InternalAdd('dxSBAR_RIBBONCANTUNMERGE', @dxSBAR_RIBBONCANTUNMERGE);
  InternalAdd('dxSBAR_RIBBONCANTUNMERGETAB', @dxSBAR_RIBBONCANTUNMERGETAB);
  InternalAdd('dxSBAR_RIBBONONEOFTABGROUPSALREADYMERGED', @dxSBAR_RIBBONONEOFTABGROUPSALREADYMERGED);
  InternalAdd('dxSBAR_RIBBONSARENOTMERGED', @dxSBAR_RIBBONSARENOTMERGED);
  InternalAdd('dxSBAR_RIBBONTABSARENOTMERGED', @dxSBAR_RIBBONTABSARENOTMERGED);
  InternalAdd('dxSBAR_RIBBON_MINIMIZERIBBON', @dxSBAR_RIBBON_MINIMIZERIBBON);
  InternalAdd('dxSBAR_RIBBON_RESTORERIBBON', @dxSBAR_RIBBON_RESTORERIBBON);
  InternalAdd('dxSBAR_RIBBON_PINRIBBON', @dxSBAR_RIBBON_PINRIBBON);
  InternalAdd('dxSBAR_RIBBONFORM_CLOSE', @dxSBAR_RIBBONFORM_CLOSE);
  InternalAdd('dxSBAR_RIBBONFORM_DISPLAYOPTIONS', @dxSBAR_RIBBONFORM_DISPLAYOPTIONS);
  InternalAdd('dxSBAR_RIBBONFORM_HELP', @dxSBAR_RIBBONFORM_HELP);
  InternalAdd('dxSBAR_RIBBONFORM_MAXIMIZE', @dxSBAR_RIBBONFORM_MAXIMIZE);
  InternalAdd('dxSBAR_RIBBONFORM_MINIMIZE', @dxSBAR_RIBBONFORM_MINIMIZE);
  InternalAdd('dxSBAR_RIBBONFORM_RESTOREDOWN', @dxSBAR_RIBBONFORM_RESTOREDOWN);
  InternalAdd('dxSBAR_RIBBONFORM_RESTOREUP', @dxSBAR_RIBBONFORM_RESTOREUP);
  InternalAdd('dxSBAR_RIBBONDISPLAYOPTIONS_AUTOHIDERIBBON_CAPTION', @dxSBAR_RIBBONDISPLAYOPTIONS_AUTOHIDERIBBON_CAPTION);
  InternalAdd('dxSBAR_RIBBONDISPLAYOPTIONS_AUTOHIDERIBBON_DESCRIPTION', @dxSBAR_RIBBONDISPLAYOPTIONS_AUTOHIDERIBBON_DESCRIPTION);
  InternalAdd('dxSBAR_RIBBONDISPLAYOPTIONS_SHOWTABS_CAPTION', @dxSBAR_RIBBONDISPLAYOPTIONS_SHOWTABS_CAPTION);
  InternalAdd('dxSBAR_RIBBONDISPLAYOPTIONS_SHOWTABS_DESCRIPTION', @dxSBAR_RIBBONDISPLAYOPTIONS_SHOWTABS_DESCRIPTION);
  InternalAdd('dxSBAR_RIBBONDISPLAYOPTIONS_SHOWTABSANDCOMMANDS_CAPTION', @dxSBAR_RIBBONDISPLAYOPTIONS_SHOWTABSANDCOMMANDS_CAPTION);
  InternalAdd('dxSBAR_RIBBONDISPLAYOPTIONS_SHOWTABSANDCOMMANDS_DESCRIPTION', @dxSBAR_RIBBONDISPLAYOPTIONS_SHOWTABSANDCOMMANDS_DESCRIPTION);
  
  InternalAdd('dxSBAR_DEFAULTCATEGORYNAME', @dxSBAR_DEFAULTCATEGORYNAME);
  InternalAdd('dxSBAR_NEWBUTTONCAPTION', @dxSBAR_NEWBUTTONCAPTION);
  InternalAdd('dxSBAR_NEWITEMCAPTION', @dxSBAR_NEWITEMCAPTION);
  InternalAdd('dxSBAR_NEWRIBBONGALLERYITEMCAPTION', @dxSBAR_NEWRIBBONGALLERYITEMCAPTION);
  InternalAdd('dxSBAR_NEWSEPARATORCAPTION', @dxSBAR_NEWSEPARATORCAPTION);
  InternalAdd('dxSBAR_NEWSUBITEMCAPTION', @dxSBAR_NEWSUBITEMCAPTION);
  InternalAdd('dxSBAR_CP_ADDSUBITEM', @dxSBAR_CP_ADDSUBITEM);
  InternalAdd('dxSBAR_CP_ADDBUTTON', @dxSBAR_CP_ADDBUTTON);
  InternalAdd('dxSBAR_CP_ADDLARGEBUTTON', @dxSBAR_CP_ADDLARGEBUTTON);
  InternalAdd('dxSBAR_CP_ADDSEPARATOR', @dxSBAR_CP_ADDSEPARATOR);
  InternalAdd('dxSBAR_CP_ADDDXITEM', @dxSBAR_CP_ADDDXITEM);
  InternalAdd('dxSBAR_CP_ADDCXITEM', @dxSBAR_CP_ADDCXITEM);
  InternalAdd('dxSBAR_CP_ADDGROUPBUTTON', @dxSBAR_CP_ADDGROUPBUTTON);
  InternalAdd('dxSBAR_CP_DELETEITEM', @dxSBAR_CP_DELETEITEM);
  InternalAdd('dxSBAR_CP_DELETELINK', @dxSBAR_CP_DELETELINK);
  InternalAdd('dxSBAR_CP_RESET', @dxSBAR_CP_RESET);
  InternalAdd('dxSBAR_CP_DELETE', @dxSBAR_CP_DELETE);
  InternalAdd('dxSBAR_CP_NAME', @dxSBAR_CP_NAME);
  InternalAdd('dxSBAR_CP_CAPTION', @dxSBAR_CP_CAPTION);
  InternalAdd('dxSBAR_CP_BUTTONPAINTSTYLEMENU', @dxSBAR_CP_BUTTONPAINTSTYLEMENU);
  InternalAdd('dxSBAR_CP_DEFAULTSTYLE', @dxSBAR_CP_DEFAULTSTYLE);
  InternalAdd('dxSBAR_CP_TEXTONLYALWAYS', @dxSBAR_CP_TEXTONLYALWAYS);
  InternalAdd('dxSBAR_CP_TEXTONLYINMENUS', @dxSBAR_CP_TEXTONLYINMENUS);
  InternalAdd('dxSBAR_CP_IMAGEANDTEXT', @dxSBAR_CP_IMAGEANDTEXT);
  InternalAdd('dxSBAR_CP_BEGINAGROUP', @dxSBAR_CP_BEGINAGROUP);
  InternalAdd('dxSBAR_CP_VISIBLE', @dxSBAR_CP_VISIBLE);
  InternalAdd('dxSBAR_CP_MOSTRECENTLYUSED', @dxSBAR_CP_MOSTRECENTLYUSED);
  InternalAdd('dxSBAR_CP_DISTRIBUTED', @dxSBAR_CP_DISTRIBUTED);
  InternalAdd('dxSBAR_CP_POSITIONMENU', @dxSBAR_CP_POSITIONMENU);
  InternalAdd('dxSBAR_CP_VIEWLEVELSMENU', @dxSBAR_CP_VIEWLEVELSMENU);
  InternalAdd('dxSBAR_CP_ALLVIEWLEVELS', @dxSBAR_CP_ALLVIEWLEVELS);
  InternalAdd('dxSBAR_CP_SINGLEVIEWLEVELITEMSUFFIX', @dxSBAR_CP_SINGLEVIEWLEVELITEMSUFFIX);
  InternalAdd('dxSBAR_CP_BUTTONGROUPMENU', @dxSBAR_CP_BUTTONGROUPMENU);
  InternalAdd('dxSBAR_CP_BUTTONGROUP', @dxSBAR_CP_BUTTONGROUP);
  InternalAdd('dxSBAR_CP_BUTTONUNGROUP', @dxSBAR_CP_BUTTONUNGROUP);
  InternalAdd('dxSBAR_ADDEX', @dxSBAR_ADDEX);
  InternalAdd('dxSBAR_RENAMEEX', @dxSBAR_RENAMEEX);
  InternalAdd('dxSBAR_DELETE', @dxSBAR_DELETE);
  InternalAdd('dxSBAR_CLEAR', @dxSBAR_CLEAR);
  InternalAdd('dxSBAR_VISIBLE', @dxSBAR_VISIBLE);
  InternalAdd('dxSBAR_OK', @dxSBAR_OK);
  InternalAdd('dxSBAR_CANCEL', @dxSBAR_CANCEL);
  InternalAdd('dxSBAR_SUBMENUEDITOR', @dxSBAR_SUBMENUEDITOR);
  InternalAdd('dxSBAR_SUBMENUEDITORCAPTION', @dxSBAR_SUBMENUEDITORCAPTION);
  InternalAdd('dxSBAR_INSERTEX', @dxSBAR_INSERTEX);
  InternalAdd('dxSBAR_MOVEUP', @dxSBAR_MOVEUP);
  InternalAdd('dxSBAR_MOVEDOWN', @dxSBAR_MOVEDOWN);
  InternalAdd('dxSBAR_POPUPMENUEDITOR', @dxSBAR_POPUPMENUEDITOR);
  InternalAdd('dxSBAR_TABSHEET1', @dxSBAR_TABSHEET1);
  InternalAdd('dxSBAR_TABSHEET2', @dxSBAR_TABSHEET2);
  InternalAdd('dxSBAR_TABSHEET3', @dxSBAR_TABSHEET3);
  InternalAdd('dxSBAR_TOOLBARS', @dxSBAR_TOOLBARS);
  InternalAdd('dxSBAR_TNEW', @dxSBAR_TNEW);
  InternalAdd('dxSBAR_TRENAME', @dxSBAR_TRENAME);
  InternalAdd('dxSBAR_TDELETE', @dxSBAR_TDELETE);
  InternalAdd('dxSBAR_TRESET', @dxSBAR_TRESET);
  InternalAdd('dxSBAR_CLOSE', @dxSBAR_CLOSE);
  InternalAdd('dxSBAR_CAPTION', @dxSBAR_CAPTION);
  InternalAdd('dxSBAR_CATEGORIES', @dxSBAR_CATEGORIES);
  InternalAdd('dxSBAR_COMMANDS', @dxSBAR_COMMANDS);
  InternalAdd('dxSBAR_DESCRIPTION', @dxSBAR_DESCRIPTION);
  InternalAdd('dxSBAR_MDIMINIMIZE', @dxSBAR_MDIMINIMIZE);
  InternalAdd('dxSBAR_MDIRESTORE', @dxSBAR_MDIRESTORE);
  InternalAdd('dxSBAR_MDICLOSE', @dxSBAR_MDICLOSE);
  InternalAdd('dxSBAR_CUSTOMIZE', @dxSBAR_CUSTOMIZE);
  InternalAdd('dxSBAR_ADDREMOVEBUTTONS', @dxSBAR_ADDREMOVEBUTTONS);
  InternalAdd('dxSBAR_MOREBUTTONS', @dxSBAR_MOREBUTTONS);
  InternalAdd('dxSBAR_RESETTOOLBAR', @dxSBAR_RESETTOOLBAR);
  InternalAdd('dxSBAR_EXPAND', @dxSBAR_EXPAND);
  InternalAdd('dxSBAR_DRAGTOMAKEMENUFLOAT', @dxSBAR_DRAGTOMAKEMENUFLOAT);
  InternalAdd('dxSBAR_MORECOMMANDS', @dxSBAR_MORECOMMANDS);
  InternalAdd('dxSBAR_SHOWBELOWRIBBON', @dxSBAR_SHOWBELOWRIBBON);
  InternalAdd('dxSBAR_SHOWABOVERIBBON', @dxSBAR_SHOWABOVERIBBON);
  InternalAdd('dxSBAR_MINIMIZERIBBON', @dxSBAR_MINIMIZERIBBON);
  InternalAdd('dxSBAR_CUSTOMIZERIBBON', @dxSBAR_CUSTOMIZERIBBON);
  InternalAdd('dxSBAR_CUSTOMIZERIBBONQAT', @dxSBAR_CUSTOMIZERIBBONQAT);
  InternalAdd('dxSBAR_ADDTOQAT', @dxSBAR_ADDTOQAT);
  InternalAdd('dxSBAR_ADDTOQATITEMNAME', @dxSBAR_ADDTOQATITEMNAME);
  InternalAdd('dxSBAR_REMOVEFROMQAT', @dxSBAR_REMOVEFROMQAT);
  InternalAdd('dxSBAR_CUSTOMIZEQAT', @dxSBAR_CUSTOMIZEQAT);
  InternalAdd('dxSBAR_ADDGALLERYNAME', @dxSBAR_ADDGALLERYNAME);
  InternalAdd('dxSBAR_SHOWALLGALLERYGROUPS', @dxSBAR_SHOWALLGALLERYGROUPS);
  InternalAdd('dxSBAR_HIDEALLGALLERYGROUPS', @dxSBAR_HIDEALLGALLERYGROUPS);
  InternalAdd('dxSBAR_CLEARGALLERYFILTER', @dxSBAR_CLEARGALLERYFILTER);
  InternalAdd('dxSBAR_GALLERYEMPTYFILTERCAPTION', @dxSBAR_GALLERYEMPTYFILTERCAPTION);
  InternalAdd('dxSBAR_PIN', @dxSBAR_PIN);
  InternalAdd('dxSBAR_UNPIN', @dxSBAR_UNPIN);
  InternalAdd('dxSBAR_TOOLBARNEWNAME', @dxSBAR_TOOLBARNEWNAME);
  InternalAdd('dxSBAR_CATEGORYADD', @dxSBAR_CATEGORYADD);
  InternalAdd('dxSBAR_CATEGORYINSERT', @dxSBAR_CATEGORYINSERT);
  InternalAdd('dxSBAR_CATEGORYRENAME', @dxSBAR_CATEGORYRENAME);
  InternalAdd('dxSBAR_TOOLBARADD', @dxSBAR_TOOLBARADD);
  InternalAdd('dxSBAR_TOOLBARRENAME', @dxSBAR_TOOLBARRENAME);
  InternalAdd('dxSBAR_CATEGORYNAME', @dxSBAR_CATEGORYNAME);
  InternalAdd('dxSBAR_TOOLBARNAME', @dxSBAR_TOOLBARNAME);
  InternalAdd('dxSBAR_CUSTOMIZINGFORM', @dxSBAR_CUSTOMIZINGFORM);
  InternalAdd('dxSBAR_MODIFY', @dxSBAR_MODIFY);
  InternalAdd('dxSBAR_PERSMENUSANDTOOLBARS', @dxSBAR_PERSMENUSANDTOOLBARS);
  InternalAdd('dxSBAR_MENUSSHOWRECENTITEMS', @dxSBAR_MENUSSHOWRECENTITEMS);
  InternalAdd('dxSBAR_SHOWFULLMENUSAFTERDELAY', @dxSBAR_SHOWFULLMENUSAFTERDELAY);
  InternalAdd('dxSBAR_RESETUSAGEDATA', @dxSBAR_RESETUSAGEDATA);
  InternalAdd('dxSBAR_OTHEROPTIONS', @dxSBAR_OTHEROPTIONS);
  InternalAdd('dxSBAR_LARGEICONS', @dxSBAR_LARGEICONS);
  InternalAdd('dxSBAR_HINTOPT1', @dxSBAR_HINTOPT1);
  InternalAdd('dxSBAR_HINTOPT2', @dxSBAR_HINTOPT2);
  InternalAdd('dxSBAR_MENUANIMATIONS', @dxSBAR_MENUANIMATIONS);
  InternalAdd('dxSBAR_MENUANIM1', @dxSBAR_MENUANIM1);
  InternalAdd('dxSBAR_MENUANIM2', @dxSBAR_MENUANIM2);
  InternalAdd('dxSBAR_MENUANIM3', @dxSBAR_MENUANIM3);
  InternalAdd('dxSBAR_MENUANIM4', @dxSBAR_MENUANIM4);
  InternalAdd('dxSBAR_MENUANIM5', @dxSBAR_MENUANIM5);
  InternalAdd('dxSBAR_CANTFINDBARMANAGERFORSTATUSBAR', @dxSBAR_CANTFINDBARMANAGERFORSTATUSBAR);
  InternalAdd('dxSBAR_BUTTONDEFAULTACTIONDESCRIPTION', @dxSBAR_BUTTONDEFAULTACTIONDESCRIPTION);
  InternalAdd('SBlob', @SBlob);
  InternalAdd('dxSBAR_EXTRAPANEHEADER', @dxSBAR_EXTRAPANEHEADER);
  InternalAdd('dxSBAR_APPMENUOUTSIDERIBBON', @dxSBAR_APPMENUOUTSIDERIBBON);
  InternalAdd('dxSBAR_GDIPLUSNEEDED', @dxSBAR_GDIPLUSNEEDED);
  InternalAdd('dxSBAR_RIBBONMORETHANONE', @dxSBAR_RIBBONMORETHANONE);
  InternalAdd('dxSBAR_RIBBONBADOWNER', @dxSBAR_RIBBONBADOWNER);
  InternalAdd('dxSBAR_RIBBONBADPARENT', @dxSBAR_RIBBONBADPARENT);
  InternalAdd('dxSBAR_RIBBONADDTAB', @dxSBAR_RIBBONADDTAB);
  InternalAdd('dxSBAR_RIBBONDELETETAB', @dxSBAR_RIBBONDELETETAB);
  InternalAdd('dxSBAR_RIBBONADDEMPTYGROUP', @dxSBAR_RIBBONADDEMPTYGROUP);
  InternalAdd('dxSBAR_RIBBONADDGROUPWITHTOOLBAR', @dxSBAR_RIBBONADDGROUPWITHTOOLBAR);
  InternalAdd('dxSBAR_RIBBONDELETEGROUP', @dxSBAR_RIBBONDELETEGROUP);
  InternalAdd('dxSBAR_ACCESSIBILITY_RIBBONNAME', @dxSBAR_ACCESSIBILITY_RIBBONNAME);
  InternalAdd('dxSBAR_ACCESSIBILITY_RIBBONTABCOLLECTIONNAME', @dxSBAR_ACCESSIBILITY_RIBBONTABCOLLECTIONNAME);

  InternalAdd('sdxRibbonCustomizationFormAddErrorMsg', @sdxRibbonCustomizationFormAddErrorMsg);
  InternalAdd('sdxRibbonCustomizationFormAllCommands', @sdxRibbonCustomizationFormAllCommands);
  InternalAdd('sdxRibbonCustomizationFormAllTabs', @sdxRibbonCustomizationFormAllTabs);
  InternalAdd('sdxRibbonCustomizationFormBeginGroup', @sdxRibbonCustomizationFormBeginGroup);
  InternalAdd('sdxRibbonCustomizationFormCommandsNotInTheRibbon', @sdxRibbonCustomizationFormCommandsNotInTheRibbon);
  InternalAdd('sdxRibbonCustomizationFormCustomElementSuffix', @sdxRibbonCustomizationFormCustomElementSuffix);
  InternalAdd('sdxRibbonCustomizationFormCustomGroups', @sdxRibbonCustomizationFormCustomGroups);
  InternalAdd('sdxRibbonCustomizationFormCustomTabsAndGroups', @sdxRibbonCustomizationFormCustomTabsAndGroups);
  InternalAdd('sdxRibbonCustomizationFormDelimiterContextTab', @sdxRibbonCustomizationFormDelimiterContextTab);
  InternalAdd('sdxRibbonCustomizationFormDisplayName', @sdxRibbonCustomizationFormDisplayName);
  InternalAdd('sdxRibbonCustomizationFormMainTabs', @sdxRibbonCustomizationFormMainTabs);
  InternalAdd('sdxRibbonCustomizationFormNewContext', @sdxRibbonCustomizationFormNewContext);
  InternalAdd('sdxRibbonCustomizationFormNewGroup', @sdxRibbonCustomizationFormNewGroup);
  InternalAdd('sdxRibbonCustomizationFormNewTab', @sdxRibbonCustomizationFormNewTab);
  InternalAdd('sdxRibbonCustomizationFormRename', @sdxRibbonCustomizationFormRename);
  InternalAdd('sdxRibbonCustomizationFormTabSuffix', @sdxRibbonCustomizationFormTabSuffix);
  InternalAdd('sdxRibbonCustomizationFormToolTabs', @sdxRibbonCustomizationFormToolTabs);

  InternalAdd('sdxRibbonCustomizationFormCaptionAdd', @sdxRibbonCustomizationFormCaptionAdd);
  InternalAdd('sdxRibbonCustomizationFormCaptionAddNewContext', @sdxRibbonCustomizationFormCaptionAddNewContext);
  InternalAdd('sdxRibbonCustomizationFormCaptionAddNewGroup', @sdxRibbonCustomizationFormCaptionAddNewGroup);
  InternalAdd('sdxRibbonCustomizationFormCaptionAddNewTab', @sdxRibbonCustomizationFormCaptionAddNewTab);
  InternalAdd('sdxRibbonCustomizationFormCaptionCancel', @sdxRibbonCustomizationFormCaptionCancel);
  InternalAdd('sdxRibbonCustomizationFormCaptionCommandsSource', @sdxRibbonCustomizationFormCaptionCommandsSource);
  InternalAdd('sdxRibbonCustomizationFormCaptionMoveDown', @sdxRibbonCustomizationFormCaptionMoveDown);
  InternalAdd('sdxRibbonCustomizationFormCaptionMoveUp', @sdxRibbonCustomizationFormCaptionMoveUp);
  InternalAdd('sdxRibbonCustomizationFormCaptionNewElement', @sdxRibbonCustomizationFormCaptionNewElement);
  InternalAdd('sdxRibbonCustomizationFormCaptionOK', @sdxRibbonCustomizationFormCaptionOK);
  InternalAdd('sdxRibbonCustomizationFormCaptionQuickAccessToolbar', @sdxRibbonCustomizationFormCaptionQuickAccessToolbar);
  InternalAdd('sdxRibbonCustomizationFormCaptionQuickAccessToolbarShowBelowRibbon', @sdxRibbonCustomizationFormCaptionQuickAccessToolbarShowBelowRibbon);
  InternalAdd('sdxRibbonCustomizationFormCaptionQuickAccessToolbarTitle', @sdxRibbonCustomizationFormCaptionQuickAccessToolbarTitle);
  InternalAdd('sdxRibbonCustomizationFormCaptionRemove', @sdxRibbonCustomizationFormCaptionRemove);
  InternalAdd('sdxRibbonCustomizationFormCaptionRename', @sdxRibbonCustomizationFormCaptionRename);
  InternalAdd('sdxRibbonCustomizationFormCaptionReset', @sdxRibbonCustomizationFormCaptionReset);
  InternalAdd('sdxRibbonCustomizationFormCaptionResetAllCustomizations', @sdxRibbonCustomizationFormCaptionResetAllCustomizations);
  InternalAdd('sdxRibbonCustomizationFormCaptionResetOnlySelectedTab', @sdxRibbonCustomizationFormCaptionResetOnlySelectedTab);
  InternalAdd('sdxRibbonCustomizationFormCaptionResetSelectedTab', @sdxRibbonCustomizationFormCaptionResetSelectedTab);
  InternalAdd('sdxRibbonCustomizationFormCaptionRibbonTitle', @sdxRibbonCustomizationFormCaptionRibbonTitle);
  InternalAdd('sdxRibbonCustomizationFormCaptionRibbonSource', @sdxRibbonCustomizationFormCaptionRibbonSource);
  InternalAdd('sdxRibbonCustomizationFormCaptionShowTab', @sdxRibbonCustomizationFormCaptionShowTab);
end;

initialization
  dxResourceStringsRepository.RegisterProduct('ExpressBars', @AddBarsResourceStringNames);

finalization
  dxResourceStringsRepository.UnRegisterProduct('ExpressBars');

end.
