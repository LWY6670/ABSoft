{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressBars DB Navigator string table constants          }
{                                                                    }
{           Copyright (c) 1998-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSBARS AND ALL ACCOMPANYING VCL  }
{   CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY.                  }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxBarDBNavStrs;

{$I cxVer.inc}

interface

resourcestring
  dxSBAR_DBNAVERROR1 = '已经有一个相同的数据导航按钮了';

  dxSBAR_DBNAVIGATORCATEGORYNAME = '数据导航器';
  dxSBAR_DELETERECORD = '是否要删除当前记录？';

  dxSBAR_BTNCAPTION_FIRST = '第一条';
  dxSBAR_BTNCAPTION_PRIOR = '上一条';
  dxSBAR_BTNCAPTION_NEXT = '下一条';
  dxSBAR_BTNCAPTION_LAST = '最后一条';
  dxSBAR_BTNCAPTION_INSERT = '插入';
  dxSBAR_BTNCAPTION_DELETE = '删除';
  dxSBAR_BTNCAPTION_EDIT = '修改';
  dxSBAR_BTNCAPTION_POST = '提交';
  dxSBAR_BTNCAPTION_CANCEL = '取消';
  dxSBAR_BTNCAPTION_REFRESH = '刷新';
implementation

uses
  dxCore;

procedure AddBarsDBNavigatorResourceStringNames(AProduct: TdxProductResourceStrings);

  procedure InternalAdd(const AResourceStringName: string; AAdress: Pointer);
  begin
    AProduct.Add(AResourceStringName, AAdress);
  end;

begin
  InternalAdd('dxSBAR_DBNAVERROR1', @dxSBAR_DBNAVERROR1);
  InternalAdd('dxSBAR_DBNAVIGATORCATEGORYNAME', @dxSBAR_DBNAVIGATORCATEGORYNAME);
  InternalAdd('dxSBAR_DELETERECORD', @dxSBAR_DELETERECORD);
  InternalAdd('dxSBAR_BTNCAPTION_FIRST', @dxSBAR_BTNCAPTION_FIRST);
  InternalAdd('dxSBAR_BTNCAPTION_PRIOR', @dxSBAR_BTNCAPTION_PRIOR);
  InternalAdd('dxSBAR_BTNCAPTION_NEXT', @dxSBAR_BTNCAPTION_NEXT);
  InternalAdd('dxSBAR_BTNCAPTION_LAST', @dxSBAR_BTNCAPTION_LAST);
  InternalAdd('dxSBAR_BTNCAPTION_INSERT', @dxSBAR_BTNCAPTION_INSERT);
  InternalAdd('dxSBAR_BTNCAPTION_DELETE', @dxSBAR_BTNCAPTION_DELETE);
  InternalAdd('dxSBAR_BTNCAPTION_EDIT', @dxSBAR_BTNCAPTION_EDIT);
  InternalAdd('dxSBAR_BTNCAPTION_POST', @dxSBAR_BTNCAPTION_POST);
  InternalAdd('dxSBAR_BTNCAPTION_CANCEL', @dxSBAR_BTNCAPTION_CANCEL);
  InternalAdd('dxSBAR_BTNCAPTION_REFRESH', @dxSBAR_BTNCAPTION_REFRESH);
end;

initialization
  dxResourceStringsRepository.RegisterProduct('ExpressBars DB Navigator', @AddBarsDBNavigatorResourceStringNames);

finalization
  dxResourceStringsRepository.UnRegisterProduct('ExpressBars DB Navigator');

end.
