{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           Express Cross Platform Library classes                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxIconLibraryEditor;

{$I cxVer.inc}

interface

uses
  Types, Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, Dialogs, cxGraphics, cxControls,
  cxContainer, cxEdit, Menus, ExtCtrls, StdCtrls, cxButtons, cxImage, cxListBox, cxLabel, cxTextEdit, cxMaskEdit,
  cxButtonEdit, dxGalleryControl, cxCheckListBox, ExtDlgs, dxGallery, cxClasses, cxGeometry, dxGDIPlusClasses,
  dxIconLibraryEditorHelpers, ComCtrls, dxCore, dxIconLibrary, ImgList, cxLookAndFeels, cxLookAndFeelPainters, cxPC,
  ActnList;

type
  TdxPopulateGalleryMode = (gpmGroups, gpmSearch);

  TdxfmImagePicker = class;

  { TdxImagePickerFormHelper }

  TdxImagePickerFormHelper = class(TInterfacedObject, IdxAdvancedPictureEditor, IdxImageCollectionEditor)
  private
    FImagePicker: TdxfmImagePicker;
  protected
    function ExecuteImageCollectionEditor(AFiles: TStrings; const ASuggestedImageSize: TSize): Boolean; virtual;
    function ExecuteAdvancedPictureEditor(APicture: TPicture; AGraphicClass: TGraphicClass; const ABuildFilter: string; AImportList: TStrings): Boolean; virtual;
    function GetImagePicker: TdxfmImagePicker;

    //IdxImageCollection
    function IdxImageCollectionEditor.Execute =  ExecuteImageCollectionEditor;
    //IdxAdvancedPictureEditor
    function IdxAdvancedPictureEditor.Execute = ExecuteAdvancedPictureEditor;

    property ImagePicker: TdxfmImagePicker read GetImagePicker;
  public
    destructor Destroy; override;
  end;

  { TdxCustomPopulateHelper }

  TdxCustomPopulateHelper = class
  private
    FImagePicker: TdxfmImagePicker;

    function GetCategoriesList: TcxCheckListBox;
    function GetCollectionList: TcxCheckListBox;
    function GetFindEditor: TcxButtonEdit;
    function GetGallery: TdxGalleryControl;
    function GetGalleryGroupHidden: TdxGalleryControlGroup;
    function GetGalleryGroupSearch: TdxGalleryControlGroup;
    function GetIconLibrary: TdxIconLibrary;
    function GetPopulateGalleryMode: TdxPopulateGalleryMode;
    function GetSizeList: TcxCheckListBox;
    function IsListBoxCheckedByText(ACheckListBox: TcxCheckListBox; const AText: string): Boolean;
    procedure SetGalleryGroupHidden(AValue: TdxGalleryControlGroup);
    procedure SetGalleryGroupSearch(AValue: TdxGalleryControlGroup);
  protected
    property CategoriesList: TcxCheckListBox read GetCategoriesList;
    property CollectionList: TcxCheckListBox read GetCollectionList;
    property FindEditor: TcxButtonEdit read GetFindEditor;
    property Gallery: TdxGalleryControl read GetGallery;
    property GalleryGroupHidden: TdxGalleryControlGroup read GetGalleryGroupHidden write SetGalleryGroupHidden;
    property GalleryGroupSearch: TdxGalleryControlGroup read GetGalleryGroupSearch write SetGalleryGroupSearch;
    property IconLibrary: TdxIconLibrary read GetIconLibrary;
    property PopulateGalleryMode: TdxPopulateGalleryMode read GetPopulateGalleryMode;
    property SizeList: TcxCheckListBox read GetSizeList;
  public
    constructor Create(AImagePicker: TdxfmImagePicker); virtual;

    procedure Populate; virtual; abstract;
  end;

  { TdxPopulateContentHelper }

  TdxPopulateContentHelper = class(TdxCustomPopulateHelper)
  private
    function AddCheckListBoxItem(ACheckListBox: TcxCheckListBox; const AText: string; AChecked: Boolean): Boolean;
    function AddGalleryGroup(const ACaption: string; AIndex: Integer; AVisible: Boolean): TdxGalleryControlGroup;
    procedure AddGalleryGroups(ACollectionItem: TdxIconLibraryCollection);
    procedure AddGalleryItem(AImageItem: TdxIconLibraryImage);
    procedure AddGalleryItems(ACategoriesItem: TdxIconLibraryCategories);
    function GetIndexForGalleryGroup(const ANameItem: string): Integer;
  public
    procedure Populate; override;
  end;

  {TdxPopulateGalleryHelper}

  TdxPopulateGalleryHelper = class(TdxCustomPopulateHelper)
  private
    FCurrentGalleryGroup: TdxGalleryControlGroup;
    FVisibleCurrentCollection: Boolean;
    FMaxSize: TSize;

    procedure DestroyGalleryItems;
    function GetGalleryItem(AIconLibraryImage: TdxIconLibraryImage): TdxGalleryControlItem;
    function GetIndexForItem(AGroup: TdxGalleryControlGroup; const ANameItem: string): Integer;
    function IsImageVisible(AImageItem: TdxIconLibraryImage): Boolean;
    procedure PopulateGalleryGroups(ACollectionItem: TdxIconLibraryCollection);
    procedure PopulateGalleryImages(ACategoriesItem: TdxIconLibraryCategories);
  public
    destructor Destroy; Override;

    procedure Populate; override;
  end;

  { TdxfmImagePicker }

  TdxfmImagePicker = class(TForm)
    actF3: TAction;
    ActionList1: TActionList;
    beFind: TcxButtonEdit;
    btnCancel: TcxButton;
    btnClear: TcxButton;
    btnLoad: TcxButton;
    btnOk: TcxButton;
    btnSave: TcxButton;
    clbCategories: TcxCheckListBox;
    clbCollection: TcxCheckListBox;
    clbSize: TcxCheckListBox;
    cxEditStyleController1: TcxEditStyleController;
    cxImageList1: TcxImageList;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    cxLabel3: TcxLabel;
    cxPageControl1: TcxPageControl;
    gcIcons: TdxGalleryControl;
    ImagePaintBox: TPaintBox;
    miCheckSelected: TMenuItem;
    miLine1: TMenuItem;
    miSelectAll: TMenuItem;
    miSelectAllinThisGroup: TMenuItem;
    miSelectNone: TMenuItem;
    miUncheckSelected: TMenuItem;
    miUnselectAllinThisGroup: TMenuItem;
    OpenDialog: TOpenPictureDialog;
    Panel1: TPanel;
    pmSelection: TPopupMenu;
    SaveDialog: TSavePictureDialog;
    tsDXImageGallery: TcxTabSheet;
    tsPictureEditor: TcxTabSheet;
    btnImport: TcxButton;
    pmImport: TPopupMenu;
    procedure beFindPropertiesButtonClick(Sender: TObject; AButtonIndex: Integer);
    procedure beFindPropertiesChange(Sender: TObject);
    procedure btnClearClick(Sender: TObject);
    procedure btnLoadClick(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure clbCategoriesClickCheck(Sender: TObject; AIndex: Integer; APrevState, ANewState: TcxCheckBoxState);
    procedure clbCollectionClickCheck(Sender: TObject; AIndex: Integer; APrevState, ANewState: TcxCheckBoxState);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure gcIconsDblClick(Sender: TObject);
    procedure ImagePaintBoxPaint(Sender: TObject);
    procedure miSelectAllinThisGroupClick(Sender: TObject);
    procedure miSelectClick(Sender: TObject);
    procedure miUncheckSelectedClick(Sender: TObject);
    procedure pmSelectionPopup(Sender: TObject);
    procedure actF3Execute(Sender: TObject);
    procedure FormShortCut(var Msg: TWMKey; var Handled: Boolean);
    procedure btnImportClick(Sender: TObject);
    procedure cxPageControl1Change(Sender: TObject);
  private
    FGalleryGroupHidden: TdxGalleryControlGroup;
    FGalleryGroupSearch: TdxGalleryControlGroup;
    FIconLibrary: TdxIconLibrary;
    FImportList: TStrings;
    FPicture: TPicture;
    FPopulateGalleryHelper: TdxPopulateGalleryHelper;
    FPopulateGalleryLockCount: Integer;
    FPopulateGalleryMode: TdxPopulateGalleryMode;
    FSelectedGroup: TdxGalleryControlGroup;

    procedure DrawPicture(ACanvas: TCanvas; R: TRect);
    procedure PopulateContent;
    procedure PopulateGallery;
    procedure SetImportList(AValue: TStrings);
    procedure UpdateGalleryItemsSelection(ASelect: Boolean);
    procedure ImportPictureClick(Sender: TObject);
    procedure PictureChangeHandler(Sender: TObject);
  protected
    function ExecuteImageCollectionEditor(AFiles: TStrings; const ASuggestedImageSize: TSize): Boolean; virtual;
    function ExecuteAdvancedPictureEditor(APicture: TPicture; AGraphicClass: TGraphicClass; const ABuildFilter: string; AImportList: TStrings): Boolean; virtual;
    function GetGalleryItem(AIconLibraryImage: TdxIconLibraryImage): TdxGalleryControlItem;
    function GetIconLibraryImage(AGalleryControlItem: TdxGalleryControlItem): TdxIconLibraryImage;

    property ImportList: TStrings read FImportList write SetImportList;
    property IconLibrary: TdxIconLibrary read FIconLibrary;
    property PopulateGalleryMode: TdxPopulateGalleryMode read FPopulateGalleryMode;
    property GalleryGroupSearch: TdxGalleryControlGroup read FGalleryGroupSearch write FGalleryGroupSearch;
    property GalleryGroupHidden: TdxGalleryControlGroup read FGalleryGroupHidden write FGalleryGroupHidden;
  end;

procedure dxExecuteImagePicker(APicture: TPicture; AImportList: TStrings = nil);

implementation

{$R *.dfm}

uses Math;

const
  dxIconLibraryRelativePath = '\ExpressLibrary\Sources\Icon Library\';

function GetdxIconLibraryPath: string;
begin
  Result := GetEnvironmentVariable('DXVCL') + dxIconLibraryRelativePath;
end;

procedure dxExecuteImagePicker(APicture: TPicture; AImportList: TStrings = nil);
var
  ADialog: TdxfmImagePicker;
begin
  ADialog := TdxfmImagePicker.Create(nil);
  try
    ADialog.ExecuteAdvancedPictureEditor(APicture, TGraphic, '', AImportList);
  finally
    ADialog.Free;
  end;
end;

{ TdxImagePickerFormHelper }

destructor TdxImagePickerFormHelper.Destroy;
begin
  FreeAndNil(FImagePicker);
  inherited Destroy;
end;

function TdxImagePickerFormHelper.ExecuteImageCollectionEditor(AFiles: TStrings; const ASuggestedImageSize: TSize): Boolean;
begin
  Result := ImagePicker.ExecuteImageCollectionEditor(AFiles, ASuggestedImageSize);
end;

function TdxImagePickerFormHelper.ExecuteAdvancedPictureEditor(APicture: TPicture;
  AGraphicClass: TGraphicClass; const ABuildFilter: string; AImportList: TStrings): Boolean;
begin
  Result := ImagePicker.ExecuteAdvancedPictureEditor(APicture, AGraphicClass, ABuildFilter, AImportList);
end;

function TdxImagePickerFormHelper.GetImagePicker: TdxfmImagePicker;
begin
  if FImagePicker = nil then
    FImagePicker := TdxfmImagePicker.Create(nil);
  Result := FImagePicker;
end;

{ TdxCustomPopulateHelper }

constructor TdxCustomPopulateHelper.Create(AImagePicker: TdxfmImagePicker);
begin
  inherited Create;
  FImagePicker := AImagePicker;
end;

function TdxCustomPopulateHelper.GetCategoriesList: TcxCheckListBox;
begin
  Result := FImagePicker.clbCategories;
end;

function TdxCustomPopulateHelper.GetCollectionList: TcxCheckListBox;
begin
  Result := FImagePicker.clbCollection;
end;

function TdxCustomPopulateHelper.GetFindEditor: TcxButtonEdit;
begin
  Result := FImagePicker.beFind;
end;

function TdxCustomPopulateHelper.GetGallery: TdxGalleryControl;
begin
  Result := FImagePicker.gcIcons;
end;

function TdxCustomPopulateHelper.GetGalleryGroupHidden: TdxGalleryControlGroup;
begin
  Result := FImagePicker.GalleryGroupHidden;
end;

function TdxCustomPopulateHelper.GetGalleryGroupSearch: TdxGalleryControlGroup;
begin
  Result := FImagePicker.GalleryGroupSearch;
end;

function TdxCustomPopulateHelper.GetIconLibrary: TdxIconLibrary;
begin
  Result := FImagePicker.IconLibrary;
end;

function TdxCustomPopulateHelper.GetPopulateGalleryMode: TdxPopulateGalleryMode;
begin
  Result := FImagePicker.PopulateGalleryMode;
end;

function TdxCustomPopulateHelper.GetSizeList: TcxCheckListBox;
begin
  Result := FImagePicker.clbSize;
end;

function TdxCustomPopulateHelper.IsListBoxCheckedByText(
  ACheckListBox: TcxCheckListBox; const AText: string): Boolean;
var
  AIndex: Integer;
begin
  AIndex := ACheckListBox.Items.IndexOf(AText);
  Result := (AIndex > -1) and ACheckListBox.Items[AIndex].Checked;
end;

procedure TdxCustomPopulateHelper.SetGalleryGroupHidden(AValue: TdxGalleryControlGroup);
begin
  FImagePicker.GalleryGroupHidden := AValue;
end;

procedure TdxCustomPopulateHelper.SetGalleryGroupSearch(AValue: TdxGalleryControlGroup);
begin
  FImagePicker.GalleryGroupSearch := AValue;
end;

{ TdxPopulateContentHelper }

procedure TdxPopulateContentHelper.Populate;
var
  I: Integer;
begin
  for I := 0 to IconLibrary.Count - 1 do
  begin
    AddCheckListBoxItem(CollectionList, IconLibrary.Items[I].Name, True);
    AddGalleryGroups(IconLibrary.Items[I]);
  end;
  GalleryGroupSearch := AddGalleryGroup('Search Result', Gallery.Gallery.Groups.Count, False);
  GalleryGroupHidden := AddGalleryGroup('Not Visible', Gallery.Gallery.Groups.Count, False);
end;

function TdxPopulateContentHelper.AddCheckListBoxItem(
  ACheckListBox: TcxCheckListBox; const AText: string; AChecked: Boolean): Boolean;

  function IsPresentInCheckListBox: Boolean;
  var
    I: Integer;
  begin
    Result := False;
    for I := 0 to ACheckListBox.Items.Count - 1 do
    begin
      Result := ACheckListBox.Items[I].Text = AText;
      if Result then
        Break;
    end;
  end;

var
  ACheckListBoxItem: TcxCheckListBoxItem;
begin
  Result := not IsPresentInCheckListBox;
  if Result then
  begin
    ACheckListBoxItem := ACheckListBox.Items.Add;
    ACheckListBoxItem.Text := AText;
    ACheckListBoxItem.Checked := AChecked;
  end;
end;

function TdxPopulateContentHelper.AddGalleryGroup(
  const ACaption: string; AIndex: Integer; AVisible: Boolean): TdxGalleryControlGroup;
begin
  Result := Gallery.Gallery.Groups.Add;
  Result.Index := AIndex;
  Result.Visible := AVisible;
  Result.Caption := ACaption;
end;

procedure TdxPopulateContentHelper.AddGalleryGroups(ACollectionItem: TdxIconLibraryCollection);
var
  I: Integer;
  ACategoriesItem: TdxIconLibraryCategories;
begin
  for I := 0 to ACollectionItem.Count - 1 do
  begin
    ACategoriesItem := ACollectionItem.Items[I];
    if AddCheckListBoxItem(CategoriesList, ACategoriesItem.Name, True) then
      AddGalleryGroup(ACategoriesItem.Name, GetIndexForGalleryGroup(ACategoriesItem.Name), True);
    AddGalleryItems(ACategoriesItem);
  end;
end;

procedure TdxPopulateContentHelper.AddGalleryItem(AImageItem: TdxIconLibraryImage);
var
  AGalleryItem: TdxGalleryControlItem;
  ABitmap: TBitmap;
begin
  AddCheckListBoxItem(SizeList, AImageItem.SizeToText, True);
  AGalleryItem := TdxGalleryControlItem.Create(nil);
  AImageItem.Tag := TdxNativeInt(AGalleryItem);
  AGalleryItem.Caption := AImageItem.Name;
  AGalleryItem.Hint := AImageItem.Name;
  AGalleryItem.Tag := TdxNativeInt(AImageItem);
  ABitmap := AImageItem.Image.GetAsBitmap;
  try
    AGalleryItem.Glyph.Assign(ABitmap);
  finally
    ABitmap.Free;
  end;
end;

procedure TdxPopulateContentHelper.AddGalleryItems(ACategoriesItem: TdxIconLibraryCategories);
var
  I: Integer;
begin
  for I := 0 to ACategoriesItem.Count - 1 do
    AddGalleryItem(ACategoriesItem.Items[I]);
end;

function TdxPopulateContentHelper.GetIndexForGalleryGroup(const ANameItem: string): Integer;
begin
  Result := 0;
  while (Result < Gallery.Gallery.Groups.Count) and
    (AnsiCompareStr(ANameItem, Gallery.Gallery.Groups[Result].Caption) > 0) do
      Inc(Result);
end;

{ TdxPopulateGalleryHelper }

destructor TdxPopulateGalleryHelper.Destroy;
begin
  DestroyGalleryItems;
  inherited Destroy;
end;

procedure TdxPopulateGalleryHelper.Populate;
var
  I: Integer;
  AItemCollection: TdxIconLibraryCollection;
begin
  Gallery.BeginUpdate;
  try
    FMaxSize := cxNullSize;
    if PopulateGalleryMode = gpmGroups then
      GalleryGroupSearch.Visible := False;

    for I := 0 to IconLibrary.Count - 1 do
    begin
      AItemCollection := IconLibrary.Items[I];
      FVisibleCurrentCollection := IsListBoxCheckedByText(CollectionList, AItemCollection.Name);
      PopulateGalleryGroups(AItemCollection);
    end;
    Gallery.OptionsView.Item.Image.Size.Size := FMaxSize;
  finally
    Gallery.EndUpdate;
  end;
end;

procedure TdxPopulateGalleryHelper.DestroyGalleryItems;

  procedure DestroyItemsInGroup(ACategoriesItem: TdxIconLibraryCategories);
  var
    I: Integer;
  begin
    for I := 0 to ACategoriesItem.Count - 1 do
      GetGalleryItem(ACategoriesItem.Items[I]).Free;
  end;

  procedure DestroyItemsInCollection(ACollectionItem: TdxIconLibraryCollection);
  var
    I: Integer;
  begin
    for I := 0 to ACollectionItem.Count - 1 do
      DestroyItemsInGroup(ACollectionItem.Items[I]);
  end;

var
  I : Integer;
begin
  for I := 0 to IconLibrary.Count - 1 do
    DestroyItemsInCollection(IconLibrary.Items[I]);
end;

function TdxPopulateGalleryHelper.GetGalleryItem(
  AIconLibraryImage: TdxIconLibraryImage): TdxGalleryControlItem;
begin
  Result := FImagePicker.GetGalleryItem(AIconLibraryImage);
end;

function TdxPopulateGalleryHelper.GetIndexForItem(
  AGroup: TdxGalleryControlGroup; const ANameItem: string): Integer;
begin
  Result := 0;
  while (Result < AGroup.ItemCount) and (AnsiCompareStr(ANameItem, AGroup.Items[Result].Caption) > 0) do
    Inc(Result);
end;

function TdxPopulateGalleryHelper.IsImageVisible(AImageItem: TdxIconLibraryImage): Boolean;
begin
  Result := FVisibleCurrentCollection and IsListBoxCheckedByText(SizeList, AImageItem.SizeToText) and
    ((FindEditor.Text = '') or (Pos(AnsiLowerCase(FindEditor.Text), AnsiLowerCase(AImageItem.Name)) > 0));
end;

procedure TdxPopulateGalleryHelper.PopulateGalleryGroups(ACollectionItem: TdxIconLibraryCollection);
var
  I: Integer;
  ACategoriesItem: TdxIconLibraryCategories;
begin
  for I := 0 to ACollectionItem.Count - 1 do
  begin
    ACategoriesItem := ACollectionItem.Items[I];
    FCurrentGalleryGroup := Gallery.Gallery.Groups[CategoriesList.Items.IndexOf(ACategoriesItem.Name)];
    if PopulateGalleryMode = gpmSearch then
    begin
      FCurrentGalleryGroup.Visible := False;
      FCurrentGalleryGroup := GalleryGroupSearch;
    end;
    PopulateGalleryImages(ACategoriesItem);
    FCurrentGalleryGroup.Visible := (PopulateGalleryMode = gpmSearch) or
      ((FCurrentGalleryGroup.ItemCount > 0) and IsListBoxCheckedByText(CategoriesList, ACategoriesItem.Name));
  end;
end;

procedure TdxPopulateGalleryHelper.PopulateGalleryImages(ACategoriesItem: TdxIconLibraryCategories);

  function GetGlyphSize(AGlyph: TBitmap): TSize;
  begin
    Result := Size(AGlyph.Width, AGlyph.Height);
  end;

var
  I: Integer;
  AImageItem: TdxIconLibraryImage;
  AGalleryItem: TdxGalleryControlItem;
begin
  for I := 0 to ACategoriesItem.Count - 1 do
  begin
    AImageItem := ACategoriesItem.Items[I];
    AGalleryItem := GetGalleryItem(AImageItem);
    AGalleryItem.Group := GalleryGroupHidden;
    if IsImageVisible(AImageItem) and IsListBoxCheckedByText(CategoriesList, ACategoriesItem.Name) then
    begin
      AGalleryItem.Group := FCurrentGalleryGroup;
      AGalleryItem.Index := GetIndexForItem(FCurrentGalleryGroup, AImageItem.Name);
      FMaxSize := cxSizeMax(FMaxSize, GetGlyphSize(AGalleryItem.Glyph));
    end;
  end;
end;

{ TdxfmImagePicker }

function TdxfmImagePicker.ExecuteImageCollectionEditor(AFiles: TStrings; const ASuggestedImageSize: TSize): Boolean;

  procedure AddItemInFileList(AItem: TdxGalleryControlItem);
  begin
    if AItem.Checked then
      AFiles.Add(GetIconLibraryImage(AItem).FullFileName);
  end;

  procedure PopulateFileListByGroup(AGroup: TdxGalleryControlGroup);
  var
    I: Integer;
  begin
    for I := 0 to AGroup.ItemCount - 1 do
      AddItemInFileList(AGroup.Items[I]);
  end;

  procedure PopulateFileList;
  var
    I: Integer;
  begin
    for I := 0 to gcIcons.Gallery.Groups.Count - 1 do
      if gcIcons.Gallery.Groups[I].Visible then
        PopulateFileListByGroup(gcIcons.Gallery.Groups[I]);
  end;

var
  I: Integer;
  ASizeString: string;
begin
  gcIcons.OptionsBehavior.ItemCheckMode := icmMultiple;
  gcIcons.PopupMenu := pmSelection;
  tsPictureEditor.TabVisible := False;
  ASizeString := IntToStr(ASuggestedImageSize.cx) + 'x' + IntToStr(ASuggestedImageSize.cy);
  for I := 0 to clbSize.Count - 1 do
    clbSize.Items[I].Checked := (clbSize.Items[I].Text = ASizeString) or cxSizeIsEmpty(ASuggestedImageSize);

  PopulateGallery;
  Result := (ShowModal = mrOk) and (gcIcons.Gallery.GetCheckedItem <> nil);
  if Result then
    PopulateFileList;
  UpdateGalleryItemsSelection(False);
end;

function TdxfmImagePicker.ExecuteAdvancedPictureEditor(
  APicture: TPicture; AGraphicClass: TGraphicClass; const ABuildFilter: string; AImportList: TStrings): Boolean;

  procedure InitInternalControls;
  begin
    OpenDialog.Options := [ofHideReadOnly, ofFileMustExist, ofShowHelp];
    SaveDialog.Options := [ofHideReadOnly, ofFileMustExist, ofShowHelp];
    OpenDialog.DefaultExt := GraphicExtension(AGraphicClass);
    SaveDialog.DefaultExt := GraphicExtension(AGraphicClass);
    if ABuildFilter = '' then
      OpenDialog.Filter := GraphicFilter(AGraphicClass)
    else
      OpenDialog.Filter := ABuildFilter;

    SaveDialog.Filter := OpenDialog.Filter;
    btnSave.Enabled := Assigned(FPicture.Graphic) and not FPicture.Graphic.Empty;
    btnClear.Enabled := Assigned(FPicture.Graphic) and not FPicture.Graphic.Empty;
    btnImport.Visible := AImportList <> nil;
    btnImport.Enabled := AImportList.Count > 0;

    tsPictureEditor.TabVisible := True;
    cxPageControl1.ActivePage := tsPictureEditor;
    gcIcons.OptionsBehavior.ItemCheckMode := icmSingleRadio;
  end;

var
  ACheckedItem: TdxGalleryControlItem;
begin
  FPicture.Assign(APicture);
  ImportList := AImportList;
  gcIcons.PopupMenu := nil;

  InitInternalControls;
  Result := ShowModal = mrOk;
  if Result then
  begin
   if (cxPageControl1.ActivePage = tsDXImageGallery) then
    begin
      ACheckedItem := gcIcons.Gallery.GetCheckedItem;
      if (ACheckedItem <> nil) and ACheckedItem.Group.Visible then
        APicture.Graphic := GetIconLibraryImage(ACheckedItem).Image;
    end
    else
      APicture.Assign(FPicture);
  end;
end;

function TdxfmImagePicker.GetGalleryItem(AIconLibraryImage: TdxIconLibraryImage): TdxGalleryControlItem;
begin
  Result := TdxGalleryControlItem(AIconLibraryImage.Tag);
end;

function TdxfmImagePicker.GetIconLibraryImage(AGalleryControlItem: TdxGalleryControlItem): TdxIconLibraryImage;
begin
  Result := TdxIconLibraryImage(AGalleryControlItem.Tag);
end;

procedure TdxfmImagePicker.DrawPicture(ACanvas: TCanvas; R: TRect);
begin
  ACanvas.Brush.Color := Color;
  if (FPicture.Width > 0) and (FPicture.Height > 0) then
  begin
    cxPaintCanvas.BeginPaint(ACanvas);
    try
      cxDrawPicture(cxPaintCanvas, R, FPicture, ifmFit);
    finally
      cxPaintCanvas.EndPaint;
    end;
  end
  else
  begin
    R := cxRectCenter(R, Size(FPicture.Width, FPicture.Height));
    ACanvas.Brush.Style := bsClear;
    ACanvas.TextOut(R.Left, R.Top, 'None');
  end;
end;

procedure TdxfmImagePicker.PopulateContent;
var
  APopulateContentHelper: TdxPopulateContentHelper;
begin
  APopulateContentHelper := TdxPopulateContentHelper.Create(Self);
  try
    APopulateContentHelper.Populate;
  finally
    APopulateContentHelper.Free;
  end;
end;

procedure TdxfmImagePicker.PopulateGallery;
begin
  if FPopulateGalleryLockCount = 0 then
    FPopulateGalleryHelper.Populate;
end;

procedure TdxfmImagePicker.SetImportList(AValue: TStrings);
var
  I: Integer;
  AMenuItem: TMenuItem;
begin
  if AValue <> nil then
    FImportList.Assign(AValue)
  else
    FImportList.Clear;

  pmImport.Items.Clear;
  for I := 0 to FImportList.Count - 1 do
  begin
    AMenuItem := TMenuItem.Create(Self);
    AMenuItem.Caption := FImportList[I];
    AMenuItem.Tag := I;
    AMenuItem.OnClick := ImportPictureClick;
    pmImport.Items.Add(AMenuItem);
  end;
end;

procedure TdxfmImagePicker.UpdateGalleryItemsSelection(ASelect: Boolean);

  procedure UpdateGalleryItemsSelectionInGroup(AGroup: TdxGalleryControlGroup);
  var
    I: Integer;
  begin
    for I := 0 to AGroup.ItemCount - 1 do
      AGroup.Items[I].Checked := ASelect;
  end;

var
  I: Integer;
begin
  gcIcons.BeginUpdate;
  try
    for I := 0 to gcIcons.Gallery.Groups.Count - 1 do
      UpdateGalleryItemsSelectionInGroup(gcIcons.Gallery.Groups[I]);
  finally
    gcIcons.EndUpdate;
  end;
end;

procedure TdxfmImagePicker.ImportPictureClick(Sender: TObject);
begin
  FPicture.Assign(TPersistent(FImportList.Objects[TMenuItem(Sender).Tag]));
end;

procedure TdxfmImagePicker.PictureChangeHandler(Sender: TObject);
begin
  ImagePaintBox.Invalidate;
  btnSave.Enabled := IsPictureAssigned(FPicture);
  btnClear.Enabled := IsPictureAssigned(FPicture);
end;

procedure TdxfmImagePicker.FormCreate(Sender: TObject);
begin
{$IFDEF DELPHI9}
  PopupMode := pmAuto;
{$ENDIF}
  FPicture := TPicture.Create;
  FPicture.OnChange := PictureChangeHandler;
  FImportList := TStringList.Create;

  FPopulateGalleryMode := gpmGroups;
  FIconLibrary := TdxIconLibrary.Create(GetdxIconLibraryPath);
  FPopulateGalleryHelper := TdxPopulateGalleryHelper.Create(Self);

  clbSize.InnerCheckListBox.MultiSelect := True;
  clbCollection.InnerCheckListBox.MultiSelect := True;
  clbCategories.InnerCheckListBox.MultiSelect := True;

  PopulateContent;
  PopulateGallery;
end;

procedure TdxfmImagePicker.FormDestroy(Sender: TObject);
begin
  FreeAndNil(FPopulateGalleryHelper);
  FreeAndNil(FIconLibrary);
  FreeAndNil(FImportList);
  FreeAndNil(FPicture);
end;

procedure TdxfmImagePicker.beFindPropertiesButtonClick(Sender: TObject; AButtonIndex: Integer);
begin
  beFind.Text := '';
end;

procedure TdxfmImagePicker.beFindPropertiesChange(Sender: TObject);
const
  PopulateModeToImageIndex: array[TdxPopulateGalleryMode] of Integer = (0, 1);
begin
  if beFind.Text <> '' then
    FPopulateGalleryMode := gpmSearch
  else
    FPopulateGalleryMode := gpmGroups;

  beFind.Properties.Buttons[0].ImageIndex := PopulateModeToImageIndex[FPopulateGalleryMode];
  PopulateGallery;
end;

procedure TdxfmImagePicker.btnLoadClick(Sender: TObject);
begin
  if OpenDialog.Execute then
    FPicture.LoadFromFile(OpenDialog.Filename);
end;

procedure TdxfmImagePicker.btnSaveClick(Sender: TObject);
begin
  if SaveDialog.Execute then
    FPicture.SaveToFile(SaveDialog.Filename);
end;

procedure TdxfmImagePicker.btnClearClick(Sender: TObject);
begin
  FPicture.Graphic := nil;
end;

procedure TdxfmImagePicker.clbCategoriesClickCheck(
  Sender: TObject; AIndex: Integer; APrevState, ANewState: TcxCheckBoxState);
begin
  gcIcons.Gallery.Groups[AIndex].Visible := (ANewState = cbsChecked) and (FPopulateGalleryMode = gpmGroups);
  PopulateGallery;
end;

procedure TdxfmImagePicker.clbCollectionClickCheck(
  Sender: TObject; AIndex: Integer; APrevState, ANewState: TcxCheckBoxState);
begin
  PopulateGallery;
end;

procedure TdxfmImagePicker.gcIconsDblClick(Sender: TObject);
var
  AItem: TdxGalleryControlItem;
begin
  AItem := gcIcons.Gallery.Groups.GetItemAtPos(gcIcons.MouseDownPos);
  if AItem <> nil then
  begin
    UpdateGalleryItemsSelection(False);
    AItem.Checked := True;
    FPicture.Graphic := GetIconLibraryImage(AItem).Image;
    ModalResult := mrOk;
  end;  
end;

procedure TdxfmImagePicker.ImagePaintBoxPaint(Sender: TObject);
begin
  DrawPicture(ImagePaintBox.Canvas, ImagePaintBox.ClientRect);
end;

procedure TdxfmImagePicker.miSelectAllinThisGroupClick(Sender: TObject);
var
  I: Integer;
begin
  if FSelectedGroup <> nil then
  begin
    gcIcons.BeginUpdate;
    try
      for I := 0 to FSelectedGroup.ItemCount - 1 do       
        FSelectedGroup.Items[I].Checked := TComponent(Sender).Tag <> 0;
    finally
      gcIcons.EndUpdate;
    end;
  end;
end;

procedure TdxfmImagePicker.miSelectClick(Sender: TObject);
var
  ACheckListBox: TcxCustomCheckListBox;
  I: Integer;
  AActiveComponent: TComponent;
begin
  AActiveComponent := TComponent(ActiveControl);
  if AActiveComponent = gcIcons then
    UpdateGalleryItemsSelection(TComponent(Sender).Tag <> 0)
  else
    if AActiveComponent is TcxCustomInnerCheckListBox then
    begin
      ACheckListBox := TcxCustomInnerCheckListBox(AActiveComponent).Container;
      for I := 0 to ACheckListBox.Items.Count - 1 do
        ACheckListBox.Selected[I] := TComponent(Sender).Tag <> 0;
    end;
end;

procedure TdxfmImagePicker.miUncheckSelectedClick(Sender: TObject);
const
  StateMap: array[Boolean] of TcxCheckBoxState = (cbsUnchecked, cbsChecked);
var
  ACheckListBox: TcxCheckListBox;
  ANewState: TcxCheckBoxState;
  APrevState: TcxCheckBoxState;
  I: Integer;
begin
  if pmSelection.PopupComponent is TcxCustomInnerCheckListBox then
  begin
    ACheckListBox := TcxCustomInnerCheckListBox(pmSelection.PopupComponent).Container as TcxCheckListBox;
    gcIcons.BeginUpdate;
    try
      Inc(FPopulateGalleryLockCount);
      try
        for I := 0 to ACheckListBox.Items.Count - 1 do
        begin
          if ACheckListBox.Selected[I] then
          begin
            ANewState := StateMap[TComponent(Sender).Tag <> 0];
            APrevState := StateMap[ACheckListBox.Items[I].Checked];
            if APrevState <> ANewState then
            begin
              ACheckListBox.Items[I].Checked := ANewState = cbsChecked;
              if Assigned(ACheckListBox.OnClickCheck) then
                ACheckListBox.OnClickCheck(ACheckListBox, I, APrevState, ANewState);
            end;
          end;
        end;
      finally
        Dec(FPopulateGalleryLockCount);
        PopulateGallery;
      end;
    finally
      gcIcons.EndUpdate;
    end;
  end;
end;

procedure TdxfmImagePicker.pmSelectionPopup(Sender: TObject);
begin
  FSelectedGroup := gcIcons.Gallery.Groups.GetGroupAtPos(gcIcons.ScreenToClient(GetMouseCursorPos));
  miCheckSelected.Visible := pmSelection.PopupComponent is TcxCustomInnerCheckListBox;
  miUncheckSelected.Visible := pmSelection.PopupComponent is TcxCustomInnerCheckListBox;
  miSelectAllinThisGroup.Visible := FSelectedGroup <> nil;
  miUnselectAllinThisGroup.Visible := FSelectedGroup <> nil;
end;

procedure TdxfmImagePicker.actF3Execute(Sender: TObject);
begin
  beFind.SetFocus;
end;

procedure TdxfmImagePicker.FormShortCut(var Msg: TWMKey; var Handled: Boolean);
begin
  if beFind.Focused then
    case Msg.CharCode of
      VK_ESCAPE:
        begin
          beFind.Text := '';
          Handled := True;
        end;
      VK_RETURN:
        begin
          gcIcons.SetFocus;
          Handled := True;
        end;
    end;    
end;

procedure TdxfmImagePicker.btnImportClick(Sender: TObject);
begin
//
end;

procedure TdxfmImagePicker.cxPageControl1Change(Sender: TObject);
begin
  if cxPageControl1.ActivePage = tsDXImageGallery then
    ActiveControl := gcIcons;
end;

end.
