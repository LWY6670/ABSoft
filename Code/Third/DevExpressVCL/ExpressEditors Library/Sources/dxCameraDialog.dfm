object frmCameraDialog: TfrmCameraDialog
  Left = 0
  Top = 0
  AutoScroll = False
  BorderIcons = [biSystemMenu]
  Caption = 'Camera Dialog'
  ClientHeight = 262
  ClientWidth = 330
  Color = clBtnFace
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object ccCamera: TdxCameraControl
    Left = 0
    Top = 0
    Width = 330
    Height = 213
    Active = True
    Align = alTop
    Anchors = [akLeft, akTop, akRight, akBottom]
  end
  object btnCancel: TButton
    Left = 243
    Top = 225
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
  end
  object btnAssign: TButton
    Left = 160
    Top = 225
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Assign'
    Enabled = False
    ModalResult = 1
    TabOrder = 1
  end
  object btnSnapshot: TButton
    Left = 77
    Top = 225
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Snapshot'
    Default = True
    TabOrder = 0
    OnClick = btnSnapshotClick
  end
end
