{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressFilterControl                                     }
{                                                                    }
{           Copyright (c) 1998-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSEDITORS AND ALL                }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY. }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit cxFilterControlStrs;

{$I cxVer.inc}

interface

resourcestring
  // cxFilterBoolOperator
  cxSFilterBoolOperatorAnd = '并且';        // all
  cxSFilterBoolOperatorOr = '或者';          // any
  cxSFilterBoolOperatorNotAnd = '非并且'; // not all
  cxSFilterBoolOperatorNotOr = '非或者';   // not any
  //
  cxSFilterRootButtonCaption = '过滤';
  cxSFilterAddCondition = '添加条件(&C)';
  cxSFilterAddGroup = '添加组(&G)';
  cxSFilterRemoveRow = '删除行(&R)';
  cxSFilterClearAll = '清除(&A)';
  cxSFilterFooterAddCondition = '按此按钮增加新条件';
  
  cxSFilterGroupCaption = '使用下面的条件';
  cxSFilterRootGroupCaption = '<根>';
  cxSFilterControlNullString = '<空>';
  
  cxSFilterErrorBuilding = '不能从源建立过滤';

  //FilterDialog
  cxSFilterDialogCaption = '定制过滤';
  cxSFilterDialogInvalidValue = '输入值非法';
  cxSFilterDialogUse = '使用';
  cxSFilterDialogSingleCharacter = '表示任何单个字符';
  cxSFilterDialogCharactersSeries = '表示任意字符';
  cxSFilterDialogOperationAnd = '并且';
  cxSFilterDialogOperationOr = '或者';
  cxSFilterDialogRows = '显示条件行:';

  // FilterControlDialog
  cxSFilterControlDialogCaption = '过滤生成器';
  cxSFilterControlDialogNewFile = '未命名.flt';
  cxSFilterControlDialogOpenDialogCaption = '打开一个已存在文件';
  cxSFilterControlDialogSaveDialogCaption = '保存当前活动文件';
  cxSFilterControlDialogActionSaveCaption = '另存为...';
  cxSFilterControlDialogActionSaveHint = '另存为|用新的名字保存当前激活的过滤器';
  cxSFilterControlDialogActionOpenCaption = '打开...';
  cxSFilterControlDialogActionOpenHint = '打开|打开现有的过滤器';
  cxSFilterControlDialogActionApplyCaption = '应用';
  cxSFilterControlDialogActionOkCaption = '确定';
  cxSFilterControlDialogActionCancelCaption = '取消';
  cxSFilterControlDialogFileExt = 'flt';
  cxSFilterControlDialogFileFilter = '过滤文件 (*.flt)|*.flt';

implementation

uses
  dxCore;

procedure AddExpressFilterControlResourceStringNames(AProduct: TdxProductResourceStrings);

  procedure InternalAdd(const AResourceStringName: string; AAdress: Pointer);
  begin
    AProduct.Add(AResourceStringName, AAdress);
  end;

begin
  InternalAdd('cxSFilterBoolOperatorAnd', @cxSFilterBoolOperatorAnd);
  InternalAdd('cxSFilterBoolOperatorOr', @cxSFilterBoolOperatorOr);
  InternalAdd('cxSFilterBoolOperatorNotAnd', @cxSFilterBoolOperatorNotAnd);
  InternalAdd('cxSFilterBoolOperatorNotOr', @cxSFilterBoolOperatorNotOr);
  InternalAdd('cxSFilterRootButtonCaption', @cxSFilterRootButtonCaption);
  InternalAdd('cxSFilterAddCondition', @cxSFilterAddCondition);
  InternalAdd('cxSFilterAddGroup', @cxSFilterAddGroup);
  InternalAdd('cxSFilterRemoveRow', @cxSFilterRemoveRow);
  InternalAdd('cxSFilterClearAll', @cxSFilterClearAll);
  InternalAdd('cxSFilterFooterAddCondition', @cxSFilterFooterAddCondition);
  InternalAdd('cxSFilterGroupCaption', @cxSFilterGroupCaption);
  InternalAdd('cxSFilterRootGroupCaption', @cxSFilterRootGroupCaption);
  InternalAdd('cxSFilterControlNullString', @cxSFilterControlNullString);
  InternalAdd('cxSFilterErrorBuilding', @cxSFilterErrorBuilding);
  InternalAdd('cxSFilterDialogCaption', @cxSFilterDialogCaption);
  InternalAdd('cxSFilterDialogInvalidValue', @cxSFilterDialogInvalidValue);
  InternalAdd('cxSFilterDialogUse', @cxSFilterDialogUse);
  InternalAdd('cxSFilterDialogSingleCharacter', @cxSFilterDialogSingleCharacter);
  InternalAdd('cxSFilterDialogCharactersSeries', @cxSFilterDialogCharactersSeries);
  InternalAdd('cxSFilterDialogOperationAnd', @cxSFilterDialogOperationAnd);
  InternalAdd('cxSFilterDialogOperationOr', @cxSFilterDialogOperationOr);
  InternalAdd('cxSFilterDialogRows', @cxSFilterDialogRows);
  InternalAdd('cxSFilterControlDialogCaption', @cxSFilterControlDialogCaption);
  InternalAdd('cxSFilterControlDialogNewFile', @cxSFilterControlDialogNewFile);
  InternalAdd('cxSFilterControlDialogOpenDialogCaption', @cxSFilterControlDialogOpenDialogCaption);
  InternalAdd('cxSFilterControlDialogSaveDialogCaption', @cxSFilterControlDialogSaveDialogCaption);
  InternalAdd('cxSFilterControlDialogActionSaveCaption', @cxSFilterControlDialogActionSaveCaption);
  InternalAdd('cxSFilterControlDialogActionSaveHint', @cxSFilterControlDialogActionSaveHint);
  InternalAdd('cxSFilterControlDialogActionOpenCaption', @cxSFilterControlDialogActionOpenCaption);
  InternalAdd('cxSFilterControlDialogActionOpenHint', @cxSFilterControlDialogActionOpenHint);
  InternalAdd('cxSFilterControlDialogActionApplyCaption', @cxSFilterControlDialogActionApplyCaption);
  InternalAdd('cxSFilterControlDialogActionOkCaption', @cxSFilterControlDialogActionOkCaption);
  InternalAdd('cxSFilterControlDialogActionCancelCaption', @cxSFilterControlDialogActionCancelCaption);
  InternalAdd('cxSFilterControlDialogFileExt', @cxSFilterControlDialogFileExt);
  InternalAdd('cxSFilterControlDialogFileFilter', @cxSFilterControlDialogFileFilter);
end;

initialization
  dxResourceStringsRepository.RegisterProduct('ExpressFilterControl', @AddExpressFilterControlResourceStringNames);

finalization
  dxResourceStringsRepository.UnRegisterProduct('ExpressFilterControl');

end.
