{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           Express OrgChart                                         }
{                                                                    }
{           Copyright (c) 1998-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSORGCHART AND ALL ACCOMPANYING  }
{   VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY.              }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE end USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxorgchrstrs;

{$I cxVer.inc}

interface

resourcestring
  sdxOrgChartEditorCancelButton = '取消';
  sdxOrgChartEditorCaption = 'TdxOrgChart 项编辑器';
  sdxOrgChartEditorItems = '项';
  sdxOrgChartEditorProperties = '项属性';

  sdxOrgChartEditorHintAntialiasing = '消除锯齿开/关';
  sdxOrgChartEditorHintApplyForAllChildren = '选定项的所有子项的设置属性';
  sdxOrgChartEditorHintDeleteItem = '删除项目';
  sdxOrgChartEditorHintInsertItem = '插入新项目';
  sdxOrgChartEditorHintInsertSubItem = '插入新子项';
  sdxOrgChartEditorHintRotate =  '90'#$B0' 旋转开/关';
  sdxOrgChartEditorHintZoom = '打开/关闭缩放';

  sdxOrgChartEditorChildAlign = '子对齐(&A)';
  sdxOrgChartEditorColor = '颜色(&C)';
  sdxOrgChartEditorHeight = '高度(&H)';
  sdxOrgChartEditorImageAlign = '图像对齐';
  sdxOrgChartEditorImageIndex = '图像索引';
  sdxOrgChartEditorShape = '形状(&S)';
  sdxOrgChartEditorText = '文本';
  sdxOrgChartEditorWidth = '宽度(&W)';

implementation

uses
  dxCore;

procedure AddOrgChartResourceStringNames(AProduct: TdxProductResourceStrings);
begin
  AProduct.Add('sdxOrgChartEditorCancelButton', @sdxOrgChartEditorCancelButton);
  AProduct.Add('sdxOrgChartEditorCaption', @sdxOrgChartEditorCaption);
  AProduct.Add('sdxOrgChartEditorItems', @sdxOrgChartEditorItems);
  AProduct.Add('sdxOrgChartEditorProperties', @sdxOrgChartEditorProperties);

  AProduct.Add('sdxOrgChartEditorHintAntialiasing', @sdxOrgChartEditorHintAntialiasing);
  AProduct.Add('sdxOrgChartEditorHintApplyForAllChildren', @sdxOrgChartEditorHintApplyForAllChildren);
  AProduct.Add('sdxOrgChartEditorHintDeleteItem', @sdxOrgChartEditorHintDeleteItem);
  AProduct.Add('sdxOrgChartEditorHintInsertItem', @sdxOrgChartEditorHintInsertItem);
  AProduct.Add('sdxOrgChartEditorHintInsertSubItem', @sdxOrgChartEditorHintInsertSubItem);
  AProduct.Add('sdxOrgChartEditorHintRotate', @sdxOrgChartEditorHintRotate);
  AProduct.Add('sdxOrgChartEditorHintZoom', @sdxOrgChartEditorHintZoom);

  AProduct.Add('sdxOrgChartEditorChildAlign', @sdxOrgChartEditorChildAlign);
  AProduct.Add('sdxOrgChartEditorColor', @sdxOrgChartEditorColor);
  AProduct.Add('sdxOrgChartEditorHeight', @sdxOrgChartEditorHeight);
  AProduct.Add('sdxOrgChartEditorImageAlign', @sdxOrgChartEditorImageAlign);
  AProduct.Add('sdxOrgChartEditorImageIndex', @sdxOrgChartEditorImageIndex);
  AProduct.Add('sdxOrgChartEditorShape', @sdxOrgChartEditorShape);
  AProduct.Add('sdxOrgChartEditorText', @sdxOrgChartEditorText);
  AProduct.Add('sdxOrgChartEditorWidth', @sdxOrgChartEditorWidth);
end;

initialization
  dxResourceStringsRepository.RegisterProduct('ExpressOrgChart', @AddOrgChartResourceStringNames);

finalization
  dxResourceStringsRepository.UnRegisterProduct('ExpressOrgChart');
end.

