{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressSpreadSheet                                       }
{                                                                    }
{           Copyright (c) 2001-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSSPREADSHEET CONTROL AND ALL    }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY. }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxSpreadSheetPackedFileFormatCore;

{$I cxVer.Inc}

interface

uses
  Windows, SysUtils, Classes, dxSpreadSheetCore, dxSpreadSheetClasses, dxZIPUtils, dxXMLDoc, dxCustomTree,
  dxSpreadSheetTypes, dxGDIPlusClasses;

type
  TdxSpreadSheetCustomPackedReader = class;
  TdxSpreadSheetCustomPackedWriter = class;

  { TdxSpreadSheetXMLNode }

  TdxSpreadSheetXMLNode = class(TdxXMLNode)
  protected
    procedure CheckTextEncoding; override;
  end;

  { TdxSpreadSheetXMLDocument }

  TdxSpreadSheetXMLDocument = class(TdxXMLDocument)
  protected
    function GetNodeClass(ARelativeNode: TdxTreeCustomNode): TdxTreeCustomNodeClass; override;
  public
    constructor Create(AOwner: TPersistent); override;
  end;

  { TdxSpreadSheetCustomPackedReader }

  TdxSpreadSheetCustomPackedReader = class(TdxSpreadSheetCustomReader)
  private
    FPackageReader: TdxZIPStreamReader;

    function CheckFileName(const AFileName: AnsiString): AnsiString;
  protected
    function IsFileExists(const AFileName: AnsiString): Boolean;
    function ReadFile(const AFileName: AnsiString): TMemoryStream; virtual;
    function ReadXML(const AFileName: AnsiString): TdxXMLDocument; virtual;
  public
    constructor Create(AOwner: TdxCustomSpreadSheet; AStream: TStream); override;
    destructor Destroy; override;
    //
    property PackageReader: TdxZIPStreamReader read FPackageReader;
  end;

  { TdxSpreadSheetCustomPackedReaderParser }

  TdxSpreadSheetCustomPackedReaderParser = class(TdxSpreadSheetCustomFilerSubTask)
  protected
    function ReadXML(const AFileName: AnsiString): TdxXMLDocument; inline;
  end;

  { TdxSpreadSheetCustomPackedWriter }

  TdxSpreadSheetCustomPackedWriter = class(TdxSpreadSheetCustomWriter)
  private
    FPackageWriter: TdxZIPStreamWriter;
  protected
    procedure WriteFile(const AFileName: AnsiString; AStream: TStream); virtual;
    procedure WriteXML(const AFileName: AnsiString; ADocument: TdxXMLDocument); virtual;
  public
    constructor Create(AOwner: TdxCustomSpreadSheet; AStream: TStream); override;
    destructor Destroy; override;
    //
    property PackageWriter: TdxZIPStreamWriter read FPackageWriter;
  end;

  { TdxSpreadSheetCustomPackedWriterBuilder }

  TdxSpreadSheetCustomPackedWriterBuilder = class(TdxSpreadSheetCustomFilerSubTask)
  protected
    procedure WriteFile(const AFileName: AnsiString; AStream: TStream); inline;
    procedure WriteXML(const AFileName: AnsiString; ADocument: TdxXMLDocument); inline;
  end;

implementation

uses
  dxCore, dxSpreadSheetStrs, Math;

{ TdxSpreadSheetXMLNode }

procedure TdxSpreadSheetXMLNode.CheckTextEncoding;
begin
  if Length(Text) > MAXSHORT then
  begin
    Text := Copy(Text, 1, MAXSHORT);
    Text := dxUnicodeStringToXMLString(dxXMLStringToUnicodeString(Text)); 
  end;
  inherited CheckTextEncoding;
end;

{ TdxSpreadSheetXMLDocument }

constructor TdxSpreadSheetXMLDocument.Create(AOwner: TPersistent);
begin
  inherited Create(AOwner);
  Standalone := 'yes';
end;

function TdxSpreadSheetXMLDocument.GetNodeClass(ARelativeNode: TdxTreeCustomNode): TdxTreeCustomNodeClass;
begin
  Result := TdxSpreadSheetXMLNode;
end;

{ TdxSpreadSheetCustomPackedReader }

constructor TdxSpreadSheetCustomPackedReader.Create(AOwner: TdxCustomSpreadSheet; AStream: TStream);
begin
  inherited Create(AOwner, AStream);
  FPackageReader := TdxZIPStreamReader.Create(Stream);
end;

destructor TdxSpreadSheetCustomPackedReader.Destroy;
begin
  FreeAndNil(FPackageReader);
  inherited Destroy;
end;

function TdxSpreadSheetCustomPackedReader.IsFileExists(const AFileName: AnsiString): Boolean;
var
  AItem: TdxZIPItem;
begin
  Result := PackageReader.Find(CheckFileName(AFileName), AItem);
end;

function TdxSpreadSheetCustomPackedReader.ReadFile(const AFileName: AnsiString): TMemoryStream;
var
  AItem: TdxZIPItem;
begin
  try
    Result := TMemoryStream.Create;
    if PackageReader.Find(CheckFileName(AFileName), AItem) then
    begin
      Result.Size := AItem.Size;
      try
        AItem.SaveTo(Result);
        Result.Position := 0;
      except
        Result.Clear;
        DoError(sdxErrorFileIsCorrupted, [AFileName], ssmtError);
      end;
    end
    else
      DoError(sdxErrorFileCannotBeFoundInPackage, [AFileName], ssmtError);
  except
    FreeAndNil(Result);
    raise;
  end;
end;

function TdxSpreadSheetCustomPackedReader.ReadXML(const AFileName: AnsiString): TdxXMLDocument;
var
  AStream: TMemoryStream;
begin
  Result := TdxSpreadSheetXMLDocument.Create(nil);
  AStream := ReadFile(AFileName);
  try
    Result.LoadFromStream(AStream);
  finally
    AStream.Free;
  end;
end;

function TdxSpreadSheetCustomPackedReader.CheckFileName(const AFileName: AnsiString): AnsiString;
begin
  if (AFileName <> '') and (AFileName[1] = '.') then
    Result := Copy(AFileName, 2, MaxInt)
  else
    Result := AFileName;
end;

{ TdxSpreadSheetCustomPackedReaderParser }

function TdxSpreadSheetCustomPackedReaderParser.ReadXML(const AFileName: AnsiString): TdxXMLDocument;
begin
  Result := TdxSpreadSheetCustomPackedReader(Owner).ReadXML(AFileName);
end;

{ TdxSpreadSheetCustomPackedWriter }

constructor TdxSpreadSheetCustomPackedWriter.Create(AOwner: TdxCustomSpreadSheet; AStream: TStream);
begin
  inherited Create(AOwner, AStream);
  FPackageWriter := TdxZIPStreamWriter.Create(Stream);
end;

destructor TdxSpreadSheetCustomPackedWriter.Destroy;
begin
  FreeAndNil(FPackageWriter);
  inherited Destroy;
end;

procedure TdxSpreadSheetCustomPackedWriter.WriteFile(const AFileName: AnsiString; AStream: TStream);
begin
  AStream.Position := 0;
  PackageWriter.AddFile(AFileName, '', AStream);
end;

procedure TdxSpreadSheetCustomPackedWriter.WriteXML(const AFileName: AnsiString; ADocument: TdxXMLDocument);
var
  AStream: TMemoryStream;
begin
  AStream := TMemoryStream.Create;
  try
    ADocument.SaveToStream(AStream);
    WriteFile(AFileName, AStream);
  finally
    AStream.Free;
  end;
end;

{ TdxSpreadSheetCustomPackedWriterBuilder }

procedure TdxSpreadSheetCustomPackedWriterBuilder.WriteFile(const AFileName: AnsiString; AStream: TStream);
begin
  TdxSpreadSheetCustomPackedWriter(Owner).WriteFile(AFileName, AStream);
end;

procedure TdxSpreadSheetCustomPackedWriterBuilder.WriteXML(const AFileName: AnsiString; ADocument: TdxXMLDocument);
begin
  TdxSpreadSheetCustomPackedWriter(Owner).WriteXML(AFileName, ADocument);
end;

end.
