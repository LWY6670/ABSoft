{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressSpreadSheet                                       }
{                                                                    }
{           Copyright (c) 2001-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSSPREADSHEET CONTROL AND ALL    }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY. }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxSpreadSheetCore;

{$I cxVer.inc}

interface

uses
{$IFDEF DELPHI16}
  System.UITypes,
{$ENDIF}
  Windows, Messages, Classes, Types, SysUtils, Graphics, Controls, Variants, Forms, Math, cxClasses, dxCore, cxGraphics,
  dxCoreClasses, StdCtrls, cxFormats, cxControls, cxGeometry, cxVariants, cxLookAndFeels, cxLookAndFeelPainters,
  dxCoreGraphics, cxInplaceContainer, dxGDIPlusClasses, cxNavigator, dxHashUtils, dxSpreadSheetTypes, cxLibraryConsts,
  dxSpreadSheetClasses, dxSpreadSheetUtils, dxSpreadSheetStrs, dxSpreadSheetGraphics, cxDrawTextUtils, cxDateUtils,
  cxStyles, Generics.Collections, dxSpreadSheetNumberFormat, cxEdit, cxMemo, cxTextEdit, cxRichEdit, dxSpreadSheetPrinting;

const
  dxSpreadSheetMovingContainerAlpha: Integer = MaxByte;
  dxSpreadSheetMovingClonedContainerAlpha: Integer = 150;
  dxSpreadSheetResizingContainerAlpha: Integer = 150;
  dxSpreadSheetRotationContainerAlpha: Integer = 150;

  dxSpreadSheetContainerRotateMarkerColor1: TdxAlphaColor = $FFE1F8CE;
  dxSpreadSheetContainerRotateMarkerColor2: TdxAlphaColor = $FF88E43A;
  dxSpreadSheetContainerRotateMarkerSize: Integer = 11;
  dxSpreadSheetContainerSelectionFrameColor: TdxAlphaColor = $FF808080;
  dxSpreadSheetContainerSizingMarkerColor1: TdxAlphaColor = $FFFFFFFF;
  dxSpreadSheetContainerSizingMarkerColor2: TdxAlphaColor = $FFCAEAED;
  dxSpreadSheetContainerSizingMarkerSize: Integer = 9;

  dxSpreadSheetContainerVersion = 1;

type
  EdxSpreadSheetError = class(EdxException);
  EdxSpreadSheetCannotChangePartOfArrayError = class(EdxSpreadSheetError);
  EdxSpreadSheetFormatError = class(EdxSpreadSheetError);
  EdxSpreadSheetReaderError = class(EdxSpreadSheetFormatError);
  EdxSpreadSheetCircularReferencesError = class(EdxSpreadSheetError);

  TdxCustomSpreadSheet = class;
  TdxSpreadSheetOptionsBehavior = class;
  TdxSpreadSheetOptionsView = class;

  TdxSpreadSheetHistory = class;
  TdxSpreadSheetHistoryAction = class;
  TdxSpreadSheetHistoryActionClass = class of TdxSpreadSheetHistoryAction;

  TdxSpreadSheetStyles = class;

  TdxSpreadSheetObjectListItem = class;
  TdxSpreadSheetObjectList = class;

  TdxSpreadSheetCell = class;
  TdxSpreadSheetCellDataFormat = class;
  TdxSpreadSheetCellFont = class;
  TdxSpreadSheetCellStyle = class;

  TdxSpreadSheetCustomHitTest = class;
  
  TdxSpreadSheetFormula = class;
  TdxSpreadSheetFormulaClass = class of TdxSpreadSheetFormula;

  TdxSpreadSheetFormulaResult = class;
  TdxSpreadSheetFormulaToken = class;
  TdxSpreadSheetFormulaTokenClass = class of TdxSpreadSheetFormulaToken;
  TdxSpreadSheetFormulaFormattedText = class;
  TdxSpreadSheetFormulaAsTextInfoList = class;

  TdxSpreadSheetFormulaController = class;

  TdxSpreadSheetContainer = class;
  TdxSpreadSheetContainerCalculator = class;
  TdxSpreadSheetContainerSelectionCellViewInfo = class;
  TdxSpreadSheetContainerViewInfo = class;

  TdxSpreadSheetCustomController = class;

  TdxSpreadSheetCustomView = class;
  TdxSpreadSheetCustomViewClass = class of TdxSpreadSheetCustomView;

  TdxSpreadSheetCustomViewViewInfo = class;

  TdxSpreadSheetViewInfoCellsList = class;
  TdxSpreadSheetCustomCellViewInfo = class;
  TdxSpreadSheetCellViewInfo = class;
  TdxSpreadSheetViewInfo = class;

  TdxSpreadSheetCustomFiler = class;
  TdxSpreadSheetCustomFilerSubTask = class;

  TdxSpreadSheetMergedCellList = class;

  TdxSpreadSheetTableView = class;
  TdxSpreadSheetTableViewCellViewInfo = class;
  TdxSpreadSheetTableViewController = class;
  TdxSpreadSheetTableViewHeaderCellViewInfo = class;
  TdxSpreadSheetTableViewInfo = class;
  TdxSpreadSheetTableViewInfoCellsList = class;
  TdxSpreadSheetTableViewReferencesHighlighter = class;

  TdxSpreadSheetTableItems = class;
  TdxSpreadSheetTableItemClass = class of TdxSpreadSheetTableItem;
  TdxSpreadSheetTableItem = class;
  TdxSpreadSheetTableColumn = class;
  TdxSpreadSheetTableRow = class;

  TdxSpreadSheetDefinedName = class;
  TdxSpreadSheetDefinedNames = class;

  TdxSpreadSheetExternalLink = class;
  TdxSpreadSheetExternalLinks = class;

  TdxSpreadSheetPageControl = class;

  TdxSpreadSheetMergedCell = class;

  TdxSpreadSheetProgressEvent = procedure (Sender: TObject; APercent: Integer) of object;

  TdxSpreadSheetPageControlButton = (sspcbFirst, sspcbPrev, sspcbNext, sspcbLast, sspcbNew);
  TdxSpreadSheetPageControlButtons = set of TdxSpreadSheetPageControlButton;

  { IdxSpreadSheetTableView }

  IdxSpreadSheetTableView = interface
  ['{5A6CD395-25BF-48FE-8FC8-B320466367BD}']
    function GetAbsoluteCellBounds(const ARowIndex, AColumnIndex: Integer; ACheckMergedCells: Boolean = True): TRect;
    function GetCell(const ARowIndex, AColumnIndex: Integer): TdxSpreadSheetCell;
    function GetCellAtAbsolutePoint(const P: TPoint; out ARowIndex, AColumnIndex: Integer): Boolean;
  end;

  IdxSpreadSheetListener = interface
  ['{F9D4DD62-FB94-45FA-8934-5A59B03DA598}']
    procedure DataChanged(Sender: TdxCustomSpreadSheet);
  end;

  { TdxSpreadSheetPersistentObject }

  TdxSpreadSheetPersistentObject = class(TInterfacedPersistent)
  strict private
    FSpreadSheet: TdxCustomSpreadSheet;
  protected
    function GetOwner: TPersistent; override;
  public
    constructor Create(ASpreadSheet: TdxCustomSpreadSheet); virtual;

    property SpreadSheet: TdxCustomSpreadSheet read FSpreadSheet;
  end;

  { TdxSpreadSheetViewPersistentObject }

  TdxSpreadSheetViewPersistentObject = class
  strict private
    FView: TdxSpreadSheetCustomView;
    function GetSpreadSheet: TdxCustomSpreadSheet;
  public
    constructor Create(AView: TdxSpreadSheetCustomView); virtual;

    property SpreadSheet: TdxCustomSpreadSheet read GetSpreadSheet;
    property View: TdxSpreadSheetCustomView read FView;
  end;

  { TdxSpreadSheetObjectList }

  TdxSpreadSheetObjectList = class(TdxSpreadSheetPersistentObject)
  strict private
    FItems: TcxObjectList;

    function GetCount: Integer;
    function GetItem(AIndex: Integer): TdxSpreadSheetObjectListItem;
  protected
    function CreateItem: TdxSpreadSheetObjectListItem; virtual;
    procedure Changed; virtual;

    property ItemList: TcxObjectList read FItems;
  public
    constructor Create(ASpreadSheet: TdxCustomSpreadSheet); override;
    destructor Destroy; override;
    procedure Clear;
    procedure Delete(AIndex: Integer);
    procedure Remove(AItem: TdxSpreadSheetObjectListItem);

    property Count: Integer read GetCount;
    property Items[Index: Integer]: TdxSpreadSheetObjectListItem read GetItem;
  end;

  { TdxSpreadSheetObjectListItem }

  TdxSpreadSheetObjectListItem = class
  strict private
    FOwner: TdxSpreadSheetObjectList;
    function GetIndex: Integer;
  protected
    procedure Changed; virtual;
  public
    constructor Create(AOwner: TdxSpreadSheetObjectList); virtual;
    destructor Destroy; override;

    property Index: Integer read GetIndex;
    property Owner: TdxSpreadSheetObjectList read FOwner;
  end;

  { TdxSpreadSheetCellCustomAttribute }

  TdxSpreadSheetCellCustomAttribute = class
  strict private
    FOwner: TdxSpreadSheetCellStyle;
  public
    constructor Create(AOwner: TdxSpreadSheetCellStyle); virtual;

    property Owner: TdxSpreadSheetCellStyle read FOwner;
  end;

  { TdxSpreadSheetCellBorder }

  TdxSpreadSheetCellBorder = class(TdxSpreadSheetCellCustomAttribute)
  strict private
    FKind: TcxBorder;

    function GetColor: TColor;
    function GetHandle: TdxSpreadSheetBordersHandle;
    function GetStyle: TdxSpreadSheetCellBorderStyle;
    procedure SetColor(const AValue: TColor);
    procedure SetHandle(const AValue: TdxSpreadSheetBordersHandle);
    procedure SetStyle(const AValue: TdxSpreadSheetCellBorderStyle);
  protected
    procedure ChangeBorder(AStyle: TdxSpreadSheetCellBorderStyle; AColor: TColor);
    //
    property Handle: TdxSpreadSheetBordersHandle read GetHandle write SetHandle;
    property Kind: TcxBorder read FKind;
  public
    constructor Create(AOwner: TdxSpreadSheetCellStyle; AKind: TcxBorder); reintroduce; virtual;
    procedure Assign(ASource: TdxSpreadSheetCellBorder); virtual;
    procedure Reset;

    property Color: TColor read GetColor write SetColor;
    property Style: TdxSpreadSheetCellBorderStyle read GetStyle write SetStyle;
  end;

  { TdxSpreadSheetCellBrush }

  TdxSpreadSheetCellBrush = class(TdxSpreadSheetCellCustomAttribute)
  strict private
    function GetBackgroundColor: TColor;
    function GetForegroundColor: TColor;
    function GetHandle: TdxSpreadSheetBrushHandle;
    function GetStyle: TdxSpreadSheetCellFillStyle;
    procedure SetBackgroundColor(const Value: TColor);
    procedure SetForegroundColor(const Value: TColor);
    procedure SetHandle(const AValue: TdxSpreadSheetBrushHandle);
    procedure SetStyle(const Value: TdxSpreadSheetCellFillStyle);
  protected
    procedure ChangeBrush(AStyle: TdxSpreadSheetCellFillStyle; ABackgroundColor: TColor; AForegroundColor: TColor);
    //
    property Handle: TdxSpreadSheetBrushHandle read GetHandle write SetHandle;
  public
    procedure Assign(ABrush: TdxSpreadSheetCellBrush); virtual;

    property BackgroundColor: TColor read GetBackgroundColor write SetBackgroundColor;
    property ForegroundColor: TColor read GetForegroundColor write SetForegroundColor;
    property Style: TdxSpreadSheetCellFillStyle read GetStyle write SetStyle;
  end;

  { TdxSpreadSheetCellFont }

  TdxSpreadSheetCellFont = class(TdxSpreadSheetCellCustomAttribute)
  strict private
    function GetCharset: TFontCharset;
    function GetColor: TColor;
    function GetHandle: TdxSpreadSheetFontHandle;
    function GetHeight: Integer;
    function GetName: TFontName;
    function GetPitch: TFontPitch;
    function GetSize: Integer;
    function GetStyle: TFontStyles;
    procedure SetCharset(const AValue: TFontCharset);
    procedure SetColor(const AValue: TColor);
    procedure SetHandle(const AValue: TdxSpreadSheetFontHandle);
    procedure SetHeight(const AValue: Integer);
    procedure SetName(const AValue: TFontName);
    procedure SetPitch(const AValue: TFontPitch);
    procedure SetSize(const AValue: Integer);
    procedure SetStyle(const AValue: TFontStyles);
  protected
    procedure ChangeFont(const AName: TFontName; ACharset: TFontCharset;
      AColor: TColor; ASize: Integer; APitch: TFontPitch; AStyle: TFontStyles);
    //
    property Handle: TdxSpreadSheetFontHandle read GetHandle write SetHandle;
  public
    procedure Assign(AFont: TdxSpreadSheetCellFont); overload;
    procedure Assign(AFont: TFont); overload;
    procedure AssignToFont(ATargetFont: TFont);
    //
    property Charset: TFontCharset read GetCharset write SetCharset;
    property Color: TColor read GetColor write SetColor;
    property Height: Integer read GetHeight write SetHeight;
    property Name: TFontName read GetName write SetName;
    property Pitch: TFontPitch read GetPitch write SetPitch default fpDefault;
    property Size: Integer read GetSize write SetSize stored False;
    property Style: TFontStyles read GetStyle write SetStyle;
  end;

  { TdxSpreadSheetCellDataFormat }
  
  TdxSpreadSheetCellDataFormat = class(TdxSpreadSheetCellCustomAttribute)
  strict private
    function GetFormatCode: TdxUnicodeString;
    function GetFormatCodeID: Integer;
    function GetFormats: TdxSpreadSheetFormats; inline;
    function GetHandle: TdxSpreadSheetFormatHandle;
    function GetIsDateTime: Boolean;
    function GetIsText: Boolean;
    procedure SetFormatCode(const AValue: TdxUnicodeString);
    procedure SetFormatCodeID(AValue: Integer);
    procedure SetHandle(AValue: TdxSpreadSheetFormatHandle);
  protected
    procedure FormatterNeeded;
    //
    property Formats: TdxSpreadSheetFormats read GetFormats;
    property Handle: TdxSpreadSheetFormatHandle read GetHandle write SetHandle;
    property IsDateTime: Boolean read GetIsDateTime;
    property IsText: Boolean read GetIsText;
  public
    procedure Assign(ADataFormat: TdxSpreadSheetCellDataFormat); virtual;
    procedure Format(const AValue: Variant; AValueType: TdxSpreadSheetCellDataType;
      AFormatSettings: TdxSpreadSheetFormatSettings; var AResult: TdxSpreadSheetNumberFormatResult);
    //
    property FormatCode: TdxUnicodeString read GetFormatCode write SetFormatCode;
    property FormatCodeID: Integer read GetFormatCodeID write SetFormatCodeID;
  end;

  { TdxSpreadSheetCellStyle }

  TdxSpreadSheetCellStyle = class(TObject)
  strict private
    FBorders: array[TcxBorder] of TdxSpreadSheetCellBorder;
    FBrush: TdxSpreadSheetCellBrush;
    FDataFormat: TdxSpreadSheetCellDataFormat;
    FFont: TdxSpreadSheetCellFont;
    FOwner: TObject;
    FPrevHandle: TdxSpreadSheetCellStyleHandle;
    FUpdateLockCount: Integer;

    procedure ChangeState(AState: TdxSpreadSheetCellState; AValue: Boolean);
    function GetAlignHorz: TdxSpreadSheetDataAlignHorz;
    function GetAlignHorzIndent: Integer;
    function GetAlignVert: TdxSpreadSheetDataAlignVert;
    function GetBorder(ABorder: TcxBorder): TdxSpreadSheetCellBorder;
    function GetBrush: TdxSpreadSheetCellBrush;
    function GetCellStyles: TdxSpreadSheetCellStyles; inline;
    function GetDataFormat: TdxSpreadSheetCellDataFormat;
    function GetIsDefault: Boolean;
    function GetFont: TdxSpreadSheetCellFont;
    function GetHidden: Boolean;
    function GetLocked: Boolean;
    function GetShrinkToFit: Boolean;
    function GetState: TdxSpreadSheetCellStates;
    function GetWordWrap: Boolean;
    procedure SetAlignHorz(const AValue: TdxSpreadSheetDataAlignHorz);
    procedure SetAlignHorzIndent(const AValue: Integer);
    procedure SetAlignVert(const AValue: TdxSpreadSheetDataAlignVert);
    procedure SetBorder(ABorder: TcxBorder; const AValue: TdxSpreadSheetCellBorder);
    procedure SetBrush(const AValue: TdxSpreadSheetCellBrush);
    procedure SetDataFormat(const AValue: TdxSpreadSheetCellDataFormat);
    procedure SetFont(const AValue: TdxSpreadSheetCellFont);
    procedure SetHidden(const AValue: Boolean);
    procedure SetLocked(const AValue: Boolean);
    procedure SetShrinkToFit(const AValue: Boolean);
    procedure SetState(const AValue: TdxSpreadSheetCellStates);
    procedure SetWordWrap(const AValue: Boolean);
  protected
    FHandle: TdxSpreadSheetCellStyleHandle;

    procedure Changed; virtual;
    procedure CloneHandle; virtual;
    function GetSpreadSheet: TdxCustomSpreadSheet; virtual;
    procedure SetHandle(const AHandle: TdxSpreadSheetCellStyleHandle); virtual;
    procedure ProcessStyleChanges(APrevStyle, ANewStyle: TdxSpreadSheetCellStyleHandle); virtual;
    procedure ReleaseHandle; virtual;
    procedure ReplaceHandle; virtual;

    property CellStyles: TdxSpreadSheetCellStyles read GetCellStyles;
    property Owner: TObject read FOwner;
    property SpreadSheet: TdxCustomSpreadSheet read GetSpreadSheet;
    property State: TdxSpreadSheetCellStates read GetState write SetState;
    property UpdateLockCount: Integer read FUpdateLockCount write FUpdateLockCount;
  public
    constructor Create(AOwner: TObject); virtual;
    destructor Destroy; override;
    procedure Assign(AStyle: TdxSpreadSheetCellStyle);
    procedure BeginUpdate;
    procedure EndUpdate;

    property AlignHorz: TdxSpreadSheetDataAlignHorz read GetAlignHorz write SetAlignHorz;
    property AlignHorzIndent: Integer read GetAlignHorzIndent write SetAlignHorzIndent;
    property AlignVert: TdxSpreadSheetDataAlignVert read GetAlignVert write SetAlignVert;
    property Borders[ABorder: TcxBorder]: TdxSpreadSheetCellBorder read GetBorder write SetBorder;
    property Brush: TdxSpreadSheetCellBrush read GetBrush write SetBrush;
    property DataFormat: TdxSpreadSheetCellDataFormat read GetDataFormat write SetDataFormat;
    property Font: TdxSpreadSheetCellFont read GetFont write SetFont;
    property Hidden: Boolean read GetHidden write SetHidden;
    property Locked: Boolean read GetLocked write SetLocked;
    property ShrinkToFit: Boolean read GetShrinkToFit write SetShrinkToFit;
    property WordWrap: Boolean read GetWordWrap write SetWordWrap;
    //
    property Handle: TdxSpreadSheetCellStyleHandle read FHandle write SetHandle;
    property IsDefault: Boolean read GetIsDefault;
  end;

  { TdxSpreadSheetDefaultCellStyle }

  TdxSpreadSheetDefaultCellStyle = class(TdxSpreadSheetCellStyle)
  protected
    procedure Changed; override;
    procedure CloneHandle; override;
    function GetSpreadSheet: TdxCustomSpreadSheet; override;
    procedure SetHandle(const AHandle: TdxSpreadSheetCellStyleHandle); override;
    procedure ReleaseHandle; override;
    procedure ReplaceHandle; override;
  public
    constructor Create(AOwner: TObject); override;
  end;

  { TdxSpreadSheetCellDisplayValue }

  TdxSpreadSheetCellDisplayValue = record
    FormatCode: TdxUnicodeString;
    FormattedValue: TdxSpreadSheetNumberFormatResult;
    Value: Variant;

    function CheckNeedUpdateValue(const AValue: Variant; const AFormatCode: TdxUnicodeString): Boolean;
    procedure Reset;
  end;

  { TdxSpreadSheetCell }

  TdxSpreadSheetCell = class(TdxDynamicListItem)
  strict private
    FArrayFormulaMasterCell: TdxSpreadSheetCell;
    FDisplayValue: TdxSpreadSheetCellDisplayValue;
    FDisplayValueIsDirty: Boolean;
    FShowFormula: TdxDefaultBoolean;

    function GetActualDataType: TdxSpreadSheetCellDataType;
    function GetActualFormulaValue: Variant;
    function GetActualShowFormula: Boolean;
    function GetColumn: TdxSpreadSheetTableColumn; inline;
    function GetColumnIndex: Integer; inline;
    function GetDisplayText: TdxUnicodeString;
    function GetDisplayValue: TdxSpreadSheetNumberFormatResult;
    function GetHasValue: Boolean;
    function GetHistory: TdxSpreadSheetHistory; inline;
    function GetIsEmpty: Boolean;
    function GetIsFormula: Boolean;
    function GetIsMerged: Boolean;
    function GetRow: TdxSpreadSheetTableRow; inline;
    function GetRowIndex: Integer; inline;
    function GetSpreadSheet: TdxCustomSpreadSheet; inline;
    function GetView: TdxSpreadSheetTableView; inline;
    procedure SetColumn(AValue: TdxSpreadSheetTableColumn);
    procedure SetColumnIndex(AValue: Integer);
    procedure SetIsEmpty(AValue: Boolean);
    procedure SetRow(AValue: TdxSpreadSheetTableRow);
    procedure SetRowIndex(AValue: Integer);
    procedure SetShowFormula(AValue: TdxDefaultBoolean);
  protected
    FData: array[0..9] of Byte;
    FDataType: TdxSpreadSheetCellDataType;
    FStyle: TdxSpreadSheetCellStyle;
    FShrinkToFitCacheContentWidth: Integer;
    FShrinkToFitCacheFontSize: Integer;

    procedure AssignData(ASource: TdxSpreadSheetCell);
    procedure Changed(AChanges: TdxSpreadSheetChanges); virtual;
    procedure CheckAndDeleteObjectData; inline;
    procedure CheckAreaReferenceTokens(var AParentToken: TdxSpreadSheetFormulaToken);
    procedure ProcessStyleChanges(APrevStyle, ANewStyle: TdxSpreadSheetCellStyleHandle);
    procedure StyleChanged; virtual;

    procedure ClearReferenceToArrayFormula;
    procedure InitializeReferenceToArrayFormula;

    function GetAsBoolean: Boolean; virtual;
    function GetAsCurrency: Currency; virtual;
    function GetAsDateTime: TDateTime; virtual;
    function GetAsError: TdxSpreadSheetFormulaErrorCode; virtual;
    function GetAsFloat: Double; virtual;
    function GetAsFormula: TdxSpreadSheetFormula; virtual;
    function GetAsInteger: Integer; virtual;
    function GetAsSharedString: TdxSpreadSheetSharedString; virtual;
    function GetAsString: TdxUnicodeString; virtual;
    function GetAsVariant: Variant; virtual;

    function IsMultiline: Boolean; virtual;
    function IsNumericValue: Boolean;

    procedure LoadFromStream(AReader: TcxReader; AStyleHandle: TdxSpreadSheetCellStyleHandle;
      AFormulasRef: TdxSpreadSheetFormulaAsTextInfoList); virtual;
    procedure SaveToStream(AWriter: TcxWriter; AStyleIndex: Integer); virtual;

    class function GetContentOffsets(ABottomBorderStyle: TdxSpreadSheetCellBorderStyle): TRect;
    function MeasureSize(ACanvas: TcxCanvas; ACalcHeight: Boolean): Integer;

    procedure SetAsBoolean(const AValue: Boolean); virtual;
    procedure SetAsCurrency(const AValue: Currency); virtual;
    procedure SetAsDateTime(const AValue: TDateTime); virtual;
    procedure SetAsError(const AValue: TdxSpreadSheetFormulaErrorCode); virtual;
    procedure SetAsFloat(const AValue: Double); virtual;
    procedure SetAsFormula(const AValue: TdxSpreadSheetFormula); virtual;
    procedure SetAsInteger(const AValue: Integer); virtual;
    procedure SetAsSharedString(AValue: TdxSpreadSheetSharedString); virtual;
    procedure SetAsString(const AValue: TdxUnicodeString); virtual;
    procedure SetAsVariant(const AValue: Variant); virtual;
    //
    property ActualDataType: TdxSpreadSheetCellDataType read GetActualDataType;
    property ActualShowFormula: Boolean read GetActualShowFormula;
    property ArrayFormulaMasterCell: TdxSpreadSheetCell read FArrayFormulaMasterCell;
    property DisplayValueIsDirty: Boolean read FDisplayValueIsDirty write FDisplayValueIsDirty;
    property History: TdxSpreadSheetHistory read GetHistory;
  public
    constructor Create(AOwner: TdxDynamicItemList; AIndex: Integer); override;
    destructor Destroy; override;
    procedure BeforeDestruction; override;
    procedure Clear;
    function GetAbsoluteBounds: TRect;
    procedure SetText(const AText: TdxUnicodeString; const AFormulaChecking: Boolean = False); virtual;

    property AsBoolean: Boolean read GetAsBoolean write SetAsBoolean;
    property AsCurrency: Currency read GetAsCurrency write SetAsCurrency;
    property AsDateTime: TDateTime read GetAsDateTime write SetAsDateTime;
    property AsError: TdxSpreadSheetFormulaErrorCode read GetAsError write SetAsError;
    property AsFloat: Double read GetAsFloat write SetAsFloat;
    property AsFormula: TdxSpreadSheetFormula read GetAsFormula write SetAsFormula;
    property AsInteger: Longint read GetAsInteger write SetAsInteger;
    property AsSharedString: TdxSpreadSheetSharedString read GetAsSharedString write SetAsSharedString;
    property AsString: TdxUnicodeString read GetAsString write SetAsString;
    property AsVariant: Variant read GetAsVariant write SetAsVariant;
    property Column: TdxSpreadSheetTableColumn read GetColumn write SetColumn;
    property ColumnIndex: Integer read GetColumnIndex write SetColumnIndex;
    property DataType: TdxSpreadSheetCellDataType read FDataType;
    property DisplayText: TdxUnicodeString read GetDisplayText;
    property DisplayValue: TdxSpreadSheetNumberFormatResult read GetDisplayValue;
    property HasValue: Boolean read GetHasValue;
    property IsEmpty: Boolean read GetIsEmpty write SetIsEmpty;
    property IsFormula: Boolean read GetIsFormula;
    property IsMerged: Boolean read GetIsMerged;
    property Row: TdxSpreadSheetTableRow read GetRow write SetRow;
    property RowIndex: Integer read GetRowIndex write SetRowIndex;
    property ShowFormula: TdxDefaultBoolean read FShowFormula write SetShowFormula;
    property SpreadSheet: TdxCustomSpreadSheet read GetSpreadSheet;
    property Style: TdxSpreadSheetCellStyle read FStyle;
    property View: TdxSpreadSheetTableView read GetView;
  end;

  { TdxSpreadSheetCustomDrawingObject }

  TdxSpreadSheetCustomDrawingObject = class(TdxSpreadSheetPersistentObject)
  strict private
    FOnChange: TNotifyEvent;
  protected
    procedure Changed; virtual;
    //
    property OnChange: TNotifyEvent read FOnChange write FOnChange;
  public
    procedure Assign(Source: TPersistent); override;
  end;

  { TdxSpreadSheetContainerAnchorPoint }

  TdxSpreadSheetContainerAnchorPoint = class(TcxOwnedPersistent)
  strict private
    FCell: TdxSpreadSheetCell;
    FFixedToCell: Boolean;
    FOffset: TPoint;

    function GetContainer: TdxSpreadSheetContainer; inline;
    procedure SetCell(const AValue: TdxSpreadSheetCell);
    procedure SetOffset(const AValue: TPoint);
  protected
    procedure Changed; virtual;
    procedure DoAssign(Source: TPersistent); override;
    function IsValidCell(AValue: TdxSpreadSheetCell): Boolean;

    procedure LoadFromStream(AReader: TcxReader); virtual;
    procedure SaveToStream(AWriter: TcxWriter); virtual;
    //
    property Container: TdxSpreadSheetContainer read GetContainer;
  public
    constructor Create(AOwner: TPersistent); override;
    //
    property Cell: TdxSpreadSheetCell read FCell write SetCell;
    property FixedToCell: Boolean read FFixedToCell write FFixedToCell;
    property Offset: TPoint read FOffset write SetOffset;
  end;

  { TdxSpreadSheetContainerTransform }

  TdxSpreadSheetContainerTransform = class(TcxOwnedPersistent)
  strict private
    FFlipHorizontally: Boolean;
    FFlipVertically: Boolean;
    FRotationAngle: Double;

    function GetContainer: TdxSpreadSheetContainer; inline;
    procedure SetFlipHorizontally(AValue: Boolean);
    procedure SetFlipVertically(AValue: Boolean);
    procedure SetRotationAngle(const AValue: Double);
  protected
    procedure Changed; virtual;
    procedure DoAssign(Source: TPersistent); override;

    procedure LoadFromStream(AReader: TcxReader); virtual;
    procedure SaveToStream(AWriter: TcxWriter); virtual;
    //
    property Container: TdxSpreadSheetContainer read GetContainer;
  public
    property FlipHorizontally: Boolean read FFlipHorizontally write SetFlipHorizontally;
    property FlipVertically: Boolean read FFlipVertically write SetFlipVertically;
    property RotationAngle: Double read FRotationAngle write SetRotationAngle;
  end;

  { TdxSpreadSheetContainer }

  TdxSpreadSheetContainerAnchorType = (catAbsolute, catOneCell, catTwoCell);

  TdxSpreadSheetContainerRestriction = (crNoCrop, crNoMove, crNoResize, crNoRotation, crNoChangeAspectUsingCornerHandles);
  TdxSpreadSheetContainerRestrictions = set of TdxSpreadSheetContainerRestriction;

  TdxSpreadSheetContainerClass = class of TdxSpreadSheetContainer;
  TdxSpreadSheetContainer = class(TdxSpreadSheetPersistentObject)
  strict private
    FAnchorPoint1: TdxSpreadSheetContainerAnchorPoint;
    FAnchorPoint2: TdxSpreadSheetContainerAnchorPoint;
    FAnchorType: TdxSpreadSheetContainerAnchorType;
    FCalculator: TdxSpreadSheetContainerCalculator;
    FDescription: TdxUnicodeString;
    FHasChanges: Boolean;
    FName: TdxUnicodeString;
    FParent: TdxSpreadSheetCustomView;
    FRestrictions: TdxSpreadSheetContainerRestrictions;
    FTitle: TdxUnicodeString;
    FTransform: TdxSpreadSheetContainerTransform;
    FUpdateCount: Integer;
    FVisible: Boolean;

    function GetFocused: Boolean;
    function GetHistory: TdxSpreadSheetHistory;
    function GetIndex: Integer;
    function GetIsUpdating: Boolean;
    procedure LoadHeaderFromStream(AReader: TcxReader; out AVersion: Word); 
    procedure SaveHeaderToStream(AWriter: TcxWriter); 
    procedure SetAnchorPoint1(const AValue: TdxSpreadSheetContainerAnchorPoint);
    procedure SetAnchorPoint2(const AValue: TdxSpreadSheetContainerAnchorPoint);
    procedure SetAnchorType(const AValue: TdxSpreadSheetContainerAnchorType);
    procedure SetFocused(const AValue: Boolean);
    procedure SetIndex(AValue: Integer);
    procedure SetParent(const AValue: TdxSpreadSheetCustomView);
    procedure SetTransform(AValue: TdxSpreadSheetContainerTransform);
    procedure SetVisible(AValue: Boolean);
  protected
    procedure CellRemoving(ACell: TdxSpreadSheetCell); virtual;
    procedure Changed;
    procedure CheckChangeObject(AProc: TProc);
    procedure CheckCreateObject; inline;
    function CreateAnchorPoint: TdxSpreadSheetContainerAnchorPoint; virtual;
    function CreateCalculator: TdxSpreadSheetContainerCalculator; virtual;
    function CreateTransform: TdxSpreadSheetContainerTransform; virtual;
    function CreateViewInfo: TdxSpreadSheetContainerViewInfo; virtual;
    procedure DoChanged; virtual;

    function CanMove: Boolean; virtual;
    function CanResize: Boolean; virtual;
    function CanRotate: Boolean; virtual;
    function KeepAspectUsingCornerHandles: Boolean; virtual;

    procedure LoadFromStream(AReader: TcxReader); virtual;
    procedure LoadSettings(AReader: TcxReader);
    procedure LoadSettingsFromStream(AReader: TcxReader; const AVersion: Word); virtual;
    procedure SaveSettings(AWriter: TcxWriter);
    procedure SaveSettingsToStream(AWriter: TcxWriter); virtual;
    procedure SaveToStream(AWriter: TcxWriter); virtual;

    property Calculator: TdxSpreadSheetContainerCalculator read FCalculator;
    property History: TdxSpreadSheetHistory read GetHistory;
    property IsUpdating: Boolean read GetIsUpdating;
  public
    constructor Create(ASpreadSheet: TdxCustomSpreadSheet); override;
    destructor Destroy; override;
    procedure BeforeDestruction; override;
    procedure Assign(Source: TPersistent); override;
    procedure BeginUpdate;
    procedure EndUpdate;
    //
    procedure BringToFront;
    procedure SendToBack;
    //
    property AnchorPoint1: TdxSpreadSheetContainerAnchorPoint read FAnchorPoint1 write SetAnchorPoint1;
    property AnchorPoint2: TdxSpreadSheetContainerAnchorPoint read FAnchorPoint2 write SetAnchorPoint2;
    property AnchorType: TdxSpreadSheetContainerAnchorType read FAnchorType write SetAnchorType;
    property Description: TdxUnicodeString read FDescription write FDescription;
    property Focused: Boolean read GetFocused write SetFocused;
    property Index: Integer read GetIndex write SetIndex;
    property Name: TdxUnicodeString read FName write FName;
    property Parent: TdxSpreadSheetCustomView read FParent write SetParent;
    property Restrictions: TdxSpreadSheetContainerRestrictions read FRestrictions write FRestrictions;
    property Title: TdxUnicodeString read FTitle write FTitle;
    property Transform: TdxSpreadSheetContainerTransform read FTransform write SetTransform;
    property Visible: Boolean read FVisible write SetVisible;
  end;

  { TdxSpreadSheetContainerCalculator }

  TdxSpreadSheetContainerCalculator = class(TObject)
  strict private
    FOwner: TdxSpreadSheetContainer;

    function GetAnchorPoint1: TdxSpreadSheetContainerAnchorPoint; inline;
    function GetAnchorPoint2: TdxSpreadSheetContainerAnchorPoint; inline;
    function GetAnchorType: TdxSpreadSheetContainerAnchorType; inline;
    function GetTransform: TdxSpreadSheetContainerTransform; inline;
    procedure SetAnchorType(const Value: TdxSpreadSheetContainerAnchorType); inline;
  protected
    function CheckForContentArea(const R: TRect): TRect;
    function GetCellAbsoluteBounds(ACell: TdxSpreadSheetCell): TRect;
    function GetContentRect: TRect;
    function GetNearCells(const R: TRect; out ACell1, ACell2: TdxSpreadSheetCell): Boolean; virtual;
    procedure ModifyBounds(ALeftModifier, ATopModifier, ARightModifier, ABottomModifier: Integer);
    procedure ResizeContainer(ADeltaX, ADeltaY: Integer);
    function TransformCoords(const R: TRect; ABackwardDirection: Boolean): TRect; virtual;
  public
    constructor Create(AOwner: TdxSpreadSheetContainer); virtual;
    function CalculateBounds: TRect; virtual;
    procedure UpdateAnchors(const ABounds: TRect); virtual;
    function UpdateAnchorsAfterResize(const APrevBounds: TRect): Boolean; virtual;
    //
    property AnchorPoint1: TdxSpreadSheetContainerAnchorPoint read GetAnchorPoint1;
    property AnchorPoint2: TdxSpreadSheetContainerAnchorPoint read GetAnchorPoint2;
    property AnchorType: TdxSpreadSheetContainerAnchorType read GetAnchorType write SetAnchorType;
    property Transform: TdxSpreadSheetContainerTransform read GetTransform;
    property Owner: TdxSpreadSheetContainer read FOwner;
  end;

  { TdxSpreadSheetShape }

  TdxSpreadSheetShapeType = (stRect, stRoundRect, stEllipse);

  TdxSpreadSheetShape = class(TdxSpreadSheetCustomDrawingObject)
  strict private
    FBrush: TdxGPBrush;
    FPen: TdxGPPen;
    FShapeType: TdxSpreadSheetShapeType;

    procedure ChangeHandler(Sender: TObject);

    procedure SetBrush(const AValue: TdxGPBrush);
    procedure SetPen(const AValue: TdxGPPen);
    procedure SetShapeType(const AValue: TdxSpreadSheetShapeType);
  public
    constructor Create(ASpreadSheet: TdxCustomSpreadSheet); override;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;

    property Brush: TdxGPBrush read FBrush write SetBrush;
    property Pen: TdxGPPen read FPen write SetPen;
    property ShapeType: TdxSpreadSheetShapeType read FShapeType write SetShapeType;
  end;

  { TdxSpreadSheetShapeContainer }

  TdxSpreadSheetShapeContainer = class(TdxSpreadSheetContainer)
  strict private
    FShape: TdxSpreadSheetShape;

    procedure SetShape(AValue: TdxSpreadSheetShape);
    procedure ShapeChangeHandler(Sender: TObject);
  protected
    function CreateShape: TdxSpreadSheetShape; virtual;
    function CreateViewInfo: TdxSpreadSheetContainerViewInfo; override;

    procedure LoadBrushFromStream(ABrush: TdxGPBrush; AReader: TcxReader);
    procedure LoadImageFromStream(AImage: TdxGPImage; AReader: TcxReader);
    procedure LoadSettingsFromStream(AReader: TcxReader; const AVersion: Word); override;
    procedure SaveBrushToStream(ABrush: TdxGPBrush; AWriter: TcxWriter);
    procedure SaveImageToStream(AImage: TdxGPImage; AWriter: TcxWriter);
    procedure SaveSettingsToStream(AWriter: TcxWriter); override;
  public
    constructor Create(ASpreadSheet: TdxCustomSpreadSheet); override;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    //
    property Shape: TdxSpreadSheetShape read FShape write SetShape;
  end;

  { TdxSpreadSheetPicture }

  TdxSpreadSheetPicture = class(TdxSpreadSheetCustomDrawingObject)
  strict private
    FCropMargins: TRect;
    FImageHandle: TdxSpreadSheetSharedImageHandle;

    function GetEmpty: Boolean;
    function GetImage: TdxSmartImage;
    procedure SetCropMargins(const AValue: TRect);
    procedure SetImage(AValue: TdxSmartImage);
    procedure SetImageHandle(const AValue: TdxSpreadSheetSharedImageHandle);
  protected
    property ImageHandle: TdxSpreadSheetSharedImageHandle read FImageHandle write SetImageHandle;
  public
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;

    property CropMargins: TRect read FCropMargins write SetCropMargins;
    property Empty: Boolean read GetEmpty;
    property Image: TdxSmartImage read GetImage write SetImage;
  end;

  { TdxSpreadSheetPictureContainer }

  TdxSpreadSheetPictureContainer = class(TdxSpreadSheetShapeContainer)
  strict private
    FPicture: TdxSpreadSheetPicture;
    FRelativeResize: Boolean;

    procedure PictureChangeHandler(Sender: TObject);
    procedure SetPicture(AValue: TdxSpreadSheetPicture);
  protected
    function CreatePicture: TdxSpreadSheetPicture; virtual;
    function CreateViewInfo: TdxSpreadSheetContainerViewInfo; override;
    procedure LoadFromStream(AReader: TcxReader); override;
    procedure LoadSettingsFromStream(AReader: TcxReader; const AVersion: Word); override;
    procedure SaveSettingsToStream(AWriter: TcxWriter); override;
    procedure SaveToStream(AWriter: TcxWriter); override;
  public
    constructor Create(ASpreadSheet: TdxCustomSpreadSheet); override;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;

    property Picture: TdxSpreadSheetPicture read FPicture write SetPicture;
    property RelativeResize: Boolean read FRelativeResize write FRelativeResize;
  end;

  { TdxSpreadSheetContainers }

  TdxSpreadSheetContainers = class(TObjectList<TdxSpreadSheetContainer>)
  strict private
    FBoundsStorage: TDictionary<TdxSpreadSheetContainer, TRect>;
    FOwner: TdxSpreadSheetCustomView;

    function InternalGetNextVisibleContainer(AStartFromIndex: Integer): TdxSpreadSheetContainer;
    function InternalGetPrevVisibleContainer(AStartFromIndex: Integer): TdxSpreadSheetContainer;
  protected
    procedure CellRemoving(ACell: TdxSpreadSheetCell);
    procedure InternalAdd(AContainer: TdxSpreadSheetContainer);
    procedure InternalRemove(AContainer: TdxSpreadSheetContainer);

    procedure AfterResize;
    procedure BeforeResize;
  public
    constructor Create(AOwner: TdxSpreadSheetCustomView); virtual;
    function Add(AClass: TdxSpreadSheetContainerClass): TdxSpreadSheetContainer;
    function AddPictureContainer: TdxSpreadSheetPictureContainer;
    function AddShapeContainer: TdxSpreadSheetShapeContainer;
    function GetFirstVisibleContainer: TdxSpreadSheetContainer;
    function GetLastVisibleContainer: TdxSpreadSheetContainer;
    function GetNextVisibleContainer(ACurrentContainer: TdxSpreadSheetContainer): TdxSpreadSheetContainer;
    function GetPrevVisibleContainer(ACurrentContainer: TdxSpreadSheetContainer): TdxSpreadSheetContainer;
    function IsAnchorCell(ACell: TdxSpreadSheetCell): Boolean;

    property Owner: TdxSpreadSheetCustomView read FOwner;
  end;

  TdxSpreadSheetForEachCallBack = function(const AValue: Variant; ACanConvertStrToNumber: Boolean;
    var AErrorCode: TdxSpreadSheetFormulaErrorCode; AData, AInfo: Pointer): Boolean;

  { TdxSpreadSheetFormulaToken }

  TdxSpreadSheetFormulaEnumReferencesProc = reference to procedure (const AArea: TRect; ASheet: TdxSpreadSheetTableView);

  TdxSpreadSheetFormulaToken = class
  private
    FChild: TdxSpreadSheetFormulaToken;
    FNext: TdxSpreadSheetFormulaToken;
    FOwner: TdxSpreadSheetFormula;
    FParent: TdxSpreadSheetFormulaToken;
    FPrev: TdxSpreadSheetFormulaToken;
    FIsDimensionCalculated: Boolean;

    function GetChildCount: Integer; inline;
    function GetFirstSibling: TdxSpreadSheetFormulaToken; inline;
    function GetFormatSettings: TdxSpreadSheetFormatSettings; inline;
    function GetHasChildren: Boolean; inline;
    function GetID: Integer;
    function GetItem(AIndex: Integer): TdxSpreadSheetFormulaToken; inline;
    function GetLastChild: TdxSpreadSheetFormulaToken; inline;
    function GetLastSibling: TdxSpreadSheetFormulaToken; inline;
    function GetSiblingCount: Integer; inline;
    function GetSpreadSheet: TdxCustomSpreadSheet; {$IFNDEF VER220} inline; {$ENDIF}
  protected
    FDimension: TdxSpreadSheetFormulaTokenDimension;

    procedure AddResult(AToken: TdxSpreadSheetFormulaToken);
    procedure AttachString(AToken: TdxSpreadSheetFormulaToken; const AValue: TdxSpreadSheetFormulaFormattedText); overload;
    procedure AttachString(AToken: TdxSpreadSheetFormulaToken; const AValue: TdxUnicodeString); overload;
    procedure AttachString(AToken: TdxSpreadSheetFormulaToken; const ABeforeValue: TdxUnicodeString;
      AValue: TdxSpreadSheetFormulaFormattedText; const AAfterValue: TdxUnicodeString); overload;
    function ExtractLastTokenAsString(AList: TdxSpreadSheetFormulaToken): TdxSpreadSheetFormulaFormattedText; virtual;
    function GetExpressionAsText(AExpression: TdxSpreadSheetFormulaToken): TdxSpreadSheetFormulaFormattedText;

    procedure Calculate(AResult: TdxSpreadSheetFormulaResult); virtual;
    procedure CalculateDimension(var ADimension: TdxSpreadSheetFormulaTokenDimension; var AErrorCode: TdxSpreadSheetFormulaErrorCode); virtual;
    procedure ClearIsDimensionCalculated;
    function DoNameChanged(AName: TdxSpreadSheetDefinedName; const ANewName: TdxUnicodeString): Boolean; virtual;
    function ExtractColumn(const AIndex: Integer; var AErrorCode: TdxSpreadSheetFormulaErrorCode): TdxSpreadSheetVector; virtual;
    function ExtractRow(const AIndex: Integer; var AErrorCode: TdxSpreadSheetFormulaErrorCode): TdxSpreadSheetVector; virtual;
    function ExtractValueAsVector(var AErrorCode: TdxSpreadSheetFormulaErrorCode): TdxSpreadSheetVector; virtual;
    function ForEach(AProc: TdxSpreadSheetForEachCallBack; const AData: Pointer; var AErrorCode: TdxSpreadSheetFormulaErrorCode): Boolean; virtual;
    function ForEachCell(ASheet: TdxSpreadSheetTableView; AStartColumn, AStartRow, AFinishColumn, AFinishRow: Integer;
      AProc: TdxSpreadSheetForEachCallBack;const AData: Pointer; var AErrorCode: TdxSpreadSheetFormulaErrorCode): Boolean; inline;
    procedure GetCellValue(ASheet: TdxSpreadSheetTableView; const ARow, AColumn: Integer;
      var AValue: Variant; var AErrorCode: TdxSpreadSheetFormulaErrorCode); inline;
    function GetSheet: TdxSpreadSheetTableView; virtual;
    function GetTokenPriority: Integer; virtual;
    procedure GetValueAsArrayItem(const ARow, AColumn: Integer; var AValue: Variant; var AErrorCode: TdxSpreadSheetFormulaErrorCode); virtual;
    function IsObligatoryDimensionCalculate: Boolean; virtual;
    procedure Offset(DY, DX: Integer); virtual;
    procedure SetNext(ANextToken: TdxSpreadSheetFormulaToken);
    procedure ToString(var AAsText: TdxSpreadSheetFormulaToken); reintroduce; virtual;
    procedure UpdateReferences(AView: TdxSpreadSheetTableView; const AArea: TRect;
      AModification: TdxSpreadSheetCellsModification; AIsDeletion: Boolean; var AModified: Boolean); virtual;

    property SpreadSheet: TdxCustomSpreadSheet read GetSpreadSheet;
  public
    destructor Destroy; override;
    function CanConvertStrToNumber: Boolean; virtual;
    procedure CheckNeighbors; virtual;
    procedure DestroyChildren;
    procedure EnumReferences(AProc: TdxSpreadSheetFormulaEnumReferencesProc); virtual;
    function ExtractFromList: TdxSpreadSheetFormulaToken;
    function GetDimension(var AErrorCode: TdxSpreadSheetFormulaErrorCode): TdxSpreadSheetFormulaTokenDimension;
    procedure GetValue(var AValue: Variant; var AErrorCode: TdxSpreadSheetFormulaErrorCode); virtual;
    function GetValueFromArray(ARowIndex, AColumnIndex: Integer; var AErrorCode: TdxSpreadSheetFormulaErrorCode): Variant;
    procedure GetValueRelatedWithCell(ACell: TdxSpreadSheetCell; var AValue: Variant; var AErrorCode: TdxSpreadSheetFormulaErrorCode); virtual;
    function IsEnumeration: Boolean; virtual;
    procedure LoadFromStream(AReader: TcxReader); virtual; 
    procedure ValidateSheet(var ASheet: TdxSpreadSheetTableView); {$IFNDEF VER220} inline; {$ENDIF}
    procedure ValidateExternalLink(var ALink: TdxSpreadSheetExternalLink); {$IFNDEF VER220} inline; {$ENDIF}
    class procedure Register;

    property ChildCount: Integer read GetChildCount;
    property FirstChild: TdxSpreadSheetFormulaToken read FChild;
    property FirstSibling: TdxSpreadSheetFormulaToken read GetFirstSibling;
    property FormatSettings: TdxSpreadSheetFormatSettings read GetFormatSettings;
    property HasChildren: Boolean read GetHasChildren;
    property ID: Integer read GetID;
    property Items[Index: Integer]: TdxSpreadSheetFormulaToken read GetItem; default;
    property LastChild: TdxSpreadSheetFormulaToken read GetLastChild;
    property LastSibling: TdxSpreadSheetFormulaToken read GetLastSibling;
    property Next: TdxSpreadSheetFormulaToken read FNext;
    property Owner: TdxSpreadSheetFormula read FOwner;
    property Parent: TdxSpreadSheetFormulaToken read FParent;
    property Prev: TdxSpreadSheetFormulaToken read FPrev;
    property Priority: Integer read GetTokenPriority;
    property SiblingCount: Integer read GetSiblingCount;
    property Sheet: TdxSpreadSheetTableView read GetSheet;
  end;

  { TdxSpreadSheetFormulaFormattedText }

  TdxSpreadSheetFormulaFormattedText = class(TdxSpreadSheetFormulaToken)
  strict private
    FRuns: TdxSpreadSheetFormattedSharedStringRuns;
    FValue: TdxUnicodeString;
  public
    constructor Create(const AValue: TdxUnicodeString = '');
    destructor Destroy; override;
    procedure Add(const AValue: TdxSpreadSheetFormulaFormattedText); overload;
    procedure Add(const AValue: TdxUnicodeString); overload;
    procedure AddInBeginning(const AValue: TdxUnicodeString);
    //
    property Runs: TdxSpreadSheetFormattedSharedStringRuns read FRuns;
    property Value: TdxUnicodeString read FValue;
  end;

  { TdxSpreadSheetFormulaResult }

  TdxSpreadSheetFormulaResult = class
  strict private
    FOwner: TdxSpreadSheetFormula;
    FErrorCode: TdxSpreadSheetFormulaErrorCode;
    FValues: TList;
    function GetCount: Integer; inline;
    function GetFormatSettings: TdxSpreadSheetFormatSettings;
    function GetItem(AIndex: Integer): TdxSpreadSheetFormulaToken; inline;
    function GetValue: Variant;
  protected
    function DoExtractDateTimeParameter(var AParameter: Variant; const AParams: TdxSpreadSheetFormulaToken; AIndex: Integer; AWithoutBoolean: Boolean): Boolean;

    property Values: TList read FValues;
  public
    constructor Create(AOwner: TdxSpreadSheetFormula);
    destructor Destroy; override;
    procedure BeforeDestruction; override;

    procedure Add(AValue: TdxSpreadSheetFormulaToken); inline;
    procedure AddValue(const AValue: Variant); inline;
    procedure AddResultValue(const AValue: TdxSpreadSheetFormulaResult); inline;
    procedure CheckValue;
    procedure Clear;
    procedure ClearValues;
    function ConvertToNumeric(var AValue: Variant; ACanConvertStr, AWithoutBoolean: Boolean): Boolean; inline;
    function ExtractCondition(AParams: TdxSpreadSheetFormulaToken; ACount, AIndex: Integer; var AOperation: TdxSpreadSheetFormulaOperation): Variant;
    //
    function ExtractColumnFromRange(const ARange: TdxSpreadSheetFormulaToken; AColumnIndex: Integer; var AErrorCode: TdxSpreadSheetFormulaErrorCode): TdxSpreadSheetVector; inline;
    function ExtractRowFromRange(const ARange: TdxSpreadSheetFormulaToken; ARowIndex: Integer; var AErrorCode: TdxSpreadSheetFormulaErrorCode): TdxSpreadSheetVector; inline;

    function ExtractDateTimeOnlyParameter(var AParameter: Variant; const AParams: TdxSpreadSheetFormulaToken; AIndex: Integer = 0): Boolean; inline;
    function ExtractDateTimeParameter(var AParameter: Variant; const AParams: TdxSpreadSheetFormulaToken; AIndex: Integer = 0): Boolean; inline;
    function ExtractDateTimeParameterWithoutBoolean(var AParameter: Variant; const AParams: TdxSpreadSheetFormulaToken; AIndex: Integer = 0): Boolean;
    function ExtractErrorCode(const AParams: TdxSpreadSheetFormulaToken): TdxSpreadSheetFormulaErrorCode; overload; inline;
    function ExtractNumericValue(var AValue: Variant): Boolean; inline;
    function ExtractNumericParameter(var AParameter: Variant; const AParams: TdxSpreadSheetFormulaToken; AIndex: Integer = 0): Boolean; overload; inline;
    function ExtractNumericParameterDef(var AParameter: Variant; const ADefaultIfNoExist, ADefaultIfNull: Variant; const AParams: TdxSpreadSheetFormulaToken; AIndex: Integer = 0): Boolean; overload; inline;
    function ExtractNumericParameterDef(var AParameter: Variant; const ADefaultIfNull: Variant; const AParams: TdxSpreadSheetFormulaToken; AIndex: Integer = 0): Boolean; overload; inline;
    function ExtractNumericParameterDefWithoutBoolean(var AParameter: Variant; const ADefault: Variant; const AParams: TdxSpreadSheetFormulaToken; AIndex: Integer = 0): Boolean; inline;
    function ExtractNumericParameterWithoutBoolean(var AParameter: Variant; const AParams: TdxSpreadSheetFormulaToken; AIndex: Integer = 0): Boolean; inline;
    function ExtractParameter(var AParameter: Variant; const AParams: TdxSpreadSheetFormulaToken; AIndex: Integer = 0): Boolean; overload; inline;
    function ExtractParameter(var AParameter: Variant; out ACanConvertStrToNumber: Boolean; const AParams: TdxSpreadSheetFormulaToken; AIndex: Integer = 0): Boolean; overload; inline;
    function ExtractParameterDef(var AParameter: Variant; ADefaultIfNull: Variant; const AParams: TdxSpreadSheetFormulaToken; AIndex: Integer = 0): Boolean; overload; inline;
    function ExtractParameterDef(var AParameter: Variant; ADefaultIfNoExist, ADefaultIfNull: Variant; const AParams: TdxSpreadSheetFormulaToken; AIndex: Integer = 0): Boolean; overload; inline;
    function ExtractStringValue: Variant; inline;
    function ExtractStringParameter(const AParams: TdxSpreadSheetFormulaToken; AIndex: Integer = 0): Variant; inline;
    function ExtractValue(out ACanConvertStrToNumber: Boolean): Variant; inline;
    function ExtractValueToken: TdxSpreadSheetFormulaToken; inline;
    procedure ForEach(AParams: TdxSpreadSheetFormulaToken; AProc: TdxSpreadSheetForEachCallBack; const AData: Pointer); overload;
    function GetParamsCount(const AParams: TdxSpreadSheetFormulaToken): Integer; inline;
    function ParameterExists(const AParams: TdxSpreadSheetFormulaToken; AIndex: Integer): Boolean; inline;
    function ParameterIsNull(var AParameter: Variant; const AParams: TdxSpreadSheetFormulaToken; AIndex: Integer = 0): Boolean; overload; inline;
    function ParameterIsNull(var AParameter: Variant; out ACanConvertStrToNumber: Boolean; const AParams: TdxSpreadSheetFormulaToken; AIndex: Integer = 0): Boolean; overload; inline;
    procedure SetError(ACode: TdxSpreadSheetFormulaErrorCode); inline;
    function Validate: Boolean; inline;

    property Count: Integer read GetCount;
    property ErrorCode: TdxSpreadSheetFormulaErrorCode read FErrorCode;
    property FormatSettings: TdxSpreadSheetFormatSettings read GetFormatSettings;
    property Items[Index: Integer]: TdxSpreadSheetFormulaToken read GetItem; default;
    property Owner: TdxSpreadSheetFormula read FOwner;
    property Value: Variant read GetValue;
  end;

  { TdxSpreadSheetFormula }

  TdxSpreadSheetFormula = class
  strict private
    FArrayFormulaSize: TSize;
    FErrorCode: TdxSpreadSheetFormulaErrorCode;
    FErrorIndex: Integer;
    FIsArrayFormula: Boolean;
    FIteration: Integer;
    FSourceText: TdxUnicodeString;

    function GetAsText: TdxUnicodeString;
    function GetFormatSettings: TdxSpreadSheetFormatSettings;
    procedure SetIsArrayFormula(AValue: Boolean);
  protected
    FOwner: TdxSpreadSheetCell;
    FResultValue: TdxSpreadSheetFormulaResult;
    FTokens: TdxSpreadSheetFormulaToken;

    procedure Add(APrev, AToken: TdxSpreadSheetFormulaToken);
    procedure AddLast(APrev, AToken: TdxSpreadSheetFormulaToken);
    procedure AddChild(AParent, AToken: TdxSpreadSheetFormulaToken);
    function Calculate(AExpression: TdxSpreadSheetFormulaToken): TdxSpreadSheetFormulaResult;
    procedure ClearResult; inline;
    procedure DestroyTokens(var ATokens: TdxSpreadSheetFormulaToken);
    procedure DoNameChanged(AName: TdxSpreadSheetDefinedName; const ANewName: TdxUnicodeString);
    procedure RemoveReferenceFromCell; virtual;
    function GetExpressionAsText(AExpression: TdxSpreadSheetFormulaToken): TdxSpreadSheetFormulaFormattedText; virtual;
    function GetColumn: Integer; virtual;
    function GetController: TdxSpreadSheetFormulaController; virtual;
    function GetMaxIterationCount: Integer; inline;
    function GetResultValue: TdxSpreadSheetFormulaResult;
    function GetRow: Integer; virtual;
    function GetSheet: TdxSpreadSheetTableView; virtual;
    function GetValue: Variant; virtual;
    function IsCaptionTextDelimited(ASheet: TdxSpreadSheetTableView): Boolean;
    procedure Offset(DY, DX: Integer);
    procedure SetArrayFormulaSize(const ASize: TSize);
    procedure SetError(AErrorCode: TdxSpreadSheetFormulaErrorCode; AErrorIndex: Integer);
    procedure SetAsOwner(AToken: TdxSpreadSheetFormulaToken);
    procedure SetOwner(ACell: TdxSpreadSheetCell);
    function UpdateReferences(AView: TdxSpreadSheetTableView; const AArea: TRect;
      AModification: TdxSpreadSheetCellsModification; AIsDeletion: Boolean): Boolean; virtual;
    //
    procedure LoadFromStream(AReader: TcxReader); 
  public
    constructor Create(AOwner: TdxSpreadSheetCell); virtual;
    destructor Destroy; override;
    function Clone: TdxSpreadSheetFormula;
    procedure EnumReferences(AProc: TdxSpreadSheetFormulaEnumReferencesProc);

    property AsText: TdxUnicodeString read GetAsText;
    property ArrayFormulaSize: TSize read FArrayFormulaSize;
    property Cell: TdxSpreadSheetCell read FOwner;
    property Column: Integer read GetColumn;
    property Controller: TdxSpreadSheetFormulaController read GetController;
    property ErrorCode: TdxSpreadSheetFormulaErrorCode read FErrorCode;
    property ErrorIndex: Integer read FErrorIndex;
    property FormatSettings: TdxSpreadSheetFormatSettings read GetFormatSettings;
    property IsArrayFormula: Boolean read FIsArrayFormula write SetIsArrayFormula;
    property Iteration: Integer read FIteration;
    property ResultValue: TdxSpreadSheetFormulaResult read GetResultValue;
    property Row: Integer read GetRow;
    property Sheet: TdxSpreadSheetTableView read GetSheet;
    property SourceText: TdxUnicodeString read FSourceText write FSourceText;
    property Tokens: TdxSpreadSheetFormulaToken read FTokens;
    property Value: Variant read GetValue;
  end;

  { TdxSpreadSheetFormulaAsTextInfo }

  TdxSpreadSheetFormulaAsTextInfo = class(TObject)
  public
    Anchor: TPoint;
    Cell: TdxSpreadSheetCell;
    Formula: TdxUnicodeString;
    IsArray: Boolean;
    IsShared: Boolean;
    Reference: TRect;

    constructor Create(AFormula: TdxSpreadSheetFormula = nil);
    procedure LoadFromStream(AReader: TcxReader);
    procedure SaveToStream(AWriter: TcxWriter);
  end;

  { TdxSpreadSheetFormulaAsTextInfoList }

  TdxSpreadSheetFormulaAsTextInfoList = class(TObjectList<TdxSpreadSheetFormulaAsTextInfo>)
  strict private
    FSpreadSheet: TdxCustomSpreadSheet;
  protected
    function CreateParser: TObject; virtual; // TdxSpreadSheetFormulaParser
  public
    constructor Create(ASpreadSheet: TdxCustomSpreadSheet); virtual;
    procedure Add(ACell: TdxSpreadSheetCell; const AFormula: TdxUnicodeString;
      AIsArray, AIsShared: Boolean; const AReference: TRect); overload;
    procedure AddFromStream(ACell: TdxSpreadSheetCell; AReader: TcxReader);
    function HasCell(ACell: TdxSpreadSheetCell): Boolean;
    procedure ResolveReferences;
    //
    property SpreadSheet: TdxCustomSpreadSheet read FSpreadSheet;
  end;

  { TdxSpreadSheetDefinedNameFormula }

  TdxSpreadSheetDefinedNameFormula = class(TdxSpreadSheetFormula)
  strict private
    FOwner: TdxSpreadSheetDefinedName;
  protected
    function GetColumn: Integer; override;
    function GetController: TdxSpreadSheetFormulaController; override;
    function GetRow: Integer; override;
    function GetSheet: TdxSpreadSheetTableView; override;
    procedure RemoveReferenceFromCell; override;
  public
    constructor Create(AOwner: TdxSpreadSheetDefinedName); reintroduce; overload;

    property Owner: TdxSpreadSheetDefinedName read FOwner;
  end;

  { TdxSpreadSheetControlFormatSettings }

  TdxSpreadSheetControlFormatSettings = class(TdxSpreadSheetFormatSettings)
  strict private
    FOwner: TdxCustomSpreadSheet;
  protected
    function GetLocaleID: Integer; override;
  public
    constructor Create(AOwner: TdxCustomSpreadSheet); virtual;
    function ExpandExternalLinks: Boolean; override;
    function GetFunctionName(const AName: Pointer): TdxUnicodeString; override;
    procedure UpdateSettings; override;

    property Owner: TdxCustomSpreadSheet read FOwner;
  end;

  { TdxSpreadSheetFormulaController }

  TdxSpreadSheetFormulaController = class(TcxObjectList, IUnknown, IdxLocalizerListener, IcxFormatControllerListener)
  strict private
    FCalculationInProcess: Boolean;
    FCircularPath: TdxSpreadSheetReferencePath;
    FCircularReference: Boolean;
    FCircularReferencePaths: TcxObjectList;
    FHasCircularReferences: Boolean;
    FOwner: TdxCustomSpreadSheet;
    function GetCircularReferencePathsAsString: TdxUnicodeString;
    function GetFormatSettings: TdxSpreadSheetFormatSettings;
    function GetItem(AIndex: Integer): TdxSpreadSheetFormula;
  protected
    procedure AddPath(ARow, AColumn: Integer; ASheet: TObject);
    procedure CheckCircularReferences;
    function CircularPathToString(APath: TdxSpreadSheetReferencePath): TdxUnicodeString;
    procedure DoNameChanged(AName: TdxSpreadSheetDefinedName; const ANewName: TdxUnicodeString);
    function GetCellFromPath(APath: TdxSpreadSheetReferencePath): TdxSpreadSheetCell;
    procedure InitializeStrings; virtual;
    function NeedRecalcuate: Boolean;
    procedure Recalculate;
    procedure RemovePath(ARow, AColumn: Integer; ASheet: TObject);
    //
    function _AddRef: Integer; stdcall;
    function _Release: Integer; stdcall;
    function QueryInterface(const IID: TGUID; out Obj): HResult; virtual; stdcall;
    // IdxLocalizerListener
    procedure TranslationChanged;
    // IcxFormatControllerListener
    procedure FormatChanged;

    property CircularPath: TdxSpreadSheetReferencePath read FCircularPath;
    property CircularReference: Boolean read FCircularReference write FCircularReference;
    property HasCircularReferences: Boolean read FHasCircularReferences write FHasCircularReferences;
  public
    constructor Create(AOwner: TdxCustomSpreadSheet); virtual;
    destructor Destroy; override;
    procedure Calculate;
    procedure UpdateReferences(AView: TdxSpreadSheetTableView; const AArea: TRect;
      AModification: TdxSpreadSheetCellsModification; AIsDeletion: Boolean);

    property CalculationInProcess: Boolean read FCalculationInProcess;
    property CircularReferencePaths: TcxObjectList read FCircularReferencePaths;
    property CircularReferencePathsAsString: TdxUnicodeString read GetCircularReferencePathsAsString;
    property FormatSettings: TdxSpreadSheetFormatSettings read GetFormatSettings;
    property Items[Index: Integer]: TdxSpreadSheetFormula read GetItem; default;
    property SpreadSheet: TdxCustomSpreadSheet read FOwner;
  end;

  { TdxSpreadSheetCustomController }

  TdxSpreadSheetCustomController = class(TObject)
  strict private
    FOwner: TObject;
    FPopupMenu: TComponent;
  protected
    function ContextPopup(const P: TPoint): Boolean; virtual;

    function CanFocusOnClick: Boolean; virtual;
    procedure DblClick; virtual;
    function GetCursor(const ACursorPos: TPoint): TCursor; virtual;
    function GetDragAndDropObjectClass(const ACursorPos: TPoint): TcxDragAndDropObjectClass; virtual;
    function GetHitTest: TdxSpreadSheetCustomHitTest; virtual;
    function GetPopupMenuClass(const ACursorPos: TPoint): TComponentClass; virtual;
    function GetSpreadSheet: TdxCustomSpreadSheet; virtual; abstract;
    function GetZoomFactor: Integer; virtual;

    procedure KeyDown(var Key: Word; Shift: TShiftState); virtual;
    procedure KeyUp(var Key: Word; Shift: TShiftState); virtual;
    procedure KeyPress(var Key: Char); virtual;

    procedure DoMouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); virtual;
    procedure DoMouseMove(Shift: TShiftState; X, Y: Integer); virtual;
    procedure DoMouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); virtual;
    function DoMouseWheel(Shift: TShiftState; WheelDelta: Integer; const P: TPoint): Boolean; virtual;

    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); 
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); 
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    function MouseWheel(Shift: TShiftState; WheelDelta: Integer; const P: TPoint): Boolean;

    property PopupMenu: TComponent read FPopupMenu;
    property SpreadSheet: TdxCustomSpreadSheet read GetSpreadSheet;
  public
    constructor Create(AOwner: TObject); virtual;
    destructor Destroy; override;

    property HitTest: TdxSpreadSheetCustomHitTest read GetHitTest;
    property Owner: TObject read FOwner;
    property ZoomFactor: Integer read GetZoomFactor;
  end;

  { TdxSpreadSheetCustomViewController }

  TdxSpreadSheetCustomViewController = class(TdxSpreadSheetCustomController)
  strict private
    FFocusedContainer: TdxSpreadSheetContainer;
    function GetContainer: TdxSpreadSheetContainers;
    function GetView: TdxSpreadSheetCustomView;
    procedure SetFocusedContainer(AValue: TdxSpreadSheetContainer);
  protected
    procedure CheckScrollArea(const P: TPoint); overload; virtual;
    procedure CheckScrollArea(X, Y: Integer); overload; inline;
    function ContainerCanDelete: Boolean; virtual;
    function ContainerProcessKeyDown(Key: Word; Shift: TShiftState): Boolean; virtual;
    function GetHitTest: TdxSpreadSheetCustomHitTest; override;
    function GetSpreadSheet: TdxCustomSpreadSheet; override;
    function GetZoomFactor: Integer; override;
    procedure GetRedoActionCount(var ARedoCount: Integer); virtual;
    procedure GetUndoActionCount(var AUndoCount: Integer); virtual;
    function Redo(const ARedoCount: Integer = 1): Boolean; virtual;
    function Undo(const AUndoCount: Integer = 1): Boolean; virtual;
  public
    property Container: TdxSpreadSheetContainers read GetContainer;
    property FocusedContainer: TdxSpreadSheetContainer read FFocusedContainer write SetFocusedContainer;
    property View: TdxSpreadSheetCustomView read GetView;
  end;

  { TdxSpreadSheetCustomHitTest }

  TdxSpreadSheetCustomHitTest = class
  strict private
    FActualHitPoint: TPoint;
    FHitCode: Integer;
    FHitObject: TdxSpreadSheetCustomCellViewInfo;
    FHitObjectData: TdxNativeInt;
    FHitPoint: TPoint;
    FOwner: TObject;
  protected
    function CanDrag(const P: TPoint): Boolean; inline;
    function GetActualHitPoint: TPoint; virtual;
    function GetCursor(const P: TPoint): TCursor; inline;
    function GetDragAndDropObjectClass: TcxDragAndDropObjectClass; overload; inline;
    function GetDragAndDropObjectClass(const P: TPoint): TcxDragAndDropObjectClass; overload; inline;
    function GetHitCode(ACode: Integer): Boolean; inline;
    procedure SetHitCode(ACode: Integer; AValue: Boolean); inline;

    property ActualHitPoint: TPoint read FActualHitPoint;
    property HitCodes[ACode: Integer]: Boolean read GetHitCode write SetHitCode;
    property Owner: TObject read FOwner;
  public
    constructor Create(AOwner: TObject); virtual;
    procedure Calculate(const AHitPoint: TPoint); virtual;
    procedure Clear;
    function GetPopupMenuClass(const P: TPoint): TComponentClass;
    procedure Recalculate;

    property HitObject: TdxSpreadSheetCustomCellViewInfo read FHitObject write FHitObject;
    property HitObjectData: TdxNativeInt read FHitObjectData write FHitObjectData;
    property HitPoint: TPoint read FHitPoint;
  end;

  { TdxSpreadSheetCustomViewOptions }

  TdxSpreadSheetCustomViewOptions = class(TcxOwnedPersistent)
  strict private
    FProtected: Boolean;
    function GetView: TdxSpreadSheetCustomView;
    procedure SetProtected(AValue: Boolean);
  protected
    procedure Changed; virtual;
    //
    property Protected: Boolean read FProtected write SetProtected default False;
    property View: TdxSpreadSheetCustomView read GetView;
  public
    procedure Assign(Source: TPersistent); override;
  end;

  { TdxSpreadSheetCustomView }

  TdxSpreadSheetCustomView = class(TdxSpreadSheetPersistentObject)
  strict private
    FCaption: TdxUnicodeString;
    FChanges: TdxSpreadSheetChanges;
    FContainers: TdxSpreadSheetContainers;
    FController: TdxSpreadSheetCustomViewController;
    FHitTest: TdxSpreadSheetCustomHitTest;
    FIsCaptionTextDelimited: Boolean;
    FIsDestroying: Boolean;
    FLockCount: Integer;
    FOptions: TdxSpreadSheetCustomViewOptions;
    FViewInfo: TdxSpreadSheetCustomViewViewInfo;
    FVisible: Boolean;

    function GetActive: Boolean;
    function GetBounds: TRect;
    function GetCellStyles: TdxSpreadSheetCellStyles;
    function GetFormatSettings: TdxSpreadSheetFormatSettings;
    function GetIndex: Integer;
    function GetIsDestroying: Boolean;
    function GetIsLocked: Boolean;
    procedure SetActive(AValue: Boolean);
    procedure SetCaption(const AValue: TdxUnicodeString);
    procedure SetIndex(AValue: Integer);
    procedure SetOptions(AValue: TdxSpreadSheetCustomViewOptions);
    procedure SetVisible(AValue: Boolean);
  protected
    FMergeCellStylesLockCount: Integer;

    procedure AddChanges(AChanges: TdxSpreadSheetChanges); inline;
    procedure AfterLoad; virtual;
    procedure Changed; virtual;
    procedure CheckChanges;
    procedure DoCheckChanges; virtual;
    procedure InitScrollBarsParameters; virtual;
    procedure SelectionChanged; virtual;
    procedure ValidateCaption; virtual;

    function CreateContainers: TdxSpreadSheetContainers; virtual;
    function CreateController: TdxSpreadSheetCustomViewController; virtual;
    function CreateHitTest: TdxSpreadSheetCustomHitTest; virtual;
    function CreateOptions: TdxSpreadSheetCustomViewOptions; virtual;
    function CreateViewInfo: TdxSpreadSheetCustomViewViewInfo; virtual;
    //
    procedure CreateSubClasses; virtual;
    procedure DestroySubClasses; virtual;
    //
    function AbsoluteToScreen(const R: TRect): TRect; virtual;
    function GetContentOrigin: TPoint; virtual;
    function GetPartOffsetByPoint(const P: TPoint): TPoint; virtual;
    function GetZoomFactor: Integer; virtual;
    //
    procedure Pack; virtual;
    procedure Scroll(AScrollBarKind: TScrollBarKind; AScrollCode: TScrollCode; var AScrollPos: Integer); virtual;
    procedure ValidateRect(const R: TRect);

    property Changes: TdxSpreadSheetChanges read FChanges write FChanges;
    property Controller: TdxSpreadSheetCustomViewController read FController;
    property FormatSettings: TdxSpreadSheetFormatSettings read GetFormatSettings;
    property IsCaptionTextDelimited: Boolean read FIsCaptionTextDelimited;
    property IsDestroying: Boolean read GetIsDestroying;
    property IsLocked: Boolean read GetIsLocked;
    property LockCount: Integer read FLockCount;
    property Options: TdxSpreadSheetCustomViewOptions read FOptions write SetOptions;
    property ViewInfo: TdxSpreadSheetCustomViewViewInfo read FViewInfo;
    property ZoomFactor: Integer read GetZoomFactor;
  public
    constructor Create(AOwner: TdxCustomSpreadSheet); override;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    procedure BeforeDestruction; override;
    procedure BeginUpdate;
    procedure EndUpdate;
    procedure Invalidate;
    procedure InvalidateRect(const R: TRect);

    property Active: Boolean read GetActive write SetActive stored False;
    property Bounds: TRect read GetBounds;
    property Caption: TdxUnicodeString read FCaption write SetCaption;
    property CellStyles: TdxSpreadSheetCellStyles read GetCellStyles;
    property Containers: TdxSpreadSheetContainers read FContainers;
    property HitTest: TdxSpreadSheetCustomHitTest read FHitTest;
    property Index: Integer read GetIndex write SetIndex stored False;
    property Visible: Boolean read FVisible write SetVisible default True;
  end;

  { TdxSpreadSheetCustomViewViewInfo }

  TdxSpreadSheetCustomViewViewInfo = class(TdxSpreadSheetViewPersistentObject)
  strict private
    FContainers: TdxSpreadSheetViewInfoCellsList;
    FIsDirty: Boolean;

    procedure SetIsDirty(AValue: Boolean);
  protected
    FBounds: TRect;

    procedure CalculateContainers; virtual;
    function CalculateContainersHitTestWithOriginOffset(
      AHitTest: TdxSpreadSheetCustomHitTest; const AOffset: TPoint): Boolean; virtual;
    procedure InitScrollBarsParameters; virtual;
    procedure Scroll(AScrollBarKind: TScrollBarKind; AScrollCode: TScrollCode; var AScrollPos: Integer); virtual;
    procedure SelectionChanged; virtual;
  public
    constructor Create(AView: TdxSpreadSheetCustomView); override;
    destructor Destroy; override;
    procedure Calculate; virtual;
    procedure Clear; virtual;
    procedure Draw(ACanvas: TcxCanvas); virtual;
    procedure Recalculate;
    procedure Validate;

    property Bounds: TRect read FBounds write FBounds;
    property Containers: TdxSpreadSheetViewInfoCellsList read FContainers;
    property IsDirty: Boolean read FIsDirty write SetIsDirty;
  end;

  { TdxSpreadSheetMergedCellsRowItem }

  TdxSpreadSheetMergedCellsRowItem = class(TcxDoublyLinkedObject)
  public
    Cell: TdxSpreadSheetMergedCell;
  end;

  { TdxSpreadSheetMergedCellsRow }

  TdxSpreadSheetMergedCellsRow = class(TcxDoublyLinkedObjectList)
  protected
    function CreateLinkedObject: TcxDoublyLinkedObject; override;
  public
    procedure AddCell(ACell: TdxSpreadSheetMergedCell);
    procedure RemoveCell(ACell: TdxSpreadSheetMergedCell);
    procedure ExtractCells(AColumn1, AColumn2: Integer; const ADest: TList<TdxSpreadSheetMergedCell>); inline;
    procedure Union(var AArea: TRect);
    function Contains(AColumn: Integer): TdxSpreadSheetMergedCell; inline;
  end;

  { TdxSpreadSheetMergedCell }

  TdxSpreadSheetMergedCell = class(TcxDoublyLinkedObject)
  strict private
    FArea: TRect;
    FOwner: TdxSpreadSheetMergedCellList;

    function GetActiveCell: TdxSpreadSheetCell;
    function GetHistory: TdxSpreadSheetHistory; inline;
    function GetNext: TdxSpreadSheetMergedCell; inline;
    function GetPrev: TdxSpreadSheetMergedCell; inline;
    function GetView: TdxSpreadSheetTableView;
    procedure SetNext(const Value: TdxSpreadSheetMergedCell); inline;
    procedure SetPrev(const Value: TdxSpreadSheetMergedCell); inline;
  protected
    procedure Initialize(AOwner: TdxSpreadSheetMergedCellList; const AArea: TRect);

    property History: TdxSpreadSheetHistory read GetHistory;
  public
    destructor Destroy; override;
    function Contains(const ARow, AColumn: Integer): Boolean;
    function Intersects(const AArea: TRect): Boolean;

    property ActiveCell: TdxSpreadSheetCell read GetActiveCell;
    property Area: TRect read FArea;
    property Next: TdxSpreadSheetMergedCell read GetNext write SetNext;
    property Owner: TdxSpreadSheetMergedCellList read FOwner;
    property Prev: TdxSpreadSheetMergedCell read GetPrev write SetPrev;
    property View: TdxSpreadSheetTableView read GetView;
  end;

  { TdxSpreadSheetMergedCellList }

  TdxSpreadSheetMergedCellList = class(TcxDoublyLinkedObjectList)
  strict private
    FCount: Integer;
    FIsDeleting: Boolean;
    FView: TdxSpreadSheetTableView;

    function GetFirst: TdxSpreadSheetMergedCell; inline;
    function GetHistory: TdxSpreadSheetHistory; inline;
    function GetItem(AIndex: Integer): TdxSpreadSheetMergedCell;
    function GetLast: TdxSpreadSheetMergedCell; inline;
  protected
    CachedCell: TdxSpreadSheetMergedCell;
    CachedExpandedArea: TRect;
    CachedPoint: TPoint;
    CachedSourceArea: TRect;
    Rows: TObjectDictionary<Integer, TdxSpreadSheetMergedCellsRow>;
    procedure Changed; inline;
    procedure ClearCacheInformation; inline;
    function CreateLinkedObject: TcxDoublyLinkedObject; override;
    procedure DoRemoving(ACell: TdxSpreadSheetMergedCell);
    function Find(ACell: TdxSpreadSheetMergedCell): Integer;
    function FindCell(ARow, AColumn: Integer): TdxSpreadSheetMergedCell;
    procedure InitializeCell(ACell: TdxSpreadSheetMergedCell; const AArea: TRect);
    //
    procedure AddRef(ACell: TdxSpreadSheetMergedCell);
    procedure RemoveRef(ACell: TdxSpreadSheetMergedCell);

    property History: TdxSpreadSheetHistory read GetHistory;
  public
    constructor Create(AView: TdxSpreadSheetTableView); virtual;
    destructor Destroy; override;
    procedure Add(const AArea: TRect); reintroduce;
    function CheckCell(ARow, AColumn: Integer): TRect;
    procedure Clear; override;
    procedure Delete(ALinkedObject: TcxDoublyLinkedObject); override;
    procedure DeleteItemsInArea(const AArea: TRect);
    function ExpandArea(const AArea: TRect): TRect; overload;
    function ExpandArea(const AColumn, ARow: Integer): TRect; overload;
    procedure Extract(const ABounds: TRect; ADest: TList<TdxSpreadSheetMergedCell>); reintroduce; overload;
    function HasItemsInArea(const AArea: TRect): Boolean;
    //
    property Count: Integer read FCount;
    property First: TdxSpreadSheetMergedCell read GetFirst;
    property Items[Index: Integer]: TdxSpreadSheetMergedCell read GetItem; default;
    property Last: TdxSpreadSheetMergedCell read GetLast;
    property View: TdxSpreadSheetTableView read FView;
  end;

  { TdxSpreadSheetTableItem }

  TdxSpreadSheetTableItem = class(TdxDynamicListItem)
  strict private
    FDefaultSize: Boolean;
    FIsCustomSize: Boolean;
    FSize: Integer;
    FStyle: TdxSpreadSheetCellStyle;
    FVisible: Boolean;

    function GetHistory: TdxSpreadSheetHistory;
    function GetNext: TdxSpreadSheetTableItem;
    function GetOwner: TdxSpreadSheetTableItems;
    function GetPrev: TdxSpreadSheetTableItem;
    function GetSize: Integer;
    function GetView: TdxSpreadSheetTableView;
    procedure SetDefaultSize(AValue: Boolean);
    procedure SetVisible(AValue: Boolean);
  protected
    procedure AfterResize;
    procedure BeforeResize;
    function CanMeasureCellSize(ACell: TdxSpreadSheetCell): Boolean;
    procedure Changed;
    function GetCell(AIndex: Integer): TdxSpreadSheetCell; virtual; abstract;
    function GetCellCount: Integer; virtual; abstract;
    function GetDisplayText: string; virtual;
    function IsItemArea(const AArea: TRect): Boolean; virtual; abstract;
    function IsValueDefined(AIndex: Integer): Boolean; inline;
    procedure MarkBestFitDirty; virtual;
    function MeasureCellSize(ACanvas: TcxCanvas; ACell: TdxSpreadSheetCell): Integer; virtual; abstract;
    procedure SetSize(const AValue: Integer); virtual;
    procedure SetSizeEx(const ASize: TSize); virtual;
    procedure ProcessStyleChanges(APrevStyle, ANewStyle: TdxSpreadSheetCellStyleHandle); virtual;
    //
    procedure Restore(AReader: TcxReader); virtual;
    procedure Store(AWriter: TcxWriter); virtual;
    //
    property History: TdxSpreadSheetHistory read GetHistory;
    property IsCustomSize: Boolean read FIsCustomSize write FIsCustomSize;
  public
    constructor Create(AOwner: TdxDynamicItemList; AIndex: Integer); override;
    destructor Destroy; override;
    procedure ApplyBestFit; virtual;
    procedure BeforeDestruction; override;
    function CreateCell(AIndex: Integer): TdxSpreadSheetCell; virtual; abstract;

    property CellCount: Integer read GetCellCount;
    property Cells[Index: Integer]: TdxSpreadSheetCell read GetCell;
    property DisplayText: string read GetDisplayText;
    property DefaultSize: Boolean read FDefaultSize write SetDefaultSize;
    property Next: TdxSpreadSheetTableItem read GetNext;
    property Owner: TdxSpreadSheetTableItems read GetOwner;
    property Prev: TdxSpreadSheetTableItem read GetPrev;
    property Size: Integer read GetSize write SetSize;
    property Style: TdxSpreadSheetCellStyle read FStyle;
    property View: TdxSpreadSheetTableView read GetView;
    property Visible: Boolean read FVisible write SetVisible;
  end;

   { TdxSpreadSheetTableItems }

  TdxSpreadSheetTableItems = class(TdxDynamicItemList)
  strict private
    FView: TdxSpreadSheetTableView;

    function GetFirst: TdxSpreadSheetTableItem;
    function GetItem(AIndex: Integer): TdxSpreadSheetTableItem; inline;
    function GetLast: TdxSpreadSheetTableItem;
    function GetSpreadSheet: TdxCustomSpreadSheet;
    procedure SetItem(AIndex: Integer; const AValue: TdxSpreadSheetTableItem); {$IFNDEF VER220} inline; {$ENDIF}
  protected
    procedure AfterResize; virtual;
    procedure BeforeResize; virtual;
    procedure Changed; virtual;
    function GetDefaultSize: Integer; virtual; abstract;
    function GetFixedItemIndex: Integer; virtual; abstract;
    function GetItemDisplayText(const AItem: TdxSpreadSheetTableItem): TdxUnicodeString; virtual;
    function GetItemSize(AIndex: Integer): Integer; inline;
    function GetItemState(AIndex: Integer): TcxButtonState; virtual;
    function GetItemStyle(AIndex: Integer): TdxSpreadSheetCellStyle; virtual;
    function GetItemText(AIndex: Integer): TdxUnicodeString; virtual;
    function GetItemVisible(AIndex: Integer): Boolean;
    function GetMaxItemIndex: Integer; virtual;
    function GetOppositeItems: TdxSpreadSheetTableItems; virtual; abstract;
    procedure ResizeItem(AIndex, ADelta: Integer); virtual;
    procedure SetDefaultSize(const AValue: Integer); virtual; abstract;

    property SpreadSheet: TdxCustomSpreadSheet read GetSpreadSheet;
  public
    constructor Create(AOwner: TdxSpreadSheetTableView); virtual;
    function CreateItem(const AIndex: Integer): TdxSpreadSheetTableItem;
    function GetDistance(AStartIndex, AFinishIndex: Integer): Integer;
    function GetItemIndexFromDistance(AStartIndex: Integer; ADistance: Integer): Integer;
    function GetNextVisibleItemIndex(AIndex: Integer; AGoForward: Boolean): Integer;
    function GetPosition(AIndex: Integer): Integer;

    property DefaultSize: Integer read GetDefaultSize write SetDefaultSize;
    property First: TdxSpreadSheetTableItem read GetFirst;
    property Items[Index: Integer]: TdxSpreadSheetTableItem read GetItem write SetItem; default;
    property Last: TdxSpreadSheetTableItem read GetLast;
    property View: TdxSpreadSheetTableView read FView;
  end;

  { TdxSpreadSheetTableItemStyle }

  TdxSpreadSheetTableItemStyle = class(TdxSpreadSheetCellStyle)
  strict private
    function GetOwner: TdxSpreadSheetTableItem; inline;
    function GetView: TdxSpreadSheetTableView; inline;
  protected
    procedure Changed; override;
    function GetSpreadSheet: TdxCustomSpreadSheet; override;
    procedure ProcessStyleChanges(APrevStyle, ANewStyle: TdxSpreadSheetCellStyleHandle); override;
  public
    property Owner: TdxSpreadSheetTableItem read GetOwner;
    property View: TdxSpreadSheetTableView read GetView;
  end;

  { TdxSpreadSheetTableColumn }

  TdxSpreadSheetTableColumn = class(TdxSpreadSheetTableItem)
  protected
    function GetCell(ARow: Integer): TdxSpreadSheetCell; override;
    function GetCellCount: Integer; override;
    function IsItemArea(const AArea: TRect): Boolean; override;
    function MeasureCellSize(ACanvas: TcxCanvas; ACell: TdxSpreadSheetCell): Integer; override;
    procedure ShiftIndex(ADelta: Integer); override;
  public
    function CreateCell(ARow: Integer): TdxSpreadSheetCell; override;
  end;

  { TdxSpreadSheetTableColumns }

  TdxSpreadSheetTableColumns = class(TdxSpreadSheetTableItems)
  strict private
    function GetItem(AIndex: Integer): TdxSpreadSheetTableColumn;
  protected
    function GetDefaultSize: Integer; override;
    function GetFixedItemIndex: Integer; override;
    function GetItemClass: TdxDynamicListItemClass; override;
    function GetItemText(AIndex: Integer): TdxUnicodeString; override;
    function GetItemState(AIndex: Integer): TcxButtonState; override;
    function GetMaxItemIndex: Integer; override;
    function GetOppositeItems: TdxSpreadSheetTableItems; override;
    procedure SetDefaultSize(const AValue: Integer); override;
  public
    function CreateItem(const AIndex: Integer): TdxSpreadSheetTableColumn;
    //
    property Items[Index: Integer]: TdxSpreadSheetTableColumn read GetItem; default;
  end;

  { TdxSpreadSheetTableRowCells }

  TdxSpreadSheetTableRowCells = class(TdxDynamicItemList)
  strict private
    FRow: TdxSpreadSheetTableRow;
  protected
    function GetItemClass: TdxDynamicListItemClass; override;
  public
    constructor Create(ARow: TdxSpreadSheetTableRow);

    property First: TdxDynamicListItem read FFirst;
    property Last: TdxDynamicListItem read FLast;
    property Row: TdxSpreadSheetTableRow read FRow write FRow;
  end;

  { TdxSpreadSheetTableRow }

  TdxSpreadSheetTableRow = class(TdxSpreadSheetTableItem)
  strict private
    FCells: TdxSpreadSheetTableRowCells;
  protected
    FBestFitIsDirty: Boolean;

    function GetCell(AColumn: Integer): TdxSpreadSheetCell; override;
    function GetCellCount: Integer; override;
    procedure InitializeCellStyle(ACellStyle: TdxSpreadSheetCellStyle; AColumn: TdxSpreadSheetTableColumn); virtual;
    function IsItemArea(const AArea: TRect): Boolean; override;
    procedure MarkBestFitDirty; override;
    function MeasureCellSize(ACanvas: TcxCanvas; ACell: TdxSpreadSheetCell): Integer; override;
    procedure SetSizeEx(const ASize: TSize); override;

    property RowCells: TdxSpreadSheetTableRowCells read FCells;
  public
    constructor Create(AOwner: TdxDynamicItemList; AIndex: Integer); override;
    destructor Destroy; override;
    procedure ApplyBestFit; override;
    function CreateCell(AColumn: Integer): TdxSpreadSheetCell; override;
  end;

  { TdxSpreadSheetTableRows }

  TdxSpreadSheetTableRows = class(TdxSpreadSheetTableItems)
  strict private
    function GetItem(AIndex: Integer): TdxSpreadSheetTableRow; inline;
  protected
    procedure CheckBestFit;
    procedure ForEachCell(const AArea: TRect; AProc: TdxDynamicItemListForEachProcRef; AGoForward: Boolean = True);
    function GetDefaultSize: Integer; override;
    function GetFixedItemIndex: Integer; override;
    function GetItemClass: TdxDynamicListItemClass; override;
    function GetItemState(AIndex: Integer): TcxButtonState; override;
    function GetMaxItemIndex: Integer; override;
    function GetOppositeItems: TdxSpreadSheetTableItems; override;
    procedure SetDefaultSize(const AValue: Integer); override;
  public
    function CreateItem(const AIndex: Integer): TdxSpreadSheetTableRow;
    //
    property Items[Index: Integer]: TdxSpreadSheetTableRow read GetItem; default;
  end;

  { TdxSpreadSheetTableViewOptionsPrint }

  TdxSpreadSheetTableViewOptionsPrint = class(TdxSpreadSheetCustomViewOptions)
  strict private
    FHeaderFooter: TdxSpreadSheetTableViewOptionsPrintHeaderFooter;
    FPage: TdxSpreadSheetTableViewOptionsPrintPage;
    FPagination: TdxSpreadSheetTableViewOptionsPrintPagination;
    FPrinting: TdxSpreadSheetTableViewOptionsPrintPrinting;
    FSource: TdxSpreadSheetTableViewOptionsPrintSource;

    procedure SetHeaderFooter(AValue: TdxSpreadSheetTableViewOptionsPrintHeaderFooter);
    procedure SetPage(AValue: TdxSpreadSheetTableViewOptionsPrintPage);
    procedure SetPagination(AValue: TdxSpreadSheetTableViewOptionsPrintPagination);
    procedure SetPrinting(AValue: TdxSpreadSheetTableViewOptionsPrintPrinting);
    procedure SetSource(AValue: TdxSpreadSheetTableViewOptionsPrintSource);
  protected
    procedure DoAssign(ASource: TPersistent); override;
  public
    constructor Create(AOwner: TPersistent); override;
    destructor Destroy; override;
    procedure Reset;
  published
    property HeaderFooter: TdxSpreadSheetTableViewOptionsPrintHeaderFooter read FHeaderFooter write SetHeaderFooter;
    property Page: TdxSpreadSheetTableViewOptionsPrintPage read FPage write SetPage;
    property Pagination: TdxSpreadSheetTableViewOptionsPrintPagination read FPagination write SetPagination;
    property Printing: TdxSpreadSheetTableViewOptionsPrintPrinting read FPrinting write SetPrinting;
    property Source: TdxSpreadSheetTableViewOptionsPrintSource read FSource write SetSource;
  end;

  { TdxSpreadSheetTableViewOptions }

  TdxSpreadSheetTableViewOptions = class(TdxSpreadSheetCustomViewOptions)
  strict private
    FDefaultColumnWidth: Integer;
    FDefaultRowHeight: Integer;
    FGridLines: TdxDefaultBoolean;
    FHeaders: TdxDefaultBoolean;
    FHorizontalScrollBar: TdxDefaultBoolean;
    FShowFormulas: TdxDefaultBoolean;
    FVerticalScrollBar: TdxDefaultBoolean;
    FZeroValues: TdxDefaultBoolean;
    FZoomFactor: Integer;

    function GetActualGridLines: Boolean;
    function GetActualHeaders: Boolean;
    function GetActualHorizontalScrollBar: Boolean;
    function GetActualShowFormulas: Boolean;
    function GetActualVerticalScrollBar: Boolean;
    function GetActualZeroValues: Boolean;
    function GetOptionsView: TdxSpreadSheetOptionsView; inline;
    procedure SetDefaultColumnWidth(AValue: Integer);
    procedure SetDefaultRowHeight(AValue: Integer);
    procedure SetGridLines(AValue: TdxDefaultBoolean);
    procedure SetHeaders(AValue: TdxDefaultBoolean);
    procedure SetHorizontalScrollBar(AValue: TdxDefaultBoolean);
    procedure SetShowFormulas(AValue: TdxDefaultBoolean);
    procedure SetVerticalScrollBar(AValue: TdxDefaultBoolean);
    procedure SetZeroValues(AValue: TdxDefaultBoolean);
    procedure SetZoomFactor(AValue: Integer);
  protected
    property OptionsView: TdxSpreadSheetOptionsView read GetOptionsView;
  public
    constructor Create(AOwner: TPersistent); override;
    procedure Assign(Source: TPersistent); override;

    property ActualGridLines: Boolean read GetActualGridLines;
    property ActualHeaders: Boolean read GetActualHeaders;
    property ActualHorizontalScrollBar: Boolean read GetActualHorizontalScrollBar;
    property ActualShowFormulas: Boolean read GetActualShowFormulas;
    property ActualVerticalScrollBar: Boolean read GetActualVerticalScrollBar;
    property ActualZeroValues: Boolean read GetActualZeroValues;
  published
    property DefaultColumnWidth: Integer read FDefaultColumnWidth write SetDefaultColumnWidth;
    property DefaultRowHeight: Integer read FDefaultRowHeight write SetDefaultRowHeight;
    property GridLines: TdxDefaultBoolean read FGridLines write SetGridLines default bDefault;
    property Headers: TdxDefaultBoolean read FHeaders write SetHeaders default bDefault;
    property HorizontalScrollBar: TdxDefaultBoolean read FHorizontalScrollBar write SetHorizontalScrollBar default bDefault;
    property Protected;
    property ShowFormulas: TdxDefaultBoolean read FShowFormulas write SetShowFormulas default bDefault;
    property VerticalScrollBar: TdxDefaultBoolean read FVerticalScrollBar write SetVerticalScrollBar default bDefault;
    property ZeroValues: TdxDefaultBoolean read FZeroValues write SetZeroValues default bDefault;
    property ZoomFactor: Integer read FZoomFactor write SetZoomFactor default dxSpreadSheetDefaultZoomFactor;
  end;

  { TdxSpreadSheetTableViewHitTest }

  TdxSpreadSheetTableViewHitTest = class(TdxSpreadSheetCustomHitTest)
  strict private
    function GetContainer: TdxSpreadSheetContainer;
    function GetView: TdxSpreadSheetTableView; inline;
  protected
    function GetActualHitPoint: TPoint; override;

    property HitAtCellHint: Boolean index hcCellHint read GetHitCode;
  public
    procedure Calculate(const AHitPoint: TPoint); override;

    property Container: TdxSpreadSheetContainer read GetContainer;
    property HitAtBackground: Boolean index hcBackground read GetHitCode;
    property HitAtCell: Boolean index hcCell read GetHitCode;
    property HitAtColumnHeader: Boolean index hcColumnHeader read GetHitCode;
    property HitAtContainer: Boolean index hcContainer read GetHitCode;
    property HitAtContainerSelection: Boolean index hcContainerSelection read GetHitCode;
    property HitAtFrozenPaneSeparator: Boolean index hcFrozenPaneSeparator read GetHitCode;
    property HitAtResizeArea: Boolean index hcResizeArea read GetHitCode;
    property HitAtRowHeader: Boolean index hcRowHeader read GetHitCode;
    property View: TdxSpreadSheetTableView read GetView;
  end;

  { TdxSpreadSheetTableViewSelection }

  TdxSpreadSheetTableViewSelection = class
  strict private
    FFocusedColumn: Integer;
    FFocusedRow: Integer;
    FItems: TObjectList<TcxRect>;
    FLockCount: Integer;
    FView: TdxSpreadSheetTableView;

    function GetArea: TRect;
    function GetChangeLocked: Boolean;
    function GetCount: Integer;
    function GetFocusedCell: TdxSpreadSheetCell;
    function GetFocusedContainer: TdxSpreadSheetContainer;
    function GetItem(AIndex: Integer): TcxRect;
    function GetMergedCells: TdxSpreadSheetMergedCellList;
    function GetSelectionByColumns: Boolean;
    function GetSelectionByRows: Boolean;
    procedure SetChangeLocked(AValue: Boolean);
    procedure SetFocusedCell(AValue: TdxSpreadSheetCell);
    procedure SetFocusedColumn(AValue: Integer);
    procedure SetFocusedContainer(const Value: TdxSpreadSheetContainer);
    procedure SetFocusedRow(AValue: Integer);
  protected
    procedure Changed; virtual;
    procedure CheckClear(const AShift: TShiftState);
    function GetActiveSelection: TRect;
    procedure GetState(ARow, AColumn: Integer; var AFocused, ASelected: Boolean);
    procedure ItemChanged(Sender: TObject); virtual;
    procedure LoadFromStream(AReader: TcxReader); virtual;
    procedure SaveToStream(AWriter: TcxWriter); virtual;

    property ChangeLocked: Boolean read GetChangeLocked write SetChangeLocked;
    property MergedCells: TdxSpreadSheetMergedCellList read GetMergedCells;
    property SelectionByColumns: Boolean read GetSelectionByColumns;
    property SelectionByRows: Boolean read GetSelectionByRows;
  public
    constructor Create(AOwner: TdxSpreadSheetTableView); virtual;
    destructor Destroy; override;
    procedure Add(const AArea: TRect; AShift: TShiftState = []; AFocusedRow: Integer = -1; AFocusedColumn: Integer = -1);
    procedure Clear;
    function HasArea(const AArea: TRect): Boolean;
    function IsCellSelected(ARow, AColumn: Integer): Boolean;
    function IsColumnSelected(AColumn: Integer): Boolean;
    function IsRowSelected(ARow: Integer): Boolean;
    function IsEntireColumnSelected(AColumn: Integer): Boolean;
    function IsEntireRowSelected(ARow: Integer): Boolean;
    procedure SelectAll;
    procedure SelectCell(ARow, AColumn: Integer; AShift: TShiftState = []);
    procedure SelectColumns(AStartColumn, AFinishColumn: Integer; AShift: TShiftState = []);
    procedure SelectRows(AStartRow, AFinishRow: Integer; AShift: TShiftState = []);
    procedure SetFocused(ARow, AColumn: Integer; AShift: TShiftState);

    property Area: TRect read GetArea;
    property Count: Integer read GetCount;
    property FocusedCell: TdxSpreadSheetCell read GetFocusedCell write SetFocusedCell;
    property FocusedColumn: Integer read FFocusedColumn write SetFocusedColumn;
    property FocusedContainer: TdxSpreadSheetContainer read GetFocusedContainer write SetFocusedContainer;
    property FocusedRow: Integer read FFocusedRow write SetFocusedRow;
    property Items[Index: Integer]: TcxRect read GetItem; default;
    property View: TdxSpreadSheetTableView read FView;
  end;

  { TdxSpreadSheetTableViewEditingController }

  TdxSpreadSheetTableViewEditingController = class(TcxCustomEditingController)
  strict private
    FDefaultEdit: TcxCustomMemo;
    FDefaultEditProperties: TcxCustomMemoProperties;
    FEditData: TcxCustomEditData;
    FEditing: Boolean;
    FEditStyle: TcxEditStyle;
    FEditStyleFont: TFont;
    FEditValue: TdxUnicodeString;
    FIsArrayFormula: Boolean;
    FOwner: TdxSpreadSheetTableViewController;
    FPropertiesValue: TcxCustomEditProperties;
    FRedoValue: Variant;
    FReferencesHighlighter: TdxSpreadSheetTableViewReferencesHighlighter;
    FReplacementMode: Boolean;

    function GetCell: TdxSpreadSheetCell;
    function GetCellViewInfo: TdxSpreadSheetTableViewCellViewInfo;
    function GetCharCount: Integer;
    function GetSelCount: Integer;
    function GetSpreadSheet: TdxCustomSpreadSheet;
    function GetView: TdxSpreadSheetTableView; inline;
  protected
    procedure AssignEditStyle; virtual;
    function CanInitEditing: Boolean; override;
    function CanRedo: Boolean; virtual;
    function CanUndo: Boolean; virtual;
    function CanUpdateEditValue: Boolean; override;
    function CanUpdateMultilineEditHeight: Boolean;
    function CreateDefaultEdit: TcxCustomMemo; virtual;
    function CreateDefaultEditProperties: TcxCustomMemoProperties; virtual;
    function CreateEditStyle: TcxEditStyle; virtual;
    function CreateReferencesHighlighter: TdxSpreadSheetTableViewReferencesHighlighter; virtual;

    function IsPartOfArrayFormula(ASender: TdxSpreadSheetCell): Boolean;

    procedure ClearEditingItem; override;
    procedure DoHideEdit(Accept: Boolean); override;
    procedure DoUpdateEdit; override;
    procedure EditActivated;
    procedure EditChanged(Sender: TObject); override;
    procedure EditFocusChanged(Sender: TObject); override;
    procedure EditKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState); override;
    procedure EditKeyPress(Sender: TObject; var Key: Char); override;
    procedure EditKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState); override;
    procedure EditValueChanged(Sender: TObject); override;
    function GetAdjustedMultilineEditBounds: TRect;
    function GetCancelEditingOnExit: Boolean; override;
    function GetCaretPosByMousePos: TPoint;
    function GetEditParent: TWinControl; override;
    function GetFocusedCellBounds: TRect; override;
    function GetIsEditing: Boolean; override;
    function GetValue: TcxEditValue; override;
    procedure HistoryChanged;
    procedure MultilineEditTextChanged; override;
    procedure SetValue(const AValue: TcxEditValue); override;
    procedure StartEditingByTimer; override;
    procedure UpdateEditPosition;
    procedure UpdateInplaceParamsPosition; override;
    //
    function PrepareEdit(AIsMouseEvent: Boolean): Boolean;
    procedure ReplaceEditValue(const AValue: Variant);
    //
    procedure Redo; virtual;
    procedure Undo; virtual;

    property CharCount: Integer read GetCharCount;
    property DefaultEdit: TcxCustomMemo read FDefaultEdit;
    property DefaultEditProperties: TcxCustomMemoProperties read FDefaultEditProperties;
    property EditStyle: TcxEditStyle read FEditStyle;
    property EditStyleFont: TFont read FEditStyleFont;
    property EditValue: TdxUnicodeString read FEditValue write FEditValue;
    property IsArrayFormula: Boolean read FIsArrayFormula write FIsArrayFormula;
    property PropertiesValue: TcxCustomEditProperties read FPropertiesValue;
    property RedoValue: Variant read FRedoValue write FRedoValue;
    property ReferencesHighlighter: TdxSpreadSheetTableViewReferencesHighlighter read FReferencesHighlighter;
    property ReplacementMode: Boolean read FReplacementMode write FReplacementMode;
    property SelCount: Integer read GetSelCount;
  public
    constructor Create(AOwner: TdxSpreadSheetTableViewController); reintroduce; virtual;
    destructor Destroy; override;

    procedure ShowEdit;
    procedure ShowEditByKey(AKey: Char);
    procedure ShowEditByMouse(X, Y: Integer; AShift: TShiftState);

    property Cell: TdxSpreadSheetCell read GetCell;
    property CellViewInfo: TdxSpreadSheetTableViewCellViewInfo read GetCellViewInfo;
    property Controller: TdxSpreadSheetTableViewController read FOwner;
    property SpreadSheet: TdxCustomSpreadSheet read GetSpreadSheet;
    property View: TdxSpreadSheetTableView read GetView;
  end;

  { TdxSpreadSheetInplaceEdit }

  TdxSpreadSheetInplaceEdit = class(TcxRichEdit)
  private
    FOwner: TdxSpreadSheetTableViewEditingController;
    function GetController: TdxSpreadSheetTableViewController;
    function GetSpreadSheet: TdxCustomSpreadSheet;
  protected
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
  public
    constructor Create(AOwner: TdxSpreadSheetTableViewEditingController);

    property Controller: TdxSpreadSheetTableViewController read GetController;
    property Owner: TdxSpreadSheetTableViewEditingController read FOwner;
    property SpreadSheet: TdxCustomSpreadSheet read GetSpreadSheet;
  end;

  { TdxSpreadSheetTableViewController }

  TdxSpreadSheetTableViewSelectionMode = (smNone, smRows, smColumns, smCells);

  TdxSpreadSheetTableViewController = class(TdxSpreadSheetCustomViewController)
  strict private
    FAnchorColumn: Integer;
    FAnchorRow: Integer;
    FEditingController: TdxSpreadSheetTableViewEditingController;
    FScrollingTimer: TcxTimer;
    FSelectionMode: TdxSpreadSheetTableViewSelectionMode;
    FShiftState: TShiftState;

    function GetColumns: TdxSpreadSheetTableColumns;
    function GetFocusedColumn: Integer; inline;
    function GetFocusedRow: Integer; inline;
    function GetFocusedRowOffsetFromTopRow(ARow: Integer): Integer;
    function GetMergedCells: TdxSpreadSheetMergedCellList;
    function GetOptionsBehavior: TdxSpreadSheetOptionsBehavior; inline;
    function GetRows: TdxSpreadSheetTableRows;
    function GetSelection: TdxSpreadSheetTableViewSelection; inline;
    function GetView: TdxSpreadSheetTableView; inline;
    function GetViewHitTest: TdxSpreadSheetTableViewHitTest; inline;
    function GetViewInfo: TdxSpreadSheetTableViewInfo; inline;
  protected
    procedure ApplyBestFitForSelectedHeaderCells(ACells: TdxSpreadSheetViewInfoCellsList);
    function CalculateNewFocusedRowIndex(AOffsetFromFirstRow: Integer): Integer;
    function CanFocusOnClick: Boolean; override;
    function CheckColumnIndex(AColumn, AIncrement: Integer): Integer;
    function CheckCommand(const AKey: TdxUnicodeChar): Boolean; virtual;
    function CheckRowIndex(ARow, AIncrement: Integer): Integer;
    procedure CheckScrollArea(const P: TPoint); override;
    function ContainerCanDelete: Boolean; override;
    function CreateEditingController: TdxSpreadSheetTableViewEditingController; virtual;
    procedure DblClick; override;
    procedure DoKeyDown(AKey: Word; AShift: TShiftState);
    procedure FocusNextColumnInTheSelection(AGoForward: Boolean);
    procedure FocusNextRowInTheSelection(AGoForward: Boolean);
    procedure GetCellFromMouseCursor(var ARow, AColumn: Integer);
    procedure GetNextSelectedAreaCell(AIndex: Integer; AGoForward: Boolean; var ARow, AColumn: Integer);
    function GetHitTest: TdxSpreadSheetCustomHitTest; override;
    function GetNextNonEmptyCell(AItems: TdxSpreadSheetTableItems; AFocusedItemIndex, AFocusedCellIndex, AIncrement: Integer): Integer;
    function GetSelectionAreaByFocusedCell(var R: TcxRect; var AIndex: Integer): Boolean;
    function GetSelectionItem(ASide: TcxBorder; AShift: TShiftState): Integer; inline;
    procedure GetRedoActionCount(var ARedoCount: Integer); override;
    procedure GetUndoActionCount(var AUndoCount: Integer); override;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure KeyPress(var Key: Char); override;
    procedure DoMouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure DoMouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure DoMouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    function DoMouseWheel(Shift: TShiftState; WheelDelta: Integer; const P: TPoint): Boolean; override;
    function Redo(const ARedoCount: Integer = 1): Boolean; override;
    function Undo(const AUndoCount: Integer = 1): Boolean; override;
    procedure ScrollingTimerHandler(Sender: TObject); virtual;
    procedure SelectColumn(AColumn: Integer; AShift: TShiftState);
    procedure SelectRow(ARow: Integer; AShift: TShiftState);
    procedure SetFocused(ARow, AColumn: Integer; AShift: TShiftState);
    procedure SetSelectionMode(ARow, AColumn: Integer; AShift: TShiftState; AMode: TdxSpreadSheetTableViewSelectionMode);

    property ScrollingTimer: TcxTimer read FScrollingTimer;
  public
    constructor Create(AOwner: TObject); override;
    destructor Destroy; override;

    property AnchorColumn: Integer read FAnchorColumn write FAnchorColumn;
    property AnchorRow: Integer read FAnchorRow write FAnchorRow;
    property Columns: TdxSpreadSheetTableColumns read GetColumns;
    property EditingController: TdxSpreadSheetTableViewEditingController read FEditingController;
    property FocusedColumn: Integer read GetFocusedColumn;
    property FocusedRow: Integer read GetFocusedRow;
    property HitTest: TdxSpreadSheetTableViewHitTest read GetViewHitTest;
    property MergedCells: TdxSpreadSheetMergedCellList read GetMergedCells;
    property OptionsBehavior: TdxSpreadSheetOptionsBehavior read GetOptionsBehavior;
    property Rows: TdxSpreadSheetTableRows read GetRows;
    property Selection: TdxSpreadSheetTableViewSelection read GetSelection;
    property SelectionMode: TdxSpreadSheetTableViewSelectionMode read FSelectionMode write FSelectionMode;
    property View: TdxSpreadSheetTableView read GetView;
    property ViewInfo: TdxSpreadSheetTableViewInfo read GetViewInfo;
  end;

  { TdxSpreadSheetTableView }

  TdxSpreadSheetTableViewMergedCellsConflictResolution = (
    mccrShowConfirmation, mccrUnmergeAllAffectedCells, mccrRaiseException);

  TdxSpreadSheetTableView = class(TdxSpreadSheetCustomView, IdxSpreadSheetTableView)
  strict private
    FArrayFormulasList: TObjectList<TdxSpreadSheetFormula>;
    FColumns: TdxSpreadSheetTableColumns;
    FFrozenColumn: Integer;
    FFrozenRow: Integer;
    FMergedCells: TdxSpreadSheetMergedCellList;
    FOptionsPrint: TdxSpreadSheetTableViewOptionsPrint;
    FRows: TdxSpreadSheetTableRows;
    FSelection: TdxSpreadSheetTableViewSelection;

    function ExtractVector(const AStartRow, AStartColumn, ALength: Integer; const AIsVertical: Boolean): TdxSpreadSheetVector;
    function GetBottomRow: Integer;
    function GetCell(ARow, AColumn: Integer): TdxSpreadSheetCell; inline;
    function GetController: TdxSpreadSheetTableViewController; inline;
    function GetDimensions: TRect; inline;
    function GetEditingController: TdxSpreadSheetTableViewEditingController; inline;
    function GetHistory: TdxSpreadSheetHistory;
    function GetHitTest: TdxSpreadSheetTableViewHitTest; inline;
    function GetIsEditing: Boolean;
    function GetItemIndex(const AItem: TdxDynamicListItem): Integer; inline;
    function GetLeftColumn: Integer;
    function GetOptions: TdxSpreadSheetTableViewOptions;
    function GetRightColumn: Integer;
    function GetStringTable: TdxSpreadSheetSharedStringTable; inline;
    function GetTopRow: Integer;
    function GetViewInfo: TdxSpreadSheetTableViewInfo; inline;
    procedure SetBottomRow(AValue: Integer);
    procedure SetFrozenColumn(AValue: Integer);
    procedure SetFrozenRow(AValue: Integer);
    procedure SetLeftColumn(AValue: Integer);
    procedure SetOptions(AValue: TdxSpreadSheetTableViewOptions);
    procedure SetOptionsPrint(AValue: TdxSpreadSheetTableViewOptionsPrint);
    procedure SetRightColumn(AValue: Integer);
    procedure SetTopRow(AValue: Integer);
  protected
    function GetAreaReferencedByArrayFormula(const ARow, AColumn: Integer): TRect; inline;
    function GetArrayFormulaArea(const ASenderRowIndex, ASenderColumnIndex: Integer): TRect; inline;
    function GetArrayFormulaSlaveCellValue(const AArea: TRect; const ARow, AColumn: Integer;
      var AErrorCode: TdxSpreadSheetFormulaErrorCode): Variant; inline;
    function IsCellReferencedByArrayFormulaArea(const ARow, AColumn: Integer; var AArea: TRect): Boolean; inline;
    procedure PopulateAreaByValue(const AValue: Variant; const AArea: TRect);
    procedure PopulateSlaveCells(const AArrayFormulaArea: TRect);

    function GetCellsModification(const AArea: TRect; AIsDeletingMode: Boolean; out AModification: TdxSpreadSheetCellsModification): Boolean;
    function GetFactuallyModifyableArea(const AArea: TRect; AModification: TdxSpreadSheetCellsModification): TRect;

    // IdxSpreadSheetTableView
    function IdxSpreadSheetTableView.GetCell = CreateCell;
    function GetAbsoluteCellBounds(const ARowIndex, AColumnIndex: Integer; ACheckMergedCells: Boolean = True): TRect;
    function GetCellAtAbsolutePoint(const P: TPoint; out ARowIndex, AColumnIndex: Integer): Boolean;

    procedure AfterLoad; override;
    function CreateColumns: TdxSpreadSheetTableColumns; virtual;
    function CreateController: TdxSpreadSheetCustomViewController; override;
    function CreateHitTest: TdxSpreadSheetCustomHitTest; override;
    function CreateMergedCells: TdxSpreadSheetMergedCellList; virtual;
    function CreateOptions: TdxSpreadSheetCustomViewOptions; override;
    function CreateOptionsPrint: TdxSpreadSheetTableViewOptionsPrint; virtual;
    function CreateRows: TdxSpreadSheetTableRows; virtual;
    function CreateSelection: TdxSpreadSheetTableViewSelection; virtual;
    function CreateViewInfo: TdxSpreadSheetCustomViewViewInfo; override;
    procedure CreateSubClasses; override;
    procedure DestroySubClasses; override;

    function DoActiveCellChanging(AColumn, ARow: Integer): Boolean; virtual;
    procedure DoCheckChanges; override;
    procedure DoCompare(const AData1, AData2: TdxSpreadSheetCellData; var Compare: Integer); virtual;
    procedure DoEditChanged; virtual;
    procedure DoEdited; virtual;
    procedure DoEditing(var AProperties: TcxCustomEditProperties; var ACanEdit: Boolean); virtual;
    procedure DoEditValueChanged; virtual;
    procedure DoInitEdit(AEdit: TcxCustomEdit); virtual;
    procedure DoInitEditValue(AEdit: TcxCustomEdit; var AValue: Variant); virtual;
    procedure DoRemoveCell(ACell: TdxSpreadSheetCell); virtual;
    procedure ExchangeValues(ACell1, ACell2: TdxSpreadSheetCell); overload; virtual;
    procedure ExchangeValues(ARow1, AColumn1, ARow2, AColumn2: Integer); overload; virtual;
    procedure Pack; override;
    procedure RemoveFromArrayTokenList(AFormula: TdxSpreadSheetFormula);


    function AbsoluteToScreen(const R: TRect): TRect; override;
    function GetContentOrigin: TPoint; override;
    function GetPartOffsetByPoint(const P: TPoint): TPoint; override;
    function GetZoomFactor: Integer; override;

    property ArrayFormulasList: TObjectList<TdxSpreadSheetFormula> read FArrayFormulasList;
    property Controller: TdxSpreadSheetTableViewController read GetController;
    property EditingController: TdxSpreadSheetTableViewEditingController read GetEditingController;
    property History: TdxSpreadSheetHistory read GetHistory;
    property StringTable: TdxSpreadSheetSharedStringTable read GetStringTable;
    property ViewInfo: TdxSpreadSheetTableViewInfo read GetViewInfo;
  public
    constructor Create(AOwner: TdxCustomSpreadSheet); override;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;

    procedure CellAtPoint(const P: TPoint; out ARow, AColumn: Integer;
      AReturnNearestCell: Boolean = False; AVisibleAreaOnly: Boolean = True); overload;
    procedure CellAtPoint(const P: TPoint; out ACell: TdxSpreadSheetCell;
      AReturnNearestCell: Boolean = False; AVisibleAreaOnly: Boolean = True); overload;
    function CreateCell(const ARow, AColumn: Integer): TdxSpreadSheetCell; virtual;

    function CanClearCells: Boolean; virtual;
    procedure ClearCells(const AArea: TRect; AClearValues: Boolean = True; AClearFormats: Boolean = True); virtual;
    procedure ClearCellsValues;

    function CanDelete: Boolean; virtual;
    procedure DeleteAllCells;
    procedure DeleteCells; overload; virtual;
    procedure DeleteCells(const AArea: TRect; AModification: TdxSpreadSheetCellsModification;
      AMergedCellsConflictResolution: TdxSpreadSheetTableViewMergedCellsConflictResolution = mccrShowConfirmation); overload; virtual;
    procedure DeleteColumns(AStartColumn: Integer; ACount: Integer);
    procedure DeleteRows(AStartRow: Integer; ACount: Integer);

    procedure AddArrayFormula(const AText: TdxUnicodeString; const AArea: TRect);
    function CanClearCellsValues: Boolean;
    function CanInsertCells(const AArea: TRect; const AModification: TdxSpreadSheetCellsModification): Boolean;
    function CanModifyDataInArea(const AArea: TRect; const AMode: TdxSpreadSheetCellsModificationMode): Boolean;
    function ExtractHorizontalVector(const ARow, AStartColumn, ALength: Integer): TdxSpreadSheetVector;
    function ExtractVerticalVector(const AStartRow, AColumn, ALength: Integer): TdxSpreadSheetVector;
    procedure GetCellValue(const ARow, AColumn: Integer; var AValue: Variant; var AErrorCode: TdxSpreadSheetFormulaErrorCode); inline;

    procedure FreezeColumns(const AColumn: Integer);
    procedure FreezePanes(const ARow, AColumn: Integer);
    procedure FreezeRows(const ARow: Integer);
    procedure UnfreezePanes;

    function CanInsert: Boolean; virtual;
    procedure InsertCells; overload; virtual;
    procedure InsertCells(const AArea: TRect; AModification: TdxSpreadSheetCellsModification;
      AMergedCellsConflictResolution: TdxSpreadSheetTableViewMergedCellsConflictResolution = mccrShowConfirmation); overload; virtual;
    procedure InsertColumns(AColumn: Integer; ACount: Integer);
    procedure InsertRows(ARow: Integer; ACount: Integer);

    procedure MakeFocusedCellVisible;
	  procedure MakeVisible(ARow, AColumn: Integer);
    procedure MakeVisibleColumn(const AColumn: Integer);
    procedure MakeVisibleRow(const ARow: Integer);

    function CanCopyToClipboard: Boolean; virtual;
    function CanCutToClipboard: Boolean; virtual;
    function CanPasteFromClipboard: Boolean; virtual;
    procedure CopyToClipboard; virtual;
    procedure CutToClipboard; virtual;
    procedure PasteFromClipboard; virtual;

    function CanMergeSelected: Boolean; virtual;
    function CanSplitSelected: Boolean; virtual;
    procedure MergeSelected; virtual;
    procedure SplitSelected; virtual;

    procedure HideEdit(Accept: Boolean);
    procedure ShowEdit;
    procedure ShowEditByKey(AKey: Char);
    procedure ShowEditByMouse(X, Y: Integer; AShift: TShiftState);

    function CanSort: Boolean; virtual;
    procedure SortByColumnValues(const AArea: TRect; const ASortOrders: array of TdxSortOrder; const AColumns: array of Integer);
    procedure SortByRowValues(const AArea: TRect; const ASortOrders: array of TdxSortOrder; const ARows: array of Integer);

    property BottomRow: Integer read GetBottomRow write SetBottomRow;
    property LeftColumn: Integer read GetLeftColumn write SetLeftColumn;
    property RightColumn: Integer read GetRightColumn write SetRightColumn;
    property TopRow: Integer read GetTopRow write SetTopRow;

    property FrozenColumn: Integer read FFrozenColumn write SetFrozenColumn default -1;
    property FrozenRow: Integer read FFrozenRow write SetFrozenRow default -1;

    property Cells[ARow, AColumn: Integer]: TdxSpreadSheetCell read GetCell;
    property Columns: TdxSpreadSheetTableColumns read FColumns;
    property MergedCells: TdxSpreadSheetMergedCellList read FMergedCells;
    property Rows: TdxSpreadSheetTableRows read FRows;
    property Selection: TdxSpreadSheetTableViewSelection read FSelection;

    property Caption;
    property Dimensions: TRect read GetDimensions;
    property HitTest: TdxSpreadSheetTableViewHitTest read GetHitTest;
    property IsEditing: Boolean read GetIsEditing;
    property Options: TdxSpreadSheetTableViewOptions read GetOptions write SetOptions;
    property OptionsPrint: TdxSpreadSheetTableViewOptionsPrint read FOptionsPrint write SetOptionsPrint;
  end;

  { TdxSpreadSheetTableViewInfo }

  TdxSpreadSheetTableViewInfo = class(TdxSpreadSheetCustomViewViewInfo)
  strict private
    FCells: TdxSpreadSheetTableViewInfoCellsList;
    FCellsArea: TRect;
    FColumnsHeader: TdxSpreadSheetViewInfoCellsList;
    FColumnsHeaderArea: TRect;
    FCommonCells: TdxSpreadSheetViewInfoCellsList;
    FContentParams: TcxViewParams;
    FFirstColumnOrigin: Integer;
    FFirstRowOrigin: Integer;
    FFirstScrollableColumn: Integer;
    FFirstScrollableColumnOffset: Integer;
    FFirstScrollableRow: Integer;
    FFirstScrollableRowOffset: Integer;
    FFocusedCell: TdxSpreadSheetTableViewCellViewInfo;
    FFrozenColumnSeparatorPosition: Integer;
    FFrozenRowSeparatorPosition: Integer;
    FHeaderParams: TcxViewParams;
    FLastVisibleColumn: Integer;
    FLastVisibleRow: Integer;
    FRowsHeader: TdxSpreadSheetViewInfoCellsList;
    FRowsHeaderArea: TRect;
    FScrollableArea: TRect;
    FSelectionBounds: TRect;
    FSelectionParams: TcxViewParams;
    FVisibleCells: TRect;
    FVisibleColumnCount: Integer;
    FVisibleRowCount: Integer;

    procedure AddOutOfBoundsNonEmptyCell(ARow: TdxSpreadSheetTableRow; AIndex: Integer; const ABounds: TRect; AGoForward: Boolean);
    function CalculatePrevPageFirstIndex(APosition: Integer; AIsHorizontal: Boolean): Integer; overload;
    function CalculatePrevPageFirstIndex(APosition, ASize, AFixedIndex: Integer; AItems: TdxSpreadSheetTableItems): Integer; overload;
    function GetColumns: TdxSpreadSheetTableColumns; inline;
    function GetColumnsHeaderHeight: Integer; inline;
    function GetDefaultCellStyle: TdxSpreadSheetCellStyle; inline;
    function GetFirstScrollableColumn: Integer;
    function GetFirstScrollableRow: Integer;
    function GetFocusedCell: TdxSpreadSheetTableViewCellViewInfo;
    function GetFrozenColumn: Integer; inline;
    function GetFrozenRow: Integer; inline;
    function GetGridLineColor: TColor;
    function GetHScrollBarPos: Integer;
    function GetLookAndFeelPainter: TcxCustomLookAndFeelPainter;
    function GetOptions: TdxSpreadSheetTableViewOptions; inline;
    function GetOptionsView: TdxSpreadSheetOptionsView; inline;
    function GetOrigin: TPoint; inline;
    function GetReferencesHighlighter: TdxSpreadSheetTableViewReferencesHighlighter;
    function GetRows: TdxSpreadSheetTableRows;  inline;
    function GetRowsHeaderWidth: Integer; inline;
    function GetSelection: TdxSpreadSheetTableViewSelection; inline;
    function GetStyles: TdxSpreadSheetStyles; inline;
    function GetTotalColumnCount: Integer;
    function GetTotalRowCount: Integer;
    function GetView: TdxSpreadSheetTableView; inline;
    function GetVScrollBarPos: Integer;
    function RemoveNeighborCellBorder(ASide: TcxBorder;
      AIndex, AStartIndex, AFinishIndex: Integer; var AEndPos: Integer): Boolean; inline;
    procedure SetCellBorderStyle(ACell: TdxSpreadSheetTableViewCellViewInfo;
      ASide: TcxBorder; AStyle, ANeighborStyle: TdxSpreadSheetCellStyle);
    procedure SetFirstScrollableColumn(AValue: Integer);
    procedure SetFirstScrollableRow(AValue: Integer);
  protected
    procedure AddCell(ARow: TdxSpreadSheetTableRow; ARowIndex, AColumnIndex: Integer; const ABounds: TRect); virtual;
    procedure AddFrozenPaneSeparator(const ABounds: TRect; var APosition: Integer);
    procedure CalculateCells; virtual;
    procedure CalculateHeaders; virtual;
    procedure CalculateLastItemIndex(ASize, AStartIndex, AFixedIndex, AFirstScrollableIndex, AMaxIndex: Integer;
      AItems: TdxSpreadSheetTableItems; var ASeparatorPosition, ALastIndex: Integer);
    procedure CalculateVisibleArea; virtual;

    function CanDrawCellSelection: Boolean; virtual;
    procedure ChangeVisibleArea(ASide: TcxBorder; AValue: Integer);
    procedure ChangeVisibleAreaBounds(ASide: TcxBorder; AValue: Integer);
    procedure CheckRowsBestFit; virtual;
    procedure DoInitHitTestWithOriginOffset(AHitTest: TdxSpreadSheetCustomHitTest; const AOffset: TPoint); virtual;
    procedure DoScroll(AScrollBarKind: TScrollBarKind; AScrollCode: TScrollCode; var AScrollPos: Integer); virtual;
    procedure DrawPart(ACanvas: TcxCanvas; const AOffset: TPoint; const AClipRect: TRect); virtual;
    function GetAreaBounds(ARow, AColumn: Integer; const AArea, ACellBounds: TRect): TRect;
    function GetCellClipArea(ARow, AColumn: Integer): TRect; inline;
    function GetCellStyle(ARow, AColumn: Integer): TdxSpreadSheetCellStyle; inline;
    function GetItemPosition(AIndex: Integer; ASide: TcxBorder): Integer;
    function GetMergedCell(ARow, AColumn: Integer): TdxSpreadSheetMergedCell;
    function GetNeighborCellStyle(ARow, AColumn: Integer; ASide: TcxBorder): TdxSpreadSheetCellStyle; inline;

    function GetPartBounds(APart: Integer): TRect;
    function GetPartCount: Integer;
    function GetPartOffset(APart: Integer): TPoint;
    function GetPartOffsetByPoint(const P: TPoint): TPoint;

    procedure InitScrollBarsParameters; override;
    procedure MergeBorderStyle(ABorder1, ABorder2: TdxSpreadSheetCellBorder;
      var AColor: TColor; var AStyle: TdxSpreadSheetCellBorderStyle);
    procedure PopulateColumnHeaders;
    procedure PopulateRowHeaders;
    procedure PostProcessRowCells(ARow: TdxSpreadSheetTableRow; AStartIndex, AFinishIndex: Integer);
    procedure Scroll(AScrollBarKind: TScrollBarKind; AScrollCode: TScrollCode; var AScrollPos: Integer); override;
    procedure SelectionChanged; override;
    procedure SetScrollBarInfo(AScrollBarKind: TScrollBarKind;
      AMin, AMax, AStep, APage, APos: Integer; AAllowShow, AAllowHide: Boolean);
    function UpdateContainerState(ACell: TdxSpreadSheetCellViewInfo; AData: TObject): Boolean;
    function UpdateContentCellState(ACell: TdxSpreadSheetCellViewInfo; AData: TObject): Boolean;
    function UpdateHeaderCellState(ACell: TdxSpreadSheetCellViewInfo; AData: TObject): Boolean;
    procedure ValidateBounds(AStartIndex, AFinishIndex, AFixedIndex, ASeparatorPosition: Integer; var AStart, AFinish: Integer);
  public
    constructor Create(AView: TdxSpreadSheetCustomView); override;
    destructor Destroy; override;
    procedure Calculate; override;
    procedure Clear; override;
    procedure Draw(ACanvas: TcxCanvas); override;
    procedure InitHitTest(AHitTest: TdxSpreadSheetCustomHitTest); virtual;
    procedure Validate;

    property Cells: TdxSpreadSheetTableViewInfoCellsList read FCells;
    property CellsArea: TRect read FCellsArea;
    property Columns: TdxSpreadSheetTableColumns read GetColumns;
    property ColumnsHeader: TdxSpreadSheetViewInfoCellsList read FColumnsHeader;
    property ColumnsHeaderArea: TRect read FColumnsHeaderArea;
    property ColumnsHeaderHeight: Integer read GetColumnsHeaderHeight;
    property CommonCells: TdxSpreadSheetViewInfoCellsList read FCommonCells;
    property ContentParams: TcxViewParams read FContentParams;
    property DefaultCellStyle: TdxSpreadSheetCellStyle read GetDefaultCellStyle;
    property FirstColumnOrigin: Integer read FFirstColumnOrigin;
    property FirstRowOrigin: Integer read FFirstRowOrigin;
    property FirstScrollableColumn: Integer read GetFirstScrollableColumn write SetFirstScrollableColumn;
    property FirstScrollableColumnOffset: Integer read FFirstScrollableColumnOffset;
    property FirstScrollableRow: Integer read GetFirstScrollableRow write SetFirstScrollableRow;
    property FirstScrollableRowOffset: Integer read FFirstScrollableRowOffset;
    property FocusedCell: TdxSpreadSheetTableViewCellViewInfo read GetFocusedCell;
    property FrozenColumn: Integer read GetFrozenColumn;
    property FrozenColumnSeparatorPosition: Integer read FFrozenColumnSeparatorPosition;
    property FrozenRow: Integer read GetFrozenRow;
    property FrozenRowSeparatorPosition: Integer read FFrozenRowSeparatorPosition;
    property GridLineColor: TColor read GetGridLineColor;
    property HeaderParams: TcxViewParams read FHeaderParams;
    property HScrollBarPos: Integer read GetHScrollBarPos;
    property LastVisibleColumn: Integer read FLastVisibleColumn;
    property LastVisibleRow: Integer read FLastVisibleRow;
    property LookAndFeelPainter: TcxCustomLookAndFeelPainter read GetLookAndFeelPainter;
    property Options: TdxSpreadSheetTableViewOptions read GetOptions;
    property OptionsView: TdxSpreadSheetOptionsView read GetOptionsView;
    property Origin: TPoint read GetOrigin;
    property ReferencesHighlighter: TdxSpreadSheetTableViewReferencesHighlighter read GetReferencesHighlighter;
    property Rows: TdxSpreadSheetTableRows read GetRows;
    property RowsHeader: TdxSpreadSheetViewInfoCellsList read FRowsHeader;
    property RowsHeaderArea: TRect read FRowsHeaderArea;
    property RowsHeaderWidth: Integer read GetRowsHeaderWidth;
    property ScrollableArea: TRect read FScrollableArea;
    property Selection: TdxSpreadSheetTableViewSelection read GetSelection;
    property SelectionBounds: TRect read FSelectionBounds;
    property SelectionParams: TcxViewParams read FSelectionParams;
    property Styles: TdxSpreadSheetStyles read GetStyles;
    property TotalColumnCount: Integer read GetTotalColumnCount;
    property TotalRowCount: Integer read GetTotalRowCount;
    property View: TdxSpreadSheetTableView read GetView;
    property VisibleCells: TRect read FVisibleCells;
    property VisibleColumnCount: Integer read FVisibleColumnCount;
    property VisibleRowCount: Integer read FVisibleRowCount;
    property VScrollBarPos: Integer read GetVScrollBarPos;
  end;

  { TdxSpreadSheetDefinedName }

  TdxSpreadSheetDefinedName = class(TdxSpreadSheetObjectListItem)
  strict private
    FCaption: TdxUnicodeString;
    FScope: TdxSpreadSheetCustomView;
    FFormula: TdxSpreadSheetDefinedNameFormula;
    function GetIndex: Integer;
    function GetReference: TdxUnicodeString;
    function GetSpreadSheet: TdxCustomSpreadSheet;
    function GetValueAsText: TdxUnicodeString;
    procedure SetCaption(const AValue: TdxUnicodeString);
    procedure SetFormula(const AValue: TdxSpreadSheetDefinedNameFormula);
    procedure SetReference(AValue: TdxUnicodeString);
    procedure SetScope(const AValue: TdxSpreadSheetCustomView);
  protected
    procedure ClearResult;
    function Compare(ACandidate: TdxSpreadSheetDefinedName): Integer;
    function CompareEx(const ACaption: TdxUnicodeString; AScope: TdxSpreadSheetCustomView; AScopeMaybeEmpty: Boolean = False): Integer;
    procedure Initialize(const ACaption, AReference: TdxUnicodeString; AScope: TdxSpreadSheetCustomView);
    procedure LoadFromStream(AReader: TcxReader);
    procedure SaveToStream(AWriter: TcxWriter);
    procedure UpdateReference;

    property Formula: TdxSpreadSheetDefinedNameFormula read FFormula write SetFormula;
  public
    destructor Destroy; override;
    procedure EnumReferences(AProc: TdxSpreadSheetFormulaEnumReferencesProc);

    property Caption: TdxUnicodeString read FCaption write SetCaption;
    property Index: Integer read GetIndex;
    property Reference: TdxUnicodeString read GetReference write SetReference;
    property Scope: TdxSpreadSheetCustomView read FScope write SetScope;
    property SpreadSheet: TdxCustomSpreadSheet read GetSpreadSheet;
    property ValueAsText: TdxUnicodeString read GetValueAsText;
  end;

  { TdxSpreadSheetDefinedNames }

  TdxSpreadSheetDefinedNames = class(TdxSpreadSheetObjectList)
  strict private
    FSorted: Boolean;

    function GetName(AIndex: Integer): TdxSpreadSheetDefinedName;
    class function CompareNames(AName1, AName2: TdxSpreadSheetDefinedName): Integer; static;
  protected
    procedure AfterLoad; virtual;
    procedure CheckSorted;
    function CreateItem: TdxSpreadSheetObjectListItem; override;
    function Find(const ACaption: TdxUnicodeString; AScope: TdxSpreadSheetCustomView; AScopeMaybeEmpty: Boolean): Integer; inline;

    property Sorted: Boolean read FSorted write FSorted;
  public
    function Add(const ACaption, AReference: TdxUnicodeString; AScope: TdxSpreadSheetCustomView = nil): TdxSpreadSheetDefinedName;
    function AddFromStream(AReader: TcxReader): TdxSpreadSheetDefinedName;
    function AddOrSet(const ACaption, AReference: TdxUnicodeString; AScope: TdxSpreadSheetCustomView = nil): TdxSpreadSheetDefinedName;
    function GetItemByName(const ACaption: TdxUnicodeString; AScope: TdxSpreadSheetCustomView): TdxSpreadSheetDefinedName;
    function IndexOf(const ACaption: TdxUnicodeString): Integer; overload;
    function IndexOf(const ACaption: TdxUnicodeString; AScope: TdxSpreadSheetCustomView): Integer; overload;

    property Items[Index: Integer]: TdxSpreadSheetDefinedName read GetName; default;
  end;

  { TdxSpreadSheetExternalLink }

  TdxSpreadSheetExternalLink = class(TdxSpreadSheetObjectListItem)
  strict private
    FOwner: TdxSpreadSheetExternalLinks;
    function GetActualTarget: TdxUnicodeString;
  protected
    FTarget: TdxUnicodeString;
  public
    property ActualTarget: TdxUnicodeString read GetActualTarget;
    property Target: TdxUnicodeString read FTarget;
    property Owner: TdxSpreadSheetExternalLinks read FOwner;
  end;

  { TdxSpreadSheetExternalLinks }

  TdxSpreadSheetExternalLinks = class(TdxSpreadSheetObjectList)
  strict private
    function GetItem(AIndex: Integer): TdxSpreadSheetExternalLink;
  protected
    function CreateItem: TdxSpreadSheetObjectListItem; override;
  public
    function Add(const ATarget: TdxUnicodeString): TdxSpreadSheetExternalLink;
    function GetLinkByTarget(const ATarget: TdxUnicodeString): TdxSpreadSheetExternalLink;

    property Items[Index: Integer]: TdxSpreadSheetExternalLink read GetItem; default;
  end;

  { TdxSpreadSheetCustomFilerProgressHelper }

  TdxSpreadSheetCustomFilerProgressHelper = class
  strict private
    FOwner: TdxSpreadSheetCustomFiler;
    FProgress: Integer;
    FStage: Integer;
    FStageCount: Integer;
    FStageTaskCount: Integer;
    FStageTaskNumber: Integer;

    procedure CalculateProgress;
    procedure SetProgress(AValue: Integer);
  public
    constructor Create(AOwner: TdxSpreadSheetCustomFiler; AStageCount: Integer);
    procedure BeforeDestruction; override;
    procedure BeginStage(ATaskCount: Integer);
    procedure EndStage;
    procedure NextTask(ASkipCount: Integer = 1);
    procedure SkipStage;
    //
    property Progress: Integer read FProgress write SetProgress;
    property Stage: Integer read FStage;
  end;

  { TdxSpreadSheetCustomFiler }

  TdxSpreadSheetCustomFiler = class(TdxSpreadSheetPersistentObject)
  strict private
    FIgnoreMessages: TdxSpreadSheetMessageTypes;
    FInvariantFormatSettings: TFormatSettings;
    FProgressHelper: TdxSpreadSheetCustomFilerProgressHelper;
    FStream: TStream;

    FOnProgress: TdxSpreadSheetProgressEvent;
  protected
    function CreateProgressHelper: TdxSpreadSheetCustomFilerProgressHelper; virtual; abstract;
    procedure DoError(const AMessage: string; AType: TdxSpreadSheetMessageType); overload;
    procedure DoError(const AFormatString: string; const AArguments: array of const; AType: TdxSpreadSheetMessageType); overload;
    procedure DoProgress(AProgress: Integer);
    procedure ExecuteSubTask(ASubTask: TdxSpreadSheetCustomFilerSubTask);
    //
    property ProgressHelper: TdxSpreadSheetCustomFilerProgressHelper read FProgressHelper;
  public
    constructor Create(AOwner: TdxCustomSpreadSheet; AStream: TStream); reintroduce; virtual;
    destructor Destroy; override;
    //
    property IgnoreMessages: TdxSpreadSheetMessageTypes read FIgnoreMessages write FIgnoreMessages;
    property InvariantFormatSettings: TFormatSettings read FInvariantFormatSettings;
    property Stream: TStream read FStream;
    //
    property OnProgress: TdxSpreadSheetProgressEvent read FOnProgress write FOnProgress;
  end;

  { TdxSpreadSheetCustomFilerSubTask }

  TdxSpreadSheetCustomFilerSubTask = class(TObject)
  strict private
    FOwner: TdxSpreadSheetCustomFiler;
    function GetSpreadSheet: TdxCustomSpreadSheet; inline;
  protected
    procedure DoError(const AFormatString: string; const AArguments: array of const; AType: TdxSpreadSheetMessageType); overload;
    procedure DoError(const AMessage: string; AType: TdxSpreadSheetMessageType); overload; inline;
    procedure ExecuteSubTask(ASubTask: TdxSpreadSheetCustomFilerSubTask); inline;
  public
    constructor Create(AOwner: TdxSpreadSheetCustomFiler);
    procedure Execute; virtual; abstract;
    //
    property Owner: TdxSpreadSheetCustomFiler read FOwner;
    property SpreadSheet: TdxCustomSpreadSheet read GetSpreadSheet;
  end;

  { TdxSpreadSheetCustomReader }

  TdxSpreadSheetCustomReaderClass = class of TdxSpreadSheetCustomReader;
  TdxSpreadSheetCustomReader = class(TdxSpreadSheetCustomFiler)
  strict private
    function GetCellStyles: TdxSpreadSheetCellStyles; {$IFNDEF VER220} inline; {$ENDIF}
    function GetStringTable: TdxSpreadSheetSharedStringTable; inline;
  protected
    function AddBorders(ABordersHandle: TdxSpreadSheetBordersHandle): TdxSpreadSheetBordersHandle;
    function AddBrush(ABrushHandle: TdxSpreadSheetBrushHandle): TdxSpreadSheetBrushHandle;
    function AddCellStyle(AStyleHandle: TdxSpreadSheetCellStyleHandle): TdxSpreadSheetCellStyleHandle;
    function AddFont(AFontHandle: TdxSpreadSheetFontHandle): TdxSpreadSheetFontHandle;
    function AddFormattedSharedString(S: TdxSpreadSheetFormattedSharedString): TdxSpreadSheetSharedString;
    function AddImage(const AStream: TStream): TdxSpreadSheetSharedImageHandle;
    function AddNumberFormat(const AFormatCode: TdxUnicodeString; Id: Integer = -1): TdxSpreadSheetFormatHandle;
    function AddSharedString(const S: TdxUnicodeString): TdxSpreadSheetSharedString;
    function AddTableView(const ACaption: TdxUnicodeString): TdxSpreadSheetTableView;

    procedure Check(AValue: Boolean; const AMessage: string; AMessageType: TdxSpreadSheetMessageType = ssmtError);

    function CreateTempCellStyle(AFont: TdxSpreadSheetFontHandle; AFormat: TdxSpreadSheetFormatHandle;
      AFill: TdxSpreadSheetBrushHandle; ABorders: TdxSpreadSheetBordersHandle): TdxSpreadSheetCellStyleHandle;
    function CreateTempBordersHandle: TdxSpreadSheetBordersHandle;
    function CreateTempBrushHandle: TdxSpreadSheetBrushHandle;
    function CreateTempFontHandle: TdxSpreadSheetFontHandle;
    function CreateTempFormattedSharedString(const S: TdxUnicodeString): TdxSpreadSheetFormattedSharedString;

    property CellStyles: TdxSpreadSheetCellStyles read GetCellStyles;
    property StringTable: TdxSpreadSheetSharedStringTable read GetStringTable;
  public
    procedure ReadData; virtual; abstract;
  end;

  { TdxSpreadSheetCustomWriter }

  TdxSpreadSheetCustomWriterClass = class of TdxSpreadSheetCustomWriter;
  TdxSpreadSheetCustomWriter = class(TdxSpreadSheetCustomFiler)
  public
    procedure WriteData; virtual; abstract;
  end;

  { TdxSpreadSheetCustomFormat }

  TdxSpreadSheetCustomFormatClass = class of TdxSpreadSheetCustomFormat;
  TdxSpreadSheetCustomFormat = class
  public
    class function CanCheckByContent: Boolean; virtual;
    class function CanReadFromStream(AStream: TStream): Boolean; virtual;
    class function CreateFormatSettings: TdxSpreadSheetFormatSettings; virtual;
    class function GetDescription: string; virtual;
    class function GetExt: string; virtual;
    class function GetReader: TdxSpreadSheetCustomReaderClass; virtual;
    class function GetWriter: TdxSpreadSheetCustomWriterClass; virtual;
    class procedure Register;
    class procedure Unregister;
  end;

  { TdxSpreadSheetFormatsRepository }

  TdxSpreadSheetFormatsRepository = class(TObject)
  strict private
    FList: TList;
    function GetCount: Integer;
    function GetItem(Index: Integer): TdxSpreadSheetCustomFormatClass;
  public
    constructor Create;
    destructor Destroy; override;
    function Find(const AFileName: string; out AFormat: TdxSpreadSheetCustomFormatClass): Boolean;
    function GetOpenDialogFilter: string;
    function GetSaveDialogFilter: string;

    procedure Register(AFormat: TdxSpreadSheetCustomFormatClass);
    procedure Unregister(AFormat: TdxSpreadSheetCustomFormatClass);

    property Count: Integer read GetCount;
    property Items[Index: Integer]: TdxSpreadSheetCustomFormatClass read GetItem; default;
  end;

  { TdxSpreadSheetCustomDragAndDropObject }

  TdxSpreadSheetCustomDragAndDropObject = class(TcxDragAndDropObject)
  strict private
    function GetControl: TdxCustomSpreadSheet;
    function GetHitTest: TdxSpreadSheetCustomHitTest; 
    function GetView: TdxSpreadSheetCustomView;
  protected
    function GetZoomFactor: Integer; override;
    function TranslateCoords(const P: TPoint): TPoint; override;
  public
    property Control: TdxCustomSpreadSheet read GetControl;
    property HitTest: TdxSpreadSheetCustomHitTest read GetHitTest;
    property View: TdxSpreadSheetCustomView read GetView;
  end;

  { TdxSpreadSheetCustomCellViewInfo }

  TdxSpreadSheetCustomCellViewInfo = class(TcxIUnknownObject)
  strict private
    FOwner: TObject;

    function GetLookAndFeelPainter: TcxCustomLookAndFeelPainter; inline;
  protected
    FDrawingStage: TdxSpreadSheetDrawingStage;

    function CanDraw(ACanvas: TcxCanvas; AStage: TdxSpreadSheetDrawingStage): Boolean; virtual;
    procedure DoDraw(ACanvas: TcxCanvas); virtual;
    function GetCursor(AHitTest: TdxSpreadSheetCustomHitTest): TCursor; virtual;
    function GetDragAndDropObjectClass(AHitTest: TdxSpreadSheetCustomHitTest): TcxDragAndDropObjectClass; virtual;
    function GetPopupMenuClass(AHitTest: TdxSpreadSheetCustomHitTest): TComponentClass; virtual;
    function GetSpreadSheet: TdxCustomSpreadSheet; virtual; abstract;
    function InitHitTest(AHitTest: TdxSpreadSheetCustomHitTest): Boolean; overload; virtual;
    function InitHitTest(AHitTest: TdxSpreadSheetCustomHitTest; const AOffset: TPoint): Boolean; overload; virtual;
    //
    property Owner: TObject read FOwner;
  public
    constructor Create(AOwner: TObject); virtual;
    procedure Draw(ACanvas: TcxCanvas; AStage: TdxSpreadSheetDrawingStage); overload; virtual; 
    procedure Draw(ACanvas: TcxCanvas; AStage: TdxSpreadSheetDrawingStage; const AOffset: TPoint); overload;
    procedure OffsetOrigin(const AOffset: TPoint); virtual;
    //
    property DrawingStage: TdxSpreadSheetDrawingStage read FDrawingStage;
    property LookAndFeelPainter: TcxCustomLookAndFeelPainter read GetLookAndFeelPainter;
    property SpreadSheet: TdxCustomSpreadSheet read GetSpreadSheet;
  end;

  { TdxSpreadSheetCellViewInfo }

  TdxSpreadSheetCellViewInfo = class(TdxSpreadSheetCustomCellViewInfo, IcxMouseTrackingCaller)
  private
    FAbsoluteBounds: TRect;
    FAbsoluteOrigin: TPoint;
    FCalculated: Boolean;
    FDisplayBounds: TRect;
    FDisplayClipRect: TRect;
    FScreenClipRect: TRect;

    function GetDisplayBounds: TRect; inline;
  protected
    FVisible: Boolean;

    procedure CalculateDisplayBounds; virtual;
    function CanDraw(ACanvas: TcxCanvas; AStage: TdxSpreadSheetDrawingStage): Boolean; override;
    procedure DoCalculate; virtual;
    function DoCustomDraw(ACanvas: TcxCanvas): Boolean; virtual;
    function ExcludeFromClipRgn(AStage: TdxSpreadSheetDrawingStage): Boolean; virtual;
    function GetZoomFactor: Integer; virtual;
    function InitHitTest(AHitTest: TdxSpreadSheetCustomHitTest): Boolean; override;
    procedure InvalidateRect(const R: TRect); virtual;
    function IsPermanent: Boolean; virtual;
    procedure SetAbsoluteOrigin(const AValue: TPoint); virtual;
    // IcxMouseTrackingCaller
    procedure MouseLeave; virtual;

    property Calculated: Boolean read FCalculated write FCalculated;
  public
    destructor Destroy; override;
    procedure Calculate; inline;
    procedure Draw(ACanvas: TcxCanvas; AStage: TdxSpreadSheetDrawingStage); override;
    procedure Invalidate; virtual;
    procedure OffsetOrigin(const AOffset: TPoint); override;
    procedure SetBounds(const AAbsoluteBounds, AScreenClipRect: TRect); overload; virtual;
    procedure SetBounds(const AAbsoluteOrigin: TPoint; const AAbsoluteBounds: TRect; const AScreenClipRect: TRect); overload; virtual;

    property AbsoluteBounds: TRect read FAbsoluteBounds;
    property AbsoluteOrigin: TPoint read FAbsoluteOrigin write SetAbsoluteOrigin;
    property DisplayBounds: TRect read GetDisplayBounds;
    property DisplayClipRect: TRect read FDisplayClipRect;
    property ScreenClipRect: TRect read FScreenClipRect;
    property Visible: Boolean read FVisible;
    property ZoomFactor: Integer read GetZoomFactor;
  end;

  { TdxSpreadSheetViewInfoCellsList }

  TdxSpreadSheetForEachViewInfoCellProc = function(AItem: TdxSpreadSheetCellViewInfo; AData: TObject): Boolean of object;

  TdxSpreadSheetViewInfoCellsList = class(TdxFastObjectList)
  strict private
    function GetItem(AIndex: Integer): TdxSpreadSheetCellViewInfo;  inline;
  public
    function CalculateHitTest(AHitTest: TdxSpreadSheetCustomHitTest; AReverse: Boolean = False): Boolean; overload;
    function CalculateHitTest(AHitTest: TdxSpreadSheetCustomHitTest; const AOffset: TPoint; AReverse: Boolean = False): Boolean; overload;
    procedure Clear; override;
    procedure Draw(ACanvas: TcxCanvas);
    procedure DrawWithOriginOffset(ACanvas: TcxCanvas; const AOffset: TPoint);
    procedure ForEach(AProc: TdxSpreadSheetForEachViewInfoCellProc; AData: TObject);
    function FindItem(AItemOwner: TObject): Integer;
    procedure OffsetOrigin(const AOffset: TPoint);

    property Items[Index: Integer]: TdxSpreadSheetCellViewInfo read GetItem; default;
  end;

  { TdxSpreadSheetTableViewInfoCellsList }

  TdxSpreadSheetTableViewInfoCellsList = class(TdxSpreadSheetViewInfoCellsList)
  strict private
    function GetItem(Index: Integer): TdxSpreadSheetTableViewCellViewInfo;
  public
    function FindItemForCell(ACell: TdxSpreadSheetCell; out AViewInfo: TdxSpreadSheetTableViewCellViewInfo): Boolean;
    //
    property Items[Index: Integer]: TdxSpreadSheetTableViewCellViewInfo read GetItem; default;
  end;

  { TdxSpreadSheetTableViewReferencesHighlighterItem }

  TdxSpreadSheetTableViewReferencesHighlighterItem = class(TObject)
  public
    Bounds: TRect;
    CellsArea: TRect;
    PositionInText: Integer;
    TextLength: Integer;
  end;

  { TdxSpreadSheetTableViewReferencesHighlighter }

  TdxSpreadSheetTableViewReferencesHighlighter = class(TdxSpreadSheetCustomCellViewInfo)
  strict private
    FReferences: TObjectList<TdxSpreadSheetTableViewReferencesHighlighterItem>;

    function GetColor(Index: Integer): TColor;
    function GetView: TdxSpreadSheetTableView;
  protected const
    CornetHitTestZoneSize = 8;
    FrameHitTestZoneSize = 6;
  protected
    FColors: TList<TColor>;
    FOrigin: TPoint;

    function CanDraw(ACanvas: TcxCanvas; AStage: TdxSpreadSheetDrawingStage): Boolean; override;
    procedure DoDraw(ACanvas: TcxCanvas); override;
    function GetAreaBounds(const AArea: TRect): TRect;
    function GetCell: TdxSpreadSheetCell; virtual; abstract;
    function GetCursor(AHitTest: TdxSpreadSheetCustomHitTest): TCursor; override;
    function GetDragAndDropObjectClass(AHitTest: TdxSpreadSheetCustomHitTest): TcxDragAndDropObjectClass; override;
    function GetSpreadSheet: TdxCustomSpreadSheet; override;
    function InitHitTest(AHitTest: TdxSpreadSheetCustomHitTest): Boolean; override;
  public
    constructor Create(AOwner: TObject); override;
    destructor Destroy; override;
    procedure Calculate(const AEdit: TcxCustomEdit); overload;
    procedure Calculate(const AFormulaText: TdxUnicodeString); overload; virtual;
    procedure CalculateColors; virtual;
    procedure HighlightReferences(AEdit: TcxCustomRichEdit); virtual;
    procedure OffsetOrigin(const AOffset: TPoint); override;
    //
    property Cell: TdxSpreadSheetCell read GetCell;
    property Colors[Index: Integer]: TColor read GetColor;
    property Origin: TPoint read FOrigin;
    property References: TObjectList<TdxSpreadSheetTableViewReferencesHighlighterItem> read FReferences;
    property View: TdxSpreadSheetTableView read GetView;
  end;

  { TdxSpreadSheetTableViewReferencesHighlighterCustomDragAndDropObject }

  TdxSpreadSheetTableViewReferencesHighlighterCustomDragAndDropObject = class(TdxSpreadSheetCustomDragAndDropObject)
  strict private
    FCursor: TCursor;
    FPrevCellArea: TRect;
    FReference: TdxSpreadSheetTableViewReferencesHighlighterItem;
    FReferenceIndex: Integer;

    function GetEdit: TcxCustomEdit;
    function GetHighlighter: TdxSpreadSheetTableViewReferencesHighlighter;
    function GetView: TdxSpreadSheetTableView;
  protected
    FBounds: TRect;
    FCapturePoint: TPoint;
    FContentOrigin: TPoint;
    FContentRect: TRect;

    procedure BeforeBeginDragAndDrop; override;
    procedure BeginDragAndDrop; override;
    function CheckForContentRect(const P: TPoint): TPoint;
    procedure CheckRowAndColumnIndexes(var ARowIndex, AColumnIndex: Integer);
    procedure DragAndDrop(const P: TPoint; var Accepted: Boolean); override;
    procedure EndDragAndDrop(Accepted: Boolean); override;
    function GetAbsoluteCellBounds(const ARowIndex, AColumnIndex: Integer): TRect;
    procedure GetCellAtAbsolutePoint(const P: TPoint; out ARowIndex, AColumnIndex: Integer);
    function GetDragAndDropCursor(Accepted: Boolean): TCursor; override;
    function GetImmediateStart: Boolean; override;
    function TranslateCoords(const P: TPoint): TPoint; override;
    procedure UpdateReference(const AReference: TRect);
  public
    property CapturePoint: TPoint read FCapturePoint;
    property ContentOrigin: TPoint read FContentOrigin;
    property ContentRect: TRect read FContentRect;
    property Edit: TcxCustomEdit read GetEdit;
    property Highlighter: TdxSpreadSheetTableViewReferencesHighlighter read GetHighlighter;
    property Reference: TdxSpreadSheetTableViewReferencesHighlighterItem read FReference;
    property ReferenceIndex: Integer read FReferenceIndex;
    property View: TdxSpreadSheetTableView read GetView;
  end;

  { TdxSpreadSheetTableViewReferencesHighlighterMoveDragAndDropObject }

  TdxSpreadSheetTableViewReferencesHighlighterMoveDragAndDropObject = class(
    TdxSpreadSheetTableViewReferencesHighlighterCustomDragAndDropObject)
  strict private
    FColumnDelta: Integer;
    FRowDelta: Integer;
  protected
    procedure BeginDragAndDrop; override;
    procedure DragAndDrop(const P: TPoint; var Accepted: Boolean); override;
  end;

  { TdxSpreadSheetTableViewReferencesHighlighterResizeDragAndDropObject } 

  TdxSpreadSheetTableViewReferencesHighlighterResizeDragAndDropObject = class(
    TdxSpreadSheetTableViewReferencesHighlighterCustomDragAndDropObject)
  strict private
    FCorner: TdxCorner;
  protected
    procedure AdjustRowAndColumnIndexes(const P: TPoint; var ARowIndex, AColumnIndex: Integer); virtual;
    procedure BeforeBeginDragAndDrop; override;
    function CalculateReferece(ARowIndex, AColumnIndex: Integer): TRect;
    procedure DragAndDrop(const P: TPoint; var Accepted: Boolean); override;
    //
    property Corner: TdxCorner read FCorner;
  end;

  { TdxSpreadSheetTableViewEditingCellReferencesHighlighter }

  TdxSpreadSheetTableViewEditingCellReferencesHighlighter = class(TdxSpreadSheetTableViewReferencesHighlighter)
  strict private
    function GetEditingController: TdxSpreadSheetTableViewEditingController;
  protected
    function GetCell: TdxSpreadSheetCell; override;
  public
    property EditingController: TdxSpreadSheetTableViewEditingController read GetEditingController;
  end;

  { TdxSpreadSheetContainerViewInfo }

  TdxSpreadSheetContainerViewInfo = class(TdxSpreadSheetCellViewInfo)
  strict private
    FAlpha: Byte;
    FContentBounds: TdxRectF;
    FIsDragging: Boolean;
    FRotationAngle: Double;
    FSelection: TdxSpreadSheetContainerSelectionCellViewInfo;
    FTransformMatrix: TdxGPMatrix;
    FTransformMatrixInv: TdxGPMatrix;

    function GetOptionsView: TdxSpreadSheetOptionsView; inline;
    function GetOwner: TdxSpreadSheetContainer; inline;
    function GetSelected: Boolean;
    function GetTransform: TdxSpreadSheetContainerTransform; inline;
    procedure InternalDraw(ACanvas: TdxGPCanvas);
    procedure SetAlpha(const AValue: Byte);
    procedure SetContentBounds(const AValue: TdxRectF);
    procedure SetIsDragging(const Value: Boolean);
    procedure SetRotationAngle(const AValue: Double);
    procedure SetSelected(const AValue: Boolean);
  protected
    FFlipHorz: Boolean;
    FFlipVert: Boolean;

    procedure CalculateDisplayBounds; override;
    procedure CalculateDisplayClipRect; virtual;
    procedure CalculateSelection; virtual;
    procedure CalculateTransformMatrix; virtual;
    function CanDraw(ACanvas: TcxCanvas; AStage: TdxSpreadSheetDrawingStage): Boolean; override;
    procedure ContentBoundsChanged; virtual;
    function CreateSelectionViewInfo: TdxSpreadSheetContainerSelectionCellViewInfo; virtual;
    procedure DoCalculate; override;
    procedure DoDraw(ACanvas: TcxCanvas); override;
    function DoInitHitTest(const P: TPoint; AHitTest: TdxSpreadSheetCustomHitTest): Boolean; virtual;
    procedure DrawBackground(ACanvas: TdxGPCanvas); virtual;
    procedure DrawBorder(ACanvas: TdxGPCanvas); virtual;
    procedure DrawContent(ACanvas: TdxGPCanvas); virtual;
    function GetBorderWidth: Single; virtual;
    function GetDragAndDropObjectClass(AHitTest: TdxSpreadSheetCustomHitTest): TcxDragAndDropObjectClass; override;
    function GetPopupMenuClass(AHitTest: TdxSpreadSheetCustomHitTest): TComponentClass; override;
    function GetRealBounds: TRect;
    function GetRealDrawingBounds: TRect; virtual;
    function GetSpreadSheet: TdxCustomSpreadSheet; override;
    function GetZoomFactor: Integer; override;
    function InitHitTest(AHitTest: TdxSpreadSheetCustomHitTest): Boolean; override;
    procedure InvalidateRect(const R: TRect); override;
    function IsPermanent: Boolean; override;
    procedure UpdateState; virtual;
  public
    constructor Create(AContainer: TdxSpreadSheetContainer); reintroduce; virtual;
    destructor Destroy; override;
    procedure Invalidate; override;
    //
    property Alpha: Byte read FAlpha write SetAlpha;
    property BorderWidth: Single read GetBorderWidth;
    property ContentBounds: TdxRectF read FContentBounds write SetContentBounds;
    property IsDragging: Boolean read FIsDragging write SetIsDragging;
    property OptionsView: TdxSpreadSheetOptionsView read GetOptionsView;
    property Owner: TdxSpreadSheetContainer read GetOwner;
    property RealBounds: TRect read GetRealBounds;
    property RealDrawingBounds: TRect read GetRealDrawingBounds;
    property RotationAngle: Double read FRotationAngle write SetRotationAngle;
    property Selected: Boolean read GetSelected write SetSelected;
    property Selection: TdxSpreadSheetContainerSelectionCellViewInfo read FSelection;
    property Transform: TdxSpreadSheetContainerTransform read GetTransform;
    property TransformMatrix: TdxGPMatrix read FTransformMatrix;
    property TransformMatrixInv: TdxGPMatrix read FTransformMatrixInv;
  end;

  { TdxSpreadSheetContainerCustomDragAndDropObject }

  TdxSpreadSheetContainerCustomDragAndDropObject = class(TdxSpreadSheetCustomDragAndDropObject)
  strict private
    FBounds: TRect;
    FContentRect: TRect;
    FTableViewIntf: IdxSpreadSheetTableView;
    FViewInfo: TdxSpreadSheetContainerViewInfo;

    function CheckIsKeyPressed(const Index: Integer): Boolean;
    function GetContainer: TdxSpreadSheetContainer; inline;
    function GetContentOrigin: TPoint;
    function GetPivotPoint: TPoint;
  protected
    FCapturePoint: TPoint;

    procedure AlignToCell(var ADeltaX, ADeltaY: Integer; const P: TPoint); virtual;
    procedure AlignToCells(var R: TRect);
    procedure BeforeBeginDragAndDrop; override;
    procedure BeginDragAndDrop; override;
    function CheckForContentArea(const R: TRect): TRect; inline;
    procedure DragAndDrop(const P: TPoint; var Accepted: Boolean); override;
    procedure EndDragAndDrop(Accepted: Boolean); override;
    function GetContainerViewInfo: TdxSpreadSheetContainerViewInfo; virtual;
    function GetContentRect: TRect; virtual;
    function GetDragAndDropCursor(Accepted: Boolean): TCursor; override;
    function GetImmediateStart: Boolean; override;
    procedure InvalidateRects(const R1, R2: TRect);
    function ProcessKeyDown(AKey: Word; AShiftState: TShiftState): Boolean; override;
    function ProcessKeyUp(AKey: Word; AShiftState: TShiftState): Boolean; override;
    procedure RecalculateAnchors; virtual;
    procedure RecalculateDragAndDropInfo;
    function TranslateCoords(const P: TPoint): TPoint; override;

    property IsAltPressed: Boolean index VK_MENU read CheckIsKeyPressed;
    property IsControlPressed: Boolean index VK_CONTROL read CheckIsKeyPressed;
    property IsShiftPressed: Boolean index VK_SHIFT read CheckIsKeyPressed;
  public
    property Bounds: TRect read FBounds;
    property CapturePoint: TPoint read FCapturePoint;
    property Container: TdxSpreadSheetContainer read GetContainer;
    property ContentOrigin: TPoint read GetContentOrigin;
    property ContentRect: TRect read FContentRect;
    property PivotPoint: TPoint read GetPivotPoint;
    property TableViewIntf: IdxSpreadSheetTableView read FTableViewIntf;
    property ViewInfo: TdxSpreadSheetContainerViewInfo read FViewInfo;
  end;

  { TdxSpreadSheetContainerCustomMoveDragAndDropObject }
  
  TdxSpreadSheetContainerCustomMoveDragAndDropObject = class(TdxSpreadSheetContainerCustomDragAndDropObject)
  protected
    procedure AlignToAxes(var R: TRect); virtual;
    procedure BeginDragAndDrop; override;
    procedure DragAndDrop(const P: TPoint; var Accepted: Boolean); override;
    function GetDragAndDropCursor(Accepted: Boolean): TCursor; override;
  end;

  { TdxSpreadSheetClonedContainerMoveDragAndDropObject } 

  TdxSpreadSheetClonedContainerMoveDragAndDropObject = class(TdxSpreadSheetContainerCustomMoveDragAndDropObject)
  strict private
    FContainer: TdxSpreadSheetContainer;

    function GetContainers: TdxSpreadSheetViewInfoCellsList;
  protected
    procedure BeginDragAndDrop; override;
    function GetContainerViewInfo: TdxSpreadSheetContainerViewInfo; override;
    function TranslateCoords(const P: TPoint): TPoint; override;
  public
    constructor Create(AContainer: TdxSpreadSheetContainer); reintroduce; virtual;
    //
    property Containers: TdxSpreadSheetViewInfoCellsList read GetContainers;
  end;

  { TdxSpreadSheetContainerMoveDragAndDropObject }

  TdxSpreadSheetContainerMoveDragAndDropObject = class(TdxSpreadSheetContainerCustomMoveDragAndDropObject)
  strict private
    FClonedContainer: TdxSpreadSheetContainer;
    FClonedContainerDragAndDropObject: TdxSpreadSheetClonedContainerMoveDragAndDropObject;

    function GetMoveClone: Boolean;
    procedure SetMoveClone(AValue: Boolean);
  protected
    function CreateDragAndDropObjectForClonnedContainer: TdxSpreadSheetClonedContainerMoveDragAndDropObject; virtual;
    procedure DragAndDrop(const P: TPoint; var Accepted: Boolean); override;
    procedure EndDragAndDrop(Accepted: Boolean); override;
    //
    property ClonedContainer: TdxSpreadSheetContainer read FClonedContainer;
    property ClonedContainerDragAndDropObject: TdxSpreadSheetClonedContainerMoveDragAndDropObject read FClonedContainerDragAndDropObject;
  public
    property MoveClone: Boolean read GetMoveClone write SetMoveClone;
  end;

  { TdxSpreadSheetContainerResizeDragAndDropObject }

  TdxSpreadSheetContainerResizeDragAndDropObject = class(TdxSpreadSheetContainerCustomDragAndDropObject)
  strict private
    procedure ApplyBounds(const AFixedPoint, AResizedPoint: TdxPointF);
    procedure GetPoints(out AFixedPoint, AResizedPoint: TdxPointF);
    function GetTransform: TdxSpreadSheetContainerTransform; inline;
  protected
    FMarker: TdxSpreadSheetSizingMarker;

    procedure AlignToCells(var ADeltaX, ADeltaY: Single); 
    function CalculateContentBounds(const ADeltaX, ADeltaY: Single): TdxRectF;
    procedure CheckAspectUsingCornerHandles(var ADeltaX, ADeltaY: Single);

    procedure BeginDragAndDrop; override;
    procedure DragAndDrop(const P: TPoint; var Accepted: Boolean); override;
    procedure EndDragAndDrop(Accepted: Boolean); override;
    function GetContainerViewInfo: TdxSpreadSheetContainerViewInfo; override;
    procedure RecalculateBounds; virtual;
  public
    property Marker: TdxSpreadSheetSizingMarker read FMarker;
    property Transform: TdxSpreadSheetContainerTransform read GetTransform;
  end;

  { TdxSpreadSheetContainerRotateDragAndDropObject }
 
  TdxSpreadSheetContainerRotateDragAndDropObject = class(TdxSpreadSheetContainerCustomDragAndDropObject)
  protected
    procedure BeginDragAndDrop; override;
    procedure DragAndDrop(const P: TPoint; var Accepted: Boolean); override;
    procedure EndDragAndDrop(Accepted: Boolean); override;
    function GetContainerViewInfo: TdxSpreadSheetContainerViewInfo; override;
    function GetDragAndDropCursor(Accepted: Boolean): TCursor; override;
  end;

  { TdxSpreadSheetContainerSelectionCellViewInfo }

  TdxSpreadSheetContainerSelectionCellViewInfo = class(TdxSpreadSheetCellViewInfo)
  strict private
    FContainerViewInfo: TdxSpreadSheetContainerViewInfo;
  protected
    FRotateMarkerSize: Integer;
    FSizeMarkersArea: TRect;
    FSizeMarkerSize: Integer;

    procedure CalculateDisplayBounds; override;
    function CanDraw(ACanvas: TcxCanvas; AStage: TdxSpreadSheetDrawingStage): Boolean; override;
    function CreatePen: TdxGpPen; virtual;
    function CreateRotateMarkerBrush: TdxGpBrush; virtual;
    function CreateSizingMarkerBrush: TdxGpBrush; virtual;
    procedure DoDraw(ACanvas: TcxCanvas); override;
    procedure DrawContent(ACanvas: TdxGPCanvas); virtual;
    procedure DrawRotateMarker(ACanvas: TdxGPCanvas; APen: TdxGPPen); virtual;
    procedure DrawSizingMarkers(ACanvas: TdxGPCanvas; APen: TdxGPPen); virtual;
    function GetContentOffsets: TRect; virtual;
    function GetCursor(AHitTest: TdxSpreadSheetCustomHitTest): TCursor; override;
    function GetDragAndDropObjectClass(AHitTest: TdxSpreadSheetCustomHitTest): TcxDragAndDropObjectClass; override;
    function GetFrameRect: TRect; virtual;
    function GetRealDrawingBounds: TRect;
    function GetRotateMarker: TRect; virtual;
    function GetSizeMarkerAtLocalPoint(const ALocalPoint: TPoint; out AMarker: TdxSpreadSheetSizingMarker): Boolean;
    function GetSizeMarkerRect(AMarker: TdxSpreadSheetSizingMarker): TRect; virtual;
    function GetSpreadSheet: TdxCustomSpreadSheet; override;
    function GetZoomFactor: Integer; override;
    function InitHitTest(AHitTest: TdxSpreadSheetCustomHitTest): Boolean; override;
    procedure InvalidateRect(const R: TRect); override;
    //
    property RotateMarkerSize: Integer read FRotateMarkerSize;
    property SizingPointSize: Integer read FSizeMarkerSize;
  public
    constructor Create(AContainerViewInfo: TdxSpreadSheetContainerViewInfo); reintroduce; virtual;
    function HasResizeMarkers: Boolean;
    function HasRotateMarker: Boolean;
    procedure Invalidate; override;
    //
    property ContainerViewInfo: TdxSpreadSheetContainerViewInfo read FContainerViewInfo;
    property ContentOffsets: TRect read GetContentOffsets;
    property FrameRect: TRect read GetFrameRect;
    property RealDrawingBounds: TRect read GetRealDrawingBounds;
    property RotateMarker: TRect read GetRotateMarker;
    property SizeMarkerRect[Marker: TdxSpreadSheetSizingMarker]: TRect read GetSizeMarkerRect;
    property SizeMarkersArea: TRect read FSizeMarkersArea;
  end;

  { TdxSpreadSheetShapeContainerViewInfo }

  TdxSpreadSheetShapeContainerViewInfo = class(TdxSpreadSheetContainerViewInfo)
  strict private
    FShapeOutlinePath: TdxGPPath;
    FShapePath: TdxGPPath;

    function GetOwner: TdxSpreadSheetShapeContainer; inline;
    function GetShape: TdxSpreadSheetShape; inline;
    function GetShapeOutlinePath: TdxGPPath;
    function GetShapePath: TdxGPPath;
  protected
    procedure ContentBoundsChanged; override;
    function CreateShapeOutlinePath: TdxGPPath; virtual;
    function CreateShapePath: TdxGPPath; virtual;
    function DoCreateShapePath(const R: TdxRectF): TdxGPPath; virtual;
    function DoInitHitTest(const P: TPoint; AHitTest: TdxSpreadSheetCustomHitTest): Boolean; override;
    procedure DrawBackground(ACanvas: TdxGPCanvas); override;
    procedure DrawBorder(ACanvas: TdxGPCanvas); override;
    function GetBorderWidth: Single; override;
    function GetRadius: Single; virtual;
    function GetRealDrawingBounds: TRect; override;
  public
    destructor Destroy; override;
    procedure RecreateShape;
    //
    property Owner: TdxSpreadSheetShapeContainer read GetOwner;
    property Shape: TdxSpreadSheetShape read GetShape;
    property ShapeOutlinePath: TdxGPPath read GetShapeOutlinePath;
    property ShapePath: TdxGPPath read GetShapePath;
  end;

  { TdxSpreadSheetPictureContainerViewInfo }

  TdxSpreadSheetPictureContainerViewInfo = class(TdxSpreadSheetShapeContainerViewInfo)
  strict private
    function GetOwner: TdxSpreadSheetPictureContainer; inline;
    function GetPicture: TdxSpreadSheetPicture; inline;
  protected
    procedure DrawContent(ACanvas: TdxGPCanvas); override;
  public
    property Owner: TdxSpreadSheetPictureContainer read GetOwner;
    property Picture: TdxSpreadSheetPicture read GetPicture;
  end;

  { TdxSpreadSheetResizeTableItemDragAndDropObject }

  TdxSpreadSheetResizeTableItemDragAndDropObject = class(TdxSpreadSheetCustomDragAndDropObject)
  strict private
    function GetHitTest: TdxSpreadSheetTableViewHitTest;
    function GetOrientation: TdxOrientation;
    function GetView: TdxSpreadSheetTableView;
  protected
    ItemIndex: Integer;
    Items: TdxSpreadSheetTableItems;
    ItemStartPos: TPoint;
    StartPos: TPoint;

    procedure BeginDragAndDrop; override;
    procedure ChangePosition;
    function CheckPoint(const P: TPoint): TPoint;
    procedure DrawSizingMark(const ARect: TRect);
    function GetDistance(const P1, P2: TPoint): Integer;
    function GetDragAndDropCursor(Accepted: Boolean): TCursor; override;
    function GetImmediateStart: Boolean; override;
    function GetSizingMarkBounds(const P: TPoint): TRect;
    function IsEntireSheetArea(const R: TRect): Boolean;
    procedure DragAndDrop(const P: TPoint; var Accepted: Boolean); override;
    procedure EndDragAndDrop(Accepted: Boolean); override;
    function TranslateCoords(const P: TPoint): TPoint; override;
  public
    constructor Create(AControl: TcxControl); override;

    property HitTest: TdxSpreadSheetTableViewHitTest read GetHitTest;
    property Orientation: TdxOrientation read GetOrientation;
    property View: TdxSpreadSheetTableView read GetView;
  end;

  { TdxSpreadSheetTableViewCustomCellViewInfo }

  TdxSpreadSheetTableViewCustomCellViewInfo = class(TdxSpreadSheetCellViewInfo)
  private
    function GetOwner: TdxSpreadSheetTableViewInfo; inline;
    function GetView: TdxSpreadSheetTableView; inline;
  protected
    function DoCustomDraw(ACanvas: TcxCanvas): Boolean; override;
    function GetPopupMenuClass(AHitTest: TdxSpreadSheetCustomHitTest): TComponentClass; override;
    function GetSpreadSheet: TdxCustomSpreadSheet; override;
    function GetZoomFactor: Integer; override;
    procedure InvalidateRect(const R: TRect); override;
  public
    property Owner: TdxSpreadSheetTableViewInfo read GetOwner;
    property View: TdxSpreadSheetTableView read GetView;
  end;

  { TdxSpreadSheetTableViewHeaderCellViewInfo }

  TdxSpreadSheetTableViewHeaderCellViewInfo = class(TdxSpreadSheetTableViewCustomCellViewInfo)
  strict private
    FAlignHorz: TAlignment;
    FAlignVert: TcxAlignmentVert;
    FDisplayText: TdxUnicodeString;
    FItem: TdxSpreadSheetTableItem;
    FIndex: Integer;
    FIsFirst: Boolean;
    FIsLast: Boolean;
    FNeighbors: TcxNeighbors;
    FState: TcxButtonState;

    procedure SetState(AValue: TcxButtonState);
  protected
    FNextNeighbor: TdxSpreadSheetTableViewHeaderCellViewInfo;
    FViewParams: TcxViewParams;

    procedure ApplyBestFit; inline;
    function DoCustomDraw(ACanvas: TcxCanvas): Boolean; override;
    procedure DoDraw(ACanvas: TcxCanvas); override;
    function GetBorders: TcxBorders; virtual;
    function GetCursor(AHitTest: TdxSpreadSheetCustomHitTest): TCursor; override;
    function GetDragAndDropObjectClass(AHitTest: TdxSpreadSheetCustomHitTest): TcxDragAndDropObjectClass; override;
    function GetTextColor: TColor; virtual;
    procedure Initialize(AItem: TdxSpreadSheetTableItem; AIndex: Integer;
      ANeighbors: TcxNeighbors; const ADisplayText: TdxUnicodeString);
    function InitHitTest(AHitTest: TdxSpreadSheetCustomHitTest): Boolean; override;

    property Item: TdxSpreadSheetTableItem read FItem;
  public
    property AlignHorz: TAlignment read FAlignHorz write FAlignHorz;
    property AlignVert: TcxAlignmentVert read FAlignVert write FAlignVert;
    property Borders: TcxBorders read GetBorders;
    property DisplayText: TdxUnicodeString read FDisplayText write FDisplayText;
    property Index: Integer read FIndex;
    property IsFirst: Boolean read FIsFirst;
    property IsLast: Boolean read FIsLast;
    property Neighbors: TcxNeighbors read FNeighbors;
    property State: TcxButtonState read FState write SetState;
    property ViewParams: TcxViewParams read FViewParams;
  end;

  { TdxSpreadSheetTableViewCellViewInfo }

  TdxSpreadSheetTableViewCellViewInfo = class(TdxSpreadSheetTableViewCustomCellViewInfo)
  strict private
    FAlignHorz: TcxTextAlignX;
    FAlignVert: TcxTextAlignY;
    FCell: TdxSpreadSheetCell;
    FColumn: Integer;
    FDrawValueInitialized: Boolean;
    FIsLeftmost: Boolean;
    FIsRightmost: Boolean;
    FMultiline: Boolean;
    FRow: Integer;
    FStyle: TdxSpreadSheetCellStyle;

    function GetBorderColor(ASide: TcxBorder): TColor;
    function GetBorderStyle(ASide: TcxBorder): TdxSpreadSheetCellBorderStyle;
    function GetIsEditing: Boolean;
    function GetIsMerged: Boolean;
  protected
    FBackgroundColor: TColor;
    FBorderColor: array[TcxBorder] of TColor;
    FBorderStyle: array[TcxBorder] of TdxSpreadSheetCellBorderStyle;
    FContentBounds: TRect;
    FDisplayText: TdxUnicodeString;
    FFillStyle: TdxSpreadSheetCellFillStyle;
    FFocused: Boolean;
    FFontSize: Integer;
    FForegroundColor: TColor;
    FIsNumeric: Boolean;
    FMergedCell: TdxSpreadSheetMergedCell;
    FSelected: Boolean;
    FTextBounds: TRect;
    FTextColor: TColor;

    function CanDraw(ACanvas: TcxCanvas; AStage: TdxSpreadSheetDrawingStage): Boolean; override;
    procedure CalculateTextBounds(const R: TRect; ACell: TdxSpreadSheetCell); virtual;
    function ContentRect(const R: TRect): TRect;
    procedure DoCalculate; override;
    function DoCustomDraw(ACanvas: TcxCanvas): Boolean; override;
    procedure DoDraw(ACanvas: TcxCanvas); override;
    procedure DrawBackground(ACanvas: TcxCanvas); virtual;
    procedure DrawBorder(ACanvas: TcxCanvas; ASide: TcxBorder); //inline;
    procedure DrawBorders(ACanvas: TcxCanvas); virtual;
    procedure DrawSelection(ACanvas: TcxCanvas); virtual;
    procedure DrawValue(ACanvas: TcxCanvas); virtual;
    function GetBorderBounds(ASide: TcxBorder): TRect; inline;
    function GetTextOutFormat: Integer; inline;
    function HasBorder(ASide: TcxBorder): Boolean; inline;
    function HasValue: Boolean; inline;
    procedure RemoveBorder(ASide: TcxBorder); inline;
    procedure SetBorderStyle(ASide: TcxBorder; AStyle: TdxSpreadSheetCellBorderStyle; AColor: TColor);
    procedure Initialize(ACell: TdxSpreadSheetCell; AStyle: TdxSpreadSheetCellStyle; ARowIndex, AColumnIndex: Integer);
    procedure InitDrawValue; virtual;
    procedure InitSelection;
    function InitHitTest(AHitTest: TdxSpreadSheetCustomHitTest): Boolean; override;
    function IsInternalBorder(ASide: TcxBorder): Boolean; inline;
    function IsPermanent: Boolean; override;
    function IsTextOutOfBounds: Boolean; inline;
    procedure UpdateState; virtual;

    property DrawValueInitialized: Boolean read FDrawValueInitialized write FDrawValueInitialized;
  public
    procedure OffsetOrigin(const AOffset: TPoint); override;
    procedure SetBounds(const AAbsoluteOrigin: TPoint; const AAbsoluteBounds: TRect; const AScreenClipRect: TRect); override;

    property AlignHorz: TcxTextAlignX read FAlignHorz write FAlignHorz;
    property AlignVert: TcxTextAlignY read FAlignVert write FAlignVert;
    property BackgroundColor: TColor read FBackgroundColor;
    property BorderColor[ASide: TcxBorder]: TColor read GetBorderColor;
    property BorderStyle[ASide: TcxBorder]: TdxSpreadSheetCellBorderStyle read GetBorderStyle;
    property Cell: TdxSpreadSheetCell read FCell;
    property Column: Integer read FColumn;
    property ContentBounds: TRect read FContentBounds;
    property DisplayText: TdxUnicodeString read FDisplayText write FDisplayText;
    property FillStyle: TdxSpreadSheetCellFillStyle read FFillStyle;
    property Focused: Boolean read FFocused;
    property FontSize: Integer read FFontSize;
    property ForegroundColor: TColor read FForegroundColor;
    property IsEditing: Boolean read GetIsEditing;
    property IsLeftmost: Boolean read FIsLeftmost;
    property IsMerged: Boolean read GetIsMerged;
    property IsRightmost: Boolean read FIsRightmost;
    property MergedCell: TdxSpreadSheetMergedCell read FMergedCell;
    property Multiline: Boolean read FMultiline write FMultiline;
    property Row: Integer read FRow;
    property Selected: Boolean read FSelected;
    property Style: TdxSpreadSheetCellStyle read FStyle;
    property TextBounds: TRect read FTextBounds;
    property TextColor: TColor read FTextColor;
  end;

  { TdxSpreadSheetTableViewFrozenPaneSeparatorCellViewInfo }

  TdxSpreadSheetTableViewFrozenPaneSeparatorCellViewInfo = class(TdxSpreadSheetTableViewCustomCellViewInfo)
  private
    FColor: TColor;
  protected
    function DoCustomDraw(ACanvas: TcxCanvas): Boolean; override;
    procedure DoDraw(ACanvas: TcxCanvas); override;
    function InitHitTest(AHitTest: TdxSpreadSheetCustomHitTest): Boolean; override;
  public
    property Color: TColor read FColor write FColor;
  end;

  { TdxSpreadSheetPageControlHitTest }

  TdxSpreadSheetPageControlHitTest = class(TdxSpreadSheetCustomHitTest)
  strict private
    function GetPageControl: TdxSpreadSheetPageControl;
  public
    procedure Calculate(const AHitPoint: TPoint); override;

    property HitAtBackground: Boolean index hcBackground read GetHitCode;
    property HitAtButton: Boolean index hcButton read GetHitCode;
    property HitAtPageTab: Boolean index hcPageTab read GetHitCode;
    property HitAtSplitter: Boolean index hcSplitter read GetHitCode;
    property PageControl: TdxSpreadSheetPageControl read GetPageControl;
  end;

  { TdxSpreadSheetPageControlController }

  TdxSpreadSheetPageControlController = class(TdxSpreadSheetCustomController)
  strict private
    FPressedObject: TdxSpreadSheetCellViewInfo;
    function GetPageControlHitTest: TdxSpreadSheetPageControlHitTest; inline;
    function GetPageControl: TdxSpreadSheetPageControl;
    procedure SetPressedObject(AValue: TdxSpreadSheetCellViewInfo);
    function UpdateCellState(ACell: TdxSpreadSheetCellViewInfo; AData: TObject): Boolean;
  protected
    function GetHitTest: TdxSpreadSheetCustomHitTest; override;
    function GetSpreadSheet: TdxCustomSpreadSheet; override;
    procedure DoMouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure DoMouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure DoMouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure UpdateStates;
  public
    property HitTest: TdxSpreadSheetPageControlHitTest read GetPageControlHitTest;
    property PageControl: TdxSpreadSheetPageControl read GetPageControl;
    property PressedObject: TdxSpreadSheetCellViewInfo read FPressedObject write SetPressedObject;
  end;

  { TdxSpreadSheetPageControlBackgroundCellViewInfo }

  TdxSpreadSheetPageControlBackgroundCellViewInfo = class(TdxSpreadSheetCellViewInfo)
  strict private
    FState: TcxButtonState;
    FViewParams: TcxViewParams;

    function GetPageControl: TdxSpreadSheetPageControl; inline;
    procedure SetState(AValue: TcxButtonState);
  protected
    function DoCustomDraw(ACanvas: TcxCanvas): Boolean; override;
    procedure DoDraw(ACanvas: TcxCanvas); override;
    function GetHitCode: Integer; virtual;
    function GetSpreadSheet: TdxCustomSpreadSheet; override;
    function GetState: TcxButtonState; virtual;
    function InitHitTest(AHitTest: TdxSpreadSheetCustomHitTest): Boolean; override;
    procedure InitViewParams; virtual;
    procedure MouseLeave; override;
    procedure UpdateState; virtual;

    property State: TcxButtonState read GetState write SetState;
  public
    constructor Create(AOwner: TObject); override;
    destructor Destroy; override;

    property PageControl: TdxSpreadSheetPageControl read GetPageControl;
    property ViewParams: TcxViewParams read FViewParams;
  end;

  { TdxSpreadSheetPageControlButtonCellViewInfo }

  TdxSpreadSheetPageControlButtonCellViewInfo = class(TdxSpreadSheetPageControlBackgroundCellViewInfo)
  strict private
    FButtonType: TdxSpreadSheetPageControlButton;
  protected
    procedure DoClick; virtual;
    procedure DoDraw(ACanvas: TcxCanvas); override;
    function GetHitCode: Integer; override;
    procedure Initialize(const ABounds, AClipRect: TRect; AType: TdxSpreadSheetPageControlButton);
    procedure UpdateState; override;
  public
    property State;
    property ButtonType: TdxSpreadSheetPageControlButton read FButtonType;
  end;

  { TdxSpreadSheetPageControlTabDragAndDropObject }

  TdxSpreadSheetPageControlTabDragAndDropObject = class(TcxDragAndDropObject)
  strict private
    function GetLeftScrollArea: TRect;
    function GetHitTest: TdxSpreadSheetPageControlHitTest;
    function GetHitTestView: TdxSpreadSheetCustomView;
    function GetPageControl: TdxSpreadSheetPageControl;
    function GetRightScrollArea: TRect;
  protected
    Arrows: TcxPlaceArrows;
    DropIndex: Integer;
    ScrollDelta: Integer;
    ScrollTimer: TcxTimer;
    View: TdxSpreadSheetCustomView;
    
    procedure DragAndDrop(const P: TPoint; var Accepted: Boolean); override;
    procedure EndDragAndDrop(Accepted: Boolean); override;
    function GetDragAndDropCursor(Accepted: Boolean): TCursor; override;
    procedure InitScrollTimer(ADelta: Integer);
    procedure ScrollTimerHandler(ASender: TObject);
  public
    constructor Create(AControl: TcxControl); override;
    destructor Destroy; override;

    property HitTest: TdxSpreadSheetPageControlHitTest read GetHitTest;
    property HitTestView: TdxSpreadSheetCustomView read GetHitTestView;
    property LeftScrollArea: TRect read GetLeftScrollArea;
    property PageControl: TdxSpreadSheetPageControl read GetPageControl;
    property RightScrollArea: TRect read GetRightScrollArea;
  end;

  { TdxSpreadSheetPageControlTabCellViewInfo }

  TdxSpreadSheetPageControlTabCellViewInfo = class(TdxSpreadSheetPageControlBackgroundCellViewInfo)
  strict private
    FView: TdxSpreadSheetCustomView;
  protected
    function CanDraw(ACanvas: TcxCanvas; AStage: TdxSpreadSheetDrawingStage): Boolean; override;
    procedure DoDraw(ACanvas: TcxCanvas); override;
    function GetDragAndDropObjectClass(AHitTest: TdxSpreadSheetCustomHitTest): TcxDragAndDropObjectClass; override;
    function GetHitCode: Integer; override;
    function GetPopupMenuClass(AHitTest: TdxSpreadSheetCustomHitTest): TComponentClass; override;
    function GetState: TcxButtonState; override;
    class function GetTextMargins: TRect; virtual;
    procedure Initialize(const ABounds, AClipRect: TRect; AView: TdxSpreadSheetCustomView);
  public
    property State;
    property View: TdxSpreadSheetCustomView read FView;
  end;

  { TdxSpreadSheetPageControlSplitterCellViewInfo }

  TdxSpreadSheetPageControlSplitterCellViewInfo = class(TdxSpreadSheetPageControlBackgroundCellViewInfo)
  protected
    function GetCursor(AHitTest: TdxSpreadSheetCustomHitTest): TCursor; override;
    procedure DoDraw(ACanvas: TcxCanvas); override;
    function ExcludeFromClipRgn(AStage: TdxSpreadSheetDrawingStage): Boolean; override;
    function GetDragAndDropObjectClass(AHitTest: TdxSpreadSheetCustomHitTest): TcxDragAndDropObjectClass; override;
    function GetHitCode: Integer; override;
    function GetState: TcxButtonState; override;
    function MeasureWidth: Integer; virtual;
  public
    property State;
  end;

  { TdxSpreadSheetPageControlSplitterDragAndDropObject }

  TdxSpreadSheetPageControlSplitterDragAndDropObject = class(TcxDragAndDropObject)
  strict private
    StartPos: TPoint;
    StartWidth: Integer;
    function GetPageControl: TdxSpreadSheetPageControl;
  protected
    procedure BeginDragAndDrop; override;
    procedure DragAndDrop(const P: TPoint; var Accepted: Boolean); override;
    procedure EndDragAndDrop(Accepted: Boolean); override;
    function GetImmediateStart: Boolean; override;
  public
    property PageControl: TdxSpreadSheetPageControl read GetPageControl;
  end;

  { TdxSpreadSheetPageControlViewInfo }

  TdxSpreadSheetPageControlViewInfo = class
  strict private
    FCells: TdxSpreadSheetViewInfoCellsList;
    FClipRect: TRect;
    FHasInvisibleTab: Boolean;
    FLeftScrollArea: TRect;
    FPageControl: TdxSpreadSheetPageControl;
    FRightScrollArea: TRect;
    FViewParams: TcxViewParams;

    procedure AddButton(var R: TRect; AType: TdxSpreadSheetPageControlButton);
    procedure AddPageTab(var R: TRect; APage: TdxSpreadSheetCustomView);
    function GetLookAndFeelPainter: TcxCustomLookAndFeelPainter; inline;
    function GetSpreadSheet: TdxCustomSpreadSheet; inline;
  protected
    Bounds: TRect;
    Pages: TList<TdxSpreadSheetCustomView>;
    procedure AddCells; virtual;
    procedure InitializeViewParams; virtual;
  public
    constructor Create(AOwner: TdxSpreadSheetPageControl); virtual;
    destructor Destroy; override;
    procedure Calculate; virtual;
    procedure Draw(ACanvas: TcxCanvas); virtual;
    procedure InitHitTest(AHitTest: TdxSpreadSheetCustomHitTest); virtual;
    function MeasureHeight: Integer; virtual;
    function MeasureWidth: Integer; virtual;

    property Cells: TdxSpreadSheetViewInfoCellsList read FCells;
    property HasInvisibleTab: Boolean read FHasInvisibleTab;
    property LeftScrollArea: TRect read FLeftScrollArea;
    property LookAndFeelPainter: TcxCustomLookAndFeelPainter read GetLookAndFeelPainter;
    property PageControl: TdxSpreadSheetPageControl read FPageControl;
    property RightScrollArea: TRect read FRightScrollArea;
    property SpreadSheet: TdxCustomSpreadSheet read GetSpreadSheet;
    property ViewParams: TcxViewParams read FViewParams;
  end;

  { TdxSpreadSheetPageControl }

  TdxSpreadSheetPageControl = class(TdxSpreadSheetPersistentObject)
  strict private
    FButtons: TdxSpreadSheetPageControlButtons;
    FController: TdxSpreadSheetPageControlController;
    FDropArrowColor: TColor;
    FFirstVisiblePageIndex: Integer;
    FHitTest: TdxSpreadSheetPageControlHitTest;
    FVisible: Boolean;
    FViewInfo: TdxSpreadSheetPageControlViewInfo;
    FWidth: Integer;

    function GetBounds: TRect;
    function GetFirstVisiblePageIndex: Integer;
    function GetVisiblePage(AIndex: Integer): TdxSpreadSheetCustomView;
    function GetVisiblePageCount: Integer;
    function IsButtonsStored: Boolean;
    procedure SetButtons(AValue: TdxSpreadSheetPageControlButtons);
    procedure SetFirstVisiblePageIndex(AValue: Integer);
    procedure SetWidth(AValue: Integer);
    procedure SetVisible(AValue: Boolean);
  protected
    procedure Changed; virtual;
    function CreateController: TdxSpreadSheetPageControlController; virtual;
    function CreateHitTest: TdxSpreadSheetPageControlHitTest; virtual;
    function CreateViewInfo: TdxSpreadSheetPageControlViewInfo; virtual;
    procedure Invalidate;
    procedure Recalculate;
    //
    procedure CreateSubClasses; virtual;
    procedure DestroySubClasses; virtual;

    property Controller: TdxSpreadSheetPageControlController read FController;
    property ViewInfo: TdxSpreadSheetPageControlViewInfo read FViewInfo;
  public
    constructor Create(ASpreadSheet: TdxCustomSpreadSheet); override;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;

    property Bounds: TRect read GetBounds;
    property HitTest: TdxSpreadSheetPageControlHitTest read FHitTest;
    property VisiblePageCount: Integer read GetVisiblePageCount;
    property VisiblePages[Index: Integer]: TdxSpreadSheetCustomView read GetVisiblePage;
  published
    property Buttons: TdxSpreadSheetPageControlButtons read FButtons write SetButtons stored IsButtonsStored;
    property DropArrowColor: TColor read FDropArrowColor write FDropArrowColor default clGreen;
    property FirstVisiblePageIndex: Integer read GetFirstVisiblePageIndex write SetFirstVisiblePageIndex default 0;
    property Visible: Boolean read FVisible write SetVisible default True;
    property Width: Integer read FWidth write SetWidth default 0;
  end;

  { TdxSpreadSheetViewInfo }

  TdxSpreadSheetViewInfo = class(TdxSpreadSheetPersistentObject)
  strict private
    function GetBackgroundParams: TcxViewParams;
    function GetLookAndFeelPainter: TcxCustomLookAndFeelPainter;
  public
    property BackgroundParams: TcxViewParams read GetBackgroundParams;
    property LookAndFeelPainter: TcxCustomLookAndFeelPainter read GetLookAndFeelPainter;
  end;

  { TdxSpreadSheetStyles }

  TdxSpreadSheetStyles = class(TcxStyles)
  strict private
    function GetSpreadSheet: TdxCustomSpreadSheet;
  protected
    procedure Changed(AIndex: Integer); override;
    procedure GetDefaultViewParams(Index: Integer; AData: TObject; out AParams: TcxViewParams); override;
  public
    procedure Assign(Source: TPersistent); override;
    function GetBackgroundStyle: TcxViewParams;
    function GetContentStyle(AView: TdxSpreadSheetCustomView): TcxViewParams;
    function GetHeaderStyle(AView: TdxSpreadSheetCustomView): TcxViewParams;
    function GetPageControlStyle: TcxViewParams;
    function GetSelectionStyle: TcxViewParams;

    property SpreadSheet: TdxCustomSpreadSheet read GetSpreadSheet;
  published
    property Background: TcxStyle index s_Background read GetValue write SetValue;
    property Content: TcxStyle index s_Content read GetValue write SetValue;
    property Header: TcxStyle  index s_Header read GetValue write SetValue;
    property PageControl: TcxStyle index s_PageControl read GetValue write SetValue;
    property Selection: TcxStyle index s_Selection read GetValue write SetValue;
  end;

  { TdxSpreadSheetOptionsBehavior }

  TdxSpreadSheetOptionsBehavior = class(TdxSpreadSheetPersistentObject)
  strict private
    FAutomaticCalculation: Boolean;
    FDeleting: Boolean;
    FEditing: Boolean;
    FFormatting: Boolean;
    FHistory: Boolean;
    FInserting: Boolean;
    FIterativeCalculation: Boolean;
    FIterativeCalculationMaxCount: Integer;
    FProtected: Boolean;

    procedure SetHistory(AValue: Boolean);
    procedure SetIterativeCalculationMaxCount(AValue: Integer);
    procedure SetProtected(AValue: Boolean);
  public
    constructor Create(ASpreadSheet: TdxCustomSpreadSheet); override;
    procedure Assign(Source: TPersistent); override;
  published
    property AutomaticCalculation: Boolean read FAutomaticCalculation write FAutomaticCalculation default True;
    property Deleting: Boolean read FDeleting write FDeleting default True;
    property Editing: Boolean read FEditing write FEditing default True;
    property Formatting: Boolean read FFormatting write FFormatting default True;
    property History: Boolean read FHistory write SetHistory default False;
    property Inserting: Boolean read FInserting write FInserting default True;
    property IterativeCalculation: Boolean read FIterativeCalculation write FIterativeCalculation default False;
    property IterativeCalculationMaxCount: Integer read FIterativeCalculationMaxCount write SetIterativeCalculationMaxCount default 0;
    property Protected: Boolean read FProtected write SetProtected default False;
  end;

  { TdxSpreadSheetOptionsView }

  TdxSpreadSheetOptionsView = class(TdxSpreadSheetPersistentObject)
  strict private
    FAntialiasing: Boolean;
    FCellAutoHeight: Boolean;
    FDateTimeSystem: TdxSpreadSheetDateTimeSystem;
    FFrozenPaneSeparatorColor: TColor;
    FFrozenPaneSeparatorWidth: Integer;
    FGridLineColor: TColor;
    FGridLines: Boolean;
    FHeaders: Boolean;
    FHorizontalScrollBar: Boolean;
    FR1C1Reference: Boolean;
    FShowFormulas: Boolean;
    FVerticalScrollBar: Boolean;
    FZeroValues: Boolean;

    function GetActualDateTimeSystem: TdxSpreadSheetDateTimeSystem;
    procedure SetAntialiasing(const AValue: Boolean);
    procedure SetDateTimeSystem(AValue: TdxSpreadSheetDateTimeSystem);
    procedure SetFrozenPaneSeparatorColor(AValue: TColor);
    procedure SetFrozenPaneSeparatorWidth(AValue: Integer);
    procedure SetGridLineColor(AValue: TColor);
    procedure SetGridLines(AValue: Boolean);
    procedure SetHeaders(AValue: Boolean);
    procedure SetHorizontalScrollBar(AValue: Boolean);
    procedure SetR1C1Reference(AValue: Boolean);
    procedure SetShowFormulas(AValue: Boolean);
    procedure SetVerticalScrollBar(AValue: Boolean);
    procedure SetZeroValues(AValue: Boolean);
  protected
    procedure Changed; virtual;
  public
    constructor Create(ASpreadSheet: TdxCustomSpreadSheet); override;
    procedure Assign(Source: TPersistent); override;

    property ActualDateTimeSystem: TdxSpreadSheetDateTimeSystem read GetActualDateTimeSystem;
  published
    property Antialiasing: Boolean read FAntialiasing write SetAntialiasing default True;
    property CellAutoHeight: Boolean read FCellAutoHeight write FCellAutoHeight default True;
    property DateTimeSystem: TdxSpreadSheetDateTimeSystem read FDateTimeSystem write SetDateTimeSystem default dts1900;
    property FrozenPaneSeparatorColor: TColor read FFrozenPaneSeparatorColor write SetFrozenPaneSeparatorColor default clDefault;
    property FrozenPaneSeparatorWidth: Integer read FFrozenPaneSeparatorWidth write SetFrozenPaneSeparatorWidth default 1;
    property GridLineColor: TColor read FGridLineColor write SetGridLineColor default clDefault;
    property GridLines: Boolean read FGridLines write SetGridLines default True;
    property Headers: Boolean read FHeaders write SetHeaders default True;
    property HorizontalScrollBar: Boolean read FHorizontalScrollBar write SetHorizontalScrollBar default True;
    property R1C1Reference: Boolean read FR1C1Reference write SetR1C1Reference default False;
    property ShowFormulas: Boolean read FShowFormulas write SetShowFormulas default False;
    property VerticalScrollBar: Boolean read FVerticalScrollBar write SetVerticalScrollBar default True;
    property ZeroValues: Boolean read FZeroValues write SetZeroValues default True;
  end;

  { TdxSpreadSheetHistoryCommand }

  TdxSpreadSheetHistoryCustomCommand = class
  private
    FOwner: TdxSpreadSheetHistoryAction;
    function GetView: TdxSpreadSheetCustomView;
  protected
    procedure Initialize; virtual;
  public
    class function ActionClass: TdxSpreadSheetHistoryActionClass; virtual;
    function CompatibleWith(ACommand: TdxSpreadSheetHistoryCustomCommand): Boolean; virtual;
    procedure Redo; virtual;
    procedure Undo; virtual;

    property Owner: TdxSpreadSheetHistoryAction read FOwner;
    property View: TdxSpreadSheetCustomView read GetView;
  end;

  { TdxSpreadSheetHistoryCellCommand }

  TdxSpreadSheetHistoryCellCommand = class(TdxSpreadSheetHistoryCustomCommand)
  private
    FColumn: Integer;
    FRow: Integer;
    FSheet: TdxSpreadSheetTableView;
  public
    constructor Create(ASheet: TdxSpreadSheetTableView; ARow, AColumn: Integer); virtual;
    class function ActionClass: TdxSpreadSheetHistoryActionClass; override;
    function CompatibleWith(ACommand: TdxSpreadSheetHistoryCustomCommand): Boolean; override;
  end;

  { TdxSpreadSheetHistoryCreateCellCommand }

  TdxSpreadSheetHistoryCreateCellCommand = class(TdxSpreadSheetHistoryCellCommand)
  private
    FHasCell: Boolean;
    FHasColumn: Boolean;
    FHasRow: Boolean;
  protected
    procedure Initialize; override;
  public
    procedure Redo; override;
    procedure Undo; override;
  end;

  { TdxSpreadSheetHistoryChangeCellCommand }

  TdxSpreadSheetHistoryChangeCellCommand = class(TdxSpreadSheetHistoryCellCommand)
  protected
    FData: TMemoryStream;
    procedure Initialize; override;
    procedure Store; virtual;
    procedure Restore; virtual;
  public
    constructor CreateEx(ACell: TdxSpreadSheetCell);
    destructor Destroy; override;
    procedure Redo; override;
    procedure Undo; override;
  end;

  { TdxSpreadSheetHistoryChangeCellStyleCommand }

  TdxSpreadSheetHistoryChangeCellStyleCommand = class(TdxSpreadSheetHistoryChangeCellCommand)
  protected
    procedure Store; override;
    procedure Restore; override;
  end;

  { TdxSpreadSheetHistoryMergeCellsCommand }

  TdxSpreadSheetHistoryMergeCellsCommand = class(TdxSpreadSheetHistoryCustomCommand)
  strict private
    function GetMergedCells: TdxSpreadSheetMergedCellList;
  protected
    FArea: TRect;
    FCell: TdxSpreadSheetMergedCell;
    FPrevArea: TRect;
    function FindCell: TdxSpreadSheetMergedCell;
    procedure UndoRedo; virtual;
  public
    constructor Create(ACell: TdxSpreadSheetMergedCell; const AArea: TRect);
    class function ActionClass: TdxSpreadSheetHistoryActionClass; override;
    procedure Redo; override;
    procedure Undo; override;

    property MergedCells: TdxSpreadSheetMergedCellList read GetMergedCells;
  end;

  { TdxSpreadSheetHistoryChangeItemCommand }

  TdxSpreadSheetHistoryChangeItemCommand = class(TdxSpreadSheetHistoryCustomCommand)
  strict private
    FData: TMemoryStream;
    FItemIndex: Integer;
    FItems: TdxSpreadSheetTableItems;
    function GetItem: TdxSpreadSheetTableItem;
  protected
    procedure Restore; virtual;
    procedure Store; virtual;

    property Data: TMemoryStream read FData write FData;
  public
    constructor Create(AItem: TdxSpreadSheetTableItem);
    destructor Destroy; override;
    procedure Redo; override;
    procedure Undo; override;

    property Item: TdxSpreadSheetTableItem read GetItem;
  end;

  { TdxSpreadSheetHistoryDeleteItemCommand }

  TdxSpreadSheetHistoryDeleteItemCommand = class(TdxSpreadSheetHistoryChangeItemCommand)
  public
    procedure Restore; override;
    procedure Store; override;
  public
    procedure Redo; override;
    procedure Undo; override;
  end;

  { TdxSpreadSheetChangeDefaultSizeCommand }

  TdxSpreadSheetChangeDefaultSizeCommand = class(TdxSpreadSheetHistoryCustomCommand)
  strict private
    FSize: Integer;
    FItems: TdxSpreadSheetTableItems;
  public
    constructor Create(AItems: TdxSpreadSheetTableItems);
    procedure Redo; override;
    procedure Undo; override;
  end;

  { TdxSpreadSheetHistoryDeleteContainerCommand }

  TdxSpreadSheetHistoryDeleteContainerCommand = class(TdxSpreadSheetHistoryCustomCommand)
  strict private
    FContainer: TdxSpreadSheetContainer;
    FContainerClass: TdxSpreadSheetContainerClass;
    FData: TMemoryStream;
  protected
    procedure Restore; virtual;
    procedure Store; virtual;
  public
    constructor Create(AContainer: TdxSpreadSheetContainer);
    destructor Destroy; override;
    procedure Redo; override;
    procedure Undo; override;

    property Container: TdxSpreadSheetContainer read FContainer;
  end;

  { TdxSpreadSheetHistoryFormulaChangedCommand }

  TdxSpreadSheetHistoryFormulaChangedCommand = class(TdxSpreadSheetHistoryCustomCommand)
  private
    FColumnIndex: Integer;
    FName: TdxSpreadSheetDefinedName;
    FRowIndex: Integer;
    FSheet: TdxSpreadSheetTableView;
    FSourceText: TdxUnicodeString;
  public
    constructor Create(AFormula: TdxSpreadSheetFormula; const ASourceText: TdxUnicodeString);
    procedure Redo; override;
    procedure Undo; override;
  end;

  { TdxSpreadSheetHistoryChangeContainerCommand }

  TdxSpreadSheetHistoryChangeContainerCommand = class(TdxSpreadSheetHistoryCustomCommand)
  strict private
    FData: TMemoryStream;
    FIndex: Integer;
    function GetContainer: TdxSpreadSheetContainer;
  protected
    procedure Restore; virtual;
    procedure Store(AContainer: TdxSpreadSheetContainer); virtual;
  public
    constructor Create(AContainer: TdxSpreadSheetContainer);
    destructor Destroy; override;
    procedure Redo; override;
    procedure Undo; override;

    property Container: TdxSpreadSheetContainer read GetContainer;
  end;

  { TdxSpreadSheetHistoryCreateContainerCommand }

  TdxSpreadSheetHistoryCreateContainerCommand = class(TdxSpreadSheetHistoryDeleteContainerCommand)
  public
    procedure Redo; override;
    procedure Undo; override;
  end;

 { TdxSpreadSheetHistoryAction }

  TdxSpreadSheetHistoryAction = class
  strict private
    FCommands: TcxObjectList;
    FHistory: TdxSpreadSheetHistory;
    FView: TdxSpreadSheetCustomView;
    FData: TStream;

    function GetCount: Integer;
    function GetCommand(AIndex: Integer): TdxSpreadSheetHistoryCustomCommand;
    function GetSpreadSheet: TdxCustomSpreadSheet;
  protected
    function AcceptCommand(ACommand: TdxSpreadSheetHistoryCustomCommand): Boolean; virtual;
    procedure AddCommand(ACommand: TdxSpreadSheetHistoryCustomCommand);
    procedure DoRedo; virtual;
    procedure DoUndo; virtual;
    class function GetDescription: string; virtual;
    procedure RestoreSelection(AData: TStream);
    function StoreSelection: TStream;

    property Commands[Index: Integer]: TdxSpreadSheetHistoryCustomCommand read GetCommand;
    property Count: Integer read GetCount;
    property History: TdxSpreadSheetHistory read FHistory;
    property SpreadSheet: TdxCustomSpreadSheet read GetSpreadSheet;
    property View: TdxSpreadSheetCustomView read FView;
  public
    constructor Create(AOwner: TdxSpreadSheetHistory); virtual;
    destructor Destroy; override;
    procedure Redo;
    procedure Undo;

    property Description: string read GetDescription;
  end;

  { TdxSpreadSheetHistoryChangeCellAction }

  TdxSpreadSheetHistoryEditCellAction = class(TdxSpreadSheetHistoryAction)
  protected
    class function GetDescription: string; override;
  end;

  { TdxSpreadSheetHistoryMergeCellsAction }

  TdxSpreadSheetHistoryMergeCellsAction = class(TdxSpreadSheetHistoryAction)
  protected
    class function GetDescription: string; override;
  end;

  { TdxSpreadSheetHistoryMergeCellsAction }

  TdxSpreadSheetHistoryClearCellsAction = class(TdxSpreadSheetHistoryAction)
  protected
    class function GetDescription: string; override;
  end;

  { TdxSpreadSheetHistoryDeleteCellsAction }

  TdxSpreadSheetHistoryDeleteCellsAction = class(TdxSpreadSheetHistoryAction)
  protected
    class function GetDescription: string; override;
  end;

  { TdxSpreadSheetHistoryInsertCellsAction }

  TdxSpreadSheetHistoryInsertCellsAction = class(TdxSpreadSheetHistoryAction)
  protected
    function AcceptCommand(ACommand: TdxSpreadSheetHistoryCustomCommand): Boolean; override;
    class function GetDescription: string; override;
  end;

  { TdxSpreadSheetHistoryFormatCellAction }

  TdxSpreadSheetHistoryFormatCellAction = class(TdxSpreadSheetHistoryAction)
  protected
    class function GetDescription: string; override;
  end;

  { TdxSpreadSheetHistorySortingAction }

  TdxSpreadSheetHistorySortingAction = class(TdxSpreadSheetHistoryAction)
  protected
    class function GetDescription: string; override;
  end;

  { TdxSpreadSheetHistoryCutToClipboardAction }

  TdxSpreadSheetHistoryCutToClipboardAction = class(TdxSpreadSheetHistoryAction)
  protected
    class function GetDescription: string; override;
  end;

  { TdxSpreadSheetHistoryPasteFromClipboardAction }

  TdxSpreadSheetHistoryPasteFromClipboardAction = class(TdxSpreadSheetHistoryAction)
  protected
    class function GetDescription: string; override;
  end;

  { TdxSpreadSheetHistoryChangeRowColumnItem }

  TdxSpreadSheetHistoryChangeRowColumnItemAction = class(TdxSpreadSheetHistoryAction)
  protected
    class function GetDescription: string; override;
  end;

  { TdxSpreadSheetHistoryChangeContainerAction }

  TdxSpreadSheetHistoryChangeContainerAction = class(TdxSpreadSheetHistoryAction)
  protected
    class function GetDescription: string; override;
  end;

  { TdxSpreadSheetHistory }

  TdxSpreadSheetHistory = class(TdxSpreadSheetPersistentObject)
  strict private
    FActionLockCount: Integer;
    FCurrentAction: TdxSpreadSheetHistoryAction;
    FInProcessRefCount: Integer;
    FLockCount: Integer;
    FRedoActions: TcxObjectList;
    FUndoActions: TcxObjectList;
    function GetInProcess: Boolean;
    function GetIsLocked: Boolean;
    function GetRedoAction(AIndex: Integer): TdxSpreadSheetHistoryAction;
    function GetRedoActionCount: Integer;
    function GetUndoAction(AIndex: Integer): TdxSpreadSheetHistoryAction;
    function GetUndoActionCount: Integer;
    procedure SetInProcess(AValue: Boolean);
  protected
    property CurrentAction: TdxSpreadSheetHistoryAction read FCurrentAction write FCurrentAction;

    property InProcess: Boolean read GetInProcess write SetInProcess;
    property IsLocked: Boolean read GetIsLocked;
    property LockCount: Integer read FLockCount write FLockCount;
    property RedoActionList: TcxObjectList read FRedoActions;
    property UndoActionList: TcxObjectList read FUndoActions;
  public
    constructor Create(ASpreadSheet: TdxCustomSpreadSheet); override;
    destructor Destroy; override;
    procedure AddCommand(ACommand: TdxSpreadSheetHistoryCustomCommand); inline;
    procedure BeginAction(AActionClass: TdxSpreadSheetHistoryActionClass);
    function CanAddCommand: Boolean;
    procedure EndAction;
    //
    procedure Clear;
    procedure Lock;
    procedure Unlock;
    procedure Redo(const ARedoCount: Integer = 1);
    procedure Undo(const AUndoCount: Integer = 1);

    property RedoActionCount: Integer read GetRedoActionCount;
    property RedoActions[Index: Integer]: TdxSpreadSheetHistoryAction read GetRedoAction;
    property UndoActionCount: Integer read GetUndoActionCount;
    property UndoActions[Index: Integer]: TdxSpreadSheetHistoryAction read GetUndoAction;
  end;

  { TdxSpreadSheetLockedStatePaintHelper }

  TdxSpreadSheetLockedStatePaintHelper = class(TcxLockedStatePaintHelper)
  strict private
    function GetSpreadSheet: TdxCustomSpreadSheet;
  protected
    function CanCreateLockedImage: Boolean; override;
    function DoPrepareImage: Boolean; override;
    function GetOptions: TcxLockedStateImageOptions; override;
    function GetControl: TcxControl; override;

    property SpreadSheet: TdxCustomSpreadSheet read GetSpreadSheet;
  end;

  { TdxSpreadSheetLockedStateImageOptions }

  TdxSpreadSheetLockedStateImageOptions = class(TcxLockedStateImageOptions)
  strict private
    FSpreadSheet: TdxCustomSpreadSheet;
  protected
    function GetFont: TFont; override;

    property SpreadSheet: TdxCustomSpreadSheet read FSpreadSheet;
  public
    constructor Create(AOwner: TComponent); override;
  published
    property AssignedValues;
    property Color;
    property Effect;
    property Font;
    property ShowText;
    property Text;
  end;

  { TdxCustomSpreadSheet }

  TdxSpreadSheetState = (sssReading, sssWriting);
  TdxSpreadSheetStates = set of TdxSpreadSheetState;

  TdxSpreadSheetViewCompareValuesEvent = procedure (Sender: TdxSpreadSheetCustomView;
    const AData1, AData2: TdxSpreadSheetCellData; var Compare: Integer) of object;

  TdxSpreadSheetPrepareLockedStateImageEvent = procedure(Sender: TdxCustomSpreadSheet; AImage: TcxBitmap32; var ADone: Boolean) of object;
  TdxSpreadSheetViewInitInplaceEditEvent = procedure (Sender: TdxSpreadSheetCustomView; AEdit: TcxCustomEdit) of object;
  TdxSpreadSheetViewInitInplaceEditValueEvent = procedure (Sender: TdxSpreadSheetCustomView; AEdit: TcxCustomEdit; var AValue: TcxEditValue) of object;
  TdxSpreadSheetViewInplaceEditingEvent = procedure (Sender: TdxSpreadSheetCustomView; var AProperties: TcxCustomEditProperties; var AAllow: Boolean) of object;
  TdxSpreadSheetViewNotifyEvent = procedure (Sender: TdxSpreadSheetCustomView) of object;

  TdxSpreadSheetCustomDrawTableViewCellEvent = procedure(Sender: TdxSpreadSheetTableView; ACanvas: TcxCanvas; AViewInfo: TdxSpreadSheetTableViewCellViewInfo; var AHandled: Boolean) of object;
  TdxSpreadSheetCustomDrawTableViewCommonCellEvent = procedure(Sender: TdxSpreadSheetTableView; ACanvas: TcxCanvas; AViewInfo: TdxSpreadSheetTableViewCustomCellViewInfo; var AHandled: Boolean) of object;
  TdxSpreadSheetCustomDrawTableViewHeaderCellEvent = procedure(Sender: TdxSpreadSheetTableView; ACanvas: TcxCanvas; AViewInfo: TdxSpreadSheetTableViewHeaderCellViewInfo; var AHandled: Boolean) of object;

  TdxSpreadSheetTableViewActiveCellChangingEvent = procedure (Sender: TdxSpreadSheetTableView;
    const ANewActiveCell: TPoint; var ACanSelect: Boolean) of object;

  TdxCustomSpreadSheet = class(TcxControl, IdxSkinSupport, IcxLockedStatePaint, IcxLockedStateFontChanged)
  strict private
    FActiveController: TdxSpreadSheetCustomController;
    FActiveSheetIndex: Integer;
    FCellStyles: TdxSpreadSheetCellStyles;
    FChanges: TdxSpreadSheetChanges;
    FDefaultCellStyle: TdxSpreadSheetDefaultCellStyle;
    FDefinedNames: TdxSpreadSheetDefinedNames;
    FExternalLinks: TdxSpreadSheetExternalLinks;
    FFormatSettings: TdxSpreadSheetFormatSettings;
    FFormulaController:  TdxSpreadSheetFormulaController;
    FHistory: TdxSpreadSheetHistory;
    FLockCount: Integer;
    FLockedStatePaintHelper: TdxSpreadSheetLockedStatePaintHelper;
    FListeners: TInterfaceList;
    FOptionsBehavior: TdxSpreadSheetOptionsBehavior;
    FOptionsLockedStateImage: TdxSpreadSheetLockedStateImageOptions;
    FOptionsView: TdxSpreadSheetOptionsView;
    FPageControl: TdxSpreadSheetPageControl;
    FProcessingChanges: Boolean;
    FSharedImages: TdxSpreadSheetSharedImages;
    FState: TdxSpreadSheetStates;
    FStringTable: TdxSpreadSheetSharedStringTable;
    FStyles: TdxSpreadSheetStyles;
    FViewInfo: TdxSpreadSheetViewInfo;

    FOnActiveCellChanging: TdxSpreadSheetTableViewActiveCellChangingEvent;
    FOnActiveSheetChanged: TNotifyEvent;
    FOnCompare: TdxSpreadSheetViewCompareValuesEvent;
    FOnCustomDrawTableViewCell: TdxSpreadSheetCustomDrawTableViewCellEvent;
    FOnCustomDrawTableViewCommonCell: TdxSpreadSheetCustomDrawTableViewCommonCellEvent;
    FOnCustomDrawTableViewHeaderCell: TdxSpreadSheetCustomDrawTableViewHeaderCellEvent;
    FOnDataChanged: TNotifyEvent;
    FOnEditChanged: TdxSpreadSheetViewNotifyEvent;
    FOnEdited: TdxSpreadSheetViewNotifyEvent;
    FOnEditing: TdxSpreadSheetViewInplaceEditingEvent;
    FOnEditValueChanged: TdxSpreadSheetViewNotifyEvent;
    FOnHistoryChanged: TNotifyEvent;
    FOnInitEdit: TdxSpreadSheetViewInitInplaceEditEvent;
    FOnInitEditValue: TdxSpreadSheetViewInitInplaceEditValueEvent;
    FOnLayoutChanged: TNotifyEvent;
    FOnPrepareLockedStateImage: TdxSpreadSheetPrepareLockedStateImageEvent;
    FOnProgress: TdxSpreadSheetProgressEvent;
    FOnSelectionChanged: TNotifyEvent;

    function GetActiveSheet: TdxSpreadSheetCustomView;
    function GetActiveSheetAsTable: TdxSpreadSheetTableView;
    function GetIsLocked: Boolean;
    function GetSheet(AIndex: Integer): TdxSpreadSheetCustomView;
    function GetSheetCount: Integer;
    function GetVisibleSheet(Index: Integer): TdxSpreadSheetCustomView;
    function GetVisibleSheetCount: Integer;
    procedure SetActiveSheet(AValue: TdxSpreadSheetCustomView);
    procedure SetActiveSheetIndex(AValue: Integer);
    procedure SetOptionsBehavior(AValue: TdxSpreadSheetOptionsBehavior);
    procedure SetOptionsLockedStateImage(AValue: TdxSpreadSheetLockedStateImageOptions);
    procedure SetOptionsView(AValue: TdxSpreadSheetOptionsView);
    procedure SetPageControl(AValue: TdxSpreadSheetPageControl);
    procedure SetStyle(AValue: TdxSpreadSheetStyles);
    //
    procedure ReadBinaryData(AStream: TStream);
    procedure WriteBinaryData(AStream: TStream);

    procedure WMCut(var Message: TMessage); message WM_CUT;
    procedure WMCopy(var Message: TMessage); message WM_COPY;
    procedure WMPaste(var Message: TMessage); message WM_PASTE;
  protected
    FSheets: TcxObjectList;
    FVisibleSheets: TList<TdxSpreadSheetCustomView>;

    procedure AddChanges(AChanges: TdxSpreadSheetChanges); virtual;
    procedure AfterLoad; virtual;
    procedure BoundsChanged; override;
    procedure CheckChanges; virtual;
    function ControllerFromPoint(const P: TPoint; var AController: TdxSpreadSheetCustomController): Boolean; overload; virtual;
    function ControllerFromPoint(X, Y: Integer; var AController: TdxSpreadSheetCustomController): Boolean; overload;
    function CreateCellStyles: TdxSpreadSheetCellStyles; virtual;
    function CreateDefaultCellStyle: TdxSpreadSheetDefaultCellStyle; virtual;
    function CreateDefinedNames: TdxSpreadSheetDefinedNames; virtual;
    function CreateExternalLinks: TdxSpreadSheetExternalLinks; virtual;
    function CreateFormatSettings: TdxSpreadSheetFormatSettings; virtual;
    function CreateFormulaController:  TdxSpreadSheetFormulaController; virtual;
    function CreateHistory:  TdxSpreadSheetHistory; virtual;
    function CreateLockedStatePaintHelper: TdxSpreadSheetLockedStatePaintHelper; virtual;
    function CreateOptionsBehavior: TdxSpreadSheetOptionsBehavior; virtual;
    function CreateOptionsLockedStateImage: TdxSpreadSheetLockedStateImageOptions; virtual;
    function CreateOptionsView: TdxSpreadSheetOptionsView; virtual;
    function CreatePageControl: TdxSpreadSheetPageControl; virtual;
    function CreateSharedImages: TdxSpreadSheetSharedImages; virtual;
    function CreateSharedStringTable: TdxSpreadSheetSharedStringTable; virtual;
    function CreateStyles: TdxSpreadSheetStyles; virtual;
    function CreateViewInfo: TdxSpreadSheetViewInfo; virtual;
    procedure CreateSubClasses; virtual;
    procedure DefineProperties(Filer: TFiler); override;
    procedure DestroySubClasses; virtual;
    procedure DblClick; override;

    function DoActiveCellChanging(AView: TdxSpreadSheetTableView; const ANewActiveCell: TPoint): Boolean; virtual;
    procedure DoActiveSheetChanged; virtual;
    procedure DoCompare(AView: TdxSpreadSheetCustomView; const AData1, AData2: TdxSpreadSheetCellData; var Compare: Integer); virtual;
    procedure DoContextPopup(MousePos: TPoint; var Handled: Boolean); override;
    procedure DoCustomDrawTableViewCell(Sender: TdxSpreadSheetTableView; ACanvas: TcxCanvas; AViewInfo: TdxSpreadSheetTableViewCellViewInfo; var AHandled: Boolean); virtual;
    procedure DoCustomDrawTableViewCommonCell(Sender: TdxSpreadSheetTableView; ACanvas: TcxCanvas; AViewInfo: TdxSpreadSheetTableViewCustomCellViewInfo; var AHandled: Boolean); virtual;
    procedure DoCustomDrawTableViewHeaderCell(Sender: TdxSpreadSheetTableView; ACanvas: TcxCanvas; AViewInfo: TdxSpreadSheetTableViewHeaderCellViewInfo; var AHandled: Boolean); virtual;
    procedure DoDataChanged; virtual;
    procedure DoEditChanged(AView: TdxSpreadSheetCustomView); virtual;
    procedure DoEdited(AView: TdxSpreadSheetCustomView); virtual;
    procedure DoEditing(AView: TdxSpreadSheetCustomView; var AProperties: TcxCustomEditProperties; var ACanEdit: Boolean); virtual;
    procedure DoEditValueChanged(AView: TdxSpreadSheetCustomView); virtual;
    procedure DoHistoryChanged;
    procedure DoInitEdit(AView: TdxSpreadSheetCustomView; AEdit: TcxCustomEdit); virtual;
    procedure DoInitEditValue(AView: TdxSpreadSheetCustomView; AEdit: TcxCustomEdit; var AValue: Variant); virtual;
    procedure DoLayoutChanged; virtual;
    procedure DoSelectionChanged(AView: TdxSpreadSheetCustomView); virtual;

    procedure DoAddSheet(ASheet: TdxSpreadSheetCustomView);
    procedure DoChangeSheetVisibility(ASheet: TdxSpreadSheetCustomView);
    procedure DoRemoveSheet(ASheet: TdxSpreadSheetCustomView);

    function CanFocusOnClick: Boolean; override;
    function DoMouseWheel(Shift: TShiftState; WheelDelta: Integer; MousePos: TPoint): Boolean; override;
    procedure DoPaint; override;
    function GetClientBounds: TRect; override;
    function GetCurrentCursor(X, Y: Integer): TCursor; override;
    function GetDragAndDropObjectClass: TcxDragAndDropObjectClass; override;
    function GetHScrollBarBounds: TRect; override;
    function GetSizeGripBounds: TRect; override;
    procedure InitScrollBarsParameters; override;
    function IsMouseWheelHandleNeeded(Shift: TShiftState; WheelDelta: Integer; MousePos: TPoint): Boolean; override;
    function IsSizeGripVisible: Boolean; override;
    procedure Loaded; override;
    procedure LookAndFeelChanged(Sender: TcxLookAndFeel; AChangedValues: TcxLookAndFeelValues); override;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure KeyUp(var Key: Word; Shift: TShiftState); override;
    procedure KeyPress(var Key: Char); override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure NotifyListeners;
    procedure Pack;
    procedure Scroll(AScrollBarKind: TScrollBarKind; AScrollCode: TScrollCode; var AScrollPos: Integer); override;
    procedure SetPaintRegion; override;
    procedure SetSheetIndex(AView: TdxSpreadSheetCustomView; AIndex: Integer); virtual;
    function StartDragAndDrop(const P: TPoint): Boolean; override;
    procedure StyleChanged(Sender: TObject);
    procedure UpdateHitTests(const P: TPoint); overload;
    procedure UpdateHitTests(X, Y: Integer); overload; inline;
    procedure UpdateVisibleSheetList;
    function ValidateSheetCaption(ACaption: TdxUnicodeString): TdxUnicodeString;

    function DoPrepareLockedStateImage: Boolean; virtual;
    // IcxLockedStatePaint
    function IcxLockedStatePaint.GetImage = GetLockedStateImage;
    function IcxLockedStatePaint.GetTopmostControl = GetLockedStateTopmostControl;
    function GetLockedStateImage: TcxBitmap32;
    function GetLockedStateTopmostControl: TcxControl;
    // IcxLockedStateFontChanged
    procedure IcxLockedStateFontChanged.FontChanged = UpdateLockedStateFont;
    procedure UpdateLockedStateFont(AFont: TFont);

    procedure InternalLoadFromStream(AStream: TStream; AFormat: TdxSpreadSheetCustomFormatClass;
      AProgressEvent: TdxSpreadSheetProgressEvent); virtual;
    procedure InternalSaveToStream(AStream: TStream; AFormat: TdxSpreadSheetCustomFormatClass;
      AProgressEvent: TdxSpreadSheetProgressEvent); virtual;
    //
    property ActiveController: TdxSpreadSheetCustomController read FActiveController write FActiveController;
    property Changes: TdxSpreadSheetChanges read FChanges write FChanges;
    property FormatSettings: TdxSpreadSheetFormatSettings read FFormatSettings write FFormatSettings;
    property IsLocked: Boolean read GetIsLocked;
    property Listeners: TInterfaceList read FListeners;
    property LockCount: Integer read FLockCount write FLockCount;
    property LockedStatePaintHelper: TdxSpreadSheetLockedStatePaintHelper read FLockedStatePaintHelper;
    property ProcessingChanges: Boolean read FProcessingChanges write FProcessingChanges;
    property SharedImages: TdxSpreadSheetSharedImages read FSharedImages;
    property State: TdxSpreadSheetStates read FState;
    property StringTable: TdxSpreadSheetSharedStringTable read FStringTable;
    property ViewInfo: TdxSpreadSheetViewInfo read FViewInfo;
    //
    property OnActiveCellChanging: TdxSpreadSheetTableViewActiveCellChangingEvent read FOnActiveCellChanging write FOnActiveCellChanging;
    property OnActiveSheetChanged: TNotifyEvent read FOnActiveSheetChanged write FOnActiveSheetChanged;
    property OnCompare: TdxSpreadSheetViewCompareValuesEvent read FOnCompare write FOnCompare;
    property OnCustomDrawTableViewCell: TdxSpreadSheetCustomDrawTableViewCellEvent read FOnCustomDrawTableViewCell write FOnCustomDrawTableViewCell;
    property OnCustomDrawTableViewCommonCell: TdxSpreadSheetCustomDrawTableViewCommonCellEvent read FOnCustomDrawTableViewCommonCell write FOnCustomDrawTableViewCommonCell;
    property OnCustomDrawTableViewHeaderCell: TdxSpreadSheetCustomDrawTableViewHeaderCellEvent read FOnCustomDrawTableViewHeaderCell write FOnCustomDrawTableViewHeaderCell;
    property OnDataChanged: TNotifyEvent read FOnDataChanged write FOnDataChanged;
    property OnEditChanged: TdxSpreadSheetViewNotifyEvent read FOnEditChanged write FOnEditChanged;
    property OnEdited: TdxSpreadSheetViewNotifyEvent read FOnEdited write FOnEdited;
    property OnEditing: TdxSpreadSheetViewInplaceEditingEvent read FOnEditing write FOnEditing;
    property OnEditValueChanged: TdxSpreadSheetViewNotifyEvent read FOnEditValueChanged write FOnEditValueChanged;
    property OnHistoryChanged: TNotifyEvent read FOnHistoryChanged write FOnHistoryChanged;
    property OnInitEdit: TdxSpreadSheetViewInitInplaceEditEvent read FOnInitEdit write FOnInitEdit;
    property OnInitEditValue: TdxSpreadSheetViewInitInplaceEditValueEvent read FOnInitEditValue write FOnInitEditValue;
    property OnLayoutChanged: TNotifyEvent read FOnLayoutChanged write FOnLayoutChanged;
    property OnPrepareLockedStateImage: TdxSpreadSheetPrepareLockedStateImageEvent read FOnPrepareLockedStateImage write FOnPrepareLockedStateImage;
    property OnProgress: TdxSpreadSheetProgressEvent read FOnProgress write FOnProgress;
    property OnSelectionChanged: TNotifyEvent read FOnSelectionChanged write FOnSelectionChanged;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    function AddSheet(const ACaption: TdxUnicodeString = ''; AViewClass: TdxSpreadSheetCustomViewClass = nil): TdxSpreadSheetCustomView;
    procedure ClearAll;
    function GetSheetByName(const ACaption: TdxUnicodeString): TdxSpreadSheetCustomView;
    procedure LayoutChanged;

    procedure BeginUpdate(AMode: TcxLockedStateImageShowingMode = lsimNever);
    procedure EndUpdate;

    procedure LoadFromFile(const AFileName: string); virtual;
    procedure LoadFromStream(AStream: TStream); overload;
    procedure LoadFromStream(AStream: TStream; AFormat: TdxSpreadSheetCustomFormatClass); overload; virtual;
    procedure SaveToFile(const AFileName: string); virtual;
    procedure SaveToStream(AStream: TStream; AFormat: TdxSpreadSheetCustomFormatClass = nil); virtual;
    //
    procedure AddListener(AListener: IdxSpreadSheetListener);
    procedure RemoveListener(AListener: IdxSpreadSheetListener);

    property ActiveSheet: TdxSpreadSheetCustomView read GetActiveSheet write SetActiveSheet;
    property ActiveSheetAsTable: TdxSpreadSheetTableView read GetActiveSheetAsTable;
    property ActiveSheetIndex: Integer read FActiveSheetIndex write SetActiveSheetIndex;
    property CellStyles: TdxSpreadSheetCellStyles read FCellStyles;
    property DefaultCellStyle: TdxSpreadSheetDefaultCellStyle read FDefaultCellStyle;
    property DefinedNames: TdxSpreadSheetDefinedNames read FDefinedNames;
    property ExternalLinks: TdxSpreadSheetExternalLinks read FExternalLinks;
    property FormulaController: TdxSpreadSheetFormulaController read FFormulaController;
    property History: TdxSpreadSheetHistory read FHistory;
    property LookAndFeel;
    property SheetCount: Integer read GetSheetCount;
    property Sheets[Index: Integer]: TdxSpreadSheetCustomView read GetSheet;
    property VisibleSheetCount: Integer read GetVisibleSheetCount;
    property VisibleSheets[Index: Integer]: TdxSpreadSheetCustomView read GetVisibleSheet;
    //
    property OptionsBehavior: TdxSpreadSheetOptionsBehavior read FOptionsBehavior write SetOptionsBehavior;
    property OptionsLockedStateImage: TdxSpreadSheetLockedStateImageOptions read FOptionsLockedStateImage write SetOptionsLockedStateImage;
    property OptionsView: TdxSpreadSheetOptionsView read FOptionsView write SetOptionsView;
    property PageControl: TdxSpreadSheetPageControl read FPageControl write SetPageControl;
    property Styles: TdxSpreadSheetStyles read FStyles write SetStyle;
  end;

 { TdxSpreadSheetCellReference }

  PdxSpreadSheetCellReference = ^TdxSpreadSheetCellReference;

  TdxSpreadSheetCellReference = record
    ColumnIndex: Integer;
    RowIndex: Integer;
    Sheet: TdxSpreadSheetTableView;
  end;

function dxSpreadSheetFormatsRepository: TdxSpreadSheetFormatsRepository;

implementation

{$R dxSpreadSheet.res}

uses
  RTLConsts, dxTypeHelpers,
  // Formats
  dxSpreadSheetFormatXLSX, dxSpreadSheetFormatXLS, dxSpreadSheetFormatCSV, dxSpreadSheetFormatODS,
  dxSpreadSheetFormatBinary,
  //
  dxSpreadSheetFormulas, dxSpreadSheetFunctions, dxSpreadSheetCoreHelpers, dxSpreadSheetPopupMenu,
  dxSpreadSheetCellsModificationDialog, dxGDIPlusAPI;

const
  TabKeyToArrowKey: array[Boolean] of Word = (VK_RIGHT, VK_LEFT);
  ReturnKeyToArrowKey: array[Boolean] of Word = (VK_DOWN, VK_UP);

type
  TcxCustomEditStyleAccess = class(TcxEditStyle);
  TcxCustomRichEditAccess = class(TcxCustomRichEdit);

var
  FSpreadSheetFormats: TdxSpreadSheetFormatsRepository = nil;

function dxSpreadSheetFormatsRepository: TdxSpreadSheetFormatsRepository;
begin
  if FSpreadSheetFormats = nil then
    FSpreadSheetFormats := TdxSpreadSheetFormatsRepository.Create;
  Result := FSpreadSheetFormats;
end;

{ TdxSpreadSheetPersistentObject }

constructor TdxSpreadSheetPersistentObject.Create(ASpreadSheet: TdxCustomSpreadSheet);
begin
  FSpreadSheet := ASpreadSheet;
end;

function TdxSpreadSheetPersistentObject.GetOwner: TPersistent;
begin
  Result := FSpreadSheet;
end;

{ TdxSpreadSheetViewPersistentObject }

constructor TdxSpreadSheetViewPersistentObject.Create(AView: TdxSpreadSheetCustomView);
begin
  FView := AView;
end;

function TdxSpreadSheetViewPersistentObject.GetSpreadSheet: TdxCustomSpreadSheet;
begin
  Result := FView.SpreadSheet;
end;

{ TdxSpreadSheetObjectListItem }

constructor TdxSpreadSheetObjectListItem.Create(AOwner: TdxSpreadSheetObjectList);
begin
  FOwner := AOwner;
  Owner.ItemList.Add(Self);
end;

destructor TdxSpreadSheetObjectListItem.Destroy;
begin
  Owner.Remove(Self);
  inherited Destroy;
end;

procedure TdxSpreadSheetObjectListItem.Changed;
begin
  Owner.Changed;
end;

function TdxSpreadSheetObjectListItem.GetIndex: Integer;
begin
  Result := Owner.ItemList.IndexOf(Self);
end;

{ TdxSpreadSheetObjectList }

constructor TdxSpreadSheetObjectList.Create(ASpreadSheet: TdxCustomSpreadSheet);
begin
  inherited Create(ASpreadSheet);
  FItems := TcxObjectList.Create;
end;

destructor TdxSpreadSheetObjectList.Destroy;
begin
  Clear;
  FreeAndNil(FItems);
  inherited Destroy;
end;

procedure TdxSpreadSheetObjectList.Clear;
var
  I: Integer;
begin
  for I := Count - 1 downto 0 do
    FItems[I].Free;
end;

procedure TdxSpreadSheetObjectList.Delete(AIndex: Integer);
begin
  Items[AIndex].Free;
end;

procedure TdxSpreadSheetObjectList.Changed;
begin
  SpreadSheet.AddChanges([sscData]);
end;

function TdxSpreadSheetObjectList.CreateItem: TdxSpreadSheetObjectListItem;
begin
  Result := nil;
end;

procedure TdxSpreadSheetObjectList.Remove(AItem: TdxSpreadSheetObjectListItem);
begin
  if FItems.Remove(AItem) >= 0 then
    Changed;
end;

function TdxSpreadSheetObjectList.GetCount: Integer;
begin
  Result := FItems.Count;
end;

function TdxSpreadSheetObjectList.GetItem(AIndex: Integer): TdxSpreadSheetObjectListItem;
begin
  if (AIndex < 0) or (AIndex >= FItems.Count) then
    TdxSpreadSheetInvalidObject.AssignTo(Result)
  else
    Result := TdxSpreadSheetObjectListItem(FItems.List[AIndex])
end;

{ TdxSpreadSheetCellDisplayValue }

function TdxSpreadSheetCellDisplayValue.CheckNeedUpdateValue(
  const AValue: Variant; const AFormatCode: TdxUnicodeString): Boolean;
begin
  Result := (AFormatCode <> FormatCode) or (VarType(AValue) <> VarType(Value)) or not VarEquals(AValue, Value);
  if Result then
  begin
    FormatCode := AFormatCode;
    Value := AValue;
  end;
end;

procedure TdxSpreadSheetCellDisplayValue.Reset;
begin
  FormattedValue.Reset;
  Value := Null;
  FormatCode := '';
end;

{ TdxSpreadSheetCell }

constructor TdxSpreadSheetCell.Create(AOwner: TdxDynamicItemList; AIndex: Integer);
begin
  inherited Create(AOwner, AIndex);
  FStyle := TdxSpreadSheetCellStyle.Create(Self);
  FShowFormula := bDefault;
  FDisplayValueIsDirty := True;
  FArrayFormulaMasterCell := nil;
end;

destructor TdxSpreadSheetCell.Destroy;
begin
  CheckAndDeleteObjectData;
  if History.CanAddCommand and (Style.Handle <> nil) then
    History.AddCommand(TdxSpreadSheetHistoryChangeCellStyleCommand.CreateEx(Self));
  FreeAndNil(FStyle);
  inherited Destroy;
end;

procedure TdxSpreadSheetCell.BeforeDestruction;
begin
  inherited BeforeDestruction;
  View.DoRemoveCell(Self);
end;

procedure TdxSpreadSheetCell.Clear;
begin
  CheckAndDeleteObjectData;
  FDataType := cdtBlank;
  Changed([sscData]);
end;

function TdxSpreadSheetCell.GetAbsoluteBounds: TRect;
begin
  Result := View.GetAbsoluteCellBounds(RowIndex, ColumnIndex);
end;

procedure TdxSpreadSheetCell.SetText(const AText: TdxUnicodeString; const AFormulaChecking: Boolean = False);

  procedure CheckFormula(const AText: TdxUnicodeString);
  var
    AParser: TdxSpreadSheetFormulaParser;
  begin
    if History.InProcess or (AsFormula = nil) or not SameText(View.FormatSettings.Operations[opEQ] + AsFormula.SourceText, AText) then
    begin
      AParser := TdxSpreadSheetFormulaParser.Create(View.SpreadSheet);
      try
        if AParser.ParseFormula(AText, Self) then
          CheckAreaReferenceTokens(AsFormula.FTokens)
        else
          AsString := AText;
      finally
        AParser.Free;
      end;
    end;
  end;

var
  AErrorCode: TdxSpreadSheetFormulaErrorCode;
  AValue: Variant;
begin
  View.BeginUpdate;
  try
    if dxSpreadSheetIsErrorString(AText, AErrorCode) and (AErrorCode <> ecNone) then
      AsError := AErrorCode
    else
      if Style.DataFormat.IsText then
        AsString := AText
      else    
        if dxTryStrToOrdinal(AText, AValue, SpreadSheet.FormatSettings) then
          AsVariant := AValue
        else
          if AFormulaChecking then
            CheckFormula(AText)
          else
            AsString := AText;
  finally
    View.EndUpdate;
  end;
end;

procedure TdxSpreadSheetCell.AssignData(ASource: TdxSpreadSheetCell);
begin
  case ASource.DataType of
    cdtString:
      AsSharedString := ASource.AsSharedString;
    cdtFormula:
      SetText(ASource.AsFormula.AsText, True);
  else
    AsVariant := ASource.AsVariant;
  end;
end;

procedure TdxSpreadSheetCell.Changed(AChanges: TdxSpreadSheetChanges);
begin
  FDisplayValueIsDirty := True;
  FShrinkToFitCacheContentWidth := 0;
  FShrinkToFitCacheFontSize := 0;
  if not Row.IsCustomSize then
    Row.FBestFitIsDirty := True;
  if not SpreadSheet.FormulaController.CalculationInProcess and (ArrayFormulaMasterCell = nil) then
    View.AddChanges(AChanges);
end;

procedure TdxSpreadSheetCell.CheckAndDeleteObjectData;
begin
  if History.CanAddCommand then
    History.AddCommand(TdxSpreadSheetHistoryChangeCellCommand.CreateEx(Self));
  if DataType = cdtString then
    TdxSpreadSheetSharedString(PObject(@FData)^).Release
  else
    if IsFormula then
      PObject(@FData)^.Free;
end;

procedure TdxSpreadSheetCell.CheckAreaReferenceTokens(var AParentToken: TdxSpreadSheetFormulaToken);
var
  AToken: TdxSpreadSheetFormulaToken;
begin
  if AParentToken = nil then
    Exit;
  if AParentToken is TdxSpreadSheetFormulaAreaReference then
    TdxSpreadSheetFormulaAreaReference(AParentToken).Check
  else
  begin
    AToken := AParentToken.FirstChild;
    while AToken <> nil do
    begin
      CheckAreaReferenceTokens(AToken);
      AToken := AToken.Next;
    end;
  end;
  AToken := AParentToken.Next;
  CheckAreaReferenceTokens(AToken);
end;

procedure TdxSpreadSheetCell.ProcessStyleChanges(APrevStyle, ANewStyle: TdxSpreadSheetCellStyleHandle);

  function CompareBorders(ABorder: TcxBorder): Boolean;
  begin
    Result :=
      (APrevStyle.Borders.BorderColor[ABorder] = ANewStyle.Borders.BorderColor[ABorder]) and
      (APrevStyle.Borders.BorderStyle[ABorder] = ANewStyle.Borders.BorderStyle[ABorder]);
  end;

  procedure CheckBorder(ABorder: TcxBorder);
  const
    OppositeBordersMap: array[TcxBorder] of TcxBorder = (bRight, bBottom, bLeft, bTop);
    HorzOffsetMap: array[TcxBorder] of Integer = (-1, 0, 1, 0);
    VertOffsetMap: array[TcxBorder] of Integer = (0, -1, 0, 1);
  var
    ACell: TdxSpreadSheetCell;
  begin
    if not CompareBorders(ABorder) then
    begin
      ACell := View.Cells[RowIndex + VertOffsetMap[ABorder], ColumnIndex + HorzOffsetMap[ABorder]];
      if ACell <> nil then
        ACell.Style.Borders[OppositeBordersMap[ABorder]].Assign(Style.Borders[ABorder]);
    end;
  end;

var
  ABorder: TcxBorder;
begin
  if not IsMerged then
  begin
    for ABorder := Low(ABorder) to High(ABorder) do
      CheckBorder(ABorder);
  end;
end;

procedure TdxSpreadSheetCell.StyleChanged;
begin
  Changed([sscLayout]);
end;

procedure TdxSpreadSheetCell.ClearReferenceToArrayFormula;
begin
  FArrayFormulaMasterCell := nil;
end;

procedure TdxSpreadSheetCell.InitializeReferenceToArrayFormula;
var
  AArea: TRect;
  ARowIndex, AColumnIndex: Integer;
begin
  ARowIndex := RowIndex;
  AColumnIndex := ColumnIndex;
  AArea := View.GetAreaReferencedByArrayFormula(ARowIndex, AColumnIndex);
  if not cxRectIsNull(AArea) and ((ARowIndex > AArea.Top) or (AColumnIndex > AArea.Left)) then
    FArrayFormulaMasterCell := View.Cells[AArea.Top, AArea.Left];
end;

function TdxSpreadSheetCell.GetAsBoolean: Boolean;
begin
  if DataType = cdtBoolean then
    Result := PBoolean(@FData)^
  else
    Result := GetAsFloat <> 0;
end;

function TdxSpreadSheetCell.GetAsCurrency: Currency;
begin
  if DataType = cdtCurrency then
    Result := PCurrency(@FData)^
  else
    Result := GetAsFloat;
end;

function TdxSpreadSheetCell.GetAsDateTime: TDateTime;
begin
  Result := GetAsFloat;
end;

function TdxSpreadSheetCell.GetAsError: TdxSpreadSheetFormulaErrorCode;
begin
  if DataType = cdtError then
    Result := TdxSpreadSheetFormulaErrorCode(FData[0])
  else
    Result := TdxSpreadSheetFormulaErrorCode(GetAsInteger);
end;

function TdxSpreadSheetCell.GetAsFloat: Double;
begin
  case DataType of
    cdtBoolean:
      Result := Integer(PBoolean(@FData)^);
    cdtCurrency:
      Result := PCurrency(@FData)^;
    cdtFloat:
      Result := PFloat(@FData)^;
    cdtDateTime:
      Result := PDateTime(@FData)^;
    cdtInteger:
      Result := PInteger(@FData)^;
  else
    Result := 0;
  end;
end;

function TdxSpreadSheetCell.GetAsFormula: TdxSpreadSheetFormula;
begin
  if IsFormula then
    Result := TdxSpreadSheetFormula(PObject(@FData)^)
  else
    Result := nil;
end;

function TdxSpreadSheetCell.GetAsInteger: Integer;
begin
  if DataType = cdtInteger then
    Result := PInteger(@FData)^
  else
    Result := Trunc(GetAsFloat);
end;

function TdxSpreadSheetCell.GetAsSharedString: TdxSpreadSheetSharedString;
begin
  if DataType = cdtString then
    Result := TdxSpreadSheetSharedString(PObject(@FData)^)
  else
    Result := nil;
end;

function TdxSpreadSheetCell.GetAsString: TdxUnicodeString;

  function FormatDateTime(const AValue: TDateTime): TdxUnicodeString;
  begin
    Result := DateTimeToStr(dxDateTimeToRealDateTime(AValue, SpreadSheet.OptionsView.ActualDateTimeSystem));
  end;

begin
  Result := '';
  case DataType of
    cdtString:
      Result := GetAsSharedString.Value;
    cdtBoolean:
      Result := dxBoolToString[PBoolean(@FData)^];
    cdtCurrency:
      Result := CurrToStr(PCurrency(@FData)^);
    cdtInteger:
      Result := IntToStr(PInteger(@FData)^);
    cdtDateTime:
      Result := FormatDateTime(PDateTime(@FData)^);

    cdtFloat:
      if Style.DataFormat.IsDateTime then
        Result := FormatDateTime(PFloat(@FData)^)
      else
        Result := FloatToStr(PFloat(@FData)^);

    cdtFormula:
      if ActualShowFormula then
        Result := AsFormula.AsText
      else
        Result := VarToStr(GetActualFormulaValue);
  end;
end;

function TdxSpreadSheetCell.GetAsVariant: Variant;
begin
  case DataType of
    cdtBoolean:
      Result := AsBoolean;
    cdtCurrency:
      Result := AsCurrency;
    cdtError:
      Result := AsError;
    cdtFloat:
      Result := AsFloat;
    cdtDateTime:
      Result := AsDateTime;
    cdtInteger:
      Result := AsInteger;
    cdtString:
      Result := AsString;
    cdtFormula:
      Result := GetActualFormulaValue;
  else
    Result := Null;
  end;
end;

function TdxSpreadSheetCell.GetActualDataType: TdxSpreadSheetCellDataType;
begin
  Result := FDataType;
  if Result = cdtFormula then
  begin
    if (AsFormula.ErrorCode <> ecNone) or (AsFormula.ResultValue <> nil) and (AsFormula.ResultValue.ErrorCode <> ecNone) then
      Result := cdtBoolean
    else
      Result := dxGetDataTypeByVariantValue(GetActualFormulaValue);
  end;
  if DisplayValue.IsText = bTrue then
    Result := cdtString;
end;

function TdxSpreadSheetCell.GetActualFormulaValue: Variant;
begin
  Result := AsFormula.Value;
  if View.Options.ActualZeroValues and dxSpreadSheetIsEmptyValue(Result) then
    Result := 0;
end;

function TdxSpreadSheetCell.GetActualShowFormula: Boolean;
begin
  Result := dxDefaultBooleanToBoolean(ShowFormula, View.Options.ActualShowFormulas);
end;

function TdxSpreadSheetCell.GetColumn: TdxSpreadSheetTableColumn;
begin
  Result := View.Columns[FIndex];
end;

function TdxSpreadSheetCell.GetColumnIndex: Integer;
begin
  Result := FIndex; 
end;

function TdxSpreadSheetCell.GetDisplayText: TdxUnicodeString;
begin
  Result := DisplayValue.Text;
end;

function TdxSpreadSheetCell.GetDisplayValue: TdxSpreadSheetNumberFormatResult;
begin
  if FDisplayValueIsDirty then
  begin
    FDisplayValueIsDirty := False;
    if ActualShowFormula and IsFormula then
    begin
      FDisplayValue.Reset;
      FDisplayValue.FormattedValue.Text := AsFormula.AsText;
      FDisplayValue.FormattedValue.IsText := bTrue;
    end
    else
      if FDisplayValue.CheckNeedUpdateValue(AsVariant, Style.DataFormat.FormatCode) then
        Style.DataFormat.Format(FDisplayValue.Value, DataType, SpreadSheet.FormatSettings, FDisplayValue.FormattedValue);
  end;
  Result := FDisplayValue.FormattedValue;
end;

function TdxSpreadSheetCell.GetHasValue: Boolean;
begin
  Result := FDataType in [cdtBoolean, cdtCurrency, cdtFloat, cdtDateTime, cdtInteger, cdtFormula];
  if FDataType = cdtString then
    Result := AsString <> '';
end;

function TdxSpreadSheetCell.GetHistory: TdxSpreadSheetHistory;
begin
  Result := SpreadSheet.History;
end;

function TdxSpreadSheetCell.GetIsEmpty: Boolean;
begin
  Result := (Self = nil) or (DataType = cdtBlank);
end;

function TdxSpreadSheetCell.GetIsFormula: Boolean;
begin
  Result := (Self <> nil) and (DataType = cdtFormula);
end;

function TdxSpreadSheetCell.GetIsMerged: Boolean;
begin
  Result := not dxSpreadSheetIsSingleCellArea(View.MergedCells.CheckCell(RowIndex, ColumnIndex));
end;

function TdxSpreadSheetCell.GetSpreadSheet: TdxCustomSpreadSheet;
begin
  Result := View.SpreadSheet;
end;

function TdxSpreadSheetCell.GetRow: TdxSpreadSheetTableRow;
begin
  Result := TdxSpreadSheetTableRowCells(FOwner).Row;
end;

function TdxSpreadSheetCell.GetRowIndex: Integer;
begin
  Result := Row.Index;
end;

function TdxSpreadSheetCell.GetView: TdxSpreadSheetTableView;
begin
  Result := Row.View;
end;

procedure TdxSpreadSheetCell.SetColumn(AValue: TdxSpreadSheetTableColumn);
var
  AItem: TdxDynamicListItem;
begin
  if (Column <> AValue) and (AValue <> nil) then
  begin
    AItem := Row.RowCells.FindItem(AValue.Index);
    if (AItem <> nil) and (AItem.Index = AValue.Index) then
      raise EdxSpreadSheetError.CreateFmt(sdxErrorCellAlreadyExists, [AValue.Index]);
    FIndex := AValue.Index;
    Changed([sscLayout, sscData]);
  end;
end;

procedure TdxSpreadSheetCell.SetColumnIndex(AValue: Integer);
begin
  dxSpreadSheetValidate(AValue, 0, dxSpreadSheetMaxColumnIndex);
  if ColumnIndex <> AValue then
    Column := View.Columns.CreateItem(AValue);
end;

procedure TdxSpreadSheetCell.SetIsEmpty(AValue: Boolean);
begin
  if (IsEmpty <> AValue) and AValue then
  begin
    CheckAndDeleteObjectData;
    FDataType := cdtBlank;
    Changed([sscData]);
  end;
end;

procedure TdxSpreadSheetCell.SetRow(AValue: TdxSpreadSheetTableRow);
var
  AItem: TdxDynamicListItem;
begin
  if (Row <> AValue) and (AValue <> nil) then
  begin
    AItem := AValue.RowCells.FindItem(Index);
    if (AItem <> nil) and (AItem.Index = Index) then
      raise EdxSpreadSheetError.CreateFmt(sdxErrorCellAlreadyExists, [Index]);

    Row.RowCells.DeleteItem(Self);
    FOwner := AValue.RowCells;
    AValue.RowCells.InsertItem(Self, AItem);
    Changed([sscLayout, sscData]);
  end;
end;

procedure TdxSpreadSheetCell.SetRowIndex(AValue: Integer);
begin
  dxSpreadSheetValidate(AValue, 0, dxSpreadSheetMaxRowIndex);
  if RowIndex <> AValue then
    Row := View.Rows.CreateItem(AValue);
end;

procedure TdxSpreadSheetCell.SetShowFormula(AValue: TdxDefaultBoolean);
begin
  if FShowFormula <> AValue then
  begin
    FShowFormula := AValue;
    Changed([sscData]);
  end;
end;

function TdxSpreadSheetCell.IsMultiline: Boolean;
begin
  Result := (Style.WordWrap or (Style.AlignVert in [ssavJustify, ssavDistributed])) and not IsNumericValue;
end;

function TdxSpreadSheetCell.IsNumericValue: Boolean;
begin
  Result := ActualDataType in [cdtCurrency, cdtFloat, cdtInteger];
end;

procedure TdxSpreadSheetCell.LoadFromStream(AReader: TcxReader;
  AStyleHandle: TdxSpreadSheetCellStyleHandle; AFormulasRef: TdxSpreadSheetFormulaAsTextInfoList);
var
  AFormula: TdxSpreadSheetFormula;
  AType: TdxSpreadSheetCellDataType;
begin
  Clear;
  Byte(AType) := AReader.ReadByte;
  case AType of
    cdtString:
      AsSharedString := SpreadSheet.StringTable.LoadItemFromStream(AReader);

    cdtFormula:
      if AReader.Version >= 4 then
      begin
        AFormulasRef.AddFromStream(Self, AReader);
        FillChar(FData, SizeOf(FData), 0);
      end
      else
      begin
        AFormula := TdxSpreadSheetFormula.Create(nil);
        AFormula.LoadFromStream(AReader);
        AsFormula := AFormula;
      end;

  else
    AReader.Stream.ReadBuffer(FData, SizeOf(FData));
  end;

  FDataType := AType;
  Style.Handle := AStyleHandle;
  DisplayValueIsDirty := True;
  Changed([sscData]);
end;

procedure TdxSpreadSheetCell.SaveToStream(AWriter: TcxWriter; AStyleIndex: Integer);
var
  AInfo: TdxSpreadSheetFormulaAsTextInfo;
begin
  AWriter.WriteInteger(AStyleIndex);
  AWriter.WriteByte(Byte(FDataType));
  case FDataType of
    cdtFormula:
      begin
        AInfo := TdxSpreadSheetFormulaAsTextInfo.Create(AsFormula);
        try
          AInfo.SaveToStream(AWriter);
        finally
          AInfo.Free;
        end;
      end;

    cdtString:
      AsSharedString.SaveToStream(AWriter);
  else
    AWriter.Stream.WriteBuffer(FData, SizeOf(FData));
  end;
end;

class function TdxSpreadSheetCell.GetContentOffsets(ABottomBorderStyle: TdxSpreadSheetCellBorderStyle): TRect;
begin
  Result := cxRect(3, 1, 2, dxSpreadSheetBorderStyleThickness[ABottomBorderStyle] div 2 + 1);
end;

function TdxSpreadSheetCell.MeasureSize(ACanvas: TcxCanvas; ACalcHeight: Boolean): Integer;
var
  R: TRect;
  AFlags: Integer;
  AHasText: Boolean;
  AIndents: TRect;
  ALineCount, AWidth, ALinesInRow: Integer;
  ATextParams: TcxTextParams;
  ATextRows: TcxTextRows;
begin
  Result := 0;
  AHasText := not IsEmpty and (DisplayValue.Text <> '');
  if not (AHasText or ACalcHeight) then
    Exit;

  R := Rect(ColumnIndex, RowIndex, ColumnIndex, RowIndex);
  if IsMerged then
    R := View.MergedCells.FindCell(R.Top, R.Left).Area;
  R := cxRectBounds(0, 0, View.Columns.GetDistance(R.Left, R.Right), View.Rows.GetDistance(R.Top, R.Bottom));

  AFlags := CXTO_CALCROWCOUNT or CXTO_CHARBREAK or CXTO_AUTOINDENTS or cxMakeFormat(taLeft, taTop);
  if IsMultiline then
    AFlags := AFlags or CXTO_WORDBREAK;

  AIndents := GetContentOffsets(Style.Borders[bBottom].Style);
  R := cxRectContent(R, AIndents);
  R.Right := Max(R.Left + 1, R.Right - Style.AlignHorzIndent);

  AWidth := 0;
  ACanvas.Font := Style.Font.Handle.GraphicObject;
  ATextParams := cxCalcTextParams(ACanvas.Handle, AFlags);
  ALinesInRow := Max(1, cxRectHeight(R) div ATextParams.FullRowHeight);
  if AHasText then
  begin
    cxMakeTextRows(ACanvas.Handle, PWideChar(DisplayValue.Text), Length(DisplayValue.Text), R, ATextParams, ATextRows, ALineCount);
    AWidth := cxGetLongestTextRowWidth(ATextRows, ALineCount) + cxTextOffset;
    cxResetTextRows(ATextRows);
  end
  else
    ALineCount := ALinesInRow;

  if ACalcHeight then
  begin
    Result := ATextParams.FullRowHeight;
    if AHasText then
      Result := ALineCount * Result;
    Result := Result + cxMarginsHeight(AIndents);
  end
  else
  begin
    if ALineCount > ALinesInRow then
      AWidth := cxRectWidth(cxGetTextRect(ACanvas.Handle, DisplayValue.Text, ALinesInRow)) + cxTextOffset * 2;
    Result := AWidth + Style.AlignHorzIndent + cxMarginsWidth(AIndents);
  end;
end;

procedure TdxSpreadSheetCell.SetAsBoolean(const AValue: Boolean);
begin
  if (DataType <> cdtBoolean) or (AsBoolean <> AValue) then
  begin
    CheckAndDeleteObjectData;
    FDataType := cdtBoolean;
    PBoolean(@FData)^ := AValue;
    Changed([sscData]);
  end;
end;

procedure TdxSpreadSheetCell.SetAsCurrency(const AValue: Currency);
begin
  if (DataType <> cdtCurrency) or (AsCurrency <> AValue) then
  begin
    CheckAndDeleteObjectData;
    FDataType := cdtCurrency;
    PCurrency(@FData)^ := AValue;
    Changed([sscData]);
  end;
end;

procedure TdxSpreadSheetCell.SetAsDateTime(const AValue: TDateTime);
begin
  if (DataType <> cdtDateTime) or (AsDateTime <> AValue) then
  begin
    CheckAndDeleteObjectData;
    FDataType := cdtDateTime;
    PDateTime(@FData)^ := AValue;
    Style.DataFormat.FormatCodeID := 14;
    Changed([sscData]);
  end;
end;

procedure TdxSpreadSheetCell.SetAsError(const AValue: TdxSpreadSheetFormulaErrorCode);
begin
  if (DataType <> cdtError) or (AsError <> AValue) then
  begin
    CheckAndDeleteObjectData;
    FDataType := cdtError;
    FData[0] := Byte(AValue);
    Changed([sscData]);
  end;
end;

procedure TdxSpreadSheetCell.SetAsFloat(const AValue: Double);
begin
  if (DataType <> cdtFloat) or (AsFloat <> AValue) then
  begin
    CheckAndDeleteObjectData;
    FDataType := cdtFloat;
    PFloat(@FData)^ := AValue;
    Changed([sscData]);
  end;
end;

procedure TdxSpreadSheetCell.SetAsFormula(const AValue: TdxSpreadSheetFormula);
begin
  if (DataType <> cdtFormula) or (PObject(@FData)^ <> AValue) then
  begin
    CheckAndDeleteObjectData;
    FDataType := cdtFormula;
    PObject(@FData)^ := AValue;
    AValue.SetOwner(Self);
    Changed([sscData]);
  end;
end;

procedure TdxSpreadSheetCell.SetAsInteger(const AValue: Integer);
begin
  if (DataType <> cdtInteger) or (AsInteger <> AValue) then
  begin
    CheckAndDeleteObjectData;
    FDataType := cdtInteger;
    PInteger(@FData)^ := AValue;
    Changed([sscData]);
  end;
end;

procedure TdxSpreadSheetCell.SetAsSharedString(AValue: TdxSpreadSheetSharedString);
begin
  if (DataType <> cdtString) or (AsSharedString <> AValue) then
  begin
    CheckAndDeleteObjectData;
    FDataType := cdtString;
    AValue.AddRef;
    PObject(@FData)^ := AValue;
    Changed([sscData]);
  end;
end;

procedure TdxSpreadSheetCell.SetAsString(const AValue: TdxUnicodeString);
begin
  SetAsSharedString(View.StringTable.Add(AValue));
end;

procedure TdxSpreadSheetCell.SetAsVariant(const AValue: Variant);
begin
  case VarType(AValue) of
    varEmpty, varNull:
      Clear;
    varSmallint, varInteger, varSingle, varShortInt, varByte, varWord:
      AsInteger := AValue;
    varDate:
      AsDateTime := AValue;
    varCurrency:
      AsCurrency := AValue;
    varDouble, varLongWord, varInt64:
      AsFloat := AValue;
    varBoolean:
      AsBoolean := AValue;
  else
    SetText(VarToWideStr(AValue), True);
  end;
end;

{ TdxSpreadSheetContainerAnchorPoint }

constructor TdxSpreadSheetContainerAnchorPoint.Create(AOwner: TPersistent);
begin
  inherited Create(AOwner);
  FFixedToCell := True;
end;

procedure TdxSpreadSheetContainerAnchorPoint.Changed;
begin
  Container.Changed;
end;

procedure TdxSpreadSheetContainerAnchorPoint.DoAssign(Source: TPersistent);
begin
  inherited DoAssign(Source);
  if Source is TdxSpreadSheetContainerAnchorPoint then
  begin
    Cell := TdxSpreadSheetContainerAnchorPoint(Source).Cell;
    Offset := TdxSpreadSheetContainerAnchorPoint(Source).Offset;
    FixedToCell := TdxSpreadSheetContainerAnchorPoint(Source).FixedToCell;
  end;
end;

function TdxSpreadSheetContainerAnchorPoint.IsValidCell(AValue: TdxSpreadSheetCell): Boolean;
begin
  Result := (AValue <> nil) and (AValue.View = Container.Parent);
end;

procedure TdxSpreadSheetContainerAnchorPoint.LoadFromStream(AReader: TcxReader);
var
  AColumnIndex: Integer;
  ARowIndex: Integer;
  ATableView: IdxSpreadSheetTableView;
begin
  AColumnIndex := AReader.ReadInteger;
  ARowIndex := AReader.ReadInteger;
  if (AColumnIndex >= 0) and (ARowIndex >= 0) and Supports(Container.Parent, IdxSpreadSheetTableView, ATableView) then
    Cell := ATableView.GetCell(ARowIndex, AColumnIndex)
  else
    Cell := nil;

  FixedToCell := AReader.ReadBoolean;
  Offset := AReader.ReadPoint;
end;

procedure TdxSpreadSheetContainerAnchorPoint.SaveToStream(AWriter: TcxWriter);
begin
  if Cell <> nil then
  begin
    AWriter.WriteInteger(Cell.ColumnIndex);
    AWriter.WriteInteger(Cell.RowIndex);
  end
  else
  begin
    AWriter.WriteInteger(-1);
    AWriter.WriteInteger(-1);
  end;
  AWriter.WriteBoolean(FixedToCell);
  AWriter.WritePoint(Offset);
end;

function TdxSpreadSheetContainerAnchorPoint.GetContainer: TdxSpreadSheetContainer;
begin
  Result := Owner as TdxSpreadSheetContainer;
end;

procedure TdxSpreadSheetContainerAnchorPoint.SetCell(const AValue: TdxSpreadSheetCell);
begin
  if (AValue <> nil) and not IsValidCell(AValue) then
    raise EdxSpreadSheetError.CreateFmt(cxGetResourceString(@sdxErrorInvalidAnchorCell),
      [TdxSpreadSheetColumnHelper.NameByIndex(AValue.ColumnIndex) + IntToStr(AValue.RowIndex + 1)]);

  if Cell <> AValue then
  begin
    FCell := AValue;
    Changed;
  end;
end;

procedure TdxSpreadSheetContainerAnchorPoint.SetOffset(const AValue: TPoint);
begin
  if not cxPointIsEqual(FOffset, AValue) then
  begin
    FOffset := AValue;
    Changed;
  end;
end;

{ TdxSpreadSheetContainerTransform }

procedure TdxSpreadSheetContainerTransform.Changed;
begin
  Container.Changed;
end;

procedure TdxSpreadSheetContainerTransform.DoAssign(Source: TPersistent);
begin
  inherited DoAssign(Source);
  if Source is TdxSpreadSheetContainerTransform then
  begin
    FlipHorizontally := TdxSpreadSheetContainerTransform(Source).FlipHorizontally;
    FlipVertically := TdxSpreadSheetContainerTransform(Source).FlipVertically;
    RotationAngle := TdxSpreadSheetContainerTransform(Source).RotationAngle;
  end;
end;

procedure TdxSpreadSheetContainerTransform.LoadFromStream(AReader: TcxReader);
begin
  FlipHorizontally := AReader.ReadBoolean;
  FlipVertically := AReader.ReadBoolean;
  RotationAngle := AReader.ReadFloat;
end;

procedure TdxSpreadSheetContainerTransform.SaveToStream(AWriter: TcxWriter);
begin
  AWriter.WriteBoolean(FlipHorizontally);
  AWriter.WriteBoolean(FlipVertically);
  AWriter.WriteFloat(RotationAngle);
end;

function TdxSpreadSheetContainerTransform.GetContainer: TdxSpreadSheetContainer;
begin
  Result := inherited Owner as TdxSpreadSheetContainer;
end;

procedure TdxSpreadSheetContainerTransform.SetFlipHorizontally(AValue: Boolean);
begin
  if FFlipHorizontally <> AValue then
  begin
    FFlipHorizontally := AValue;
    Changed;
  end;
end;

procedure TdxSpreadSheetContainerTransform.SetFlipVertically(AValue: Boolean);
begin
  if FFlipVertically <> AValue then
  begin
    FFlipVertically := AValue;
    Changed;
  end;
end;

procedure TdxSpreadSheetContainerTransform.SetRotationAngle(const AValue: Double);
var
  ABounds: TRect;
begin
  if FRotationAngle <> AValue then
  begin
    if not Container.IsUpdating then
    begin
      ABounds := Container.Calculator.CalculateBounds;
      FRotationAngle := AValue;
      Container.Calculator.UpdateAnchors(ABounds);
    end
    else
      FRotationAngle := AValue;

    Changed;
  end;
end;

{ TdxSpreadSheetContainer }

constructor TdxSpreadSheetContainer.Create(ASpreadSheet: TdxCustomSpreadSheet);
begin
  inherited Create(ASpreadSheet);
  FCalculator := CreateCalculator;
  FAnchorPoint1 := CreateAnchorPoint;
  FAnchorPoint2 := CreateAnchorPoint;
  FTransform := CreateTransform;
  FVisible := True;
end;

destructor TdxSpreadSheetContainer.Destroy;
begin
  FreeAndNil(FAnchorPoint2);
  FreeAndNil(FAnchorPoint1);
  FreeAndNil(FCalculator);
  FreeAndNil(FTransform);
  inherited Destroy;
end;

procedure TdxSpreadSheetContainer.BeforeDestruction;
begin
  inherited BeforeDestruction;
  if not SpreadSheet.IsDestroying then
  begin
    Parent := nil;
    if History.CanAddCommand then
    begin
      History.BeginAction(TdxSpreadSheetHistoryChangeContainerAction);
      History.AddCommand(TdxSpreadSheetHistoryDeleteContainerCommand.Create(Self));
      History.EndAction;
    end;
  end;
end;

procedure TdxSpreadSheetContainer.Assign(Source: TPersistent);
begin
  if Source is TdxSpreadSheetContainer then
  begin
    BeginUpdate;
    try
      AnchorPoint1 := TdxSpreadSheetContainer(Source).AnchorPoint1;
      AnchorPoint2 := TdxSpreadSheetContainer(Source).AnchorPoint2;
      AnchorType := TdxSpreadSheetContainer(Source).AnchorType;
      Description := TdxSpreadSheetContainer(Source).Description;
      Name := TdxSpreadSheetContainer(Source).Name;
      Restrictions := TdxSpreadSheetContainer(Source).Restrictions;
      Title := TdxSpreadSheetContainer(Source).Title;
      Transform := TdxSpreadSheetContainer(Source).Transform;
      Visible := TdxSpreadSheetContainer(Source).Visible;
    finally
      EndUpdate;
    end;
  end;
end;

procedure TdxSpreadSheetContainer.BeginUpdate;
begin
  Inc(FUpdateCount);
end;

procedure TdxSpreadSheetContainer.EndUpdate;
begin
  Dec(FUpdateCount);
  if (FUpdateCount = 0) and FHasChanges then
    Changed;
end;

procedure TdxSpreadSheetContainer.BringToFront;
begin
  Index := MaxInt;
end;

procedure TdxSpreadSheetContainer.SendToBack;
begin
  Index := 0;
end;

procedure TdxSpreadSheetContainer.CellRemoving(ACell: TdxSpreadSheetCell);
var
  R: TRect;
begin
  if (ACell = AnchorPoint1.Cell) or (ACell = AnchorPoint2.Cell) then
  begin
    if AnchorType <> catAbsolute then
    begin
      R := Calculator.CalculateBounds;
      AnchorType := catAbsolute;
      Calculator.UpdateAnchors(R);
    end;
    AnchorPoint1.Cell := nil;
    AnchorPoint2.Cell := nil;
  end;
end;

procedure TdxSpreadSheetContainer.Changed;
begin
  if IsUpdating then
    FHasChanges := True
  else
  begin
    FHasChanges := False;
    DoChanged;
  end;
end;

procedure TdxSpreadSheetContainer.CheckChangeObject(AProc: TProc);
begin
  if History.IsLocked then
    AProc
  else
  begin
    History.BeginAction(TdxSpreadSheetHistoryChangeContainerAction);
    try
      History.AddCommand(TdxSpreadSheetHistoryChangeContainerCommand.Create(Self));
      AProc;
    finally
      History.EndAction;
    end;
  end;
end;

procedure TdxSpreadSheetContainer.CheckCreateObject;
begin
  if not History.IsLocked and not SpreadSheet.IsLoading and (SpreadSheet.State = []) then
    History.AddCommand(TdxSpreadSheetHistoryCreateContainerCommand.Create(Self));
end;

function TdxSpreadSheetContainer.CreateAnchorPoint: TdxSpreadSheetContainerAnchorPoint;
begin
  Result := TdxSpreadSheetContainerAnchorPoint.Create(Self);
end;

function TdxSpreadSheetContainer.CreateCalculator: TdxSpreadSheetContainerCalculator;
begin
  Result := TdxSpreadSheetContainerCalculator.Create(Self);
end;

function TdxSpreadSheetContainer.CreateTransform: TdxSpreadSheetContainerTransform;
begin
  Result := TdxSpreadSheetContainerTransform.Create(Self);
end;

function TdxSpreadSheetContainer.CreateViewInfo: TdxSpreadSheetContainerViewInfo;
begin
  Result := TdxSpreadSheetContainerViewInfo.Create(Self);
end;

procedure TdxSpreadSheetContainer.DoChanged;
begin
  SpreadSheet.LayoutChanged;
end;

function TdxSpreadSheetContainer.CanMove: Boolean;
begin
  Result := not ((crNoMove in Restrictions) or Parent.Options.Protected);
end;

function TdxSpreadSheetContainer.CanResize: Boolean;
begin
  Result := not ((crNoResize in Restrictions) or Parent.Options.Protected);
end;

function TdxSpreadSheetContainer.CanRotate: Boolean;
begin
  Result := not ((crNoRotation in Restrictions) or Parent.Options.Protected);
end;

function TdxSpreadSheetContainer.KeepAspectUsingCornerHandles: Boolean;
begin
  Result := crNoChangeAspectUsingCornerHandles in Restrictions;
end;

procedure TdxSpreadSheetContainer.LoadFromStream(AReader: TcxReader);
begin
  BeginUpdate;
  try
    LoadSettings(AReader);
  finally
    EndUpdate;
  end;
end;

procedure TdxSpreadSheetContainer.LoadSettings(AReader: TcxReader);
var
  AVersion: Word;
begin
  LoadHeaderFromStream(AReader, AVersion);
  LoadSettingsFromStream(AReader, AVersion);
end;

procedure TdxSpreadSheetContainer.LoadSettingsFromStream(AReader: TcxReader; const AVersion: Word);
var
  AIndex: TdxSpreadSheetContainerRestriction;
begin
  BeginUpdate;
  try
    AnchorPoint1.LoadFromStream(AReader);
    AnchorPoint2.LoadFromStream(AReader);
    Description := AReader.ReadWideString;
    Name := AReader.ReadWideString;
    Title := AReader.ReadWideString;
    Visible := AReader.ReadBoolean;
    Transform.LoadFromStream(AReader);

    Restrictions := [];
    for AIndex := Low(AIndex) to High(AIndex) do
    begin
      if AReader.ReadBoolean then
        Restrictions := Restrictions + [AIndex];
    end;
  finally
    EndUpdate;
  end;
end;

procedure TdxSpreadSheetContainer.SaveSettings(AWriter: TcxWriter);
begin
  SaveHeaderToStream(AWriter);
  SaveSettingsToStream(AWriter);
end;

procedure TdxSpreadSheetContainer.SaveSettingsToStream(AWriter: TcxWriter);
var
  AIndex: TdxSpreadSheetContainerRestriction;
begin
  AnchorPoint1.SaveToStream(AWriter);
  AnchorPoint2.SaveToStream(AWriter);
  AWriter.WriteWideString(Description);
  AWriter.WriteWideString(Name);
  AWriter.WriteWideString(Title);
  AWriter.WriteBoolean(Visible);
  Transform.SaveToStream(AWriter);
  for AIndex := Low(AIndex) to High(AIndex) do
    AWriter.WriteBoolean(AIndex in Restrictions);
end;

procedure TdxSpreadSheetContainer.SaveToStream(AWriter: TcxWriter);
begin
  SaveSettings(AWriter);
end;

function TdxSpreadSheetContainer.GetFocused: Boolean;
begin
  Result := (Parent <> nil) and (Parent.Controller.FocusedContainer = Self);
end;

function TdxSpreadSheetContainer.GetHistory: TdxSpreadSheetHistory;
begin
  Result := SpreadSheet.History;
end;

function TdxSpreadSheetContainer.GetIndex: Integer;
begin
  if Parent <> nil then
    Result := Parent.Containers.IndexOf(Self)
  else
    Result := -1;
end;

function TdxSpreadSheetContainer.GetIsUpdating: Boolean;
begin
  Result := FUpdateCount > 0;
end;

procedure TdxSpreadSheetContainer.LoadHeaderFromStream(AReader: TcxReader; out AVersion: Word);
begin
  AnchorType := TdxSpreadSheetContainerAnchorType(AReader.ReadWord);
  AVersion := AReader.ReadWord;
end;

procedure TdxSpreadSheetContainer.SaveHeaderToStream(AWriter: TcxWriter);
begin
  AWriter.WriteWord(Ord(AnchorType));
  AWriter.WriteWord(dxSpreadSheetContainerVersion);
end;

procedure TdxSpreadSheetContainer.SetAnchorPoint1(const AValue: TdxSpreadSheetContainerAnchorPoint);
begin
  FAnchorPoint1.Assign(AValue);
end;

procedure TdxSpreadSheetContainer.SetAnchorPoint2(const AValue: TdxSpreadSheetContainerAnchorPoint);
begin
  FAnchorPoint2.Assign(AValue);
end;

procedure TdxSpreadSheetContainer.SetAnchorType(const AValue: TdxSpreadSheetContainerAnchorType);
begin
  if FAnchorType <> AValue then
  begin
    FAnchorType := AValue;
    Changed;
  end;
end;

procedure TdxSpreadSheetContainer.SetFocused(const AValue: Boolean);
begin
  if Focused <> AValue then
  begin
    if AValue then
      Parent.Controller.FocusedContainer := Self
    else
      Parent.Controller.FocusedContainer := nil;
  end;
end;

procedure TdxSpreadSheetContainer.SetIndex(AValue: Integer);
begin
  if Parent <> nil then
  begin
    AValue := Max(Min(AValue, Parent.Containers.Count - 1), 0);
    if Index <> AValue then
    begin
      Parent.Containers.Extract(Self);
      Parent.Containers.Insert(AValue, Self);
      Changed;
    end;
  end;
end;

procedure TdxSpreadSheetContainer.SetParent(const AValue: TdxSpreadSheetCustomView);
var
  ABounds: TRect;
begin
  if FParent <> AValue then
  begin
    BeginUpdate;
    try
      Focused := False;
      ABounds := Calculator.CalculateBounds;
      if FParent <> nil then
        FParent.Containers.InternalRemove(Self);
      if AValue <> nil then
        AValue.Containers.InternalAdd(Self);
      FParent := AValue;
      AnchorPoint1.Cell := nil;
      AnchorPoint2.Cell := nil;
      Calculator.UpdateAnchors(ABounds);
      Changed;
    finally
      EndUpdate;
    end;
  end;
end;

procedure TdxSpreadSheetContainer.SetTransform(AValue: TdxSpreadSheetContainerTransform);
begin
  FTransform.Assign(AValue);
end;

procedure TdxSpreadSheetContainer.SetVisible(AValue: Boolean);
begin
  if FVisible <> AValue then
  begin
    Focused := False;
    FVisible := AValue;
    Changed;
  end;
end;

{ TdxSpreadSheetCustomDrawingObject }

procedure TdxSpreadSheetCustomDrawingObject.Assign(Source: TPersistent);
begin
  // do nothing
end;

procedure TdxSpreadSheetCustomDrawingObject.Changed;
begin
  dxCallNotify(OnChange, Self);
end;

{ TdxSpreadSheetContainerCalculator }

constructor TdxSpreadSheetContainerCalculator.Create(AOwner: TdxSpreadSheetContainer);
begin
  inherited Create;
  FOwner := AOwner;
end;

function TdxSpreadSheetContainerCalculator.CalculateBounds: TRect;
begin
  Result := cxNullRect;
  case AnchorType of
    catOneCell, catTwoCell:
      Result.TopLeft := cxPointOffset(GetCellAbsoluteBounds(AnchorPoint1.Cell).TopLeft, AnchorPoint1.Offset);
    catAbsolute:
      Result.TopLeft := AnchorPoint1.Offset;
  end;

  case AnchorType of
    catAbsolute, catOneCell:
      Result := cxRectSetSize(Result, AnchorPoint2.Offset.X, AnchorPoint2.Offset.Y);
    catTwoCell:
      Result.BottomRight := cxPointOffset(GetCellAbsoluteBounds(AnchorPoint2.Cell).TopLeft, AnchorPoint2.Offset);
  end;

  Result := cxRectAdjust(TransformCoords(Result, True));
end;

procedure TdxSpreadSheetContainerCalculator.UpdateAnchors(const ABounds: TRect);
var
  ACell1: TdxSpreadSheetCell;
  ACell2: TdxSpreadSheetCell;
  ARect: TRect;
begin
  Owner.BeginUpdate;
  try
    ARect := TransformCoords(ABounds, False);
    case AnchorType of
      catOneCell:
        if GetNearCells(ARect, ACell1, ACell2) then
        begin
          AnchorPoint1.Cell := ACell1;
          AnchorPoint1.Offset := cxPointOffset(ARect.TopLeft, GetCellAbsoluteBounds(ACell1).TopLeft, False);
          AnchorPoint2.Offset := cxPointOffset(ARect.BottomRight, ARect.TopLeft, False);
        end
        else
          AnchorType := catAbsolute;

      catTwoCell:
        if GetNearCells(ARect, ACell1, ACell2) then
        begin
          AnchorPoint1.Cell := ACell1;
          AnchorPoint1.Offset := cxPointOffset(ARect.TopLeft, GetCellAbsoluteBounds(ACell1).TopLeft, False);

          AnchorPoint2.Cell := ACell2;
          AnchorPoint2.Offset := cxPointOffset(ARect.BottomRight, GetCellAbsoluteBounds(ACell2).TopLeft, False);
        end
        else
          AnchorType := catAbsolute;
    end;

    if AnchorType = catAbsolute then
    begin
      AnchorPoint1.Offset := ARect.TopLeft;
      AnchorPoint2.Offset := cxPointOffset(ARect.BottomRight, ARect.TopLeft, False);
    end;
  finally
    Owner.EndUpdate;
  end;
end;

function TdxSpreadSheetContainerCalculator.UpdateAnchorsAfterResize(const APrevBounds: TRect): Boolean;
begin
  if AnchorPoint1.FixedToCell then
  begin
    Result := (AnchorType = catTwoCell) and not AnchorPoint2.FixedToCell;
    if Result then
      UpdateAnchors(cxRectSetSize(CalculateBounds, cxSize(APrevBounds)));
  end
  else
  begin
    Result := AnchorType in [catOneCell, catTwoCell];
    if Result then
      UpdateAnchors(APrevBounds);
  end;
end;

function TdxSpreadSheetContainerCalculator.CheckForContentArea(const R: TRect): TRect;
var
  ADeltaX: Integer;
  ADeltaY: Integer;
  ARect: TRect;
  AContentRect: TRect;
  AView: TdxSpreadSheetCustomView;
  AViewInfo: TdxSpreadSheetContainerViewInfo;
  AIndex: Integer;
begin
  AView := Owner.Parent;
  AIndex := AView.ViewInfo.Containers.FindItem(Owner);
  AViewInfo := AView.ViewInfo.Containers.Items[AIndex] as TdxSpreadSheetContainerViewInfo;
  ARect := cxRectCenter(R, cxSize(AViewInfo.RealBounds));

  AContentRect := GetContentRect;
  ADeltaX := Max(AContentRect.Left - ARect.Left, 0);
  if ADeltaX = 0 then
    ADeltaX := Min(AContentRect.Right - ARect.Right, 0);

  ADeltaY := Max(AContentRect.Top - ARect.Top, 0);
  if ADeltaY = 0 then
    ADeltaY := Min(AContentRect.Bottom - ARect.Bottom, 0);

  Result := cxRectOffset(R, ADeltaX, ADeltaY);
end;

function TdxSpreadSheetContainerCalculator.GetCellAbsoluteBounds(ACell: TdxSpreadSheetCell): TRect;
begin
  if ACell <> nil then
    Result := ACell.View.GetAbsoluteCellBounds(ACell.RowIndex, ACell.ColumnIndex, False)
  else
    Result := cxNullRect;
end;

function TdxSpreadSheetContainerCalculator.GetContentRect: TRect;
var
  ATableView: IdxSpreadSheetTableView;
begin
  Result := cxNullRect;
  if Supports(Owner.Parent, IdxSpreadSheetTableView, ATableView) then
    Result.BottomRight := ATableView.GetAbsoluteCellBounds(dxSpreadSheetMaxRowIndex, dxSpreadSheetMaxColumnIndex, False).BottomRight;
end;

function TdxSpreadSheetContainerCalculator.GetNearCells(const R: TRect; out ACell1, ACell2: TdxSpreadSheetCell): Boolean;
var
  AColumnIndex: Integer;
  ARowIndex: Integer;
  ATableView: IdxSpreadSheetTableView;
begin
  ACell1 := nil;
  ACell2 := nil;
  if Supports(Owner.Parent, IdxSpreadSheetTableView, ATableView) then
  begin
    if ATableView.GetCellAtAbsolutePoint(R.TopLeft, ARowIndex, AColumnIndex) then
      ACell1 := ATableView.GetCell(ARowIndex, AColumnIndex);
    if ATableView.GetCellAtAbsolutePoint(R.BottomRight, ARowIndex, AColumnIndex) then
      ACell2 := ATableView.GetCell(ARowIndex, AColumnIndex);
  end;
  Result := (ACell1 <> nil) and (ACell2 <> nil);
end;

procedure TdxSpreadSheetContainerCalculator.ModifyBounds(ALeftModifier, ATopModifier, ARightModifier,
  ABottomModifier: Integer);
var
  ARect: TRect;
begin
  ARect := CalculateBounds;
  ARect.TopLeft := cxPointOffset(ARect.TopLeft, ALeftModifier, ATopModifier);
  ARect.BottomRight := cxPointOffset(ARect.BottomRight, ARightModifier, ABottomModifier);
  UpdateAnchors(CheckForContentArea(ARect));
end;

procedure TdxSpreadSheetContainerCalculator.ResizeContainer(ADeltaX, ADeltaY: Integer);
var
  AFactorX: Integer;
  AFactorY: Integer;
  ARect: TRect;
begin
  ARect := CalculateBounds;
  AFactorX := MulDiv(ARect.Width, 10, 100);
  AFactorY := MulDiv(ARect.Height, 10, 100);

  if ADeltaX > 0 then
    AFactorX := Max(AFactorX, 1);
  if ADeltaY > 0 then
    AFactorY := Max(AFactorY, 1);

  if Owner.KeepAspectUsingCornerHandles then
  begin
    if ADeltaX = 0 then
      ADeltaX := ADeltaY
    else
      ADeltaY := ADeltaX;
  end;

  ModifyBounds(-ADeltaX * AFactorX, -ADeltaY * AFactorY, ADeltaX * AFactorX, ADeltaY * AFactorY);
end;

function TdxSpreadSheetContainerCalculator.TransformCoords(const R: TRect; ABackwardDirection: Boolean): TRect;
var
  AMatrix: TdxGPMatrix;
begin
  Result := R;
  if Abs(Sin(Transform.RotationAngle * Pi / 180)) > Sin(Pi / 4) then
  begin
    AMatrix := TdxGPMatrix.Create;
    try
      AMatrix.Rotate(90, cxRectCenter(dxRectF(Result)));
      if ABackwardDirection then
        AMatrix.Invert;
      Result := cxRectAdjust(AMatrix.TransformRect(Result));
    finally
      AMatrix.Free;
    end;
  end;
end;

function TdxSpreadSheetContainerCalculator.GetAnchorPoint1: TdxSpreadSheetContainerAnchorPoint;
begin
  Result := Owner.AnchorPoint1;
end;

function TdxSpreadSheetContainerCalculator.GetAnchorPoint2: TdxSpreadSheetContainerAnchorPoint;
begin
  Result := Owner.AnchorPoint2;
end;

function TdxSpreadSheetContainerCalculator.GetAnchorType: TdxSpreadSheetContainerAnchorType;
begin
  Result := Owner.AnchorType;
end;

function TdxSpreadSheetContainerCalculator.GetTransform: TdxSpreadSheetContainerTransform;
begin
  Result := Owner.Transform;
end;

procedure TdxSpreadSheetContainerCalculator.SetAnchorType(const Value: TdxSpreadSheetContainerAnchorType);
begin
  Owner.AnchorType := Value;
end;

{ TdxSpreadSheetShape }

constructor TdxSpreadSheetShape.Create(ASpreadSheet: TdxCustomSpreadSheet);
begin
  inherited Create(ASpreadSheet);
  FPen := TdxGPPen.Create;
  FPen.OnChange := ChangeHandler;
  FBrush := TdxGPBrush.Create;
  FBrush.OnChange := ChangeHandler;
end;

destructor TdxSpreadSheetShape.Destroy;
begin
  FreeAndNil(FBrush);
  FreeAndNil(FPen);
  inherited Destroy;
end;

procedure TdxSpreadSheetShape.Assign(Source: TPersistent);
begin
  inherited Assign(Source);
  if Source is TdxSpreadSheetShape then
  begin
    Pen := TdxSpreadSheetShape(Source).Pen;
    Brush := TdxSpreadSheetShape(Source).Brush;
    ShapeType := TdxSpreadSheetShape(Source).ShapeType;
  end;
end;

procedure TdxSpreadSheetShape.ChangeHandler(Sender: TObject);
begin
  Changed;
end;

procedure TdxSpreadSheetShape.SetBrush(const AValue: TdxGPBrush);
begin
  FBrush.Assign(AValue);
end;

procedure TdxSpreadSheetShape.SetPen(const AValue: TdxGPPen);
begin
  FPen.Assign(AValue);
end;

procedure TdxSpreadSheetShape.SetShapeType(const AValue: TdxSpreadSheetShapeType);
begin
  if AValue <> FShapeType then
  begin
    FShapeType := AValue;
    Changed;
  end;
end;

{ TdxSpreadSheetShapeContainer }

constructor TdxSpreadSheetShapeContainer.Create(ASpreadSheet: TdxCustomSpreadSheet);
begin
  inherited Create(ASpreadSheet);
  FShape := CreateShape;
  FShape.OnChange := ShapeChangeHandler;
end;

destructor TdxSpreadSheetShapeContainer.Destroy;
begin
  FreeAndNil(FShape);
  inherited Destroy;
end;

procedure TdxSpreadSheetShapeContainer.Assign(Source: TPersistent);
begin
  inherited Assign(Source);
  if Source is TdxSpreadSheetShapeContainer then
    Shape := TdxSpreadSheetShapeContainer(Source).Shape;
end;

function TdxSpreadSheetShapeContainer.CreateShape: TdxSpreadSheetShape;
begin
  Result := TdxSpreadSheetShape.Create(SpreadSheet);
end;

function TdxSpreadSheetShapeContainer.CreateViewInfo: TdxSpreadSheetContainerViewInfo;
begin
  Result := TdxSpreadSheetShapeContainerViewInfo.Create(Self);
end;

procedure TdxSpreadSheetShapeContainer.LoadBrushFromStream(ABrush: TdxGPBrush; AReader: TcxReader);
var
  AColor: TdxAlphaColor;
  ACount: Integer;
  AOffset: Single;
  I: Integer;
begin
  ABrush.Color := AReader.ReadCardinal;
  ABrush.Style := TdxGPBrushStyle(AReader.ReadInteger);
  ABrush.GradientMode := TdxGPBrushGradientMode(AReader.ReadInteger);
  ACount := AReader.ReadInteger;
  for I := 0 to ACount - 1 do
  begin
    AOffset := AReader.ReadSingle;
    AColor := AReader.ReadCardinal;
    ABrush.GradientPoints.Add(AOffset, AColor);
  end;
  LoadImageFromStream(ABrush.Texture, AReader);
end;

procedure TdxSpreadSheetShapeContainer.LoadImageFromStream(AImage: TdxGPImage; AReader: TcxReader);
var
  AImageData: TMemoryStream;
begin
  AImageData := TMemoryStream.Create;
  try
    AImageData.Size := AReader.ReadInteger;
    if AImageData.Size > 0 then
    begin
      AReader.Stream.ReadBuffer(AImageData.Memory^, AImageData.Size);
      AImage.LoadFromStream(AImageData);
    end
    else
      AImage.Clear;
  finally
    AImageData.Free;
  end;
end;

procedure TdxSpreadSheetShapeContainer.LoadSettingsFromStream(AReader: TcxReader; const AVersion: Word);
begin
  BeginUpdate;
  try
    inherited LoadSettingsFromStream(AReader, AVersion);
    Shape.ShapeType := TdxSpreadSheetShapeType(AReader.ReadInteger);
    LoadBrushFromStream(Shape.Brush, AReader);
    Shape.Pen.Style := TdxGPPenStyle(AReader.ReadInteger);
    Shape.Pen.Width := AReader.ReadSingle;
    LoadBrushFromStream(Shape.Pen.Brush, AReader);
  finally
    EndUpdate;
  end;
end;

procedure TdxSpreadSheetShapeContainer.SaveBrushToStream(ABrush: TdxGPBrush; AWriter: TcxWriter);
var
  I: Integer;
begin
  AWriter.WriteCardinal(ABrush.Color);
  AWriter.WriteInteger(Ord(ABrush.Style));
  AWriter.WriteInteger(Ord(ABrush.GradientMode));
  AWriter.WriteInteger(ABrush.GradientPoints.Count);
  for I := 0 to ABrush.GradientPoints.Count - 1 do
  begin
    AWriter.WriteSingle(ABrush.GradientPoints.Offsets[I]);
    AWriter.WriteCardinal(ABrush.GradientPoints.Colors[I]);
  end;
  SaveImageToStream(ABrush.Texture, AWriter);
end;

procedure TdxSpreadSheetShapeContainer.SaveImageToStream(AImage: TdxGPImage; AWriter: TcxWriter);
var
  AImageData: TMemoryStream;
begin
  if (AImage <> nil) and not AImage.Empty then
  begin
    AImageData := TMemoryStream.Create;
    try
      AImage.SaveToStream(AImageData);
      AImageData.Position := 0;
      AWriter.WriteInteger(AImageData.Size);
      AWriter.Stream.WriteBuffer(AImageData.Memory^, AImageData.Size);
    finally
      AImageData.Free;
    end;
  end
  else
    AWriter.WriteInteger(0);
end;

procedure TdxSpreadSheetShapeContainer.SaveSettingsToStream(AWriter: TcxWriter);
begin
  inherited SaveSettingsToStream(AWriter);
  AWriter.WriteInteger(Ord(Shape.ShapeType));
  SaveBrushToStream(Shape.Brush, AWriter);

  AWriter.WriteInteger(Ord(Shape.Pen.Style));
  AWriter.WriteSingle(Shape.Pen.Width);
  SaveBrushToStream(Shape.Pen.Brush, AWriter);
end;

procedure TdxSpreadSheetShapeContainer.SetShape(AValue: TdxSpreadSheetShape);
begin
  Shape.Assign(AValue);
end;

procedure TdxSpreadSheetShapeContainer.ShapeChangeHandler(Sender: TObject);
begin
  Changed;
end;

{ TdxSpreadSheetPicture }

destructor TdxSpreadSheetPicture.Destroy;
begin
  ImageHandle := nil;
  inherited Destroy;
end;

procedure TdxSpreadSheetPicture.Assign(Source: TPersistent);
begin
  inherited Assign(Source);
  if Source is TdxSpreadSheetPicture then
  begin
    CropMargins := TdxSpreadSheetPicture(Source).CropMargins;
    ImageHandle := TdxSpreadSheetPicture(Source).ImageHandle;
  end;
end;

function TdxSpreadSheetPicture.GetEmpty: Boolean;
begin
  Result := (Image = nil) or Image.Empty;
end;

function TdxSpreadSheetPicture.GetImage: TdxSmartImage;
begin
  if ImageHandle <> nil then
    Result := ImageHandle.Image
  else
    Result := nil;
end;

procedure TdxSpreadSheetPicture.SetCropMargins(const AValue: TRect);
begin
  if not cxRectIsEqual(FCropMargins, AValue) then
  begin
    FCropMargins := AValue;
    Changed;
  end;
end;

procedure TdxSpreadSheetPicture.SetImage(AValue: TdxSmartImage);
begin
  if AValue <> nil then
    ImageHandle := SpreadSheet.SharedImages.Add(AValue)
  else
    ImageHandle := nil;
end;

procedure TdxSpreadSheetPicture.SetImageHandle(const AValue: TdxSpreadSheetSharedImageHandle);
begin
  if dxChangeHandle(TdxHashTableItem(FImageHandle), AValue) then
    Changed;
end;

{ TdxSpreadSheetPictureContainer }

constructor TdxSpreadSheetPictureContainer.Create(ASpreadSheet: TdxCustomSpreadSheet);
begin
  inherited Create(ASpreadSheet);
  FRelativeResize := True;
  Restrictions := Restrictions + [crNoChangeAspectUsingCornerHandles];
  FPicture := CreatePicture;
  FPicture.OnChange := PictureChangeHandler;
end;

destructor TdxSpreadSheetPictureContainer.Destroy;
begin
  FreeAndNil(FPicture);
  inherited Destroy;
end;

procedure TdxSpreadSheetPictureContainer.Assign(Source: TPersistent);
begin
  inherited Assign(Source);
  if Source is TdxSpreadSheetPictureContainer then
    Picture := TdxSpreadSheetPictureContainer(Source).Picture;
end;

function TdxSpreadSheetPictureContainer.CreatePicture: TdxSpreadSheetPicture;
begin
  Result := TdxSpreadSheetPicture.Create(SpreadSheet);
end;

function TdxSpreadSheetPictureContainer.CreateViewInfo: TdxSpreadSheetContainerViewInfo;
begin
  Result := TdxSpreadSheetPictureContainerViewInfo.Create(Self);
end;

procedure TdxSpreadSheetPictureContainer.LoadFromStream(AReader: TcxReader);
var
  AImage: TdxSmartImage;
begin
  BeginUpdate;
  try
    inherited LoadFromStream(AReader);
    AImage := TdxSmartImage.Create;
    try
      LoadImageFromStream(AImage, AReader);
      if AImage.Empty then
        Picture.Image := nil
      else
        Picture.Image := AImage;
    finally
      AImage.Free;
    end;
  finally
    EndUpdate;
  end;
end;

procedure TdxSpreadSheetPictureContainer.LoadSettingsFromStream(AReader: TcxReader; const AVersion: Word);
begin
  BeginUpdate;
  try
    inherited LoadSettingsFromStream(AReader, AVersion);
    Picture.CropMargins := AReader.ReadRect;
    if AVersion >= 1 then
      RelativeResize := AReader.ReadBoolean;
  finally
    EndUpdate;
  end;
end;

procedure TdxSpreadSheetPictureContainer.SaveSettingsToStream(AWriter: TcxWriter);
begin
  inherited SaveSettingsToStream(AWriter);
  AWriter.WriteRect(Picture.CropMargins);
  AWriter.WriteBoolean(RelativeResize);
end;

procedure TdxSpreadSheetPictureContainer.SaveToStream(AWriter: TcxWriter);
begin
  inherited SaveToStream(AWriter);
  SaveImageToStream(Picture.Image, AWriter);
end;

procedure TdxSpreadSheetPictureContainer.PictureChangeHandler(Sender: TObject);
begin
  Changed;
end;

procedure TdxSpreadSheetPictureContainer.SetPicture(AValue: TdxSpreadSheetPicture);
begin
  FPicture.Assign(AValue);
end;

{ TdxSpreadSheetContainers }

constructor TdxSpreadSheetContainers.Create(AOwner: TdxSpreadSheetCustomView);
begin
  inherited Create;
  FOwner := AOwner;
end;

function TdxSpreadSheetContainers.Add(AClass: TdxSpreadSheetContainerClass): TdxSpreadSheetContainer;
begin
  Result := AClass.Create(Owner.SpreadSheet);
  Result.Parent := Owner;
end;

function TdxSpreadSheetContainers.AddPictureContainer: TdxSpreadSheetPictureContainer;
begin
  Result := TdxSpreadSheetPictureContainer(Add(TdxSpreadSheetPictureContainer));
end;

function TdxSpreadSheetContainers.AddShapeContainer: TdxSpreadSheetShapeContainer;
begin
  Result := TdxSpreadSheetShapeContainer(Add(TdxSpreadSheetShapeContainer));
end;

function TdxSpreadSheetContainers.GetFirstVisibleContainer: TdxSpreadSheetContainer;
begin
  Result := InternalGetNextVisibleContainer(0);
end; 

function TdxSpreadSheetContainers.GetLastVisibleContainer: TdxSpreadSheetContainer;
begin
  Result := InternalGetPrevVisibleContainer(Count - 1);
end;

function TdxSpreadSheetContainers.GetNextVisibleContainer(
  ACurrentContainer: TdxSpreadSheetContainer): TdxSpreadSheetContainer;
begin 
  Result := InternalGetNextVisibleContainer(ACurrentContainer.Index + 1);
end; 

function TdxSpreadSheetContainers.GetPrevVisibleContainer(
  ACurrentContainer: TdxSpreadSheetContainer): TdxSpreadSheetContainer;
begin
  Result := InternalGetPrevVisibleContainer(ACurrentContainer.Index - 1);
end;

function TdxSpreadSheetContainers.IsAnchorCell(ACell: TdxSpreadSheetCell): Boolean;
var
  AItem: TdxSpreadSheetContainer;
  I: Integer;
begin
  Result := False;
  for I := 0 to Count - 1 do
  begin
    AItem := Items[I];
    Result := (AItem.AnchorPoint1.Cell = ACell) or (AItem.AnchorPoint2.Cell = ACell);
    if Result then
      Break;
  end;
end;

procedure TdxSpreadSheetContainers.CellRemoving(ACell: TdxSpreadSheetCell);
var
  I: Integer;
begin
  for I := Count - 1 downto 0 do
    Items[I].CellRemoving(ACell);
end;

procedure TdxSpreadSheetContainers.InternalAdd(AContainer: TdxSpreadSheetContainer);
begin
  inherited Add(AContainer);
end;

procedure TdxSpreadSheetContainers.InternalRemove(AContainer: TdxSpreadSheetContainer);
begin
  Extract(AContainer);
end;

procedure TdxSpreadSheetContainers.AfterResize;
var
  AContainer: TdxSpreadSheetContainer;
begin
  if FBoundsStorage <> nil then
  begin
    for AContainer in FBoundsStorage.Keys do
      AContainer.Calculator.UpdateAnchorsAfterResize(FBoundsStorage.Items[AContainer]);
    FreeAndNil(FBoundsStorage);
  end;
end;

procedure TdxSpreadSheetContainers.BeforeResize;
var
  I: Integer;
begin
  dxTestCheck(FBoundsStorage = nil, 'TdxSpreadSheetContainers.BeforeResize');

  if Count > 0 then
  begin
    FBoundsStorage := TDictionary<TdxSpreadSheetContainer, TRect>.Create;
    for I := 0 to Count - 1 do
      FBoundsStorage.Add(Items[I], Items[I].Calculator.CalculateBounds);
  end;
end;

function TdxSpreadSheetContainers.InternalGetNextVisibleContainer(AStartFromIndex: Integer): TdxSpreadSheetContainer;
var
  I: Integer;
begin
  Result := nil;
  for I := AStartFromIndex to Count - 1 do
    if Items[I].Visible then
    begin
      Result := Items[I];
      Break;
    end;
end; 

function TdxSpreadSheetContainers.InternalGetPrevVisibleContainer(AStartFromIndex: Integer): TdxSpreadSheetContainer;
var
  I: Integer;
begin
  Result := nil;
  for I := AStartFromIndex downto 0 do
    if Items[I].Visible then
    begin
      Result := Items[I];
      Break;
    end;
end; 

{ TdxSpreadSheetFormulaToken }

destructor TdxSpreadSheetFormulaToken.Destroy;
begin
  ExtractFromList;
  DestroyChildren;
  inherited Destroy;
end;

procedure TdxSpreadSheetFormulaToken.DestroyChildren;
begin
  while FChild <> nil do
    FChild.Free;
end;

procedure TdxSpreadSheetFormulaToken.AddResult(AToken: TdxSpreadSheetFormulaToken);
begin
  // do nothing
end;

procedure TdxSpreadSheetFormulaToken.AttachString(AToken: TdxSpreadSheetFormulaToken; const AValue: TdxUnicodeString);
begin
  AttachString(AToken, TdxSpreadSheetFormulaFormattedText.Create(AValue));
end;

procedure TdxSpreadSheetFormulaToken.AttachString(AToken: TdxSpreadSheetFormulaToken; const AValue: TdxSpreadSheetFormulaFormattedText);
begin
  Owner.AddChild(AToken, AValue);
end;

procedure TdxSpreadSheetFormulaToken.AttachString(AToken: TdxSpreadSheetFormulaToken;
  const ABeforeValue: TdxUnicodeString; AValue: TdxSpreadSheetFormulaFormattedText; const AAfterValue: TdxUnicodeString);
begin
  AValue.AddInBeginning(ABeforeValue);
  AValue.Add(AAfterValue);
  AttachString(AToken, AValue);
end;

function TdxSpreadSheetFormulaToken.ExtractLastTokenAsString(AList: TdxSpreadSheetFormulaToken): TdxSpreadSheetFormulaFormattedText;
var
  AToken: TdxSpreadSheetFormulaToken;
begin
  AToken := AList.GetLastChild;
  if AToken is TdxSpreadSheetFormulaFormattedText then
    Result := TdxSpreadSheetFormulaFormattedText(AToken.ExtractFromList)
  else
    Result := TdxSpreadSheetFormulaFormattedText.Create;
end;

function TdxSpreadSheetFormulaToken.GetExpressionAsText(AExpression: TdxSpreadSheetFormulaToken): TdxSpreadSheetFormulaFormattedText;
begin
  Result := Owner.GetExpressionAsText(AExpression);
end;

procedure TdxSpreadSheetFormulaToken.Calculate(AResult: TdxSpreadSheetFormulaResult);
begin
  AResult.Add(Self);
end;

procedure TdxSpreadSheetFormulaToken.CheckNeighbors;
begin
end;

procedure TdxSpreadSheetFormulaToken.LoadFromStream(AReader: TcxReader);
var
  I: Integer;
  AToken: TdxSpreadSheetFormulaToken;
begin
  for I := 0 to AReader.ReadInteger - 1 do
  begin
    AToken := dxSpreadSheetFormulaTokensRepository[AReader.ReadInteger].Create as TdxSpreadSheetFormulaToken;
    AToken.FOwner := Owner;
    Owner.AddChild(Self, AToken);
    AToken.LoadFromStream(AReader);
  end;
end;

procedure TdxSpreadSheetFormulaToken.ValidateSheet(var ASheet: TdxSpreadSheetTableView); 
begin
  if TdxSpreadSheetInvalidObject.IsLive(ASheet) then 
    if Owner.Controller.SpreadSheet.FSheets.IndexOf(ASheet) < 0 then
      TdxSpreadSheetInvalidObject.AssignTo(ASheet);
end;

procedure TdxSpreadSheetFormulaToken.ValidateExternalLink(var ALink: TdxSpreadSheetExternalLink);
begin
  if TdxSpreadSheetInvalidObject.IsLive(ALink) then
    if Owner.Controller.SpreadSheet.ExternalLinks.ItemList.IndexOf(ALink) < 0 then
      TdxSpreadSheetInvalidObject.AssignTo(ALink);
end;

class procedure TdxSpreadSheetFormulaToken.Register;
begin
  if dxSpreadSheetFormulaTokensRepository.IndexOf(Self) = -1 then
    dxSpreadSheetFormulaTokensRepository.Add(Self);
end;

procedure TdxSpreadSheetFormulaToken.EnumReferences(AProc: TdxSpreadSheetFormulaEnumReferencesProc);
var
  AToken: TdxSpreadSheetFormulaToken;
begin
  AToken := FChild;
  while AToken <> nil do
  begin
    AToken.EnumReferences(AProc);
    AToken := AToken.Next;
  end;
end;

function TdxSpreadSheetFormulaToken.ExtractFromList: TdxSpreadSheetFormulaToken;
begin
  if FPrev <> nil then
    FPrev.FNext := FNext;
  if FNext <> nil then
    FNext.FPrev := FPrev;
  if (FPrev = nil) and (FParent <> nil) then
    FParent.FChild := FNext;
  FParent := nil;
  FPrev := nil;
  FNext := nil;
  Result := Self;
end;

function TdxSpreadSheetFormulaToken.ExtractColumn(const AIndex: Integer; var AErrorCode: TdxSpreadSheetFormulaErrorCode): TdxSpreadSheetVector;
begin
  Result := ExtractValueAsVector(AErrorCode);
end;

function TdxSpreadSheetFormulaToken.ExtractRow(const AIndex: Integer; var AErrorCode: TdxSpreadSheetFormulaErrorCode): TdxSpreadSheetVector;
begin
  Result := ExtractValueAsVector(AErrorCode);
end;

function TdxSpreadSheetFormulaToken.ExtractValueAsVector(var AErrorCode: TdxSpreadSheetFormulaErrorCode): TdxSpreadSheetVector;
var
  AValue: Variant;
  AValueErrorCode: TdxSpreadSheetFormulaErrorCode;
begin
  AErrorCode := ecNone;
  Result := TdxSpreadSheetVector.Create;
  GetValue(AValue, AValueErrorCode);
  Result.Add(TdxSpreadSheetVectorValue.Create(AValue, AValueErrorCode));
end;

function TdxSpreadSheetFormulaToken.ForEach(AProc: TdxSpreadSheetForEachCallBack;
  const AData: Pointer; var AErrorCode: TdxSpreadSheetFormulaErrorCode): Boolean;
var
  AValue: Variant;
begin
  GetValue(AValue, AErrorCode);
  Result := AProc(AValue, CanConvertStrToNumber, AErrorCode, AData, nil) and (AErrorCode = ecNone);
end;

function TdxSpreadSheetFormulaToken.ForEachCell(ASheet: TdxSpreadSheetTableView;
  AStartColumn, AStartRow, AFinishColumn, AFinishRow: Integer; AProc: TdxSpreadSheetForEachCallBack;
  const AData: Pointer; var AErrorCode: TdxSpreadSheetFormulaErrorCode): Boolean;
var
  AColumn, ARow: Integer;
  AValue: Variant;
  ACellReference: TdxSpreadSheetCellReference;
begin
  Result := True;
  for ARow := AStartRow to AFinishRow do
  begin
    if not Result or (AErrorCode <> ecNone) then Break;
    for AColumn := AStartColumn to AFinishColumn do
    begin
      if not Result or (AErrorCode <> ecNone) then Break;
      GetCellValue(ASheet, ARow, AColumn, AValue, AErrorCode);
      ACellReference.RowIndex := ARow;
      ACellReference.ColumnIndex := AColumn;
      ACellReference.Sheet := ASheet;
      Result := AProc(AValue, CanConvertStrToNumber, AErrorCode, AData, @ACellReference);
    end;
  end;
end;

procedure TdxSpreadSheetFormulaToken.GetCellValue(ASheet: TdxSpreadSheetTableView; const ARow, AColumn: Integer;
  var AValue: Variant; var AErrorCode: TdxSpreadSheetFormulaErrorCode);
begin
  ASheet.GetCellValue(ARow, AColumn, AValue, AErrorCode);
end;

function TdxSpreadSheetFormulaToken.GetSheet: TdxSpreadSheetTableView;
begin
  Result := Owner.Sheet;
end;

function TdxSpreadSheetFormulaToken.CanConvertStrToNumber: Boolean;
begin
  Result := True;
end;

function TdxSpreadSheetFormulaToken.GetTokenPriority: Integer;
begin
  Result := -1;
end;

procedure TdxSpreadSheetFormulaToken.Offset(DY, DX: Integer);
begin
end;

procedure TdxSpreadSheetFormulaToken.CalculateDimension(var ADimension: TdxSpreadSheetFormulaTokenDimension;
  var AErrorCode: TdxSpreadSheetFormulaErrorCode);
begin
  ADimension.RowCount := 1;
  ADimension.ColumnCount := 1;
  AErrorCode := ecNone;
end;

procedure TdxSpreadSheetFormulaToken.ClearIsDimensionCalculated;
begin
  FIsDimensionCalculated := False;
end;

function TdxSpreadSheetFormulaToken.DoNameChanged(AName: TdxSpreadSheetDefinedName; const ANewName: TdxUnicodeString): Boolean;
var
  AChanged: Boolean;
  AToken: TdxSpreadSheetFormulaToken;
begin
  Result := False;
  AToken := FChild;
  while AToken <> nil do
  begin
    AChanged := AToken.DoNameChanged(AName, ANewName);
    Result := Result or AChanged;
    AToken := AToken.Next;
  end;
end;

function TdxSpreadSheetFormulaToken.GetDimension(var AErrorCode: TdxSpreadSheetFormulaErrorCode): TdxSpreadSheetFormulaTokenDimension;
begin
  if not FIsDimensionCalculated or IsObligatoryDimensionCalculate then
    CalculateDimension(FDimension, AErrorCode)
  else
    AErrorCode := ecNone;
  Result := FDimension;
  FIsDimensionCalculated := True;
  if (AErrorCode = ecNone) and (FDimension.RowCount = 0) then
    AErrorCode := ecValue;
end;

procedure TdxSpreadSheetFormulaToken.GetValue(
  var AValue: Variant; var AErrorCode: TdxSpreadSheetFormulaErrorCode);
var
  AItemResult: TdxSpreadSheetFormulaResult;
begin
  if Owner = nil then
  begin
    AErrorCode := ecValue;
    Exit;
  end;

  AItemResult := Owner.Calculate(FirstChild);
  try
    AValue := AItemResult.Value;
    AErrorCode := AItemResult.ErrorCode;
  finally
    AItemResult.Free;
  end;
end;

function TdxSpreadSheetFormulaToken.GetValueFromArray(ARowIndex, AColumnIndex: Integer;
  var AErrorCode: TdxSpreadSheetFormulaErrorCode): Variant;

  procedure CheckIndex(var AIndex: Integer; ACount: Integer);
  begin
    if (AIndex > ACount - 1) then
      if ACount = 1 then
        AIndex := 0
      else
        AErrorCode := ecNA;
  end;

var
  ADimension: TdxSpreadSheetFormulaTokenDimension;
begin
  Result := Null;
  if Self = nil then
    Exit;
  ADimension := GetDimension(AErrorCode);
  CheckIndex(ARowIndex, ADimension.RowCount);
  if AErrorCode <> ecNA then
    CheckIndex(AColumnIndex, ADimension.ColumnCount);
  if AErrorCode <> ecNA then
    GetValueAsArrayItem(ARowIndex, AColumnIndex, Result, AErrorCode);
end;

procedure TdxSpreadSheetFormulaToken.GetValueAsArrayItem(const ARow, AColumn: Integer;
  var AValue: Variant; var AErrorCode: TdxSpreadSheetFormulaErrorCode);
begin
  GetValue(AValue, AErrorCode);
end;

procedure TdxSpreadSheetFormulaToken.GetValueRelatedWithCell(ACell: TdxSpreadSheetCell;
  var AValue: Variant; var AErrorCode: TdxSpreadSheetFormulaErrorCode);
begin
  GetValue(AValue, AErrorCode);
end;

function TdxSpreadSheetFormulaToken.IsEnumeration: Boolean;
begin
  Result := False;
end;

function TdxSpreadSheetFormulaToken.IsObligatoryDimensionCalculate: Boolean;
begin
  Result := False;
end;

procedure TdxSpreadSheetFormulaToken.SetNext(ANextToken: TdxSpreadSheetFormulaToken);
begin
  FNext := ANextToken;
end;

procedure TdxSpreadSheetFormulaToken.ToString(var AAsText: TdxSpreadSheetFormulaToken);
begin
  if FChild = nil then
    AttachString(AAsText, '')
  else
    AttachString(AAsText, Owner.GetExpressionAsText(FChild));
end;

procedure TdxSpreadSheetFormulaToken.UpdateReferences(AView: TdxSpreadSheetTableView; const AArea: TRect;
  AModification: TdxSpreadSheetCellsModification; AIsDeletion: Boolean; var AModified: Boolean);
var
  AChild: TdxSpreadSheetFormulaToken;
begin
  AChild := FirstChild;
  while AChild <> nil do
  begin
    AChild.UpdateReferences(AView, AArea, AModification, AIsDeletion, AModified);
    AChild := AChild.Next;
  end;
end;

function TdxSpreadSheetFormulaToken.GetChildCount: Integer;
var
  AToken: TdxSpreadSheetFormulaToken;
begin
  Result := 0;
  AToken := FChild;
  while AToken <> nil do
  begin
    Inc(Result);
    AToken := AToken.FNext;
  end;
end;

function TdxSpreadSheetFormulaToken.GetFirstSibling: TdxSpreadSheetFormulaToken;
begin
  Result := Self;
  while Result.Prev <> nil do
    Result := Result.Prev;
end;

function TdxSpreadSheetFormulaToken.GetFormatSettings: TdxSpreadSheetFormatSettings;
begin
  Result := Owner.FormatSettings;
end;

function TdxSpreadSheetFormulaToken.GetItem(AIndex: Integer): TdxSpreadSheetFormulaToken;
begin
  if AIndex < 0 then
    Result := nil
  else
  begin
    Result := FChild;
    while (AIndex > 0) and (Result <> nil) do
    begin
      Result := Result.Next;
      Dec(AIndex);
    end;
  end; 
end;

function TdxSpreadSheetFormulaToken.GetLastChild: TdxSpreadSheetFormulaToken;
begin
  Result := FChild;
  if Result = nil then Exit;
  while Result.FNext <> nil do
    Result := Result.FNext;
end;

function TdxSpreadSheetFormulaToken.GetHasChildren: Boolean;
begin
  Result := FChild <> nil; 
end;

function TdxSpreadSheetFormulaToken.GetID: Integer;
begin
  Result := dxSpreadSheetFormulaTokensRepository.IndexOf(ClassType);
end;

function TdxSpreadSheetFormulaToken.GetLastSibling: TdxSpreadSheetFormulaToken;
begin
  if Self = nil then
    Result := nil
  else
  begin
    Result := Self;
    while Result.FNext <> nil do
      Result := Result.FNext;
  end;
end;

function TdxSpreadSheetFormulaToken.GetSiblingCount: Integer;
var
  AItem: TdxSpreadSheetFormulaToken;
begin
  AItem := FirstSibling;
  Result := 0;
  while AItem <> nil do
  begin
    Inc(Result);
    AItem := AItem.Next;
  end;
end;

function TdxSpreadSheetFormulaToken.GetSpreadSheet: TdxCustomSpreadSheet;
begin
  Result := Owner.Controller.SpreadSheet;
end;

{ TdxSpreadSheetFormulaFormattedText }

constructor TdxSpreadSheetFormulaFormattedText.Create(const AValue: TdxUnicodeString = '');
begin
  inherited Create;
  FValue := AValue;
  FRuns := TdxSpreadSheetFormattedSharedStringRuns.Create;
end;

destructor TdxSpreadSheetFormulaFormattedText.Destroy;
begin
  FreeAndNil(FRuns);
  inherited Destroy;
end;

procedure TdxSpreadSheetFormulaFormattedText.Add(const AValue: TdxSpreadSheetFormulaFormattedText);
begin
  try
    AValue.Runs.Offset(Length(Value));
    Runs.Append(AValue.Runs);
    FValue := FValue + AValue.Value;
  finally
    AValue.Free;
  end;
end;

procedure TdxSpreadSheetFormulaFormattedText.Add(const AValue: TdxUnicodeString);
begin
  FValue := Value + AValue;
end;

procedure TdxSpreadSheetFormulaFormattedText.AddInBeginning(const AValue: TdxUnicodeString);
begin
  Runs.Offset(Length(AValue));
  FValue := AValue + Value;
end;

{ TdxSpreadSheetFormulaResult }

constructor TdxSpreadSheetFormulaResult.Create(AOwner: TdxSpreadSheetFormula);
begin
  inherited Create;
  FOwner := AOwner;
  FValues := TList.Create;
  FValues.Capacity := 128;
end;

destructor TdxSpreadSheetFormulaResult.Destroy;
begin
  Clear;
  FreeAndNil(FValues);
  inherited Destroy;
end;

procedure TdxSpreadSheetFormulaResult.BeforeDestruction;
begin
  inherited BeforeDestruction;
  if Owner <> nil then
  begin
    if Owner.FResultValue = Self then
      Owner.FResultValue := nil;
  end;
end;

procedure TdxSpreadSheetFormulaResult.Add(AValue: TdxSpreadSheetFormulaToken);
begin
  FValues.Add(AValue);
end;

procedure TdxSpreadSheetFormulaResult.AddValue(const AValue: Variant);
begin
  FValues.Add(TdxSpreadSheetFormulaVariantToken.Create(AValue));
end;

procedure TdxSpreadSheetFormulaResult.AddResultValue(const AValue: TdxSpreadSheetFormulaResult);
begin
  if AValue = nil then
    SetError(ecValue)
  else
    try
      if AValue.Validate then
      begin
        dxAppendList(AValue.Values, Values);
        AValue.Values.Clear;
      end
      else
        SetError(AValue.ErrorCode)
    finally
      AValue.Free;
    end;
end;

procedure TdxSpreadSheetFormulaResult.CheckValue;
begin
  if Values.Count > 0 then
  begin
    if not TdxSpreadSheetFormulaToken(Values.Last).IsEnumeration then
      GetValue;
  end
  else
    GetValue;
end;

procedure TdxSpreadSheetFormulaResult.Clear;
begin
  FErrorCode := ecNone;
  ClearValues;
end;

procedure TdxSpreadSheetFormulaResult.ClearValues;
var
  I: Integer;
begin
  try
    for I := 0 to Count - 1 do
      if Items[I].Owner = nil then
        Items[I].Free;
  finally
    Values.Clear;
  end;
end;

function TdxSpreadSheetFormulaResult.ConvertToNumeric(var AValue: Variant; ACanConvertStr, AWithoutBoolean: Boolean): Boolean;
var
  ANumeric: Double;
begin
  if VarIsStr(AValue) and not ACanConvertStr then
    SetError(ecValue)
  else
    if dxSpreadSheetIsEmptyValue(AValue) then
      AValue := 0
    else
      if VarType(AValue) = varBoolean then
        if AWithoutBoolean then
          SetError(ecValue)
        else
          AValue := Integer(AValue = True)
      else
        if VarIsStr(AValue) and TryStrToFloat(AValue, ANumeric) then
          AValue := ANumeric
        else
          if not VarIsNumeric(AValue) and not VarIsDate(AValue) then
            SetError(ecValue);
  Result := Validate;
end;

function TdxSpreadSheetFormulaResult.ExtractCondition(AParams: TdxSpreadSheetFormulaToken;
  ACount, AIndex: Integer; var AOperation: TdxSpreadSheetFormulaOperation): Variant;
var
  ARes: TdxSpreadSheetFormulaResult;
  AParamCount: Integer;
begin
  Result := Null;
  AParamCount := GetParamsCount(AParams);
  if (AParamCount <= AIndex) or (ACount > 0) and (AParamCount > ACount) then
    SetError(ecValue);
  while (AParams <> nil) and (AIndex > 0) do
  begin
    AParams := AParams.Next;
    Dec(AIndex);
  end;
  if AParams <> nil then
    AParams := AParams.FirstChild;
  if AParams = nil then
    SetError(ecValue);
  if ErrorCode <> ecNone then Exit;
  AOperation := opEQ;
  ARes := Owner.Calculate(AParams);
  try
    Result := VarToStr(ARes.Value);
    SetError(ARes.ErrorCode);
    if ErrorCode = ecNone then
      Result := dxSpreadSheetExtractOperation(Result, AOperation, Owner.FormatSettings);
  finally
    ARes.Free;
  end;
end;

function TdxSpreadSheetFormulaResult.ExtractColumnFromRange(const ARange: TdxSpreadSheetFormulaToken;
  AColumnIndex: Integer; var AErrorCode: TdxSpreadSheetFormulaErrorCode): TdxSpreadSheetVector;
begin
  AErrorCode := ecNone;
  Result := ARange.ExtractColumn(AColumnIndex, AErrorCode);
  if AErrorCode <> ecNone then
    SetError(AErrorCode);
end;

function TdxSpreadSheetFormulaResult.ExtractRowFromRange(const ARange: TdxSpreadSheetFormulaToken;
  ARowIndex: Integer; var AErrorCode: TdxSpreadSheetFormulaErrorCode): TdxSpreadSheetVector;
begin
  AErrorCode := ecNone;
  Result := ARange.ExtractRow(ARowIndex, AErrorCode);
  if AErrorCode <> ecNone then
    SetError(AErrorCode);
end;

function TdxSpreadSheetFormulaResult.GetCount: Integer;
begin
  Result := FValues.Count;
end;

function TdxSpreadSheetFormulaResult.GetFormatSettings: TdxSpreadSheetFormatSettings;
begin
  Result := Owner.FormatSettings;
end;

function TdxSpreadSheetFormulaResult.GetItem(AIndex: Integer): TdxSpreadSheetFormulaToken;
begin
  Result := TdxSpreadSheetFormulaToken(FValues[AIndex]);
end;

function TdxSpreadSheetFormulaResult.GetValue: Variant;
begin
  if Validate and (Values.Count > 0) then
  begin
    TdxSpreadSheetFormulaToken(Values.Last).GetValueRelatedWithCell(Owner.Cell, Result, FErrorCode);
    if Validate then
      Exit;
  end;
  if Validate then
    FErrorCode := ecValue;
  case FErrorCode of
    ecNull:
      Result := cxGetResourceString(@serNullError);
    ecDivByZero:
      Result := cxGetResourceString(@serDivZeroError);
    ecValue:
      Result := cxGetResourceString(@serValueError);
    ecRefErr:
      Result := cxGetResourceString(@serRefError);
    ecNUM:
      Result := cxGetResourceString(@serNumError);
    ecName:
      Result := cxGetResourceString(@serNameError);
    ecNA:
      Result := cxGetResourceString(@serNAError);
  else
  end;
end;

function TdxSpreadSheetFormulaResult.DoExtractDateTimeParameter(var AParameter: Variant;
  const AParams: TdxSpreadSheetFormulaToken; AIndex: Integer; AWithoutBoolean: Boolean): Boolean;
var
  AIsNumeric: Boolean;
begin
  if AWithoutBoolean then
    AIsNumeric := ExtractNumericParameterWithoutBoolean(AParameter, AParams, AIndex)
  else
    AIsNumeric := ExtractNumericParameter(AParameter, AParams, AIndex);

  if AIsNumeric then
  begin
    if AParameter < 0 then
      SetError(ecNUM);
    Result := Validate;
  end
  else
  begin
    SetError(ecNone);
    Result := ExtractDateTimeOnlyParameter(AParameter, AParams, AIndex);
  end;
end;

function TdxSpreadSheetFormulaResult.ExtractDateTimeOnlyParameter(var AParameter: Variant;
  const AParams: TdxSpreadSheetFormulaToken; AIndex: Integer = 0): Boolean;
var
  ADate: TDateTime;
  AFloat: Double;
begin
  if ExtractParameter(AParameter, AParams, AIndex) then
    if VarIsStr(AParameter) and TryStrToFloat(VarToStr(AParameter), AFloat) then
      SetError(ecValue)
    else
      if not dxConvertToXLSDate(AParameter, ADate) then
        SetError(ecValue)
      else
        AParameter := ADate;
  Result := Validate;
end;

function TdxSpreadSheetFormulaResult.ExtractDateTimeParameter(var AParameter: Variant;
  const AParams: TdxSpreadSheetFormulaToken; AIndex: Integer = 0): Boolean;
begin
  Result := DoExtractDateTimeParameter(AParameter, AParams, AIndex, False);
end;

function TdxSpreadSheetFormulaResult.ExtractDateTimeParameterWithoutBoolean(var AParameter: Variant;
  const AParams: TdxSpreadSheetFormulaToken; AIndex: Integer = 0): Boolean;
begin
  Result := DoExtractDateTimeParameter(AParameter, AParams, AIndex, True);
end;

function TdxSpreadSheetFormulaResult.ExtractErrorCode(const AParams: TdxSpreadSheetFormulaToken): TdxSpreadSheetFormulaErrorCode;
var
  AParamsResult: TdxSpreadSheetFormulaResult;
begin
  AParamsResult := Owner.Calculate(AParams);
  Result := AParamsResult.ErrorCode;
  AParamsResult.Free;
end;

function TdxSpreadSheetFormulaResult.ExtractNumericValue(var AValue: Variant): Boolean;
var
  AToken: TdxSpreadSheetFormulaToken;
  AIntValue: Integer;
  AFloatValue: Double;
  ACanConvertStrToNumber: Boolean;
begin
  Result := True;
  AToken := ExtractValueToken;
  AValue := Null;
  if AToken <> nil then
  begin
    ACanConvertStrToNumber := AToken.CanConvertStrToNumber;
    AToken.GetValue(AValue, FErrorCode);
    if (FErrorCode = ecNone) and VarIsStr(AValue) then
    begin
      if ACanConvertStrToNumber then
        SetError(ecValue)
      else
        if TryStrToInt(AValue, AIntValue) then
          AValue := AIntValue
        else
           if dxTryStrToNumeric(AValue, AFloatValue) then
             AValue := AFloatValue
           else
             SetError(ecValue)
    end
    else
      if ErrorCode = ecNone then
        Result := ConvertToNumeric(AValue, ACanConvertStrToNumber, False);
  end;
  Result := Result and (ErrorCode = ecNone);
end;

function TdxSpreadSheetFormulaResult.ExtractNumericParameter(var AParameter: Variant;
  const AParams: TdxSpreadSheetFormulaToken; AIndex: Integer = 0): Boolean;
var
  ADate: TDateTime;
  AValue: Variant;
  ACanConvertStrToNumber: Boolean;
begin
  ExtractParameter(AParameter, ACanConvertStrToNumber, AParams, AIndex);
  AValue := AParameter;
  Result := ConvertToNumeric(AParameter, ACanConvertStrToNumber, False);
  if not Result and dxConvertToXLSDate(AValue, ADate) then
  begin
    Result := True;
    SetError(ecNone);
    AParameter := ADate;
  end;
end;

function TdxSpreadSheetFormulaResult.ExtractNumericParameterDef(var AParameter: Variant;
  const ADefaultIfNoExist, ADefaultIfNull: Variant; const AParams: TdxSpreadSheetFormulaToken; AIndex: Integer = 0): Boolean;
begin
  Result := True;
  if not ParameterExists(AParams, AIndex) then
    AParameter := ADefaultIfNoExist
  else
    Result := ExtractNumericParameterDef(AParameter, ADefaultIfNull, AParams, AIndex);
end;

function TdxSpreadSheetFormulaResult.ExtractNumericParameterDef(var AParameter: Variant;
  const ADefaultIfNull: Variant; const AParams: TdxSpreadSheetFormulaToken; AIndex: Integer = 0): Boolean;
var
  ACanConvertStrToNumber: Boolean;
begin
  Result := ParameterIsNull(AParameter, ACanConvertStrToNumber, AParams, AIndex);
  if Result then
    AParameter := ADefaultIfNull
  else
    Result := ConvertToNumeric(AParameter, ACanConvertStrToNumber, False);
end;

function TdxSpreadSheetFormulaResult.ExtractNumericParameterDefWithoutBoolean(var AParameter: Variant;
  const ADefault: Variant; const AParams: TdxSpreadSheetFormulaToken; AIndex: Integer = 0): Boolean;
var
  ACanConvertStrToNumber: Boolean;
begin
  Result := ParameterIsNull(AParameter, ACanConvertStrToNumber, AParams, AIndex);
  if Result then
    AParameter := ADefault
  else
    Result := ConvertToNumeric(AParameter, ACanConvertStrToNumber, True);
end;

function TdxSpreadSheetFormulaResult.ExtractNumericParameterWithoutBoolean(var AParameter: Variant;
  const AParams: TdxSpreadSheetFormulaToken; AIndex: Integer = 0): Boolean;
var
  ACanConvertStrToNumber: Boolean;
begin
  Result := ExtractParameter(AParameter, ACanConvertStrToNumber, AParams, AIndex) and
    ConvertToNumeric(AParameter, ACanConvertStrToNumber, True);
end;

function TdxSpreadSheetFormulaResult.ExtractParameter(var AParameter: Variant; const AParams: TdxSpreadSheetFormulaToken;
  AIndex: Integer = 0): Boolean;
var
  ACanConvertStrToNumber: Boolean;
begin
  Result := ExtractParameter(AParameter, ACanConvertStrToNumber, AParams, AIndex);
end;

function TdxSpreadSheetFormulaResult.ExtractParameter(var AParameter: Variant; out ACanConvertStrToNumber: Boolean;
  const AParams: TdxSpreadSheetFormulaToken; AIndex: Integer = 0): Boolean;
var
  AItemIndex: Integer;
  AItem: TdxSpreadSheetFormulaToken;
  AItemResult: TdxSpreadSheetFormulaResult;
begin
  AParameter := Null;
  ACanConvertStrToNumber := False;
  AItemIndex := 0;
  AItem := AParams;
  while AItem <> nil do
  begin
    if AItemIndex = AIndex then
    begin
      AItemResult := Owner.Calculate(AItem.FirstChild);
      try
        if not AItemResult.Validate then
          SetError(AItemResult.ErrorCode)
        else
        begin
          AParameter := AItemResult.Value;
          ACanConvertStrToNumber := AItem.FirstChild.CanConvertStrToNumber;
        end;
      finally
        AItemResult.Free;
      end;
      Break;
    end;
    Inc(AItemIndex);
    AItem := AItem.Next;
  end;
  Result := Validate;
end;

function TdxSpreadSheetFormulaResult.ExtractParameterDef(var AParameter: Variant; ADefaultIfNull: Variant;
  const AParams: TdxSpreadSheetFormulaToken; AIndex: Integer = 0): Boolean;
begin
  if ParameterIsNull(AParameter, AParams, AIndex) then
    AParameter := ADefaultIfNull;
  Result := Validate;
end;

function TdxSpreadSheetFormulaResult.ExtractParameterDef(var AParameter: Variant; ADefaultIfNoExist, ADefaultIfNull: Variant;
  const AParams: TdxSpreadSheetFormulaToken; AIndex: Integer = 0): Boolean;
begin
  AParameter := ADefaultIfNoExist;
  if ParameterExists(AParams, AIndex) then
    ExtractParameterDef(AParameter, ADefaultIfNull, AParams, AIndex);
  Result := Validate;
end;

function TdxSpreadSheetFormulaResult.ExtractStringValue: Variant;
var
  ACanConvertStrToNumber: Boolean;
begin
  Result := VarToStr(ExtractValue(ACanConvertStrToNumber));
end;

function TdxSpreadSheetFormulaResult.ExtractStringParameter(const AParams: TdxSpreadSheetFormulaToken; AIndex: Integer = 0): Variant;
var
  ACanConvertStrToNumber: Boolean;
begin
  Result := Null;
  if ExtractParameter(Result, ACanConvertStrToNumber, AParams, AIndex) then
    Result := VarToStr(Result);
end;

function TdxSpreadSheetFormulaResult.ExtractValue(out ACanConvertStrToNumber: Boolean): Variant;
var
  AValue: TdxSpreadSheetFormulaToken;
begin
  AValue := ExtractValueToken;
  ACanConvertStrToNumber := False;
  if AValue <> nil then
  begin
    ACanConvertStrToNumber := AValue.CanConvertStrToNumber;
    AValue.GetValueRelatedWithCell(Owner.Cell, Result, FErrorCode);
    if AValue.Owner = nil then
      AValue.Free;
  end;
end;

function TdxSpreadSheetFormulaResult.ExtractValueToken: TdxSpreadSheetFormulaToken;
begin
  Result := nil;
  if Validate then
  begin
    if Values.Count > 0 then
    begin
      Result := TdxSpreadSheetFormulaToken(Values.Last);
      Values.Count := Values.Count - 1;
    end
    else
      Result := TdxSpreadSheetFormulaNullToken.Create;
  end
  else
    if FErrorCode = ecNone then
      SetError(ecValue);
end;

procedure TdxSpreadSheetFormulaResult.ForEach(AParams: TdxSpreadSheetFormulaToken; AProc: TdxSpreadSheetForEachCallBack;
  const AData: Pointer);
var
  I: Integer;
  AItemResult: TdxSpreadSheetFormulaResult;
  AToken: TdxSpreadSheetFormulaToken;
  AErrorCode: TdxSpreadSheetFormulaErrorCode;
begin
  if not Validate then
    Exit;
  AToken := AParams.FirstChild;
  if (AToken <> nil) and (AToken.SiblingCount = 1) and AToken.IsEnumeration then
    AToken.ForEach(AProc, AData, FErrorCode)
  else
  begin
    AItemResult := Owner.Calculate(AToken);
    try
      if (AItemResult.Values.Count = 1) and TdxSpreadSheetFormulaToken(AItemResult.Values[0]).IsEnumeration then
      begin
        if AItemResult.ErrorCode = ecNull then
        begin
          AErrorCode := AItemResult.ErrorCode;
          AProc(Unassigned, False, AErrorCode, AData, nil);
        end
        else
          TdxSpreadSheetFormulaToken(AItemResult.Values[0]).ForEach(AProc, AData, FErrorCode);
      end
      else
      begin
        if not AItemResult.Validate then
        begin
          AItemResult.ClearValues;
          AItemResult.Add(TdxSpreadSheetFormulaErrorValueToken.Create(AItemResult.ErrorCode));
        end;
        AErrorCode := AItemResult.ErrorCode;
        for I := 0 to AItemResult.Count - 1 do
          if not AItemResult[I].ForEach(AProc, AData, AErrorCode) then
            Break;
        if AErrorCode <> ecNone then
          SetError(AErrorCode);
      end;
    finally
      AItemResult.Free;
    end;
  end;
end;

function TdxSpreadSheetFormulaResult.GetParamsCount(const AParams: TdxSpreadSheetFormulaToken): Integer;
var
  AParam: TdxSpreadSheetFormulaToken;
begin
  if AParams = nil then
    Result := 0
  else
  if AParams.Parent <> nil then
    Result := AParams.Parent.ChildCount
  else
    begin
      Result := 1;
      AParam := AParams.Next;
      while AParam <> nil do
      begin
        Inc(Result);
        AParam := AParam.Next;
      end;
      AParam := AParams.Prev;
      while AParam <> nil do
      begin
        Inc(Result);
        AParam := AParam.Prev;
      end;
    end;
end;

function TdxSpreadSheetFormulaResult.ParameterExists(const AParams: TdxSpreadSheetFormulaToken; AIndex: Integer): Boolean;
begin
  Result := (AParams <> nil) and (GetParamsCount(AParams) - 1 >= AIndex);
end;

function TdxSpreadSheetFormulaResult.ParameterIsNull(var AParameter: Variant;
  const AParams: TdxSpreadSheetFormulaToken; AIndex: Integer = 0): Boolean;
begin
  Result := ExtractParameter(AParameter, AParams, AIndex) and VarIsNull(AParameter);
end;

function TdxSpreadSheetFormulaResult.ParameterIsNull(var AParameter: Variant; out ACanConvertStrToNumber: Boolean;
  const AParams: TdxSpreadSheetFormulaToken; AIndex: Integer = 0): Boolean;
begin
  Result := ExtractParameter(AParameter, ACanConvertStrToNumber, AParams, AIndex) and VarIsNull(AParameter);
end;

procedure TdxSpreadSheetFormulaResult.SetError(ACode: TdxSpreadSheetFormulaErrorCode);
begin
  FErrorCode := ACode;
end;

function TdxSpreadSheetFormulaResult.Validate: Boolean;
begin
  Result := FErrorCode = ecNone;
end;

{ TdxSpreadSheetFormulaAsTextInfo }

constructor TdxSpreadSheetFormulaAsTextInfo.Create(AFormula: TdxSpreadSheetFormula);
begin
  inherited Create;
  if AFormula <> nil then
  begin
    Formula := AFormula.AsText;
    Anchor := Point(AFormula.Cell.ColumnIndex, AFormula.Cell.RowIndex);
    IsArray := AFormula.IsArrayFormula;
  end;
end;

procedure TdxSpreadSheetFormulaAsTextInfo.LoadFromStream(AReader: TcxReader);
begin
  Anchor :=  AReader.ReadPoint;
  Formula := AReader.ReadWideString;
  IsArray := AReader.ReadBoolean;
  IsShared := AReader.ReadBoolean;
  Reference := AReader.ReadRect;
end;

procedure TdxSpreadSheetFormulaAsTextInfo.SaveToStream(AWriter: TcxWriter);
begin
  AWriter.WritePoint(Anchor);
  AWriter.WriteWideString(Formula);
  AWriter.WriteBoolean(IsArray);
  AWriter.WriteBoolean(IsShared);
  AWriter.WriteRect(Reference);
end;

{ TdxSpreadSheetFormulaAsTextInfoList }

constructor TdxSpreadSheetFormulaAsTextInfoList.Create(ASpreadSheet: TdxCustomSpreadSheet);
begin
  inherited Create(True);
  FSpreadSheet := ASpreadSheet;
end;

procedure TdxSpreadSheetFormulaAsTextInfoList.Add(ACell: TdxSpreadSheetCell;
  const AFormula: TdxUnicodeString; AIsArray, AIsShared: Boolean; const AReference: TRect);
var
  AInfo: TdxSpreadSheetFormulaAsTextInfo;
begin
  AInfo := TdxSpreadSheetFormulaAsTextInfo.Create;
  AInfo.Cell := ACell;
  AInfo.Formula := AFormula;
  AInfo.IsArray := AIsArray;
  AInfo.IsShared := AIsShared;
  AInfo.Reference := AReference;
  AInfo.Anchor := cxPoint(ACell.ColumnIndex, ACell.RowIndex);
  Add(AInfo);
end;

procedure TdxSpreadSheetFormulaAsTextInfoList.AddFromStream(ACell: TdxSpreadSheetCell; AReader: TcxReader);
var
  AInfo: TdxSpreadSheetFormulaAsTextInfo;
begin
  AInfo := TdxSpreadSheetFormulaAsTextInfo.Create;
  AInfo.Cell := ACell;
  AInfo.LoadFromStream(AReader);
  Add(AInfo);
end;

function TdxSpreadSheetFormulaAsTextInfoList.HasCell(ACell: TdxSpreadSheetCell): Boolean;
var
  I: Integer;
begin
  Result := False;
  for I := 0 to Count - 1 do
    if Items[I].Cell = ACell then
      Exit(True);
end;

procedure TdxSpreadSheetFormulaAsTextInfoList.ResolveReferences;
var
  AColumn: Integer;
  AInfo: TdxSpreadSheetFormulaAsTextInfo;
  AParser: TdxSpreadSheetFormulaParser;
  ARow: Integer;
  I: Integer;
begin
  AParser := CreateParser as TdxSpreadSheetFormulaParser;
  try
    for I := 0 to Count - 1 do
    begin
      AInfo := Items[I];
      if AInfo.Formula = '' then Continue;
      if AParser.ParseFormula(AInfo.Formula, AInfo.Cell) then
      begin
        AInfo.Cell.AsFormula.Offset(AInfo.Cell.RowIndex - AInfo.Anchor.Y, AInfo.Cell.ColumnIndex - AInfo.Anchor.X);
        AInfo.Cell.AsFormula.IsArrayFormula := AInfo.IsArray;
        if AInfo.IsShared then
        begin
          for ARow := AInfo.Reference.Top to AInfo.Reference.Bottom do
            for AColumn := AInfo.Reference.Left to AInfo.Reference.Right do
              if (ARow <> AInfo.Cell.RowIndex) or (AColumn <> AInfo.Cell.ColumnIndex) then
                AInfo.Cell.View.CreateCell(ARow, AColumn).AsFormula := AInfo.Cell.AsFormula.Clone;
        end;
      end
      else
        raise EdxSpreadSheetError.CreateFmt(sdxErrorInvalidFormula, [AInfo.Formula]);
    end;
  finally
    AParser.Free;
  end;
end;

function TdxSpreadSheetFormulaAsTextInfoList.CreateParser: TObject;
begin
  Result := TdxSpreadSheetFormulaParser.Create(SpreadSheet);
end;

{ TdxSpreadSheetDefinedNameFormula }

constructor TdxSpreadSheetDefinedNameFormula.Create(AOwner: TdxSpreadSheetDefinedName);
begin
  FOwner := AOwner; 
end;

function TdxSpreadSheetDefinedNameFormula.GetColumn: Integer;
begin
  Result := 0;
end;

function TdxSpreadSheetDefinedNameFormula.GetController: TdxSpreadSheetFormulaController;
begin
  Result := Owner.SpreadSheet.FormulaController;
end;

function TdxSpreadSheetDefinedNameFormula.GetRow: Integer;
begin
  Result := 0;
end;

function TdxSpreadSheetDefinedNameFormula.GetSheet: TdxSpreadSheetTableView;
begin
  Result := nil;
end;

procedure TdxSpreadSheetDefinedNameFormula.RemoveReferenceFromCell;
begin
end;

{ TdxSpreadSheetFormula }

constructor TdxSpreadSheetFormula.Create(AOwner: TdxSpreadSheetCell);
begin
  FOwner := AOwner;
  if Cell <> nil then
    Controller.Add(Self);
end;

destructor TdxSpreadSheetFormula.Destroy;
begin
  if Controller <> nil then // if in the destroy section
    Controller.Remove(Self);
  if Cell <> nil then
    Cell.View.RemoveFromArrayTokenList(Self);
  ClearResult;
  DestroyTokens(FTokens);
  RemoveReferenceFromCell;
  inherited Destroy;
end;

function TdxSpreadSheetFormula.Clone: TdxSpreadSheetFormula;
var
  AParser: TdxSpreadSheetFormulaParser;
begin
  AParser := TdxSpreadSheetFormulaParser.Create(Sheet.SpreadSheet);
  try
    Result := TdxSpreadSheetFormula.Create(nil);
    Result.FOwner := Cell; 
    AParser.ParseFormula(AsText, Result);
    Result.FOwner := nil; // # T138686: relative references incorrect resolves after clone becasue anchor is lost
    Result.FErrorIndex := ErrorIndex;
  finally
    AParser.Free;
  end;
end;

procedure TdxSpreadSheetFormula.EnumReferences(AProc: TdxSpreadSheetFormulaEnumReferencesProc);
var
  AToken: TdxSpreadSheetFormulaToken;
begin
  AToken := FTokens;
  while AToken <> nil do
  begin
    AToken.EnumReferences(AProc);
    AToken := AToken.Next;
  end;
end;

procedure TdxSpreadSheetFormula.Add(APrev, AToken: TdxSpreadSheetFormulaToken);
begin
  if APrev.FNext <> nil then
  begin
    AToken.FNext := APrev.FNext;
    AToken.FNext.FPrev := AToken;
  end;
  APrev.FNext := AToken;
  AToken.FPrev := APrev;
  while AToken <> nil do
  begin
    AToken.FParent := APrev.Parent;
    AToken := AToken.FNext;
  end;
end;

procedure TdxSpreadSheetFormula.AddLast(APrev, AToken: TdxSpreadSheetFormulaToken);
begin
  while APrev.Next <> nil do
    APrev := APrev.Next;
  Add(APrev, AToken);
end;

procedure TdxSpreadSheetFormula.AddChild(AParent, AToken: TdxSpreadSheetFormulaToken);
var
  ALast: TdxSpreadSheetFormulaToken;
begin
  if AParent.FChild = nil then
    AParent.FChild := AToken
  else
  begin
    ALast := AParent.FChild;
    while ALast.Next <> nil do
      ALast := ALast.Next;
    ALast.FNext := AToken;
    AToken.FPrev := ALast;
  end;
  while AToken <> nil do
  begin
    AToken.FParent := AParent;
    AToken := AToken.Next;
  end;
end;

function TdxSpreadSheetFormula.Calculate(AExpression: TdxSpreadSheetFormulaToken): TdxSpreadSheetFormulaResult;
begin
  Result := TdxSpreadSheetFormulaResult.Create(Self);
  try
    if AExpression = nil then
    begin
      Result.SetError(FErrorCode);
      Exit;
    end;
    while (AExpression <> nil) and Result.Validate do
    begin
      AExpression.Calculate(Result);
      AExpression := AExpression.Next;
    end;
    Result.CheckValue;
  except
  end;
end;

procedure TdxSpreadSheetFormula.ClearResult;
begin
  if Cell <> nil then
    Cell.DisplayValueIsDirty := True;
  FIteration := 0;
  FreeAndNil(FResultValue);
end;

procedure TdxSpreadSheetFormula.DestroyTokens(var ATokens: TdxSpreadSheetFormulaToken);
var
  AToken, ANext: TdxSpreadSheetFormulaToken;
begin
  AToken := ATokens;
  ATokens := nil;
  while AToken <> nil do
  begin
    ANext := AToken.Next;
    AToken.Free;
    AToken := ANext;
  end;
end;

procedure TdxSpreadSheetFormula.DoNameChanged(AName: TdxSpreadSheetDefinedName; const ANewName: TdxUnicodeString);
begin
  if Tokens = nil then Exit;
  if Tokens.DoNameChanged(AName, ANewName) and (Cell <> nil) then
    Cell.DisplayValueIsDirty := True;
end;

procedure TdxSpreadSheetFormula.RemoveReferenceFromCell;
begin
  if Cell.IsFormula and (Cell.AsFormula = Self) then
    Cell.FDataType := cdtBlank;
end;

function TdxSpreadSheetFormula.GetColumn: Integer;
begin
  Result := Cell.ColumnIndex;
end;

function TdxSpreadSheetFormula.GetController: TdxSpreadSheetFormulaController;
begin
  if Sheet <> nil then
    Result := Sheet.SpreadSheet.FormulaController
  else
    Result := nil;
end;

function TdxSpreadSheetFormula.GetMaxIterationCount: Integer;
begin
  if Controller.SpreadSheet.OptionsBehavior.IterativeCalculation then
    Result := Controller.SpreadSheet.OptionsBehavior.IterativeCalculationMaxCount + 1
  else
    Result := 1
end;

function TdxSpreadSheetFormula.GetExpressionAsText(AExpression: TdxSpreadSheetFormulaToken): TdxSpreadSheetFormulaFormattedText;
var
  AToken: TdxSpreadSheetFormulaToken;
begin
  if AExpression = nil then
    Exit(TdxSpreadSheetFormulaFormattedText.Create);

  AToken := TdxSpreadSheetFormulaToken.Create;
  try
    while AExpression <> nil do
    begin
      AExpression.ToString(AToken);
      AExpression := AExpression.Next;
    end;
    Result := AToken.ExtractLastTokenAsString(AToken);
  finally
    AToken.Free;
  end;
end;

function TdxSpreadSheetFormula.IsCaptionTextDelimited(ASheet: TdxSpreadSheetTableView): Boolean;
begin
  Result := (ASheet <> nil) and ASheet.IsCaptionTextDelimited;
end;

procedure TdxSpreadSheetFormula.Offset(DY, DX: Integer);

  procedure DoOffset(AToken: TdxSpreadSheetFormulaToken);
  begin
    while AToken <> nil do
    begin
      AToken.Offset(DY, DX);
      DoOffset(AToken.FirstChild);
      AToken := AToken.Next;
    end;
  end;

begin
  DoOffset(Tokens);
end;

procedure TdxSpreadSheetFormula.SetAsOwner(AToken: TdxSpreadSheetFormulaToken);
begin
  while AToken <> nil do
  begin
    AToken.FOwner := Self;
    SetAsOwner(AToken.FChild);
    AToken := AToken.Next;
  end;
end;

procedure TdxSpreadSheetFormula.SetOwner(ACell: TdxSpreadSheetCell);
begin
  if FOwner = nil then
  begin
    FOwner := ACell;
    Controller.Add(Self);
    FSourceText := AsText;
  end;
end;

function TdxSpreadSheetFormula.UpdateReferences(AView: TdxSpreadSheetTableView; const AArea: TRect;
  AModification: TdxSpreadSheetCellsModification; AIsDeletion: Boolean): Boolean;
begin
  Result := False;
  if Tokens = nil then Exit;
  Tokens.UpdateReferences(AView, AArea, AModification, AIsDeletion, Result);
end;

procedure TdxSpreadSheetFormula.LoadFromStream(AReader: TcxReader);
var
  APrevToken: TdxSpreadSheetFormulaToken;
  AToken: TdxSpreadSheetFormulaToken;
  I: Integer;
begin
  APrevToken := nil;
  FIsArrayFormula := AReader.ReadBoolean;
  for I := 0 to AReader.ReadInteger - 1 do
  begin
    AToken := dxSpreadSheetFormulaTokensRepository[AReader.ReadInteger].Create as TdxSpreadSheetFormulaToken;
    AToken.FOwner := Self;
    if I = 0 then
      FTokens := AToken
    else
      Add(APrevToken, AToken);
    AToken.LoadFromStream(AReader);
    APrevToken := AToken;
  end;
end;

procedure TdxSpreadSheetFormula.SetArrayFormulaSize(const ASize: TSize);
begin
  FArrayFormulaSize := ASize;
end;

procedure TdxSpreadSheetFormula.SetError(AErrorCode: TdxSpreadSheetFormulaErrorCode; AErrorIndex: Integer);
begin
  FErrorCode := AErrorCode;
  FErrorIndex := AErrorIndex;
end;

function TdxSpreadSheetFormula.GetResultValue: TdxSpreadSheetFormulaResult;
var
  AIteration: Integer;
begin
  Result := FResultValue;
  if not Controller.CalculationInProcess then Exit;
  if FResultValue = nil then
  begin
    AIteration := FIteration;
    Inc(FIteration);
    try
      if (Iteration > 1) and (Iteration >= GetMaxIterationCount) then
      begin
        Result := TdxSpreadSheetFormulaResult.Create(Self);
        Result.AddValue(0);
        Controller.CircularReference := Controller.CircularReference or (GetMaxIterationCount = 1)
      end
      else
        Result := Calculate(FTokens);

      if Controller.CircularReference then
      begin
        Result.Clear;
        Result.AddValue(0);
      end;
    finally
      if FResultValue <> nil then
        FreeAndNil(FResultValue);    
      FResultValue := Result;
      if AIteration = 0 then
      begin
        FIteration := 0;
        if IsArrayFormula and not cxSizeIsEmpty(ArrayFormulaSize) then
          Cell.View.PopulateSlaveCells(dxSpreadSheetArea(cxPoint(Cell.ColumnIndex, Cell.RowIndex), ArrayFormulaSize));
      end;
    end
  end
  else
    Result := FResultValue;
end;

function TdxSpreadSheetFormula.GetRow: Integer;
begin
  Result := Cell.RowIndex;
end;

function TdxSpreadSheetFormula.GetSheet: TdxSpreadSheetTableView;
begin
  if Cell = nil then
    Result := nil
  else
    Result := Cell.View;
end;

function TdxSpreadSheetFormula.GetValue: Variant;
begin
  if ResultValue = nil then
    Result := 0
  else
    Result := ResultValue.Value;
end;

function TdxSpreadSheetFormula.GetAsText: TdxUnicodeString;
var
  AFormattedText: TdxSpreadSheetFormulaFormattedText;
begin
  Result := FormatSettings.Operations[opEQ];
  if ErrorIndex <> 0 then
    Result := Result + FSourceText
  else
  begin
    AFormattedText := GetExpressionAsText(FTokens);
    if AFormattedText <> nil then
    try
      Result := Result + AFormattedText.Value;
    finally
      AFormattedText.Free;
    end;
  end;
end;

function TdxSpreadSheetFormula.GetFormatSettings: TdxSpreadSheetFormatSettings;
begin
  Result := Controller.FormatSettings;
end;

procedure TdxSpreadSheetFormula.SetIsArrayFormula(AValue: Boolean);
begin
  if AValue <> FIsArrayFormula then
  begin
    FIsArrayFormula := AValue;
    ClearResult;
  end;
end;

{ TdxSpreadSheetControlFormatSettings }

constructor TdxSpreadSheetControlFormatSettings.Create(AOwner: TdxCustomSpreadSheet);
begin
  FOwner := AOwner; 
  inherited Create;
end;

function TdxSpreadSheetControlFormatSettings.ExpandExternalLinks: Boolean;
begin
  Result := True; 
end;

function TdxSpreadSheetControlFormatSettings.GetFunctionName(const AName: Pointer): TdxUnicodeString;
begin
  Result := dxSpreadSheetUpperCase(dxStringToWideString(cxGetResourceString(AName)));
end;

procedure TdxSpreadSheetControlFormatSettings.UpdateSettings;
begin
  inherited UpdateSettings;
  CurrencyFormat := dxFormatSettings.CurrencyString;
  Data.ListSeparator := dxFormatSettings.ListSeparator;
  Data.DecimalSeparator := dxFormatSettings.DecimalSeparator;

  if ListSeparator = DecimalSeparator then
  begin
    if DecimalSeparator = ',' then
      ListSeparator := ';'
    else
      ListSeparator := ',';
  end;

  if ListSeparator = ';' then
    ArraySeparator := ':'
  else
    ArraySeparator := ';';

  R1C1Reference := Owner.OptionsView.R1C1Reference;
  DateTimeSystem := Owner.OptionsView.ActualDateTimeSystem;
  UpdateOperations;
end;

function TdxSpreadSheetControlFormatSettings.GetLocaleID: Integer;
begin
  Result := GetThreadLocale;
end;

{ TdxSpreadSheetFormulaController }

constructor TdxSpreadSheetFormulaController.Create(AOwner: TdxCustomSpreadSheet);
begin
  inherited Create(True);
  FOwner := AOwner;
  FCircularReferencePaths := TcxObjectList.Create;
end;

destructor TdxSpreadSheetFormulaController.Destroy;
begin
  FreeAndNil(FCircularReferencePaths);
  inherited Destroy;
end;

procedure TdxSpreadSheetFormulaController.Calculate;
var
  I: Integer;
  AFormula: TdxSpreadSheetFormula;
  ACell: TdxSpreadSheetCell;
  AArea: TRect;
begin
  FCalculationInProcess := True;
  CircularReferencePaths.Clear;
  FCircularPath := nil;
  try
    for I := 0 to SpreadSheet.DefinedNames.Count - 1 do
      SpreadSheet.DefinedNames[I].ClearResult;
    for I := 0 to Count - 1 do
      Items[I].ClearResult;
    FCircularReferencePaths.Capacity := Count;
    I := 0;
    while I < Count do
    begin
      AFormula := Items[I];
      ACell := AFormula.Cell;
      if not AFormula.IsArrayFormula and (ACell <> nil) and
             ACell.View.IsCellReferencedByArrayFormulaArea(ACell.RowIndex, ACell.ColumnIndex, AArea) then
        AFormula.Free
      else
      begin
        FCircularReference := False;
        FCircularPath := TdxSpreadSheetReferencePath.Create(AFormula.Row, AFormula.Column, AFormula.Sheet);
        try
          AFormula.GetResultValue;
        finally
          if FCircularReference then
            CircularReferencePaths.Add(FCircularPath)
          else
            FreeAndNil(FCircularPath);
        end;
        Inc(I);
      end;
    end;
  finally
    FCalculationInProcess := False;
  end;
  if Count > 0 then
    SpreadSheet.AddChanges([sscData]);
end;

procedure TdxSpreadSheetFormulaController.UpdateReferences(AView: TdxSpreadSheetTableView;
  const AArea: TRect; AModification: TdxSpreadSheetCellsModification; AIsDeletion: Boolean);
var
  I: Integer;
  AsText: TdxUnicodeString;
  AReferenceChanged: Boolean;
begin
  for I := 0 to Count - 1 do
  begin
    AsText := Items[I].AsText;
    AReferenceChanged := Items[I].UpdateReferences(AView, AArea, AModification, AIsDeletion);
    if AIsDeletion and AReferenceChanged and SpreadSheet.History.CanAddCommand then
      SpreadSheet.History.AddCommand(TdxSpreadSheetHistoryFormulaChangedCommand.Create(Items[I], AsText));
  end;
end;

procedure TdxSpreadSheetFormulaController.AddPath(ARow, AColumn: Integer; ASheet: TObject);
begin
  if CircularPath <> nil then
    CircularPath.Add(ARow, AColumn, ASheet);
end;

procedure TdxSpreadSheetFormulaController.CheckCircularReferences;
begin
  if HasCircularReferences and (CircularReferencePaths.Count > 0) then
    Exit
  else
  begin
    HasCircularReferences := CircularReferencePaths.Count > 0;
    if HasCircularReferences then
      raise EdxSpreadSheetCircularReferencesError.Create(sdxErrorCircularMessage);
  end;
end;

function TdxSpreadSheetFormulaController.CircularPathToString(
  APath: TdxSpreadSheetReferencePath): TdxUnicodeString;
var
  APathAsString: TdxUnicodeString;
begin
  Result := '';
  while APath <> nil do
  begin
    APathAsString := TdxSpreadSheetColumnHelper.NameByIndex(APath.Column) + IntToStr(APath.Row + 1);
    if APath.Sheet <> nil then
      APathAsString := TdxSpreadSheetTableView(APath.Sheet).Caption + '!' + APathAsString;
    if Result <> '' then
      Result := Result + ' -> '
    else
      Result := cxGetResourceString(@sdxErrorCircularPathPrefix);
    Result := Result + APathAsString;
    APath := APath.Next;
  end;
end;

procedure TdxSpreadSheetFormulaController.DoNameChanged(AName: TdxSpreadSheetDefinedName;
  const ANewName: TdxUnicodeString);
var
  I: Integer;
begin
  for I := 0 to Count - 1 do
    Items[I].DoNameChanged(AName, ANewName);
end;

function TdxSpreadSheetFormulaController.GetCellFromPath(APath: TdxSpreadSheetReferencePath): TdxSpreadSheetCell;
begin
  Result := TdxSpreadSheetTableView(APath.Sheet).Cells[APath.Row, APath.Column];
end;

procedure TdxSpreadSheetFormulaController.InitializeStrings;
begin
  FormatSettings.UpdateSettings;
end;

function TdxSpreadSheetFormulaController.NeedRecalcuate: Boolean;
begin
  Result := SpreadSheet.OptionsBehavior.AutomaticCalculation;
end;

procedure TdxSpreadSheetFormulaController.Recalculate;
begin
  if not NeedRecalcuate then Exit;
  Calculate;
end;

procedure TdxSpreadSheetFormulaController.RemovePath(ARow, AColumn: Integer; ASheet: TObject);
begin
  if not CircularReference and (CircularPath <> nil) then
    CircularPath.Remove(ARow, AColumn, ASheet);
end;

function TdxSpreadSheetFormulaController._AddRef: Integer;
begin
  Result := -1;
end;

function TdxSpreadSheetFormulaController._Release: Integer;
begin
  Result := -1;
end;

function TdxSpreadSheetFormulaController.QueryInterface(const IID: TGUID; out Obj): HResult;
begin
  if GetInterface(IID, Obj) then
    Result := 0
  else
    Result := cxE_NOINTERFACE;
end;

procedure TdxSpreadSheetFormulaController.FormatChanged;
begin
  InitializeStrings;
end;

procedure TdxSpreadSheetFormulaController.TranslationChanged;
begin
  InitializeStrings;
  dxSpreadSheetFunctionsRepository.TranslationChanged(FormatSettings);
end;

function TdxSpreadSheetFormulaController.GetCircularReferencePathsAsString: TdxUnicodeString;
var
  I: Integer;
begin
  Result := '';
  if CircularReferencePaths.Count = 0 then Exit;
  for I := 0 to CircularReferencePaths.Count - 1 do
  begin
    if I > 0 then
      Result := Result + dxCRLF;
    Result := Result + CircularPathToString(TdxSpreadSheetReferencePath(CircularReferencePaths[I]));
  end;
end;

function TdxSpreadSheetFormulaController.GetFormatSettings: TdxSpreadSheetFormatSettings;
begin
  Result := SpreadSheet.FormatSettings;
end;

function TdxSpreadSheetFormulaController.GetItem(AIndex: Integer): TdxSpreadSheetFormula;
begin
  Result := TdxSpreadSheetFormula(inherited Items[AIndex]);
end;

{ TdxSpreadSheetCustomController }

constructor TdxSpreadSheetCustomController.Create(AOwner: TObject);
begin
  inherited Create;
  FOwner:= AOwner;
end;

destructor TdxSpreadSheetCustomController.Destroy;
begin
  if (SpreadSheet <> nil) and (SpreadSheet.ActiveController = Self) then
    SpreadSheet.ActiveController := nil;
  FreeAndNil(FPopupMenu);
  inherited Destroy;
end;

function TdxSpreadSheetCustomController.ContextPopup(const P: TPoint): Boolean;
var
  APopupMenuClass: TComponentClass;
begin
  APopupMenuClass := GetPopupMenuClass(P);
  if (FPopupMenu = nil) or (FPopupMenu.ClassType <> APopupMenuClass) then
  begin
    FreeAndNil(FPopupMenu);
    if APopupMenuClass <> nil then
      FPopupMenu := APopupMenuClass.Create(SpreadSheet);
  end;
  Result := (FPopupMenu <> nil) and (FPopupMenu as TdxSpreadSheetCustomPopupMenu).Popup(P);
end;

function TdxSpreadSheetCustomController.CanFocusOnClick: Boolean; 
begin
  Result := True;
end;

procedure TdxSpreadSheetCustomController.DblClick;
begin
end;

function TdxSpreadSheetCustomController.GetCursor(const ACursorPos: TPoint): TCursor;
begin
  Result := HitTest.GetCursor(ACursorPos);
end;

function TdxSpreadSheetCustomController.GetDragAndDropObjectClass(const ACursorPos: TPoint): TcxDragAndDropObjectClass;
begin
  Result := HitTest.GetDragAndDropObjectClass(ACursorPos);
end;

function TdxSpreadSheetCustomController.GetHitTest: TdxSpreadSheetCustomHitTest;
begin
  Result := nil;
end;

function TdxSpreadSheetCustomController.GetPopupMenuClass(const ACursorPos: TPoint): TComponentClass;
begin
  Result := HitTest.GetPopupMenuClass(ACursorPos);
end;

function TdxSpreadSheetCustomController.GetZoomFactor: Integer;
begin 
  Result := 100;
end; 

procedure TdxSpreadSheetCustomController.KeyDown(var Key: Word; Shift: TShiftState);
begin
end;

procedure TdxSpreadSheetCustomController.KeyUp(var Key: Word; Shift: TShiftState);
begin
end;

procedure TdxSpreadSheetCustomController.KeyPress(var Key: Char);
begin
end;

procedure TdxSpreadSheetCustomController.DoMouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  // do nothing
end;

procedure TdxSpreadSheetCustomController.DoMouseMove(Shift: TShiftState; X, Y: Integer);
begin
  // do nothing
end;

procedure TdxSpreadSheetCustomController.DoMouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  // do nothing
end;

function TdxSpreadSheetCustomController.DoMouseWheel(Shift: TShiftState; WheelDelta: Integer; const P: TPoint): Boolean;
begin
  Result := False;
end;

procedure TdxSpreadSheetCustomController.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  DoMouseDown(Button, Shift, MulDiv(X, 100, ZoomFactor), MulDiv(Y, 100, ZoomFactor));
end;

procedure TdxSpreadSheetCustomController.MouseMove(Shift: TShiftState; X, Y: Integer);
begin
  DoMouseMove(Shift, MulDiv(X, 100, ZoomFactor), MulDiv(Y, 100, ZoomFactor));
end;

procedure TdxSpreadSheetCustomController.MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  DoMouseUp(Button, Shift, MulDiv(X, 100, ZoomFactor), MulDiv(Y, 100, ZoomFactor));
end;

function TdxSpreadSheetCustomController.MouseWheel(Shift: TShiftState; WheelDelta: Integer; const P: TPoint): Boolean;
begin
  Result := DoMouseWheel(Shift, WheelDelta, cxPointScale(P, 100, ZoomFactor));
end;

{ TdxSpreadSheetCustomViewOptions }

procedure TdxSpreadSheetCustomViewOptions.Assign(Source: TPersistent);
begin
  if Source is TdxSpreadSheetCustomViewOptions then
    Protected := TdxSpreadSheetCustomViewOptions(Source).Protected;
end;

procedure TdxSpreadSheetCustomViewOptions.Changed;
begin
  View.Changed;
end;

function TdxSpreadSheetCustomViewOptions.GetView: TdxSpreadSheetCustomView;
begin
  Result := inherited Owner as TdxSpreadSheetCustomView;
end;

procedure TdxSpreadSheetCustomViewOptions.SetProtected(AValue: Boolean);
begin
  if FProtected <> AValue then
  begin
    FProtected := AValue;
    Changed;
  end;
end;

{ TdxSpreadSheetCustomView }

constructor TdxSpreadSheetCustomView.Create(AOwner: TdxCustomSpreadSheet);
begin
  inherited Create(AOwner);
  FVisible := True;
  CreateSubClasses;
  SpreadSheet.DoAddSheet(Self);
end;

destructor TdxSpreadSheetCustomView.Destroy;
begin
  DestroySubClasses;
  SpreadSheet.DoRemoveSheet(Self);
  inherited Destroy;
end;

procedure TdxSpreadSheetCustomView.Assign(Source: TPersistent);
begin
  if Source is TdxSpreadSheetCustomView then
  begin
    Caption := TdxSpreadSheetCustomView(Source).Caption;
    Visible := TdxSpreadSheetCustomView(Source).Visible;
  end
  else
    inherited Assign(Source);
end;

procedure TdxSpreadSheetCustomView.BeforeDestruction;
begin
  inherited BeforeDestruction;
  FIsDestroying := True;
end;

procedure TdxSpreadSheetCustomView.BeginUpdate;
begin
  Inc(FLockCount);
end;

procedure TdxSpreadSheetCustomView.EndUpdate;
begin
  Dec(FLockCount);
  if FLockCount = 0 then
    CheckChanges;
end;

procedure TdxSpreadSheetCustomView.Invalidate;
begin
  InvalidateRect(ViewInfo.Bounds);
end;

procedure TdxSpreadSheetCustomView.InvalidateRect(const R: TRect);
begin
  if Visible then
    SpreadSheet.InvalidateRect(cxRectScale(R, ZoomFactor, 100), False);
end;

procedure TdxSpreadSheetCustomView.AddChanges(AChanges: TdxSpreadSheetChanges);
begin
  FChanges := FChanges + AChanges;
  CheckChanges;
end;

procedure TdxSpreadSheetCustomView.AfterLoad;
begin
end;

procedure TdxSpreadSheetCustomView.Changed;
begin
  AddChanges([sscLayout]);
end;

procedure TdxSpreadSheetCustomView.CheckChanges;
begin
  if not (IsDestroying or IsLocked or (Changes = [])) then
    DoCheckChanges;
end;

procedure TdxSpreadSheetCustomView.DoCheckChanges;
begin
  if sscData in Changes then
  begin
    if SpreadSheet <> nil then
    begin
      Changes := Changes - [sscData];
      SpreadSheet.AddChanges([sscData])
    end
    else
      Changes := Changes + [sscLayout]
  end;
  if sscLayout in Changes then
  begin
    Changes := Changes - [sscLayout];
    if Active then
      ViewInfo.Recalculate
    else
      ViewInfo.IsDirty := True;
    SpreadSheet.DoLayoutChanged;
  end;
end;

procedure TdxSpreadSheetCustomView.InitScrollBarsParameters;
begin
  ViewInfo.InitScrollBarsParameters;
end;

procedure TdxSpreadSheetCustomView.SelectionChanged;
begin
  if IsLocked then Exit;
  ViewInfo.SelectionChanged;
  SpreadSheet.DoSelectionChanged(Self);
end;

procedure TdxSpreadSheetCustomView.ValidateCaption;
var
  I: Integer;
  J: TdxSpreadSheetFormulaOperation;
begin
  for I := 0 to SpreadSheet.SheetCount - 1 do
  begin
    if (SpreadSheet.Sheets[I] <> Self) and dxSpreadSheetTextIsEqual(SpreadSheet.Sheets[I].Caption, FCaption) then
      Abort;
  end;

  FIsCaptionTextDelimited := False;
  for I := 1 to Length(FCaption) do
  begin
    FIsCaptionTextDelimited := FIsCaptionTextDelimited or
      dxSpreadSheetCharIsEqual(FCaption[I], dxCharToUnicodeChar(FormatSettings.Data.DecimalSeparator));
    if not FIsCaptionTextDelimited then
    begin
      for J := Low(FormatSettings.Operations) to High(FormatSettings.Operations) do
        FIsCaptionTextDelimited := FIsCaptionTextDelimited or ((Length(FormatSettings.Operations[J]) > 0) and
          dxSpreadSheetCharIsEqual(FCaption[I], FormatSettings.Operations[J][1]));
    end;
    if FIsCaptionTextDelimited then Break;
  end;
end;

function TdxSpreadSheetCustomView.CreateController: TdxSpreadSheetCustomViewController;
begin
  Result := TdxSpreadSheetCustomViewController.Create(Self);
end;

function TdxSpreadSheetCustomView.CreateContainers: TdxSpreadSheetContainers;
begin
  Result := TdxSpreadSheetContainers.Create(Self);
end;

function TdxSpreadSheetCustomView.CreateHitTest: TdxSpreadSheetCustomHitTest;
begin
  Result := TdxSpreadSheetCustomHitTest.Create(Self);
end;

function TdxSpreadSheetCustomView.CreateOptions: TdxSpreadSheetCustomViewOptions;
begin
  Result := TdxSpreadSheetCustomViewOptions.Create(Self);
end;

function TdxSpreadSheetCustomView.CreateViewInfo: TdxSpreadSheetCustomViewViewInfo;
begin
  Result := TdxSpreadSheetCustomViewViewInfo.Create(Self);
end;

procedure TdxSpreadSheetCustomView.CreateSubClasses;
begin
  FOptions := CreateOptions;
  FViewInfo := CreateViewInfo;
  FContainers := CreateContainers;
  FHitTest := CreateHitTest;
  FController := CreateController;
end;

procedure TdxSpreadSheetCustomView.DestroySubClasses;
begin
  Containers.Clear;
  FreeAndNil(FViewInfo);
  FreeAndNil(FHitTest);
  FreeAndNil(FController);
  FreeAndNil(FContainers);
  FreeAndNil(FOptions);
end;

function TdxSpreadSheetCustomView.AbsoluteToScreen(const R: TRect): TRect;
begin
  Result := R;
end;

function TdxSpreadSheetCustomView.GetContentOrigin: TPoint;
begin
  Result := cxNullPoint;
end;

function TdxSpreadSheetCustomView.GetPartOffsetByPoint(const P: TPoint): TPoint;
begin
  Result := cxNullPoint;
end;

function TdxSpreadSheetCustomView.GetZoomFactor: Integer;
begin
  Result := 100;
end;

procedure TdxSpreadSheetCustomView.Pack;
begin
  // do nothing
end;

procedure TdxSpreadSheetCustomView.Scroll(AScrollBarKind: TScrollBarKind; AScrollCode: TScrollCode; var AScrollPos: Integer);
begin
  ViewInfo.Scroll(AScrollBarKind, AScrollCode, AScrollPos);
end;

procedure TdxSpreadSheetCustomView.ValidateRect(const R: TRect);
var
  Area: TRect;
begin
  Area := cxRectScale(R, ZoomFactor, 100);
  if Visible and SpreadSheet.HandleAllocated then
    Windows.ValidateRect(SpreadSheet.Handle, @Area);
end;

function TdxSpreadSheetCustomView.GetActive: Boolean;
begin
  Result := SpreadSheet.ActiveSheet = self;
end;

function TdxSpreadSheetCustomView.GetBounds: TRect;
begin
  if Visible then
    Result := SpreadSheet.ClientBounds
  else
    Result := cxInvalidRect;
end;

function TdxSpreadSheetCustomView.GetCellStyles: TdxSpreadSheetCellStyles;
begin
  Result := SpreadSheet.CellStyles;
end;

function TdxSpreadSheetCustomView.GetIndex: Integer;
begin
  Result := SpreadSheet.FSheets.IndexOf(Self);
end;

function TdxSpreadSheetCustomView.GetIsDestroying: Boolean;
begin
  Result := SpreadSheet.IsDestroying or FIsDestroying;
end;

function TdxSpreadSheetCustomView.GetIsLocked: Boolean;
begin
  Result := (FLockCount > 0) or IsDestroying;
  if not Result and (SpreadSheet <> nil) then
    Result := SpreadSheet.IsLocked;
end;

function TdxSpreadSheetCustomView.GetFormatSettings: TdxSpreadSheetFormatSettings;
begin
  Result := SpreadSheet.FormatSettings;
end;

procedure TdxSpreadSheetCustomView.SetActive(AValue: Boolean);
begin
  SpreadSheet.ActiveSheet := Self;
end;

procedure TdxSpreadSheetCustomView.SetCaption(const AValue: TdxUnicodeString);
var
  AOldCaption: TdxUnicodeString;
begin
  if dxSpreadSheetCompareText(FCaption, AValue) <> 0 then
  begin
    AOldCaption := FCaption;
    try
      FCaption := AValue;
      ValidateCaption;
      SpreadSheet.LayoutChanged;
    except
      on EAbort do
      begin
        FCaption := AOldCaption;
        raise;
      end;
    end;
  end;
end;

procedure TdxSpreadSheetCustomView.SetIndex(AValue: Integer);
begin
  SpreadSheet.SetSheetIndex(Self, AValue);
end;

procedure TdxSpreadSheetCustomView.SetOptions(AValue: TdxSpreadSheetCustomViewOptions);
begin
  FOptions.Assign(AValue);
end;

procedure TdxSpreadSheetCustomView.SetVisible(AValue: Boolean);
begin
  if AValue <> FVisible then
  begin
    FVisible := AValue;
    SpreadSheet.DoChangeSheetVisibility(Self);
  end;
end;

{ TdxSpreadSheetCustomViewViewInfo }

constructor TdxSpreadSheetCustomViewViewInfo.Create(AView: TdxSpreadSheetCustomView);
begin
  inherited Create(AView);
  FContainers := TdxSpreadSheetViewInfoCellsList.Create;
end;

destructor TdxSpreadSheetCustomViewViewInfo.Destroy;
begin
  FreeAndNil(FContainers);
  inherited Destroy;
end;

procedure TdxSpreadSheetCustomViewViewInfo.Calculate;
begin
  FBounds := View.SpreadSheet.ClientBounds;
  FIsDirty := False;
end;

procedure TdxSpreadSheetCustomViewViewInfo.CalculateContainers;
var
  ACell: TdxSpreadSheetContainerViewInfo;
  AContainer: TdxSpreadSheetContainer;
  AIndex: Integer;
  AOrigin: TPoint;
  I: Integer;
begin
  AOrigin := View.GetContentOrigin;
  for I := 0 to View.Containers.Count - 1 do
  begin
    AContainer := View.Containers[I];
    if AContainer.Visible then
    begin
      AIndex := Containers.FindItem(AContainer);
      if AIndex < 0 then
      begin
        ACell := AContainer.CreateViewInfo;
        ACell.SetBounds(AOrigin, AContainer.Calculator.CalculateBounds, Bounds);
        Containers.Add(ACell);
      end
      else
        Containers.Move(AIndex, I);
    end;
  end;
end;

function TdxSpreadSheetCustomViewViewInfo.CalculateContainersHitTestWithOriginOffset(
  AHitTest: TdxSpreadSheetCustomHitTest; const AOffset: TPoint): Boolean;
var
  AViewInfo: TdxSpreadSheetContainerViewInfo;
  I: Integer;
begin
  Containers.OffsetOrigin(AOffset);
  try
    Result := False;
    for I := Containers.Count - 1 downto 0 do
    begin
      AViewInfo := TdxSpreadSheetContainerViewInfo(Containers[I]);
      if AViewInfo.Selected and AViewInfo.Selection.InitHitTest(AHitTest) then
      begin
        Result := True;
        Break;
      end;
    end;
    Result := Result or Containers.CalculateHitTest(AHitTest, True);
  finally
    Containers.OffsetOrigin(cxPointInvert(AOffset));
  end;
end;

procedure TdxSpreadSheetCustomViewViewInfo.Clear;
begin
  FContainers.Clear;
end;

procedure TdxSpreadSheetCustomViewViewInfo.Draw(ACanvas: TcxCanvas);
begin
end;

procedure TdxSpreadSheetCustomViewViewInfo.Recalculate;
begin
  Calculate;
  if View.Active then
    View.Invalidate;
end;

procedure TdxSpreadSheetCustomViewViewInfo.Validate;
begin
  if IsDirty then
    Calculate;
end;

procedure TdxSpreadSheetCustomViewViewInfo.InitScrollBarsParameters;
begin
end;

procedure TdxSpreadSheetCustomViewViewInfo.Scroll(
  AScrollBarKind: TScrollBarKind; AScrollCode: TScrollCode; var AScrollPos: Integer);
begin
end;

procedure TdxSpreadSheetCustomViewViewInfo.SelectionChanged;
begin
end;

procedure TdxSpreadSheetCustomViewViewInfo.SetIsDirty(AValue: Boolean);
begin
  if AValue <> FIsDirty then
  begin
    if not AValue then
    begin
      Calculate;
      View.Invalidate;
    end
    else
      FIsDirty := AValue;
  end;
end;

{ TdxSpreadSheetMergedCellsRow }

procedure TdxSpreadSheetMergedCellsRow.AddCell(ACell: TdxSpreadSheetMergedCell);
begin
  TdxSpreadSheetMergedCellsRowItem(inherited Add).Cell := ACell;
end;

procedure TdxSpreadSheetMergedCellsRow.RemoveCell(ACell: TdxSpreadSheetMergedCell);
var
  AItem: TdxSpreadSheetMergedCellsRowItem;
begin
  AItem := TdxSpreadSheetMergedCellsRowItem(First);
  while AItem <> nil do
  begin
    if AItem.Cell = ACell then
    begin
      Delete(AItem);
      Break;
    end
    else
      AItem := TdxSpreadSheetMergedCellsRowItem(AItem.Next)
  end;
end;

procedure TdxSpreadSheetMergedCellsRow.ExtractCells(AColumn1, AColumn2: Integer;
  const ADest: TList<TdxSpreadSheetMergedCell>);
var
  AItem: TdxSpreadSheetMergedCellsRowItem;
begin
  AItem := TdxSpreadSheetMergedCellsRowItem(First);
  while AItem <> nil do
  begin
    if (AItem.Cell.Area.Right >= AColumn1) and (AItem.Cell.Area.Left <= AColumn2) and (ADest.IndexOf(AItem.Cell) < 0) then
      ADest.Add(AItem.Cell);
    AItem := TdxSpreadSheetMergedCellsRowItem(AItem.Next);
  end;
end;

procedure TdxSpreadSheetMergedCellsRow.Union(var AArea: TRect);
var
  R: TRect;
  AItem: TdxSpreadSheetMergedCellsRowItem;
begin
  AItem := TdxSpreadSheetMergedCellsRowItem(First);
  while AItem <> nil do
  begin
    R := AItem.Cell.Area;
    if InRange(AArea.Left, R.Left, R.Right) or InRange(AArea.Right, R.Left, R.Right) then
      AArea := dxSpreadSheetCellsUnion(AArea, R);
    AItem := TdxSpreadSheetMergedCellsRowItem(AItem.Next);
  end;
end;

function TdxSpreadSheetMergedCellsRow.Contains(AColumn: Integer): TdxSpreadSheetMergedCell;
var
  AItem: TdxSpreadSheetMergedCellsRowItem;
begin
  Result := nil;
  AItem := TdxSpreadSheetMergedCellsRowItem(First);
  while AItem <> nil do
  begin
    if InRange(AColumn, AItem.Cell.Area.Left, AItem.Cell.Area.Right) then
    begin
      Result := AItem.Cell;
      Break;
    end
    else
      AItem := TdxSpreadSheetMergedCellsRowItem(AItem.Next);
  end;
end;

function TdxSpreadSheetMergedCellsRow.CreateLinkedObject: TcxDoublyLinkedObject;
begin
  Result := TdxSpreadSheetMergedCellsRowItem.Create;
end;

{ TdxSpreadSheetMergedCell }

destructor TdxSpreadSheetMergedCell.Destroy;
begin
  if Owner <> nil then
  begin
    Owner.DoRemoving(Self);
    if Owner.History.CanAddCommand then
      Owner.History.AddCommand(TdxSpreadSheetHistoryMergeCellsCommand.Create(nil, Area));
  end;
  inherited Destroy;
end;

function TdxSpreadSheetMergedCell.Contains(const ARow, AColumn: Integer): Boolean;
begin
  Result := dxSpreadSheetContains(FArea, ARow, AColumn)
end;

function TdxSpreadSheetMergedCell.Intersects(const AArea: TRect): Boolean;
begin
  Result := dxSpreadSheetIntersects(FArea, AArea);
end;

procedure TdxSpreadSheetMergedCell.Initialize(AOwner: TdxSpreadSheetMergedCellList; const AArea: TRect);
begin
  if FOwner <> nil then
    FOwner.RemoveRef(Self);
  FOwner := AOwner;
  FArea := AArea;
  if FOwner <> nil then
    FOwner.AddRef(Self);
end;

function TdxSpreadSheetMergedCell.GetActiveCell: TdxSpreadSheetCell;
begin
  Result := View.Cells[FArea.Top, FArea.Left];
end;

function TdxSpreadSheetMergedCell.GetHistory: TdxSpreadSheetHistory;
begin
  Result := Owner.History;
end;

function TdxSpreadSheetMergedCell.GetNext: TdxSpreadSheetMergedCell;
begin
  Result := TdxSpreadSheetMergedCell(inherited Next);
end;

function TdxSpreadSheetMergedCell.GetPrev: TdxSpreadSheetMergedCell;
begin
  Result := TdxSpreadSheetMergedCell(inherited Prev);
end;

function TdxSpreadSheetMergedCell.GetView: TdxSpreadSheetTableView;
begin
  Result := FOwner.View;
end;

procedure TdxSpreadSheetMergedCell.SetNext(const Value: TdxSpreadSheetMergedCell);
begin
  inherited Next := Value;
end;

procedure TdxSpreadSheetMergedCell.SetPrev(const Value: TdxSpreadSheetMergedCell);
begin
  inherited Prev := Value;
end;

{ TdxSpreadSheetMergedCellList }

constructor TdxSpreadSheetMergedCellList.Create(AView: TdxSpreadSheetTableView);
begin
  inherited Create;
  Rows := TObjectDictionary<Integer, TdxSpreadSheetMergedCellsRow>.Create([doOwnsValues], 1024);
  FView := AView;
end;

destructor TdxSpreadSheetMergedCellList.Destroy;
begin
  Rows.Free;
  inherited Destroy;
end;

procedure TdxSpreadSheetMergedCellList.Add(const AArea: TRect);
var
  ACell: TdxSpreadSheetMergedCell;
begin
  if not dxSpreadSheetIsSingleCellArea(AArea) then
  begin
    View.BeginUpdate;
    try
      if not (sssReading in View.SpreadSheet.State) and not View.History.InProcess then
        DeleteItemsInArea(AArea);
      ACell := TdxSpreadSheetMergedCell(inherited Add);
      Inc(FCount);
      if History.CanAddCommand then
      begin
        History.BeginAction(TdxSpreadSheetHistoryMergeCellsAction);
        try
          History.AddCommand(TdxSpreadSheetHistoryMergeCellsCommand.Create(ACell, AArea));
          InitializeCell(ACell, AArea);
        finally
          View.History.EndAction;
        end;
      end
      else
        InitializeCell(ACell, AArea);
    finally
      View.EndUpdate;
    end;
  end;
end;

function TdxSpreadSheetMergedCellList.CheckCell(ARow, AColumn: Integer): TRect;
var
  ACell: TdxSpreadSheetMergedCell;
begin
  ACell := FindCell(ARow, AColumn);
  if ACell <> nil then
    Result := ACell.Area
  else
    Result := Rect(AColumn, ARow, AColumn, ARow);
end;

procedure TdxSpreadSheetMergedCellList.Clear;
begin
  FIsDeleting := True;
  try
    inherited Clear;
  finally
    FCount := 0;
    FIsDeleting := False;
    Changed;
  end;
end;

procedure TdxSpreadSheetMergedCellList.Delete(ALinkedObject: TcxDoublyLinkedObject);
begin
  ALinkedObject.Free;
  ClearCacheInformation;
end;

procedure TdxSpreadSheetMergedCellList.DeleteItemsInArea(const AArea: TRect);
var
  I: Integer;
  R: TRect;
  AList: TList<TdxSpreadSheetMergedCell>;
begin
  View.BeginUpdate;
  try
    AList := TList<TdxSpreadSheetMergedCell>.Create;
    try
      Extract(AArea, AList);
      for I := 0 to AList.Count - 1 do
        if dxSpreadSheetIntersects(AList[I].Area, AArea, R) and cxRectIsEqual(R, AList[I].Area) then
          AList[I].Free;
    finally
      AList.Free;
    end;
  finally
    View.EndUpdate;
  end;
end;

procedure TdxSpreadSheetMergedCellList.DoRemoving(ACell: TdxSpreadSheetMergedCell);
begin
  Dec(FCount);
  if not FIsDeleting then
  begin
    RemoveRef(ACell);
    inherited Extract(ACell);
    Changed;
  end;
end;

function TdxSpreadSheetMergedCellList.Find(ACell: TdxSpreadSheetMergedCell): Integer;
var
  AItem: TdxSpreadSheetMergedCell;
begin
  Result := 0;
  AItem := First;
  while (AItem <> nil) and (AItem <> ACell) do
  begin
    Inc(Result);
    AItem := AItem.Next;
  end;
  if AItem = nil then
    Result := -1;
end;

function TdxSpreadSheetMergedCellList.FindCell(ARow, AColumn: Integer): TdxSpreadSheetMergedCell;
var
  APoint: TPoint;
  ARowCells: TdxSpreadSheetMergedCellsRow;
begin
  Result := nil;
  if (ARow < 0) or (AColumn < 0) or (Count = 0) then
    Exit;
  APoint := Point(AColumn, ARow);
  if cxPointIsEqual(CachedPoint, APoint) or ((CachedCell <> nil) and CachedCell.Contains(ARow, AColumn)) then
    Result := CachedCell
  else
  begin
    if Rows.TryGetValue(ARow, ARowCells) then
      Result := ARowCells.Contains(AColumn);
    CachedPoint := APoint;
    CachedCell := Result;
  end;
end;

procedure TdxSpreadSheetMergedCellList.InitializeCell(ACell: TdxSpreadSheetMergedCell; const AArea: TRect);
begin
  ACell.Initialize(Self, AArea);
  if not (sssReading in View.SpreadSheet.State) and not History.InProcess then
    TdxSpreadSheetTableViewMergeCellStyleHelper.Calculate(ACell);
  Changed;
end;

procedure TdxSpreadSheetMergedCellList.AddRef(ACell: TdxSpreadSheetMergedCell);
var
  ARowIndex: Integer;
  ARowCells: TdxSpreadSheetMergedCellsRow;
begin
  for ARowIndex := Max(0, ACell.Area.Top) to ACell.Area.Bottom do
  begin
    if not Rows.TryGetValue(ARowIndex, ARowCells) then
    begin
      ARowCells := TdxSpreadSheetMergedCellsRow.Create;
      Rows.Add(ARowIndex, ARowCells);
    end;
    ARowCells.AddCell(ACell);
  end;
end;

procedure TdxSpreadSheetMergedCellList.RemoveRef(ACell: TdxSpreadSheetMergedCell);
var
  ARowIndex: Integer;
  ARowCells: TdxSpreadSheetMergedCellsRow;
begin
  for ARowIndex := Max(0, ACell.Area.Top) to ACell.Area.Bottom do
    if Rows.TryGetValue(ARowIndex, ARowCells) then
      ARowCells.RemoveCell(ACell);
end;

function TdxSpreadSheetMergedCellList.ExpandArea(const AArea: TRect): TRect;
var
  R: TRect;
  ARowIndex: Integer;
  ARowCells: TdxSpreadSheetMergedCellsRow;
begin
  Result := AArea;
  if (AArea.Left < 0) or (AArea.Top < 0) or (Count = 0) then
    Exit;
  if cxRectIsEqual(Result, CachedSourceArea) then
    Result := CachedExpandedArea
  else
  begin
    repeat
      R := Result;
      for ARowIndex := Max(0, Result.Top) to Result.Bottom do
        if Rows.TryGetValue(ARowIndex, ARowCells) then
          ARowCells.Union(Result);
    until ((Result.Top = R.Top) and (Result.Bottom = R.Bottom)) or (cxRectWidth(Result) * cxRectHeight(Result) = 0);
    CachedSourceArea := AArea;
    CachedExpandedArea := Result;
  end;
end;

function TdxSpreadSheetMergedCellList.ExpandArea(const AColumn, ARow: Integer): TRect;
var
  ACell: TdxSpreadSheetMergedCell;
begin
  Result := cxRect(AColumn, ARow, AColumn, ARow);
  ACell := FindCell(ARow, AColumn);
  if ACell <> nil then
    Result := ACell.Area;
end;

procedure TdxSpreadSheetMergedCellList.Extract(const ABounds: TRect; ADest: TList<TdxSpreadSheetMergedCell>);
var
  ARowIndex: Integer;
  ARowCells: TdxSpreadSheetMergedCellsRow;
begin
  ADest.Clear;
  ADest.Capacity := Count;
  for ARowIndex in Rows.Keys do
    if Rows.TryGetValue(ARowIndex, ARowCells) then
      ARowCells.ExtractCells(ABounds.Left, ABounds.Right, ADest);
end;

function TdxSpreadSheetMergedCellList.HasItemsInArea(const AArea: TRect): Boolean;
var
  ACell: TdxSpreadSheetMergedCell;
begin
  Result := False;
  ACell := Last;
  while ACell <> nil do
  begin
    if ACell.Intersects(AArea) then
      Exit(True);
    ACell := ACell.Prev;
  end;
end;

procedure TdxSpreadSheetMergedCellList.Changed;
begin
  View.Changed;
  ClearCacheInformation;
end;

procedure TdxSpreadSheetMergedCellList.ClearCacheInformation;
begin
  CachedCell := nil;
  CachedExpandedArea := cxInvalidRect;
  CachedPoint := cxInvalidPoint;
  CachedSourceArea := cxInvalidRect;
end;

function TdxSpreadSheetMergedCellList.CreateLinkedObject: TcxDoublyLinkedObject;
begin
  Result := TdxSpreadSheetMergedCell.Create;
end;

function TdxSpreadSheetMergedCellList.GetFirst: TdxSpreadSheetMergedCell;
begin
  Result := TdxSpreadSheetMergedCell(inherited First);
end;

function TdxSpreadSheetMergedCellList.GetHistory: TdxSpreadSheetHistory;
begin
  Result := View.History;
end;

function TdxSpreadSheetMergedCellList.GetItem(AIndex: Integer): TdxSpreadSheetMergedCell;
begin
  Result := First;
  while (Result <> nil) and (AIndex > 0) do
  begin
    Result := Result.Next;
    Dec(AIndex)
  end;
end;

function TdxSpreadSheetMergedCellList.GetLast: TdxSpreadSheetMergedCell;
begin
  Result := TdxSpreadSheetMergedCell(inherited Last);
end;

{ TdxSpreadSheetTableItem }

constructor TdxSpreadSheetTableItem.Create(AOwner: TdxDynamicItemList; AIndex: Integer);
begin
  inherited Create(AOwner, AIndex);
  FStyle := TdxSpreadSheetTableItemStyle.Create(Self);
  FDefaultSize := True;
  FVisible := True;
end;

destructor TdxSpreadSheetTableItem.Destroy;
begin
  FreeAndNil(FStyle);
  inherited Destroy;
end;

procedure TdxSpreadSheetTableItem.ApplyBestFit;
var
  ACell: TdxSpreadSheetCell;
  ASize: Integer;
  I: Integer;
begin
  ASize := 0;
  for I := 0 to CellCount - 1 do
  begin
    ACell := Cells[I];
    if (ACell <> nil) and CanMeasureCellSize(ACell) then
      ASize := Max(ASize, MeasureCellSize(cxScreenCanvas, ACell));
  end;
  cxScreenCanvas.Dormant;
  if ASize = 0 then
    ASize := Owner.DefaultSize;
  if ASize <> Size then
    SetSize(ASize);
  FIsCustomSize := False;
end;

procedure TdxSpreadSheetTableItem.BeforeDestruction;
begin
  inherited BeforeDestruction;
  if not Owner.FIsDeletion then
    History.AddCommand(TdxSpreadSheetHistoryDeleteItemCommand.Create(Self));
end;

procedure TdxSpreadSheetTableItem.AfterResize;
begin
  Owner.AfterResize;
end;

procedure TdxSpreadSheetTableItem.BeforeResize;
begin
  Owner.BeforeResize;
end;

function TdxSpreadSheetTableItem.CanMeasureCellSize(ACell: TdxSpreadSheetCell): Boolean;
var
  AMergedCell: TdxSpreadSheetMergedCell;
begin
  AMergedCell := View.MergedCells.FindCell(ACell.RowIndex, ACell.ColumnIndex);
  Result := (AMergedCell = nil) or (IsItemArea(AMergedCell.Area) and
    cxPointIsEqual(AMergedCell.Area.TopLeft,Point(ACell.ColumnIndex, ACell.RowIndex)));
end;

procedure TdxSpreadSheetTableItem.Changed;
begin
  Owner.Changed;
end;

function TdxSpreadSheetTableItem.GetDisplayText: string;
begin
  Result := Owner.GetItemDisplayText(Self);
end;

procedure TdxSpreadSheetTableItem.Restore(AReader: TcxReader);
begin
  BeforeResize;
  try
    if AReader.ReadBoolean then
      Style.Handle := View.CellStyles.CreateStyleFromStream(AReader);
    FDefaultSize := AReader.ReadBoolean;
    FIsCustomSize := AReader.ReadBoolean;
    FVisible := AReader.ReadBoolean;
    FSize := AReader.ReadInteger;
  finally
    AfterResize;
    Changed;
  end;
end;

procedure TdxSpreadSheetTableItem.Store(AWriter: TcxWriter);
begin
  AWriter.WriteBoolean(FStyle.Handle <> nil);
  if FStyle.Handle <> nil then
    FStyle.Handle.SaveToStream(AWriter);
  AWriter.WriteBoolean(FDefaultSize);
  AWriter.WriteBoolean(FIsCustomSize);
  AWriter.WriteBoolean(FVisible);
  AWriter.WriteInteger(FSize);
end;

function TdxSpreadSheetTableItem.GetHistory: TdxSpreadSheetHistory;
begin
  Result := Owner.SpreadSheet.History;
end;

function TdxSpreadSheetTableItem.GetNext: TdxSpreadSheetTableItem;
begin
  Result := TdxSpreadSheetTableItem(FNext);
end;

function TdxSpreadSheetTableItem.GetOwner: TdxSpreadSheetTableItems;
begin
  Result := TdxSpreadSheetTableItems(FOwner);
end;

function TdxSpreadSheetTableItem.GetPrev: TdxSpreadSheetTableItem;
begin
  Result := TdxSpreadSheetTableItem(FPrev);
end;

function TdxSpreadSheetTableItem.GetSize: Integer;
begin
  if not Visible then
    Result := 0
  else
    if DefaultSize then
      Result := Owner.DefaultSize
    else
      Result := FSize;
end;

function TdxSpreadSheetTableItem.GetView: TdxSpreadSheetTableView;
begin
  Result := Owner.View;
end;

procedure TdxSpreadSheetTableItem.SetVisible(AValue: Boolean);
begin
  History.BeginAction(TdxSpreadSheetHistoryChangeRowColumnItemAction);
  try
    if History.CanAddCommand then
      History.AddCommand(TdxSpreadSheetHistoryChangeItemCommand.Create(Self));
    if AValue and Visible and not DefaultSize and (FSize = 0) then
      Size := Owner.DefaultSize
    else
      if AValue <> FVisible then
      begin
        FVisible := AValue;
        Changed;
      end;
  finally
    History.EndAction;
  end;
end;

procedure TdxSpreadSheetTableItem.ProcessStyleChanges(APrevStyle, ANewStyle: TdxSpreadSheetCellStyleHandle);
var
  ACell: TdxSpreadSheetCell;
  AList: TdxFastList;
  APrevStyleIsDefault: Boolean;
  AStyleMergeHelper: TdxSpreadSheetTableItemStyleMergeHelper;
  I: Integer;
begin
  AStyleMergeHelper := TdxSpreadSheetTableItemStyleMergeHelper.Create(Self, APrevStyle, ANewStyle);
  try
    View.BeginUpdate;
    try
      if View.FMergeCellStylesLockCount = 0 then
      begin
        APrevStyleIsDefault := APrevStyle = Style.CellStyles.DefaultStyle;
        AList := TdxFastList.Create(CellCount);
        try
          Owner.GetOppositeItems.ForEach(
            procedure (AItem: TdxDynamicListItem; AData: Pointer)
            var
              ATableItem: TdxSpreadSheetTableItem;
            begin
              ATableItem := TdxSpreadSheetTableItem(AItem);
              ACell := ATableItem.GetCell(Index);
              if (ACell = nil) and not ATableItem.Style.IsDefault then
              begin
                ACell := ATableItem.CreateCell(Index);
                if APrevStyleIsDefault then
                  ACell.Style.Handle := ATableItem.Style.Handle;
              end;
              if ACell <> nil then
                AList.Add(ACell);
            end);

          for I := 0 to AList.Count - 1 do
            AStyleMergeHelper.ProcessCell(TdxSpreadSheetCell(AList.List[I]));
        finally
          AList.Free;
        end;
      end;
      if not IsCustomSize and AStyleMergeHelper.IsUpdateBestFitNeeded then
        MarkBestFitDirty;
    finally
      View.EndUpdate;
    end;
  finally
    AStyleMergeHelper.Free;
  end;
end;

function TdxSpreadSheetTableItem.IsValueDefined(AIndex: Integer): Boolean;
var
  ACell: TdxSpreadSheetCell;
begin
  ACell := GetCell(AIndex);
  Result := (ACell <> nil) and ACell.HasValue;
end;

procedure TdxSpreadSheetTableItem.MarkBestFitDirty;
begin
  // do nothing
end;

procedure TdxSpreadSheetTableItem.SetDefaultSize(AValue: Boolean);
begin
  if FDefaultSize <> AValue then
  begin
    BeforeResize;
    try
      FDefaultSize := AValue;
      FIsCustomSize := not DefaultSize;
    finally
      AfterResize;
    end;
    Changed;
  end;
end;

procedure TdxSpreadSheetTableItem.SetSize(const AValue: Integer);
var
  AIsReading: Boolean;
begin
  if DefaultSize or (Size <> AValue) then
  begin
    AIsReading := sssReading in Owner.SpreadSheet.State; 
    if not AIsReading then
      BeforeResize;
    History.BeginAction(TdxSpreadSheetHistoryChangeRowColumnItemAction);
    try
      if History.CanAddCommand then
        History.AddCommand(TdxSpreadSheetHistoryChangeItemCommand.Create(Self));
      FSize := AValue;
      FDefaultSize := False;
      FIsCustomSize := True;
      FVisible := True;
    finally
      History.EndAction;
      if not AIsReading then
      begin
        AfterResize;
        Changed;
      end;
    end;
  end;
end;

procedure TdxSpreadSheetTableItem.SetSizeEx(const ASize: TSize);
begin
  SetSize(ASize.cx);
end;

{ TdxSpreadSheetTableItems }

constructor TdxSpreadSheetTableItems.Create(AOwner: TdxSpreadSheetTableView);
begin
  inherited Create;
  FView := AOwner;
end;

function TdxSpreadSheetTableItems.CreateItem(const AIndex: Integer): TdxSpreadSheetTableItem;
begin
  Result := TdxSpreadSheetTableItem(inherited CreateItem(AIndex));
end;

function TdxSpreadSheetTableItems.GetDistance(AStartIndex, AFinishIndex: Integer): Integer;
var
  ADefaultSize: Integer;
  APrevItemIndex: Integer;
  ASize: Integer;
begin
  AStartIndex := Min(AStartIndex, GetMaxItemIndex);
  AFinishIndex := Min(AFinishIndex, GetMaxItemIndex);

  if AFinishIndex >= AStartIndex then
  begin
    ASize := 0;
    APrevItemIndex := AStartIndex;
    ADefaultSize := DefaultSize;
    ForEach(
      procedure (AItem: TdxDynamicListItem; AData: Pointer)
      begin
        Inc(ASize, (AItem.Index - APrevItemIndex) * ADefaultSize);
        Inc(ASize, TdxSpreadSheetTableItem(AItem).Size);
        APrevItemIndex := AItem.Index + 1;
      end,
      AStartIndex, AFinishIndex);

    Result := ASize + (AFinishIndex - APrevItemIndex + 1) * ADefaultSize;
  end
  else
    Result := 0;
end;

function TdxSpreadSheetTableItems.GetItemIndexFromDistance(AStartIndex: Integer; ADistance: Integer): Integer;
var
  AInc: Integer;
begin
  AInc := Sign(ADistance);
  Result := AStartIndex + AInc;
  if AInc = 0 then Exit;
  ADistance := Abs(ADistance);
  while InRange(Result, 1, GetMaxItemIndex - 1) and (ADistance > 0) do
  begin
    Dec(ADistance, GetItemSize(Result));
    if ADistance > 0 then
      Result := Result + AInc;
  end;
end;

function TdxSpreadSheetTableItems.GetNextVisibleItemIndex(AIndex: Integer; AGoForward: Boolean): Integer;
begin
  Result := Max(0, Min(AIndex + ValueIncr[AGoForward], GetMaxItemIndex));
  if AGoForward then
    while (Result < GetMaxItemIndex) and not GetItemVisible(Result) do
      Inc(Result)
  else
    while (Result > 0) and not GetItemVisible(Result) do
      Dec(Result);

  if not GetItemVisible(Result) then
    Result := GetNextVisibleItemIndex(Result, not AGoForward);
end;

function TdxSpreadSheetTableItems.GetPosition(AIndex: Integer): Integer;
var
  ADefaultSize: Integer;
  APrevItemIndex: Integer;
  ASize: Integer;
begin
  Result := 0;
  if AIndex >= 0 then
  begin
    ASize := 0;
    ADefaultSize := DefaultSize;
    APrevItemIndex := 0;

    ForEach(
      procedure (AItem: TdxDynamicListItem; AData: Pointer)
      begin
        Inc(ASize, (AItem.Index - APrevItemIndex) * ADefaultSize);
        Inc(ASize, TdxSpreadSheetTableItem(AItem).Size);
        APrevItemIndex := AItem.Index + 1;
      end,
      0, AIndex - 1);

    Result := ASize + (AIndex - APrevItemIndex) * ADefaultSize;
    if (GetFixedItemIndex >= 0) and (AIndex > GetFixedItemIndex) then
      Inc(Result, View.SpreadSheet.OptionsView.FrozenPaneSeparatorWidth - 1);
  end;
end;

procedure TdxSpreadSheetTableItems.AfterResize;
begin
  if not View.History.InProcess then
    View.Containers.AfterResize;
end;

procedure TdxSpreadSheetTableItems.BeforeResize;
begin
  if not View.History.InProcess then
    View.Containers.BeforeResize;
end;

procedure TdxSpreadSheetTableItems.Changed;
begin
  View.AddChanges([sscLayout]);
end;

function TdxSpreadSheetTableItems.GetItemDisplayText(const AItem: TdxSpreadSheetTableItem): TdxUnicodeString;
begin
  Result := GetItemText(AItem.Index);
end;

function TdxSpreadSheetTableItems.GetItemSize(AIndex: Integer): Integer;
var
  AItem: TdxSpreadSheetTableItem;
begin
  AItem := Items[AIndex];
  if AItem <> nil then
    Result := AItem.Size
  else
    Result := DefaultSize;
end;

function TdxSpreadSheetTableItems.GetItemState(AIndex: Integer): TcxButtonState;
begin
  Result := cxbsDefault;
end;

function TdxSpreadSheetTableItems.GetItemStyle(AIndex: Integer): TdxSpreadSheetCellStyle;
var
  AItem: TdxSpreadSheetTableItem;
begin
  AItem := GetItem(AIndex);
  if AItem <> nil then
    Result := AItem.Style
  else
    Result := nil;
end;

function TdxSpreadSheetTableItems.GetItemText(AIndex: Integer): TdxUnicodeString;
begin
  Result := IntToStr(AIndex + 1);
end;

function TdxSpreadSheetTableItems.GetItemVisible(AIndex: Integer): Boolean;
var
  AItem: TdxSpreadSheetTableItem;
begin
  AItem := GetItem(AIndex);
  Result := (AItem = nil) or (AItem.Size > 0);
end;

function TdxSpreadSheetTableItems.GetMaxItemIndex: Integer;
begin
  Result := MaxInt;
end;

procedure TdxSpreadSheetTableItems.ResizeItem(AIndex, ADelta: Integer);
var
  AItem: TdxSpreadSheetTableItem;
begin
  if ADelta > 0 then
  begin
    if not GetItemVisible(AIndex + 1) then
    begin
      Inc(AIndex);
      while not GetItemVisible(AIndex + 1) do
        Inc(AIndex);
      Items[AIndex].Size := ADelta;
    end
    else
    begin
      AItem := TdxSpreadSheetTableItem(ItemNeeded(AIndex));
      AItem.Size := AItem.Size + ADelta;
    end;
  end
  else
  begin
    while (AIndex >= 0) and (ADelta < 0) do
    begin
      Inc(ADelta, GetItemSize(AIndex));
      AItem := TdxSpreadSheetTableItem(ItemNeeded(AIndex));
      AItem.Size := Max(0, ADelta);
      Dec(AIndex)
    end;
  end;
end;

function TdxSpreadSheetTableItems.GetFirst: TdxSpreadSheetTableItem;
begin
  Result := TdxSpreadSheetTableItem(FFirst);
end;

function TdxSpreadSheetTableItems.GetItem(AIndex: Integer): TdxSpreadSheetTableItem;
begin
  Result := TdxSpreadSheetTableItem(inherited Items[AIndex]);
end;

function TdxSpreadSheetTableItems.GetLast: TdxSpreadSheetTableItem;
begin
  Result := TdxSpreadSheetTableItem(FLast);
end;

function TdxSpreadSheetTableItems.GetSpreadSheet: TdxCustomSpreadSheet;
begin
  Result := View.SpreadSheet;
end;

procedure TdxSpreadSheetTableItems.SetItem(AIndex: Integer; const AValue: TdxSpreadSheetTableItem);
begin
  inherited Items[AIndex] := AValue;
end;

{ TdxSpreadSheetTableItemStyle }

procedure TdxSpreadSheetTableItemStyle.Changed;
begin
  if Owner.Style = Self then
    View.Changed;
end;

function TdxSpreadSheetTableItemStyle.GetSpreadSheet: TdxCustomSpreadSheet;
begin
  Result := View.SpreadSheet
end;

procedure TdxSpreadSheetTableItemStyle.ProcessStyleChanges(APrevStyle, ANewStyle: TdxSpreadSheetCellStyleHandle);
begin
  Owner.ProcessStyleChanges(APrevStyle, ANewStyle);
end;

function TdxSpreadSheetTableItemStyle.GetOwner: TdxSpreadSheetTableItem;
begin
  Result := TdxSpreadSheetTableItem(inherited Owner);
end;

function TdxSpreadSheetTableItemStyle.GetView: TdxSpreadSheetTableView;
begin
  Result := Owner.View;
end;

{ TdxSpreadSheetTableColumn }

function TdxSpreadSheetTableColumn.CreateCell(ARow: Integer): TdxSpreadSheetCell;
var
  ARowItem: TdxSpreadSheetTableItem;
begin
  ARowItem := View.Rows.CreateItem(ARow);
  Result := ARowItem.CreateCell(Index)
end;

function TdxSpreadSheetTableColumn.GetCell(ARow: Integer): TdxSpreadSheetCell;
var
  ARowItem: TdxSpreadSheetTableItem;
begin
  ARowItem := View.Rows[ARow];
  if ARowItem <> nil then
    Result := ARowItem.Cells[Index]
  else
    Result := nil;
end;

function TdxSpreadSheetTableColumn.GetCellCount: Integer;
begin
  Result := View.Dimensions.Bottom + 1;
end;

function TdxSpreadSheetTableColumn.IsItemArea(const AArea: TRect): Boolean;
begin
  Result := (AArea.Right - AArea.Left = 0) and (AArea.Left = Index);
end;

function TdxSpreadSheetTableColumn.MeasureCellSize(ACanvas: TcxCanvas; ACell: TdxSpreadSheetCell): Integer;
begin
  Result := ACell.MeasureSize(ACanvas, False);
end;

procedure TdxSpreadSheetTableColumn.ShiftIndex(ADelta: Integer);
begin
  if CellCount > 0 then
  begin
    View.Rows.ForEach(
      procedure (AItem: TdxDynamicListItem; AData: Pointer)
      var
        ACell: TdxSpreadSheetCell;
      begin
        ACell := TdxSpreadSheetTableRow(AItem).Cells[Index];
        if ACell <> nil then
          ACell.ShiftIndex(ADelta);
      end);
  end;
  inherited ShiftIndex(ADelta);
end;

{ TdxSpreadSheetTableColumns }

function TdxSpreadSheetTableColumns.CreateItem(const AIndex: Integer): TdxSpreadSheetTableColumn;
begin
  Result := TdxSpreadSheetTableColumn(inherited CreateItem(AIndex));
end;

function TdxSpreadSheetTableColumns.GetDefaultSize: Integer;
begin
  Result := View.Options.DefaultColumnWidth;
end;

function TdxSpreadSheetTableColumns.GetFixedItemIndex: Integer;
begin
  Result := View.FrozenColumn;
end;

function TdxSpreadSheetTableColumns.GetItemClass: TdxDynamicListItemClass;
begin
  Result := TdxSpreadSheetTableColumn;
end;

function TdxSpreadSheetTableColumns.GetItemText(AIndex: Integer): TdxUnicodeString;
begin
  Result := TdxSpreadSheetColumnHelper.NameByIndex(AIndex, View.SpreadSheet.OptionsView.R1C1Reference);
end;

function TdxSpreadSheetTableColumns.GetItemState(AIndex: Integer): TcxButtonState;
begin
  if View.Selection.IsColumnSelected(AIndex) then
    Result := cxbsHot
  else
    Result := inherited GetItemState(AIndex);
end;

function TdxSpreadSheetTableColumns.GetMaxItemIndex: Integer;
begin
  Result := dxSpreadSheetMaxColumnIndex;
end;

function TdxSpreadSheetTableColumns.GetOppositeItems: TdxSpreadSheetTableItems;
begin
  Result := View.Rows;
end;

procedure TdxSpreadSheetTableColumns.SetDefaultSize(const AValue: Integer);
begin
  View.Options.DefaultColumnWidth := AValue;
end;

function TdxSpreadSheetTableColumns.GetItem(AIndex: Integer): TdxSpreadSheetTableColumn;
begin
  Result := TdxSpreadSheetTableColumn(inherited Items[AIndex])
end;

{ TdxSpreadSheetTableRowCells }

constructor TdxSpreadSheetTableRowCells.Create(ARow: TdxSpreadSheetTableRow);
begin
  FRow := ARow;
end;

function TdxSpreadSheetTableRowCells.GetItemClass: TdxDynamicListItemClass;
begin
  Result := TdxSpreadSheetCell;
end;

{ TdxSpreadSheetTableRow }

constructor TdxSpreadSheetTableRow.Create(AOwner: TdxDynamicItemList; AIndex: Integer);
begin
  inherited Create(AOwner, AIndex);
  FCells := TdxSpreadSheetTableRowCells.Create(Self);
end;

destructor TdxSpreadSheetTableRow.Destroy;
begin
  FreeAndNil(FCells);
  inherited Destroy;
end;

procedure TdxSpreadSheetTableRow.ApplyBestFit;
begin
  inherited ApplyBestFit;
  FBestFitIsDirty := False;
end;

function TdxSpreadSheetTableRow.CreateCell(AColumn: Integer): TdxSpreadSheetCell;
var
  AIsNewlyCreated: Boolean;
begin
  Result := TdxSpreadSheetCell(FCells.CreateItem(AColumn, AIsNewlyCreated));
  if AIsNewlyCreated then
  begin
    View.Columns.CreateItem(AColumn);
    if not (sssReading in View.SpreadSheet.State) then
    begin
      InitializeCellStyle(Result.Style, Result.Column);
      Result.InitializeReferenceToArrayFormula;
    end;
  end;
end;

function TdxSpreadSheetTableRow.GetCell(AColumn: Integer): TdxSpreadSheetCell;
begin
  Result := TdxSpreadSheetCell(FCells[AColumn]);
end;

function TdxSpreadSheetTableRow.GetCellCount: Integer;
begin
  if FCells.FLast <> nil then
    Result := FCells.FLast.Index + 1
  else
    Result := 0;
end;

procedure TdxSpreadSheetTableRow.InitializeCellStyle(ACellStyle: TdxSpreadSheetCellStyle; AColumn: TdxSpreadSheetTableColumn);
begin
  ACellStyle.BeginUpdate;
  try
    if Style.IsDefault then
      ACellStyle.Assign(AColumn.Style)
    else
      ACellStyle.Assign(Style);
    ACellStyle.Borders[bTop] := Style.Borders[bTop];
    ACellStyle.Borders[bBottom] := Style.Borders[bBottom];
    ACellStyle.Borders[bLeft] := AColumn.Style.Borders[bLeft];
    ACellStyle.Borders[bRight] := AColumn.Style.Borders[bRight];
  finally
    ACellStyle.EndUpdate;
  end;
end;

function TdxSpreadSheetTableRow.IsItemArea(const AArea: TRect): Boolean;
begin
  Result := (AArea.Bottom - AArea.Top = 0) and (AArea.Top = Index);
end;

procedure TdxSpreadSheetTableRow.MarkBestFitDirty;
begin
  FBestFitIsDirty := True;
  Changed;
end;

function TdxSpreadSheetTableRow.MeasureCellSize(ACanvas: TcxCanvas; ACell: TdxSpreadSheetCell): Integer;
begin
  Result := ACell.MeasureSize(ACanvas, True);
end;

procedure TdxSpreadSheetTableRow.SetSizeEx(const ASize: TSize);
begin
  SetSize(ASize.cy);
end;

{ TdxSpreadSheetTableRows }

function TdxSpreadSheetTableRows.CreateItem(const AIndex: Integer): TdxSpreadSheetTableRow;
begin
  Result := TdxSpreadSheetTableRow(inherited CreateItem(AIndex));
end;

procedure TdxSpreadSheetTableRows.CheckBestFit;
begin
  if not View.SpreadSheet.OptionsView.CellAutoHeight or View.History.InProcess then Exit;
  ForEach(
    procedure (AItem: TdxDynamicListItem; AData: Pointer)
    var
      ARow: TdxSpreadSheetTableRow;
    begin
      ARow := TdxSpreadSheetTableRow(AItem);
      if ARow.FBestFitIsDirty then
      begin
        ARow.FBestFitIsDirty := False;
        if not ARow.IsCustomSize then
          ARow.ApplyBestFit;
      end;
    end);
end;

procedure TdxSpreadSheetTableRows.ForEachCell(const AArea: TRect;
  AProc: TdxDynamicItemListForEachProcRef; AGoForward: Boolean = True);
begin
  ForEach(
    procedure (AItem: TdxDynamicListItem; AData: Pointer)
    begin
      TdxSpreadSheetTableRow(AItem).RowCells.ForEach(AProc, PRect(AData)^.Left, PRect(AData)^.Right, AGoForward);
    end,
    AArea.Top, AArea.Bottom, AGoForward, @AArea);
end;

function TdxSpreadSheetTableRows.GetDefaultSize: Integer;
begin
  Result := View.Options.DefaultRowHeight;
end;

function TdxSpreadSheetTableRows.GetFixedItemIndex: Integer;
begin
  Result := View.FrozenRow;
end;

function TdxSpreadSheetTableRows.GetItemClass: TdxDynamicListItemClass;
begin
  Result := TdxSpreadSheetTableRow;
end;

function TdxSpreadSheetTableRows.GetItemState(AIndex: Integer): TcxButtonState;
begin
  if View.Selection.IsRowSelected(AIndex) then
    Result := cxbsHot
  else
    Result := inherited GetItemState(AIndex);
end;

function TdxSpreadSheetTableRows.GetMaxItemIndex: Integer;
begin
  Result := dxSpreadSheetMaxRowIndex;
end;

function TdxSpreadSheetTableRows.GetOppositeItems: TdxSpreadSheetTableItems;
begin
  Result := View.Columns;
end;

procedure TdxSpreadSheetTableRows.SetDefaultSize(const AValue: Integer);
begin
  View.Options.DefaultRowHeight := AValue;
end;

function TdxSpreadSheetTableRows.GetItem(AIndex: Integer): TdxSpreadSheetTableRow;
begin
  Result := TdxSpreadSheetTableRow(inherited Items[AIndex]);
end;

{ TdxSpreadSheetTableViewOptionsPrint }

constructor TdxSpreadSheetTableViewOptionsPrint.Create(AOwner: TPersistent);
begin
  inherited Create(AOwner);
  FHeaderFooter := TdxSpreadSheetTableViewOptionsPrintHeaderFooter.Create;
  FPage := TdxSpreadSheetTableViewOptionsPrintPage.Create;
  FPagination := TdxSpreadSheetTableViewOptionsPrintPagination.Create;
  FPrinting := TdxSpreadSheetTableViewOptionsPrintPrinting.Create;
  FSource := TdxSpreadSheetTableViewOptionsPrintSource.Create;
end;

destructor TdxSpreadSheetTableViewOptionsPrint.Destroy;
begin
  FreeAndNil(FHeaderFooter);
  FreeAndNil(FPage);
  FreeAndNil(FPagination);
  FreeAndNil(FPrinting);
  FreeAndNil(FSource);
  inherited Destroy;
end;

procedure TdxSpreadSheetTableViewOptionsPrint.Reset;
begin
  HeaderFooter.Reset;
  Page.Reset;
  Pagination.Reset;
  Printing.Reset;
  Source.Reset;
end;

procedure TdxSpreadSheetTableViewOptionsPrint.DoAssign(ASource: TPersistent);
begin
  inherited DoAssign(ASource);
  if ASource is TdxSpreadSheetTableViewOptionsPrint then
  begin
    HeaderFooter := TdxSpreadSheetTableViewOptionsPrint(ASource).HeaderFooter;
    Page := TdxSpreadSheetTableViewOptionsPrint(ASource).Page;
    Pagination := TdxSpreadSheetTableViewOptionsPrint(ASource).Pagination;
    Printing := TdxSpreadSheetTableViewOptionsPrint(ASource).Printing;
    Source := TdxSpreadSheetTableViewOptionsPrint(ASource).Source;
  end;
end;

procedure TdxSpreadSheetTableViewOptionsPrint.SetHeaderFooter(AValue: TdxSpreadSheetTableViewOptionsPrintHeaderFooter);
begin
  FHeaderFooter.Assign(AValue);
end;

procedure TdxSpreadSheetTableViewOptionsPrint.SetPage(AValue: TdxSpreadSheetTableViewOptionsPrintPage);
begin
  FPage.Assign(AValue);
end;

procedure TdxSpreadSheetTableViewOptionsPrint.SetPagination(AValue: TdxSpreadSheetTableViewOptionsPrintPagination);
begin
  FPagination.Assign(AValue);
end;

procedure TdxSpreadSheetTableViewOptionsPrint.SetPrinting(AValue: TdxSpreadSheetTableViewOptionsPrintPrinting);
begin
  FPrinting.Assign(AValue);
end;

procedure TdxSpreadSheetTableViewOptionsPrint.SetSource(AValue: TdxSpreadSheetTableViewOptionsPrintSource);
begin
  FSource.Assign(AValue);
end;

{ TdxSpreadSheetTableViewOptions }

constructor TdxSpreadSheetTableViewOptions.Create(AOwner: TPersistent);
begin
  inherited Create(AOwner);
  FDefaultColumnWidth := dxSpreadSheetDefaultColumnWidth;
  FDefaultRowHeight := dxSpreadSheetDefaultRowHeight;
  FHeaders := bDefault;
  FGridLines := bDefault;
  FHorizontalScrollBar := bDefault;
  FVerticalScrollBar := bDefault;
  FZoomFactor := dxSpreadSheetDefaultZoomFactor;
  FShowFormulas := bDefault;
  FZeroValues := bDefault;
end;

procedure TdxSpreadSheetTableViewOptions.Assign(Source: TPersistent);
begin
  inherited Assign(Source);
  if Source is TdxSpreadSheetTableViewOptions then
  begin
    FGridLines := TdxSpreadSheetTableViewOptions(Source).GridLines;
    FHeaders := TdxSpreadSheetTableViewOptions(Source).Headers;
    FHorizontalScrollBar := TdxSpreadSheetTableViewOptions(Source).HorizontalScrollBar;
    FShowFormulas := TdxSpreadSheetTableViewOptions(Source).ShowFormulas;
    FVerticalScrollBar := TdxSpreadSheetTableViewOptions(Source).VerticalScrollBar;
    FZeroValues := TdxSpreadSheetTableViewOptions(Source).ZeroValues;
    FZoomFactor := TdxSpreadSheetTableViewOptions(Source).ZoomFactor;
    Changed;
  end;
end;

function TdxSpreadSheetTableViewOptions.GetActualGridLines: Boolean;
begin
  Result := dxDefaultBooleanToBoolean(GridLines, OptionsView.GridLines);
end;

function TdxSpreadSheetTableViewOptions.GetActualHeaders: Boolean;
begin
  Result := dxDefaultBooleanToBoolean(Headers, OptionsView.Headers);
end;

function TdxSpreadSheetTableViewOptions.GetActualHorizontalScrollBar: Boolean;
begin
  Result := dxDefaultBooleanToBoolean(HorizontalScrollBar, OptionsView.HorizontalScrollBar);
end;

function TdxSpreadSheetTableViewOptions.GetActualShowFormulas: Boolean;
begin
  Result := dxDefaultBooleanToBoolean(ShowFormulas, OptionsView.ShowFormulas);
end;

function TdxSpreadSheetTableViewOptions.GetActualVerticalScrollBar: Boolean;
begin
  Result := dxDefaultBooleanToBoolean(VerticalScrollBar, OptionsView.VerticalScrollBar);
end;

function TdxSpreadSheetTableViewOptions.GetActualZeroValues: Boolean;
begin
  Result := dxDefaultBooleanToBoolean(ZeroValues, OptionsView.ZeroValues);
end;

function TdxSpreadSheetTableViewOptions.GetOptionsView: TdxSpreadSheetOptionsView;
begin
  Result := View.SpreadSheet.OptionsView;
end;

procedure TdxSpreadSheetTableViewOptions.SetDefaultColumnWidth(AValue: Integer);
begin
  if AValue <> DefaultColumnWidth then
  begin
    FDefaultColumnWidth := Max(AValue, 0);
    Changed;
  end;
end;

procedure TdxSpreadSheetTableViewOptions.SetDefaultRowHeight(AValue: Integer);
begin
  if AValue <> DefaultRowHeight then
  begin
    FDefaultRowHeight := Max(AValue, 0);
    Changed;
  end;
end;

procedure TdxSpreadSheetTableViewOptions.SetGridLines(AValue: TdxDefaultBoolean);
begin
  if AValue <> FGridLines then
  begin
    FGridLines := AValue;
    Changed;
  end;
end;

procedure TdxSpreadSheetTableViewOptions.SetHeaders(AValue: TdxDefaultBoolean);
begin
  if FHeaders <> AValue then
  begin
    FHeaders := AValue;
    Changed;
  end;
end;

procedure TdxSpreadSheetTableViewOptions.SetShowFormulas(AValue: TdxDefaultBoolean);
begin
  if AValue <> FShowFormulas then
  begin
    FShowFormulas := AValue;
    View.AddChanges([sscData]);
  end;
end;

procedure TdxSpreadSheetTableViewOptions.SetHorizontalScrollBar(AValue: TdxDefaultBoolean);
begin
  if FHorizontalScrollBar <> AValue then
  begin
    FHorizontalScrollBar := AValue;
    Changed;
  end;
end;

procedure TdxSpreadSheetTableViewOptions.SetVerticalScrollBar(AValue: TdxDefaultBoolean);
begin
  if FVerticalScrollBar <> AValue then
  begin
    FVerticalScrollBar := AValue;
    Changed;
  end;
end;

procedure TdxSpreadSheetTableViewOptions.SetZeroValues(AValue: TdxDefaultBoolean);
begin
  if FZeroValues <> AValue then
  begin
    FZeroValues := AValue;
    View.AddChanges([sscData]);
  end;
end;

procedure TdxSpreadSheetTableViewOptions.SetZoomFactor(AValue: Integer);
begin
  AValue := Max(dxSpreadSheetMinimumZoomFactor, Min(dxSpreadSheetMaximumZoomFactor, AValue));
  if AValue <> FZoomFactor then
  begin
    FZoomFactor := AValue;
    View.AddChanges([sscLayout, sscZoom]);
  end;
end;

{ TdxSpreadSheetTableViewHitTest }

procedure TdxSpreadSheetTableViewHitTest.Calculate(const AHitPoint: TPoint);
begin
  inherited Calculate(AHitPoint);
  View.ViewInfo.InitHitTest(Self);
end;

function TdxSpreadSheetTableViewHitTest.GetActualHitPoint: TPoint;
begin
  Result := cxPointScale(HitPoint, 100, View.ZoomFactor);
end;

function TdxSpreadSheetTableViewHitTest.GetContainer: TdxSpreadSheetContainer;
begin
  Result := TdxSpreadSheetContainerViewInfo(HitObject).Owner;
end;

function TdxSpreadSheetTableViewHitTest.GetView: TdxSpreadSheetTableView;
begin
  Result := TdxSpreadSheetTableView(Owner);
end;

{ TdxSpreadSheetTableViewSelection }

constructor TdxSpreadSheetTableViewSelection.Create(AOwner: TdxSpreadSheetTableView);
begin
  inherited Create;
  FView := AOwner;
  FItems := TObjectList<TcxRect>.Create;
  FItems.Add(TcxRect.Create(nil));
end;

destructor TdxSpreadSheetTableViewSelection.Destroy;
begin
  FreeAndNil(FItems);
  inherited Destroy;
end;

procedure TdxSpreadSheetTableViewSelection.Add(const AArea: TRect;
  AShift: TShiftState = []; AFocusedRow: Integer = -1; AFocusedColumn: Integer = -1);
var
  ARect: TcxRect;
begin
  ChangeLocked := True;
  try
    CheckClear(AShift);

    if (AFocusedColumn < AArea.Left) or (AFocusedColumn > AArea.Right) then
      AFocusedColumn := AArea.Left;
    if (AFocusedRow < AArea.Top) or (AFocusedRow > AArea.Bottom) then
      AFocusedRow := AArea.Top;

    if (FFocusedColumn <> AFocusedColumn) or (FFocusedRow <> AFocusedRow) then
      if View.DoActiveCellChanging(AFocusedColumn, AFocusedRow) then
      begin
        FFocusedColumn := AFocusedColumn;
        FFocusedRow := AFocusedRow;
      end;

    ARect := TcxRect.Create(nil);
    ARect.OnChange := ItemChanged;
    if dxSpreadSheetIsEntireRowOrColumn(AArea) then
      ARect.Rect := AArea
    else
      ARect.Rect := MergedCells.ExpandArea(AArea);

    FItems.Add(ARect);
  finally
    ChangeLocked := False;
  end;
end;

procedure TdxSpreadSheetTableViewSelection.Clear;
begin
  FItems.Clear;
  FFocusedColumn := -1;
  FFocusedRow := -1;
  Changed;
end;

function TdxSpreadSheetTableViewSelection.HasArea(const AArea: TRect): Boolean;
var
  I: Integer;
begin
  Result := False;
  for I := 0 to FItems.Count - 1 do
    if cxRectIsEqual(AArea, FItems[I].Rect) then
    begin
      Result := True;
      Break;
    end;
end;

function TdxSpreadSheetTableViewSelection.IsCellSelected(ARow, AColumn: Integer): Boolean;
var
  I: Integer;
begin
  Result := (ARow = FocusedRow) and (AColumn = FocusedColumn);
  I := 0;
  while not Result and (I < Count) do
  begin
    Result := dxSpreadSheetContains(Items[I].Rect, ARow, AColumn);
    Inc(I);
  end;
end;

function TdxSpreadSheetTableViewSelection.IsColumnSelected(AColumn: Integer): Boolean;
var
  I: Integer;
begin
  I := 0;
  Result := AColumn = FocusedColumn;
  while not Result and (I < Count) do
  begin
    Result := InRange(AColumn, Items[I].Left, Items[I].Right);
    Inc(I);
  end;
end;

function TdxSpreadSheetTableViewSelection.IsRowSelected(ARow: Integer): Boolean;
var
  I: Integer;
begin
  I := 0;
  Result := ARow = FocusedRow;
  while not Result and (I < Count) do
  begin
    Result := InRange(ARow, Items[I].Top, Items[I].Bottom);
    Inc(I);
  end;
end;

function TdxSpreadSheetTableViewSelection.IsEntireColumnSelected(AColumn: Integer): Boolean;
var
  AItem: TcxRect;
  I: Integer;
begin
  Result := False;
  for I := 0 to Count - 1 do
  begin
    AItem := Items[I];
    if dxSpreadSheetIsEntireColumn(AItem.Rect) then
    begin
      if InRange(AColumn, AItem.Left, AItem.Right) then
        Exit(True);
    end;
  end;
end;

function TdxSpreadSheetTableViewSelection.IsEntireRowSelected(ARow: Integer): Boolean;
var
  AItem: TcxRect;
  I: Integer;
begin
  Result := False;
  for I := 0 to Count - 1 do
  begin
    AItem := Items[I];
    if dxSpreadSheetIsEntireRow(AItem.Rect) then
    begin
      if InRange(ARow, AItem.Top, AItem.Bottom) then
        Exit(True);
    end;
  end;
end;

procedure TdxSpreadSheetTableViewSelection.SelectAll;
begin
  Add(Rect(0, 0, MaxInt, MaxInt), [], View.TopRow, View.LeftColumn);
end;

procedure TdxSpreadSheetTableViewSelection.SelectCell(ARow, AColumn: Integer; AShift: TShiftState = []);
begin
  Add(Rect(AColumn, ARow, AColumn, ARow), AShift);
end;

procedure TdxSpreadSheetTableViewSelection.SelectColumns(AStartColumn, AFinishColumn: Integer; AShift: TShiftState = []);
var
  ARow: Integer;
begin
  ARow := FocusedRow;
  if not (ssShift in AShift) or not cxInRange(ARow, View.TopRow, View.BottomRow) or not SelectionByColumns then
    ARow := View.TopRow;
  Add(Rect(Min(AStartColumn, AFinishColumn), 0,
    Max(AStartColumn, AFinishColumn), MaxInt), AShift, ARow);
end;

procedure TdxSpreadSheetTableViewSelection.SelectRows(AStartRow, AFinishRow: Integer; AShift: TShiftState = []);
var
  AColumn: Integer;
begin
  AColumn := FocusedColumn;
  if not (ssShift in AShift) and not cxInRange(AColumn, View.LeftColumn, View.RightColumn) or not SelectionByRows then
    AColumn := View.LeftColumn;
  Add(Rect(0, Min(AStartRow, AFinishRow), MaxInt, Max(AStartRow, AFinishRow)), AShift, -1, AColumn);
end;

procedure TdxSpreadSheetTableViewSelection.SetFocused(ARow, AColumn: Integer; AShift: TShiftState);

  function CheckMoveFocus: Boolean;
  begin
    Result := False;
    if IsCellSelected(ARow, AColumn) and View.DoActiveCellChanging(AColumn, ARow) then
    begin
      CheckClear(AShift);
      Result := Count > 0;
      if Result then
      begin
        FFocusedRow := ARow;
        FFocusedColumn := AColumn;
        Changed;
      end;
    end;
  end;

begin
  dxSpreadSheetValidate(ARow, 0, dxSpreadSheetMaxRowIndex);
  dxSpreadSheetValidate(AColumn, 0, dxSpreadSheetMaxColumnIndex);
  if (ARow <> FFocusedRow) or (AColumn <> FFocusedColumn) then
  begin
    ChangeLocked := True;
    try
      if not CheckMoveFocus then
        Add(MergedCells.ExpandArea(AColumn, ARow), AShift, ARow, AColumn);
      if (FocusedRow = ARow) and (FocusedColumn = AColumn) then
        View.MakeVisible(FocusedRow, FocusedColumn);
    finally
      ChangeLocked := False;
    end;
  end;
end;

procedure TdxSpreadSheetTableViewSelection.Changed;
begin
  if not ChangeLocked then
    View.SelectionChanged;
end;

procedure TdxSpreadSheetTableViewSelection.CheckClear(const AShift: TShiftState);
begin
  if [ssCtrl, ssShift] * AShift = [] then
    Clear;
end;

function TdxSpreadSheetTableViewSelection.GetActiveSelection: TRect;
var
  I: Integer;
begin
  Result := Rect(FocusedColumn, FocusedRow, FocusedColumn, FocusedRow);
  for I := 0 to Count - 1 do
    if dxSpreadSheetContains(Items[I].Rect, FocusedRow, FocusedColumn) then
    begin
      Result := Items[I].Rect;
      Break;
    end;
end;

procedure TdxSpreadSheetTableViewSelection.GetState(ARow, AColumn: Integer; var AFocused, ASelected: Boolean);
var
  I: Integer;
begin
  ASelected := False;
  AFocused := dxSpreadSheetContains(MergedCells.ExpandArea(FocusedColumn, FocusedRow), ARow, AColumn);
  for I := 0 to Count - 1 do
  begin
    ASelected := dxSpreadSheetContains(Items[I].Rect, ARow, AColumn);
    if ASelected then Break;
  end
end;

procedure TdxSpreadSheetTableViewSelection.ItemChanged(Sender: TObject);
begin
  ChangeLocked := True;
  try
    TcxRect(Sender).Rect := cxRectAdjust(TcxRect(Sender).Rect);
  finally
    ChangeLocked := False;
  end;
end;

procedure TdxSpreadSheetTableViewSelection.LoadFromStream(AReader: TcxReader);
var
  ARect: TcxRect;
  I, ACount, AIndex: Integer;
begin
  ChangeLocked := True;
  try
    FItems.Clear;
    FFocusedRow := AReader.ReadInteger;
    FFocusedColumn := AReader.ReadInteger;
    AIndex := AReader.ReadInteger;
    if AIndex > 0 then
      View.Controller.FocusedContainer := View.Containers[AIndex]
    else
      View.Controller.FocusedContainer := nil;
    ACount := AReader.ReadInteger;
    for I := 0 to ACount - 1 do
    begin
      ARect := TcxRect.Create(nil);
      ARect.Rect := AReader.ReadRect;
      ARect.OnChange := ItemChanged;
      FItems.Add(ARect);
    end;
  finally
    ChangeLocked := False;
    Changed;
  end;
end;

procedure TdxSpreadSheetTableViewSelection.SaveToStream(AWriter: TcxWriter);
var
  I: Integer;
begin
  AWriter.WriteInteger(FFocusedRow);
  AWriter.WriteInteger(FFocusedColumn);
  if View.Controller.FocusedContainer <> nil then
    AWriter.WriteInteger(View.Controller.FocusedContainer.Index)
  else
    AWriter.WriteInteger(-1);
  AWriter.WriteInteger(Count);
  for I := 0 to Count - 1 do
    AWriter.WriteRect(Items[I].Rect);
end;

function TdxSpreadSheetTableViewSelection.GetArea: TRect;
var
  I: Integer;
begin
  Result := Rect(FocusedColumn, FocusedRow, FocusedColumn, FocusedRow);
  I := 0;
  if Count = 1 then
    Result := Items[0].Rect
  else
    while I < Count do
    begin
      Result := dxSpreadSheetCellsUnion(Result, Items[I].Rect);
      Inc(I);
    end;
end;

function TdxSpreadSheetTableViewSelection.GetChangeLocked: Boolean;
begin
  Result := (FLockCount > 0) or View.IsLocked;
end;

function TdxSpreadSheetTableViewSelection.GetCount: Integer;
begin
  Result := FItems.Count;
end;

function TdxSpreadSheetTableViewSelection.GetFocusedCell: TdxSpreadSheetCell;
begin
  Result := View.Cells[FocusedRow, FocusedColumn];
end;

function TdxSpreadSheetTableViewSelection.GetFocusedContainer: TdxSpreadSheetContainer;
begin
  Result := View.Controller.FocusedContainer;
end;

function TdxSpreadSheetTableViewSelection.GetItem(AIndex: Integer): TcxRect;
begin
  Result := FItems[AIndex];
end;

function TdxSpreadSheetTableViewSelection.GetMergedCells: TdxSpreadSheetMergedCellList;
begin
  Result := View.MergedCells;
end;

function TdxSpreadSheetTableViewSelection.GetSelectionByColumns: Boolean;
begin
  Result := (Count > 0) and (Items[Count - 1].Bottom = MaxInt);
end;

function TdxSpreadSheetTableViewSelection.GetSelectionByRows: Boolean;
begin
  Result := (Count > 0) and (Items[Count - 1].Right = MaxInt);
end;

procedure TdxSpreadSheetTableViewSelection.SetChangeLocked(AValue: Boolean);
begin
  if AValue then
    Inc(FLockCount)
  else
    Dec(FLockCount);
  if FLockCount = 0 then
    Changed;
end;

procedure TdxSpreadSheetTableViewSelection.SetFocusedCell(AValue: TdxSpreadSheetCell);
begin
  if AValue <> nil then
    SetFocused(AValue.RowIndex, AValue.ColumnIndex, []);
end;

procedure TdxSpreadSheetTableViewSelection.SetFocusedColumn(AValue: Integer);
begin
  SetFocused(FocusedRow, AValue, []);
end;

procedure TdxSpreadSheetTableViewSelection.SetFocusedContainer(const Value: TdxSpreadSheetContainer);
begin
  View.Controller.FocusedContainer := Value;
end;

procedure TdxSpreadSheetTableViewSelection.SetFocusedRow(AValue: Integer);
begin
  SetFocused(AValue, FocusedColumn, []);
end;

{ TdxSpreadSheetTableViewEditingController }

constructor TdxSpreadSheetTableViewEditingController.Create(AOwner: TdxSpreadSheetTableViewController);
begin
  inherited Create(AOwner.View.SpreadSheet);
  FOwner := AOwner;
  FEditStyleFont := TFont.Create;
  FReferencesHighlighter := CreateReferencesHighlighter;
  FDefaultEditProperties := CreateDefaultEditProperties;
  FDefaultEditProperties.HideSelection := False;
  FDefaultEditProperties.WantReturns := True;
  FDefaultEdit := CreateDefaultEdit;
  FEditStyle := CreateEditStyle;
end;

destructor TdxSpreadSheetTableViewEditingController.Destroy;
begin
  FreeAndNil(FReferencesHighlighter);
  FreeAndNil(FDefaultEditProperties);
  FreeAndNil(FDefaultEdit);
  FreeAndNil(FEditData);
  FreeAndNil(FEditStyleFont);
  FreeAndNil(FEditStyle);
  inherited Destroy;
end;

procedure TdxSpreadSheetTableViewEditingController.ShowEdit;
begin
  FEditValue := '';
  ReplacementMode := False;
  if PrepareEdit(True) then
  begin
    FEdit.Activate(FEditData);
    EditActivated;
  end;
end;

procedure TdxSpreadSheetTableViewEditingController.ShowEditByKey(AKey: Char);
begin
  FEditValue := AKey;
  ReplacementMode := True;
  if PrepareEdit(False) then
  begin
    FEdit.Activate(FEditData);
    EditActivated;
  end;
end;

procedure TdxSpreadSheetTableViewEditingController.ShowEditByMouse(X, Y: Integer; AShift: TShiftState);
begin
  FEditValue := '';
  View.HitTest.Calculate(cxPoint(X, Y));
  if not View.HitTest.HitAtCell then
    Exit;

  ReplacementMode := False;
  if PrepareEdit(True) then
  begin
    FEdit.ActivateByMouse(AShift, X, Y, FEditData);
    EditActivated;
    if FEdit = DefaultEdit then
      DefaultEdit.CaretPos := GetCaretPosByMousePos;
  end;
end;

procedure TdxSpreadSheetTableViewEditingController.AssignEditStyle;
var
  AStyle: TcxCustomEditStyleAccess;
begin
  EditStyleFont.Assign(CellViewInfo.Style.Font.Handle.GraphicObject);
  EditStyleFont.Size := MulDiv(EditStyleFont.Size, View.ZoomFactor, 100);

  AStyle := TcxCustomEditStyleAccess(EditStyle);
  AStyle.FAssignedValues := AStyle.FAssignedValues + [svFont, svColor, svButtonTransparency];
  AStyle.StyleData.Font := EditStyleFont;
  AStyle.StyleData.Color := CellViewInfo.BackgroundColor;
  AStyle.StyleData.FontColor := CellViewInfo.TextColor;
  AStyle.ButtonTransparency := ebtHideInactive;
  AStyle.Changed;
  AStyle.BorderStyle := ebsNone;
  AStyle.TransparentBorder := False;
  Edit.Style.Assign(EditStyle);
end;

function TdxSpreadSheetTableViewEditingController.CanInitEditing: Boolean;
begin
  Result := View.Active and View.SpreadSheet.OptionsBehavior.Editing;
  if Result then
  begin
    if SpreadSheet.DragAndDropState = ddsNone then
      View.MakeFocusedCellVisible;
    if (CellViewInfo <> nil) and not (View.Options.Protected and CellViewInfo.Style.Locked) then
    begin
      FPropertiesValue := DefaultEditProperties;
      View.DoEditing(FPropertiesValue, Result);
    end
    else
      Result := False;
  end;
end;

function TdxSpreadSheetTableViewEditingController.CanRedo: Boolean;
begin
  Result := IsEditing and not VarIsNull(RedoValue) and VarEquals(Edit.EditValue, EditValue);
end;

function TdxSpreadSheetTableViewEditingController.CanUndo: Boolean;
begin
  Result := IsEditing and Edit.EditModified;
end;

function TdxSpreadSheetTableViewEditingController.CanUpdateEditValue: Boolean;
begin
  Result := inherited CanUpdateEditValue and (CellViewInfo <> nil);
end;

function TdxSpreadSheetTableViewEditingController.CanUpdateMultilineEditHeight: Boolean;
begin
  Result := CanUpdateEditValue and not Edit.IsHiding;
end;

function TdxSpreadSheetTableViewEditingController.CreateDefaultEdit: TcxCustomMemo;
begin
  Result := TdxSpreadSheetInplaceEdit.Create(Self);
end;

function TdxSpreadSheetTableViewEditingController.CreateDefaultEditProperties: TcxCustomMemoProperties;
begin
  Result := TcxRichEditProperties.Create(View);
  TcxRichEditProperties(Result).AutoSelect := False;
  TcxRichEditProperties(Result).WantReturns := False;
  TcxRichEditProperties(Result).WantTabs := False;
  TcxRichEditProperties(Result).PlainText := True;
end;

function TdxSpreadSheetTableViewEditingController.CreateEditStyle: TcxEditStyle;
begin
  Result := TcxEditStyle.Create(nil, True);
end;

function TdxSpreadSheetTableViewEditingController.CreateReferencesHighlighter: TdxSpreadSheetTableViewReferencesHighlighter;
begin
  Result := TdxSpreadSheetTableViewEditingCellReferencesHighlighter.Create(View);
end;

procedure TdxSpreadSheetTableViewEditingController.ClearEditingItem;
begin
end;

function TdxSpreadSheetTableViewEditingController.IsPartOfArrayFormula(ASender: TdxSpreadSheetCell): Boolean;
begin
  Result := not cxRectIsNull(View.GetAreaReferencedByArrayFormula(ASender.RowIndex, ASender.ColumnIndex));
end;

procedure TdxSpreadSheetTableViewEditingController.DoHideEdit(Accept: Boolean);
var
  AEditValue: Variant;
begin
  if FEdit = nil then
    Exit;
  if Accept then
  begin
    FEdit.Deactivate; 
    AEditValue := FEdit.EditValue;
  end;
  if Edit <> nil then
  begin
    UninitEdit;
    Edit.EditModified := False;
    if SpreadSheet.CanFocusEx and (Edit <> nil) and Edit.IsFocused then
      SpreadSheet.SetFocus;
  end;
  try
    if Accept then
      SetValue(AEditValue);
  finally
    HideInplaceEditor;
    ReferencesHighlighter.Calculate('');
    FEditing := False;
    View.Pack;
    View.Invalidate;
    if not SpreadSheet.IsDestroying then
    begin
      View.DoEdited;
      HistoryChanged;
    end;
  end;
end;

procedure TdxSpreadSheetTableViewEditingController.DoUpdateEdit;
begin
  if not IsEditing or (Edit = nil) then
    Exit;

  if EditPreparing then
  begin
    View.DoInitEdit(Edit);
    UpdateEditPosition;
    Edit.Visible := True;
  end;
end;

procedure TdxSpreadSheetTableViewEditingController.EditActivated;
begin
  if Edit = DefaultEdit then
  begin
    DefaultEdit.SelectAll;
    DefaultEdit.SelStart := MaxInt;
  end;
  ReferencesHighlighter.CalculateColors;
  ReferencesHighlighter.Calculate(Edit);
  Edit.EditModified := False;
  RedoValue := Null;
  HistoryChanged;
end;

procedure TdxSpreadSheetTableViewEditingController.EditChanged(Sender: TObject);
begin
  MultilineEditTextChanged;
  inherited EditChanged(Sender);
  ReferencesHighlighter.Calculate(Edit);
  View.DoEditChanged;
  View.Invalidate;
  HistoryChanged;
end;

procedure TdxSpreadSheetTableViewEditingController.EditFocusChanged(Sender: TObject);
begin
  inherited EditFocusChanged(Sender);
  SpreadSheet.FocusChanged;
end;

procedure TdxSpreadSheetTableViewEditingController.EditKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited EditKeyDown(Sender, Key, Shift);
  if Assigned(SpreadSheet.OnKeyDown) then
    SpreadSheet.OnKeyDown(SpreadSheet, Key, Shift);
  case Key of
    VK_ESCAPE:
      HideEdit(False);
    VK_TAB:
      if [ssCtrl] * Shift = [] then
        HideEdit(True);
    Word('Z'):
      if ssCtrl in Shift then
      begin
        Undo;
        Key := 0;
      end;
    Word('Y'):
      if ssCtrl in Shift then
      begin
        Redo;
        Key := 0;
      end;
  end;
  if ReplacementMode then
    case Key of
      VK_UP, VK_DOWN, VK_LEFT, VK_RIGHT,
      VK_NEXT, VK_PRIOR,
      VK_HOME, VK_END:
        HideEdit(True);
    end;

  Controller.KeyDown(Key, Shift);

  if Edit = nil then
    Key := 0;
  HistoryChanged;
end;

procedure TdxSpreadSheetTableViewEditingController.EditKeyPress(Sender: TObject; var Key: Char);
begin
  if Assigned(SpreadSheet.OnKeyPress) then
    SpreadSheet.OnKeyPress(SpreadSheet, Key);
  if Key = #27 then
    Key := #0;
end;

procedure TdxSpreadSheetTableViewEditingController.EditKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Assigned(SpreadSheet.OnKeyUp) then
    SpreadSheet.OnKeyUp(SpreadSheet, Key, Shift);
end;

procedure TdxSpreadSheetTableViewEditingController.EditValueChanged(Sender: TObject);
begin
  inherited EditValueChanged(Sender);
  View.DoEditValueChanged;
end;

function TdxSpreadSheetTableViewEditingController.GetAdjustedMultilineEditBounds: TRect;
var
  AClientBounds: TRect;
  AColumn: Integer;
  AMergedArea: TRect;
  ARow: Integer;
  AText: string;
  R: TRect;
begin
  Result := GetFocusedCellBounds;
  if Edit.Visible then
    Result.BottomRight := Edit.BoundsRect.BottomRight;

  R := cxRectSetSize(Result, 0, 0);
  AClientBounds := SpreadSheet.ClientBounds;
  cxScreenCanvas.Font := DefaultEdit.Style.Font;
  AText := DefaultEdit.Text;
  if (AText <> '') and dxCharInSet(AText[Length(AText)], [#13, #10]) then
    AText := AText + ' ';
  Result := cxRectSetSize(Result, DefaultEditProperties.GetEditContentSize(
    cxScreenCanvas, DefaultEdit.Style, True, AText, cxDefaultEditSizeProperties));
  cxScreenCanvas.Dormant;

  Result := cxRectSetLeft(cxRectSetTop(Result, R.Top), R.Left);
  AMergedArea := View.MergedCells.CheckCell(View.Selection.FocusedRow, View.Selection.FocusedColumn);

  AColumn := AMergedArea.Left;
  while (AColumn < dxSpreadSheetMaxColumnCount) and ((R.Right < Result.Right) or (AColumn <= AMergedArea.Right)) do
  begin
    Inc(R.Right, View.Columns.GetItemSize(AColumn));
    Inc(AColumn);
  end;
  Result.Right := Min(R.Right - 1, AClientBounds.Right);

  ARow := AMergedArea.Top;
  while (ARow < dxSpreadSheetMaxRowCount) and ((R.Bottom < Result.Bottom) or (ARow <= AMergedArea.Bottom)) do
  begin
    Inc(R.Bottom, View.Rows.GetItemSize(ARow));
    Inc(ARow);
  end;
  Result.Bottom := Min(R.Bottom - 1, AClientBounds.Bottom);

  if IsRectEmpty(GetFocusedCellBounds) then
    Result := cxRectSetOrigin(Result, cxInvisiblePoint);
end;

function TdxSpreadSheetTableViewEditingController.GetCancelEditingOnExit: Boolean;
begin
  Result := False;
end;

function TdxSpreadSheetTableViewEditingController.GetCaretPosByMousePos: TPoint;
var
  P: TPoint;
  S: string;
begin
  Result := cxNullPoint;
  P := DefaultEdit.ScreenToClient(GetMouseCursorPos);
  if P.X <= 0 then Exit;
  Dec(P.X, 2);
  Result.X := 1;
  Result.Y := Min(DefaultEdit.Lines.Count, P.Y div cxTextHeight(DefaultEdit.Style.Font));
  S := DefaultEdit.Lines[Result.Y];
  while (Result.X <= Length(S)) and (cxTextWidth(DefaultEdit.Style.Font, Copy(S, 1, Result.X)) < P.X) do
    Inc(Result.X);
end;

function TdxSpreadSheetTableViewEditingController.GetEditParent: TWinControl;
begin
  Result := SpreadSheet;
end;

function TdxSpreadSheetTableViewEditingController.GetFocusedCellBounds: TRect;
var
  ABounds: TRect;
begin
  ABounds := CellViewInfo.DisplayBounds;
  if CellViewInfo.IsMerged then
    ABounds := View.ViewInfo.GetAreaBounds(CellViewInfo.Row, CellViewInfo.Column, CellViewInfo.MergedCell.Area, ABounds);

  Result := View.AbsoluteToScreen(CellViewInfo.ContentRect(ABounds));
  if cxRectIntersect(Result, Result, View.ViewInfo.CellsArea) then
    Result := cxRectScale(cxRectInflate(Result, 2, 1, 2, 0), View.Options.ZoomFactor, 100);
end;

function TdxSpreadSheetTableViewEditingController.GetIsEditing: Boolean;
begin
  Result := View.Active and FEditing;
end;

function TdxSpreadSheetTableViewEditingController.GetValue: TcxEditValue;
var
  AArrayFormulaArea: TRect;
  ACell: TdxSpreadSheetCell;
begin
  if EditValue = '' then
  begin
    ACell := Cell;
    if ACell <> nil then
    begin
      EditValue := ACell.AsString;
      if ACell.View.IsCellReferencedByArrayFormulaArea(ACell.RowIndex, ACell.ColumnIndex, AArrayFormulaArea) then
        EditValue := ACell.View.Cells[AArrayFormulaArea.Top, AArrayFormulaArea.Left].AsFormula.AsText
      else
      if Cell.IsFormula then
        EditValue := ACell.AsFormula.AsText;
    end
    else
      EditValue := '';
  end;
  Result := EditValue;
  View.DoInitEditValue(Edit, Result);
end;

procedure TdxSpreadSheetTableViewEditingController.MultilineEditTextChanged;
var
  R: TRect;
begin
  if CanUpdateMultilineEditHeight then
  begin
    R := GetAdjustedMultilineEditBounds;
    if not cxRectIsEqual(Edit.BoundsRect, R) then
    begin
      Edit.BoundsRect := R;
      Edit.ScrollContent(dirUp);
    end;
  end;
end;

procedure TdxSpreadSheetTableViewEditingController.HistoryChanged;
begin
  View.SpreadSheet.DoHistoryChanged;
end;

procedure TdxSpreadSheetTableViewEditingController.SetValue(const AValue: TcxEditValue);
var
  P: TPoint;
  ACell: TdxSpreadSheetCell;
  ASelectedArea: TRect;
begin
  ACell := Cell;
  if (ACell <> nil) and IsPartOfArrayFormula(ACell) then
  begin
    ASelectedArea := View.GetArrayFormulaArea(ACell.RowIndex, ACell.ColumnIndex);
    if not(IsArrayFormula and View.CanModifyDataInArea(ASelectedArea, cmmReplace)) then
      raise EdxSpreadSheetCannotChangePartOfArrayError.Create(sdxErrorCannotChangePartOfArray)
    else
      View.AddArrayFormula(AValue, ASelectedArea);
  end
  else
    if VarIsNull(AValue) and (ACell <> nil) then
      ACell.SetText(AValue, True)
    else
      if not VarIsNull(AValue) then
      begin
        if CellViewInfo.IsMerged then
          P := CellViewInfo.MergedCell.Area.TopLeft
        else
          P := cxPoint(CellViewInfo.Column, CellViewInfo.Row);
        View.BeginUpdate;
        try
          if IsArrayFormula then
            View.AddArrayFormula(AValue, View.GetArrayFormulaArea(P.Y, P.X))
          else
            View.CreateCell(P.Y, P.X).SetText(AValue, True);
        finally
          View.EndUpdate;
        end;
      end;
end;

procedure TdxSpreadSheetTableViewEditingController.StartEditingByTimer;
begin
end;

procedure TdxSpreadSheetTableViewEditingController.UpdateEditPosition;
begin
  if Edit <> nil then
    Edit.BoundsRect := GetAdjustedMultilineEditBounds;
end;

procedure TdxSpreadSheetTableViewEditingController.UpdateInplaceParamsPosition;
begin
  // do nothing
end;

function TdxSpreadSheetTableViewEditingController.PrepareEdit(AIsMouseEvent: Boolean): Boolean;
begin
  Result := IsEditing;
  if EditPreparing or EditHiding or not CanInitEditing then
    Exit;

  FEditPreparing := True;
  try
    Result := SpreadSheet.Focused;
    if not Result then
      Exit;

    if CellViewInfo <> nil then
      View.CreateCell(CellViewInfo.Row, CellViewInfo.Column);

    Result := CellViewInfo <> nil;
    if Result then
    try
      FreeAndNil(FEditData);
      if PropertiesValue = DefaultEditProperties then
      begin
        FEdit := DefaultEdit;
        DefaultEdit.Properties.Assign(DefaultEditProperties);
        DefaultEdit.Properties.HideSelection := False;
      end
      else
        FEdit := EditList.GetEdit(PropertiesValue);

      AssignEditStyle;
      Edit.Visible := False;
      Edit.Parent := nil;
    except
      Result := False;
      raise;
    end;

    FEditing := Result;
    if Edit <> nil then
    begin
      CellViewInfo.Invalidate;
      InitEdit;
      UpdateEditPosition;
    end
    else
      Result := False;
  finally
    FEditPreparing := False;
  end;
  if Result then
    View.Invalidate;
end;


procedure TdxSpreadSheetTableViewEditingController.ReplaceEditValue(const AValue: Variant);
begin
  if Edit is TcxCustomTextEdit then
  begin
    SendMessage(Edit.Handle, WM_SETREDRAW, 0, 0);
    try
      Edit.LockChangeEvents(False, False);
      TcxCustomTextEdit(Edit).EditValue := AValue;
    finally
      TcxCustomTextEdit(Edit).SelStart := MaxInt;
      TcxCustomTextEdit(Edit).SelLength := 0;
      Edit.LockChangeEvents(True, False);
      SendMessage(Edit.Handle, WM_SETREDRAW, 1, 1);
    end;
  end
  else
    Edit.EditValue := AValue;
end;

procedure TdxSpreadSheetTableViewEditingController.Redo;
begin
  if not CanRedo then Exit;
  ReplaceEditValue(RedoValue);
  HistoryChanged;
end;

procedure TdxSpreadSheetTableViewEditingController.Undo;
begin
  if not CanUndo then Exit;
  RedoValue := Edit.EditValue;
  ReplaceEditValue(EditValue);
  Edit.EditModified := False;
  HistoryChanged;
end;

function TdxSpreadSheetTableViewEditingController.GetCell: TdxSpreadSheetCell;
begin
  if CellViewInfo <> nil then
  begin
    Result := CellViewInfo.Cell;
    if CellViewInfo.IsMerged then
      Result := CellViewInfo.MergedCell.ActiveCell;
  end
  else
    Result := nil;
end;

function TdxSpreadSheetTableViewEditingController.GetCellViewInfo: TdxSpreadSheetTableViewCellViewInfo;
begin
  Result := View.ViewInfo.FocusedCell;
end;

function TdxSpreadSheetTableViewEditingController.GetCharCount: Integer;
begin
  if Edit is TcxCustomMemo then
    Result := Length(TcxCustomMemo(Edit).Lines[TcxCustomMemo(Edit).CaretPos.Y])
  else
    Result := Length(EditValue);
end;

function TdxSpreadSheetTableViewEditingController.GetSpreadSheet: TdxCustomSpreadSheet;
begin
  Result := View.SpreadSheet;
end;

function TdxSpreadSheetTableViewEditingController.GetView: TdxSpreadSheetTableView;
begin
  Result := Controller.View;
end;

function TdxSpreadSheetTableViewEditingController.GetSelCount: Integer;
begin
  if Edit is TcxCustomTextEdit then
    Result := TcxCustomTextEdit(Edit).SelLength
  else
    Result := 0;
end;

{ TdxSpreadSheetInplaceEdit }

constructor TdxSpreadSheetInplaceEdit.Create(AOwner: TdxSpreadSheetTableViewEditingController);
begin
  FOwner := AOwner;
  inherited Create(nil, True);
end;

procedure TdxSpreadSheetInplaceEdit.KeyDown(var Key: Word; Shift: TShiftState);
var
  AShiftStates: TShiftState;
const
  AArrayFormulaShiftStates: TShiftState = [ssCtrl, ssShift];
  AValidShiftStatesForEditing: TShiftState = [ssShift, ssCtrl, ssAlt];
begin
   if Key = VK_RETURN then
   begin
     AShiftStates := AValidShiftStatesForEditing * Shift;
     if (AShiftStates = [ssShift]) or (AShiftStates  = []) then
       Owner.HideEdit(True)
     else
     begin
       if (AShiftStates = [ssCtrl]) or (AShiftStates = AArrayFormulaShiftStates) then
       begin
         Owner.IsArrayFormula := AShiftStates = AArrayFormulaShiftStates;
         Owner.HideEdit(True);
         Owner.IsArrayFormula := False;
       end
       else
         if AShiftStates =  [ssAlt] then
           InnerRich.SelText := dxCRLF;
       Key := 0;
     end;
     if Key <> 0 then
       Controller.KeyDown(Key, Shift);
  end;
  inherited KeyDown(Key, Shift);
end;

function TdxSpreadSheetInplaceEdit.GetController: TdxSpreadSheetTableViewController;
begin
  Result := FOwner.Controller;
end;

function TdxSpreadSheetInplaceEdit.GetSpreadSheet: TdxCustomSpreadSheet;
begin
  Result := FOwner.SpreadSheet;
end;

{ TdxSpreadSheetTableViewController }

constructor TdxSpreadSheetTableViewController.Create(AOwner: TObject);
begin
  inherited Create(AOwner);
  FEditingController := CreateEditingController;
  FScrollingTimer := TcxTimer.Create(View.SpreadSheet);
  FScrollingTimer.Enabled := False;
  FScrollingTimer.Interval := 25;
  FScrollingTimer.OnTimer := ScrollingTimerHandler;
end;

destructor TdxSpreadSheetTableViewController.Destroy;
begin
  FreeAndNil(FEditingController);
  FreeAndNil(FScrollingTimer);
  inherited Destroy;
end;

procedure TdxSpreadSheetTableViewController.ApplyBestFitForSelectedHeaderCells(ACells: TdxSpreadSheetViewInfoCellsList);
var
  ACell: TdxSpreadSheetTableViewHeaderCellViewInfo;
  I: Integer;
begin
  View.BeginUpdate;
  try
    for I := 0 to ACells.Count - 1 do
    begin
      ACell := TdxSpreadSheetTableViewHeaderCellViewInfo(ACells.Items[I]);
      if ACell.State = cxbsHot then
        ACell.ApplyBestFit;
    end;
  finally
    View.EndUpdate;
  end;
end;

function TdxSpreadSheetTableViewController.CalculateNewFocusedRowIndex(AOffsetFromFirstRow: Integer): Integer;
begin
  Result := ViewInfo.FirstScrollableRow;
  while (AOffsetFromFirstRow > 0) and (Result <= View.BottomRow) do
  begin
    if Rows.GetItemVisible(Result) then
      Dec(AOffsetFromFirstRow);
    Inc(Result);
  end;
  dxSpreadSheetValidate(Result, ViewInfo.FirstScrollableRow, View.BottomRow);
end;

function TdxSpreadSheetTableViewController.CanFocusOnClick: Boolean;
begin
  Result := HitTest.HitObject <> EditingController.ReferencesHighlighter;
end;

function TdxSpreadSheetTableViewController.CheckColumnIndex(AColumn, AIncrement: Integer): Integer;
var
  R: TRect;
begin
  Result := Columns.GetNextVisibleItemIndex(AColumn, AIncrement > 0);
  R := MergedCells.ExpandArea(Result, FocusedRow);
  if cxRectWidth(R) = 0 then
    Exit;

  if (AIncrement > 0) then
  begin
    if (Result > R.Left) and (AColumn + AIncrement > R.Left) then
      Result := Columns.GetNextVisibleItemIndex(R.Right, True)
  end
  else
    if (Result < R.Right) and (AColumn + AIncrement < R.Right) then
      Result := Columns.GetNextVisibleItemIndex(R.Left, False)
end;

function TdxSpreadSheetTableViewController.CheckCommand(const AKey: TdxUnicodeChar): Boolean;
begin
  Result := True;
  case AKey of
    '-':
      View.DeleteCells;
    '+':
      View.InsertCells;
    'A':
      Selection.SelectAll;
    'C':
      View.CopyToClipboard;
    'X':
      View.CutToClipboard;
    'V':
      View.PasteFromClipboard;
    'Z':
      SpreadSheet.History.Undo(1);
    'Y':
      SpreadSheet.History.Redo(1);
  else
    Result := False;
  end;
end;

function TdxSpreadSheetTableViewController.CheckRowIndex(ARow, AIncrement: Integer): Integer;
var
  R: TRect;
begin
  Result := Rows.GetNextVisibleItemIndex(ARow, AIncrement > 0);
  R := MergedCells.ExpandArea(FocusedColumn, ARow);
  if R.Height = 0 then
    Exit;

  if AIncrement > 0 then
  begin
    if (Result > R.Top) and (ARow + AIncrement > R.Top) then
      Result := Rows.GetNextVisibleItemIndex(R.Bottom, True)
  end
  else
    if (Result < R.Bottom) and (ARow + AIncrement < R.Bottom) then
      Result := Rows.GetNextVisibleItemIndex(R.Top, False)
end;

procedure TdxSpreadSheetTableViewController.CheckScrollArea(const P: TPoint);
begin
  FScrollingTimer.Enabled := ((View.SpreadSheet.DragAndDropState = ddsInProcess) or (SelectionMode <> smNone)) and
    not PtInRect(cxRectInflate(ViewInfo.ScrollableArea, -dxSpreadSheetScrollAreaWidth), P) and
    (PtInRect(ViewInfo.ScrollableArea, P) or not PtInRect(ViewInfo.CellsArea, P));
end;

function TdxSpreadSheetTableViewController.ContainerCanDelete: Boolean;
begin
  Result := View.CanDelete;
end;

function TdxSpreadSheetTableViewController.CreateEditingController: TdxSpreadSheetTableViewEditingController;
begin
  Result := TdxSpreadSheetTableViewEditingController.Create(Self);
end;

procedure TdxSpreadSheetTableViewController.DblClick;
var
  ACell: TdxSpreadSheetTableViewHeaderCellViewInfo;
  APos: TPoint;
begin
  APos := View.SpreadSheet.GetMouseCursorClientPos;
  View.HitTest.Calculate(APos);
  if View.HitTest.HitAtResizeArea then
  begin
    if HitTest.HitAtColumnHeader or HitTest.HitAtRowHeader then
    begin
      ACell := TdxSpreadSheetTableViewHeaderCellViewInfo(HitTest.HitObject);
      if ACell.State = cxbsHot then
      begin
        if HitTest.HitAtColumnHeader then
          ApplyBestFitForSelectedHeaderCells(ViewInfo.ColumnsHeader)
        else
          ApplyBestFitForSelectedHeaderCells(ViewInfo.RowsHeader);
      end
      else
        ACell.ApplyBestFit;
    end;
  end
  else
    EditingController.ShowEditByMouse(APos.X, APos.Y, FShiftState);
end;

procedure TdxSpreadSheetTableViewController.DoKeyDown(AKey: Word; AShift: TShiftState);
begin
  KeyDown(AKey, AShift);
end;

procedure TdxSpreadSheetTableViewController.FocusNextColumnInTheSelection(AGoForward: Boolean);
var
  R: TcxRect;
  AIndex, ARow, AColumn: Integer;
begin
  if not GetSelectionAreaByFocusedCell(R, AIndex) then
    DoKeyDown(TabKeyToArrowKey[not AGoForward], [])
  else
  begin
    ARow := FocusedRow;
    AColumn := FocusedColumn;
    AColumn := CheckColumnIndex(AColumn, ValueIncr[AGoForward]);
    if AColumn > R.Right then
    begin
      AColumn := CheckColumnIndex(R.Left - 1, 1);
      ARow := CheckRowIndex(FocusedRow, 1);
    end
    else
      if AColumn < R.Left then
      begin
        AColumn := CheckColumnIndex(R.Right + 1, -1);
        ARow := CheckRowIndex(FocusedRow, -1);
      end;
    if not InRange(ARow, R.Top, R.Bottom) then
      GetNextSelectedAreaCell(AIndex, AGoForward, ARow, AColumn);
    Selection.SetFocused(ARow, AColumn, [ssCtrl])
  end;
end;

procedure TdxSpreadSheetTableViewController.FocusNextRowInTheSelection(AGoForward: Boolean);
var
  R: TcxRect;
  AIndex, ARow, AColumn: Integer;
begin
  if GetSelectionAreaByFocusedCell(R, AIndex) then
  begin
    ARow := FocusedRow;
    AColumn := FocusedColumn;
    ARow := CheckRowIndex(ARow, ValueIncr[AGoForward]);
    if ARow > R.Bottom then
    begin
      ARow := CheckRowIndex(R.Top - 1, 1);
      AColumn := CheckColumnIndex(FocusedColumn, 1);
    end
    else
      if ARow < R.Top then
      begin
        ARow := CheckRowIndex(R.Bottom + 1, -1);
        AColumn := CheckColumnIndex(FocusedColumn, -1);
      end;

    if not InRange(AColumn, R.Left, R.Right) then
      GetNextSelectedAreaCell(AIndex, AGoForward, ARow, AColumn);
    Selection.SetFocused(ARow, AColumn, [ssCtrl])
  end
  else
    DoKeyDown(ReturnKeyToArrowKey[not AGoForward], []);
end;

procedure TdxSpreadSheetTableViewController.GetCellFromMouseCursor(var ARow, AColumn: Integer);
begin
  ARow := -1;
  AColumn := -1;
  if HitTest.HitAtColumnHeader then
    AColumn := TdxSpreadSheetTableViewHeaderCellViewInfo(HitTest.HitObject).Index
  else
    if HitTest.HitAtRowHeader then
      ARow := TdxSpreadSheetTableViewHeaderCellViewInfo(HitTest.HitObject).Index
    else
      View.CellAtPoint(HitTest.ActualHitPoint, ARow, AColumn, True);
end;

procedure TdxSpreadSheetTableViewController.GetNextSelectedAreaCell(
  AIndex: Integer; AGoForward: Boolean; var ARow, AColumn: Integer);
begin
  AIndex := AIndex + ValueIncr[AGoForward];
  if AIndex >= Selection.Count then
    AIndex := 0
  else
    if AIndex < 0 then
      AIndex := Selection.Count - 1;
  ARow := CheckRowIndex(Selection[AIndex].Top - 1, 1);
  AColumn := CheckColumnIndex(Selection[AIndex].Left - 1, 1);
end;

function TdxSpreadSheetTableViewController.GetHitTest: TdxSpreadSheetCustomHitTest;
begin
  Result := HitTest;
end;

function TdxSpreadSheetTableViewController.GetNextNonEmptyCell(
  AItems: TdxSpreadSheetTableItems; AFocusedItemIndex, AFocusedCellIndex, AIncrement: Integer): Integer;
var
  AIndex: Integer;
  AItem: TdxSpreadSheetTableItem;
begin
  AItem := AItems.Items[AFocusedItemIndex];
  if AItem <> nil then
  begin
    AIndex := Min(AFocusedCellIndex, AItem.CellCount) + AIncrement;
    while InRange(AIndex, 0, AItem.CellCount - 1) do
    begin
      if AItem.Cells[AIndex] <> nil then
        Exit(AIndex);
      Inc(AIndex, AIncrement);
    end;
  end;
  if AIncrement > 0 then
    Result := AItems.GetOppositeItems.GetMaxItemIndex
  else
    Result := 0;
end;

function TdxSpreadSheetTableViewController.GetSelectionAreaByFocusedCell(var R: TcxRect; var AIndex: Integer): Boolean;
begin
  Result := False;
  R := nil;
  AIndex := Selection.Count - 1;
  while not Result and (AIndex >= 0) do
  begin
    R := Selection[AIndex];
    Result := dxSpreadSheetContains(R.Rect, FocusedRow, FocusedColumn) and (R.Width + R.Height > 1);
    if not Result then
      Dec(AIndex);
  end;
end;

function TdxSpreadSheetTableViewController.GetSelectionItem(ASide: TcxBorder; AShift: TShiftState): Integer;
var
  R: TRect;
begin
  if (ssShift in AShift) and (Selection.Count > 0) then
  begin
    R := Selection[Selection.Count - 1].Rect;
    case ASide of
      bLeft:
        Result := IfThen(R.Left < FocusedColumn, R.Left, IfThen(FocusedColumn < R.Right, R.Right, FocusedColumn));
      bTop:
        Result := IfThen(R.Top < FocusedRow, R.Top, IfThen(FocusedRow < R.Bottom, R.Bottom, FocusedRow));
      bRight:
        Result := IfThen(R.Right > FocusedColumn, R.Right, IfThen(FocusedColumn > R.Left, R.Left, FocusedColumn));
    else
      Result := IfThen(R.Bottom > FocusedRow, R.Bottom, IfThen(FocusedRow > R.Top, R.Top, FocusedRow));
    end;
  end
  else
    if ASide in [bLeft, bRight] then
      Result := FocusedColumn
    else
      Result := FocusedRow;
end;

procedure TdxSpreadSheetTableViewController.GetRedoActionCount(var ARedoCount: Integer);
begin
  if EditingController.IsEditing then
  begin
    if EditingController.CanRedo then
      ARedoCount := 1
    else
      ARedoCount := 0;
  end;
end;

procedure TdxSpreadSheetTableViewController.GetUndoActionCount(var AUndoCount: Integer);
begin
  if EditingController.IsEditing then
  begin
    if EditingController.CanUndo then
      AUndoCount := 1
    else
      AUndoCount := 0;
  end;
end;

procedure TdxSpreadSheetTableViewController.KeyDown(var Key: Word; Shift: TShiftState);
var
  AShift: TShiftState;
  ASelStart, ASelFinish, APosition, AFocusedRowOffset, ARow, AColumn: Integer;
begin
  if (FocusedContainer <> nil) and ContainerProcessKeyDown(Key, Shift) then
    Exit;
  if EditingController.IsEditing then
    Exit;

  case Key of
    VK_ADD:
      if ssCtrl in Shift then
        CheckCommand('+');

    VK_SUBTRACT:
      if ssCtrl in Shift then
        CheckCommand('-');

    VK_F2:
      EditingController.ShowEdit;

    VK_LEFT:
      if ssCtrl in Shift then
        SelectColumn(CheckColumnIndex(GetNextNonEmptyCell(Rows, FocusedRow, FocusedColumn, -1) + 1, -1), Shift - [ssCtrl])
      else
        SelectColumn(CheckColumnIndex(GetSelectionItem(bLeft, Shift), -1), Shift);

    VK_UP:
      if ssCtrl in Shift then
        SelectRow(CheckRowIndex(GetNextNonEmptyCell(Columns, FocusedColumn, FocusedRow, -1) + 1, -1), Shift - [ssCtrl])
      else
        SelectRow(CheckRowIndex(GetSelectionItem(bTop, Shift), -1), Shift);

    VK_DOWN:
      if ssCtrl in Shift then
        SelectRow(CheckRowIndex(GetNextNonEmptyCell(Columns, FocusedColumn, FocusedRow, 1) - 1, 1), Shift - [ssCtrl])
      else
        SelectRow(CheckRowIndex(GetSelectionItem(bBottom, Shift), 1), Shift);

    VK_RIGHT:
      if ssCtrl in Shift then
        SelectColumn(CheckColumnIndex(GetNextNonEmptyCell(Rows, FocusedRow, FocusedColumn, 1) - 1, 1), Shift - [ssCtrl])
      else
        SelectColumn(CheckColumnIndex(GetSelectionItem(bRight, Shift), 1), Shift);

    VK_PRIOR:
      begin
        AFocusedRowOffset := GetFocusedRowOffsetFromTopRow(GetSelectionItem(bTop, Shift));
        ViewInfo.Scroll(sbVertical, scPageUp, APosition);
        SelectRow(CalculateNewFocusedRowIndex(AFocusedRowOffset), Shift);
      end;

    VK_NEXT:
      begin
        AFocusedRowOffset := GetFocusedRowOffsetFromTopRow(GetSelectionItem(bBottom, Shift));
        ViewInfo.Scroll(sbVertical, scPageDown, APosition);
        SelectRow(CalculateNewFocusedRowIndex(AFocusedRowOffset), Shift);
      end;

    VK_HOME:
      if ssCtrl in Shift then
      begin
        if View.FrozenRow >= 0 then
          SetFocused(CheckRowIndex(View.FrozenRow, 1), CheckColumnIndex(View.Dimensions.Left, -1), Shift - [ssCtrl])
        else
          SetFocused(CheckRowIndex(View.Dimensions.Top, -1), CheckColumnIndex(View.Dimensions.Left, -1), Shift - [ssCtrl])
      end
      else
        SelectColumn(CheckColumnIndex(View.Dimensions.Left, -1), Shift);

    VK_END:
      if ssCtrl in Shift then
        SetFocused(CheckRowIndex(View.Dimensions.Bottom - 1, 1), CheckColumnIndex(View.Dimensions.Right - 1, 1), Shift - [ssCtrl])
      else
        if Rows[FocusedRow] = nil then
          SelectColumn(0, Shift)
        else
          SelectColumn(CheckColumnIndex(Rows[FocusedRow].CellCount - 1, 1), Shift);

    VK_TAB:
      if Selection.Count > 0 then
        FocusNextColumnInTheSelection(not (ssShift in Shift))
      else
        DoKeyDown(TabKeyToArrowKey[ssShift in Shift], []);

    VK_INSERT:
      begin
        if Shift = [ssCtrl]  then
          CheckCommand('C')
        else
          if Shift = [ssShift]  then
            CheckCommand('V')
      end;

    VK_BACK:
      if ssAlt in Shift then
      begin
        CheckCommand('Z');
        Key := 0;
      end;

    VK_RETURN:
      if Selection.Count > 0 then
        FocusNextRowInTheSelection(not (ssShift in Shift))
      else
        DoKeyDown(ReturnKeyToArrowKey[ssShift in Shift], []);

    VK_DELETE:
      if OptionsBehavior.Editing then
      begin
        if ssCtrl in Shift then
          CheckCommand(WideChar(Key))
        else
          if ssShift in Shift then
            CheckCommand('X')
          else
            View.ClearCellsValues;
      end;

    VK_F9:
      View.SpreadSheet.FormulaController.Calculate;

    VK_SPACE:
      if [ssShift, ssCtrl] * Shift <> [] then
      begin
        ARow := FocusedRow;
        AColumn := FocusedColumn;
        if ssCtrl in Shift then
        begin
          AShift := [ssShift];
          if (SelectionMode <> smColumns) and (Selection.Count <= 1) then
            AShift := [];
          ASelStart := Selection.GetActiveSelection.Left;
          ASelFinish := Selection.GetActiveSelection.Right;
          if Selection.IsEntireRowSelected(FocusedRow) then
            Selection.SelectAll
          else
            Selection.SelectColumns(ASelStart, ASelFinish, AShift);
        end;
        if ssShift in Shift then
        begin
          AShift := [ssShift];
          if (SelectionMode <> smRows) and (Selection.Count <= 1) then
            AShift := [];
          ASelStart := Selection.GetActiveSelection.Top;
          ASelFinish := Selection.GetActiveSelection.Bottom;
          if Selection.IsEntireColumnSelected(FocusedColumn) then
            Selection.SelectAll
          else
            Selection.SelectRows(ASelStart, ASelFinish, AShift);
        end;
        Selection.SetFocused(ARow, AColumn, [ssShift]);
      end;
  else
    if ssCtrl in Shift then
      CheckCommand(WideChar(Key));
  end;
end;

procedure TdxSpreadSheetTableViewController.KeyPress(var Key: Char);
begin
{  if Key = #13 then
    EditingController.HideEdit(True);
    }
  if Key < #$21 then
    Key := #0
  else
    EditingController.ShowEditByKey(Key);
end;

procedure TdxSpreadSheetTableViewController.DoMouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  ARow, AColumn: Integer;
begin
  SelectionMode := smNone;
  if HitTest.HitAtResizeArea or not PtInRect(ViewInfo.Bounds, Point(X, Y)) then
    Exit;

  if HitTest.HitAtContainer then
    FocusedContainer := HitTest.Container
  else
    if not HitTest.HitAtContainerSelection then
      FocusedContainer := nil;

  if FocusedContainer <> nil then
    Exit;
  
  GetCellFromMouseCursor(ARow, AColumn);
  FShiftState := Shift;
  if (ARow = -1) and (AColumn = -1) then
  begin
    if HitTest.HitAtColumnHeader then
      Selection.SelectAll;
  end
  else
    if ARow = -1 then
    begin
      if (Button <> mbRight) or not Selection.IsEntireColumnSelected(AColumn) then
      begin
        SetSelectionMode(FocusedRow, AColumn, Shift, smColumns);
        Selection.SelectColumns(AnchorColumn, AColumn, Shift);
      end;
    end
    else
      if AColumn = -1 then
      begin
        if (Button <> mbRight) or not Selection.IsEntireRowSelected(ARow) then
        begin
          SetSelectionMode(ARow, FocusedColumn, Shift, smRows);
          Selection.SelectRows(AnchorRow, ARow, Shift);
        end;
      end
      else
        if (Button <> mbRight) or not Selection.IsCellSelected(ARow, AColumn) then
          SetSelectionMode(ARow, AColumn, Shift, smCells);
end;

procedure TdxSpreadSheetTableViewController.DoMouseMove(Shift: TShiftState; X, Y: Integer);
var
  R: TcxRect;
  ARow, AColumn: Integer;
begin
  if not (ssLeft in Shift) or (SelectionMode = smNone) or (Selection.Count = 0) then
    Exit;

  FShiftState := Shift;
  CheckScrollArea(X, Y); 
  GetCellFromMouseCursor(ARow, AColumn);
  R := Selection.Items[Selection.Count - 1];
  dxSpreadSheetValidate(ARow, 0, dxSpreadSheetMaxRowIndex);
  dxSpreadSheetValidate(AColumn, 0, dxSpreadSheetMaxColumnIndex);
  case SelectionMode of
    smRows:
      R.Rect := cxRectAdjust(Rect(0, ARow, MaxInt, FocusedRow));
    smColumns:
      R.Rect := cxRectAdjust(Rect(AColumn, 0, FocusedColumn, MaxInt));
    smCells:
      R.Rect := MergedCells.ExpandArea(cxRectAdjust(Rect(AColumn, ARow, FocusedColumn, FocusedRow)));
  end;
end;

procedure TdxSpreadSheetTableViewController.DoMouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  SelectionMode := smNone;
  ScrollingTimer.Enabled := False;
end;

function TdxSpreadSheetTableViewController.DoMouseWheel(
  Shift: TShiftState; WheelDelta: Integer; const P: TPoint): Boolean;
begin
  EditingController.HideEdit(True);
  if ssCtrl in Shift then
  begin
    Result := True;
    View.Options.ZoomFactor := View.Options.ZoomFactor + Sign(WheelDelta) * 15;
  end
  else
  begin
    WheelDelta := Max(Abs(WheelDelta) div 2, 1) * Sign(WheelDelta);
    ViewInfo.FirstScrollableRow := Rows.GetItemIndexFromDistance(View.ViewInfo.FirstScrollableRow, -WheelDelta);
    ViewInfo.Validate;
    Result := True;
  end
end;

function TdxSpreadSheetTableViewController.Redo(const ARedoCount: Integer = 1): Boolean;
begin
  Result := EditingController.IsEditing;
  if Result then
    EditingController.Redo
  else
    Result := inherited Redo(ARedoCount);
end;

function TdxSpreadSheetTableViewController.Undo(const AUndoCount: Integer = 1): Boolean;
begin
  Result := EditingController.IsEditing;
  if Result then
    EditingController.Undo
  else
    Result := inherited Undo(AUndoCount);
end;

procedure TdxSpreadSheetTableViewController.ScrollingTimerHandler(Sender: TObject);
var
  R: TRect;
  P: TPoint;
  AScrollPos: Integer;
begin
  AScrollPos := 0;
  P := View.SpreadSheet.GetMouseCursorClientPos;
  R := cxRectInflate(ViewInfo.ScrollableArea, -dxSpreadSheetScrollAreaWidth);
  if (P.Y < R.Top) and not InRange(FocusedRow, 0, View.FrozenRow) and (SelectionMode <> smColumns) then
    ViewInfo.DoScroll(sbVertical, scLineUp, AScrollPos);
  if (P.X < R.Left) and not InRange(FocusedColumn, 0, View.FrozenColumn) and (SelectionMode <> smRows) then
    View.ViewInfo.DoScroll(sbHorizontal, scLineUp, AScrollPos);
  if (P.Y > R.Bottom) and (SelectionMode <> smColumns) then
    ViewInfo.DoScroll(sbVertical, scLineDown, AScrollPos);
  if (P.X > R.Right) and  (SelectionMode <> smRows) then
    ViewInfo.DoScroll(sbHorizontal, scLineDown, AScrollPos);
  if ViewInfo.IsDirty then
  begin
    ViewInfo.Validate;
    MouseMove(FShiftState, P.X, P.Y);
  end;
end;

procedure TdxSpreadSheetTableViewController.SelectColumn(AColumn: Integer; AShift: TShiftState);
var
  R: TcxRect;
  R1: TRect;
begin
  if (Selection.Count = 0) or not (ssShift in AShift)  then
    SetFocused(FocusedRow, AColumn, AShift)
  else
  begin
    R := Selection[Selection.Count - 1];
    R1 := cxRectAdjust(Rect(FocusedColumn, R.Top, AColumn, R.Bottom));

    if dxSpreadSheetIsEntireRowOrColumn(R1) then
      R.Rect := R1
    else
      R.Rect := MergedCells.ExpandArea(R1);

    View.MakeVisibleColumn(IfThen(AColumn > FocusedColumn, R.Right, R.Left));
  end;
end;

procedure TdxSpreadSheetTableViewController.SelectRow(ARow: Integer; AShift: TShiftState);
var
  R: TcxRect;
  R1: TRect;
begin
  if (Selection.Count = 0) or not (ssShift in AShift) then
    SetFocused(ARow, FocusedColumn, AShift)
  else
  begin
    R := Selection[Selection.Count - 1];
    R1 := cxRectAdjust(Rect(R.Left, FocusedRow, R.Right, ARow));

    if dxSpreadSheetIsEntireRowOrColumn(R1) then
      R.Rect := R1
    else
      R.Rect := MergedCells.ExpandArea(R1);

    View.MakeVisibleRow(IfThen(ARow > FocusedRow, R.Bottom, R.Top));
  end;
end;

procedure TdxSpreadSheetTableViewController.SetFocused(ARow, AColumn: Integer; AShift: TShiftState);
var
  R: TcxRect;
begin
  if Selection.Count > 0 then
    R := Selection[Selection.Count - 1]
  else
    R := nil;

  if (R = nil) or not (ssShift in AShift) then
    Selection.SetFocused(ARow, AColumn, AShift)
  else
  begin
    if R.Right = MaxInt then
      R.Rect := cxRectAdjust(Rect(0, ARow, MaxInt, FocusedRow))
    else
      if R.Bottom = MaxInt then
        R.Rect := cxRectAdjust(Rect(AColumn, 0, FocusedColumn, MaxInt))
      else
        R.Rect := MergedCells.ExpandArea(cxRectAdjust(Rect(AColumn, ARow, FocusedColumn, FocusedRow)));

    View.MakeVisible(ARow, AColumn);
  end;
end;

procedure TdxSpreadSheetTableViewController.SetSelectionMode(
  ARow, AColumn: Integer; AShift: TShiftState; AMode: TdxSpreadSheetTableViewSelectionMode);
begin
  SelectionMode := AMode;
  case AMode of
    smRows:
      if ssShift in AShift then
        AnchorRow := FocusedRow
      else
        AnchorRow := ARow;

    smColumns:
      if ssShift in AShift then
        AnchorColumn := FocusedColumn
      else
        AnchorColumn := AColumn;

  else
    SetFocused(ARow, AColumn, AShift);
  end;
end;

function TdxSpreadSheetTableViewController.GetColumns: TdxSpreadSheetTableColumns;
begin
  Result := View.Columns;
end;

function TdxSpreadSheetTableViewController.GetFocusedColumn: Integer;
begin
  Result := Selection.FocusedColumn;
end;

function TdxSpreadSheetTableViewController.GetFocusedRow: Integer;
begin
  Result := Selection.FocusedRow;
end;

function TdxSpreadSheetTableViewController.GetFocusedRowOffsetFromTopRow(ARow: Integer): Integer;
var
  ACount: Integer;
begin
  Result := ViewInfo.FirstScrollableRow;
  dxSpreadSheetValidate(ARow, ViewInfo.FirstScrollableRow, View.BottomRow);
  ACount := 0;
  while Result < ARow do
  begin
    if Rows.GetItemVisible(Result) then
      Inc(ACount);
    Inc(Result);
  end;
  Result := ACount;
end;

function TdxSpreadSheetTableViewController.GetMergedCells: TdxSpreadSheetMergedCellList;
begin
  Result := View.MergedCells;
end;

function TdxSpreadSheetTableViewController.GetOptionsBehavior: TdxSpreadSheetOptionsBehavior;
begin
  Result := View.SpreadSheet.OptionsBehavior;
end;

function TdxSpreadSheetTableViewController.GetRows: TdxSpreadSheetTableRows;
begin
  Result := View.Rows;
end;

function TdxSpreadSheetTableViewController.GetSelection: TdxSpreadSheetTableViewSelection;
begin
  Result := View.Selection;
end;

function TdxSpreadSheetTableViewController.GetView: TdxSpreadSheetTableView;
begin
  Result := TdxSpreadSheetTableView(Owner);
end;

function TdxSpreadSheetTableViewController.GetViewHitTest: TdxSpreadSheetTableViewHitTest;
begin
  Result := View.HitTest;
end;

function TdxSpreadSheetTableViewController.GetViewInfo: TdxSpreadSheetTableViewInfo;
begin
  Result := View.ViewInfo;
end;

{ TdxSpreadSheetTableView }

constructor TdxSpreadSheetTableView.Create(AOwner: TdxCustomSpreadSheet);
begin
  inherited Create(AOwner);
  FOptionsPrint := CreateOptionsPrint;
  FFrozenColumn := -1;
  FFrozenRow := -1;
  FArrayFormulasList := TObjectList<TdxSpreadSheetFormula>.Create(False);
end;

destructor TdxSpreadSheetTableView.Destroy;
begin
  History.Lock;
  try
    FreeAndNil(FOptionsPrint);
    inherited Destroy;
  finally
    History.Unlock;
  end;
end;

procedure TdxSpreadSheetTableView.Assign(Source: TPersistent);
begin
  if Source is TdxSpreadSheetTableView then
  begin
    ViewInfo.FirstScrollableColumn := 0;
    ViewInfo.FirstScrollableRow := 0;
    FFrozenColumn := TdxSpreadSheetTableView(Source).FFrozenColumn;
    FFrozenRow := TdxSpreadSheetTableView(Source).FFrozenRow;
    LeftColumn := TdxSpreadSheetTableView(Source).LeftColumn;
    TopRow := TdxSpreadSheetTableView(Source).TopRow;
    Options := TdxSpreadSheetTableView(Source).Options;
    OptionsPrint := TdxSpreadSheetTableView(Source).OptionsPrint;
  end;
  inherited Assign(Source);
end;

procedure TdxSpreadSheetTableView.CellAtPoint(const P: TPoint; out ARow, AColumn: Integer; 
  AReturnNearestCell: Boolean = False; AVisibleAreaOnly: Boolean = True);

  function FindItem(APos, AMinPos, AMaxPos, AFirstIndex, ALastIndex: Integer;
    ACells: TdxSpreadSheetViewInfoCellsList; ACheckHorizontal: Boolean): Integer;
  var
    ACell: TdxSpreadSheetCellViewInfo;
    I: Integer;
  begin
    Result := -1;
    if AReturnNearestCell then
      Result := AFirstIndex;
    if APos >= AMaxPos then
      Result := ALastIndex
    else
      if APos > AMinPos then
      begin
        Result := TdxSpreadSheetTableViewHeaderCellViewInfo(ACells[ACells.Count - 1]).Index;
        if ACheckHorizontal and (APos > ViewInfo.FrozenColumnSeparatorPosition + SpreadSheet.OptionsView.FrozenPaneSeparatorWidth - 1) then
          Inc(APos, ViewInfo.FirstScrollableColumnOffset)
        else
          if not ACheckHorizontal and (APos > ViewInfo.FrozenRowSeparatorPosition + SpreadSheet.OptionsView.FrozenPaneSeparatorWidth - 1) then
            Inc(APos, ViewInfo.FirstScrollableRowOffset);

        for I := 0 to ACells.Count - 1 do
        begin
          ACell := ACells[I];
          if (ACell.DisplayBounds.Width > 0) and (ACheckHorizontal and (APos < ACell.DisplayBounds.Left)) or
            (not ACheckHorizontal and (APos < ACell.DisplayBounds.Top)) then
          begin
            Result := TdxSpreadSheetTableViewHeaderCellViewInfo(ACells[I - 1]).Index;
            Break;
          end;
        end;
      end;
  end;

var
  R: TRect;
begin
  ARow := -1;
  AColumn := -1;
  ViewInfo.Validate;
  R := ViewInfo.CellsArea;
  if AReturnNearestCell or not AVisibleAreaOnly or PtInRect(R, P) then
  begin  
    AColumn := FindItem(P.X, R.Left, R.Right, LeftColumn, ViewInfo.VisibleCells.Right, ViewInfo.ColumnsHeader, True);
    ARow := FindItem(P.Y, R.Top, R.Bottom, TopRow, ViewInfo.VisibleCells.Bottom, ViewInfo.RowsHeader, False);
  end;
end;

procedure TdxSpreadSheetTableView.CellAtPoint(const P: TPoint; out ACell: TdxSpreadSheetCell; 
  AReturnNearestCell: Boolean = False; AVisibleAreaOnly: Boolean = True);
var
  ARow, AColumn: Integer;
begin
  CellAtPoint(P, ARow, AColumn, AReturnNearestCell, AVisibleAreaOnly);
  if (ARow >= 0) and (AColumn >= 0) then
    ACell := Cells[ARow, AColumn];
end;

function TdxSpreadSheetTableView.CreateCell(const ARow, AColumn: Integer): TdxSpreadSheetCell;
begin
  if History.CanAddCommand and (Rows.Items[ARow] = nil) then
    History.AddCommand(TdxSpreadSheetHistoryCreateCellCommand.Create(Self, ARow, AColumn));
  Result := TdxSpreadSheetTableRow(Rows.ItemNeeded(ARow)).CreateCell(AColumn);
end;

function TdxSpreadSheetTableView.CanClearCells: Boolean;
begin
  Result := SpreadSheet.OptionsBehavior.Editing;
end;

procedure TdxSpreadSheetTableView.ClearCells(const AArea: TRect; AClearValues: Boolean = True; AClearFormats: Boolean = True);
begin
  if CanClearCells and (AClearValues or AClearFormats) then
  begin
    Rows.ForEachCell(AArea,
      procedure (ACell: TdxDynamicListItem; AData: Pointer)
      begin
        if not (Options.Protected and TdxSpreadSheetCell(ACell).Style.Locked) then
        begin
          if AClearFormats then
          begin
            TdxSpreadSheetCell(ACell).Style.Handle := CellStyles.DefaultStyle;
            if Options.Protected then
              TdxSpreadSheetCell(ACell).Style.Locked := False;
          end;
          if AClearValues then
            TdxSpreadSheetCell(ACell).IsEmpty := True;
        end;
      end);
    Pack;
  end;
end;

procedure TdxSpreadSheetTableView.ClearCellsValues;
var
  I: Integer;
begin
  if not CanClearCellsValues then
    raise EdxSpreadSheetCannotChangePartOfArrayError.Create(sdxErrorCannotChangePartOfArray);
  BeginUpdate;
  try
    History.BeginAction(TdxSpreadSheetHistoryClearCellsAction);
    try
      for I := 0 to Selection.Count - 1 do
        ClearCells(Selection.Items[I].Rect, True, False);
    finally
      History.EndAction;
    end;
  finally
    EndUpdate;
  end;
end;

function TdxSpreadSheetTableView.CanDelete: Boolean;
begin
  Result := SpreadSheet.OptionsBehavior.Editing and SpreadSheet.OptionsBehavior.Deleting and not Options.Protected;
end;

procedure TdxSpreadSheetTableView.DeleteAllCells;
begin
  if (Rows.First = nil) or not CanDelete then
    Exit;

  BeginUpdate;
  try
    Rows.ForEach(
      procedure (AItem: TdxDynamicListItem; AData: Pointer)
      begin
        TdxSpreadSheetTableRow(AItem).RowCells.Clear;
      end);
    MergedCells.Clear;
  finally
    EndUpdate;
  end;
end;

procedure TdxSpreadSheetTableView.DeleteCells;
var
  AArea: TRect;
  AModification: TdxSpreadSheetCellsModification;
begin
  if CanDelete then
  begin
    if Selection.Count <> 1 then
      raise EdxSpreadSheetError.Create(cxGetResourceString(@sdxErrorInvalidSelection));
    AArea := Selection.Area;
    if GetCellsModification(AArea, True, AModification) then
      DeleteCells(AArea, AModification);
  end;
end;

procedure TdxSpreadSheetTableView.DeleteCells(const AArea: TRect; AModification: TdxSpreadSheetCellsModification;
  AMergedCellsConflictResolution: TdxSpreadSheetTableViewMergedCellsConflictResolution = mccrShowConfirmation);
var
  AHelper: TdxSpreadSheetTableViewDeleteCellsHelper;
begin
  if CanDelete then
  begin
    AHelper := TdxSpreadSheetTableViewDeleteCellsHelper.Create(Self);
    try
      AHelper.MergedCellsConflictResolution := AMergedCellsConflictResolution;
      AHelper.Modification := AModification;
      if not CanModifyDataInArea(GetFactuallyModifyableArea(AArea, AHelper.Modification), cmmDelete) then
        raise EdxSpreadSheetCannotChangePartOfArrayError.Create(sdxErrorCannotChangePartOfArray)
      else
        AHelper.Process(AArea);
    finally
      AHelper.Free;
    end;
  end;
end;

procedure TdxSpreadSheetTableView.DeleteColumns(AStartColumn: Integer; ACount: Integer);
begin
  DeleteCells(cxRect(AStartColumn, 0, AStartColumn + ACount - 1, 0), cmShiftColumns, mccrRaiseException);
end;

procedure TdxSpreadSheetTableView.DeleteRows(AStartRow: Integer; ACount: Integer);
begin
  DeleteCells(cxRect(0, AStartRow, 0, AStartRow + ACount - 1), cmShiftRows, mccrRaiseException);
end;

procedure TdxSpreadSheetTableView.AddArrayFormula(const AText: TdxUnicodeString; const AArea: TRect);
var
  ACell: TdxSpreadSheetCell;
  AFormula: TdxSpreadSheetFormula;
  AExistingArea, ADestArea: TRect;
  ADestSize: TSize;
begin
  if not CanModifyDataInArea(AArea, cmmReplace) then
    raise EdxSpreadSheetCannotChangePartOfArrayError.Create(sdxErrorCannotChangePartOfArray);
  BeginUpdate;
  try
    ADestArea := AArea;
    AExistingArea := GetAreaReferencedByArrayFormula(AArea.Top, AArea.Left);
    if (AExistingArea.Right > ADestArea.Right) or (AExistingArea.Bottom > ADestArea.Bottom) then
      ADestArea := AExistingArea;
    ADestSize := dxSpreadSheetAreaSize(ADestArea);

    ACell := CreateCell(ADestArea.Top, ADestArea.Left);
    if ACell.AsFormula <> nil then
      ACell.AsFormula.Free;
    ACell.SetText(AText, True);
    if ACell.IsFormula then
    begin
      ACell.AsFormula.SetArrayFormulaSize(ADestSize);
      ACell.AsFormula.IsArrayFormula := True;
    end
    else
      PopulateAreaByValue(AText, ADestArea);
    AFormula := ACell.AsFormula;
    if (AFormula <> nil) and ((ADestSize.cx > 1) or (ADestSize.cy > 1)) then
      FArrayFormulasList.Add(AFormula);
  finally
    EndUpdate;
  end;
end;

function TdxSpreadSheetTableView.CanClearCellsValues: Boolean;
var
  I: Integer;
begin
  Result := True;
  for I := 0 to Selection.Count - 1 do
  begin
    Result := CanModifyDataInArea(Selection.Items[I].Rect, cmmClear);
    if not Result then
      Break;
  end;
end;

function TdxSpreadSheetTableView.CanInsertCells(const AArea: TRect; const AModification: TdxSpreadSheetCellsModification): Boolean;
var
  I: Integer;
  AFormula: TdxSpreadSheetFormula;
  AArrayFormulaArea, AIntersecting: TRect;
begin
  Result := True;
  for I := 0 to ArrayFormulasList.Count - 1 do
  begin
    AFormula := ArrayFormulasList[I];
    AArrayFormulaArea := dxSpreadSheetArea(cxPoint(AFormula.Cell.ColumnIndex, AFormula.Cell.RowIndex), AFormula.ArrayFormulaSize);
    if AModification = cmShiftRows then
      Result := (AArea.Top <= AArrayFormulaArea.Top) or (AArea.Top > AArrayFormulaArea.Bottom)
    else
      if AModification = cmShiftColumns then
        Result := (AArea.Left <= AArrayFormulaArea.Left) or (AArea.Left > AArrayFormulaArea.Right)
      else
        Result := not dxSpreadSheetIntersects(AArea, AArrayFormulaArea, AIntersecting) or cxRectIsEqual(AIntersecting, AArrayFormulaArea);
  end;
end;

function TdxSpreadSheetTableView.CanModifyDataInArea(const AArea: TRect; const AMode: TdxSpreadSheetCellsModificationMode): Boolean;
var
  I: Integer;
  AFormula: TdxSpreadSheetFormula;
  AArrayFormulaArea, AIntersecting: TRect;
begin
  Result := True;
  if (AArea.Top = AArea.Bottom) and (AArea.Left = AArea.Right) and (AMode = cmmReplace) then
    Exit;
  for I := 0 to ArrayFormulasList.Count - 1 do
  begin
    AFormula := ArrayFormulasList[I];
    AArrayFormulaArea := dxSpreadSheetArea(cxPoint(AFormula.Cell.ColumnIndex, AFormula.Cell.RowIndex), AFormula.ArrayFormulaSize);
    Result := not dxSpreadSheetIntersects(AArea, AArrayFormulaArea, AIntersecting) or cxRectIsEqual(AIntersecting, AArrayFormulaArea);
  end;
end;

function TdxSpreadSheetTableView.ExtractVector(const AStartRow, AStartColumn, ALength: Integer; const AIsVertical: Boolean): TdxSpreadSheetVector;
var
  I, dRow, dColumn: Integer;
  AValue: Variant;
  AErrorCode: TdxSpreadSheetFormulaErrorCode;
begin
  Result := TdxSpreadSheetVector.Create;
  dRow := 0;
  dColumn := 0;
  for I := 0 to ALength - 1 do
  begin
    if AIsVertical then
      dRow := I
    else
      dColumn := I;
    AErrorCode := ecNone;
    GetCellValue(AStartRow + dRow, AStartColumn + dColumn, AValue, AErrorCode);
    Result.Add(TdxSpreadSheetVectorValue.Create(AValue, AErrorCode));
  end;
end;

function TdxSpreadSheetTableView.ExtractHorizontalVector(const ARow, AStartColumn, ALength: Integer): TdxSpreadSheetVector;
begin
  Result := ExtractVector(ARow, AStartColumn, ALength, False);
end;

function TdxSpreadSheetTableView.ExtractVerticalVector(const AStartRow, AColumn, ALength: Integer): TdxSpreadSheetVector;
begin
  Result := ExtractVector(AStartRow, AColumn, ALength, True);
end;

procedure TdxSpreadSheetTableView.GetCellValue(const ARow, AColumn: Integer;
  var AValue: Variant; var AErrorCode: TdxSpreadSheetFormulaErrorCode);
var
  ACell: TdxSpreadSheetCell;
  AResult: TdxSpreadSheetFormulaResult;
  AArrayFormulaArea: TRect;
begin
  if AErrorCode <> ecNone then Exit;
  if not TdxSpreadSheetInvalidObject.IsLive(Self) or (AColumn < 0) or (ARow < 0) then
  begin
    AErrorCode := ecRefErr;
    Exit;
  end;
  if IsCellReferencedByArrayFormulaArea(ARow, AColumn, AArrayFormulaArea) then
    AValue := GetArrayFormulaSlaveCellValue(AArrayFormulaArea, ARow, AColumn, AErrorCode)
  else
  begin
    ACell := Cells[ARow, AColumn];
    if ACell = nil then
      AValue := Unassigned
    else
    begin
      if ACell.IsFormula then
      begin
        SpreadSheet.FormulaController.AddPath(ARow, AColumn, Self);
        try
          AResult := ACell.AsFormula.ResultValue;
          AErrorCode := AResult.ErrorCode;
          AValue := AResult.Value;
        finally
          SpreadSheet.FormulaController.RemovePath(ARow, AColumn, Self);
        end;
      end
      else
      begin
        if ACell.DataType = cdtError then
        begin
          AErrorCode := ACell.AsError;
          AValue := dxSpreadSheetErrorCodeToString(AErrorCode);
        end
        else
          AValue := ACell.AsVariant;
      end;
    end;
  end;
end;

procedure TdxSpreadSheetTableView.FreezeColumns(const AColumn: Integer);
begin
  BeginUpdate;
  try
    FrozenRow := -1;
    FrozenColumn := AColumn;
  finally
    EndUpdate;
  end;
end;

procedure TdxSpreadSheetTableView.FreezePanes(const ARow, AColumn: Integer);
begin
  BeginUpdate;
  try
    FrozenRow := ARow;
    FrozenColumn := AColumn;
  finally
    EndUpdate;
  end;
end;

procedure TdxSpreadSheetTableView.FreezeRows(const ARow: Integer);
begin
  BeginUpdate;
  try
    FrozenColumn := -1;
    FrozenRow := ARow;
  finally
    EndUpdate;
  end;
end;

procedure TdxSpreadSheetTableView.UnfreezePanes;
begin
  BeginUpdate;
  try
    FrozenColumn := -1;
    FrozenRow := -1;
  finally
    EndUpdate;
  end;
end;

function TdxSpreadSheetTableView.CanInsert: Boolean;
begin
  Result := CanDelete;
end;

procedure TdxSpreadSheetTableView.InsertCells;
var
  AArea: TRect;
  AModification: TdxSpreadSheetCellsModification;
begin
  if CanInsert then
  begin
    if Selection.Count <> 1 then
      raise EdxSpreadSheetError.Create(cxGetResourceString(@sdxErrorInvalidSelection));
    AArea := Selection.Area;
    if GetCellsModification(AArea, False, AModification) then
      InsertCells(AArea, AModification);
  end;
end;

procedure TdxSpreadSheetTableView.InsertCells(const AArea: TRect; AModification: TdxSpreadSheetCellsModification;
  AMergedCellsConflictResolution: TdxSpreadSheetTableViewMergedCellsConflictResolution = mccrShowConfirmation);
var
  AHelper: TdxSpreadSheetTableViewInsertCellsHelper;
begin
  if CanInsert then
  begin
    AHelper := TdxSpreadSheetTableViewInsertCellsHelper.Create(Self);
    try
      AHelper.MergedCellsConflictResolution := AMergedCellsConflictResolution;
      AHelper.Modification := AModification;
      if not CanInsertCells(GetFactuallyModifyableArea(AArea, AHelper.Modification), AModification) then
        raise EdxSpreadSheetCannotChangePartOfArrayError.Create(sdxErrorCannotChangePartOfArray)
      else
        AHelper.Process(AArea);
    finally
      AHelper.Free;
    end;
  end;
end;

procedure TdxSpreadSheetTableView.InsertColumns(AColumn: Integer; ACount: Integer);
begin
  InsertCells(cxRect(AColumn, 0, AColumn + ACount - 1, 0), cmShiftColumns, mccrRaiseException);
end;

procedure TdxSpreadSheetTableView.InsertRows(ARow: Integer; ACount: Integer);
begin
  InsertCells(cxRect(0, ARow, 0, ARow + ACount - 1), cmShiftRows, mccrRaiseException);
end;

procedure TdxSpreadSheetTableView.MakeFocusedCellVisible;
begin
  MakeVisible(Selection.FocusedRow, Selection.FocusedColumn);
end;

procedure TdxSpreadSheetTableView.MakeVisible(ARow, AColumn: Integer);
begin
  dxSpreadSheetValidate(ARow, 0, dxSpreadSheetMaxRowIndex);
  dxSpreadSheetValidate(AColumn, 0, dxSpreadSheetMaxColumnIndex);
  if not InRange(ARow, TopRow, BottomRow) then
  begin
    if ARow < TopRow then
      ViewInfo.ChangeVisibleAreaBounds(bTop, ARow)
    else
      if ARow > BottomRow then
        ViewInfo.ChangeVisibleAreaBounds(bBottom, ARow);
  end
  else
    if ARow > FrozenRow then
      ViewInfo.FirstScrollableRow := Min(ViewInfo.FirstScrollableRow, ARow);
  if not InRange(AColumn, LeftColumn, RightColumn) then
  begin
    if AColumn < LeftColumn then
      ViewInfo.ChangeVisibleAreaBounds(bLeft, AColumn)
    else
      if AColumn > RightColumn then
        ViewInfo.ChangeVisibleAreaBounds(bRight, AColumn);
  end
  else
    if AColumn > FrozenColumn then
      ViewInfo.FirstScrollableColumn := Min(AColumn, ViewInfo.FirstScrollableColumn);
  ViewInfo.Validate;
end;

procedure TdxSpreadSheetTableView.MakeVisibleColumn(const AColumn: Integer);
begin
  MakeVisible(TopRow, AColumn);
end;

procedure TdxSpreadSheetTableView.MakeVisibleRow(const ARow: Integer);
begin
  MakeVisible(ARow, LeftColumn);
end;

function TdxSpreadSheetTableView.CanCopyToClipboard: Boolean;
begin
  Result := not IsEditing;
end;

function TdxSpreadSheetTableView.CanCutToClipboard: Boolean;
begin
  Result := not IsEditing and CanClearCells;
end;

function TdxSpreadSheetTableView.CanPasteFromClipboard: Boolean;
begin
  Result := not IsEditing and Controller.OptionsBehavior.Editing and TdxSpreadSheetTableViewClipboardHelper.CanPaste;
end;

procedure TdxSpreadSheetTableView.CopyToClipboard;
var
  AHelper: TdxSpreadSheetTableViewClipboardHelper;
begin
  if CanCopyToClipboard then
  begin
    AHelper := TdxSpreadSheetTableViewClipboardHelper.Create(Self);
    try
      AHelper.Copy;
    finally
      AHelper.Free;
    end;
  end;
end;

procedure TdxSpreadSheetTableView.CutToClipboard;
var
  AHelper: TdxSpreadSheetTableViewClipboardHelper;
begin
  if CanCutToClipboard then
  begin
    History.BeginAction(TdxSpreadSheetHistoryCutToClipboardAction);
    try
      AHelper := TdxSpreadSheetTableViewClipboardHelper.Create(Self);
      try
        AHelper.Cut;
      finally
        AHelper.Free;
      end;
    finally
      History.EndAction;
    end;
  end;
end;

procedure TdxSpreadSheetTableView.PasteFromClipboard;
var
  AHelper: TdxSpreadSheetTableViewClipboardHelper;
begin
  if CanPasteFromClipboard then
  begin
    History.BeginAction(TdxSpreadSheetHistoryPasteFromClipboardAction);
    try
      AHelper := TdxSpreadSheetTableViewClipboardHelper.Create(Self);
      try
        AHelper.Paste;
      finally
        AHelper.Free;
      end;
    finally
      History.EndAction;
    end;
  end;
end;

function TdxSpreadSheetTableView.CanMergeSelected: Boolean;
begin
  Result := SpreadSheet.OptionsBehavior.Editing and not Options.Protected;
end;

function TdxSpreadSheetTableView.CanSplitSelected: Boolean;
begin
  Result := SpreadSheet.OptionsBehavior.Editing and not Options.Protected;
end;

procedure TdxSpreadSheetTableView.MergeSelected;
var
  I: Integer;
begin
  if CanMergeSelected then
  begin
    History.BeginAction(TdxSpreadSheetHistoryMergeCellsAction);
    try
      BeginUpdate;
      try
        SplitSelected;
        for I := 0 to Selection.Count - 1 do
          MergedCells.Add(Selection.Items[I].Rect);
      finally
        EndUpdate;
      end;
    finally
      History.EndAction;
    end;
  end;
end;

procedure TdxSpreadSheetTableView.SplitSelected;
var
  I: Integer;
begin
  if CanSplitSelected then
  begin
    BeginUpdate;
    try
      for I := 0 to Selection.Count - 1 do
        MergedCells.DeleteItemsInArea(Selection.Items[I].Rect);
    finally
      EndUpdate;
    end;
  end;
end;

procedure TdxSpreadSheetTableView.HideEdit(Accept: Boolean);
begin
  EditingController.HideEdit(Accept);
end;

procedure TdxSpreadSheetTableView.ShowEdit;
begin
  EditingController.ShowEdit;
end;

procedure TdxSpreadSheetTableView.ShowEditByKey(AKey: Char);
begin
  EditingController.ShowEditByKey(AKey);
end;

procedure TdxSpreadSheetTableView.ShowEditByMouse(X, Y: Integer; AShift: TShiftState);
begin
  EditingController.ShowEditByMouse(X, Y, AShift);
end;

function TdxSpreadSheetTableView.CanSort: Boolean;
begin
  Result := not Options.Protected;
end;

procedure TdxSpreadSheetTableView.SortByColumnValues(const AArea: TRect;
  const ASortOrders: array of TdxSortOrder; const AColumns: array of Integer);
var
  AHelper: TdxSpreadSheetTableViewSortingHelper;
begin
  if CanSort then
  begin
    AHelper := TdxSpreadSheetTableViewSortingHelper.Create(Self);
    try
      AHelper.SortByColumnValues(AArea, ASortOrders, AColumns);
    finally
      AHelper.Free;
    end;
  end;
end;

procedure TdxSpreadSheetTableView.SortByRowValues(const AArea: TRect;
  const ASortOrders: array of TdxSortOrder; const ARows: array of Integer);
var
  AHelper: TdxSpreadSheetTableViewSortingHelper;
begin
  if CanSort then
  begin
    AHelper := TdxSpreadSheetTableViewSortingHelper.Create(Self);
    try
      AHelper.SortByRowValues(AArea, ASortOrders, ARows);
    finally
      AHelper.Free;
    end;
  end;
end;

function TdxSpreadSheetTableView.GetAreaReferencedByArrayFormula(const ARow, AColumn: Integer): TRect;
var
  I: Integer;
  AFormula: TdxSpreadSheetFormula;
  AMasterCellRow, AMasterCellColumn: Integer;
  ASize: TSize;
begin
  Result := cxNullRect;
  for I := 0 to ArrayFormulasList.Count - 1 do
  begin
    AFormula := ArrayFormulasList[I];
    AMasterCellRow := AFormula.Cell.RowIndex;
    AMasterCellColumn := AFormula.Cell.ColumnIndex;
    ASize := AFormula.ArrayFormulaSize;
    if (ARow >= AMasterCellRow) and (ARow < AMasterCellRow + ASize.cy) and
       (AColumn >= AMasterCellColumn) and (AColumn < AMasterCellColumn + ASize.cx) then
    begin
      Result := dxSpreadSheetArea(cxPoint(AMasterCellColumn, AMasterCellRow), ASize);
      Break;
    end;
  end;
end;

function TdxSpreadSheetTableView.GetArrayFormulaArea(const ASenderRowIndex, ASenderColumnIndex: Integer): TRect;
begin
  if (Selection.Count > 0) then
  begin
    Result := Selection[Selection.Count - 1].Rect;
    if not((ASenderRowIndex >= Result.Top) and (ASenderRowIndex <= Result.Bottom) and
      (ASenderColumnIndex >= Result.Left) and (ASenderColumnIndex <= Result.Right)) then
      Result := cxRect(ASenderColumnIndex, ASenderRowIndex, ASenderColumnIndex, ASenderRowIndex);
  end
  else
    Result := cxRect(ASenderColumnIndex, ASenderRowIndex, ASenderColumnIndex, ASenderRowIndex);
end;

function TdxSpreadSheetTableView.GetArrayFormulaSlaveCellValue(const AArea: TRect; const ARow, AColumn: Integer;
  var AErrorCode: TdxSpreadSheetFormulaErrorCode): Variant;
var
  AMasterResult: TdxSpreadSheetFormulaResult;
  AResultToken: TdxSpreadSheetFormulaToken;
begin
  AMasterResult := Cells[AArea.Top, AArea.Left].AsFormula.ResultValue;
  if AMasterResult.Validate then
  begin
    AResultToken := AMasterResult.Values[AMasterResult.Count - 1];
    Result := AResultToken.GetValueFromArray(ARow - AArea.Top, AColumn - AArea.Left, AErrorCode);
  end
  else
  begin
    AErrorCode := AMasterResult.ErrorCode;
    Result := dxSpreadSheetErrorCodeToString(AErrorCode);
  end;
end;

function TdxSpreadSheetTableView.IsCellReferencedByArrayFormulaArea(const ARow, AColumn: Integer; var AArea: TRect): Boolean;
begin
  AArea := GetAreaReferencedByArrayFormula(ARow, AColumn);
  Result := not(cxRectIsNull(AArea) or ((ARow = AArea.Top) and (AColumn = AArea.Left)));
end;

procedure TdxSpreadSheetTableView.PopulateAreaByValue(const AValue: Variant; const AArea: TRect);
var
  ARow, AColumn: Integer;
begin
  BeginUpdate;
  try
    for ARow := AArea.Top to AArea.Bottom do
      for AColumn := AArea.Left to AArea.Right do
        CreateCell(ARow, AColumn).AsVariant := AValue;
  finally
    EndUpdate;
  end;
end;

procedure TdxSpreadSheetTableView.PopulateSlaveCells(const AArrayFormulaArea: TRect);
var
  ARow, AColumn: Integer;
  AValue: Variant;
  AErrorCode: TdxSpreadSheetFormulaErrorCode;
begin
  for ARow := AArrayFormulaArea.Top to AArrayFormulaArea.Bottom do
    for AColumn := AArrayFormulaArea.Left to AArrayFormulaArea.Right do
    begin
      if (ARow = AArrayFormulaArea.Top) and (AColumn = AArrayFormulaArea.Left) then
        Continue;
      AValue := GetArrayFormulaSlaveCellValue(AArrayFormulaArea, ARow, AColumn, AErrorCode);
      if AErrorCode = ecNone then
        CreateCell(ARow, AColumn).AsVariant := AValue
      else
        CreateCell(ARow, AColumn).AsError := AErrorCode;
    end;
end;

function TdxSpreadSheetTableView.GetCellsModification(const AArea: TRect;
  AIsDeletingMode: Boolean; out AModification: TdxSpreadSheetCellsModification): Boolean;
begin
  Result := True;
  if dxSpreadSheetIsEntireColumn(AArea) then
    AModification := cmShiftColumns
  else
    if dxSpreadSheetIsEntireRow(AArea) then
      AModification := cmShiftRows
    else
      Result := ShowCellsModificationDialog(AModification, AIsDeletingMode);
end;

function TdxSpreadSheetTableView.GetFactuallyModifyableArea(const AArea: TRect;
  AModification: TdxSpreadSheetCellsModification): TRect;
begin
  case AModification of
    cmShiftCellsHorizontally:
      Result := cxRect(AArea.Left, AArea.Top, dxSpreadSheetMaxColumnIndex, AArea.Bottom);

    cmShiftCellsVertically:
      Result := cxRect(AArea.Left, AArea.Top, AArea.Right, dxSpreadSheetMaxRowIndex);

    cmShiftColumns:
      Result := cxRect(AArea.Left, 0, AArea.Right, dxSpreadSheetMaxRowIndex);

    cmShiftRows:
      Result := cxRect(0, AArea.Top, dxSpreadSheetMaxColumnIndex, AArea.Bottom);
  end;
end;

function TdxSpreadSheetTableView.GetAbsoluteCellBounds(
  const ARowIndex, AColumnIndex: Integer; ACheckMergedCells: Boolean = True): TRect;
begin
  if ACheckMergedCells then
    Result := MergedCells.CheckCell(ARowIndex, AColumnIndex)
  else
    Result := cxRectBounds(AColumnIndex, ARowIndex, 0, 0);

  Result := Rect(Columns.GetPosition(Result.Left), Rows.GetPosition(Result.Top),
    Columns.GetPosition(Result.Right) + Columns.GetItemSize(Result.Right),
    Rows.GetPosition(Result.Bottom) + Rows.GetItemSize(Result.Bottom));
end;

function TdxSpreadSheetTableView.GetCellAtAbsolutePoint(const P: TPoint; out ARowIndex, AColumnIndex: Integer): Boolean;

  function FindItem(X, AMaxIndex: Integer; AItems: TdxSpreadSheetTableItems): Integer;
  var
    I: Integer;
  begin
    Result := -1;
    for I := 0 to AMaxIndex do
    begin
      if X >= AItems.GetPosition(I) then
        Result := I
      else
        Break;
    end;
  end;

begin
  ViewInfo.Validate;
  AColumnIndex := FindItem(P.X, dxSpreadSheetMaxColumnIndex, Columns);
  ARowIndex := FindItem(P.Y, dxSpreadSheetMaxRowIndex, Rows);
  Result := (AColumnIndex >= 0) and (ARowIndex >= 0);
end;

procedure TdxSpreadSheetTableView.AfterLoad;
begin
  BeginUpdate;
  try
    Rows.ForEach(
      procedure (AItem: TdxDynamicListItem; AData: Pointer)
      var
        ARow: TdxSpreadSheetTableRow;
      begin
        ARow := TdxSpreadSheetTableRow(AItem);
        if not ARow.IsCustomSize and SpreadSheet.OptionsView.CellAutoHeight then
          ARow.ApplyBestFit;
      end);
  finally
    EndUpdate;
  end;
end;

function TdxSpreadSheetTableView.CreateColumns: TdxSpreadSheetTableColumns;
begin
  Result := TdxSpreadSheetTableColumns.Create(Self);
end;

function TdxSpreadSheetTableView.CreateController: TdxSpreadSheetCustomViewController;
begin
  Result := TdxSpreadSheetTableViewController.Create(Self);
end;

function TdxSpreadSheetTableView.CreateHitTest: TdxSpreadSheetCustomHitTest;
begin
  Result := TdxSpreadSheetTableViewHitTest.Create(Self);
end;

function TdxSpreadSheetTableView.CreateMergedCells: TdxSpreadSheetMergedCellList;
begin
  Result := TdxSpreadSheetMergedCellList.Create(Self);
end;

function TdxSpreadSheetTableView.CreateOptions: TdxSpreadSheetCustomViewOptions;
begin
  Result := TdxSpreadSheetTableViewOptions.Create(Self);
end;

function TdxSpreadSheetTableView.CreateOptionsPrint: TdxSpreadSheetTableViewOptionsPrint;
begin
  Result := TdxSpreadSheetTableViewOptionsPrint.Create(Self);
end;

function TdxSpreadSheetTableView.CreateRows: TdxSpreadSheetTableRows;
begin
  Result := TdxSpreadSheetTableRows.Create(Self);
end;

function TdxSpreadSheetTableView.CreateSelection: TdxSpreadSheetTableViewSelection;
begin
  Result := TdxSpreadSheetTableViewSelection.Create(Self);
end;

function TdxSpreadSheetTableView.CreateViewInfo: TdxSpreadSheetCustomViewViewInfo;
begin
  Result := TdxSpreadSheetTableViewInfo.Create(Self);
end;

procedure TdxSpreadSheetTableView.CreateSubClasses;
begin
  inherited CreateSubClasses;
  FSelection := CreateSelection;
  FRows := CreateRows;
  FColumns := CreateColumns;
  FMergedCells := CreateMergedCells;
end;

procedure TdxSpreadSheetTableView.DestroySubClasses;
begin
  inherited DestroySubClasses;
  FreeAndNil(FMergedCells);
  FreeAndNil(FColumns);
  FreeAndNil(FRows);
  FreeAndNil(FSelection);
  FreeAndNil(FArrayFormulasList);
end;

function TdxSpreadSheetTableView.DoActiveCellChanging(AColumn, ARow: Integer): Boolean;
begin
  Result := SpreadSheet.DoActiveCellChanging(Self, cxPoint(AColumn, ARow));
end;

procedure TdxSpreadSheetTableView.DoCheckChanges;
var
  AZoomChanged: Boolean;
begin
  AZoomChanged := sscZoom in Changes;
  inherited DoCheckChanges;
  if AZoomChanged then
  begin
    Changes := Changes - [sscZoom];
    MakeFocusedCellVisible;
  end;
end;

procedure TdxSpreadSheetTableView.DoCompare(const AData1, AData2: TdxSpreadSheetCellData; var Compare: Integer);
begin
  SpreadSheet.DoCompare(Self, AData1, AData2, Compare);
end;

procedure TdxSpreadSheetTableView.DoEditChanged;
begin
  SpreadSheet.DoEditChanged(Self);
end;

procedure TdxSpreadSheetTableView.DoEdited;
begin
  SpreadSheet.DoEdited(Self);
end;

procedure TdxSpreadSheetTableView.DoEditing(var AProperties: TcxCustomEditProperties; var ACanEdit: Boolean);
begin
  SpreadSheet.DoEditing(Self, AProperties, ACanEdit);
end;

procedure TdxSpreadSheetTableView.DoEditValueChanged;
begin
  SpreadSheet.DoEditValueChanged(Self);
end;

procedure TdxSpreadSheetTableView.DoInitEdit(AEdit: TcxCustomEdit);
begin
  SpreadSheet.DoInitEdit(Self, AEdit);
end;

procedure TdxSpreadSheetTableView.DoInitEditValue(AEdit: TcxCustomEdit; var AValue: Variant);
begin
  SpreadSheet.DoInitEditValue(Self, AEdit, AValue);
end;

procedure TdxSpreadSheetTableView.DoRemoveCell(ACell: TdxSpreadSheetCell);
begin
  if (Containers <> nil) and (Containers.Count > 0) then
    Containers.CellRemoving(ACell);
end;

procedure TdxSpreadSheetTableView.ExchangeValues(ACell1, ACell2: TdxSpreadSheetCell);
var
  ADataType: TdxSpreadSheetCellDataType;
  AData: array[0..9] of Byte;
begin
  if History.CanAddCommand then
  begin
    History.AddCommand(TdxSpreadSheetHistoryChangeCellCommand.CreateEx(ACell1));
    History.AddCommand(TdxSpreadSheetHistoryChangeCellCommand.CreateEx(ACell2));
  end;
  ADataType := ACell1.FDataType;
  ACell1.FDataType := ACell2.FDataType;
  ACell2.FDataType := ADataType;
  cxCopyData(@ACell1.FData, @AData, SizeOf(AData));
  cxCopyData(@ACell2.FData, @ACell1.FData, SizeOf(AData));
  cxCopyData(@AData, @ACell2.FData, SizeOf(AData));
  ACell1.Changed([sscData]);
  ACell2.Changed([sscData]);
end;

procedure TdxSpreadSheetTableView.ExchangeValues(ARow1, AColumn1, ARow2, AColumn2: Integer);
begin
  if Cells[ARow1, AColumn1] <> Cells[ARow2, AColumn2] then
    ExchangeValues(CreateCell(ARow1, AColumn1),  CreateCell(ARow2, AColumn2));
end;

procedure TdxSpreadSheetTableView.Pack;
var
  AHelper: TdxSpreadSheetTableViewPackHelper;
begin
  AHelper := TdxSpreadSheetTableViewPackHelper.Create(Self);
  try
    AHelper.Pack;
  finally
    AHelper.Free;
  end;
end;

procedure TdxSpreadSheetTableView.RemoveFromArrayTokenList(AFormula: TdxSpreadSheetFormula);
begin
  FArrayFormulasList.Remove(AFormula);
end;

function TdxSpreadSheetTableView.AbsoluteToScreen(const R: TRect): TRect;
var
  AOrg: TPoint;
begin
  AOrg := cxNullPoint;
  Result := cxRectOffset(R, ViewInfo.Origin, False);
  if (FrozenColumn >= 0) and (Result.Left >= ViewInfo.FrozenColumnSeparatorPosition) then
    AOrg.X := ViewInfo.FirstScrollableColumnOffset;
  if (FrozenRow >= 0) and (Result.Top >= ViewInfo.FrozenRowSeparatorPosition) then
    AOrg.Y := ViewInfo.FirstScrollableRowOffset;
  Result := cxRectOffset(Result, AOrg, False);
end;

function TdxSpreadSheetTableView.GetContentOrigin: TPoint;
begin
  Result := cxPointInvert(ViewInfo.CellsArea.TopLeft);
end;

function TdxSpreadSheetTableView.GetPartOffsetByPoint(const P: TPoint): TPoint;
begin
  Result := ViewInfo.GetPartOffsetByPoint(P);
end;

function TdxSpreadSheetTableView.GetZoomFactor: Integer;
begin
  Result := Options.ZoomFactor;
end;

function TdxSpreadSheetTableView.GetBottomRow: Integer;
begin
  ViewInfo.Validate;
  Result := Max(TopRow, ViewInfo.LastVisibleRow);
end;

function TdxSpreadSheetTableView.GetCell(ARow, AColumn: Integer): TdxSpreadSheetCell;
var
  ARowItem: TdxSpreadSheetTableItem;
begin
  ARowItem := Rows[ARow];
  if ARowItem <> nil then
    Result := ARowItem.Cells[AColumn]
  else
    Result := nil;
end;

function TdxSpreadSheetTableView.GetController: TdxSpreadSheetTableViewController;
begin
  Result := TdxSpreadSheetTableViewController(inherited Controller);
end;

function TdxSpreadSheetTableView.GetDimensions: TRect;
begin
  Result := Rect(GetItemIndex(Columns.First), GetItemIndex(Rows.First),
    GetItemIndex(Columns.Last), GetItemIndex(Rows.Last));
end;

function TdxSpreadSheetTableView.GetEditingController: TdxSpreadSheetTableViewEditingController;
begin
  Result := Controller.EditingController;
end;

function TdxSpreadSheetTableView.GetHistory: TdxSpreadSheetHistory;
begin
  Result := SpreadSheet.History;
end;

function TdxSpreadSheetTableView.GetHitTest: TdxSpreadSheetTableViewHitTest;
begin
  Result := TdxSpreadSheetTableViewHitTest(inherited HitTest);
end;

function TdxSpreadSheetTableView.GetIsEditing: Boolean;
begin
  Result := EditingController.IsEditing;
end;

function TdxSpreadSheetTableView.GetItemIndex(const AItem: TdxDynamicListItem): Integer;
begin
  if AItem <> nil then
    Result := AItem.Index
  else
    Result := 0;
end;

function TdxSpreadSheetTableView.GetLeftColumn: Integer;
begin
  ViewInfo.Validate;
  Result := ViewInfo.VisibleCells.Left;
end;

function TdxSpreadSheetTableView.GetOptions: TdxSpreadSheetTableViewOptions;
begin
  Result := TdxSpreadSheetTableViewOptions(inherited Options);
end;

function TdxSpreadSheetTableView.GetRightColumn: Integer;
begin
  ViewInfo.Validate;
  Result := Max(LeftColumn, ViewInfo.LastVisibleColumn);
end;

function TdxSpreadSheetTableView.GetStringTable: TdxSpreadSheetSharedStringTable;
begin
  Result := SpreadSheet.StringTable;
end;

function TdxSpreadSheetTableView.GetTopRow: Integer;
begin
  ViewInfo.Validate;
  Result := ViewInfo.VisibleCells.Top;
end;

function TdxSpreadSheetTableView.GetViewInfo: TdxSpreadSheetTableViewInfo;
begin
  Result := TdxSpreadSheetTableViewInfo(inherited ViewInfo);
end;

procedure TdxSpreadSheetTableView.SetBottomRow(AValue: Integer);
begin
  ViewInfo.ChangeVisibleArea(bBottom, AValue);
end;

procedure TdxSpreadSheetTableView.SetFrozenColumn(AValue: Integer);
begin
  dxSpreadSheetValidate(AValue, -1, dxSpreadSheetMaxColumnIndex);
  if AValue <> FFrozenColumn then
  begin
    FFrozenColumn := AValue;
    ViewInfo.FirstScrollableColumn := 0;
    Changed;
  end;
end;

procedure TdxSpreadSheetTableView.SetFrozenRow(AValue: Integer);
begin
  dxSpreadSheetValidate(AValue, -1, dxSpreadSheetMaxRowIndex);
  if AValue <> FFrozenRow then
  begin
    FFrozenRow := AValue;
    ViewInfo.FirstScrollableRow := 0;
    Changed;
  end;
end;

procedure TdxSpreadSheetTableView.SetLeftColumn(AValue: Integer);
begin
  ViewInfo.ChangeVisibleArea(bLeft, AValue);
end;

procedure TdxSpreadSheetTableView.SetOptions(AValue: TdxSpreadSheetTableViewOptions);
begin
  inherited Options := AValue;
end;

procedure TdxSpreadSheetTableView.SetOptionsPrint(AValue: TdxSpreadSheetTableViewOptionsPrint);
begin
  FOptionsPrint.Assign(AValue);
end;

procedure TdxSpreadSheetTableView.SetRightColumn(AValue: Integer);
begin
  ViewInfo.ChangeVisibleArea(bRight, AValue);
end;

procedure TdxSpreadSheetTableView.SetTopRow(AValue: Integer);
begin
  ViewInfo.ChangeVisibleArea(bTop, AValue);
end;

{ TdxSpreadSheetTableViewInfo }

constructor TdxSpreadSheetTableViewInfo.Create(AView: TdxSpreadSheetCustomView);
begin
  inherited Create(AView);
  FCells := TdxSpreadSheetTableViewInfoCellsList.Create;
  FColumnsHeader := TdxSpreadSheetViewInfoCellsList.Create;
  FRowsHeader := TdxSpreadSheetViewInfoCellsList.Create;
  FCommonCells := TdxSpreadSheetViewInfoCellsList.Create;
  FSelectionBounds := cxInvalidRect;
  IsDirty := True;
end;

destructor TdxSpreadSheetTableViewInfo.Destroy;
begin
  Clear;
  FreeAndNil(FCommonCells);
  FreeAndNil(FColumnsHeader);
  FreeAndNil(FRowsHeader);
  FreeAndNil(FCells);
  inherited Destroy;
end;

procedure TdxSpreadSheetTableViewInfo.Calculate;
var
  AdjustCells: Boolean;
begin
  inherited Calculate;
  Clear;
  CheckRowsBestFit;
  FBounds.BottomRight := cxPointScale(FBounds.BottomRight, 100, View.ZoomFactor);
  FColumnsHeaderArea := cxInvalidRect;
  FRowsHeaderArea := cxInvalidRect;
  FCellsArea := FBounds;
  FContentParams := Styles.GetContentStyle(View);
  FHeaderParams := Styles.GetHeaderStyle(View);
  FSelectionParams := Styles.GetSelectionStyle;
  for AdjustCells := False to True do
  begin
    if Options.ActualHeaders then
    begin
      FCellsArea.Top := Bounds.Top + LookAndFeelPainter.HeaderHeight(cxTextHeight(HeaderParams.Font)) + cxTextOffset;
      FCellsArea.Left := Bounds.Left + LookAndFeelPainter.HeaderWidth(
        cxScreenCanvas, cxBordersAll, IntToStr(Max(100, VisibleCells.Bottom)), HeaderParams.Font) + cxTextOffset * 2;
      FColumnsHeaderArea := cxRectSetTop(Bounds, Bounds.Top, FCellsArea.Top - Bounds.Top);
      FRowsHeaderArea:= cxRectSetLeft(Bounds, Bounds.Left, FCellsArea.Left - Bounds.Left);
    end;
    CalculateVisibleArea;
  end;
  cxScreenCanvas.Dormant;
  CalculateHeaders;
  CalculateCells;
  CalculateContainers;
  SelectionChanged;
  if View.IsEditing then
    View.EditingController.UpdateEditPosition;
  SpreadSheet.UpdateScrollBars;
end;

procedure TdxSpreadSheetTableViewInfo.Clear;
begin
  inherited Clear;
  if (FFocusedCell <> nil) and not FFocusedCell.IsPermanent then
    FFocusedCell := nil;
  ColumnsHeader.Clear;
  RowsHeader.Clear;
  Cells.Clear;
  CommonCells.Clear;
end;

procedure TdxSpreadSheetTableViewInfo.Draw(ACanvas: TcxCanvas);
var
  I: Integer;
  AForm: XFORM;
begin
  dxSetZoomFactor(ACanvas, View.ZoomFactor, AForm);
  try
    for I := 0 to GetPartCount - 1 do
      DrawPart(ACanvas, GetPartOffset(I), GetPartBounds(I));
    ACanvas.ExcludeClipRect(cxRect(Bounds.TopLeft, CellsArea.BottomRight));
  finally
    SetWorldTransform(ACanvas.Handle, AForm);
  end;
end;

procedure TdxSpreadSheetTableViewInfo.InitHitTest(AHitTest: TdxSpreadSheetCustomHitTest);
begin
  if not IsDirty and (View.Changes = []) then
  begin
    if PtInRect(Bounds, AHitTest.ActualHitPoint) then
      DoInitHitTestWithOriginOffset(AHitTest, GetPartOffsetByPoint(AHitTest.ActualHitPoint));
  end;
end;

procedure TdxSpreadSheetTableViewInfo.Validate;
begin
  if IsDirty then
  begin
    Calculate;
    View.Invalidate;
  end;
end;

procedure TdxSpreadSheetTableViewInfo.AddCell(ARow: TdxSpreadSheetTableRow;
  ARowIndex, AColumnIndex: Integer; const ABounds: TRect);
var
  ACell: TdxSpreadSheetCell;
  ACellViewInfo: TdxSpreadSheetTableViewCellViewInfo;
  ASide: TcxBorder;
  AStyle: TdxSpreadSheetCellStyle;
begin
  if ARow <> nil then
    ACell := ARow.GetCell(AColumnIndex)
  else
    ACell := nil;

  if not Cells.FindItemForCell(ACell, ACellViewInfo) then
  begin
    ACellViewInfo := TdxSpreadSheetTableViewCellViewInfo.Create(Self);
    Cells.Add(ACellViewInfo);
  end;

  if ACell <> nil then
    AStyle := ACell.Style
  else
    AStyle := GetCellStyle(ARowIndex, AColumnIndex);

  ACellViewInfo.Initialize(ACell, AStyle, ARowIndex, AColumnIndex);
  ACellViewInfo.SetBounds(cxNullPoint, ABounds, Bounds);
  for ASide := Low(ASide) to High(ASide) do
    SetCellBorderStyle(ACellViewInfo, ASide, AStyle, GetNeighborCellStyle(ARowIndex, AColumnIndex, ASide));
  ACellViewInfo.InitDrawValue;
end;

procedure TdxSpreadSheetTableViewInfo.AddFrozenPaneSeparator(const ABounds: TRect; var APosition: Integer);
var
  ACell: TdxSpreadSheetTableViewFrozenPaneSeparatorCellViewInfo;
begin
  ACell := TdxSpreadSheetTableViewFrozenPaneSeparatorCellViewInfo.Create(Self);
  ACell.Color := cxGetActualColor(OptionsView.FrozenPaneSeparatorColor, LookAndFeelPainter.SpreadSheetFrozenPaneSeparatorColor);
  ACell.SetBounds(ABounds, Bounds);
  if OptionsView.FrozenPaneSeparatorWidth > 0 then
    Inc(APosition, OptionsView.FrozenPaneSeparatorWidth - 1);
  CommonCells.Add(ACell);
end;

procedure TdxSpreadSheetTableViewInfo.CalculateCells;
var
  ABounds: TRect;
  AColumn: TdxSpreadSheetTableviewHeaderCellViewInfo;
  ARow: TdxSpreadSheetTableviewHeaderCellViewInfo;
  AStartIndex: Integer;
  I, J: Integer;
begin
  if FocusedCell <> nil then
    FocusedCell.SetBounds(cxNullPoint, cxRectSetOrigin(FocusedCell.AbsoluteBounds, cxInvisiblePoint), Bounds);
  for I := 0 to RowsHeader.Count - 1 do
  begin
    ARow :=  TdxSpreadSheetTableviewHeaderCellViewInfo(RowsHeader[I]);
    AStartIndex := Cells.Count;
    for J := 1 to ColumnsHeader.Count - 1 do
    begin
      AColumn := TdxSpreadSheetTableviewHeaderCellViewInfo(ColumnsHeader[J]);
      ABounds := cxRect(AColumn.AbsoluteBounds.Left - 1, ARow.AbsoluteBounds.Top - 1,
        AColumn.AbsoluteBounds.Right - 1, ARow.AbsoluteBounds.Bottom - 1);
      if J = 1 then
        AddOutOfBoundsNonEmptyCell(TdxSpreadSheetTableRow(ARow.Item), AColumn.Index, ABounds, False);
      AddCell(TdxSpreadSheetTableRow(ARow.Item), ARow.Index, AColumn.Index, ABounds);
      if J = ColumnsHeader.Count - 1 then
        AddOutOfBoundsNonEmptyCell(TdxSpreadSheetTableRow(ARow.Item), AColumn.Index, ABounds, True);
    end;
    PostProcessRowCells(TdxSpreadSheetTableRow(ARow.Item), AStartIndex, Cells.Count - 1);
  end;
end;

procedure TdxSpreadSheetTableViewInfo.CalculateHeaders;
begin
  FVisibleColumnCount := 0;
  FVisibleRowCount := 0;
  FScrollableArea := CellsArea;

  PopulateColumnHeaders;
  PopulateRowHeaders;

  if FrozenColumn >= 0 then
    FScrollableArea.Left := FrozenColumnSeparatorPosition - 1 + OptionsView.FrozenPaneSeparatorWidth;
  if FrozenRow >= 0 then
    FScrollableArea.Top := FrozenRowSeparatorPosition - 1 + OptionsView.FrozenPaneSeparatorWidth;
  FVisibleColumnCount := Max(FVisibleColumnCount, 1);
  FVisibleRowCount := Max(FVisibleRowCount, 1);
end;

procedure TdxSpreadSheetTableViewInfo.CalculateVisibleArea;
begin
  if FrozenColumn >= 0 then
    FVisibleCells.Left := 0;
  if FrozenRow >= 0 then
    FVisibleCells.Top := 0;
  CalculateLastItemIndex(cxRectWidth(CellsArea), VisibleCells.Left, FrozenColumn, FFirstScrollableColumn,
     dxSpreadSheetMaxColumnIndex, Columns, FFrozenColumnSeparatorPosition, FVisibleCells.Right);
  if FrozenColumn >= 0 then
    Inc(FFrozenColumnSeparatorPosition, FCellsArea.Left);
  CalculateLastItemIndex(cxRectHeight(CellsArea), VisibleCells.Top, FrozenRow, FFirstScrollableRow,
    dxSpreadSheetMaxRowIndex, Rows, FFrozenRowSeparatorPosition, FVisibleCells.Bottom);
  if FrozenRow >= 0 then
    Inc(FFrozenRowSeparatorPosition, FCellsArea.Top);
  FFirstColumnOrigin := Columns.GetDistance(0, VisibleCells.Left - 1);
  FFirstRowOrigin := Rows.GetDistance(0, VisibleCells.Top - 1);
  FFirstScrollableColumnOffset := Columns.GetDistance(FrozenColumn + 1, FirstScrollableColumn - 1);
  FFirstScrollableRowOffset := Rows.GetDistance(FrozenRow + 1, FirstScrollableRow - 1);
end;

function TdxSpreadSheetTableViewInfo.CanDrawCellSelection: Boolean;
begin
  Result := View.Controller.FocusedContainer = nil;
end;

procedure TdxSpreadSheetTableViewInfo.ChangeVisibleArea(ASide: TcxBorder; AValue: Integer);
begin
  ChangeVisibleAreaBounds(ASide, AValue);
  Calculate;
  View.Invalidate;
end;

procedure TdxSpreadSheetTableViewInfo.ChangeVisibleAreaBounds(ASide: TcxBorder; AValue: Integer);
var
  AIndex: Integer;
begin
  case ASide of
    bLeft, bRight:
      dxSpreadSheetValidate(AValue, 0, dxSpreadSheetMaxColumnIndex);
    bTop, bBottom:
      dxSpreadSheetValidate(AValue, 0, dxSpreadSheetMaxRowIndex);
  end;

  case ASide of
    bLeft:
      FVisibleCells.Left := AValue;
    bTop:
      FVisibleCells.Top := AValue;
    bRight:
      begin
        AIndex := CalculatePrevPageFirstIndex(AValue, True);
        if FrozenColumn < 0 then
          FVisibleCells.Left := AIndex
        else
          FirstScrollableColumn := AIndex;
      end;
    bBottom:
      begin
        AIndex := CalculatePrevPageFirstIndex(AValue, False);
        if FrozenRow < 0 then
          FVisibleCells.Top := AIndex
        else
          FirstScrollableRow := AIndex;
      end;
  end;
  IsDirty := True;
end;

procedure TdxSpreadSheetTableViewInfo.CheckRowsBestFit;
begin
  View.BeginUpdate;
  try
    Rows.CheckBestFit;
  finally
    View.EndUpdate;
  end;
end;

procedure TdxSpreadSheetTableViewInfo.DoInitHitTestWithOriginOffset(
  AHitTest: TdxSpreadSheetCustomHitTest; const AOffset: TPoint);
begin
  if not (
    ReferencesHighlighter.InitHitTest(AHitTest, AOffset) or
    ColumnsHeader.CalculateHitTest(AHitTest, AOffset) or
    RowsHeader.CalculateHitTest(AHitTest, AOffset) or
    CalculateContainersHitTestWithOriginOffset(AHitTest, AOffset) or
    Cells.CalculateHitTest(AHitTest, AOffset) or
    CommonCells.CalculateHitTest(AHitTest))
  then
    AHitTest.SetHitCode(hcBackground, True);
end;

procedure TdxSpreadSheetTableViewInfo.DoScroll(AScrollBarKind: TScrollBarKind;
  AScrollCode: TScrollCode; var AScrollPos: Integer);
var
  AFixedIndex: Integer;
  APosition: Integer;
  AScrollPage: Integer;
begin
  if AScrollCode = scEndScroll then
    Exit;

  APosition := IfThen(AScrollBarKind = sbHorizontal, FirstScrollableColumn, FirstScrollableRow);
  AScrollPage := IfThen(AScrollBarKind = sbHorizontal,
    VisibleColumnCount - (FrozenColumn + 1), VisibleRowCount - (FrozenRow + 1));
  AFixedIndex := IfThen(AScrollBarKind = sbHorizontal, FrozenColumn, FrozenRow);

  case AScrollCode of
    scLineUp:
      Dec(APosition);
    scLineDown:
      Inc(APosition);
    scPageUp:
      if AScrollBarKind = sbHorizontal then
        APosition := CalculatePrevPageFirstIndex(View.Controller.CheckColumnIndex(APosition, -1), True)
      else
        APosition := CalculatePrevPageFirstIndex(View.Controller.CheckRowIndex(APosition, -1), False);
    scPageDown:
      Inc(APosition, AScrollPage);
    scTrack:
      APosition := AScrollPos + (AFixedIndex + 1);
  end;

  if AScrollBarKind = sbHorizontal then
    FirstScrollableColumn := APosition
  else
    FirstScrollableRow := APosition;

  AScrollPos := IfThen(AScrollBarKind = sbHorizontal, HScrollBarPos, VScrollBarPos);
end;

procedure TdxSpreadSheetTableViewInfo.DrawPart(ACanvas: TcxCanvas; const AOffset: TPoint; const AClipRect: TRect);
var
  R: TRect;
begin
  ACanvas.SaveClipRegion;
  try
    ACanvas.IntersectClipRect(AClipRect);

    CommonCells.Draw(ACanvas);
    ColumnsHeader.DrawWithOriginOffset(ACanvas, AOffset);
    RowsHeader.DrawWithOriginOffset(ACanvas, AOffset);

    if cxRectIntersect(R, AClipRect, CellsArea) then
    begin
      ACanvas.IntersectClipRect(R);
      ACanvas.SaveClipRegion;
      try
        Cells.DrawWithOriginOffset(ACanvas, AOffset);
      finally
        ACanvas.RestoreClipRegion;
      end;

      ReferencesHighlighter.Draw(ACanvas, dsSecond, AOffset);

      if CanDrawCellSelection then
      begin
        R := cxRectOffset(SelectionBounds, AOffset, False);
        if cxRectIntersect(CellsArea, R) then
        begin
          ACanvas.SaveClipRegion;
          try
            TdxSpreadSheetSelectionPainter.Draw(ACanvas, R, SelectionParams.Color, [ssseFrame, ssseCorners], [coBottomRight]);
          finally
            ACanvas.RestoreClipRegion;
          end;
        end;
      end;

      Containers.DrawWithOriginOffset(ACanvas, AOffset);
    end;
  finally
    ACanvas.RestoreClipRegion;
  end;
  ACanvas.ExcludeClipRect(AClipRect);
end;

function TdxSpreadSheetTableViewInfo.GetAreaBounds(ARow, AColumn: Integer; const AArea, ACellBounds: TRect): TRect;
begin
  Result := cxRectInflate(ACellBounds, Columns.GetDistance(AArea.Left, AColumn - 1), Rows.GetDistance(AArea.Top, ARow - 1),
    Columns.GetDistance(AColumn + 1, AArea.Right), Rows.GetDistance(ARow + 1, AArea.Bottom));
  if FrozenRow >= 0 then
  begin
    if (ARow > FrozenRow) and (Result.Top <> ACellBounds.Top) then
      Dec(Result.Top, OptionsView.FrozenPaneSeparatorWidth - 1)
    else
      if (ARow <= FrozenRow) and (Result.Bottom <> ACellBounds.Bottom) then
        Inc(Result.Bottom, OptionsView.FrozenPaneSeparatorWidth - 1);
  end;
  if FrozenColumn >= 0 then
  begin
    if (AColumn > FrozenColumn) and (Result.Left <> ACellBounds.Left) then
      Dec(Result.Left, OptionsView.FrozenPaneSeparatorWidth - 1)
    else
      if (ARow <= FrozenColumn) and (Result.Bottom <> ACellBounds.Right) then
        Inc(Result.Right, OptionsView.FrozenPaneSeparatorWidth - 1);
  end;
end;

function TdxSpreadSheetTableViewInfo.GetCellClipArea(ARow, AColumn: Integer): TRect;
begin
  Result := CellsArea;
  if AColumn <= FrozenColumn then
    Result.Right := FrozenColumnSeparatorPosition + 1
  else
    if AColumn > FrozenColumn then
      Result.Left := FrozenColumnSeparatorPosition + OptionsView.FrozenPaneSeparatorWidth - 1;
  if ARow <= FrozenRow then
    Result.Bottom := FrozenRowSeparatorPosition + 1
  else
    if ARow > FrozenRow then
      Result.Top := FrozenRowSeparatorPosition + OptionsView.FrozenPaneSeparatorWidth - 1;
end;

function TdxSpreadSheetTableViewInfo.GetCellStyle(ARow, AColumn: Integer): TdxSpreadSheetCellStyle;
var
  ACell: TdxSpreadSheetCell;
begin
  ACell := nil;
  if InRange(ARow, 0, dxSpreadSheetMaxRowIndex) and InRange(AColumn, 0, dxSpreadSheetMaxColumnIndex) then
    ACell := View.Cells[ARow, AColumn];
  if ACell <> nil then
    Result := ACell.Style
  else
  begin
    Result := Rows.GetItemStyle(ARow);
    if (Result = nil) or Result.IsDefault then
      Result := Columns.GetItemStyle(AColumn);
    if Result = nil then
      Result := View.SpreadSheet.DefaultCellStyle;
  end;
end;

function TdxSpreadSheetTableViewInfo.GetItemPosition(AIndex: Integer; ASide: TcxBorder): Integer;
var
  AItems: TdxSpreadSheetTableItems;
  AMin, AMax, AMinValue, AMaxValue: Integer;
begin
  AItems := Columns;
  if AIndex < 0 then
    AIndex := MaxInt;
  AMin := VisibleCells.Left;
  AMinValue := CellsArea.Left - dxSpreadSheetSelectionThickness;
  AMax := VisibleCells.Right;
  AMaxValue := IfThen(CellsArea.Right = Bounds.Right, Bounds.Right + dxSpreadSheetSelectionThickness, CellsArea.Right) +
    FirstColumnOrigin + FirstScrollableColumnOffset;

  if ASide in [bTop, bBottom] then
  begin
    AItems := Rows;
    AMin := VisibleCells.Top;
    AMinValue := CellsArea.Top - dxSpreadSheetSelectionThickness;
    AMax := VisibleCells.Bottom;
    AMaxValue := IfThen(CellsArea.Bottom = Bounds.Bottom,  Bounds.Bottom + dxSpreadSheetSelectionThickness,
      CellsArea.Bottom) + FirstRowOrigin + FirstScrollableRowOffset;
  end;

  if AIndex < AMin then
    Result := AMinValue
  else
    if AIndex > AMax then
      Result := AMaxValue
    else
      Result := AItems.GetPosition(AIndex);

  if (ASide in [bRight, bBottom]) then
    if AIndex >= AMin then
      Inc(Result, AItems.GetItemSize(AIndex))
    else
      Result := 0;
end;

function TdxSpreadSheetTableViewInfo.GetMergedCell(ARow, AColumn: Integer): TdxSpreadSheetMergedCell;
begin
  Result := View.MergedCells.FindCell(ARow, AColumn);
end;

function TdxSpreadSheetTableViewInfo.GetNeighborCellStyle(
  ARow, AColumn: Integer; ASide: TcxBorder): TdxSpreadSheetCellStyle;
begin
  case ASide of
    bLeft:
      Result := GetCellStyle(ARow, AColumn - 1);
    bTop:
      Result := GetCellStyle(ARow - 1, AColumn);
    bRight:
      Result := GetCellStyle(ARow, AColumn + 1);
  else
    Result := GetCellStyle(ARow + 1, AColumn);
  end;
end;

function TdxSpreadSheetTableViewInfo.GetPartBounds(APart: Integer): TRect;
var
  AWidth: Integer;
begin
  Result := Bounds;
  AWidth := Max(0, OptionsView.FrozenPaneSeparatorWidth - 1);
  case APart of
    0:
    begin
      if FrozenColumn >= 0 then
        Result.Right := FrozenColumnSeparatorPosition + AWidth;
      if FrozenRow >= 0 then
        Result.Bottom := FrozenRowSeparatorPosition + AWidth;
    end;
    1:
      if FrozenRow >= 0 then
      begin
        Result.Top := FrozenRowSeparatorPosition + AWidth;
        if FrozenColumn >= 0 then
          Result.Right := FrozenColumnSeparatorPosition + AWidth
      end
      else
        Result := GetPartBounds(2);
    2:
      if FrozenColumn >= 0 then
      begin
        Result.Left := FrozenColumnSeparatorPosition + AWidth;
        if FrozenRow >= 0 then
          Result.Bottom := FrozenRowSeparatorPosition + AWidth;
      end;
    3:
    begin
      Result.Left := FrozenColumnSeparatorPosition + AWidth;
      Result.Top := FrozenRowSeparatorPosition + AWidth;
    end;
  end;
end;

function TdxSpreadSheetTableViewInfo.GetPartCount: Integer;
begin
  Result := 1 + Byte(FrozenColumn >= 0) + Byte(FrozenRow >=0) + Byte((FrozenColumn >= 0) and (FrozenRow >=0));
end;

function TdxSpreadSheetTableViewInfo.GetPartOffset(APart: Integer): TPoint;
begin
  Result := cxPoint(FirstColumnOrigin, FirstRowOrigin);
  case APart of
    1:
     if FrozenRow >= 0 then
       Inc(Result.Y, FirstScrollableRowOffset)
     else
       Result := GetPartOffset(2);
    2:
     if FrozenColumn >= 0 then
       Inc(Result.X, FirstScrollableColumnOffset);
    3:
      Result := cxPointOffset(Result, FirstScrollableColumnOffset, FirstScrollableRowOffset);
  end;
end;

function TdxSpreadSheetTableViewInfo.GetPartOffsetByPoint(const P: TPoint): TPoint;

  function GetPartIndex(const P: TPoint): Integer;
  begin
    case GetPartCount of
      2:
        if FrozenColumn >= 0 then
          Result := Ord(P.X > GetPartBounds(0).Right)
        else
          Result := Ord(P.Y > GetPartBounds(0).Bottom);
      4:
        Result := Ord(P.Y > GetPartBounds(0).Bottom) + 2 * Ord(P.X > GetPartBounds(0).Right);
    else
      Result := 0;
    end;
  end;

begin
  Result := GetPartOffset(GetPartIndex(P));
end;

procedure TdxSpreadSheetTableViewInfo.InitScrollBarsParameters;
begin
  SetScrollBarInfo(sbVertical, 0, TotalRowCount, 1, VisibleRowCount, VScrollBarPos,
    Options.ActualVerticalScrollBar, Options.ActualVerticalScrollBar);
  SetScrollBarInfo(sbHorizontal, 0, TotalColumnCount, 1, VisibleColumnCount, HScrollBarPos,
    Options.ActualHorizontalScrollBar, Options.ActualHorizontalScrollBar);
end;

procedure TdxSpreadSheetTableViewInfo.MergeBorderStyle(
  ABorder1, ABorder2: TdxSpreadSheetCellBorder; var AColor: TColor; var AStyle: TdxSpreadSheetCellBorderStyle);
var
  ADelta: Integer;
begin
  ADelta := Integer(ABorder1.Style) - Integer(ABorder2.Style);
  if (ADelta < 0) or (ADelta = 0) and (ABorder1.Color = clDefault) then
  begin
    AStyle := ABorder2.Style;
    AColor := ABorder2.Color;
  end
  else
  begin
    AColor := ABorder1.Color;
    AStyle := ABorder1.Style;
  end;
  if AStyle = sscbsDefault then
    AColor := clDefault;
end;

procedure TdxSpreadSheetTableViewInfo.PopulateColumnHeaders;
var
  ACell: TdxSpreadSheetTableViewHeaderCellViewInfo;
  ADisplayBounds: TRect;
  APrevCell: TdxSpreadSheetTableViewHeaderCellViewInfo;
  ASize, I: Integer;
  R: TRect;
begin
  ADisplayBounds := cxRect(Bounds.TopLeft, FCellsArea.TopLeft);
  R := cxRectOffset(ADisplayBounds, FirstColumnOrigin, FirstRowOrigin);

  ACell := TdxSpreadSheetTableViewHeaderCellViewInfo.Create(Self);
  ACell.Initialize(nil, -1, [nRight, nBottom], '');
  ACell.SetBounds(cxNullPoint, R, Bounds);
  ColumnsHeader.Add(ACell);

  APrevCell := nil;
  I := VisibleCells.Left;
  FLastVisibleColumn := I;
  while I <= VisibleCells.Right do
  begin
    if (ADisplayBounds.Right = FrozenColumnSeparatorPosition) and (I <= FirstScrollableColumn) then
    begin
      AddFrozenPaneSeparator(cxRectSetLeft(Bounds, FFrozenColumnSeparatorPosition - 1, OptionsView.FrozenPaneSeparatorWidth), R.Right);
      I := FirstScrollableColumn;
      Inc(R.Right, FirstScrollableColumnOffset);
    end;
    if Columns.GetItemVisible(I) or (I = VisibleCells.Left) then
    begin
      ACell := TdxSpreadSheetTableViewHeaderCellViewInfo.Create(Self);
      ASize := Columns.GetItemSize(I);
      R := cxRectSetLeft(R, R.Right, ASize);
      ADisplayBounds := cxRectSetLeft(ADisplayBounds, ADisplayBounds.Right, ASize);
      if ADisplayBounds.Right <= Bounds.Right then
      begin
        Inc(FVisibleColumnCount);
        FLastVisibleColumn := I;
      end;
      ACell.Initialize(Columns[I], I, [nLeft, nRight], Columns.GetItemText(I));
      ACell.SetBounds(cxNullPoint, R, Bounds);
      ColumnsHeader.Add(ACell);

      if APrevCell <> nil then
        APrevCell.FNextNeighbor := ACell;
      APrevCell := ACell;
    end;
    Inc(I);
  end;
  FCellsArea.Right := Min(ADisplayBounds.Right, CellsArea.Right);
end;

procedure TdxSpreadSheetTableViewInfo.PopulateRowHeaders;
var
  ACell: TdxSpreadSheetTableViewHeaderCellViewInfo;
  ADisplayBounds: TRect;
  APrevCell: TdxSpreadSheetTableViewHeaderCellViewInfo;
  ASize, I: Integer;
  R: TRect;
begin
  ADisplayBounds := cxRect(Bounds.TopLeft, FCellsArea.TopLeft);
  R := cxRectOffset(ADisplayBounds, FirstColumnOrigin, FirstRowOrigin);

  APrevCell := nil;
  I := VisibleCells.Top;
  FLastVisibleRow := I;
  while I <= VisibleCells.Bottom do
  begin
    if (ADisplayBounds.Bottom = FrozenRowSeparatorPosition) and (I <= FirstScrollableRow) then
    begin
      AddFrozenPaneSeparator(cxRectSetTop(Bounds, FFrozenRowSeparatorPosition - 1, OptionsView.FrozenPaneSeparatorWidth), R.Bottom);
      I := FirstScrollableRow;
      Inc(R.Bottom, FirstScrollableRowOffset);
    end;
    if Rows.GetItemVisible(I) or (I = VisibleCells.Top) then
    begin
      ACell := TdxSpreadSheetTableViewHeaderCellViewInfo.Create(Self);
      ASize := Rows.GetItemSize(I);
      R := cxRectSetTop(R, R.Bottom, ASize);
      ADisplayBounds := cxRectSetTop(ADisplayBounds, ADisplayBounds.Bottom, ASize);
      if ADisplayBounds.Bottom <= Bounds.Bottom then
      begin
        Inc(FVisibleRowCount);
        FLastVisibleRow := I;
      end;
      ACell.Initialize(Rows[I], I, [nTop, nBottom], Rows.GetItemText(I));
      ACell.SetBounds(cxNullPoint, R, FBounds);
      RowsHeader.Add(ACell);

      if APrevCell <> nil then
        APrevCell.FNextNeighbor := ACell;
      APrevCell := ACell;
    end;
    Inc(I);
  end;
  FCellsArea.Bottom := Min(ADisplayBounds.Bottom, CellsArea.Bottom);
end;

procedure TdxSpreadSheetTableViewInfo.PostProcessRowCells(
  ARow: TdxSpreadSheetTableRow; AStartIndex, AFinishIndex: Integer);
var
  AIndex, I, APos: Integer;
  ACell: TdxSpreadSheetTableViewCellViewInfo;
begin
  if ARow = nil then Exit;
  for AIndex := AStartIndex to AFinishIndex do
  begin
    ACell := TdxSpreadSheetTableViewCellViewInfo(Cells[AIndex]);
    if not ACell.IsTextOutOfBounds then Continue;
    if ACell.TextBounds.Right > ACell.DisplayBounds.Right - cxTextOffset then
    begin
      I := AIndex;
      APos := ACell.ContentBounds.Right;
      while RemoveNeighborCellBorder(bRight, I, AStartIndex, AFinishIndex, APos) do
        Inc(I);
      ACell.FContentBounds.Right := Min(ACell.ContentBounds.Right, APos);
    end;
    if ACell.TextBounds.Left < ACell.DisplayBounds.Left + cxTextOffset + 1 then
    begin
      I := AIndex;
      APos := ACell.ContentBounds.Left;
      while RemoveNeighborCellBorder(bLeft, I, AStartIndex, AFinishIndex, APos) do
        Dec(I);
      ACell.FContentBounds.Left := Max(ACell.ContentBounds.Left, APos);
    end;
  end;
end;

procedure TdxSpreadSheetTableViewInfo.Scroll(AScrollBarKind: TScrollBarKind;
  AScrollCode: TScrollCode; var AScrollPos: Integer);
begin
  if AScrollCode = scEndScroll then Exit;
  DoScroll(AScrollBarKind, AScrollCode, AScrollPos);
  Validate;
end;

procedure TdxSpreadSheetTableViewInfo.SelectionChanged;
var
  ASelectedArea: TRect;
begin
  inherited SelectionChanged;
  RowsHeader.ForEach(UpdateHeaderCellState, Rows);
  ColumnsHeader.ForEach(UpdateHeaderCellState, Columns);
  Containers.ForEach(UpdateContainerState, nil);
  Cells.ForEach(UpdateContentCellState, nil);

  if Selection.Count > 1 then
    ASelectedArea := cxRectBounds(Selection.FocusedColumn, Selection.FocusedRow, 0, 0)
  else
    ASelectedArea := Selection.Area;

  if not dxSpreadSheetIsEntireRowOrColumn(ASelectedArea) then
    ASelectedArea := View.MergedCells.ExpandArea(ASelectedArea);

  FSelectionBounds := cxRect(GetItemPosition(ASelectedArea.Left, bLeft), GetItemPosition(ASelectedArea.Top, bTop),
    GetItemPosition(ASelectedArea.Right, bRight), GetItemPosition(ASelectedArea.Bottom, bBottom));
  FSelectionBounds := cxRectOffset(FSelectionBounds, CellsArea.Left, CellsArea.Top);
  View.Invalidate;
end;

procedure TdxSpreadSheetTableViewInfo.SetScrollBarInfo(AScrollBarKind: TScrollBarKind;
  AMin, AMax, AStep, APage, APos: Integer; AAllowShow, AAllowHide: Boolean);
begin
  SpreadSheet.SetScrollBarInfo(AScrollBarKind, AMin, AMax, AStep, APage, APos, AAllowShow, AAllowHide);
end;

function TdxSpreadSheetTableViewInfo.UpdateContainerState(
  ACell: TdxSpreadSheetCellViewInfo; AData: TObject): Boolean;
begin
  Result := True;
  TdxSpreadSheetContainerViewInfo(ACell).UpdateState;
end;

function TdxSpreadSheetTableViewInfo.UpdateContentCellState(
  ACell: TdxSpreadSheetCellViewInfo; AData: TObject): Boolean;
begin
  Result := True;
  TdxSpreadSheetTableViewCellViewInfo(ACell).UpdateState;
  if TdxSpreadSheetTableViewCellViewInfo(ACell).Focused then
    FFocusedCell := TdxSpreadSheetTableViewCellViewInfo(ACell);
end;

function TdxSpreadSheetTableViewInfo.UpdateHeaderCellState(
  ACell: TdxSpreadSheetCellViewInfo; AData: TObject): Boolean;
begin
  Result := True;
  with TdxSpreadSheetTableViewHeaderCellViewInfo(ACell) do
    State := TdxSpreadSheetTableItems(AData).GetItemState(Index);
end;

procedure TdxSpreadSheetTableViewInfo.ValidateBounds(
  AStartIndex, AFinishIndex, AFixedIndex, ASeparatorPosition: Integer; var AStart, AFinish: Integer);
begin
  if AFixedIndex < 0 then Exit;
  if AFinishIndex > AFixedIndex then
    AFinish := Max(AFinish, ASeparatorPosition);
  if AStartIndex > AFixedIndex  then
    AStart := Max(AStart, ASeparatorPosition);
end;

procedure TdxSpreadSheetTableViewInfo.AddOutOfBoundsNonEmptyCell(
  ARow: TdxSpreadSheetTableRow; AIndex: Integer; const ABounds: TRect; AGoForward: Boolean);
var
  I: Integer;
begin
  if ARow = nil then
    Exit;

  if AGoForward then
  begin
    for I := AIndex + 1 to ARow.RowCells.LastIndex do
      if Columns.GetItemVisible(I) and ARow.IsValueDefined(I) then
      begin
        if not (ARow.Cells[I].IsMerged or ARow.IsValueDefined(I - 1)) then
          AddCell(ARow, ARow.Index, I, cxRectOffset(ABounds, Columns.GetDistance(AIndex + 1, I), 0));
        Break;
      end;
  end
  else
    for I := AIndex - 1 downto ARow.RowCells.FirstIndex do
      if Columns.GetItemVisible(I) and ARow.IsValueDefined(I) then
      begin
        if not (ARow.Cells[I].IsMerged or ARow.IsValueDefined(I + 1)) then
          AddCell(ARow, ARow.Index, I, cxRectOffset(ABounds, -Columns.GetDistance(I, AIndex - 1), 0));
        Break;
      end;
end;

procedure TdxSpreadSheetTableViewInfo.CalculateLastItemIndex(ASize, AStartIndex, AFixedIndex, AFirstScrollableIndex,
  AMaxIndex: Integer; AItems: TdxSpreadSheetTableItems; var ASeparatorPosition, ALastIndex: Integer);
var
  AItemSize: Integer;
begin
  ALastIndex := AStartIndex;
  ASeparatorPosition := 0;
  while (ASize > 0) and (ALastIndex < AMaxIndex) do
  begin
    AItemSize := AItems.GetItemSize(ALastIndex);
    if ALastIndex <= AFixedIndex then
      Inc(ASeparatorPosition, AItemSize);
    Dec(ASize, AItemSize);
    if (AFixedIndex = ALastIndex) then
    begin
      ALastIndex := AFirstScrollableIndex;
      Dec(ASize, OptionsView.FrozenPaneSeparatorWidth - 1);
    end
    else
      if ASize > 0 then
        Inc(ALastIndex);
  end;
  if AStartIndex > AFixedIndex then
    ASeparatorPosition := -1;
end;

function TdxSpreadSheetTableViewInfo.CalculatePrevPageFirstIndex(APosition: Integer; AIsHorizontal: Boolean): Integer;
begin
  if AIsHorizontal then
    Result := CalculatePrevPageFirstIndex(APosition, cxRectWidth(ScrollableArea), FrozenColumn, Columns)
  else
    Result := CalculatePrevPageFirstIndex(APosition, cxRectHeight(ScrollableArea), FrozenRow, Rows)
end;

function TdxSpreadSheetTableViewInfo.CalculatePrevPageFirstIndex(
  APosition, ASize, AFixedIndex: Integer; AItems: TdxSpreadSheetTableItems): Integer;
begin
  Result := Max(AFixedIndex + 1, APosition);
  if Result = AFixedIndex + 1 then Exit;
  while (Result > AFixedIndex) and (ASize > 0) do
  begin
    Dec(ASize, AItems.GetItemSize(Result));
    if ASize > 0 then
      Dec(Result)
    else
      if ASize < 0 then
      begin
        Inc(Result);
        while AItems.GetItemSize(Result) <= 0 do
          Inc(Result);
      end;
  end;
  Result := Max(AFixedIndex + 1, Result);
end;

function TdxSpreadSheetTableViewInfo.GetColumns: TdxSpreadSheetTableColumns;
begin
  Result := View.Columns;
end;

function TdxSpreadSheetTableViewInfo.GetColumnsHeaderHeight: Integer;
begin
  Result := CellsArea.Top - Bounds.Top;
end;

function TdxSpreadSheetTableViewInfo.GetDefaultCellStyle: TdxSpreadSheetCellStyle;
begin
  Result := View.SpreadSheet.DefaultCellStyle;
end;

function TdxSpreadSheetTableViewInfo.GetFirstScrollableColumn: Integer;
begin
  Result := VisibleCells.Left;
  if FrozenColumn >= 0 then
    Result := FFirstScrollableColumn;
end;

function TdxSpreadSheetTableViewInfo.GetFirstScrollableRow: Integer;
begin
  Result := VisibleCells.Top;
  if FrozenRow >= 0 then
    Result := FFirstScrollableRow;
end;

function TdxSpreadSheetTableViewInfo.GetFrozenColumn: Integer;
begin
  Result := View.FrozenColumn;
end;

function TdxSpreadSheetTableViewInfo.GetFrozenRow: Integer;
begin
  Result := View.FrozenRow;
end;

function TdxSpreadSheetTableViewInfo.GetFocusedCell: TdxSpreadSheetTableViewCellViewInfo;
begin
  Validate;
  Result := FFocusedCell;
end;

function TdxSpreadSheetTableViewInfo.GetGridLineColor: TColor;
begin
  Result := cxGetActualColor(OptionsView.GridLineColor, LookAndFeelPainter.DefaultGridLineColor);
end;

function TdxSpreadSheetTableViewInfo.GetHScrollBarPos: Integer;
begin
  Result := FirstScrollableColumn - (FrozenColumn + 1);
end;

function TdxSpreadSheetTableViewInfo.GetLookAndFeelPainter: TcxCustomLookAndFeelPainter;
begin
  Result := View.SpreadSheet.LookAndFeelPainter;
end;

function TdxSpreadSheetTableViewInfo.GetOrigin: TPoint;
begin
  Result := cxPoint(FirstColumnOrigin, FirstRowOrigin);
end;

function TdxSpreadSheetTableViewInfo.GetOptions: TdxSpreadSheetTableViewOptions;
begin
  Result := View.Options;
end;

function TdxSpreadSheetTableViewInfo.GetOptionsView: TdxSpreadSheetOptionsView;
begin
  Result := SpreadSheet.OptionsView;
end;

function TdxSpreadSheetTableViewInfo.GetReferencesHighlighter: TdxSpreadSheetTableViewReferencesHighlighter;
begin
  Result := View.EditingController.ReferencesHighlighter;
end;

function TdxSpreadSheetTableViewInfo.GetRows: TdxSpreadSheetTableRows;
begin
  Result := View.Rows;
end;

function TdxSpreadSheetTableViewInfo.GetRowsHeaderWidth: Integer;
begin
  Result := CellsArea.Left - Bounds.Left;
end;

function TdxSpreadSheetTableViewInfo.GetSelection: TdxSpreadSheetTableViewSelection;
begin
  Result := View.Selection;
end;

function TdxSpreadSheetTableViewInfo.GetStyles: TdxSpreadSheetStyles;
begin
  Result := View.SpreadSheet.Styles;
end;

function TdxSpreadSheetTableViewInfo.GetTotalColumnCount: Integer;
begin
  Result := HScrollBarPos + VisibleColumnCount;
  if  HScrollBarPos > 0 then
    Dec(Result);
  Result := Max(Result, Columns.LastIndex);
end;

function TdxSpreadSheetTableViewInfo.GetTotalRowCount: Integer;
begin
  Result := VScrollBarPos + VisibleRowCount;
  if VScrollBarPos > 0 then
    Dec(Result);
  Result := Max(Result, Rows.LastIndex);
end;

function TdxSpreadSheetTableViewInfo.GetView: TdxSpreadSheetTableView;
begin
  Result := TdxSpreadSheetTableView(inherited View);
end;

function TdxSpreadSheetTableViewInfo.GetVScrollBarPos: Integer;
begin
  Result := FirstScrollableRow - (FrozenRow + 1)
end;

function TdxSpreadSheetTableViewInfo.RemoveNeighborCellBorder(ASide: TcxBorder;
  AIndex, AStartIndex, AFinishIndex: Integer; var AEndPos: Integer): Boolean;
var
  AHasValue: Boolean;
  ACell, ANeighborCell: TdxSpreadSheetTableViewCellViewInfo;
begin
  ACell := TdxSpreadSheetTableViewCellViewInfo(Cells[AIndex]);
  Result := False;
  AHasValue := False;
  if (ASide = bRight) and (AIndex < AFinishIndex) then
  begin
    ANeighborCell := TdxSpreadSheetTableViewCellViewInfo(Cells[AIndex + 1]);
    AHasValue := ANeighborCell.HasValue;
    Result := not AHasValue;
    if Result then
    begin
      ANeighborCell.RemoveBorder(bLeft);
      Result := AEndPos > ANeighborCell.ContentBounds.Right;
    end
    else
      AEndPos := ACell.DisplayBounds.Right - cxTextOffset;
  end
  else
    if (ASide = bLeft) and (AIndex > AStartIndex) then
    begin
      ANeighborCell := TdxSpreadSheetTableViewCellViewInfo(Cells[AIndex - 1]);
      AHasValue := ANeighborCell.HasValue;
      Result := not AHasValue;
      if Result then
      begin
        ANeighborCell.RemoveBorder(bRight);
        Result := AEndPos < ANeighborCell.ContentBounds.Left;
      end
      else
        AEndPos := ACell.DisplayBounds.Left + cxTextOffset + 1;
    end
    else
      AEndPos := cxRectGetItem(TdxSpreadSheetTableViewCellViewInfo(Cells[AIndex]).ContentBounds, Integer(ASide));
  if not AHasValue then
    ACell.RemoveBorder(ASide);
end;

procedure TdxSpreadSheetTableViewInfo.SetCellBorderStyle(
  ACell: TdxSpreadSheetTableViewCellViewInfo; ASide: TcxBorder; AStyle, ANeighborStyle: TdxSpreadSheetCellStyle);
const
  DefaultStyles: array[Boolean] of TdxSpreadSheetCellBorderStyle = (sscbsNone, sscbsThin);
  NeighborBorderSide: array[TcxBorder] of TcxBorder = (bRight, bBottom, bLeft, bTop);
var
  ABorderColor: TColor;
  ABorderStyle: TdxSpreadSheetCellBorderStyle;
begin
  if ACell.IsInternalBorder(ASide) then
  begin
    ABorderColor := clNone;
    ABorderStyle := sscbsNone;
  end
  else
  begin
    MergeBorderStyle(AStyle.Borders[ASide], ANeighborStyle.Borders[NeighborBorderSide[ASide]], ABorderColor, ABorderStyle);

    if dxSpreadSheetIsColorDefault(ABorderColor) then
    begin
      if ABorderStyle <> sscbsDefault then
        ABorderColor := clBlack
      else
        ABorderColor := GridLineColor;
    end;
  end;

  if ABorderStyle = sscbsDefault then
  begin
    if cxColorIsValid(AStyle.Brush.BackgroundColor) or cxColorIsValid(ANeighborStyle.Brush.BackgroundColor) then
      ABorderStyle := sscbsNone
    else
      ABorderStyle := DefaultStyles[Options.ActualGridLines];
  end;

  ACell.SetBorderStyle(ASide, ABorderStyle, ABorderColor);
end;

procedure TdxSpreadSheetTableViewInfo.SetFirstScrollableColumn(AValue: Integer);
var
  APrevValue: Integer;
begin
  APrevValue := FirstScrollableColumn;
  dxSpreadSheetValidate(AValue, FrozenColumn + 1, dxSpreadSheetMaxColumnIndex - (FrozenColumn + 1));
  if FrozenColumn < 0 then
    FVisibleCells.Left := AValue
  else
    FFirstScrollableColumn := Max(AValue, FrozenColumn + 1);
  IsDirty := IsDirty or (APrevValue <> FirstScrollableColumn);
end;

procedure TdxSpreadSheetTableViewInfo.SetFirstScrollableRow(AValue: Integer);
var
  APrevValue: Integer;
begin
  APrevValue := FirstScrollableRow;
  dxSpreadSheetValidate(AValue, FrozenRow + 1, dxSpreadSheetMaxRowIndex - (FrozenRow + 1));
  if FrozenRow < 0 then
    FVisibleCells.Top := AValue
  else
    FFirstScrollableRow := Max(AValue, FrozenRow + 1);
  IsDirty := IsDirty or (APrevValue <> FirstScrollableRow);
end;

{ TdxSpreadSheetDefinedName }

destructor TdxSpreadSheetDefinedName.Destroy;
begin
  Formula := nil;
  inherited Destroy;
end;

procedure TdxSpreadSheetDefinedName.EnumReferences(AProc: TdxSpreadSheetFormulaEnumReferencesProc);
begin
  if Formula <> nil then
    Formula.EnumReferences(AProc);
end;

procedure TdxSpreadSheetDefinedName.LoadFromStream(AReader: TcxReader);
var
  AFormula: TdxSpreadSheetDefinedNameFormula;
begin
  if not AReader.ReadBoolean then
    Formula := nil
  else
    if AReader.Version >= 4 then
      Reference := AReader.ReadWideString
    else
    begin
      AFormula := TdxSpreadSheetDefinedNameFormula.Create(Self);
      AFormula.LoadFromStream(AReader);
      Formula := AFormula;
    end;
end;

procedure TdxSpreadSheetDefinedName.SaveToStream(AWriter: TcxWriter);
begin
  AWriter.WriteWideString(Caption);
  if Scope <> nil then
    AWriter.WriteInteger(Scope.Index)
  else
    AWriter.WriteInteger(-1);

  AWriter.WriteBoolean(Formula <> nil);
  if Formula <> nil then
    AWriter.WriteWideString(Reference);
end;

procedure TdxSpreadSheetDefinedName.UpdateReference;
var
  AReference: TdxUnicodeString;
begin
  AReference := Reference;
  FreeAndNil(FFormula);
  Reference := AReference;
end;

procedure TdxSpreadSheetDefinedName.ClearResult;
begin
  if Formula <> nil then
    Formula.ClearResult;
end;

function TdxSpreadSheetDefinedName.Compare(ACandidate: TdxSpreadSheetDefinedName): Integer;
begin
  if Self = ACandidate then
    Result := 0
  else
    Result := CompareEx(ACandidate.Caption, ACandidate.Scope);
end;

function TdxSpreadSheetDefinedName.CompareEx(const ACaption: TdxUnicodeString;
  AScope: TdxSpreadSheetCustomView; AScopeMaybeEmpty: Boolean = False): Integer;
begin
  Result := dxSpreadSheetCompareText(Caption, ACaption);
  if Result = 0 then
  begin
    if (Scope <> nil) and (AScope <> nil) then
      Result := Scope.Index - AScope.Index
    else
      Result := dxCompareValues(Scope, AScope);

    if (Result <> 0) and AScopeMaybeEmpty and (AScope = nil) then
      Result := 0;
  end;
end;

procedure TdxSpreadSheetDefinedName.Initialize(const ACaption, AReference: TdxUnicodeString;
  AScope: TdxSpreadSheetCustomView);
begin
  FCaption := ACaption;
  FScope := AScope;
  Reference := AReference;
end;

function TdxSpreadSheetDefinedName.GetIndex: Integer;
begin
  Result := Owner.ItemList.IndexOf(Self);
end;

function TdxSpreadSheetDefinedName.GetReference: TdxUnicodeString;
begin
  if Formula = nil then
    Result := ''
  else
    Result := Formula.AsText;
end;

function TdxSpreadSheetDefinedName.GetSpreadSheet: TdxCustomSpreadSheet;
begin
  Result := Owner.SpreadSheet;
end;

function TdxSpreadSheetDefinedName.GetValueAsText: TdxUnicodeString;
begin
  Result := VarToStr(Formula.Value);
end;

procedure TdxSpreadSheetDefinedName.SetCaption(const AValue: TdxUnicodeString);
begin
  if not dxSpreadSheetTextIsEqual(AValue, FCaption) then
  begin
    SpreadSheet.FormulaController.DoNameChanged(Self, AValue);
    FCaption := AValue;
    Changed;
  end;
end;

procedure TdxSpreadSheetDefinedName.SetFormula(const AValue: TdxSpreadSheetDefinedNameFormula);
begin
  if FFormula <> AValue then
  begin
    FreeAndNil(FFormula);
    FFormula := AValue;
    Changed;
  end;
end;

procedure TdxSpreadSheetDefinedName.SetReference(AValue: TdxUnicodeString);
var
  AFormula: TdxSpreadSheetFormula;
  AParser: TdxSpreadSheetFormulaParser;
begin
  if dxSpreadSheetTextIsEqual(AValue, Reference) then
    Exit;

  AParser := TdxSpreadSheetFormulaParser.Create(SpreadSheet);
  try
    AFormula := TdxSpreadSheetDefinedNameFormula.Create(Self);
    AValue := Trim(AValue);
    if (Length(AValue) >= 1) and (AValue[1] <> SpreadSheet.FormatSettings.Operations[opEQ]) then
      AValue := SpreadSheet.FormatSettings.Operations[opEQ] + AValue;
    if AParser.ParseFormula(AValue, AFormula) then
      Formula := AFormula as TdxSpreadSheetDefinedNameFormula
    else
    begin
      FreeAndNil(AFormula);
      raise EdxSpreadSheetError.CreateFmt(cxGetResourceString(@sdxErrorInvalidReference), [AValue, AFormula.ErrorIndex]);
    end;
  finally
    AParser.Free;
  end;
end;

procedure TdxSpreadSheetDefinedName.SetScope(const AValue: TdxSpreadSheetCustomView);
begin
  FScope := AValue;
end;

{ TdxSpreadSheetDefinedNames }

function TdxSpreadSheetDefinedNames.Add(const ACaption, AReference: TdxUnicodeString;
  AScope: TdxSpreadSheetCustomView = nil): TdxSpreadSheetDefinedName;
begin
  if Find(ACaption, AScope, False) <> -1 then
    raise EdxSpreadSheetError.CreateFmt(cxGetResourceString(@sdxErrorDefinedNameAlreadyExists), [ACaption]);
  Result := CreateItem as TdxSpreadSheetDefinedName;
  Result.Initialize(ACaption, AReference, AScope);
end;

function TdxSpreadSheetDefinedNames.AddFromStream(AReader: TcxReader): TdxSpreadSheetDefinedName;
var
  ACaption: TdxUnicodeString;
  AScope: TdxSpreadSheetCustomView;
  AScopeIndex: Integer;
begin
  ACaption := AReader.ReadWideString;
  AScopeIndex := AReader.ReadInteger;
  if AScopeIndex >= 0 then
    AScope := SpreadSheet.Sheets[AScopeIndex]
  else
    AScope := nil;

  Result := Add(ACaption, '', AScope);
  Result.LoadFromStream(AReader);
end;

function TdxSpreadSheetDefinedNames.AddOrSet(const ACaption, AReference: TdxUnicodeString;
  AScope: TdxSpreadSheetCustomView = nil): TdxSpreadSheetDefinedName;
begin
  Result := GetItemByName(ACaption, AScope);
  if Result = nil then
    Result := CreateItem as TdxSpreadSheetDefinedName;
  Result.Initialize(ACaption, AReference, AScope);
end;

function TdxSpreadSheetDefinedNames.GetItemByName(
  const ACaption: TdxUnicodeString; AScope: TdxSpreadSheetCustomView): TdxSpreadSheetDefinedName;
var
  AIndex: Integer;
begin
  AIndex := IndexOf(ACaption, AScope);
  if AIndex >= 0 then
    Result := Items[AIndex]
  else
    Result := nil;
end;

function TdxSpreadSheetDefinedNames.IndexOf(const ACaption: TdxUnicodeString): Integer;
begin
  Result := Find(ACaption, nil, False);
  if Result < 0 then
    Result := Find(ACaption, nil, True);
end;

function TdxSpreadSheetDefinedNames.IndexOf(const ACaption: TdxUnicodeString; AScope: TdxSpreadSheetCustomView): Integer;
begin
  Result := Find(ACaption, AScope, False);
end;

procedure TdxSpreadSheetDefinedNames.AfterLoad;
var
  I: Integer;
begin
  SpreadSheet.BeginUpdate;
  try
    for I := 0 to Count - 1 do
      Items[I].UpdateReference;
  finally
    SpreadSheet.EndUpdate;
  end;
end;

procedure TdxSpreadSheetDefinedNames.CheckSorted;
begin
  if Sorted then Exit;
  ItemList.Sort(@CompareNames);
  Sorted := True;
end;

function TdxSpreadSheetDefinedNames.CreateItem: TdxSpreadSheetObjectListItem;
begin
  Result := TdxSpreadSheetDefinedName.Create(Self);
  Sorted := False;
end;

function TdxSpreadSheetDefinedNames.Find(const ACaption: TdxUnicodeString;
  AScope: TdxSpreadSheetCustomView; AScopeMaybeEmpty: Boolean): Integer;
var
  L, H, I, C: Integer;
  AItem: TdxSpreadSheetDefinedName;
begin
  Result := -1;
  CheckSorted;
  if Count = 0 then Exit;
  L := 0;
  H := ItemList.Count - 1;
  while L <= H do
  begin
    I := (L + H) shr 1;
    AItem := TdxSpreadSheetDefinedName(ItemList.List[I]);
    C := AItem.CompareEx(ACaption, AScope, AScopeMaybeEmpty);
    if C = 0 then
    begin
      Result := I;
      Exit;
    end
    else
      if C < 0 then
        L := I + 1
      else
        H := I - 1;
  end;
end;

function TdxSpreadSheetDefinedNames.GetName(AIndex: Integer): TdxSpreadSheetDefinedName;
begin
  Result := TdxSpreadSheetDefinedName(inherited Items[AIndex]);
end;

class function TdxSpreadSheetDefinedNames.CompareNames(AName1, AName2: TdxSpreadSheetDefinedName): Integer;
begin
  Result := AName1.Compare(AName2);
end;

{ TdxSpreadSheetExternalLink }

function TdxSpreadSheetExternalLink.GetActualTarget: TdxUnicodeString;
begin
  Result := Target;
end;

{ TdxSpreadSheetExternalLinks }

function TdxSpreadSheetExternalLinks.Add(const ATarget: TdxUnicodeString): TdxSpreadSheetExternalLink;
begin
  Result := TdxSpreadSheetExternalLink(CreateItem);
  Result.FTarget := ATarget;
end;

function TdxSpreadSheetExternalLinks.GetLinkByTarget(const ATarget: TdxUnicodeString): TdxSpreadSheetExternalLink;
var
  I: Integer;
begin
  for I := 0 to Count - 1 do
  begin
    Result := Items[I];
    if dxSpreadSheetTextIsEqual(Result.Target, ATarget) then Exit;
  end;
  Result := nil;
end;

function TdxSpreadSheetExternalLinks.CreateItem: TdxSpreadSheetObjectListItem;
begin
  Result := TdxSpreadSheetExternalLink.Create(Self);
end;

function TdxSpreadSheetExternalLinks.GetItem(AIndex: Integer): TdxSpreadSheetExternalLink;
begin
  Result := TdxSpreadSheetExternalLink(inherited Items[AIndex]);
end;

{ TdxSpreadSheetCellCustomAttribute }

constructor TdxSpreadSheetCellCustomAttribute.Create(AOwner: TdxSpreadSheetCellStyle);
begin
  inherited Create;
  FOwner := AOwner;
end;

{ TdxSpreadSheetCellBorder }

constructor TdxSpreadSheetCellBorder.Create(AOwner: TdxSpreadSheetCellStyle; AKind: TcxBorder);
begin
  inherited Create(AOwner);
  FKind := AKind;
end;

procedure TdxSpreadSheetCellBorder.Assign(ASource: TdxSpreadSheetCellBorder);
begin
  ChangeBorder(ASource.Style, ASource.Color);
end;

procedure TdxSpreadSheetCellBorder.ChangeBorder(AStyle: TdxSpreadSheetCellBorderStyle; AColor: TColor);
var
  AHandle: TdxSpreadSheetBordersHandle;
begin
  if (Style <> AStyle) or (Color <> AColor) then
  begin
    AHandle := Handle.Clone;
    AHandle.BorderColor[Kind] := AColor;
    AHandle.BorderStyle[Kind] := AStyle;
    Handle := AHandle;
  end;
end;

function TdxSpreadSheetCellBorder.GetColor: TColor;
begin
  Result := Handle.BorderColor[Kind];
end;

function TdxSpreadSheetCellBorder.GetHandle: TdxSpreadSheetBordersHandle;
begin
  Result := Owner.Handle.Borders;
end;

function TdxSpreadSheetCellBorder.GetStyle: TdxSpreadSheetCellBorderStyle;
begin
  Result := Handle.BorderStyle[Kind];
end;

procedure TdxSpreadSheetCellBorder.Reset;
begin
  ChangeBorder(sscbsDefault, clDefault);
end;

procedure TdxSpreadSheetCellBorder.SetColor(const AValue: TColor);
begin
  ChangeBorder(Style, AValue);
end;

procedure TdxSpreadSheetCellBorder.SetHandle(const AValue: TdxSpreadSheetBordersHandle);
begin
  Owner.BeginUpdate;
  try
    Owner.Handle.Borders := Owner.CellStyles.Borders.AddBorders(AValue);
  finally
    Owner.EndUpdate;
  end;
end;

procedure TdxSpreadSheetCellBorder.SetStyle(const AValue: TdxSpreadSheetCellBorderStyle);
begin
  ChangeBorder(AValue, Color);
end;

{ TdxSpreadSheetCellBrush }

procedure TdxSpreadSheetCellBrush.Assign(ABrush: TdxSpreadSheetCellBrush);
begin
  ChangeBrush(ABrush.Style, ABrush.BackgroundColor, ABrush.ForegroundColor);
end;

procedure TdxSpreadSheetCellBrush.ChangeBrush(AStyle: TdxSpreadSheetCellFillStyle; ABackgroundColor, AForegroundColor: TColor);
var
  ABrushHandle: TdxSpreadSheetBrushHandle;
begin
  if (Style <> AStyle) or (BackgroundColor <> ABackgroundColor) or (ForegroundColor <> AForegroundColor) then
  begin
    ABrushHandle := Handle.Clone;
    ABrushHandle.BackgroundColor := ABackgroundColor;
    ABrushHandle.ForegroundColor := AForegroundColor;
    ABrushHandle.Style := AStyle;
    Handle := ABrushHandle;
  end;
end;

function TdxSpreadSheetCellBrush.GetBackgroundColor: TColor;
begin
  Result := Handle.BackgroundColor;
end;

function TdxSpreadSheetCellBrush.GetStyle: TdxSpreadSheetCellFillStyle;
begin
  Result := Handle.Style;
end;

function TdxSpreadSheetCellBrush.GetForegroundColor: TColor;
begin
  Result := Handle.ForegroundColor;
end;

function TdxSpreadSheetCellBrush.GetHandle: TdxSpreadSheetBrushHandle;
begin
  Result := Owner.Handle.Brush;
end;

procedure TdxSpreadSheetCellBrush.SetBackgroundColor(const Value: TColor);
begin
  ChangeBrush(Style, Value, ForegroundColor);
end;

procedure TdxSpreadSheetCellBrush.SetStyle(const Value: TdxSpreadSheetCellFillStyle);
begin
  ChangeBrush(Value, BackgroundColor, ForegroundColor);
end;

procedure TdxSpreadSheetCellBrush.SetForegroundColor(const Value: TColor);
begin
  ChangeBrush(Style, BackgroundColor, Value);
end;

procedure TdxSpreadSheetCellBrush.SetHandle(const AValue: TdxSpreadSheetBrushHandle);
begin
  Owner.BeginUpdate;
  try
    Owner.Handle.Brush := Owner.CellStyles.Brushes.AddBrush(AValue);
  finally
    Owner.EndUpdate;
  end;
end;

{ TdxSpreadSheetCellFont }

procedure TdxSpreadSheetCellFont.Assign(AFont: TdxSpreadSheetCellFont);
begin
  ChangeFont(AFont.Name, AFont.Charset, AFont.Color, AFont.Size, AFont.Pitch, AFont.Style);
end;

procedure TdxSpreadSheetCellFont.Assign(AFont: TFont);
begin
  ChangeFont(AFont.Name, AFont.Charset, AFont.Color, AFont.Size, AFont.Pitch, AFont.Style);
end;

procedure TdxSpreadSheetCellFont.AssignToFont(ATargetFont: TFont);
begin
  Handle.AssignToFont(ATargetFont);
end;

procedure TdxSpreadSheetCellFont.ChangeFont(const AName: TFontName; ACharset: TFontCharset;
  AColor: TColor; ASize: Integer; APitch: TFontPitch; AStyle: TFontStyles);
var
  AFont: TdxSpreadSheetFontHandle;
begin
  if (Charset <> ACharset) or (Color <> AColor) or (Size <> ASize) or (Pitch <> APitch) or (Style <> AStyle) or (Name <> AName) then
  begin
    AFont := Handle.Clone;
    AFont.Name := AName;
    AFont.Charset := ACharset;
    AFont.Color := AColor;
    AFont.Size := ASize;
    AFont.Pitch := APitch;
    AFont.Style := AStyle;
    Handle := AFont;
  end;
end;

function TdxSpreadSheetCellFont.GetCharset: TFontCharset;
begin
  Result := Handle.Charset;
end;

function TdxSpreadSheetCellFont.GetColor: TColor;
begin
  Result := Handle.Color;
end;

function TdxSpreadSheetCellFont.GetHandle: TdxSpreadSheetFontHandle;
begin
  Result := Owner.Handle.Font;  
end;

function TdxSpreadSheetCellFont.GetHeight: Integer;
begin
  Result := -MulDiv(Size, 72, cxGetPixelsPerInch.cx)
end;

function TdxSpreadSheetCellFont.GetName: TFontName;
begin
  Result := Handle.Name;
end;

function TdxSpreadSheetCellFont.GetPitch: TFontPitch;
begin
  Result := Handle.Pitch;
end;

function TdxSpreadSheetCellFont.GetSize: Integer;
begin
  Result := Handle.Size;
end;

function TdxSpreadSheetCellFont.GetStyle: TFontStyles;
begin
  Result := Handle.Style;
end;

procedure TdxSpreadSheetCellFont.SetCharset(const AValue: TFontCharset);
begin
  ChangeFont(Name, AValue, Color, Size, Pitch, Style);
end;

procedure TdxSpreadSheetCellFont.SetColor(const AValue: TColor);
begin
  ChangeFont(Name, Charset, AValue, Size, Pitch, Style);
end;

procedure TdxSpreadSheetCellFont.SetHandle(const AValue: TdxSpreadSheetFontHandle);
begin
  Owner.BeginUpdate;
  try
    Owner.Handle.Font := Owner.CellStyles.Fonts.AddFont(AValue);
  finally
    Owner.EndUpdate;
  end;
end;

procedure TdxSpreadSheetCellFont.SetHeight(const AValue: Integer);
begin
  Size := -MulDiv(AValue, cxGetPixelsPerInch.cx, 72);
end;

procedure TdxSpreadSheetCellFont.SetName(const AValue: TFontName);
begin
  ChangeFont(AValue, Charset, Color, Size, Pitch, Style);
end;

procedure TdxSpreadSheetCellFont.SetPitch(const AValue: TFontPitch);
begin
  ChangeFont(Name, Charset, Color, Size, AValue, Style);
end;

procedure TdxSpreadSheetCellFont.SetSize(const AValue: Integer);
begin
  ChangeFont(Name, Charset, Color, AValue, Pitch, Style);
end;

procedure TdxSpreadSheetCellFont.SetStyle(const AValue: TFontStyles);
begin
  ChangeFont(Name, Charset, Color, Size, Pitch, AValue);
end;

{ TdxSpreadSheetCellDataFormat }

procedure TdxSpreadSheetCellDataFormat.Assign(ADataFormat: TdxSpreadSheetCellDataFormat);
begin
  FormatCode := ADataFormat.FormatCode;
end;

procedure TdxSpreadSheetCellDataFormat.Format(const AValue: Variant; AValueType: TdxSpreadSheetCellDataType;
  AFormatSettings: TdxSpreadSheetFormatSettings; var AResult: TdxSpreadSheetNumberFormatResult);
begin
  FormatterNeeded;
  TdxSpreadSheetNumberFormat(Handle.Formatter).Format(AValue, AValueType, AFormatSettings, AResult);
end;

procedure TdxSpreadSheetCellDataFormat.FormatterNeeded;
begin
  if Handle.Formatter = nil then
    Handle.Formatter := TdxSpreadSheetNumberFormat.Create(FormatCode, FormatCodeID);
end;

function TdxSpreadSheetCellDataFormat.GetFormatCode: TdxUnicodeString;
begin
  Result := Handle.FormatCode;
end;

function TdxSpreadSheetCellDataFormat.GetFormatCodeID: Integer;
begin
  Result := Handle.FormatCodeID;
end;

function TdxSpreadSheetCellDataFormat.GetFormats: TdxSpreadSheetFormats;
begin
  Result := Owner.CellStyles.Formats;
end;

function TdxSpreadSheetCellDataFormat.GetHandle: TdxSpreadSheetFormatHandle;
begin
  Result := Owner.Handle.DataFormat;
end;

function TdxSpreadSheetCellDataFormat.GetIsDateTime: Boolean;
begin
  FormatterNeeded;
  Result := TdxSpreadSheetNumberFormat(Handle.Formatter).IsDateTime;
end;

function TdxSpreadSheetCellDataFormat.GetIsText: Boolean;
begin
  FormatterNeeded;
  Result := TdxSpreadSheetNumberFormat(Handle.Formatter).IsText;
end;

procedure TdxSpreadSheetCellDataFormat.SetFormatCode(const AValue: TdxUnicodeString);
var
  ID: Integer;
begin
  if FormatCode <> AValue then
  begin
    ID := Formats.PredefinedFormats.GetIDByFormatCode(AValue);
    if ID >= 0 then
      FormatCodeID := ID
    else
      Handle := Formats.AddFormat(AValue);
  end;
end;

procedure TdxSpreadSheetCellDataFormat.SetFormatCodeID(AValue: Integer);
var
  AHandle: TdxSpreadSheetFormatHandle;
begin
  if FormatCodeID <> AValue then
  begin
    AHandle := Formats.PredefinedFormats.GetFormatHandleByID(AValue);
    if AHandle = nil then
      raise EdxSpreadSheetError.CreateFmt(sdxErrorInvalidFormatCodeID, [AValue]);
    Handle := AHandle;
  end;
end;

procedure TdxSpreadSheetCellDataFormat.SetHandle(AValue: TdxSpreadSheetFormatHandle);
begin
  if AValue <> Handle then
  begin
    Owner.BeginUpdate;
    try
      Owner.Handle.DataFormat := AValue;
    finally
      Owner.EndUpdate;
    end;
  end;
end;

{ TdxSpreadSheetCellStyle }

constructor TdxSpreadSheetCellStyle.Create(AOwner: TObject);
begin
  inherited Create;
  FOwner := AOwner;
  Handle := CellStyles.DefaultStyle;
end;

destructor TdxSpreadSheetCellStyle.Destroy;
var
  ABorder: TcxBorder;
begin
  ReleaseHandle;
  FreeAndNil(FBrush);
  FreeAndNil(FFont);
  FreeAndNil(FDataFormat);
  for ABorder := Low(ABorder) to High(ABorder) do
    FreeAndNil(FBorders[ABorder]);
  inherited Destroy;
end;

procedure TdxSpreadSheetCellStyle.Assign(AStyle: TdxSpreadSheetCellStyle);
var
  ABorder: TcxBorder;
begin
  if AStyle <> Self then
  begin
    BeginUpdate;
    try
      AlignHorz := AStyle.AlignHorz;
      AlignHorzIndent := AStyle.AlignHorzIndent;
      AlignVert := AStyle.AlignVert;
      Brush := AStyle.Brush;
      DataFormat := AStyle.DataFormat;
      Font := AStyle.Font;
      State := AStyle.State;
      for ABorder := Low(ABorder) to High(ABorder) do
        Borders[ABorder] := AStyle.Borders[ABorder];
    finally
      EndUpdate;
    end;
  end;
end;

procedure TdxSpreadSheetCellStyle.BeginUpdate;
begin
  Inc(FUpdateLockCount);
  if UpdateLockCount = 1 then
    CloneHandle;
end;

procedure TdxSpreadSheetCellStyle.EndUpdate;
begin
  Dec(FUpdateLockCount);
  if UpdateLockCount = 0 then
    ReplaceHandle;
end;

function TdxSpreadSheetCellStyle.GetState: TdxSpreadSheetCellStates;
begin
  Result := Handle.States;
end;

procedure TdxSpreadSheetCellStyle.SetState(const AValue: TdxSpreadSheetCellStates);
begin
  if State <> AValue then
  begin
    BeginUpdate;
    try
      Handle.States := AValue;
    finally
      EndUpdate;
    end;
  end;
end;

procedure TdxSpreadSheetCellStyle.Changed;
begin
  if (UpdateLockCount = 0) and (TdxSpreadSheetCell(Owner).Style = Self) then
    TdxSpreadSheetCell(Owner).StyleChanged;
end;

procedure TdxSpreadSheetCellStyle.CloneHandle;
begin
  FPrevHandle := Handle;
  FPrevHandle.AddRef;
  Handle := Handle.Clone;
end;

function TdxSpreadSheetCellStyle.GetSpreadSheet: TdxCustomSpreadSheet;
begin
  Result := TdxSpreadSheetCell(Owner).View.SpreadSheet;
end;

function TdxSpreadSheetCellStyle.GetCellStyles: TdxSpreadSheetCellStyles;
begin
  Result := SpreadSheet.CellStyles;
end;

procedure TdxSpreadSheetCellStyle.SetHandle(const AHandle: TdxSpreadSheetCellStyleHandle);
begin
  if (FHandle <> nil) and SpreadSheet.History.CanAddCommand and (TdxHashTableItem(FHandle) <> AHandle) and
    (Owner is TdxSpreadSheetCell) and (TdxSpreadSheetCell(Owner).Style <> nil) then
      SpreadSheet.History.AddCommand(TdxSpreadSheetHistoryChangeCellStyleCommand.CreateEx(TdxSpreadSheetCell(Owner)));
  if dxChangeHandle(TdxHashTableItem(FHandle), AHandle) then
    Changed;
end;

procedure TdxSpreadSheetCellStyle.ProcessStyleChanges(APrevStyle, ANewStyle: TdxSpreadSheetCellStyleHandle);
begin
  TdxSpreadSheetCell(Owner).ProcessStyleChanges(APrevStyle, ANewStyle);
end;

procedure TdxSpreadSheetCellStyle.ReleaseHandle;
begin
  Handle := nil;
end;

procedure TdxSpreadSheetCellStyle.ReplaceHandle;
begin
  if FPrevHandle.IsEqual(Handle) then
    Handle := FPrevHandle
  else
  begin
    Handle := CellStyles.AddStyle(Handle.Clone);
    ProcessStyleChanges(FPrevHandle, Handle);
  end;
  FPrevHandle.Release;
  FPrevHandle := nil;
end;

procedure TdxSpreadSheetCellStyle.ChangeState(AState: TdxSpreadSheetCellState; AValue: Boolean);
begin
  if AValue then
    State := State + [AState]
  else
    State := State - [AState];
end;

function TdxSpreadSheetCellStyle.GetAlignHorz: TdxSpreadSheetDataAlignHorz;
begin
  Result := Handle.AlignHorz;
end;

function TdxSpreadSheetCellStyle.GetAlignHorzIndent: Integer;
begin
  Result := Handle.AlignHorzIndent;
end;

function TdxSpreadSheetCellStyle.GetAlignVert: TdxSpreadSheetDataAlignVert;
begin
  Result := Handle.AlignVert;
end;

function TdxSpreadSheetCellStyle.GetBorder(ABorder: TcxBorder): TdxSpreadSheetCellBorder;
begin
  if FBorders[ABorder] = nil then
    FBorders[ABorder] := TdxSpreadSheetCellBorder.Create(Self, ABorder);
  Result := FBorders[ABorder];
end;

function TdxSpreadSheetCellStyle.GetBrush: TdxSpreadSheetCellBrush;
begin
  if FBrush = nil then
    FBrush := TdxSpreadSheetCellBrush.Create(Self);
  Result := FBrush;
end;

function TdxSpreadSheetCellStyle.GetHidden: Boolean;
begin
  Result := csHidden in Handle.States;
end;

function TdxSpreadSheetCellStyle.GetDataFormat: TdxSpreadSheetCellDataFormat;
begin
  if FDataFormat = nil then
    FDataFormat := TdxSpreadSheetCellDataFormat.Create(Self);
  Result := FDataFormat;
end;

function TdxSpreadSheetCellStyle.GetIsDefault: Boolean;
begin
  Result := Handle = GetCellStyles.DefaultStyle;
end;

function TdxSpreadSheetCellStyle.GetFont: TdxSpreadSheetCellFont;
begin
  if FFont = nil then
    FFont := TdxSpreadSheetCellFont.Create(Self);
  Result := FFont;
end;

function TdxSpreadSheetCellStyle.GetLocked: Boolean;
begin
  Result := csLocked in Handle.States;
end;

function TdxSpreadSheetCellStyle.GetShrinkToFit: Boolean;
begin
  Result := csShrinkToFit in Handle.States;
end;

function TdxSpreadSheetCellStyle.GetWordWrap: Boolean;
begin
  Result := csWordWrap in Handle.States;
end;

procedure TdxSpreadSheetCellStyle.SetAlignHorz(const AValue: TdxSpreadSheetDataAlignHorz);
begin
  if AlignHorz <> AValue then
  begin
    BeginUpdate;
    try
      Handle.AlignHorz := AValue;
    finally
      EndUpdate;
    end;
  end;
end;

procedure TdxSpreadSheetCellStyle.SetAlignHorzIndent(const AValue: Integer);
begin
  if AlignHorzIndent <> AValue then
  begin
    BeginUpdate;
    try
      Handle.AlignHorzIndent := AValue;
    finally
      EndUpdate;
    end;
  end;
end;

procedure TdxSpreadSheetCellStyle.SetAlignVert(const AValue: TdxSpreadSheetDataAlignVert);
begin
  if AlignVert <> AValue then
  begin
    BeginUpdate;
    try
      Handle.AlignVert := AValue;
    finally
      EndUpdate;
    end;
  end;
end;

procedure TdxSpreadSheetCellStyle.SetBorder(ABorder: TcxBorder; const AValue: TdxSpreadSheetCellBorder);
begin
  Borders[ABorder].Assign(AValue);
end;

procedure TdxSpreadSheetCellStyle.SetBrush(const AValue: TdxSpreadSheetCellBrush);
begin
  Brush.Assign(AValue);
end;

procedure TdxSpreadSheetCellStyle.SetDataFormat(const AValue: TdxSpreadSheetCellDataFormat);
begin
  DataFormat.Assign(AValue);
end;

procedure TdxSpreadSheetCellStyle.SetFont(const AValue: TdxSpreadSheetCellFont);
begin
  Font.Assign(AValue);
end;

procedure TdxSpreadSheetCellStyle.SetHidden(const AValue: Boolean);
begin
  ChangeState(csHidden, AValue);
end;

procedure TdxSpreadSheetCellStyle.SetLocked(const AValue: Boolean);
begin
  ChangeState(csLocked, AValue);
end;

procedure TdxSpreadSheetCellStyle.SetShrinkToFit(const AValue: Boolean);
begin
  ChangeState(csShrinkToFit, AValue);
end;

procedure TdxSpreadSheetCellStyle.SetWordWrap(const AValue: Boolean);
begin
  ChangeState(csWordWrap, AValue);
end;

{ TdxSpreadSheetCustomFilerProgressHelper }

constructor TdxSpreadSheetCustomFilerProgressHelper.Create(AOwner: TdxSpreadSheetCustomFiler; AStageCount: Integer);
begin
  inherited Create;
  FOwner := AOwner;
  FStageCount := AStageCount;
  FStageTaskCount := 1;
end;

procedure TdxSpreadSheetCustomFilerProgressHelper.BeforeDestruction;
begin
  Progress := 100;
  inherited BeforeDestruction;
end;

procedure TdxSpreadSheetCustomFilerProgressHelper.BeginStage(ATaskCount: Integer);
begin
  FStage := Stage + 1;
  FStageTaskNumber := 1;
  FStageTaskCount := ATaskCount;
  CalculateProgress;
end;

procedure TdxSpreadSheetCustomFilerProgressHelper.EndStage;
begin
  FStageTaskNumber := FStageTaskCount;
  CalculateProgress;
end;

procedure TdxSpreadSheetCustomFilerProgressHelper.NextTask(ASkipCount: Integer = 1);
begin
  FStageTaskNumber := Min(FStageTaskNumber + ASkipCount, FStageTaskCount);
  CalculateProgress;
end;

procedure TdxSpreadSheetCustomFilerProgressHelper.CalculateProgress;
begin
  if (FStageTaskCount > 0) and (FStageCount > 0) then
    Progress := Trunc(100 * ((Stage - 1) + FStageTaskNumber / FStageTaskCount) / FStageCount);
end;

procedure TdxSpreadSheetCustomFilerProgressHelper.SetProgress(AValue: Integer);
begin
  if FProgress <> AValue then
  begin
    FProgress := AValue;
    FOwner.DoProgress(Progress);
  end;
end;

procedure TdxSpreadSheetCustomFilerProgressHelper.SkipStage;
begin
  BeginStage(1);
  EndStage;
end;

{ TdxSpreadSheetCustomFiler }

constructor TdxSpreadSheetCustomFiler.Create(AOwner: TdxCustomSpreadSheet; AStream: TStream);
begin
  inherited Create(AOwner);
  FStream := AStream;
  FIgnoreMessages := [ssmtWarning];
  FProgressHelper := CreateProgressHelper;
  dxGetLocaleFormatSettings(dxGetInvariantLocaleID, FInvariantFormatSettings);
end;

destructor TdxSpreadSheetCustomFiler.Destroy;
begin
  FreeAndNil(FProgressHelper);
  inherited Destroy;
end;

procedure TdxSpreadSheetCustomFiler.DoError(const AMessage: string; AType: TdxSpreadSheetMessageType);
begin
  if not (AType in IgnoreMessages) then
    raise EdxSpreadSheetReaderError.Create(AMessage);
end;

procedure TdxSpreadSheetCustomFiler.DoError(
  const AFormatString: string; const AArguments: array of const; AType: TdxSpreadSheetMessageType);
begin
  DoError(Format(AFormatString, AArguments), AType);
end;

procedure TdxSpreadSheetCustomFiler.DoProgress(AProgress: Integer);
begin
  if Assigned(OnProgress) then
    OnProgress(SpreadSheet, AProgress);
end;

procedure TdxSpreadSheetCustomFiler.ExecuteSubTask(ASubTask: TdxSpreadSheetCustomFilerSubTask);
begin
  try
    ASubTask.Execute;
  finally
    ASubTask.Free;
  end;
end;

{ TdxSpreadSheetCustomFilerSubTask }

constructor TdxSpreadSheetCustomFilerSubTask.Create(AOwner: TdxSpreadSheetCustomFiler);
begin
  inherited Create;
  FOwner := AOwner;
end;

procedure TdxSpreadSheetCustomFilerSubTask.DoError(
  const AFormatString: string; const AArguments: array of const; AType: TdxSpreadSheetMessageType);
begin
  Owner.DoError(AFormatString, AArguments, AType);
end;

procedure TdxSpreadSheetCustomFilerSubTask.DoError(const AMessage: string; AType: TdxSpreadSheetMessageType);
begin
  Owner.DoError(AMessage, AType);
end;

procedure TdxSpreadSheetCustomFilerSubTask.ExecuteSubTask(ASubTask: TdxSpreadSheetCustomFilerSubTask);
begin
  Owner.ExecuteSubTask(ASubTask);
end;

function TdxSpreadSheetCustomFilerSubTask.GetSpreadSheet: TdxCustomSpreadSheet;
begin
  Result := Owner.SpreadSheet;
end;

{ TdxSpreadSheetCustomReader }

function TdxSpreadSheetCustomReader.AddBorders(ABordersHandle: TdxSpreadSheetBordersHandle): TdxSpreadSheetBordersHandle;
begin
  Result := CellStyles.Borders.AddBorders(ABordersHandle);
end;

function TdxSpreadSheetCustomReader.AddCellStyle(AStyleHandle: TdxSpreadSheetCellStyleHandle): TdxSpreadSheetCellStyleHandle;
begin
  Result := CellStyles.AddStyle(AStyleHandle);
end;

function TdxSpreadSheetCustomReader.AddBrush(ABrushHandle: TdxSpreadSheetBrushHandle): TdxSpreadSheetBrushHandle;
begin
  Result := CellStyles.Brushes.AddBrush(ABrushHandle);
end;

function TdxSpreadSheetCustomReader.AddFont(AFontHandle: TdxSpreadSheetFontHandle): TdxSpreadSheetFontHandle;
begin
  Result := CellStyles.Fonts.AddFont(AFontHandle);
end;

function TdxSpreadSheetCustomReader.AddFormattedSharedString(S: TdxSpreadSheetFormattedSharedString): TdxSpreadSheetSharedString;
begin
  Result := SpreadSheet.StringTable.Add(S);
end;

function TdxSpreadSheetCustomReader.AddImage(const AStream: TStream): TdxSpreadSheetSharedImageHandle;
begin
  Result := SpreadSheet.SharedImages.Add(AStream);
end;

function TdxSpreadSheetCustomReader.AddNumberFormat(
  const AFormatCode: TdxUnicodeString; Id: Integer = -1): TdxSpreadSheetFormatHandle;
begin
  if Id < 0 then
    Id := CellStyles.Formats.PredefinedFormats.GetIDByFormatCode(AFormatCode);
  Result := CellStyles.Formats.AddFormat(AFormatCode, Id);
end;

function TdxSpreadSheetCustomReader.AddSharedString(const S: TdxUnicodeString): TdxSpreadSheetSharedString;
begin
  Result := SpreadSheet.StringTable.Add(S);
end;

function TdxSpreadSheetCustomReader.AddTableView(const ACaption: TdxUnicodeString): TdxSpreadSheetTableView;
begin
  Result := TdxSpreadSheetTableView(SpreadSheet.AddSheet(ACaption, TdxSpreadSheetTableView));
end;

procedure TdxSpreadSheetCustomReader.Check(AValue: Boolean;
  const AMessage: string; AMessageType: TdxSpreadSheetMessageType = ssmtError);
begin
  if not AValue then
    DoError(AMessage, AMessageType);
end;

function TdxSpreadSheetCustomReader.CreateTempBordersHandle: TdxSpreadSheetBordersHandle;
begin
  Result := CellStyles.Borders.CreateBorders;
end;

function TdxSpreadSheetCustomReader.CreateTempCellStyle(
  AFont: TdxSpreadSheetFontHandle; AFormat: TdxSpreadSheetFormatHandle;
  AFill: TdxSpreadSheetBrushHandle; ABorders: TdxSpreadSheetBordersHandle): TdxSpreadSheetCellStyleHandle;
begin
  Result := CellStyles.CreateStyle(AFont, AFormat, AFill, ABorders);
end;

function TdxSpreadSheetCustomReader.CreateTempBrushHandle: TdxSpreadSheetBrushHandle;
begin
  Result := CellStyles.Brushes.CreateBrush;
end;

function TdxSpreadSheetCustomReader.CreateTempFontHandle: TdxSpreadSheetFontHandle;
begin
  Result := CellStyles.Fonts.CreateFont;
end;

function TdxSpreadSheetCustomReader.CreateTempFormattedSharedString(const S: TdxUnicodeString): TdxSpreadSheetFormattedSharedString;
begin
  Result := TdxSpreadSheetFormattedSharedString.CreateObject(S);
end;

function TdxSpreadSheetCustomReader.GetCellStyles: TdxSpreadSheetCellStyles;
begin
  Result := SpreadSheet.CellStyles;
end;

function TdxSpreadSheetCustomReader.GetStringTable: TdxSpreadSheetSharedStringTable;
begin
  Result := SpreadSheet.StringTable;
end;

{ TdxSpreadSheetCustomFormat }

class function TdxSpreadSheetCustomFormat.CanCheckByContent: Boolean;
begin
  Result := True;
end;

class function TdxSpreadSheetCustomFormat.CanReadFromStream(AStream: TStream): Boolean;
begin
  Result := False;
end;

class function TdxSpreadSheetCustomFormat.CreateFormatSettings: TdxSpreadSheetFormatSettings;
begin
  Result := nil;
end;

class function TdxSpreadSheetCustomFormat.GetDescription: string;
begin
  Result := '';
end;

class function TdxSpreadSheetCustomFormat.GetExt: string;
begin
  Result := '';
end;

class function TdxSpreadSheetCustomFormat.GetReader: TdxSpreadSheetCustomReaderClass;
begin
  Result := nil;
end;

class function TdxSpreadSheetCustomFormat.GetWriter: TdxSpreadSheetCustomWriterClass;
begin
  Result := nil;
end;

class procedure TdxSpreadSheetCustomFormat.Register;
begin
  dxSpreadSheetFormatsRepository.Register(Self);
end;

class procedure TdxSpreadSheetCustomFormat.Unregister;
begin
  if FSpreadSheetFormats <> nil then
    dxSpreadSheetFormatsRepository.Unregister(Self);
end;

{ TdxSpreadSheetFormatsRepository }

constructor TdxSpreadSheetFormatsRepository.Create;
begin
  inherited Create;
  FList := TList.Create;
end;

destructor TdxSpreadSheetFormatsRepository.Destroy;
begin
  FreeAndNil(FList);
  inherited Destroy;
end;

function TdxSpreadSheetFormatsRepository.Find(const AFileName: string; out AFormat: TdxSpreadSheetCustomFormatClass): Boolean;
var
  AExt: string;
  I: Integer;
begin
  Result := False;
  AExt := ExtractFileExt(AFileName);
  for I := 0 to Count - 1 do
    if SameText(AExt, Items[I].GetExt) then
    begin
      AFormat := Items[I];
      Result := True;
      Break;
    end;
end;

function TdxSpreadSheetFormatsRepository.GetOpenDialogFilter: string;
var
  AExts: string;
  AItem: TdxSpreadSheetCustomFormatClass;
  I: Integer;
begin
  Result := '';
  AExts := '';
  for I := 0 to Count - 1 do
  begin
    AItem := Items[I];
    if (AItem.GetReader <> nil) and (AItem.GetDescription <> '') then
    begin
      Result := Result + Format('%s (*%s)|*%s|', [AItem.GetDescription, AItem.GetExt, AItem.GetExt]);
      AExts := AExts + '*' + AItem.GetExt + ';';
    end;
  end;
  Result := cxGetResourceString(@sdxFileDialogAllSupported) + '|' + AExts + '|' + Result;
end;

function TdxSpreadSheetFormatsRepository.GetSaveDialogFilter: string;
var
  AItem: TdxSpreadSheetCustomFormatClass;
  I: Integer;
begin
  Result := '';
  for I := 0 to Count - 1 do
  begin
    AItem := Items[I];
    if (AItem.GetWriter <> nil) and (AItem.GetDescription <> '') then
      Result := Result + Format('%s (*%s)|*%s|', [AItem.GetDescription, AItem.GetExt, AItem.GetExt]);
  end;
end;

procedure TdxSpreadSheetFormatsRepository.Register(AFormat: TdxSpreadSheetCustomFormatClass);
begin
  FList.Add(AFormat);
end;

procedure TdxSpreadSheetFormatsRepository.Unregister(AFormat: TdxSpreadSheetCustomFormatClass);
begin
  FList.Remove(AFormat)
end;

function TdxSpreadSheetFormatsRepository.GetCount: Integer;
begin
  Result := FList.Count;
end;

function TdxSpreadSheetFormatsRepository.GetItem(Index: Integer): TdxSpreadSheetCustomFormatClass;
begin
  Result := TdxSpreadSheetCustomFormatClass(FList[Index]);
end;

{ TdxSpreadSheetCustomViewController }

procedure TdxSpreadSheetCustomViewController.CheckScrollArea(const P: TPoint);
begin
  // do nothing
end;

procedure TdxSpreadSheetCustomViewController.CheckScrollArea(X, Y: Integer);
begin
  CheckScrollArea(cxPoint(X, Y));
end;

function TdxSpreadSheetCustomViewController.ContainerCanDelete: Boolean;
begin
  Result := True;
end;

function TdxSpreadSheetCustomViewController.ContainerProcessKeyDown(Key: Word; Shift: TShiftState): Boolean;

  procedure MoveContainer(ADeltaX, ADeltaY: Integer);
  begin
    if FocusedContainer.CanMove then
      FocusedContainer.CheckChangeObject(procedure
      begin
        FocusedContainer.Calculator.ModifyBounds(ADeltaX, ADeltaY, ADeltaX, ADeltaY)
      end);
  end;

  procedure ResizeContainer(ADeltaX, ADeltaY: Integer);
  begin
    if FocusedContainer.CanResize then
      FocusedContainer.CheckChangeObject(procedure
      begin
        FocusedContainer.Calculator.ResizeContainer(ADeltaX, ADeltaY)
      end);
  end;

const
  RotationDelta: array[Boolean] of Integer = (15, 1);
var
  AContainer: TdxSpreadSheetContainer;
begin
  Result := True;
  case Key of
    VK_ESCAPE:
      FocusedContainer := nil;

    VK_DELETE:
      if ContainerCanDelete then
        if ssShift in Shift then
          (View as TdxSpreadSheetTableView).CutToClipboard
        else
          FocusedContainer.Free;

    VK_TAB:
      begin
        if ssShift in Shift then
          AContainer := Container.GetPrevVisibleContainer(FocusedContainer)
        else
          AContainer := Container.GetNextVisibleContainer(FocusedContainer);

        if AContainer = nil then
        begin
          if ssShift in Shift then
            AContainer := Container.GetLastVisibleContainer
          else
            AContainer := Container.GetFirstVisibleContainer;
        end;

        FocusedContainer := AContainer;
      end;

    VK_LEFT, VK_RIGHT:
      if [ssAlt, ssShift] * Shift = [ssAlt] then
      begin
        if FocusedContainer.CanRotate then
        begin
          FocusedContainer.CheckChangeObject(procedure
          begin
            FocusedContainer.Transform.RotationAngle := FocusedContainer.Transform.RotationAngle +
              ValueIncr[Key = VK_RIGHT] * RotationDelta[ssCtrl in Shift];
          end);
        end
      end
      else
        if [ssAlt, ssShift] * Shift = [ssShift] then
          ResizeContainer(ValueIncr[Key = VK_RIGHT], 0)
        else
          if [ssAlt, ssShift] * Shift = [] then
            MoveContainer(ValueIncr[Key = VK_RIGHT], 0);

    VK_UP, VK_DOWN:
      if [ssAlt, ssShift] * Shift = [ssShift] then
        ResizeContainer(0, ValueIncr[Key = VK_DOWN])
      else
        if [ssAlt, ssShift] * Shift = [] then
          MoveContainer(0, ValueIncr[Key = VK_DOWN]);
  else
    Result := False;
  end;
end;

function TdxSpreadSheetCustomViewController.GetContainer: TdxSpreadSheetContainers;
begin
  Result := View.Containers;
end;

function TdxSpreadSheetCustomViewController.GetHitTest: TdxSpreadSheetCustomHitTest;
begin
  Result := View.HitTest;
end;

function TdxSpreadSheetCustomViewController.GetSpreadSheet: TdxCustomSpreadSheet;
begin
  Result := View.SpreadSheet;
end;

function TdxSpreadSheetCustomViewController.GetView: TdxSpreadSheetCustomView;
begin
  Result := TdxSpreadSheetCustomView(Owner);
end;

procedure TdxSpreadSheetCustomViewController.GetRedoActionCount(var ARedoCount: Integer);
begin
end;

procedure TdxSpreadSheetCustomViewController.GetUndoActionCount(var AUndoCount: Integer);
begin
end;

function TdxSpreadSheetCustomViewController.GetZoomFactor: Integer;
begin
  Result := View.ZoomFactor;
end;

function TdxSpreadSheetCustomViewController.Redo(const ARedoCount: Integer = 1): Boolean;
begin
  Result := False;
end;

function TdxSpreadSheetCustomViewController.Undo(const AUndoCount: Integer = 1): Boolean;
begin
  Result := False;
end;

procedure TdxSpreadSheetCustomViewController.SetFocusedContainer(AValue: TdxSpreadSheetContainer);
begin
  if (AValue <> nil) and (AValue.Parent <> View) then
    Exit;
  if FFocusedContainer <> AValue then
  begin
    FFocusedContainer := AValue;
    if not View.IsLocked then
      View.SelectionChanged;
  end;
end;

{ TdxSpreadSheetCustomHitTest }

constructor TdxSpreadSheetCustomHitTest.Create(AOwner: TObject);
begin
  FOwner := AOwner;
end;

procedure TdxSpreadSheetCustomHitTest.Calculate(const AHitPoint: TPoint);
begin
  Clear;
  FHitPoint := AHitPoint;
  FActualHitPoint := GetActualHitPoint;
end;

procedure TdxSpreadSheetCustomHitTest.Clear;
begin
  FHitCode := 0;
  FHitObjectData := 0;
  FHitObject := nil;
end;

function TdxSpreadSheetCustomHitTest.GetPopupMenuClass(const P: TPoint): TComponentClass;
begin
  Calculate(P);
  if HitObject <> nil then
    Result := HitObject.GetPopupMenuClass(Self)
  else
    Result := nil;
end;

procedure TdxSpreadSheetCustomHitTest.Recalculate;
begin
  Calculate(HitPoint);
end;

function TdxSpreadSheetCustomHitTest.GetActualHitPoint: TPoint;
begin
  Result := FHitPoint;
end;

function TdxSpreadSheetCustomHitTest.GetCursor(const P: TPoint): TCursor;
begin
  Calculate(P);
  if HitObject <> nil then
    Result := HitObject.GetCursor(Self)
  else
    Result := crDefault;
end;

function TdxSpreadSheetCustomHitTest.CanDrag(const P: TPoint): Boolean;
begin
  Result := GetDragAndDropObjectClass(P) <> nil;
end;

function TdxSpreadSheetCustomHitTest.GetDragAndDropObjectClass: TcxDragAndDropObjectClass;
begin
  if HitObject <> nil then
    Result := HitObject.GetDragAndDropObjectClass(Self)
  else
    Result := nil;
end;

function TdxSpreadSheetCustomHitTest.GetDragAndDropObjectClass(const P: TPoint): TcxDragAndDropObjectClass;
begin
  Calculate(P);
  Result := GetDragAndDropObjectClass;
end;

function TdxSpreadSheetCustomHitTest.GetHitCode(ACode: Integer): Boolean;
begin
  Result := FHitCode and ACode <> 0
end;

procedure TdxSpreadSheetCustomHitTest.SetHitCode(ACode: Integer; AValue: Boolean);
begin
  if AValue then
    FHitCode := FHitCode or ACode
  else
    FHitCode := FHitCode and not ACode;
end;

{ TdxSpreadSheetCustomDragAndDropObject }

function TdxSpreadSheetCustomDragAndDropObject.GetZoomFactor: Integer;
begin
  Result := View.ZoomFactor;
end;

function TdxSpreadSheetCustomDragAndDropObject.TranslateCoords(const P: TPoint): TPoint;
begin
  Result := cxPointOffset(inherited TranslateCoords(P), View.GetPartOffsetByPoint(P));
end;

function TdxSpreadSheetCustomDragAndDropObject.GetControl: TdxCustomSpreadSheet;
begin
  Result := TdxCustomSpreadSheet(inherited Control);
end;

function TdxSpreadSheetCustomDragAndDropObject.GetHitTest: TdxSpreadSheetCustomHitTest;
begin
  Result := Control.ActiveController.HitTest;
end;

function TdxSpreadSheetCustomDragAndDropObject.GetView: TdxSpreadSheetCustomView;
begin
  Result := Control.ActiveSheet;
end;

{ TdxSpreadSheetCustomCellViewInfo }

constructor TdxSpreadSheetCustomCellViewInfo.Create(AOwner: TObject);
begin
  inherited Create;
  FOwner := AOwner;
end;

procedure TdxSpreadSheetCustomCellViewInfo.Draw(ACanvas: TcxCanvas; AStage: TdxSpreadSheetDrawingStage);
begin
  if CanDraw(ACanvas, AStage) then
  begin
    FDrawingStage := AStage;
    DoDraw(ACanvas);
  end;
end;

procedure TdxSpreadSheetCustomCellViewInfo.Draw(ACanvas: TcxCanvas; AStage: TdxSpreadSheetDrawingStage; const AOffset: TPoint);
begin
  OffsetOrigin(AOffset);
  try
    Draw(ACanvas, AStage);
  finally
    OffsetOrigin(cxPointInvert(AOffset));
  end;
end;

procedure TdxSpreadSheetCustomCellViewInfo.OffsetOrigin(const AOffset: TPoint);
begin
  // do nothing
end;

function TdxSpreadSheetCustomCellViewInfo.CanDraw(ACanvas: TcxCanvas; AStage: TdxSpreadSheetDrawingStage): Boolean;
begin
  Result := AStage = dsFirst;
end;

procedure TdxSpreadSheetCustomCellViewInfo.DoDraw(ACanvas: TcxCanvas);
begin
  // do nothing
end;

function TdxSpreadSheetCustomCellViewInfo.GetCursor(AHitTest: TdxSpreadSheetCustomHitTest): TCursor;
begin
  Result := crDefault;
end;

function TdxSpreadSheetCustomCellViewInfo.GetDragAndDropObjectClass(AHitTest: TdxSpreadSheetCustomHitTest): TcxDragAndDropObjectClass;
begin
  Result := nil;
end;

function TdxSpreadSheetCustomCellViewInfo.GetPopupMenuClass(AHitTest: TdxSpreadSheetCustomHitTest): TComponentClass;
begin
  Result := nil;
end;

function TdxSpreadSheetCustomCellViewInfo.InitHitTest(AHitTest: TdxSpreadSheetCustomHitTest): Boolean;
begin
  Result := False;
end;

function TdxSpreadSheetCustomCellViewInfo.InitHitTest(AHitTest: TdxSpreadSheetCustomHitTest; const AOffset: TPoint): Boolean;
begin
  OffsetOrigin(AOffset);
  try
    Result := InitHitTest(AHitTest);
  finally
    OffsetOrigin(cxPointInvert(AOffset));
  end;
end;

function TdxSpreadSheetCustomCellViewInfo.GetLookAndFeelPainter: TcxCustomLookAndFeelPainter;
begin
  Result := SpreadSheet.LookAndFeelPainter;
end;

{ TdxSpreadSheetCellViewInfo }

destructor TdxSpreadSheetCellViewInfo.Destroy;
begin
  EndMouseTracking(Self);
  inherited Destroy;
end;

procedure TdxSpreadSheetCellViewInfo.Calculate;
begin
  if not Calculated then
    DoCalculate;
end;

procedure TdxSpreadSheetCellViewInfo.Draw(ACanvas: TcxCanvas; AStage: TdxSpreadSheetDrawingStage);
begin
  if CanDraw(ACanvas, AStage) then
  begin
    if not DoCustomDraw(ACanvas) then
    begin
      FDrawingStage := AStage;
      DoDraw(ACanvas);
    end;
    if ExcludeFromClipRgn(AStage) then
      ACanvas.ExcludeClipRect(DisplayClipRect);
  end;
end;

procedure TdxSpreadSheetCellViewInfo.Invalidate;
begin
  Calculate;
  if Visible then
    InvalidateRect(DisplayBounds);
end;

procedure TdxSpreadSheetCellViewInfo.OffsetOrigin(const AOffset: TPoint);
begin
  AbsoluteOrigin := cxPointOffset(AbsoluteOrigin, AOffset);
end;

procedure TdxSpreadSheetCellViewInfo.SetBounds(const AAbsoluteBounds, AScreenClipRect: TRect);
begin
  SetBounds(cxNullPoint, AAbsoluteBounds, AScreenClipRect);
end;

procedure TdxSpreadSheetCellViewInfo.SetBounds(const AAbsoluteOrigin: TPoint;
  const AAbsoluteBounds: TRect; const AScreenClipRect: TRect);
begin
  FAbsoluteOrigin := AAbsoluteOrigin;
  FAbsoluteBounds := AAbsoluteBounds;
  FScreenClipRect := AScreenClipRect;
  Calculated := False;
end;

procedure TdxSpreadSheetCellViewInfo.CalculateDisplayBounds;
begin
  FDisplayBounds := cxRectOffset(AbsoluteBounds, AbsoluteOrigin, False);
  FVisible := cxRectIntersect(FDisplayClipRect, FDisplayBounds, FScreenClipRect);
end;

function TdxSpreadSheetCellViewInfo.CanDraw(ACanvas: TcxCanvas; AStage: TdxSpreadSheetDrawingStage): Boolean;
begin
  Result := Visible and ACanvas.RectVisible(DisplayBounds) and (AStage = dsFirst);
end;

procedure TdxSpreadSheetCellViewInfo.DoCalculate;
begin
  CalculateDisplayBounds;
  FCalculated := True;
end;

function TdxSpreadSheetCellViewInfo.DoCustomDraw(ACanvas: TcxCanvas): Boolean;
begin
  Result := False;
end;

function TdxSpreadSheetCellViewInfo.ExcludeFromClipRgn(AStage: TdxSpreadSheetDrawingStage): Boolean;
begin
  Result := False;
end;

function TdxSpreadSheetCellViewInfo.GetZoomFactor: Integer;
begin
  Result := 100;
end;

procedure TdxSpreadSheetCellViewInfo.SetAbsoluteOrigin(const AValue: TPoint);
begin
  if not cxPointIsEqual(AValue, FAbsoluteOrigin) then
  begin
    FAbsoluteOrigin := AValue;
    if Calculated then
      CalculateDisplayBounds
    else
      Calculate;
  end;
end;

function TdxSpreadSheetCellViewInfo.GetDisplayBounds: TRect;
begin
  Calculate;
  Result := FDisplayBounds;
end;

function TdxSpreadSheetCellViewInfo.InitHitTest(AHitTest: TdxSpreadSheetCustomHitTest): Boolean;
begin
  Result := PtInRect(DisplayClipRect, AHitTest.ActualHitPoint);
  if Result then
    AHitTest.HitObject := Self;
end;

procedure TdxSpreadSheetCellViewInfo.InvalidateRect(const R: TRect);
begin
  SpreadSheet.InvalidateRect(cxRectScale(R, ZoomFactor, 100), False);
end;

function TdxSpreadSheetCellViewInfo.IsPermanent: Boolean;
begin
  Result := False;
end;

procedure TdxSpreadSheetCellViewInfo.MouseLeave;
begin
  // do nothing
end;

{ TdxSpreadSheetViewInfoCellsList }

function TdxSpreadSheetViewInfoCellsList.CalculateHitTest(
  AHitTest: TdxSpreadSheetCustomHitTest; AReverse: Boolean = False): Boolean;
begin
  Result := CalculateHitTest(AHitTest, cxNullPoint, AReverse);
end;

function TdxSpreadSheetViewInfoCellsList.CalculateHitTest(
  AHitTest: TdxSpreadSheetCustomHitTest; const AOffset: TPoint; AReverse: Boolean = False): Boolean;
var
  I: Integer;
begin
  Result := False;
  if AReverse then
    for I := Count - 1 downto 0 do
    begin
      Result := Items[I].InitHitTest(AHitTest, AOffset);
      if Result then
        Break;
    end
  else
    for I := 0 to Count - 1 do
    begin
      Result := Items[I].InitHitTest(AHitTest, AOffset);
      if Result then
        Break;
    end;
end;

procedure TdxSpreadSheetViewInfoCellsList.Clear;
var
  AItem: TdxSpreadSheetCellViewInfo;
  I: Integer;
begin
  OwnsObjects := False;
  try
    for I := Count - 1 downto 0 do
    begin
      AItem := Items[I];
      if not AItem.IsPermanent then
      begin
        Delete(I);
        AItem.Free;
      end;
    end;
  finally
    OwnsObjects := True;
  end;
end;

procedure TdxSpreadSheetViewInfoCellsList.Draw(ACanvas: TcxCanvas);
var
  AStage: TdxSpreadSheetDrawingStage;
  I: Integer;
begin
  for AStage := dsFirst to dsSecond do
  begin
    for I := 0 to Count - 1 do
      Items[I].Draw(ACanvas, AStage);
  end;
end;

procedure TdxSpreadSheetViewInfoCellsList.DrawWithOriginOffset(ACanvas: TcxCanvas; const AOffset: TPoint);
begin
  OffsetOrigin(AOffset);
  try
    Draw(ACanvas);
  finally
    OffsetOrigin(cxPointInvert(AOffset));
  end;
end;

procedure TdxSpreadSheetViewInfoCellsList.ForEach(AProc: TdxSpreadSheetForEachViewInfoCellProc; AData: TObject);
var
  I: Integer;
begin
  for I := 0 to Count - 1 do
  begin
    if not AProc(Items[I], AData) then
      Break;
  end;
end;

function TdxSpreadSheetViewInfoCellsList.FindItem(AItemOwner: TObject): Integer;
var
  I: Integer;
begin
  for I := 0 to Count - 1 do
  begin
    if Items[I].Owner = AItemOwner then
      Exit(I);
  end;
  Result := -1;
end;

procedure TdxSpreadSheetViewInfoCellsList.OffsetOrigin(const AOffset: TPoint);
var
  I: Integer;
begin
  for I := 0 to Count - 1 do
    Items[I].OffsetOrigin(AOffset);
end;

function TdxSpreadSheetViewInfoCellsList.GetItem(AIndex: Integer): TdxSpreadSheetCellViewInfo;
begin
  Result := TdxSpreadSheetCellViewInfo(inherited Items[AIndex]);
  Result.Calculate;
end;

{ TdxSpreadSheetTableViewInfoCellsList }

function TdxSpreadSheetTableViewInfoCellsList.FindItemForCell(
  ACell: TdxSpreadSheetCell; out AViewInfo: TdxSpreadSheetTableViewCellViewInfo): Boolean;
var
  I: Integer;
begin
  Result := False;
  if ACell <> nil then
    for I := 0 to Count - 1 do
    begin
      AViewInfo := TdxSpreadSheetTableViewCellViewInfo(List[I]);
      if AViewInfo.Cell = ACell then
        Exit(True);
    end;
end;

function TdxSpreadSheetTableViewInfoCellsList.GetItem(Index: Integer): TdxSpreadSheetTableViewCellViewInfo;
begin
  Result := TdxSpreadSheetTableViewCellViewInfo(inherited Items[Index]);
end;

{ TdxSpreadSheetTableViewReferencesHighlighter }

constructor TdxSpreadSheetTableViewReferencesHighlighter.Create(AOwner: TObject);
begin
  inherited Create(AOwner);
  FColors := TList<TColor>.Create;
  FReferences := TObjectList<TdxSpreadSheetTableViewReferencesHighlighterItem>.Create;
end;

destructor TdxSpreadSheetTableViewReferencesHighlighter.Destroy;
begin
  FreeAndNil(FReferences);
  FreeAndNil(FColors);
  inherited Destroy;
end;

procedure TdxSpreadSheetTableViewReferencesHighlighter.Calculate(const AEdit: TcxCustomEdit);
begin
  Calculate(AEdit.EditingValue);
  if AEdit is TcxCustomRichEdit then
    HighlightReferences(TcxCustomRichEdit(AEdit));
end;

procedure TdxSpreadSheetTableViewReferencesHighlighter.Calculate(const AFormulaText: TdxUnicodeString);
var
  AFormattedText: TdxSpreadSheetFormulaFormattedText;
  AHighlightedReferenceIndex: Integer;
  AIndex: Integer;
  AItem: TdxSpreadSheetTableViewReferencesHighlighterItem;
  AReferences: TList<TRect>;
  I: Integer;
begin
  References.Clear;
  if Cell <> nil then
  begin
    FOrigin := cxPointInvert(View.GetContentOrigin);
    AReferences := TList<TRect>.Create;
    try
      if TdxSpreadSheetFormulaReferencesHelper.GetReferences(Cell, AFormulaText, AReferences, AFormattedText) then
      try
        for I := 0 to AReferences.Count - 1 do
          AReferences[I] := cxRectAdjust(AReferences[I]);

        for I := 0 to AReferences.Count - 1 do
          if dxSpreadSheetIsValidArea(AReferences[I]) then
          begin
            AItem := TdxSpreadSheetTableViewReferencesHighlighterItem.Create;
            AItem.CellsArea := AReferences[I];
            AItem.Bounds := GetAreaBounds(AItem.CellsArea);
            References.Add(AItem);
          end;

        AIndex := 0;
        AHighlightedReferenceIndex := 0;
        for I := 0 to AFormattedText.Runs.Count - 2 do
          if AFormattedText.Runs[I].Tag <> 0 then
          begin
            if dxSpreadSheetIsValidArea(AReferences[AHighlightedReferenceIndex]) then
            begin
              References[AIndex].PositionInText := AFormattedText.Runs[I].StartIndex;
              References[AIndex].TextLength := AFormattedText.Runs[I + 1].StartIndex - AFormattedText.Runs[I].StartIndex;
              Inc(AIndex);
            end;
            Inc(AHighlightedReferenceIndex);
          end;
      finally
        AFormattedText.Free;
      end;
    finally
      AReferences.Free;
    end;
  end;
end;

procedure TdxSpreadSheetTableViewReferencesHighlighter.CalculateColors;
var
  AValue: TdxHSL;
  I: Integer;
begin
  FColors.Clear;
  AValue := TdxColorSpaceConverter.ColorToHSL(View.SpreadSheet.LookAndFeelPainter.SpreadSheetSelectionColor);
  AValue.L := Min(Max(AValue.L, 0.4), 0.8);
  AValue.S := Max(AValue.S, 0.4);

  FColors.Capacity := Length(dxSpreadSheetSelectionColors);
  for I := 0 to Length(dxSpreadSheetSelectionColors) - 1 do
  begin
    AValue.H := TdxColorSpaceConverter.ColorToHSL(dxSpreadSheetSelectionColors[I]).H;
    FColors.Add(TdxColorSpaceConverter.HSLToColor(AValue));
  end;
end;

procedure TdxSpreadSheetTableViewReferencesHighlighter.HighlightReferences(AEdit: TcxCustomRichEdit);

  procedure ApplyColor(const AItem: TdxSpreadSheetTableViewReferencesHighlighterItem; AColor: TColor);
  begin
    AEdit.SelStart := AItem.PositionInText - 1;
    AEdit.SelLength := AItem.TextLength;
    AEdit.SelAttributes.Color := AColor;
  end;

var
  ASavedSelLength: Integer;
  ASavedSelStart: Integer;
  I: Integer;
begin
  SendMessage(AEdit.Handle, WM_SETREDRAW, 0, 0);
  try
    AEdit.LockChangeEvents(True, False);
    ASavedSelStart := AEdit.SelStart;
    ASavedSelLength := AEdit.SelLength;
    try
      AEdit.SelectAll;
      AEdit.SelAttributes.Color := TcxCustomRichEditAccess(AEdit).ActiveStyle.TextColor;
      for I := 0 to References.Count - 1 do
        ApplyColor(References[I], Colors[I])
    finally
      AEdit.SelStart := ASavedSelStart;
      AEdit.SelLength := ASavedSelLength;
      AEdit.LockChangeEvents(False, False);
    end;
  finally
    SendMessage(AEdit.Handle, WM_SETREDRAW, 1, 1);
    AEdit.InvalidateWithChildren;
  end;
end;

procedure TdxSpreadSheetTableViewReferencesHighlighter.OffsetOrigin(const AOffset: TPoint);
begin
  FOrigin := cxPointOffset(FOrigin, AOffset, False);
end;

function TdxSpreadSheetTableViewReferencesHighlighter.CanDraw(ACanvas: TcxCanvas; AStage: TdxSpreadSheetDrawingStage): Boolean;
begin
  Result := (AStage = dsSecond) and (References.Count > 0);
end;

procedure TdxSpreadSheetTableViewReferencesHighlighter.DoDraw(ACanvas: TcxCanvas);
var
  AOrigin: TPoint;
  I: Integer;
begin
  AOrigin := cxPointScale(Origin, View.ZoomFactor, 100);
  MoveWindowOrg(ACanvas.Handle, AOrigin.X, AOrigin.Y);
  ACanvas.SaveClipRegion;
  try
    for I := 0 to References.Count - 1 do
      TdxSpreadSheetSelectionPainter.Draw(ACanvas, References[I].Bounds, Colors[I], [ssseCorners], [coTopLeft..coBottomRight]);
    for I := 0 to References.Count - 1 do
      TdxSpreadSheetSelectionPainter.Draw(ACanvas, References[I].Bounds, Colors[I], [ssseFrame, ssseBackground], []);
  finally
    ACanvas.RestoreClipRegion;
    MoveWindowOrg(ACanvas.Handle, -AOrigin.X, -AOrigin.Y);
  end;
end;

function TdxSpreadSheetTableViewReferencesHighlighter.GetAreaBounds(const AArea: TRect): TRect;
begin
  Result := cxRectBounds(View.Columns.GetDistance(0, AArea.Left - 1), View.Rows.GetDistance(0, AArea.Top - 1),
    View.Columns.GetDistance(AArea.Left, AArea.Right), View.Rows.GetDistance(AArea.Top, AArea.Bottom));
end;

function TdxSpreadSheetTableViewReferencesHighlighter.GetCursor(AHitTest: TdxSpreadSheetCustomHitTest): TCursor;
begin
  case HiWord(AHitTest.HitObjectData) of
    Ord(coTopLeft), Ord(coBottomRight):
      Result := crSizeNWSE;      
    Ord(coTopRight), Ord(coBottomLeft):
      Result := crSizeNESW;
  else
    Result := crSizeAll;
  end;
end;

function TdxSpreadSheetTableViewReferencesHighlighter.GetDragAndDropObjectClass(
  AHitTest: TdxSpreadSheetCustomHitTest): TcxDragAndDropObjectClass;
begin
  case HiWord(AHitTest.HitObjectData) of
    Ord(coTopLeft), Ord(coBottomRight), Ord(coTopRight), Ord(coBottomLeft):
      Result := TdxSpreadSheetTableViewReferencesHighlighterResizeDragAndDropObject;
  else
    Result := TdxSpreadSheetTableViewReferencesHighlighterMoveDragAndDropObject;
  end;
end;

function TdxSpreadSheetTableViewReferencesHighlighter.GetSpreadSheet: TdxCustomSpreadSheet;
begin
  Result := View.SpreadSheet;
end;

function TdxSpreadSheetTableViewReferencesHighlighter.InitHitTest(AHitTest: TdxSpreadSheetCustomHitTest): Boolean;

  procedure SetupHitTest(AData: Integer);
  begin  
    AHitTest.HitObject := Self;
    AHitTest.HitObjectData := AData;
    AHitTest.SetHitCode(hcResizeArea, True);
  end;

var
  ACorner: TdxCorner;
  ACornerBounds: TRect;
  ARect: TRect;
  I: Integer;
begin
  Result := False;
  for I := 0 to References.Count - 1 do
  begin
    ARect := cxRectOffset(References[I].Bounds, Origin);

    for ACorner := Low(TdxCorner) to High(TdxCorner) do
    begin
      ACornerBounds := TdxSpreadSheetSelectionPainter.GetCornerBounds(ARect, ACorner);
      ACornerBounds := cxRectCenter(ACornerBounds, CornetHitTestZoneSize, CornetHitTestZoneSize);
      if PtInRect(ACornerBounds, AHitTest.ActualHitPoint) then
      begin
        SetupHitTest(MakeLong(I, Ord(ACorner)));
        Exit(True);
      end;
    end;
    
    if PtInRect(cxRectInflate(ARect, FrameHitTestZoneSize div 2), AHitTest.ActualHitPoint) and not 
      PtInRect(cxRectInflate(ARect, -FrameHitTestZoneSize div 2), AHitTest.ActualHitPoint) then
    begin
      SetupHitTest(MakeLong(I, MaxWord));
      Exit(True);
    end;     
  end;
end;

function TdxSpreadSheetTableViewReferencesHighlighter.GetColor(Index: Integer): TColor;
begin
  Result := FColors[Index mod FColors.Count];
end;

function TdxSpreadSheetTableViewReferencesHighlighter.GetView: TdxSpreadSheetTableView;
begin
  Result := TdxSpreadSheetTableView(Owner);
end;

{ TdxSpreadSheetTableViewReferencesHighlighterCustomDragAndDropObject }

procedure TdxSpreadSheetTableViewReferencesHighlighterCustomDragAndDropObject.BeforeBeginDragAndDrop;
begin
  inherited BeforeBeginDragAndDrop;
  FContentRect := cxNullRect;
  FContentRect.BottomRight := GetAbsoluteCellBounds(dxSpreadSheetMaxRowIndex, dxSpreadSheetMaxColumnIndex).BottomRight;
  FContentOrigin := View.GetContentOrigin;
  FCursor := HitTest.HitObject.GetCursor(HitTest);
  FReferenceIndex := LoWord(HitTest.HitObjectData);
  FReference := Highlighter.References[ReferenceIndex];
  FPrevCellArea := Reference.CellsArea;
  FBounds := Reference.Bounds;
end;

procedure TdxSpreadSheetTableViewReferencesHighlighterCustomDragAndDropObject.BeginDragAndDrop;
begin
  inherited BeginDragAndDrop;
  FCapturePoint := CurMousePos;
end;

function TdxSpreadSheetTableViewReferencesHighlighterCustomDragAndDropObject.CheckForContentRect(const P: TPoint): TPoint;
begin
  Result.X := Max(ContentRect.Left, Min(ContentRect.Right, P.X));
  Result.Y := Max(ContentRect.Top, Min(ContentRect.Bottom, P.Y));
end;

procedure TdxSpreadSheetTableViewReferencesHighlighterCustomDragAndDropObject.CheckRowAndColumnIndexes(
  var ARowIndex, AColumnIndex: Integer);
begin
  AColumnIndex := Max(Min(AColumnIndex, dxSpreadSheetMaxColumnIndex), 0);
  ARowIndex := Max(Min(ARowIndex, dxSpreadSheetMaxRowIndex), 0);
end;

procedure TdxSpreadSheetTableViewReferencesHighlighterCustomDragAndDropObject.DragAndDrop(const P: TPoint; var Accepted: Boolean);
begin
  inherited DragAndDrop(P, Accepted);
  View.Controller.CheckScrollArea(cxPointScale(GetClientCursorPos, 100, ZoomFactor));
end;

procedure TdxSpreadSheetTableViewReferencesHighlighterCustomDragAndDropObject.EndDragAndDrop(Accepted: Boolean);
begin
  if not Accepted then
    UpdateReference(FPrevCellArea);
  inherited EndDragAndDrop(Accepted);
end;

function TdxSpreadSheetTableViewReferencesHighlighterCustomDragAndDropObject.GetAbsoluteCellBounds(
  const ARowIndex, AColumnIndex: Integer): TRect;
begin
  Result := View.GetAbsoluteCellBounds(ARowIndex, AColumnIndex, False);
end;

procedure TdxSpreadSheetTableViewReferencesHighlighterCustomDragAndDropObject.GetCellAtAbsolutePoint(
  const P: TPoint; out ARowIndex, AColumnIndex: Integer);
begin
  View.GetCellAtAbsolutePoint(P, ARowIndex, AColumnIndex);
end;

function TdxSpreadSheetTableViewReferencesHighlighterCustomDragAndDropObject.GetDragAndDropCursor(Accepted: Boolean): TCursor;
begin
  if Accepted then
    Result := FCursor
  else
    Result := inherited GetDragAndDropCursor(Accepted);
end;

function TdxSpreadSheetTableViewReferencesHighlighterCustomDragAndDropObject.GetImmediateStart: Boolean;
begin
  Result := True;
end;

function TdxSpreadSheetTableViewReferencesHighlighterCustomDragAndDropObject.TranslateCoords(const P: TPoint): TPoint;
begin
  Result := cxPointOffset(inherited TranslateCoords(P), ContentOrigin);
end;

procedure TdxSpreadSheetTableViewReferencesHighlighterCustomDragAndDropObject.UpdateReference(const AReference: TRect);
var
  AFormulaText: TdxUnicodeString;
  AIndexDelta: Integer;
  AReferenceAsString: TdxUnicodeString;
  I: Integer;
begin
  if not cxRectIsEqual(AReference, Reference.CellsArea) then
  begin
    AReferenceAsString := dxReferenceToString(AReference,
      Control.OptionsView.R1C1Reference, Highlighter.Cell.RowIndex, Highlighter.Cell.ColumnIndex);

    AIndexDelta := Length(AReferenceAsString) - Reference.TextLength;
    AFormulaText := Edit.EditingValue;
    AFormulaText := Copy(AFormulaText, 1, Reference.PositionInText - 1) + AReferenceAsString +
      Copy(AFormulaText, Reference.PositionInText + Reference.TextLength, MaxInt);
    Reference.CellsArea := AReference;
    Reference.Bounds := Highlighter.GetAreaBounds(cxRectAdjust(AReference));
    Reference.TextLength := Length(AReferenceAsString);

    if AIndexDelta <> 0 then
    begin
      for I := ReferenceIndex + 1 to Highlighter.References.Count - 1 do
        Inc(Highlighter.References[I].PositionInText, AIndexDelta);
    end;

    Edit.LockChangeEvents(True, False);
    try
      Edit.EditValue := AFormulaText;
      if Edit is TcxCustomRichEdit then
        Highlighter.HighlightReferences(TcxCustomRichEdit(Edit));
    finally
      Edit.LockChangeEvents(False, False);
      View.EditingController.UpdateEditPosition;
    end;

    View.Invalidate;
  end;
end;

function TdxSpreadSheetTableViewReferencesHighlighterCustomDragAndDropObject.GetEdit: TcxCustomEdit;
begin
  Result := View.EditingController.Edit;
end;

function TdxSpreadSheetTableViewReferencesHighlighterCustomDragAndDropObject.GetHighlighter: TdxSpreadSheetTableViewReferencesHighlighter;
begin
  Result := Control.ActiveSheetAsTable.ViewInfo.ReferencesHighlighter;
end;

function TdxSpreadSheetTableViewReferencesHighlighterCustomDragAndDropObject.GetView: TdxSpreadSheetTableView;
begin
  Result := Highlighter.View;
end;

{ TdxSpreadSheetTableViewReferencesHighlighterMoveDragAndDropObject }

procedure TdxSpreadSheetTableViewReferencesHighlighterMoveDragAndDropObject.BeginDragAndDrop;
var
  AColumnIndex: Integer;
  ARowIndex: Integer;
begin
  inherited BeginDragAndDrop;
  GetCellAtAbsolutePoint(CapturePoint, ARowIndex, AColumnIndex);

  ARowIndex := Max(Reference.CellsArea.Top, Min(Reference.CellsArea.Bottom, ARowIndex));
  AColumnIndex := Max(Reference.CellsArea.Left, Min(Reference.CellsArea.Right, AColumnIndex));

  FColumnDelta := Reference.CellsArea.Left - AColumnIndex;
  FRowDelta := Reference.CellsArea.Top - ARowIndex;
end;

procedure TdxSpreadSheetTableViewReferencesHighlighterMoveDragAndDropObject.DragAndDrop(const P: TPoint; var Accepted: Boolean);
var
  AColumnIndex: Integer;
  ARowIndex: Integer;
begin
  Accepted := True;
  GetCellAtAbsolutePoint(P, ARowIndex, AColumnIndex);
  Inc(ARowIndex, FRowDelta);
  Inc(AColumnIndex, FColumnDelta);
  CheckRowAndColumnIndexes(ARowIndex, AColumnIndex);
  UpdateReference(cxRectSetOrigin(Reference.CellsArea, Point(AColumnIndex, ARowIndex)));
  inherited DragAndDrop(P, Accepted);
end;

{ TdxSpreadSheetTableViewReferencesHighlighterResizeDragAndDropObject }

procedure TdxSpreadSheetTableViewReferencesHighlighterResizeDragAndDropObject.AdjustRowAndColumnIndexes(
  const P: TPoint; var ARowIndex, AColumnIndex: Integer);

  procedure AdjustIndex(var AIndex: Integer; X, ACenterX: Integer; AIsForwardCorner: Boolean);
  begin
    if AIsForwardCorner then
    begin
      if X < ACenterX then
        Dec(AIndex);
    end
    else
      if X > ACenterX then
        Inc(AIndex);
  end;

var
  ACellCenter: TPoint;
begin
  ACellCenter := cxRectCenter(GetAbsoluteCellBounds(ARowIndex, AColumnIndex));
  AdjustIndex(AColumnIndex, P.X, ACellCenter.X, (Corner in [coTopRight, coBottomRight]) = (Reference.CellsArea.Width >= 0));
  AdjustIndex(ARowIndex, P.Y, ACellCenter.Y, (Corner in [coBottomLeft, coBottomRight]) = (Reference.CellsArea.Height >= 0));
end;

procedure TdxSpreadSheetTableViewReferencesHighlighterResizeDragAndDropObject.BeforeBeginDragAndDrop;
begin
  inherited BeforeBeginDragAndDrop;
  FCorner := TdxCorner(HiWord(HitTest.HitObjectData));
end;

function TdxSpreadSheetTableViewReferencesHighlighterResizeDragAndDropObject.CalculateReferece(ARowIndex, AColumnIndex: Integer): TRect;
begin
  Result := Reference.CellsArea;
  if Corner in [coTopLeft, coTopRight] then
    Result.Top := ARowIndex;
  if Corner in [coTopLeft, coBottomLeft] then
    Result.Left := AColumnIndex;
  if Corner in [coTopRight, coBottomRight] then
    Result.Right := AColumnIndex;
  if Corner in [coBottomLeft, coBottomRight] then
    Result.Bottom := ARowIndex;
end;

procedure TdxSpreadSheetTableViewReferencesHighlighterResizeDragAndDropObject.DragAndDrop(const P: TPoint; var Accepted: Boolean);
var
  AColumnIndex: Integer;
  ARowIndex: Integer;
begin
  Accepted := True;
  GetCellAtAbsolutePoint(P, ARowIndex, AColumnIndex);
  AdjustRowAndColumnIndexes(P, ARowIndex, AColumnIndex);
  CheckRowAndColumnIndexes(ARowIndex, AColumnIndex);
  UpdateReference(CalculateReferece(ARowIndex, AColumnIndex));
  inherited DragAndDrop(P, Accepted);
end;

{ TdxSpreadSheetTableViewEditingCellReferencesHighlighter }

function TdxSpreadSheetTableViewEditingCellReferencesHighlighter.GetCell: TdxSpreadSheetCell;
begin
  if EditingController.IsEditing then
    Result := EditingController.Cell
  else
    Result := nil;
end;

function TdxSpreadSheetTableViewEditingCellReferencesHighlighter.GetEditingController: TdxSpreadSheetTableViewEditingController;
begin
  Result := View.EditingController;
end;

{ TdxSpreadSheetContainerViewInfo }

constructor TdxSpreadSheetContainerViewInfo.Create(AContainer: TdxSpreadSheetContainer);
begin
  inherited Create(AContainer);
  FAlpha := MaxByte;
  FTransformMatrix := TdxGPMatrix.Create;
  FTransformMatrixInv := TdxGPMatrix.Create;
end;

destructor TdxSpreadSheetContainerViewInfo.Destroy;
begin
  FreeAndNil(FTransformMatrixInv);
  FreeAndNil(FTransformMatrix);
  FreeAndNil(FSelection);
  inherited Destroy;
end;

procedure TdxSpreadSheetContainerViewInfo.Invalidate;
begin
  if Selected then
    Selection.Invalidate;
  InvalidateRect(RealDrawingBounds);
end;

procedure TdxSpreadSheetContainerViewInfo.CalculateDisplayBounds;
var
  AMargins: TdxRectF;
begin
  if IsDragging and Calculated then
  begin
    AMargins := dxRectF(ContentBounds.Left - DisplayBounds.Left, ContentBounds.Top - DisplayBounds.Top,
      DisplayBounds.Right - ContentBounds.Right, DisplayBounds.Bottom - ContentBounds.Bottom);
  end
  else
    AMargins := dxNullRectF;

  FDisplayBounds := cxRectOffset(AbsoluteBounds, AbsoluteOrigin, False);
  FCalculated := True;
  ContentBounds := cxRectContent(dxRectF(FDisplayBounds), AMargins);
  CalculateTransformMatrix;
  CalculateSelection;
  CalculateDisplayClipRect;
end;

procedure TdxSpreadSheetContainerViewInfo.CalculateDisplayClipRect;
var
  R: TRect;
begin
  R := RealDrawingBounds;
  if Selected then
    R := cxRectUnion(R, Selection.RealDrawingBounds);
  FVisible := cxRectIntersect(FDisplayClipRect, R, FScreenClipRect);
end;

procedure TdxSpreadSheetContainerViewInfo.CalculateSelection;
begin
  if Selected then
  begin
    Selection.SetBounds(AbsoluteOrigin, cxRectInflate(AbsoluteBounds, Selection.ContentOffsets), ScreenClipRect);
    Selection.Calculate;
  end;
end;

procedure TdxSpreadSheetContainerViewInfo.CalculateTransformMatrix;
var
  APivotPoint: TdxPointF;
begin
  APivotPoint := dxPointF(cxRectCenter(DisplayBounds));

  TransformMatrix.Reset;
  TransformMatrix.Rotate(RotationAngle, APivotPoint);

  TransformMatrixInv.Assign(TransformMatrix);
  TransformMatrixInv.Invert;
end;

function TdxSpreadSheetContainerViewInfo.CanDraw(ACanvas: TcxCanvas; AStage: TdxSpreadSheetDrawingStage): Boolean;
begin
  Result := Visible and ACanvas.RectVisible(DisplayClipRect);
end;

procedure TdxSpreadSheetContainerViewInfo.ContentBoundsChanged;
begin
  CalculateDisplayClipRect;
end;

function TdxSpreadSheetContainerViewInfo.CreateSelectionViewInfo: TdxSpreadSheetContainerSelectionCellViewInfo;
begin
  Result := TdxSpreadSheetContainerSelectionCellViewInfo.Create(Self);
end;

procedure TdxSpreadSheetContainerViewInfo.DoCalculate;
begin
  FRotationAngle := Transform.RotationAngle;
  FFlipHorz := Transform.FlipHorizontally;
  FFlipVert := Transform.FlipVertically;
  CalculateDisplayBounds;
end;

procedure TdxSpreadSheetContainerViewInfo.DoDraw(ACanvas: TcxCanvas);
var
  ABitmap: TcxBitmap32;
  ARect: TRect;
begin
  if (DrawingStage = dsFirst) and (Alpha = MaxByte) then
  begin
    dxGPPaintCanvas.BeginPaint(ACanvas.Handle, RealDrawingBounds);
    try
      InternalDraw(dxGPPaintCanvas);
    finally
      dxGPPaintCanvas.EndPaint;
    end;
  end;
  
  if (DrawingStage = dsSecond) and (Alpha > 0) and (Alpha < MaxByte) then
  begin
    ARect := RealDrawingBounds;
    ABitmap := TcxBitmap32.CreateSize(ARect, True);
    try
      SetWindowOrgEx(ABitmap.Canvas.Handle, ARect.Left, ARect.Top, nil);

      dxGPPaintCanvas.BeginPaint(ABitmap.Canvas.Handle, ABitmap.ClientRect);
      try
        InternalDraw(dxGPPaintCanvas);
      finally
        dxGPPaintCanvas.EndPaint;
      end;

      SetWindowOrgEx(ABitmap.Canvas.Handle, 0, 0, nil);

      dxGPPaintCanvas.BeginPaint(ACanvas.Handle, ARect);
      try
        dxGPPaintCanvas.DrawBitmap(ABitmap, ARect, Alpha);
      finally
        dxGPPaintCanvas.EndPaint;
      end;
    finally
      ABitmap.Free;
    end;
  end;

  if Selected and not FIsDragging then
    Selection.Draw(ACanvas, DrawingStage);
end;

function TdxSpreadSheetContainerViewInfo.DoInitHitTest(const P: TPoint; AHitTest: TdxSpreadSheetCustomHitTest): Boolean;
begin
  Result := PtInRect(cxRect(ContentBounds), P);
end;

procedure TdxSpreadSheetContainerViewInfo.DrawBackground(ACanvas: TdxGPCanvas);
begin
  // do nothing
end;

procedure TdxSpreadSheetContainerViewInfo.DrawBorder(ACanvas: TdxGPCanvas);
begin
  // do nothing
end;

procedure TdxSpreadSheetContainerViewInfo.DrawContent(ACanvas: TdxGPCanvas);
begin
  // do nothing
end;

function TdxSpreadSheetContainerViewInfo.GetBorderWidth: Single;
begin
  Result := 0;
end;

function TdxSpreadSheetContainerViewInfo.GetDragAndDropObjectClass(AHitTest: TdxSpreadSheetCustomHitTest): TcxDragAndDropObjectClass;
begin
  if Owner.CanMove then
    Result := TdxSpreadSheetContainerMoveDragAndDropObject
  else
    Result := nil;
end;

function TdxSpreadSheetContainerViewInfo.GetPopupMenuClass(AHitTest: TdxSpreadSheetCustomHitTest): TComponentClass;
begin
  Result := TdxSpreadSheetBuiltInTableViewPopupMenu;
end;

function TdxSpreadSheetContainerViewInfo.GetRealBounds: TRect;
begin
  Result := cxRect(TransformMatrix.GetBoundingRectangle(ContentBounds));
end;

function TdxSpreadSheetContainerViewInfo.GetRealDrawingBounds: TRect;
begin
  Result := cxRect(TransformMatrix.GetBoundingRectangle(cxRectInflate(ContentBounds, Ceil(BorderWidth))), False);
end;

function TdxSpreadSheetContainerViewInfo.GetSpreadSheet: TdxCustomSpreadSheet;
begin
  Result := Owner.SpreadSheet;
end;

function TdxSpreadSheetContainerViewInfo.GetTransform: TdxSpreadSheetContainerTransform;
begin
  Result := Owner.Transform;
end;

function TdxSpreadSheetContainerViewInfo.GetZoomFactor: Integer;
begin
  if Owner.Parent <> nil then
    Result := Owner.Parent.ZoomFactor
  else
    Result := inherited GetZoomFactor;
end;

function TdxSpreadSheetContainerViewInfo.InitHitTest(AHitTest: TdxSpreadSheetCustomHitTest): Boolean;
begin
  Result := Selected and Selection.InitHitTest(AHitTest);
  if not Result then
  begin
    Result := DoInitHitTest(TransformMatrixInv.TransformPoint(AHitTest.ActualHitPoint), AHitTest);
    if Result then
    begin
      AHitTest.HitObject := Self;
      AHitTest.HitCodes[hcContainer] := True;
    end;
  end;
end;

procedure TdxSpreadSheetContainerViewInfo.InvalidateRect(const R: TRect);
begin
  if Owner.Parent <> nil then
    inherited InvalidateRect(Owner.Parent.AbsoluteToScreen(R));
end;

function TdxSpreadSheetContainerViewInfo.IsPermanent: Boolean;
begin
  Result := FIsDragging;
end;

procedure TdxSpreadSheetContainerViewInfo.UpdateState;
begin
  Selected := Owner.Focused;
end;

function TdxSpreadSheetContainerViewInfo.GetOptionsView: TdxSpreadSheetOptionsView;
begin
  Result := SpreadSheet.OptionsView;
end;

function TdxSpreadSheetContainerViewInfo.GetOwner: TdxSpreadSheetContainer;
begin
  Result := TdxSpreadSheetContainer(inherited Owner);
end;

function TdxSpreadSheetContainerViewInfo.GetSelected: Boolean;
begin
  Result := Selection <> nil;
end;

procedure TdxSpreadSheetContainerViewInfo.InternalDraw(ACanvas: TdxGPCanvas);
const
  CompositionQualityMap: array[Boolean] of Integer = (CompositingQualityHighSpeed, CompositingQualityHighQuality);
  InterpolationModeMap: array[Boolean] of TdxGPInterpolationMode = (imLowQuality, imHighQuality);
  PixelOffsetModeMap: array[Boolean] of TdxGpPixelOffsetMode = (PixelOffsetModeHighSpeed, PixelOffsetModeHighQuality);
  SmoothingModeMap: array[Boolean] of TdxGPSmoothingMode = (smHighSpeed, smAntiAlias);
begin
  ACanvas.InterpolationMode := InterpolationModeMap[OptionsView.Antialiasing];
  ACanvas.SmoothingMode := SmoothingModeMap[OptionsView.Antialiasing];
  GdipSetCompositingQuality(ACanvas.Handle, CompositionQualityMap[OptionsView.Antialiasing]);
  GdipSetPixelOffsetMode(ACanvas.Handle, PixelOffsetModeMap[OptionsView.Antialiasing]);

  ACanvas.SetWorldTransform(TransformMatrix);
  ACanvas.FlipWorldTransform(FFlipHorz, FFlipVert, cxRectCenter(ContentBounds));

  DrawBackground(ACanvas);
  DrawContent(ACanvas);
  DrawBorder(ACanvas);
end;

procedure TdxSpreadSheetContainerViewInfo.SetAlpha(const AValue: Byte);
begin
  if Alpha <> AValue then
  begin
    FAlpha := AValue;
    Invalidate;
  end;
end;

procedure TdxSpreadSheetContainerViewInfo.SetContentBounds(const AValue: TdxRectF);
begin
  if FContentBounds <> AValue then
  begin
    FContentBounds := AValue;
    ContentBoundsChanged;
  end;
end;

procedure TdxSpreadSheetContainerViewInfo.SetIsDragging(const Value: Boolean);
begin
  if IsDragging <> Value then
  begin
    FIsDragging := Value;
    Invalidate;
  end;
end;

procedure TdxSpreadSheetContainerViewInfo.SetRotationAngle(const AValue: Double);
begin
  if RotationAngle <> AValue then
  begin
    FRotationAngle := AValue;
    CalculateTransformMatrix;
  end;
end;

procedure TdxSpreadSheetContainerViewInfo.SetSelected(const AValue: Boolean);
begin
  if Selected <> AValue then
  begin
    if AValue then
    begin
      FSelection := CreateSelectionViewInfo;
      CalculateSelection;
    end
    else
    begin
      Selection.Invalidate;
      FreeAndNil(FSelection);
    end;
    CalculateDisplayClipRect;
  end;
end;

{ TdxSpreadSheetContainerCustomDragAndDropObject }

procedure TdxSpreadSheetContainerCustomDragAndDropObject.AlignToCell(var ADeltaX, ADeltaY: Integer; const P: TPoint);

  procedure SetDelta(var ADelta: Integer; ANewValue: Integer);
  begin
    if (ADelta = MaxInt) or (Abs(ANewValue) < Abs(ADelta)) then
      ADelta := ANewValue;
  end;

var
  ACellBounds: TRect;
  AColumnIndex: Integer;  
  ARowIndex: Integer;
begin
  if (TableViewIntf <> nil) and TableViewIntf.GetCellAtAbsolutePoint(P, ARowIndex, AColumnIndex) then
  begin
    ACellBounds := TableViewIntf.GetAbsoluteCellBounds(ARowIndex, AColumnIndex, False);
    if P.X > cxRectCenter(ACellBounds).X then
      SetDelta(ADeltaX, ACellBounds.Right - P.X)
    else
      SetDelta(ADeltaX, ACellBounds.Left - P.X);

    if P.Y > cxRectCenter(ACellBounds).Y then
      SetDelta(ADeltaY, ACellBounds.Bottom - P.Y)
    else
      SetDelta(ADeltaY, ACellBounds.Top - P.Y);
  end;
end;

procedure TdxSpreadSheetContainerCustomDragAndDropObject.AlignToCells(var R: TRect);
var
  ADeltaX: Integer; 
  ADeltaY: Integer;
begin
  ADeltaX := MaxInt;
  ADeltaY := MaxInt;
  AlignToCell(ADeltaX, ADeltaY, R.TopLeft);
  AlignToCell(ADeltaX, ADeltaY, R.BottomRight);
  R := cxRectOffset(R, IfThen(ADeltaX <> MaxInt, ADeltaX), IfThen(ADeltaY <> MaxInt, ADeltaY));
end;

procedure TdxSpreadSheetContainerCustomDragAndDropObject.BeforeBeginDragAndDrop;
begin
  inherited BeforeBeginDragAndDrop;
  FViewInfo := GetContainerViewInfo;
  FViewInfo.IsDragging := True;
  Supports(View, IdxSpreadSheetTableView, FTableViewIntf);
end;

procedure TdxSpreadSheetContainerCustomDragAndDropObject.BeginDragAndDrop;
begin
  inherited BeginDragAndDrop;
  FCapturePoint := CurMousePos;
  FBounds := ViewInfo.AbsoluteBounds;
  FContentRect := GetContentRect;
end;

function TdxSpreadSheetContainerCustomDragAndDropObject.CheckForContentArea(const R: TRect): TRect;
begin
  Result := Container.Calculator.CheckForContentArea(R);
end;

procedure TdxSpreadSheetContainerCustomDragAndDropObject.DragAndDrop(const P: TPoint; var Accepted: Boolean);
begin
  inherited DragAndDrop(P, Accepted);
  View.Controller.CheckScrollArea(cxPointScale(GetClientCursorPos, 100, ZoomFactor));
end;

procedure TdxSpreadSheetContainerCustomDragAndDropObject.EndDragAndDrop(Accepted: Boolean);
begin
  inherited EndDragAndDrop(Accepted);
  if Accepted and not cxRectIsEqual(Bounds, ViewInfo.AbsoluteBounds) then
    if not cxRectIsEqual(FBounds, ViewInfo.FAbsoluteBounds) then
      Container.CheckChangeObject(RecalculateAnchors);
  ViewInfo.Alpha := MaxByte;
  ViewInfo.IsDragging := False;
  Control.LayoutChanged;
end;

function TdxSpreadSheetContainerCustomDragAndDropObject.GetContainerViewInfo: TdxSpreadSheetContainerViewInfo;
begin
  Result := HitTest.HitObject as TdxSpreadSheetContainerViewInfo;
end;

function TdxSpreadSheetContainerCustomDragAndDropObject.GetContentRect: TRect;
begin
  Result := cxNullRect;
  if TableViewIntf <> nil then
    Result.BottomRight := TableViewIntf.GetAbsoluteCellBounds(dxSpreadSheetMaxRowIndex, dxSpreadSheetMaxColumnIndex, False).BottomRight;
end;

function TdxSpreadSheetContainerCustomDragAndDropObject.GetDragAndDropCursor(Accepted: Boolean): TCursor;
begin
  if Accepted then
    Result := ViewInfo.GetCursor(Control.ActiveController.HitTest)
  else
    Result := crNoDrop
end;

function TdxSpreadSheetContainerCustomDragAndDropObject.GetImmediateStart: Boolean;
begin
  Result := True;
end;

procedure TdxSpreadSheetContainerCustomDragAndDropObject.InvalidateRects(const R1, R2: TRect);
begin
  View.InvalidateRect(View.AbsoluteToScreen(R1));
  View.InvalidateRect(View.AbsoluteToScreen(R2));
  Control.Update;
end;

function TdxSpreadSheetContainerCustomDragAndDropObject.ProcessKeyDown(AKey: Word; AShiftState: TShiftState): Boolean;
begin
  case AKey of
    VK_CONTROL, VK_LCONTROL, VK_RCONTROL,
    VK_SHIFT, VK_LSHIFT, VK_RSHIFT,
    VK_MENU, VK_LMENU, VK_RMENU:
      Result := True;
  else    
    Result := inherited ProcessKeyDown(AKey, AShiftState); 
  end;
  RecalculateDragAndDropInfo;
end;

function TdxSpreadSheetContainerCustomDragAndDropObject.ProcessKeyUp(AKey: Word; AShiftState: TShiftState): Boolean;
begin
  Result := inherited ProcessKeyUp(AKey, AShiftState);
  RecalculateDragAndDropInfo; 
end;

procedure TdxSpreadSheetContainerCustomDragAndDropObject.RecalculateAnchors;
begin
  Container.Calculator.UpdateAnchors(ViewInfo.AbsoluteBounds);
end;

procedure TdxSpreadSheetContainerCustomDragAndDropObject.RecalculateDragAndDropInfo;
var
  AAccepted: Boolean;
begin
  DragAndDrop(CurMousePos, AAccepted);
end;

function TdxSpreadSheetContainerCustomDragAndDropObject.TranslateCoords(const P: TPoint): TPoint;
begin
  Result := cxPointOffset(inherited TranslateCoords(P), ContentOrigin);
end;

function TdxSpreadSheetContainerCustomDragAndDropObject.CheckIsKeyPressed(const Index: Integer): Boolean;
begin
  Result := GetAsyncKeyState(Index) < 0;
end;

function TdxSpreadSheetContainerCustomDragAndDropObject.GetContainer: TdxSpreadSheetContainer;
begin
  Result := ViewInfo.Owner;
end;

function TdxSpreadSheetContainerCustomDragAndDropObject.GetContentOrigin: TPoint;
begin
  Result := View.GetContentOrigin;
end;

function TdxSpreadSheetContainerCustomDragAndDropObject.GetPivotPoint: TPoint;
begin
  Result := cxRectCenter(Bounds);
end;

{ TdxSpreadSheetContainerCustomMoveDragAndDropObject }

procedure TdxSpreadSheetContainerCustomMoveDragAndDropObject.AlignToAxes(var R: TRect);
var
  P1, P2: TPoint;
begin 
  P1 := cxRectCenter(R);
  P2 := cxRectCenter(Bounds);
  if Abs(cxPointDistance(cxPoint(P2.X, P1.Y), P2)) > Abs(cxPointDistance(cxPoint(P1.X, P2.Y), P2)) then
    R := cxRectOffset(Bounds, 0, P1.Y - P2.Y)
  else
    R := cxRectOffset(Bounds, P1.X - P2.X, 0);
end; 

procedure TdxSpreadSheetContainerCustomMoveDragAndDropObject.BeginDragAndDrop;
begin
  inherited BeginDragAndDrop; 
  ViewInfo.Alpha := dxSpreadSheetMovingContainerAlpha;  
end;

procedure TdxSpreadSheetContainerCustomMoveDragAndDropObject.DragAndDrop(const P: TPoint; var Accepted: Boolean);
var
  APrevRealDrawingBounds: TRect;
  ARect: TRect;
begin
  Accepted := True;
  APrevRealDrawingBounds := ViewInfo.RealDrawingBounds;
  ARect := cxRectOffset(Bounds, P.X - CapturePoint.X, P.Y - CapturePoint.Y);
  if IsAltPressed then
    AlignToCells(ARect);
  if IsShiftPressed then
    AlignToAxes(ARect);
  ViewInfo.SetBounds(ContentOrigin, CheckForContentArea(ARect), ViewInfo.ScreenClipRect);
  ViewInfo.Calculate;
  InvalidateRects(APrevRealDrawingBounds, ViewInfo.RealDrawingBounds);
  inherited DragAndDrop(P, Accepted);
end;

function TdxSpreadSheetContainerCustomMoveDragAndDropObject.GetDragAndDropCursor(Accepted: Boolean): TCursor;
begin
  if Accepted then
    Result := crSizeAll
  else
    Result := inherited GetDragAndDropCursor(Accepted);
end;

{ TdxSpreadSheetClonedContainerMoveDragAndDropObject }

constructor TdxSpreadSheetClonedContainerMoveDragAndDropObject.Create(AContainer: TdxSpreadSheetContainer);
begin
  FContainer := AContainer;
  inherited Create(FContainer.SpreadSheet); 
end;

procedure TdxSpreadSheetClonedContainerMoveDragAndDropObject.BeginDragAndDrop;
begin
  inherited BeginDragAndDrop;
  ViewInfo.Alpha := dxSpreadSheetMovingClonedContainerAlpha;  
end;

function TdxSpreadSheetClonedContainerMoveDragAndDropObject.GetContainerViewInfo: TdxSpreadSheetContainerViewInfo;
begin
  Result := Containers.Items[Containers.FindItem(FContainer)] as TdxSpreadSheetContainerViewInfo;
end;

function TdxSpreadSheetClonedContainerMoveDragAndDropObject.TranslateCoords(const P: TPoint): TPoint;
begin
  Result := P;
end;

function TdxSpreadSheetClonedContainerMoveDragAndDropObject.GetContainers: TdxSpreadSheetViewInfoCellsList;
begin
  Result := FContainer.Parent.ViewInfo.Containers;
end;

{ TdxSpreadSheetContainerMoveDragAndDropObject }

function TdxSpreadSheetContainerMoveDragAndDropObject.CreateDragAndDropObjectForClonnedContainer: TdxSpreadSheetClonedContainerMoveDragAndDropObject;
begin
  Result := TdxSpreadSheetClonedContainerMoveDragAndDropObject.Create(ClonedContainer);
end;

procedure TdxSpreadSheetContainerMoveDragAndDropObject.DragAndDrop(const P: TPoint; var Accepted: Boolean);
begin
  MoveClone := IsControlPressed; 
  if ClonedContainerDragAndDropObject <> nil then
    ClonedContainerDragAndDropObject.DoDragAndDrop(P, Accepted)
  else  
    inherited DragAndDrop(P, Accepted);
end;

procedure TdxSpreadSheetContainerMoveDragAndDropObject.EndDragAndDrop(Accepted: Boolean);
begin
  if ClonedContainerDragAndDropObject <> nil then
  begin
    if Accepted then
    begin
      Container.History.BeginAction(TdxSpreadSheetHistoryChangeContainerAction);
      try
        Container.History.AddCommand(TdxSpreadSheetHistoryCreateContainerCommand.Create(ClonedContainerDragAndDropObject.Container));
        ClonedContainerDragAndDropObject.DoEndDragAndDrop(Accepted);
        FreeAndNil(FClonedContainerDragAndDropObject);
        ClonedContainer.Focused := True;
        inherited EndDragAndDrop(Accepted);
      finally
        Container.History.EndAction;
      end;
      Exit;
    end
    else
      MoveClone := False;
  end;
  inherited EndDragAndDrop(Accepted);
end;

function TdxSpreadSheetContainerMoveDragAndDropObject.GetMoveClone: Boolean;
begin
  Result := ClonedContainer <> nil;
end;

procedure TdxSpreadSheetContainerMoveDragAndDropObject.SetMoveClone(AValue: Boolean);
var
  AAccepted: Boolean;
begin                              
  if MoveClone <> AValue then
  begin
    if AValue then
    begin    
      FClonedContainer := TdxSpreadSheetContainerClass(Container.ClassType).Create(Control);
      FClonedContainer.Parent := View;
      FClonedContainer.Assign(Container);
      DragAndDrop(CapturePoint, AAccepted);
      FClonedContainerDragAndDropObject := CreateDragAndDropObjectForClonnedContainer;
      FClonedContainerDragAndDropObject.DoBeginDragAndDrop;
      FClonedContainerDragAndDropObject.FCapturePoint := CapturePoint;
    end
    else
    begin
      FClonedContainerDragAndDropObject.DoEndDragAndDrop(False);
      FreeAndNil(FClonedContainerDragAndDropObject); 
      FreeAndNil(FClonedContainer);      
    end;
  end;
end;

{ TdxSpreadSheetContainerResizeDragAndDropObject }

procedure TdxSpreadSheetContainerResizeDragAndDropObject.AlignToCells(var ADeltaX, ADeltaY: Single);
var
  dX, dY: Integer; 
  R: TdxRectF;
begin
  dX := MaxInt;
  dY := MaxInt;

  R := CalculateContentBounds(ADeltaX, ADeltaY);
  case Marker of
    smTop, smTopRight:
      AlignToCell(dX, dY, cxPoint(R.TopRight, False));
    smLeft, smTopLeft:
      AlignToCell(dX, dY, cxPoint(R.TopLeft, False));
    smBottom, smBottomLeft:
      AlignToCell(dX, dY, cxPoint(R.BottomLeft, False));
    smRight, smBottomRight:
      AlignToCell(dX, dY, cxPoint(R.BottomRight, False));
  end;

  if Marker in [smLeft, smTopLeft, smBottomLeft, smRight, smTopRight, smBottomRight] then
    ADeltaX := ADeltaX + IfThen(dX <> MaxInt, dX);
  if Marker in [smTop, smTopLeft, smTopRight, smBottom, smBottomLeft, smBottomRight] then
    ADeltaY := ADeltaY + IfThen(dY <> MaxInt, dY);
end;

function TdxSpreadSheetContainerResizeDragAndDropObject.CalculateContentBounds(const ADeltaX, ADeltaY: Single): TdxRectF;
begin
  Result := dxRectF(Bounds);
  if Marker in [smRight, smTopRight, smBottomRight] then
    Result.Right := Result.Right + ADeltaX;
  if Marker in [smTop, smTopLeft, smTopRight] then
    Result.Top := Result.Top + ADeltaY;
  if Marker in [smLeft, smTopLeft, smBottomLeft] then
    Result.Left := Result.Left + ADeltaX;
  if Marker in [smBottom, smBottomLeft, smBottomRight] then
    Result.Bottom := Result.Bottom + ADeltaY;

  if IsControlPressed then
  begin  
    if Marker in [smRight, smTopRight, smBottomRight] then
      Result.Left := Result.Left - ADeltaX;
    if Marker in [smTop, smTopLeft, smTopRight] then
      Result.Bottom := Result.Bottom - ADeltaY;
    if Marker in [smLeft, smTopLeft, smBottomLeft] then
      Result.Right := Result.Right - ADeltaX;
    if Marker in [smBottom, smBottomLeft, smBottomRight] then
      Result.Top := Result.Top - ADeltaY;
  end;
end;

procedure TdxSpreadSheetContainerResizeDragAndDropObject.CheckAspectUsingCornerHandles(var ADeltaX, ADeltaY: Single);
var
  ARatio: Single;
  ARect: TdxRectF;
  ASign: Integer;
begin
  if (Bounds.Width = 0) or (Bounds.Height = 0) then
    Exit;

  ARatio := Bounds.Height / Bounds.Width;
  ASign := IfThen(Marker in [smBottomLeft, smTopRight], -1, 1);
  ARect := CalculateContentBounds(ADeltaX, ASign * ADeltaX * ARatio);

  case Marker of
    smTopLeft, smTopRight:
      ARect.Top := ARect.Top + CapturePoint.Y - ViewInfo.AbsoluteBounds.Top;
    smBottomLeft, smBottomRight:
      ARect.Bottom := ARect.Bottom + CapturePoint.Y - ViewInfo.AbsoluteBounds.Bottom;
  end;

  if (CapturePoint.Y + ADeltaY < ARect.Top) or (CapturePoint.Y + ADeltaY > ARect.Bottom) then
    ADeltaX := ASign * ADeltaY / ARatio
  else
    ADeltaY := ASign * ADeltaX * ARatio;
end;

procedure TdxSpreadSheetContainerResizeDragAndDropObject.BeginDragAndDrop;
begin
  inherited BeginDragAndDrop;
  FMarker := TdxSpreadSheetSizingMarker(HitTest.HitObjectData);
  FCapturePoint := ViewInfo.TransformMatrixInv.TransformPoint(CapturePoint);
  ViewInfo.Alpha := dxSpreadSheetResizingContainerAlpha;
  ViewInfo.IsDragging := True;
end;

procedure TdxSpreadSheetContainerResizeDragAndDropObject.DragAndDrop(const P: TPoint; var Accepted: Boolean);
var
  ADeltaX: Single;
  ADeltaY: Single;
  APrevRealDrawingBounds: TRect;
  ARect: TdxRectF;
  P1: TdxPointF;
begin
  Accepted := True;
  APrevRealDrawingBounds := ViewInfo.RealDrawingBounds;
  P1 := ViewInfo.TransformMatrixInv.TransformPoint(dxPointF(P));

  ADeltaX := P1.X - FCapturePoint.X;
  ADeltaY := P1.Y - FCapturePoint.Y;

  if IsAltPressed then
    AlignToCells(ADeltaX, ADeltaY);

  if Container.KeepAspectUsingCornerHandles and (Marker in [smTopLeft, smTopRight, smBottomRight, smBottomLeft]) then
    CheckAspectUsingCornerHandles(ADeltaX, ADeltaY);

  ARect := CalculateContentBounds(ADeltaX, ADeltaY);
  ViewInfo.FFlipHorz := Transform.FlipHorizontally;
  ViewInfo.FFlipVert := Transform.FlipVertically;
  if ARect.Left > ARect.Right then
    ViewInfo.FFlipHorz := not ViewInfo.FFlipHorz;
  if ARect.Top > ARect.Bottom then
    ViewInfo.FFlipVert := not ViewInfo.FFlipVert;
  ViewInfo.ContentBounds := cxRectAdjustF(cxRectOffsetF(ARect, dxPointF(ContentOrigin), False));

  InvalidateRects(APrevRealDrawingBounds, ViewInfo.RealDrawingBounds);
  inherited DragAndDrop(P, Accepted);
end;

procedure TdxSpreadSheetContainerResizeDragAndDropObject.EndDragAndDrop(Accepted: Boolean);
begin
  Control.BeginUpdate;
  Container.BeginUpdate;
  try
    Container.History.BeginAction(TdxSpreadSheetHistoryChangeContainerAction);
    if Accepted then
    begin
      Container.History.AddCommand(TdxSpreadSheetHistoryChangeContainerCommand.Create(Container));
      RecalculateBounds;
      Transform.FlipHorizontally := ViewInfo.FFlipHorz;
      Transform.FlipVertically := ViewInfo.FFlipVert;
    end;
    inherited EndDragAndDrop(Accepted);
  finally
    Container.History.EndAction;
    Container.EndUpdate;
    Control.EndUpdate;
  end;
end;

function TdxSpreadSheetContainerResizeDragAndDropObject.GetContainerViewInfo: TdxSpreadSheetContainerViewInfo;
begin
  Result := (HitTest.HitObject as TdxSpreadSheetContainerSelectionCellViewInfo).ContainerViewInfo;
end;

procedure TdxSpreadSheetContainerResizeDragAndDropObject.RecalculateBounds;
var
  AFixedPoint: TdxPointF;
  AMatrix: TdxGPMatrix;
  APivotPoint: TdxPointF;
  AResizedPoint: TdxPointF;
begin
  GetPoints(AFixedPoint, AResizedPoint);

  AFixedPoint := ViewInfo.TransformMatrix.TransformPoint(AFixedPoint);
  AResizedPoint := ViewInfo.TransformMatrix.TransformPoint(AResizedPoint);

  AResizedPoint := cxPointOffset(AResizedPoint, dxPointF(ViewInfo.AbsoluteOrigin));
  AFixedPoint := cxPointOffset(AFixedPoint, dxPointF(ViewInfo.AbsoluteOrigin));

  AMatrix := TdxGPMatrix.Create;
  try
    APivotPoint := dxPointF((AResizedPoint.X + AFixedPoint.X) / 2, (AResizedPoint.Y + AFixedPoint.Y) / 2);

    AMatrix.Rotate(Transform.RotationAngle, APivotPoint);
    AMatrix.Invert;

    AResizedPoint := AMatrix.TransformPoint(AResizedPoint);
    AFixedPoint := AMatrix.TransformPoint(AFixedPoint);
  finally
    AMatrix.Free;
  end;

  ApplyBounds(AFixedPoint, AResizedPoint);
end;

procedure TdxSpreadSheetContainerResizeDragAndDropObject.ApplyBounds(const AFixedPoint, AResizedPoint: TdxPointF);
var
  R: TdxRectF;
begin
  case Marker of
    smTopLeft, smTop:
      R := dxRectF(AResizedPoint.X, AResizedPoint.Y, AFixedPoint.X, AFixedPoint.Y);
    smTopRight, smRight:
      R := dxRectF(AFixedPoint.X, AResizedPoint.Y, AResizedPoint.X, AFixedPoint.Y);
    smBottomRight, smBottom:
      R := dxRectF(AFixedPoint.X, AFixedPoint.Y, AResizedPoint.X, AResizedPoint.Y);
  else
    R := dxRectF(AResizedPoint.X, AFixedPoint.Y, AFixedPoint.X, AResizedPoint.Y);
  end;
  ViewInfo.SetBounds(ContentOrigin, CheckForContentArea(cxRect(R, False)), ViewInfo.ScreenClipRect);
end;

procedure TdxSpreadSheetContainerResizeDragAndDropObject.GetPoints(out AFixedPoint, AResizedPoint: TdxPointF);
begin
  case Marker of
    smTopLeft, smTop:
      begin
        AResizedPoint := ViewInfo.ContentBounds.TopLeft;
        AFixedPoint := ViewInfo.ContentBounds.BottomRight;
      end;

    smTopRight, smRight:
      begin
        AResizedPoint := ViewInfo.ContentBounds.TopRight;
        AFixedPoint := ViewInfo.ContentBounds.BottomLeft;
      end;

    smBottomRight, smBottom:
      begin
        AResizedPoint := ViewInfo.ContentBounds.BottomRight;
        AFixedPoint := ViewInfo.ContentBounds.TopLeft;
      end;

  else
    begin
      AResizedPoint := ViewInfo.ContentBounds.BottomLeft;
      AFixedPoint := ViewInfo.ContentBounds.TopRight;
    end;
  end;
end;

function TdxSpreadSheetContainerResizeDragAndDropObject.GetTransform: TdxSpreadSheetContainerTransform;
begin
  Result := Container.Transform;
end;

{ TdxSpreadSheetContainerRotateDragAndDropObject }

procedure TdxSpreadSheetContainerRotateDragAndDropObject.BeginDragAndDrop;
begin
  inherited BeginDragAndDrop;
  FCapturePoint := cxPointOffset(FCapturePoint, ContentOrigin, False);
  FCapturePoint := ViewInfo.TransformMatrixInv.TransformPoint(CapturePoint);
  FCapturePoint := cxPointOffset(FCapturePoint, ContentOrigin);
  
  ViewInfo.Alpha := dxSpreadSheetRotationContainerAlpha;
end;

procedure TdxSpreadSheetContainerRotateDragAndDropObject.DragAndDrop(const P: TPoint; var Accepted: Boolean);
const
  AlignAngle = 15;
var
  AAngle: Double;
  ACosValue: Double;
  ALengths: Double;
  APrevRealDrawingBounds: TRect;
  AVector1: TPoint;
  AVector2: TPoint;
begin
  Accepted := True;
  AVector1 := cxPointOffset(CapturePoint, PivotPoint, False);
  AVector2 := cxPointOffset(P, PivotPoint, False);

  ALengths := Sqrt(Sqr(AVector1.X) + Sqr(AVector1.Y)) * Sqrt(Sqr(AVector2.X) + Sqr(AVector2.Y));
  if not IsZero(ALengths) then
  begin
    ACosValue := (AVector1.X * AVector2.X + AVector1.Y * AVector2.Y) / ALengths;
    if SameValue(ACosValue, 1) then
      AAngle := 0
    else
      AAngle := 180 / Pi * ArcCos(ACosValue) * ValueIncr[P.X >= CapturePoint.X];

    if IsShiftPressed then
      AAngle := AlignAngle * Round(AAngle / AlignAngle);

    APrevRealDrawingBounds := ViewInfo.RealDrawingBounds;
    ViewInfo.RotationAngle := AAngle;
    InvalidateRects(APrevRealDrawingBounds, ViewInfo.RealDrawingBounds);
  end;
  inherited DragAndDrop(P, Accepted);
end;

procedure TdxSpreadSheetContainerRotateDragAndDropObject.EndDragAndDrop(Accepted: Boolean);
begin
  if Accepted and not SameValue(ViewInfo.Transform.RotationAngle, ViewInfo.RotationAngle) then
  begin
    Container.CheckChangeObject(procedure
    begin
      ViewInfo.Transform.RotationAngle := ViewInfo.RotationAngle;
      ViewInfo.SetBounds(ContentOrigin, CheckForContentArea(Bounds), ViewInfo.ScreenClipRect);
      inherited EndDragAndDrop(Accepted);
    end);
  end
  else
    inherited EndDragAndDrop(Accepted);
end;

function TdxSpreadSheetContainerRotateDragAndDropObject.GetContainerViewInfo: TdxSpreadSheetContainerViewInfo;
begin
  Result := (HitTest.HitObject as TdxSpreadSheetContainerSelectionCellViewInfo).ContainerViewInfo;
end;

function TdxSpreadSheetContainerRotateDragAndDropObject.GetDragAndDropCursor(Accepted: Boolean): TCursor;
begin
  if Accepted then
    Result := crdxSpreadSheetRotation
  else
    Result := inherited GetDragAndDropCursor(Accepted);
end;

{ TdxSpreadSheetContainerSelectionCellViewInfo }

constructor TdxSpreadSheetContainerSelectionCellViewInfo.Create(AContainerViewInfo: TdxSpreadSheetContainerViewInfo);
begin
  inherited Create(AContainerViewInfo.Owner);
  FContainerViewInfo := AContainerViewInfo;
  FRotateMarkerSize := dxSpreadSheetContainerRotateMarkerSize;
  FSizeMarkerSize := dxSpreadSheetContainerSizingMarkerSize;
end;

function TdxSpreadSheetContainerSelectionCellViewInfo.HasResizeMarkers: Boolean;
begin
  Result := ContainerViewInfo.Owner.CanResize;
end;

function TdxSpreadSheetContainerSelectionCellViewInfo.HasRotateMarker: Boolean;
begin
  Result := ContainerViewInfo.Owner.CanRotate;
end;

procedure TdxSpreadSheetContainerSelectionCellViewInfo.Invalidate;
begin
  InvalidateRect(RealDrawingBounds);
end;

procedure TdxSpreadSheetContainerSelectionCellViewInfo.CalculateDisplayBounds;
begin
  FDisplayBounds := cxRectOffset(AbsoluteBounds, AbsoluteOrigin, False);
  FCalculated := True;
  FVisible := cxRectIntersect(FDisplayClipRect, RealDrawingBounds, FScreenClipRect);
  FSizeMarkersArea := DisplayBounds;
  if HasRotateMarker then
    Inc(FSizeMarkersArea.Top, SizingPointSize + RotateMarkerSize);
end;

function TdxSpreadSheetContainerSelectionCellViewInfo.CanDraw(ACanvas: TcxCanvas; AStage: TdxSpreadSheetDrawingStage): Boolean;
begin
  Result := AStage = dsSecond;
end;

function TdxSpreadSheetContainerSelectionCellViewInfo.CreatePen: TdxGpPen;
begin
  Result := TdxGPPen.Create;
  Result.Brush.Color := dxSpreadSheetContainerSelectionFrameColor;
end;

function TdxSpreadSheetContainerSelectionCellViewInfo.CreateSizingMarkerBrush: TdxGpBrush;
begin
  Result := TdxGPBrush.Create;
  Result.Style := gpbsGradient;
  Result.GradientMode := gpbgmVertical;
  Result.GradientPoints.Add(0, dxSpreadSheetContainerSizingMarkerColor1);
  Result.GradientPoints.Add(0.5, dxSpreadSheetContainerSizingMarkerColor2);
  Result.GradientPoints.Add(1, dxSpreadSheetContainerSizingMarkerColor1);
end;

function TdxSpreadSheetContainerSelectionCellViewInfo.CreateRotateMarkerBrush: TdxGpBrush;
begin
  Result := TdxGPBrush.Create;
  Result.Style := gpbsGradient;
  Result.GradientMode := gpbgmVertical;
  Result.GradientPoints.Add(0, dxSpreadSheetContainerRotateMarkerColor1);
  Result.GradientPoints.Add(0.5, dxSpreadSheetContainerRotateMarkerColor2);
  Result.GradientPoints.Add(1, dxSpreadSheetContainerRotateMarkerColor1);
end;

procedure TdxSpreadSheetContainerSelectionCellViewInfo.DoDraw(ACanvas: TcxCanvas);
const
  Map: array[Boolean] of TdxGPSmoothingMode = (smHighSpeed, smAntiAlias);
begin
  dxGPPaintCanvas.BeginPaint(ACanvas.Handle, RealDrawingBounds);
  try
    dxGPPaintCanvas.SmoothingMode := Map[SpreadSheet.OptionsView.Antialiasing];
    dxGPPaintCanvas.SetWorldTransform(ContainerViewInfo.TransformMatrix);
    DrawContent(dxGPPaintCanvas);
  finally
    dxGPPaintCanvas.EndPaint;
  end;
end;

procedure TdxSpreadSheetContainerSelectionCellViewInfo.DrawContent(ACanvas: TdxGPCanvas);
var
  APen: TdxGPPen;
begin
  ACanvas.SaveClipRegion;
  try
    APen := CreatePen;
    try
      if HasResizeMarkers then
        DrawSizingMarkers(ACanvas, APen);
      if HasRotateMarker then
        DrawRotateMarker(ACanvas, APen);
      ACanvas.Rectangle(FrameRect, APen, nil);
    finally
      APen.Free;
    end;
  finally
    ACanvas.RestoreClipRegion;
  end;
end;

procedure TdxSpreadSheetContainerSelectionCellViewInfo.DrawRotateMarker(ACanvas: TdxGPCanvas; APen: TdxGPPen);
var
  ABrush: TdxGPBrush;
  ARect: TRect;
begin
  ABrush := CreateRotateMarkerBrush;
  try
    ARect := RotateMarker;
    ACanvas.Ellipse(ARect, APen, ABrush);
    ACanvas.SetClipRect(ARect, gmExclude);
    ACanvas.Line(cxRectCenter(ARect).X, ARect.Bottom, cxRectCenter(ARect).X, FrameRect.Top, APen);
  finally
    ABrush.Free;
  end;
end;

procedure TdxSpreadSheetContainerSelectionCellViewInfo.DrawSizingMarkers(ACanvas: TdxGPCanvas; APen: TdxGPPen);
var
  ABrush: TdxGPBrush;
  AMarker: TdxSpreadSheetSizingMarker;
  ARect: TRect;
begin
  ABrush := CreateSizingMarkerBrush;
  try
    for AMarker := Low(AMarker) to High(AMarker) do
    begin
      ARect := SizeMarkerRect[AMarker];
      ACanvas.Rectangle(ARect, APen, ABrush);
      ACanvas.SetClipRect(ARect, gmExclude);
    end;
  finally
    ABrush.Free;
  end;
end;

function TdxSpreadSheetContainerSelectionCellViewInfo.GetContentOffsets: TRect;
var
  AHalfSize: Integer;
begin
  AHalfSize := SizingPointSize div 2;
  Result := cxRect(AHalfSize, AHalfSize, AHalfSize, AHalfSize);
  if HasRotateMarker then
    Inc(Result.Top, RotateMarkerSize + SizingPointSize);
end;

function TdxSpreadSheetContainerSelectionCellViewInfo.GetCursor(AHitTest: TdxSpreadSheetCustomHitTest): TCursor;
const
  CursorMap: array[0..7, TdxSpreadSheetSizingMarker] of TCursor = (
    (crSizeWE, crSizeNWSE, crSizeNS, crSizeNESW, crSizeWE, crSizeNWSE, crSizeNS, crSizeNESW),
    (crSizeNWSE, crSizeNS, crSizeNESW, crSizeWE, crSizeNWSE, crSizeNS, crSizeNESW, crSizeWE),
    (crSizeNS, crSizeNESW, crSizeWE, crSizeNWSE, crSizeNS, crSizeNESW, crSizeWE, crSizeNWSE),
    (crSizeNESW, crSizeWE, crSizeNWSE, crSizeNS, crSizeNESW, crSizeWE, crSizeNWSE, crSizeNS),

    (crSizeWE, crSizeNWSE, crSizeNS, crSizeNESW, crSizeWE, crSizeNWSE, crSizeNS, crSizeNESW),
    (crSizeNWSE, crSizeNS, crSizeNESW, crSizeWE, crSizeNWSE, crSizeNS, crSizeNESW, crSizeWE),
    (crSizeNS, crSizeNESW, crSizeWE, crSizeNWSE, crSizeNS, crSizeNESW, crSizeWE, crSizeNWSE),
    (crSizeNESW, crSizeWE, crSizeNWSE, crSizeNS, crSizeNESW, crSizeWE, crSizeNWSE, crSizeNS)
  );
var
  AAngleIndex: Integer;
begin
  if AHitTest.HitObjectData < 0 then
    Result := crdxSpreadSheetRotate
  else
  begin
    AAngleIndex := Round(ContainerViewInfo.Transform.RotationAngle / 45) mod 8;
    if AAngleIndex < 0 then
      Inc(AAngleIndex, 8);
    Result := CursorMap[AAngleIndex, TdxSpreadSheetSizingMarker(AHitTest.HitObjectData)];
  end;
end;

function TdxSpreadSheetContainerSelectionCellViewInfo.GetDragAndDropObjectClass(
  AHitTest: TdxSpreadSheetCustomHitTest): TcxDragAndDropObjectClass;
begin
  if AHitTest.HitObjectData < 0 then
    Result := TdxSpreadSheetContainerRotateDragAndDropObject
  else
    Result := TdxSpreadSheetContainerResizeDragAndDropObject;
end;

function TdxSpreadSheetContainerSelectionCellViewInfo.GetFrameRect: TRect;
begin
  Result := cxRectInflate(SizeMarkersArea, -SizingPointSize div 2);
end;

function TdxSpreadSheetContainerSelectionCellViewInfo.GetRealDrawingBounds: TRect;
begin
  Result := cxRectInflate(cxRect(ContainerViewInfo.TransformMatrix.GetBoundingRectangle(dxRectF(DisplayBounds))), 1);
end;

function TdxSpreadSheetContainerSelectionCellViewInfo.GetRotateMarker: TRect;
begin
  if HasRotateMarker then
    Result := cxRectCenterHorizontally(cxRectSetHeight(DisplayBounds, RotateMarkerSize), RotateMarkerSize)
  else
    Result := cxNullRect;
end;

function TdxSpreadSheetContainerSelectionCellViewInfo.GetSizeMarkerAtLocalPoint(
  const ALocalPoint: TPoint; out AMarker: TdxSpreadSheetSizingMarker): Boolean;
var
  AIndex: TdxSpreadSheetSizingMarker;
begin
  Result := False;
  for AIndex := Low(TdxSpreadSheetSizingMarker) to High(TdxSpreadSheetSizingMarker) do
    if PtInRect(SizeMarkerRect[AIndex], ALocalPoint) then
    begin
      AMarker := AIndex;
      Result := True;
      Break;
    end;
end;

function TdxSpreadSheetContainerSelectionCellViewInfo.GetSizeMarkerRect(AMarker: TdxSpreadSheetSizingMarker): TRect;
begin
  if HasResizeMarkers then
  begin
    Result := cxRectSetSize(SizeMarkersArea, SizingPointSize, SizingPointSize);
    if AMarker in [smTopRight, smBottomRight] then
      Result := cxRectSetRight(Result, SizeMarkersArea.Right);
    if AMarker in [smBottomLeft, smBottomRight] then
      Result := cxRectSetBottom(Result, SizeMarkersArea.Bottom);

    if AMarker in [smLeft, smRight] then
      Result := cxRectOffset(Result, 0, (cxRectHeight(SizeMarkersArea) - SizingPointSize) div 2)
    else
      if AMarker = smBottom then
        Result := cxRectOffset(Result, 0, cxRectHeight(SizeMarkersArea) - SizingPointSize);

    if AMarker in [smTop, smBottom] then
      Result := cxRectOffset(Result, (cxRectWidth(SizeMarkersArea) - SizingPointSize) div 2, 0)
    else
      if AMarker = smRight then
        Result := cxRectOffset(Result, cxRectWidth(SizeMarkersArea) - SizingPointSize, 0);
  end
  else
    Result := cxNullRect;
end;

function TdxSpreadSheetContainerSelectionCellViewInfo.GetSpreadSheet: TdxCustomSpreadSheet;
begin
  Result := ContainerViewInfo.SpreadSheet;
end;

function TdxSpreadSheetContainerSelectionCellViewInfo.GetZoomFactor: Integer;
begin 
  Result := ContainerViewInfo.ZoomFactor;
end; 

function TdxSpreadSheetContainerSelectionCellViewInfo.InitHitTest(AHitTest: TdxSpreadSheetCustomHitTest): Boolean;
var
  ALocalPoint: TPoint;
  AMarker: TdxSpreadSheetSizingMarker;
begin
  ALocalPoint := ContainerViewInfo.TransformMatrixInv.TransformPoint(AHitTest.ActualHitPoint);

  Result := PtInRect(RotateMarker, ALocalPoint);
  if Result then
  begin
    AHitTest.HitObject := Self;
    AHitTest.HitObjectData := -1;
    AHitTest.HitCodes[hcContainerSelection] := True;
  end
  else
    if GetSizeMarkerAtLocalPoint(ALocalPoint, AMarker) then
    begin
      AHitTest.HitObject := Self;
      AHitTest.HitObjectData := Ord(AMarker);
      AHitTest.HitCodes[hcContainerSelection] := True;
      Result := True;
    end;
end;

procedure TdxSpreadSheetContainerSelectionCellViewInfo.InvalidateRect(const R: TRect);
begin
  ContainerViewInfo.InvalidateRect(R);
end;

{ TdxSpreadSheetResizeTableItemDragAndDropObject }

constructor TdxSpreadSheetResizeTableItemDragAndDropObject.Create(AControl: TcxControl);
var
  AHeaderViewInfo: TdxSpreadSheetTableViewHeaderCellViewInfo;
begin
  inherited Create(AControl);
  StartPos := Control.GetMouseCursorClientPos;

  if HitTest.HitAtColumnHeader then
    Items := View.Columns
  else
    if HitTest.HitAtRowHeader then
      Items := View.Rows
    else
    begin
      AControl.FinishDragAndDrop(False);
      Exit;
    end;

  AHeaderViewInfo := TdxSpreadSheetTableViewHeaderCellViewInfo(HitTest.HitObject);
  ItemIndex := AHeaderViewInfo.Index;
  if Items.GetItemVisible(ItemIndex + 1) then
    ItemStartPos := AHeaderViewInfo.AbsoluteBounds.TopLeft
  else
    ItemStartPos := cxPoint(AHeaderViewInfo.AbsoluteBounds.Left, AHeaderViewInfo.AbsoluteBounds.Bottom);

  ItemStartPos := cxPointOffset(ItemStartPos, cxSimplePoint, False);
  ItemStartPos := cxPointOffset(ItemStartPos, View.GetPartOffsetByPoint(StartPos), False);
  ItemStartPos := cxPointScale(ItemStartPos, View.Options.ZoomFactor, 100);
end;

procedure TdxSpreadSheetResizeTableItemDragAndDropObject.BeginDragAndDrop;
begin
  inherited BeginDragAndDrop;
  PrevMousePos := StartPos;
  DrawSizingMark(GetSizingMarkBounds(StartPos));
  DrawSizingMark(GetSizingMarkBounds(ItemStartPos));
end;

procedure TdxSpreadSheetResizeTableItemDragAndDropObject.ChangePosition;
begin
  PrevMousePos := CheckPoint(PrevMousePos);
  CurMousePos := CheckPoint(CurMousePos);
  if GetDistance(PrevMousePos, CurMousePos) <> 0 then
  begin
    DrawSizingMark(GetSizingMarkBounds(PrevMousePos));
    ChangeMousePos(CurMousePos);
    DrawSizingMark(GetSizingMarkBounds(CurMousePos));
  end;
end;

function TdxSpreadSheetResizeTableItemDragAndDropObject.CheckPoint(const P: TPoint): TPoint;
var
  R: TRect;
begin
  R := cxRectScale(View.ViewInfo.CellsArea, View.Options.ZoomFactor, 100);
  Result.X := Max(R.Left, Min(P.X, R.Right));
  Result.Y := Max(R.Top, Min(P.Y, R.Bottom));
end;

procedure TdxSpreadSheetResizeTableItemDragAndDropObject.DrawSizingMark(const ARect: TRect);
begin
  Canvas.Pen.Color := clBlack;
  Canvas.Pen.Style := psDot;
  Canvas.Pen.Mode := pmXor;
  if Orientation = orHorizontal then
  begin
    Canvas.MoveTo(ARect.Left, ARect.Top);
    Canvas.LineTo(ARect.Left, ARect.Bottom);
  end
  else
  begin
    Canvas.MoveTo(ARect.Left, ARect.Top);
    Canvas.LineTo(ARect.Right, ARect.Top);
  end
end;

function TdxSpreadSheetResizeTableItemDragAndDropObject.GetDistance(const P1, P2: TPoint): Integer;
begin
  Result := P1.X - P2.X;
  if Orientation <> orHorizontal then
    Result := P1.Y - P2.Y;
end;

function TdxSpreadSheetResizeTableItemDragAndDropObject.GetDragAndDropCursor(Accepted: Boolean): TCursor;
const
  Cursors: array[TdxOrientation] of TCursor = (crcxHorzSize, crcxVertSize);
begin
  Result := Cursors[Orientation];
end;

function TdxSpreadSheetResizeTableItemDragAndDropObject.GetImmediateStart: Boolean;
begin
  Result := True;
end;

function TdxSpreadSheetResizeTableItemDragAndDropObject.GetSizingMarkBounds(const P: TPoint): TRect;
begin
  if Orientation = orHorizontal then
    Result := cxRectSetLeft(Control.ClientBounds, P.X, 1)
  else
    Result := cxRectSetTop(Control.ClientBounds, P.Y, 1);
end;

function TdxSpreadSheetResizeTableItemDragAndDropObject.IsEntireSheetArea(const R: TRect): Boolean;
begin
  Result := cxPointIsEqual(R.TopLeft, cxNullPoint) and
    (R.Right >= dxSpreadSheetMaxColumnIndex) and (R.Bottom >= dxSpreadSheetMaxRowIndex);
end;

procedure TdxSpreadSheetResizeTableItemDragAndDropObject.DragAndDrop(const P: TPoint; var Accepted: Boolean);
begin
  inherited DragAndDrop(P, Accepted);
  ChangePosition;
end;

procedure TdxSpreadSheetResizeTableItemDragAndDropObject.EndDragAndDrop(Accepted: Boolean);
var
  I: Integer;
  ARect, AEntireArea: TRect;
  ADelta: Integer;
  AItemSize: Integer;
  AFullItemSelected, AEntireAreaSelected: Boolean;
begin
  ADelta := MulDiv(GetDistance(CurMousePos, StartPos), 100, View.Options.ZoomFactor);
  Accepted := Accepted and (ADelta <> 0);
  if Accepted then
  begin
    View.BeginUpdate;
    try
      View.History.BeginAction(TdxSpreadSheetHistoryChangeRowColumnItemAction);
      Items.ResizeItem(ItemIndex, ADelta);
      if Items.GetItemState(ItemIndex) = cxbsHot then
      begin
        AItemSize := Items.GetItemSize(ItemIndex);
        if Orientation = orHorizontal then
          AFullItemSelected := View.Selection.IsEntireColumnSelected(ItemIndex)
        else
          AFullItemSelected := View.Selection.IsEntireRowSelected(ItemIndex);
        if AFullItemSelected then
        begin
          AEntireArea := Rect(0, 0, dxSpreadSheetMaxColumnIndex, dxSpreadSheetMaxRowIndex);
          ARect := View.Selection.Items[0].Rect;
          AEntireAreaSelected := IsEntireSheetArea(ARect);
          if not AEntireAreaSelected then
            for I := 1 to View.Selection.Count - 1 do
            begin
              ARect := dxSpreadSheetCellsUnion(View.Selection.Items[I].Rect, ARect);
              AEntireAreaSelected := AEntireAreaSelected or IsEntireSheetArea(View.Selection.Items[I].Rect);
            end;
          if AEntireAreaSelected then
          begin
            Items.ForEach(
              procedure (AItem: TdxDynamicListItem; AData: Pointer)
              begin
                TdxSpreadSheetTableItem(AItem).Size := AItemSize;
              end);
            View.History.AddCommand(TdxSpreadSheetChangeDefaultSizeCommand.Create(Items));
            Items.DefaultSize := AItemSize;
          end
          else
          begin
            cxRectIntersect(ARect, AEntireArea, ARect);
            if Orientation = orVertical then
              ARect := cxRectRotate(ARect);
            for I := ARect.Left to ARect.Right do
              if ((Orientation = orHorizontal) and View.Selection.IsEntireColumnSelected(I)) or
                 ((Orientation = orVertical) and View.Selection.IsEntireRowSelected(I)) then
                   TdxSpreadSheetTableItem(Items.ItemNeeded(I)).Size := AItemSize;
          end;
        end;
      end;
    finally
      View.History.EndAction;
      View.EndUpdate;
    end;
  end;
  View.Invalidate;
end;

function TdxSpreadSheetResizeTableItemDragAndDropObject.TranslateCoords(const P: TPoint): TPoint;
begin
  Result := P;
end;

function TdxSpreadSheetResizeTableItemDragAndDropObject.GetHitTest: TdxSpreadSheetTableViewHitTest;
begin
  Result := View.HitTest;
end;

function TdxSpreadSheetResizeTableItemDragAndDropObject.GetOrientation: TdxOrientation;
begin
  if Items = View.Rows then
    Result := orVertical
  else
    Result := orHorizontal;
end;

function TdxSpreadSheetResizeTableItemDragAndDropObject.GetView: TdxSpreadSheetTableView;
begin
  Result := Control.ActiveSheetAsTable;
end;

{ TdxSpreadSheetTableViewCustomCellViewInfo }

function TdxSpreadSheetTableViewCustomCellViewInfo.DoCustomDraw(ACanvas: TcxCanvas): Boolean;
begin
  SpreadSheet.DoCustomDrawTableViewCommonCell(View, ACanvas, Self, Result);
end;

function TdxSpreadSheetTableViewCustomCellViewInfo.GetPopupMenuClass(AHitTest: TdxSpreadSheetCustomHitTest): TComponentClass;
begin
  Result := TdxSpreadSheetBuiltInTableViewPopupMenu;
end;

function TdxSpreadSheetTableViewCustomCellViewInfo.GetSpreadSheet: TdxCustomSpreadSheet;
begin
  Result := View.SpreadSheet;
end;

function TdxSpreadSheetTableViewCustomCellViewInfo.GetZoomFactor: Integer;
begin
  Result := View.ZoomFactor;
end;

function TdxSpreadSheetTableViewCustomCellViewInfo.GetOwner: TdxSpreadSheetTableViewInfo;
begin
  Result := TdxSpreadSheetTableViewInfo(inherited Owner);
end;

function TdxSpreadSheetTableViewCustomCellViewInfo.GetView: TdxSpreadSheetTableView;
begin
  Result := TdxSpreadSheetTableViewInfo(Owner).View;
end;

procedure TdxSpreadSheetTableViewCustomCellViewInfo.InvalidateRect(const R: TRect);
begin
  inherited InvalidateRect(View.AbsoluteToScreen(R));
end;

{ TdxSpreadSheetTableViewHeaderCellViewInfo }

procedure TdxSpreadSheetTableViewHeaderCellViewInfo.ApplyBestFit;
begin
  if Item <> nil then
    Item.ApplyBestFit;
end;

function TdxSpreadSheetTableViewHeaderCellViewInfo.DoCustomDraw(ACanvas: TcxCanvas): Boolean;
begin
  ACanvas.Font.Assign(ViewParams.Font);
  ACanvas.Font.Color := ViewParams.TextColor;
  ACanvas.Brush.Color := ViewParams.Color;
  SpreadSheet.DoCustomDrawTableViewHeaderCell(View, ACanvas, Self, Result);
end;

procedure TdxSpreadSheetTableViewHeaderCellViewInfo.DoDraw(ACanvas: TcxCanvas);
begin
  FViewParams.TextColor := GetTextColor;
  LookAndFeelPainter.DrawSpreadSheetHeader(ACanvas, DisplayBounds, cxTextRect(DisplayBounds), Neighbors, Borders, State, AlignHorz,
    AlignVert, False, False, DisplayText, ACanvas.Font, ViewParams.TextColor, ViewParams.Color, nil, IsLast);
end;

function TdxSpreadSheetTableViewHeaderCellViewInfo.GetBorders: TcxBorders;

  function GetItems: TdxSpreadSheetTableItems;
  begin
    if Neighbors * [nLeft, nRight] <> [] then
      Result := Owner.Columns
    else
      Result := Owner.Rows;
  end;

begin
  Result := LookAndFeelPainter.HeaderBorders(Neighbors);
  if not GetItems.GetItemVisible(Index - 1) then
  begin
    if Neighbors * [nLeft, nRight] <> [] then
      Include(Result, bLeft)
    else
      Include(Result, bTop);
  end;
end;

function TdxSpreadSheetTableViewHeaderCellViewInfo.GetCursor(AHitTest: TdxSpreadSheetCustomHitTest): TCursor;
const
  Cursors: array[Boolean] of TCursor = (crcxVertSize, crcxHorzSize);
  ArrowCursors: array[Boolean] of TCursor = (crdxSpreadSheetLeftArrow, crdxSpreadSheetDownArrow);
begin
  if AHitTest.GetHitCode(hcResizeArea) then
    Result := Cursors[AHitTest.GetHitCode(hcColumnHeader)]
  else
    Result := ArrowCursors[AHitTest.GetHitCode(hcColumnHeader)]
end;

function TdxSpreadSheetTableViewHeaderCellViewInfo.GetDragAndDropObjectClass(
  AHitTest: TdxSpreadSheetCustomHitTest): TcxDragAndDropObjectClass;
begin
  if AHitTest.GetHitCode(hcRowHeader or hcColumnHeader) and AHitTest.GetHitCode(hcResizeArea) then
    Result := TdxSpreadSheetResizeTableItemDragAndDropObject
  else
    Result := nil;
end;

function TdxSpreadSheetTableViewHeaderCellViewInfo.GetTextColor: TColor;
const
  ColorMap: array[Boolean] of TColor = (clBlack, clRed);
begin
  Result := cxGetActualColor(ViewParams.TextColor, ColorMap[State = cxbsHot]);
end;

procedure TdxSpreadSheetTableViewHeaderCellViewInfo.Initialize(AItem: TdxSpreadSheetTableItem;
  AIndex: Integer; ANeighbors: TcxNeighbors; const ADisplayText: TdxUnicodeString);
begin
  FDisplayText := ADisplayText;
  FViewParams := TdxSpreadSheetTableViewInfo(Owner).HeaderParams;
  FAlignHorz := taCenter;
  FAlignVert := cxClasses.vaCenter;
  FIndex := AIndex;
  FNeighbors := ANeighbors;
  FItem := AItem;
  FIsFirst := Index = -1;
  FIsLast := [nRight, nBottom] * FNeighbors = [];
end;

function TdxSpreadSheetTableViewHeaderCellViewInfo.InitHitTest(AHitTest: TdxSpreadSheetCustomHitTest): Boolean;

  function IsNextNeighborTooSmall: Boolean;
  begin
    Result := (FNextNeighbor <> nil) and (FNextNeighbor.Item <> nil) and
      (FNextNeighbor.Item.Size < 2 * dxSpreadSheetResizeDelta);
  end;

  function IsResizeArea: Boolean;
  var
    AResizeArea: TRect;
    AResizeAreaSize: Integer;
  begin
    if IsNextNeighborTooSmall then
      AResizeAreaSize := dxSpreadSheetResizeDelta
    else
      AResizeAreaSize := dxSpreadSheetResizeDelta * 2;

    if Neighbors * [nLeft, nRight] <> [] then
      AResizeArea := cxRectSetLeft(DisplayBounds, DisplayBounds.Right - dxSpreadSheetResizeDelta, AResizeAreaSize)
    else
      AResizeArea := cxRectSetTop(DisplayBounds, DisplayBounds.Bottom - dxSpreadSheetResizeDelta, AResizeAreaSize);

    Result := cxRectPtIn(AResizeArea, AHitTest.ActualHitPoint) and not View.Options.Protected;
  end;

const
  HitCodes: array[Boolean] of Integer = (hcRowHeader, hcColumnHeader);
begin
  Result := inherited InitHitTest(AHitTest);
  if Index < 0 then
  begin
    if Result then
      AHitTest.SetHitCode(hcColumnHeader, True);
    Exit;
  end;

  AHitTest.SetHitCode(hcResizeArea, IsResizeArea);
  Result := Result or AHitTest.GetHitCode(hcResizeArea);
  AHitTest.SetHitCode(HitCodes[Neighbors * [nLeft, nRight] <> []], Result);
  if Result then
    AHitTest.HitObject := Self;
end;

procedure TdxSpreadSheetTableViewHeaderCellViewInfo.SetState(AValue: TcxButtonState);
begin
  if AValue <> FState then
  begin
    FState := AValue;
    Invalidate;
  end;
end;

{ TdxSpreadSheetShapeContainerViewInfo }

destructor TdxSpreadSheetShapeContainerViewInfo.Destroy;
begin
  FreeAndNil(FShapePath);
  FreeAndNil(FShapeOutlinePath);
  inherited Destroy;
end;

procedure TdxSpreadSheetShapeContainerViewInfo.RecreateShape;
begin
  FreeAndNil(FShapePath);
  FreeAndNil(FShapeOutlinePath);
end;

procedure TdxSpreadSheetShapeContainerViewInfo.ContentBoundsChanged;
begin
  inherited ContentBoundsChanged;
  RecreateShape;
end;

function TdxSpreadSheetShapeContainerViewInfo.CreateShapeOutlinePath: TdxGPPath;
begin
  Result := DoCreateShapePath(cxRectInflate(ContentBounds, Shape.Pen.Width / 2));
end;

function TdxSpreadSheetShapeContainerViewInfo.CreateShapePath: TdxGPPath;
begin
  Result := DoCreateShapePath(ContentBounds);
end;

function TdxSpreadSheetShapeContainerViewInfo.DoCreateShapePath(const R: TdxRectF): TdxGPPath;
var
  ARadius: Single;
begin
  Result := TdxGPPath.Create;
  case Shape.ShapeType of
    stRect:
      Result.AddRect(R);
    stEllipse:
      Result.AddEllipse(R);
    stRoundRect:
      begin
        ARadius := GetRadius;
        Result.AddRoundRect(R, ARadius, ARadius);
      end;
  end;
end;

function TdxSpreadSheetShapeContainerViewInfo.DoInitHitTest(const P: TPoint; AHitTest: TdxSpreadSheetCustomHitTest): Boolean;
begin
  Result := inherited DoInitHitTest(P, AHitTest) and
    (ShapePath.IsPointInPath(P) or ShapeOutlinePath.IsPointInPathOutline(P, Shape.Pen));
end;

procedure TdxSpreadSheetShapeContainerViewInfo.DrawBackground(ACanvas: TdxGPCanvas);
begin
  ACanvas.Path(ShapePath, nil, Shape.Brush);
end;

procedure TdxSpreadSheetShapeContainerViewInfo.DrawBorder(ACanvas: TdxGPCanvas);
begin
  ACanvas.Path(ShapeOutlinePath, Shape.Pen, nil);
end;

function TdxSpreadSheetShapeContainerViewInfo.GetBorderWidth: Single;
begin
  Result := Shape.Pen.Width;
end;

function TdxSpreadSheetShapeContainerViewInfo.GetRadius: Single;
begin
  Result := Min(ContentBounds.Width / 6, cxRectHeight(DisplayBounds) / 6);
end;

function TdxSpreadSheetShapeContainerViewInfo.GetRealDrawingBounds: TRect;
begin
  Result := cxRectInflate(inherited GetRealDrawingBounds, 1);
end;

function TdxSpreadSheetShapeContainerViewInfo.GetOwner: TdxSpreadSheetShapeContainer;
begin
  Result := TdxSpreadSheetShapeContainer(inherited Owner);
end;

function TdxSpreadSheetShapeContainerViewInfo.GetShape: TdxSpreadSheetShape;
begin
  Result := Owner.Shape;
end;

function TdxSpreadSheetShapeContainerViewInfo.GetShapeOutlinePath: TdxGPPath;
begin
  if FShapeOutlinePath = nil then
    FShapeOutlinePath := CreateShapeOutlinePath;
  Result := FShapeOutlinePath;
end;

function TdxSpreadSheetShapeContainerViewInfo.GetShapePath: TdxGPPath;
begin
  if FShapePath = nil then
    FShapePath := CreateShapePath;
  Result := FShapePath;
end;

{ TdxSpreadSheetPictureContainerViewInfo }

procedure TdxSpreadSheetPictureContainerViewInfo.DrawContent(ACanvas: TdxGPCanvas);
begin
  if not Picture.Empty then
  begin
    ACanvas.SaveClipRegion;
    try
      ACanvas.SetClipPath(ShapePath, gmIntersect);
      ACanvas.Draw(Picture.ImageHandle.Image, cxRectInflate(ContentBounds, dxRectF(Picture.CropMargins)));
    finally
      ACanvas.RestoreClipRegion;
    end;
  end;
end;

function TdxSpreadSheetPictureContainerViewInfo.GetOwner: TdxSpreadSheetPictureContainer;
begin
  Result := TdxSpreadSheetPictureContainer(inherited Owner);
end;

function TdxSpreadSheetPictureContainerViewInfo.GetPicture: TdxSpreadSheetPicture;
begin
  Result := Owner.Picture;
end;

{ TdxSpreadSheetTableViewCellViewInfo }

procedure TdxSpreadSheetTableViewCellViewInfo.OffsetOrigin(const AOffset: TPoint);
begin
  FTextBounds := cxRectOffset(FTextBounds, AOffset, False);
  FContentBounds := cxRectOffset(FContentBounds, AOffset, False);
  inherited OffsetOrigin(AOffset);
end;

procedure TdxSpreadSheetTableViewCellViewInfo.SetBounds(const AAbsoluteOrigin: TPoint;
  const AAbsoluteBounds: TRect; const AScreenClipRect: TRect);
begin
  inherited SetBounds(AAbsoluteOrigin, AAbsoluteBounds, AScreenClipRect);
  Calculate;
end;

function TdxSpreadSheetTableViewCellViewInfo.GetBorderBounds(ASide: TcxBorder): TRect;
var
  ASize: Integer;
begin
  ASize := dxSpreadSheetBorderStyleThickness[BorderStyle[ASide]];
  case ASide of
    bLeft:
      Result := cxRectSetRight(DisplayBounds, DisplayBounds.Left + 1 + ASize div 3, ASize);
    bTop:
      Result := cxRectSetBottom(DisplayBounds, DisplayBounds.Top + 1 + ASize div 3, ASize);
    bRight:
      Result := cxRectSetLeft(DisplayBounds, DisplayBounds.Right + 1 - ASize + ASize div 3, ASize);
    bBottom:
      Result := cxRectSetTop(DisplayBounds, DisplayBounds.Bottom + 1 - ASize + ASize div 3, ASize);
  end;

  if ASide in [bLeft, bRight] then
  begin
    if ASize >= 2 then
    begin
      if BorderStyle[bTop] <> sscbsNone then
        Inc(Result.Top, 1 + ASize div 3 - dxSpreadSheetBorderStyleThickness[BorderStyle[bTop]]);
      if BorderStyle[bBottom] <> sscbsNone then
        Inc(Result.Bottom, dxSpreadSheetBorderStyleThickness[BorderStyle[bBottom]] - 1);
    end
    else
      Inc(Result.Top, Byte(BorderStyle[bTop] <> sscbsNone));
  end
  else
  begin
    if ASize >= 2 then
    begin
      if BorderStyle[bLeft] <> sscbsNone then
        Inc(Result.Left, 1 + ASize div 3 - dxSpreadSheetBorderStyleThickness[BorderStyle[bLeft]]);
      if BorderStyle[bRight] <> sscbsNone then
        Inc(Result.Right, dxSpreadSheetBorderStyleThickness[BorderStyle[bRight]] - 1);
    end
    else
      Inc(Result.Right);
  end;
end;

function TdxSpreadSheetTableViewCellViewInfo.GetTextOutFormat: Integer;
begin
  Result := cxMakeFormat(AlignHorz, AlignVert);
  if Multiline then
    Result := Result or CXTO_WORDBREAK or CXTO_PREVENT_TOP_EXCEED;
  if FIsNumeric then
    Result := Result or CXTO_PREVENT_LEFT_EXCEED;
end;

function TdxSpreadSheetTableViewCellViewInfo.HasBorder(ASide: TcxBorder): Boolean;
begin
  Result := FBorderStyle[ASide] <> sscbsNone;
end;

function TdxSpreadSheetTableViewCellViewInfo.HasValue: Boolean;
begin
  InitDrawValue;
  Result := (DisplayText <> '') or (FMergedCell <> nil);
end;

procedure TdxSpreadSheetTableViewCellViewInfo.RemoveBorder(ASide: TcxBorder);
begin
  FBorderStyle[ASide] := sscbsNone;
  FBorderColor[ASide] := clNone;
  Calculated := False;
end;

procedure TdxSpreadSheetTableViewCellViewInfo.SetBorderStyle(
  ASide: TcxBorder; AStyle: TdxSpreadSheetCellBorderStyle; AColor: TColor);
begin
  FBorderStyle[ASide] := AStyle;
  FBorderColor[ASide] := AColor;
  Calculated := False;
end;

procedure TdxSpreadSheetTableViewCellViewInfo.Initialize(
  ACell: TdxSpreadSheetCell; AStyle: TdxSpreadSheetCellStyle; ARowIndex, AColumnIndex: Integer);
begin
  FCell := ACell;
  FStyle := AStyle;
  FFillStyle := Style.Brush.Style;
  FRow := ARowIndex;
  FColumn := AColumnIndex;
  FMergedCell := Owner.GetMergedCell(ARowIndex, AColumnIndex);
  InitSelection;
end;

procedure TdxSpreadSheetTableViewCellViewInfo.InitDrawValue;
const
  GeneralAlignToTextAlign: array[TdxSpreadSheetCellDataType] of TcxTextAlignX =
    (taLeft, taCenterX, taCenterX, taRight, taRight, taRight, taRight, taLeft, taLeft);
  HorzAlignToTextAlignH: array[TdxSpreadSheetDataAlignHorz] of TcxTextAlignX =
    (taLeft, taLeft, taCenterX, taRight, taLeft, taJustifyX, taDistributeX);
  VertAlignToTextAlignY: array[TdxSpreadSheetDataAlignVert] of TcxTextAlignY =
    (taTop, taCenterY, taBottom, taDistributeY, taDistributeY);
var
  ACell: TdxSpreadSheetCell;
  R: TRect;
begin
  if DrawValueInitialized then
    Exit;

  Calculate;
  FContentBounds := ContentRect(DisplayBounds);

  if IsMerged then
  begin
    ACell := MergedCell.ActiveCell;
    FContentBounds := ContentRect(Owner.GetAreaBounds(Row, Column, MergedCell.Area, DisplayBounds));
  end
  else
    ACell := Cell;

  if ACell = nil then
  begin
    FTextColor := cxGetActualColor(Style.Font.Color, View.ViewInfo.ContentParams.TextColor);
    FTextBounds := FContentBounds;
    FIsNumeric := False;
  end
  else
  begin
    FFontSize := ACell.Style.Font.Size;
    FTextColor := cxGetActualColor(ACell.DisplayValue.Color, ACell.Style.Font.Color);
    FTextColor := cxGetActualColor(FTextColor, View.ViewInfo.ContentParams.TextColor);
    FIsNumeric := ACell.IsNumericValue;
    FMultiline := ACell.IsMultiline;
    FAlignHorz := HorzAlignToTextAlignH[ACell.Style.AlignHorz];
    FAlignVert := VertAlignToTextAlignY[ACell.Style.AlignVert];

    R := FContentBounds;
    if ACell.Style.AlignHorz in [ssahLeft, ssahDistributed] then
      Inc(R.Left, ACell.Style.AlignHorzIndent);
    if ACell.Style.AlignHorz in [ssahRight, ssahDistributed] then
      Dec(R.Right, ACell.Style.AlignHorzIndent);
    FTextBounds := R;

    if ACell.Style.AlignHorz = ssahGeneral then
      FAlignHorz := GeneralAlignToTextAlign[ACell.ActualDataType]
    else
      if ACell.Style.AlignHorz = ssahFill then
      begin
        AlignVert := taBottom;
        FMultiline := False;
      end;

    FDisplayText := ACell.DisplayText;
    if FDisplayText <> '' then
    begin
      CalculateTextBounds(R, ACell);
      if not IsMerged then
      begin
        FContentBounds.Left := Min(FTextBounds.Left, ContentBounds.Left);
        FContentBounds.Right := Max(FTextBounds.Right, ContentBounds.Right);
      end;
    end;
  end;

  DrawValueInitialized := True;
end;

procedure TdxSpreadSheetTableViewCellViewInfo.InitSelection;
var
  ASelected: Boolean;
begin
  if not View.ViewInfo.CanDrawCellSelection then
  begin
    FFocused := False;
    FSelected := False;
    Exit;
  end;
  View.Selection.GetState(Row, Column, FFocused, FSelected);
  if IsMerged then
  begin
    View.Selection.GetState(MergedCell.Area.Top, MergedCell.Area.Left, FFocused, ASelected);
    if Focused then
      FSelected := False;
  end;
end;

function TdxSpreadSheetTableViewCellViewInfo.InitHitTest(AHitTest: TdxSpreadSheetCustomHitTest): Boolean;
begin
  Result := inherited InitHitTest(AHitTest);
  AHitTest.SetHitCode(hcCell, Result);
end;

function TdxSpreadSheetTableViewCellViewInfo.IsTextOutOfBounds: Boolean;
var
  R: TRect;
begin
  InitDrawValue;
  R := ContentRect(DisplayBounds);
  Result := not IsMerged and ((ContentBounds.Left < R.Left) or (ContentBounds.Right > R.Right))
end;

function TdxSpreadSheetTableViewCellViewInfo.IsInternalBorder(ASide: TcxBorder): Boolean;
begin
  Result := IsMerged;
  if Result then
    case ASide of
      bLeft:
        Result := Column <> MergedCell.Area.Left;
      bTop:
        Result := Row <> MergedCell.Area.Top;
      bRight:
        Result := Column <> MergedCell.Area.Right;
      bBottom:
        Result := Row <> MergedCell.Area.Bottom;
    end;
end;

function TdxSpreadSheetTableViewCellViewInfo.IsPermanent: Boolean;
begin
  Result := not View.IsDestroying and IsEditing;
end;

procedure TdxSpreadSheetTableViewCellViewInfo.UpdateState;
var
  APrevFocused: Boolean;
  APrevSelected: Boolean;
begin
  APrevSelected := Selected;
  APrevFocused := Focused;
  InitSelection;
  if APrevFocused or Focused or (Selected <> APrevSelected) then
    InvalidateRect(cxRectInflate(DisplayBounds, MulDiv(3, 100, ZoomFactor)));
end;

function TdxSpreadSheetTableViewCellViewInfo.CanDraw(ACanvas: TcxCanvas; AStage: TdxSpreadSheetDrawingStage): Boolean;
begin
  Result := Visible;
  if Result then
  begin
    InitDrawValue;
    Result := ACanvas.RectVisible(cxRectInflate(DisplayClipRect, dxSpreadSheetBorderStyleThickness[sscbsThick])) or
      ACanvas.RectVisible(ContentBounds);
  end;
end;

procedure TdxSpreadSheetTableViewCellViewInfo.CalculateTextBounds(const R: TRect; ACell: TdxSpreadSheetCell);
var
  AFont: TFont;
  AHelper: TdxAdjustFontSizeHelper;
  APrevTransform: TXForm;
begin
  AFont := Style.Font.Handle.GraphicObject;
  if Style.ShrinkToFit then
  begin
    if ACell.FShrinkToFitCacheContentWidth <> R.Width then
    begin
      if (ACell.FShrinkToFitCacheFontSize <> AFont.Size) or (R.Width < ACell.FShrinkToFitCacheContentWidth) then
      begin
        AHelper := TdxAdjustFontSizeHelper.Create;
        try
          AHelper.ZoomFactor := ZoomFactor;
          AHelper.Font.Assign(AFont);
          AHelper.Calculate(R.Width, FDisplayText);
          ACell.FShrinkToFitCacheFontSize := AHelper.Font.Size;
        finally
          AHelper.Free;
        end;
      end;
      ACell.FShrinkToFitCacheContentWidth := R.Width;
    end;
    FFontSize := ACell.FShrinkToFitCacheFontSize;
  end;

  FTextBounds := R;
  cxScreenCanvas.Font := AFont;
  cxScreenCanvas.Font.Size := FontSize;
  dxSetZoomFactor(cxScreenCanvas, ZoomFactor, APrevTransform);
  cxTextOut(cxScreenCanvas.Handle, FDisplayText, FTextBounds, GetTextOutFormat or CXTO_CALCRECT);
  SetWorldTransform(cxScreenCanvas.Handle, APrevTransform);
  cxScreenCanvas.Dormant;

  case AlignHorz of
    taLeft:
      FTextBounds := cxRectSetLeft(FTextBounds, R.Left);
    taRight:
      FTextBounds := cxRectSetRight(FTextBounds, R.Right);
    taCenterX:
      FTextBounds := cxRectSetLeft(FTextBounds, cxRectCenter(R).X - FTextBounds.Width div 2, FTextBounds.Width);
  else
    FTextBounds := cxRectSetXPos(FTextBounds, R.Left, R.Right);
  end;

  if AlignVert = taCenterY then
    FTextBounds := cxRectSetTop(FTextBounds, cxRectCenter(R).Y - FTextBounds.Height div 2, FTextBounds.Height);

  if FIsNumeric then
    FTextBounds.Left := Max(FTextBounds.Left, FContentBounds.Left);
end;

function TdxSpreadSheetTableViewCellViewInfo.ContentRect(const R: TRect): TRect;
begin
  Result := cxRectContent(R, TdxSpreadSheetCell.GetContentOffsets(FBorderStyle[bBottom]));
end;

procedure TdxSpreadSheetTableViewCellViewInfo.DoCalculate;
begin
  FBackgroundColor := dxSpreadSheetGetColorDefault(Style.Brush.BackgroundColor, View.ViewInfo.ContentParams.Color);
  FForegroundColor := dxSpreadSheetGetColorDefault(Style.Brush.ForegroundColor, View.ViewInfo.ContentParams.TextColor);
  inherited DoCalculate;
  FVisible := Visible or cxRectIntersect(DisplayClipRect, TextBounds);
end;

function TdxSpreadSheetTableViewCellViewInfo.DoCustomDraw(ACanvas: TcxCanvas): Boolean;
begin
  ACanvas.Font := Style.Font.Handle.GraphicObject;
  ACanvas.Font.Size := FontSize;
  ACanvas.Font.Color := TextColor;
  ACanvas.Brush.Color := BackgroundColor;
  SpreadSheet.DoCustomDrawTableViewCell(View, ACanvas, Self, Result);
end;

procedure TdxSpreadSheetTableViewCellViewInfo.DoDraw(ACanvas: TcxCanvas);
begin
  if DrawingStage = dsFirst then
    DrawBackground(ACanvas);
  DrawBorders(ACanvas);
  if DrawingStage = dsFirst then
  begin
    if Selected and not Focused then
      DrawSelection(ACanvas);
  end
  else
    DrawValue(ACanvas);
end;

procedure TdxSpreadSheetTableViewCellViewInfo.DrawBackground(ACanvas: TcxCanvas);
begin
  dxSpreadSheetDrawBackground(ACanvas, DisplayBounds, ACanvas.Brush.Color, FForegroundColor, FillStyle);
end;

procedure TdxSpreadSheetTableViewCellViewInfo.DrawBorder(ACanvas: TcxCanvas; ASide: TcxBorder);
begin
  if HasBorder(ASide) and ((DrawingStage <> dsFirst) = (dxSpreadSheetBorderStyleThickness[BorderStyle[ASide]] >= 2)) then
  begin
    dxSpreadSheetDrawBorder(ACanvas, GetBorderBounds(ASide), BorderColor[ASide],
      BackgroundColor, BorderStyle[ASide], ASide in [bTop, bBottom]);
  end;
end;

procedure TdxSpreadSheetTableViewCellViewInfo.DrawBorders(ACanvas: TcxCanvas);
var
  APrevForm: TXForm;
  ASide: TcxBorder;
  AStoredBounds: TRect;
begin
  dxSetZoomFactor(ACanvas, 100, APrevForm);
  try
    AStoredBounds := FDisplayBounds;
    if APrevForm.eM11 <> 1 then
      FDisplayBounds := cxRectScale(FDisplayBounds, Round(APrevForm.eM11 * 100), 100);
    for ASide := Low(TcxBorder) to High(TcxBorder) do
      DrawBorder(ACanvas, ASide);
    FDisplayBounds := AStoredBounds;
  finally
    SetWorldTransform(ACanvas.Handle, APrevForm);
  end;
end;

procedure TdxSpreadSheetTableViewCellViewInfo.DrawSelection(ACanvas: TcxCanvas);
begin
  dxGpFillRect(ACanvas.Handle, DisplayBounds, View.ViewInfo.SelectionParams.Color, 127);
end;

procedure TdxSpreadSheetTableViewCellViewInfo.DrawValue(ACanvas: TcxCanvas);
var
  R: TRect;
  ACell: TdxSpreadSheetCell;
begin
  if IsMerged then
    ACell := MergedCell.ActiveCell
  else
    ACell := Cell;

  if View.IsEditing and (View.EditingController.Cell = ACell) then
    Exit;

  if not DrawValueInitialized then
    InitDrawValue;
  if (DisplayText = '') or not cxRectIntersect(R, ContentBounds, FScreenClipRect) then
    Exit;
  if ACanvas.RectVisible(R) then
  begin
    ACanvas.SaveClipRegion;
    try
      ACanvas.IntersectClipRect(R);
      cxTextOut(ACanvas.Handle, DisplayText, FTextBounds, GetTextOutFormat, nil, 0, 0, 0, ACanvas.Font.Color);
    finally
      ACanvas.RestoreClipRegion;
      ACanvas.ExcludeClipRect(R);
    end;
  end;
end;

function TdxSpreadSheetTableViewCellViewInfo.GetBorderColor(ASide: TcxBorder): TColor;
begin
  Result := FBorderColor[ASide];
end;

function TdxSpreadSheetTableViewCellViewInfo.GetBorderStyle(ASide: TcxBorder): TdxSpreadSheetCellBorderStyle;
begin
  Result := FBorderStyle[ASide];
end;

function TdxSpreadSheetTableViewCellViewInfo.GetIsEditing: Boolean;
begin
  Result := View.IsEditing and (Self = View.EditingController.CellViewInfo);
end;

function TdxSpreadSheetTableViewCellViewInfo.GetIsMerged: Boolean;
begin
  Result := FMergedCell <> nil;
end;

{ TdxSpreadSheetTableViewFrozenPaneSeparatorCellViewInfo }

function TdxSpreadSheetTableViewFrozenPaneSeparatorCellViewInfo.DoCustomDraw(ACanvas: TcxCanvas): Boolean;
begin
  ACanvas.SetBrushColor(Color);
  Result := inherited DoCustomDraw(ACanvas);
end;

procedure TdxSpreadSheetTableViewFrozenPaneSeparatorCellViewInfo.DoDraw(ACanvas: TcxCanvas);
var
  R: TRect;
  APrevForm: TXForm;
begin
  dxSetZoomFactor(ACanvas, 100, APrevForm);
  try
    R := DisplayBounds;
    if APrevForm.eM11 <> 1 then
    begin
      R := cxRectScale(DisplayBounds, Round(APrevForm.eM11 * 100), 100);
      if cxRectHeight(R) <= 0 then
        R := cxRectSetHeight(R, 1);
      if cxRectWidth(R) <= 0 then
        R := cxRectSetWidth(R, 1);
    end;
    ACanvas.FillRect(R);
    ACanvas.ExcludeClipRect(R);
  finally
    SetWorldTransform(ACanvas.Handle, APrevForm);
  end;
end;

function TdxSpreadSheetTableViewFrozenPaneSeparatorCellViewInfo.InitHitTest(AHitTest: TdxSpreadSheetCustomHitTest): Boolean;
begin
  Result := inherited InitHitTest(AHitTest);
  AHitTest.SetHitCode(hcFrozenPaneSeparator, Result);
end;

{ TdxSpreadSheetPageControlHitTest }

procedure TdxSpreadSheetPageControlHitTest.Calculate(const AHitPoint: TPoint);
begin
  inherited Calculate(AHitPoint);
  PageControl.ViewInfo.InitHitTest(Self);
end;

function TdxSpreadSheetPageControlHitTest.GetPageControl: TdxSpreadSheetPageControl;
begin
  Result := TdxSpreadSheetPageControl(Owner);
end;

{ TdxSpreadSheetPageControlController }

function TdxSpreadSheetPageControlController.GetHitTest: TdxSpreadSheetCustomHitTest;
begin
  Result := PageControl.HitTest;
end;

procedure TdxSpreadSheetPageControlController.DoMouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  UpdateStates;
  if HitTest.HitAtPageTab then
    TdxSpreadSheetPageControlTabCellViewInfo(HitTest.HitObject).View.Active := True
  else
    if HitTest.HitAtButton then
    begin
      if Button = mbLeft then
        TdxSpreadSheetPageControlButtonCellViewInfo(HitTest.HitObject).DoClick;
    end;
end;

procedure TdxSpreadSheetPageControlController.DoMouseMove(Shift: TShiftState; X, Y: Integer);
begin
  UpdateStates;
end;

procedure TdxSpreadSheetPageControlController.DoMouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  UpdateStates;
end;

procedure TdxSpreadSheetPageControlController.UpdateStates;
begin
  PageControl.ViewInfo.Cells.ForEach(UpdateCellState, nil);
end;

function TdxSpreadSheetPageControlController.GetPageControlHitTest: TdxSpreadSheetPageControlHitTest;
begin
  Result := TdxSpreadSheetPageControlHitTest(inherited HitTest);
end;

function TdxSpreadSheetPageControlController.GetSpreadSheet: TdxCustomSpreadSheet;
begin
  Result := PageControl.SpreadSheet;
end;

function TdxSpreadSheetPageControlController.GetPageControl: TdxSpreadSheetPageControl;
begin
  Result := TdxSpreadSheetPageControl(Owner);
end;

procedure TdxSpreadSheetPageControlController.SetPressedObject(AValue: TdxSpreadSheetCellViewInfo);
begin
  if AValue <> FPressedObject then
  begin
    FPressedObject := AValue;
    UpdateStates;
  end;
end;

function TdxSpreadSheetPageControlController.UpdateCellState(
  ACell: TdxSpreadSheetCellViewInfo; AData: TObject): Boolean;
begin
  Result := True;
  if ACell is TdxSpreadSheetPageControlBackgroundCellViewInfo then
    TdxSpreadSheetPageControlBackgroundCellViewInfo(ACell).UpdateState;
end;

{ TdxSpreadSheetPageControlBackgroundCellViewInfo }

constructor TdxSpreadSheetPageControlBackgroundCellViewInfo.Create(AOwner: TObject);
begin
  inherited Create(AOwner);
  State := cxbsNormal;
  InitViewParams;
end;

destructor TdxSpreadSheetPageControlBackgroundCellViewInfo.Destroy;
begin
  EndMouseTracking(Self);
  inherited Destroy;
end;

function TdxSpreadSheetPageControlBackgroundCellViewInfo.DoCustomDraw(ACanvas: TcxCanvas): Boolean;
begin
  Result := False;
  ACanvas.Font.Assign(ViewParams.Font);
  ACanvas.Font.Color := ViewParams.TextColor;
  ACanvas.Brush.Color := ViewParams.Color;
  if State = cxbsPressed then
    ACanvas.Font.Style := ACanvas.Font.Style + [fsBold]
  else
    ACanvas.Font.Style := ACanvas.Font.Style - [fsBold];
end;

procedure TdxSpreadSheetPageControlBackgroundCellViewInfo.DoDraw(ACanvas: TcxCanvas);
begin
  LookAndFeelPainter.DrawScrollBarBackground(ACanvas, DisplayBounds, True);
end;

function TdxSpreadSheetPageControlBackgroundCellViewInfo.GetHitCode: Integer;
begin
  Result := hcBackground;
end;

function TdxSpreadSheetPageControlBackgroundCellViewInfo.GetSpreadSheet: TdxCustomSpreadSheet;
begin
  Result := PageControl.SpreadSheet;
end;

function TdxSpreadSheetPageControlBackgroundCellViewInfo.GetState: TcxButtonState;
begin
  Result := FState;
end;

function TdxSpreadSheetPageControlBackgroundCellViewInfo.InitHitTest(AHitTest: TdxSpreadSheetCustomHitTest): Boolean;
begin
  Result := inherited InitHitTest(AHitTest);
  AHitTest.SetHitCode(GetHitCode, Result);
  if not Result then Exit;

  if GetHitCode = hcPageTab then
    Result := Result;

  if GetHitCode = hcBackground then
    Result := False
  else
    AHitTest.SetHitCode(hcBackground, False);
end;

procedure TdxSpreadSheetPageControlBackgroundCellViewInfo.InitViewParams;
begin
  FViewParams := PageControl.ViewInfo.ViewParams;
end;

procedure TdxSpreadSheetPageControlBackgroundCellViewInfo.MouseLeave;
begin
  State := cxbsNormal;
end;

procedure TdxSpreadSheetPageControlBackgroundCellViewInfo.UpdateState;
begin
  if (PageControl.Controller.PressedObject = Self) and (PageControl.HitTest.HitObject = Self) then
    State := cxbsPressed
  else
    if (PageControl.Controller.PressedObject = nil) and (PageControl.HitTest.HitObject = Self) then
      State := cxbsHot
    else
      State := cxbsNormal;
end;

function TdxSpreadSheetPageControlBackgroundCellViewInfo.GetPageControl: TdxSpreadSheetPageControl;
begin
  Result := TdxSpreadSheetPageControlViewInfo(Owner).PageControl;
end;

procedure TdxSpreadSheetPageControlBackgroundCellViewInfo.SetState(AValue: TcxButtonState);
begin
  if (AValue <> State) and (State <> cxbsDisabled) then
  begin
    FState := AValue;
    if (State in [cxbsHot, cxbsPressed]) and SpreadSheet.HandleAllocated then
      BeginMouseTracking(SpreadSheet, DisplayClipRect, Self)
    else
      EndMouseTracking(Self);
    if State = AValue then
      Invalidate;
  end;
end;

{ TdxSpreadSheetPageControlButtonCellViewInfo }

procedure TdxSpreadSheetPageControlButtonCellViewInfo.DoClick;
begin
  if State = cxbsDisabled then Exit;
  case ButtonType of
    sspcbFirst:
      PageControl.FirstVisiblePageIndex := 0;
    sspcbPrev:
      PageControl.FirstVisiblePageIndex := PageControl.FirstVisiblePageIndex - 1;
    sspcbNext:
      PageControl.FirstVisiblePageIndex := PageControl.FirstVisiblePageIndex + 1;
    sspcbLast:
      PageControl.FirstVisiblePageIndex := PageControl.ViewInfo.Pages.Count - 1;
    sspcbNew:
      SpreadSheet.AddSheet('').Active := True;
  end;
end;

procedure TdxSpreadSheetPageControlButtonCellViewInfo.DoDraw(ACanvas: TcxCanvas);
const
  GlyphIndexMap: array[TdxSpreadSheetPageControlButton] of Integer = (0, 2, 3, 5, 7);
var
  AGlyphRect: TRect;
begin
  LookAndFeelPainter.DrawNavigatorButton(ACanvas, DisplayBounds, State, ViewParams.Color);
  LookAndFeelPainter.DrawNavigatorBorder(ACanvas, DisplayBounds, False);

  ACanvas.SaveState;
  try
    AGlyphRect := cxRectCenter(DisplayBounds, LookAndFeelPainter.NavigatorButtonGlyphSize);
    if State = cxbsPressed then
      AGlyphRect := cxRectOffset(AGlyphRect, LookAndFeelPainter.NavigatorButtonPressedGlyphOffset);

    LookAndFeelPainter.DrawNavigatorButtonGlyph(ACanvas, NavigatorImages,
      GlyphIndexMap[ButtonType], AGlyphRect, State <> cxbsDisabled, False);
  finally
    ACanvas.RestoreState;
  end;
end;

function TdxSpreadSheetPageControlButtonCellViewInfo.GetHitCode: Integer;
begin
  Result := hcButton;
end;

procedure TdxSpreadSheetPageControlButtonCellViewInfo.Initialize(
  const ABounds, AClipRect: TRect; AType: TdxSpreadSheetPageControlButton);
begin
  SetBounds(ABounds, AClipRect);
  FButtonType := AType;
  State := cxbsNormal;
  Calculate;
end;

procedure TdxSpreadSheetPageControlButtonCellViewInfo.UpdateState;
begin
  inherited UpdateState;
  case ButtonType of
    sspcbNew:
      if SpreadSheet.OptionsBehavior.Protected then
        State := cxbsDisabled;

    sspcbFirst, sspcbPrev:
      if PageControl.FirstVisiblePageIndex = 0 then
        State := cxbsDisabled;

    sspcbNext, sspcbLast:
      if not PageControl.ViewInfo.HasInvisibleTab then
        State := cxbsDisabled;
  end;
end;

{ TdxSpreadSheetPageControlTabDragAndDropObject }

constructor TdxSpreadSheetPageControlTabDragAndDropObject.Create(AControl: TcxControl);
begin
  inherited Create(AControl);
  Arrows := TcxPlaceArrows.CreateArrows(PageControl.DropArrowColor, clBtnText);
  ScrollTimer := TcxTimer.Create(AControl);
  ScrollTimer.Interval := cxScrollWidthDragInterval;
  ScrollTimer.Enabled := False;
  ScrollTimer.OnTimer := ScrollTimerHandler;
  View := HitTestView;
end;

destructor TdxSpreadSheetPageControlTabDragAndDropObject.Destroy;
begin
  FreeAndNil(Arrows);
  FreeAndNil(ScrollTimer);
  inherited Destroy;
end;

procedure TdxSpreadSheetPageControlTabDragAndDropObject.DragAndDrop(const P: TPoint; var Accepted: Boolean);
const
  BordersMap: array[Boolean] of TcxBorder = (bLeft, bRight);
var
  ASide: TcxBorder;
  R: TRect;
begin
  DropIndex := -1;
  ASide := bLeft;
  ChangeMousePos(P);

  if PtInRect(LeftScrollArea, P) then
    InitScrollTimer(-1)
  else
    if PtInRect(RightScrollArea, P) then
      InitScrollTimer(1)
    else
      ScrollTimer.Enabled := False;

  R := cxInvalidRect;
  if HitTest.HitAtPageTab then
    R := TdxSpreadSheetCellViewInfo(HitTest.HitObject).DisplayClipRect;

  if PtInRect(R, P) then
  begin
    ASide := BordersMap[cxRectCenter(TdxSpreadSheetCellViewInfo(HitTest.HitObject).DisplayClipRect).X < P.X];
    R := cxRectOffset(R, PageControl.SpreadSheet.ClientToScreen(cxNullPoint));
    R.Right := R.Right - Byte(ASide = bRight);
    DropIndex := HitTestView.Index + Byte(ASide = bRight);
    if DropIndex > View.Index then
      Dec(DropIndex);
  end;

  Accepted := (DropIndex >= 0) and (DropIndex <> View.Index) and not View.SpreadSheet.OptionsBehavior.Protected;
  if Accepted then
    Arrows.MoveTo(R, ASide)
  else
    Arrows.Hide;
  inherited DragAndDrop(P, Accepted);
end;

procedure TdxSpreadSheetPageControlTabDragAndDropObject.EndDragAndDrop(Accepted: Boolean);
begin
  inherited EndDragAndDrop(Accepted);
  if Accepted and (DropIndex >= 0) then
    View.Index := DropIndex;
end;

function TdxSpreadSheetPageControlTabDragAndDropObject.GetDragAndDropCursor(Accepted: Boolean): TCursor;
const
  Cursors: array[Boolean] of TCursor = (crDrag, crNoDrop);
begin
  Result := Cursors[View.SpreadSheet.OptionsBehavior.Protected];
end;

procedure TdxSpreadSheetPageControlTabDragAndDropObject.InitScrollTimer(ADelta: Integer);
begin
  ScrollTimer.Enabled := True;
  ScrollDelta := ADelta;
end;

procedure TdxSpreadSheetPageControlTabDragAndDropObject.ScrollTimerHandler(ASender: TObject);
var
  AAccepted: Boolean;
begin
  PageControl.FirstVisiblePageIndex := PageControl.FirstVisiblePageIndex  + ScrollDelta;
  DragAndDrop(CurMousePos, AAccepted);
end;

function TdxSpreadSheetPageControlTabDragAndDropObject.GetLeftScrollArea: TRect;
begin
  Result := PageControl.ViewInfo.LeftScrollArea;
end;

function TdxSpreadSheetPageControlTabDragAndDropObject.GetHitTest: TdxSpreadSheetPageControlHitTest;
begin
  Result := PageControl.HitTest;
end;

function TdxSpreadSheetPageControlTabDragAndDropObject.GetHitTestView: TdxSpreadSheetCustomView;
begin
   Result := nil;
   if HitTest.HitAtPageTab then
     Result := TdxSpreadSheetPageControlTabCellViewInfo(HitTest.HitObject).View;
end;

function TdxSpreadSheetPageControlTabDragAndDropObject.GetPageControl: TdxSpreadSheetPageControl;
begin
  Result := TdxCustomSpreadSheet(Control).PageControl;
end;

function TdxSpreadSheetPageControlTabDragAndDropObject.GetRightScrollArea: TRect;
begin
  Result := PageControl.ViewInfo.RightScrollArea;
end;

{ TdxSpreadSheetPageControlTabCellViewInfo }

function TdxSpreadSheetPageControlTabCellViewInfo.CanDraw(
  ACanvas: TcxCanvas; AStage: TdxSpreadSheetDrawingStage): Boolean;
begin
  Result := Visible and ACanvas.RectVisible(DisplayBounds);
end;

procedure TdxSpreadSheetPageControlTabCellViewInfo.DoDraw(ACanvas: TcxCanvas);
var
  ATextRect: TRect;
begin
  if (DrawingStage = dsSecond) = (State = cxbsPressed) then
  begin
    LookAndFeelPainter.DrawNavigatorButton(ACanvas, DisplayBounds, State, ViewParams.Color);
    LookAndFeelPainter.DrawNavigatorBorder(ACanvas, DisplayBounds, False);

    ATextRect := cxRectContent(DisplayBounds, GetTextMargins);
    cxTextOut(ACanvas.Handle, View.Caption, ATextRect,
      cxMakeFormat(taLeft, taBottom), nil, 0, 0, 0, ColorToRGB(ViewParams.TextColor));
  end;
end;

function TdxSpreadSheetPageControlTabCellViewInfo.GetDragAndDropObjectClass(
  AHitTest: TdxSpreadSheetCustomHitTest): TcxDragAndDropObjectClass;
begin
  if not SpreadSheet.OptionsBehavior.Protected then
    Result := TdxSpreadSheetPageControlTabDragAndDropObject
  else
    Result := nil;
end;

function TdxSpreadSheetPageControlTabCellViewInfo.GetHitCode: Integer;
begin
  Result := hcPageTab;
end;

function TdxSpreadSheetPageControlTabCellViewInfo.GetPopupMenuClass(AHitTest: TdxSpreadSheetCustomHitTest): TComponentClass;
begin
  Result := TdxSpreadSheetBuiltInPageControlTabPopupMenu;
end;

function TdxSpreadSheetPageControlTabCellViewInfo.GetState: TcxButtonState;
begin
  Result := inherited GetState;
  if View = SpreadSheet.ActiveSheet then
    Result := cxbsPressed
  else
    if (Result = cxbsHot) and (SpreadSheet.DragAndDropState <> ddsNone) then
      Result := cxbsNormal;
end;

class function TdxSpreadSheetPageControlTabCellViewInfo.GetTextMargins: TRect;
begin
  Result := cxRectBounds(cxTextOffset * 5, cxTextOffset * 2, 0, 0);
end;

procedure TdxSpreadSheetPageControlTabCellViewInfo.Initialize(
  const ABounds, AClipRect: TRect; AView: TdxSpreadSheetCustomView);
begin
  FView := AView;
  cxScreenCanvas.Font := ViewParams.Font;
  if View.Active then
    cxScreenCanvas.Font.Style := cxScreenCanvas.Font.Style + [fsBold];
  SetBounds(cxRectSetWidth(ABounds, cxScreenCanvas.TextWidth(View.Caption) + cxMarginsWidth(GetTextMargins)), AClipRect);
  cxScreenCanvas.Dormant;
  UpdateState;
end;

{ TdxSpreadSheetPageControlSplitterCellViewInfo }

function TdxSpreadSheetPageControlSplitterCellViewInfo.GetCursor(AHitTest: TdxSpreadSheetCustomHitTest): TCursor;
begin
  if AHitTest.GetHitCode(hcSplitter) then
    Result := crcxHorzSize
  else
    Result := crDefault;
end;

procedure TdxSpreadSheetPageControlSplitterCellViewInfo.DoDraw(ACanvas: TcxCanvas);
begin
  LookAndFeelPainter.DrawScrollBarSplitter(ACanvas, DisplayBounds, State);
end;

function TdxSpreadSheetPageControlSplitterCellViewInfo.ExcludeFromClipRgn(AStage: TdxSpreadSheetDrawingStage): Boolean;
begin
  Result := True;
end;

function TdxSpreadSheetPageControlSplitterCellViewInfo.GetDragAndDropObjectClass(AHitTest: TdxSpreadSheetCustomHitTest): TcxDragAndDropObjectClass;
begin
  Result := TdxSpreadSheetPageControlSplitterDragAndDropObject;
end;

function TdxSpreadSheetPageControlSplitterCellViewInfo.GetHitCode: Integer;
begin
  Result := hcSplitter;
end;

function TdxSpreadSheetPageControlSplitterCellViewInfo.GetState: TcxButtonState;
begin
  Result := inherited GetState;
  if (SpreadSheet.DragAndDropState = ddsInProcess) and
    SpreadSheet.DragAndDropObject.InheritsFrom(TdxSpreadSheetPageControlSplitterDragAndDropObject)
  then
    Result := cxbsHot;
end;

function TdxSpreadSheetPageControlSplitterCellViewInfo.MeasureWidth: Integer;
begin
  Result := SpreadSheet.LookAndFeelPainter.GetSplitterSize(True).cx;
  if Result = 0 then
    Result := 17;
  Result := MulDiv(Result, 2, 3);
end;

{ TdxSpreadSheetPageControlSplitterDragAndDropObject }

procedure TdxSpreadSheetPageControlSplitterDragAndDropObject.BeginDragAndDrop;
begin
  StartPos := GetMouseCursorPos;
  StartWidth := cxRectWidth(PageControl.ViewInfo.Bounds);
end;

procedure TdxSpreadSheetPageControlSplitterDragAndDropObject.DragAndDrop(const P: TPoint; var Accepted: Boolean);
begin
  PageControl.Width := StartWidth + (GetMouseCursorPos.X - StartPos.X);
end;

procedure TdxSpreadSheetPageControlSplitterDragAndDropObject.EndDragAndDrop(Accepted: Boolean);
begin
  PageControl.Invalidate;
end;

function TdxSpreadSheetPageControlSplitterDragAndDropObject.GetImmediateStart: Boolean;
begin
  Result := True;
end;

function TdxSpreadSheetPageControlSplitterDragAndDropObject.GetPageControl: TdxSpreadSheetPageControl;
begin
  Result := TdxCustomSpreadSheet(Control).PageControl;
end;

{ TdxSpreadSheetPageControlViewInfo }

constructor TdxSpreadSheetPageControlViewInfo.Create(AOwner: TdxSpreadSheetPageControl);
begin
  FPageControl := AOwner;
  FCells := TdxSpreadSheetViewInfoCellsList.Create;
  Pages := TList<TdxSpreadSheetCustomView>.Create;
end;

destructor TdxSpreadSheetPageControlViewInfo.Destroy;
begin
  FreeAndNil(FCells);
  FreeAndNil(Pages);
  inherited Destroy;
end;

procedure TdxSpreadSheetPageControlViewInfo.Calculate;
var
  I: Integer;
begin
  Cells.Clear;
  Pages.Clear;
  PageControl.HitTest.Clear;
  InitializeViewParams;
  for I := 0 to SpreadSheet.VisibleSheetCount - 1 do
    Pages.Add(SpreadSheet.VisibleSheets[I]);
  Bounds := SpreadSheet.ClientBounds;
  Bounds := cxRectBounds(Bounds.Left, Bounds.Bottom, MeasureWidth, MeasureHeight);
  FLeftScrollArea := cxInvalidRect;
  FRightScrollArea := cxInvalidRect;
  AddCells;
  PageControl.HitTest.Recalculate;
end;

procedure TdxSpreadSheetPageControlViewInfo.Draw(ACanvas: TcxCanvas);
begin
  ACanvas.SaveClipRegion;
  try
    ACanvas.IntersectClipRect(Bounds);
    Cells.Draw(ACanvas);
  finally
    ACanvas.RestoreClipRegion;
    ACanvas.ExcludeClipRect(Bounds);
  end;
end;

procedure TdxSpreadSheetPageControlViewInfo.InitHitTest(AHitTest: TdxSpreadSheetCustomHitTest);
begin
  if PtInRect(Bounds, AHitTest.ActualHitPoint) then
    Cells.CalculateHitTest(AHitTest);
end;

function TdxSpreadSheetPageControlViewInfo.MeasureHeight: Integer;
begin
  Result := LookAndFeelPainter.SizeGripSize.cy;
  if PageControl.Visible then
  begin
    Result := Max(Result, LookAndFeelPainter.NavigatorButtonMinSize.cy);

    InitializeViewParams;
    cxScreenCanvas.Font := ViewParams.Font;
    cxScreenCanvas.Font.Style := cxScreenCanvas.Font.Style + [fsBold];
    Result := Max(Result, cxScreenCanvas.TextHeight(dxMeasurePattern) +
      cxMarginsHeight(TdxSpreadSheetPageControlTabCellViewInfo.GetTextMargins));
    cxScreenCanvas.Dormant;
  end;
end;

function TdxSpreadSheetPageControlViewInfo.MeasureWidth: Integer;
var
  AWidth: Integer;
begin
  if not PageControl.Visible then
  begin
    Result := 0;
    Exit;
  end;
  Result := cxRectWidth(SpreadSheet.ClientBounds);
  AWidth := PageControl.Width;
  if SpreadSheet.HScrollBarVisible then
  begin
    if AWidth = 0 then
      Result := MulDiv(Result, 2, 3)
    else
      Result := Min(AWidth, Result - LookAndFeelPainter.SizeGripSize.cx * 4);
  end;
end;

procedure TdxSpreadSheetPageControlViewInfo.AddCells;
var
  R: TRect;
  I: Integer;
  AButton: TdxSpreadSheetPageControlButton;
  ABackground: TdxSpreadSheetPageControlBackgroundCellViewInfo;
  ASplitter: TdxSpreadSheetPageControlSplitterCellViewInfo;
begin
  ABackground := TdxSpreadSheetPageControlBackgroundCellViewInfo.Create(Self);
  ABackground.SetBounds(Bounds, Bounds);
  FClipRect := Bounds;
  Cells.Add(ABackground);

  if SpreadSheet.HScrollBarVisible then
  begin
    ASplitter := TdxSpreadSheetPageControlSplitterCellViewInfo.Create(Self);
    ASplitter.SetBounds(cxRectSetRight(Bounds, Bounds.Right, ASplitter.MeasureWidth), Bounds);
    Cells.Add(ASplitter);
    FClipRect.Right := ASplitter.DisplayBounds.Left;
  end;

  FHasInvisibleTab := False;
  R := cxRectSetWidth(Bounds, 0);
  for AButton := sspcbFirst to sspcbLast do
    AddButton(R, AButton);

  if PageControl.FirstVisiblePageIndex > 0 then
    FLeftScrollArea := cxRectSetXPos(R, Bounds.Left, R.Right + dxSpreadSheetResizeDelta * 3);

  for I := PageControl.FirstVisiblePageIndex to Pages.Count - 1 do
  begin
    AddPageTab(R, Pages[I]);
    FHasInvisibleTab := FHasInvisibleTab or ((R.Right > FClipRect.Right) and (PageControl.FirstVisiblePageIndex < Pages.Count - 1));
  end;

  if HasInvisibleTab then
    FRightScrollArea := cxRectSetXPos(R, FClipRect.Right - dxSpreadSheetResizeDelta * 3, SpreadSheet.ClientBounds.Right)
  else
    FRightScrollArea.Left := R.Right;

  AddButton(R, sspcbNew);
  PageControl.Controller.UpdateStates;
end;

procedure TdxSpreadSheetPageControlViewInfo.InitializeViewParams;
begin
  FViewParams := SpreadSheet.Styles.GetPageControlStyle;
end;

procedure TdxSpreadSheetPageControlViewInfo.AddButton(var R: TRect; AType: TdxSpreadSheetPageControlButton);
var
  ACell: TdxSpreadSheetPageControlButtonCellViewInfo;
begin
  if not (AType in PageControl.Buttons) then
    Exit;

  R := cxRectSetLeft(R, R.Right, LookAndFeelPainter.NavigatorButtonMinSize.cy);
  if LookAndFeelPainter.NavigatorBorderOverlap and (R.Left > Bounds.Left) then
    OffsetRect(R, -LookAndFeelPainter.NavigatorBorderSize, 0);
  ACell := TdxSpreadSheetPageControlButtonCellViewInfo.Create(Self);
  ACell.Initialize(R, FClipRect, AType);
  Cells.Add(ACell);
end;

procedure TdxSpreadSheetPageControlViewInfo.AddPageTab(var R: TRect; APage: TdxSpreadSheetCustomView);
var
  ACell: TdxSpreadSheetPageControlTabCellViewInfo;
begin
  ACell := TdxSpreadSheetPageControlTabCellViewInfo.Create(Self);
  R.Left := R.Right;
  if LookAndFeelPainter.NavigatorBorderOverlap and (R.Left > Bounds.Left) then
    OffsetRect(R, -LookAndFeelPainter.NavigatorBorderSize, 0);
  ACell.Initialize(R, FClipRect, APage);
  R.Right := ACell.AbsoluteBounds.Right;
  Cells.Add(ACell);
end;

function TdxSpreadSheetPageControlViewInfo.GetLookAndFeelPainter: TcxCustomLookAndFeelPainter;
begin
  Result := SpreadSheet.LookAndFeelPainter;
end;

function TdxSpreadSheetPageControlViewInfo.GetSpreadSheet: TdxCustomSpreadSheet;
begin
  Result := PageControl.SpreadSheet;
end;

{ TdxSpreadSheetPageControl }

constructor TdxSpreadSheetPageControl.Create(ASpreadSheet: TdxCustomSpreadSheet);
begin
  inherited Create(ASpreadSheet);
  CreateSubClasses;
  FButtons := [sspcbFirst..sspcbNew];
  FVisible := True;
  FDropArrowColor := clGreen;
end;

destructor TdxSpreadSheetPageControl.Destroy;
begin
  DestroySubClasses;
  inherited Destroy;
end;

procedure TdxSpreadSheetPageControl.Assign(Source: TPersistent);
begin
  if Source is TdxSpreadSheetPageControl then
  begin
    FVisible := False;
    Buttons := TdxSpreadSheetPageControl(Source).Buttons;
    DropArrowColor := TdxSpreadSheetPageControl(Source).DropArrowColor;
    FirstVisiblePageIndex := TdxSpreadSheetPageControl(Source).FirstVisiblePageIndex;
    Width := TdxSpreadSheetPageControl(Source).Width;
    Visible := TdxSpreadSheetPageControl(Source).Visible;
  end;
end;

procedure TdxSpreadSheetPageControl.Changed;
begin
  ViewInfo.Calculate;
  if Visible then
    Invalidate;
end;

function TdxSpreadSheetPageControl.CreateController: TdxSpreadSheetPageControlController;
begin
  Result := TdxSpreadSheetPageControlController.Create(Self);
end;

function TdxSpreadSheetPageControl.CreateHitTest: TdxSpreadSheetPageControlHitTest;
begin
  Result := TdxSpreadSheetPageControlHitTest.Create(Self);
end;

function TdxSpreadSheetPageControl.CreateViewInfo: TdxSpreadSheetPageControlViewInfo;
begin
  Result := TdxSpreadSheetPageControlViewInfo.Create(Self);
end;

procedure TdxSpreadSheetPageControl.Invalidate;
begin
  if Visible then
    SpreadSheet.InvalidateRect(cxRectSetWidth(ViewInfo.Bounds, SpreadSheet.Width), False);
end;

procedure TdxSpreadSheetPageControl.Recalculate;
begin
  ViewInfo.Calculate;
  Invalidate;
end;

procedure TdxSpreadSheetPageControl.CreateSubClasses;
begin
  FHitTest := CreateHitTest;
  FViewInfo := CreateViewInfo;
  FController := CreateController;
end;

procedure TdxSpreadSheetPageControl.DestroySubClasses;
begin
  FreeAndNil(FViewInfo);
  FreeAndNil(FHitTest);
  FreeAndNil(FController);
end;

function TdxSpreadSheetPageControl.GetBounds: TRect;
begin
  if Visible then
    Result := ViewInfo.Bounds
  else
    Result := cxInvalidRect;
end;

function TdxSpreadSheetPageControl.GetFirstVisiblePageIndex: Integer;
begin
  Result := FFirstVisiblePageIndex;
end;

function TdxSpreadSheetPageControl.GetVisiblePage(AIndex: Integer): TdxSpreadSheetCustomView;
begin
  Result := ViewInfo.Pages[AIndex];
end;

function TdxSpreadSheetPageControl.GetVisiblePageCount: Integer;
begin
  Result := ViewInfo.Pages.Count;
end;

function TdxSpreadSheetPageControl.IsButtonsStored: Boolean;
begin
  Result := Buttons <> [sspcbFirst..sspcbNew];
end;

procedure TdxSpreadSheetPageControl.SetButtons(AValue: TdxSpreadSheetPageControlButtons);
begin
  if AValue <> FButtons then
  begin
    FButtons := AValue;
    Changed;
  end;
end;

procedure TdxSpreadSheetPageControl.SetFirstVisiblePageIndex(AValue: Integer);
begin
  AValue := Max(0, Min(AValue, VisiblePageCount - 1));
  if AValue <> FFirstVisiblePageIndex then
  begin
    FFirstVisiblePageIndex := AValue;
    Changed;
  end;
end;

procedure TdxSpreadSheetPageControl.SetWidth(AValue: Integer);
begin
  AValue := Max(0, AValue);
  if AValue <> FWidth then
  begin
    FWidth := AValue;
    SpreadSheet.SetInternalControlsBounds;
    Changed;
  end;
end;

procedure TdxSpreadSheetPageControl.SetVisible(AValue: Boolean);
begin
  if AValue <> FVisible then
  begin
    FVisible := AValue;
    SpreadSheet.BoundsChanged;
  end;
end;

{ TdxSpreadSheetViewInfo }

function TdxSpreadSheetViewInfo.GetBackgroundParams: TcxViewParams;
begin
  Result := SpreadSheet.Styles.GetBackgroundStyle;
end;

function TdxSpreadSheetViewInfo.GetLookAndFeelPainter: TcxCustomLookAndFeelPainter;
begin
  Result := SpreadSheet.LookAndFeelPainter;
end;

{ TdxSpreadSheetStyles }

procedure TdxSpreadSheetStyles.Assign(Source: TPersistent);
begin
  if Source is TdxSpreadSheetStyles then
  begin
    Background := TdxSpreadSheetStyles(Source).Background;
    Content := TdxSpreadSheetStyles(Source).Content;
    Header := TdxSpreadSheetStyles(Source).Header;
end;
end;

procedure TdxSpreadSheetStyles.Changed(AIndex: Integer);
begin
  SpreadSheet.LayoutChanged;
end;

procedure TdxSpreadSheetStyles.GetDefaultViewParams(Index: Integer; AData: TObject; out AParams: TcxViewParams);
begin
  inherited GetDefaultViewParams(Index, AData, AParams);
  AParams.Font := SpreadSheet.Font;
  AParams.TextColor := AParams.Font.Color;
  case Index of
    s_Selection:
      AParams.Color := SpreadSheet.LookAndFeelPainter.SpreadSheetSelectionColor;

    s_Background:
      begin
        AParams.Color := SpreadSheet.LookAndFeelPainter.DefaultContentColor;
        AParams.TextColor := SpreadSheet.LookAndFeelPainter.DefaultContentTextColor;
      end;

    s_Content:
      begin
        AParams.Color := SpreadSheet.LookAndFeelPainter.SpreadSheetContentColor;
        AParams.TextColor := SpreadSheet.LookAndFeelPainter.SpreadSheetContentTextColor;
      end;

    s_Header:
      begin
        AParams.Color := SpreadSheet.LookAndFeelPainter.DefaultHeaderColor;
        AParams.TextColor := SpreadSheet.LookAndFeelPainter.DefaultHeaderTextColor;
      end;

    s_PageControl:
      begin
        AParams.Color := SpreadSheet.LookAndFeelPainter.DefaultTabColor;
        AParams.TextColor := SpreadSheet.LookAndFeelPainter.NavigatorButtonTextColor(cxbsNormal);
      end;
   end;
end;

function TdxSpreadSheetStyles.GetBackgroundStyle: TcxViewParams;
begin
  GetViewParams(s_Background, nil, nil, Result);
end;

function TdxSpreadSheetStyles.GetContentStyle(AView: TdxSpreadSheetCustomView): TcxViewParams;
begin
  GetViewParams(s_Content, AView, nil, Result);
end;

function TdxSpreadSheetStyles.GetHeaderStyle(AView: TdxSpreadSheetCustomView): TcxViewParams;
begin
  GetViewParams(s_Header, AView, nil, Result);
end;

function TdxSpreadSheetStyles.GetPageControlStyle: TcxViewParams;
begin
  GetViewParams(s_PageControl, nil, nil, Result);
end;

function TdxSpreadSheetStyles.GetSelectionStyle: TcxViewParams;
begin
  GetViewParams(s_Selection, nil, nil, Result);
end;

function TdxSpreadSheetStyles.GetSpreadSheet: TdxCustomSpreadSheet;
begin
  Result := Owner as TdxCustomSpreadSheet;
end;

{ TdxSpreadSheetOptionsBehavior }

constructor TdxSpreadSheetOptionsBehavior.Create(ASpreadSheet: TdxCustomSpreadSheet);
begin
  inherited Create(ASpreadSheet);
  FAutomaticCalculation := True;
  FDeleting := True;
  FEditing := True;
  FFormatting := True;
  FInserting := True;
end;

procedure TdxSpreadSheetOptionsBehavior.Assign(Source: TPersistent);
begin
  if Source is TdxSpreadSheetOptionsBehavior then
  begin
    AutomaticCalculation := TdxSpreadSheetOptionsBehavior(Source).AutomaticCalculation;
    Deleting := TdxSpreadSheetOptionsBehavior(Source).Deleting;
    Editing := TdxSpreadSheetOptionsBehavior(Source).Editing;
    Formatting := TdxSpreadSheetOptionsBehavior(Source).Formatting;
    History := TdxSpreadSheetOptionsBehavior(Source).History;
    Inserting := TdxSpreadSheetOptionsBehavior(Source).Inserting;
    IterativeCalculation := TdxSpreadSheetOptionsBehavior(Source).IterativeCalculation;
    IterativeCalculationMaxCount := TdxSpreadSheetOptionsBehavior(Source).IterativeCalculationMaxCount;
    Protected := TdxSpreadSheetOptionsBehavior(Source).Protected;
  end;
end;

procedure TdxSpreadSheetOptionsBehavior.SetHistory(AValue: Boolean);
begin
  if FHistory <> AValue then
  begin
    FHistory := AValue;
    SpreadSheet.History.Clear;
  end;
end;

procedure TdxSpreadSheetOptionsBehavior.SetIterativeCalculationMaxCount(AValue: Integer);
begin
  FIterativeCalculationMaxCount := Max(AValue, 0);
end;

procedure TdxSpreadSheetOptionsBehavior.SetProtected(AValue: Boolean);
begin
  if FProtected <> AValue then
  begin
    FProtected := AValue;
    SpreadSheet.LayoutChanged;
  end;
end;

{ TdxSpreadSheetOptionsView }

constructor TdxSpreadSheetOptionsView.Create(ASpreadSheet: TdxCustomSpreadSheet);
begin
  inherited Create(ASpreadSheet);
  FAntialiasing := True;
  FCellAutoHeight := True;
  FDateTimeSystem := dts1900;
  FFrozenPaneSeparatorWidth := 1;
  FFrozenPaneSeparatorColor := clDefault;
  FGridLineColor := clDefault;
  FGridLines := True;
  FHeaders := True;
  FHorizontalScrollBar := True;
  FR1C1Reference := False;
  FShowFormulas := False;
  FVerticalScrollBar := True;
  FZeroValues := True;
end;

procedure TdxSpreadSheetOptionsView.Assign(Source: TPersistent);
begin
  if Source is TdxSpreadSheetOptionsView then
  begin
    FAntialiasing := TdxSpreadSheetOptionsView(Source).Antialiasing;
    FCellAutoHeight := TdxSpreadSheetOptionsView(Source).CellAutoHeight;
    FDateTimeSystem := TdxSpreadSheetOptionsView(Source).DateTimeSystem;
    FFrozenPaneSeparatorColor := TdxSpreadSheetOptionsView(Source).FrozenPaneSeparatorColor;
    FFrozenPaneSeparatorWidth := TdxSpreadSheetOptionsView(Source).FrozenPaneSeparatorWidth;
    FGridLineColor := TdxSpreadSheetOptionsView(Source).FGridLineColor;
    FGridLines := TdxSpreadSheetOptionsView(Source).GridLines;
    FHeaders := TdxSpreadSheetOptionsView(Source).Headers;
    FHorizontalScrollBar := TdxSpreadSheetOptionsView(Source).FHorizontalScrollBar;
    FR1C1Reference := TdxSpreadSheetOptionsView(Source).R1C1Reference;
    FShowFormulas :=  TdxSpreadSheetOptionsView(Source).FShowFormulas;
    FVerticalScrollBar := TdxSpreadSheetOptionsView(Source).FVerticalScrollBar;
    FZeroValues := TdxSpreadSheetOptionsView(Source).ZeroValues;
    Changed;
  end;
end;

procedure TdxSpreadSheetOptionsView.Changed;
begin
  SpreadSheet.FormatSettings.UpdateSettings;
  SpreadSheet.AddChanges([sscData, sscLayout]);
end;

function TdxSpreadSheetOptionsView.GetActualDateTimeSystem: TdxSpreadSheetDateTimeSystem;
begin
  Result := DateTimeSystem;
  if Result = dtsDefault then
    Result := dts1900;
end;

procedure TdxSpreadSheetOptionsView.SetAntialiasing(const AValue: Boolean);
begin
  if Antialiasing <> AValue then
  begin
    FAntialiasing := AValue;
    Changed;
  end;
end;

procedure TdxSpreadSheetOptionsView.SetDateTimeSystem(AValue: TdxSpreadSheetDateTimeSystem);
begin
  if AValue <> FDateTimeSystem then
  begin
    FDateTimeSystem := AValue;
    Changed;
  end;
end;

procedure TdxSpreadSheetOptionsView.SetFrozenPaneSeparatorColor(AValue: TColor);
begin
  if AValue <> FFrozenPaneSeparatorColor then
  begin
    FFrozenPaneSeparatorColor := AValue;
    Changed;
  end;
end;

procedure TdxSpreadSheetOptionsView.SetFrozenPaneSeparatorWidth(AValue: Integer);
begin
  dxSpreadSheetValidate(AValue, 0, dxSpreadSheetMaxSeparatorWidth);
  if AValue <> FFrozenPaneSeparatorWidth then
  begin
    FFrozenPaneSeparatorWidth := AValue;
    Changed;
  end;
end;

procedure TdxSpreadSheetOptionsView.SetGridLineColor(AValue: TColor);
begin
  if AValue <> FGridLineColor then
  begin
    FGridLineColor := AValue;
    Changed;
  end;
end;

procedure TdxSpreadSheetOptionsView.SetGridLines(AValue: Boolean);
begin
  if AValue <> FGridLines then
  begin
    FGridLines := AValue;
    Changed;
  end;
end;

procedure TdxSpreadSheetOptionsView.SetHeaders(AValue: Boolean);
begin
  if AValue <> FHeaders then
  begin
    FHeaders := AValue;
    Changed;
  end;
end;

procedure TdxSpreadSheetOptionsView.SetHorizontalScrollBar(AValue: Boolean);
begin
  if AValue <> FHorizontalScrollBar then
  begin
    FHorizontalScrollBar := AValue;
    Changed;
  end;
end;

procedure TdxSpreadSheetOptionsView.SetR1C1Reference(AValue: Boolean);
begin
  if AValue <> FR1C1Reference then
  begin
    FR1C1Reference := AValue;
    Changed;
  end;
end;

procedure TdxSpreadSheetOptionsView.SetShowFormulas(AValue: Boolean);
begin
  if AValue <> FShowFormulas then
  begin
    FShowFormulas := AValue;
    Changed;
  end;
end;

procedure TdxSpreadSheetOptionsView.SetVerticalScrollBar(AValue: Boolean);
begin
  if AValue <> FVerticalScrollBar then
  begin
    FVerticalScrollBar := AValue;
    Changed;
  end;
end;

procedure TdxSpreadSheetOptionsView.SetZeroValues(AValue: Boolean);
begin
  if FZeroValues <> AValue then
  begin
    FZeroValues := AValue;
    Changed;
  end;
end;

{ TdxSpreadSheetHistoryCustomCommand }

procedure TdxSpreadSheetHistoryCustomCommand.Initialize;
begin
end;

class function TdxSpreadSheetHistoryCustomCommand.ActionClass: TdxSpreadSheetHistoryActionClass;
begin
  Result := TdxSpreadSheetHistoryAction;
end;

function TdxSpreadSheetHistoryCustomCommand.CompatibleWith(ACommand: TdxSpreadSheetHistoryCustomCommand): Boolean;
begin
  Result := False;
end;

procedure TdxSpreadSheetHistoryCustomCommand.Redo;
begin
end;

procedure TdxSpreadSheetHistoryCustomCommand.Undo;
begin
end;

function TdxSpreadSheetHistoryCustomCommand.GetView: TdxSpreadSheetCustomView;
begin
  Result := Owner.View;
end;

{ TdxSpreadSheetHistoryCellCommand }

constructor TdxSpreadSheetHistoryCellCommand.Create(ASheet: TdxSpreadSheetTableView; ARow, AColumn: Integer);
begin
  FRow := ARow;
  FColumn := AColumn;
  FSheet := ASheet;
end;

class function TdxSpreadSheetHistoryCellCommand.ActionClass: TdxSpreadSheetHistoryActionClass;
begin
  Result := TdxSpreadSheetHistoryEditCellAction;
end;

function TdxSpreadSheetHistoryCellCommand.CompatibleWith(ACommand: TdxSpreadSheetHistoryCustomCommand): Boolean;
begin
  Result := (ACommand is TdxSpreadSheetHistoryCellCommand) and
    (TdxSpreadSheetHistoryCellCommand(ACommand).FSheet = FSheet) and
    (TdxSpreadSheetHistoryCellCommand(ACommand).FRow = FRow) and
    (TdxSpreadSheetHistoryCellCommand(ACommand).FColumn = FColumn);
end;

{ TdxSpreadSheetHistoryCreateCellCommand }

procedure TdxSpreadSheetHistoryCreateCellCommand.Redo;
begin
  if FHasCell then Exit;
  FSheet.CreateCell(FRow, FColumn);
end;

procedure TdxSpreadSheetHistoryCreateCellCommand.Undo;
begin
  if FHasCell then Exit;
  FSheet.Cells[FRow, FColumn].Free;
  if not FHasRow then
    FSheet.Rows[FRow].Free;
  if not FHasColumn then
    FSheet.Columns[FRow].Free;
end;

procedure TdxSpreadSheetHistoryCreateCellCommand.Initialize;
begin
  FHasCell := FSheet.Cells[FRow, FColumn] <> nil;
  if not FHasCell then
  begin
    FHasColumn := FSheet.Columns[FColumn] <> nil;
    FHasRow := FSheet.Rows[FRow] <> nil;
  end;
end;

{ TdxSpreadSheetHistoryChangeCellCommand }

constructor TdxSpreadSheetHistoryChangeCellCommand.CreateEx(ACell: TdxSpreadSheetCell);
begin
  inherited Create(ACell.View, ACell.RowIndex, ACell.ColumnIndex);
end;

destructor TdxSpreadSheetHistoryChangeCellCommand.Destroy;
begin
  FreeAndNil(FData);
  inherited Destroy;
end;

procedure TdxSpreadSheetHistoryChangeCellCommand.Initialize;
begin
  Store;
end;

procedure TdxSpreadSheetHistoryChangeCellCommand.Store;
var
  AWriter: TcxWriter;
begin
  FData := TMemoryStream.Create;
  AWriter := TcxWriter.Create(FData, dxSpreadSheetBinaryFormatVersion);
  try
    FSheet.Cells[FRow, FColumn].SaveToStream(AWriter, 0);
  finally
    AWriter.Free;
  end;
end;

procedure TdxSpreadSheetHistoryChangeCellCommand.Restore;
var
  ACell: TdxSpreadSheetCell;
  AFormulaRef: TdxSpreadSheetFormulaAsTextInfoList;
  AReader: TcxReader;
begin
  FData.Position := 0;
  AReader := TcxReader.Create(FData, dxSpreadSheetBinaryFormatVersion);
  try
    Store;
    AReader.ReadInteger; // skip style index;
    AFormulaRef := TdxSpreadSheetFormulaAsTextInfoList.Create(FSheet.SpreadSheet);
    try
      ACell := FSheet.CreateCell(FRow, FColumn);
      ACell.LoadFromStream(AReader, ACell.Style.Handle, AFormulaRef);
      if ACell.DataType = cdtFormula then
        AFormulaRef.ResolveReferences;
    finally
      AFormulaRef.Free;
    end;
  finally
    AReader.Stream.Free;
    AReader.Free;
  end;
end;

procedure TdxSpreadSheetHistoryChangeCellCommand.Redo;
begin
  Restore;
end;

procedure TdxSpreadSheetHistoryChangeCellCommand.Undo;
begin
  Restore;
end;

{ TdxSpreadSheetHistoryChangeCellStyleCommand }

procedure TdxSpreadSheetHistoryChangeCellStyleCommand.Store;
var
  AWriter: TcxWriter;
begin
  FData := TMemoryStream.Create;
  AWriter := TcxWriter.Create(FData, dxSpreadSheetBinaryFormatVersion);
  try
    FSheet.CreateCell(FRow, FColumn).Style.Handle.SaveToStream(AWriter);
  finally
    AWriter.Free;
  end;
end;

procedure TdxSpreadSheetHistoryChangeCellStyleCommand.Restore;
var
  AReader: TcxReader;
begin
  FData.Position := 0;
  AReader := TcxReader.Create(FData, dxSpreadSheetBinaryFormatVersion);
  try
    Store;
    FSheet.CreateCell(FRow, FColumn).Style.Handle := FSheet.SpreadSheet.CellStyles.CreateStyleFromStream(AReader);
  finally
    AReader.Stream.Free;
    AReader.Free;
  end;
end;

{ TdxSpreadSheetHistoryMergeCellsCommand }

constructor TdxSpreadSheetHistoryMergeCellsCommand.Create(ACell: TdxSpreadSheetMergedCell; const AArea: TRect);
begin
  if ACell <> nil then
  begin
    FPrevArea := ACell.Area;
    FArea := AArea;
  end
  else
    FPrevArea := AArea;
end;

class function TdxSpreadSheetHistoryMergeCellsCommand.ActionClass: TdxSpreadSheetHistoryActionClass;
begin
  Result := TdxSpreadSheetHistoryMergeCellsAction;
end;

procedure TdxSpreadSheetHistoryMergeCellsCommand.Redo;
begin
  UndoRedo;
end;

procedure TdxSpreadSheetHistoryMergeCellsCommand.Undo;
begin
  UndoRedo;
end;

function TdxSpreadSheetHistoryMergeCellsCommand.GetMergedCells: TdxSpreadSheetMergedCellList;
begin
   Result := TdxSpreadSheetTableView(View).MergedCells;
end;

function TdxSpreadSheetHistoryMergeCellsCommand.FindCell: TdxSpreadSheetMergedCell;
var
  ACells: TdxSpreadSheetMergedCellList;
begin
  ACells := TdxSpreadSheetTableView(View).MergedCells;
  Result := FCell;
  if ACells.Find(FCell) >= 0 then
    Exit;
  Result := ACells.First;
  while (Result <> nil) and not cxRectIsEqual(Result.Area, FArea) do
    Result := Result.Next;
end;

procedure TdxSpreadSheetHistoryMergeCellsCommand.UndoRedo;
var
  R: TRect;
  ACell: TdxSpreadSheetMergedCell;
begin
  ACell := FindCell;
  if cxRectIsNull(FPrevArea) then
  begin
    ACell.Free;
    FCell := nil;
  end
  else
    if ACell <> nil then
      ACell.Initialize(ACell.Owner, FPrevArea)
    else
    begin
      MergedCells.Add(FPrevArea);
      FCell := MergedCells.Last;
    end;
  R := FArea;
  FArea := FPrevArea;
  FPrevArea := R;
end;

{ TdxSpreadSheetHistoryChangeItemCommand }

constructor TdxSpreadSheetHistoryChangeItemCommand.Create(AItem: TdxSpreadSheetTableItem);
begin
  FItemIndex := AItem.Index;
  FItems := AItem.Owner;
  Store;
end;

destructor TdxSpreadSheetHistoryChangeItemCommand.Destroy;
begin
  FreeAndNil(FData);
  inherited Destroy;
end;

procedure TdxSpreadSheetHistoryChangeItemCommand.Redo;
begin
  Restore;
end;

procedure TdxSpreadSheetHistoryChangeItemCommand.Undo;
begin
  Restore;
end;

procedure TdxSpreadSheetHistoryChangeItemCommand.Restore;
var
  AReader: TcxReader;
begin
  FData.Position := 0;
  AReader := TcxReader.Create(FData, dxSpreadSheetBinaryFormatVersion);
  try
    Store;
    Item.Restore(AReader);
  finally
    AReader.Stream.Free;
    AReader.Free;
  end;
end;

procedure TdxSpreadSheetHistoryChangeItemCommand.Store;
var
  AWriter: TcxWriter;
begin
  FData := TMemoryStream.Create;
  AWriter := TcxWriter.Create(FData, dxSpreadSheetBinaryFormatVersion);
  try
    Item.Store(AWriter);
  finally
    AWriter.Free;
  end;
end;

function TdxSpreadSheetHistoryChangeItemCommand.GetItem: TdxSpreadSheetTableItem;
begin
  Result := FItems.CreateItem(FItemIndex);
end;

{ TdxSpreadSheetHistoryDeleteItemCommand }

procedure TdxSpreadSheetHistoryDeleteItemCommand.Redo;
begin
  Item.Free;
end;

procedure TdxSpreadSheetHistoryDeleteItemCommand.Undo;
begin
  Restore;
end;

procedure TdxSpreadSheetHistoryDeleteItemCommand.Restore;
var
  AReader: TcxReader;
begin
  Data.Position := 0;
  AReader := TcxReader.Create(Data, dxSpreadSheetBinaryFormatVersion);
  try
    Item.Restore(AReader);
  finally
    AReader.Free;
  end;
end;

procedure TdxSpreadSheetHistoryDeleteItemCommand.Store;
var
  AWriter: TcxWriter;
begin
  Data := TMemoryStream.Create;
  AWriter := TcxWriter.Create(Data, dxSpreadSheetBinaryFormatVersion);
  try
    Item.Store(AWriter);
  finally
    AWriter.Free;
  end;
end;


{ TdxSpreadSheetChangeDefaultSizeCommand }

constructor TdxSpreadSheetChangeDefaultSizeCommand.Create(AItems: TdxSpreadSheetTableItems);
begin
  FItems := AItems;
  FSize := FItems.DefaultSize;
end;

procedure TdxSpreadSheetChangeDefaultSizeCommand.Redo;
begin
  Undo;
end;

procedure TdxSpreadSheetChangeDefaultSizeCommand.Undo;
var
  ASize: Integer;
begin
  ASize := FItems.DefaultSize;
  FItems.DefaultSize := FSize;
  FSize := ASize;
end;

{ TdxSpreadSheetHistoryDeleteContainerCommand }

constructor TdxSpreadSheetHistoryDeleteContainerCommand.Create(AContainer: TdxSpreadSheetContainer);
begin
  FContainer := AContainer;
  FContainerClass := TdxSpreadSheetContainerClass(AContainer.ClassType);
  Store;
end;

destructor TdxSpreadSheetHistoryDeleteContainerCommand.Destroy;
begin
  FreeAndNil(FData);
  inherited Destroy;
end;

procedure TdxSpreadSheetHistoryDeleteContainerCommand.Redo;
begin
  FContainer.Free;
end;

procedure TdxSpreadSheetHistoryDeleteContainerCommand.Undo;
begin
  Restore;
end;

procedure TdxSpreadSheetHistoryDeleteContainerCommand.Restore;
var
  AReader: TcxReader;
begin
  FData.Position := 0;
  FContainer := View.Containers.Add(FContainerClass);
  AReader := TcxReader.Create(FData, dxSpreadSheetBinaryFormatVersion);
  try
    FContainer.LoadFromStream(AReader);
  finally
    AReader.Free;
  end;
end;

procedure TdxSpreadSheetHistoryDeleteContainerCommand.Store;
var
  AWriter: TcxWriter;
begin
  FData := TMemoryStream.Create;
  AWriter := TcxWriter.Create(FData, dxSpreadSheetBinaryFormatVersion);
  try
    FContainer.SaveToStream(AWriter);
  finally
    AWriter.Free;
  end;
end;

{ TdxSpreadSheetHistoryFormulaChangedCommand }

constructor TdxSpreadSheetHistoryFormulaChangedCommand.Create(
  AFormula: TdxSpreadSheetFormula; const ASourceText: TdxUnicodeString);
begin
  FColumnIndex := AFormula.Column;
  FRowIndex := AFormula.Row;
  FSheet := AFormula.Sheet;
  if AFormula is TdxSpreadSheetDefinedNameFormula then
    FName := TdxSpreadSheetDefinedNameFormula(AFormula).Owner;
  FSourceText := ASourceText;
end;

procedure TdxSpreadSheetHistoryFormulaChangedCommand.Redo;
begin
  // todo: when insert all reference will be valid
end;

procedure TdxSpreadSheetHistoryFormulaChangedCommand.Undo;
begin
  if (FName = nil) and (FSheet <> nil) then
    FSheet.CreateCell(FRowIndex, FColumnIndex).SetText(FSourceText, True)
  else
    if View.SpreadSheet.DefinedNames.ItemList.IndexOf(FName) >= 0 then
      FName.Reference := FSourceText;
end;

{ TdxSpreadSheetHistoryChangeContainerCommand }

constructor TdxSpreadSheetHistoryChangeContainerCommand.Create(AContainer: TdxSpreadSheetContainer);
begin
  FIndex := AContainer.Index;
  Store(AContainer);
end;

destructor TdxSpreadSheetHistoryChangeContainerCommand.Destroy;
begin
  FreeAndNil(FData);
  inherited Destroy;
end;

procedure TdxSpreadSheetHistoryChangeContainerCommand.Redo;
begin
  Restore;
end;

procedure TdxSpreadSheetHistoryChangeContainerCommand.Undo;
begin
  Restore;
end;

procedure TdxSpreadSheetHistoryChangeContainerCommand.Restore;
var
  AReader: TcxReader;
begin
  FData.Position := 0;
  AReader := TcxReader.Create(FData, dxSpreadSheetBinaryFormatVersion);
  try
    Store(Container);
    Container.LoadSettings(AReader);
  finally
    AReader.Stream.Free;
    AReader.Free;
  end;
end;

procedure TdxSpreadSheetHistoryChangeContainerCommand.Store(AContainer: TdxSpreadSheetContainer);
var
  AWriter: TcxWriter;
begin
  FData := TMemoryStream.Create;
  AWriter := TcxWriter.Create(FData, dxSpreadSheetBinaryFormatVersion);
  try
    AContainer.SaveSettings(AWriter);
  finally
    AWriter.Free;
  end;
end;

function TdxSpreadSheetHistoryChangeContainerCommand.GetContainer: TdxSpreadSheetContainer;
begin
  Result := View.Containers[FIndex];
end;

{ TdxSpreadSheetHistoryCreateContainerCommand }

procedure TdxSpreadSheetHistoryCreateContainerCommand.Redo;
begin
  inherited Undo;
end;

procedure TdxSpreadSheetHistoryCreateContainerCommand.Undo;
begin
  Container.Free;
end;

{ TdxSpreadSheetHistoryAction }

constructor TdxSpreadSheetHistoryAction.Create(AOwner: TdxSpreadSheetHistory);
begin
  FHistory := AOwner;
  FCommands := TcxObjectList.Create;
  FView := SpreadSheet.ActiveSheet;
  FData := StoreSelection;
end;

destructor TdxSpreadSheetHistoryAction.Destroy;
begin
  FreeAndNil(FData);
  if FHistory.CurrentAction = Self then
    FHistory.CurrentAction := nil;
  FreeAndNil(FCommands);
  inherited Destroy;
end;

procedure TdxSpreadSheetHistoryAction.Redo;
var
  AData: TStream;
begin
  History.CurrentAction := nil;
  if History.RedoActions[History.RedoActionCount - 1] <> Self then
    Exit
  else
  begin
    SpreadSheet.BeginUpdate;
    History.InProcess := True;
    try
      AData := StoreSelection;
      DoRedo;
      History.RedoActionList.Remove(Self);
      History.UndoActionList.Add(Self);
      RestoreSelection(AData);
    finally
      History.InProcess := False;
      SpreadSheet.EndUpdate;
    end;
  end;
end;

procedure TdxSpreadSheetHistoryAction.Undo;
var
  AData: TStream;
begin
  History.CurrentAction := nil;
  if History.UndoActions[History.UndoActionCount - 1] <> Self then
    Exit
  else
  begin
    SpreadSheet.BeginUpdate;
    History.InProcess := True;
    try
      AData := StoreSelection;
      DoUndo;
      History.UndoActionList.Remove(Self);
      History.RedoActionList.Add(Self);
      RestoreSelection(AData);
    finally
      History.InProcess := False;
      SpreadSheet.EndUpdate;
    end;
  end;
end;

function TdxSpreadSheetHistoryAction.AcceptCommand(ACommand: TdxSpreadSheetHistoryCustomCommand): Boolean;
begin
  Result := True;
end;

procedure TdxSpreadSheetHistoryAction.AddCommand(ACommand: TdxSpreadSheetHistoryCustomCommand);
begin
  FCommands.Add(ACommand);
  ACommand.FOwner := Self;
  ACommand.Initialize;
end;

procedure TdxSpreadSheetHistoryAction.DoRedo;
var
  I: Integer;
begin
  SpreadSheet.ActiveSheet := View;
  for I := 0 to Count - 1 do
    Commands[I].Redo;
end;

procedure TdxSpreadSheetHistoryAction.DoUndo;
var
  I: Integer;
begin
  SpreadSheet.ActiveSheet := View;
  for I := Count - 1 downto 0 do
    Commands[I].Undo;
end;

class function TdxSpreadSheetHistoryAction.GetDescription: string;
begin
  Result := '';
end;

function TdxSpreadSheetHistoryAction.StoreSelection: TStream;
var
  AWriter: TcxWriter;
begin
  Result := TMemoryStream.Create;
  AWriter := TcxWriter.Create(Result, dxSpreadSheetBinaryFormatVersion);
  try
    TdxSpreadSheetTableView(View).Selection.SaveToStream(AWriter);
  finally
    AWriter.Free;
  end;
end;

procedure TdxSpreadSheetHistoryAction.RestoreSelection(AData: TStream);
var
  AReader: TcxReader;
begin
  AReader := TcxReader.Create(FData, dxSpreadSheetBinaryFormatVersion);
  try
    FData.Position := 0;
    TdxSpreadSheetTableView(View).Selection.LoadFromStream(AReader);
  finally
    FData.Free;
    AReader.Free;
  end;
  FData := AData;
end;

function TdxSpreadSheetHistoryAction.GetCount: Integer;
begin
  Result := FCommands.Count;
end;

function TdxSpreadSheetHistoryAction.GetCommand(AIndex: Integer): TdxSpreadSheetHistoryCustomCommand;
begin
  Result := FCommands[AIndex] as TdxSpreadSheetHistoryCustomCommand;
end;

function TdxSpreadSheetHistoryAction.GetSpreadSheet: TdxCustomSpreadSheet;
begin
  Result := History.SpreadSheet;
end;

{ TdxSpreadSheetHistoryEditCellAction }

class function TdxSpreadSheetHistoryEditCellAction.GetDescription: string;
begin
  Result := cxGetResourceString(@sdxActionCellEditing);
end;

{ TdxSpreadSheetHistoryMergeCellsAction }

class function TdxSpreadSheetHistoryMergeCellsAction.GetDescription: string;
begin
  Result := cxGetResourceString(@sdxActionCellsMerge);
end;

{ TdxSpreadSheetHistoryClearCellsAction }

class function TdxSpreadSheetHistoryClearCellsAction.GetDescription: string;
begin
  Result := cxGetResourceString(@sdxActionClearCells);
end;

{ TdxSpreadSheetHistoryDeleteCellsAction }

class function TdxSpreadSheetHistoryDeleteCellsAction.GetDescription: string;
begin
  Result := cxGetResourceString(@sdxActionDeleteCells);
end;

{ TdxSpreadSheetHistoryInsertCellsAction }

function TdxSpreadSheetHistoryInsertCellsAction.AcceptCommand(ACommand: TdxSpreadSheetHistoryCustomCommand): Boolean;
begin
  Result := not (ACommand is TdxSpreadSheetHistoryChangeCellStyleCommand);
end;

class function TdxSpreadSheetHistoryInsertCellsAction.GetDescription: string;
begin
  Result := cxGetResourceString(@sdxActionInsertCells);
end;

{ TdxSpreadSheetHistoryFormatCellAction }

class function TdxSpreadSheetHistoryFormatCellAction.GetDescription: string;
begin
  Result := cxGetResourceString(@sdxActionFormatCells);
end;

{ TdxSpreadSheetHistorySortingAction }

class function TdxSpreadSheetHistorySortingAction.GetDescription: string;
begin
  Result := cxGetResourceString(@sdxActionSortCells);
end;

{ TdxSpreadSheetHistoryCutToClipboardAction }

class function TdxSpreadSheetHistoryCutToClipboardAction.GetDescription: string;
begin
  Result := cxGetResourceString(@sdxActionCutCells);
end;

{ TdxSpreadSheetHistoryPasteFromClipboardAction }

class function TdxSpreadSheetHistoryPasteFromClipboardAction.GetDescription: string;
begin
  Result := cxGetResourceString(@sdxActionPasteCells);
end;

{ TdxSpreadSheetHistoryChangeRowColumnItemAction }

class function TdxSpreadSheetHistoryChangeRowColumnItemAction.GetDescription: string;
begin
  Result := cxGetResourceString(@sdxActionChangeRowColumn);
end;

{ TdxSpreadSheetHistoryChangeContainerAction }

class function TdxSpreadSheetHistoryChangeContainerAction.GetDescription: string;
begin
  Result := cxGetResourceString(@sdxActionChangeContainer);
end;

{ TdxSpreadSheetHistory }

constructor TdxSpreadSheetHistory.Create(ASpreadSheet: TdxCustomSpreadSheet);
begin
  inherited Create(ASpreadSheet);
  FRedoActions := TcxObjectList.Create;
  FUndoActions  := TcxObjectList.Create;
end;

destructor TdxSpreadSheetHistory.Destroy;
begin
  Clear;
  FreeAndNil(FRedoActions);
  FreeAndNil(FUndoActions);
  inherited Destroy;
end;

function TdxSpreadSheetHistory.CanAddCommand: Boolean;
begin
  Result := not (IsLocked or InProcess);
end;

procedure TdxSpreadSheetHistory.AddCommand(ACommand: TdxSpreadSheetHistoryCustomCommand);
begin
  if not CanAddCommand or ((FCurrentAction <> nil) and not FCurrentAction.AcceptCommand(ACommand)) then
    ACommand.Free
  else
  begin
    if (FCurrentAction = nil) or (FActionLockCount = 0) then
    begin
      if UndoActionCount > 0 then
      begin
        FCurrentAction := UndoActions[UndoActionCount - 1];
        if (FCurrentAction.Count = 0) or not FCurrentAction.Commands[0].CompatibleWith(ACommand) then
          FCurrentAction := nil;
      end;
      if FCurrentAction = nil then
      begin
        FCurrentAction := ACommand.ActionClass.Create(Self);
        FUndoActions.Add(FCurrentAction);
        SpreadSheet.DoHistoryChanged;
      end;
      FRedoActions.Clear;
    end;
    FCurrentAction.AddCommand(ACommand);
  end;
end;

procedure TdxSpreadSheetHistory.BeginAction(AActionClass: TdxSpreadSheetHistoryActionClass);
begin
  Inc(FActionLockCount);
  if InProcess then Exit;
  if not IsLocked then
  begin
    if FActionLockCount = 1 then
      FCurrentAction := AActionClass.Create(Self);
  end
  else
    FCurrentAction := nil;
  if not IsLocked then
    FRedoActions.Clear;
end;

procedure TdxSpreadSheetHistory.EndAction();
begin
  Dec(FActionLockCount);
  if (FActionLockCount = 0) and (FCurrentAction <> nil) then
  begin
    if FCurrentAction.Count > 0 then
      FUndoActions.Add(FCurrentAction)
    else
      FCurrentAction.Free;
    FCurrentAction := nil;
    if UndoActionCount > 0 then
      SpreadSheet.DoHistoryChanged;
  end;
end;

procedure TdxSpreadSheetHistory.Clear;
var
  ACount: Integer;
begin
  FCurrentAction := nil;
  ACount := FUndoActions.Count + RedoActionCount;
  FUndoActions.Clear;
  FRedoActions.Clear;
  if ACount > 0 then
    SpreadSheet.DoHistoryChanged;
end;

procedure TdxSpreadSheetHistory.Lock;
begin
  Inc(FLockCount);
end;

procedure TdxSpreadSheetHistory.Unlock;
begin
  Dec(FLockCount);
  if FLockCount = 0 then
    Clear;
end;

procedure TdxSpreadSheetHistory.Redo(const ARedoCount: Integer = 1);
var
  I: Integer;
begin
  if (ARedoCount <= 0) or (RedoActionCount = 0) then Exit;
  if (SpreadSheet.ActiveSheet <> nil) and SpreadSheet.ActiveSheet.Controller.Redo(ARedoCount) then
    Exit;
  FCurrentAction := nil;
  Inc(FLockCount);
  InProcess := True;
  SpreadSheet.BeginUpdate;
  try
    if RedoActionCount > 0 then
      SpreadSheet.AddChanges([sscData, sscLayout]);
    for I := RedoActionCount - 1 downto Max(0, RedoActionCount - ARedoCount) do
      RedoActions[I].Redo;
  finally
    Dec(FLockCount);
    try
      SpreadSheet.EndUpdate;
    finally
      FInProcessRefCount := 0;
    end;
  end;
  SpreadSheet.DoHistoryChanged;
end;

procedure TdxSpreadSheetHistory.Undo(const AUndoCount: Integer = 1);
var
  I: Integer;
begin
  if (AUndoCount <= 0) or (UndoActionCount = 0) then Exit;
  if (SpreadSheet.ActiveSheet <> nil) and SpreadSheet.ActiveSheet.Controller.Undo(AUndoCount) then Exit;
  Inc(FLockCount);
  FCurrentAction := nil;
  InProcess := True;
  SpreadSheet.BeginUpdate;
  try
    if UndoActionCount > 0 then
      SpreadSheet.AddChanges([sscData, sscLayout]);
    for I := UndoActionCount - 1 downto Max(0, UndoActionCount - AUndoCount) do
      UndoActions[I].Undo;
  finally
    Dec(FLockCount);
    try
      SpreadSheet.EndUpdate;
    finally
      FInProcessRefCount := 0;
    end;
  end;
  SpreadSheet.DoHistoryChanged;
end;

function TdxSpreadSheetHistory.GetInProcess: Boolean;
begin
  Result := FInProcessRefCount > 0;
end;

function TdxSpreadSheetHistory.GetIsLocked: Boolean;
begin
  Result := (FLockCount > 0) or SpreadSheet.IsDestroying or SpreadSheet.IsLoading or not SpreadSheet.OptionsBehavior.History;
end;

function TdxSpreadSheetHistory.GetRedoAction(AIndex: Integer): TdxSpreadSheetHistoryAction;
begin
  if AIndex >= FRedoActions.Count then
    Result := nil
  else
    Result := TdxSpreadSheetHistoryAction(FRedoActions[AIndex]);
end;

function TdxSpreadSheetHistory.GetRedoActionCount: Integer;
begin
  Result := FRedoActions.Count;
  if not SpreadSheet.IsDestroying and (SpreadSheet.ActiveSheet <> nil) then
    SpreadSheet.ActiveSheet.Controller.GetRedoActionCount(Result);
end;

function TdxSpreadSheetHistory.GetUndoAction(AIndex: Integer): TdxSpreadSheetHistoryAction;
begin
  if AIndex >= FUndoActions.Count then
    Result := nil
  else
    Result := TdxSpreadSheetHistoryAction(FUndoActions[AIndex]);
end;

function TdxSpreadSheetHistory.GetUndoActionCount: Integer;
begin
  Result := FUndoActions.Count;
  if not SpreadSheet.IsDestroying and (SpreadSheet.ActiveSheet <> nil) then
    SpreadSheet.ActiveSheet.Controller.GetUndoActionCount(Result);
end;

procedure TdxSpreadSheetHistory.SetInProcess(AValue: Boolean);
begin
  if AValue then
    Inc(FInProcessRefCount)
  else
    Dec(FInProcessRefCount);
end;

{ TdxSpreadSheetLockedStatePaintHelper }

function TdxSpreadSheetLockedStatePaintHelper.CanCreateLockedImage: Boolean;
begin
  Result := inherited CanCreateLockedImage and not SpreadSheet.IsLocked;
end;

function TdxSpreadSheetLockedStatePaintHelper.DoPrepareImage: Boolean;
begin
  Result := SpreadSheet.DoPrepareLockedStateImage;
end;

function TdxSpreadSheetLockedStatePaintHelper.GetControl: TcxControl;
begin
  Result := SpreadSheet;
end;

function TdxSpreadSheetLockedStatePaintHelper.GetOptions: TcxLockedStateImageOptions;
begin
  Result := SpreadSheet.OptionsLockedStateImage;
end;

function TdxSpreadSheetLockedStatePaintHelper.GetSpreadSheet: TdxCustomSpreadSheet;
begin
  Result := TdxCustomSpreadSheet(Owner);
end;

{ TdxSpreadSheetLockedStateImageOptions }

constructor TdxSpreadSheetLockedStateImageOptions.Create(AOwner: TComponent);
begin
  inherited Create(AOwner) ;
  FSpreadSheet := AOwner as TdxCustomSpreadSheet;
  Enabled := True;
end;

function TdxSpreadSheetLockedStateImageOptions.GetFont: TFont;
begin
  Result := SpreadSheet.Font;
end;

{ TdxCustomSpreadSheet }

constructor TdxCustomSpreadSheet.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  CreateSubClasses;
  Keys := [kAll..kTab];
  Width := 460;
  Height := 240;
  AddSheet;
end;

destructor TdxCustomSpreadSheet.Destroy;
begin
  DestroySubClasses;
  inherited Destroy;
end;

function TdxCustomSpreadSheet.AddSheet(const ACaption: TdxUnicodeString = '';
  AViewClass: TdxSpreadSheetCustomViewClass = nil): TdxSpreadSheetCustomView;
begin
  Result := GetSheetByName(ACaption);
  if Result <> nil then Exit;
  if AViewClass = nil then
    AViewClass := TdxSpreadSheetTableView;
  Result := AViewClass.Create(Self);
  Result.Caption := ValidateSheetCaption(ACaption);
  LayoutChanged;
end;

procedure TdxCustomSpreadSheet.ClearAll;
begin
  History.Lock;
  try
    while SheetCount > 0 do
      Sheets[SheetCount - 1].Free;
    DefinedNames.Clear;
    FActiveSheetIndex := 0;
  finally
    History.Unlock;
    History.Clear;
  end;
end;

function TdxCustomSpreadSheet.GetSheetByName(const ACaption: TdxUnicodeString): TdxSpreadSheetCustomView;
var
  I: Integer;
begin
  Result := nil;
  for I := 0 to SheetCount - 1 do
  begin
    if (Result = nil) and (dxSpreadSheetCompareText(ACaption, Sheets[I].Caption) = 0) then
      Result := Sheets[I];
  end;
end;

procedure TdxCustomSpreadSheet.LayoutChanged;
begin
  AddChanges([sscLayout])
end;

procedure TdxCustomSpreadSheet.BeginUpdate(AMode: TcxLockedStateImageShowingMode = lsimNever);
begin
  LockedStatePaintHelper.BeginLockedPaint(AMode); 
  Inc(FLockCount);
end;

procedure TdxCustomSpreadSheet.EndUpdate;
begin
  Dec(FLockCount);
  if FLockCount = 0 then
    CheckChanges;
  LockedStatePaintHelper.EndLockedPaint;
end;

procedure TdxCustomSpreadSheet.LoadFromFile(const AFileName: string);
var
  AFileStream: TFileStream;
  AFormat: TdxSpreadSheetCustomFormatClass;
  AStream: TMemoryStream;
begin
  AStream := TMemoryStream.Create;
  try
    AFileStream := TFileStream.Create(AFileName, fmOpenRead or fmShareDenyNone);
    try
      AStream.LoadFromStream(AFileStream);
    finally
      AFileStream.Free;
    end;

    if dxSpreadSheetFormatsRepository.Find(AFileName, AFormat) and
      (not AFormat.CanCheckByContent or AFormat.CanReadFromStream(AStream))
    then
      LoadFromStream(AStream, AFormat)
    else
      LoadFromStream(AStream);
  finally
    AStream.Free;
  end;
end;

procedure TdxCustomSpreadSheet.LoadFromStream(AStream: TStream);
var
  AFormat: TdxSpreadSheetCustomFormatClass;
  I: Integer;
begin
  for I := 0 to dxSpreadSheetFormatsRepository.Count - 1 do
  begin
    AFormat := dxSpreadSheetFormatsRepository[I];
    if AFormat.CanCheckByContent and AFormat.CanReadFromStream(AStream) then
    begin
      LoadFromStream(AStream, AFormat);
      Exit;
    end;
  end;
  raise EdxSpreadSheetFormatError.Create(cxGetResourceString(@sdxErrorUnsupportedDocumentFormat));
end;

procedure TdxCustomSpreadSheet.LoadFromStream(AStream: TStream; AFormat: TdxSpreadSheetCustomFormatClass);
begin
  InternalLoadFromStream(AStream, AFormat, OnProgress);
end;

procedure TdxCustomSpreadSheet.SaveToFile(const AFileName: string);
var
  AFormat: TdxSpreadSheetCustomFormatClass;
  AStream: TFileStream;
begin
  if not dxSpreadSheetFormatsRepository.Find(AFileName, AFormat) then
    raise EdxSpreadSheetFormatError.Create(cxGetResourceString(@sdxErrorUnsupportedDocumentFormat));

  AStream := TFileStream.Create(AFileName, fmCreate);
  try
    SaveToStream(AStream, AFormat);
  finally
    AStream.Free;
  end;
end;

procedure TdxCustomSpreadSheet.SaveToStream(AStream: TStream; AFormat: TdxSpreadSheetCustomFormatClass = nil);
begin
  InternalSaveToStream(AStream, AFormat, OnProgress);
end;

procedure TdxCustomSpreadSheet.AddListener(AListener: IdxSpreadSheetListener);
begin
  if FListeners.IndexOf(AListener) = -1 then
    FListeners.Add(AListener);
end;

procedure TdxCustomSpreadSheet.RemoveListener(AListener: IdxSpreadSheetListener);
begin
  FListeners.Remove(AListener);
end;

procedure TdxCustomSpreadSheet.AddChanges(AChanges: TdxSpreadSheetChanges);
begin
  FChanges := FChanges + AChanges;
  CheckChanges;
end;

procedure TdxCustomSpreadSheet.AfterLoad;
var
  I: Integer;
begin
  for I := 0 to SheetCount - 1 do
    Sheets[I].AfterLoad;
  DefinedNames.AfterLoad;
  FormulaController.Calculate;
end;

procedure TdxCustomSpreadSheet.BoundsChanged;
begin
  inherited BoundsChanged;
  LayoutChanged;
  Invalidate;
end;

procedure TdxCustomSpreadSheet.CheckChanges;
var
  I: Integer;
  ADataChanged: Boolean;
begin
  if IsLocked or ProcessingChanges then
    Exit;

  if Changes = [] then
  begin
    for I := 0 to SheetCount - 1 do
      Sheets[I].CheckChanges;
  end;
  if Changes = [] then
    Exit;

  ADataChanged := False;
  ProcessingChanges := True;
  try
    if sscData in Changes then
    begin
      FormulaController.Recalculate;
      Changes := Changes + [sscLayout] - [sscData];
      DoDataChanged;
      ADataChanged := True;
    end;
    if sscLayout in Changes then
    begin
      Changes := Changes - [sscLayout];
      for I := 0 to SheetCount - 1 do
        Sheets[I].AddChanges([sscLayout]);
      if ActiveSheet = nil then
      begin
        UpdateScrollBars;
        Invalidate;
      end;
      PageControl.Recalculate;
      DoLayoutChanged;
    end;
  finally
    ProcessingChanges := False;
    CheckChanges;
  end;
  FormulaController.CheckCircularReferences;
  if ADataChanged then
    NotifyListeners;
end;

function TdxCustomSpreadSheet.ControllerFromPoint(
  const P: TPoint; var AController: TdxSpreadSheetCustomController): Boolean;
begin
  AController := ActiveController;
  if AController = nil then
  begin
    if (ActiveSheet <> nil) and PtInRect(ActiveSheet.Bounds,  P) then
      AController := ActiveSheet.Controller
    else
      if PtInRect(PageControl.Bounds, P) then
        AController := PageControl.Controller;
  end;
  Result := AController <> nil;
end;

function TdxCustomSpreadSheet.ControllerFromPoint(
  X, Y: Integer; var AController: TdxSpreadSheetCustomController): Boolean;
begin
  Result := ControllerFromPoint(Point(X, Y), AController);
end;

function TdxCustomSpreadSheet.CreateCellStyles: TdxSpreadSheetCellStyles;
begin
  Result := TdxSpreadSheetCellStyles.Create;
  Result.OnChange := StyleChanged;
end;

function TdxCustomSpreadSheet.CreateDefaultCellStyle: TdxSpreadSheetDefaultCellStyle;
begin
  Result := TdxSpreadSheetDefaultCellStyle.Create(Self);
end;

function TdxCustomSpreadSheet.CreateDefinedNames: TdxSpreadSheetDefinedNames;
begin
  Result := TdxSpreadSheetDefinedNames.Create(Self); 
end;

function TdxCustomSpreadSheet.CreateExternalLinks: TdxSpreadSheetExternalLinks;
begin
  Result := TdxSpreadSheetExternalLinks.Create(Self);
end;

function TdxCustomSpreadSheet.CreateFormatSettings: TdxSpreadSheetFormatSettings;
begin
  Result := TdxSpreadSheetControlFormatSettings.Create(Self);
end;

function TdxCustomSpreadSheet.CreateFormulaController:  TdxSpreadSheetFormulaController;
begin
  Result := TdxSpreadSheetFormulaController.Create(Self);
end;

function TdxCustomSpreadSheet.CreateHistory: TdxSpreadSheetHistory;
begin
  Result := TdxSpreadSheetHistory.Create(Self);
end;

function TdxCustomSpreadSheet.CreateLockedStatePaintHelper: TdxSpreadSheetLockedStatePaintHelper;
begin
  Result := TdxSpreadSheetLockedStatePaintHelper.Create(Self);
end;

function TdxCustomSpreadSheet.CreateOptionsBehavior: TdxSpreadSheetOptionsBehavior;
begin
  Result := TdxSpreadSheetOptionsBehavior.Create(Self);
end;

function TdxCustomSpreadSheet.CreateOptionsLockedStateImage: TdxSpreadSheetLockedStateImageOptions;
begin
  Result := TdxSpreadSheetLockedStateImageOptions.Create(Self);
end;

function TdxCustomSpreadSheet.CreateOptionsView: TdxSpreadSheetOptionsView;
begin
  Result := TdxSpreadSheetOptionsView.Create(Self);
end;

function TdxCustomSpreadSheet.CreatePageControl: TdxSpreadSheetPageControl;
begin
  Result := TdxSpreadSheetPageControl.Create(Self);
end;

function TdxCustomSpreadSheet.CreateSharedImages: TdxSpreadSheetSharedImages;
begin
  Result := TdxSpreadSheetSharedImages.Create;
end;

function TdxCustomSpreadSheet.CreateSharedStringTable: TdxSpreadSheetSharedStringTable;
begin
  Result := TdxSpreadSheetSharedStringTable.Create(CellStyles.Fonts);
end;

function TdxCustomSpreadSheet.CreateStyles: TdxSpreadSheetStyles;
begin
  Result := TdxSpreadSheetStyles.Create(Self);
end;

function TdxCustomSpreadSheet.CreateViewInfo: TdxSpreadSheetViewInfo;
begin
  Result := TdxSpreadSheetViewInfo.Create(Self);
end;

procedure TdxCustomSpreadSheet.CreateSubClasses;
begin
  FListeners := TInterfaceList.Create();
  FStyles := CreateStyles;
  FFormulaController := CreateFormulaController;
  FLockedStatePaintHelper := CreateLockedStatePaintHelper;
  FOptionsLockedStateImage := CreateOptionsLockedStateImage;
  FOptionsBehavior := CreateOptionsBehavior;
  FOptionsView := CreateOptionsView;
  FCellStyles := CreateCellStyles;
  FDefaultCellStyle := CreateDefaultCellStyle;
  FStringTable := CreateSharedStringTable;
  FSharedImages := CreateSharedImages;
  FSheets := TcxObjectList.Create;
  FVisibleSheets := TList<TdxSpreadSheetCustomView>.Create;
  FFormatSettings := CreateFormatSettings;
  FDefinedNames := CreateDefinedNames;
  FExternalLinks := CreateExternalLinks;
  FPageControl := CreatePageControl;
  FHistory := CreateHistory;
  FViewInfo := CreateViewInfo;

  dxResourceStringsRepository.AddListener(FormulaController);
end;

procedure TdxCustomSpreadSheet.DefineProperties(Filer: TFiler);
begin
  inherited DefineProperties(Filer);
  Filer.DefineBinaryProperty('Data', ReadBinaryData, WriteBinaryData, True);
end;

procedure TdxCustomSpreadSheet.DestroySubClasses;
begin
  History.Lock;
  dxResourceStringsRepository.RemoveListener(FormulaController);
  FreeAndNil(FSheets);
  FreeAndNil(FVisibleSheets);
  FreeAndNil(FDefinedNames);
  FreeAndNil(FFormulaController);
  FreeAndNil(FStringTable);
  FreeAndNil(FSharedImages);
  FreeAndNil(FDefaultCellStyle);
  FreeAndNil(FLockedStatePaintHelper);
  FreeAndNil(FOptionsLockedStateImage);
  FreeAndNil(FCellStyles);
  FreeAndNil(FOptionsView);
  FreeAndNil(FOptionsBehavior);
  FreeAndNil(FFormatSettings);
  FreeAndNil(FDefinedNames);
  FreeAndNil(FExternalLinks);
  FreeAndNil(FViewInfo);
  FreeAndNil(FStyles);
  FreeAndNil(FPageControl);
  FreeAndNil(FHistory);
  FListeners.Free;
end;

procedure TdxCustomSpreadSheet.DblClick;
var
  AController: TdxSpreadSheetCustomController;
begin
  AController := ActiveController;
  if (AController <> nil) or ControllerFromPoint(GetMouseCursorClientPos, AController) then
    AController.DblClick;
  inherited DblClick;
end;

function TdxCustomSpreadSheet.DoActiveCellChanging(AView: TdxSpreadSheetTableView; const ANewActiveCell: TPoint): Boolean;
begin
  Result := True;
  if not (csReading in ComponentState) then
  begin
    if Assigned(OnActiveCellChanging) then
      OnActiveCellChanging(AView, ANewActiveCell, Result);
  end;
end;

procedure TdxCustomSpreadSheet.DoActiveSheetChanged;
begin
  LayoutChanged;
  BoundsChanged;
  dxCallNotify(OnActiveSheetChanged, Self);
end;

procedure TdxCustomSpreadSheet.DoCompare(AView: TdxSpreadSheetCustomView;
  const AData1, AData2: TdxSpreadSheetCellData; var Compare: Integer);
begin
  if Assigned(OnCompare) then
    OnCompare(AView, AData1, AData2, Compare);
end;

procedure TdxCustomSpreadSheet.DoContextPopup(MousePos: TPoint; var Handled: Boolean);
begin
  inherited DoContextPopup(MousePos, Handled);
  if not Handled then
  begin
    if (MousePos.X = -1) and (MousePos.Y = -1) or (ActiveController = nil) then
      Handled := (ActiveSheet <> nil) and ActiveSheet.Controller.ContextPopup(GetMouseCursorClientPos)
    else
      Handled := ActiveController.ContextPopup(MousePos);
  end;
end;

procedure TdxCustomSpreadSheet.DoCustomDrawTableViewCell(Sender: TdxSpreadSheetTableView; ACanvas: TcxCanvas;
  AViewInfo: TdxSpreadSheetTableViewCellViewInfo; var AHandled: Boolean);
begin
  AHandled := False;
  if Assigned(FOnCustomDrawTableViewCell) then
    FOnCustomDrawTableViewCell(Sender, ACanvas, AViewInfo, AHandled);
end;

procedure TdxCustomSpreadSheet.DoCustomDrawTableViewCommonCell(Sender: TdxSpreadSheetTableView; ACanvas: TcxCanvas;
  AViewInfo: TdxSpreadSheetTableViewCustomCellViewInfo; var AHandled: Boolean);
begin
  AHandled := False;
  if Assigned(FOnCustomDrawTableViewCommonCell) then
    FOnCustomDrawTableViewCommonCell(Sender, ACanvas, AViewInfo, AHandled);
end;

procedure TdxCustomSpreadSheet.DoCustomDrawTableViewHeaderCell(Sender: TdxSpreadSheetTableView; ACanvas: TcxCanvas;
  AViewInfo: TdxSpreadSheetTableViewHeaderCellViewInfo; var AHandled: Boolean);
begin
  AHandled := False;
  if Assigned(FOnCustomDrawTableViewHeaderCell) then
    FOnCustomDrawTableViewHeaderCell(Sender, ACanvas, AViewInfo, AHandled);
end;

procedure TdxCustomSpreadSheet.DoDataChanged;
begin
  dxCallNotify(OnDataChanged, Self);
end;

procedure TdxCustomSpreadSheet.DoEditChanged(AView: TdxSpreadSheetCustomView);
begin
  if Assigned(OnEditChanged) then
    OnEditChanged(AView);
end;

procedure TdxCustomSpreadSheet.DoEdited(AView: TdxSpreadSheetCustomView);
begin
  if Assigned(OnEdited) then
    OnEdited(AView);
end;

procedure TdxCustomSpreadSheet.DoEditing(AView: TdxSpreadSheetCustomView;
  var AProperties: TcxCustomEditProperties; var ACanEdit: Boolean);
begin
  if Assigned(OnEditing) then
    OnEditing(AView, AProperties, ACanEdit);
end;

procedure TdxCustomSpreadSheet.DoEditValueChanged(AView: TdxSpreadSheetCustomView);
begin
  if Assigned(OnEditValueChanged) then
    OnEditValueChanged(AView);
end;

procedure TdxCustomSpreadSheet.DoHistoryChanged;
begin
  if Assigned(OnHistoryChanged) and not (IsLoading or IsDestroying) then
    OnHistoryChanged(Self);
end;

procedure TdxCustomSpreadSheet.DoInitEdit(AView: TdxSpreadSheetCustomView; AEdit: TcxCustomEdit);
begin
  if Assigned(OnInitEdit) then
    OnInitEdit(AView, AEdit);
end;

procedure TdxCustomSpreadSheet.DoInitEditValue(AView: TdxSpreadSheetCustomView; AEdit: TcxCustomEdit; var AValue: Variant);
begin
  if Assigned(OnInitEditValue) then
    OnInitEditValue(AView, AEdit, AValue);
end;

procedure TdxCustomSpreadSheet.DoLayoutChanged;
begin
  if not IsLoading then
    CallNotify(OnLayoutChanged, Self);
end;

procedure TdxCustomSpreadSheet.DoSelectionChanged(AView: TdxSpreadSheetCustomView);
begin
  if not IsLoading then
    CallNotify(OnSelectionChanged, AView);
end;

procedure TdxCustomSpreadSheet.DoAddSheet(ASheet: TdxSpreadSheetCustomView);
begin
  History.Lock;
  try
    FSheets.Add(ASheet);
    if ASheet.Visible then
      FVisibleSheets.Add(ASheet);
  finally
    History.Unlock;
  end;
end;

procedure TdxCustomSpreadSheet.DoChangeSheetVisibility(ASheet: TdxSpreadSheetCustomView);
begin
  BeginUpdate;
  try
    UpdateVisibleSheetList;
    ActiveSheetIndex := ActiveSheetIndex;
    LayoutChanged;
  finally
    EndUpdate;
  end;
end;

procedure TdxCustomSpreadSheet.DoRemoveSheet(ASheet: TdxSpreadSheetCustomView);
begin
  if not IsDestroying then
  begin
    History.Lock;
    try
      BeginUpdate;
      try
        FVisibleSheets.Remove(ASheet);
        FSheets.Remove(ASheet);
        ActiveSheetIndex := ActiveSheetIndex;
        LayoutChanged;
      finally
        EndUpdate;
      end;
    finally
      History.Unlock;
    end;
  end;
end;

function TdxCustomSpreadSheet.CanFocusOnClick: Boolean;
begin
  Result := inherited CanFocusOnClick and ((ActiveController = nil) or ActiveController.CanFocusOnClick);
end;

function TdxCustomSpreadSheet.DoMouseWheel(Shift: TShiftState; WheelDelta: Integer; MousePos: TPoint): Boolean;
var
  AController: TdxSpreadSheetCustomController;
begin
  Result := inherited DoMouseWheel(Shift, WheelDelta, MousePos);
  if not Result then
  begin
    AController := ActiveController;
    if (AController <> nil) or ControllerFromPoint(GetMouseCursorClientPos, AController) then
      Result := AController.MouseWheel(Shift, WheelDelta, ScreenToClient(MousePos));
  end;
end;

procedure TdxCustomSpreadSheet.DoPaint;
begin
  inherited DoPaint;
  if LockCount = 0 then
  begin
    if PageControl.Visible then
      PageControl.ViewInfo.Draw(Canvas);
    Canvas.FillRect(ClientBounds, ViewInfo.BackgroundParams);
    if ActiveSheet <> nil then
      ActiveSheet.ViewInfo.Draw(Canvas);
  end;
  Canvas.FillRect(ClientBounds, ViewInfo.BackgroundParams);
end;

function TdxCustomSpreadSheet.GetClientBounds: TRect;
begin
  Result := inherited GetClientBounds;
  if not HScrollBarVisible and PageControl.Visible then
    Dec(Result.Bottom, PageControl.ViewInfo.MeasureHeight);
end;

function TdxCustomSpreadSheet.GetCurrentCursor(X, Y: Integer): TCursor;
var
  AController: TdxSpreadSheetCustomController;
begin
  Result := inherited GetCurrentCursor(X, Y);
  if ControllerFromPoint(Point(X, Y), AController) then
    Result := AController.GetCursor(Point(X, Y));
end;

function TdxCustomSpreadSheet.GetHScrollBarBounds: TRect;
begin
  Result := inherited GetHScrollBarBounds;
  Inc(Result.Left, PageControl.ViewInfo.MeasureWidth);
  Result.Top := Result.Bottom - PageControl.ViewInfo.MeasureHeight;
end;

function TdxCustomSpreadSheet.GetSizeGripBounds: TRect;
begin
  Result := cxRect(ClientBounds.BottomRight, ClientBounds.BottomRight);
  if HScrollBarVisible or PageControl.Visible then
    Result.Bottom := Result.Top + PageControl.ViewInfo.MeasureHeight;
  if VScrollBarVisible then
    Result.Right := Result.Left + VScrollBar.Width;
end;

function TdxCustomSpreadSheet.GetDragAndDropObjectClass: TcxDragAndDropObjectClass;
begin
  if (ActiveController <> nil) and (ActiveController.HitTest.HitObject <> nil) then
    Result := ActiveController.HitTest.GetDragAndDropObjectClass
  else
    Result := nil;

  if Result = nil then
    Result := inherited GetDragAndDropObjectClass;
end;

procedure TdxCustomSpreadSheet.InitScrollBarsParameters;
begin
  if ActiveSheet <> nil then
    ActiveSheet.InitScrollBarsParameters
  else
  begin
    SetScrollBarInfo(sbHorizontal, 0, 1, 1, 1, 0, False, True);
    SetScrollBarInfo(sbVertical, 0, 1, 1, 1, 0, False, True);
  end;
end;

function TdxCustomSpreadSheet.IsMouseWheelHandleNeeded(Shift: TShiftState; WheelDelta: Integer; MousePos: TPoint): Boolean;
begin
  Result := False;
end;

function TdxCustomSpreadSheet.IsSizeGripVisible: Boolean;
begin
  Result := inherited IsSizeGripVisible or (PageControl.Visible and VScrollBarVisible);
end;

procedure TdxCustomSpreadSheet.Loaded;
begin
  inherited Loaded;
  CheckChanges;
  if ActiveSheet <> nil then
    DoSelectionChanged(ActiveSheet);
end;

procedure TdxCustomSpreadSheet.LookAndFeelChanged(Sender: TcxLookAndFeel; AChangedValues: TcxLookAndFeelValues);
begin
  LayoutChanged;
end;

procedure TdxCustomSpreadSheet.KeyDown(var Key: Word; Shift: TShiftState);
begin
  inherited KeyDown(Key, Shift);
  if ActiveSheet <> nil then
    ActiveSheet.Controller.KeyDown(Key, Shift);
end;

procedure TdxCustomSpreadSheet.KeyUp(var Key: Word; Shift: TShiftState);
begin
  inherited KeyUp(Key, Shift);
  if ActiveSheet <> nil then
    ActiveSheet.Controller.KeyUp(Key, Shift);
  if Key = VK_BACK then
    Key := 0;
end;

procedure TdxCustomSpreadSheet.KeyPress(var Key: Char);
begin
  inherited KeyPress(Key);
  if ActiveSheet <> nil then
    ActiveSheet.Controller.KeyPress(Key)
end;

procedure TdxCustomSpreadSheet.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  AController: TdxSpreadSheetCustomController;
begin
  UpdateHitTests(X, Y);
  if ControllerFromPoint(X, Y, AController) then
    ActiveController := AController
  else
    ActiveController := nil;
  inherited MouseDown(Button, Shift, X, Y);
  if ActiveController <> nil then
    ActiveController.MouseDown(Button, Shift, X, Y);
  UpdateHitTests(X, Y);
end;

procedure TdxCustomSpreadSheet.MouseMove(Shift: TShiftState; X, Y: Integer);
var
  AController: TdxSpreadSheetCustomController;
begin
  UpdateHitTests(X, Y);
  inherited MouseMove(Shift, X, Y);
  if ControllerFromPoint(X, Y, AController) then
    AController.MouseMove(Shift, X, Y);
  UpdateHitTests(X, Y);
end;

procedure TdxCustomSpreadSheet.MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  AController: TdxSpreadSheetCustomController;
begin
  UpdateHitTests(X, Y);
  inherited MouseUp(Button, Shift, X, Y);
  if ControllerFromPoint(X, Y, AController) then
    AController.MouseUp(Button, Shift, X, Y);
  ActiveController := nil;
  UpdateHitTests(X, Y);
end;

procedure TdxCustomSpreadSheet.NotifyListeners;
var
  I: Integer;
  AListener: IdxSpreadSheetListener;
begin
  for I := Listeners.Count - 1 downto 0 do
    if Supports(Listeners[I], IdxSpreadSheetListener, AListener) then
      AListener.DataChanged(Self);
end;

procedure TdxCustomSpreadSheet.Pack;
var
  I: Integer;
begin
  for I := 0 to SheetCount - 1 do
    Sheets[I].Pack;
end;

procedure TdxCustomSpreadSheet.Scroll(AScrollBarKind: TScrollBarKind; AScrollCode: TScrollCode; var AScrollPos: Integer);
begin
  if ActiveSheet <> nil then
    ActiveSheet.Scroll(AScrollBarKind, AScrollCode, AScrollPos);
end;

procedure TdxCustomSpreadSheet.SetPaintRegion;
begin
  inherited SetPaintRegion;
  if PageControl.Visible then
    Canvas.SetClipRegion(TcxRegion.Create(PageControl.ViewInfo.Bounds), roAdd);
end;

procedure TdxCustomSpreadSheet.SetSheetIndex(AView: TdxSpreadSheetCustomView; AIndex: Integer);
var
  AActive: Boolean;
begin
  AActive := AView.Active;
  FSheets.Remove(AView);
  FSheets.Insert(AIndex, AView);
  UpdateVisibleSheetList;
  if AActive then
    FActiveSheetIndex := FSheets.IndexOf(AView);
  LayoutChanged;
end;

function TdxCustomSpreadSheet.StartDragAndDrop(const P: TPoint): Boolean;
var
  AController: TdxSpreadSheetCustomController;
begin
  Result := ControllerFromPoint(P, AController) and AController.HitTest.CanDrag(P);
end;

procedure TdxCustomSpreadSheet.StyleChanged(Sender: TObject);
begin
end;

procedure TdxCustomSpreadSheet.UpdateHitTests(X, Y: Integer);
begin
  UpdateHitTests(Point(X, Y));
end;

procedure TdxCustomSpreadSheet.UpdateVisibleSheetList;
var
  ASheet: TdxSpreadSheetCustomView;
  I: Integer;
begin
  FVisibleSheets.Clear;
  FVisibleSheets.Capacity := SheetCount;
  for I := 0 to SheetCount - 1 do
  begin
    ASheet := Sheets[I];
    if ASheet.Visible then
      FVisibleSheets.Add(ASheet);
  end;
end;

procedure TdxCustomSpreadSheet.UpdateHitTests(const P: TPoint);
begin
  if ActiveSheet <> nil then
    ActiveSheet.HitTest.Calculate(P);
  PageControl.HitTest.Calculate(P)
end;

function TdxCustomSpreadSheet.ValidateSheetCaption(ACaption: TdxUnicodeString): TdxUnicodeString;
var
  AFormatString: TdxUnicodeString;
  AIndex: Integer;
begin
  if ACaption = '' then
  begin
    AFormatString := cxGetResourceString(@sdxDefaultSheetCaption);
    AIndex := 1;
    while GetSheetByName(Format(AFormatString, [AIndex])) <> nil do
      Inc(AIndex);
    ACaption := Format(AFormatString, [AIndex]);
  end;
  Result := ACaption;
end;

function TdxCustomSpreadSheet.DoPrepareLockedStateImage: Boolean;
begin
  Result := False;
  if Assigned(OnPrepareLockedStateImage) then
    OnPrepareLockedStateImage(Self, LockedStatePaintHelper.Bitmap, Result);
end;

function TdxCustomSpreadSheet.GetLockedStateImage: TcxBitmap32;
begin
  Result := LockedStatePaintHelper.GetImage;
end;

function TdxCustomSpreadSheet.GetLockedStateTopmostControl: TcxControl;
begin
  Result := Self;
end;

procedure TdxCustomSpreadSheet.UpdateLockedStateFont(AFont: TFont);
begin
  OptionsLockedStateImage.UpdateFont(AFont);
end;

procedure TdxCustomSpreadSheet.InternalLoadFromStream(AStream: TStream;
  AFormat: TdxSpreadSheetCustomFormatClass; AProgressEvent: TdxSpreadSheetProgressEvent);

  procedure ExchangeFormatSettings(var AFormatSettings: TdxSpreadSheetFormatSettings);
  begin
    if AFormatSettings <> nil then
      ExchangePointers(AFormatSettings, FFormatSettings);
    FormulaController.TranslationChanged;
  end;

var
  AFormatSettings: TdxSpreadSheetFormatSettings;
  AMemStream: TMemoryStream;
  AReader: TdxSpreadSheetCustomReader;
begin
  if AFormat.GetReader = nil then
    raise EdxSpreadSheetFormatError.Create(cxGetResourceString(@sdxErrorUnsupportedDocumentFormat));

  BeginUpdate(lsimImmediate);
  ShowHourglassCursor;
  try
    History.Lock;
    Include(FState, sssReading);
    try
      History.Clear;
      AFormatSettings := AFormat.CreateFormatSettings;
      try
        ExchangeFormatSettings(AFormatSettings);
        AMemStream := TMemoryStream.Create;
        try
          InternalSaveToStream(AMemStream, TdxSpreadSheetBinaryFormat, nil);
          try
            ClearAll;
            AReader := AFormat.GetReader.Create(Self, AStream);
            try
              AReader.OnProgress := AProgressEvent;
              AReader.ReadData;
            finally
              AReader.Free;
            end;
            Pack;
          except
            AMemStream.Position := 0;
            InternalLoadFromStream(AMemStream, TdxSpreadSheetBinaryFormat, nil);
            raise;
          end;
        finally
          AMemStream.Free;
        end;
      finally
        ExchangeFormatSettings(AFormatSettings);
        AFormatSettings.Free;
      end;
      AfterLoad;
    finally
      Exclude(FState, sssReading);
      History.Unlock;
    end;
  finally
    HideHourglassCursor;
    EndUpdate;
  end;
end;

procedure TdxCustomSpreadSheet.InternalSaveToStream(AStream: TStream;
  AFormat: TdxSpreadSheetCustomFormatClass; AProgressEvent: TdxSpreadSheetProgressEvent);
var
  AFormatSettings: TdxSpreadSheetFormatSettings;
  AWriter: TdxSpreadSheetCustomWriter;
begin
  if AFormat = nil then
    AFormat := TdxSpreadSheetBinaryFormat;

  if AFormat.GetWriter = nil then
    raise EdxSpreadSheetFormatError.Create(cxGetResourceString(@sdxErrorUnsupportedDocumentFormat));

  BeginUpdate(lsimImmediate);
  try
    Include(FState, sssWriting);
    try
      Pack;
      AFormatSettings := AFormat.CreateFormatSettings;
      try
        if AFormatSettings <> nil then
          ExchangePointers(AFormatSettings, FFormatSettings);
        AWriter := AFormat.GetWriter.Create(Self, AStream);
        try
          AWriter.OnProgress := AProgressEvent;
          AWriter.WriteData;
        finally
          AWriter.Free;
        end;
      finally
        if AFormatSettings <> nil then
          ExchangePointers(AFormatSettings, FFormatSettings);
        AFormatSettings.Free;
      end;
    finally
      Exclude(FState, sssWriting);
    end;
  finally
    EndUpdate;
  end;
end;

function TdxCustomSpreadSheet.GetActiveSheet: TdxSpreadSheetCustomView;
begin
  if (ActiveSheetIndex >= 0) and (ActiveSheetIndex < SheetCount) then
    Result := Sheets[ActiveSheetIndex]
  else
    Result := nil;
end;

function TdxCustomSpreadSheet.GetActiveSheetAsTable: TdxSpreadSheetTableView;
begin
  Result := ActiveSheet as TdxSpreadSheetTableView;
end;

function TdxCustomSpreadSheet.GetIsLocked: Boolean;
begin
  Result := IsLoading or IsDestroying or (LockCount > 0); 
end;

function TdxCustomSpreadSheet.GetSheet(AIndex: Integer): TdxSpreadSheetCustomView;
begin
  Result := TdxSpreadSheetCustomView(FSheets[AIndex]); 
end;

function TdxCustomSpreadSheet.GetSheetCount: Integer;
begin
  Result := FSheets.Count;
end;

function TdxCustomSpreadSheet.GetVisibleSheet(Index: Integer): TdxSpreadSheetCustomView;
begin
  Result := TdxSpreadSheetCustomView(FVisibleSheets[Index]);
end;

function TdxCustomSpreadSheet.GetVisibleSheetCount: Integer;
begin
  Result := FVisibleSheets.Count;
end;

procedure TdxCustomSpreadSheet.SetActiveSheet(AValue: TdxSpreadSheetCustomView);
begin
  if AValue <> nil then
    ActiveSheetIndex := AValue.Index;
end;

procedure TdxCustomSpreadSheet.SetActiveSheetIndex(AValue: Integer);

  function GetNearAvailableIndex(AIndex: Integer): Integer;
  var
    I: Integer;
  begin
    Result := -1;
    for I := 0 to VisibleSheetCount - 1 do
    begin
      if AIndex >= VisibleSheets[I].Index then
        Result := VisibleSheets[I].Index;
    end;
    if (Result < 0) and (VisibleSheetCount > 0) then
    begin
      if AIndex > FVisibleSheets.Last.Index then
        Result := FVisibleSheets.Last.Index
      else
        Result := FVisibleSheets.First.Index;
    end;
  end;

begin
  AValue := GetNearAvailableIndex(AValue);
  if AValue <> ActiveSheetIndex then
  begin
    FActiveSheetIndex := AValue;
    DoActiveSheetChanged;
  end;
end;

procedure TdxCustomSpreadSheet.SetOptionsBehavior(AValue: TdxSpreadSheetOptionsBehavior);
begin
  FOptionsBehavior.Assign(AValue);
end;

procedure TdxCustomSpreadSheet.SetOptionsLockedStateImage(AValue: TdxSpreadSheetLockedStateImageOptions);
begin
  FOptionsLockedStateImage.Assign(AValue);
end;

procedure TdxCustomSpreadSheet.SetOptionsView(AValue: TdxSpreadSheetOptionsView);
begin
  FOptionsView.Assign(AValue);
end;

procedure TdxCustomSpreadSheet.SetPageControl(AValue: TdxSpreadSheetPageControl);
begin
  FPageControl.Assign(AValue);
end;

procedure TdxCustomSpreadSheet.SetStyle(AValue: TdxSpreadSheetStyles);
begin
  FStyles.Assign(AValue);
end;

procedure TdxCustomSpreadSheet.ReadBinaryData(AStream: TStream);
var
  AMemStream: TMemoryStream;
  ASize: Integer;
begin
  AMemStream := TMemoryStream.Create;
  try
    AStream.ReadBuffer(ASize, SizeOf(ASize));
    AMemStream.Size := ASize;
    AStream.ReadBuffer(AMemStream.Memory^, AMemStream.Size);
    AMemStream.Position := 0;
    InternalLoadFromStream(AMemStream, TdxSpreadSheetBinaryFormat, nil);
  finally
    AMemStream.Free;
  end;
end;

procedure TdxCustomSpreadSheet.WMCopy(var Message: TMessage);
begin
  if ActiveSheet is TdxSpreadSheetTableView then
    TdxSpreadSheetTableView(ActiveSheet).CopyToClipboard;
end;

procedure TdxCustomSpreadSheet.WMCut(var Message: TMessage);
begin
  if ActiveSheet is TdxSpreadSheetTableView then
    TdxSpreadSheetTableView(ActiveSheet).CutToClipboard;
end;

procedure TdxCustomSpreadSheet.WMPaste(var Message: TMessage);
begin
  if ActiveSheet is TdxSpreadSheetTableView then
    TdxSpreadSheetTableView(ActiveSheet).PasteFromClipboard;
end;

procedure TdxCustomSpreadSheet.WriteBinaryData(AStream: TStream);
var
  AMemStream: TMemoryStream;
  ASize: Integer;
begin
  AMemStream := TMemoryStream.Create;
  try
    InternalSaveToStream(AMemStream, TdxSpreadSheetBinaryFormat, nil);
    ASize := AMemStream.Size;
    AStream.WriteBuffer(ASize, SizeOf(ASize));
    AStream.WriteBuffer(AMemStream.Memory^, ASize);
  finally
    AMemStream.Free;
  end;
end;

{ TdxSpreadSheetDefaultCellStyle }

constructor TdxSpreadSheetDefaultCellStyle.Create(AOwner: TObject);
begin
  inherited Create(AOwner);
  FHandle := CellStyles.DefaultStyle;
end;

function TdxSpreadSheetDefaultCellStyle.GetSpreadSheet: TdxCustomSpreadSheet;
begin
  Result := TdxCustomSpreadSheet(Owner);
end;

procedure TdxSpreadSheetDefaultCellStyle.Changed;
begin
  SpreadSheet.LayoutChanged;
end;

procedure TdxSpreadSheetDefaultCellStyle.CloneHandle;
begin
  // do nothing
end;

procedure TdxSpreadSheetDefaultCellStyle.SetHandle(const AHandle: TdxSpreadSheetCellStyleHandle);
begin
  if CellStyles.DefaultStyle <> AHandle then
    CellStyles.DefaultStyle.Assign(AHandle);
end;

procedure TdxSpreadSheetDefaultCellStyle.ReleaseHandle;
begin
  // do nothing
end;

procedure TdxSpreadSheetDefaultCellStyle.ReplaceHandle;
begin
  // do nothing
end;

initialization
  Screen.Cursors[crdxSpreadSheetRotate] := LoadCursor(HInstance, 'DXSPREADSHEET_ROTATECURSOR');
  Screen.Cursors[crdxSpreadSheetRotation] := LoadCursor(HInstance, 'DXSPREADSHEET_ROTATIONCURSOR');
  Screen.Cursors[crdxSpreadSheetDownArrow] := LoadCursor(HInstance, 'DXSPREADSHEET_DOWNARROW');
  Screen.Cursors[crdxSpreadSheetLeftArrow] := LoadCursor(HInstance, 'DXSPREADSHEET_LEFTARROW');

  RegisterClasses([TdxSpreadSheetTableView, TdxSpreadSheetContainer, TdxSpreadSheetShapeContainer, TdxSpreadSheetPictureContainer]);

finalization
  FreeAndNil(FSpreadSheetFormats);
end.
