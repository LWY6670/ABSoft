{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressSpreadSheet                                       }
{                                                                    }
{           Copyright (c) 2001-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSSPREADSHEET CONTROL AND ALL    }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY. }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxSpreadSheetPrinting;

{$I cxVer.Inc}

interface

uses
  Types, Classes, cxGeometry, dxCore, Generics.Collections;

type

  { TdxSpreadSheetTableViewOptionsPrintCustomPersistent }

  TdxSpreadSheetTableViewOptionsPrintCustomPersistent = class(TPersistent)
  public
    procedure AfterConstruction; override;
    procedure Reset; virtual; abstract;
  end;

  { TdxSpreadSheetTableViewOptionsPrintHeaderFooterText }

  TdxSpreadSheetTableViewOptionsPrintHeaderFooterText = class(TdxSpreadSheetTableViewOptionsPrintCustomPersistent)
  strict private const
    SectionCount = 3;
  strict private
    FAssigned: Boolean;
    FValues: array [0..SectionCount - 1] of string;

    function GetValue(const Index: Integer): string;
    procedure SetValue(const Index: Integer; const Value: string);
  public
    procedure Assign(Source: TPersistent); override;
    procedure Reset; override;
  published
    property Assigned: Boolean read FAssigned write FAssigned default False;
    property LeftSection: string index 0 read GetValue write SetValue;
    property CenterSection: string index 1 read GetValue write SetValue;
    property RightSection: string index 2 read GetValue write SetValue;
  end;

  { TdxSpreadSheetTableViewOptionsPrintHeaderFooter }

  TdxSpreadSheetTableViewOptionsPrintHeaderFooter = class(TdxSpreadSheetTableViewOptionsPrintCustomPersistent)
  strict private
    FAlignWithMargins: TdxDefaultBoolean;
    FCommonFooter: TdxSpreadSheetTableViewOptionsPrintHeaderFooterText;
    FCommonHeader: TdxSpreadSheetTableViewOptionsPrintHeaderFooterText;
    FEvenPagesFooter: TdxSpreadSheetTableViewOptionsPrintHeaderFooterText;
    FEvenPagesHeader: TdxSpreadSheetTableViewOptionsPrintHeaderFooterText;
    FFirstPageFooter: TdxSpreadSheetTableViewOptionsPrintHeaderFooterText;
    FFirstPageHeader: TdxSpreadSheetTableViewOptionsPrintHeaderFooterText;
    FScaleWithDocument: TdxDefaultBoolean;

    function GetAssigned: Boolean;
    procedure SetCommonFooter(AValue: TdxSpreadSheetTableViewOptionsPrintHeaderFooterText);
    procedure SetCommonHeader(AValue: TdxSpreadSheetTableViewOptionsPrintHeaderFooterText);
    procedure SetEvenPagesFooter(AValue: TdxSpreadSheetTableViewOptionsPrintHeaderFooterText);
    procedure SetEvenPagesHeader(AValue: TdxSpreadSheetTableViewOptionsPrintHeaderFooterText);
    procedure SetFirstPageFooter(AValue: TdxSpreadSheetTableViewOptionsPrintHeaderFooterText);
    procedure SetFirstPageHeader(AValue: TdxSpreadSheetTableViewOptionsPrintHeaderFooterText);
  public
    constructor Create;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    procedure Reset; override;

    property Assigned: Boolean read GetAssigned;
  published
    property AlignWithMargins: TdxDefaultBoolean read FAlignWithMargins write FAlignWithMargins default bDefault;
    property ScaleWithDocument: TdxDefaultBoolean read FScaleWithDocument write FScaleWithDocument default bDefault;
    property CommonFooter: TdxSpreadSheetTableViewOptionsPrintHeaderFooterText read FCommonFooter write SetCommonFooter;
    property CommonHeader: TdxSpreadSheetTableViewOptionsPrintHeaderFooterText read FCommonHeader write SetCommonHeader;
    property EvenPagesFooter: TdxSpreadSheetTableViewOptionsPrintHeaderFooterText read FEvenPagesFooter write SetEvenPagesFooter;
    property EvenPagesHeader: TdxSpreadSheetTableViewOptionsPrintHeaderFooterText read FEvenPagesHeader write SetEvenPagesHeader;
    property FirstPageFooter: TdxSpreadSheetTableViewOptionsPrintHeaderFooterText read FFirstPageFooter write SetFirstPageFooter;
    property FirstPageHeader: TdxSpreadSheetTableViewOptionsPrintHeaderFooterText read FFirstPageHeader write SetFirstPageHeader;
  end;

  { TdxSpreadSheetTableViewOptionsPrintRect }

  TdxSpreadSheetTableViewOptionsPrintRect = class(TcxRect)
  strict private
    FAssigned: Boolean;
  protected
    procedure DoChange; override;
  public
    procedure Assign(Source: TPersistent); override;
    procedure Reset;
  published
    property Assigned: Boolean read FAssigned write FAssigned default False;
  end;

  { TdxSpreadSheetTableViewOptionsPrintPagePaper }

  TdxSpreadSheetTableViewOptionsPrintPagePaper = class(TdxSpreadSheetTableViewOptionsPrintCustomPersistent)
  strict private
    FAssigned: Boolean;
    FCustomSize: TdxPointDoublePersistent;
    FSizeID: Integer;

    procedure CustomSizeChangeHandler(Sender: TObject);
    procedure SetCustomSize(AValue: TdxPointDoublePersistent);
    procedure SetSizeID(AValue: Integer);
  public
    constructor Create;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    procedure Reset; override;
  published
    property Assigned: Boolean read FAssigned write FAssigned default False;
    property CustomSize: TdxPointDoublePersistent read FCustomSize write SetCustomSize;
    property SizeID: Integer read FSizeID write SetSizeID default 0;
  end;

  { TdxSpreadSheetTableViewOptionsPrintPageMargins }

  TdxSpreadSheetTableViewOptionsPrintPageMargins = class(TdxSpreadSheetTableViewOptionsPrintCustomPersistent)
  strict private const
    ValuesCount = 6;
  strict private
    FAssigned: Boolean;
    FValues: array[0..ValuesCount - 1] of Double;

    function GetValue(AIndex: Integer): Double;
    function IsValueStored(AIndex: Integer): Boolean;
    procedure SetValue(AIndex: Integer; const AValue: Double);
  public
    procedure Assign(Source: TPersistent); override;
    procedure Reset; override;
  published
    property Assigned: Boolean read FAssigned write FAssigned default False;
    property Footer: Double index 0 read GetValue write SetValue stored IsValueStored;
    property Header: Double index 1 read GetValue write SetValue stored IsValueStored;
    property Bottom: Double index 2 read GetValue write SetValue stored IsValueStored;
    property Left: Double index 3 read GetValue write SetValue stored IsValueStored;
    property Right: Double index 4 read GetValue write SetValue stored IsValueStored;
    property Top: Double index 5 read GetValue write SetValue stored IsValueStored;
  end;

  { TdxSpreadSheetTableViewOptionsPrintPage }

  TdxSpreadSheetTableViewOptionsPrintPageOrientation = (oppoDefault, oppoLandscape, oppoPortrait);
  TdxSpreadSheetTableViewOptionsPrintPageScaleMode = (oppsmDefault, oppsmAdjustToScale, oppsmFitToPage);

  TdxSpreadSheetTableViewOptionsPrintPage = class(TdxSpreadSheetTableViewOptionsPrintCustomPersistent)
  strict private
    FFirstPageNumber: Cardinal;
    FFitToHeight: Cardinal;
    FFitToWidth: Cardinal;
    FMargins: TdxSpreadSheetTableViewOptionsPrintPageMargins;
    FOrientation: TdxSpreadSheetTableViewOptionsPrintPageOrientation;
    FPaper: TdxSpreadSheetTableViewOptionsPrintPagePaper;
    FScale: Integer;
    FScaleMode: TdxSpreadSheetTableViewOptionsPrintPageScaleMode;

    procedure SetFitToHeight(AValue: Cardinal);
    procedure SetFitToWidth(AValue: Cardinal);
    procedure SetMargins(AValue: TdxSpreadSheetTableViewOptionsPrintPageMargins);
    procedure SetPaper(AValue: TdxSpreadSheetTableViewOptionsPrintPagePaper);
    procedure SetScale(AValue: Integer);
  public
    constructor Create;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    procedure Reset; override;
  published
    property FirstPageNumber: Cardinal read FFirstPageNumber write FFirstPageNumber default 0;
    property FitToHeight: Cardinal read FFitToHeight write SetFitToHeight default 0;
    property FitToWidth: Cardinal read FFitToWidth write SetFitToWidth default 0;
    property Margins: TdxSpreadSheetTableViewOptionsPrintPageMargins read FMargins write SetMargins;
    property Orientation: TdxSpreadSheetTableViewOptionsPrintPageOrientation read FOrientation write FOrientation default oppoDefault;
    property Paper: TdxSpreadSheetTableViewOptionsPrintPagePaper read FPaper write SetPaper;
    property Scale: Integer read FScale write SetScale default 100;
    property ScaleMode: TdxSpreadSheetTableViewOptionsPrintPageScaleMode read FScaleMode write FScaleMode default oppsmDefault;
  end;

  { TdxSpreadSheetTableViewOptionsPrintPagination }

  TdxSpreadSheetTableViewOptionsPrintPagination = class(TdxSpreadSheetTableViewOptionsPrintCustomPersistent)
  strict private
    FColumnPageBreaks: TList<Cardinal>;
    FRowPageBreaks: TList<Cardinal>;

    procedure SetColumnPageBreaks(AValue: TList<Cardinal>);
    procedure SetRowPageBreaks(AValue: TList<Cardinal>);
  public
    constructor Create;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    procedure Reset; override;

    property ColumnPageBreaks: TList<Cardinal> read FColumnPageBreaks write SetColumnPageBreaks;
    property RowPageBreaks: TList<Cardinal> read FRowPageBreaks write SetRowPageBreaks;
  end;

  { TdxSpreadSheetTableViewOptionsPrintPrinting }

  TdxSpreadSheetTableViewOptionsPrintPrintingPageOrder = (opppDefault, opppDownThenOver, opppOverThenDown);

  TdxSpreadSheetTableViewOptionsPrintPrinting = class(TdxSpreadSheetTableViewOptionsPrintCustomPersistent)
  strict private
    FBlackAndWhite: TdxDefaultBoolean;
    FCopies: Cardinal;
    FDraft: TdxDefaultBoolean;
    FHorizontalCentered: TdxDefaultBoolean;
    FPageOrder: TdxSpreadSheetTableViewOptionsPrintPrintingPageOrder;
    FVerticalCentered: TdxDefaultBoolean;
  public
    procedure Assign(Source: TPersistent); override;
    procedure Reset; override;
  published
    property BlackAndWhite: TdxDefaultBoolean read FBlackAndWhite write FBlackAndWhite default bDefault;
    property Copies: Cardinal read FCopies write FCopies default 0;
    property Draft: TdxDefaultBoolean read FDraft write FDraft default bDefault;
    property HorizontalCentered: TdxDefaultBoolean read FHorizontalCentered write FHorizontalCentered default bDefault;
    property PageOrder: TdxSpreadSheetTableViewOptionsPrintPrintingPageOrder read FPageOrder write FPageOrder default opppDefault;
    property VerticalCentered: TdxDefaultBoolean read FVerticalCentered write FVerticalCentered default bDefault;
  end;

  { TdxSpreadSheetTableViewOptionsPrintSource }

  TdxSpreadSheetTableViewOptionsPrintSourceErrorIndication = (pseiDefault, pseiBlank, pseiDash, pseiDisplayText, pseiNA);

  TdxSpreadSheetTableViewOptionsPrintSource = class(TdxSpreadSheetTableViewOptionsPrintCustomPersistent)
  strict private
    FArea: TdxSpreadSheetTableViewOptionsPrintRect;
    FErrorIndication: TdxSpreadSheetTableViewOptionsPrintSourceErrorIndication;
    FGridLines: TdxDefaultBoolean;
    FHeaders: TdxDefaultBoolean;

    procedure SetArea(AValue: TdxSpreadSheetTableViewOptionsPrintRect);
  public
    constructor Create;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    procedure Reset; override;
  published
    property Area: TdxSpreadSheetTableViewOptionsPrintRect read FArea write SetArea;
    property ErrorIndication: TdxSpreadSheetTableViewOptionsPrintSourceErrorIndication read FErrorIndication write FErrorIndication default pseiDefault;
    property GridLines: TdxDefaultBoolean read FGridLines write FGridLines default bDefault;
    property Headers: TdxDefaultBoolean read FHeaders write FHeaders default bDefault;
  end;

implementation

uses
  SysUtils, Math;

{ TdxSpreadSheetTableViewOptionsPrintCustomPersistent }

procedure TdxSpreadSheetTableViewOptionsPrintCustomPersistent.AfterConstruction;
begin
  inherited AfterConstruction;
  Reset
end;

{ TdxSpreadSheetTableViewOptionsPrintHeaderFooterText }

procedure TdxSpreadSheetTableViewOptionsPrintHeaderFooterText.Assign(Source: TPersistent);
var
  I: Integer;
begin
  if Source is TdxSpreadSheetTableViewOptionsPrintHeaderFooterText then
  begin
    for I := 0 to SectionCount - 1 do
      SetValue(I, TdxSpreadSheetTableViewOptionsPrintHeaderFooterText(Source).GetValue(I));
    Assigned := TdxSpreadSheetTableViewOptionsPrintHeaderFooterText(Source).Assigned;
  end;
end;

procedure TdxSpreadSheetTableViewOptionsPrintHeaderFooterText.Reset;
var
  I: Integer;
begin
  for I := 0 to SectionCount - 1 do
    SetValue(I, '');
  Assigned := False;
end;

function TdxSpreadSheetTableViewOptionsPrintHeaderFooterText.GetValue(const Index: Integer): string;
begin
  Result := FValues[Index];
end;

procedure TdxSpreadSheetTableViewOptionsPrintHeaderFooterText.SetValue(const Index: Integer; const Value: string);
begin
  Assigned := True;
  FValues[Index] := Value;
end;

{ TdxSpreadSheetTableViewOptionsPrintHeaderFooter }

constructor TdxSpreadSheetTableViewOptionsPrintHeaderFooter.Create;
begin
  inherited Create;
  FCommonHeader := TdxSpreadSheetTableViewOptionsPrintHeaderFooterText.Create;
  FCommonFooter := TdxSpreadSheetTableViewOptionsPrintHeaderFooterText.Create;
  FEvenPagesHeader := TdxSpreadSheetTableViewOptionsPrintHeaderFooterText.Create;
  FEvenPagesFooter := TdxSpreadSheetTableViewOptionsPrintHeaderFooterText.Create;
  FFirstPageHeader := TdxSpreadSheetTableViewOptionsPrintHeaderFooterText.Create;
  FFirstPageFooter := TdxSpreadSheetTableViewOptionsPrintHeaderFooterText.Create;
end;

destructor TdxSpreadSheetTableViewOptionsPrintHeaderFooter.Destroy;
begin
  FreeAndNil(FCommonFooter);
  FreeAndNil(FCommonHeader);
  FreeAndNil(FEvenPagesFooter);
  FreeAndNil(FEvenPagesHeader);
  FreeAndNil(FFirstPageFooter);
  FreeAndNil(FFirstPageHeader);
  inherited Destroy;
end;

procedure TdxSpreadSheetTableViewOptionsPrintHeaderFooter.Assign(Source: TPersistent);
begin
  if Source is TdxSpreadSheetTableViewOptionsPrintHeaderFooter then
  begin
    AlignWithMargins := TdxSpreadSheetTableViewOptionsPrintHeaderFooter(Source).AlignWithMargins;
    ScaleWithDocument := TdxSpreadSheetTableViewOptionsPrintHeaderFooter(Source).ScaleWithDocument;
    CommonFooter := TdxSpreadSheetTableViewOptionsPrintHeaderFooter(Source).CommonFooter;
    CommonHeader := TdxSpreadSheetTableViewOptionsPrintHeaderFooter(Source).CommonHeader;
    EvenPagesFooter := TdxSpreadSheetTableViewOptionsPrintHeaderFooter(Source).EvenPagesFooter;
    EvenPagesHeader := TdxSpreadSheetTableViewOptionsPrintHeaderFooter(Source).EvenPagesHeader;
    FirstPageFooter := TdxSpreadSheetTableViewOptionsPrintHeaderFooter(Source).FirstPageFooter;
    FirstPageHeader := TdxSpreadSheetTableViewOptionsPrintHeaderFooter(Source).FirstPageHeader;
  end;
end;

procedure TdxSpreadSheetTableViewOptionsPrintHeaderFooter.Reset;
begin
  AlignWithMargins := bDefault;
  ScaleWithDocument := bDefault;
  CommonFooter.Reset;
  CommonHeader.Reset;
  EvenPagesFooter.Reset;
  EvenPagesHeader.Reset;
  FirstPageFooter.Reset;
  FirstPageHeader.Reset;
end;

function TdxSpreadSheetTableViewOptionsPrintHeaderFooter.GetAssigned: Boolean;
begin
  Result := (AlignWithMargins <> bDefault) or (ScaleWithDocument <> bDefault) or
    CommonFooter.Assigned or CommonHeader.Assigned or
    EvenPagesFooter.Assigned or EvenPagesHeader.Assigned or
    FirstPageFooter.Assigned or FirstPageFooter.Assigned;
end;

procedure TdxSpreadSheetTableViewOptionsPrintHeaderFooter.SetCommonFooter(
  AValue: TdxSpreadSheetTableViewOptionsPrintHeaderFooterText);
begin
  FCommonFooter.Assign(AValue);
end;

procedure TdxSpreadSheetTableViewOptionsPrintHeaderFooter.SetCommonHeader(
  AValue: TdxSpreadSheetTableViewOptionsPrintHeaderFooterText);
begin
  FCommonHeader.Assign(AValue);
end;

procedure TdxSpreadSheetTableViewOptionsPrintHeaderFooter.SetEvenPagesFooter(
  AValue: TdxSpreadSheetTableViewOptionsPrintHeaderFooterText);
begin
  FEvenPagesFooter.Assign(AValue);
end;

procedure TdxSpreadSheetTableViewOptionsPrintHeaderFooter.SetEvenPagesHeader(
  AValue: TdxSpreadSheetTableViewOptionsPrintHeaderFooterText);
begin
  FEvenPagesHeader.Assign(AValue);
end;

procedure TdxSpreadSheetTableViewOptionsPrintHeaderFooter.SetFirstPageFooter(
  AValue: TdxSpreadSheetTableViewOptionsPrintHeaderFooterText);
begin
  FFirstPageFooter.Assign(AValue);
end;

procedure TdxSpreadSheetTableViewOptionsPrintHeaderFooter.SetFirstPageHeader(
  AValue: TdxSpreadSheetTableViewOptionsPrintHeaderFooterText);
begin
  FEvenPagesHeader.Assign(AValue);
end;

{ TdxSpreadSheetTableViewOptionsPrintRect }

procedure TdxSpreadSheetTableViewOptionsPrintRect.Assign(Source: TPersistent);
begin
  inherited Assign(Source);
  if Source is TdxSpreadSheetTableViewOptionsPrintRect then
    Assigned := TdxSpreadSheetTableViewOptionsPrintRect(Source).Assigned;
end;

procedure TdxSpreadSheetTableViewOptionsPrintRect.Reset;
begin
  Rect := cxNullRect;
  Assigned := False;
end;

procedure TdxSpreadSheetTableViewOptionsPrintRect.DoChange;
begin
  Assigned := True;
  inherited DoChange;
end;

{ TdxSpreadSheetTableViewOptionsPrintPagePaper }

constructor TdxSpreadSheetTableViewOptionsPrintPagePaper.Create;
begin
  inherited Create;
  FCustomSize := TdxPointDoublePersistent.Create(nil);
  FCustomSize.OnChange := CustomSizeChangeHandler;
end;

destructor TdxSpreadSheetTableViewOptionsPrintPagePaper.Destroy;
begin
  FreeAndNil(FCustomSize);
  inherited Destroy;
end;

procedure TdxSpreadSheetTableViewOptionsPrintPagePaper.Assign(Source: TPersistent);
begin
  if Source is TdxSpreadSheetTableViewOptionsPrintPagePaper then
  begin
    CustomSize := TdxSpreadSheetTableViewOptionsPrintPagePaper(Source).CustomSize;
    SizeID := TdxSpreadSheetTableViewOptionsPrintPagePaper(Source).SizeID;
    Assigned := TdxSpreadSheetTableViewOptionsPrintPagePaper(Source).Assigned; // last
  end;
end;

procedure TdxSpreadSheetTableViewOptionsPrintPagePaper.Reset;
begin
  SizeID := 0;
  CustomSize.Reset;
  Assigned := False;
end;

procedure TdxSpreadSheetTableViewOptionsPrintPagePaper.CustomSizeChangeHandler(Sender: TObject);
begin
  Assigned := True;
  SizeID := 0;
end;

procedure TdxSpreadSheetTableViewOptionsPrintPagePaper.SetCustomSize(AValue: TdxPointDoublePersistent);
begin
  FCustomSize.Assign(AValue);
end;

procedure TdxSpreadSheetTableViewOptionsPrintPagePaper.SetSizeID(AValue: Integer);
begin
  Assigned := True;
  FSizeID := Min(Max(AValue, 0), 118);
end;

{ TdxSpreadSheetTableViewOptionsPrintPagination }

constructor TdxSpreadSheetTableViewOptionsPrintPagination.Create;
begin
  inherited Create;
  FRowPageBreaks := TList<Cardinal>.Create;
  FColumnPageBreaks := TList<Cardinal>.Create;
end;

destructor TdxSpreadSheetTableViewOptionsPrintPagination.Destroy;
begin
  FreeAndNil(FColumnPageBreaks);
  FreeAndNil(FRowPageBreaks);
  inherited Destroy;
end;

procedure TdxSpreadSheetTableViewOptionsPrintPagination.Assign(Source: TPersistent);
begin
  if Source is TdxSpreadSheetTableViewOptionsPrintPagination then
  begin
    ColumnPageBreaks := TdxSpreadSheetTableViewOptionsPrintPagination(Source).ColumnPageBreaks;
    RowPageBreaks := TdxSpreadSheetTableViewOptionsPrintPagination(Source).RowPageBreaks;
  end;
end;

procedure TdxSpreadSheetTableViewOptionsPrintPagination.Reset;
begin
  ColumnPageBreaks.Clear;
  RowPageBreaks.Clear;
end;

procedure TdxSpreadSheetTableViewOptionsPrintPagination.SetColumnPageBreaks(AValue: TList<Cardinal>);
begin
  FColumnPageBreaks.Clear;
  FColumnPageBreaks.Capacity := AValue.Count;
  FColumnPageBreaks.AddRange(AValue);
end;

procedure TdxSpreadSheetTableViewOptionsPrintPagination.SetRowPageBreaks(AValue: TList<Cardinal>);
begin
  FRowPageBreaks.Clear;
  FRowPageBreaks.Capacity := AValue.Count;
  FRowPageBreaks.AddRange(AValue);
end;

{ TdxSpreadSheetTableViewOptionsPrintPageMargins }

procedure TdxSpreadSheetTableViewOptionsPrintPageMargins.Assign(Source: TPersistent);
var
  I: Integer;
begin
  if Source is TdxSpreadSheetTableViewOptionsPrintPageMargins then
  begin
    for I := 0 to ValuesCount - 1 do
      FValues[I] := TdxSpreadSheetTableViewOptionsPrintPageMargins(Source).FValues[I];
    Assigned := TdxSpreadSheetTableViewOptionsPrintPageMargins(Source).Assigned;
  end;
end;

procedure TdxSpreadSheetTableViewOptionsPrintPageMargins.Reset;
var
  I: Integer;
begin
  for I := 0 to ValuesCount - 1 do
    FValues[I] := 0;
  Assigned := False;
end;

function TdxSpreadSheetTableViewOptionsPrintPageMargins.GetValue(AIndex: Integer): Double;
begin
  Result := FValues[AIndex];
end;

function TdxSpreadSheetTableViewOptionsPrintPageMargins.IsValueStored(AIndex: Integer): Boolean;
begin
  Result := not IsZero(GetValue(AIndex));
end;

procedure TdxSpreadSheetTableViewOptionsPrintPageMargins.SetValue(AIndex: Integer; const AValue: Double);
begin
  Assigned := True;
  FValues[AIndex] := AValue;
end;

{ TdxSpreadSheetTableViewOptionsPrintPage }

constructor TdxSpreadSheetTableViewOptionsPrintPage.Create;
begin
  inherited Create;
  FMargins := TdxSpreadSheetTableViewOptionsPrintPageMargins.Create;
  FPaper := TdxSpreadSheetTableViewOptionsPrintPagePaper.Create;
end;

destructor TdxSpreadSheetTableViewOptionsPrintPage.Destroy;
begin
  FreeAndNil(FMargins);
  FreeAndNil(FPaper);
  inherited Destroy;
end;

procedure TdxSpreadSheetTableViewOptionsPrintPage.Assign(Source: TPersistent);
begin
  if Source is TdxSpreadSheetTableViewOptionsPrintPage then
  begin
    FitToHeight := TdxSpreadSheetTableViewOptionsPrintPage(Source).FitToHeight;
    FitToWidth := TdxSpreadSheetTableViewOptionsPrintPage(Source).FitToWidth;
    Orientation := TdxSpreadSheetTableViewOptionsPrintPage(Source).Orientation;
    FirstPageNumber := TdxSpreadSheetTableViewOptionsPrintPage(Source).FirstPageNumber;
    Margins := TdxSpreadSheetTableViewOptionsPrintPage(Source).Margins;
    Paper := TdxSpreadSheetTableViewOptionsPrintPage(Source).Paper;
    Scale := TdxSpreadSheetTableViewOptionsPrintPage(Source).Scale;
    ScaleMode := TdxSpreadSheetTableViewOptionsPrintPage(Source).ScaleMode;
  end;
end;

procedure TdxSpreadSheetTableViewOptionsPrintPage.Reset;
begin
  Scale := 100;
  FitToHeight := 0;
  FitToWidth := 0;
  ScaleMode := oppsmDefault;
  Orientation := oppoDefault;
  FirstPageNumber := 0;
  Margins.Reset;
end;

procedure TdxSpreadSheetTableViewOptionsPrintPage.SetFitToHeight(AValue: Cardinal);
begin
  FFitToHeight := AValue;
  FScaleMode := oppsmFitToPage;
end;

procedure TdxSpreadSheetTableViewOptionsPrintPage.SetFitToWidth(AValue: Cardinal);
begin
  FFitToWidth := AValue;
  FScaleMode := oppsmFitToPage;
end;

procedure TdxSpreadSheetTableViewOptionsPrintPage.SetMargins(AValue: TdxSpreadSheetTableViewOptionsPrintPageMargins);
begin
  FMargins.Assign(AValue);
end;

procedure TdxSpreadSheetTableViewOptionsPrintPage.SetPaper(AValue: TdxSpreadSheetTableViewOptionsPrintPagePaper);
begin
  FPaper.Assign(AValue);
end;

procedure TdxSpreadSheetTableViewOptionsPrintPage.SetScale(AValue: Integer);
begin
  FScale := Min(Max(AValue, 10), 400);
  FScaleMode := oppsmAdjustToScale;
end;

{ TdxSpreadSheetTableViewOptionsPrintPrinting }

procedure TdxSpreadSheetTableViewOptionsPrintPrinting.Assign(Source: TPersistent);
begin
  if Source is TdxSpreadSheetTableViewOptionsPrintPrinting then
  begin
    Draft := TdxSpreadSheetTableViewOptionsPrintPrinting(Source).Draft;
    BlackAndWhite := TdxSpreadSheetTableViewOptionsPrintPrinting(Source).BlackAndWhite;
    Copies := TdxSpreadSheetTableViewOptionsPrintPrinting(Source).Copies;
    HorizontalCentered := TdxSpreadSheetTableViewOptionsPrintPrinting(Source).HorizontalCentered;
    PageOrder := TdxSpreadSheetTableViewOptionsPrintPrinting(Source).PageOrder;
    VerticalCentered := TdxSpreadSheetTableViewOptionsPrintPrinting(Source).VerticalCentered;
  end;
end;

procedure TdxSpreadSheetTableViewOptionsPrintPrinting.Reset;
begin
  BlackAndWhite := bDefault;
  Copies := 0;
  HorizontalCentered := bDefault;
  VerticalCentered := bDefault;
  PageOrder := opppDefault;
  Draft := bDefault;
end;

{ TdxSpreadSheetTableViewOptionsPrintSource }

constructor TdxSpreadSheetTableViewOptionsPrintSource.Create;
begin
  inherited Create;
  FArea := TdxSpreadSheetTableViewOptionsPrintRect.Create(Self);
end;

destructor TdxSpreadSheetTableViewOptionsPrintSource.Destroy;
begin
  FreeAndNil(FArea);
  inherited Destroy;
end;

procedure TdxSpreadSheetTableViewOptionsPrintSource.Assign(Source: TPersistent);
begin
  if Source is TdxSpreadSheetTableViewOptionsPrintSource then
  begin
    Area := TdxSpreadSheetTableViewOptionsPrintSource(Source).Area;
    ErrorIndication := TdxSpreadSheetTableViewOptionsPrintSource(Source).ErrorIndication;
    GridLines := TdxSpreadSheetTableViewOptionsPrintSource(Source).GridLines;
    Headers := TdxSpreadSheetTableViewOptionsPrintSource(Source).Headers;
  end;
end;

procedure TdxSpreadSheetTableViewOptionsPrintSource.Reset;
begin
  Area.Reset;
  ErrorIndication := pseiDefault;
  GridLines := bDefault;
  Headers := bDefault;
end;

procedure TdxSpreadSheetTableViewOptionsPrintSource.SetArea(AValue: TdxSpreadSheetTableViewOptionsPrintRect);
begin
  FArea.Assign(AValue);
end;

end.
