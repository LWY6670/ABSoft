{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressSpreadSheet                                       }
{                                                                    }
{           Copyright (c) 2001-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSSPREADSHEET CONTROL AND ALL    }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY. }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxSpreadSheetDialogStrs;

{$I cxVer.Inc}

interface

uses
  dxCore, cxClasses, dxSpreadSheetTypes, dxSpreadSheetGraphics, Graphics, dxGDIPlusClasses;

resourcestring
  sdxFormatCellsDialogAuto = 'Automatic';
  sdxFormatCellsDialogButtonCancel = 'Cancel';
  sdxFormatCellsDialogButtonColorAuto = 'Auto';
  sdxFormatCellsDialogButtonOK = 'OK';
  sdxFormatCellsDialogCaption = 'Format Cells';
  sdxFormatCellsDialogNone = 'None';
  sdxFormatCellsDialogSample = 'Sample';

  // Number Page
  sdxFormatCellsDialogGroupNumber = 'Number';
  sdxFormatCellsDialogCategory = '&Category:';
  sdxFormatCellsDialogCategoryAccounting = 'Accounting';
  sdxFormatCellsDialogCategoryAccountingDescription =
    'Accounting formats line up the currency symbols and decimal points in a column.';
  sdxFormatCellsDialogCategoryCurrency = 'Currency';
  sdxFormatCellsDialogCategoryCurrencyDescription =
    'Currency formats are used for general monetary values. ' +
    'Use Accounting formats to align decimal points in a column.';
  sdxFormatCellsDialogCategoryCustom = 'Custom';
  sdxFormatCellsDialogCategoryCustomDescription =
    'Type the number format code, using one of the existing codes as a starting point.';
  sdxFormatCellsDialogCategoryDate = 'Date';
  sdxFormatCellsDialogCategoryDateDescription =
    'Date formats display date and time serial numbers as date values.';
  sdxFormatCellsDialogCategoryDateNote =
    'Date formats that begin with an asterisk (*) respond to changes in regional date and time settings that are specified for the operation system.';
  sdxFormatCellsDialogCategoryFraction = 'Fraction';
  sdxFormatCellsDialogCategoryGeneral = 'General';
  sdxFormatCellsDialogCategoryGeneralNotes = 'General format cells have no specific number format.';
  sdxFormatCellsDialogCategoryNumber = 'Number';
  sdxFormatCellsDialogCategoryNumberDescription =
    'Number is used for general display of numbers. ' +
    'Currency and Accounting offer specialized formatting for monetary values.';
  sdxFormatCellsDialogCategoryPercentage = 'Percentage';
  sdxFormatCellsDialogCategoryPercentageDescription =
    'Percentage formats multiply the cell value by 100 and display the result with a percent symbol.';
  sdxFormatCellsDialogCategoryScientific = 'Scientific';
  sdxFormatCellsDialogCategoryText = 'Text';
  sdxFormatCellsDialogCategoryTextNotes =
    'Text format cells are treated as text even when a number '+
    'is in the cell. The cell is displayed exactly as entered.';
  sdxFormatCellsDialogCategoryTime = 'Time';
  sdxFormatCellsDialogCategoryTimeDescription =
    'Time formats display date and time serial numbers as date values.';
  sdxFormatCellsDialogCustomCode = '&Type:';
  sdxFormatCellsDialogDecimalPlaces = '&Decimal places:';
  sdxFormatCellsDialogNumberFormatTemplates = '&Type:';
  sdxFormatCellsDialogUseThousandSeparator = '&Use 1000 Separator (%s)';

  // Alignment Page
  sdxFormatCellsDialogGroupTextAlignment = 'Alignment';
  sdxFormatCellsDialogMergeCells = '&Merge cells';
  sdxFormatCellsDialogShrinkToFit = 'Shrin&k to fit';
  sdxFormatCellsDialogTextAlignHorz = '&Horizontal:';
  sdxFormatCellsDialogTextAlignHorzIndent = '&Indent:';
  sdxFormatCellsDialogTextAlignment = 'Text alignment';
  sdxFormatCellsDialogTextAlignVert = '&Vertical:';
  sdxFormatCellsDialogTextControl = 'Text control';
  sdxFormatCellsDialogWrapText = '&Wrap text';

  // Font Page
  sdxFormatCellsDialogButtonResetFont = '&Reset';
  sdxFormatCellsDialogFontColor = '&Color:';
  sdxFormatCellsDialogFontName = '&Font:';
  sdxFormatCellsDialogFontNotInstalled =
    'This font is not installed on the system. The closest available font will be used for printing.';
  sdxFormatCellsDialogFontPreview = 'Preview';
  sdxFormatCellsDialogFontPrintNotes =
    'This is a TrueType font. The same font will be used on both your printer and your screen.';
  sdxFormatCellsDialogFontSize = '&Size:';
  sdxFormatCellsDialogFontStyle = 'F&ont style:';
  sdxFormatCellsDialogFontStrikethrough = 'Stri&kethrough';
  sdxFormatCellsDialogFontUnderline = '&Underline:';
  sdxFormatCellsDialogGroupFontEffects = 'Effects';
  sdxFormatCellsDialogUnderlineNode = 'None';
  sdxFormatCellsDialogUnderlineSingle = 'Single';

  // Border Page
  sdxFormatCellsDialogBorder = 'Border';
  sdxFormatCellsDialogBorderInside = '&Inside';
  sdxFormatCellsDialogBorderLine = 'Line';
  sdxFormatCellsDialogBorderLineColor = '&Color:';
  sdxFormatCellsDialogBorderLineStyle = '&Style:';
  sdxFormatCellsDialogBorderNone = '&None';
  sdxFormatCellsDialogBorderOutline = '&Outline';
  sdxFormatCellsDialogBorderPresets = 'Presets';
  sdxFormatCellsDialogBordersHint =
    'The selected border style can be applied by clicking the presets, preview diagram or the buttons above.';
  sdxFormatCellsDialogPreviewText = 'Text';

  // Fill Page
  sdxFormatCellsDialogBackgroundColor = 'Background &color:';
  sdxFormatCellsDialogFill = 'Fill';
  sdxFormatCellsDialogMoreColors = '&More colors...';
  sdxFormatCellsDialogNoColor = 'No color';
  sdxFormatCellsDialogPatternColor = 'P&attern color:';
  sdxFormatCellsDialogPatternStyle = '&Pattern style:';

  // Protection Page
  sdxFormatCellsDialogHidden = 'H&idden';
  sdxFormatCellsDialogLocked = '&Locked';
  sdxFormatCellsDialogProtection = 'Protection';
  sdxFormatCellsDialogProtectionNotes =
    'Locking cells or hiding formulas has no effect until you protect the worksheet.';

  // Font Style
  sdxFontStyleBold = 'Bold';
  sdxFontStyleBoldItalic = 'Bold Italic';
  sdxFontStyleItalic = 'Italic';
  sdxFontStyleRegular = 'Regular';
  sdxFontStyleStrikeOut = 'Strike Out';
  sdxFontStyleUnderline = 'Underline';

  // Horizontal Alignment
  sdxHorzAlignCenter = 'Center';
  sdxHorzAlignDistributed = 'Distributed (Indent)';
  sdxHorzAlignFill = 'Fill';
  sdxHorzAlignGeneral = 'General';
  sdxHorzAlignJustify = 'Justify';
  sdxHorzAlignLeft = 'Left (Indent)';
  sdxHorzAlignRight = 'Right (Indent)';

  // Vertical Alignment
  sdxVertAlignBottom = 'Bottom';
  sdxVertAlignCenter = 'Center';
  sdxVertAlignDistributed = 'Distributed';
  sdxVertAlignJustify = 'Justify';
  sdxVertAlignTop = 'Top';

  // Cell Fill Styles
  sdxCellFillStyleSolid = 'Solid';
  sdxCellFillStyleGray75 = 'Gray 75%';
  sdxCellFillStyleGray50 = 'Gray 50%';
  sdxCellFillStyleGray25 = 'Gray 25%';
  sdxCellFillStyleGray12 = 'Gray 12%';
  sdxCellFillStyleGray6 = 'Gray 6%';
  sdxCellFillStyleHorzStrip = 'Horizontal Strip';
  sdxCellFillStyleVertStrip = 'Vertical Strip';
  sdxCellFillStyleRevDiagonalStrip = 'Reverse Diagonal Strip';
  sdxCellFillStyleDiagonalStrip = 'Diagonal Strip';
  sdxCellFillStyleDiagCrossHatch = 'Diagonal Crosshatch';
  sdxCellFillStyleThickDiagonalCrossHatch = 'Thick Diagonal Crosshatch';
  sdxCellFillStyleThinHorzStrip = 'Thin Horizontal Strip';
  sdxCellFillStyleThinVertStrip = 'Thin Vertical Strip';
  sdxCellFillStyleThinRevDiagonalStrip = 'Thin Reverse Diagonal Strip';
  sdxCellFillStyleThinDiagonalStrip = 'Thin Diagonal Strip';
  sdxCellFillStyleThinDiagCrossHatch = 'Thin Diagonal Crosshatch';
  sdxCellFillStyleThinHorzCrossHatch = 'Thin Horizontal Crosshatch';

  // Cells Modification Dialog
  sdxCellsModificationDialogInsertCaption = 'Insert';
  sdxCellsModificationDialogDeleteCaption = 'Delete';

  // Cells Shifting
  sdxShiftCellsDown = 'Shift cells &down';
  sdxShiftCellsLeft = 'Shift cells &left';
  sdxShiftCellsRight = 'Shift cells r&ight';
  sdxShiftCellsUp = 'Shift cells &up';
  sdxShiftColumn = 'Entire &column';
  sdxShiftRow = 'Entire &row';

  // Container Customization Dialog
  sdxContainerCustomizationDialogCaption = 'Customize Object';
  sdxContainerCustomizationDialogButtonAdd = '&Add';
  sdxContainerCustomizationDialogButtonCancel = 'Cancel';
  sdxContainerCustomizationDialogButtonColor = '&Color';
  sdxContainerCustomizationDialogButtonLoad = '&Load';
  sdxContainerCustomizationDialogButtonOK = 'OK';
  sdxContainerCustomizationDialogButtonRemove = 'Remo&ve';
  sdxContainerCustomizationDialogButtonSave = '&Save';
  sdxContainerCustomizationDialogReset = 'Re&set';

  // Container Customization Dialog - Fill
  sdxContainerCustomizationDialogDirection = '&Direction:';
  sdxContainerCustomizationDialogGradientFill = '&Gradient fill';
  sdxContainerCustomizationDialogGroupFill = 'Fill';
  sdxContainerCustomizationDialogNoFill = '&No fill';
  sdxContainerCustomizationDialogSolidFill = '&Solid fill';
  sdxContainerCustomizationDialogStops = 'Stops:';
  sdxContainerCustomizationDialogTextureFill = '&Texture fill';

  // Container Customization Dialog - Line
  sdxContainerCustomizationDialogGradientLine = '&Gradient line';
  sdxContainerCustomizationDialogLine = 'Line';
  sdxContainerCustomizationDialogLineStyle = '&Style:';
  sdxContainerCustomizationDialogLineWidth = '&Width:';
  sdxContainerCustomizationDialogNoLine = '&No line';
  sdxContainerCustomizationDialogSolidLine = '&Solid line';

  // Container Customization Dialog - Properties
  sdxContainerCustomizationDialogGroupProperties = 'Properties';
  sdxContainerCustomizationDialogPositioning = 'Positioning';
  sdxContainerCustomizationDialogAbsolute = '&Don''t move or size with cells';
  sdxContainerCustomizationDialogOneCells = '&Move but don''t size with cells';
  sdxContainerCustomizationDialogTwoCells = 'Move and &size with cells';

  // Container Customization Dialog - Size
  sdxContainerCustomizationDialogCropBottom = 'Botto&m:';
  sdxContainerCustomizationDialogCropFrom = 'Crop from';
  sdxContainerCustomizationDialogCropLeft = '&Left:';
  sdxContainerCustomizationDialogCropRight = 'Ri&ght:';
  sdxContainerCustomizationDialogCropTop = 'To&p:';
  sdxContainerCustomizationDialogGroupSize = 'Size';
  sdxContainerCustomizationDialogHeight = 'H&eight:';
  sdxContainerCustomizationDialogLockAspectRatio = 'Lock &aspect ratio';
  sdxContainerCustomizationDialogOriginalSize = 'Original size';
  sdxContainerCustomizationDialogOriginalSizeFormatString = 'Height: %d, Width: %d';
  sdxContainerCustomizationDialogRelativeToPictureSize = '&Relative to original picture size';
  sdxContainerCustomizationDialogRotation = 'Ro&tation:';
  sdxContainerCustomizationDialogScale = 'Scale';
  sdxContainerCustomizationDialogScaleHeight = '&Height:';
  sdxContainerCustomizationDialogScaleWidth = '&Width:';
  sdxContainerCustomizationDialogSizeAndRotate = 'Size and rotate';
  sdxContainerCustomizationDialogWidth = 'Wi&dth:';

  sdxGradientModeBackwardDiagonal = 'Backward Diagonal';
  sdxGradientModeForwardDiagonal = 'Forward Diagonal';
  sdxGradientModeHorizontal = 'Horizontal';
  sdxGradientModeVertical = 'Vertical';

  sdxPenStyleDash = 'Dash';
  sdxPenStyleDashDot = 'Dash Dot';
  sdxPenStyleDashDotDot = 'Dash Dot Dot';
  sdxPenStyleDot = 'Dot';
  sdxPenStyleSolid = 'Solid';

  // Unhide sheet dialog
  sdxUnhideSheetDialogCaption = 'Unhide';
  sdxUnhideSheetDialogHiddenSheets = '&Unhide sheet:';

const
  dxCellFillStyleNames: array[TdxSpreadSheetCellFillStyle] of Pointer = (
    @sdxCellFillStyleSolid, @sdxCellFillStyleGray75, @sdxCellFillStyleGray50, @sdxCellFillStyleGray25,
    @sdxCellFillStyleGray12, @sdxCellFillStyleGray6, @sdxCellFillStyleHorzStrip, @sdxCellFillStyleVertStrip,
    @sdxCellFillStyleRevDiagonalStrip, @sdxCellFillStyleDiagonalStrip, @sdxCellFillStyleDiagCrossHatch,
    @sdxCellFillStyleThickDiagonalCrossHatch, @sdxCellFillStyleThinHorzStrip, @sdxCellFillStyleThinVertStrip,
    @sdxCellFillStyleThinRevDiagonalStrip, @sdxCellFillStyleThinDiagonalStrip, @sdxCellFillStyleThinDiagCrossHatch,
    @sdxCellFillStyleThinHorzCrossHatch
  );

  dxFontStyleNames: array[TFontStyle] of Pointer = (
    @sdxFontStyleBold, @sdxFontStyleItalic, @sdxFontStyleUnderline, @sdxFontStyleStrikeOut
  );

  dxSpreadSheetDataHorzAlignNames: array [TdxSpreadSheetDataAlignHorz] of Pointer = (
    @sdxHorzAlignGeneral, @sdxHorzAlignLeft, @sdxHorzAlignCenter, @sdxHorzAlignRight, @sdxHorzAlignFill,
    @sdxHorzAlignJustify, @sdxHorzAlignDistributed
  );

  dxSpreadSheetDataVertAlignNames: array [TdxSpreadSheetDataAlignVert] of Pointer = (
    @sdxVertAlignTop, @sdxVertAlignCenter, @sdxVertAlignBottom, @sdxVertAlignJustify, @sdxVertAlignDistributed
  );

  dxSpreadSheetNumberFormatCategoryNames: array[TdxSpreadSheetNumberFormatCategory] of Pointer = (
    @sdxFormatCellsDialogCategoryGeneral, @sdxFormatCellsDialogCategoryNumber, @sdxFormatCellsDialogCategoryCurrency,
    @sdxFormatCellsDialogCategoryAccounting, @sdxFormatCellsDialogCategoryDate, @sdxFormatCellsDialogCategoryTime,
    @sdxFormatCellsDialogCategoryPercentage, @sdxFormatCellsDialogCategoryFraction, @sdxFormatCellsDialogCategoryScientific,
    @sdxFormatCellsDialogCategoryText, @sdxFormatCellsDialogCategoryCustom
  );

  dxGradientModeNames: array[TdxGPBrushGradientMode] of Pointer = (
    @sdxGradientModeHorizontal, @sdxGradientModeVertical, @sdxGradientModeForwardDiagonal, @sdxGradientModeBackwardDiagonal
  );

  dxPenStyleNames: array[TdxGPPenStyle] of Pointer = (
    @sdxPenStyleSolid, @sdxPenStyleDash, @sdxPenStyleDot, @sdxPenStyleDashDot, @sdxPenStyleDashDotDot
  );

procedure AddSpreadSheetDialogsResourceStringNames(AProduct: TdxProductResourceStrings);
implementation

procedure AddSpreadSheetDialogsResourceStringNames(AProduct: TdxProductResourceStrings);
begin
  AProduct.Add('sdxCellFillStyleDiagCrossHatch', @sdxCellFillStyleDiagCrossHatch);
  AProduct.Add('sdxCellFillStyleDiagonalStrip', @sdxCellFillStyleDiagonalStrip);
  AProduct.Add('sdxCellFillStyleGray12', @sdxCellFillStyleGray12);
  AProduct.Add('sdxCellFillStyleGray25', @sdxCellFillStyleGray25);
  AProduct.Add('sdxCellFillStyleGray50', @sdxCellFillStyleGray50);
  AProduct.Add('sdxCellFillStyleGray6', @sdxCellFillStyleGray6);
  AProduct.Add('sdxCellFillStyleGray75', @sdxCellFillStyleGray75);
  AProduct.Add('sdxCellFillStyleHorzStrip', @sdxCellFillStyleHorzStrip);
  AProduct.Add('sdxCellFillStyleRevDiagonalStrip', @sdxCellFillStyleRevDiagonalStrip);
  AProduct.Add('sdxCellFillStyleSolid', @sdxCellFillStyleSolid);
  AProduct.Add('sdxCellFillStyleThickDiagonalCrossHatch', @sdxCellFillStyleThickDiagonalCrossHatch);
  AProduct.Add('sdxCellFillStyleThinDiagCrossHatch', @sdxCellFillStyleThinDiagCrossHatch);
  AProduct.Add('sdxCellFillStyleThinDiagonalStrip', @sdxCellFillStyleThinDiagonalStrip);
  AProduct.Add('sdxCellFillStyleThinHorzCrossHatch', @sdxCellFillStyleThinHorzCrossHatch);
  AProduct.Add('sdxCellFillStyleThinHorzStrip', @sdxCellFillStyleThinHorzStrip);
  AProduct.Add('sdxCellFillStyleThinRevDiagonalStrip', @sdxCellFillStyleThinRevDiagonalStrip);
  AProduct.Add('sdxCellFillStyleThinVertStrip', @sdxCellFillStyleThinVertStrip);
  AProduct.Add('sdxCellFillStyleVertStrip', @sdxCellFillStyleVertStrip);
  AProduct.Add('sdxFontStyleBold', @sdxFontStyleBold);
  AProduct.Add('sdxFontStyleBoldItalic', @sdxFontStyleBoldItalic);
  AProduct.Add('sdxFontStyleItalic', @sdxFontStyleItalic);
  AProduct.Add('sdxFontStyleRegular', @sdxFontStyleRegular);
  AProduct.Add('sdxFontStyleStrikeOut', @sdxFontStyleStrikeOut);
  AProduct.Add('sdxFontStyleUnderline', @sdxFontStyleUnderline);
  AProduct.Add('sdxFormatCellsDialogAuto', @sdxFormatCellsDialogAuto);
  AProduct.Add('sdxFormatCellsDialogBackgroundColor', @sdxFormatCellsDialogBackgroundColor);
  AProduct.Add('sdxFormatCellsDialogBorder', @sdxFormatCellsDialogBorder);
  AProduct.Add('sdxFormatCellsDialogBorderInside', @sdxFormatCellsDialogBorderInside);
  AProduct.Add('sdxFormatCellsDialogBorderLine', @sdxFormatCellsDialogBorderLine);
  AProduct.Add('sdxFormatCellsDialogBorderLineColor', @sdxFormatCellsDialogBorderLineColor);
  AProduct.Add('sdxFormatCellsDialogBorderLineStyle', @sdxFormatCellsDialogBorderLineStyle);
  AProduct.Add('sdxFormatCellsDialogBorderNone', @sdxFormatCellsDialogBorderNone);
  AProduct.Add('sdxFormatCellsDialogBorderOutline', @sdxFormatCellsDialogBorderOutline);
  AProduct.Add('sdxFormatCellsDialogBorderPresets', @sdxFormatCellsDialogBorderPresets);
  AProduct.Add('sdxFormatCellsDialogBordersHint', @sdxFormatCellsDialogBordersHint);
  AProduct.Add('sdxFormatCellsDialogButtonCancel', @sdxFormatCellsDialogButtonCancel);
  AProduct.Add('sdxFormatCellsDialogButtonColorAuto', @sdxFormatCellsDialogButtonColorAuto);
  AProduct.Add('sdxFormatCellsDialogButtonOK', @sdxFormatCellsDialogButtonOK);
  AProduct.Add('sdxFormatCellsDialogButtonResetFont', @sdxFormatCellsDialogButtonResetFont);
  AProduct.Add('sdxFormatCellsDialogCaption', @sdxFormatCellsDialogCaption);
  AProduct.Add('sdxFormatCellsDialogCategory', @sdxFormatCellsDialogCategory);
  AProduct.Add('sdxFormatCellsDialogCategoryAccounting', @sdxFormatCellsDialogCategoryAccounting);
  AProduct.Add('sdxFormatCellsDialogCategoryAccountingDescription', @sdxFormatCellsDialogCategoryAccountingDescription);
  AProduct.Add('sdxFormatCellsDialogCategoryCurrency', @sdxFormatCellsDialogCategoryCurrency);
  AProduct.Add('sdxFormatCellsDialogCategoryCurrencyDescription', @sdxFormatCellsDialogCategoryCurrencyDescription);
  AProduct.Add('sdxFormatCellsDialogCategoryCustom', @sdxFormatCellsDialogCategoryCustom);
  AProduct.Add('sdxFormatCellsDialogCategoryCustomDescription', @sdxFormatCellsDialogCategoryCustomDescription);
  AProduct.Add('sdxFormatCellsDialogCategoryDate', @sdxFormatCellsDialogCategoryDate);
  AProduct.Add('sdxFormatCellsDialogCategoryDateDescription', @sdxFormatCellsDialogCategoryDateDescription);
  AProduct.Add('sdxFormatCellsDialogCategoryDateNote', @sdxFormatCellsDialogCategoryDateNote);
  AProduct.Add('sdxFormatCellsDialogCategoryFraction', @sdxFormatCellsDialogCategoryFraction);
  AProduct.Add('sdxFormatCellsDialogCategoryGeneral', @sdxFormatCellsDialogCategoryGeneral);
  AProduct.Add('sdxFormatCellsDialogCategoryGeneralNotes', @sdxFormatCellsDialogCategoryGeneralNotes);
  AProduct.Add('sdxFormatCellsDialogCategoryNumber', @sdxFormatCellsDialogCategoryNumber);
  AProduct.Add('sdxFormatCellsDialogCategoryNumberDescription', @sdxFormatCellsDialogCategoryNumberDescription);
  AProduct.Add('sdxFormatCellsDialogCategoryPercentage', @sdxFormatCellsDialogCategoryPercentage);
  AProduct.Add('sdxFormatCellsDialogCategoryPercentageDescription', @sdxFormatCellsDialogCategoryPercentageDescription);
  AProduct.Add('sdxFormatCellsDialogCategoryScientific', @sdxFormatCellsDialogCategoryScientific);
  AProduct.Add('sdxFormatCellsDialogCategoryText', @sdxFormatCellsDialogCategoryText);
  AProduct.Add('sdxFormatCellsDialogCategoryTextNotes', @sdxFormatCellsDialogCategoryTextNotes);
  AProduct.Add('sdxFormatCellsDialogCategoryTime', @sdxFormatCellsDialogCategoryTime);
  AProduct.Add('sdxFormatCellsDialogCategoryTimeDescription', @sdxFormatCellsDialogCategoryTimeDescription);
  AProduct.Add('sdxFormatCellsDialogCustomCode', @sdxFormatCellsDialogCustomCode);
  AProduct.Add('sdxFormatCellsDialogDecimalPlaces', @sdxFormatCellsDialogDecimalPlaces);
  AProduct.Add('sdxFormatCellsDialogFill', @sdxFormatCellsDialogFill);
  AProduct.Add('sdxFormatCellsDialogFontColor', @sdxFormatCellsDialogFontColor);
  AProduct.Add('sdxFormatCellsDialogFontName', @sdxFormatCellsDialogFontName);
  AProduct.Add('sdxFormatCellsDialogFontNotInstalled', @sdxFormatCellsDialogFontNotInstalled);
  AProduct.Add('sdxFormatCellsDialogFontPreview', @sdxFormatCellsDialogFontPreview);
  AProduct.Add('sdxFormatCellsDialogFontPrintNotes', @sdxFormatCellsDialogFontPrintNotes);
  AProduct.Add('sdxFormatCellsDialogFontSize', @sdxFormatCellsDialogFontSize);
  AProduct.Add('sdxFormatCellsDialogFontStrikethrough', @sdxFormatCellsDialogFontStrikethrough);
  AProduct.Add('sdxFormatCellsDialogFontStyle', @sdxFormatCellsDialogFontStyle);
  AProduct.Add('sdxFormatCellsDialogFontUnderline', @sdxFormatCellsDialogFontUnderline);
  AProduct.Add('sdxFormatCellsDialogGroupFontEffects', @sdxFormatCellsDialogGroupFontEffects);
  AProduct.Add('sdxFormatCellsDialogGroupNumber', @sdxFormatCellsDialogGroupNumber);
  AProduct.Add('sdxFormatCellsDialogGroupTextAlignment', @sdxFormatCellsDialogGroupTextAlignment);
  AProduct.Add('sdxFormatCellsDialogHidden', @sdxFormatCellsDialogHidden);
  AProduct.Add('sdxFormatCellsDialogLocked', @sdxFormatCellsDialogLocked);
  AProduct.Add('sdxFormatCellsDialogMergeCells', @sdxFormatCellsDialogMergeCells);
  AProduct.Add('sdxFormatCellsDialogMoreColors', @sdxFormatCellsDialogMoreColors);
  AProduct.Add('sdxFormatCellsDialogNoColor', @sdxFormatCellsDialogNoColor);
  AProduct.Add('sdxFormatCellsDialogNone', @sdxFormatCellsDialogNone);
  AProduct.Add('sdxFormatCellsDialogNumberFormatTemplates', @sdxFormatCellsDialogNumberFormatTemplates);
  AProduct.Add('sdxFormatCellsDialogPatternColor', @sdxFormatCellsDialogPatternColor);
  AProduct.Add('sdxFormatCellsDialogPatternStyle', @sdxFormatCellsDialogPatternStyle);
  AProduct.Add('sdxFormatCellsDialogPreviewText', @sdxFormatCellsDialogPreviewText);
  AProduct.Add('sdxFormatCellsDialogProtection', @sdxFormatCellsDialogProtection);
  AProduct.Add('sdxFormatCellsDialogProtectionNotes', @sdxFormatCellsDialogProtectionNotes);
  AProduct.Add('sdxFormatCellsDialogSample', @sdxFormatCellsDialogSample);
  AProduct.Add('sdxFormatCellsDialogShrinkToFit', @sdxFormatCellsDialogShrinkToFit);
  AProduct.Add('sdxFormatCellsDialogTextAlignHorz', @sdxFormatCellsDialogTextAlignHorz);
  AProduct.Add('sdxFormatCellsDialogTextAlignHorzIndent', @sdxFormatCellsDialogTextAlignHorzIndent);
  AProduct.Add('sdxFormatCellsDialogTextAlignment', @sdxFormatCellsDialogTextAlignment);
  AProduct.Add('sdxFormatCellsDialogTextAlignVert', @sdxFormatCellsDialogTextAlignVert);
  AProduct.Add('sdxFormatCellsDialogTextControl', @sdxFormatCellsDialogTextControl);
  AProduct.Add('sdxFormatCellsDialogUnderlineNode', @sdxFormatCellsDialogUnderlineNode);
  AProduct.Add('sdxFormatCellsDialogUnderlineSingle', @sdxFormatCellsDialogUnderlineSingle);
  AProduct.Add('sdxFormatCellsDialogUseThousandSeparator', @sdxFormatCellsDialogUseThousandSeparator);
  AProduct.Add('sdxFormatCellsDialogWrapText', @sdxFormatCellsDialogWrapText);
  AProduct.Add('sdxHorzAlignCenter', @sdxHorzAlignCenter);
  AProduct.Add('sdxHorzAlignDistributed', @sdxHorzAlignDistributed);
  AProduct.Add('sdxHorzAlignFill', @sdxHorzAlignFill);
  AProduct.Add('sdxHorzAlignGeneral', @sdxHorzAlignGeneral);
  AProduct.Add('sdxHorzAlignJustify', @sdxHorzAlignJustify);
  AProduct.Add('sdxHorzAlignLeft', @sdxHorzAlignLeft);
  AProduct.Add('sdxHorzAlignRight', @sdxHorzAlignRight);
  AProduct.Add('sdxVertAlignBottom', @sdxVertAlignBottom);
  AProduct.Add('sdxVertAlignCenter', @sdxVertAlignCenter);
  AProduct.Add('sdxVertAlignDistributed', @sdxVertAlignDistributed);
  AProduct.Add('sdxVertAlignJustify', @sdxVertAlignJustify);
  AProduct.Add('sdxVertAlignTop', @sdxVertAlignTop);

  AProduct.Add('sdxCellsModificationDialogInsertCaption', @sdxCellsModificationDialogInsertCaption);
  AProduct.Add('sdxCellsModificationDialogDeleteCaption', @sdxCellsModificationDialogDeleteCaption);
  AProduct.Add('sdxShiftCellsDown', @sdxShiftCellsDown);
  AProduct.Add('sdxShiftCellsLeft', @sdxShiftCellsLeft);
  AProduct.Add('sdxShiftCellsRight', @sdxShiftCellsRight);
  AProduct.Add('sdxShiftCellsUp', @sdxShiftCellsUp);
  AProduct.Add('sdxShiftColumn', @sdxShiftColumn);
  AProduct.Add('sdxShiftRow', @sdxShiftRow);

  AProduct.Add('sdxContainerCustomizationDialogCaption', @sdxContainerCustomizationDialogCaption);
  AProduct.Add('sdxContainerCustomizationDialogButtonAdd', @sdxContainerCustomizationDialogButtonAdd);
  AProduct.Add('sdxContainerCustomizationDialogButtonCancel', @sdxContainerCustomizationDialogButtonCancel);
  AProduct.Add('sdxContainerCustomizationDialogButtonColor', @sdxContainerCustomizationDialogButtonColor);
  AProduct.Add('sdxContainerCustomizationDialogButtonLoad', @sdxContainerCustomizationDialogButtonLoad);
  AProduct.Add('sdxContainerCustomizationDialogButtonOK', @sdxContainerCustomizationDialogButtonOK);
  AProduct.Add('sdxContainerCustomizationDialogButtonRemove', @sdxContainerCustomizationDialogButtonRemove);
  AProduct.Add('sdxContainerCustomizationDialogButtonSave', @sdxContainerCustomizationDialogButtonSave);
  AProduct.Add('sdxContainerCustomizationDialogReset', @sdxContainerCustomizationDialogReset);
  AProduct.Add('sdxContainerCustomizationDialogDirection', @sdxContainerCustomizationDialogDirection);
  AProduct.Add('sdxContainerCustomizationDialogGradientFill', @sdxContainerCustomizationDialogGradientFill);
  AProduct.Add('sdxContainerCustomizationDialogGroupFill', @sdxContainerCustomizationDialogGroupFill);
  AProduct.Add('sdxContainerCustomizationDialogNoFill', @sdxContainerCustomizationDialogNoFill);
  AProduct.Add('sdxContainerCustomizationDialogSolidFill', @sdxContainerCustomizationDialogSolidFill);
  AProduct.Add('sdxContainerCustomizationDialogStops', @sdxContainerCustomizationDialogStops);
  AProduct.Add('sdxContainerCustomizationDialogTextureFill', @sdxContainerCustomizationDialogTextureFill);
  AProduct.Add('sdxContainerCustomizationDialogGradientLine', @sdxContainerCustomizationDialogGradientLine);
  AProduct.Add('sdxContainerCustomizationDialogLine', @sdxContainerCustomizationDialogLine);
  AProduct.Add('sdxContainerCustomizationDialogLineStyle', @sdxContainerCustomizationDialogLineStyle);
  AProduct.Add('sdxContainerCustomizationDialogLineWidth', @sdxContainerCustomizationDialogLineWidth);
  AProduct.Add('sdxContainerCustomizationDialogNoLine', @sdxContainerCustomizationDialogNoLine);
  AProduct.Add('sdxContainerCustomizationDialogSolidLine', @sdxContainerCustomizationDialogSolidLine);
  AProduct.Add('sdxContainerCustomizationDialogGroupProperties', @sdxContainerCustomizationDialogGroupProperties);
  AProduct.Add('sdxContainerCustomizationDialogPositioning', @sdxContainerCustomizationDialogPositioning);
  AProduct.Add('sdxContainerCustomizationDialogAbsolute', @sdxContainerCustomizationDialogAbsolute);
  AProduct.Add('sdxContainerCustomizationDialogOneCells', @sdxContainerCustomizationDialogOneCells);
  AProduct.Add('sdxContainerCustomizationDialogTwoCells', @sdxContainerCustomizationDialogTwoCells);
  AProduct.Add('sdxContainerCustomizationDialogCropBottom', @sdxContainerCustomizationDialogCropBottom);
  AProduct.Add('sdxContainerCustomizationDialogCropFrom', @sdxContainerCustomizationDialogCropFrom);
  AProduct.Add('sdxContainerCustomizationDialogCropLeft', @sdxContainerCustomizationDialogCropLeft);
  AProduct.Add('sdxContainerCustomizationDialogCropRight', @sdxContainerCustomizationDialogCropRight);
  AProduct.Add('sdxContainerCustomizationDialogCropTop', @sdxContainerCustomizationDialogCropTop);
  AProduct.Add('sdxContainerCustomizationDialogGroupSize', @sdxContainerCustomizationDialogGroupSize);
  AProduct.Add('sdxContainerCustomizationDialogHeight', @sdxContainerCustomizationDialogHeight);
  AProduct.Add('sdxContainerCustomizationDialogLockAspectRatio', @sdxContainerCustomizationDialogLockAspectRatio);
  AProduct.Add('sdxContainerCustomizationDialogOriginalSize', @sdxContainerCustomizationDialogOriginalSize);
  AProduct.Add('sdxContainerCustomizationDialogOriginalSizeFormatString', @sdxContainerCustomizationDialogOriginalSizeFormatString);
  AProduct.Add('sdxContainerCustomizationDialogRelativeToPictureSize', @sdxContainerCustomizationDialogRelativeToPictureSize);
  AProduct.Add('sdxContainerCustomizationDialogRotation', @sdxContainerCustomizationDialogRotation);
  AProduct.Add('sdxContainerCustomizationDialogScale', @sdxContainerCustomizationDialogScale);
  AProduct.Add('sdxContainerCustomizationDialogScaleHeight', @sdxContainerCustomizationDialogScaleHeight);
  AProduct.Add('sdxContainerCustomizationDialogScaleWidth', @sdxContainerCustomizationDialogScaleWidth);
  AProduct.Add('sdxContainerCustomizationDialogSizeAndRotate', @sdxContainerCustomizationDialogSizeAndRotate);
  AProduct.Add('sdxContainerCustomizationDialogWidth', @sdxContainerCustomizationDialogWidth);

  AProduct.Add('sdxGradientModeBackwardDiagonal', @sdxGradientModeBackwardDiagonal);
  AProduct.Add('sdxGradientModeForwardDiagonal', @sdxGradientModeForwardDiagonal);
  AProduct.Add('sdxGradientModeHorizontal', @sdxGradientModeHorizontal);
  AProduct.Add('sdxGradientModeVertical', @sdxGradientModeVertical);

  AProduct.Add('sdxPenStyleDash', @sdxPenStyleDash);
  AProduct.Add('sdxPenStyleDashDot', @sdxPenStyleDashDot);
  AProduct.Add('sdxPenStyleDashDotDot', @sdxPenStyleDashDotDot);
  AProduct.Add('sdxPenStyleDot', @sdxPenStyleDot);
  AProduct.Add('sdxPenStyleSolid', @sdxPenStyleSolid);end;

end.
