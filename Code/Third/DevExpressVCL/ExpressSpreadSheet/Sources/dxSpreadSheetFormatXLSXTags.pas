{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressSpreadSheet                                       }
{                                                                    }
{           Copyright (c) 2001-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSSPREADSHEET CONTROL AND ALL    }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY. }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxSpreadSheetFormatXLSXTags;

{$I cxVer.Inc}

interface

uses
{$IFDEF DELPHI16}
  System.UITypes,
{$ENDIF}
  Types, dxSpreadSheetTypes, dxSpreadSheetClasses, dxSpreadSheetCore, dxSpreadSheetGraphics,
  Graphics, cxGeometry, cxGraphics, dxGDIPlusClasses, dxSpreadSheetPrinting;

type
  TdxSpreadSheetXLSXCellType = (sxctUnknown, sxctBoolean, sxctError,
    sxctFloat, sxctFormula, sxctSharedString, sxctString, sxctRichText);

const
  sdxXLSXCommonContentType = AnsiString('application/vnd.openxmlformats-officedocument.spreadsheetml');
  sdxXLSXCommonRelationshipPath = AnsiString('http://schemas.openxmlformats.org/officeDocument/2006/relationships');

  sdxXLSXChartsheetRelationship = sdxXLSXCommonRelationshipPath + AnsiString('/chartsheet');
  sdxXLSXDrawingRelationship = sdxXLSXCommonRelationshipPath + AnsiString('/drawing');
  sdxXLSXExternalLinkPathRelationship = sdxXLSXCommonRelationshipPath + AnsiString('/externalLinkPath');
  sdxXLSXExternalLinkRelationship = sdxXLSXCommonRelationshipPath + AnsiString('/externalLink');
  sdxXLSXImageRelationship = sdxXLSXCommonRelationshipPath + AnsiString('/image');
  sdxXLSXSharedStringRelationship = sdxXLSXCommonRelationshipPath + AnsiString('/sharedStrings');
  sdxXLSXStyleRelationship = sdxXLSXCommonRelationshipPath + AnsiString('/styles');
  sdxXLSXThemeRelationship = sdxXLSXCommonRelationshipPath + AnsiString('/theme');
  sdxXLSXWorkbookRelationship = sdxXLSXCommonRelationshipPath + AnsiString('/officeDocument');
  sdxXLSXWorksheetRelationship = sdxXLSXCommonRelationshipPath + AnsiString('/worksheet');

  sdxXLSXDrawingContentType = AnsiString('application/vnd.openxmlformats-officedocument.drawing+xml');
  sdxXLSXExternalLinkContentType = sdxXLSXCommonContentType + AnsiString('.externalLink+xml');
  sdxXLSXRelsContentType = AnsiString('application/vnd.openxmlformats-package.relationships+xml');
  sdxXLSXSharedStringsContentType = sdxXLSXCommonContentType + AnsiString('.sharedStrings+xml');
  sdxXLSXStylesContentType = sdxXLSXCommonContentType + AnsiString('.styles+xml');
  sdxXLSXWorkbookContentType = sdxXLSXCommonContentType + AnsiString('.sheet.main+xml');
  sdxXLSXWorksheetContentType = sdxXLSXCommonContentType + AnsiString('.worksheet+xml');

  sdxXLSXContentTypeNamespace = AnsiString('http://schemas.openxmlformats.org/package/2006/content-types');
  sdxXLSXDrawingNamespace = AnsiString('http://schemas.openxmlformats.org/drawingml/2006/main');
  sdxXLSXDrawingNamespaceXDR = AnsiString('http://schemas.openxmlformats.org/drawingml/2006/spreadsheetDrawing');

  sdxXLSXRelsNamespace = AnsiString('http://schemas.openxmlformats.org/package/2006/relationships');
  sdxXLSXWorkbookNamespace = AnsiString('http://schemas.openxmlformats.org/spreadsheetml/2006/main');

  // FileName templates
  sdxXLSXFileTemplateDrawing = AnsiString('xl/drawings/drawing%d.xml');
  sdxXLSXFileTemplateExternalLink = AnsiString('xl/externalLinks/externalLink%d.xml');
  sdxXLSXFileTemplateImage = AnsiString('xl/media/image%d.%s');
  sdxXLSXFileTemplateWorksheet = AnsiString('xl/worksheets/sheet%d.xml');

  sdxXLSXContentTypeFileName = '[Content_Types].xml';
  sdxXLSXSharedStringsFileName = 'xl/sharedStrings.xml';
  sdxXLSXStylesFileName = 'xl/styles.xml';
  sdxXLSXWorkbookFileName = 'xl/workbook.xml';

const
  // Attributes
  sdxXLSXAttrActiveCell = AnsiString('activeCell');
  sdxXLSXAttrActivePane = AnsiString('activePane');
  sdxXLSXAttrActiveTab = AnsiString('activeTab');
  sdxXLSXAttrAlternateText = AnsiString('descr');
  sdxXLSXAttrAngle = AnsiString('ang');
  sdxXLSXAttrApplyAlignment = AnsiString('applyAlignment');
  sdxXLSXAttrApplyBorder = AnsiString('applyBorder');
  sdxXLSXAttrApplyFill = AnsiString('applyFill');
  sdxXLSXAttrApplyFont = AnsiString('applyFont');
  sdxXLSXAttrApplyNumberFormat = AnsiString('applyNumberFormat');
  sdxXLSXAttrApplyProtection = AnsiString('applyProtection');
  sdxXLSXAttrAutoFilter = 'autoFilter';
  sdxXLSXAttrBaseColumnWidth = AnsiString('baseColWidth');
  sdxXLSXAttrBestFit = AnsiString('bestFit');
  sdxXLSXAttrBorderId = AnsiString('borderId');
  sdxXLSXAttrBreakID = AnsiString('id');
  sdxXLSXAttrBreakManual = AnsiString('man');
  sdxXLSXAttrBreaksCount = AnsiString('count');
  sdxXLSXAttrBreaksManualBreakCount = AnsiString('manualBreakCount');
  sdxXLSXAttrCellColumn = AnsiString('r');
  sdxXLSXAttrCellType = AnsiString('t');
  sdxXLSXAttrContentType = AnsiString('ContentType');
  sdxXLSXAttrCoordExtX = AnsiString('cx');
  sdxXLSXAttrCoordExtY = AnsiString('cy');
  sdxXLSXAttrCoordX = AnsiString('x');
  sdxXLSXAttrCoordY = AnsiString('y');
  sdxXLSXAttrCount = AnsiString('count');
  sdxXLSXAttrCustomFormat = AnsiString('customFormat');
  sdxXLSXAttrCustomHeight = AnsiString('customHeight');
  sdxXLSXAttrCustomWidth = AnsiString('customWidth');
  sdxXLSXAttrDate1904 = AnsiString('date1904');
  sdxXLSXAttrDefaultColumnWidth = AnsiString('defaultColWidth');
  sdxXLSXAttrDefaultRowHeight = AnsiString('defaultRowHeight');
  sdxXLSXAttrDeleteColumns = AnsiString('deleteColumns');
  sdxXLSXAttrDeleteRows = 'deleteRows';
  sdxXLSXAttrDrawingResourceEmbed = AnsiString('r:embed');
  sdxXLSXAttrDrawingResourceLink = AnsiString('r:link');
  sdxXLSXAttrEditAs = AnsiString('editAs');
  sdxXLSXAttrExtension = AnsiString('Extension');
  sdxXLSXAttrFillId = AnsiString('fillId');
  sdxXLSXAttrFitToPage = AnsiString('fitToPage');
  sdxXLSXAttrFlip = AnsiString('flip');
  sdxXLSXAttrFlipH = AnsiString('flipH');
  sdxXLSXAttrFlipV = AnsiString('flipV');
  sdxXLSXAttrFontId = AnsiString('fontId');
  sdxXLSXAttrFormatCells = AnsiString('formatCells');
  sdxXLSXAttrFormatCode = AnsiString('formatCode');
  sdxXLSXAttrFormatColumns = AnsiString('formatColumns');
  sdxXLSXAttrFormatRows = AnsiString('formatRows');
  sdxXLSXAttrGradientPointPos = AnsiString('pos');
  sdxXLSXAttrGridLines = 'showGridLines';
  sdxXLSXAttrHeaderFooterAlignWithMargins = AnsiString('alignWithMargins');
  sdxXLSXAttrHeaderFooterDifferentFirst = AnsiString('differentFirst');
  sdxXLSXAttrHeaderFooterDifferentOddEven = AnsiString('differentOddEven');
  sdxXLSXAttrHeaderFooterScaleWithDocument = AnsiString('scaleWithDoc');
  sdxXLSXAttrHidden = AnsiString('hidden');
  sdxXLSXAttrHorizontal = AnsiString('horizontal');
  sdxXLSXAttrId = AnsiString('Id');
  sdxXLSXAttrIndent = 'indent';
  sdxXLSXAttrIndexed = AnsiString('indexed');
  sdxXLSXAttrInsertColumns = AnsiString('insertColumns');
  sdxXLSXAttrInsertHyperlinks = AnsiString('insertHyperlinks');
  sdxXLSXAttrInsertRows = AnsiString('insertRows');
  sdxXLSXAttrIterate = AnsiString('iterate');
  sdxXLSXAttrIterateCount = AnsiString('iterateCount');
  sdxXLSXAttrLineWidth = AnsiString('w');
  sdxXLSXAttrLocalSheetId = AnsiString('localSheetId');
  sdxXLSXAttrLocked = AnsiString('locked');
  sdxXLSXAttrLockStructure = AnsiString('lockStructure');
  sdxXLSXAttrMacro = AnsiString('macro');
  sdxXLSXAttrMax = AnsiString('max');
  sdxXLSXAttrMin = AnsiString('min');
  sdxXLSXAttrName = AnsiString('name');
  sdxXLSXAttrNumFmtId = AnsiString('numFmtId');
  sdxXLSXAttrPageMarginsBottom = AnsiString('bottom');
  sdxXLSXAttrPageMarginsFooter = AnsiString('footer');
  sdxXLSXAttrPageMarginsHeader = AnsiString('header');
  sdxXLSXAttrPageMarginsLeft = AnsiString('left');
  sdxXLSXAttrPageMarginsRight = AnsiString('right');
  sdxXLSXAttrPageMarginsTop = AnsiString('top');
  sdxXLSXAttrPageSetupBlackAndWhite = AnsiString('blackAndWhite');
  sdxXLSXAttrPageSetupCellComments = AnsiString('cellComments');
  sdxXLSXAttrPageSetupCopies = AnsiString('copies');
  sdxXLSXAttrPageSetupDraft = AnsiString('draft');
  sdxXLSXAttrPageSetupErrors = AnsiString('errors');
  sdxXLSXAttrPageSetupFirstPageNumber = AnsiString('firstPageNumber');
  sdxXLSXAttrPageSetupFitToHeight = AnsiString('fitToHeight');
  sdxXLSXAttrPageSetupFitToWidth = AnsiString('fitToWidth');
  sdxXLSXAttrPageSetupHorizontalDPI = AnsiString('horizontalDpi');
  sdxXLSXAttrPageSetupOrientation = AnsiString('orientation');
  sdxXLSXAttrPageSetupPageOrder = AnsiString('pageOrder');
  sdxXLSXAttrPageSetupPaperHeight = AnsiString('paperHeight');
  sdxXLSXAttrPageSetupPaperSize = AnsiString('paperSize');
  sdxXLSXAttrPageSetupPaperWidth = AnsiString('paperWidth');
  sdxXLSXAttrPageSetupScale = AnsiString('scale');
  sdxXLSXAttrPageSetupUseFirstPageNumber = AnsiString('useFirstPageNumber');
  sdxXLSXAttrPageSetupVerticalDPI = AnsiString('verticalDpi');
  sdxXLSXAttrPane = AnsiString('pane');
  sdxXLSXAttrPartName = AnsiString('PartName');
  sdxXLSXAttrPatternType = AnsiString('patternType');
  sdxXLSXAttrPivotTables = 'pivotTables';
  sdxXLSXAttrPreferRelativeResize = AnsiString('preferRelativeResize');
  sdxXLSXAttrPreset = AnsiString('prst');
  sdxXLSXAttrPrintOptionsGridLines = AnsiString('gridLines');
  sdxXLSXAttrPrintOptionsGridLinesSet = AnsiString('gridLinesSet');
  sdxXLSXAttrPrintOptionsHeadings = AnsiString('headings');
  sdxXLSXAttrPrintOptionsHorzCenter = AnsiString('horizontalCentered');
  sdxXLSXAttrPrintOptionsVertCenter = AnsiString('verticalCentered');
  sdxXLSXAttrRef = AnsiString('ref');
  sdxXLSXAttrRefIndex = AnsiString('idx');
  sdxXLSXAttrRefMode = AnsiString('refMode');
  sdxXLSXAttrRGB = AnsiString('rgb');
  sdxXLSXAttrRId = AnsiString('r:id');
  sdxXLSXAttrRot = AnsiString('rot');
  sdxXLSXAttrRotateWithShape = AnsiString('rotWithShape');
  sdxXLSXAttrRowHeight = AnsiString('ht');
  sdxXLSXAttrRowIndex = AnsiString('r');
  sdxXLSXAttrScaled = AnsiString('scaled');
  sdxXLSXAttrSheet = AnsiString('sheet');
  sdxXLSXAttrSheetId = AnsiString('sheetId');
  sdxXLSXAttrShowFormulas = 'showFormulas';
  sdxXLSXAttrShrinkToFit = AnsiString('shrinkToFit');
  sdxXLSXAttrSort = 'sort';
  sdxXLSXAttrSplitX = AnsiString('xSplit');
  sdxXLSXAttrSplitY = AnsiString('ySplit');
  sdxXLSXAttrSqRef = AnsiString('sqref');
  sdxXLSXAttrState = AnsiString('state');
  sdxXLSXAttrStyle = AnsiString('style');
  sdxXLSXAttrStyleIndex = AnsiString('s');
  sdxXLSXAttrTabSelected = AnsiString('tabSelected');
  sdxXLSXAttrTarget = AnsiString('Target');
  sdxXLSXAttrTargetMode = AnsiString('TargetMode');
  sdxXLSXAttrTextLink = AnsiString('textlink');
  sdxXLSXAttrTextRotation = AnsiString('textRotation');
  sdxXLSXAttrTheme = AnsiString('theme');
  sdxXLSXAttrThemeTint = AnsiString('tint');
  sdxXLSXAttrTileAlign = 'algn';
  sdxXLSXAttrTileSX = 'sx';
  sdxXLSXAttrTileSY = 'sy';
  sdxXLSXAttrTileTX = 'tx';
  sdxXLSXAttrTileTY = 'ty';
  sdxXLSXAttrTitle = AnsiString('title');
  sdxXLSXAttrTopLeftCell = AnsiString('topLeftCell');
  sdxXLSXAttrType = AnsiString('Type');
  sdxXLSXAttrUniqueCount = AnsiString('uniqueCount');
  sdxXLSXAttrVal = AnsiString('val');
  sdxXLSXAttrVertical = AnsiString('vertical');
  sdxXLSXAttrWidth = AnsiString('width');
  sdxXLSXAttrWorkbookViewId = AnsiString('workbookViewId');
  sdxXLSXAttrWrapText = AnsiString('wrapText');
  sdxXLSXAttrXFId = AnsiString('xfId');
  sdxXLSXAttrXMLNS = AnsiString('xmlns');
  sdxXLSXAttrXMLNSA = AnsiString('xmlns:a');
  sdxXLSXAttrXMLNSR = AnsiString('xmlns:r');
  sdxXLSXAttrXMLNSXDR = AnsiString('xmlns:xdr');
  sdxXLSXAttrZeroValues = 'showZeros';
  sdxXLSXAttrZoomScale = AnsiString('zoomScale');
  sdxXLSXAttrZoomScaleNormal = AnsiString('zoomScaleNormal');

  // Nodes
  sdxXLSXNodeAlignment = AnsiString('alignment');
  sdxXLSXNodeAnchorAbsolute = AnsiString('xdr:absoluteAnchor');
  sdxXLSXNodeAnchorFrom = AnsiString('xdr:from');
  sdxXLSXNodeAnchorOneCell = AnsiString('xdr:oneCellAnchor');
  sdxXLSXNodeAnchorTo = AnsiString('xdr:to');
  sdxXLSXNodeAnchorTwoCell = AnsiString('xdr:twoCellAnchor');
  sdxXLSXNodeAVList = AnsiString('a:avLst');
  sdxXLSXNodeBackgroundColor = AnsiString('bgColor');
  sdxXLSXNodeBookViews = AnsiString('bookViews');
  sdxXLSXNodeBreak = AnsiString('brk');
  sdxXLSXNodeCalcPr = AnsiString('calcPr');
  sdxXLSXNodeCellFunction = AnsiString('f');
  sdxXLSXNodeCellRichText = AnsiString('is');
  sdxXLSXNodeCellStylePatternFill = AnsiString('patternFill');
  sdxXLSXNodeCellValue = AnsiString('v');
  sdxXLSXNodeCharset = AnsiString('charset');
  sdxXLSXNodeClientData = AnsiString('xdr:clientData');
  sdxXLSXNodeColBreaks = AnsiString('colBreaks');
  sdxXLSXNodeColor = AnsiString('color');
  sdxXLSXNodeColorAlpha = AnsiString('a:alpha');
  sdxXLSXNodeColumn = AnsiString('col');
  sdxXLSXNodeColumns = AnsiString('cols');
  sdxXLSXNodeCoordExt = AnsiString('a:ext');
  sdxXLSXNodeCoordOff = AnsiString('a:off');
  sdxXLSXNodeDefault = AnsiString('Default');
  sdxXLSXNodeDefinedName = AnsiString('definedName');
  sdxXLSXNodeDefinedNames = AnsiString('definedNames');
  sdxXLSXNodeDiagonal = AnsiString('diagonal');
  sdxXLSXNodeDimension = AnsiString('dimension');
  sdxXLSXNodeDrawing = AnsiString('drawing');
  sdxXLSXNodeDrawingAttributeSourceRect = AnsiString('a:srcRect');
  sdxXLSXNodeDrawingBlip = AnsiString('a:blip');
  sdxXLSXNodeDrawingBlipFill = AnsiString('xdr:blipFill');
  sdxXLSXNodeDrawingDescription = AnsiString('xdr:cNvPr');
  sdxXLSXNodeDrawingHeader = AnsiString('xdr:wsDr');
  sdxXLSXNodeDrawingPatternBackgroundColor = AnsiString('a:bgClr');
  sdxXLSXNodeDrawingPatternForegroundColor = AnsiString('a:fgClr');
  sdxXLSXNodeDrawingPictureAttributes = AnsiString('xdr:cNvPicPr');
  sdxXLSXNodeDrawingPictureContainer = AnsiString('xdr:pic');
  sdxXLSXNodeDrawingPictureDescription = AnsiString('xdr:nvPicPr');
  sdxXLSXNodeDrawingPictureLocks = AnsiString('a:picLocks');
  sdxXLSXNodeDrawingPosMarkerColumn = AnsiString('xdr:col');
  sdxXLSXNodeDrawingPosMarkerColumnOffset = AnsiString('xdr:colOff');
  sdxXLSXNodeDrawingPosMarkerRow = AnsiString('xdr:row');
  sdxXLSXNodeDrawingPosMarkerRowOffset = AnsiString('xdr:rowOff');
  sdxXLSXNodeDrawingShapeAttributesEx = AnsiString('xdr:cNvSpPr');
  sdxXLSXNodeDrawingShapeContainer = AnsiString('xdr:sp');
  sdxXLSXNodeDrawingShapeDesription = AnsiString('xdr:nvSpPr');
  sdxXLSXNodeDrawingShapeGeometry = AnsiString('a:prstGeom');
  sdxXLSXNodeDrawingShapeLocks = AnsiString('a:spLocks');
  sdxXLSXNodeDrawingShapeProperties = AnsiString('xdr:spPr');
  sdxXLSXNodeDrawingStyle = AnsiString('xdr:style');
  sdxXLSXNodeDrawingXForm = AnsiString('a:xfrm');
  sdxXLSXNodeEvenFooter = AnsiString('evenFooter');
  sdxXLSXNodeEvenHeader = AnsiString('evenHeader');
  sdxXLSXNodeExt = AnsiString('xdr:ext');
  sdxXLSXNodeExternalBook = AnsiString('externalBook');
  sdxXLSXNodeExternalLink = AnsiString('externalLink');
  sdxXLSXNodeExternalReference = AnsiString('externalReference');
  sdxXLSXNodeExternalReferences = AnsiString('externalReferences');
  sdxXLSXNodeFillRect = AnsiString('a:fillRect');
  sdxXLSXNodeFillRef = AnsiString('a:fillRef');
  sdxXLSXNodeFirstFooter = AnsiString('firstFooter');
  sdxXLSXNodeFirstHeader = AnsiString('firstHeader');
  sdxXLSXNodeFontName = AnsiString('rFont');
  sdxXLSXNodeForegroundColor = AnsiString('fgColor');
  sdxXLSXNodeGradientFill = AnsiString('a:gradFill');
  sdxXLSXNodeGradientPoint = AnsiString('a:gs');
  sdxXLSXNodeGradientPoints = AnsiString('a:gsLst');
  sdxXLSXNodeHeaderFooter = AnsiString('headerFooter');
  sdxXLSXNodeLine = AnsiString('a:ln');
  sdxXLSXNodeLinearGradientFill = AnsiString('a:lin');
  sdxXLSXNodeLineDash = AnsiString('a:prstDash');
  sdxXLSXNodeLineRef = AnsiString('a:lnRef');
  sdxXLSXNodeLumMod = AnsiString('a:lumMod');
  sdxXLSXNodeLumOff = AnsiString('a:lumOff');
  sdxXLSXNodeMergeCell = AnsiString('mergeCell');
  sdxXLSXNodeMergeCells = AnsiString('mergeCells');
  sdxXLSXNodeName = AnsiString('name');
  sdxXLSXNodeNoFill = AnsiString('a:noFill');
  sdxXLSXNodeOddFooter = AnsiString('oddFooter');
  sdxXLSXNodeOddHeader = AnsiString('oddHeader');
  sdxXLSXNodeOverride = AnsiString('Override');
  sdxXLSXNodePageMargins = AnsiString('pageMargins');
  sdxXLSXNodePageSetup = AnsiString('pageSetup');
  sdxXLSXNodePageSetUpPr = AnsiString('pageSetUpPr');
  sdxXLSXNodePane = AnsiString('pane');
  sdxXLSXNodePatternFill = AnsiString('a:pattFill');
  sdxXLSXNodePos = AnsiString('xdr:pos');
  sdxXLSXNodePrintOptions = AnsiString('printOptions');
  sdxXLSXNodeProtection = AnsiString('protection');
  sdxXLSXNodeRelationship = AnsiString('Relationship');
  sdxXLSXNodeRelationships = AnsiString('Relationships');
  sdxXLSXNodeRichTextRun = AnsiString('r');
  sdxXLSXNodeRichTextRunParagraph = AnsiString('rPr');
  sdxXLSXNodeRow = AnsiString('row');
  sdxXLSXNodeRowBreaks = AnsiString('rowBreaks');
  sdxXLSXNodeSchemeColor = AnsiString('a:schemeClr');
  sdxXLSXNodeSelection = AnsiString('selection');
  sdxXLSXNodeSheet = AnsiString('sheet');
  sdxXLSXNodeSheetData = AnsiString('sheetData');
  sdxXLSXNodeSheetFormatPr = AnsiString('sheetFormatPr');
  sdxXLSXNodeSheetPr = AnsiString('sheetPr');
  sdxXLSXNodeSheetProtection = AnsiString('sheetProtection');
  sdxXLSXNodeSheets = AnsiString('sheets');
  sdxXLSXNodeSheetsView = AnsiString('sheetViews');
  sdxXLSXNodeSheetView = AnsiString('sheetView');
  sdxXLSXNodeSI = AnsiString('si');
  sdxXLSXNodeSolidFill = AnsiString('a:solidFill');
  sdxXLSXNodeSST = AnsiString('sst');
  sdxXLSXNodeStretch = AnsiString('a:stretch');
  sdxXLSXNodeStyleBorder = AnsiString('border');
  sdxXLSXNodeStyleBorders = AnsiString('borders');
  sdxXLSXNodeStyleCellStyleXfs = AnsiString('cellStyleXfs');
  sdxXLSXNodeStyleCellXf = AnsiString('xf');
  sdxXLSXNodeStyleCellXfs = AnsiString('cellXfs');
  sdxXLSXNodeStyleFill = AnsiString('fill');
  sdxXLSXNodeStyleFills = AnsiString('fills');
  sdxXLSXNodeStyleFont = AnsiString('font');
  sdxXLSXNodeStyleFonts = AnsiString('fonts');
  sdxXLSXNodeStyleNumberFormat = AnsiString('numFmt');
  sdxXLSXNodeStyleNumberFormats = AnsiString('numFmts');
  sdxXLSXNodeStyleSheet = AnsiString('styleSheet');
  sdxXLSXNodeSZ = AnsiString('sz');
  sdxXLSXNodeText = AnsiString('t');
  sdxXLSXNodeTexturedFill = AnsiString('a:blipFill');
  sdxXLSXNodeThemesColorScheme = AnsiString('a:clrScheme');
  sdxXLSXNodeThemesCustomColor = AnsiString('a:srgbClr');
  sdxXLSXNodeThemesElements = AnsiString('a:themeElements');
  sdxXLSXNodeThemesFormatScheme = AnsiString('a:fmtScheme');
  sdxXLSXNodeThemesFormatSchemeFillStyleList = AnsiString('a:fillStyleLst');
  sdxXLSXNodeThemesFormatSchemeLineStyleList = AnsiString('a:lnStyleLst');
  sdxXLSXNodeThemesSystemColor = AnsiString('a:sysClr');
  sdxXLSXNodeTile = 'a:tile';
  sdxXLSXNodeWorkbook = AnsiString('workbook');
  sdxXLSXNodeWorkbookPr = AnsiString('workbookPr');
  sdxXLSXNodeWorkbookProtection = AnsiString('workbookProtection');
  sdxXLSXNodeWorkBookView = AnsiString('workbookView');
  sdxXLSXNodeWorksheet = AnsiString('worksheet');

  // Mime Types
  sdxXLSXMimeTypeJPG = 'image/jpeg';
  sdxXLSXMimeTypeJPGExt = 'jpeg';
  sdxXLSXMimeTypePNG = 'image/png';
  sdxXLSXMimeTypePNGExt = 'png';
  sdxXLSXMimeTypeRELS = sdxXLSXRelsContentType;
  sdxXLSXMimeTypeRELSExt = 'rels';
  sdxXLSXMimeTypeXML = 'application/xml';
  sdxXLSXMimeTypeXMLExt = 'xml';

  // Values
  sdxXLSXValueA1 = AnsiString('A1');
  sdxXLSXValueArray = AnsiString('array');
  sdxXLSXValueEditAsAbsolute = AnsiString('absolute');
  sdxXLSXValueEditAsOneCell = AnsiString('oneCell');
  sdxXLSXValueEditAsTwoCell = AnsiString('twoCell');
  sdxXLSXValueFrozen = AnsiString('frozen');
  sdxXLSXValueR1C1 = AnsiString('R1C1');
  sdxXLSXValueShared = AnsiString('shared');
  sdxXLSXValueTargetModeExternal = AnsiString('External');
  sdxXLSXValuePaneBottomLeft = AnsiString('bottomLeft');
  sdxXLSXValuePaneBottomRight = AnsiString('bottomRight');
  sdxXLSXValuePaneTopRight = AnsiString('topRight');

  sdxXLSXPrintAreaDefinedName = '_xlnm.Print_Area';

const
  dxXLSXAlignHorzNames: array[TdxSpreadSheetDataAlignHorz] of AnsiString = (
    'general', 'left', 'center', 'right', 'fill', 'justify', 'distributed'
  );
  dxXLSXAlignVertNames: array[TdxSpreadSheetDataAlignVert] of AnsiString = (
    'top', 'center', 'bottom', 'justify', 'distributed'
  );
  dxXLSXBorderNames: array [TcxBorder] of AnsiString = (
    'left', 'top', 'right', 'bottom'
  );

  dxXLSXCellBorderStyleNames: array [TdxSpreadSheetCellBorderStyle] of AnsiString = (
    '', 'hair', 'dotted', 'dashDotDot', 'dashDot', 'dashed', 'thin', 'mediumDashDotDot', 'slantDashDot',
    'mediumDashDot', 'mediumDashed', 'medium', 'thick', 'double', 'none'
  );

  dxXLSXCellDataTypeNames: array[TdxSpreadSheetXLSXCellType] of AnsiString = (
    '?', 'b', 'e', 'n', 'str', 's', '', 'inlineStr'
  );
  dxXLSXCellFillStyleNames: array [TdxSpreadSheetCellFillStyle] of AnsiString = (
    'solid', 'darkGray', 'mediumGray', 'lightGray', 'gray125', 'gray0625',
    'darkHorizontal', 'darkVertical', 'darkDown', 'darkUp', 'darkGrid', 'darkTrellis',
    'lightHorizontal', 'lightVertical', 'lightDown', 'lightUp', 'lightGrid', 'lightTrellis'
  );
  dxXLSXFontStyles: array[TFontStyle] of AnsiString = ('b', 'i', 'u', 'strike');

  dxXLSXRestrictionNames: array[TdxSpreadSheetContainerRestriction] of AnsiString = (
    'noCrop', 'noMove', 'noResize', 'noRot', 'noChangeAspect'
  );

  dxXLSXShapeTypeMap: array[TdxSpreadSheetShapeType] of AnsiString = (
    'rect', 'roundRect', 'ellipse'
  );

  dxXLSXPenStyleMap: array[TdxGPPenStyle] of AnsiString = (
    'solid', 'dash', 'dot', 'dashDot', 'lgDashDotDot'
  );

  dxXLSXPrintErrorIndication: array[TdxSpreadSheetTableViewOptionsPrintSourceErrorIndication] of AnsiString = (
    '', 'blank', 'dash', 'displayed', 'NA'
  );

  dxXLSXPrintPageOrientation: array[TdxSpreadSheetTableViewOptionsPrintPageOrientation] of AnsiString = (
    'default', 'landscape', 'portrait'
  );

  dxXLSXPrintPageOrder: array[TdxSpreadSheetTableViewOptionsPrintPrintingPageOrder] of AnsiString = (
    '', 'downThenOver', 'overThenDown'
  );

type

  { TdxSpreadSheetXLSXHelper }

  TdxSpreadSheetXLSXHelper = class
  public
    class function AlignHorzToString(const S: TdxSpreadSheetDataAlignHorz): AnsiString;
    class function AlignVertToString(const S: TdxSpreadSheetDataAlignVert): AnsiString;
    class function BorderStyleToString(const S: TdxSpreadSheetCellBorderStyle): AnsiString;
    class function FillStyleToString(const S: TdxSpreadSheetBrushHandle): AnsiString;
    class function PenStyleToString(const S: TdxGPPenStyle): AnsiString;
    class function ShapeTypeToString(const S: TdxSpreadSheetShapeType): AnsiString;

    class function StringToAlignHorz(const S: AnsiString): TdxSpreadSheetDataAlignHorz;
    class function StringToAlignVert(const S: AnsiString): TdxSpreadSheetDataAlignVert;
    class function StringToBorderStyle(const S: AnsiString): TdxSpreadSheetCellBorderStyle;
    class function StringToCellType(const S: AnsiString): TdxSpreadSheetXLSXCellType;
    class function StringToFillStyle(const S: AnsiString): TdxSpreadSheetCellFillStyle;
    class function StringToPenStyle(const S: AnsiString): TdxGPPenStyle;
    class function StringToPrintErrorIndication(const S: AnsiString): TdxSpreadSheetTableViewOptionsPrintSourceErrorIndication;
    class function StringToPrintPageOrder(const S: AnsiString): TdxSpreadSheetTableViewOptionsPrintPrintingPageOrder;
    class function StringToPrintPageOrientation(const S: AnsiString): TdxSpreadSheetTableViewOptionsPrintPageOrientation;
    class function StringToShapeType(const S: AnsiString): TdxSpreadSheetShapeType;
  end;

implementation

uses
{$IFDEF DELPHI12}
  AnsiStrings,
{$ENDIF}
  SysUtils;

{ TdxSpreadSheetXLSXHelper }

class function TdxSpreadSheetXLSXHelper.AlignHorzToString(const S: TdxSpreadSheetDataAlignHorz): AnsiString;
begin
  Result := dxXLSXAlignHorzNames[S];
end;

class function TdxSpreadSheetXLSXHelper.AlignVertToString(const S: TdxSpreadSheetDataAlignVert): AnsiString;
begin
  Result := dxXLSXAlignVertNames[S];
end;

class function TdxSpreadSheetXLSXHelper.BorderStyleToString(const S: TdxSpreadSheetCellBorderStyle): AnsiString;
begin
  Result := dxXLSXCellBorderStyleNames[S];
end;

class function TdxSpreadSheetXLSXHelper.FillStyleToString(const S: TdxSpreadSheetBrushHandle): AnsiString;
begin
  if cxColorIsValid(S.ForegroundColor) or cxColorIsValid(S.BackgroundColor) or (S.Style <> sscfsSolid) then
    Result := dxXLSXCellFillStyleNames[S.Style]
  else
    Result := 'none';
end;

class function TdxSpreadSheetXLSXHelper.PenStyleToString(const S: TdxGPPenStyle): AnsiString;
begin
  Result := dxXLSXPenStyleMap[S];
end;

class function TdxSpreadSheetXLSXHelper.ShapeTypeToString(const S: TdxSpreadSheetShapeType): AnsiString;
begin
  Result := dxXLSXShapeTypeMap[S];
end;

class function TdxSpreadSheetXLSXHelper.StringToAlignHorz(const S: AnsiString): TdxSpreadSheetDataAlignHorz;
var
  I: TdxSpreadSheetDataAlignHorz;
begin
  Result := ssahGeneral;
  for I := Low(TdxSpreadSheetDataAlignHorz) to High(TdxSpreadSheetDataAlignHorz) do
    if SameText(S, dxXLSXAlignHorzNames[I]) then
    begin
      Result := I;
      Break;
    end;
end;

class function TdxSpreadSheetXLSXHelper.StringToAlignVert(const S: AnsiString): TdxSpreadSheetDataAlignVert;
var
  I: TdxSpreadSheetDataAlignVert;
begin
  Result := ssavBottom;
  for I := Low(TdxSpreadSheetDataAlignVert) to High(TdxSpreadSheetDataAlignVert) do
    if SameText(S, dxXLSXAlignVertNames[I]) then
    begin
      Result := I;
      Break;
    end;
end;

class function TdxSpreadSheetXLSXHelper.StringToFillStyle(const S: AnsiString): TdxSpreadSheetCellFillStyle;
var
  I: TdxSpreadSheetCellFillStyle;
begin
  Result := sscfsSolid;
  for I := Low(TdxSpreadSheetCellFillStyle) to High(TdxSpreadSheetCellFillStyle) do
    if SameText(S, dxXLSXCellFillStyleNames[I]) then
    begin
      Result := I;
      Break;
    end;
end;

class function TdxSpreadSheetXLSXHelper.StringToPenStyle(const S: AnsiString): TdxGPPenStyle;
const
  sLongDash = AnsiString('lgDash');
  sLongDashDot = AnsiString('lgDashDot');
  sLongDashDotDot = AnsiString('lgDashDotDot');
  sSystemDash = AnsiString('sysDash');
  sSystemDashDot = AnsiString('sysDashDot');
  sSystemDashDotDot = AnsiString('sysDashDotDot');
  sSystemDot = AnsiString('sysDot');
var
  I: TdxGPPenStyle;
begin
  Result := gppsSolid;
  for I := Low(TdxGPPenStyle) to High(TdxGPPenStyle) do
    if SameText(S, dxXLSXPenStyleMap[I]) then
    begin
      Result := I;
      Exit;
    end;

  if SameText(S, sLongDash) or SameText(S, sSystemDash) then
    Result := gppsDash
  else

  if SameText(S, sLongDashDot) or SameText(S, sSystemDashDot) then
    Result := gppsDashDot
  else

  if SameText(S, sLongDashDotDot) or SameText(S, sSystemDashDotDot) then
    Result := gppsDashDotDot
  else

  if SameText(S, sSystemDot) then
    Result := gppsDot;
end;

class function TdxSpreadSheetXLSXHelper.StringToPrintErrorIndication(
  const S: AnsiString): TdxSpreadSheetTableViewOptionsPrintSourceErrorIndication;
var
  I: TdxSpreadSheetTableViewOptionsPrintSourceErrorIndication;
begin
  for I := Low(TdxSpreadSheetTableViewOptionsPrintSourceErrorIndication) to High(TdxSpreadSheetTableViewOptionsPrintSourceErrorIndication) do
  begin
    if SameText(S, dxXLSXPrintErrorIndication[I]) then
      Exit(I);
  end;
  Result := pseiDefault;
end;

class function TdxSpreadSheetXLSXHelper.StringToPrintPageOrder(const S: AnsiString): TdxSpreadSheetTableViewOptionsPrintPrintingPageOrder;
var
  I: TdxSpreadSheetTableViewOptionsPrintPrintingPageOrder;
begin
  for I := Low(TdxSpreadSheetTableViewOptionsPrintPrintingPageOrder) to High(TdxSpreadSheetTableViewOptionsPrintPrintingPageOrder) do
  begin
    if SameText(S, dxXLSXPrintPageOrder[I]) then
      Exit(I);
  end;
  Result := opppDefault;
end;

class function TdxSpreadSheetXLSXHelper.StringToPrintPageOrientation(
  const S: AnsiString): TdxSpreadSheetTableViewOptionsPrintPageOrientation;
var
  I: TdxSpreadSheetTableViewOptionsPrintPageOrientation;
begin
  for I := Low(TdxSpreadSheetTableViewOptionsPrintPageOrientation) to High(TdxSpreadSheetTableViewOptionsPrintPageOrientation) do
  begin
    if SameText(S, dxXLSXPrintPageOrientation[I]) then
      Exit(I);
  end;
  Result := oppoDefault;
end;

class function TdxSpreadSheetXLSXHelper.StringToShapeType(const S: AnsiString): TdxSpreadSheetShapeType;
var
  I: TdxSpreadSheetShapeType;
begin
  for I := Low(TdxSpreadSheetShapeType) to High(TdxSpreadSheetShapeType) do
  begin
    if SameText(S, dxXLSXShapeTypeMap[I]) then
      Exit(I);
  end;
  Result := stRect;
end;

class function TdxSpreadSheetXLSXHelper.StringToBorderStyle(const S: AnsiString): TdxSpreadSheetCellBorderStyle;
var
  I: TdxSpreadSheetCellBorderStyle;
begin
  for I := Low(TdxSpreadSheetCellBorderStyle) to High(TdxSpreadSheetCellBorderStyle) do
  begin
    if SameText(S, dxXLSXCellBorderStyleNames[I]) then
      Exit(I);
  end;
  Result := sscbsDefault;
end;

class function TdxSpreadSheetXLSXHelper.StringToCellType(const S: AnsiString): TdxSpreadSheetXLSXCellType;
var
  I: TdxSpreadSheetXLSXCellType;
begin
  for I := Low(TdxSpreadSheetXLSXCellType) to High(TdxSpreadSheetXLSXCellType) do
  begin
    if SameText(S, dxXLSXCellDataTypeNames[I]) then
      Exit(I);
  end;
  Result := sxctUnknown;
end;

end.
