{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressSpreadSheet                                       }
{                                                                    }
{           Copyright (c) 2001-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSSPREADSHEET CONTROL AND ALL    }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY. }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxSpreadSheetFunctionsText;

{$I cxVer.inc}

interface

uses
{$IFDEF DELPHI16}
  System.UITypes,
{$ENDIF}
  Windows, SysUtils, Classes, StrUtils, DateUtils, Variants, dxCore,
  dxSpreadSheetCore, dxSpreadSheetTypes, dxSpreadSheetUtils, dxSpreadSheetClasses, dxSpreadSheetCoreHelpers;

procedure fnChar(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnClean(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnCode(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnConcatenate(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnDollar(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnExact(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnFind(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnFixed(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnLeft(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnLen(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnLower(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnMid(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnProper(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnReplace(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnRept(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnRight(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnSearch(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnSubstitute(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnT(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnText(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnTrim(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnUpper(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnValue(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);

function dxMaskSearch(const AMask, AText: TdxUnicodeString; AStartSearch: Integer): Integer;

implementation

uses cxClasses, dxSpreadSheetFunctionsMath, dxSpreadSheetNumberFormat;

type
  { TdxMaskToken }

  TdxMaskToken = class
  private
    FNum: Integer;
    FOwner: TcxObjectList;
    FToken: TdxUnicodeString;
    FWildChar: Char;
    function GetLength: Integer;
    function GetNextToken: TdxMaskToken;
  public
    constructor Create(AOwner: TcxObjectList; ANum: Integer; AToken: TdxUnicodeString; AWildChar: Char);
    function IsPinned: Boolean;

    property Length: Integer read GetLength;
    property NextToken: TdxMaskToken read GetNextToken;
    property Num: Integer read FNum;
    property Owner: TcxObjectList read FOwner;
    property Token: TdxUnicodeString read FToken;
    property WildChar: Char read FWildChar;
  end;

  constructor TdxMaskToken.Create(AOwner: TcxObjectList; ANum: Integer; AToken: TdxUnicodeString; AWildChar: Char);
  begin
    inherited Create;
    FOwner := AOwner;
    FNum := ANum;
    FWildChar := AWildChar;
    FToken := AToken;
  end;

  function TdxMaskToken.GetLength: Integer;
  begin
    Result := System.Length(FToken);
    if WildChar = '?' then
      Inc(Result);
  end;

  function TdxMaskToken.GetNextToken: TdxMaskToken;
  begin
    Result := nil;
    if Num < Owner.Count - 1 then
      Result := TdxMaskToken(Owner[Num + 1]);
  end;

  function TdxMaskToken.IsPinned: Boolean;
  begin
    Result := (Num > 0) and (TdxMaskToken(Owner[Num - 1]).WildChar <> '*');
  end;

function dxMaskSearch(const AMask, AText: TdxUnicodeString; AStartSearch: Integer): Integer;

  function CleanToken(const AToken: TdxUnicodeString): TdxUnicodeString;
  const
    ACleanStr: array[0..2] of TdxUnicodeString = ('~*', '~?', '""');
  var
    I, P: Integer;
  begin
    Result := AToken;
    for I := Low(ACleanStr) to High(ACleanStr) do
    begin
      P := Pos(ACleanStr[I], Result);
      while P > 0 do
      begin
        Delete(Result, P, 1);
        P := Pos(ACleanStr[I], Result);
      end;
    end;
  end;

  procedure PopulateTokenList(ATokenList: TcxObjectList; const S: TdxUnicodeString);
  var
    ANum, I, L: Integer;
    ATokenIsReady: Boolean;
    AToken: TdxUnicodeString;
    AWildChar: Char;
  begin
    ANum := 0;
    AToken := '';
    AWildChar := #0;
    L := Length(S);
    for I := 1 to L do
    begin
      ATokenIsReady := CharInSet(S[I], ['*', '?']) and ((I = 1) or ((I > 1) and (S[I - 1] <> '~')));
      if ATokenIsReady then
        AWildChar := S[I]
      else
      begin
        AToken := AToken + S[I];
        ATokenIsReady := I = L;
      end;

      if ATokenIsReady then
      begin
        AToken := CleanToken(AToken);
        ATokenList.Add(TdxMaskToken.Create(ATokenList, ANum, AToken, AWildChar));
        Inc(ANum);
        AToken := '';
        AWildChar := #0;
      end;
    end;
  end;

  function ExecuteSearch(AToken: TdxMaskToken; const S: TdxUnicodeString; AStart: Integer): Integer;
  var
    ANextToken: TdxMaskToken;
  begin
    Result := 0;
    while AStart + AToken.Length - 1 <= Length(S) do
    begin
      if AToken.Token = '' then
        Result := AStart
      else
      begin
        Result := PosEx(AToken.Token, S, AStart);
        if ((Result > AStart) and AToken.IsPinned) or (Result + AToken.Length - 1 > Length(S)) then
          Result := 0;
      end;
      if Result > 0 then
      begin
        ANextToken := AToken.NextToken;
        if (ANextToken <> nil) and (ExecuteSearch(ANextToken, S, Result + AToken.Length) = 0) then
          Result := 0;
      end;
      if (Result = 0) and not AToken.IsPinned then
        Inc(AStart)
      else
        Break;
    end;
  end;

var
  ATokenList: TcxObjectList;
begin
  Result := 0;
  if (AStartSearch < 1) or (AStartSearch > Length(AText)) then
    Exit;
  ATokenList := TcxObjectList.Create;
  try
    PopulateTokenList(ATokenList, dxSpreadSheetUpperCase(AMask));
    Result := ExecuteSearch(TdxMaskToken(ATokenList[0]), dxSpreadSheetUpperCase(AText), AStartSearch);
  finally
    ATokenList.Free;
  end;
end;

function dxGetFormattedStr(ANumber: Extended; AFormat: TFloatFormat; ADigits: Integer): string;
const
  APrecision = 18;
begin
  Result := FloatToStrF(ANumber, AFormat, APrecision, ADigits);
  if ADigits <= 0 then
    Delete(Result, Length(Result) - APrecision, APrecision + 1);
end;

function dxSubtitute(var AText: TdxUnicodeString; const AFromText, AToText: TdxUnicodeString; AReplaceAll: Boolean; AInstance: Integer): Boolean;
var
  AStart, ANum: Integer;
begin
  Result := AReplaceAll or (AInstance > 0);
  if not Result or (AFromText = '') then
    Exit;
  if AReplaceAll then
    AText := ReplaceStr(AText, AFromText, AToText)
  else
  begin
    AStart := 1;
    ANum := 1;
    while (AStart <> 0) and (AStart <= Length(AText)) do
    begin
      AStart := PosEx(AFromText, AText, AStart);
      if (AStart > 0) and (ANum = AInstance) then
      begin
        Delete(AText, AStart, Length(AFromText));
        Insert(AToText, AText, AStart);
      end
      else
      begin
        Inc(AStart, Integer(AStart > 0));
        Inc(ANum);
      end;
    end;
  end;
end;

procedure fnChar(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParameter: Variant;
begin
  if Sender.ExtractNumericParameter(AParameter, AParams) then
  begin
    AParameter := Trunc(AParameter);
    if (AParameter < 1) or (AParameter > 255) then
      Sender.SetError(ecValue)
    else
      Sender.AddValue(Chr(Byte(AParameter)));
  end;
end;

procedure fnClean(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParameter: Variant;
  st, AResult: TdxUnicodeString;
  I: Integer;
begin
  AParameter := Sender.ExtractStringParameter(AParams);
  st := AParameter;
  AResult := '';
  for I := 1 to Length(st) do
    if not CharInSet(st[I], [#0.. #31]) then
      AResult := AResult + st[I];
  Sender.AddValue(AResult);
end;

procedure fnCode(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  st: TdxUnicodeString;
begin
  st := VarToStr(Sender.ExtractStringParameter(AParams));
  if st = '' then
    Sender.SetError(ecValue)
  else
    Sender.AddValue(Ord(st[1]));
end;

function cbConcatenate(const AValue: Variant; ACanConvertStrToNumber: Boolean;
  var AErrorCode: TdxSpreadSheetFormulaErrorCode; AData: TdxSpreadSheetEnumValues; AInfo: Pointer): Boolean;
begin
  Result := AErrorCode = ecNone;
  if Result then
    AData.ResultValue := AData.ResultValue + VarToStr(AValue)
  else
    AData.SetErrorCode(AErrorCode);
end;

procedure fnConcatenate(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AData: TdxSpreadSheetEnumValues;
  ADataInfo: TdxSpreadSheetEnumValuesProcessingInfo;
begin
  ADataInfo.Init(False, False, False, False);
  AData := TdxSpreadSheetEnumValues.Create(Sender.FormatSettings, '', ADataInfo);
  try
    dxSpreadSheetCalculateForEachParam(Sender, AParams, AData, @cbConcatenate);
    if AData.Validate then
      Sender.AddValue(AData.ResultValue)
    else
      Sender.SetError(AData.ErrorCode);
  finally
    FreeAndNil(AData);
  end;
end;

procedure fnDollar(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  ANumber, ADigits: Variant;
  AExtNumber: Extended;
  AResult: TdxUnicodeString;
begin
  if Sender.ExtractNumericParameter(ANumber, AParams) and
     Sender.ExtractNumericParameterDef(ADigits, 2, 0, AParams, 1) then
  begin
    ADigits := Trunc(ADigits);
    AExtNumber := ANumber;
    if dxSpreadSheetRound(Sender, AExtNumber, ADigits) then
    begin
      AResult := '$' + dxGetFormattedStr(Abs(AExtNumber), ffNumber, ADigits);
      if AExtNumber < 0 then
        AResult := '(' + AResult + ')';
      Sender.AddValue(AResult);
    end;
  end;
end;

procedure fnExact(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AText1, AText2: TdxUnicodeString;
begin
  AText1 := VarToStr(Sender.ExtractStringParameter(AParams));
  AText2 := VarToStr(Sender.ExtractStringParameter(AParams.Next));
  Sender.AddValue(AnsiCompareStr(AText1, AText2) = 0);
end;

procedure fnFind(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  ASubstr, AStr: TdxUnicodeString;
  AStart: Variant;
  AIntStart, AResult: Integer;
begin
  ASubstr := VarToStr(Sender.ExtractStringParameter(AParams));
  AStr := VarToStr(Sender.ExtractStringParameter(AParams.Next));
  if Sender.ExtractNumericParameterDef(AStart, 1, 0, AParams, 2) then
  begin
    AIntStart := Trunc(AStart);
    if (AIntStart < 1) or (AIntStart > Length(AStr)) then
      Sender.SetError(ecValue)
    else
      if ASubstr = '' then
        Sender.AddValue(AIntStart)
      else
      begin
        AResult := PosEx(ASubstr, AStr, AIntStart);
        if AResult > 0 then
          Sender.AddValue(AResult)
        else
          Sender.SetError(ecValue);
      end;
  end;
end;

procedure fnFixed(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
const
  AFormat: array[Boolean] of TFloatFormat = (ffFixed, ffNumber);
var
  ANumber, ADigits, ANoCommas: Variant;
  AExtNumber: Extended;
begin
  if Sender.ExtractNumericParameter(ANumber, AParams) and
     Sender.ExtractNumericParameterDef(ADigits, 2, 2, AParams, 1) and
     Sender.ExtractNumericParameterDef(ANoCommas, 0, 0, AParams, 2) then
  begin
    ADigits := Trunc(ADigits);
    AExtNumber := Extended(ANumber);
    if dxSpreadSheetRound(Sender, AExtNumber, ADigits) then
      Sender.AddValue(dxGetFormattedStr(AExtNumber, AFormat[Trunc(ANoCommas) = 0], ADigits));
  end;
end;

procedure fnLeft(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AStr: TdxUnicodeString;
  ALength: Variant;
  AIntLength: Integer;
begin
  AStr := VarToStr(Sender.ExtractStringParameter(AParams));
  if Sender.ExtractNumericParameterDef(ALength, 1, 0, AParams, 1) then
    if ALength < 0 then
      Sender.SetError(ecValue)
    else
    begin
      AIntLength := Trunc(ALength);
      if AIntLength = 0 then
        Sender.AddValue('')
      else
        if AIntLength > Length(AStr) then
          Sender.AddValue(AStr)
        else
          Sender.AddValue(LeftStr(AStr, AIntLength));
    end;
end;

procedure fnLen(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
begin
  Sender.AddValue(Length(Sender.ExtractStringParameter(AParams)));
end;

procedure fnLower(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
begin
  Sender.AddValue(dxSpreadSheetLowerCase(Sender.ExtractStringParameter(AParams)));
end;

procedure fnMid(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AStr: TdxUnicodeString;
  AStart, ACount: Variant;
  AIntStart, AIntCount: Integer;
begin
  AStr := VarToStr(Sender.ExtractStringParameter(AParams));
  if Sender.ExtractNumericParameter(AStart, AParams, 1) and Sender.ExtractNumericParameter(ACount, AParams, 2) then
  begin
    AIntStart := Trunc(AStart);
    AIntCount := Trunc(ACount);
    if (AIntStart < 1) or (AIntCount < 0) then
      Sender.SetError(ecValue)
    else
      if (AIntStart > Length(AStr)) or (AIntCount = 0) then
        Sender.AddValue('')
      else
        Sender.AddValue(MidStr(AStr, AIntStart, AIntCount));
  end;
end;

procedure fnProper(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);

  function IsPriorCharNonLetter(const ch: Char): Boolean;
  begin
    Result := (WideUpperCase(ch) = ch) and (WideLowerCase(ch) = ch);
  end;

var
  S, AResult: TdxUnicodeString;
  ALen, I: Integer;
begin
  S := dxSpreadSheetLowerCase(Sender.ExtractStringParameter(AParams));
  AResult := '';
  ALen := Length(S);
  if ALen > 0 then
    for I := 1 to ALen do
      if (I = 1) or ((I > 1) and IsPriorCharNonLetter(S[I - 1])) then
        AResult := AResult + WideUpperCase(S[I])
      else
        AResult := AResult + S[I];
  Sender.AddValue(AResult);
end;

procedure fnReplace(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  ASource, ANew: TdxUnicodeString;
  AStart, ACount: Variant;
  AIntStart, AIntCount: Integer;
begin
  ASource := Sender.ExtractStringParameter(AParams);
  ANew := Sender.ExtractStringParameter(AParams, 3);
  if Sender.ExtractNumericParameter(AStart, AParams, 1) and Sender.ExtractNumericParameter(ACount, AParams, 2) then
  begin
    AIntStart := Trunc(AStart);
    AIntCount := Trunc(ACount);
    if (AIntStart < 1) or (AIntCount < 0) then
      Sender.SetError(ecValue)
    else
    begin
      Delete(ASource, AIntStart, AIntCount);
      Insert(ANew, ASource, AIntStart);
      Sender.AddValue(ASource);
    end;
  end;
end;

procedure fnRept(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AText, AResult: TdxUnicodeString;
  ACount: Variant;
  I: Integer;
begin
  AText := Sender.ExtractStringParameter(AParams);
  if Sender.ExtractNumericParameter(ACount, AParams, 1) then
  begin
    ACount := Trunc(ACount);
    if (ACount < 0) or (ACount * Length(AText) > 32767) then
      Sender.SetError(ecValue)
    else
    begin
      AResult := '';
      for I := 1 to ACount do
        AResult := AResult + AText;
      Sender.AddValue(AResult);
    end;
  end;
end;

procedure fnRight(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AStr: TdxUnicodeString;
  ACount: Variant;
  AIntCount: Integer;
begin
  AStr := VarToStr(Sender.ExtractStringParameter(AParams));
  if Sender.ExtractNumericParameterDef(ACount, 1, 0, AParams, 1) then
    if ACount < 0 then
      Sender.SetError(ecValue)
    else
    begin
      AIntCount := Trunc(ACount);
      if AIntCount = 0 then
        Sender.AddValue('')
      else
        if AIntCount > Length(AStr) then
          Sender.AddValue(AStr)
        else
          Sender.AddValue(RightStr(AStr, AIntCount));
    end;
end;

procedure fnSearch(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  ASubstr, S: TdxUnicodeString;
  AStart: Variant;
  AResult: Integer;
begin
  ASubstr := VarToStr(Sender.ExtractStringParameter(AParams));
  S := VarToStr(Sender.ExtractStringParameter(AParams, 1));
  if Sender.ExtractNumericParameterDef(AStart, 1, AParams, 2) then
  begin
    AStart := Trunc(AStart);
    if (AStart < 1) or (AStart > Length(S)) then
      Sender.SetError(ecValue)
    else
      if ASubstr = '' then
        Sender.AddValue(AStart)
      else
      begin
        AResult := dxMaskSearch(ASubstr, S, AStart);
        if AResult > 0 then
          Sender.AddValue(AResult)
        else
          Sender.SetError(ecValue);
      end;
  end;
end;

procedure fnSubstitute(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AText, AOld, ANew: TdxUnicodeString;
  AInstance: Variant;
  AReplaceAll: Boolean;
begin
  AText := Sender.ExtractStringParameter(AParams);
  AOld := Sender.ExtractStringParameter(AParams, 1);
  ANew := Sender.ExtractStringParameter(AParams, 2);
  AReplaceAll := not Sender.ParameterExists(AParams, 3);
  AInstance := 0;
  if not AReplaceAll then
    if not Sender.ExtractNumericParameter(AInstance, AParams, 3) then
      Exit;
  if dxSubtitute(AText, AOld, ANew, AReplaceAll, Trunc(AInstance)) then
    Sender.AddValue(AText)
  else
    Sender.SetError(ecValue);
end;

procedure fnT(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParameter: Variant;
begin
  if Sender.ExtractParameter(AParameter, AParams) then
    if VarIsStr(AParameter) then
      Sender.AddValue(AParameter)
    else
      Sender.AddValue('');
end;

procedure fnText(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParameter, AFormatText: Variant;
  AFormat: TdxSpreadSheetCustomNumberFormat;
  AFormatSettings: TFormatSettings;
  AResult: TdxSpreadSheetNumberFormatResult;
begin
  if Sender.ExtractParameterDef(AParameter, 0, AParams) and Sender.ExtractParameterDef(AFormatText, '', AParams, 1) then
  begin
    if (AFormatText = '') and dxIsNumberOrDateTime(AParameter) then
      Sender.AddValue('')
    else
    begin
      AFormat := TdxSpreadSheetPredefinedNumberFormat.Create(AFormatText);
      try
        dxGetLocaleFormatSettings(dxGetInvariantLocaleID, AFormatSettings);
        AFormat.Format(AParameter, dxGetDataTypeByVariantValue(AParameter), dts1900, AFormatSettings, AResult);
        if AResult.IsError then
          Sender.SetError(ecValue)
        else
          Sender.AddValue(AResult.Text);
      finally
        AFormat.Free;
      end;
    end;
  end;
end;

procedure fnTrim(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  S: TdxUnicodeString;
  APos: Integer;
begin
  S := Sender.ExtractStringParameter(AParams);
  S := Trim(S);
  repeat
    APos := Pos('  ', S);
    if APos > 0 then
      Delete(S, APos, 1);
  until APos = 0;
  Sender.AddValue(S);
end;

procedure fnUpper(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
begin
  Sender.AddValue(dxSpreadSheetUpperCase(Sender.ExtractStringParameter(AParams)));
end;

procedure fnValue(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParameter: Variant;
begin
  if Sender.ExtractNumericParameterWithoutBoolean(AParameter, AParams) or
     Sender.ExtractDateTimeParameterWithoutBoolean(AParameter, AParams)  then
    Sender.AddValue(AParameter);
end;

end.
