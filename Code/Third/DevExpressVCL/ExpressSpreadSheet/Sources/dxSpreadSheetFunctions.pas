{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressSpreadSheet                                       }
{                                                                    }
{           Copyright (c) 2001-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSSPREADSHEET CONTROL AND ALL    }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY. }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxSpreadSheetFunctions;

{$I cxVer.inc}

interface

uses
{$IFDEF DELPHI16}
  System.UITypes,
{$ENDIF}
  Windows, Classes, SysUtils, Types, 
  dxCore, dxCoreClasses, cxClasses, dxSpreadSheetCore, dxSpreadSheetTypes, dxSpreadSheetUtils,
  dxSpreadSheetFormulas, dxSpreadSheetStrs, dxSpreadSheetClasses,
  dxSpreadSheetFunctionsCompatibility, dxSpreadSheetFunctionsDateTime, dxSpreadSheetFunctionsInformation,
  dxSpreadSheetFunctionsFinancial, dxSpreadSheetFunctionsLogical, dxSpreadSheetFunctionsLookup,
  dxSpreadSheetFunctionsMath, dxSpreadSheetFunctionsStatistical, dxSpreadSheetFunctionsText,
  dxSpreadSheetFunctionsParamInfoCompatibility, dxSpreadSheetFunctionsParamInfoDateTime, dxSpreadSheetFunctionsParamInfoInformation,
  dxSpreadSheetFunctionsParamInfoFinancial, dxSpreadSheetFunctionsParamInfoLogical, dxSpreadSheetFunctionsParamInfoLookup,
  dxSpreadSheetFunctionsParamInfoMath, dxSpreadSheetFunctionsParamInfoStatistical, dxSpreadSheetFunctionsParamInfoText;

type
  { TdxSpreadSheetFunctionsRepository }

  TdxSpreadSheetFunctionsRepository = class(TcxObjectList)
  private
    FSorted: Boolean;
    function GetItem(AIndex: Integer): TdxSpreadSheetFunctionInfo;
    procedure SetSorted(AValue: Boolean);
  protected
    property Sorted: Boolean read FSorted write SetSorted;
  public
    procedure Add(const AName: TcxResourceStringID; AProc: TdxSpreadSheetFunction; AParamInfo: TdxSpreadSheetFunctionParamInfo;
      AResultKind: TdxSpreadSheetFunctionResultKind; AToken: Word; AType: TdxSpreadSheetFunctionType = ftCommon);
    function AddUnknown(const AName: TdxUnicodeString; AType: TdxSpreadSheetFunctionType = ftCommon): TdxSpreadSheetFunctionInfo;
    function GetInfoByID(const ID: Integer): TdxSpreadSheetFunctionInfo; inline;
    function GetInfoByName(const AName: TdxUnicodeString): TdxSpreadSheetFunctionInfo; overload; inline;
    function GetInfoByName(const AName: PdxUnicodeChar; ALength: Integer): TdxSpreadSheetFunctionInfo; overload; inline;
    procedure TranslationChanged(AFormatSettings: TdxSpreadSheetFormatSettings);

    property Items[Index: Integer]: TdxSpreadSheetFunctionInfo read GetItem;
  end;

  function dxSpreadSheetFunctionsRepository: TdxSpreadSheetFunctionsRepository;

implementation


type

  { TdxSpreadSheedFunctionRec }

  TdxSpreadSheedFunctionRec = record
    NamePtr: Pointer;
    ParamInfo: TdxSpreadSheetFunctionParamInfo;
    Proc: TdxSpreadSheetFunction;
    ResultKind: TdxSpreadSheetFunctionResultKind;
    Token: Word;
    TypeID: TdxSpreadSheetFunctionType;
  end;

var
  Repository: TdxSpreadSheetFunctionsRepository;

  PredefinedFunctions: array[0..241] of TdxSpreadSheedFunctionRec = (
    // Compatibility
   (NamePtr: @sfnBetaDist; ParamInfo: fpiBetaDist; Proc: fnBetaDist; ResultKind: frkValue; Token: 270; TypeID: ftCompatibility),
   (NamePtr: @sfnBetaInv; ParamInfo: fpiBetaInv; Proc: fnBetaInv; ResultKind: frkValue; Token: 272; TypeID: ftCompatibility),
   (NamePtr: @sfnBinomDist; ParamInfo: fpiBinomDist; Proc: fnBinomDist; ResultKind: frkValue; Token: 273; TypeID: ftCompatibility),
   (NamePtr: @sfnChiDist; ParamInfo: fpiChiDist; Proc: fnChiDist; ResultKind: frkValue; Token: 274; TypeID: ftCompatibility),
   (NamePtr: @sfnChiInv; ParamInfo: fpiChiInv; Proc: fnChiInv; ResultKind: frkValue; Token: 275; TypeID: ftCompatibility),
   (NamePtr: @sfnCovar; ParamInfo: fpiCovariance_P; Proc: fnCovariance_P; ResultKind: frkNonArrayValue; Token: 308; TypeID: ftCompatibility),
   (NamePtr: @sfnExponDist; ParamInfo: fpiExponDist; Proc: fnExponDist; ResultKind: frkValue; Token: 280; TypeID: ftCompatibility),
   (NamePtr: @sfnGammaDist; ParamInfo: fpiGamma_Dist; Proc: fnGamma_Dist; ResultKind: frkValue; Token: 286; TypeID: ftCompatibility),
   (NamePtr: @sfnGammaInv; ParamInfo: fpiGamma_Inv; Proc: fnGamma_Inv; ResultKind: frkValue; Token: 287; TypeID: ftCompatibility),
   (NamePtr: @sfnHypgeomDist; ParamInfo: fpiHypgeomDist; Proc: fnHypgeomDist; ResultKind: frkValue; Token: 289; TypeID: ftCompatibility),
   (NamePtr: @sfnNormDist; ParamInfo: fpiNormDist; Proc: fnNormDist; ResultKind: frkValue; Token: 293; TypeID: ftCompatibility),
   (NamePtr: @sfnNormInv; ParamInfo: fpiNormInv; Proc: fnNormInv; ResultKind: frkValue; Token: 295; TypeID: ftCompatibility),
   (NamePtr: @sfnNormSDist; ParamInfo: fpiNormSDist; Proc: fnNormSDist; ResultKind: frkValue; Token: 294; TypeID: ftCompatibility),
   (NamePtr: @sfnNormSInv; ParamInfo: fpiNormSInv; Proc: fnNormSInv; ResultKind: frkValue; Token: 296; TypeID: ftCompatibility),
   (NamePtr: @sfnPercentile; ParamInfo: fpiPercentile_Inc; Proc: fnPercentile_Inc; ResultKind: frkValue; Token: 328; TypeID: ftCompatibility),
   (NamePtr: @sfnQuartile; ParamInfo: fpiQuartile_Inc; Proc: fnQuartile_Inc; ResultKind: frkValue; Token: 327; TypeID: ftCompatibility),
   (NamePtr: @sfnPoisson; ParamInfo: fpiPoisson; Proc: fnPoisson; ResultKind: frkValue; Token: 300; TypeID: ftCompatibility),
   (NamePtr: @sfnRank; ParamInfo: fpiRank; Proc: fnRank; ResultKind: frkValue; Token: 216; TypeID: ftCompatibility),
   (NamePtr: @sfnStDev; ParamInfo: fpiStDev; Proc: fnStDev; ResultKind: frkNonArrayValue; Token: 12; TypeID: ftCompatibility),
   (NamePtr: @sfnStDevP; ParamInfo: fpiStDevP; Proc: fnStDevP; ResultKind: frkNonArrayValue; Token: 193; TypeID: ftCompatibility),
   (NamePtr: @sfnTDist; ParamInfo: fpiTDist; Proc: fnTDist; ResultKind: frkValue; Token: 301; TypeID: ftCompatibility),
   (NamePtr: @sfnTInv; ParamInfo: fpiTInv; Proc: fnTInv; ResultKind: frkValue; Token: 332; TypeID: ftCompatibility),
   (NamePtr: @sfnVar; ParamInfo: fpiVar; Proc: fnVar; ResultKind: frkNonArrayValue; Token: 46; TypeID: ftCompatibility),
   (NamePtr: @sfnVarP; ParamInfo: fpiVarP; Proc: fnVarP; ResultKind: frkNonArrayValue; Token: 194; TypeID: ftCompatibility),
   (NamePtr: @sfnWeibull; ParamInfo: fpiWeibull; Proc: fnWeibull; ResultKind: frkValue; Token: 302; TypeID: ftCompatibility),
    // DateTime
   (NamePtr: @sfnDate; ParamInfo: fpiDate; Proc: fnDate; ResultKind: frkValue; Token: 65; TypeID: ftDateTime),
   (NamePtr: @sfnDateValue; ParamInfo: fpiDateValue; Proc: fnDateValue; ResultKind: frkValue; Token: 140; TypeID: ftDateTime),
   (NamePtr: @sfnDay; ParamInfo: fpiDay; Proc: fnDay; ResultKind: frkValue; Token: 67; TypeID: ftDateTime),
   (NamePtr: @sfnDays; ParamInfo: fpiDays; Proc: fnDays; ResultKind: frkValue; Token: 255; TypeID: ftDateTime),
   (NamePtr: @sfnDays360; ParamInfo: fpiDays360; Proc: fnDays360; ResultKind: frkValue; Token: 220; TypeID: ftDateTime),
   (NamePtr: @sfnEDate; ParamInfo: fpiEDate; Proc: fnEDate; ResultKind: frkValue; Token: 255; TypeID: ftDateTime),
   (NamePtr: @sfnEOMonth; ParamInfo: fpiEOMonth; Proc: fnEOMonth; ResultKind: frkValue; Token: 255; TypeID: ftDateTime),
   (NamePtr: @sfnHour; ParamInfo: fpiHour; Proc: fnHour; ResultKind: frkValue; Token: 71; TypeID: ftDateTime),
   (NamePtr: @sfnIsoWeekNum; ParamInfo: fpiIsoWeekNum; Proc: fnIsoWeekNum; ResultKind: frkValue; Token: 255; TypeID: ftDateTime),
   (NamePtr: @sfnMinute; ParamInfo: fpiMinute; Proc: fnMinute; ResultKind: frkValue; Token: 72; TypeID: ftDateTime),
   (NamePtr: @sfnMonth; ParamInfo: fpiMonth; Proc: fnMonth; ResultKind: frkValue; Token: 68; TypeID: ftDateTime),
   (NamePtr: @sfnNow; ParamInfo: fpiNow; Proc: fnNow; ResultKind: frkValue; Token: 74; TypeID: ftDateTime),
   (NamePtr: @sfnSecond; ParamInfo: fpiSecond; Proc: fnSecond; ResultKind: frkValue; Token: 73; TypeID: ftDateTime),
   (NamePtr: @sfnTime; ParamInfo: fpiTime; Proc: fnTime; ResultKind: frkValue; Token: 66; TypeID: ftDateTime),
   (NamePtr: @sfnTimeValue; ParamInfo: fpiTimeValue; Proc: fnTimeValue; ResultKind: frkValue; Token: 141; TypeID: ftDateTime),
   (NamePtr: @sfnToday; ParamInfo: fpiToday; Proc: fnToday; ResultKind: frkValue; Token: 221; TypeID: ftDateTime),
   (NamePtr: @sfnWeekDay; ParamInfo: fpiWeekDay; Proc: fnWeekDay; ResultKind: frkValue; Token: 70; TypeID: ftDateTime),
   (NamePtr: @sfnWeekNum; ParamInfo: fpiWeekNum; Proc: fnWeekNum; ResultKind: frkValue; Token: 255; TypeID: ftDateTime),
   (NamePtr: @sfnYear; ParamInfo: fpiYear; Proc: fnYear; ResultKind: frkValue; Token: 69; TypeID: ftDateTime),
   (NamePtr: @sfnYearFrac; ParamInfo: fpiYearFrac; Proc: fnYearFrac; ResultKind: frkValue; Token: 255; TypeID: ftDateTime),
  // Financial
   (NamePtr: @sfnFV; ParamInfo: fpiFV; Proc: fnFV; ResultKind: frkValue; Token: 57; TypeID: ftFinancial),
   (NamePtr: @sfnIPMT; ParamInfo: fpiIPMT; Proc: fnIPMT; ResultKind: frkValue; Token: 167; TypeID: ftFinancial),
   (NamePtr: @sfnNPV; ParamInfo: fpiNPV; Proc: fnNPV; ResultKind: frkValue; Token: 11; TypeID: ftFinancial),
   (NamePtr: @sfnNPer; ParamInfo: fpiNPer; Proc: fnNPer; ResultKind: frkValue; Token: 58; TypeID: ftFinancial),
   (NamePtr: @sfnPMT; ParamInfo: fpiPMT; Proc: fnPMT; ResultKind: frkValue; Token: 59; TypeID: ftFinancial),
   (NamePtr: @sfnPPMT; ParamInfo: fpiPPMT; Proc: fnPPMT; ResultKind: frkValue; Token: 168; TypeID: ftFinancial),
   (NamePtr: @sfnPV; ParamInfo: fpiPV; Proc: fnPV; ResultKind: frkValue; Token: 56; TypeID: ftFinancial),
    // Information
   (NamePtr: @sfnIsBlank; ParamInfo: fpiIsBlank; Proc: fnIsBlank; ResultKind: frkValue; Token: 129; TypeID: ftInformation),
   (NamePtr: @sfnIsErr; ParamInfo: fpiIsErr; Proc: fnIsErr; ResultKind: frkValue; Token: 126; TypeID: ftInformation),
   (NamePtr: @sfnIsError; ParamInfo: fpiIsError; Proc: fnIsError; ResultKind: frkValue; Token: 3; TypeID: ftInformation),
   (NamePtr: @sfnIsEven; ParamInfo: fpiIsEven; Proc: fnIsEven; ResultKind: frkValue; Token: 255; TypeID: ftInformation),
   (NamePtr: @sfnIsLogical; ParamInfo: fpiIsLogical; Proc: fnIsLogical; ResultKind: frkValue; Token: 198; TypeID: ftInformation),
   (NamePtr: @sfnIsNA; ParamInfo: fpiIsNA; Proc: fnIsNA; ResultKind: frkValue; Token: 2; TypeID: ftInformation),
   (NamePtr: @sfnIsNonText; ParamInfo: fpiIsNonText; Proc: fnIsNonText; ResultKind: frkValue; Token: 190; TypeID: ftInformation),
   (NamePtr: @sfnIsNumber; ParamInfo: fpiIsNumber; Proc: fnIsNumber; ResultKind: frkValue; Token: 128; TypeID: ftInformation),
   (NamePtr: @sfnIsOdd; ParamInfo: fpiIsOdd; Proc: fnIsOdd; ResultKind: frkValue; Token: 255; TypeID: ftInformation),
   (NamePtr: @sfnIsText; ParamInfo: fpiIsText; Proc: fnIsText; ResultKind: frkValue; Token: 127; TypeID: ftInformation),
   (NamePtr: @sfnN; ParamInfo: fpiN; Proc: fnN; ResultKind: frkValue; Token: 131; TypeID: ftInformation),
   (NamePtr: @sfnNA; ParamInfo: fpiNA; Proc: fnNA; ResultKind: frkValue; Token: 10; TypeID: ftInformation),
    // Lookup and reference
   (NamePtr: @sfnAddress; ParamInfo: fpiAddress; Proc: fnAddress; ResultKind: frkValue; Token: 219; TypeID: ftLookupAndReference),
   (NamePtr: @sfnColumn; ParamInfo: fpiColumn; Proc: fnColumn; ResultKind: frkValue; Token: 9; TypeID: ftLookupAndReference),
   (NamePtr: @sfnColumns; ParamInfo: fpiColumns; Proc: fnColumns; ResultKind: frkValue; Token: 77; TypeID: ftLookupAndReference),
   (NamePtr: @sfnFormulaText; ParamInfo: fpiFormulaText; Proc: fnFormulaText; ResultKind: frkValue; Token: 255; TypeID: ftLookupAndReference),
   (NamePtr: @sfnHLookup; ParamInfo: fpiHLookup; Proc: fnHLookup; ResultKind: frkValue; Token: 101; TypeID: ftLookupAndReference),
   (NamePtr: @sfnLookup; ParamInfo: fpiLookup; Proc: fnLookup; ResultKind: frkValue; Token: 28; TypeID: ftLookupAndReference),
   (NamePtr: @sfnMatch; ParamInfo: fpiMatch; Proc: fnMatch; ResultKind: frkValue; Token: 64; TypeID: ftLookupAndReference),
   (NamePtr: @sfnRow; ParamInfo: fpiRow; Proc: fnRow; ResultKind: frkValue; Token: 8; TypeID: ftLookupAndReference),
   (NamePtr: @sfnRows; ParamInfo: fpiRows; Proc: fnRows; ResultKind: frkValue; Token: 76; TypeID: ftLookupAndReference),
   (NamePtr: @sfnTranspose; ParamInfo: fpiTranspose; Proc: fnTranspose; ResultKind: frkArray; Token: 83; TypeID: ftLookupAndReference),
   (NamePtr: @sfnVLookup; ParamInfo: fpiVLookup; Proc: fnVLookup; ResultKind: frkValue; Token: 102; TypeID: ftLookupAndReference),
    // Logical
   (NamePtr: @sfnAnd; ParamInfo: fpiAND; Proc: fnAND; ResultKind: frkValue; Token: 36; TypeID: ftLogical),
   (NamePtr: @sfnFalse; ParamInfo: fpiFalse; Proc: fnFalse; ResultKind: frkValue; Token: 35; TypeID: ftLogical),
   (NamePtr: @sfnIF; ParamInfo: fpiIF; Proc: fnIF; ResultKind: frkValue; Token: 1; TypeID: ftLogical),
   (NamePtr: @sfnIfError; ParamInfo: fpiIfError; Proc: fnIfError; ResultKind: frkValue; Token: 255; TypeID: ftLogical),
   (NamePtr: @sfnIfNA; ParamInfo: fpiIfNA; Proc: fnIfNA; ResultKind: frkValue; Token: 255; TypeID: ftLogical),
   (NamePtr: @sfnNot; ParamInfo: fpiNOT; Proc: fnNOT; ResultKind: frkValue; Token: 38; TypeID: ftLogical),
   (NamePtr: @sfnOr; ParamInfo: fpiOR; Proc: fnOR; ResultKind: frkValue; Token: 37; TypeID: ftLogical),
   (NamePtr: @sfnTrue; ParamInfo: fpiTrue; Proc: fnTrue; ResultKind: frkValue; Token: 34; TypeID: ftLogical),
   (NamePtr: @sfnXor; ParamInfo: fpiXOR; Proc: fnXOR; ResultKind: frkValue; Token: 255; TypeID: ftLogical),
    // Math and trigonometry
   (NamePtr: @sfnAbs; ParamInfo: fpiABS; Proc: fnABS; ResultKind: frkValue; Token: 24; TypeID: ftMath),
   (NamePtr: @sfnAcos; ParamInfo: fpiACOS; Proc: fnACOS; ResultKind: frkValue; Token: 99; TypeID: ftMath),
   (NamePtr: @sfnAcosh; ParamInfo: fpiACOSH; Proc: fnACOSH; ResultKind: frkValue; Token: 233; TypeID: ftMath),
   (NamePtr: @sfnAcot; ParamInfo: fpiACOT; Proc: fnACOT; ResultKind: frkValue; Token: 255; TypeID: ftMath),
   (NamePtr: @sfnAcoth; ParamInfo: fpiACOTH; Proc: fnACOTH; ResultKind: frkValue; Token: 255; TypeID: ftMath),
   (NamePtr: @sfnAsin; ParamInfo: fpiASIN; Proc: fnASIN; ResultKind: frkValue; Token: 98; TypeID: ftMath),
   (NamePtr: @sfnAsinh; ParamInfo: fpiASINH; Proc: fnASINH; ResultKind: frkValue; Token: 232; TypeID: ftMath),
   (NamePtr: @sfnAtan; ParamInfo: fpiATAN; Proc: fnATAN; ResultKind: frkValue; Token: 18; TypeID: ftMath),
   (NamePtr: @sfnAtan2; ParamInfo: fpiATAN2; Proc: fnATAN2; ResultKind: frkValue; Token: 97; TypeID: ftMath),
   (NamePtr: @sfnAtanh; ParamInfo: fpiATANH; Proc: fnATANH; ResultKind: frkValue; Token: 234; TypeID: ftMath),
   (NamePtr: @sfnBase; ParamInfo: fpiBase; Proc: fnBase; ResultKind: frkValue; Token: 255; TypeID: ftMath),
   (NamePtr: @sfnCeiling; ParamInfo: fpiCeiling; Proc: fnCeiling; ResultKind: frkValue; Token: 288; TypeID: ftMath),
   (NamePtr: @sfnCeiling_Math; ParamInfo: fpiCeiling_Math; Proc: fnCeiling_Math; ResultKind: frkValue; Token: 255; TypeID: ftMath),
   (NamePtr: @sfnCeiling_Precise; ParamInfo: fpiCeiling_Precise; Proc: fnCeiling_Precise; ResultKind: frkValue; Token: 255; TypeID: ftMath),
   (NamePtr: @sfnCos; ParamInfo: fpiCOS; Proc: fnCOS; ResultKind: frkValue; Token: 16; TypeID: ftMath),
   (NamePtr: @sfnCosh; ParamInfo: fpiCOSH; Proc: fnCOSH; ResultKind: frkValue; Token: 230; TypeID: ftMath),
   (NamePtr: @sfnCot; ParamInfo: fpiCOT; Proc: fnCOT; ResultKind: frkValue; Token: 255; TypeID: ftMath),
   (NamePtr: @sfnCoth; ParamInfo: fpiCOTH; Proc: fnCOTH; ResultKind: frkValue; Token: 255; TypeID: ftMath),
   (NamePtr: @sfnCombin; ParamInfo: fpiCombin; Proc: fnCombin; ResultKind: frkValue; Token: 276; TypeID: ftMath),
   (NamePtr: @sfnCombinA; ParamInfo: fpiCombinA; Proc: fnCombinA; ResultKind: frkValue; Token: 255; TypeID: ftMath),
   (NamePtr: @sfnCsc; ParamInfo: fpiCSC; Proc: fnCSC; ResultKind: frkValue; Token: 255; TypeID: ftMath),
   (NamePtr: @sfnCsch; ParamInfo: fpiCSCH; Proc: fnCSCH; ResultKind: frkValue; Token: 255; TypeID: ftMath),
   (NamePtr: @sfnDecimal; ParamInfo: fpiDecimal; Proc: fnDecimal; ResultKind: frkValue; Token: 255; TypeID: ftMath),
   (NamePtr: @sfnDegrees; ParamInfo: fpiDegrees; Proc: fnDegrees; ResultKind: frkValue; Token: 343; TypeID: ftMath),
   (NamePtr: @sfnExp; ParamInfo: fpiExp; Proc: fnExp; ResultKind: frkValue; Token: 21; TypeID: ftMath),
   (NamePtr: @sfnEven; ParamInfo: fpiEven; Proc: fnEven; ResultKind: frkValue; Token: 279; TypeID: ftMath),
   (NamePtr: @sfnFact; ParamInfo: fpiFact; Proc: fnFact; ResultKind: frkValue; Token: 184; TypeID: ftMath),
   (NamePtr: @sfnFactDouble; ParamInfo: fpiFactDouble; Proc: fnFactDouble; ResultKind: frkValue; Token: 255; TypeID: ftMath),
   (NamePtr: @sfnFloor; ParamInfo: fpiFloor; Proc: fnFloor; ResultKind: frkValue; Token: 285; TypeID: ftMath),
   (NamePtr: @sfnFloor_Math; ParamInfo: fpiFloor_Math; Proc: fnFloor_Math; ResultKind: frkValue; Token: 255; TypeID: ftMath),
   (NamePtr: @sfnFloor_Precise; ParamInfo: fpiFloor_Precise; Proc: fnFloor_Precise; ResultKind: frkValue; Token: 255; TypeID: ftMath),
   (NamePtr: @sfnInt; ParamInfo: fpiINT; Proc: fnINT; ResultKind: frkValue; Token: 25; TypeID: ftMath),
   (NamePtr: @sfnIso_Ceiling; ParamInfo: fpiIso_Ceiling; Proc: fnIso_Ceiling; ResultKind: frkValue; Token: 255; TypeID: ftMath),
   (NamePtr: @sfnLn; ParamInfo: fpiLN; Proc: fnLN; ResultKind: frkValue; Token: 22; TypeID: ftMath),
   (NamePtr: @sfnLog; ParamInfo: fpiLOG; Proc: fnLOG; ResultKind: frkValue; Token: 109; TypeID: ftMath),
   (NamePtr: @sfnLog10; ParamInfo: fpiLOG10; Proc: fnLOG10; ResultKind: frkValue; Token: 23; TypeID: ftMath),
   (NamePtr: @sfnMMult; ParamInfo: fpiMMult; Proc: fnMMult; ResultKind: frkArray; Token: 165; TypeID: ftMath),
   (NamePtr: @sfnMod; ParamInfo: fpiMOD; Proc: fnMOD; ResultKind: frkValue; Token: 39; TypeID: ftMath),
   (NamePtr: @sfnMRound; ParamInfo: fpiMRound; Proc: fnMRound; ResultKind: frkValue; Token: 255; TypeID: ftMath),
   (NamePtr: @sfnOdd; ParamInfo: fpiOdd; Proc: fnOdd; ResultKind: frkValue; Token: 298; TypeID: ftMath),
   (NamePtr: @sfnPi; ParamInfo: fpiPI; Proc: fnPI; ResultKind: frkValue; Token: 19; TypeID: ftMath),
   (NamePtr: @sfnPower; ParamInfo: fpiPower; Proc: fnPower; ResultKind: frkValue; Token: 337; TypeID: ftMath),
   (NamePtr: @sfnProduct; ParamInfo: fpiProduct; Proc: fnProduct; ResultKind: frkNonArrayValue; Token: 183; TypeID: ftMath),
   (NamePtr: @sfnQuotient; ParamInfo: fpiQuotient; Proc: fnQuotient; ResultKind: frkValue; Token: 255; TypeID: ftMath),
   (NamePtr: @sfnRadians; ParamInfo: fpiRadians; Proc: fnRadians; ResultKind: frkValue; Token: 342; TypeID: ftMath),
   (NamePtr: @sfnRand; ParamInfo: fpiRand; Proc: fnRand; ResultKind: frkValue; Token: 63; TypeID: ftMath),
   (NamePtr: @sfnRandBetween; ParamInfo: fpiRandBetween; Proc: fnRandBetween; ResultKind: frkValue; Token: 255; TypeID: ftMath),
   (NamePtr: @sfnRound; ParamInfo: fpiRound; Proc: fnRound; ResultKind: frkValue; Token: 27; TypeID: ftMath),
   (NamePtr: @sfnRoundDown; ParamInfo: fpiRoundDown; Proc: fnRoundDown; ResultKind: frkValue; Token: 213; TypeID: ftMath),
   (NamePtr: @sfnRoundUp; ParamInfo: fpiRoundUp; Proc: fnRoundUp; ResultKind: frkValue; Token: 212; TypeID: ftMath),
   (NamePtr: @sfnSec; ParamInfo: fpiSec; Proc: fnSec; ResultKind: frkValue; Token: 255; TypeID: ftMath),
   (NamePtr: @sfnSech; ParamInfo: fpiSech; Proc: fnSech; ResultKind: frkValue; Token: 255; TypeID: ftMath),
   (NamePtr: @sfnSign; ParamInfo: fpiSign; Proc: fnSign; ResultKind: frkValue; Token: 26; TypeID: ftMath),
   (NamePtr: @sfnSin; ParamInfo: fpiSin; Proc: fnSin; ResultKind: frkValue; Token: 15; TypeID: ftMath),
   (NamePtr: @sfnSinh; ParamInfo: fpiSinH; Proc: fnSinH; ResultKind: frkValue; Token: 229; TypeID: ftMath),
   (NamePtr: @sfnSqrt; ParamInfo: fpiSQRT; Proc: fnSQRT; ResultKind: frkValue; Token: 20; TypeID: ftMath),
   (NamePtr: @sfnSqrtPi; ParamInfo: fpiSQRTPI; Proc: fnSQRTPI; ResultKind: frkValue; Token: 255; TypeID: ftMath),
   (NamePtr: @sfnSum; ParamInfo: fpiSum; Proc: fnSum; ResultKind: frkNonArrayValue; Token: 4; TypeID: ftMath),
   (NamePtr: @sfnSumIF; ParamInfo: fpiSumIF; Proc: fnSumIF; ResultKind: frkNonArrayValue; Token: 345; TypeID: ftMath),
   (NamePtr: @sfnSumSQ; ParamInfo: fpiSumSQ; Proc: fnSumSQ; ResultKind: frkNonArrayValue; Token: 321; TypeID: ftMath),
   (NamePtr: @sfnTan; ParamInfo: fpiTan; Proc: fnTan;  ResultKind: frkValue; Token: 17; TypeID: ftMath),
   (NamePtr: @sfnTanh; ParamInfo: fpiTanH; Proc: fnTanH; ResultKind: frkValue; Token: 231; TypeID: ftMath),
   (NamePtr: @sfnTrunc; ParamInfo: fpiTrunc; Proc: fnTrunc; ResultKind: frkValue; Token: 197; TypeID: ftMath),
   // Statistical
   (NamePtr: @sfnAveDev; ParamInfo: fpiAveDev; Proc: fnAveDev; ResultKind: frkNonArrayValue; Token: 269; TypeID: ftStatistical),
   (NamePtr: @sfnAverage; ParamInfo: fpiAverage; Proc: fnAverage; ResultKind: frkNonArrayValue; Token: 5; TypeID: ftStatistical),
   (NamePtr: @sfnAverageA; ParamInfo: fpiAverageA; Proc: fnAverageA; ResultKind: frkNonArrayValue; Token: 361; TypeID: ftStatistical),
   (NamePtr: @sfnAverageIF; ParamInfo: fpiAverageIF; Proc: fnAverageIF; ResultKind: frkNonArrayValue; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnBeta_Dist; ParamInfo: fpiBeta_Dist; Proc: fnBeta_Dist; ResultKind: frkValue; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnBeta_Inv; ParamInfo: fpiBeta_Inv; Proc: fnBeta_Inv; ResultKind: frkValue; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnBinom_Dist; ParamInfo: fpiBinom_Dist; Proc: fnBinom_Dist; ResultKind: frkValue; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnBinom_Dist_Range; ParamInfo: fpiBinom_Dist_Range; Proc: fnBinom_Dist_Range; ResultKind: frkValue; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnChiSQ_Dist; ParamInfo: fpiChiSQ_Dist; Proc: fnChiSQ_Dist; ResultKind: frkValue; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnChiSQ_Dist_RT; ParamInfo: fpiChiSQ_Dist_RT; Proc: fnChiSQ_Dist_RT; ResultKind: frkValue; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnChiSQ_Inv; ParamInfo: fpiChiSQ_Inv; Proc: fnChiSQ_Inv; ResultKind: frkValue; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnChiSQ_Inv_RT; ParamInfo: fpiChiSQ_Inv_RT; Proc: fnChiSQ_Inv_RT; ResultKind: frkValue; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnCorrel; ParamInfo: fpiCorrel; Proc: fnCorrel; ResultKind: frkNonArrayValue; Token: 307; TypeID: ftStatistical),
   (NamePtr: @sfnCovariance_P; ParamInfo: fpiCovariance_P; Proc: fnCovariance_P; ResultKind: frkNonArrayValue; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnCovariance_S; ParamInfo: fpiCovariance_S; Proc: fnCovariance_S; ResultKind: frkNonArrayValue; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnCount; ParamInfo: fpiCount; Proc: fnCount; ResultKind: frkNonArrayValue; Token: 0; TypeID: ftStatistical),
   (NamePtr: @sfnCountA; ParamInfo: fpiCountA; Proc: fnCountA; ResultKind: frkNonArrayValue; Token: 169; TypeID: ftStatistical),
   (NamePtr: @sfnCountBlank; ParamInfo: fpiCountBlank; Proc: fnCountBlank; ResultKind: frkNonArrayValue; Token: 347; TypeID: ftStatistical),
   (NamePtr: @sfnCountIF; ParamInfo: fpiCountIF; Proc: fnCountIF; ResultKind: frkNonArrayValue; Token: 346; TypeID: ftStatistical),
   (NamePtr: @sfnDevSQ; ParamInfo: fpiDevSQ; Proc: fnDevSQ; ResultKind: frkNonArrayValue; Token: 318; TypeID: ftStatistical),
   (NamePtr: @sfnExpon_Dist; ParamInfo: fpiExpon_Dist; Proc: fnExpon_Dist; ResultKind: frkValue; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnForecast; ParamInfo: fpiForecast; Proc: fnForecast; ResultKind: frkValue; Token: 309; TypeID: ftStatistical),
   (NamePtr: @sfnGamma; ParamInfo: fpiGamma; Proc: fnGamma; ResultKind: frkValue; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnGamma_Dist; ParamInfo: fpiGamma_Dist; Proc: fnGamma_Dist; ResultKind: frkValue; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnGamma_Inv; ParamInfo: fpiGamma_Inv; Proc: fnGamma_Inv; ResultKind: frkValue; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnGammaLn; ParamInfo: fpiGammaLn; Proc: fnGammaLn; ResultKind: frkValue; Token: 271; TypeID: ftStatistical),
   (NamePtr: @sfnGammaLn_Precise; ParamInfo: fpiGammaLn_Precise; Proc: fnGammaLn_Precise; ResultKind: frkValue; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnGauss; ParamInfo: fpiGauss; Proc: fnGauss; ResultKind: frkValue; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnGeomean; ParamInfo: fpiGeomean; Proc: fnGeomean; ResultKind: frkNonArrayValue; Token: 319; TypeID: ftStatistical),
   (NamePtr: @sfnHypgeom_Dist; ParamInfo: fpiHypgeom_Dist; Proc: fnHypgeom_Dist; ResultKind: frkValue; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnIntercept; ParamInfo: fpiIntercept; Proc: fnIntercept; ResultKind: frkNonArrayValue; Token: 311; TypeID: ftStatistical),
   (NamePtr: @sfnLarge; ParamInfo: fpiLarge; Proc: fnLarge; ResultKind: frkValue; Token: 325; TypeID: ftStatistical),
   (NamePtr: @sfnMax; ParamInfo: fpiMax; Proc: fnMax; ResultKind: frkNonArrayValue; Token: 7; TypeID: ftStatistical),
   (NamePtr: @sfnMaxA; ParamInfo: fpiMaxA; Proc: fnMaxA; ResultKind: frkNonArrayValue; Token: 362; TypeID: ftStatistical),
   (NamePtr: @sfnMedian; ParamInfo: fpiMedian; Proc: fnMedian; ResultKind: frkNonArrayValue; Token: 227; TypeID: ftStatistical),
   (NamePtr: @sfnMin; ParamInfo: fpiMin; Proc: fnMin; ResultKind: frkNonArrayValue; Token: 6; TypeID: ftStatistical),
   (NamePtr: @sfnMinA; ParamInfo: fpiMinA; Proc: fnMinA; ResultKind: frkNonArrayValue; Token: 363; TypeID: ftStatistical),
   (NamePtr: @sfnNorm_Dist; ParamInfo: fpiNorm_Dist; Proc: fnNorm_Dist; ResultKind: frkValue; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnNorm_Inv; ParamInfo: fpiNorm_Inv; Proc: fnNorm_Inv; ResultKind: frkValue; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnNorm_S_Dist; ParamInfo: fpiNorm_S_Dist; Proc: fnNorm_S_Dist; ResultKind: frkValue; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnNorm_S_Inv; ParamInfo: fpiNorm_S_Inv; Proc: fnNorm_S_Inv; ResultKind: frkValue; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnPearson; ParamInfo: fpiPearson; Proc: fnPearson; ResultKind: frkNonArrayValue; Token: 312; TypeID: ftStatistical),
   (NamePtr: @sfnPercentile_Exc; ParamInfo: fpiPercentile_Exc; Proc: fnPercentile_Exc; ResultKind: frkValue; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnPercentile_Inc; ParamInfo: fpiPercentile_Inc; Proc: fnPercentile_Inc; ResultKind: frkValue; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnPermut; ParamInfo: fpiPermut; Proc: fnPermut; ResultKind: frkValue; Token: 299; TypeID: ftStatistical),
   (NamePtr: @sfnPoisson_Dist; ParamInfo: fpiPoisson_Dist; Proc: fnPoisson_Dist; ResultKind: frkValue; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnQuartile_Exc; ParamInfo: fpiQuartile_Exc; Proc: fnQuartile_Exc; ResultKind: frkValue; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnQuartile_Inc; ParamInfo: fpiQuartile_Inc; Proc: fnQuartile_Inc; ResultKind: frkValue; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnRank_Avg; ParamInfo: fpiRank_Avg; Proc: fnRank_Avg; ResultKind: frkValue; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnRank_Eq; ParamInfo: fpiRank_Eq; Proc: fnRank_Eq; ResultKind: frkValue; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnRSQ; ParamInfo: fpiRSQ; Proc: fnRSQ; ResultKind: frkNonArrayValue; Token: 313; TypeID: ftStatistical),
   (NamePtr: @sfnSkew; ParamInfo: fpiSkew; Proc: fnSkew; ResultKind: frkNonArrayValue; Token: 323; TypeID: ftStatistical),
   (NamePtr: @sfnSkew_P; ParamInfo: fpiSkew_P; Proc: fnSkew_P; ResultKind: frkNonArrayValue; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnSlope; ParamInfo: fpiSlope; Proc: fnSlope; ResultKind: frkNonArrayValue; Token: 315; TypeID: ftStatistical),
   (NamePtr: @sfnSmall; ParamInfo: fpiSmall; Proc: fnSmall; ResultKind: frkValue; Token: 326; TypeID: ftStatistical),
   (NamePtr: @sfnStandardize; ParamInfo: fpiStandardize; Proc: fnStandardize; ResultKind: frkValue; Token: 297; TypeID: ftStatistical),
   (NamePtr: @sfnStDev_S; ParamInfo: fpiStDev_S; Proc: fnStDev_S; ResultKind: frkNonArrayValue; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnStDev_P; ParamInfo: fpiStDev_P; Proc: fnStDev_P; ResultKind: frkNonArrayValue; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnStDevA; ParamInfo: fpiStDevA; Proc: fnStDevA; ResultKind: frkNonArrayValue; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnStDevPA; ParamInfo: fpiStDevPA; Proc: fnStDevPA; ResultKind: frkNonArrayValue; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnSTEYX; ParamInfo: fpiSTEYX; Proc: fnSTEYX; ResultKind: frkNonArrayValue; Token: 314; TypeID: ftStatistical),
   (NamePtr: @sfnSubTotal; ParamInfo: fpiSubTotal; Proc: fnSubTotal; ResultKind: frkValue; Token: 344; TypeID: ftStatistical),
   (NamePtr: @sfnT_Dist; ParamInfo: fpiT_Dist; Proc: fnT_Dist; ResultKind: frkValue; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnT_Dist_2T; ParamInfo: fpiT_Dist_2T; Proc: fnT_Dist_2T; ResultKind: frkValue; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnT_Dist_RT; ParamInfo: fpiT_Dist_RT; Proc: fnT_Dist_RT; ResultKind: frkValue; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnT_Inv; ParamInfo: fpiT_Inv; Proc: fnT_Inv; ResultKind: frkValue; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnT_Inv_2T; ParamInfo: fpiT_Inv_2T; Proc: fnT_Inv_2T; ResultKind: frkValue; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnVar_P; ParamInfo: fpiVar_P; Proc: fnVar_P; ResultKind: frkNonArrayValue; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnVar_S; ParamInfo: fpiVar_S; Proc: fnVar_S; ResultKind: frkNonArrayValue; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnVarA; ParamInfo: fpiVarA; Proc: fnVarA; ResultKind: frkNonArrayValue; Token: 367; TypeID: ftStatistical),
   (NamePtr: @sfnVarPA; ParamInfo: fpiVarPA; Proc: fnVarPA; ResultKind: frkNonArrayValue; Token: 365; TypeID: ftStatistical),
   (NamePtr: @sfnWeibull_Dist; ParamInfo: fpiWeibull_Dist; Proc: fnWeibull_Dist; ResultKind: frkValue; Token: 255; TypeID: ftStatistical),
   // Text
   (NamePtr: @sfnChar; ParamInfo: fpiChar; Proc: fnChar; ResultKind: frkValue; Token: 111; TypeID: ftText),
   (NamePtr: @sfnClean; ParamInfo: fpiClean; Proc: fnClean; ResultKind: frkValue; Token: 162; TypeID: ftText),
   (NamePtr: @sfnCode; ParamInfo: fpiCode; Proc: fnCode; ResultKind: frkValue; Token: 121; TypeID: ftText),
   (NamePtr: @sfnConcatenate; ParamInfo: fpiConcatenate; Proc: fnConcatenate; ResultKind: frkNonArrayValue; Token: 336; TypeID: ftText),
   (NamePtr: @sfnDollar; ParamInfo: fpiDollar; Proc: fnDollar; ResultKind: frkValue; Token: 13; TypeID: ftText),
   (NamePtr: @sfnExact; ParamInfo: fpiExact; Proc: fnExact; ResultKind: frkValue; Token: 117; TypeID: ftText),
   (NamePtr: @sfnFind; ParamInfo: fpiFind; Proc: fnFind; ResultKind: frkValue; Token: 124; TypeID: ftText),
   (NamePtr: @sfnFixed; ParamInfo: fpiFixed; Proc: fnFixed; ResultKind: frkValue; Token: 14; TypeID: ftText),
   (NamePtr: @sfnLeft; ParamInfo: fpiLeft; Proc: fnLeft; ResultKind: frkValue; Token: 115; TypeID: ftText),
   (NamePtr: @sfnLen; ParamInfo: fpiLen; Proc: fnLen; ResultKind: frkValue; Token: 32; TypeID: ftText),
   (NamePtr: @sfnLower; ParamInfo: fpiLower; Proc: fnLower; ResultKind: frkValue; Token: 112; TypeID: ftText),
   (NamePtr: @sfnMid; ParamInfo: fpiMid; Proc: fnMid; ResultKind: frkValue; Token: 31; TypeID: ftText),
   (NamePtr: @sfnProper; ParamInfo: fpiProper; Proc: fnProper; ResultKind: frkValue; Token: 114; TypeID: ftText),
   (NamePtr: @sfnReplace; ParamInfo: fpiReplace; Proc: fnReplace; ResultKind: frkValue; Token: 119; TypeID: ftText),
   (NamePtr: @sfnRept; ParamInfo: fpiRept; Proc: fnRept; ResultKind: frkValue; Token: 30; TypeID: ftText),
   (NamePtr: @sfnRight; ParamInfo: fpiRight; Proc: fnRight; ResultKind: frkValue; Token: 116; TypeID: ftText),
   (NamePtr: @sfnSearch; ParamInfo: fpiSearch; Proc: fnSearch; ResultKind: frkValue; Token: 82; TypeID: ftText),
   (NamePtr: @sfnSubstitute; ParamInfo: fpiSubstitute; Proc: fnSubstitute; ResultKind: frkValue; Token: 120; TypeID: ftText),
   (NamePtr: @sfnT; ParamInfo: fpiT; Proc: fnT; ResultKind: frkValue; Token: 130; TypeID: ftText),
   (NamePtr: @sfnText; ParamInfo: fpiText; Proc: fnText; ResultKind: frkValue; Token: 48; TypeID: ftText),
   (NamePtr: @sfnTrim; ParamInfo: fpiTrim; Proc: fnTrim; ResultKind: frkValue; Token: 118; TypeID: ftText),
   (NamePtr: @sfnUpper; ParamInfo: fpiUpper; Proc: fnUpper; ResultKind: frkValue; Token: 113; TypeID: ftText),
   (NamePtr: @sfnValue; ParamInfo: fpiValue; Proc: fnValue; ResultKind: frkValue; Token: 33; TypeID: ftText)
   );

function dxSpreadSheetFunctionsRepository: TdxSpreadSheetFunctionsRepository;
begin
  if Repository = nil then
    Repository := TdxSpreadSheetFunctionsRepository.Create();
  Result := Repository;
end;

function dxCompareFunctions(AInfo1, AInfo2: TdxSpreadSheetFunctionInfo): Integer;
begin
  if AInfo1 = AInfo2 then
    Result := 0
  else
    Result := dxSpreadSheetCompareText(AInfo1.Name, AInfo2.Name)
end;

{ TdxSpreadSheetFunctionsRepository }

procedure TdxSpreadSheetFunctionsRepository.Add(const AName: TcxResourceStringID; AProc: TdxSpreadSheetFunction;
  AParamInfo: TdxSpreadSheetFunctionParamInfo; AResultKind: TdxSpreadSheetFunctionResultKind;
  AToken: Word; AType: TdxSpreadSheetFunctionType = ftCommon);
var
  AInfo: TdxSpreadSheetFunctionInfo;
begin
  AInfo := TdxSpreadSheetFunctionInfo.Create;
  AInfo.NamePtr := AName;
  AInfo.Proc := AProc;
  AInfo.ParamInfo := AParamInfo;
  AInfo.ResultKind := AResultKind;
  AInfo.Token := AToken;
  AInfo.TypeID := AType;
  AInfo.UpdateInfo(nil);
  inherited Add(AInfo);
  Sorted := False;
end;

function TdxSpreadSheetFunctionsRepository.AddUnknown(const AName: TdxUnicodeString;
  AType: TdxSpreadSheetFunctionType = ftCommon): TdxSpreadSheetFunctionInfo;
begin
  Result := TdxSpreadSheetFunctionInfo.Create;
  Result.NamePtr := nil;
  Result.Name := AName;
  Result.Proc := nil;
  Result.ParamInfo := nil;
  inherited Add(Result);
  Sorted := False;
end;

function TdxSpreadSheetFunctionsRepository.GetInfoByID(const ID: Integer): TdxSpreadSheetFunctionInfo;
var
  I: Integer;
begin
  Result := nil;
  for I := 0 to Count - 1 do
    if Items[I].Token = ID then
    begin
      Result := Items[I];
      Break;
    end;
end;

function TdxSpreadSheetFunctionsRepository.GetInfoByName(const AName: TdxUnicodeString): TdxSpreadSheetFunctionInfo;
begin
  Result := GetInfoByName(@AName[1], Length(AName));
end;

function TdxSpreadSheetFunctionsRepository.GetInfoByName(
  const AName: PdxUnicodeChar; ALength: Integer): TdxSpreadSheetFunctionInfo;
var
  L, H, I, C: TdxNativeInt;
begin
  Sorted := True;
  Result := nil;
  L := 0;
  H := Count - 1;
  while L <= H do
  begin
    I := (L + H) shr 1;
    C := dxSpreadSheetCompareText(TdxSpreadSheetFunctionInfo(List[I]).Name, AName, ALength);
    if C < 0 then
      L := I + 1
    else
    begin
      H := I - 1;
      if C = 0 then
      begin
        Result := TdxSpreadSheetFunctionInfo(List[I]);
        Break;
      end;
    end;
  end;
end;

procedure TdxSpreadSheetFunctionsRepository.TranslationChanged;
var
  I: Integer; 
begin
  Sorted := False;
  for I := 0 to Count - 1 do
    Items[I].UpdateInfo(AFormatSettings);
  Sorted := True;
end;

function TdxSpreadSheetFunctionsRepository.GetItem(
  AIndex: Integer): TdxSpreadSheetFunctionInfo;
begin
  Result := TdxSpreadSheetFunctionInfo(inherited Items[AIndex]);
end;

procedure TdxSpreadSheetFunctionsRepository.SetSorted(AValue: Boolean);
begin
  if FSorted <> AValue then
  begin
    FSorted := AValue;
    if Sorted then
      Sort(@dxCompareFunctions);
  end;
end;

procedure RegisterFunctions;
var
  I: Integer;
begin
  for I := Low(PredefinedFunctions) to High(PredefinedFunctions) do
    dxSpreadSheetFunctionsRepository.Add(PredefinedFunctions[I].NamePtr,
      PredefinedFunctions[I].Proc, PredefinedFunctions[I].ParamInfo, PredefinedFunctions[I].ResultKind,
      PredefinedFunctions[I].Token, PredefinedFunctions[I].TypeID);
end;

procedure UnregisterFunctions;
begin
  FreeAndNil(Repository);
end;

initialization
  RegisterFunctions;

finalization
  UnregisterFunctions;

end.
