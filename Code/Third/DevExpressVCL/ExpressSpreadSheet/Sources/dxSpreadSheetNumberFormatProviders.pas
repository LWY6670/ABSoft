{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressSpreadSheet                                       }
{                                                                    }
{           Copyright (c) 2001-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSSPREADSHEET CONTROL AND ALL    }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY. }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxSpreadSheetNumberFormatProviders;

{$I cxVer.inc}

interface

uses
{$IFDEF DELPHI16}
  System.UITypes,
{$ENDIF}
  Types, Graphics, dxCoreClasses, dxCore, Classes, SysUtils, cxClasses,
  dxSpreadSheetClasses, dxSpreadSheetTypes, dxSpreadSheetNumberFormat, Contnrs;

type
  TdxSpreadSheetEmptyNumberFormatProvider = class;

  { TdxSpreadSheetCustomNumberFormatWrapper }

  TdxSpreadSheetCustomNumberFormatWrapper = class(TdxSpreadSheetCustomNumberFormatProvider)
  private
    FSource: TdxSpreadSheetCustomNumberFormatProvider;
  protected
    function GetHasMilliseconds: Boolean; override;
    function GetIs12HourTime: Boolean; override;
    function GetIsDateTime: Boolean; override;
    function GetIsFraction: Boolean; override;
    function GetIsNumeric: Boolean; override;
    function GetIsNumericNoFraction: Boolean; override;
    function GetIsText: Boolean; override;
    procedure SetHasMilliseconds(const AValue: Boolean); override;
    procedure SetIs12HourTime(const AValue: Boolean); override;
    procedure SetIsDateTime(const AValue: Boolean); override;
    procedure SetIsFraction(const AValue: Boolean); override;
    procedure SetIsNumeric(const AValue: Boolean); override;
    procedure SetIsNumericNoFraction(const AValue: Boolean); override;
    procedure SetIsText(const AValue: Boolean); override;
  public
    constructor Create(ASource: TdxSpreadSheetCustomNumberFormatProvider);
    destructor Destroy; override;
    procedure Format(const AValue: Variant; const AValueInfo: TdxSpreadSheetNumberFormatValueInfo;
      var AResult: TdxSpreadSheetNumberFormatResult); override;
    //
    property Source: TdxSpreadSheetCustomNumberFormatProvider read FSource;
  end;

  { TdxSpreadSheetCustomDateTimeFormatProvider }

  TdxSpreadSheetCustomDateTimeFormatProvider = class(TdxSpreadSheetCustomNumberFormatProvider)
  protected
    procedure DoFormat(const AValue: Variant; const AValueInfo: TdxSpreadSheetNumberFormatValueInfo;
      var AResult: TdxSpreadSheetNumberFormatResult); virtual; abstract;
    function GetIs12HourTime: Boolean; override;
    function GetIsDateTime: Boolean; override;
  public
    procedure Format(const AValue: Variant; const AValueInfo: TdxSpreadSheetNumberFormatValueInfo;
      var AResult: TdxSpreadSheetNumberFormatResult); override;
  end;

  { TdxSpreadSheetCustomElapsedTimeFormatProvider }

  TdxSpreadSheetCustomElapsedTimeFormatProviderClass = class of TdxSpreadSheetCustomElapsedTimeFormatProvider;
  TdxSpreadSheetCustomElapsedTimeFormatProvider = class(TdxSpreadSheetCustomDateTimeFormatProvider)
  private
    FFormatString: TdxUnicodeString;
    FIs12HourTime: Boolean;
  protected
    function CalculateActualValue(const AValue: Extended): Cardinal; virtual; abstract;
    procedure DoFormat(const AValue: Variant; const AValueInfo: TdxSpreadSheetNumberFormatValueInfo;
      var AResult: TdxSpreadSheetNumberFormatResult); override;
    function GetIs12HourTime: Boolean; override;
    procedure SetIs12HourTime(const AValue: Boolean); override;
  public
    constructor Create(const AFormatString, AFormatCode: TdxUnicodeString);
  end;

  { TdxSpreadSheetCustomFractionFormatProvider }

  TdxSpreadSheetCustomFractionFormatProvider = class(TdxSpreadSheetCustomNumberFormatProvider)
  private
    FDividentFormatProvider: TdxSpreadSheetCustomNumberFormat;
    FDivisorFormatProvider: TdxSpreadSheetCustomNumberFormat;
    FFormatOneDividerValue: Boolean;
  protected
    function CalculateLeadingSpacesCount(const AValue: TdxUnicodeString): Integer;
    function CalculateRationalApproximation(AValue: Extended): TSize; virtual; abstract;
    function ConvertLeadingSpacesToTrailingSpaces(const AValue: TdxUnicodeString): TdxUnicodeString;
    function FormatFraction(const AValue: Extended; const AFormatSettings: TFormatSettings): TdxUnicodeString;
    function GetIsFraction: Boolean; override;
    function GetIsNumeric: Boolean; override;
    function ReplaceSpacesWithZero(const AValue: TdxUnicodeString): TdxUnicodeString;
  public
    constructor Create(const ADividentFormatCode, ADivisorFormatCode: TdxUnicodeString);
    destructor Destroy; override;
    procedure Format(const AValue: Variant; const AValueInfo: TdxSpreadSheetNumberFormatValueInfo;
      var AResult: TdxSpreadSheetNumberFormatResult); override;
    //
    property FormatOneDividerValue: Boolean read FFormatOneDividerValue write FFormatOneDividerValue;
  end;

  { TdxSpreadSheetCustomSystemDateTimeFormatProvider }

  TdxSpreadSheetCustomSystemDateTimeFormatProvider = class(TdxSpreadSheetCustomDateTimeFormatProvider)
  private
    FFormats: TStringList;
  protected
    procedure DoFormat(const AValue: Variant; const AValueInfo: TdxSpreadSheetNumberFormatValueInfo;
      var AResult: TdxSpreadSheetNumberFormatResult); override;
    function GetDateTimeFormatString(const AFormatSettings: TFormatSettings): TdxUnicodeString; virtual; abstract;
  public
    constructor Create; virtual;
    destructor Destroy; override;
  end;

  { TdxSpreadSheetColorNumberFormatProvider }

  TdxSpreadSheetColorNumberFormatProvider = class(TdxSpreadSheetCustomNumberFormatWrapper)
  private
    FColor: TColor;
  protected
    class procedure RemoveColorFormattingString(var AFormatCode: TdxUnicodeString;
      var ATargetColor: TColor; const AColorString: TdxUnicodeString; AColor: TColor);
    class procedure RemoveIndexedColorFormattingString(var AFormatCode: TdxUnicodeString; var ATargetColor: TColor);
  public
    constructor Create(ASource: TdxSpreadSheetCustomNumberFormatProvider; AColor: TColor);
    procedure Format(const AValue: Variant; const AValueInfo: TdxSpreadSheetNumberFormatValueInfo;
      var AResult: TdxSpreadSheetNumberFormatResult); override;
    class function Parse(var AFormatCode: TdxUnicodeString; out AColor: TColor): Boolean; virtual;
    //
    property Color: TColor read FColor;
  end;

  { TdxSpreadSheetConcatenateNumberFormatProvider }

  TdxSpreadSheetConcatenateNumberFormatProvider = class(TdxSpreadSheetCustomNumberFormatProvider)
  private
    FHasMilliseconds: Boolean;
    FIs12HourTime: Boolean;
    FIsDateTime: Boolean;
    FIsFraction: Boolean;
    FIsNumeric: Boolean;
    FIsNumericNoFraction: Boolean;
    FIsText: Boolean;
    FLCID: Cardinal;
    FLCIDFormatSettings: TFormatSettings;
    FProviders: TdxSpreadSheetCustomNumberFormatProviders;
    procedure SetLCID(AValue: Cardinal);
  protected
    procedure ReplaceTrailingNumberFormats;

    function GetHasMilliseconds: Boolean; override;
    function GetIs12HourTime: Boolean; override;
    function GetIsDateTime: Boolean; override;
    function GetIsFraction: Boolean; override;
    function GetIsNumeric: Boolean; override;
    function GetIsNumericNoFraction: Boolean; override;
    function GetIsText: Boolean; override;
    procedure SetHasMilliseconds(const AValue: Boolean); override;
    procedure SetIs12HourTime(const AValue: Boolean); override;
    procedure SetIsDateTime(const AValue: Boolean); override;
    procedure SetIsFraction(const AValue: Boolean); override;
    procedure SetIsNumeric(const AValue: Boolean); override;
    procedure SetIsNumericNoFraction(const AValue: Boolean); override;
    procedure SetIsText(const AValue: Boolean); override;
  public
    constructor Create; virtual;
    destructor Destroy; override;
    procedure AddProvider(AProvider: TdxSpreadSheetCustomNumberFormatProvider);
    procedure CalculateFormatInfo;
    procedure Format(const AValue: Variant; const AValueInfo: TdxSpreadSheetNumberFormatValueInfo;
      var AResult: TdxSpreadSheetNumberFormatResult); override;
    //
    property LCID: Cardinal read FLCID write SetLCID;
    property Providers: TdxSpreadSheetCustomNumberFormatProviders read FProviders;
  end;

  { TdxSpreadSheetConditionalNumberFormatProvider }

  TdxSpreadSheetConditionalNumberFormatProvider = class(TdxSpreadSheetCustomNumberFormatProvider)
  protected
    function GetActualFormatProvider(const AValue: Variant;
      AValueType: TdxSpreadSheetCellDataType): TdxSpreadSheetCustomNumberFormatProvider; virtual; abstract;
  public
    procedure Format(const AValue: Variant; const AValueInfo: TdxSpreadSheetNumberFormatValueInfo;
      var AResult: TdxSpreadSheetNumberFormatResult); overload; override;
  end;

  { TdxSpreadSheetCurrencyAndLanguageInfoFormatProvider }

  TdxSpreadSheetCurrencyAndLanguageInfoFormatProvider = class(TdxSpreadSheetCustomNumberFormatProvider)
  private
    FCurrencyString: TdxUnicodeString;
    FLanguageInfo: Cardinal;
  public
    constructor Create(const ACurrencyString: TdxUnicodeString; ALanguageInfo: Cardinal);
    procedure Format(const AValue: Variant; const AValueInfo: TdxSpreadSheetNumberFormatValueInfo;
      var AResult: TdxSpreadSheetNumberFormatResult); override;
    //
    property LanguageInfo: Cardinal read FLanguageInfo;
  end;

  { TdxSpreadSheetDayOfWeekFormatProvider }

  TdxSpreadSheetDayOfWeekFormatProvider = class(TdxSpreadSheetCustomDateTimeFormatProvider)
  private
    FFormatString: TdxUnicodeString;
  protected
    procedure DoFormat(const AValue: Variant; const AValueInfo: TdxSpreadSheetNumberFormatValueInfo;
      var AResult: TdxSpreadSheetNumberFormatResult); override;
  public
    constructor Create(const AFormatString: TdxUnicodeString);
  end;

  { TdxSpreadSheetDefaultConditionalNumberFormatProvider }

  TdxSpreadSheetDefaultConditionalNumberFormatProvider = class(TdxSpreadSheetConditionalNumberFormatProvider)
  private
    FEmptyNumberFormatProvider: TdxSpreadSheetEmptyNumberFormatProvider;
    FProviders: TdxSpreadSheetCustomNumberFormatProviders;
  protected
    function GetActualFormatProvider(const AValue: Variant;
      AValueType: TdxSpreadSheetCellDataType): TdxSpreadSheetCustomNumberFormatProvider; override;
  public
    constructor Create; virtual;
    destructor Destroy; override;
    procedure AddProvider(AProvider: TdxSpreadSheetCustomNumberFormatProvider);
    //
    property EmptyNumberFormatProvider: TdxSpreadSheetEmptyNumberFormatProvider read FEmptyNumberFormatProvider;
    property Providers: TdxSpreadSheetCustomNumberFormatProviders read FProviders;
  end;

  { TdxSpreadSheetElapsedHoursFormatProvider }

  TdxSpreadSheetElapsedHoursFormatProvider = class(TdxSpreadSheetCustomElapsedTimeFormatProvider)
  protected
    function CalculateActualValue(const AValue: Extended): Cardinal; override;
  end;

  { TdxSpreadSheetElapsedMinutesFormatProvider }

  TdxSpreadSheetElapsedMinutesFormatProvider = class(TdxSpreadSheetCustomElapsedTimeFormatProvider)
  protected
    function CalculateActualValue(const AValue: Extended): Cardinal; override;
  end;

  { TdxSpreadSheetElapsedSecondsFormatProvider }

  TdxSpreadSheetElapsedSecondsFormatProvider = class(TdxSpreadSheetCustomElapsedTimeFormatProvider)
  protected
    function CalculateActualValue(const AValue: Extended): Cardinal; override;
  end;

  { TdxSpreadSheetEmptyNumberFormatProvider }

  TdxSpreadSheetEmptyNumberFormatProvider = class(TdxSpreadSheetCustomNumberFormatProvider)
  public
    procedure Format(const AValue: Variant; const AValueInfo: TdxSpreadSheetNumberFormatValueInfo;
      var AResult: TdxSpreadSheetNumberFormatResult); override;
  end;

  { TdxSpreadSheetExplicitTextFormatProvider }

  TdxSpreadSheetExplicitTextFormatProvider = class(TdxSpreadSheetCustomNumberFormatProvider)
  private
    FText: TdxUnicodeString;
  public
    constructor Create(const AText: TdxUnicodeString);
    procedure Format(const AValue: Variant; const AValueInfo: TdxSpreadSheetNumberFormatValueInfo;
      var AResult: TdxSpreadSheetNumberFormatResult); override;
  end;

  { TdxSpreadSheetFakeNumberFormatProvider }

  TdxSpreadSheetFakeNumberFormatProvider = class(TdxSpreadSheetCustomNumberFormat)
  protected
    procedure Initialize; override;
  public
    procedure Format(const AValue: Variant; const AValueInfo: TdxSpreadSheetNumberFormatValueInfo;
      var AResult: TdxSpreadSheetNumberFormatResult); override;
  end;

  { TdxSpreadSheetFractionExplicitDivisorFormatProvider }

  TdxSpreadSheetFractionExplicitDivisorFormatProvider = class(TdxSpreadSheetCustomFractionFormatProvider)
  private
    FDivisor: Integer;
  protected
    function CalculateRationalApproximation(AValue: Extended): TSize; override;
  public
    constructor Create(const ADividentFormatCode, ADivisorFormatCode: TdxUnicodeString; ADivisor: Integer);
   end;

  { TdxSpreadSheetFractionImplicitDivisorFormatProvider }

  TdxSpreadSheetFractionImplicitDivisorFormatProvider = class(TdxSpreadSheetCustomFractionFormatProvider)
  private
    FMaxDivisor: Integer;
  protected
    function CalculateRationalApproximation(AValue: Extended): TSize; override;
  public
    constructor Create(const ADividentFormatCode, ADivisorFormatCode: TdxUnicodeString; AMaxDivisor: Integer);
  end;

  { TdxSpreadSheetFullMonthFormatProvider }

  TdxSpreadSheetFullMonthFormatProvider = class(TdxSpreadSheetCustomDateTimeFormatProvider)
  protected
    procedure DoFormat(const AValue: Variant; const AValueInfo: TdxSpreadSheetNumberFormatValueInfo;
      var AResult: TdxSpreadSheetNumberFormatResult); override;
  end;

  { TdxSpreadSheetFirstLetterOfMonthFormatProvider }

  TdxSpreadSheetFirstLetterOfMonthFormatProvider = class(TdxSpreadSheetFullMonthFormatProvider)
  protected
    procedure DoFormat(const AValue: Variant; const AValueInfo: TdxSpreadSheetNumberFormatValueInfo;
      var AResult: TdxSpreadSheetNumberFormatResult); override;
  end;

  { TdxSpreadSheetMillisecondsFormatProvider }

  TdxSpreadSheetMillisecondsFormatProvider = class(TdxSpreadSheetCustomDateTimeFormatProvider)
  private
    FFormatString: TdxUnicodeString;
  protected
    procedure DoFormat(const AValue: Variant; const AValueInfo: TdxSpreadSheetNumberFormatValueInfo;
      var AResult: TdxSpreadSheetNumberFormatResult); override;
  public
    constructor Create(const AMilliSecondsFormatString: TdxUnicodeString);
  end;

  { TdxSpreadSheetNegativeNumberFormatProvider }

  TdxSpreadSheetNegativeNumberFormatProvider = class(TdxSpreadSheetCustomNumberFormatWrapper)
  public
    procedure Format(const AValue: Variant; const AValueInfo: TdxSpreadSheetNumberFormatValueInfo;
      var AResult: TdxSpreadSheetNumberFormatResult); override;
  end;

  { TdxSpreadSheetNumberWithTrailingThousandsSeparatorsFormatProvider }

  TdxSpreadSheetNumberWithTrailingThousandsSeparatorsFormatProvider = class(TdxSpreadSheetCustomNumberFormat)
  private
    FDivisor: Integer;
  protected
    function GetIsNumeric: Boolean; override;
  public
    constructor Create(const AFormatCode: TdxUnicodeString; ADivisor: Integer); reintroduce;
    procedure Format(const AValue: Variant; const AValueInfo: TdxSpreadSheetNumberFormatValueInfo;
      var AResult: TdxSpreadSheetNumberFormatResult); override;
  end;

  { TdxSpreadSheetSystemLongDateFormatProvider }

  TdxSpreadSheetSystemLongDateFormatProvider = class(TdxSpreadSheetCustomSystemDateTimeFormatProvider)
  protected
    function GetDateTimeFormatString(const AFormatSettings: TFormatSettings): TdxUnicodeString; override;
  end;

  { TdxSpreadSheetSystemShortDateFormatProvider }

  TdxSpreadSheetSystemShortDateFormatProvider = class(TdxSpreadSheetCustomSystemDateTimeFormatProvider)
  protected
    function GetDateTimeFormatString(const AFormatSettings: TFormatSettings): TdxUnicodeString; override;
  end;

  { TdxSpreadSheetSystemLongTimeFormatProvider }

  TdxSpreadSheetSystemLongTimeFormatProvider = class(TdxSpreadSheetCustomSystemDateTimeFormatProvider)
  protected
    function GetDateTimeFormatString(const AFormatSettings: TFormatSettings): TdxUnicodeString; override;
  end;

  { TdxSpreadSheetSystemShortTimeNoTimeDesignatorFormatProvider }

  TdxSpreadSheetSystemShortTimeNoTimeDesignatorFormatProvider = class(TdxSpreadSheetCustomSystemDateTimeFormatProvider)
  protected
    function GetDateTimeFormatString(const AFormatSettings: TFormatSettings): TdxUnicodeString; override;
  end;

  { TdxSpreadSheetTimeDesignatorFormatProvider }

  TdxSpreadSheetTimeDesignatorFormatProvider = class(TdxSpreadSheetCustomNumberFormatProvider)
  private
    FAMText: TdxUnicodeString;
    FPMText: TdxUnicodeString;
  protected
    function GetIs12HourTime: Boolean; override;
    function GetIsDateTime: Boolean; override;
  public
    constructor Create(const AAMText, APMText: TdxUnicodeString);
    procedure Format(const AValue: Variant; const AValueInfo: TdxSpreadSheetNumberFormatValueInfo;
      var AResult: TdxSpreadSheetNumberFormatResult); override;
  end;

  { TdxSpreadSheetCustomNumberFormatProviderFactory }

  TdxSpreadSheetCustomNumberFormatProviderFactory = class(TObject)
  private
    function InsertFormat(const AFormatCode: TdxUnicodeString;
      ATargetProviders: TdxSpreadSheetCustomNumberFormatProviders;
      AIndex, ANewIndex, AMatchLength: Integer; AProvider: TdxSpreadSheetCustomNumberFormatProvider;
      ACustomProviderClass: TdxSpreadSheetCustomNumberFormatClass): Integer;
  protected
    function CreateProvider(const AFormatCode: TdxUnicodeString;
      AMatch: TdxSpreadSheetNumberFormatPatternMatch): TdxSpreadSheetCustomNumberFormatProvider; virtual;
    function GetRegExp: TdxUnicodeString; virtual; abstract;
  public
    procedure Populate(const AFormatCode: TdxUnicodeString; AProviders: TdxSpreadSheetCustomNumberFormatProviders;
      ACustomProviderClass: TdxSpreadSheetCustomNumberFormatClass); virtual;
  end;

  { TdxSpreadSheetCurrencyAndLanguageInfoFormatProviderFactory }

  TdxSpreadSheetCurrencyAndLanguageInfoFormatProviderFactory = class(TdxSpreadSheetCustomNumberFormatProviderFactory)
  protected
    function CreateProvider(const AFormatCode: TdxUnicodeString;
      AMatch: TdxSpreadSheetNumberFormatPatternMatch): TdxSpreadSheetCustomNumberFormatProvider; override;
    function GetRegExp: TdxUnicodeString; override;
  end;

  { TdxSpreadSheetCustomElapsedTimeFormatProviderFactory }

  TdxSpreadSheetCustomElapsedTimeFormatProviderFactory = class(TdxSpreadSheetCustomNumberFormatProviderFactory)
  protected
    function CreateProvider(const AFormatCode: TdxUnicodeString;
      AMatch: TdxSpreadSheetNumberFormatPatternMatch): TdxSpreadSheetCustomNumberFormatProvider; override;
    function GetProviderClass: TdxSpreadSheetCustomElapsedTimeFormatProviderClass; virtual; abstract;
  end;

  { TdxSpreadSheetCustomFractionNumberFormatProviderFactory }

  TdxSpreadSheetCustomFractionNumberFormatProviderFactory = class(TdxSpreadSheetCustomNumberFormatProviderFactory)
  protected
    function CreateProvider(const AFormatCode: TdxUnicodeString;
      AMatch: TdxSpreadSheetNumberFormatPatternMatch): TdxSpreadSheetCustomNumberFormatProvider; override;
    function DoCreateProvider(const ADivident, ADivisor: TdxUnicodeString): TdxSpreadSheetCustomNumberFormatProvider; virtual; abstract;
  end;

  { TdxSpreadSheetDayOfWeekFormatProviderFactory }

  TdxSpreadSheetDayOfWeekFormatProviderFactory = class(TdxSpreadSheetCustomNumberFormatProviderFactory)
  protected
    function CreateProvider(const AFormatCode: TdxUnicodeString;
      AMatch: TdxSpreadSheetNumberFormatPatternMatch): TdxSpreadSheetCustomNumberFormatProvider; override;
    function GetRegExp: TdxUnicodeString; override;
  end;

  { TdxSpreadSheetElapsedHoursFormatProviderFactory }

  TdxSpreadSheetElapsedHoursFormatProviderFactory = class(TdxSpreadSheetCustomElapsedTimeFormatProviderFactory)
  protected
    function GetProviderClass: TdxSpreadSheetCustomElapsedTimeFormatProviderClass; override;
    function GetRegExp: TdxUnicodeString; override;
  end;

  { TdxSpreadSheetElapsedMinutesFormatProviderFactory }

  TdxSpreadSheetElapsedMinutesFormatProviderFactory = class(TdxSpreadSheetCustomElapsedTimeFormatProviderFactory)
  protected
    function GetProviderClass: TdxSpreadSheetCustomElapsedTimeFormatProviderClass; override;
    function GetRegExp: TdxUnicodeString; override;
  end;

  { TdxSpreadSheetElapsedSecondsFormatProviderFactory }

  TdxSpreadSheetElapsedSecondsFormatProviderFactory = class(TdxSpreadSheetCustomElapsedTimeFormatProviderFactory)
  protected
    function GetProviderClass: TdxSpreadSheetCustomElapsedTimeFormatProviderClass; override;
    function GetRegExp: TdxUnicodeString; override;
  end;

  { TdxSpreadSheetExplicitTextFormatProviderFactory }

  TdxSpreadSheetExplicitTextFormatProviderFactory = class(TdxSpreadSheetCustomNumberFormatProviderFactory)
  protected
    function CreateProvider(const AFormatCode: TdxUnicodeString;
      AMatch: TdxSpreadSheetNumberFormatPatternMatch): TdxSpreadSheetCustomNumberFormatProvider; override;
    function GetRegExp: TdxUnicodeString; override;
    function GetText(const AFormatCode: TdxUnicodeString; AMatch: TdxSpreadSheetNumberFormatPatternMatch): TdxUnicodeString; virtual;
  end;

  { TdxSpreadSheetExplicitSymbolFormatProviderFactory }

  TdxSpreadSheetExplicitSymbolFormatProviderFactory = class(TdxSpreadSheetExplicitTextFormatProviderFactory)
  protected
    function GetRegExp: TdxUnicodeString; override;
    function GetText(const AFormatCode: TdxUnicodeString; AMatch: TdxSpreadSheetNumberFormatPatternMatch): TdxUnicodeString; override;
  end;

  { TdxSpreadSheetFirstLetterOfMonthFormatProviderFactory }

  TdxSpreadSheetFirstLetterOfMonthFormatProviderFactory = class(TdxSpreadSheetCustomNumberFormatProviderFactory)
  protected
    function CreateProvider(const AFormatCode: TdxUnicodeString;
      AMatch: TdxSpreadSheetNumberFormatPatternMatch): TdxSpreadSheetCustomNumberFormatProvider; override;
    function GetRegExp: TdxUnicodeString; override;
  end;

  { TdxSpreadSheetFractionExplicitDivisorFormatProviderFactory }

  TdxSpreadSheetFractionExplicitDivisorFormatProviderFactory = class(TdxSpreadSheetCustomFractionNumberFormatProviderFactory)
  protected
    function DoCreateProvider(const ADivident: TdxUnicodeString;
      const ADivisor: TdxUnicodeString): TdxSpreadSheetCustomNumberFormatProvider; override;
    function GetRegExp: TdxUnicodeString; override;
  end;

  { TdxSpreadSheetFractionImplicitDivisorFormatProviderFactory }

  TdxSpreadSheetFractionImplicitDivisorFormatProviderFactory = class(TdxSpreadSheetCustomFractionNumberFormatProviderFactory)
  protected
    function DoCreateProvider(const ADivident: TdxUnicodeString;
      const ADivisor: TdxUnicodeString): TdxSpreadSheetCustomNumberFormatProvider; override;
    function GetRegExp: TdxUnicodeString; override;
  end;

  { TdxSpreadSheetFullMonthFormatProviderFactory }

  TdxSpreadSheetFullMonthFormatProviderFactory = class(TdxSpreadSheetCustomNumberFormatProviderFactory)
  protected
    function CreateProvider(const AFormatCode: TdxUnicodeString;
      AMatch: TdxSpreadSheetNumberFormatPatternMatch): TdxSpreadSheetCustomNumberFormatProvider; override;
    function GetRegExp: TdxUnicodeString; override;
  end;

  { TdxSpreadSheetMillisecondsNumberFormatProviderFactory }

  TdxSpreadSheetMillisecondsNumberFormatProviderFactory = class(TdxSpreadSheetCustomNumberFormatProviderFactory)
  protected
    function CreateProvider(const AFormatCode: TdxUnicodeString;
      AMatch: TdxSpreadSheetNumberFormatPatternMatch): TdxSpreadSheetCustomNumberFormatProvider; override;
    function GetRegExp: TdxUnicodeString; override;
  end;

  { TdxSpreadSheetNumberWithTrailingThousandsSeparatorsFormatProviderFactory }

  TdxSpreadSheetNumberWithTrailingThousandsSeparatorsFormatProviderFactory = class(TdxSpreadSheetCustomNumberFormatProviderFactory)
  protected
    function CreateProvider(const AFormatCode: TdxUnicodeString;
      AMatch: TdxSpreadSheetNumberFormatPatternMatch): TdxSpreadSheetCustomNumberFormatProvider; override;
    function GetRegExp: TdxUnicodeString; override;
  end;

  { TdxSpreadSheetSymbolFillNumberFormatProviderFactory }

  TdxSpreadSheetSymbolFillNumberFormatProviderFactory = class(TdxSpreadSheetExplicitTextFormatProviderFactory)
  protected
    function GetRegExp: TdxUnicodeString; override;
    function GetText(const AFormatCode: TdxUnicodeString; AMatch: TdxSpreadSheetNumberFormatPatternMatch): TdxUnicodeString; override;
  end;

  { TdxSpreadSheetSymbolSpaceNumberFormatProviderFactory }

  TdxSpreadSheetSymbolSpaceNumberFormatProviderFactory = class(TdxSpreadSheetExplicitTextFormatProviderFactory)
  protected
    function GetRegExp: TdxUnicodeString; override;
    function GetText(const AFormatCode: TdxUnicodeString; AMatch: TdxSpreadSheetNumberFormatPatternMatch): TdxUnicodeString; override;
  end;

  { TdxSpreadSheetTimeDesignatorProviderFactory }

  TdxSpreadSheetTimeDesignatorProviderFactory = class(TdxSpreadSheetCustomNumberFormatProviderFactory)
  private
    FAMText: TdxUnicodeString;
    FPMText: TdxUnicodeString;
  protected
    function CreateProvider(const AFormatCode: TdxUnicodeString;
      AMatch: TdxSpreadSheetNumberFormatPatternMatch): TdxSpreadSheetCustomNumberFormatProvider; override;
    function GetRegExp: TdxUnicodeString; override;
  public
    constructor Create(const AAMText, APMText: TdxUnicodeString); virtual;
  end;

  { TdxSpreadSheetSimpleNumberFormatProviderFactory }

  TdxSpreadSheetSimpleNumberFormatProviderFactory = class
  private
    function CalculateLCID(AProvider: TdxSpreadSheetCurrencyAndLanguageInfoFormatProvider): Cardinal;
    function CalculateSystemDateTimeProvider(
      AProvider: TdxSpreadSheetCurrencyAndLanguageInfoFormatProvider): TdxSpreadSheetCustomDateTimeFormatProvider;
    function FindCurrencyAndLanguageInfoFormatProvider(
      AProviders: TdxSpreadSheetCustomNumberFormatProviders): TdxSpreadSheetCurrencyAndLanguageInfoFormatProvider;
  protected
    procedure CreateFakeProviders(AProviders: TdxSpreadSheetCustomNumberFormatProviders;
      AFactory: TdxSpreadSheetCustomNumberFormatProviderFactory);
    procedure CreateProviders(AProviders: TdxSpreadSheetCustomNumberFormatProviders;
      AFactory: TdxSpreadSheetCustomNumberFormatProviderFactory; ACustomProviderClass: TdxSpreadSheetCustomNumberFormatClass = nil);
    procedure ValidateFakeProviders(AProviders: TdxSpreadSheetCustomNumberFormatProviders);

    function ProcessMinutes(const AFormatCode: TdxUnicodeString; AFrom, ATo, AStep: Integer; AIgnoreChar: TdxUnicodeChar): TdxUnicodeString;
    function ProcessMonthAndMinutes(const AFormatCode: TdxUnicodeString): TdxUnicodeString;
    procedure ProcessMonthAndMinutesSpecifiers(AProviders: TdxSpreadSheetCustomNumberFormatProviders; AIs12HourTime: Boolean);
  public
    function CreateProvider(const AFormatCode: TdxUnicodeString; ID: Integer): TdxSpreadSheetConcatenateNumberFormatProvider;
  end;

implementation

uses
  StrUtils, cxGraphics, Math, cxGeometry, Windows, Variants, dxSpreadSheetUtils;

type
  TdxSpreadSheetCustomNumberFormatAccess = class(TdxSpreadSheetCustomNumberFormat);

{ TdxSpreadSheetCustomDateTimeFormatProvider }

procedure TdxSpreadSheetCustomDateTimeFormatProvider.Format(
  const AValue: Variant; const AValueInfo: TdxSpreadSheetNumberFormatValueInfo; var AResult: TdxSpreadSheetNumberFormatResult);
var
  ATempValue: Double;
begin
  AResult.Reset;
  if dxIsNumericType(AValueInfo.ValueType) then
  begin
    ATempValue := AValue;
    if (ATempValue < 0) or (ATempValue >= Double(MaxDateTime)) then
    begin
      AResult.IsError := True;
      AResult.Text := '#';
    end
    else
      DoFormat(AValue, AValueInfo, AResult);
  end;
end;

function TdxSpreadSheetCustomDateTimeFormatProvider.GetIs12HourTime: Boolean;
begin
  Result := True;
end;

function TdxSpreadSheetCustomDateTimeFormatProvider.GetIsDateTime: Boolean;
begin
  Result := True;
end;

{ TdxSpreadSheetCustomElapsedTimeFormatProvider }

constructor TdxSpreadSheetCustomElapsedTimeFormatProvider.Create(const AFormatString, AFormatCode: TdxUnicodeString);
begin
  inherited Create;
  FFormatString := AFormatString;
  FFormatCode := AFormatCode;
end;

procedure TdxSpreadSheetCustomElapsedTimeFormatProvider.DoFormat(
  const AValue: Variant; const AValueInfo: TdxSpreadSheetNumberFormatValueInfo; var AResult: TdxSpreadSheetNumberFormatResult);
begin
  AResult.Reset;
  AResult.Text := FormatFloat(FFormatString, CalculateActualValue(AValue), AValueInfo.FormatSettings);
end;

function TdxSpreadSheetCustomElapsedTimeFormatProvider.GetIs12HourTime: Boolean;
begin
  Result := FIs12HourTime;
end;

procedure TdxSpreadSheetCustomElapsedTimeFormatProvider.SetIs12HourTime(const AValue: Boolean);
begin
  FIs12HourTime := AValue;
end;

{ TdxSpreadSheetCustomFractionFormatProvider }

constructor TdxSpreadSheetCustomFractionFormatProvider.Create(
  const ADividentFormatCode, ADivisorFormatCode: TdxUnicodeString);
begin
  inherited Create;
  FDivisorFormatProvider := TdxSpreadSheetCustomNumberFormat.Create(ADivisorFormatCode);
  FDividentFormatProvider := TdxSpreadSheetCustomNumberFormat.Create(ADividentFormatCode);
end;

destructor TdxSpreadSheetCustomFractionFormatProvider.Destroy;
begin
  FreeAndNil(FDividentFormatProvider);
  FreeAndNil(FDivisorFormatProvider);
  inherited Destroy;
end;

procedure TdxSpreadSheetCustomFractionFormatProvider.Format(
  const AValue: Variant; const AValueInfo: TdxSpreadSheetNumberFormatValueInfo; var AResult: TdxSpreadSheetNumberFormatResult);
begin
  AResult.Reset;
  if dxIsNumericType(AValueInfo.ValueType) then
    AResult.Text := FormatFraction(AValue, AValueInfo.FormatSettings);
end;

function TdxSpreadSheetCustomFractionFormatProvider.CalculateLeadingSpacesCount(const AValue: TdxUnicodeString): Integer;
var
  I: Integer;
begin
  Result := 0;
  for I := 1 to Length(AValue) do
  begin
    if AValue[I] <> ' ' then
      Break;
    Inc(Result);
  end;
end;

function TdxSpreadSheetCustomFractionFormatProvider.ConvertLeadingSpacesToTrailingSpaces(
  const AValue: TdxUnicodeString): TdxUnicodeString;
var
  ALeadingSpaceCount: Integer;
begin
  ALeadingSpaceCount := CalculateLeadingSpacesCount(AValue);
  if ALeadingSpaceCount > 0 then
    Result := Copy(AValue, ALeadingSpaceCount + 1, MaxInt) + DupeString(' ', ALeadingSpaceCount)
  else
    Result := AValue;
end;

function TdxSpreadSheetCustomFractionFormatProvider.FormatFraction(
  const AValue: Extended; const AFormatSettings: TFormatSettings): TdxUnicodeString;

  function DoFormatNumeric(AProvider: TdxSpreadSheetCustomNumberFormat; const AValue: Extended): TdxUnicodeString;
  var
    AValueInfo: TdxSpreadSheetNumberFormatValueInfo;
  begin
    AValueInfo.DateTimeSystem := dts1900;
    AValueInfo.FormatSettings := AFormatSettings;
    AValueInfo.ValueType := cdtFloat;
    Result := TdxSpreadSheetCustomNumberFormatAccess(AProvider).FormatNumeric(AValue, AValueInfo);
  end;

var
  AApproximation: TSize;
  ADivident: TdxUnicodeString;
  ADivisor: TdxUnicodeString;
begin
  AApproximation := CalculateRationalApproximation(Abs(AValue));

  ADivident := DoFormatNumeric(FDividentFormatProvider, Sign(AValue) * AApproximation.cx);
  if AApproximation.cx = 0 then
    ADivident := ReplaceSpacesWithZero(ADivident);

  ADivisor := ConvertLeadingSpacesToTrailingSpaces(DoFormatNumeric(FDivisorFormatProvider, AApproximation.cy));

  Result := ADivident + '/' + ADivisor;
  if (AApproximation.cy = 1) and not FormatOneDividerValue then
    Result := DupeString(' ', Length(Result));
end;

function TdxSpreadSheetCustomFractionFormatProvider.GetIsFraction: Boolean;
begin
  Result := True;
end;

function TdxSpreadSheetCustomFractionFormatProvider.GetIsNumeric: Boolean;
begin
  Result := True;
end;

function TdxSpreadSheetCustomFractionFormatProvider.ReplaceSpacesWithZero(const AValue: TdxUnicodeString): TdxUnicodeString;
var
  ALeadingSpaceCount: Integer;
begin
  ALeadingSpaceCount := CalculateLeadingSpacesCount(AValue);
  if ALeadingSpaceCount < Length(AValue) then
    Result := AValue
  else
    Result := Copy(AValue, 1, Length(AValue) - 1) + '0';
end;

{ TdxSpreadSheetCustomSystemDateTimeFormatProvider }

constructor TdxSpreadSheetCustomSystemDateTimeFormatProvider.Create;
begin
  inherited Create;
  FFormats := TStringList.Create;
end;

destructor TdxSpreadSheetCustomSystemDateTimeFormatProvider.Destroy;
var
  I: Integer;
begin
  for I := 0 to FFormats.Count - 1 do
    FFormats.Objects[I].Free;
  FreeAndNil(FFormats);
  inherited Destroy;
end;

procedure TdxSpreadSheetCustomSystemDateTimeFormatProvider.DoFormat(
  const AValue: Variant; const AValueInfo: TdxSpreadSheetNumberFormatValueInfo; var AResult: TdxSpreadSheetNumberFormatResult);
var
  AFormatString: TdxUnicodeString;
  AIndex: Integer;
begin
  AFormatString := GetDateTimeFormatString(AValueInfo.FormatSettings);
  AIndex := FFormats.IndexOf(AFormatString);
  if AIndex < 0 then
    AIndex := FFormats.AddObject(AFormatString, TdxSpreadSheetCustomNumberFormat.Create(AFormatString));
  TdxSpreadSheetCustomNumberFormat(FFormats.Objects[AIndex]).Format(AValue, AValueInfo, AResult);
end;

{ TdxSpreadSheetColorNumberFormatProvider }

constructor TdxSpreadSheetColorNumberFormatProvider.Create(
  ASource: TdxSpreadSheetCustomNumberFormatProvider; AColor: TColor);
begin
  inherited Create(ASource);
  FColor := AColor;
end;

procedure TdxSpreadSheetColorNumberFormatProvider.Format(
  const AValue: Variant; const AValueInfo: TdxSpreadSheetNumberFormatValueInfo; var AResult: TdxSpreadSheetNumberFormatResult);
begin
  inherited Format(AValue, AValueInfo, AResult);
  AResult.Color := Color;
end;

class function TdxSpreadSheetColorNumberFormatProvider.Parse(var AFormatCode: TdxUnicodeString; out AColor: TColor): Boolean;
const
  ColorMap: array[0..7] of TColor = (
    clRed, clBlack, clWhite, clBlue, clFuchsia, clYellow, clAqua, clGreen
  );
  ColorTags: array[0..7] of TdxUnicodeString = (
    '[Red]', '[Black]', '[White]', '[Blue]', '[Magenta]', '[Yellow]', '[Cyan]', '[Green]'
  );
var
  I: Integer;
begin
  AColor := clNone;
  for I := 0 to Length(ColorTags) - 1 do
    RemoveColorFormattingString(AFormatCode, AColor, ColorTags[I], ColorMap[I]);
  RemoveIndexedColorFormattingString(AFormatCode, AColor);
  Result := cxColorIsValid(AColor);
end;

class procedure TdxSpreadSheetColorNumberFormatProvider.RemoveColorFormattingString(
  var AFormatCode: TdxUnicodeString; var ATargetColor: TColor; const AColorString: TdxUnicodeString; AColor: TColor);
var
  AIndex: Integer;
begin
  repeat
    AIndex := Pos(UpperCase(AColorString), UpperCase(AFormatCode));
    if AIndex > 0 then
    begin
      ATargetColor := AColor;
      Delete(AFormatCode, AIndex, Length(AColorString));
    end;
  until AIndex = 0;
end;

class procedure TdxSpreadSheetColorNumberFormatProvider.RemoveIndexedColorFormattingString(
  var AFormatCode: TdxUnicodeString; var ATargetColor: TColor);

  procedure DoRemoveIndexedColorFormattingString(AMatch: TdxSpreadSheetNumberFormatPatternMatch);
  var
    AIndex: Integer;
  begin
    AIndex := StrToIntDef(Copy(AFormatCode, AMatch.Position + 6, AMatch.Length - 7), 0);
    if InRange(AIndex - 1, Low(dxExcelStandardColors), High(dxExcelStandardColors)) then
    begin
      ATargetColor := dxExcelStandardColors[AIndex - 1];
      Delete(AFormatCode, AMatch.Position, AMatch.Length);
    end;
  end;

var
  APattern: TdxSpreadSheetNumberFormatPattern;
  I: Integer;
begin
  APattern := TdxSpreadSheetNumberFormatPattern.Create('\[Color\d+\]');
  try
    APattern.Process(AFormatCode);
    for I := 0 to APattern.MatchCount - 1 do
      DoRemoveIndexedColorFormattingString(APattern.Matches[I])
  finally
    APattern.Free;
  end;
end;

{ TdxSpreadSheetCustomNumberFormatWrapper }

constructor TdxSpreadSheetCustomNumberFormatWrapper.Create(ASource: TdxSpreadSheetCustomNumberFormatProvider);
begin
  inherited Create;
  FSource := ASource;
end;

destructor TdxSpreadSheetCustomNumberFormatWrapper.Destroy;
begin
  FreeAndNil(FSource);
  inherited Destroy;
end;

procedure TdxSpreadSheetCustomNumberFormatWrapper.Format(
  const AValue: Variant; const AValueInfo: TdxSpreadSheetNumberFormatValueInfo; var AResult: TdxSpreadSheetNumberFormatResult);
begin
  Source.Format(AValue, AValueInfo, AResult);
end;

function TdxSpreadSheetCustomNumberFormatWrapper.GetHasMilliseconds: Boolean;
begin
  Result := Source.HasMilliseconds;
end;

function TdxSpreadSheetCustomNumberFormatWrapper.GetIs12HourTime: Boolean;
begin
  Result := Source.Is12HourTime;
end;

function TdxSpreadSheetCustomNumberFormatWrapper.GetIsDateTime: Boolean;
begin
  Result := Source.IsDateTime;
end;

function TdxSpreadSheetCustomNumberFormatWrapper.GetIsFraction: Boolean;
begin
  Result := Source.IsFraction;
end;

function TdxSpreadSheetCustomNumberFormatWrapper.GetIsNumeric: Boolean;
begin
  Result := Source.IsNumeric;
end;

function TdxSpreadSheetCustomNumberFormatWrapper.GetIsNumericNoFraction: Boolean;
begin
  Result := Source.IsNumericNoFraction;
end;

function TdxSpreadSheetCustomNumberFormatWrapper.GetIsText: Boolean;
begin
  Result := Source.IsText;
end;

procedure TdxSpreadSheetCustomNumberFormatWrapper.SetHasMilliseconds(const AValue: Boolean);
begin
  Source.HasMilliseconds := AValue;
end;

procedure TdxSpreadSheetCustomNumberFormatWrapper.SetIs12HourTime(const AValue: Boolean);
begin
  Source.Is12HourTime := AValue;
end;

procedure TdxSpreadSheetCustomNumberFormatWrapper.SetIsDateTime(const AValue: Boolean);
begin
  Source.IsDateTime := AValue;
end;

procedure TdxSpreadSheetCustomNumberFormatWrapper.SetIsFraction(const AValue: Boolean);
begin
  Source.IsFraction := AValue;
end;

procedure TdxSpreadSheetCustomNumberFormatWrapper.SetIsNumeric(const AValue: Boolean);
begin
  Source.IsNumeric := AValue;
end;

procedure TdxSpreadSheetCustomNumberFormatWrapper.SetIsNumericNoFraction(const AValue: Boolean);
begin
  Source.IsNumericNoFraction := AValue;
end;

procedure TdxSpreadSheetCustomNumberFormatWrapper.SetIsText(const AValue: Boolean);
begin
  Source.IsText := AValue;
end;

{ TdxSpreadSheetConcatenateNumberFormatProvider }

constructor TdxSpreadSheetConcatenateNumberFormatProvider.Create;
begin
  inherited Create;
  FProviders := TdxSpreadSheetCustomNumberFormatProviders.Create;
end;

destructor TdxSpreadSheetConcatenateNumberFormatProvider.Destroy;
begin
  FreeAndNil(FProviders);
  inherited Destroy;
end;

procedure TdxSpreadSheetConcatenateNumberFormatProvider.AddProvider(AProvider: TdxSpreadSheetCustomNumberFormatProvider);
begin
  FProviders.Add(AProvider)
end;

procedure TdxSpreadSheetConcatenateNumberFormatProvider.CalculateFormatInfo;
var
  AProvider: TdxSpreadSheetCustomNumberFormatProvider;
  I: Integer;
begin
  for I := 0 to Providers.Count - 1 do
  begin
    AProvider := Providers[I];
    IsDateTime := IsDateTime or AProvider.IsDateTime;
    Is12HourTime := Is12HourTime or AProvider.Is12HourTime;
    IsFraction := IsFraction or AProvider.IsFraction;
    IsNumeric := IsNumeric or AProvider.IsNumeric;
    IsNumericNoFraction := IsNumericNoFraction or AProvider.IsNumericNoFraction;
    IsText := IsText or AProvider.IsText;
    HasMilliseconds := HasMilliseconds or AProvider.HasMilliseconds;
  end;

  if IsText then
  begin
    IsNumeric := False;
    IsNumericNoFraction := False;
    IsDateTime := False;
    IsFraction := False;
    Is12HourTime := False;
    HasMilliseconds := False;
  end;

  for I := 0 to Providers.Count - 1 do
  begin
    AProvider := Providers[I];
    AProvider.IsDateTime := IsDateTime;
    AProvider.Is12HourTime := Is12HourTime;
    AProvider.IsFraction := IsFraction;
    AProvider.IsNumeric := IsNumeric;
    AProvider.IsNumericNoFraction := IsNumericNoFraction;
    AProvider.IsText := IsText;
    AProvider.HasMilliseconds := HasMilliseconds;
  end;
end;

procedure TdxSpreadSheetConcatenateNumberFormatProvider.Format(
  const AValue: Variant; const AValueInfo: TdxSpreadSheetNumberFormatValueInfo; var AResult: TdxSpreadSheetNumberFormatResult);
var
  AActualFormatSettings: TFormatSettings;
  AFormatResult: TdxSpreadSheetNumberFormatResult;
  AFractionValue: Variant;
  AProvider: TdxSpreadSheetCustomNumberFormatProvider;
  AResultParts: array of TdxUnicodeString;
  I: Integer;
begin
  if LCID <> 0 then
    AActualFormatSettings := FLCIDFormatSettings
  else
    AActualFormatSettings := AValueInfo.FormatSettings;

  if dxIsNumericType(AValueInfo.ValueType) and IsFraction and IsNumericNoFraction then
    AFractionValue := Abs(AValue - Int(AValue))
  else
    AFractionValue := AValue;

  SetLength(AResultParts, Providers.Count);
  for I := 0 to Providers.Count - 1 do
  begin
    AProvider := Providers[I];
    if AProvider.IsFraction then
    begin
      (AProvider as TdxSpreadSheetCustomFractionFormatProvider).FormatOneDividerValue := not IsNumericNoFraction;
      AProvider.Format(AFractionValue, AValueInfo.ValueType, AValueInfo.DateTimeSystem, AActualFormatSettings, AFormatResult);
    end
    else
      if IsFraction and dxIsNumericType(AValueInfo.ValueType) then
        AProvider.Format(Int(AValue), AValueInfo.ValueType, AValueInfo.DateTimeSystem, AActualFormatSettings, AFormatResult)
      else
        AProvider.Format(AValue, AValueInfo.ValueType, AValueInfo.DateTimeSystem, AActualFormatSettings, AFormatResult);

    if AFormatResult.IsError then
    begin
      AResult := AFormatResult;
      Exit;
    end;

    AResultParts[I] := AFormatResult.Text;
  end;

  AResult.Reset;
  AResult.Text := '';
  for I := 0 to Length(AResultParts) - 1 do
    AResult.Text := AResult.Text + AResultParts[I];

  if IsFraction and dxIsNumericType(AValueInfo.ValueType) and (Trim(AResult.Text) = '') then
    AResult.Text := '0' + DupeString(' ', Length(AResult.Text));
end;

procedure TdxSpreadSheetConcatenateNumberFormatProvider.ReplaceTrailingNumberFormats;
var
  ANumberFormatsCount: Integer;
  AProvider: TdxSpreadSheetCustomNumberFormat;
  I: Integer;
begin
  ANumberFormatsCount := 0;
  for I := 0 to Providers.Count - 1 do
  begin
    if Providers[I] is TdxSpreadSheetCustomNumberFormat then
    begin
      AProvider := TdxSpreadSheetCustomNumberFormat(Providers[I]);
      if AProvider.IsNumeric and AProvider.IsNumericNoFraction and not
        (AProvider.IsText or AProvider.IsDateTime) and AProvider.ContainsNumericSpecifiers
      then
      begin
        if ANumberFormatsCount <> 0 then
          Providers[I] := TdxSpreadSheetExplicitTextFormatProvider.Create(AProvider.FormatCode);
        Inc(ANumberFormatsCount);
      end;
    end;
  end;
end;

function TdxSpreadSheetConcatenateNumberFormatProvider.GetHasMilliseconds: Boolean;
begin
  Result := FHasMilliseconds;
end;

function TdxSpreadSheetConcatenateNumberFormatProvider.GetIs12HourTime: Boolean;
begin
  Result := FIs12HourTime;
end;

function TdxSpreadSheetConcatenateNumberFormatProvider.GetIsDateTime: Boolean;
begin
  Result := FIsDateTime;
end;

function TdxSpreadSheetConcatenateNumberFormatProvider.GetIsFraction: Boolean;
begin
  Result := FIsFraction;
end;

function TdxSpreadSheetConcatenateNumberFormatProvider.GetIsNumeric: Boolean;
begin
  Result := FIsNumeric;
end;

function TdxSpreadSheetConcatenateNumberFormatProvider.GetIsNumericNoFraction: Boolean;
begin
  Result := FIsNumericNoFraction;
end;

function TdxSpreadSheetConcatenateNumberFormatProvider.GetIsText: Boolean;
begin
  Result := FIsText;
end;

procedure TdxSpreadSheetConcatenateNumberFormatProvider.SetHasMilliseconds(const AValue: Boolean);
begin
  FHasMilliseconds := AValue;
end;

procedure TdxSpreadSheetConcatenateNumberFormatProvider.SetIs12HourTime(const AValue: Boolean);
begin
  FIs12HourTime := AValue;
end;

procedure TdxSpreadSheetConcatenateNumberFormatProvider.SetIsDateTime(const AValue: Boolean);
begin
  FIsDateTime := AValue;
end;

procedure TdxSpreadSheetConcatenateNumberFormatProvider.SetIsFraction(const AValue: Boolean);
begin
  FIsFraction := AValue;
end;

procedure TdxSpreadSheetConcatenateNumberFormatProvider.SetIsNumeric(const AValue: Boolean);
begin
  FIsNumeric := AValue;
end;

procedure TdxSpreadSheetConcatenateNumberFormatProvider.SetIsNumericNoFraction(const AValue: Boolean);
begin
  FIsNumericNoFraction := AValue;
end;

procedure TdxSpreadSheetConcatenateNumberFormatProvider.SetIsText(const AValue: Boolean);
begin
  FIsText := AValue;
end;

procedure TdxSpreadSheetConcatenateNumberFormatProvider.SetLCID(AValue: Cardinal);
begin
  if LCID <> AValue then
  begin
    FLCID := AValue;
    if LCID <> 0 then
      dxGetLocaleFormatSettings(LCID, FLCIDFormatSettings);
  end;
end;

{ TdxSpreadSheetConditionalNumberFormatProvider }

procedure TdxSpreadSheetConditionalNumberFormatProvider.Format(const AValue: Variant;
  const AValueInfo: TdxSpreadSheetNumberFormatValueInfo; var AResult: TdxSpreadSheetNumberFormatResult);
var
  AActualProvider: TdxSpreadSheetCustomNumberFormatProvider;
begin
  AActualProvider := GetActualFormatProvider(AValue, AValueInfo.ValueType);
  if (AValueInfo.ValueType = cdtString) and not AActualProvider.IsText then
  begin
    AResult.Reset;
    if not VarIsNull(AValue) then
      AResult.Text := AValue;
  end
  else
    AActualProvider.Format(AValue, AValueInfo, AResult);
end;

{ TdxSpreadSheetCurrencyAndLanguageInfoFormatProvider }

constructor TdxSpreadSheetCurrencyAndLanguageInfoFormatProvider.Create(
  const ACurrencyString: TdxUnicodeString; ALanguageInfo: Cardinal);
begin
  inherited Create;
  FCurrencyString := ACurrencyString;
  FLanguageInfo := ALanguageInfo;
end;

procedure TdxSpreadSheetCurrencyAndLanguageInfoFormatProvider.Format(
  const AValue: Variant; const AValueInfo: TdxSpreadSheetNumberFormatValueInfo; var AResult: TdxSpreadSheetNumberFormatResult);
begin
  AResult.Reset;
  AResult.Text := FCurrencyString;
end;

{ TdxSpreadSheetDayOfWeekFormatProvider }

constructor TdxSpreadSheetDayOfWeekFormatProvider.Create(const AFormatString: TdxUnicodeString);
begin
  inherited Create;
  FFormatString := AFormatString;
end;

procedure TdxSpreadSheetDayOfWeekFormatProvider.DoFormat(
  const AValue: Variant; const AValueInfo: TdxSpreadSheetNumberFormatValueInfo; var AResult: TdxSpreadSheetNumberFormatResult);
begin
  AResult.Reset;
  AResult.Text := FormatDateTime(FFormatString, AValue, AValueInfo.FormatSettings);
end;

{ TdxSpreadSheetDefaultConditionalNumberFormatProvider }

constructor TdxSpreadSheetDefaultConditionalNumberFormatProvider.Create;
begin
  inherited Create;
  FProviders := TdxSpreadSheetCustomNumberFormatProviders.Create;
  FEmptyNumberFormatProvider := TdxSpreadSheetEmptyNumberFormatProvider.Create;
end;

destructor TdxSpreadSheetDefaultConditionalNumberFormatProvider.Destroy;
begin
  FreeAndNil(FEmptyNumberFormatProvider);
  FreeAndNil(FProviders);
  inherited Destroy;
end;

procedure TdxSpreadSheetDefaultConditionalNumberFormatProvider.AddProvider(AProvider: TdxSpreadSheetCustomNumberFormatProvider);
begin
  FProviders.Add(AProvider)
end;

function TdxSpreadSheetDefaultConditionalNumberFormatProvider.GetActualFormatProvider(
  const AValue: Variant; AValueType: TdxSpreadSheetCellDataType): TdxSpreadSheetCustomNumberFormatProvider;
begin
  dxTestCheck(Providers.Count > 0, 'GetActualFormatProvider');

  if Providers.Count = 1 then
  begin
    if not dxIsNumericType(AValueType) and Providers[0].IsNumeric then
      Result := FEmptyNumberFormatProvider
    else
      Result := Providers[0];
  end
  else

  if Providers.Count = 2 then
  begin
    if not dxIsNumericType(AValueType) then
      Result := FEmptyNumberFormatProvider
    else
      if AValue < 0 then
        Result := Providers[1]
      else
        Result := Providers[0];
  end
  else

  if Providers.Count >= 3 then
  begin
    if not dxIsNumericType(AValueType) then
    begin
      if Providers.Count >= 4 then
        Result := Providers[3]
      else
        Result := FEmptyNumberFormatProvider;
    end
    else
      if AValue < 0 then
        Result := Providers[1]
      else
        if AValue = 0 then
          Result := Providers[2]
        else
          Result := Providers[0];
  end
  else
    if dxIsNumericType(AValueType) then
      Result := Providers[0]
    else
      Result := FEmptyNumberFormatProvider;
end;

{ TdxSpreadSheetElapsedHoursFormatProvider }

function TdxSpreadSheetElapsedHoursFormatProvider.CalculateActualValue(const AValue: Extended): Cardinal;
begin
  Result := Trunc(AValue * 24);
end;

{ TdxSpreadSheetElapsedMinutesFormatProvider }

function TdxSpreadSheetElapsedMinutesFormatProvider.CalculateActualValue(const AValue: Extended): Cardinal;
begin
  Result := Trunc(AValue * 24 * 60);
end;

{ TdxSpreadSheetElapsedSecondsFormatProvider }

function TdxSpreadSheetElapsedSecondsFormatProvider.CalculateActualValue(const AValue: Extended): Cardinal;
var
  AResult: Extended;
begin
  AResult := AValue * 24 * 60 * 60;
  Result := Trunc(AResult);
  if Abs(AResult - Result - 1) < 1E-9 then
    Inc(Result);
end;

{ TdxSpreadSheetEmptyNumberFormatProvider }

procedure TdxSpreadSheetEmptyNumberFormatProvider.Format(const AValue: Variant;
  const AValueInfo: TdxSpreadSheetNumberFormatValueInfo; var AResult: TdxSpreadSheetNumberFormatResult);
begin
  if AValueInfo.ValueType = cdtBoolean then
    inherited Format(AValue, AValueInfo, AResult)
  else
  begin
    AResult.Reset;
    AResult.Text := AValue;
  end;
end;

{ TdxSpreadSheetExplicitTextFormatProvider }

constructor TdxSpreadSheetExplicitTextFormatProvider.Create(const AText: TdxUnicodeString);
begin
  inherited Create;
  FText := AText;
end;

procedure TdxSpreadSheetExplicitTextFormatProvider.Format(const AValue: Variant;
  const AValueInfo: TdxSpreadSheetNumberFormatValueInfo; var AResult: TdxSpreadSheetNumberFormatResult);
begin
  AResult.Reset;
  AResult.Text := FText;
  AResult.IsText := bTrue;
end;

{ TdxSpreadSheetFakeNumberFormatProvider }

procedure TdxSpreadSheetFakeNumberFormatProvider.Format(
  const AValue: Variant; const AValueInfo: TdxSpreadSheetNumberFormatValueInfo; var AResult: TdxSpreadSheetNumberFormatResult);
begin
  raise EdxSpreadSheetNumberFormatError.Create('Not supported');
end;

procedure TdxSpreadSheetFakeNumberFormatProvider.Initialize;
begin
  // do nothing
end;

{ TdxSpreadSheetFractionExplicitDivisorFormatProvider }

constructor TdxSpreadSheetFractionExplicitDivisorFormatProvider.Create(
  const ADividentFormatCode, ADivisorFormatCode: TdxUnicodeString; ADivisor: Integer);
begin
  inherited Create(ADividentFormatCode, ADivisorFormatCode);
  FDivisor := ADivisor;
end;

function TdxSpreadSheetFractionExplicitDivisorFormatProvider.CalculateRationalApproximation(AValue: Extended): TSize;
begin
  Result := cxSize(Round(AValue * FDivisor), FDivisor);
end;

{ TdxSpreadSheetFractionImplicitDivisorFormatProvider }

constructor TdxSpreadSheetFractionImplicitDivisorFormatProvider.Create(
  const ADividentFormatCode, ADivisorFormatCode: TdxUnicodeString; AMaxDivisor: Integer);
begin
  inherited Create(ADividentFormatCode, ADivisorFormatCode);
  FMaxDivisor := AMaxDivisor;
end;

function TdxSpreadSheetFractionImplicitDivisorFormatProvider.CalculateRationalApproximation(AValue: Extended): TSize;
var
  AApproximation: Extended;
  ABestDivident: Integer;
  ABestDivisor: Integer;
  ADivident: Integer;
  ADivisor: Integer;
  AIntPart: Integer;
  ALeftDivident: Integer;
  ALeftDivisor: Integer;
  AMinimalError: Extended;
  ARightDivident: Integer;
  ARightDivisor: Integer;
begin
  dxTestCheck(AValue >= 0, 'CalculateRationalApproximation');

  AIntPart := Trunc(AValue);
  AValue := AValue - AIntPart;

  ALeftDivident := 0;
  ALeftDivisor := 1;
  ARightDivident := 1;
  ARightDivisor := 1;

  if Abs(AValue) < Abs(AValue - ARightDivident) then
  begin
    AMinimalError := Abs(AValue);
    ABestDivident := ALeftDivident;
    ABestDivisor := ALeftDivisor;
  end
  else
  begin
    AMinimalError := Abs(AValue - ARightDivident);
    ABestDivident := ARightDivident;
    ABestDivisor := ARightDivisor;
  end;

  while True do
  begin
    ADivisor := ALeftDivisor + ARightDivisor;
    if ADivisor > FMaxDivisor then
      Break;

    ADivident := ALeftDivident + ARightDivident;
    if AValue * ADivisor < ADivident then
    begin
      ARightDivident := ADivident;
      ARightDivisor := ADivisor;
    end
    else
    begin
      ALeftDivident := ADivident;
      ALeftDivisor := ADivisor;
    end;

    AApproximation := ADivident / ADivisor;
    if Abs(AValue - AApproximation) < AMinimalError then
    begin
      AMinimalError := Abs(AValue - AApproximation);
      ABestDivident := ADivident;
      ABestDivisor := ADivisor;
    end;
  end;
  Result := cxSize(ABestDivident + AIntPart * ABestDivisor, ABestDivisor);
end;

{ TdxSpreadSheetFullMonthFormatProvider }

procedure TdxSpreadSheetFullMonthFormatProvider.DoFormat(const AValue: Variant;
  const AValueInfo: TdxSpreadSheetNumberFormatValueInfo; var AResult: TdxSpreadSheetNumberFormatResult);
begin
  AResult.Reset;
  AResult.Text := TdxSpreadSheetNumberFormatDateTimeHelper.Format(
    AValue, 'MMMM', AValueInfo.FormatSettings, Is12HourTime, HasMilliseconds);
end;

{ TdxSpreadSheetFirstLetterOfMonthFormatProvider }

procedure TdxSpreadSheetFirstLetterOfMonthFormatProvider.DoFormat(
  const AValue: Variant; const AValueInfo: TdxSpreadSheetNumberFormatValueInfo; var AResult: TdxSpreadSheetNumberFormatResult);
begin
  inherited DoFormat(AValue, AValueInfo, AResult);
  AResult.Text := Copy(AResult.Text, 1, 1);
end;

{ TdxSpreadSheetMillisecondsFormatProvider }

constructor TdxSpreadSheetMillisecondsFormatProvider.Create(const AMilliSecondsFormatString: TdxUnicodeString);
begin
  inherited Create;
  FFormatString := AMilliSecondsFormatString;
end;

procedure TdxSpreadSheetMillisecondsFormatProvider.DoFormat(
  const AValue: Variant; const AValueInfo: TdxSpreadSheetNumberFormatValueInfo; var AResult: TdxSpreadSheetNumberFormatResult);
var
  X, ASeconds, AMilliSeconds: Word;
begin
  AResult.Reset;
  try
    DecodeTime(dxDateTimeToRealDateTime(AValue, AValueInfo.DateTimeSystem), X, X, ASeconds, AMilliSeconds);
    AResult.Text := FormatFloat('00', ASeconds, AValueInfo.FormatSettings) +
      FormatFloat(FFormatString, AMilliSeconds / 1000, AValueInfo.FormatSettings);
  except
    AResult.IsError := True;
  end;
end;

{ TdxSpreadSheetNegativeNumberFormatProvider }

procedure TdxSpreadSheetNegativeNumberFormatProvider.Format(
  const AValue: Variant; const AValueInfo: TdxSpreadSheetNumberFormatValueInfo; var AResult: TdxSpreadSheetNumberFormatResult);
begin
  if dxIsNumericType(AValueInfo.ValueType) then
    inherited Format(Abs(AValue), AValueInfo, AResult)
  else
    inherited Format(AValue, AValueInfo, AResult);
end;

{ TdxSpreadSheetNumberWithTrailingThousandsSeparatorsFormatProvider }

constructor TdxSpreadSheetNumberWithTrailingThousandsSeparatorsFormatProvider.Create(
  const AFormatCode: TdxUnicodeString; ADivisor: Integer);
begin
  inherited Create(AFormatCode);
  FDivisor := ADivisor;
end;

procedure TdxSpreadSheetNumberWithTrailingThousandsSeparatorsFormatProvider.Format(
  const AValue: Variant; const AValueInfo: TdxSpreadSheetNumberFormatValueInfo;
  var AResult: TdxSpreadSheetNumberFormatResult);
begin
  if dxIsNumericType(AValueInfo.ValueType) then
    inherited Format(AValue / FDivisor, AValueInfo, AResult)
  else
    inherited Format(AValue, AValueInfo, AResult);
end;

function TdxSpreadSheetNumberWithTrailingThousandsSeparatorsFormatProvider.GetIsNumeric: Boolean;
begin
  Result := True;
end;

{ TdxSpreadSheetSystemLongDateFormatProvider }

function TdxSpreadSheetSystemLongDateFormatProvider.GetDateTimeFormatString(
  const AFormatSettings: TFormatSettings): TdxUnicodeString;
begin
  Result := AFormatSettings.LongDateFormat;
end;

{ TdxSpreadSheetSystemShortDateFormatProvider }

function TdxSpreadSheetSystemShortDateFormatProvider.GetDateTimeFormatString(
  const AFormatSettings: TFormatSettings): TdxUnicodeString;
begin
  Result := AFormatSettings.ShortDateFormat;
end;

{ TdxSpreadSheetSystemLongTimeFormatProvider }

function TdxSpreadSheetSystemLongTimeFormatProvider.GetDateTimeFormatString(
  const AFormatSettings: TFormatSettings): TdxUnicodeString;
begin
  Result := AFormatSettings.LongTimeFormat;
end;

{ TdxSpreadSheetSystemShortTimeNoTimeDesignatorFormatProvider }

function TdxSpreadSheetSystemShortTimeNoTimeDesignatorFormatProvider.GetDateTimeFormatString(
  const AFormatSettings: TFormatSettings): TdxUnicodeString;
const
  TimeDesignator = ' tt';
begin
  Result := AFormatSettings.ShortTimeFormat;
  if Copy(Result, Length(Result) - Length(TimeDesignator), Length(TimeDesignator)) = TimeDesignator then
    Result := StringReplace(Copy(Result, 1, Length(Result) - Length(TimeDesignator)), 'h', 'H', [rfReplaceAll]);
end;

{ TdxSpreadSheetTimeDesignatorFormatProvider }

constructor TdxSpreadSheetTimeDesignatorFormatProvider.Create(const AAMText, APMText: TdxUnicodeString);
begin
  inherited Create;
  FAMText := AAMText;
  FPMText := APMText;
end;

procedure TdxSpreadSheetTimeDesignatorFormatProvider.Format(
  const AValue: Variant; const AValueInfo: TdxSpreadSheetNumberFormatValueInfo; var AResult: TdxSpreadSheetNumberFormatResult);
var
  ATempValue: Extended;
begin
  AResult.Reset;
  if dxIsNumericType(AValueInfo.ValueType) then
  begin
    ATempValue := AValue;
    if ATempValue < 0 then
    begin
      AResult.IsError := True;
      AResult.Text := '#';
    end
    else
      if ATempValue - Int(ATempValue) < 0.5 then
        AResult.Text := FAMText
      else
        AResult.Text := FPMText;
  end
  else
    AResult.Text := FAMText
end;

function TdxSpreadSheetTimeDesignatorFormatProvider.GetIs12HourTime: Boolean;
begin
  Result := True;
end;

function TdxSpreadSheetTimeDesignatorFormatProvider.GetIsDateTime: Boolean;
begin
  Result := True;
end;

{ TdxSpreadSheetCustomNumberFormatProviderFactory }

procedure TdxSpreadSheetCustomNumberFormatProviderFactory.Populate(
  const AFormatCode: TdxUnicodeString; AProviders: TdxSpreadSheetCustomNumberFormatProviders;
  ACustomProviderClass: TdxSpreadSheetCustomNumberFormatClass);
var
  AIndex: Integer;
  AMatch: TdxSpreadSheetNumberFormatPatternMatch;
  APattern: TdxSpreadSheetNumberFormatPattern;
  AProvider: TdxSpreadSheetCustomNumberFormatProvider;
  I: Integer;
begin
  if AFormatCode <> '' then
  try
    APattern := TdxSpreadSheetNumberFormatPattern.Create(GetRegExp);
    try
      AIndex := 1;
      APattern.Process(AFormatCode);
      for I := 0 to APattern.MatchCount - 1 do
      begin
        AMatch := APattern.Matches[I];
        AProvider := CreateProvider(AFormatCode, AMatch);
        if AProvider <> nil then
          AIndex := InsertFormat(AFormatCode, AProviders, AIndex, AMatch.Position, AMatch.Length, AProvider, ACustomProviderClass);
      end;
    finally
      APattern.Free;
    end;
  except
    // do nothing
  end;
end;

function TdxSpreadSheetCustomNumberFormatProviderFactory.CreateProvider(const AFormatCode: TdxUnicodeString;
  AMatch: TdxSpreadSheetNumberFormatPatternMatch): TdxSpreadSheetCustomNumberFormatProvider;
begin
  Result := nil;
end;

function TdxSpreadSheetCustomNumberFormatProviderFactory.InsertFormat(const AFormatCode: TdxUnicodeString;
  ATargetProviders: TdxSpreadSheetCustomNumberFormatProviders; AIndex, ANewIndex, AMatchLength: Integer;
  AProvider: TdxSpreadSheetCustomNumberFormatProvider; ACustomProviderClass: TdxSpreadSheetCustomNumberFormatClass): Integer;
begin
  if ATargetProviders.Count > 0 then
    ATargetProviders.Extract(ATargetProviders.Last).Free;
  if ANewIndex > AIndex then
    ATargetProviders.Add(ACustomProviderClass.Create(Copy(AFormatCode, AIndex, ANewIndex - AIndex)));
  ATargetProviders.Add(AProvider);
  if ANewIndex + AMatchLength <= Length(AFormatCode) then
    ATargetProviders.Add(ACustomProviderClass.Create(Copy(AFormatCode, ANewIndex + AMatchLength, MaxInt)));
  Result := ANewIndex + AMatchLength;
end;

{ TdxSpreadSheetCustomElapsedTimeFormatProviderFactory }

function TdxSpreadSheetCustomElapsedTimeFormatProviderFactory.CreateProvider(
  const AFormatCode: TdxUnicodeString; AMatch: TdxSpreadSheetNumberFormatPatternMatch): TdxSpreadSheetCustomNumberFormatProvider;
begin
  if AMatch.Length > 2 then
    Result := GetProviderClass.Create(DupeString('0', AMatch.Length - 2), Copy(AFormatCode, AMatch.Position + 1, AMatch.Length - 2))
  else
    Result := nil
end;

{ TdxSpreadSheetCustomFractionNumberFormatProviderFactory }

function TdxSpreadSheetCustomFractionNumberFormatProviderFactory.CreateProvider(
  const AFormatCode: TdxUnicodeString; AMatch: TdxSpreadSheetNumberFormatPatternMatch): TdxSpreadSheetCustomNumberFormatProvider;
var
  APosition: Integer;
  ATempText: TdxUnicodeString;
begin
  ATempText := Copy(AFormatCode, AMatch.Position, AMatch.Length);
  APosition := Pos('/', ATempText);
  if APosition > 0 then
    Result := DoCreateProvider(Copy(ATempText, 1, APosition - 1), Copy(ATempText, APosition + 1, MaxInt))
  else
    Result := nil;
end;

{ TdxSpreadSheetCurrencyAndLanguageInfoFormatProviderFactory }

function TdxSpreadSheetCurrencyAndLanguageInfoFormatProviderFactory.CreateProvider(const AFormatCode: TdxUnicodeString;
  AMatch: TdxSpreadSheetNumberFormatPatternMatch): TdxSpreadSheetCustomNumberFormatProvider;
var
  ADelimPos: Integer;
  ALangInfoString: TdxUnicodeString;
  ATempFormatCode: TdxUnicodeString;
  AValue: Integer;
begin
  ATempFormatCode := Copy(AFormatCode, AMatch.Position, AMatch.Length);

  ADelimPos := Pos('-', ATempFormatCode);
  ALangInfoString := Copy(ATempFormatCode, ADelimPos + 1, Length(ATempFormatCode) - ADelimPos - 1);
  if TryStrToInt('$' + ALangInfoString, AValue) then
    Result := TdxSpreadSheetCurrencyAndLanguageInfoFormatProvider.Create(Copy(ATempFormatCode, 3, ADelimPos - 3), AValue)
  else
    Result := nil;
end;

function TdxSpreadSheetCurrencyAndLanguageInfoFormatProviderFactory.GetRegExp: TdxUnicodeString;
begin
  Result := '\[\$[^-]*-([0-9a-fA-F]{1,8})\]';
end;

{ TdxSpreadSheetExplicitTextFormatProviderFactory }

function TdxSpreadSheetExplicitTextFormatProviderFactory.CreateProvider(const AFormatCode: TdxUnicodeString;
  AMatch: TdxSpreadSheetNumberFormatPatternMatch): TdxSpreadSheetCustomNumberFormatProvider;
var
  AText: TdxUnicodeString;
begin
  AText := GetText(AFormatCode, AMatch);
  if AText <> '' then
    Result := TdxSpreadSheetExplicitTextFormatProvider.Create(AText)
  else
    Result := nil;
end;

function TdxSpreadSheetExplicitTextFormatProviderFactory.GetRegExp: TdxUnicodeString;
begin
  Result := '"[^"]*"';
end;

function TdxSpreadSheetExplicitTextFormatProviderFactory.GetText(
  const AFormatCode: TdxUnicodeString; AMatch: TdxSpreadSheetNumberFormatPatternMatch): TdxUnicodeString;
begin
  Result := Copy(AFormatCode, AMatch.Position + 1, AMatch.Length - 2);
end;

{ TdxSpreadSheetExplicitSymbolFormatProviderFactory }

function TdxSpreadSheetExplicitSymbolFormatProviderFactory.GetRegExp: TdxUnicodeString;
begin
  Result := '\\.';
end;

function TdxSpreadSheetExplicitSymbolFormatProviderFactory.GetText(
  const AFormatCode: TdxUnicodeString; AMatch: TdxSpreadSheetNumberFormatPatternMatch): TdxUnicodeString;
begin
  Result := Copy(AFormatCode, AMatch.Position + 1, 1);
end;

{ TdxSpreadSheetFirstLetterOfMonthFormatProviderFactory }

function TdxSpreadSheetFirstLetterOfMonthFormatProviderFactory.CreateProvider(const AFormatCode: TdxUnicodeString;
  AMatch: TdxSpreadSheetNumberFormatPatternMatch): TdxSpreadSheetCustomNumberFormatProvider;
begin
  Result := TdxSpreadSheetFirstLetterOfMonthFormatProvider.Create;
end;

function TdxSpreadSheetFirstLetterOfMonthFormatProviderFactory.GetRegExp: TdxUnicodeString;
begin
  Result := 'm{5}';
end;

{ TdxSpreadSheetFractionExplicitDivisorFormatProviderFactory }

function TdxSpreadSheetFractionExplicitDivisorFormatProviderFactory.DoCreateProvider(
  const ADivident, ADivisor: TdxUnicodeString): TdxSpreadSheetCustomNumberFormatProvider;
var
  AValue: Integer;
begin
  if TryStrToInt(ADivisor, AValue) then
    Result := TdxSpreadSheetFractionExplicitDivisorFormatProvider.Create(ADivident, '0', Max(1, AValue))
  else
    Result := nil;
end;

function TdxSpreadSheetFractionExplicitDivisorFormatProviderFactory.GetRegExp: TdxUnicodeString;
begin
  Result := '([\?#0]+)\/(\d+)';
end;

{ TdxSpreadSheetFractionImplicitDivisorFormatProviderFactory }

function TdxSpreadSheetFractionImplicitDivisorFormatProviderFactory.DoCreateProvider(
  const ADivident, ADivisor: TdxUnicodeString): TdxSpreadSheetCustomNumberFormatProvider;
var
  AMaxDivisorValue: Integer;
begin
  AMaxDivisorValue := Trunc(IntPower(10, Length(ADivisor)));
  if AMaxDivisorValue > 1 then
    Result := TdxSpreadSheetFractionImplicitDivisorFormatProvider.Create(ADivident, ADivisor, AMaxDivisorValue)
  else
    Result := nil;
end;

function TdxSpreadSheetFractionImplicitDivisorFormatProviderFactory.GetRegExp: TdxUnicodeString;
begin
  Result := '([\?#0]+)\/([\?#0]+)';
end;

{ TdxSpreadSheetFullMonthFormatProviderFactory }

function TdxSpreadSheetFullMonthFormatProviderFactory.CreateProvider(const AFormatCode: TdxUnicodeString;
  AMatch: TdxSpreadSheetNumberFormatPatternMatch): TdxSpreadSheetCustomNumberFormatProvider;
begin
  Result := TdxSpreadSheetFullMonthFormatProvider.Create;
end;

function TdxSpreadSheetFullMonthFormatProviderFactory.GetRegExp: TdxUnicodeString;
begin
  Result := 'm{6,}';
end;

{ TdxSpreadSheetDayOfWeekFormatProviderFactory }

function TdxSpreadSheetDayOfWeekFormatProviderFactory.CreateProvider(
  const AFormatCode: TdxUnicodeString; AMatch: TdxSpreadSheetNumberFormatPatternMatch): TdxSpreadSheetCustomNumberFormatProvider;
begin
  Result := TdxSpreadSheetDayOfWeekFormatProvider.Create(Copy(AFormatCode, AMatch.Position, Min(4, AMatch.Length)));
end;

function TdxSpreadSheetDayOfWeekFormatProviderFactory.GetRegExp: TdxUnicodeString;
begin
  Result := 'd{3,}';
end;

{ TdxSpreadSheetElapsedHoursFormatProviderFactory }

function TdxSpreadSheetElapsedHoursFormatProviderFactory.GetProviderClass: TdxSpreadSheetCustomElapsedTimeFormatProviderClass;
begin
  Result := TdxSpreadSheetElapsedHoursFormatProvider;
end;

function TdxSpreadSheetElapsedHoursFormatProviderFactory.GetRegExp: TdxUnicodeString;
begin
  Result := '\[[h,H]+\]';
end;

{ TdxSpreadSheetElapsedMinutesFormatProviderFactory }

function TdxSpreadSheetElapsedMinutesFormatProviderFactory.GetProviderClass: TdxSpreadSheetCustomElapsedTimeFormatProviderClass;
begin
  Result := TdxSpreadSheetElapsedMinutesFormatProvider;
end;

function TdxSpreadSheetElapsedMinutesFormatProviderFactory.GetRegExp: TdxUnicodeString;
begin
  Result := '\[m+\]';
end;

{ TdxSpreadSheetElapsedSecondsFormatProviderFactory }

function TdxSpreadSheetElapsedSecondsFormatProviderFactory.GetProviderClass: TdxSpreadSheetCustomElapsedTimeFormatProviderClass;
begin
  Result := TdxSpreadSheetElapsedSecondsFormatProvider;
end;

function TdxSpreadSheetElapsedSecondsFormatProviderFactory.GetRegExp: TdxUnicodeString;
begin
  Result := '\[s+\]';
end;

{ TdxSpreadSheetMillisecondsNumberFormatProviderFactory }

function TdxSpreadSheetMillisecondsNumberFormatProviderFactory.CreateProvider(
  const AFormatCode: TdxUnicodeString; AMatch: TdxSpreadSheetNumberFormatPatternMatch): TdxSpreadSheetCustomNumberFormatProvider;
begin
  Result := TdxSpreadSheetMillisecondsFormatProvider.Create(Copy(AFormatCode, AMatch.Position + 2, AMatch.Length - 2));
end;

function TdxSpreadSheetMillisecondsNumberFormatProviderFactory.GetRegExp: TdxUnicodeString;
begin
  Result := '[s,S][s,S]\.0{1,3}';
end;

{ TdxSpreadSheetNumberWithTrailingThousandsSeparatorsFormatProviderFactory }

function TdxSpreadSheetNumberWithTrailingThousandsSeparatorsFormatProviderFactory.CreateProvider(
  const AFormatCode: TdxUnicodeString; AMatch: TdxSpreadSheetNumberFormatPatternMatch): TdxSpreadSheetCustomNumberFormatProvider;
var
  ADelimCount: Integer;
  ADelimPos: Integer;
  ATempFormatCode: TdxUnicodeString;
begin
  ATempFormatCode := Copy(AFormatCode, AMatch.Position, AMatch.Length);
  ADelimPos := Pos(',', ATempFormatCode);

  ADelimCount := 0;
  while (ADelimPos <= Length(ATempFormatCode)) and (ATempFormatCode[ADelimPos] = ',') do
  begin
    Inc(ADelimCount);
    Inc(ADelimPos);
  end;

  Result := TdxSpreadSheetNumberWithTrailingThousandsSeparatorsFormatProvider.Create(
    Copy(ATempFormatCode, 1, ADelimPos - 1), Trunc(IntPower(1000, ADelimCount)));
end;

function TdxSpreadSheetNumberWithTrailingThousandsSeparatorsFormatProviderFactory.GetRegExp: TdxUnicodeString;
begin
  Result := '([?#0,]*\.*[?#0]*)(,+)([^?#0]+|$)';
end;

{ TdxSpreadSheetSymbolFillNumberFormatProviderFactory }

function TdxSpreadSheetSymbolFillNumberFormatProviderFactory.GetRegExp: TdxUnicodeString;
begin
  Result := '\*.';
end;

function TdxSpreadSheetSymbolFillNumberFormatProviderFactory.GetText(
  const AFormatCode: TdxUnicodeString; AMatch: TdxSpreadSheetNumberFormatPatternMatch): TdxUnicodeString;
begin
  if AMatch.Length = 2 then
    Result := Copy(AFormatCode, AMatch.Position + 1, 1)
  else
    Result := EmptyStr;
end;

{ TdxSpreadSheetSymbolSpaceNumberFormatProviderFactory }

function TdxSpreadSheetSymbolSpaceNumberFormatProviderFactory.GetRegExp: TdxUnicodeString;
begin
  Result := '_.';
end;

function TdxSpreadSheetSymbolSpaceNumberFormatProviderFactory.GetText(
  const AFormatCode: TdxUnicodeString; AMatch: TdxSpreadSheetNumberFormatPatternMatch): TdxUnicodeString;
begin
  if AMatch.Length = 2 then
    Result := ' '
  else
    Result := EmptyStr;
end;

{ TdxSpreadSheetTimeDesignatorProviderFactory }

constructor TdxSpreadSheetTimeDesignatorProviderFactory.Create(const AAMText, APMText: TdxUnicodeString);
begin
  inherited Create;
  FAMText := AAMText;
  FPMText := APMText;
end;

function TdxSpreadSheetTimeDesignatorProviderFactory.CreateProvider(
  const AFormatCode: TdxUnicodeString; AMatch: TdxSpreadSheetNumberFormatPatternMatch): TdxSpreadSheetCustomNumberFormatProvider;
begin
  Result := TdxSpreadSheetTimeDesignatorFormatProvider.Create(FAMText, FPMText);
end;

function TdxSpreadSheetTimeDesignatorProviderFactory.GetRegExp: TdxUnicodeString;
begin
  Result := FAMText + '\/' + FPMText;
end;

{ TdxSpreadSheetSimpleNumberFormatProviderFactory }

function TdxSpreadSheetSimpleNumberFormatProviderFactory.CreateProvider(
  const AFormatCode: TdxUnicodeString; ID: Integer): TdxSpreadSheetConcatenateNumberFormatProvider;
var
  ALanguageInfoProvider: TdxSpreadSheetCurrencyAndLanguageInfoFormatProvider;
  ASystemDateTimeProvider: TdxSpreadSheetCustomDateTimeFormatProvider;
begin
  Result := TdxSpreadSheetConcatenateNumberFormatProvider.Create;
  if ID = 14 then
    Result.AddProvider(TdxSpreadSheetSystemShortDateFormatProvider.Create)
  else
  begin
    Result.AddProvider(TdxSpreadSheetFakeNumberFormatProvider.Create(AFormatCode));

    CreateFakeProviders(Result.Providers, TdxSpreadSheetExplicitTextFormatProviderFactory.Create);

    CreateFakeProviders(Result.Providers, TdxSpreadSheetCurrencyAndLanguageInfoFormatProviderFactory.Create);
    ALanguageInfoProvider := FindCurrencyAndLanguageInfoFormatProvider(Result.Providers);
    ASystemDateTimeProvider := CalculateSystemDateTimeProvider(ALanguageInfoProvider);
    if ASystemDateTimeProvider <> nil then
    begin
      Result.Providers.Clear;
      Result.Providers.Add(ASystemDateTimeProvider);
      Result.CalculateFormatInfo;
      Exit;
    end;

    Result.LCID := CalculateLCID(ALanguageInfoProvider);

    CreateFakeProviders(Result.Providers, TdxSpreadSheetExplicitSymbolFormatProviderFactory.Create);
    CreateFakeProviders(Result.Providers, TdxSpreadSheetSymbolSpaceNumberFormatProviderFactory.Create);
    CreateFakeProviders(Result.Providers, TdxSpreadSheetSymbolFillNumberFormatProviderFactory.Create);

    CreateFakeProviders(Result.Providers, TdxSpreadSheetTimeDesignatorProviderFactory.Create('AM', 'PM'));
    CreateFakeProviders(Result.Providers, TdxSpreadSheetTimeDesignatorProviderFactory.Create('am', 'pm'));
    CreateFakeProviders(Result.Providers, TdxSpreadSheetTimeDesignatorProviderFactory.Create('A', 'P'));
    CreateFakeProviders(Result.Providers, TdxSpreadSheetTimeDesignatorProviderFactory.Create('a', 'p'));

    CreateFakeProviders(Result.Providers, TdxSpreadSheetElapsedHoursFormatProviderFactory.Create);
    CreateFakeProviders(Result.Providers, TdxSpreadSheetElapsedMinutesFormatProviderFactory.Create);
    CreateFakeProviders(Result.Providers, TdxSpreadSheetElapsedSecondsFormatProviderFactory.Create);

    CreateFakeProviders(Result.Providers, TdxSpreadSheetFractionImplicitDivisorFormatProviderFactory.Create);
    CreateFakeProviders(Result.Providers, TdxSpreadSheetFractionExplicitDivisorFormatProviderFactory.Create);

    CreateFakeProviders(Result.Providers, TdxSpreadSheetDayOfWeekFormatProviderFactory.Create);
    CreateFakeProviders(Result.Providers, TdxSpreadSheetFullMonthFormatProviderFactory.Create);
    CreateFakeProviders(Result.Providers, TdxSpreadSheetFirstLetterOfMonthFormatProviderFactory.Create);
    ValidateFakeProviders(Result.Providers);
  end;
  Result.CalculateFormatInfo;

  if Result.IsDateTime then
  begin
    ProcessMonthAndMinutesSpecifiers(Result.Providers, Result.Is12HourTime);
    CreateProviders(Result.Providers, TdxSpreadSheetMillisecondsNumberFormatProviderFactory.Create);
  end
  else
  begin
    CreateProviders(Result.Providers, TdxSpreadSheetNumberWithTrailingThousandsSeparatorsFormatProviderFactory.Create);
    Result.ReplaceTrailingNumberFormats;
  end;
end;

procedure TdxSpreadSheetSimpleNumberFormatProviderFactory.CreateFakeProviders(
  AProviders: TdxSpreadSheetCustomNumberFormatProviders; AFactory: TdxSpreadSheetCustomNumberFormatProviderFactory);
begin
  CreateProviders(AProviders, AFactory, TdxSpreadSheetFakeNumberFormatProvider);
end;

procedure TdxSpreadSheetSimpleNumberFormatProviderFactory.CreateProviders(
  AProviders: TdxSpreadSheetCustomNumberFormatProviders; AFactory: TdxSpreadSheetCustomNumberFormatProviderFactory;
  ACustomProviderClass: TdxSpreadSheetCustomNumberFormatClass = nil);
var
  ANewProviders: TdxSpreadSheetCustomNumberFormatProviders;
  AProvider: TdxSpreadSheetCustomNumberFormatProvider;
  I, J: Integer;
begin
  try
    ANewProviders := TdxSpreadSheetCustomNumberFormatProviders.Create(False);
    try
      if ACustomProviderClass = nil then
        ACustomProviderClass := TdxSpreadSheetCustomNumberFormat;

      for I := AProviders.Count - 1 downto 0 do
      begin
        AProvider := AProviders[I];
        if AProvider is TdxSpreadSheetCustomNumberFormat then
        begin
          ANewProviders.Clear;
          AFactory.Populate(AProvider.FormatCode, ANewProviders, ACustomProviderClass);
          if ANewProviders.Count > 0 then
          begin
            AProviders.Delete(I);
            for J := 0 to ANewProviders.Count - 1 do
              AProviders.Insert(I + J, ANewProviders.Items[J]);
          end;
        end;
      end;
    finally
      ANewProviders.Free;
    end;
  finally
    AFactory.Free;
  end;
end;

procedure TdxSpreadSheetSimpleNumberFormatProviderFactory.ValidateFakeProviders(
  AProviders: TdxSpreadSheetCustomNumberFormatProviders);
var
  I: Integer;
begin
  for I := 0 to AProviders.Count - 1 do
    if AProviders[I] is TdxSpreadSheetFakeNumberFormatProvider then
      AProviders[I] := TdxSpreadSheetCustomNumberFormat.Create(AProviders[I].FormatCode);
end;

function TdxSpreadSheetSimpleNumberFormatProviderFactory.CalculateLCID(
  AProvider: TdxSpreadSheetCurrencyAndLanguageInfoFormatProvider): Cardinal;
var
  ALangID: Cardinal;
begin
  Result := 0;
  if AProvider <> nil then
  begin
    // Language info format (byte 3 is most significant byte):
    // Bytes 0,1: 16-bit Language ID (LID).
    // Byte 2: Calendar type. High bit indicates that input is parsed using specified calendar.
    // Byte 3: Number system type. High bit indicates that input is parsed using specified number system.
    ALangID := AProvider.LanguageInfo and $FFFF;
    if dxGetSubLangID(ALangID) <> LANG_NEUTRAL then
      Result := dxMakeLCID(ALangID, SORT_DEFAULT);
  end
end;

function TdxSpreadSheetSimpleNumberFormatProviderFactory.CalculateSystemDateTimeProvider(
  AProvider: TdxSpreadSheetCurrencyAndLanguageInfoFormatProvider): TdxSpreadSheetCustomDateTimeFormatProvider;
begin
  Result := nil;
  if AProvider <> nil then
    case AProvider.LanguageInfo of
      $F800:
        Result := TdxSpreadSheetSystemLongDateFormatProvider.Create;
      $F400:
        Result := TdxSpreadSheetSystemLongTimeFormatProvider.Create;
    end;
end;

function TdxSpreadSheetSimpleNumberFormatProviderFactory.FindCurrencyAndLanguageInfoFormatProvider(
  AProviders: TdxSpreadSheetCustomNumberFormatProviders): TdxSpreadSheetCurrencyAndLanguageInfoFormatProvider;
var
  I: Integer;
begin
  Result := nil;
  for I := 0 to AProviders.Count - 1 do
    if AProviders.Items[I] is TdxSpreadSheetCurrencyAndLanguageInfoFormatProvider then
    begin
      Result := TdxSpreadSheetCurrencyAndLanguageInfoFormatProvider(AProviders.Items[I]);
      Break;
    end;
end;

function TdxSpreadSheetSimpleNumberFormatProviderFactory.ProcessMinutes(
  const AFormatCode: TdxUnicodeString; AFrom, ATo, AStep: Integer; AIgnoreChar: TdxUnicodeChar): TdxUnicodeString;
const
  FormatDigits = 'mMdyhHs';
var
  AChar: TdxUnicodeChar;
  I: Integer;
begin
  Result := AFormatCode;
  I := AFrom;
  while True do
  begin
    AChar := Result[I];
    if (AChar <> AIgnoreChar) and (Pos(AChar, FormatDigits) > 0) then
    begin
      if AChar <> 'M' then
        Break;
      Result[I] := 'N';
    end;
    if I = ATo then
      Break;
    Inc(I, AStep);
  end;
end;

function TdxSpreadSheetSimpleNumberFormatProviderFactory.ProcessMonthAndMinutes(
  const AFormatCode: TdxUnicodeString): TdxUnicodeString;
var
  AHoursIndex, ASecondsIndex: Integer;
begin
  Result := StringReplace(AFormatCode, 'm', 'M', [rfReplaceAll]);
  Result := StringReplace(Result, 'H', 'h', [rfReplaceAll]);

  AHoursIndex := Pos('h', Result);
  ASecondsIndex := Pos('s', Result);

  if (AHoursIndex > 0) and (ASecondsIndex > 0) then
  begin
    if AHoursIndex < ASecondsIndex then
    begin
      Result := ProcessMinutes(Result, AHoursIndex, ASecondsIndex, 1, 'h');
      Result := ProcessMinutes(Result, ASecondsIndex - 1, AHoursIndex, -1, 's');
    end
    else
    begin
      Result := ProcessMinutes(Result, AHoursIndex, Length(Result), 1, 'h');
      Result := ProcessMinutes(Result, ASecondsIndex - 1, -1, -1, 's');
    end;
  end
  else
    if AHoursIndex > 0 then
      Result := ProcessMinutes(Result, AHoursIndex, Length(Result), 1, 'h')
    else
      if ASecondsIndex > 0 then
        Result := ProcessMinutes(Result, ASecondsIndex - 1, 0, -1, 's');
end;

procedure TdxSpreadSheetSimpleNumberFormatProviderFactory.ProcessMonthAndMinutesSpecifiers(
  AProviders: TdxSpreadSheetCustomNumberFormatProviders; AIs12HourTime: Boolean);

  procedure ApplyNewFormatCode(const ANewFormatCode: TdxUnicodeString; AProviders: TdxSpreadSheetCustomNumberFormatProviders);
  var
    AFormatCode: TdxUnicodeString;
    AIndex, I: Integer;
    AProvider: TdxSpreadSheetCustomNumberFormatProvider;
  begin
    AIndex := 1;
    for I := 0 to AProviders.Count - 1 do
    begin
      AProvider := AProviders[I];
      AFormatCode := AProvider.FormatCode;
      if AFormatCode <> '' then
      begin
        if AProvider is TdxSpreadSheetCustomNumberFormat then
          TdxSpreadSheetCustomNumberFormat(AProvider).InnerFormatCode := Copy(ANewFormatCode, AIndex, Length(AFormatCode));
      end;
      Inc(AIndex, Length(AFormatCode));
    end;
  end;

  function CreateFormatCode(AProviders: TdxSpreadSheetCustomNumberFormatProviders): TdxUnicodeString;
  var
    I: Integer;
  begin
    Result := EmptyStr;
    for I := 0 to AProviders.Count - 1 do
      Result := Result + AProviders[I].FormatCode;
  end;

var
  AFormatCode, ANewFormatCode: TdxUnicodeString;
begin
  AFormatCode := CreateFormatCode(AProviders);
  ANewFormatCode := ProcessMonthAndMinutes(AFormatCode);
  dxTestCheck(Length(AFormatCode) = Length(ANewFormatCode), 'ProcessMonthAndMinutesSpecifiers');
  ApplyNewFormatCode(ANewFormatCode, AProviders);
end;

end.
