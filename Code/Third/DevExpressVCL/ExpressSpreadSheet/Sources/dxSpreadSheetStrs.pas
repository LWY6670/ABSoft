{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressSpreadSheet                                       }
{                                                                    }
{           Copyright (c) 2001-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSSPREADSHEET CONTROL AND ALL    }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY. }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxSpreadSheetStrs;

{$I cxVer.Inc}

interface

uses
  dxCore, cxClasses, dxSpreadSheetTypes, dxSpreadSheetGraphics, Graphics;

resourcestring
  //
  sdxTrue   = 'True';
  sdxFalse  = 'False';

  // string errors
  serNameError    = '#NAME?';
  serNAError      = '#N/A';
  // all following messages must end with the "!" character
  serNullError    = '#NULL!';
  serDivZeroError = '#DIV/0!';
  serValueError   = '#VALUE!';
  serNumError     = '#NUM!';
  serRefError     = '#REF!';

  // compatibility function names
  sfnBetaDist         = 'BETADIST';
  sfnBetaInv          = 'BETAINV';
  sfnBinomDist        = 'BINOMDIST';
  sfnChiDist          = 'CHIDIST';
  sfnChiInv           = 'CHIINV';
  sfnCovar            = 'COVAR';
  sfnExponDist        = 'EXPONDIST';
  sfnGammaDist        = 'GAMMADIST';
  sfnGammaInv         = 'GAMMAINV';
  sfnHypgeomDist      = 'HYPGEOMDIST';
  sfnNormDist         = 'NORMDIST';
  sfnNormInv          = 'NORMINV';
  sfnNormSDist        = 'NORMSDIST';
  sfnNormSInv         = 'NORMSINV';
  sfnPercentile       = 'PERCENTILE';
  sfnPoisson          = 'POISSON';
  sfnQuartile         = 'QUARTILE';
  sfnRank             = 'RANK';
  sfnStDev            = 'STDEV';
  sfnStDevP           = 'STDEVP';
  sfnTDist            = 'TDIST';
  sfnTInv             = 'TINV';
  sfnVar              = 'VAR';
  sfnVarP             = 'VARP';
  sfnWeibull          = 'WEIBULL';

  // date and time function names
  sfnDate            = 'DATE';
  sfnDateValue       = 'DATEVALUE';
  sfnDay             = 'DAY';
  sfnDays            = 'DAYS';
  sfnDays360         = 'DAYS360';
  sfnEDate           = 'EDATE';
  sfnEOMonth         = 'EOMONTH';
  sfnHour            = 'HOUR';
  sfnIsoWeekNum      = 'ISOWEEKNUM';
  sfnMinute          = 'MINUTE';
  sfnMonth           = 'MONTH';
  sfnNow             = 'NOW';
  sfnSecond          = 'SECOND';
  sfnTime            = 'TIME';
  sfnTimeValue       = 'TIMEVALUE';
  sfnToday           = 'TODAY';
  sfnWeekDay         = 'WEEKDAY';
  sfnWeekNum         = 'WEEKNUM';
  sfnYear            = 'YEAR';
  sfnYearFrac        = 'YEARFRAC';

  // financial function names
  sfnFV              = 'FV';
  sfnIPMT            = 'IPMT';
  sfnNPer            = 'NPER';
  sfnNPV             = 'NPV';
  sfnPMT             = 'PMT';
  sfnPPMT            = 'PPMT';
  sfnPV              = 'PV';

  // information function names
  sfnIsBlank         = 'ISBLANK';
  sfnIsErr           = 'ISERR';
  sfnIsEven          = 'ISEVEN';
  sfnIsError         = 'ISERROR';
  sfnIsLogical       = 'ISLOGICAL';
  sfnIsNA            = 'ISNA';
  sfnIsNonText       = 'ISNONTEXT';
  sfnIsNumber        = 'ISNUMBER';
  sfnIsOdd           = 'ISODD';
  sfnIsText          = 'ISTEXT';
  sfnN               = 'N';
  sfnNA              = 'NA';

  // logical function names
  sfnAnd             = 'AND';
  sfnFalse           = 'FALSE';
  sfnIF              = 'IF';
  sfnIfError         = 'IFERROR';
  sfnIfNA            = 'IFNA';
  sfnNot             = 'NOT';
  sfnOr              = 'OR';
  sfnTrue            = 'TRUE';
  sfnXor             = 'XOR';

  // lookup and reference
  sfnAddress         = 'ADDRESS';
  sfnColumn          = 'COLUMN';
  sfnColumns         = 'COLUMNS';
  sfnHLookup         = 'HLOOKUP';
  sfnLookup          = 'LOOKUP';
  sfnMatch           = 'MATCH';
  sfnRow             = 'ROW';
  sfnRows            = 'ROWS';
  sfnFormulaText     = 'FORMULATEXT';
  sfnVLookup         = 'VLOOKUP';
  sfnTranspose       = 'TRANSPOSE';

  // math and trigonometry function names
  sfnAbs             = 'ABS';
  sfnAcos            = 'ACOS';
  sfnAcosh           = 'ACOSH';
  sfnAcot            = 'ACOT';
  sfnAcoth           = 'ACOTH';
  sfnAsin            = 'ASIN';
  sfnAsinh           = 'ASINH';
  sfnAtan            = 'ATAN';
  sfnAtan2           = 'ATAN2';
  sfnAtanh           = 'ATANH';
  sfnBase            = 'BASE';
  sfnCeiling         = 'CEILING';
  sfnCeiling_Math    = 'CEILING.MATH';
  sfnCeiling_Precise = 'CEILING.PRECISE';
  sfnConcatenate     = 'CONCATENATE';
  sfnCos             = 'COS';
  sfnCosh            = 'COSH';
  sfnCombin          = 'COMBIN';
  sfnCombinA         = 'COMBINA';
  sfnCot             = 'COT';
  sfnCoth            = 'COTH';
  sfnCsc             = 'CSC';
  sfnCsch            = 'CSCH';
  sfnDecimal         = 'DECIMAL';
  sfnDegrees         = 'DEGREES';
  sfnEven            = 'EVEN';
  sfnExp             = 'EXP';
  sfnFact            = 'FACT';
  sfnFactDouble      = 'FACTDOUBLE';
  sfnFloor           = 'FLOOR';
  sfnFloor_Math      = 'FLOOR.MATH';
  sfnFloor_Precise   = 'FLOOR.PRECISE';
  sfnInt             = 'INT';
  sfnIso_Ceiling     = 'ISO.CEILING';
  sfnLn              = 'LN';
  sfnLog             = 'LOG';
  sfnLog10           = 'LOG10';
  sfnMMult           = 'MMULT';
  sfnMod             = 'MOD';
  sfnMRound          = 'MROUND';
  sfnOdd             = 'ODD';
  sfnPi              = 'PI';
  sfnPower           = 'POWER';
  sfnProduct         = 'PRODUCT';
  sfnQuotient        = 'QUOTIENT';
  sfnRadians         = 'RADIANS';
  sfnRand            = 'RAND';
  sfnRandBetween     = 'RANDBETWEEN';
  sfnRound           = 'ROUND';
  sfnRoundDown       = 'ROUNDDOWN';
  sfnRoundUp         = 'ROUNDUP';
  sfnSec             = 'SEC';
  sfnSech            = 'SECH';
  sfnSign            = 'SIGN';
  sfnSin             = 'SIN';
  sfnSinh            = 'SINH';
  sfnSqrt            = 'SQRT';
  sfnSqrtPi          = 'SQRTPI';
  sfnSum             = 'SUM';
  sfnSumIF           = 'SUMIF';
  sfnSumSQ           = 'SUMSQ';
  sfnTan             = 'TAN';
  sfnTanh            = 'TANH';

  // statistical function names
  sfnAveDev           = 'AVEDEV';
  sfnAverage          = 'AVERAGE';
  sfnAverageA         = 'AVERAGEA';
  sfnAverageIF        = 'AVERAGEIF';
  sfnBeta_Dist        = 'BETA.DIST';
  sfnBeta_Inv         = 'BETA.INV';
  sfnBinom_Dist       = 'BINOM.DIST';
  sfnBinom_Dist_Range = 'BINOM.DIST.RANGE';
  sfnChiSQ_Dist       = 'CHISQ.DIST';
  sfnChiSQ_Dist_RT    = 'CHISQ.DIST.RT';
  sfnChiSQ_Inv        = 'CHISQ.INV';
  sfnChiSQ_Inv_RT     = 'CHISQ.INV.RT';
  sfnCorrel           = 'CORREL';
  sfnCount            = 'COUNT';
  sfnCounta           = 'COUNTA';
  sfnCountBlank       = 'COUNTBLANK';
  sfnCountIF          = 'COUNTIF';
  sfnCovariance_P     = 'COVARIANCE.P';
  sfnCovariance_S     = 'COVARIANCE.S';
  sfnDevSQ            = 'DEVSQ';
  sfnExpon_Dist       = 'EXPON.DIST';
  sfnForecast         = 'FORECAST';
  sfnGamma            = 'GAMMA';
  sfnGamma_Dist       = 'GAMMA.DIST';
  sfnGamma_Inv        = 'GAMMA.INV';
  sfnGammaLn          = 'GAMMALN';
  sfnGammaLn_Precise  = 'GAMMALN.PRECISE';
  sfnGauss            = 'GAUSS';
  sfnGeomean          = 'GEOMEAN';
  sfnHypgeom_Dist     = 'HYPGEOM.DIST';
  sfnIntercept        = 'INTERCEPT';
  sfnLarge            = 'LARGE';
  sfnMax              = 'MAX';
  sfnMaxA             = 'MAXA';
  sfnMedian           = 'MEDIAN';
  sfnMin              = 'MIN';
  sfnMinA             = 'MINA';
  sfnNorm_Dist        = 'NORM.DIST';
  sfnNorm_Inv         = 'NORM.INV';
  sfnNorm_S_Dist      = 'NORM.S.DIST';
  sfnNorm_S_Inv       = 'NORM.S.INV';
  sfnPearson          = 'PEARSON';
  sfnPercentile_Inc   = 'PERCENTILE.INC';
  sfnPercentile_Exc   = 'PERCENTILE.EXC';
  sfnPermut           = 'PERMUT';
  sfnPoisson_Dist     = 'POISSON.DIST';
  sfnQuartile_Inc     = 'QUARTILE.INC';
  sfnQuartile_Exc     = 'QUARTILE.EXC';
  sfnRank_Avg         = 'RANK.AVG';
  sfnRank_Eq          = 'RANK.EQ';
  sfnRSQ              = 'RSQ';
  sfnSkew             = 'SKEW';
  sfnSkew_P           = 'SKEW.P';
  sfnSlope            = 'SLOPE';
  sfnSmall            = 'SMALL';
  sfnStandardize      = 'STANDARDIZE';
  sfnStDev_S          = 'STDEV.S';
  sfnStDev_P          = 'STDEV.P';
  sfnStDevA           = 'STDEVA';
  sfnStDevPA          = 'STDEVPA';
  sfnSTEYX            = 'STEYX';
  sfnSubTotal         = 'SUBTOTAL';
  sfnT_Dist           = 'T.DIST';
  sfnT_Dist_2T        = 'T.DIST.2T';
  sfnT_Dist_RT        = 'T.DIST.RT';
  sfnT_Inv            = 'T.INV';
  sfnT_Inv_2T         = 'T.INV.2T';
  sfnVar_P            = 'VAR.P';
  sfnVar_S            = 'VAR.S';
  sfnVarA             = 'VARA';
  sfnVarPA            = 'VARPA';
  sfnWeibull_Dist     = 'WEIBULL.DIST';

  // text function names
  sfnChar       = 'CHAR';
  sfnClean      = 'CLEAN';
  sfnCode       = 'CODE';
  sfnDollar     = 'DOLLAR';
  sfnExact      = 'EXACT';
  sfnFind       = 'FIND';
  sfnFixed      = 'FIXED';
  sfnLeft       = 'LEFT';
  sfnLen        = 'LEN';
  sfnLower      = 'LOWER';
  sfnMid        = 'MID';
  sfnProper     = 'PROPER';
  sfnReplace    = 'REPLACE';
  sfnRept       = 'REPT';
  sfnRight      = 'RIGHT';
  sfnSearch     = 'SEARCH';
  sfnSubstitute = 'SUBSTITUTE';
  sfnT          = 'T';
  sfnText       = 'TEXT';
  sfnTrim       = 'TRIM';
  sfnTrunc      = 'TRUNC';
  sfnUpper      = 'UPPER';
  sfnValue      = 'VALUE';

  sdxDefaultSheetCaption = 'Sheet%d';

  sdxErrorCannotRenameSheet = 'Cannot rename a sheet to the same name as existing sheet.';
  sdxErrorCannotChangePartOfArray = 'You cannot change part of an array.';
  sdxErrorCellProtected = 'The cell that you are trying to change is protected and therefore read-only.';
  sdxErrorColorValueIsNotSpecified = 'Color value is not specified';
  sdxErrorDefinedNameAlreadyExists = 'Duplicate name "%s" is found';
  sdxErrorDocumentIsCorrupted = 'Document is corrupted';
  sdxErrorExternalLinkAlreadyExists = 'An external link named "%s" already exists';
  sdxErrorFileCannotBeFoundInPackage = 'File "%s" cannot be found in the package';
  sdxErrorFileIsCorrupted = 'File "%s" is corrupted';
  sdxErrorInternal = 'Internal error: "%s"';
  sdxErrorInvalidAnchorCell = 'The "%s" cell cannot be used as an anchor';
  sdxErrorInvalidAnchorDefinition = 'Invalid anchor definition';
  sdxErrorInvalidColor = 'The "%s" color value is not supported';
  sdxErrorInvalidColorIndex = 'The "%d" color index is invalid';
  sdxErrorInvalidColumnIndex = 'The "%s" column index is invalid';
  sdxErrorInvalidDocumentType = 'Unsupported document type';
  sdxErrorInvalidFormatCodeID = 'The "%d" format code ID is invalid';
  sdxErrorInvalidFormula = 'The "%s" formula is invalid';
  sdxErrorInvalidReference = 'The "%s" reference at position %d is invalid';
  sdxErrorInvalidRelationshipId = 'The "%s" relationship ID is invalid';
  sdxErrorInvalidSelection = 'This command cannot be used on multiple selections or an empty selection';
  sdxErrorInvalidSharedStringIndex = 'The "%d" shared string index is invalid';
  sdxErrorInvalidSheetId = 'Sheet with ID="%s" cannot be found';
  sdxErrorInvalidStyleIndex = 'The "%d" style index is invalid';
  sdxErrorPictureCannotBeFound = 'Picture "%s" cannot be found';
  sdxErrorUnsupportedDocumentFormat = 'Unsupported document format';
  sdxErrorUnsupportedSheetType = 'Unsupported sheet type';

  sdxErrorCellAlreadyExists = 'Cell with "%d" index already exists';
  sdxErrorPossibleDataLoss =
    'To prevent possible loss of data, shifting non-blank cells off the worksheet has been canceled. ' +
    'Select another location in which to insert new cells, or delete data from the end of your worksheet.';
  sdxErrorCannotMoveBecauseOfMergedCells = 'This operation will cause some merged cells to unmerge.';
  sdxUnmergeCellsConfirmation = 'This operation will cause some merged cells to unmerge. Do you wish to continue?';

  //
  sdxErrorCircularMessage = 'Warning' + #13#10 +
    'One or more formulas contain a circular reference and may not calculate correctly. Circular references are any' +
    'references within a formula that depend upon the results of that same formula. For example, a cell that refers' +
    'to its own value or a cell that refers to another cell which depends on the original cell''s value both contain' +
    'circular references.' +
    #13#10#13#10 +
    'Click OK to create a circular reference and continue.';

  sdxErrorCircularPathPrefix = 'The ';

  // Popup Menu
  sdxBuiltInPopupMenuBringToFront = 'Bring to F&ront';
  sdxBuiltInPopupMenuClearContents = 'Clear Co&ntents';
  sdxBuiltInPopupMenuCopy = '&Copy';
  sdxBuiltInPopupMenuCustomizeObject = 'C&ustomize Object...';
  sdxBuiltInPopupMenuCut = 'Cu&t';
  sdxBuiltInPopupMenuDelete = '&Delete';
  sdxBuiltInPopupMenuDeleteDialog = '&Delete...';
  sdxBuiltInPopupMenuFormatCells = '&Format Cells...';
  sdxBuiltInPopupMenuHide = '&Hide';
  sdxBuiltInPopupMenuInsert = '&Insert';
  sdxBuiltInPopupMenuInsertDialog = '&Insert...';
  sdxBuiltInPopupMenuMergeCells = '&Merge Cells';
  sdxBuiltInPopupMenuPaste = '&Paste';
  sdxBuiltInPopupMenuRename = '&Rename';
  sdxBuiltInPopupMenuSendToBack = 'Send to Bac&k';
  sdxBuiltInPopupMenuSplitCells = 'U&nmerge Cells';
  sdxBuiltInPopupMenuUnhide = '&Unhide';
  sdxBuiltInPopupMenuUnhideDialog = '&Unhide...';

  // Rename Sheet Dialog
  sdxRenameDialogCaption = 'Rename Sheet';
  sdxRenameDialogSheetName = 'Sheet Name:';

  // File Dialog
  sdxFileDialogAllSupported = 'All Supported';

  //
  sdxActionCellEditing = 'Cell Editing';
  sdxActionCellsMerge = 'Cells Merge';
  sdxActionClearCells = 'Clear Cell(s)';
  sdxActionDeleteCells = 'Delete Cells';
  sdxActionInsertCells = 'Insert Cells';
  sdxActionSortCells = 'Sort Cells';
  sdxActionFormatCells = 'Format Cells';
  sdxActionCutCells = 'Cut Cells';
  sdxActionPasteCells = 'Paste Cells';
  sdxActionChangeRowColumn = 'Change Row/Column';
  sdxActionChangeContainer = 'Change Container';

const
  dxSpreadSheetGeneralNumberFormat = 'GENERAL'; // don't localize
  dxAbsoluteReferenceChar: TdxUnicodeString = '$';
  dxReferenceLeftParenthesis: TdxUnicodeString = '[';
  dxReferenceRightParenthesis: TdxUnicodeString = ']';
  dxRCRowReferenceChar: TdxUnicodeString = 'R';
  dxRCColumnReferenceChar: TdxUnicodeString = 'C';

  dxRefSeparator: TdxUnicodeString = '!';
  dxAreaSeparator: TdxUnicodeString = ':';

  dxStringMarkChar: TdxUnicodeString = '"';
  dxStringMarkChar2: TdxUnicodeString = '''';

  dxExponentChar:  TdxUnicodeString = 'E';

  dxLeftParenthesis: TdxUnicodeString = '(';
  dxRightParenthesis: TdxUnicodeString = ')';

  dxLeftArrayParenthesis: TdxUnicodeString = '{';
  dxRightArrayParenthesis: TdxUnicodeString = '}';

  dxSignNegative: TdxUnicodeString = '-';
  dxSignPositive: TdxUnicodeString = '-';

  dxErrorPrefix: TdxUnicodeString = '#';

  dxDefaultOperations: TdxSpreadSheetOperationStrings = (
    '+', '-', '*', '/', '^', '&', '<', '<=', '=', '>=', '>', '<>', ' ', ',', ':', '', '-', '%', ''
  );

  dxBoolToString: array[Boolean] of TdxUnicodeString = ('FALSE', 'TRUE');

implementation

uses
  dxSpreadSheetDialogStrs;

procedure AddSpreadSheetResourceStringNames(AProduct: TdxProductResourceStrings);
begin
  AddSpreadSheetDialogsResourceStringNames(AProduct);
  AProduct.Add('sdxTrue', @sdxTrue);
  AProduct.Add('sdxFalse', @sdxFalse);
  AProduct.Add('serNameError', @serNameError);
  AProduct.Add('serNAError', @serNAError);
  AProduct.Add('serNullError', @serNullError);
  AProduct.Add('serDivZeroError', @serDivZeroError);
  AProduct.Add('serValueError', @serValueError);
  AProduct.Add('serNumError', @serNumError);
  AProduct.Add('serRefError', @serRefError);
  AProduct.Add('sfnBetaDist', @sfnBetaDist);
  AProduct.Add('sfnBetaInv', @sfnBetaInv);
  AProduct.Add('sfnBinomDist', @sfnBinomDist);
  AProduct.Add('sfnChiDist', @sfnChiDist);
  AProduct.Add('sfnChiInv', @sfnChiInv);
  AProduct.Add('sfnCovar', @sfnCovar);
  AProduct.Add('sfnExponDist', @sfnExponDist);
  AProduct.Add('sfnGammaDist', @sfnGammaDist);
  AProduct.Add('sfnGammaInv', @sfnGammaInv);
  AProduct.Add('sfnHypgeomDist', @sfnHypgeomDist);
  AProduct.Add('sfnNormDist', @sfnNormDist);
  AProduct.Add('sfnNormInv', @sfnNormInv);
  AProduct.Add('sfnNormSDist', @sfnNormSDist);
  AProduct.Add('sfnNormSInv', @sfnNormSInv);
  AProduct.Add('sfnPercentile', @sfnPercentile);
  AProduct.Add('sfnPoisson', @sfnPoisson);
  AProduct.Add('sfnQuartile', @sfnQuartile);
  AProduct.Add('sfnRank', @sfnRank);
  AProduct.Add('sfnStDev', @sfnStDev);
  AProduct.Add('sfnStDevP', @sfnStDevP);
  AProduct.Add('sfnTDist', @sfnTDist);
  AProduct.Add('sfnTInv', @sfnTInv);
  AProduct.Add('sfnVar', @sfnVar);
  AProduct.Add('sfnVarP', @sfnVarP);
  AProduct.Add('sfnWeibull', @sfnWeibull);
  AProduct.Add('sfnDate', @sfnDate);
  AProduct.Add('sfnDateValue', @sfnDateValue);
  AProduct.Add('sfnDay', @sfnDay);
  AProduct.Add('sfnDays', @sfnDays);
  AProduct.Add('sfnDays360', @sfnDays360);
  AProduct.Add('sfnEDate', @sfnEDate);
  AProduct.Add('sfnEOMonth', @sfnEOMonth);
  AProduct.Add('sfnHour', @sfnHour);
  AProduct.Add('sfnIsoWeekNum', @sfnIsoWeekNum);
  AProduct.Add('sfnMinute', @sfnMinute);
  AProduct.Add('sfnMonth', @sfnMonth);
  AProduct.Add('sfnNow', @sfnNow);
  AProduct.Add('sfnSecond', @sfnSecond);
  AProduct.Add('sfnTime', @sfnTime);
  AProduct.Add('sfnTimeValue', @sfnTimeValue);
  AProduct.Add('sfnToday', @sfnToday);
  AProduct.Add('sfnWeekDay', @sfnWeekDay);
  AProduct.Add('sfnWeekNum', @sfnWeekNum);
  AProduct.Add('sfnYear', @sfnYear);
  AProduct.Add('sfnYearFrac', @sfnYearFrac);
  AProduct.Add('sfnFV', @sfnFV);
  AProduct.Add('sfnIPMT', @sfnIPMT);
  AProduct.Add('sfnNPer', @sfnNPer);
  AProduct.Add('sfnNPV', @sfnNPV);
  AProduct.Add('sfnPMT', @sfnPMT);
  AProduct.Add('sfnPPMT', @sfnPPMT);
  AProduct.Add('sfnPV', @sfnPV);
  AProduct.Add('sfnIsBlank', @sfnIsBlank);
  AProduct.Add('sfnIsErr', @sfnIsErr);
  AProduct.Add('sfnIsEven', @sfnIsEven);
  AProduct.Add('sfnIsError', @sfnIsError);
  AProduct.Add('sfnIsLogical', @sfnIsLogical);
  AProduct.Add('sfnIsNA', @sfnIsNA);
  AProduct.Add('sfnIsNonText', @sfnIsNonText);
  AProduct.Add('sfnIsNumber', @sfnIsNumber);
  AProduct.Add('sfnIsOdd', @sfnIsOdd);
  AProduct.Add('sfnIsText', @sfnIsText);
  AProduct.Add('sfnN', @sfnN);
  AProduct.Add('sfnNA', @sfnNA);
  AProduct.Add('sfnAnd', @sfnAnd);
  AProduct.Add('sfnFalse', @sfnFalse);
  AProduct.Add('sfnIF', @sfnIF);
  AProduct.Add('sfnIfError', @sfnIfError);
  AProduct.Add('sfnIfNA', @sfnIfNA);
  AProduct.Add('sfnNot', @sfnNot);
  AProduct.Add('sfnOr', @sfnOr);
  AProduct.Add('sfnTrue', @sfnTrue);
  AProduct.Add('sfnXor', @sfnXor);
  AProduct.Add('sfnAddress', @sfnAddress);
  AProduct.Add('sfnColumn', @sfnColumn);
  AProduct.Add('sfnColumns', @sfnColumns);
  AProduct.Add('sfnHLookup', @sfnHLookup);
  AProduct.Add('sfnLookup', @sfnLookup);
  AProduct.Add('sfnMatch', @sfnMatch);
  AProduct.Add('sfnRow', @sfnRow);
  AProduct.Add('sfnRows', @sfnRows);
  AProduct.Add('sfnFormulaText', @sfnFormulaText);
  AProduct.Add('sfnVLookup', @sfnVLookup);
  AProduct.Add('sfnTranspose', @sfnTranspose);
  AProduct.Add('sfnAbs', @sfnAbs);
  AProduct.Add('sfnAcos', @sfnAcos);
  AProduct.Add('sfnAcosh', @sfnAcosh);
  AProduct.Add('sfnAcot', @sfnAcot);
  AProduct.Add('sfnAcoth', @sfnAcoth);
  AProduct.Add('sfnAsin', @sfnAsin);
  AProduct.Add('sfnAsinh', @sfnAsinh);
  AProduct.Add('sfnAtan', @sfnAtan);
  AProduct.Add('sfnAtan2', @sfnAtan2);
  AProduct.Add('sfnAtanh', @sfnAtanh);
  AProduct.Add('sfnBase', @sfnBase);
  AProduct.Add('sfnCeiling', @sfnCeiling);
  AProduct.Add('sfnCeiling_Math', @sfnCeiling_Math);
  AProduct.Add('sfnCeiling_Precise', @sfnCeiling_Precise);
  AProduct.Add('sfnConcatenate', @sfnConcatenate);
  AProduct.Add('sfnCos', @sfnCos);
  AProduct.Add('sfnCosh', @sfnCosh);
  AProduct.Add('sfnCombin', @sfnCombin);
  AProduct.Add('sfnCombinA', @sfnCombinA);
  AProduct.Add('sfnCot', @sfnCot);
  AProduct.Add('sfnCoth', @sfnCoth);
  AProduct.Add('sfnCsc', @sfnCsc);
  AProduct.Add('sfnCsch', @sfnCsch);
  AProduct.Add('sfnDecimal', @sfnDecimal);
  AProduct.Add('sfnDegrees', @sfnDegrees);
  AProduct.Add('sfnEven', @sfnEven);
  AProduct.Add('sfnExp', @sfnExp);
  AProduct.Add('sfnFact', @sfnFact);
  AProduct.Add('sfnFactDouble', @sfnFactDouble);
  AProduct.Add('sfnFloor', @sfnFloor);
  AProduct.Add('sfnFloor_Math', @sfnFloor_Math);
  AProduct.Add('sfnFloor_Precise', @sfnFloor_Precise);
  AProduct.Add('sfnInt', @sfnInt);
  AProduct.Add('sfnIso_Ceiling', @sfnIso_Ceiling);
  AProduct.Add('sfnLn', @sfnLn);
  AProduct.Add('sfnLog', @sfnLog);
  AProduct.Add('sfnLog10', @sfnLog10);
  AProduct.Add('sfnMMult', @sfnMMult);
  AProduct.Add('sfnMod', @sfnMod);
  AProduct.Add('sfnMRound', @sfnMRound);
  AProduct.Add('sfnOdd', @sfnOdd);
  AProduct.Add('sfnPi', @sfnPi);
  AProduct.Add('sfnPower', @sfnPower);
  AProduct.Add('sfnProduct', @sfnProduct);
  AProduct.Add('sfnQuotient', @sfnQuotient);
  AProduct.Add('sfnRadians', @sfnRadians);
  AProduct.Add('sfnRand', @sfnRand);
  AProduct.Add('sfnRandBetween', @sfnRandBetween);
  AProduct.Add('sfnRound', @sfnRound);
  AProduct.Add('sfnRoundDown', @sfnRoundDown);
  AProduct.Add('sfnRoundUp', @sfnRoundUp);
  AProduct.Add('sfnSec', @sfnSec);
  AProduct.Add('sfnSech', @sfnSech);
  AProduct.Add('sfnSign', @sfnSign);
  AProduct.Add('sfnSin', @sfnSin);
  AProduct.Add('sfnSinh', @sfnSinh);
  AProduct.Add('sfnSqrt', @sfnSqrt);
  AProduct.Add('sfnSqrtPi', @sfnSqrtPi);
  AProduct.Add('sfnSum', @sfnSum);
  AProduct.Add('sfnSumIF', @sfnSumIF);
  AProduct.Add('sfnSumSQ', @sfnSumSQ);
  AProduct.Add('sfnTan', @sfnTan);
  AProduct.Add('sfnTanh', @sfnTanh);
  AProduct.Add('sfnAveDev', @sfnAveDev);
  AProduct.Add('sfnAverage', @sfnAverage);
  AProduct.Add('sfnAverageA', @sfnAverageA);
  AProduct.Add('sfnAverageIF', @sfnAverageIF);
  AProduct.Add('sfnBeta_Dist', @sfnBeta_Dist);
  AProduct.Add('sfnBeta_Inv', @sfnBeta_Inv);
  AProduct.Add('sfnBinom_Dist', @sfnBinom_Dist);
  AProduct.Add('sfnBinom_Dist_Range', @sfnBinom_Dist_Range);
  AProduct.Add('sfnChiSQ_Dist', @sfnChiSQ_Dist);
  AProduct.Add('sfnChiSQ_Dist_RT', @sfnChiSQ_Dist_RT);
  AProduct.Add('sfnChiSQ_Inv', @sfnChiSQ_Inv);
  AProduct.Add('sfnChiSQ_Inv_RT', @sfnChiSQ_Inv_RT);
  AProduct.Add('sfnCorrel', @sfnCorrel);
  AProduct.Add('sfnCount', @sfnCount);
  AProduct.Add('sfnCounta', @sfnCounta);
  AProduct.Add('sfnCountBlank', @sfnCountBlank);
  AProduct.Add('sfnCountIF', @sfnCountIF);
  AProduct.Add('sfnCovariance_P', @sfnCovariance_P);
  AProduct.Add('sfnCovariance_S', @sfnCovariance_S);
  AProduct.Add('sfnDevSQ', @sfnDevSQ);
  AProduct.Add('sfnExpon_Dist', @sfnExpon_Dist);
  AProduct.Add('sfnForecast', @sfnForecast);
  AProduct.Add('sfnGamma', @sfnGamma);
  AProduct.Add('sfnGamma_Dist', @sfnGamma_Dist);
  AProduct.Add('sfnGamma_Inv', @sfnGamma_Inv);
  AProduct.Add('sfnGammaLn', @sfnGammaLn);
  AProduct.Add('sfnGammaLn_Precise', @sfnGammaLn_Precise);
  AProduct.Add('sfnGauss', @sfnGauss);
  AProduct.Add('sfnGeomean', @sfnGeomean);
  AProduct.Add('sfnHypgeom_Dist', @sfnHypgeom_Dist);
  AProduct.Add('sfnIntercept', @sfnIntercept);
  AProduct.Add('sfnLarge', @sfnLarge);
  AProduct.Add('sfnMax', @sfnMax);
  AProduct.Add('sfnMaxA', @sfnMaxA);
  AProduct.Add('sfnMedian', @sfnMedian);
  AProduct.Add('sfnMin', @sfnMin);
  AProduct.Add('sfnMinA', @sfnMinA);
  AProduct.Add('sfnNorm_Dist', @sfnNorm_Dist);
  AProduct.Add('sfnNorm_Inv', @sfnNorm_Inv);
  AProduct.Add('sfnNorm_S_Dist', @sfnNorm_S_Dist);
  AProduct.Add('sfnNorm_S_Inv', @sfnNorm_S_Inv);
  AProduct.Add('sfnPearson', @sfnPearson);
  AProduct.Add('sfnPercentile_Inc', @sfnPercentile_Inc);
  AProduct.Add('sfnPercentile_Exc', @sfnPercentile_Exc);
  AProduct.Add('sfnPermut', @sfnPermut);
  AProduct.Add('sfnPoisson_Dist', @sfnPoisson_Dist);
  AProduct.Add('sfnQuartile_Inc', @sfnQuartile_Inc);
  AProduct.Add('sfnQuartile_Exc', @sfnQuartile_Exc);
  AProduct.Add('sfnRank_Avg', @sfnRank_Avg);
  AProduct.Add('sfnRank_Eq', @sfnRank_Eq);
  AProduct.Add('sfnRSQ', @sfnRSQ);
  AProduct.Add('sfnSkew', @sfnSkew);
  AProduct.Add('sfnSkew_P', @sfnSkew_P);
  AProduct.Add('sfnSlope', @sfnSlope);
  AProduct.Add('sfnSmall', @sfnSmall);
  AProduct.Add('sfnStandardize', @sfnStandardize);
  AProduct.Add('sfnStDev_S', @sfnStDev_S);
  AProduct.Add('sfnStDev_P', @sfnStDev_P);
  AProduct.Add('sfnStDevA', @sfnStDevA);
  AProduct.Add('sfnStDevPA', @sfnStDevPA);
  AProduct.Add('sfnSTEYX', @sfnSTEYX);
  AProduct.Add('sfnSubTotal', @sfnSubTotal);
  AProduct.Add('sfnT_Dist', @sfnT_Dist);
  AProduct.Add('sfnT_Dist_2T', @sfnT_Dist_2T);
  AProduct.Add('sfnT_Dist_RT', @sfnT_Dist_RT);
  AProduct.Add('sfnT_Inv', @sfnT_Inv);
  AProduct.Add('sfnT_Inv_2T', @sfnT_Inv_2T);
  AProduct.Add('sfnVar_P', @sfnVar_P);
  AProduct.Add('sfnVar_S', @sfnVar_S);
  AProduct.Add('sfnVarA', @sfnVarA);
  AProduct.Add('sfnVarPA', @sfnVarPA);
  AProduct.Add('sfnWeibull_Dist', @sfnWeibull_Dist);
  AProduct.Add('sfnChar', @sfnChar);
  AProduct.Add('sfnClean', @sfnClean);
  AProduct.Add('sfnCode', @sfnCode);
  AProduct.Add('sfnDollar', @sfnDollar);
  AProduct.Add('sfnExact', @sfnExact);
  AProduct.Add('sfnFind', @sfnFind);
  AProduct.Add('sfnFixed', @sfnFixed);
  AProduct.Add('sfnLeft', @sfnLeft);
  AProduct.Add('sfnLen', @sfnLen);
  AProduct.Add('sfnLower', @sfnLower);
  AProduct.Add('sfnMid', @sfnMid);
  AProduct.Add('sfnProper', @sfnProper);
  AProduct.Add('sfnReplace', @sfnReplace);
  AProduct.Add('sfnRept', @sfnRept);
  AProduct.Add('sfnRight', @sfnRight);
  AProduct.Add('sfnSearch', @sfnSearch);
  AProduct.Add('sfnSubstitute', @sfnSubstitute);
  AProduct.Add('sfnT', @sfnT);
  AProduct.Add('sfnText', @sfnText);
  AProduct.Add('sfnTrim', @sfnTrim);
  AProduct.Add('sfnTrunc', @sfnTrunc);
  AProduct.Add('sfnUpper', @sfnUpper);
  AProduct.Add('sfnValue', @sfnValue);
  AProduct.Add('sdxDefaultSheetCaption', @sdxDefaultSheetCaption);

  AProduct.Add('sdxErrorCannotRenameSheet', @sdxErrorCannotRenameSheet);
  AProduct.Add('sdxErrorCannotChangePartOfArray', @sdxErrorCannotChangePartOfArray);
  AProduct.Add('sdxErrorCellProtected', @sdxErrorCellProtected);
  AProduct.Add('sdxErrorColorValueIsNotSpecified', @sdxErrorColorValueIsNotSpecified);
  AProduct.Add('sdxErrorDefinedNameAlreadyExists', @sdxErrorDefinedNameAlreadyExists);
  AProduct.Add('sdxErrorDocumentIsCorrupted', @sdxErrorDocumentIsCorrupted);
  AProduct.Add('sdxErrorExternalLinkAlreadyExists', @sdxErrorExternalLinkAlreadyExists);
  AProduct.Add('sdxErrorFileCannotBeFoundInPackage', @sdxErrorFileCannotBeFoundInPackage);
  AProduct.Add('sdxErrorFileIsCorrupted', @sdxErrorFileIsCorrupted);
  AProduct.Add('sdxErrorInternal', @sdxErrorInternal);
  AProduct.Add('sdxErrorInvalidAnchorCell', @sdxErrorInvalidAnchorCell);
  AProduct.Add('sdxErrorInvalidAnchorDefinition', @sdxErrorInvalidAnchorDefinition);
  AProduct.Add('sdxErrorInvalidColor', @sdxErrorInvalidColor);
  AProduct.Add('sdxErrorInvalidColorIndex', @sdxErrorInvalidColorIndex);
  AProduct.Add('sdxErrorInvalidColumnIndex', @sdxErrorInvalidColumnIndex);
  AProduct.Add('sdxErrorInvalidDocumentType', @sdxErrorInvalidDocumentType);
  AProduct.Add('sdxErrorInvalidFormatCodeID', @sdxErrorInvalidFormatCodeID);
  AProduct.Add('sdxErrorInvalidFormula', @sdxErrorInvalidFormula);
  AProduct.Add('sdxErrorInvalidReference', @sdxErrorInvalidReference);
  AProduct.Add('sdxErrorInvalidRelationshipId', @sdxErrorInvalidRelationshipId);
  AProduct.Add('sdxErrorInvalidSelection', @sdxErrorInvalidSelection);
  AProduct.Add('sdxErrorInvalidSharedStringIndex', @sdxErrorInvalidSharedStringIndex);
  AProduct.Add('sdxErrorInvalidSheetId', @sdxErrorInvalidSheetId);
  AProduct.Add('sdxErrorInvalidStyleIndex', @sdxErrorInvalidStyleIndex);
  AProduct.Add('sdxErrorPictureCannotBeFound', @sdxErrorPictureCannotBeFound);
  AProduct.Add('sdxErrorUnsupportedDocumentFormat', @sdxErrorUnsupportedDocumentFormat);
  AProduct.Add('sdxErrorUnsupportedSheetType', @sdxErrorUnsupportedSheetType);
  AProduct.Add('sdxErrorCellAlreadyExists', @sdxErrorCellAlreadyExists);
  AProduct.Add('sdxErrorPossibleDataLoss', @sdxErrorPossibleDataLoss);
  AProduct.Add('sdxErrorCannotMoveBecauseOfMergedCells', @sdxErrorCannotMoveBecauseOfMergedCells);
  AProduct.Add('sdxUnmergeCellsConfirmation', @sdxUnmergeCellsConfirmation);
  AProduct.Add('sdxErrorCircularMessage', @sdxErrorCircularMessage);
  AProduct.Add('sdxErrorCircularPathPrefix', @sdxErrorCircularPathPrefix);

  AProduct.Add('sdxBuiltInPopupMenuBringToFront', @sdxBuiltInPopupMenuBringToFront);
  AProduct.Add('sdxBuiltInPopupMenuClearContents', @sdxBuiltInPopupMenuClearContents);
  AProduct.Add('sdxBuiltInPopupMenuCopy', @sdxBuiltInPopupMenuCopy);
  AProduct.Add('sdxBuiltInPopupMenuCustomizeObject', @sdxBuiltInPopupMenuCustomizeObject);
  AProduct.Add('sdxBuiltInPopupMenuCut', @sdxBuiltInPopupMenuCut);
  AProduct.Add('sdxBuiltInPopupMenuDelete', @sdxBuiltInPopupMenuDelete);
  AProduct.Add('sdxBuiltInPopupMenuDeleteDialog', @sdxBuiltInPopupMenuDeleteDialog);
  AProduct.Add('sdxBuiltInPopupMenuFormatCells', @sdxBuiltInPopupMenuFormatCells);
  AProduct.Add('sdxBuiltInPopupMenuHide', @sdxBuiltInPopupMenuHide);
  AProduct.Add('sdxBuiltInPopupMenuInsert', @sdxBuiltInPopupMenuInsert);
  AProduct.Add('sdxBuiltInPopupMenuInsertDialog', @sdxBuiltInPopupMenuInsertDialog);
  AProduct.Add('sdxBuiltInPopupMenuMergeCells', @sdxBuiltInPopupMenuMergeCells);
  AProduct.Add('sdxBuiltInPopupMenuPaste', @sdxBuiltInPopupMenuPaste);
  AProduct.Add('sdxBuiltInPopupMenuRename', @sdxBuiltInPopupMenuRename);
  AProduct.Add('sdxBuiltInPopupMenuSendToBack', @sdxBuiltInPopupMenuSendToBack);
  AProduct.Add('sdxBuiltInPopupMenuSplitCells', @sdxBuiltInPopupMenuSplitCells);
  AProduct.Add('sdxBuiltInPopupMenuUnhide', @sdxBuiltInPopupMenuUnhide);
  AProduct.Add('sdxBuiltInPopupMenuUnhideDialog', @sdxBuiltInPopupMenuUnhideDialog);

  AProduct.Add('sdxRenameDialogCaption', @sdxRenameDialogCaption);
  AProduct.Add('sdxRenameDialogSheetName', @sdxRenameDialogSheetName);

  AProduct.Add('sdxFileDialogAllSupported', @sdxFileDialogAllSupported);

  AProduct.Add('sdxActionCellEditing', @sdxActionCellEditing);
  AProduct.Add('sdxActionCellsMerge', @sdxActionCellsMerge);
  AProduct.Add('sdxActionClearCells', @sdxActionClearCells);
  AProduct.Add('sdxActionDeleteCells', @sdxActionDeleteCells);
  AProduct.Add('sdxActionInsertCells', @sdxActionInsertCells);
  AProduct.Add('sdxActionSortCells', @sdxActionSortCells);
  AProduct.Add('sdxActionFormatCells', @sdxActionFormatCells);
  AProduct.Add('sdxActionCutCells', @sdxActionCutCells);
  AProduct.Add('sdxActionPasteCells', @sdxActionPasteCells);
  AProduct.Add('sdxActionChangeRowColumn', @sdxActionChangeRowColumn);
  AProduct.Add('sdxActionChangeContainer', @sdxActionChangeContainer);

end;

initialization
  dxResourceStringsRepository.RegisterProduct('ExpressSpreadSheet 2', @AddSpreadSheetResourceStringNames);

finalization
  dxResourceStringsRepository.UnRegisterProduct('ExpressSpreadSheet 2');
end.
