{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressGaugeControl                                      }
{                                                                    }
{           Copyright (c) 2013-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSGAUGECONTROL AND ALL           }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxGaugeControlSelection;

{$I cxVer.inc}

interface

uses
  SysUtils, Classes, Windows, cxControls, cxGeometry, dxGaugeCustomScale;

type
  TdxGaugeControlCustomSelectionHelper = class;
  TdxGaugeControlCustomSelectionHelperClass = class of TdxGaugeControlCustomSelectionHelper;
  TdxGaugeControlMoveSelectionEvent = procedure(const ADelta: TdxPointF) of object;

  { TdxGaugeControlCustomSelectionHelper }

  TdxGaugeControlCustomSelectionHelper = class(TPersistent)
  private
    FControl: TcxControl;
    FIsActive: Boolean;
    FSelections: TList;

    function IsEmptySelection: Boolean;
    function IsExistsScale(AScale: TdxGaugeCustomScale): Boolean;
    function IsMultiSelection: Boolean;
    function GetScale(AScaleIndex: Integer): TdxGaugeCustomScale;
    function GetFirstScaleIndex: Integer;
    procedure AddSelection(AScale: TdxGaugeCustomScale);
    procedure InternalSelect(AScale: TdxGaugeCustomScale; AIsSelectionKeyPressed: Boolean);
    procedure SelectLastScale;
    procedure SelectNextScale;
    procedure SelectPreviousScale;
  protected
    function GetIsControlSelected: Boolean; virtual;
    procedure SetSelection; virtual;
    procedure SelectComponent(AComponent: TComponent); virtual;
    procedure ShowRangesEditor; virtual;
    procedure ShowScalesEditor; virtual;

    function GetActive(ASelection: TList): Boolean;
    function KeyDown(AKey: Integer): Boolean;
    procedure Changed;
    procedure DeleteSelections;
    procedure PopulateSelections(ASelection: TList);
    procedure Select(AScale: TdxGaugeCustomScale; AIsShiftPressed: Boolean);

    property Control: TcxControl read FControl;
    property IsActive: Boolean read FIsActive write FIsActive;
    property IsControlSelected: Boolean read GetIsControlSelected;
    property Selections: TList read FSelections;
  public
    constructor Create(AControl: TcxControl); virtual;
    destructor Destroy; override;
  end;

  { TdxGaugeControlCustomSelectionHelpers }

  TdxGaugeControlSelectionHelpers = class(TList)
  protected
    function FindActiveHelper(out AHelper: TdxGaugeControlCustomSelectionHelper): Boolean;
  public
    constructor Create;
    destructor Destroy; override;

    procedure RegistrySelectionHelper(AHelper: TdxGaugeControlCustomSelectionHelper);
    procedure UnregistrySelectionHelper(AHelper: TdxGaugeControlCustomSelectionHelper);
  end;

function dxGaugeControlSelectionHelpers: TdxGaugeControlSelectionHelpers;

implementation

uses
  Types, dxHooks, dxGaugeControl, dxGaugeQuantitativeScale;

const
  dxCenterPositionDelta = 0.5;

type
  TdxGaugeQuantitativeScaleAccess = class(TdxGaugeQuantitativeScale);
  TdxCustomGaugeControlAccess = class(TdxCustomGaugeControl);

var
  FGaugeControlSelectionHelpers: TdxGaugeControlSelectionHelpers;

function dxGaugeControlSelectionHelpers: TdxGaugeControlSelectionHelpers;
begin
  if FGaugeControlSelectionHelpers = nil then
    FGaugeControlSelectionHelpers := TdxGaugeControlSelectionHelpers.Create;
  Result := FGaugeControlSelectionHelpers;
end;

{ TdxGaugeControlCustomSelectionHelper }

constructor TdxGaugeControlCustomSelectionHelper.Create(AControl: TcxControl);
begin
  inherited Create;
  FControl := AControl as TdxCustomGaugeControl;
  FSelections := TList.Create;
end;

destructor TdxGaugeControlCustomSelectionHelper.Destroy;
begin
  FreeAndNil(FSelections);
  inherited Destroy;
end;

function TdxGaugeControlCustomSelectionHelper.GetIsControlSelected: Boolean;
begin
  Result := False;
end;

procedure TdxGaugeControlCustomSelectionHelper.SetSelection;
begin
//do nothing
end;

procedure TdxGaugeControlCustomSelectionHelper.SelectComponent(AComponent: TComponent);
begin
//do nothing
end;

procedure TdxGaugeControlCustomSelectionHelper.ShowScalesEditor;
begin
//do nothing
end;

procedure TdxGaugeControlCustomSelectionHelper.ShowRangesEditor;
begin
//do nothing
end;

function TdxGaugeControlCustomSelectionHelper.GetActive(ASelection: TList): Boolean;

  function IsExistsRange(ARange: TdxGaugeCustomRange): Boolean;
  var
    I, J: Integer;
    AScale: TdxGaugeCustomScale;
  begin
    Result := False;
    for I := 0 to TdxCustomGaugeControl(Control).Scales.Count - 1 do
    begin
      AScale := TdxCustomGaugeControl(Control).Scales[I];
      if AScale is TdxGaugeQuantitativeScale then
        for J := 0 to TdxGaugeQuantitativeScaleAccess(AScale).Ranges.Count - 1 do
        begin
          Result := TdxGaugeQuantitativeScaleAccess(AScale).Ranges.IndexOf(ARange) <> -1;
          if Result then
            Break;
        end;
    end;
  end;

begin
  Result := (ASelection.Count > 0) and ((TObject(ASelection.Last) is TdxGaugeCustomScale) and
    IsExistsScale(TdxGaugeCustomScale(ASelection.Last)) or
    (TObject(ASelection.Last) is TdxGaugeCustomRange) and IsExistsRange(TdxGaugeCustomRange(ASelection.Last)));
end;

function TdxGaugeControlCustomSelectionHelper.KeyDown(AKey: Integer): Boolean;
begin
  Result := False;
  case AKey of
    VK_DELETE:
      begin
        DeleteSelections;
        Result := TdxCustomGaugeControl(Control).Scales.Count = 0;
      end;
    VK_ESCAPE:
      begin
        SelectComponent(Control);
        Result := True;
      end;
    VK_RIGHT:
      SelectNextScale;
    VK_LEFT:
      SelectPreviousScale
  end;
end;

procedure TdxGaugeControlCustomSelectionHelper.Changed;
begin
  TdxCustomGaugeControl(Control).InvalidateRect(Control.ClientRect, False);
end;

procedure TdxGaugeControlCustomSelectionHelper.DeleteSelections;
begin
  while Selections.Count > 0 do
    TdxGaugeCustomScale(Selections[0]).Free;
  TdxCustomGaugeControlAccess(Control).Modified;
  if TdxCustomGaugeControlAccess(Control).Scales.Count > 0 then
    SelectLastScale
  else
    SelectComponent(Control);
end;

procedure TdxGaugeControlCustomSelectionHelper.PopulateSelections(ASelection: TList);
var
  I: Integer;
begin
  for I := 0 to ASelection.Count - 1 do
    if IsExistsScale(ASelection[I]) then
      InternalSelect(ASelection[I], IsCtrlPressed or (GetAsyncKeyState(VK_SHIFT) < 0));
end;

procedure TdxGaugeControlCustomSelectionHelper.Select(AScale: TdxGaugeCustomScale; AIsShiftPressed: Boolean);
begin
  if AScale <> nil then
  begin
    InternalSelect(AScale, AIsShiftPressed);
    SetSelection;
  end
  else
    Selections.Clear;
end;

function TdxGaugeControlCustomSelectionHelper.IsEmptySelection: Boolean;
begin
  Result := Selections.Count = 0;
end;

function TdxGaugeControlCustomSelectionHelper.IsExistsScale(AScale: TdxGaugeCustomScale): Boolean;
begin
  Result := TdxCustomGaugeControl(Control).Scales.IndexOf(AScale) <> -1;
end;

function TdxGaugeControlCustomSelectionHelper.IsMultiSelection: Boolean;
begin
  Result := Selections.Count > 1;
end;

function TdxGaugeControlCustomSelectionHelper.GetScale(AScaleIndex: Integer): TdxGaugeCustomScale;
begin
  Result := TdxGaugeCustomScale(TdxCustomGaugeControl(Control).Scales[AScaleIndex]);
end;

function TdxGaugeControlCustomSelectionHelper.GetFirstScaleIndex: Integer;
begin
  Result := TdxGaugeCustomScale(Selections[0]).Index
end;

procedure TdxGaugeControlCustomSelectionHelper.AddSelection(AScale: TdxGaugeCustomScale);
begin
  Selections.Add(AScale);
end;

procedure TdxGaugeControlCustomSelectionHelper.InternalSelect(AScale: TdxGaugeCustomScale; AIsSelectionKeyPressed: Boolean);
begin
  if not AIsSelectionKeyPressed then
    Selections.Clear;
  if Selections.IndexOf(AScale) = -1 then
    AddSelection(AScale)
  else
    if AIsSelectionKeyPressed then
      Selections.Remove(AScale);
end;

procedure TdxGaugeControlCustomSelectionHelper.SelectLastScale;
begin
  if TdxCustomGaugeControl(Control).Scales.Count > 0 then
    Select(GetScale(TdxGaugeControl(Control).Scales.Count - 1), False);
end;

procedure TdxGaugeControlCustomSelectionHelper.SelectNextScale;
begin
  if not (IsEmptySelection or IsMultiSelection) and
    (GetFirstScaleIndex < TdxCustomGaugeControl(Control).Scales.Count - 1) then
    Select(GetScale(GetFirstScaleIndex + 1), False);
end;

procedure TdxGaugeControlCustomSelectionHelper.SelectPreviousScale;
begin
  if not (IsEmptySelection or IsMultiSelection) and (GetFirstScaleIndex > 0) then
    Select(GetScale(GetFirstScaleIndex - 1), False);
end;

{ TdxGaugeControlCustomSelectionHelpers }

function ProcessKeyboardMessage(AKey: WPARAM; AFlags: LPARAM): Boolean;

  function KeyPressed: Boolean;
  begin
    Result := (AFlags shr 31) and 1 = 0;
  end;

var
  AHelper: TdxGaugeControlCustomSelectionHelper;
begin
  Result := False;
  if KeyPressed and dxGaugeControlSelectionHelpers.FindActiveHelper(AHelper) then
  begin
    Result := AHelper.Selections.Count > 0;
    if Result then
      Result := AHelper.KeyDown(AKey);
  end
end;

procedure dxGaugeControlKeyboardHook(ACode: Integer; wParam: WPARAM; lParam: LPARAM; var AHookResult: LRESULT);
begin
  if (ACode = HC_ACTION) then
    AHookResult := Integer(ProcessKeyboardMessage(wParam, lParam));
end;

constructor TdxGaugeControlSelectionHelpers.Create;
begin
  inherited Create;
  dxSetHook(htKeyboard, dxGaugeControlKeyboardHook);
end;

destructor TdxGaugeControlSelectionHelpers.Destroy;
begin
  dxReleaseHook(dxGaugeControlKeyboardHook);
  inherited Destroy;
end;

procedure TdxGaugeControlSelectionHelpers.RegistrySelectionHelper(AHelper: TdxGaugeControlCustomSelectionHelper);
begin
  if IndexOf(AHelper) = -1 then
    Add(AHelper);
end;

procedure TdxGaugeControlSelectionHelpers.UnregistrySelectionHelper(AHelper: TdxGaugeControlCustomSelectionHelper);
begin
  Remove(AHelper);
end;

function TdxGaugeControlSelectionHelpers.FindActiveHelper(out AHelper: TdxGaugeControlCustomSelectionHelper): Boolean;
var
  I: Integer;
begin
  Result := False;
  for I := 0 to Count - 1 do
  begin
    AHelper := Items[I];
    Result := AHelper.IsActive and AHelper.Control.HandleAllocated and
      ((AHelper.Control.Handle = GetFocus) or ((AHelper.Control.Parent <> nil) and
        AHelper.Control.Parent.HandleAllocated and cxIsParentFocused(AHelper.Control.Parent.Handle)));
    if Result then
      Break;
  end;
end;

initialization

finalization
  FreeAndNil(FGaugeControlSelectionHelpers);

end.
