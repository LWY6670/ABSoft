{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressGaugeControl                                      }
{                                                                    }
{           Copyright (c) 2013-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSGAUGECONTROL AND ALL           }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxGaugeDBScale;

{$I cxVer.inc}

interface

uses
  cxDB, cxDataUtils, dxGaugeCircularScale, dxGaugeDigitalScale, dxGaugeLinearScale;

type
  { TdxGaugeDBCircularScale }

  TdxGaugeDBCircularScale = class(TdxGaugeCustomCircularScale)
  protected
    class function GetScaleName: string; override;
    function GetDataBindingClass: TcxCustomDataBindingClass; override;
  published
    property AnchorScaleIndex;
    property DataBinding;
    property OptionsLayout;
    property OptionsView;
    property Ranges;
    property StyleName;
    property Visible;
  end;

  { TdxGaugeDBCircularHalfScale }

  TdxGaugeDBCircularHalfScale = class(TdxGaugeCustomCircularHalfScale)
  protected
    class function GetScaleName: string; override;
    function GetDataBindingClass: TcxCustomDataBindingClass; override;
  published
    property AnchorScaleIndex;
    property DataBinding;
    property OptionsLayout;
    property OptionsView;
    property Ranges;
    property StyleName;
    property Visible;
  end;

  { TdxGaugeDBCircularQuarterLeftScale }

  TdxGaugeDBCircularQuarterLeftScale = class(TdxGaugeCustomCircularQuarterLeftScale)
  protected
    class function GetScaleName: string; override;
    function GetDataBindingClass: TcxCustomDataBindingClass; override;
  published
    property AnchorScaleIndex;
    property DataBinding;
    property OptionsLayout;
    property OptionsView;
    property Ranges;
    property StyleName;
    property Visible;
  end;

  { TdxGaugeDBCircularQuarterRightScale }

  TdxGaugeDBCircularQuarterRightScale = class(TdxGaugeCustomCircularQuarterRightScale)
  protected
    class function GetScaleName: string; override;
    function GetDataBindingClass: TcxCustomDataBindingClass; override;
  published
    property AnchorScaleIndex;
    property DataBinding;
    property OptionsLayout;
    property OptionsView;
    property Ranges;
    property StyleName;
    property Visible;
  end;

  { TdxGaugeDBLinearScale }

  TdxGaugeDBLinearScale = class(TdxGaugeCustomLinearScale)
  protected
    class function GetScaleName: string; override;
    function GetDataBindingClass: TcxCustomDataBindingClass; override;
  published
    property AnchorScaleIndex;
    property DataBinding;
    property OptionsLayout;
    property OptionsView;
    property Ranges;
    property StyleName;
    property Visible;
  end;

  { TdxGaugeDBDigitalScale }

  TdxGaugeDBDigitalScale = class(TdxGaugeCustomDigitalScale)
  protected
    class function GetScaleName: string; override;
    function GetDataBindingClass: TcxCustomDataBindingClass; override;
  published
    property AnchorScaleIndex;
    property DataBinding;
    property OptionsLayout;
    property OptionsView;
    property StyleName;
    property Visible;
  end;

implementation

uses
  dxGaugeCustomScale;

const
  sdxGaugeDBCustomScaleNamePreffix = 'DB ';

{ TdxGaugeDBCircularScale }

class function TdxGaugeDBCircularScale.GetScaleName: string;
begin
  Result := sdxGaugeDBCustomScaleNamePreffix + inherited GetScaleName;
end;

function TdxGaugeDBCircularScale.GetDataBindingClass: TcxCustomDataBindingClass;
begin
  Result := TcxDBDataBinding;
end;

{ TdxGaugeDBCircularHalfScale }

class function TdxGaugeDBCircularHalfScale.GetScaleName: string;
begin
  Result := sdxGaugeDBCustomScaleNamePreffix + inherited GetScaleName;
end;

function TdxGaugeDBCircularHalfScale.GetDataBindingClass: TcxCustomDataBindingClass;
begin
  Result := TcxDBDataBinding;
end;

{ TdxGaugeDBCircularQuarterLeftScale }

class function TdxGaugeDBCircularQuarterLeftScale.GetScaleName: string;
begin
  Result := sdxGaugeDBCustomScaleNamePreffix + inherited GetScaleName;
end;

function TdxGaugeDBCircularQuarterLeftScale.GetDataBindingClass: TcxCustomDataBindingClass;
begin
  Result := TcxDBDataBinding;
end;

{ TdxGaugeDBCircularQuarterRightScale }

class function TdxGaugeDBCircularQuarterRightScale.GetScaleName: string;
begin
  Result := sdxGaugeDBCustomScaleNamePreffix + inherited GetScaleName;
end;

function TdxGaugeDBCircularQuarterRightScale.GetDataBindingClass: TcxCustomDataBindingClass;
begin
  Result := TcxDBDataBinding;
end;

{ TdxGaugeDBLinearScale }

class function TdxGaugeDBLinearScale.GetScaleName: string;
begin
  Result := sdxGaugeDBCustomScaleNamePreffix + inherited GetScaleName;
end;

function TdxGaugeDBLinearScale.GetDataBindingClass: TcxCustomDataBindingClass;
begin
  Result := TcxDBDataBinding;
end;

{ TdxGaugeDBDigitalScale }

class function TdxGaugeDBDigitalScale.GetScaleName: string;
begin
  Result := sdxGaugeDBCustomScaleNamePreffix + inherited GetScaleName;
end;

function TdxGaugeDBDigitalScale.GetDataBindingClass: TcxCustomDataBindingClass;
begin
  Result := TcxDBDataBinding;
end;

initialization
  dxGaugeScaleFactory.RegisterScale(TdxGaugeDBCircularScale, False);
  dxGaugeScaleFactory.RegisterScale(TdxGaugeDBCircularHalfScale, False);
  dxGaugeScaleFactory.RegisterScale(TdxGaugeDBCircularQuarterLeftScale, False);
  dxGaugeScaleFactory.RegisterScale(TdxGaugeDBCircularQuarterRightScale, False);
  dxGaugeScaleFactory.RegisterScale(TdxGaugeDBDigitalScale, False);
  dxGaugeScaleFactory.RegisterScale(TdxGaugeDBLinearScale, False);

finalization
  dxGaugeUnregisterScales;

end.
