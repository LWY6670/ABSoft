{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressGaugeControl                                      }
{                                                                    }
{           Copyright (c) 2013-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSGAUGECONTROL AND ALL           }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxGaugeUtils;

{$I cxVer.inc}

interface

uses
  Windows, Graphics, Classes, dxCore, dxCoreGraphics, cxGeometry, dxGDIPlusAPI, dxGDIPlusClasses;

procedure dxGaugeDrawText(AGPGraphics: TdxGPGraphics; const AText: string; const ARect: TdxRectF; AFont: TFont;
  AHorzAlignment: TAlignment = taCenter); overload;
procedure dxGaugeDrawText(AGPGraphics: TdxGPGraphics; const AText: string; const ARect: TdxRectF; AFont: TFont;
  ARotationAngle: Single; AHorzAlignment: TAlignment = taCenter); overload;
procedure dxGaugeDrawText(AGPGraphics: TdxGPGraphics; const AText: string; const ARect: TdxRectF; ARadius, AAngle: Single;
  AFont: TFont); overload;
procedure dxGaugeDrawText(AGPGraphics: TdxGPGraphics; const AText: string; const ARotatePoint: TdxPointF; ARadius, AAngle: Single;
  AFont: TFont); overload;
procedure dxGaugeDrawText(AGPGraphics: TdxGPGraphics; const AText: string; const ATextRectSize: TdxSizeF;
  const ARect: TdxRectF; ARadius, AAngle: Single; AFont: TFont); overload;
procedure dxGaugeDrawText(AGPGraphics: TdxGPGraphics; const AText: string; const ATextRectSize: TdxSizeF;
  const ARotatePoint: TdxPointF; ARadius, AAngle: Single; AFont: TFont); overload;

procedure dxGaugeDrawBitmap(AGPGraphics: TdxGPGraphics; ABitmap: TBitmap; const ARect: TdxRectF);
procedure dxGaugeDrawImage(AGPGraphics: TdxGPGraphics; AImage: TGraphic; const ARect: TdxRectF);
procedure dxGaugeDrawRectangles(AGPGraphics: TdxGPGraphics; const ARects: TRects);
procedure dxGaugeDrawSector(AGPGraphics: TdxGPGraphics; const ARect: TdxRectF; ASectorWidth: Single;
  AStartAngle, ASweepAngle: Single; ABackgroundColor, ABorderColor: TdxAlphaColor);
procedure dxGaugeDrawSelections(AGPGraphics: TdxGPGraphics; const ARects: TRects);
procedure dxGaugeRotateAndDrawImage(AGPGraphics: TdxGPGraphics; AImage: TGraphic; const ADestImageSize: TdxSizeF;
  const ABounds: TdxRectF; ARadius, ARotateAngle: Single); overload;
procedure dxGaugeRotateAndDrawImage(AGPGraphics: TdxGPGraphics; AImage: TGraphic; const ADestImageSize: TdxSizeF;
  ARotatePoint: TdxPointF; ARadius, ARotateAngle: Single); overload;

function dxGaugeGetSelectorRect(const ABounds: TRect): TRect;
function dxGaugeValueRange(AMaxValue, AMinValue: Single): Single;
procedure dxGaugeRaiseException(const AFormatString: string; const AArguments: array of const);
procedure dxGaugeReverseList(AList: TList); {$IFDEF DELPHI9}inline;{$ENDIF}

implementation

uses
  Types, Math, SysUtils, cxGraphics, dxCompositeShape;

const
  dxSelectionAlphaChannel = 20;
  dxSelectionBackgroundColor: TColor = $582801;
  dxSelectionBorderAlphaChannel = 255;
  dxSelectionBorderColor: TColor = $BD8753;
  dxSelectionMarkerBackgroundColor: TColor = clWhite;
  dxSelectionMarkerSize = 5;

type
  TdxCompositeShapeAccess = class(TdxCompositeShape);

  { TdxSelectionPainter }

  TdxSelectionPainter = class  
  private
    class function GetMarkers(const ARect: TRect): TRects;
    class procedure DrawSelection(AGPGraphics: TdxGPGraphics; const ARect: TRect; ADrawMarkers: Boolean);
    class procedure DrawSelectionBackground(AGPGraphics: TdxGPGraphics; const ARect: TRect);
    class procedure DrawSelectionMarkers(AGPGraphics: TdxGPGraphics; const ARect: TRect);

    class procedure InternalDrawSelections(AGPGraphics: TdxGPGraphics; const ARects: TRects;
      ADrawMarkers: Boolean = True);
  public
    class procedure DrawRectangle(AGPGraphics: TdxGPGraphics; const ARect: TRect; ABorderStyle: TPenStyle;
      ABackgroundAlpha, ABorderAlpha: Byte);
    class procedure DrawSelections(ACanvas: TcxCanvas; const ARect: TRect;
      const ASelectionRects: TRects; ANeedDrawMarkers: Boolean = True); overload;
    class procedure DrawSelections(AGPGraphics: TdxGPGraphics; const ARects: TRects); overload;
  end;

procedure dxGaugeDrawText(AGPGraphics: TdxGPGraphics; const AText: string; const ARect: TdxRectF; AFont: TFont;
  AHorzAlignment: TAlignment = taCenter);
begin
  dxGPDrawText(AGPGraphics, AText, ARect, AFont, dxColorToAlphaColor(AFont.Color), AHorzAlignment, taVerticalCenter,
    False, TextRenderingHintAntiAlias);
end;

procedure dxGaugeDrawText(AGPGraphics: TdxGPGraphics; const AText: string; const ARect: TdxRectF; AFont: TFont;
  ARotationAngle: Single; AHorzAlignment: TAlignment = taCenter);
begin
  AGPGraphics.RotateWorldTransform(ARotationAngle, cxRectCenter(ARect));
  dxGaugeDrawText(AGPGraphics, AText, ARect, AFont, AHorzAlignment);
  AGPGraphics.RotateWorldTransform(-ARotationAngle, cxRectCenter(ARect));
end;

procedure dxGaugeDrawText(AGPGraphics: TdxGPGraphics; const AText: string; const ARect: TdxRectF; ARadius, AAngle: Single;
  AFont: TFont);
begin
  dxGaugeDrawText(AGPGraphics, AText, cxRectCenter(ARect), ARadius, AAngle, AFont);
end;

procedure dxGaugeDrawText(AGPGraphics: TdxGPGraphics; const AText: string; const ARotatePoint: TdxPointF; ARadius,
  AAngle: Single; AFont: TFont);
var
  ATextRect: TdxRectF;
begin
  dxGPGetTextRect(AGPGraphics, AText, AFont, False, cxRectF(cxNullRect), ATextRect);
  dxGaugeDrawText(AGPGraphics, AText, dxSizeF(ATextRect.Width, ATextRect.Height), ARotatePoint, ARadius,
    AAngle, AFont);
end;

procedure dxGaugeDrawText(AGPGraphics: TdxGPGraphics; const AText: string; const ATextRectSize: TdxSizeF;
  const ARect: TdxRectF; ARadius, AAngle: Single; AFont: TFont);
begin
  dxGaugeDrawText(AGPGraphics, AText, ATextRectSize, cxRectCenter(ARect), ARadius, AAngle, AFont);
end;

procedure dxGaugeDrawText(AGPGraphics: TdxGPGraphics; const AText: string; const ATextRectSize: TdxSizeF;
  const ARotatePoint: TdxPointF; ARadius, AAngle: Single; AFont: TFont);
var
  P: TdxPointF;
  ASize: TdxSizeF;
  ATextRect: TdxRectF;
begin
  ASize := dxSizeF(ATextRectSize.cx / 2, ATextRectSize.cy / 2);
  P := dxRingPoint(ARotatePoint, ARadius, DegToRad(AAngle));
  ATextRect := cxRectF(P.X - ASize.cx, P.Y - ASize.cy, P.X + ASize.cx, P.Y + ASize.cy);
  dxGaugeDrawText(AGPGraphics, AText, ATextRect, AFont);
end;

procedure dxGaugeDrawBitmap(AGPGraphics: TdxGPGraphics; ABitmap: TBitmap; const ARect: TdxRectF);
var
  AImage: TdxGPImage;
begin
  AImage := TdxGPImage.CreateFromBitmap(ABitmap);
  AGPGraphics.Draw(AImage, cxRectOffsetF(ARect, dxPointF(-1, -1)));
  AImage.Free;
end;

procedure dxGaugeDrawImage(AGPGraphics: TdxGPGraphics; AImage: TGraphic; const ARect: TdxRectF);
var
  ATempBitmap: TcxBitmap32;
begin
  if AImage <> nil then
    if AImage is TdxCompositeShape then
      TdxCompositeShapeAccess(AImage).DrawF(AGPGraphics, ARect)
    else
    begin
      ATempBitmap := TcxBitmap32.CreateSize(AImage.Width, AImage.Height, True);
      dxGaugeDrawBitmap(AGPGraphics, ATempBitmap, ARect);
      ATempBitmap.Free;
    end;
end;

procedure dxGaugeDrawRectangle(AGPGraphics: TdxGPGraphics; const ARect: TRect; AHatchBrushStyle: TdxGpHatchStyle;
  ABackgroundColor, AForegroundColor: TColor; ABackgroundAlpha, AForegroundAlpha: Byte);
begin
  TdxSelectionPainter.DrawRectangle(AGPGraphics, ARect, psDash, ABackgroundAlpha, AForegroundAlpha);
end;

procedure dxGaugeDrawRectangles(AGPGraphics: TdxGPGraphics; const ARects: TRects);
var
  I: Integer;
begin
  for I := Low(ARects) to High(ARects) do
    dxGaugeDrawRectangle(AGPGraphics, ARects[I], HatchStyleForwardDiagonal,
      dxSelectionBackgroundColor, dxSelectionBorderColor, dxSelectionAlphaChannel, dxSelectionBorderAlphaChannel);
end;

procedure dxGaugeDrawSector(AGPGraphics: TdxGPGraphics; const ARect: TdxRectF; ASectorWidth: Single;
  AStartAngle, ASweepAngle: Single; ABackgroundColor, ABorderColor: TdxAlphaColor);

  procedure InitPath(APath: TdxGPPath);
  var
    R: TdxRectF;
  begin
    APath.FigureStart;
    if ASectorWidth < ARect.Width / 2 then
    begin
      R := cxRectContent(ARect, dxRectF(ASectorWidth, ASectorWidth, ASectorWidth, ASectorWidth));
      APath.AddArc(R.Left, R.Top, R.Width, R.Height, -AStartAngle, -ASweepAngle);
      APath.AddArc(ARect.Left, ARect.Top, ARect.Width, ARect.Height, -AStartAngle - ASweepAngle, ASweepAngle)
    end
    else
      APath.AddPie(ARect.Left, ARect.Top, ARect.Width, ARect.Height, -AStartAngle, -ASweepAngle);
    APath.FigureFinish;
  end;

var
  ABrush: TdxGPBrush;
  APath: TdxGPPath;
  APen: TdxGPPen;
begin
  if ASectorWidth > 0 then
  begin
    APen := TdxGPPen.Create;
    ABrush := TdxGPBrush.Create;
    APath := TdxGPPath.Create(gpfmWinding);
    try
      APen.Brush.Color := ABorderColor;
      ABrush.Color := ABackgroundColor;
      InitPath(APath);
      AGPGraphics.Path(APath, APen, ABrush);
    finally
      APath.Free;
      ABrush.Free;
      APen.Free;
    end;
  end;
end;

procedure dxGaugeDrawSelections(AGPGraphics: TdxGPGraphics; const ARects: TRects);
begin
  TdxSelectionPainter.DrawSelections(AGPGraphics, ARects);
end;

procedure dxGaugeRotateAndDrawImage(AGPGraphics: TdxGPGraphics; AImage: TGraphic; const ADestImageSize: TdxSizeF;
  const ABounds: TdxRectF; ARadius, ARotateAngle: Single);
begin
  dxGaugeRotateAndDrawImage(AGPGraphics, AImage, ADestImageSize, cxRectCenter(ABounds), ARadius, ARotateAngle);
end;

procedure dxGaugeRotateAndDrawImage(AGPGraphics: TdxGPGraphics; AImage: TGraphic; const ADestImageSize: TdxSizeF;
  ARotatePoint: TdxPointF; ARadius, ARotateAngle: Single);
var
  P: TdxPointF;
  APreviousWorldTransform: TdxGPMatrix;
begin
  ARotateAngle := -ARotateAngle;
  P := dxRingPoint(ARotatePoint, ARadius, DegToRad(-ARotateAngle));
  APreviousWorldTransform := AGPGraphics.GetWorldTransform;
  AGPGraphics.RotateWorldTransform(ARotateAngle, P);
  dxGaugeDrawImage(AGPGraphics, AImage,
    cxRectF(P.X, P.Y - ADestImageSize.cy / 2, P.X + ADestImageSize.cx, P.Y + ADestImageSize.cy / 2));
  AGPGraphics.SetWorldTransform(APreviousWorldTransform);
end;

procedure dxGaugeReverseList(AList: TList);
var
  ATemp: Pointer;
  AStartIndex, AEndIndex: Integer;
begin
  AStartIndex := 0;
  AEndIndex := AList.Count - 1;
  while AStartIndex < AEndIndex do
  begin
    ATemp := AList[AStartIndex];
    AList[AStartIndex] := AList[AEndIndex];
    AList[AEndIndex] := ATemp;
    Inc(AStartIndex);
    Dec(AEndIndex);
  end;
end;

function dxGaugeGetSelectorRect(const ABounds: TRect): TRect;
const
  SelectorRectOffset = 5;
  SelectorRectWidth = 15;
begin
  Result := ABounds;
  Result.Left := Result.Right - SelectorRectWidth;
  Result.Top := Result.Bottom - SelectorRectWidth;
  Result := cxRectOffset(Result, -SelectorRectOffset, -SelectorRectOffset);
end;

function dxGaugeValueRange(AMaxValue, AMinValue: Single): Single;
begin
  Result := Abs(AMaxValue - AMinValue);
end;

procedure dxGaugeRaiseException(const AFormatString: string; const AArguments: array of const);
begin
  raise EdxException.Create(Format(AFormatString, AArguments));
end;

{ TdxSelectionPainter }

class procedure TdxSelectionPainter.DrawRectangle(AGPGraphics: TdxGPGraphics; const ARect: TRect;
  ABorderStyle: TPenStyle; ABackgroundAlpha, ABorderAlpha: Byte);
var
  R: TRect;
begin
  R := ARect;
  Inc(R.Right);
  Inc(R.Bottom);
  AGPGraphics.Rectangle(R, dxSelectionBorderColor, dxSelectionBackgroundColor, 1, ABorderStyle, ABorderAlpha,
    ABackgroundAlpha);
end;

class procedure TdxSelectionPainter.DrawSelections(ACanvas: TcxCanvas; const ARect: TRect; const ASelectionRects: TRects;
  ANeedDrawMarkers: Boolean = True);
var
  AGPGraphics: TdxGPGraphics;
begin
  AGPGraphics := dxGpBeginPaint(ACanvas.Handle, ARect);
  try
    DrawSelections(AGPGraphics, ASelectionRects);
  finally
    dxGpEndPaint(AGPGraphics);
  end;
end;

class procedure TdxSelectionPainter.DrawSelections(AGPGraphics: TdxGPGraphics; const ARects: TRects);
begin
  InternalDrawSelections(AGPGraphics, ARects);
end;

class function TdxSelectionPainter.GetMarkers(const ARect: TRect): TRects;

  function dxGetSelectionMarker(const P: TPoint; AMarkerWidth: Integer): TRect;
  begin
    Result := cxRectInflate(cxRect(P, P), (AMarkerWidth - 1) div 2, (AMarkerWidth - 1) div 2);
    Inc(Result.Bottom);
    Inc(Result.Right);
  end;

  function dxGetSelectionMarkers(const ABorderBounds: TRect; AMarkerWidth: Integer): TRects;
  var
    AMiddleX, AMiddleY: Integer;
  begin
    SetLength(Result, 8);
    AMiddleX := (ABorderBounds.Left + ABorderBounds.Right - 1) div 2;
    AMiddleY := (ABorderBounds.Top + ABorderBounds.Bottom - 1) div 2;
    Result[0] := dxGetSelectionMarker(ABorderBounds.TopLeft, AMarkerWidth);
    Result[1] := dxGetSelectionMarker(Point(AMiddleX, ABorderBounds.Top), AMarkerWidth);
    Result[2] := dxGetSelectionMarker(Point(ABorderBounds.Right - 1, ABorderBounds.Top), AMarkerWidth);
    Result[3] := dxGetSelectionMarker(Point(ABorderBounds.Right - 1, AMiddleY), AMarkerWidth);
    Result[4] := dxGetSelectionMarker(Point(ABorderBounds.Right - 1, ABorderBounds.Bottom - 1), AMarkerWidth);
    Result[5] := dxGetSelectionMarker(Point(AMiddleX, ABorderBounds.Bottom - 1), AMarkerWidth);
    Result[6] := dxGetSelectionMarker(Point(ABorderBounds.Left, ABorderBounds.Bottom - 1), AMarkerWidth);
    Result[7] := dxGetSelectionMarker(Point(ABorderBounds.Left, AMiddleY), AMarkerWidth);
  end;

begin
  Result := dxGetSelectionMarkers(ARect, dxSelectionMarkerSize);
end;

class procedure TdxSelectionPainter.DrawSelection(AGPGraphics: TdxGPGraphics; const ARect: TRect; ADrawMarkers: Boolean);
begin
  DrawSelectionBackground(AGPGraphics, ARect);
  if ADrawMarkers then
    DrawSelectionMarkers(AGPGraphics, ARect);
end;

class procedure TdxSelectionPainter.DrawSelectionBackground(AGPGraphics: TdxGPGraphics; const ARect: TRect);
begin
  DrawRectangle(AGPGraphics, ARect, psSolid, dxSelectionAlphaChannel, dxSelectionBorderAlphaChannel);
end;

class procedure TdxSelectionPainter.DrawSelectionMarkers(AGPGraphics: TdxGPGraphics; const ARect: TRect);
var
  I: Integer;
  AMarkers: TRects;
begin
  AMarkers := GetMarkers(ARect);
  for I := Low(AMarkers) to High(AMarkers) do
    AGPGraphics.Rectangle(AMarkers[I], dxSelectionBorderColor, dxSelectionMarkerBackgroundColor, 1, psSolid, 255, 255);
end;

class procedure TdxSelectionPainter.InternalDrawSelections(AGPGraphics: TdxGPGraphics; const ARects: TRects;
  ADrawMarkers: Boolean = True);
var
  I: Integer;
begin
  for I := Low(ARects) to High(ARects) do
    DrawSelection(AGPGraphics, ARects[I], ADrawMarkers);
end;

end.

