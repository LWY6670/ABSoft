{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressGaugeControl                                      }
{                                                                    }
{           Copyright (c) 2013-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSGAUGECONTROL AND ALL           }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxGaugeQuantitativeScale;

{$I cxVer.inc}

interface

uses
  SysUtils, Classes, Graphics, Windows, Generics.Collections,
  cxGeometry, dxCoreGraphics, cxGraphics, dxGDIPlusClasses, dxCompositeShape, dxXMLDoc, cxClasses, dxGaugeCustomScale;

type
  TdxGaugeCustomRange = class;
  TdxGaugeCustomRangeClass = class of TdxGaugeCustomRange;
  TdxGaugeCustomRangeParameters = class;
  TdxGaugeCustomRangeParametersClass = class of TdxGaugeCustomRangeParameters;
  TdxGaugeCustomRangeViewInfo = class;
  TdxGaugeCustomRangeViewInfoClass = class of TdxGaugeCustomRangeViewInfo;
  TdxGaugeRangeCollection = class;
  TdxGaugeQuantitativeScale = class;
  TdxGaugeQuantitativeScaleClass = class of TdxGaugeQuantitativeScale;
  TdxGaugeQuantitativeScaleParameters = class;
  TdxGaugeQuantitativeScaleParametersClass = class of TdxGaugeQuantitativeScaleParameters;
  TdxGaugeQuantitativeScaleViewInfo = class;
  TdxGaugeQuantitativeScaleViewInfoClass = class of TdxGaugeQuantitativeScaleViewInfo;

  TdxGaugeQuantitativeScaleParameterValue = (spvFontColor, spvFontName, spvFontSize, spvShowFirstTick, spvShowLabels,
    spvShowLastTick, spvShowTicks, spvMajorTickCount, spvMinorTickCount);
  TdxGaugeQuantitativeScaleParameterValues = set of TdxGaugeQuantitativeScaleParameterValue;

  { TdxGaugeQuantitativeScaleDefaultParameters }

  TdxGaugeQuantitativeScaleDefaultParameters = class(TdxGaugeCustomScaleDefaultParameters)
  public
    // Font
    FontColor: TColor;
    FontName: string;
    FontSize: Integer;
    // Labels
    LabelOffset: Single;
    // Ticks
    AllowOverlapMajorTicks: Boolean;
    ShowFirstTick: Boolean;
    ShowLabels: Boolean;
    ShowLastTick: Boolean;
    ShowTicks: Boolean;
    // Major ticks
    MajorTickCount: Integer;
    MajorTickOffset: Single;
    MajorTickScaleFactor: TdxPointF;
    MajorTickType: TdxGaugeElementType;
    // Minor ticks
    MinorTickCount: Integer;
    MinorTickOffset: Single;
    MinorTickScaleFactor: TdxPointF;
    MinorTickType: TdxGaugeElementType;
    // Value indicator
    SpindleCapSize: Single;
    ValueIndicatorStartOffset: Single;
    ValueIndicatorEndOffset: Single;
  end;

  { TdxGaugeQuantitativeScaleParameters }

  TdxGaugeQuantitativeScaleParameters = class(TdxGaugeCustomScaleParameters)
  public
    Font: Pointer;
    MajorTickCount: Integer;
    MaxValue: Single;
    MinValue: Single;
    MinorTickCount: Integer;
    ShowFirstTick: Boolean;
    ShowLabels: Boolean;
    ShowLastTick: Boolean;
    ShowTicks: Boolean;
    ShowValueIndicator: Boolean;
  end;

  { TdxGaugeScaleTick }

  TdxGaugeScaleTick = class
  public
    Value: Single;
    Visible: Boolean;
  end;

  TdxGaugeScaleTicks = TObjectList<TdxGaugeScaleTick>;

  { TdxGaugeScaleTickDrawParameters }

  TdxGaugeScaleTickDrawParameters = record
    Image: TGraphic;
    Offset: Single;
    OriginalSize: TdxSizeF;
    ScaleFactor: TdxPointF;
    ShowLabel: Boolean;
    ShowTick: Boolean;
    Size: TdxSizeF;
  end;

  { TdxGaugeCustomRangeParameters }

  TdxGaugeCustomRangeParameters = class
  public
    Color: TdxAlphaColor;
    ValueEnd: Single;
    ValueStart: Single;
    Visible: Boolean;
    WidthFactor: Single;
    // Scale Info
    ScaleBounds: TdxRectF;
    ScaleMaxValue: Single;
    ScaleMinValue: Single;

    procedure Assign(ASource: TdxGaugeCustomRangeParameters);
  end;

  { TdxGaugeQuantitativeScaleOptionsView }

  TdxGaugeQuantitativeScaleOptionsView = class(TdxGaugeCustomScaleOptionsView)
  private
    function GetMajorTickCount: Integer;
    function GetMaxValue: Single;
    function GetMinorTickCount: Integer;
    function GetMinValue: Single;
    function GetFont: TFont;
    function GetShowFirstTick: Boolean;
    function GetShowLabels: Boolean;
    function GetShowLastTick: Boolean;
    function GetShowTicks: Boolean;
    procedure SetMajorTickCount(const AValue: Integer);
    procedure SetMaxValue(const AValue: Single);
    procedure SetMinorTickCount(const AValue: Integer);
    procedure SetMinValue(const AValue: Single);
    procedure SetFont(const AValue: TFont);
    procedure SetShowFirstTick(const AValue: Boolean);
    procedure SetShowLabels(const AValue: Boolean);
    procedure SetShowLastTick(const AValue: Boolean);
    procedure SetShowTicks(const AValue: Boolean);

    function GetScale: TdxGaugeQuantitativeScale;

    function IsMajorTickCountStored: Boolean;
    function IsMaxValueStored: Boolean;
    function IsMinorTickCountStored: Boolean;
    function IsFontStored: Boolean;

    function IsShowFirstTickStored: Boolean;
    function IsShowLabelsStored: Boolean;
    function IsShowLastTickStored: Boolean;
    function IsShowTicksStored: Boolean;
  protected
    function GetShowValueIndicator: Boolean;
    procedure SetShowValueIndicator(const AValue: Boolean);

    property ShowValueIndicator: Boolean read GetShowValueIndicator write SetShowValueIndicator default True;
  published
    property Font: TFont read GetFont write SetFont stored IsFontStored;
    property MajorTickCount: Integer read GetMajorTickCount write SetMajorTickCount stored IsMajorTickCountStored;
    property MaxValue: Single read GetMaxValue write SetMaxValue stored IsMaxValueStored;
    property MinorTickCount: Integer read GetMinorTickCount write SetMinorTickCount stored IsMinorTickCountStored;
    property MinValue: Single read GetMinValue write SetMinValue;
    property ShowFirstTick: Boolean read GetShowFirstTick write SetShowFirstTick stored IsShowFirstTickStored;
    property ShowLabels: Boolean read GetShowLabels write SetShowLabels stored IsShowLabelsStored;
    property ShowLastTick: Boolean read GetShowLastTick write SetShowLastTick stored IsShowLastTickStored;
    property ShowTicks: Boolean read GetShowTicks write SetShowTicks stored IsShowTicksStored;
  end;

  { TdxGaugeQuantitativeScaleViewInfo }

  TdxGaugeQuantitativeScaleViewInfo = class(TdxGaugeCustomScaleViewInfo)
  private
    FFont: TFont;
    FLabelOffset: Single;
    FLabelPrecision: Integer;
    FMaxLabelSize: TdxSizeF;

    FMajorTicks: TdxGaugeScaleTicks;
    FMinorTicks: TdxGaugeScaleTicks;

    FMajorTick: TdxCompositeShape;
    FMinorTick: TdxCompositeShape;

    function CreateTick(AValue: Single; AVisible: Boolean): TdxGaugeScaleTick;
    function GetScaleDefaultParameters: TdxGaugeQuantitativeScaleDefaultParameters;
    function GetScaleParameters: TdxGaugeQuantitativeScaleParameters;
    procedure CalculateFontSize;
    procedure CalculateLabelOffset;
    procedure CalculateLabelPrecision;
    procedure CalculateMaxLabelSize(AGPGraphics: TdxGPGraphics);
    procedure CalculateMinorTicks(AMajorTick: TdxGaugeScaleTick; ATickDrawParameters:
      TdxGaugeScaleTickDrawParameters);
    procedure CalculateTicks;

    procedure DrawTicks(AGPGraphics: TdxGPGraphics);
  protected
    function GetScaleParametersClass: TdxGaugeCustomScaleParametersClass; override;

    procedure CalculateContent; override;
    procedure DrawLayer(AGPGraphics: TdxGPGraphics; ALayerIndex: Integer); override;
    procedure LoadScaleElements; override;
    procedure PopulateParameters(AScaleParameters: TdxGaugeCustomScaleParameters); override;

    function GetMajorTickStartPosition: Single; virtual; abstract;
    function GetMajorTicksPositionStep: Single; virtual; abstract;
    function GetMaxLabelValue: Single; virtual;
    function GetMinorTicksPositionStep: Single; virtual; abstract;
    procedure CalculateReferenceParameter; virtual; abstract;
    procedure DoDrawValueIndicator(AGPGraphics: TdxGPGraphics); virtual; abstract;
    procedure DrawLabel(AGPGraphics: TdxGPGraphics; AValue, AOffset: Single); virtual; abstract;
    procedure DrawTick(AGPGraphics: TdxGPGraphics; AValue: Single;
      const ATickDrawParameters: TdxGaugeScaleTickDrawParameters); virtual; abstract;
    procedure DrawElements(AGPGraphics: TdxGPGraphics); virtual;

    function CanDrawLabel(ADrawParameters: TdxGaugeScaleTickDrawParameters): Boolean;
    function GetTickDrawParameters(AIsMajorTicks: Boolean): TdxGaugeScaleTickDrawParameters;
    function GetLabelOffset: Single;
    function GetLabelText(AValue: Single): string;
    function GetMaxLabelSize: TdxSizeF;
    function GetMajorTickCount: Integer;
    function GetMaxLenghtLabelValue: Single;
    function GetMinorTickCount: Integer;
    function GetScaleValue: Single;
    function GetValueRange: Single;
    function GetValueStep: Single;
    function IsMajorTickVisible(AIndex: Integer): Boolean;
    procedure DrawValueIndicator(AGPGraphics: TdxGPGraphics);

    property Font: TFont read FFont;
  public
    constructor Create(AScaleType: TdxGaugeScaleType; const AStyleName: string); override;
    destructor Destroy; override;
  end;

  { TdxGaugeQuantitativeScale }

  TdxGaugeQuantitativeScale = class(TdxGaugeCustomScale)
  private
    FAssignedValues: TdxGaugeQuantitativeScaleParameterValues;
    FFont: TFont;
    FFontLockCount: Integer;
    FRanges: TdxGaugeRangeCollection;

    function GetFont: TFont;
    function GetMajorTickCount: Integer;
    function GetMaxValue: Single;
    function GetMinorTickCount: Integer;
    function GetMinValue: Single;
    function GetShowFirstTick: Boolean;
    function GetShowLabels: Boolean;
    function GetShowLastTick: Boolean;
    function GetShowTicks: Boolean;
    function GetValue: Single;
    procedure SetFont(const AValue: TFont);
    procedure SetMajorTickCount(const AValue: Integer);
    procedure SetMaxValue(const AValue: Single);
    procedure SetMinorTickCount(const AValue: Integer);
    procedure SetMinValue(const AValue: Single);
    procedure SetRanges(const AValue: TdxGaugeRangeCollection);
    procedure SetShowFirstTick(const AValue: Boolean);
    procedure SetShowLabels(const AValue: Boolean);
    procedure SetShowLastTick(const AValue: Boolean);
    procedure SetShowTicks(const AValue: Boolean);
    procedure SetValue(const AValue: Single);

    procedure FontChangeHandler(ASender: TObject);
    procedure RangeChangeHandler(Sender: TObject; AItem: TcxComponentCollectionItem;
      AAction: TcxComponentCollectionNotification);

    function GetScaleParameters: TdxGaugeQuantitativeScaleParameters;
    function GetViewInfo: TdxGaugeQuantitativeScaleViewInfo;
    function IsReadingComponent: Boolean;
    procedure ApplyStyleFontParameters;
    procedure CalculateRanges;
    procedure CreateFont;
    procedure CreateRanges;
    procedure UpdateScaleValue;

    function IsFontStored: Boolean;
    function IsMajorTickCountStored: Boolean;
    function IsMaxValueStored: Boolean;
    function IsMinorTickCountStored: Boolean;
    function IsShowFirstTickStored: Boolean;
    function IsShowLabelsStored: Boolean;
    function IsShowLastTickStored: Boolean;
    function IsShowTicksStored: Boolean;
  protected
    function GetOptionsViewClass: TdxGaugeCustomScaleOptionsViewClass; override;
    function GetScaleParametersClass: TdxGaugeCustomScaleParametersClass; override;
    function GetViewInfoClass: TdxGaugeCustomScaleViewInfoClass; override;

    function GetValidValue(const AValue: Variant): Variant; override;
    procedure ApplyStyleParameters; override;
    procedure Calculate(const AScaleBounds: TdxRectF; const ACachBounds: TRect); override;
    procedure DoAssign(AScale: TdxGaugeCustomScale); override;
    procedure DoDrawLayer(AGPGraphics: TdxGPGraphics; ALayerIndex: Integer); override;
    procedure GetChildren(AProc: TGetChildProc; ARoot: TComponent); override;
    procedure InitScaleParameters; override;
    procedure InternalRestoreStyleParameters; override;
    procedure Loaded; override;

    function GetRangeClass: TdxGaugeCustomRangeClass; virtual; abstract;
    procedure PopulateRangeScaleInfo(ARangeIndex: Integer); virtual;

    function GetShowValueIndicator: Boolean;
    procedure SetShowValueIndicator(const AValue: Boolean);

    procedure DrawRanges(AGPGraphics: TdxGPGraphics; ALayerIndex: Integer);

    property Font: TFont read GetFont;
    property Ranges: TdxGaugeRangeCollection read FRanges write SetRanges;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    property Value: Single read GetValue write SetValue;
  end;

  { TdxGaugeQuantitativeScaleStyleReader }

  TdxGaugeQuantitativeScaleStyleReader = class(TdxGaugeCustomScaleStyleReader)
  protected
    procedure ReadCustomQuantitativeScaleParameters(ANode: TdxXMLNode; AParameters: TdxGaugeQuantitativeScaleDefaultParameters);
    procedure ReadFontParameters(ANode: TdxXMLNode; AParameters: TdxGaugeCustomScaleDefaultParameters);
    procedure ReadLabelsParameters(ANode: TdxXMLNode; AParameters: TdxGaugeQuantitativeScaleDefaultParameters);
    procedure ReadMajorTicksParameters(ANode: TdxXMLNode; AParameters: TdxGaugeQuantitativeScaleDefaultParameters);
    procedure ReadMinorTicksParameters(ANode: TdxXMLNode; AParameters: TdxGaugeQuantitativeScaleDefaultParameters);
    procedure ReadTicksVisibility(ANode: TdxXMLNode; AParameters: TdxGaugeQuantitativeScaleDefaultParameters);
  protected
    function GetDefaultParametersClass: TdxGaugeCustomScaleDefaultParametersClass; override;
    procedure ReadParameters(ANode: TdxXMLNode; AParameters: TdxGaugeCustomScaleDefaultParameters); override;

    procedure ReadCommonScaleParameters(ANode: TdxXMLNode;
      AParameters: TdxGaugeQuantitativeScaleDefaultParameters); virtual;
  end;

  { TdxGaugeCustomRangeViewInfo }

  TdxGaugeCustomRangeViewInfo = class
  protected
    FBounds: TdxRectF;
    FRangeParameters: TdxGaugeCustomRangeParameters;

    function GetRangeParametersClass: TdxGaugeCustomRangeParametersClass; virtual;
    procedure CalculateBounds; virtual; abstract;
    procedure DoDraw(AGPGraphics: TdxGPGraphics); virtual; abstract;

    procedure Calculate(ARangeParameters: TdxGaugeCustomRangeParameters);

    function GetValueRange: Single;
    procedure Draw(AGPGraphics: TdxGPGraphics);
  public
    constructor Create;
    destructor Destroy; override;
  end;

  { TdxGaugeCustomRange }

  TdxGaugeCustomRange = class(TcxComponentCollectionItem)
  private
    FRangeParameters: TdxGaugeCustomRangeParameters;
    FViewInfo: TdxGaugeCustomRangeViewInfo;

    function GetColor: TdxAlphaColor;
    function GetValueEnd: Single;
    function GetValueStart: Single;
    function GetVisible: Boolean;
    function GetWidthFactor: Single;
    procedure SetColor(const AValue: TdxAlphaColor);
    procedure SetValueEnd(const AValue: Single);
    procedure SetValueStart(const AValue: Single);
    procedure SetVisible(const AValue: Boolean);
    procedure SetWidthFactor(const AValue: Single);

    function GetValidValue(AValue: Single): Single;
    procedure DoAssign(ARange: TdxGaugeCustomRange);

    procedure ReadWidthFactor(AReader: TReader);
    procedure WriteWidthFactor(AWriter: TWriter);

    function IsColorStored: Boolean;
    function IsWidthFactorStored: Boolean;
  protected
    function GetCollectionFromParent(AParent: TComponent): TcxComponentCollection; override;
    procedure DefineProperties(AFiler: TFiler); override;

    function GetRangeParametersClass: TdxGaugeCustomRangeParametersClass; virtual;
    function GetViewInfoClass: TdxGaugeCustomRangeViewInfoClass; virtual;

    procedure Calculate;

    property ViewInfo: TdxGaugeCustomRangeViewInfo read FViewInfo;

    property Color: TdxAlphaColor read GetColor write SetColor stored IsColorStored;
    property RangeParameters: TdxGaugeCustomRangeParameters read FRangeParameters;
    property ValueEnd: Single read GetValueEnd write SetValueEnd;
    property ValueStart: Single read GetValueStart write SetValueStart;
    property Visible: Boolean read GetVisible write SetVisible default True;
    property WidthFactor: Single read GetWidthFactor write SetWidthFactor stored IsWidthFactorStored;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure Assign(ASource: TPersistent); override;
  end;

  { TdxGaugeRangeCollection }

  TdxGaugeRangeCollection = class(TcxComponentCollection)
  private
    function GetRange(AIndex: Integer): TdxGaugeCustomRange;
    procedure SetRange(AIndex: Integer; const AValue: TdxGaugeCustomRange);
  protected
    function GetItemPrefixName: string; override;

    property Items[Index: Integer]: TdxGaugeCustomRange read GetRange write SetRange; default;
  public
    procedure Assign(ASource: TPersistent); override;
  end;

implementation

uses
  Types, Math, dxCore, cxFormats, dxGaugeUtils;

const
  dxGaugeCustomRangeWidthFactor = 0.1;
  dxGaugeCustomScaleRangeColor = clDefault;

type
  TdxGaugeScaleStyleAccess = class(TdxGaugeScaleStyle);

{ TdxGaugeCustomRangeParameters }

procedure TdxGaugeCustomRangeParameters.Assign(ASource: TdxGaugeCustomRangeParameters);
begin
  cxCopyData(ASource, Self, SizeOf(ASource), SizeOf(Self), ASource.InstanceSize -  SizeOf(ASource));
end;

{ TdxGaugeQuantitativeScaleOptionsView }

function TdxGaugeQuantitativeScaleOptionsView.GetShowValueIndicator: Boolean;
begin
  Result := GetScale.GetShowValueIndicator;
end;

procedure TdxGaugeQuantitativeScaleOptionsView.SetShowValueIndicator(const AValue: Boolean);
begin
  GetScale.SetShowValueIndicator(AValue);
end;

function TdxGaugeQuantitativeScaleOptionsView.GetMajorTickCount: Integer;
begin
  Result := GetScale.GetMajorTickCount;
end;

function TdxGaugeQuantitativeScaleOptionsView.GetMaxValue: Single;
begin
  Result := GetScale.GetMaxValue;
end;

function TdxGaugeQuantitativeScaleOptionsView.GetMinorTickCount: Integer;
begin
  Result := GetScale.GetMinorTickCount;
end;

function TdxGaugeQuantitativeScaleOptionsView.GetMinValue: Single;
begin
  Result := GetScale.GetMinValue;
end;

function TdxGaugeQuantitativeScaleOptionsView.GetFont: TFont;
begin
  Result := GetScale.FFont;
end;

function TdxGaugeQuantitativeScaleOptionsView.GetShowFirstTick: Boolean;
begin
  Result := GetScale.GetShowFirstTick;
end;

function TdxGaugeQuantitativeScaleOptionsView.GetShowLabels: Boolean;
begin
  Result := GetScale.GetShowLabels;
end;

function TdxGaugeQuantitativeScaleOptionsView.GetShowLastTick: Boolean;
begin
  Result := GetScale.GetShowLastTick;
end;

function TdxGaugeQuantitativeScaleOptionsView.GetShowTicks: Boolean;
begin
  Result := GetScale.GetShowTicks;
end;

procedure TdxGaugeQuantitativeScaleOptionsView.SetMajorTickCount(const AValue: Integer);
begin
  GetScale.SetMajorTickCount(AValue);
end;

procedure TdxGaugeQuantitativeScaleOptionsView.SetMaxValue(const AValue: Single);
begin
  GetScale.SetMaxValue(AValue);
end;

procedure TdxGaugeQuantitativeScaleOptionsView.SetMinorTickCount(const AValue: Integer);
begin
  GetScale.SetMinorTickCount(AValue);
end;

procedure TdxGaugeQuantitativeScaleOptionsView.SetMinValue(const AValue: Single);
begin
  GetScale.SetMinValue(AValue);
end;

procedure TdxGaugeQuantitativeScaleOptionsView.SetFont(const AValue: TFont);
begin
  GetScale.SetFont(AValue);
end;

procedure TdxGaugeQuantitativeScaleOptionsView.SetShowFirstTick(const AValue: Boolean);
begin
  GetScale.SetShowFirstTick(AValue);
end;

procedure TdxGaugeQuantitativeScaleOptionsView.SetShowLabels(const AValue: Boolean);
begin
  GetScale.SetShowLabels(AValue);
end;

procedure TdxGaugeQuantitativeScaleOptionsView.SetShowLastTick(const AValue: Boolean);
begin
  GetScale.SetShowLastTick(AValue);
end;

procedure TdxGaugeQuantitativeScaleOptionsView.SetShowTicks(const AValue: Boolean);
begin
  GetScale.SetShowTicks(AValue);
end;

function TdxGaugeQuantitativeScaleOptionsView.GetScale: TdxGaugeQuantitativeScale;
begin
  Result := inherited GetScale as TdxGaugeQuantitativeScale;
end;

function TdxGaugeQuantitativeScaleOptionsView.IsMajorTickCountStored: Boolean;
begin
  Result := GetScale.IsMajorTickCountStored;
end;

function TdxGaugeQuantitativeScaleOptionsView.IsMaxValueStored: Boolean;
begin
  Result := GetScale.IsMaxValueStored;
end;

function TdxGaugeQuantitativeScaleOptionsView.IsMinorTickCountStored: Boolean;
begin
  Result := GetScale.IsMinorTickCountStored;
end;

function TdxGaugeQuantitativeScaleOptionsView.IsFontStored: Boolean;
begin
  Result := GetScale.IsFontStored;
end;

function TdxGaugeQuantitativeScaleOptionsView.IsShowFirstTickStored: Boolean;
begin
  Result := GetScale.IsShowFirstTickStored;
end;

function TdxGaugeQuantitativeScaleOptionsView.IsShowLabelsStored: Boolean;
begin
  Result := GetScale.IsShowLabelsStored;
end;

function TdxGaugeQuantitativeScaleOptionsView.IsShowLastTickStored: Boolean;
begin
  Result := GetScale.IsShowLastTickStored;
end;

function TdxGaugeQuantitativeScaleOptionsView.IsShowTicksStored: Boolean;
begin
  Result := GetScale.IsShowTicksStored;
end;

{ TdxGaugeQuantitativeScaleViewInfo }

constructor TdxGaugeQuantitativeScaleViewInfo.Create(AScaleType: TdxGaugeScaleType; const AStyleName: string);
begin
  inherited Create(AScaleType, AStyleName);
  FFont := TFont.Create;
  FMajorTicks := TdxGaugeScaleTicks.Create(True);
  FMinorTicks := TdxGaugeScaleTicks.Create(True);
end;

destructor TdxGaugeQuantitativeScaleViewInfo.Destroy;
begin
  FreeAndNil(FMinorTicks);
  FreeAndNil(FMajorTicks);
  FreeAndNil(FFont);
  inherited Destroy;
end;

function TdxGaugeQuantitativeScaleViewInfo.GetScaleParametersClass: TdxGaugeCustomScaleParametersClass;
begin
  Result := TdxGaugeQuantitativeScaleParameters;
end;

procedure TdxGaugeQuantitativeScaleViewInfo.CalculateContent;
begin
  inherited CalculateContent;
  CalculateReferenceParameter;
  CalculateTicks;
  CalculateLabelPrecision;
  CalculateFontSize;
end;

procedure TdxGaugeQuantitativeScaleViewInfo.DrawLayer(AGPGraphics: TdxGPGraphics; ALayerIndex: Integer);
begin
  inherited DrawLayer(AGPGraphics, ALayerIndex);
  case ALayerIndex of
    1:
      DrawElements(AGPGraphics);
    2:
      DrawValueIndicator(AGPGraphics);
  end;
end;

procedure TdxGaugeQuantitativeScaleViewInfo.LoadScaleElements;
begin
  inherited LoadScaleElements;
  FMajorTick := TdxGaugeScaleStyleAccess(Style).GetElement(GetScaleDefaultParameters.MajorTickType);
  FMinorTick := TdxGaugeScaleStyleAccess(Style).GetElement(GetScaleDefaultParameters.MinorTickType);
end;

procedure TdxGaugeQuantitativeScaleViewInfo.PopulateParameters(AScaleParameters: TdxGaugeCustomScaleParameters);
begin
  inherited PopulateParameters(AScaleParameters);
  FFont.Assign((AScaleParameters as TdxGaugeQuantitativeScaleParameters).Font);
end;

function TdxGaugeQuantitativeScaleViewInfo.GetMaxLabelValue: Single;
begin
  Result := RoundTo((GetScaleParameters.MaxValue + GetValueStep) / 10, -4);
end;

procedure TdxGaugeQuantitativeScaleViewInfo.DrawElements(AGPGraphics: TdxGPGraphics);
begin
  DrawTicks(AGPGraphics);
end;

function TdxGaugeQuantitativeScaleViewInfo.CanDrawLabel(ADrawParameters: TdxGaugeScaleTickDrawParameters): Boolean;
begin
  Result := ADrawParameters.ShowLabel and GetScaleParameters.ShowLabels;
end;

function TdxGaugeQuantitativeScaleViewInfo.GetTickDrawParameters(AIsMajorTicks: Boolean): TdxGaugeScaleTickDrawParameters;
var
  AScaleDefaultParameters: TdxGaugeQuantitativeScaleDefaultParameters;
begin
  AScaleDefaultParameters := GetScaleDefaultParameters;
  Result.ShowLabel := AIsMajorTicks;
  Result.ShowTick := GetScaleParameters.ShowTicks;
  if AIsMajorTicks then
  begin
    Result.Image := FMajorTick;
    Result.Offset := AScaleDefaultParameters.MajorTickOffset;
    Result.ScaleFactor := AScaleDefaultParameters.MajorTickScaleFactor;
    Result.Size := GetElementImageSize(Result.Image, AScaleDefaultParameters.MajorTickScaleFactor);
  end
  else
  begin
    Result.Image := FMinorTick;
    Result.Offset := AScaleDefaultParameters.MinorTickOffset;
    Result.ScaleFactor := AScaleDefaultParameters.MinorTickScaleFactor;
    Result.Size := GetElementImageSize(Result.Image, AScaleDefaultParameters.MinorTickScaleFactor);
  end;
  Result.OriginalSize := GetElementImageOriginalSize(Result.Image);
end;

function TdxGaugeQuantitativeScaleViewInfo.GetLabelOffset: Single;
begin
  Result := FLabelOffset;
end;

function TdxGaugeQuantitativeScaleViewInfo.GetLabelText(AValue: Single): string;
begin
  Result := Format('%*.*f', [Pos(dxFormatSettings.DecimalSeparator,
    dxFloatToStr(AValue, dxFormatSettings.DecimalSeparator)), FLabelPrecision, AValue]);
end;

function TdxGaugeQuantitativeScaleViewInfo.GetMaxLabelSize: TdxSizeF;
begin
  Result := FMaxLabelSize;
end;

function TdxGaugeQuantitativeScaleViewInfo.GetMajorTickCount: Integer;
begin
  Result := GetScaleParameters.MajorTickCount - 1;
end;

function TdxGaugeQuantitativeScaleViewInfo.GetMaxLenghtLabelValue: Single;
var
  I: Integer;
begin
  Result := 0;
  for I := 0 to FMajorTicks.Count - 1 do
    if Length(dxFloatToStr(FMajorTicks[I].Value)) > Length(dxFloatToStr(Result)) then
      Result := FMajorTicks[I].Value;
end;

function TdxGaugeQuantitativeScaleViewInfo.GetMinorTickCount: Integer;
begin
  Result := GetScaleParameters.MinorTickCount + 1;
end;

function TdxGaugeQuantitativeScaleViewInfo.GetScaleValue: Single;
begin
  Result := GetScaleParameters.Value;
end;

function TdxGaugeQuantitativeScaleViewInfo.GetValueRange: Single;
begin
  Result := dxGaugeValueRange(GetScaleParameters.MaxValue, GetScaleParameters.MinValue);
end;

function TdxGaugeQuantitativeScaleViewInfo.GetValueStep: Single;
begin
  Result := GetValueRange / GetMajorTickCount;
end;

function TdxGaugeQuantitativeScaleViewInfo.IsMajorTickVisible(AIndex: Integer): Boolean;
var
  AScaleParameters: TdxGaugeQuantitativeScaleParameters;
  ACanDrawLastTick, ACanDrawFirstTick: Boolean;
begin
  AScaleParameters := GetScaleParameters;
  ACanDrawFirstTick := (AIndex = 0) and AScaleParameters.ShowFirstTick;
  ACanDrawLastTick := (AIndex = GetMajorTickCount) and AScaleParameters.ShowLastTick;
  Result := ((AIndex <> 0) or ACanDrawFirstTick) and ((AIndex <> GetMajorTickCount) or ACanDrawLastTick);
end;

procedure TdxGaugeQuantitativeScaleViewInfo.DrawValueIndicator(AGPGraphics: TdxGPGraphics);
begin
  if GetScaleParameters.ShowValueIndicator then
    DoDrawValueIndicator(AGPGraphics);
end;

function TdxGaugeQuantitativeScaleViewInfo.CreateTick(AValue: Single; AVisible: Boolean):
  TdxGaugeScaleTick;
begin
  Result := TdxGaugeScaleTick.Create;
  Result.Value := AValue;
  Result.Visible := AVisible;
end;

function TdxGaugeQuantitativeScaleViewInfo.GetScaleDefaultParameters: TdxGaugeQuantitativeScaleDefaultParameters;
begin
  Result := ScaleDefaultParameters as TdxGaugeQuantitativeScaleDefaultParameters;
end;

function TdxGaugeQuantitativeScaleViewInfo.GetScaleParameters: TdxGaugeQuantitativeScaleParameters;
begin
  Result := FScaleParameters as TdxGaugeQuantitativeScaleParameters;
end;

procedure TdxGaugeQuantitativeScaleViewInfo.CalculateFontSize;
begin
  Font.Size := Max(Round(Font.Size * Min(FScaleFactor.X, FScaleFactor.Y)), 1);
end;

procedure TdxGaugeQuantitativeScaleViewInfo.CalculateLabelOffset;
var
  ASize: Single;
begin
  ASize := FMaxLabelSize.cx / 2;
  if GetScaleDefaultParameters.LabelOffset < 0 then
    ASize := -ASize;
  FLabelOffset := ASize + GetScaleDefaultParameters.LabelOffset * FScaleFactor.X;
end;

procedure TdxGaugeQuantitativeScaleViewInfo.CalculateLabelPrecision;
var
  ARoundingValue: string;
  AFormatSettings: TFormatSettings;
  ADecimalSeparatorPosition: Integer;
begin
  dxGetLocaleFormatSettings(dxGetInvariantLocaleID, AFormatSettings);
  ARoundingValue := dxFloatToStr(RoundTo(GetMaxLenghtLabelValue / 10, -4),
    AFormatSettings.DecimalSeparator);
  ADecimalSeparatorPosition := Pos(AFormatSettings.DecimalSeparator, ARoundingValue);
  if ADecimalSeparatorPosition > 0 then
    FLabelPrecision := Length(Copy(ARoundingValue, ADecimalSeparatorPosition + 1, MaxInt)) - 1
  else
    FLabelPrecision := 0;
end;

procedure TdxGaugeQuantitativeScaleViewInfo.CalculateMaxLabelSize(AGPGraphics: TdxGPGraphics);
var
  ATextRect: TdxRectF;
begin
  dxGPGetTextRect(AGPGraphics, GetLabelText(GetMaxLabelValue), FFont, False, cxRectF(cxNullRect), ATextRect);
  FMaxLabelSize := dxSizeF(ATextRect.Width, ATextRect.Height);
end;

procedure TdxGaugeQuantitativeScaleViewInfo.CalculateTicks;
var
  I: Integer;
  AValue, AValueStep: Single;
  AMajorTickDrawParameters, AMinorTickDrawParameters: TdxGaugeScaleTickDrawParameters;
  ATick: TdxGaugeScaleTick;
begin
  FMajorTicks.Clear;
  FMinorTicks.Clear;
  AValue := GetScaleParameters.MinValue;
  AValueStep := GetValueStep;
  AMajorTickDrawParameters := GetTickDrawParameters(True);
  AMinorTickDrawParameters := GetTickDrawParameters(False);
  for I := 0 to GetMajorTickCount do
  begin
    ATick := CreateTick(AValue, IsMajorTickVisible(I));
    FMajorTicks.Add(ATick);
    CalculateMinorTicks(ATick, AMinorTickDrawParameters);
    AValue := AValue + AValueStep;
  end;
end;

procedure TdxGaugeQuantitativeScaleViewInfo.CalculateMinorTicks(AMajorTick: TdxGaugeScaleTick; ATickDrawParameters:
  TdxGaugeScaleTickDrawParameters);

  function AllowOverlapMajorTicks: Boolean;
  begin
    Result := (GetScaleDefaultParameters.AllowOverlapMajorTicks and (GetScaleParameters.MinorTickCount > 0));
  end;

  function GetTickVisible(AIndex: Integer): Boolean;
  begin
    Result := (AIndex <> 0) and (AIndex < GetMinorTickCount) or AllowOverlapMajorTicks;
  end;

  function IsLastMajorTick: Boolean;
  begin
    Result := FMajorTicks.IndexOf(AMajorTick) = GetMajorTickCount;
  end;

var
  I: Integer;
  AValue: Single;
begin
  AValue := AMajorTick.Value;
  if IsLastMajorTick then
    FMinorTicks.Add(CreateTick(AValue, AllowOverlapMajorTicks))
  else
    for I := 0 to GetMinorTickCount do
    begin
      FMinorTicks.Add(CreateTick(AValue, GetTickVisible(I)));
      AValue := AValue + GetValueStep / GetMinorTickCount;
    end;
end;

procedure TdxGaugeQuantitativeScaleViewInfo.DrawTicks(AGPGraphics: TdxGPGraphics);

  procedure InternalDrawTicks(ATicks: TdxGaugeScaleTicks; AIsMajorTicks: Boolean);
  var
    I: Integer;
    ATickDrawParameters: TdxGaugeScaleTickDrawParameters;
  begin
    ATickDrawParameters := GetTickDrawParameters(AIsMajorTicks);
    for I := 0 to ATicks.Count - 1 do
      if ATicks[I].Visible then
        DrawTick(AGPGraphics, ATicks[I].Value, ATickDrawParameters);
  end;

begin
  if GetScaleParameters.ShowTicks or GetScaleParameters.ShowLabels then
  begin
    CalculateMaxLabelSize(AGPGraphics);
    CalculateLabelOffset;
    InternalDrawTicks(FMajorTicks, True);
    InternalDrawTicks(FMinorTicks, False);
  end;
end;

{ TdxGaugeQuantitativeScale }

constructor TdxGaugeQuantitativeScale.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  CreateFont;
  CreateRanges;
  ApplyStyleFontParameters;
end;

destructor TdxGaugeQuantitativeScale.Destroy;
begin
  FreeAndNil(FRanges);
  FreeAndNil(FFont);
  inherited Destroy;
end;

function TdxGaugeQuantitativeScale.GetOptionsViewClass: TdxGaugeCustomScaleOptionsViewClass;
begin
  Result := TdxGaugeQuantitativeScaleOptionsView;
end;

function TdxGaugeQuantitativeScale.GetScaleParametersClass: TdxGaugeCustomScaleParametersClass;
begin
  Result := TdxGaugeQuantitativeScaleParameters;
end;

function TdxGaugeQuantitativeScale.GetViewInfoClass: TdxGaugeCustomScaleViewInfoClass;
begin
  Result := TdxGaugeQuantitativeScaleViewInfo;
end;

function TdxGaugeQuantitativeScale.GetValidValue(const AValue: Variant): Variant;
begin
  Result := Min(Max(AValue, GetScaleParameters.MinValue), GetScaleParameters.MaxValue);
end;

procedure TdxGaugeQuantitativeScale.ApplyStyleParameters;
var
  AParameters: TdxGaugeQuantitativeScaleParameters;
  ADefaultParameters: TdxGaugeQuantitativeScaleDefaultParameters;
begin
  ApplyStyleFontParameters;
  AParameters := GetScaleParameters;
  ADefaultParameters := GetViewInfo.GetScaleDefaultParameters;
  if not(spvShowFirstTick in FAssignedValues) then
    AParameters.ShowFirstTick := ADefaultParameters.ShowFirstTick;
  if not(spvShowLabels in FAssignedValues) then
    AParameters.ShowLabels := ADefaultParameters.ShowLabels;
  if not(spvShowLastTick in FAssignedValues) then
    AParameters.ShowLastTick := ADefaultParameters.ShowLastTick;
  if not(spvShowTicks in FAssignedValues) then
    AParameters.ShowTicks := ADefaultParameters.ShowTicks;
  if not(spvMajorTickCount in FAssignedValues) then
    AParameters.MajorTickCount := ADefaultParameters.MajorTickCount;
  if not(spvMinorTickCount in FAssignedValues) then
    AParameters.MinorTickCount := ADefaultParameters.MinorTickCount;
end;

procedure TdxGaugeQuantitativeScale.Calculate(const AScaleBounds: TdxRectF; const ACachBounds: TRect);
begin
  GetScaleParameters.Font := FFont;
  inherited Calculate(AScaleBounds, ACachBounds);
  CalculateRanges;
end;

procedure TdxGaugeQuantitativeScale.DoAssign(AScale: TdxGaugeCustomScale);
begin
  inherited DoAssign(AScale);
  Font.Assign((AScale as TdxGaugeQuantitativeScale).Font);
  Ranges.Assign((AScale as TdxGaugeQuantitativeScale).Ranges);
  FAssignedValues := (AScale as TdxGaugeQuantitativeScale).FAssignedValues;
end;

procedure TdxGaugeQuantitativeScale.DoDrawLayer(AGPGraphics: TdxGPGraphics; ALayerIndex: Integer);
begin
  DrawRanges(AGPGraphics, ALayerIndex);
  inherited DoDrawLayer(AGPGraphics, ALayerIndex);
end;

procedure TdxGaugeQuantitativeScale.GetChildren(AProc: TGetChildProc; ARoot: TComponent);
var
  I: Integer;
begin
  for I := 0 to Ranges.Count - 1 do
    if Ranges[I].Owner = ARoot then
      AProc(Ranges[I]);
end;

procedure TdxGaugeQuantitativeScale.InitScaleParameters;
begin
  inherited InitScaleParameters;
  GetScaleParameters.MaxValue := 100;
  GetScaleParameters.MinValue := 0;
  GetScaleParameters.Value := GetScaleParameters.MinValue;
  GetScaleParameters.ShowLabels := True;
  GetScaleParameters.ShowValueIndicator := True;
end;

procedure TdxGaugeQuantitativeScale.InternalRestoreStyleParameters;
begin
  FAssignedValues := [];
  GetScaleParameters.ShowValueIndicator := True;
  GetScaleParameters.ShowBackground := True;
  inherited InternalRestoreStyleParameters;
end;

procedure TdxGaugeQuantitativeScale.Loaded;
var
  AStyleName: string;
  AStoredFont: TFont;
begin
  inherited Loaded;
  if spvFontSize in FAssignedValues then
  begin
    Collection.BeginUpdate;
    AStoredFont := TFont.Create;
    try
      AStyleName := StyleName;
      AStoredFont.Assign(Font);
      StyleName := '';
      StyleName := AStyleName;
      FreeAndNil(FFont);
      CreateFont;
      Font.Assign(AStoredFont);
    finally
      AStoredFont.Free;
      Collection.EndUpdate;
    end;
  end;
end;

procedure TdxGaugeQuantitativeScale.PopulateRangeScaleInfo(ARangeIndex: Integer);
begin
  Ranges[ARangeIndex].RangeParameters.ScaleBounds := GetViewInfo.GetContentBounds;
  Ranges[ARangeIndex].RangeParameters.ScaleMaxValue := GetMaxValue;
  Ranges[ARangeIndex].RangeParameters.ScaleMinValue := GetMinValue;
end;

function TdxGaugeQuantitativeScale.GetShowValueIndicator: Boolean;
begin
  Result := GetScaleParameters.ShowValueIndicator;
end;

procedure TdxGaugeQuantitativeScale.SetShowValueIndicator(const AValue: Boolean);
begin
  if GetScaleParameters.ShowValueIndicator <> AValue then
  begin
    GetScaleParameters.ShowValueIndicator := AValue;
    ScaleChanged([sclDynamicLayer, sclStaticLayer]);
  end;
end;

procedure TdxGaugeQuantitativeScale.DrawRanges(AGPGraphics: TdxGPGraphics; ALayerIndex: Integer);
var
  I: Integer;
begin
  if ALayerIndex = 1 then
    for I := 0 to Ranges.Count - 1 do
      Ranges[I].ViewInfo.Draw(AGPGraphics);
end;

function TdxGaugeQuantitativeScale.GetFont: TFont;
begin
  Result := FFont;
end;

function TdxGaugeQuantitativeScale.GetMajorTickCount: Integer;
begin
  Result := GetScaleParameters.MajorTickCount;
end;

function TdxGaugeQuantitativeScale.GetMaxValue: Single;
begin
  Result := GetScaleParameters.MaxValue;
end;

function TdxGaugeQuantitativeScale.GetMinorTickCount: Integer;
begin
  Result := GetScaleParameters.MinorTickCount;
end;

function TdxGaugeQuantitativeScale.GetMinValue: Single;
begin
  Result := GetScaleParameters.MinValue;
end;

function TdxGaugeQuantitativeScale.GetShowFirstTick: Boolean;
begin
  Result := GetScaleParameters.ShowFirstTick;
end;

function TdxGaugeQuantitativeScale.GetShowLabels: Boolean;
begin
  Result := GetScaleParameters.ShowLabels;
end;

function TdxGaugeQuantitativeScale.GetShowLastTick: Boolean;
begin
  Result := GetScaleParameters.ShowLastTick;
end;

function TdxGaugeQuantitativeScale.GetShowTicks: Boolean;
begin
  Result := GetScaleParameters.ShowTicks;
end;

function TdxGaugeQuantitativeScale.GetValue: Single;
begin
  Result := ScaleParameters.Value;
end;

procedure TdxGaugeQuantitativeScale.SetFont(const AValue: TFont);
begin
  FFont.Assign(AValue);
end;

procedure TdxGaugeQuantitativeScale.SetMajorTickCount(const AValue: Integer);
begin
  if GetScaleParameters.MajorTickCount <> AValue then
  begin
    GetScaleParameters.MajorTickCount := Max(AValue, 2);
    if GetScaleParameters.MajorTickCount = GetViewInfo.GetScaleDefaultParameters.MajorTickCount then
      Exclude(FAssignedValues, spvMajorTickCount)
    else
      Include(FAssignedValues, spvMajorTickCount);
    ScaleChanged([sclStaticLayer]);
  end;
end;

procedure TdxGaugeQuantitativeScale.SetMaxValue(const AValue: Single);
var
  AParameters: TdxGaugeQuantitativeScaleParameters;
begin
  AParameters := GetScaleParameters;
  if (AParameters.MaxValue <> AValue) and ((AValue > AParameters.MinValue) or IsReadingComponent) then
  begin
    AParameters.MaxValue := AValue;
    UpdateScaleValue;
    ScaleChanged([sclStaticLayer, sclDynamicLayer]);
  end;
end;

procedure TdxGaugeQuantitativeScale.SetMinorTickCount(const AValue: Integer);
begin
  if (GetScaleParameters.MinorTickCount <> AValue) and (AValue >= 0) then
  begin
    GetScaleParameters.MinorTickCount := AValue;
    if GetScaleParameters.MinorTickCount = GetViewInfo.GetScaleDefaultParameters.MinorTickCount then
      Exclude(FAssignedValues, spvMinorTickCount)
    else
      Include(FAssignedValues, spvMinorTickCount);
    ScaleChanged([sclStaticLayer]);
  end;
end;

procedure TdxGaugeQuantitativeScale.SetMinValue(const AValue: Single);
var
  AParameters: TdxGaugeQuantitativeScaleParameters;
begin
  AParameters := GetScaleParameters;
  if (AParameters.MinValue <> AValue) and ((AValue < AParameters.MaxValue) or IsReadingComponent) then
  begin
    AParameters.MinValue := AValue;
    UpdateScaleValue;
    ScaleChanged([sclStaticLayer, sclDynamicLayer]);
  end;
end;

procedure TdxGaugeQuantitativeScale.SetRanges(const AValue: TdxGaugeRangeCollection);
begin
  FRanges.Assign(AValue);
end;

procedure TdxGaugeQuantitativeScale.SetShowFirstTick(const AValue: Boolean);
begin
  if GetScaleParameters.ShowFirstTick <> AValue then
  begin
    GetScaleParameters.ShowFirstTick := AValue;
    if GetScaleParameters.ShowFirstTick = GetViewInfo.GetScaleDefaultParameters.ShowFirstTick then
      Exclude(FAssignedValues, spvShowFirstTick)
    else
      Include(FAssignedValues, spvShowFirstTick);
    ScaleChanged([sclStaticLayer]);
  end;
end;

procedure TdxGaugeQuantitativeScale.SetShowLabels(const AValue: Boolean);
begin
  if GetScaleParameters.ShowLabels <> AValue then
  begin
    GetScaleParameters.ShowLabels := AValue;
    if GetScaleParameters.ShowLabels = GetViewInfo.GetScaleDefaultParameters.ShowLabels then
      Exclude(FAssignedValues, spvShowLabels)
    else
      Include(FAssignedValues, spvShowLabels);
    ScaleChanged([sclStaticLayer]);
  end;
end;

procedure TdxGaugeQuantitativeScale.SetShowLastTick(const AValue: Boolean);
begin
  if GetScaleParameters.ShowLastTick <> AValue then
  begin
    GetScaleParameters.ShowLastTick := AValue;
    if GetScaleParameters.ShowLastTick = GetViewInfo.GetScaleDefaultParameters.ShowLastTick then
      Exclude(FAssignedValues, spvShowLastTick)
    else
      Include(FAssignedValues, spvShowLastTick);
    ScaleChanged([sclStaticLayer]);
  end;
end;

procedure TdxGaugeQuantitativeScale.SetShowTicks(const AValue: Boolean);
begin
  if GetScaleParameters.ShowTicks <> AValue then
  begin
    GetScaleParameters.ShowTicks := AValue;
    if GetScaleParameters.ShowTicks = GetViewInfo.GetScaleDefaultParameters.ShowLastTick then
      Exclude(FAssignedValues, spvShowTicks)
    else
      Include(FAssignedValues, spvShowTicks);
    ScaleChanged([sclStaticLayer]);
  end;
end;

procedure TdxGaugeQuantitativeScale.SetValue(const AValue: Single);
begin
  InternalSetValue(AValue);
end;

procedure TdxGaugeQuantitativeScale.FontChangeHandler(ASender: TObject);
begin
  if FFontLockCount = 0 then
  begin
    if FFont.Name = GetViewInfo.GetScaleDefaultParameters.FontName then
      Exclude(FAssignedValues, spvFontName)
    else
      Include(FAssignedValues, spvFontName);
    if FFont.Color = GetViewInfo.GetScaleDefaultParameters.FontColor then
      Exclude(FAssignedValues, spvFontColor)
    else
      Include(FAssignedValues, spvFontColor);
    if not IsReadingComponent and (Font.Size = GetViewInfo.GetScaleDefaultParameters.FontSize) then
      Exclude(FAssignedValues, spvFontSize)
    else
      Include(FAssignedValues, spvFontSize);
    ScaleChanged([sclStaticLayer]);
  end;
end;

procedure TdxGaugeQuantitativeScale.RangeChangeHandler(Sender: TObject; AItem: TcxComponentCollectionItem;
  AAction: TcxComponentCollectionNotification);
begin
  ScaleChanged([sclStaticLayer]);
end;

function TdxGaugeQuantitativeScale.GetScaleParameters: TdxGaugeQuantitativeScaleParameters;
begin
  Result := ScaleParameters as TdxGaugeQuantitativeScaleParameters;
end;

function TdxGaugeQuantitativeScale.GetViewInfo: TdxGaugeQuantitativeScaleViewInfo;
begin
  Result := ViewInfo as TdxGaugeQuantitativeScaleViewInfo;
end;

function TdxGaugeQuantitativeScale.IsReadingComponent: Boolean;
begin
  Result := ComponentState * [csReading] <> [];
end;

procedure TdxGaugeQuantitativeScale.ApplyStyleFontParameters;
begin
  if Assigned(FFont) then
  begin
    Inc(FFontLockCount);
    if not(spvFontName in FAssignedValues) then
      FFont.Name := GetViewInfo.GetScaleDefaultParameters.FontName;
    if not IsReadingComponent and not (spvFontSize in FAssignedValues) then
      FFont.Size := GetViewInfo.GetScaleDefaultParameters.FontSize;
   if not(spvFontColor in FAssignedValues) then
      FFont.Color := GetViewInfo.GetScaleDefaultParameters.FontColor;
    Dec(FFontLockCount);
  end;
end;

procedure TdxGaugeQuantitativeScale.CalculateRanges;
var
  I: Integer;
begin
  for I := 0 to Ranges.Count - 1 do
  begin
    PopulateRangeScaleInfo(I);
    Ranges[I].Calculate;
  end;
end;

procedure TdxGaugeQuantitativeScale.CreateFont;
begin
  FFont := TFont.Create;
  FFont.OnChange := FontChangeHandler;
end;

procedure TdxGaugeQuantitativeScale.CreateRanges;
begin
  FRanges := TdxGaugeRangeCollection.Create(Self, GetRangeClass);
  FRanges.OnChange := RangeChangeHandler;
end;

procedure TdxGaugeQuantitativeScale.UpdateScaleValue;
begin
  GetScaleParameters.Value := Min(Max(GetScaleParameters.Value, GetScaleParameters.MinValue), GetScaleParameters.MaxValue);
end;

function TdxGaugeQuantitativeScale.IsFontStored: Boolean;
begin
  Result := (spvFontColor in FAssignedValues) or (spvFontName in FAssignedValues) or (spvFontSize in FAssignedValues);
end;

function TdxGaugeQuantitativeScale.IsMajorTickCountStored: Boolean;
begin
  Result := spvMajorTickCount in FAssignedValues;
end;

function TdxGaugeQuantitativeScale.IsMaxValueStored: Boolean;
begin
  Result := not SameValue(GetScaleParameters.MaxValue, 100);
end;

function TdxGaugeQuantitativeScale.IsMinorTickCountStored: Boolean;
begin
  Result := spvMinorTickCount in FAssignedValues;
end;

function TdxGaugeQuantitativeScale.IsShowFirstTickStored: Boolean;
begin
  Result := spvShowFirstTick in FAssignedValues;
end;

function TdxGaugeQuantitativeScale.IsShowLabelsStored: Boolean;
begin
  Result := spvShowLabels in FAssignedValues;
end;

function TdxGaugeQuantitativeScale.IsShowLastTickStored: Boolean;
begin
  Result := spvShowLastTick in FAssignedValues;
end;

function TdxGaugeQuantitativeScale.IsShowTicksStored: Boolean;
begin
  Result := spvShowTicks in FAssignedValues;
end;

{ TdxGaugeQuantitativeScaleStyleReader }

function TdxGaugeQuantitativeScaleStyleReader.GetDefaultParametersClass: TdxGaugeCustomScaleDefaultParametersClass;
begin
  Result := TdxGaugeQuantitativeScaleDefaultParameters;
end;

procedure TdxGaugeQuantitativeScaleStyleReader.ReadParameters(ANode: TdxXMLNode;
  AParameters: TdxGaugeCustomScaleDefaultParameters);
var
  AScaleParameters: TdxGaugeQuantitativeScaleDefaultParameters;
begin
  AScaleParameters := AParameters as TdxGaugeQuantitativeScaleDefaultParameters;
  ReadFontParameters(ANode, AScaleParameters);
  ReadLabelsParameters(ANode, AScaleParameters);
  ReadTicksVisibility(ANode, AScaleParameters);
  ReadMajorTicksParameters(ANode, AScaleParameters);
  ReadMinorTicksParameters(ANode, AScaleParameters);
  ReadCustomQuantitativeScaleParameters(ANode, AScaleParameters);
end;

procedure TdxGaugeQuantitativeScaleStyleReader.ReadCommonScaleParameters(ANode: TdxXMLNode;
  AParameters: TdxGaugeQuantitativeScaleDefaultParameters);
begin
//do nothing
end;

procedure TdxGaugeQuantitativeScaleStyleReader.ReadCustomQuantitativeScaleParameters(ANode: TdxXMLNode;
  AParameters: TdxGaugeQuantitativeScaleDefaultParameters);
begin
  ReadCommonScaleParameters(ANode, AParameters);
end;

procedure TdxGaugeQuantitativeScaleStyleReader.ReadFontParameters(ANode: TdxXMLNode;
  AParameters: TdxGaugeCustomScaleDefaultParameters);
var
  AFontNode: TdxXMLNode;
  AScaleDefaultParameters: TdxGaugeQuantitativeScaleDefaultParameters;
begin
  if GetChildNode(ANode, 'Font', AFontNode) then
  begin
    AScaleDefaultParameters := AParameters as TdxGaugeQuantitativeScaleDefaultParameters;
    AScaleDefaultParameters.FontColor := GetAttributeValueAsColor(GetChildNode(AFontNode, 'Color'), 'Value');
    AScaleDefaultParameters.FontName := GetAttributeValueAsString(GetChildNode(AFontNode, 'Name'), 'Value');
    AScaleDefaultParameters.FontSize := GetAttributeValueAsInteger(GetChildNode(AFontNode, 'Size'), 'Value');
  end;
end;

procedure TdxGaugeQuantitativeScaleStyleReader.ReadLabelsParameters(ANode: TdxXMLNode;
  AParameters: TdxGaugeQuantitativeScaleDefaultParameters);
var
  ALabelsNode: TdxXMLNode;
begin
  if GetChildNode(ANode, 'Labels', ALabelsNode) then
  begin
    AParameters.LabelOffset := GetAttributeValueAsDouble(GetChildNode(ALabelsNode, 'Offset'), 'Value');
    AParameters.ShowLabels := GetAttributeValueAsBoolean(GetChildNode(ALabelsNode, 'ShowLabels'), 'Value');
  end;
end;

procedure TdxGaugeQuantitativeScaleStyleReader.ReadMajorTicksParameters(ANode: TdxXMLNode;
  AParameters: TdxGaugeQuantitativeScaleDefaultParameters);
var
  AMajorTicksNode: TdxXMLNode;
begin
  if GetChildNode(ANode, 'MajorTicks', AMajorTicksNode) then
  begin
    AParameters.MajorTickCount := GetAttributeValueAsInteger(GetChildNode(AMajorTicksNode, 'Count'), 'Value');
    AParameters.MajorTickOffset := GetAttributeValueAsDouble(GetChildNode(AMajorTicksNode, 'Offset'), 'Value');
    AParameters.MajorTickScaleFactor := GetAttributeValueAsPointF(GetChildNode(AMajorTicksNode, 'ScaleFactor'), 'Value');
    AParameters.MajorTickType := GetAttributeValueAsElementType(GetChildNode(AMajorTicksNode, 'TickType'), 'Value');
  end;
end;

procedure TdxGaugeQuantitativeScaleStyleReader.ReadMinorTicksParameters(ANode: TdxXMLNode;
  AParameters: TdxGaugeQuantitativeScaleDefaultParameters);
var
  AMinorTicksNode: TdxXMLNode;
begin
  if GetChildNode(ANode, 'MinorTicks', AMinorTicksNode) then
  begin
    AParameters.MinorTickCount := GetAttributeValueAsInteger(GetChildNode(AMinorTicksNode, 'Count'), 'Value');
    AParameters.MinorTickOffset := GetAttributeValueAsDouble(GetChildNode(AMinorTicksNode, 'Offset'), 'Value');
    AParameters.MinorTickScaleFactor := GetAttributeValueAsPointF(GetChildNode(AMinorTicksNode, 'ScaleFactor'), 'Value');
    AParameters.MinorTickType := GetAttributeValueAsElementType(GetChildNode(AMinorTicksNode, 'TickType'), 'Value');
  end;
end;

procedure TdxGaugeQuantitativeScaleStyleReader.ReadTicksVisibility(ANode: TdxXMLNode;
  AParameters: TdxGaugeQuantitativeScaleDefaultParameters);
var
  ATicksVisibilityNode: TdxXMLNode;
begin
  if GetChildNode(ANode, 'TicksVisibility', ATicksVisibilityNode) then
  begin
    AParameters.AllowOverlapMajorTicks := GetAttributeValueAsBoolean(GetChildNode(ATicksVisibilityNode,
      'AllowOverlapMajorTicks'), 'Value');
    AParameters.ShowFirstTick := GetAttributeValueAsBoolean(GetChildNode(ATicksVisibilityNode, 'ShowFirstTick'), 'Value');
    AParameters.ShowLastTick := GetAttributeValueAsBoolean(GetChildNode(ATicksVisibilityNode, 'ShowLastTick'), 'Value');
    AParameters.ShowTicks := GetAttributeValueAsBoolean(GetChildNode(ATicksVisibilityNode, 'ShowTicks'), 'Value');
  end;
end;

{ TdxGaugeCustomRangeViewInfo }

constructor TdxGaugeCustomRangeViewInfo.Create;
begin
  inherited Create;
  FRangeParameters := GetRangeParametersClass.Create;
end;

destructor TdxGaugeCustomRangeViewInfo.Destroy;
begin
  FreeAndNil(FRangeParameters);
  inherited Destroy;
end;

function TdxGaugeCustomRangeViewInfo.GetRangeParametersClass: TdxGaugeCustomRangeParametersClass;
begin
  Result := TdxGaugeCustomRangeParameters;
end;

procedure TdxGaugeCustomRangeViewInfo.Calculate(ARangeParameters: TdxGaugeCustomRangeParameters);
begin
  FRangeParameters.Assign(ARangeParameters);
  CalculateBounds;
end;

function TdxGaugeCustomRangeViewInfo.GetValueRange: Single;
begin
  Result := dxGaugeValueRange(FRangeParameters.ScaleMaxValue, FRangeParameters.ScaleMinValue);
end;

procedure TdxGaugeCustomRangeViewInfo.Draw(AGPGraphics: TdxGPGraphics);
begin
  if FRangeParameters.Visible and (FBounds.Width > 0) and (FBounds.Height > 0) then
    DoDraw(AGPGraphics);
end;

{ TdxGaugeCustomRange }

constructor TdxGaugeCustomRange.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FRangeParameters := GetRangeParametersClass.Create;
  FRangeParameters.Color := dxColorToAlphaColor(clDefault);
  FRangeParameters.Visible := True;
  FRangeParameters.WidthFactor := dxGaugeCustomRangeWidthFactor;
  FViewInfo := GetViewInfoClass.Create;
end;

destructor TdxGaugeCustomRange.Destroy;
begin
  FreeAndNil(FViewInfo);
  FreeAndNil(FRangeParameters);
  inherited Destroy;
end;

procedure TdxGaugeCustomRange.Assign(ASource: TPersistent);
begin
  if ASource is TdxGaugeCustomRange then
  begin
    Collection.BeginUpdate;
    try
      DoAssign(TdxGaugeCustomRange(ASource));
    finally
      Collection.EndUpdate;
    end;
  end
  else
    inherited Assign(ASource);
end;

function TdxGaugeCustomRange.GetCollectionFromParent(AParent: TComponent): TcxComponentCollection;
begin
  Result := (AParent as TdxGaugeQuantitativeScale).Ranges;
end;

procedure TdxGaugeCustomRange.DefineProperties(AFiler: TFiler);
begin
  inherited DefineProperties(AFiler);
  AFiler.DefineProperty('WidthFactor', ReadWidthFactor, WriteWidthFactor, SameValue(WidthFactor, 0));
end;

function TdxGaugeCustomRange.GetRangeParametersClass: TdxGaugeCustomRangeParametersClass;
begin
  Result := TdxGaugeCustomRangeParameters;
end;

function TdxGaugeCustomRange.GetViewInfoClass: TdxGaugeCustomRangeViewInfoClass;
begin
  Result := TdxGaugeCustomRangeViewInfo;
end;

procedure TdxGaugeCustomRange.Calculate;
begin
  ViewInfo.Calculate(FRangeParameters);
end;

function TdxGaugeCustomRange.GetColor: TdxAlphaColor;
begin
  Result := FRangeParameters.Color;
end;

function TdxGaugeCustomRange.GetValueEnd: Single;
begin
  Result := FRangeParameters.ValueEnd;
end;

function TdxGaugeCustomRange.GetValueStart: Single;
begin
  Result := FRangeParameters.ValueStart;
end;

function TdxGaugeCustomRange.GetVisible: Boolean;
begin
  Result := FRangeParameters.Visible;
end;

function TdxGaugeCustomRange.GetWidthFactor: Single;
begin
  Result := FRangeParameters.WidthFactor;
end;

procedure TdxGaugeCustomRange.SetColor(const AValue: TdxAlphaColor);
begin
  if FRangeParameters.Color <> AValue then
  begin
    FRangeParameters.Color := AValue;
    Changed(False);
  end;
end;

procedure TdxGaugeCustomRange.SetValueEnd(const AValue: Single);
begin
  if FRangeParameters.ValueEnd <> AValue then
  begin
    FRangeParameters.ValueEnd := GetValidValue(AValue);
    Changed(False);
  end;
end;

procedure TdxGaugeCustomRange.SetValueStart(const AValue: Single);
begin
  if FRangeParameters.ValueStart <> AValue then
  begin
    FRangeParameters.ValueStart := GetValidValue(AValue);
    Changed(False);
  end;
end;

procedure TdxGaugeCustomRange.SetVisible(const AValue: Boolean);
begin
  if FRangeParameters.Visible <> AValue then
  begin
    FRangeParameters.Visible := AValue;
    Changed(False);
  end;
end;

procedure TdxGaugeCustomRange.SetWidthFactor(const AValue: Single);
begin
  if not SameValue(FRangeParameters.WidthFactor, AValue) then
  begin
    FRangeParameters.WidthFactor := Min(Max(AValue, 0), 1);
    Changed(False);
  end;
end;

function TdxGaugeCustomRange.GetValidValue(AValue: Single): Single;

  function GetScale: TdxGaugeQuantitativeScale;
  begin
    Result := Collection.ParentComponent as TdxGaugeQuantitativeScale;
  end;

begin
  Result := Min(Max(AValue, GetScale.GetMinValue), GetScale.GetMaxValue);
end;

procedure TdxGaugeCustomRange.DoAssign(ARange: TdxGaugeCustomRange);
begin
  FRangeParameters.Assign(ARange.RangeParameters);
end;

procedure TdxGaugeCustomRange.ReadWidthFactor(AReader: TReader);
begin
  WidthFactor := AReader.ReadDouble;
end;

procedure TdxGaugeCustomRange.WriteWidthFactor(AWriter: TWriter);
begin
  AWriter.WriteDouble(WidthFactor);
end;

function TdxGaugeCustomRange.IsColorStored: Boolean;
begin
  Result := FRangeParameters.Color <> dxColorToAlphaColor(clDefault);
end;

function TdxGaugeCustomRange.IsWidthFactorStored: Boolean;
begin
  Result := not SameValue(RoundTo(FRangeParameters.WidthFactor, -5), dxGaugeCustomRangeWidthFactor);
end;

{ TdxGaugeRangeCollection }

procedure TdxGaugeRangeCollection.Assign(ASource: TPersistent);
var
  I: Integer;
begin
  if ASource is TdxGaugeRangeCollection then
  begin
    Clear;
    for I := 0 to TdxGaugeRangeCollection(ASource).Count - 1 do
      Add;
    for I := 0 to TdxGaugeRangeCollection(ASource).Count - 1 do
      Items[I].Assign(TdxGaugeRangeCollection(ASource).Items[I]);
  end
  else
    inherited Assign(ASource);
end;

function TdxGaugeRangeCollection.GetItemPrefixName: string;
begin
  Result := 'TdxGauge';
end;

function TdxGaugeRangeCollection.GetRange(AIndex: Integer): TdxGaugeCustomRange;
begin
  Result := inherited GetItem(AIndex) as TdxGaugeCustomRange;
end;

procedure TdxGaugeRangeCollection.SetRange(AIndex: Integer; const AValue: TdxGaugeCustomRange);
begin
  inherited SetItem(AIndex, AValue);
end;

end.
