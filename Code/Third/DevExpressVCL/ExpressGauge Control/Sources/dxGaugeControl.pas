{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressGaugeControl                                      }
{                                                                    }
{           Copyright (c) 2013-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSGAUGECONTROL AND ALL           }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxGaugeControl;

{$I cxVer.inc}

interface

uses
  SysUtils, Classes, Controls, Windows, Graphics, Messages, Contnrs, Menus, Generics.Collections,
  dxCore, cxGeometry, cxGraphics, dxGDIPlusClasses, cxClasses, cxControls, cxLookAndFeels, dxGaugeCustomScale,
  dxGaugeControlSelection;

type
  TdxCustomGaugeControlController = class;
  TdxGaugeControlHitTest = class;
  TdxCustomGaugeControlPainter = class;

  TdxGaugeDrawMode = (dmByLayer, dmByScale);

  { TdxCustomGaugeControl }

  TdxCustomGaugeControl = class(TcxControl, IdxSkinSupport)
  private
    FController: TdxCustomGaugeControlController;
    FDrawMode: TdxGaugeDrawMode;
    FIsCreatingPreview: Boolean;
    FLockCount: Integer;
    FPainter: TdxCustomGaugeControlPainter;
    FScales: TdxGaugeScaleCollection;
    FTransparent: Boolean;

    procedure SetDrawMode(const AValue: TdxGaugeDrawMode);
    procedure SetTransparent(const AValue: Boolean);

    procedure AddScaleHandler(ASender: TObject);
    procedure DeleteScaleHandler(ASender: TObject);
    procedure ScaleChangeHandler(Sender: TObject; AItem: TcxComponentCollectionItem;
      AAction: TcxComponentCollectionNotification);

    function GetLocked: Boolean;
    function GetLayerCount: Integer;
    function IsShiftPressed: Boolean;

    procedure Changed;
    procedure DoInvalidate;
  protected
    function GetDesignHitTest(X, Y: Integer; Shift: TShiftState): Boolean; override;
    function GetDragAndDropObjectClass: TcxDragAndDropObjectClass; override;
    function HasBackground: Boolean; override;
    function IsDoubleBufferedNeeded: Boolean; override;
    function IsTransparentBackground: Boolean; override;
    function NeedRedrawOnResize: Boolean; override;
    procedure ChangeScale(M, D: Integer); override;
    procedure DoPaint; override;
    procedure Loaded; override;
    procedure LookAndFeelChanged(Sender: TcxLookAndFeel; AChangedValues: TcxLookAndFeelValues); override;
    procedure Resize; override;
    function StartDragAndDrop(const P: TPoint): Boolean; override;
    procedure WndProc(var Message: TMessage); override;

    function CreatePreviewBitmap: TBitmap;

    property IsCreatingPreview: Boolean read FIsCreatingPreview;
    property Controller: TdxCustomGaugeControlController read FController;
    property DrawMode: TdxGaugeDrawMode read FDrawMode write SetDrawMode;
    property LayerCount: Integer read GetLayerCount;
    property Locked: Boolean read GetLocked;
    property Painter: TdxCustomGaugeControlPainter read FPainter;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure Assign(ASource: TPersistent); override;
    procedure GetChildren(AProc: TGetChildProc; ARoot: TComponent); override;

    function AddScale(AScaleClass: TdxGaugeCustomScaleClass): TdxGaugeCustomScale;
    procedure DeleteScale(AScaleIndex: Integer);
    procedure Clear;

    procedure BeginUpdate;
    procedure CancelUpdate;
    procedure EndUpdate;

    property Scales: TdxGaugeScaleCollection read FScales;
    property Transparent: Boolean read FTransparent write SetTransparent default False;
  end;

  { TdxGaugeControl }

  TdxGaugeControl = class(TdxCustomGaugeControl)
  published
    property Align;
    property Anchors;
    property BorderStyle default cxcbsDefault;
    property Color default clWindow;
    property Constraints;
    property LookAndFeel;
    property ParentColor default False;
    property Scales;
    property ShowHint default False;
    property Transparent;
    property Visible;

    property OnClick;
    property OnDblClick;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnResize;
  end;

  { TdxGaugeControlHitTest }

  TdxGaugeControlHitTest = class
  private
    FControl: TdxCustomGaugeControl;
    FHitObject: TdxGaugeCustomScale;
  protected
    procedure Calculate(const P: TPoint);

    property Control: TdxCustomGaugeControl read FControl;
    property HitObject: TdxGaugeCustomScale read FHitObject write FHitObject;
  public
    constructor Create(AControl: TdxCustomGaugeControl);
  end;

  { TdxCustomGaugeControlController }

  TdxCustomGaugeControlController = class
  private
    FChangedLayers: TdxGaugeScaleChangedLayers;
    FControl: TdxCustomGaugeControl;
    FDesignPopupMenu: TPopupMenu;
    FLayerCount: Integer;
    FHitTest: TdxGaugeControlHitTest;
    FSelectionHelper: TdxGaugeControlCustomSelectionHelper;
    FZOrders: TList;

    function GetIsControlSelected: Boolean;
    function GetSelections: TList;
    procedure MoveScale(AScale: TdxGaugeCustomScale; const ACenterPositionDelta: TdxPointF);
    procedure MoveScales(const ACenterPositionDelta: TdxPointF);
    procedure PopulateAnchoredScales(AAnchoredScales: TList; AScale: TdxGaugeCustomScale);
    // ZOrder
    procedure BringBackward(AScale: TdxGaugeCustomScale);
    procedure BringForward(AScale: TdxGaugeCustomScale);
    procedure BringToFront(AScale: TdxGaugeCustomScale);
    procedure DoChangeZOrder(AScale: TdxGaugeCustomScale; AType: TdxGaugeScaleZOrderChangeType);
    procedure SendToBack(AScale: TdxGaugeCustomScale);
    procedure SetZOrder(AScale: TdxGaugeCustomScale; const AValue: Integer);
    // Design Popup Menu
    function CheckDesignPopupMenu(AShift: TShiftState): Boolean;
    function CreatePopupMenuItem(const ACaption: string; ATag: Integer; AOnClick: TNotifyEvent): TMenuItem;
    procedure MenuItemOnClickHandler(ASender: TObject);
    procedure PopupDesignMenu(const APopupPoint: TPoint);
    procedure PopulateDesignPopupMenu;
    procedure ShowDesignPopupMenu(AShift: TShiftState);
    // Internal
    function CanCalculate(const ABounds: TRect): Boolean;
    function CheckComponentState: Boolean;
    function GetScaleBounds(AScale: TdxGaugeCustomScale): TdxRectF;
    procedure Calculate;
    procedure DoCalculateScales(AScaleList: TList);
  protected
    function CanAnchorScale(AScaleIndex, AAnchorScaleIndex: Integer): Boolean;
    function CreateAnchoredScaleList(AAnchorScaleList: TList): TList;
    function GetDesignHitTest(X, Y: Integer; AShift: TShiftState): Boolean;
    procedure GetScalesByDependences(AList: TList);
    procedure Select(AScale: TdxGaugeCustomScale; AShift: TShiftState = []);
    procedure ShowRangesEditor;
    procedure ShowScalesEditor;
    procedure UpdateAnchorReferences(AScale: TdxGaugeCustomScale);
    // ZOrder
    procedure AddZOrder(AScale: TdxGaugeCustomScale);
    procedure ChangeZOrder(AScale: TdxGaugeCustomScale; AType: TdxGaugeScaleZOrderChangeType); overload;
    procedure ChangeZOrder(AScales: TList; AType: TdxGaugeScaleZOrderChangeType); overload;
    procedure ChangeSelectionsZOrders(AType: TdxGaugeScaleZOrderChangeType);
    procedure PopulateZOrders;
    procedure RemoveZOrder(AScale: TdxGaugeCustomScale);

    property ChangedLayers: TdxGaugeScaleChangedLayers read FChangedLayers write FChangedLayers;
    property Control: TdxCustomGaugeControl read FControl;
    property IsControlSelected: Boolean read GetIsControlSelected;
    property LayerCount: Integer read FLayerCount;
    property HitTest: TdxGaugeControlHitTest read FHitTest;
    property Selections: TList read GetSelections;
    property ZOrders: TList read FZOrders;
  public
    constructor Create(AControl: TdxCustomGaugeControl);
    destructor Destroy; override;
  end;

  { TdxCustomGaugeControlPainter }

  TdxCustomGaugeControlPainter = class
  private
    FControl: TdxCustomGaugeControl;
    FDynamicElementsLayer: TcxBitmap32;
    FStaticElementsLayer: TcxBitmap32;
    FScaleDynamicLayerIndexes: array of Integer;
    FScaleStaticLayerIndexes: array of Integer;

    function NeedDrawSelector: Boolean;
    procedure DrawBackground(ACanvas: TcxCanvas);
    procedure DrawBorders(ACanvas: TcxCanvas);
    procedure DrawBitmaps(AGPGraphics: TdxGPGraphics);
    procedure DrawControlSelector(ACanvas: TcxCanvas);
    procedure DrawGaugeLayer(ABitmap: TcxBitmap32; const AScaleLayerIndexes: array of Integer);
    procedure DrawLayer(AGPGraphics: TdxGPGraphics; const AScaleLayerIndexes: array of Integer);
    procedure DrawScales(AGPGraphics: TdxGPGraphics; ADrawMode: TdxGaugeDrawMode);
    procedure DrawSelections(AGPGraphics: TdxGPGraphics);
    procedure PrepareBitmap(ABitmap: TcxBitmap32);
    procedure PrepareBitmaps;
  protected
    procedure DrawScale(AGPGraphics: TdxGPGraphics; const R: TRect; AScaleIndex: Integer);
    procedure Paint(ACanvas: TcxCanvas; ADrawMode: TdxGaugeDrawMode);
  public
    constructor Create(AControl: TdxCustomGaugeControl);
    destructor Destroy; override;
  end;

var
  dxGaugeControlSelectionHelperClass: TdxGaugeControlCustomSelectionHelperClass;

implementation

uses
  Types, Math, Forms, dxGaugeQuantitativeScale, dxGaugeUtils;

type
  TdxCustomGaugeControlAccess = class(TdxCustomGaugeControl);
  TdxGaugeControlCustomSelectionHelperAccess = class(TdxGaugeControlCustomSelectionHelper);
  TdxGaugeCustomScaleAccess = class(TdxGaugeCustomScale);
  TdxGaugeScaleCollectionAccess = class(TdxGaugeScaleCollection);
  TdxGaugeScaleFactoryAccess = class(TdxGaugeScaleFactory);
  TdxGaugeQuantitativeScaleAccess = class(TdxGaugeQuantitativeScale);

  { TdxGaugeControlDragImageInfo }

  TdxGaugeControlDragImage = class(TcxDragImage)
  private
    FOffset: TPoint;
  protected
    property Offset: TPoint read FOffset write FOffset;
  end;

  { TdxGaugeControlDragAndDropObject }

  TdxGaugeControlDragAndDropObject = class(TcxDragAndDropObject)
  private
    FDragImages: TList;
    FStartDragPoint: TPoint;

    function CreateDragImage(ADragScale: TdxGaugeCustomScaleAccess): TdxGaugeControlDragImage;
    function GetControl: TdxCustomGaugeControl;
    function GetMousePosDelta: TPoint;
    function GetScaleSize(ADragScale: TdxGaugeCustomScale): TSize;
    procedure DrawDragImage(ACanvas: TcxCanvas; const R: TRect; AScale: TdxGaugeCustomScale);
    procedure InitDragImages;
    procedure PopulateDragImages(AScaleList: TList);
    procedure ShowDragImages(const ACursorPos: TPoint);
  protected
    function GetDragAndDropCursor(Accepted: Boolean): TCursor; override;
    function ProcessKeyDown(AKey: Word; AShiftState: TShiftState): Boolean; override;
    procedure BeginDragAndDrop; override;
    procedure DragAndDrop(const P: TPoint; var Accepted: Boolean); override;
    procedure EndDragAndDrop(Accepted: Boolean); override;
    procedure Init(ADragScale: TdxGaugeCustomScale; const P: TPoint);
  end;

{ TdxCustomGaugeControl }

constructor TdxCustomGaugeControl.Create(AOwner: TComponent);
const
  GaugeControlWidth = 250;
  GaugeControlHeigth = 250;
begin
  inherited Create(AOwner);
  BorderStyle := cxcbsDefault;
  Color := clWindow;
  ParentColor := False;
  Width := GaugeControlWidth;
  Height := GaugeControlHeigth;

  FScales := TdxGaugeScaleCollection.Create(Self, TdxGaugeCustomScale);
  FScales.OnChange := ScaleChangeHandler;
  TdxGaugeScaleCollectionAccess(FScales).OnAdded := AddScaleHandler;
  TdxGaugeScaleCollectionAccess(FScales).OnDeleted := DeleteScaleHandler;
  FController := TdxCustomGaugeControlController.Create(Self);
  FPainter := TdxCustomGaugeControlPainter.Create(Self);
end;

destructor TdxCustomGaugeControl.Destroy;
begin
  FreeAndNil(FPainter);
  FreeAndNil(FController);
  FreeAndNil(FScales);
  inherited Destroy;
end;

procedure TdxCustomGaugeControl.Assign(ASource: TPersistent);
begin
  if ASource is TdxCustomGaugeControl then
  begin
    BeginUpdate;
    try
      Scales.Assign(TdxCustomGaugeControl(ASource).Scales);
      Transparent := TdxCustomGaugeControl(ASource).Transparent;
      Color := TdxCustomGaugeControl(ASource).Color;
    finally
      EndUpdate;
    end;
  end
  else
    inherited Assign(ASource);
end;

procedure TdxCustomGaugeControl.GetChildren(AProc: TGetChildProc; ARoot: TComponent);
var
  I: Integer;
begin
  for I := 0 to Scales.Count - 1 do
    if Scales[I].Owner = ARoot then
      AProc(Scales[I]);
end;

function TdxCustomGaugeControl.AddScale(AScaleClass: TdxGaugeCustomScaleClass): TdxGaugeCustomScale;
begin
  Result := Scales.Add(AScaleClass);
  if IsDesigning then
    Controller.Select(Result, [ssLeft]);
end;

procedure TdxCustomGaugeControl.DeleteScale(AScaleIndex: Integer);
begin
  Scales.Delete(AScaleIndex);
end;

procedure TdxCustomGaugeControl.Clear;
begin
  Scales.Clear;
end;

procedure TdxCustomGaugeControl.BeginUpdate;
begin
  Inc(FLockCount);
end;

procedure TdxCustomGaugeControl.CancelUpdate;
begin
  Dec(FLockCount);
end;

procedure TdxCustomGaugeControl.EndUpdate;
begin
  Dec(FLockCount);
  if FLockCount = 0 then
    Changed;
end;

function TdxCustomGaugeControl.GetDesignHitTest(X, Y: Integer; Shift: TShiftState): Boolean;
begin
  Result := inherited GetDesignHitTest(X, Y, Shift);
  if not Result then
    Result := Controller.GetDesignHitTest(X, Y, Shift);
end;

function TdxCustomGaugeControl.GetDragAndDropObjectClass: TcxDragAndDropObjectClass;
begin
  Result := TdxGaugeControlDragAndDropObject;
end;

function TdxCustomGaugeControl.HasBackground: Boolean;
begin
  Result := True;
end;

function TdxCustomGaugeControl.IsDoubleBufferedNeeded: Boolean;
begin
  Result := True;
end;

function TdxCustomGaugeControl.IsTransparentBackground: Boolean;
begin
  Result := Transparent;
end;

function TdxCustomGaugeControl.NeedRedrawOnResize: Boolean;
begin
  Result := True;
end;

procedure TdxCustomGaugeControl.ChangeScale(M, D: Integer);
var
  I: Integer;
begin
  inherited ChangeScale(M, D);
  for I := 0 to Scales.Count - 1 do
    if Scales[I] is TdxGaugeQuantitativeScale then
      TdxGaugeQuantitativeScaleAccess(Scales[I]).Font.Height :=
      MulDiv(TdxGaugeQuantitativeScaleAccess(Scales[I]).Font.Height, M, D);
end;

procedure TdxCustomGaugeControl.DoPaint;
begin
  Painter.Paint(Canvas, DrawMode);
end;

procedure TdxCustomGaugeControl.Loaded;
begin
  inherited Loaded;
  Controller.PopulateZOrders;
end;

procedure TdxCustomGaugeControl.LookAndFeelChanged(Sender: TcxLookAndFeel; AChangedValues: TcxLookAndFeelValues);
begin
  DoInvalidate;
end;

procedure TdxCustomGaugeControl.Resize;
begin
  inherited Resize;
  Controller.ChangedLayers := [sclStaticLayer, sclDynamicLayer];
  Changed;
end;

function TdxCustomGaugeControl.StartDragAndDrop(const P: TPoint): Boolean;
begin
  Result := Controller.HitTest.HitObject <> nil;
  if Result then
    TdxGaugeControlDragAndDropObject(DragAndDropObject).Init(Controller.HitTest.HitObject, P);
end;

procedure TdxCustomGaugeControl.WndProc(var Message: TMessage);
begin
  if IsDesigning and (Message.Msg = WM_LBUTTONDBLCLK) then
    Controller.ShowScalesEditor;
  inherited WndProc(Message);
end;

function TdxCustomGaugeControl.CreatePreviewBitmap: TBitmap;
var
  AStoredChangedLayers: TdxGaugeScaleChangedLayers;
begin
  Result := TBitmap.Create;
  Result.SetSize(Width, Height);
  Result.PixelFormat := pf32bit;
  AStoredChangedLayers := Controller.ChangedLayers;
  Controller.ChangedLayers := [sclStaticLayer, sclDynamicLayer];
  cxPaintCanvas.BeginPaint(Result.Canvas);
  try
    FIsCreatingPreview := True;
    Painter.Paint(cxPaintCanvas, dmByLayer);
  finally
    FIsCreatingPreview := False;
    cxPaintCanvas.EndPaint;
  end;
  Controller.ChangedLayers := AStoredChangedLayers;
end;

procedure TdxCustomGaugeControl.SetDrawMode(const AValue: TdxGaugeDrawMode);
begin
  if FDrawMode <> AValue then
  begin
    FDrawMode := AValue;
    Controller.ChangedLayers := [sclStaticLayer, sclDynamicLayer];
    DoInvalidate;
  end;
end;

procedure TdxCustomGaugeControl.SetTransparent(const AValue: Boolean);
begin
  if FTransparent <> AValue then
  begin
    FTransparent := AValue;
    Changed;
  end;
end;

procedure TdxCustomGaugeControl.AddScaleHandler(ASender: TObject);
begin
  Controller.AddZOrder(ASender as TdxGaugeCustomScale);
end;

procedure TdxCustomGaugeControl.DeleteScaleHandler(ASender: TObject);
begin
  Controller.RemoveZOrder(ASender as TdxGaugeCustomScale);
end;

procedure TdxCustomGaugeControl.ScaleChangeHandler(Sender: TObject; AItem: TcxComponentCollectionItem;
  AAction: TcxComponentCollectionNotification);
begin
  Changed;
end;

function TdxCustomGaugeControl.GetLocked: Boolean;
begin
  Result := FLockCount <> 0;
end;

function TdxCustomGaugeControl.GetLayerCount: Integer;
begin
  Result := Controller.LayerCount;
end;

function TdxCustomGaugeControl.IsShiftPressed: Boolean;
begin
  Result := GetAsyncKeyState(VK_SHIFT) < 0;
end;

procedure TdxCustomGaugeControl.Changed;
begin
  if FLockCount = 0 then
  begin
    Controller.Calculate;
    DoInvalidate;
  end;
end;

procedure TdxCustomGaugeControl.DoInvalidate;
begin
  InvalidateRect(ClientRect, False);
end;

{ TdxGaugeControlHitTest }

constructor TdxGaugeControlHitTest.Create(AControl: TdxCustomGaugeControl);
begin
  inherited Create;
  FControl := AControl;
end;

procedure TdxGaugeControlHitTest.Calculate(const P: TPoint);

  function cxRectPtIn(const P: TPoint; const R: TdxRectF): Boolean; {$IFDEF DELPHI9} inline; {$ENDIF}
  begin
    Result := (P.X >= R.Left) and (P.X <= R.Right) and (P.Y >= R.Top) and (P.Y <= R.Bottom);
  end;

var
  I: Integer;
begin
  FHitObject := nil;
  for I := Control.Controller.ZOrders.Count - 1 downto 0 do
    if cxRectPtIn(P, TdxGaugeCustomScaleAccess(Control.Controller.ZOrders[I]).ContentBounds) then
    begin
      FHitObject := Control.Controller.ZOrders[I];
      Break;
    end;
end;

{   TdxCustomGaugeControlController }

constructor TdxCustomGaugeControlController.Create(AControl: TdxCustomGaugeControl);
begin
  inherited Create;
  FControl := AControl;
  FHitTest := TdxGaugeControlHitTest.Create(FControl);
  FZOrders := TList.Create;
  if Control.IsDesigning then
  begin
    FSelectionHelper := dxGaugeControlSelectionHelperClass.Create(Control);
    dxGaugeControlSelectionHelpers.RegistrySelectionHelper(FSelectionHelper);
  end
  else
    FSelectionHelper := nil;
end;

destructor TdxCustomGaugeControlController.Destroy;
begin
  if Control.IsDesigning then
    dxGaugeControlSelectionHelpers.UnregistrySelectionHelper(FSelectionHelper);
  FreeAndNil(FSelectionHelper);
  FreeAndNil(FZOrders);
  FreeAndNil(FHitTest);
  inherited Destroy;
end;

function TdxCustomGaugeControlController.CanAnchorScale(AScaleIndex, AAnchorScaleIndex: Integer): Boolean;

  function GetParentIndex(AIndex: Integer): Integer;
  begin
    if AIndex > -1 then
      Result := TdxGaugeCustomScaleAccess(Control.Scales[AIndex]).AnchorScaleIndex
    else
      Result := -1;
  end;

  function CheckIndexes(AIndex, AParentIndex: Integer): Boolean;
  begin
    Result := (AParentIndex = -1) or (AIndex <> AParentIndex) and (AParentIndex <> AScaleIndex) and
      CheckIndexes(AParentIndex, GetParentIndex(AParentIndex));
  end;

  function CheckAnchorScaleIndex: Boolean;
  begin
    Result := (AAnchorScaleIndex >= -1) and (AAnchorScaleIndex < Control.Scales.Count);
  end;

begin
  Result := CheckAnchorScaleIndex and ((((AAnchorScaleIndex = -1) and (AScaleIndex > -1)) or
    (AScaleIndex > -1) and (AScaleIndex <> AAnchorScaleIndex) and CheckIndexes(AAnchorScaleIndex,
    GetParentIndex(AAnchorScaleIndex))));
end;

function TdxCustomGaugeControlController.CreateAnchoredScaleList(AAnchorScaleList: TList): TList;
var
  I: Integer;
begin
  Result := TList.Create;
  for I := 0 to AAnchorScaleList.Count - 1 do
    PopulateAnchoredScales(Result, TdxGaugeCustomScale(AAnchorScaleList[I]));
end;

function TdxCustomGaugeControlController.GetDesignHitTest(X, Y: Integer; AShift: TShiftState): Boolean;
var
  P: TPoint;
begin
  P := Point(X, Y);
  Result := PtInRect(dxGaugeGetSelectorRect(Control.ClientBounds), P);
  if Result and (AShift * [ssRight, ssLeft] <> []) then
  begin
    TdxGaugeControlCustomSelectionHelperAccess(FSelectionHelper).SelectComponent(Control);
    Result := False;
  end
  else
  begin
    HitTest.Calculate(P);
    Result := HitTest.HitObject <> nil;
    if Result then
      if ssLeft in AShift then
        Select(HitTest.HitObject, AShift)
      else
        Result := CheckDesignPopupMenu(AShift);
  end;
end;

procedure TdxCustomGaugeControlController.GetScalesByDependences(AList: TList);

  function GetIndex(AScale: TdxGaugeCustomScale): Integer;
  var
    AAnchorScale: TdxGaugeCustomScale;
  begin
    AAnchorScale := TdxGaugeCustomScaleAccess(AScale).AnchorScale;
    if AAnchorScale = nil then
      Result := AList.Count
    else
    begin
      Result := AList.IndexOf(AAnchorScale);
      if Result = -1 then
        Result := 0;
    end;
  end;

var
  I: Integer;
begin
  for I := 0 to FControl.Scales.Count - 1 do
    AList.Insert(GetIndex(FControl.Scales[I]), FControl.Scales[I]);
  dxGaugeReverseList(AList);
end;

procedure TdxCustomGaugeControlController.Select(AScale: TdxGaugeCustomScale; AShift: TShiftState);
begin
  if ssLeft in AShift then
    TdxGaugeControlCustomSelectionHelperAccess(FSelectionHelper).Select(AScale, Control.IsShiftPressed);
end;

procedure TdxCustomGaugeControlController.ShowRangesEditor;
begin
  if Control.IsDesigning then
    TdxGaugeControlCustomSelectionHelperAccess(FSelectionHelper).ShowRangesEditor;
end;

procedure TdxCustomGaugeControlController.ShowScalesEditor;
begin
  if Control.IsDesigning then
    TdxGaugeControlCustomSelectionHelperAccess(FSelectionHelper).ShowScalesEditor;
end;

procedure TdxCustomGaugeControlController.AddZOrder(AScale: TdxGaugeCustomScale);
begin
  if CheckComponentState then
  begin
    if ZOrders.IndexOf(AScale) = -1 then
      ZOrders.Add(AScale);
    FChangedLayers := [sclStaticLayer, sclDynamicLayer];
  end;
end;

procedure TdxCustomGaugeControlController.ChangeZOrder(AScale: TdxGaugeCustomScale; AType: TdxGaugeScaleZOrderChangeType);
begin
  DoChangeZOrder(AScale, AType);
end;

procedure TdxCustomGaugeControlController.ChangeZOrder(AScales: TList; AType: TdxGaugeScaleZOrderChangeType);
var
  I: Integer;
begin
  Control.BeginUpdate;
  for I := 0 to AScales.Count - 1 do
    DoChangeZOrder(TdxGaugeCustomScale(AScales[I]), AType);
  Control.EndUpdate;
end;

procedure TdxCustomGaugeControlController.ChangeSelectionsZOrders(AType: TdxGaugeScaleZOrderChangeType);
begin
  ChangeZOrder(Selections, AType);
end;

procedure TdxCustomGaugeControlController.PopulateZOrders;

  function SortScalesByZOrder(AItem1, AItem2: Pointer): Integer;
  begin
    Result := dxCompareValues(TdxGaugeCustomScaleAccess(AItem1).FZOrder, TdxGaugeCustomScaleAccess(AItem2).FZOrder);
  end;

var
  I: Integer;
  AScales: TList;
begin
  AScales := TList.Create;
  try
    for I := 0 to Control.Scales.Count - 1 do
      AScales.Add(Control.Scales[I]);
    AScales.Sort(@SortScalesByZOrder);
    ZOrders.Clear;
    ZOrders.Assign(AScales);
  finally
    AScales.Free;
  end;
  Control.Changed;
end;

procedure TdxCustomGaugeControlController.RemoveZOrder(AScale: TdxGaugeCustomScale);
begin
  if CheckComponentState then
  begin
    if FSelectionHelper <> nil then
      Selections.Remove(AScale);
    UpdateAnchorReferences(AScale);
    ZOrders.Remove(AScale);
    FChangedLayers := [sclStaticLayer, sclDynamicLayer];
  end;
end;

function TdxCustomGaugeControlController.GetIsControlSelected: Boolean;
begin
  Result := (FSelectionHelper <> nil) and TdxGaugeControlCustomSelectionHelperAccess(FSelectionHelper).IsControlSelected;
end;

function TdxCustomGaugeControlController.GetSelections: TList;
begin
  Result := TdxGaugeControlCustomSelectionHelperAccess(FSelectionHelper).Selections;
end;

procedure TdxCustomGaugeControlController.MoveScale(AScale: TdxGaugeCustomScale; const ACenterPositionDelta: TdxPointF);

  procedure SetCenterPositionFactor(AScaleAccess: TdxGaugeCustomScaleAccess);

    function GetFactorDelta: TdxPointF;
    var
      AAnchorBounds: TdxRectF;
    begin
      AAnchorBounds := GetScaleBounds(AScaleAccess);
      Result := cxPointF(ACenterPositionDelta.X / AAnchorBounds.Width, ACenterPositionDelta.Y / AAnchorBounds.Height);
    end;

  begin
    AScaleAccess.CenterPositionFactorX := AScaleAccess.CenterPositionFactorX + GetFactorDelta.X;
    AScaleAccess.CenterPositionFactorY := AScaleAccess.CenterPositionFactorY + GetFactorDelta.Y;
  end;

  procedure SetCenterPosition(AScaleAccess: TdxGaugeCustomScaleAccess);
  begin
    AScaleAccess.CenterPositionX := AScaleAccess.CenterPositionX + Round(ACenterPositionDelta.X);
    AScaleAccess.CenterPositionY := AScaleAccess.CenterPositionY + Round(ACenterPositionDelta.Y);
  end;

begin
  if TdxGaugeCustomScaleAccess(AScale).CenterPositionType = sptFactor then
    SetCenterPositionFactor(TdxGaugeCustomScaleAccess(AScale))
  else
    SetCenterPosition(TdxGaugeCustomScaleAccess(AScale));
end;

procedure TdxCustomGaugeControlController.MoveScales(const ACenterPositionDelta: TdxPointF);
var
  I: Integer;
  AScaleAccess: TdxGaugeCustomScaleAccess;
  ASelection: TList;
begin
  Control.BeginUpdate;
  try
    ASelection := Selections;
    for I := 0 to ASelection.Count - 1 do
    begin
      AScaleAccess := TdxGaugeCustomScaleAccess(ASelection[I]);
      if (AScaleAccess.AnchorScaleIndex = -1) or (ASelection.IndexOf(AScaleAccess.AnchorScale) = -1) then
        MoveScale(AScaleAccess, ACenterPositionDelta);
    end;
  finally
    Control.EndUpdate;
  end;
end;

procedure TdxCustomGaugeControlController.PopulateAnchoredScales(AAnchoredScales: TList; AScale: TdxGaugeCustomScale);

  procedure AddAnchoredScales(AScales: TList);
  var
    AScale: TdxGaugeCustomScale;
  begin
    while AScales.Count > 0 do
    begin
      AScale := TdxGaugeCustomScaleAccess(AScales[0]);
      if AAnchoredScales.IndexOf(AScale) = -1 then
      begin
        AAnchoredScales.Add(AScale);
        PopulateAnchoredScales(AAnchoredScales, AScale);
      end;
      AScales.Remove(AScale);
    end;
  end;

  function CreateInternalAnchoredScaleList(AScale: TdxGaugeCustomScale): TList;
  var
    I: Integer;
    AScaleAccess: TdxGaugeCustomScaleAccess;
  begin
    Result := TList.Create;
    for I := 0 to TdxCustomGaugeControl(Control).Scales.Count - 1 do
    begin
      AScaleAccess := TdxGaugeCustomScaleAccess(TdxCustomGaugeControl(Control).Scales[I]);
      if AScaleAccess.AnchorScale = AScale then
        Result.Add(AScaleAccess);
    end;
  end;

var
  AScales: TList;
begin
  AScales := CreateInternalAnchoredScaleList(AScale);
  try
    AddAnchoredScales(AScales);
  finally
    AScales.Free;
  end;
end;

procedure TdxCustomGaugeControlController.BringBackward(AScale: TdxGaugeCustomScale);
begin
  SetZOrder(AScale, TdxGaugeCustomScaleAccess(AScale).ZOrder - 1);
end;

procedure TdxCustomGaugeControlController.BringForward(AScale: TdxGaugeCustomScale);
begin
  SetZOrder(AScale, TdxGaugeCustomScaleAccess(AScale).ZOrder + 1);
end;

procedure TdxCustomGaugeControlController.BringToFront(AScale: TdxGaugeCustomScale);
begin
  SetZOrder(AScale, Control.Scales.Count - 1);
end;

procedure TdxCustomGaugeControlController.DoChangeZOrder(AScale: TdxGaugeCustomScale; AType: TdxGaugeScaleZOrderChangeType);
begin
  case AType of
    octBringToFront:
      BringToFront(AScale);
    octSendToBack:
      SendToBack(AScale);
    octBringForward:
      BringForward(AScale);
    octBringBackward:
      BringBackward(AScale);
  end;
end;

procedure TdxCustomGaugeControlController.SendToBack(AScale: TdxGaugeCustomScale);
begin
  SetZOrder(AScale, 0);
end;

procedure TdxCustomGaugeControlController.SetZOrder(AScale: TdxGaugeCustomScale; const AValue: Integer);
var
  AZOrder: Integer;
begin
  AZOrder := ZOrders.IndexOf(AScale);
  if (AZOrder <> AValue) and not (csReading in Control.ComponentState) then
    ZOrders.Move(AZOrder, Max(Min(AValue, ZOrders.Count - 1), 0));
  FChangedLayers := [sclStaticLayer, sclDynamicLayer];
  Control.Invalidate;
end;

procedure TdxCustomGaugeControlController.UpdateAnchorReferences(AScale: TdxGaugeCustomScale);
var
  I: Integer;
begin
  if Control.ComponentState * [csDestroying] = [] then
  begin
    Control.Scales.BeginUpdate;
    for I := 0 to Control.Scales.Count - 1 do
      if TdxGaugeCustomScaleAccess(Control.Scales[I]).AnchorScale = AScale then
        TdxGaugeCustomScaleAccess(Control.Scales[I]).AnchorScale := TdxGaugeCustomScaleAccess(AScale).AnchorScale;
    Control.Scales.EndUpdate(False);
  end;
end;

function TdxCustomGaugeControlController.CheckDesignPopupMenu(AShift: TShiftState): Boolean;
begin
  Result := ssRight in AShift;
  if Result then
    ShowDesignPopupMenu(AShift);
end;

function TdxCustomGaugeControlController.CreatePopupMenuItem(const ACaption: string; ATag: Integer;
  AOnClick: TNotifyEvent): TMenuItem;
begin
  Result := TMenuItem.Create(nil);
  Result.Caption := ACaption;
  Result.Tag := ATag;
  Result.OnClick := AOnClick;
end;

procedure TdxCustomGaugeControlController.MenuItemOnClickHandler(ASender: TObject);

  procedure DoRestoreSelectionsParameters;
  var
    I: Integer;
  begin
    Control.BeginUpdate;
    for I := 0 to Selections.Count - 1 do
      TdxGaugeCustomScale(Selections[I]).RestoreStyleParameters;
    Control.EndUpdate;
    Control.Modified;
  end;

  function GetZOrderChangeType(AMenuItemTag: Integer): TdxGaugeScaleZOrderChangeType;
  begin
    Result := TdxGaugeScaleZOrderChangeType(AMenuItemTag);
  end;

var
  AItem: TMenuItem;
begin
  AItem := ASender as TMenuItem;
  case AItem.Tag of
    4:
      DoRestoreSelectionsParameters;
    5:
      ShowRangesEditor;
    6:
      if Control.IsDesigning then
        TdxGaugeControlCustomSelectionHelperAccess(FSelectionHelper).DeleteSelections;
    else
      ChangeSelectionsZOrders(GetZOrderChangeType(AItem.Tag));
  end;
end;

procedure TdxCustomGaugeControlController.PopupDesignMenu(const APopupPoint: TPoint);
begin
  FDesignPopupMenu := TPopupMenu.Create(nil);
  try
    PopulateDesignPopupMenu;
    FDesignPopupMenu.Popup(APopupPoint.X, APopupPoint.Y);
    Application.ProcessMessages;
  finally
    FreeAndNil(FDesignPopupMenu);
  end;
end;

procedure TdxCustomGaugeControlController.PopulateDesignPopupMenu;

  function ShowRangesMenuItem: Boolean;
  begin
    Result := (TObject(Selections[0]) is TdxGaugeQuantitativeScale) and not (Selections.Count > 1);
  end;

  procedure AddZOrdersMenuItems;
  const
    dxGaugeScaleZOrderChangeTypeName: array[TdxGaugeScaleZOrderChangeType] of string =
      ('Send Backward', 'Bring Forward', 'Bring to Front', 'Send to Back');
  var
    I: TdxGaugeScaleZOrderChangeType;
  begin
    if ShowRangesMenuItem then
      FDesignPopupMenu.Items.Add(CreatePopupMenuItem('Edit Ranges...', 5, MenuItemOnClickHandler));
    FDesignPopupMenu.Items.Add(NewLine);
    for I := Low(TdxGaugeScaleZOrderChangeType) to High(TdxGaugeScaleZOrderChangeType) do
      FDesignPopupMenu.Items.Add(CreatePopupMenuItem(dxGaugeScaleZOrderChangeTypeName[I], Integer(I),
        MenuItemOnClickHandler));
    FDesignPopupMenu.Items.Add(NewLine);
  end;

  procedure AddRestoreScaleParametersMenuItem;
  const
    sdxGaugeScaleRestoreStyleParameters = 'Restore Style Parameters';
  begin
    FDesignPopupMenu.Items.Add(CreatePopupMenuItem(sdxGaugeScaleRestoreStyleParameters, 4, MenuItemOnClickHandler));
  end;

  procedure AddDeleteScaleMenuItem;
  begin
    FDesignPopupMenu.Items.Add(CreatePopupMenuItem('-', 0, nil));
    FDesignPopupMenu.Items.Add(CreatePopupMenuItem('Delete', 6, MenuItemOnClickHandler));
    FDesignPopupMenu.Items.Add(CreatePopupMenuItem('-', 0, nil));
  end;

begin
  AddZOrdersMenuItems;
  AddDeleteScaleMenuItem;
  AddRestoreScaleParametersMenuItem;
end;

procedure TdxCustomGaugeControlController.ShowDesignPopupMenu(AShift: TShiftState);
begin
  if Selections.IndexOf(HitTest.HitObject) = -1 then
    Select(HitTest.HitObject, [ssLeft]);
  PopupDesignMenu(GetMouseCursorPos);
end;

function TdxCustomGaugeControlController.CanCalculate(const ABounds: TRect): Boolean;
begin
  Result := (cxRectWidth(ABounds) > 0) and (cxRectHeight(ABounds) > 0);
end;

function TdxCustomGaugeControlController.CheckComponentState: Boolean;
begin
  Result := FControl.ComponentState * [csLoading, csReading, csDestroying] = [];
end;

function TdxCustomGaugeControlController.GetScaleBounds(AScale: TdxGaugeCustomScale): TdxRectF;
var
  AAnchorScaleIndex: Integer;
begin
  AAnchorScaleIndex := TdxGaugeCustomScaleAccess(AScale).AnchorScaleIndex;
  if AAnchorScaleIndex = -1 then
    Result := cxRectF(FControl.ClientBounds)
  else
    Result := TdxGaugeCustomScaleAccess(FControl.Scales[AAnchorScaleIndex]).ContentBounds;
end;

procedure TdxCustomGaugeControlController.Calculate;
var
  AScales: TList;
begin
  if FControl.HandleAllocated and CheckComponentState and CanCalculate(FControl.ClientBounds) then
  begin
    AScales := TList.Create;
    try
      GetScalesByDependences(AScales);
      DoCalculateScales(AScales);
    finally
      AScales.Free;
    end;
  end;
end;

procedure TdxCustomGaugeControlController.DoCalculateScales(AScaleList: TList);

  procedure PopulateChangedLayers(AScaleChangedLayers: TdxGaugeScaleChangedLayers);
  var
    ALayer: TdxGaugeScaleChangedLayer;
  begin
    for ALayer in AScaleChangedLayers do
      Include(FChangedLayers, ALayer);
  end;

var
  I: Integer;
  AScale: TdxGaugeCustomScaleAccess;
begin
  FLayerCount := 0;
  for I := 0 to AScaleList.Count - 1 do
  begin
    AScale := TdxGaugeCustomScaleAccess(AScaleList[I]);
    AScale.Calculate(GetScaleBounds(AScale), FControl.ClientBounds);
    FLayerCount := Max(FLayerCount, AScale.GetLayerCount);
    PopulateChangedLayers(AScale.ChangedLayers);
    AScale.ChangedLayers := [];
  end;
end;

{ TdxGaugeControlDragAndDropObject }

function TdxGaugeControlDragAndDropObject.ProcessKeyDown(AKey: Word; AShiftState: TShiftState): Boolean;
begin
  Result := AKey <> VK_ESCAPE;
end;

function TdxGaugeControlDragAndDropObject.GetDragAndDropCursor(Accepted: Boolean): TCursor;
begin
  Result := crDefault;
end;

procedure TdxGaugeControlDragAndDropObject.BeginDragAndDrop;
begin
  InitDragImages;
  inherited BeginDragAndDrop;
end;

procedure TdxGaugeControlDragAndDropObject.DragAndDrop(const P: TPoint; var Accepted: Boolean);
begin
  Accepted := PtInRect(Control.ClientBounds, P);
  ShowDragImages(GetMouseCursorPos);
  inherited DragAndDrop(P, Accepted);
end;

procedure TdxGaugeControlDragAndDropObject.EndDragAndDrop(Accepted: Boolean);
begin
  if Accepted then
    GetControl.Controller.MoveScales(cxPointF(GetMousePosDelta));
  FreeAndNil(FDragImages);
  inherited EndDragAndDrop(Accepted);
  TdxCustomGaugeControlAccess(Control).Modified;
end;

procedure TdxGaugeControlDragAndDropObject.Init(ADragScale: TdxGaugeCustomScale; const P: TPoint);
begin
  FStartDragPoint := P;
end;

function TdxGaugeControlDragAndDropObject.CreateDragImage(ADragScale: TdxGaugeCustomScaleAccess): TdxGaugeControlDragImage;
begin
  Result := TdxGaugeControlDragImage.Create;
  Result.SetBounds(0, 0, GetScaleSize(ADragScale).cx, GetScaleSize(ADragScale).cy);
  Result.Offset := cxPointOffset(cxRect(ADragScale.ContentBounds, False).TopLeft, FStartDragPoint, False);
end;

function TdxGaugeControlDragAndDropObject.GetControl: TdxCustomGaugeControl;
begin
  Result := Control as TdxCustomGaugeControl;
end;

function TdxGaugeControlDragAndDropObject.GetMousePosDelta: TPoint;
begin
  Result.X := CurMousePos.X - FStartDragPoint.X;
  Result.Y := CurMousePos.Y - FStartDragPoint.Y;
end;

function TdxGaugeControlDragAndDropObject.GetScaleSize(ADragScale: TdxGaugeCustomScale): TSize;
begin
  Result := cxRectSize(TdxGaugeCustomScaleAccess(ADragScale).GetSelectionRect);
end;

procedure TdxGaugeControlDragAndDropObject.DrawDragImage(ACanvas: TcxCanvas; const R: TRect; AScale: TdxGaugeCustomScale);
var
  AGPGraphics: TdxGPGraphics;
  AScaleAccess: TdxGaugeCustomScaleAccess;
begin
  AScaleAccess := TdxGaugeCustomScaleAccess(AScale);
  AGPGraphics := dxGpBeginPaint(ACanvas.Handle, R);
  try
    AGPGraphics.SmoothingMode := smAntiAlias;
    AGPGraphics.TranslateWorldTransform(-AScaleAccess.ContentBounds.Left, -AScaleAccess.ContentBounds.Top);
    GetControl.Painter.DrawScale(AGPGraphics, R, AScaleAccess.Index);
  finally
    dxGpEndPaint(AGPGraphics);
  end;
end;

procedure TdxGaugeControlDragAndDropObject.InitDragImages;
var
  ADragScaleList: TList;
  ATempList: TList;
begin
  FDragImages := TObjectList.Create(True);
  ADragScaleList := TList.Create;
  ADragScaleList.Assign(GetControl.Controller.Selections);
  ATempList := GetControl.Controller.CreateAnchoredScaleList(ADragScaleList);
  try
    ADragScaleList.Assign(ATempList, laOr);
    PopulateDragImages(ADragScaleList);
  finally
    ATempList.Free;
    ADragScaleList.Free;
  end;
end;

procedure TdxGaugeControlDragAndDropObject.PopulateDragImages(AScaleList: TList);

  function CompareByZOrder(AItem1, AItem2: Pointer): Integer;
  var
    AScale1, AScale2: TdxGaugeCustomScaleAccess;
  begin
    AScale1 := TdxGaugeCustomScaleAccess(AItem1);
    AScale2 := TdxGaugeCustomScaleAccess(AItem2);
    Result := dxCompareValues(AScale1.ZOrder, AScale2.ZOrder);
  end;

var
  I: Integer;
  ADragImage: TdxGaugeControlDragImage;
begin
  AScaleList.Sort(@CompareByZOrder);
  for I := 0 to AScaleList.Count - 1 do
  begin
    ADragImage := CreateDragImage(TdxGaugeCustomScaleAccess(AScaleList[I]));
    DrawDragImage(ADragImage.Canvas, ADragImage.ClientRect, TdxGaugeCustomScaleAccess(AScaleList[I]));
    FDragImages.Add(ADragImage);
  end;
end;

procedure TdxGaugeControlDragAndDropObject.ShowDragImages(const ACursorPos: TPoint);
var
  I: Integer;
begin
  if FDragImages <> nil then
    for I := 0 to FDragImages.Count - 1 do
    begin
      TdxGaugeControlDragImage(FDragImages[I]).MoveTo(cxPointOffset(ACursorPos,
        TdxGaugeControlDragImage(FDragImages[I]).Offset));
      TdxGaugeControlDragImage(FDragImages[I]).Visible := True;
    end;
end;

{ TdxCustomGaugeControlPainter }

constructor TdxCustomGaugeControlPainter.Create(AControl: TdxCustomGaugeControl);
begin
  inherited Create;
  FControl := AControl;

  FStaticElementsLayer := TcxBitmap32.CreateSize(FControl.Width, FControl.Height, True);
  SetLength(FScaleStaticLayerIndexes, 2);
  FScaleStaticLayerIndexes[0] := 0;
  FScaleStaticLayerIndexes[1] := 1;

  FDynamicElementsLayer := TcxBitmap32.CreateSize(FControl.Width, FControl.Height, True);
  SetLength(FScaleDynamicLayerIndexes, 2);
  FScaleDynamicLayerIndexes[0] := 2;
  FScaleDynamicLayerIndexes[1] := 3;
end;

destructor TdxCustomGaugeControlPainter.Destroy;
begin
  SetLength(FScaleDynamicLayerIndexes, 0);
  FreeAndNil(FDynamicElementsLayer);
  SetLength(FScaleStaticLayerIndexes, 0);
  FreeAndNil(FStaticElementsLayer);
  inherited Destroy;
end;

procedure TdxCustomGaugeControlPainter.DrawScale(AGPGraphics: TdxGPGraphics; const R: TRect; AScaleIndex: Integer);
var
  I: Integer;
  AScaleAccess: TdxGaugeCustomScaleAccess;
begin
  AScaleAccess := TdxGaugeCustomScaleAccess(FControl.Scales[AScaleIndex]);
  for I := 0 to AScaleAccess.GetLayerCount - 1 do
    AScaleAccess.DrawLayer(AGPGraphics, I);
end;

procedure TdxCustomGaugeControlPainter.Paint(ACanvas: TcxCanvas; ADrawMode: TdxGaugeDrawMode);
var
  AGPGraphics: TdxGPGraphics;
begin
  if not FControl.Locked then
  begin
    DrawBackground(ACanvas);
    AGPGraphics := dxGpBeginPaint(ACanvas.Handle, FControl.ClientBounds);
    try
      AGPGraphics.SmoothingMode := smAntiAlias;
      DrawScales(AGPGraphics, ADrawMode);
      DrawSelections(AGPGraphics);
    finally
      dxGpEndPaint(AGPGraphics);
    end;
    if NeedDrawSelector then
      DrawControlSelector(ACanvas);
    DrawBorders(ACanvas);
  end;
end;

function TdxCustomGaugeControlPainter.NeedDrawSelector: Boolean;
begin
  Result := FControl.IsDesigning and not FControl.IsCreatingPreview;
end;

procedure TdxCustomGaugeControlPainter.DrawBackground(ACanvas: TcxCanvas);
begin
  FControl.LookAndFeelPainter.DrawGaugeControlBackground(ACanvas, FControl.Bounds, FControl.Transparent,
    FControl.Color);
end;

procedure TdxCustomGaugeControlPainter.DrawBorders(ACanvas: TcxCanvas);
begin
  if FControl.BorderStyle = cxcbsDefault then
    FControl.LookAndFeelPainter.DrawBorder(ACanvas, FControl.Bounds);
end;

procedure TdxCustomGaugeControlPainter.DrawBitmaps(AGPGraphics: TdxGPGraphics);
begin
  AGPGraphics.DrawBitmap(FStaticElementsLayer, FControl.ClientRect);
  AGPGraphics.DrawBitmap(FDynamicElementsLayer, FControl.ClientRect);
end;

procedure TdxCustomGaugeControlPainter.DrawControlSelector(ACanvas: TcxCanvas);
begin
  cxDrawDesignRect(ACanvas, dxGaugeGetSelectorRect(FControl.ClientBounds), FControl.Controller.IsControlSelected);
end;

procedure TdxCustomGaugeControlPainter.DrawGaugeLayer(ABitmap: TcxBitmap32; const AScaleLayerIndexes: array of Integer);
var
  AGPGraphics: TdxGPGraphics;
begin
  AGPGraphics := dxGpBeginPaint(ABitmap.Canvas.Handle, Rect(0, 0, ABitmap.Width, ABitmap.Height));
  try
    AGPGraphics.SmoothingMode := smAntiAlias;
    DrawLayer(AGPGraphics, AScaleLayerIndexes);
  finally
    dxGpEndPaint(AGPGraphics);
  end;
end;

procedure TdxCustomGaugeControlPainter.DrawLayer(AGPGraphics: TdxGPGraphics; const AScaleLayerIndexes: array of Integer);
var
  ALayerIndex, AScaleIndex: Integer;
begin
  for ALayerIndex := Low(AScaleLayerIndexes) to High(AScaleLayerIndexes) do
    for AScaleIndex := 0 to FControl.Controller.ZOrders.Count - 1 do
      TdxGaugeCustomScaleAccess(FControl.Controller.ZOrders[AScaleIndex]).DrawLayer(AGPGraphics,AScaleLayerIndexes[ALayerIndex]);
end;

procedure TdxCustomGaugeControlPainter.DrawScales(AGPGraphics: TdxGPGraphics; ADrawMode: TdxGaugeDrawMode);

  function GetScaleIndex(AZOrder: Integer): Integer;
  begin
    Result := TdxGaugeCustomScale(FControl.Controller.ZOrders[AZOrder]).Index;
  end;

var
  I: Integer;
begin
  case ADrawMode of
    dmByLayer:
      begin
        PrepareBitmaps;
        DrawBitmaps(AGPGraphics);
      end;
    dmByScale:
      begin
        FDynamicElementsLayer.SetSize(0, 0);
        FStaticElementsLayer.SetSize(0, 0);
        for I := 0 to FControl.Controller.ZOrders.Count - 1 do
          DrawScale(AGPGraphics, FControl.ClientBounds, GetScaleIndex(I));
        FControl.Controller.ChangedLayers := [];
      end;
  end;
end;

procedure TdxCustomGaugeControlPainter.DrawSelections(AGPGraphics: TdxGPGraphics);

  function GetScaleSelectionRects(AScaleList: TList): TRects;
  var
    I: Integer;
  begin
    SetLength(Result, AScaleList.Count);
    for I := 0 to AScaleList.Count - 1 do
      Result[I] := TdxGaugeCustomScaleAccess(AScaleList[I]).GetSelectionRect;
  end;

type
  dxDrawRectsProc = procedure(AGPGraphics: TdxGPGraphics; const ARects: TRects);

  procedure DrawRects(AGPGraphics: TdxGPGraphics; AScaleList: TList; ADrawRectsProc: dxDrawRectsProc);
  var
    ARects: TRects;
  begin
    ARects := GetScaleSelectionRects(AScaleList);
    ADrawRectsProc(AGPGraphics, ARects);
    SetLength(ARects, 0);
  end;

var
  AAnchoredScales: TList;
begin
  if FControl.IsDesigning then
  begin
    AAnchoredScales := FControl.Controller.CreateAnchoredScaleList(FControl.Controller.Selections);
    try
      DrawRects(AGPGraphics, AAnchoredScales, @dxGaugeDrawRectangles);
      DrawRects(AGPGraphics, FControl.Controller.Selections, @dxGaugeDrawSelections);
    finally
      AAnchoredScales.Free;
    end;
  end;
end;

procedure TdxCustomGaugeControlPainter.PrepareBitmap(ABitmap: TcxBitmap32);
begin
  ABitmap.BeginUpdate;
  ABitmap.SetSize(FControl.Width, FControl.Height);
  ABitmap.Clear;
  ABitmap.EndUpdate(True);
end;

procedure TdxCustomGaugeControlPainter.PrepareBitmaps;

  procedure ExcludeChangeLayer(AChangedLayer: TdxGaugeScaleChangedLayer);
  begin
    FControl.Controller.ChangedLayers := FControl.Controller.ChangedLayers - [AChangedLayer];
  end;

  procedure PrepareLayerBitmap(AChangedLayer: TdxGaugeScaleChangedLayer);
  var
    ABitmap: TcxBitmap32;
    AScaleLayerIndexes: array of Integer;
  begin
    if AChangedLayer in FControl.Controller.ChangedLayers then
    begin
      ABitmap := nil;
      case AChangedLayer of
        sclStaticLayer:
          begin
            ABitmap := FStaticElementsLayer;
            AScaleLayerIndexes := Pointer(FScaleStaticLayerIndexes);
            ExcludeChangeLayer(sclStaticLayer);
          end;
        sclDynamicLayer:
          begin
            ABitmap := FDynamicElementsLayer;
            AScaleLayerIndexes := Pointer(FScaleDynamicLayerIndexes);
            ExcludeChangeLayer(sclDynamicLayer);
          end;
      end;
      PrepareBitmap(ABitmap);
      DrawGaugeLayer(ABitmap, AScaleLayerIndexes);
    end;
  end;

begin
  PrepareLayerBitmap(sclStaticLayer);
  PrepareLayerBitmap(sclDynamicLayer);
end;

end.
