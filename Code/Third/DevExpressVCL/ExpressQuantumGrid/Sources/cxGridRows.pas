{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressQuantumGrid                                       }
{                                                                    }
{           Copyright (c) 1998-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSQUANTUMGRID AND ALL            }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY. }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit cxGridRows;

{$I cxVer.inc}

interface

uses
  Types, Windows, Classes, Graphics, Controls, Forms, StdCtrls,
  dxCore, cxClasses, cxControls, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  dxCoreClasses, cxCustomData, cxPC, cxGridCommon, cxGridLevel, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDetailsSite, cxScrollBar,
  cxGridInplaceEditForm;

const
  htPreview = htGridBase + 31;
  htInplaceEditFormAreaScrollBar = htGridBase + 32;
  htInplaceEditFormButton = htGridBase + 33;
  htInplaceEditFormCancelButton = htGridBase + 34;
  htInplaceEditFormUpdateButton = htGridBase + 35;
  htInplaceEditFormButtonsPanel = htGridBase + 36;
  htInplaceEditFormArea = htGridBase + 37;
  cxGridInplaceEditFormButtonsOffset = 15;
  cxGridInplaceEditFormButtonsTopOffset = 7;
  cxGridInplaceEditFormButtonsBottomOffset = 11;
  cxGridInplaceEditFormButtonMinHeight = 23;
  cxGridInplaceEditFormButtonMinWidth = 76;

type
  TcxGridDataCellViewInfoClass = class of TcxGridDataCellViewInfo;
  TcxGridDataCellViewInfo = class;
  TcxGridDataRowCellsAreaViewInfo = class;
  TcxGridDataRowViewInfo = class;
  TcxGridNewItemRowViewInfo = class;
  TcxGridGroupCellViewInfo = class;
  TcxGridGroupSummaryCellViewInfo = class;
  TcxGridGroupRowViewInfo = class;
  TcxGridMasterDataRowDetailsSiteLeftTabsViewInfo = class;
  TcxGridDetailsSiteViewInfo = class;
  TcxGridExpandButtonCellViewInfo = class;
  TcxGridMasterDataRowViewInfo = class;
  TcxGridInplaceEditFormAreaViewInfo = class;
  TcxGridInplaceEditFormAreaScrollBarViewInfo = class;
  TcxGridInplaceEditFormButtonsPanelViewInfo = class;
  TcxGridInplaceEditFormButtonViewInfo = class;

  { hit tests }

  TcxGridPreviewHitTest = class(TcxGridRecordCellHitTest)
  protected
    class function GetHitTestCode: Integer; override;
  end;

  TcxGridInplaceEditFormAreaScrollBarHitTest = class(TcxCustomGridViewHitTest)
  protected
    class function GetHitTestCode: Integer; override;
  end;

  TcxGridInplaceEditFormButtonHitTest = class(TcxCustomGridViewHitTest)
  protected
    class function GetHitTestCode: Integer; override;
  end;

  TcxGridInplaceEditFormCancelButtonHitTest = class(TcxCustomGridViewHitTest)
  protected
    class function GetHitTestCode: Integer; override;
  end;

  TcxGridInplaceEditFormUpdateButtonHitTest = class(TcxCustomGridViewHitTest)
  protected
    class function GetHitTestCode: Integer; override;
  end;

  TcxGridInplaceEditFormButtonsPanelHitTest = class(TcxCustomGridViewHitTest)
  protected
    class function GetHitTestCode: Integer; override;
  end;

  TcxGridInplaceEditFormAreaHitTest = class(TcxCustomGridViewHitTest)
  protected
    class function GetHitTestCode: Integer; override;
  end;

  { painters }

  { TcxGridDataCellPainter }

  TcxGridDataCellPainter = class(TcxGridTableDataCellPainter)
  private
    function GetViewInfo: TcxGridDataCellViewInfo;
  protected
    function CanDelayedDrawBorder: Boolean;
    procedure DrawBorder(ABorder: TcxBorder); override;
    function ExcludeFromClipRect: Boolean; override;
    property ViewInfo: TcxGridDataCellViewInfo read GetViewInfo;
  end;

  { TcxGridInplaceEditFormButtonPainter }

  TcxGridInplaceEditFormButtonPainter = class(TcxGridCustomButtonPainter)
  private
    function GetViewInfo: TcxGridInplaceEditFormButtonViewInfo;
  protected
    function CanDrawFocusRect: Boolean; override;
    procedure DrawButton; virtual;
    function ExcludeFromClipRect: Boolean; override;
    function GetGridViewInfo: TcxCustomGridViewInfo; override;
    procedure Paint; override;

    property ViewInfo: TcxGridInplaceEditFormButtonViewInfo read GetViewInfo;
  end;

  { TcxGridInplaceEditFormButtonsPanelPainter }

  TcxGridInplaceEditFormButtonsPanelPainter = class(TcxCustomGridCellPainter)
  private
    function GetViewInfo: TcxGridInplaceEditFormButtonsPanelViewInfo;
  protected
    procedure DrawBackground; override;
    procedure DrawButtons; virtual;
    procedure DrawContent; override;
    procedure DrawSeparator; virtual;

    property ViewInfo: TcxGridInplaceEditFormButtonsPanelViewInfo read GetViewInfo;
  end;

  { TcxGridInplaceEditFormAreaScrollBarPainter }

  TcxGridInplaceEditFormAreaScrollBarPainter = class(TcxCustomGridCellPainter)
  private
    function GetViewInfo: TcxGridInplaceEditFormAreaScrollBarViewInfo;
  protected
    procedure DrawContent; override;

    property ViewInfo: TcxGridInplaceEditFormAreaScrollBarViewInfo read GetViewInfo;
  end;

  { TcxGridInplaceEditFormAreaPainter }

  TcxGridInplaceEditFormAreaPainter = class(TcxCustomGridCellPainter)
  private
    function GetViewInfo: TcxGridInplaceEditFormAreaViewInfo;
  protected
    procedure DrawBackground; override;
    procedure DrawButtonsPanel; virtual;
    procedure DrawContent; override;
    procedure DrawGridViewItems; virtual;
    procedure DrawInplaceEditFormContainer; virtual;
    procedure DrawLayoutGroups;
    procedure DrawScrollBars; virtual;

    property ViewInfo: TcxGridInplaceEditFormAreaViewInfo read GetViewInfo;
  end;

  { TcxGridDataRowPainter }

  TcxGridDataRowPainter = class(TcxCustomGridRowPainter)
  private
    function GetViewInfo: TcxGridDataRowViewInfo;
  protected
    procedure DrawCells; virtual;
    procedure DrawInplaceEditFormArea; virtual;
    procedure DrawTopRightEmptyArea; virtual;
    function GetShowCells: Boolean; virtual;
    procedure Paint; override;

    property ShowCells: Boolean read GetShowCells;
    property ViewInfo: TcxGridDataRowViewInfo read GetViewInfo;
  end;

  { TcxGridNewItemRowPainter }

  TcxGridNewItemRowPainter = class(TcxGridDataRowPainter)
  private
    function GetViewInfo: TcxGridNewItemRowViewInfo;
  protected
    procedure DrawBackground; override;
    procedure DrawSeparator; override;
    function ExcludeFromClipRect: Boolean; override;
    function GetShowCells: Boolean; override;
    procedure Paint; override;
    property ViewInfo: TcxGridNewItemRowViewInfo read GetViewInfo;
  end;

  { TcxGridMasterDataRowDetailsSiteTabsPainter }

  TcxGridMasterDataRowDetailsSiteTabsPainter = class(TcxGridDetailsSiteTabsPainter)
  private
    function GetViewInfo: TcxGridMasterDataRowDetailsSiteLeftTabsViewInfo;
  protected
    procedure DrawBottomGridLine; virtual;
    procedure Paint; override;

    property ViewInfo: TcxGridMasterDataRowDetailsSiteLeftTabsViewInfo read GetViewInfo;
  end;

  { TcxGridExpandButtonCellPainter }

  TcxGridExpandButtonCellPainter = class(TcxCustomGridCellPainter)
  private
    function GetViewInfo: TcxGridExpandButtonCellViewInfo;
  protected
    procedure DrawBorder(ABorder: TcxBorder); override;
    property ViewInfo: TcxGridExpandButtonCellViewInfo read GetViewInfo;
  end;

  { TcxGridMasterDataRowPainter }

  TcxGridMasterDataRowPainter = class(TcxGridDataRowPainter)
  private
    function GetViewInfo: TcxGridMasterDataRowViewInfo;
  protected
    procedure DrawCells; override;
    procedure DrawDetailsSite; virtual;
    //procedure DrawDetailsArea; virtual;
    procedure DrawExpandButtonCell; virtual;
    function NeedsPainting: Boolean; override;
    procedure Paint; override;

    property ViewInfo: TcxGridMasterDataRowViewInfo read GetViewInfo;
  end;

  { TcxGridGroupCellPainter }

  TcxGridGroupCellPainter = class(TcxCustomGridCellPainter)
  private
    function GetViewInfo: TcxGridGroupCellViewInfo;
  protected
    procedure DrawBorder(ABorder: TcxBorder); override;

    property ViewInfo: TcxGridGroupCellViewInfo read GetViewInfo;
  end;

  { TcxGridGroupSummaryCellPainter }

  TcxGridGroupSummaryCellPainter = class(TcxCustomGridCellPainter)
  private
    function GetViewInfo: TcxGridGroupSummaryCellViewInfo;
  protected
    procedure Paint; override;

    property ViewInfo: TcxGridGroupSummaryCellViewInfo read GetViewInfo;
  end;

  { TcxGridGroupRowPainter }

  TcxGridGroupRowPainter = class(TcxCustomGridRowPainter)
  private
    function GetViewInfo: TcxGridGroupRowViewInfo;
  protected
    procedure DrawBackground; override;
    procedure DrawSeparator; override;
    procedure DrawSummaryCells; virtual;
    procedure Paint; override;

    property ViewInfo: TcxGridGroupRowViewInfo read GetViewInfo;
  end;

  { view infos }

  { TcxGridCellViewInfo }

  TcxGridCellViewInfo = class(TcxGridTableCellViewInfo)
  private
    function GetGridView: TcxGridTableView;
    function GetGridLines: TcxGridLines;
    function GetGridRecord: TcxCustomGridRow;
    function GetGridViewInfo: TcxGridTableViewInfo;
    function GetRecordViewInfo: TcxCustomGridRowViewInfo;
  protected
    function GetBorderColor(AIndex: TcxBorder): TColor; override;
    function GetBorderWidth(AIndex: TcxBorder): Integer; override;
    procedure GetViewParams(var AParams: TcxViewParams); override;

    property GridLines: TcxGridLines read GetGridLines;
  public
    property GridRecord: TcxCustomGridRow read GetGridRecord;
    property GridView: TcxGridTableView read GetGridView;
    property GridViewInfo: TcxGridTableViewInfo read GetGridViewInfo;
    property RecordViewInfo: TcxCustomGridRowViewInfo read GetRecordViewInfo;
  end;

  // data row

  { TcxGridDataCellViewInfo }

  TcxGridDataCellViewInfo = class(TcxGridTableDataCellViewInfo)
  private
    FIsMerged: Boolean;
    FIsMerging: Boolean;
    FMergedCells: TList;
    FMergingCell: TcxGridDataCellViewInfo;
    FOriginalHeight: Integer;
    function GetCacheItem: TcxGridTableViewInfoCacheItem;
    function GetGridView: TcxGridTableView;
    function GetGridViewInfo: TcxGridTableViewInfo;
    function GetItem: TcxGridColumn;
    function GetMergedCell(Index: Integer): TcxGridDataCellViewInfo;
    function GetMergedCellCount: Integer;
    function GetMergedCellOfFocusedRow: TcxGridDataCellViewInfo;
    function GetRecordViewInfo: TcxGridDataRowViewInfo;
  protected
    OriginalBounds: TRect;
    procedure AfterRowsViewInfoCalculate; virtual;
    procedure AfterRowsViewInfoOffset; virtual;
    function CalculateSelected: Boolean; override;
    function CalculateWidth: Integer; override;
    function CanBeMergingCell: Boolean; virtual;
    function CanCellMerging: Boolean; virtual;
    function GetAlwaysSelected: Boolean; override;
    function GetBorderColor(AIndex: TcxBorder): TColor; override;
    function GetBorders: TcxBorders; override;
    function GetBorderWidth(AIndex: TcxBorder): Integer; override;
    function GetFocused: Boolean; override;
    function GetMultiLine: Boolean; override;
    function GetPainterClass: TcxCustomGridCellPainterClass; override;
    function GetVisible: Boolean; override;
    function GetVisibleForHitTest: Boolean; override;
    function HasHitTestPoint(const P: TPoint): Boolean; override;
    procedure Offset(DX, DY: Integer); override;
    procedure RemoveMergedCell(ACellViewInfo: TcxGridDataCellViewInfo);
    function SupportsEditing: Boolean; override;

    property CacheItem: TcxGridTableViewInfoCacheItem read GetCacheItem;
    property MergedCellOfFocusedRow: TcxGridDataCellViewInfo read GetMergedCellOfFocusedRow;
    property GridView: TcxGridTableView read GetGridView;
    property GridViewInfo: TcxGridTableViewInfo read GetGridViewInfo;
    property OriginalHeight: Integer read FOriginalHeight;
  public
    destructor Destroy; override;
    procedure BeforeRecalculation; override;
    function CanDrawSelected: Boolean; override;
    procedure Calculate(ALeftBound, ATopBound: Integer; AWidth: Integer = -1;
      AHeight: Integer = -1); override;
    function MouseMove(AHitTest: TcxCustomGridHitTest; AShift: TShiftState): Boolean; override;
    procedure Paint(ACanvas: TcxCanvas = nil); override;

    property IsMerged: Boolean read FIsMerged;
    property IsMerging: Boolean read FIsMerging;
    property Item: TcxGridColumn read GetItem;
    property MergedCellCount: Integer read GetMergedCellCount;
    property MergedCells[Index: Integer]: TcxGridDataCellViewInfo read GetMergedCell;
    property MergingCell: TcxGridDataCellViewInfo read FMergingCell;
    property RecordViewInfo: TcxGridDataRowViewInfo read GetRecordViewInfo;
  end;

  { TcxGridDataRowCellsAreaViewInfo }

  TcxGridDataRowCellsAreaViewInfoClass = class of TcxGridDataRowCellsAreaViewInfo;

  TcxGridDataRowCellsAreaViewInfo = class(TcxGridCellViewInfo)
  private
    function GetRecordViewInfo: TcxGridDataRowViewInfo;
  protected
    function CalculateHeight: Integer; override;
    function CalculateVisible: Boolean; virtual;
    function CalculateWidth: Integer; override;
    function GetBorders: TcxBorders; override;
    function GetIsBottom: Boolean; virtual;
  public
    constructor Create(ARecordViewInfo: TcxCustomGridRecordViewInfo); override;
    function CanDrawSelected: Boolean; override;
    function DrawMergedCells: Boolean; virtual;

    property IsBottom: Boolean read GetIsBottom;
    property RecordViewInfo: TcxGridDataRowViewInfo read GetRecordViewInfo;
  end;

  { TcxGridPreviewCellViewInfo }

  TcxGridPreviewCellViewInfoClass = class of TcxGridPreviewCellViewInfo;

  TcxGridPreviewCellViewInfo = class(TcxGridDataCellViewInfo)
  private
    function GetPreview: TcxGridPreview;
  protected
    function CalculateHeight: Integer; override;
    function CalculateWidth: Integer; override;
    function GetAutoHeight: Boolean; override;
    function GetBackgroundBitmap: TBitmap; override;
    function GetBorders: TcxBorders; override;
    procedure GetEditViewDataContentOffsets(var R: TRect); override;
    function GetHeight: Integer; override;
    function GetHitTestClass: TcxCustomGridHitTestClass; override;
    function GetMaxLineCount: Integer; override;
    function GetMultiLine: Boolean; override;
    function GetTextAreaBounds: TRect; override;
    function SupportsZeroHeight: Boolean; override;

    property Preview: TcxGridPreview read GetPreview;
  end;

  { TcxGridInplaceEditFormAreaScrollBar }

  TcxGridInplaceEditFormAreaScrollBar = class(TcxControlScrollBarHelper)
  private
    FViewInfo: TcxGridInplaceEditFormAreaViewInfo;
  protected
    procedure Scroll(ScrollCode: TScrollCode; var ScrollPos: Integer); override;
  public
    constructor Create(AOwner: IcxScrollBarOwner; AViewInfo: TcxGridInplaceEditFormAreaViewInfo); reintroduce; virtual;

    property ViewInfo: TcxGridInplaceEditFormAreaViewInfo read FViewInfo;
  end;

  TcxGridInplaceEditFormAreaScrollBarClass = class of TcxGridInplaceEditFormAreaScrollBar;

  { TcxGridInplaceEditFormAreaScrollBarViewInfo }

  TcxGridInplaceEditFormAreaScrollBarViewInfo = class(TcxCustomGridViewCellViewInfo, IcxScrollBarOwner)
  private
    FViewInfo: TcxGridInplaceEditFormAreaViewInfo;
    FScrollBar: TcxGridInplaceEditFormAreaScrollBar;
    FScrollBarController: TcxScrollBarController;

    procedure CreateScrollBar;
    procedure DestroyScrollBar;

    function GetKind: TScrollBarKind;
    function GetMax: Integer;
    function GetMin: Integer;
    function GetPageSize: Integer;
    function GetPosition: Integer;
    procedure SetKind(Value: TScrollBarKind);
    procedure SetMax(Value: Integer);
    procedure SetMin(Value: Integer);
    procedure SetPageSize(Value: Integer);
    procedure SetPosition(Value: Integer);

    //IcxScrollBarOwner
    function IcxScrollBarOwner.GetControl = GetScrollBarOwner;
    function GetScrollBarOwner: TWinControl;
    function GetLookAndFeel: TcxLookAndFeel;
  protected
    function CalculateHeight: Integer; override;
    function CalculateWidth: Integer; override;
    function GetHeight: Integer; override;
    function GetHitTestClass: TcxCustomGridHitTestClass; override;
    function GetPainterClass: TcxCustomGridCellPainterClass; override;
    function GetScrollBarClass: TcxGridInplaceEditFormAreaScrollBarClass; virtual;
    function GetScrollBarControllerClass: TcxScrollBarControllerClass; virtual;
    function GetWidth: Integer; override;
    procedure SetHeight(Value: Integer); override;
    procedure SetWidth(Value: Integer); override;

    function CaptureMouseOnPress: Boolean; override;
    function GetHotTrack: Boolean; override;

    property ScrollBar: TcxGridInplaceEditFormAreaScrollBar read FScrollBar;
  public
    constructor Create(AViewInfo: TcxGridInplaceEditFormAreaViewInfo); reintroduce; virtual;
    destructor Destroy; override;

    procedure Calculate(ALeftBound, ATopBound: Integer; AWidth: Integer = -1;
      AHeight: Integer = -1); override;
    procedure SetScrollParams(AMin, AMax, APosition, APageSize: Integer; ARedraw: Boolean = True);

    //mouse
    function MouseDown(AHitTest: TcxCustomGridHitTest; AButton: TMouseButton;
      AShift: TShiftState): Boolean; override;
    procedure MouseLeave; override;
    function MouseMove(AHitTest: TcxCustomGridHitTest; AShift: TShiftState): Boolean; override;
    function MouseUp(AHitTest: TcxCustomGridHitTest; AButton: TMouseButton;
      AShift: TShiftState): Boolean; override;

    property InplaceEditFormAreaViewInfo: TcxGridInplaceEditFormAreaViewInfo read FViewInfo;
    property Kind: TScrollBarKind read GetKind write SetKind;
    property Max: Integer read GetMax write SetMax;
    property Min: Integer read GetMin write SetMin;
    property PageSize: Integer read GetPageSize write SetPageSize;
    property Position: Integer read GetPosition write SetPosition;
  end;

  TcxGridInplaceEditFormAreaScrollBarViewInfoClass = class of TcxGridInplaceEditFormAreaScrollBarViewInfo;

  { TcxGridInplaceEditFormAreaScrollBars }

  TcxGridInplaceEditFormAreaScrollBars = class
  private
    FViewInfo: TcxGridInplaceEditFormAreaViewInfo;
    //scrollbars
    FHorizontalScrollBar: TcxGridInplaceEditFormAreaScrollBarViewInfo;
    FVerticalScrollBar: TcxGridInplaceEditFormAreaScrollBarViewInfo;
    procedure CreateScrollBars;
    procedure DestroyScrollBars;
    function GetContainerViewInfo: TcxGridInplaceEditFormContainerViewInfo;
  protected
    procedure CalculateHorizontalScrollBar; virtual;
    procedure CalculateScrollBarViewInfo(AScrollBarViewInfo: TcxGridInplaceEditFormAreaScrollBarViewInfo;
      ASize, ALeft, ATop: Integer);
    procedure CalculateVerticalScrollBar; virtual;
    procedure CheckHorizontalScrollPosition; virtual;
    procedure CheckPosition; virtual;
    procedure CheckVerticalScrollPosition; virtual;
    function GetContainerOffsetFromScrollPosition(AScrollBarViewInfo: TcxGridInplaceEditFormAreaScrollBarViewInfo;
      AKind: TScrollBarKind): Integer;
    function GetHorizontalScrollBarHeight: Integer; virtual;
    function GetVerticalScrollBarWidth: Integer; virtual;
    function GetScrollBarClass: TcxGridInplaceEditFormAreaScrollBarViewInfoClass; virtual;
    function HasHorizontalScrollBar: Boolean;
    function HasVerticalScrollBar: Boolean;
  public
    constructor Create(AInplaceEditFormAreaViewInfo: TcxGridInplaceEditFormAreaViewInfo); virtual;
    destructor Destroy; override;

    procedure Calculate; virtual;
    function GetContainerOffset: TPoint; virtual;
    function GetHitTest(const P: TPoint): TcxCustomGridHitTest; virtual;
    procedure Paint(ACanvas: TcxCanvas); virtual;

    property HorizontalScrollBar: TcxGridInplaceEditFormAreaScrollBarViewInfo read FHorizontalScrollBar;
    property VerticalScrollBar: TcxGridInplaceEditFormAreaScrollBarViewInfo read FVerticalScrollBar;

    property ContainerViewInfo: TcxGridInplaceEditFormContainerViewInfo read GetContainerViewInfo;
    property ViewInfo: TcxGridInplaceEditFormAreaViewInfo read FViewInfo;
  end;

  TcxGridInplaceEditFormAreaScrollBarsClass = class of TcxGridInplaceEditFormAreaScrollBars;

  { TcxGridInplaceEditFormButtonViewInfo }

  TcxGridInplaceEditFormButtonViewInfo = class(TcxGridPartCustomCellViewInfo)
  private
    FViewInfo: TcxGridInplaceEditFormButtonsPanelViewInfo;

    function GetGridView: TcxGridTableView;
    function GetInplaceEditFormAreaViewInfo: TcxGridInplaceEditFormAreaViewInfo;
  protected
    function DoCalculateHeight: Integer; override;
    function DoCalculateWidth: Integer; override;
    function GetHitTestClass: TcxCustomGridHitTestClass; override;
    function GetPainterClass: TcxCustomGridCellPainterClass; override;
    procedure GetViewParams(var AParams: TcxViewParams); override;
  public
    constructor Create(AButtonsPanelViewInfo: TcxGridInplaceEditFormButtonsPanelViewInfo); reintroduce; virtual;

    property ButtonsPanelViewInfo: TcxGridInplaceEditFormButtonsPanelViewInfo read FViewInfo;
    property GridView: TcxGridTableView read GetGridView;
    property ViewInfo: TcxGridInplaceEditFormAreaViewInfo read GetInplaceEditFormAreaViewInfo;
  end;

  TcxGridInplaceEditFormButtonViewInfoClass = class of TcxGridInplaceEditFormButtonViewInfo;

  { TcxGridInplaceEditFormCancelButtonViewInfo }

  TcxGridInplaceEditFormCancelButtonViewInfo = class(TcxGridInplaceEditFormButtonViewInfo)
  protected
    procedure Click; override;
    function GetHitTestClass: TcxCustomGridHitTestClass; override;
    function GetText: string; override;
    function GetFocused: Boolean; override;
  end;

  { TcxGridInplaceEditFormUpdateButtonViewInfo }

  TcxGridInplaceEditFormUpdateButtonViewInfo = class(TcxGridInplaceEditFormButtonViewInfo)
  protected
    procedure Click; override;
    function GetEnabled: Boolean; override;
    function GetHitTestClass: TcxCustomGridHitTestClass; override;
    function GetHotTrack: Boolean; override;
    function GetText: string; override;
    function GetFocused: Boolean; override;
  end;

  { TcxGridInplaceEditFormButtonsPanelViewInfo }

  TcxGridInplaceEditFormButtonsPanelViewInfo = class(TcxCustomGridViewCellViewInfo)
  private
    FItems: TdxFastObjectList;
    FViewInfo: TcxGridInplaceEditFormAreaViewInfo;
    function GetButtonCount: Integer; {$IFDEF DELPHI9} inline; {$ENDIF}
    function GetGridView: TcxGridTableView;
    function GetForm: TcxGridTableViewInplaceEditForm;
    function GetItem(Index: Integer): TcxGridInplaceEditFormButtonViewInfo;
    function GetMaxButtonWidth: Integer;
    procedure SetButtonsWidth(AValue: Integer);
  protected
    procedure CreateButtons; virtual;
    procedure DestroyButtons; virtual;

    function AddItem(AItemClass: TcxGridInplaceEditFormButtonViewInfoClass): TcxGridInplaceEditFormButtonViewInfo;
    function GetButtonRightIndent: Integer;
    procedure CalculateButtons; virtual;
    function CalculateHeight: Integer; override;
    function CalculateWidth: Integer; override;
    function GetHeight: Integer; override;
    function GetHitTestClass: TcxCustomGridHitTestClass; override;
    function GetPainterClass: TcxCustomGridCellPainterClass; override;
    function GetSeparatorBound: TRect; virtual;
    function GetSeparatorHeight: Integer;
    procedure GetViewParams(var AParams: TcxViewParams); override;
    function GetWidth: Integer; override;

    property ButtonCount: Integer read GetButtonCount;
    property Form: TcxGridTableViewInplaceEditForm read GetForm;
    property Items[Index: Integer]: TcxGridInplaceEditFormButtonViewInfo read GetItem; default;
  public
    constructor Create(AInplaceEditFormAreaViewInfo: TcxGridInplaceEditFormAreaViewInfo); reintroduce; virtual;
    destructor Destroy; override;

    procedure Calculate(ALeftBound, ATopBound: Integer; AWidth: Integer = -1;
      AHeight: Integer = -1); override;
    function GetHitTest(const P: TPoint): TcxCustomGridHitTest; override;

    property GridView: TcxGridTableView read GetGridView;
    property ViewInfo: TcxGridInplaceEditFormAreaViewInfo read FViewInfo;
  end;

  TcxGridInplaceEditFormButtonsPanelViewInfoClass = class of TcxGridInplaceEditFormButtonsPanelViewInfo;

  TcxGridInplaceEditFormAreaViewInfoCacheInfo = class
  private
    FHeight: Integer;
    FIsHeightAssigned: Boolean;
    FIsRestHeightAssigned: Boolean;
    FRestHeight: Integer;
    procedure SetHeight(const Value: Integer);
    procedure SetRestHeight(const Value: Integer);
  public
    property Height: Integer read FHeight write SetHeight;
    property IsHeightAssigned: Boolean read FIsHeightAssigned write FIsHeightAssigned;
    property IsRestHeightAssigned: Boolean read FIsRestHeightAssigned write FIsRestHeightAssigned;
    property RestHeight: Integer read FRestHeight write SetRestHeight;
  end;

  { TcxGridInplaceEditFormAreaViewInfo }

  TcxGridInplaceEditFormAreaViewInfo = class(TcxCustomGridViewCellViewInfo, IcxScrollBarOwner)
  private
    FButtonsPanelViewInfo: TcxGridInplaceEditFormButtonsPanelViewInfo;
    FCacheInfo: TcxGridInplaceEditFormAreaViewInfoCacheInfo;
    FGridItemViewInfos: TcxGridInplaceEditFormContainerGridItemViewInfos;
    FIsNeedHorizontaScrollBar: Boolean;
    FIsNeedVerticalScrollBar: Boolean;
    FRecordViewInfo: TcxGridDataRowViewInfo;
    FScrollBars: TcxGridInplaceEditFormAreaScrollBars;
    procedure CreateButtonsPanel;
    procedure CreateGridItemViewInfos;
    procedure CreateScrollBars;
    procedure DestroyButtonsPanel;
    procedure DestroyGridItemViewInfos;
    procedure DestroyScrollBars;
    function GetContainerViewInfo: TcxGridInplaceEditFormContainerViewInfo;
    function GetGridRecord: TcxGridDataRow;
    function GetGridView: TcxGridTableView;
    function GetGridViewInfo: TcxGridTableViewInfo;
    function GetInplaceEditForm: TcxGridTableViewInplaceEditForm;
  protected
    //IcxScrollBarOwner
    function IcxScrollBarOwner.GetControl = GetScrollBarOwner;
    function GetScrollBarOwner: TWinControl;
    function GetLookAndFeel: TcxLookAndFeel;

    function CalculateAvailableHeight: Integer; virtual;
    function CalculateHeight: Integer; override;
    procedure CalculateLayoutContainerViewInfo; virtual;
    function CalculateWidth: Integer; override;
    procedure CheckFocusedItem;
    function FocusedItemKind: TcxGridFocusedItemKind;
    function GetAvailableHeight: Integer; virtual;
    function GetBoundsForGridItem(AGridItem: TcxCustomGridTableItem): TRect; virtual;
    function GetDataHeight: Integer; virtual;
    function GetDataWidth: Integer; virtual;
    function GetGridItemHitTest(const P: TPoint): TcxCustomGridHitTest; virtual;
    function GetHeight: Integer; override;
    function GetInplaceEditFormClientBounds: TRect; virtual;
    procedure GetViewParams(var AParams: TcxViewParams); override;
    function GetWidth: Integer; override;
    function IsPointInLayoutContainer(const P: TPoint): Boolean;
    procedure Offset(DX, DY: Integer); override;
    procedure UpdateOnScroll;

    function GetGridItemViewInfoClass: TcxGridTableDataCellViewInfoClass; virtual;
    function GetHitTestClass: TcxCustomGridHitTestClass; override;
    function GetButtonsPanelViewInfoClass: TcxGridInplaceEditFormButtonsPanelViewInfoClass; virtual;
    function GetScrollBarsClass: TcxGridInplaceEditFormAreaScrollBarsClass; virtual;
    function GetPainterClass: TcxCustomGridCellPainterClass; override;

    property AvailableHeight: Integer read GetAvailableHeight;
    property CacheInfo: TcxGridInplaceEditFormAreaViewInfoCacheInfo read FCacheInfo;
    property DataHeight: Integer read GetDataHeight;
    property DataWidth: Integer read GetDataWidth;
    property GridItemViewInfos: TcxGridInplaceEditFormContainerGridItemViewInfos read FGridItemViewInfos;
    property GridRecord: TcxGridDataRow read GetGridRecord;
    property InplaceEditForm: TcxGridTableViewInplaceEditForm read GetInplaceEditForm;
    property IsNeedHorizontalScrollBar: Boolean read FIsNeedHorizontaScrollBar write FIsNeedHorizontaScrollBar;
    property IsNeedVerticalScrollBar: Boolean read FIsNeedVerticalScrollBar write FIsNeedVerticalScrollBar;
  public
    constructor Create(AGridViewInfo: TcxCustomGridViewInfo; ARecordViewInfo: TcxGridDataRowViewInfo); reintroduce; virtual;
    destructor Destroy; override;

    procedure BeforeRecalculation; override;
    procedure Calculate(ALeftBound, ATopBound: Integer; AWidth: Integer = -1;
      AHeight: Integer = -1); override;
    function FindCellViewInfo(AGridItem: TcxCustomGridTableItem): TcxGridTableDataCellViewInfo;
    function GetBoundsForEdit(const ADataCellBounds: TRect): TRect; virtual;
    function GetHitTest(const P: TPoint): TcxCustomGridHitTest; override;
    procedure InvalidateUpdateButtonInfo;
    procedure MakeDataCellVisible(ACellViewInfo: TcxGridInplaceEditFormDataCellViewInfo); virtual;
    procedure MakeItemVisible(AItem: TcxCustomGridTableItem);

    //mouse
    function MouseDown(AHitTest: TcxCustomGridHitTest; AButton: TMouseButton;
      AShift: TShiftState): Boolean; override;
    function MouseMove(AHitTest: TcxCustomGridHitTest; AShift: TShiftState): Boolean; override;
    function MouseUp(AHitTest: TcxCustomGridHitTest; AButton: TMouseButton;
      AShift: TShiftState): Boolean; override;

    property ButtonsPanelViewInfo: TcxGridInplaceEditFormButtonsPanelViewInfo read FButtonsPanelViewInfo;
    property GridView: TcxGridTableView read GetGridView;
    property GridViewInfo: TcxGridTableViewInfo read GetGridViewInfo;
    property ContainerViewInfo: TcxGridInplaceEditFormContainerViewInfo read GetContainerViewInfo;
    property RecordViewInfo: TcxGridDataRowViewInfo read FRecordViewInfo;
    property ScrollBars: TcxGridInplaceEditFormAreaScrollBars read FScrollBars;
  end;

  TcxGridInplaceEditFormAreaViewInfoClass = class of TcxGridInplaceEditFormAreaViewInfo;

  { TcxGridDataRowViewInfo }

  TcxGridDataRowViewInfo = class(TcxCustomGridRowViewInfo)
  private
    FCellHeight: Integer;
    FCellsAreaBounds: TRect;
    FCellsAreaViewInfo: TcxGridDataRowCellsAreaViewInfo;
    FCellViewInfos: TList;
    FEmptyAreaBounds: TRect;
    FInplaceEditFormAreaViewInfo: TcxGridInplaceEditFormAreaViewInfo;
    FPreviewViewInfo: TcxGridPreviewCellViewInfo;

    function FindCellViewInfoInTableRow(AItem: TcxCustomGridTableItem): TcxGridTableDataCellViewInfo;
    function FindCellViewInfoOnInplaceEditForm(AItem: TcxCustomGridTableItem): TcxGridTableDataCellViewInfo;
    function GetCellViewInfo(Index: Integer): TcxGridDataCellViewInfo;
    function GetCellViewInfoCount: Integer;
    function GetGridRecord: TcxGridDataRow;
    function GetHasInplaceEditFormArea: Boolean;
    function GetHasPreview: Boolean;
    function GetInplaceEditForm: TcxGridTableViewInplaceEditForm;
    function GetInternalCellViewInfo(Index: Integer): TcxGridDataCellViewInfo;

    procedure CreateViewInfos;
    procedure DestroyViewInfos;
  protected
    procedure CalculateExpandButtonBounds(var ABounds: TRect); override;

    function AdjustToIntegralBottomBound(var ABound: Integer): Boolean; override; 
    procedure AfterRowsViewInfoCalculate; override;
    procedure AfterRowsViewInfoOffset; override;
    procedure ApplyMergedCellsBounds(var R: TRect; AItem: TcxCustomGridTableItem);
    procedure ApplyMergingCellsBounds(var R: TRect);
    procedure CalculateCellViewInfo(AIndex: Integer); virtual;
    function CalculateDataAutoHeight(ACheckEditingCell: Boolean = True): Integer;
    function CalculateHeight: Integer; override;
    procedure CalculateInplaceEditForm; virtual;
    procedure CalculateTableRowEmptyAreaBounds;
    function CalculateMultilineEditMinHeight: Integer; override;
    function CanDelayedDrawDataCellBorders: Boolean; virtual;
    function CanSize: Boolean; override;
    procedure CheckRowHeight(var AValue: Integer); override;
    procedure DoToggleExpanded; override;
    function FindCellViewInfo(AItem: TcxCustomGridTableItem): TcxGridTableDataCellViewInfo; virtual;
    function GetAutoHeight: Boolean; override;
    function GetBackgroundBitmapBounds: TRect; override;
    function GetBaseHeight: Integer; override;
    function GetInplaceEditFormLeftPosition: Integer; virtual;
    function GetInplaceEditFormTopPosition: Integer; virtual;
    function GetPainterClass: TcxCustomGridCellPainterClass; override;
    procedure GetViewParams(var AParams: TcxViewParams); override;
    function IsNeedHideTableRowCells: Boolean; virtual;
    function NeedToggleExpandRecord(AHitTest: TcxCustomGridHitTest; AButton: TMouseButton;
      AShift: TShiftState): Boolean; override;
    procedure Offset(DX, DY: Integer); override;
    procedure SetRowHeight(Value: Integer); override;

    procedure CancelInplaceEditFormEditing; virtual;
    procedure UpdateInplaceEditFormEditing; virtual;

    function GetCellHeight(AIndex: Integer): Integer; reintroduce; virtual;
    function GetCellHeightValue: Integer; virtual;
    function GetCellLeftBound(AIndex: Integer): Integer; virtual;
    function GetCellTopBound(AIndex: Integer): Integer; virtual;
    function GetCellsAreaBounds: TRect; virtual;
    function GetCellsAreaViewInfoClass: TcxGridDataRowCellsAreaViewInfoClass; virtual;
    function GetCellViewInfoClass(AIndex: Integer): TcxGridDataCellViewInfoClass; virtual;
    function GetCellWidth(AIndex: Integer): Integer; virtual;
    function GetDataWidth: Integer; override;
    function GetInplaceEditFormAreaViewInfoClass: TcxGridInplaceEditFormAreaViewInfoClass; virtual;
    function GetPreviewViewInfoClass: TcxGridPreviewCellViewInfoClass; virtual;
    function GetShowInplaceEditFormContainer: Boolean; virtual;
    function GetShowPreview: Boolean; virtual;
    function GetTableRowEmptyAreaLeft: Integer; virtual;
    function GetWidth: Integer; override;
    function IsCellVisible(AIndex: Integer): Boolean; virtual;
    function HasFocusRect: Boolean; override;

    property CellHeight: Integer read GetCellHeightValue;
    property EmptyAreaBounds: TRect read FEmptyAreaBounds;
    property InplaceEditForm: TcxGridTableViewInplaceEditForm read GetInplaceEditForm;
    property ShowInplaceEditFormArea: Boolean read GetShowInplaceEditFormContainer;
    property ShowPreview: Boolean read GetShowPreview;
  public
    constructor Create(ARecordsViewInfo: TcxCustomGridRecordsViewInfo;
      ARecord: TcxCustomGridRecord); override;
    destructor Destroy; override;
    procedure BeforeCellRecalculation(ACell: TcxGridTableCellViewInfo); virtual;
    procedure BeforeRecalculation; override;
    procedure Calculate(ALeftBound, ATopBound: Integer; AWidth: Integer = -1;
      AHeight: Integer = -1); override;
    function GetAreaBoundsForPainting: TRect; override;
    function GetBoundsForInvalidate(AItem: TcxCustomGridTableItem): TRect; override;
    function GetBoundsForItem(AItem: TcxCustomGridTableItem): TRect; override;
    function GetCellBorders(AIsRight, AIsBottom: Boolean): TcxBorders; virtual;
    function GetCellViewInfoByItem(AItem: TcxCustomGridTableItem): TcxGridTableDataCellViewInfo; override;
    function GetHitTest(const P: TPoint): TcxCustomGridHitTest; override;
    function GetInplaceEditFormClientBounds: TRect; virtual;

    property CellsAreaBounds: TRect read GetCellsAreaBounds;
    property CellsAreaViewInfo: TcxGridDataRowCellsAreaViewInfo read FCellsAreaViewInfo;
    property CellViewInfoCount: Integer read GetCellViewInfoCount;
    property CellViewInfos[Index: Integer]: TcxGridDataCellViewInfo read GetCellViewInfo;
    property GridRecord: TcxGridDataRow read GetGridRecord;
    property HasInplaceEditFormArea: Boolean read GetHasInplaceEditFormArea;
    property HasPreview: Boolean read GetHasPreview;
    property InternalCellViewInfos[Index: Integer]: TcxGridDataCellViewInfo read GetInternalCellViewInfo;
    property InplaceEditFormAreaViewInfo: TcxGridInplaceEditFormAreaViewInfo read FInplaceEditFormAreaViewInfo;
    property PreviewViewInfo: TcxGridPreviewCellViewInfo read FPreviewViewInfo;
  end;

  // new item row

  { TcxGridNewItemRowViewInfo }

  TcxGridNewItemRowViewInfo = class(TcxGridDataRowViewInfo)
  private
    FHeight: Integer;
  protected
    function CalculateSelected: Boolean; override;
    function CanDelayedDrawDataCellBorders: Boolean; override;
    function CanShowDataCellHint: Boolean; override;
    function GetAlignmentHorz: TAlignment; override;
    function GetAlignmentVert: TcxAlignmentVert; override;
    function GetAutoHeight: Boolean; override;
    function GetCellLeftBound(AIndex: Integer): Integer; override;
    function GetCellWidth(AIndex: Integer): Integer; override;
    function GetHeight: Integer; override;
    function GetInfoText: string; virtual;
    function GetOptions: TcxGridSpecialRowOptions; virtual;
    function GetSeparatorColor: TColor; override;
    function GetSeparatorWidth: Integer; override;
    function GetShowInfoText: Boolean; virtual;
    function GetShowPreview: Boolean; override;
    function GetStyleIndex: Integer; virtual;
    function GetText: string; override;
    function GetTextAreaBounds: TRect; override;
    procedure GetViewParams(var AParams: TcxViewParams); override;
    function HasFooters: Boolean; override;
    function HasLastHorzGridLine: Boolean; override;

    function GetPainterClass: TcxCustomGridCellPainterClass; override;

    property InfoText: string read GetInfoText;
    property Options: TcxGridSpecialRowOptions read GetOptions;
    property ShowInfoText: Boolean read GetShowInfoText;
  end;

  // filtering row

  { TcxGridFilterRowViewInfo }

  TcxGridFilterRowViewInfo = class(TcxGridNewItemRowViewInfo)
  private
    function GetGridRecord: TcxGridFilterRow;
  protected
    function GetOptions: TcxGridSpecialRowOptions; override;
    function GetShowInfoText: Boolean; override;
    function GetShowInplaceEditFormContainer: Boolean; override;
    function GetStyleIndex: Integer; override;
  public
    property GridRecord: TcxGridFilterRow read GetGridRecord;
  end;

  // details site

  { TcxGridMasterDataRowDetailsSiteLeftTabsViewInfo }

  TcxGridMasterDataRowDetailsSiteLeftTabsViewInfo = class(TcxGridDetailsSiteLeftTabsViewInfo)
  private
    function GetBottomGridLineColor: TColor;
    function GetBottomGridLineWidth: Integer;
    function GetSiteViewInfo: TcxGridDetailsSiteViewInfo;
  protected
    function CalculateHeight: Integer; override;
    function GetBoundsRect: TRect; override;
    function GetPainterClass: TcxCustomGridCellPainterClass; override;
    function GetBottomGridLineBounds: TRect; virtual;
    property BottomGridLineWidth: Integer read GetBottomGridLineWidth;
  public
    function HasBottomGridLine: Boolean; virtual;
    property BottomGridLineBounds: TRect read GetBottomGridLineBounds;
    property BottomGridLineColor: TColor read GetBottomGridLineColor;
    property SiteViewInfo: TcxGridDetailsSiteViewInfo read GetSiteViewInfo;
  end;

  TcxGridDetailsSiteViewInfoClass = class of TcxGridDetailsSiteViewInfo;

  { TcxGridDetailsSiteViewInfo }

  TcxGridDetailsSiteViewInfo = class(TcxCustomGridDetailsSiteViewInfo)
  private
    FMasterDataRowViewInfo: TcxGridMasterDataRowViewInfo;
    function GetCacheItem: TcxGridMasterTableViewInfoCacheItem;
    function GetMasterGridView: TcxGridTableView;
    function GetMasterGridViewInfo: TcxGridTableViewInfo;
  protected
    procedure ControlFocusChanged; virtual;
    function GetActiveGridView: TcxCustomGridView; override;
    function GetActiveGridViewExists: Boolean; override;
    function GetActiveGridViewValue: TcxCustomGridView; override;
    function GetActiveLevel: TcxGridLevel; override;
    function GetCanvas: TcxCanvas; override;
    function GetContainer: TcxControl; override;
    function GetDesignController: TcxCustomGridDesignController; override;
    function GetFullyVisible: Boolean; override;
    function GetHeight: Integer; override;
    function GetMasterRecord: TObject; override;
    function GetMaxHeight: Integer; override;
    function GetMaxNormalHeight: Integer; override;
    function GetMaxWidth: Integer; override;
    function GetNormalHeight: Integer; override;
    function GetTabsViewInfoClass: TcxCustomGridDetailsSiteTabsViewInfoClass; override;
    function GetVisible: Boolean; override;
    function GetWidth: Integer; override;
    procedure InitTabHitTest(AHitTest: TcxGridDetailsSiteTabHitTest); override;

    property CacheItem: TcxGridMasterTableViewInfoCacheItem read GetCacheItem;
    property MasterGridView: TcxGridTableView read GetMasterGridView;
    property MasterGridViewInfo: TcxGridTableViewInfo read GetMasterGridViewInfo;
    property MasterDataRowViewInfo: TcxGridMasterDataRowViewInfo read FMasterDataRowViewInfo;
  public
    constructor Create(AMasterDataRowViewInfo: TcxGridMasterDataRowViewInfo); reintroduce; virtual;
    destructor Destroy; override;
    procedure ChangeActiveTab(ALevel: TcxGridLevel; AFocusView: Boolean = False); override;
    function DetailHasData(ALevel: TcxGridLevel): Boolean; override;
    function HasMaxHeight: Boolean;
    function SupportsTabAccelerators: Boolean; override;
  end;

  // master data row

  { TcxGridExpandButtonCellViewInfo }

  TcxGridExpandButtonCellViewInfoClass = class of TcxGridExpandButtonCellViewInfo;

  TcxGridExpandButtonCellViewInfo = class(TcxGridCellViewInfo)
  private
    function GetRecordViewInfo: TcxGridMasterDataRowViewInfo;
    function GetRightBorderRestSpaceBounds: TRect;
  protected
    function CalculateHeight: Integer; override;
    function CalculateWidth: Integer; override;
    function GetBackgroundBitmap: TBitmap; override;
    function GetBaseWidth: Integer; virtual;
    function GetBorderBounds(AIndex: TcxBorder): TRect; override;
    function GetBorders: TcxBorders; override;
    function GetPainterClass: TcxCustomGridCellPainterClass; override;

    property BaseWidth: Integer read GetBaseWidth;
    property RightBorderRestSpaceBounds: TRect read GetRightBorderRestSpaceBounds;
  public
    property RecordViewInfo: TcxGridMasterDataRowViewInfo read GetRecordViewInfo;
  end;

  { TcxGridDetailsAreaViewInfo }

  TcxGridDetailsAreaViewInfoClass = class of TcxGridDetailsAreaViewInfo;

  TcxGridDetailsAreaViewInfo = class(TcxGridCellViewInfo)
  private
    function GetRecordViewInfo: TcxGridMasterDataRowViewInfo;
  protected
    function CalculateHeight: Integer; override;
    function CalculateWidth: Integer; override;
    function GetBorders: TcxBorders; override;
  public
    property RecordViewInfo: TcxGridMasterDataRowViewInfo read GetRecordViewInfo;
  end;

  { TcxGridMasterDataRowViewInfo }

  TcxGridMasterDataRowViewInfo = class(TcxGridDataRowViewInfo)
  private
    FDetailsAreaViewInfo: TcxGridDetailsAreaViewInfo;
    FDetailsSiteViewInfo: TcxGridDetailsSiteViewInfo;
    FExpandButtonCellViewInfo: TcxGridExpandButtonCellViewInfo;
    FRestHeight: Integer;
    function GetCacheItem: TcxGridMasterTableViewInfoCacheItem;
    function GetDetailsSiteIndentBounds: TRect;
    function GetGridRecord: TcxGridMasterDataRow;
  protected
    procedure CalculateExpandButtonBounds(var ABounds: TRect); override;
    function CalculateHeight: Integer; override;
    function CalculateRestHeight(ARowHeight: Integer): Integer; virtual;
    procedure ControlFocusChanged; override;
    function GetDataHeight: Integer; override;
    function GetDataIndent: Integer; override;
    function GetDataWidth: Integer; override;
    function GetDetailsSiteVisible: Boolean; virtual;
    function GetExpandButtonAreaBounds: TRect; override;
    function GetMaxHeight: Integer; override;
    function GetPixelScrollSize: Integer; override;
    function GetTableRowEmptyAreaLeft: Integer; override;
    function IsDetailVisible(ADetail: TcxCustomGridView): Boolean; override;
    function IsFullyVisible: Boolean; override;
    procedure VisibilityChanged(AVisible: Boolean); override;

    function GetPainterClass: TcxCustomGridCellPainterClass; override;

    function GetDetailsAreaViewInfoClass: TcxGridDetailsAreaViewInfoClass; virtual;
    function GetDetailsSiteViewInfoClass: TcxGridDetailsSiteViewInfoClass; virtual;
    function GetExpandButtonCellViewInfoClass: TcxGridExpandButtonCellViewInfoClass; virtual;

    property CacheItem: TcxGridMasterTableViewInfoCacheItem read GetCacheItem;
    property DetailsAreaViewInfo: TcxGridDetailsAreaViewInfo read FDetailsAreaViewInfo;
    property DetailsSiteIndentBounds: TRect read GetDetailsSiteIndentBounds;
    property ExpandButtonCellViewInfo: TcxGridExpandButtonCellViewInfo read FExpandButtonCellViewInfo;
    property RestHeight: Integer read FRestHeight write FRestHeight;
  public
    constructor Create(ARecordsViewInfo: TcxCustomGridRecordsViewInfo;
      ARecord: TcxCustomGridRecord); override;
    destructor Destroy; override;
    procedure BeforeRecalculation; override;
    procedure Calculate(ALeftBound, ATopBound: Integer; AWidth: Integer = -1;
      AHeight: Integer = -1); override;
    function GetHitTest(const P: TPoint): TcxCustomGridHitTest; override;
    function ProcessDialogChar(ACharCode: Word): Boolean; override;
    function SupportsTabAccelerators: Boolean; virtual;
    property DetailsSiteViewInfo: TcxGridDetailsSiteViewInfo read FDetailsSiteViewInfo;
    property DetailsSiteVisible: Boolean read GetDetailsSiteVisible;
    property GridRecord: TcxGridMasterDataRow read GetGridRecord;
  end;

  // group row

  { TcxGridGroupCellViewInfo }

  TcxGridGroupCellViewInfo = class(TcxGridCellViewInfo)
  private
    function GetExpandedAreaBounds: TRect;
    function GetGridRecord: TcxGridGroupRow;
    function GetRecordViewInfo: TcxGridGroupRowViewInfo;
    function GetRowStyle: TcxGridGroupRowStyle;
  protected
    function CalculateHeight: Integer; override;
    function CalculateWidth: Integer; override;
    function CustomDraw(ACanvas: TcxCanvas): Boolean; override;
    function GetAlignmentVert: TcxAlignmentVert; override;
    function GetAlwaysSelected: Boolean; override;
    function GetBackgroundBitmap: TBitmap; override;
    function GetBorderBounds(AIndex: TcxBorder): TRect; override;
    function GetBorderColor(AIndex: TcxBorder): TColor; override;
    function GetBorders: TcxBorders; override;
    function GetBorderWidth(AIndex: TcxBorder): Integer; override;
    function GetPainterClass: TcxCustomGridCellPainterClass; override;
    function GetText: string; override;
    function GetTextAreaBounds: TRect; override;
    procedure GetViewParams(var AParams: TcxViewParams); override;
    function HasCustomDraw: Boolean; override;

    property ExpandedAreaBounds: TRect read GetExpandedAreaBounds;
    property RowStyle: TcxGridGroupRowStyle read GetRowStyle;
  public
    function CanDrawSelected: Boolean; override;
    property GridRecord: TcxGridGroupRow read GetGridRecord;
    property RecordViewInfo: TcxGridGroupRowViewInfo read GetRecordViewInfo;
  end;

  { TcxGridGroupRowSpacerViewInfo }

  TcxGridGroupRowSpacerViewInfoClass = class of TcxGridGroupRowSpacerViewInfo;

  TcxGridGroupRowSpacerViewInfo = class(TcxCustomGridViewCellViewInfo)
  private
    FRowViewInfo: TcxGridGroupRowViewInfo;
    function GetGridView: TcxGridTableView;
  protected
    function CalculateWidth: Integer; override;
    function GetAlignmentVert: TcxAlignmentVert; override;
    function GetText: string; override;
    function GetTextAreaBounds: TRect; override;
    procedure GetViewParams(var AParams: TcxViewParams); override;
    function GetWidth: Integer; override;
    function HasBackground: Boolean; override;
  public
    constructor Create(ARowViewInfo: TcxGridGroupRowViewInfo; const AText: string); reintroduce; virtual;
    property GridView: TcxGridTableView read GetGridView;
    property RowViewInfo: TcxGridGroupRowViewInfo read FRowViewInfo;
  end;

  { TcxGridGroupSummaryCellViewInfo }

  TcxGridGroupSummaryCellViewInfoClass = class of TcxGridGroupSummaryCellViewInfo;

  TcxGridGroupSummaryCellViewInfo = class(TcxCustomGridViewCellViewInfo)
  private
    FIsLast: Boolean;
    FRowViewInfo: TcxGridGroupRowViewInfo;
    FSeparatorViewInfo: TcxGridGroupRowSpacerViewInfo;
    FSummaryItem: TcxDataSummaryItem;
    FTextWidth: Integer;
    FValue: Variant;
    function GetColumn: TcxGridColumn;
    function GetGridView: TcxGridTableView;
    procedure SetIsLast(Value: Boolean);
  protected
    function CalculateWidth: Integer; override;
    function CanAlignWithColumn: Boolean;
    function CustomDraw(ACanvas: TcxCanvas): Boolean; override;
    function GetAlignmentHorz: TAlignment; override;
    function GetAlignmentVert: TcxAlignmentVert; override;
    function GetDesignSelectionBounds: TRect; override;
    function GetHitTestClass: TcxCustomGridHitTestClass; override;
    function GetIsDesignSelected: Boolean; override;
    function GetPainterClass: TcxCustomGridCellPainterClass; override;
    function GetSeparatorBounds: TRect; virtual;
    function GetSeparatorText: string; virtual;
    function GetShowEndEllipsis: Boolean; override;
    function GetText: string; override;
    function GetTextAreaBounds: TRect; override;
    procedure GetViewParams(var AParams: TcxViewParams); override;
    function HasBackground: Boolean; override;
    function HasCustomDraw: Boolean; override;
    procedure InitHitTest(AHitTest: TcxCustomGridHitTest); override;
    procedure Offset(DX, DY: Integer); override;
  public
    constructor Create(ARowViewInfo: TcxGridGroupRowViewInfo;
      ASummaryItem: TcxDataSummaryItem; const AValue: Variant); reintroduce; virtual;
    destructor Destroy; override;
    procedure BeforeRecalculation; override;
    procedure Calculate(ALeftBound, ATopBound: Integer; AWidth: Integer = -1;
      AHeight: Integer = -1); override;
    function MouseDown(AHitTest: TcxCustomGridHitTest; AButton: TMouseButton;
      AShift: TShiftState): Boolean; override;
    property Column: TcxGridColumn read GetColumn;
    property GridView: TcxGridTableView read GetGridView;
    property IsLast: Boolean read FIsLast write SetIsLast;
    property RowViewInfo: TcxGridGroupRowViewInfo read FRowViewInfo;
    property SeparatorViewInfo: TcxGridGroupRowSpacerViewInfo read FSeparatorViewInfo;
    property SummaryItem: TcxDataSummaryItem read FSummaryItem;
    property Value: Variant read FValue;
  end;

  { TcxGridGroupRowViewInfo }

  TcxGridGroupRowViewInfo = class(TcxCustomGridRowViewInfo)
  private
    FCellViewInfo: TcxGridGroupCellViewInfo;
    FSummaryBeginningSpacerViewInfo: TcxGridGroupRowSpacerViewInfo;
    FSummaryCellsWithoutColumns: TList;
    FSummaryCellViewInfos: TList;
    FSummaryEndingSpacerViewInfo: TcxGridGroupRowSpacerViewInfo;
    function GetGridRecord: TcxGridGroupRow;
    function GetRowStyle: TcxGridGroupRowStyle;
    function GetSummaryCellLayout: TcxGridGroupSummaryLayout;
    function GetSummaryCellViewInfo(Index: Integer): TcxGridGroupSummaryCellViewInfo;
    function GetSummaryCellViewInfoCount: Integer;
  protected
    procedure CalculateExpandButtonBounds(var ABounds: TRect); override;
    function CalculateHeight: Integer; override;
    function CanSize: Boolean; override;
    procedure CheckRowHeight(var AValue: Integer); override;
    function GetAutoHeight: Boolean; override;
    function GetBackgroundBitmap: TBitmap; override;
    function GetExpandButtonAreaBounds: TRect; override;
    function GetFocusRectBounds: TRect; override;
    function GetPainterClass: TcxCustomGridCellPainterClass; override;
    function GetSeparatorBounds: TRect; override;
    function GetSeparatorIndentBounds: TRect; virtual;
    function GetShowSeparator: Boolean; override;
    function HasFocusRect: Boolean; override;
    function HasFooter(ALevel: Integer): Boolean; override;
    procedure Offset(DX, DY: Integer); override;
    procedure SetRowHeight(Value: Integer); override;

    function CreateSpacerViewInfo(const AText: string): TcxGridGroupRowSpacerViewInfo;
    procedure CreateSpacerViewInfos; virtual;
    procedure DestroySpacerViewInfos; virtual;
    function GetSpacerViewInfoClass: TcxGridGroupRowSpacerViewInfoClass; virtual;

    function AddSummaryCellViewInfo(ASummaryItem: TcxDataSummaryItem;
      const AValue: Variant): TcxGridGroupSummaryCellViewInfo;
    procedure CreateSummaryCellViewInfos; virtual;
    procedure DestroySummaryCellViewInfos; virtual;
    function GetSummaryCellViewInfoClass: TcxGridGroupSummaryCellViewInfoClass; virtual;

    procedure CalculateSummaryCells; overload; virtual;
    procedure CalculateSummaryCells(ACellViewInfos: TList;
      const AAreaBounds: TRect; AAlignment: TAlignment; AAutoWidth: Boolean); overload; virtual;
    function CalculateSummaryCellWidths(ACellViewInfos: TList; AAvailableWidth: Integer;
      AAutoWidth: Boolean): TcxAutoWidthObject; virtual;
    function GetColumnSummaryCellsAreaBounds(AColumnHeaderViewInfo: TcxGridColumnHeaderViewInfo): TRect; virtual;
    procedure GetColumnSummaryCellViewInfos(AColumn: TcxGridColumn; AList: TList);
    function GetSummaryBeginningSpacerBounds: TRect; virtual;
    function GetSummaryEndingSpacerBounds: TRect; virtual;
    function GetSummaryBeginningSpacerText: string; virtual;
    function GetSummaryEndingSpacerText: string; virtual;
    function GetSummaryCellAutoWidth: Boolean; virtual;
    function GetSummaryCellsAreaBounds(AForAlignableCells: Boolean): TRect; virtual;
    function GetSummaryCellsBounds(const AAreaBounds: TRect;
      AWidths: TcxAutoWidthObject; AAlignment: TAlignment; AAutoWidth: Boolean): TRect; virtual;
    function GetUnalignableSummaryCells: TList; virtual;
    function HasUnalignableSummaryCells: Boolean; virtual;

    property SummaryBeginningSpacerViewInfo: TcxGridGroupRowSpacerViewInfo read FSummaryBeginningSpacerViewInfo;
    property SummaryEndingSpacerViewInfo: TcxGridGroupRowSpacerViewInfo read FSummaryEndingSpacerViewInfo;
    property SummaryCellAutoWidth: Boolean read GetSummaryCellAutoWidth;
    property SummaryCellLayout: TcxGridGroupSummaryLayout read GetSummaryCellLayout;
    property SummaryCells: TList read FSummaryCellViewInfos;
    property SummaryCellsWithoutColumns: TList read FSummaryCellsWithoutColumns;
    property UnalignableSummaryCells: TList read GetUnalignableSummaryCells;

    property RowStyle: TcxGridGroupRowStyle read GetRowStyle;
  public
    constructor Create(ARecordsViewInfo: TcxCustomGridRecordsViewInfo;
      ARecord: TcxCustomGridRecord); override;
    destructor Destroy; override;
    procedure BeforeRecalculation; override;
    procedure Calculate(ALeftBound, ATopBound: Integer; AWidth: Integer = -1;
      AHeight: Integer = -1); override;
    function GetHitTest(const P: TPoint): TcxCustomGridHitTest; override;
    property CellViewInfo: TcxGridGroupCellViewInfo read FCellViewInfo;
    property GridRecord: TcxGridGroupRow read GetGridRecord;
    property SeparatorIndentBounds: TRect read GetSeparatorIndentBounds;
    property SummaryCellViewInfoCount: Integer read GetSummaryCellViewInfoCount;
    property SummaryCellViewInfos[Index: Integer]: TcxGridGroupSummaryCellViewInfo read GetSummaryCellViewInfo;
  end;

implementation

uses
  SysUtils, Math, RTLConsts, cxVariants, cxGrid, cxEdit, dxOffice11, cxGridStrs, cxGeometry,
  dxLayoutContainer, dxLayoutLookAndFeels;

{ TcxGridPreviewHitTest }

class function TcxGridPreviewHitTest.GetHitTestCode: Integer;
begin
  Result := htPreview;
end;

{ TcxGridInplaceEditFormAreaScrollBarHitTest }

class function TcxGridInplaceEditFormAreaScrollBarHitTest.GetHitTestCode: Integer;
begin
  Result := htInplaceEditFormAreaScrollBar;
end;

{ TcxGridInplaceEditFormButtonHitTest }

class function TcxGridInplaceEditFormButtonHitTest.GetHitTestCode: Integer;
begin
  Result := htInplaceEditFormButton;
end;

{ TcxGridInplaceEditFormCancelButtonHitTest }

class function TcxGridInplaceEditFormCancelButtonHitTest.GetHitTestCode: Integer;
begin
  Result := htInplaceEditFormCancelButton;
end;

{ TcxGridInplaceEditFormUpdateButtonHitTest }

class function TcxGridInplaceEditFormUpdateButtonHitTest.GetHitTestCode: Integer;
begin
  Result := htInplaceEditFormUpdateButton;
end;

{ TcxGridInplaceEditFormButtonsPanelHitTest }

class function TcxGridInplaceEditFormButtonsPanelHitTest.GetHitTestCode: Integer;
begin
  Result := htInplaceEditFormButtonsPanel
end;

{ TcxGridInplaceEditFormAreaHitTest }

class function TcxGridInplaceEditFormAreaHitTest.GetHitTestCode: Integer;
begin
  Result := htInplaceEditFormArea;
end;

{ TcxGridDataCellPainter }

function TcxGridDataCellPainter.GetViewInfo: TcxGridDataCellViewInfo;
begin
  Result := TcxGridDataCellViewInfo(inherited ViewInfo);
end;

function TcxGridDataCellPainter.CanDelayedDrawBorder: Boolean;
begin
  Result := not ExcludeFromClipRect and ViewInfo.RecordViewInfo.CanDelayedDrawDataCellBorders;
end;

procedure TcxGridDataCellPainter.DrawBorder(ABorder: TcxBorder);
begin
  if CanDelayedDrawBorder then
    TcxGridTablePainter(ViewInfo.GridViewInfo.Painter).AddGridLine(ViewInfo.BorderBounds[ABorder])
  else
    inherited DrawBorder(ABorder);
end;

function TcxGridDataCellPainter.ExcludeFromClipRect: Boolean;
begin
  Result := ViewInfo.RecordViewInfo.CellsAreaViewInfo.Visible;
end;

{ TcxGridInplaceEditFormButtonPainter }

function TcxGridInplaceEditFormButtonPainter.CanDrawFocusRect: Boolean;
begin
  Result := ViewInfo.Focused;
end;

procedure TcxGridInplaceEditFormButtonPainter.DrawButton;
begin
  Canvas.Font := ViewInfo.Params.Font;
  ViewInfo.LookAndFeelPainter.DrawButton(Canvas, ViewInfo.Bounds, ViewInfo.Text,
    ViewInfo.ButtonState);
end;

function TcxGridInplaceEditFormButtonPainter.ExcludeFromClipRect: Boolean;
begin
  Result := False;
end;

function TcxGridInplaceEditFormButtonPainter.GetGridViewInfo: TcxCustomGridViewInfo;
begin
  Result := ViewInfo.GridViewInfo;
end;

procedure TcxGridInplaceEditFormButtonPainter.Paint;
begin
  DrawButton;
  DrawFocusRect;
end;

function TcxGridInplaceEditFormButtonPainter.GetViewInfo: TcxGridInplaceEditFormButtonViewInfo;
begin
  Result := TcxGridInplaceEditFormButtonViewInfo(inherited ViewInfo);
end;

{ TcxGridInplaceEditFormButtonsPanelPainter }

procedure TcxGridInplaceEditFormButtonsPanelPainter.DrawBackground;
begin
//do nothing
end;

procedure TcxGridInplaceEditFormButtonsPanelPainter.DrawButtons;
var
  I: Integer;
begin
  for I := 0 to ViewInfo.ButtonCount - 1 do
    ViewInfo.Items[I].Paint(Canvas);
end;

procedure TcxGridInplaceEditFormButtonsPanelPainter.DrawContent;
begin
  inherited DrawContent;
  DrawSeparator;
  DrawButtons;
end;

procedure TcxGridInplaceEditFormButtonsPanelPainter.DrawSeparator;
begin
  ViewInfo.LookAndFeelPainter.DrawSeparator(Canvas, ViewInfo.GetSeparatorBound, False);
end;

function TcxGridInplaceEditFormButtonsPanelPainter.GetViewInfo: TcxGridInplaceEditFormButtonsPanelViewInfo;
begin
  Result := TcxGridInplaceEditFormButtonsPanelViewInfo(inherited ViewInfo);
end;

{ TcxGridInplaceEditFormAreaScrollBarPainter }

procedure TcxGridInplaceEditFormAreaScrollBarPainter.DrawContent;
begin
  inherited DrawContent;
  ViewInfo.ScrollBar.Paint(Canvas);
end;

function TcxGridInplaceEditFormAreaScrollBarPainter.GetViewInfo: TcxGridInplaceEditFormAreaScrollBarViewInfo;
begin
  Result := TcxGridInplaceEditFormAreaScrollBarViewInfo(inherited ViewInfo);
end;

{ TcxGridInplaceEditFormAreaPainter }

procedure TcxGridInplaceEditFormAreaPainter.DrawBackground;
var
  R: TRect;
begin
  R := cxRectInflate(ViewInfo.Bounds, 1, 0);
  Canvas.SaveClipRegion;
  try
    Canvas.ExcludeFrameRect(R, 1, [bLeft, bRight]);
    ViewInfo.LookAndFeelPainter.LayoutViewDrawRecordContent(Canvas, R,
      cxgpCenter, cxbsNormal, [bTop, bBottom]);
  finally
    Canvas.RestoreClipRegion;
  end;
end;

procedure TcxGridInplaceEditFormAreaPainter.DrawButtonsPanel;
begin
  ViewInfo.ButtonsPanelViewInfo.Paint(Canvas);
end;

procedure TcxGridInplaceEditFormAreaPainter.DrawContent;
begin
  inherited DrawContent;
  DrawInplaceEditFormContainer;
  DrawButtonsPanel;
  DrawScrollBars;
end;

procedure TcxGridInplaceEditFormAreaPainter.DrawGridViewItems;
var
  I: Integer;
begin
  for I := 0 to ViewInfo.GridItemViewInfos.Count - 1 do
    ViewInfo.GridItemViewInfos[I].Paint(Canvas);
end;

procedure TcxGridInplaceEditFormAreaPainter.DrawInplaceEditFormContainer;
var
  AClipRegion: TcxRegion;
  ATopBorderBounds: TRect;
begin
  ATopBorderBounds := ViewInfo.Bounds;
  ATopBorderBounds.Bottom := ATopBorderBounds.Top + 1;
  AClipRegion := Canvas.GetClipRegion;
  try
    Canvas.IntersectClipRect(ViewInfo.Bounds);
    Canvas.ExcludeClipRect(ViewInfo.ButtonsPanelViewInfo.Bounds);
    Canvas.ExcludeClipRect(ATopBorderBounds);
    DrawLayoutGroups;
    DrawGridViewItems;
  finally
    Canvas.SetClipRegion(AClipRegion, roSet);
  end;
end;

procedure TcxGridInplaceEditFormAreaPainter.DrawLayoutGroups;
begin
  ViewInfo.ContainerViewInfo.Paint(Canvas);
end;

procedure TcxGridInplaceEditFormAreaPainter.DrawScrollBars;
begin
  ViewInfo.ScrollBars.Paint(Canvas);
end;

function TcxGridInplaceEditFormAreaPainter.GetViewInfo: TcxGridInplaceEditFormAreaViewInfo;
begin
  Result := TcxGridInplaceEditFormAreaViewInfo(inherited ViewInfo);
end;

{ TcxGridDataRowPainter }

function TcxGridDataRowPainter.GetViewInfo: TcxGridDataRowViewInfo;
begin
  Result := TcxGridDataRowViewInfo(inherited ViewInfo);
end;

procedure TcxGridDataRowPainter.DrawCells;
begin
  ViewInfo.RecordsViewInfo.PainterClass.DrawDataRowCells(ViewInfo);
  with ViewInfo do
    if HasPreview and (PreviewViewInfo.Height <> 0) then
      PreviewViewInfo.Paint;
end;

procedure TcxGridDataRowPainter.DrawInplaceEditFormArea;
begin
  if ViewInfo.HasInplaceEditFormArea and
    (ViewInfo.InplaceEditFormAreaViewInfo.Height <> 0) then
    ViewInfo.InplaceEditFormAreaViewInfo.Paint;
end;

procedure TcxGridDataRowPainter.DrawTopRightEmptyArea;
begin
  Canvas.FillRect(ViewInfo.EmptyAreaBounds, ViewInfo.Params.Color);
end;

function TcxGridDataRowPainter.GetShowCells: Boolean;
begin
  Result := True;
end;

procedure TcxGridDataRowPainter.Paint;
var
  AClipRegion: TcxRegion;
begin
  if ShowCells then
  begin
    AClipRegion := Canvas.GetClipRegion;
    try
      DrawInplaceEditFormArea;
      DrawCells;
      DrawTopRightEmptyArea;
    finally
      Canvas.SetClipRegion(AClipRegion, roSet);
    end;
  end;
  inherited Paint;
end;

{ TcxGridNewItemRowPainter }

function TcxGridNewItemRowPainter.GetViewInfo: TcxGridNewItemRowViewInfo;
begin
  Result := TcxGridNewItemRowViewInfo(inherited ViewInfo);
end;

procedure TcxGridNewItemRowPainter.DrawBackground;
begin
  if not ShowCells and not ViewInfo.Transparent then
    DrawBackground(ViewInfo.ContentBounds)
  else
    inherited DrawBackground;
end;

procedure TcxGridNewItemRowPainter.DrawSeparator;
begin
  ViewInfo.LookAndFeelPainter.DrawHeader(Canvas, ViewInfo.SeparatorBounds,
    ViewInfo.SeparatorBounds, [], cxBordersAll, cxbsNormal, taLeftJustify, vaTop,
    False, False, '', nil, ViewInfo.LookAndFeelPainter.DefaultHeaderTextColor,
    ViewInfo.SeparatorColor);
end;

function TcxGridNewItemRowPainter.ExcludeFromClipRect: Boolean;
begin
  Result := True;
end;

function TcxGridNewItemRowPainter.GetShowCells: Boolean;
begin
  Result := ViewInfo.Text = '';
end;

procedure TcxGridNewItemRowPainter.Paint;
begin
  inherited Paint;
  DrawText;
end;

{ TcxGridMasterDataRowDetailsSiteTabsPainter }

function TcxGridMasterDataRowDetailsSiteTabsPainter.GetViewInfo: TcxGridMasterDataRowDetailsSiteLeftTabsViewInfo;
begin
  Result := TcxGridMasterDataRowDetailsSiteLeftTabsViewInfo(inherited ViewInfo);
end;

procedure TcxGridMasterDataRowDetailsSiteTabsPainter.DrawBottomGridLine;
begin
  Canvas.Brush.Color := ViewInfo.BottomGridLineColor;
  Canvas.FillRect(ViewInfo.BottomGridLineBounds);
end;

procedure TcxGridMasterDataRowDetailsSiteTabsPainter.Paint;
begin
  inherited;
  if ViewInfo.HasBottomGridLine then
    DrawBottomGridLine;
end;

{ TcxGridExpandButtonCellPainter }

function TcxGridExpandButtonCellPainter.GetViewInfo: TcxGridExpandButtonCellViewInfo;
begin
  Result := TcxGridExpandButtonCellViewInfo(inherited ViewInfo);
end;

procedure TcxGridExpandButtonCellPainter.DrawBorder(ABorder: TcxBorder);
begin
  inherited;
  if (ABorder = bRight) and not ViewInfo.Transparent then
    DrawBackground(ViewInfo.RightBorderRestSpaceBounds);
end;

{ TcxGridMasterDataRowPainter }

function TcxGridMasterDataRowPainter.GetViewInfo: TcxGridMasterDataRowViewInfo;
begin
  Result := TcxGridMasterDataRowViewInfo(inherited ViewInfo);
end;

procedure TcxGridMasterDataRowPainter.DrawCells;
begin
  inherited;
  DrawExpandButtonCell;
end;

procedure TcxGridMasterDataRowPainter.DrawDetailsSite;
begin
  ViewInfo.DetailsSiteViewInfo.Paint(Canvas);
  ViewInfo.GridViewInfo.Painter.ExcludeFromBackground(ViewInfo.DetailsSiteViewInfo.Bounds);
end;

{procedure TcxGridMasterDataRowPainter.DrawDetailsArea;
begin
  ViewInfo.DetailsAreaViewInfo.Paint;
end;}

procedure TcxGridMasterDataRowPainter.DrawExpandButtonCell;

  procedure SetExcludeClipRect;
  begin
    Canvas.ExcludeClipRect(ViewInfo.CellsAreaBounds);
    if ViewInfo.HasPreview then
      Canvas.ExcludeClipRect(ViewInfo.PreviewViewInfo.Bounds);
    if ViewInfo.HasInplaceEditFormArea then
      Canvas.ExcludeClipRect(ViewInfo.InplaceEditFormAreaViewInfo.Bounds);
  end;

var
  AClipRegion: TcxRegion;
begin
  AClipRegion := Canvas.GetClipRegion;
  try
    SetExcludeClipRect;
    ViewInfo.ExpandButtonCellViewInfo.Paint;
  finally
    Canvas.SetClipRegion(AClipRegion, roSet);
  end;
end;

function TcxGridMasterDataRowPainter.NeedsPainting: Boolean;
begin
  Result := inherited NeedsPainting or
    ViewInfo.DetailsSiteVisible and Canvas.RectVisible(ViewInfo.DetailsAreaViewInfo.GetAreaBoundsForPainting);
end;

procedure TcxGridMasterDataRowPainter.Paint;
begin
  if ViewInfo.DetailsSiteVisible then
  begin
    DrawDetailsSite;
    //DrawDetailsArea;
  end;
  inherited;
end;

{ TcxGridGroupCellPainter }

function TcxGridGroupCellPainter.GetViewInfo: TcxGridGroupCellViewInfo;
begin
  Result := TcxGridGroupCellViewInfo(inherited ViewInfo);
end;

procedure TcxGridGroupCellPainter.DrawBorder(ABorder: TcxBorder);
begin
  inherited;
  with Canvas, ViewInfo do
    if (ABorder = bBottom) and GridRecord.Expanded then
      if GridViewInfo.LevelIndentBackgroundBitmap = nil then
      begin
        Brush.Color := GridViewInfo.LevelIndentColors[GridRecord.Level];
        FillRect(ExpandedAreaBounds);
      end
      else
        FillRect(ExpandedAreaBounds, GridViewInfo.LevelIndentBackgroundBitmap);
end;

{ TcxGridGroupSummaryCellPainter }

function TcxGridGroupSummaryCellPainter.GetViewInfo: TcxGridGroupSummaryCellViewInfo;
begin
  Result := TcxGridGroupSummaryCellViewInfo(inherited ViewInfo);
end;

procedure TcxGridGroupSummaryCellPainter.Paint;
begin
  inherited;
  ViewInfo.SeparatorViewInfo.Paint(Canvas);
end;

{ TcxGridGroupRowPainter }

function TcxGridGroupRowPainter.GetViewInfo: TcxGridGroupRowViewInfo;
begin
  Result := TcxGridGroupRowViewInfo(inherited ViewInfo);
end;

procedure TcxGridGroupRowPainter.DrawBackground;
begin
end;

procedure TcxGridGroupRowPainter.DrawSeparator;
var
  R: TRect;
begin
  R := ViewInfo.SeparatorIndentBounds;
  if not IsRectEmpty(R) then
    DrawIndentPart(ViewInfo.Level, R);
  inherited;
end;

procedure TcxGridGroupRowPainter.DrawSummaryCells;
var
  I: Integer;
begin
  if ViewInfo.SummaryBeginningSpacerViewInfo <> nil then
    ViewInfo.SummaryBeginningSpacerViewInfo.Paint(Canvas);
  for I := 0 to ViewInfo.SummaryCellViewInfoCount - 1 do
    ViewInfo.SummaryCellViewInfos[I].Paint(Canvas);
  if ViewInfo.SummaryEndingSpacerViewInfo <> nil then
    ViewInfo.SummaryEndingSpacerViewInfo.Paint(Canvas);
end;

procedure TcxGridGroupRowPainter.Paint;
begin
  ViewInfo.CellViewInfo.Paint;
  DrawSummaryCells;
  inherited;
end;

{ TcxGridCellViewInfo }

function TcxGridCellViewInfo.GetGridView: TcxGridTableView;
begin
  Result := TcxGridTableView(inherited GridView);
end;

function TcxGridCellViewInfo.GetGridLines: TcxGridLines;
begin
  Result := RecordViewInfo.GridLines;
end;

function TcxGridCellViewInfo.GetGridRecord: TcxCustomGridRow;
begin
  Result := TcxCustomGridRow(inherited GridRecord);
end;

function TcxGridCellViewInfo.GetGridViewInfo: TcxGridTableViewInfo;
begin
  Result := TcxGridTableViewInfo(inherited GridViewInfo);
end;

function TcxGridCellViewInfo.GetRecordViewInfo: TcxCustomGridRowViewInfo;
begin
  Result := TcxCustomGridRowViewInfo(inherited RecordViewInfo);
end;

function TcxGridCellViewInfo.GetBorderColor(AIndex: TcxBorder): TColor;
begin
  Result := GridViewInfo.GridLineColor;
end;

function TcxGridCellViewInfo.GetBorderWidth(AIndex: TcxBorder): Integer;
begin
  Result := GridViewInfo.GridLineWidth;
end;

procedure TcxGridCellViewInfo.GetViewParams(var AParams: TcxViewParams);
begin
  GridView.Styles.GetDataCellParams(GridRecord, nil, AParams, True, Self);
end;

{ TcxGridDataCellViewInfo }

destructor TcxGridDataCellViewInfo.Destroy;
var
  I: Integer;
begin
  if FIsMerging then
  begin
    for I := 0 to MergedCellCount - 1 do
      MergedCells[I].FMergingCell := nil;
    FMergedCells.Free;
  end  
  else
    if FIsMerged and (FMergingCell <> nil) then
      FMergingCell.RemoveMergedCell(Self);
  inherited;
end;

function TcxGridDataCellViewInfo.GetCacheItem: TcxGridTableViewInfoCacheItem;
begin
  Result := TcxGridTableViewInfoCacheItem(inherited CacheItem);
end;

function TcxGridDataCellViewInfo.GetGridView: TcxGridTableView;
begin
  Result := TcxGridTableView(inherited GridView);
end;

function TcxGridDataCellViewInfo.GetGridViewInfo: TcxGridTableViewInfo;
begin
  Result := TcxGridTableViewInfo(inherited GridViewInfo);
end;

function TcxGridDataCellViewInfo.GetItem: TcxGridColumn;
begin
  Result := TcxGridColumn(inherited Item);
end;

function TcxGridDataCellViewInfo.GetMergedCell(Index: Integer): TcxGridDataCellViewInfo;
begin
  Result := TcxGridDataCellViewInfo(FMergedCells[Index]);
end;

function TcxGridDataCellViewInfo.GetMergedCellCount: Integer;
begin
  Result := FMergedCells.Count;
end;

function TcxGridDataCellViewInfo.GetMergedCellOfFocusedRow: TcxGridDataCellViewInfo;
var
  I: Integer;
begin
  for I := 0 to MergedCellCount - 1 do
  begin
    Result := MergedCells[I];
    if Result.RecordViewInfo.Focused then Exit;
  end;
  Result := nil;
end;

function TcxGridDataCellViewInfo.GetRecordViewInfo: TcxGridDataRowViewInfo;
begin
  Result := TcxGridDataRowViewInfo(inherited RecordViewInfo);
end;

procedure TcxGridDataCellViewInfo.AfterRowsViewInfoCalculate;
var
  AProperties: TcxCustomEditProperties;
  AValue: TcxEditValue;
  I: Integer;
  ARowViewInfo: TcxCustomGridRowViewInfo;
  ACellViewInfo: TcxGridDataCellViewInfo;

  procedure UpdateBounds;
  begin
    if FIsMerging then
    begin
      Bounds.Bottom := MergedCells[MergedCellCount - 1].Bounds.Bottom;
      Recalculate;
    end
    else
      if Height <> FOriginalHeight then
      begin
        Bounds.Bottom := Bounds.Top + FOriginalHeight;
        Recalculate;
      end;
  end;

begin
  if FIsMerged then
  begin
    UpdateBounds;
    Exit;
  end;
  if not CanCellMerging or GridViewInfo.IsInternalUse then Exit;
  AProperties := Properties;
  AValue := DisplayValue;
  for I := RecordViewInfo.Index + 1 to RecordViewInfo.RecordsViewInfo.Count - 1 do
  begin
    ARowViewInfo := RecordViewInfo.RecordsViewInfo[I];
    if ARowViewInfo is TcxGridDataRowViewInfo then
    begin
      ACellViewInfo := TcxGridDataRowViewInfo(ARowViewInfo).InternalCellViewInfos[Item.VisibleIndex];
      if Item.DoCompareValuesForCellMerging(
        RecordViewInfo.GridRecord, AProperties, AValue,
        TcxGridDataRowViewInfo(ARowViewInfo).GridRecord, ACellViewInfo.Properties, ACellViewInfo.DisplayValue) then
      begin
        if not FIsMerging then
        begin
          FIsMerging := True;
          FMergedCells := TList.Create;
        end;
        FMergedCells.Add(ACellViewInfo);
        ACellViewInfo.FIsMerged := True;
        ACellViewInfo.FMergingCell := Self;
        if not ACellViewInfo.CanBeMergingCell then Break;
      end
      else
        Break;
    end
    else
      Break;
  end;
  UpdateBounds;
end;

procedure TcxGridDataCellViewInfo.AfterRowsViewInfoOffset;
begin
  FIsMerging := False;
  FIsMerged := False;
  FreeAndNil(FMergedCells);
  FMergingCell := nil;
end;

function TcxGridDataCellViewInfo.CalculateSelected: Boolean;
var
  AMergedCellOfFocusedRow: TcxGridDataCellViewInfo;

  procedure CheckMergedCells;
  var
    I: Integer;
  begin
    if not Result and GridView.OptionsSelection.MultiSelect then
      for I := 0 to MergedCellCount - 1 do
      begin
        Result := MergedCells[I].Selected;
        if Result then Break;
      end;
  end;

begin
  Result := inherited CalculateSelected;
  if FIsMerging then
    if not GridView.OptionsSelection.MultiSelect or
      GridView.OptionsSelection.InvertSelect or Item.Focused then
      if not RecordViewInfo.Focused then
      begin
        AMergedCellOfFocusedRow := MergedCellOfFocusedRow;
        if AMergedCellOfFocusedRow <> nil then
          Result := AMergedCellOfFocusedRow.Selected
        else
          CheckMergedCells;
      end
      else
    else
      CheckMergedCells;
end;

function TcxGridDataCellViewInfo.CalculateWidth: Integer;
begin
  Result := RecordViewInfo.GetCellWidth(Item.VisibleIndex);
end;

function TcxGridDataCellViewInfo.CanBeMergingCell: Boolean;
begin
  Result := not RecordViewInfo.Expanded;
end;

function TcxGridDataCellViewInfo.CanCellMerging: Boolean;
begin
  Result := CanBeMergingCell and TcxGridColumnAccess.CanCellMerging(Item);
end;

function TcxGridDataCellViewInfo.CanDrawSelected: Boolean;
begin
  if RecordViewInfo.HasInplaceEditFormArea and not GridView.OptionsSelection.InvertSelect then
    Result := False
  else
    Result := inherited CanDrawSelected;
end;

function TcxGridDataCellViewInfo.GetAlwaysSelected: Boolean;
begin
  if RecordViewInfo.HasInplaceEditFormArea and GridView.OptionsSelection.InvertSelect then
    Result := True
  else
    Result := inherited GetAlwaysSelected;
end;

function TcxGridDataCellViewInfo.GetBorderColor(AIndex: TcxBorder): TColor;
begin
  Result := GridViewInfo.GridLineColor;
end;

function TcxGridDataCellViewInfo.GetBorders: TcxBorders;
begin
  Result := RecordViewInfo.GetCellBorders(Item.IsMostRight,
    Item.IsMostBottom and RecordViewInfo.CellsAreaViewInfo.IsBottom);
end;

function TcxGridDataCellViewInfo.GetBorderWidth(AIndex: TcxBorder): Integer;
begin
  Result := GridViewInfo.GridLineWidth;
end;

function TcxGridDataCellViewInfo.GetFocused: Boolean;
var
  AMergedCellOfFocusedRow: TcxGridDataCellViewInfo;
begin
  Result := inherited GetFocused;
  if FIsMerging and not RecordViewInfo.Focused then
  begin
    AMergedCellOfFocusedRow := MergedCellOfFocusedRow;
    if AMergedCellOfFocusedRow <> nil then
      Result := AMergedCellOfFocusedRow.Focused;
  end;
end;

function TcxGridDataCellViewInfo.GetMultiLine: Boolean;
begin
  Result := inherited GetMultiLine or RecordViewInfo.AutoHeight;
end;

function TcxGridDataCellViewInfo.GetPainterClass: TcxCustomGridCellPainterClass;
begin
  Result := TcxGridDataCellPainter;
end;

function TcxGridDataCellViewInfo.GetVisible: Boolean;
begin
  Result := not FIsMerged and inherited GetVisible;
end;

function TcxGridDataCellViewInfo.GetVisibleForHitTest: Boolean;
begin
  Result := inherited GetVisible;
end;

function TcxGridDataCellViewInfo.HasHitTestPoint(const P: TPoint): Boolean;
begin
  if IsMerging then
    Result := PtInRect(OriginalBounds, P)
  else
    Result := inherited HasHitTestPoint(P);
end;

procedure TcxGridDataCellViewInfo.Offset(DX, DY: Integer);
begin
  inherited;
  OffsetRect(OriginalBounds, DX, DY);
end;

procedure TcxGridDataCellViewInfo.RemoveMergedCell(ACellViewInfo: TcxGridDataCellViewInfo);
begin
  FMergedCells.Remove(ACellViewInfo);
end;

function TcxGridDataCellViewInfo.SupportsEditing: Boolean;
begin
  Result := not GridView.IsInplaceEditFormMode or GridRecord.IsNewItemRecord;
end;

procedure TcxGridDataCellViewInfo.BeforeRecalculation;
begin
  inherited;
  RecordViewInfo.BeforeCellRecalculation(Self);
end;

procedure TcxGridDataCellViewInfo.Calculate(ALeftBound, ATopBound: Integer;
  AWidth: Integer = -1; AHeight: Integer = -1);
begin
  inherited;
  if not FIsMerging then
  begin
    OriginalBounds := Bounds;
    FOriginalHeight := Height;
  end;
  if FIsMerged and (FMergingCell <> nil) then
    FMergingCell.Recalculate;
end;

function TcxGridDataCellViewInfo.MouseMove(AHitTest: TcxCustomGridHitTest;
  AShift: TShiftState): Boolean;
begin
  if IsMerged then
    Result := MergingCell.MouseMove(AHitTest, AShift)
  else
    Result := inherited MouseMove(AHitTest, AShift);
end;

procedure TcxGridDataCellViewInfo.Paint(ACanvas: TcxCanvas = nil);
begin
  inherited;
  if FIsMerged and RecordViewInfo.CellsAreaViewInfo.DrawMergedCells then
    FMergingCell.Paint(ACanvas);
end;

{ TcxGridDataRowCellsAreaViewInfo }

constructor TcxGridDataRowCellsAreaViewInfo.Create(ARecordViewInfo: TcxCustomGridRecordViewInfo);
begin
  inherited;
  Visible := CalculateVisible;
end;

function TcxGridDataRowCellsAreaViewInfo.GetRecordViewInfo: TcxGridDataRowViewInfo;
begin
  Result := TcxGridDataRowViewInfo(inherited RecordViewInfo);
end;

function TcxGridDataRowCellsAreaViewInfo.CalculateHeight: Integer;
begin
  Result := 0;
end;

function TcxGridDataRowCellsAreaViewInfo.CalculateVisible: Boolean;
begin
  Result := RecordViewInfo.GridViewInfo.HeaderViewInfo.Count = 0;
end;

function TcxGridDataRowCellsAreaViewInfo.CalculateWidth: Integer;
begin
  Result := 0;
end;

function TcxGridDataRowCellsAreaViewInfo.GetBorders: TcxBorders;
begin
  Result := GridViewInfo.GetCellBorders(True, IsBottom);
end;

function TcxGridDataRowCellsAreaViewInfo.GetIsBottom: Boolean;
begin
  Result :=
    not RecordViewInfo.HasPreview or (GridView.Preview.Place = ppTop) or
    (RecordViewInfo.PreviewViewInfo.Height = 0);
end;

function TcxGridDataRowCellsAreaViewInfo.CanDrawSelected: Boolean;
begin
  Result := True;
end;

function TcxGridDataRowCellsAreaViewInfo.DrawMergedCells: Boolean;
begin
  Result := RecordViewInfo.Transparent;
end;

{ TcxGridPreviewCellViewInfo }

function TcxGridPreviewCellViewInfo.GetPreview: TcxGridPreview;
begin
  Result := GridView.Preview;
end;

function TcxGridPreviewCellViewInfo.CalculateHeight: Integer;

  function GetMaxValue: Integer;
  begin
    if Preview.MaxLineCount = 0 then
      Result := 0
    else
    begin
      Result := Preview.MaxLineCount * GridViewInfo.GetFontHeight(Params.Font);
      GetCellTextAreaSize(Result);
    end;
  end;

begin
  if CacheItem.IsPreviewHeightAssigned then
    Result := Height
  else
  begin
    if AutoHeight then
      Result := inherited CalculateHeight
    else
    begin
      CalculateParams;
      Result := GetMaxValue;
    end;
    if Result <> 0 then
      Result := RecordViewInfo.RecordsViewInfo.GetCellHeight(Result);
  end;
end;

function TcxGridPreviewCellViewInfo.CalculateWidth: Integer;
begin
  Result := RecordViewInfo.DataWidth;
end;

function TcxGridPreviewCellViewInfo.GetAutoHeight: Boolean;
begin
  Result := Preview.AutoHeight;
end;

function TcxGridPreviewCellViewInfo.GetBackgroundBitmap: TBitmap;
begin
  Result := GridView.BackgroundBitmaps.GetBitmap(bbPreview);
end;

function TcxGridPreviewCellViewInfo.GetBorders: TcxBorders;
begin
  Result := GridViewInfo.GetCellBorders(True,
    (Preview.Place = ppBottom) or (RecordViewInfo.CellViewInfoCount = 0));
end;

procedure TcxGridPreviewCellViewInfo.GetEditViewDataContentOffsets(var R: TRect);
begin
  inherited;
  R.Left := Preview.LeftIndent - (cxGridCellTextOffset - cxGridEditOffset);
  R.Right := Preview.RightIndent - (cxGridCellTextOffset - cxGridEditOffset);
end;

function TcxGridPreviewCellViewInfo.GetHeight: Integer;
begin
  if CacheItem.IsPreviewHeightAssigned then
    Result := CacheItem.PreviewHeight
  else
  begin
    Result := CalculateHeight;
    CacheItem.PreviewHeight := Result;
  end;
end;

function TcxGridPreviewCellViewInfo.GetHitTestClass: TcxCustomGridHitTestClass;
begin
  Result := TcxGridPreviewHitTest;
end;

function TcxGridPreviewCellViewInfo.GetMaxLineCount: Integer;
begin
  Result := Preview.MaxLineCount;
end;

function TcxGridPreviewCellViewInfo.GetMultiLine: Boolean;
begin
  Result := True;
end;

function TcxGridPreviewCellViewInfo.GetTextAreaBounds: TRect;
begin
  Result := inherited GetTextAreaBounds;
  InflateRect(Result, cxGridCellTextOffset, 0);
  Inc(Result.Left, Preview.LeftIndent);
  Dec(Result.Right, Preview.RightIndent);
end;

function TcxGridPreviewCellViewInfo.SupportsZeroHeight: Boolean;
begin
  Result := True;
end;

{ TcxGridInplaceEditFormAreaScrollBar }

constructor TcxGridInplaceEditFormAreaScrollBar.Create(AOwner: IcxScrollBarOwner;
  AViewInfo: TcxGridInplaceEditFormAreaViewInfo);
begin
  inherited Create(AOwner);
  FViewInfo := AViewInfo;
end;

procedure TcxGridInplaceEditFormAreaScrollBar.Scroll(ScrollCode: TScrollCode; var ScrollPos: Integer);
begin
  inherited Scroll(ScrollCode, ScrollPos);
  if (ScrollPos + PageSize) > Max then
    ScrollPos := Max - PageSize;
  ViewInfo.UpdateOnScroll;
end;

{ TcxGridInplaceEditFormAreaScrollBarViewInfo }

constructor TcxGridInplaceEditFormAreaScrollBarViewInfo.Create(
  AViewInfo: TcxGridInplaceEditFormAreaViewInfo);
begin
  inherited Create(AViewInfo.GridViewInfo);
  FViewInfo := AViewInfo;
  CreateScrollBar;
end;

destructor TcxGridInplaceEditFormAreaScrollBarViewInfo.Destroy;
begin
  DestroyScrollBar;
  inherited Destroy;
end;

procedure TcxGridInplaceEditFormAreaScrollBarViewInfo.Calculate(ALeftBound, ATopBound: Integer;
  AWidth: Integer = -1; AHeight: Integer = -1);
begin
  inherited Calculate(ALeftBound, ATopBound, AWidth, AHeight);
  ScrollBar.Left := Bounds.Left;
  ScrollBar.Top := Bounds.Top;
  ScrollBar.Calculate;
end;

procedure TcxGridInplaceEditFormAreaScrollBarViewInfo.SetScrollParams(
  AMin, AMax, APosition, APageSize: Integer; ARedraw: Boolean = True);
begin
  ScrollBar.SetScrollParams(AMin, AMax, APosition, APageSize, ARedraw);
end;

function TcxGridInplaceEditFormAreaScrollBarViewInfo.MouseDown(AHitTest: TcxCustomGridHitTest; AButton: TMouseButton;
  AShift: TShiftState): Boolean;
begin
  Result := inherited MouseDown(AHitTest, AButton, AShift);
  FScrollBarController.MouseDown(AButton, AShift, AHitTest.Pos.X, AHitTest.Pos.Y);
end;

procedure TcxGridInplaceEditFormAreaScrollBarViewInfo.MouseLeave;
begin
  inherited MouseLeave;
  if State = gcsNone then
    FScrollBarController.MouseLeave(Control);
end;

function TcxGridInplaceEditFormAreaScrollBarViewInfo.MouseMove(
  AHitTest: TcxCustomGridHitTest; AShift: TShiftState): Boolean;
begin
  Result := inherited MouseMove(AHitTest, AShift);
  FScrollBarController.MouseMove(AShift, AHitTest.Pos.X, AHitTest.Pos.Y);
end;

function TcxGridInplaceEditFormAreaScrollBarViewInfo.MouseUp(AHitTest: TcxCustomGridHitTest; AButton: TMouseButton;
  AShift: TShiftState): Boolean;
begin
  Result := inherited MouseUp(AHitTest, AButton, AShift);
  FScrollBarController.MouseUp(AButton, AShift, AHitTest.Pos.X, AHitTest.Pos.Y);
end;

function TcxGridInplaceEditFormAreaScrollBarViewInfo.CalculateHeight: Integer;
begin
  Result := ScrollBar.Height;
end;

function TcxGridInplaceEditFormAreaScrollBarViewInfo.CalculateWidth: Integer;
begin
  Result := ScrollBar.Width;
end;

function TcxGridInplaceEditFormAreaScrollBarViewInfo.GetHeight: Integer;
begin
  Result := CalculateHeight;
end;

function TcxGridInplaceEditFormAreaScrollBarViewInfo.GetHitTestClass: TcxCustomGridHitTestClass;
begin
  Result := TcxGridInplaceEditFormAreaScrollBarHitTest;
end;

function TcxGridInplaceEditFormAreaScrollBarViewInfo.GetPainterClass: TcxCustomGridCellPainterClass;
begin
  Result := TcxGridInplaceEditFormAreaScrollBarPainter;
end;

function TcxGridInplaceEditFormAreaScrollBarViewInfo.GetScrollBarClass: TcxGridInplaceEditFormAreaScrollBarClass;
begin
  Result := TcxGridInplaceEditFormAreaScrollBar;
end;

function TcxGridInplaceEditFormAreaScrollBarViewInfo.GetScrollBarControllerClass: TcxScrollBarControllerClass;
begin
  Result := TcxScrollBarController;
end;

function TcxGridInplaceEditFormAreaScrollBarViewInfo.GetWidth: Integer;
begin
  Result := CalculateWidth;
end;

procedure TcxGridInplaceEditFormAreaScrollBarViewInfo.SetHeight(Value: Integer);
begin
  inherited SetHeight(Value);
  ScrollBar.Height := Value;
end;

procedure TcxGridInplaceEditFormAreaScrollBarViewInfo.SetWidth(Value: Integer);
begin
  inherited SetWidth(Value);
  ScrollBar.Width := Value;
end;

function TcxGridInplaceEditFormAreaScrollBarViewInfo.CaptureMouseOnPress: Boolean;
begin
  Result := True;
end;

function TcxGridInplaceEditFormAreaScrollBarViewInfo.GetHotTrack: Boolean;
begin
  Result := True;
end;

procedure TcxGridInplaceEditFormAreaScrollBarViewInfo.CreateScrollBar;
begin
  FScrollBar := GetScrollBarClass.Create(Self, InplaceEditFormAreaViewInfo);
  FScrollBar.UnlimitedTracking := True;
  FScrollBarController := GetScrollBarControllerClass.Create(FScrollBar);
end;

procedure TcxGridInplaceEditFormAreaScrollBarViewInfo.DestroyScrollBar;
begin
  FreeAndNil(FScrollBarController);
  FreeAndNil(FScrollBar);
end;

function TcxGridInplaceEditFormAreaScrollBarViewInfo.GetKind: TScrollBarKind;
begin
  Result := ScrollBar.Kind;
end;

function TcxGridInplaceEditFormAreaScrollBarViewInfo.GetMax: Integer;
begin
  Result := ScrollBar.Max;
end;

function TcxGridInplaceEditFormAreaScrollBarViewInfo.GetMin: Integer;
begin
  Result := ScrollBar.Min;
end;

function TcxGridInplaceEditFormAreaScrollBarViewInfo.GetPageSize: Integer;
begin
  Result := ScrollBar.PageSize;
end;

function TcxGridInplaceEditFormAreaScrollBarViewInfo.GetPosition: Integer;
begin
  Result := ScrollBar.Position;
end;

procedure TcxGridInplaceEditFormAreaScrollBarViewInfo.SetKind(Value: TScrollBarKind);
begin
  ScrollBar.Kind := Value;
  if Value = sbHorizontal then
  begin
    ScrollBar.Height := GetScrollBarSize.cy;
    ScrollBar.Width := GetScrollBarSize.cx;
  end
  else
  begin
    ScrollBar.Height := GetScrollBarSize.cx;
    ScrollBar.Width := GetScrollBarSize.cy;
  end;
end;

procedure TcxGridInplaceEditFormAreaScrollBarViewInfo.SetMax(Value: Integer);
begin
  ScrollBar.Max := Value;
end;

procedure TcxGridInplaceEditFormAreaScrollBarViewInfo.SetMin(Value: Integer);
begin
  ScrollBar.Min := Value;
end;

procedure TcxGridInplaceEditFormAreaScrollBarViewInfo.SetPageSize(Value: Integer);
begin
  ScrollBar.PageSize := Value;
end;

procedure TcxGridInplaceEditFormAreaScrollBarViewInfo.SetPosition(Value: Integer);
begin
  ScrollBar.Position := Value;
end;

function TcxGridInplaceEditFormAreaScrollBarViewInfo.GetScrollBarOwner: TWinControl;
begin
  Result := Control;
end;

function TcxGridInplaceEditFormAreaScrollBarViewInfo.GetLookAndFeel: TcxLookAndFeel;
begin
  Result := GridView.LookAndFeel;
end;

{ TcxGridInplaceEditFormAreaScrollBars }

constructor TcxGridInplaceEditFormAreaScrollBars.Create(AInplaceEditFormAreaViewInfo: TcxGridInplaceEditFormAreaViewInfo);
begin
  inherited Create;
  FViewInfo := AInplaceEditFormAreaViewInfo;
  CreateScrollBars;
end;

destructor TcxGridInplaceEditFormAreaScrollBars.Destroy;
begin
  DestroyScrollBars;
  inherited Destroy;
end;

procedure TcxGridInplaceEditFormAreaScrollBars.Calculate;
begin
  CalculateHorizontalScrollBar;
  CalculateVerticalScrollBar;
end;

function TcxGridInplaceEditFormAreaScrollBars.GetContainerOffset: TPoint;
begin
  Result := Point(0, 0);
  Result.X := GetContainerOffsetFromScrollPosition(HorizontalScrollBar, sbHorizontal);
  Result.Y := GetContainerOffsetFromScrollPosition(VerticalScrollBar, sbVertical);
end;

function TcxGridInplaceEditFormAreaScrollBars.GetHitTest(const P: TPoint): TcxCustomGridHitTest;
var
  AHitTest: TcxCustomGridHitTest;
begin
  Result := nil;
  if HasHorizontalScrollBar then
  begin
    AHitTest := HorizontalScrollBar.GetHitTest(P);
    if AHitTest <> nil then
    begin
      Result := AHitTest;
      Exit;
    end;
  end;
  if HasVerticalScrollBar then
  begin
    AHitTest := VerticalScrollBar.GetHitTest(P);
    if AHitTest <> nil then
    begin
      Result := AHitTest;
      Exit;
    end;
  end;
end;

procedure TcxGridInplaceEditFormAreaScrollBars.Paint(ACanvas: TcxCanvas);
begin
  if HasHorizontalScrollBar then
    HorizontalScrollBar.Paint(ACanvas);
  if HasVerticalScrollBar then
    VerticalScrollBar.Paint(ACanvas);
end;

procedure TcxGridInplaceEditFormAreaScrollBars.CalculateHorizontalScrollBar;
var
  ABounds: TRect;
begin
  if not HasHorizontalScrollBar then
    Exit;
  ABounds := ViewInfo.Bounds;
  CalculateScrollBarViewInfo(HorizontalScrollBar, ViewInfo.Width, ABounds.Left, ABounds.Top + ViewInfo.DataHeight);
end;

procedure TcxGridInplaceEditFormAreaScrollBars.CalculateScrollBarViewInfo(
  AScrollBarViewInfo: TcxGridInplaceEditFormAreaScrollBarViewInfo; ASize, ALeft, ATop: Integer);
var
  APageSize, APosition: Integer;
begin
  if AScrollBarViewInfo.Kind = sbHorizontal then
  begin
    AScrollBarViewInfo.Width := ASize;
    APageSize := Round(ASize * ViewInfo.DataWidth / ContainerViewInfo.ContentWidth);
  end
  else
  begin
    AScrollBarViewInfo.Height := ASize;
    APageSize := Round(ASize * ViewInfo.DataHeight / ContainerViewInfo.ContentHeight);
  end;
  APosition := AScrollBarViewInfo.Position;
  AScrollBarViewInfo.SetScrollParams(0, ASize, APosition, APageSize, False);
  AScrollBarViewInfo.Calculate(ALeft, ATop);
end;

procedure TcxGridInplaceEditFormAreaScrollBars.CalculateVerticalScrollBar;
var
  ABounds: TRect;
begin
  if not HasVerticalScrollBar then
    Exit;
  ABounds := ViewInfo.Bounds;
  CalculateScrollBarViewInfo(VerticalScrollBar,
    ViewInfo.DataHeight,
    ABounds.Right - VerticalScrollBar.Width, ABounds.Top);
end;

procedure TcxGridInplaceEditFormAreaScrollBars.CheckHorizontalScrollPosition;
begin
  if HasHorizontalScrollBar then
    HorizontalScrollBar.Position := Round((ViewInfo.Bounds.Left - ContainerViewInfo.Offset.X) *
      (HorizontalScrollBar.Max - HorizontalScrollBar.Min - HorizontalScrollBar.PageSize)/
      (ContainerViewInfo.ContentWidth - ViewInfo.DataWidth))
end;

procedure TcxGridInplaceEditFormAreaScrollBars.CheckPosition;
begin
  CheckHorizontalScrollPosition;
  CheckVerticalScrollPosition;
end;

procedure TcxGridInplaceEditFormAreaScrollBars.CheckVerticalScrollPosition;
begin
  if HasVerticalScrollBar then
    VerticalScrollBar.Position := Round((ViewInfo.Bounds.Top - ContainerViewInfo.Offset.Y) *
      (VerticalScrollBar.Max - VerticalScrollBar.Min - VerticalScrollBar.PageSize)/
      (ContainerViewInfo.ContentHeight - ViewInfo.DataHeight))
end;

function TcxGridInplaceEditFormAreaScrollBars.GetContainerOffsetFromScrollPosition(
  AScrollBarViewInfo: TcxGridInplaceEditFormAreaScrollBarViewInfo; AKind: TScrollBarKind): Integer;
var
  ADelta, AStartPosition: Integer;
begin
  if AKind = sbHorizontal then
    Result :=  ViewInfo.Bounds.Left
  else
    Result := ViewInfo.Bounds.Top;
  if AScrollBarViewInfo <> nil then
  begin
    if (AScrollBarViewInfo.Max - AScrollBarViewInfo.Min - AScrollBarViewInfo.PageSize) <= 0 then
      Result := 0
    else
    begin
      if AKind = sbHorizontal then
      begin
        ADelta := ContainerViewInfo.ContentWidth - ViewInfo.DataWidth;
        AStartPosition := ViewInfo.Bounds.Left
      end
      else
      begin
        ADelta := ContainerViewInfo.ContentHeight - ViewInfo.DataHeight;
        AStartPosition := ViewInfo.Bounds.Top;
      end;
      Result := AStartPosition - Round(AScrollBarViewInfo.Position * ADelta /
        (AScrollBarViewInfo.Max - AScrollBarViewInfo.Min - AScrollBarViewInfo.PageSize))
    end;
  end;
end;

function TcxGridInplaceEditFormAreaScrollBars.GetHorizontalScrollBarHeight: Integer;
begin
  if HasHorizontalScrollBar then
    Result := HorizontalScrollBar.Height
  else
    Result := 0;
end;

function TcxGridInplaceEditFormAreaScrollBars.GetVerticalScrollBarWidth: Integer;
begin
  if HasVerticalScrollBar then
    Result := VerticalScrollBar.Width
  else
    Result := 0;
end;

function TcxGridInplaceEditFormAreaScrollBars.GetScrollBarClass: TcxGridInplaceEditFormAreaScrollBarViewInfoClass;
begin
  Result := TcxGridInplaceEditFormAreaScrollBarViewInfo;
end;

function TcxGridInplaceEditFormAreaScrollBars.HasHorizontalScrollBar: Boolean;
begin
  Result := HorizontalScrollBar <> nil;
end;

function TcxGridInplaceEditFormAreaScrollBars.HasVerticalScrollBar: Boolean;
begin
  Result := VerticalScrollBar <> nil;
end;

procedure TcxGridInplaceEditFormAreaScrollBars.CreateScrollBars;
begin
  ViewInfo.IsNeedHorizontalScrollBar := ContainerViewInfo.ContentWidth > ViewInfo.DataWidth;
  ViewInfo.IsNeedVerticalScrollBar := ContainerViewInfo.ContentHeight > ViewInfo.DataHeight;
  if ViewInfo.IsNeedVerticalScrollBar and not ViewInfo.IsNeedHorizontalScrollBar then 
    ViewInfo.IsNeedHorizontalScrollBar := ContainerViewInfo.ContentWidth > ViewInfo.DataWidth;
  if ViewInfo.IsNeedHorizontalScrollBar and (FHorizontalScrollBar = nil) then
    FHorizontalScrollBar := GetScrollBarClass.Create(ViewInfo);
  if ViewInfo.IsNeedVerticalScrollBar then
  begin
    FVerticalScrollBar := GetScrollBarClass.Create(ViewInfo);
    FVerticalScrollBar.Kind := sbVertical;
  end;
end;

procedure TcxGridInplaceEditFormAreaScrollBars.DestroyScrollBars;
begin
  FreeAndNil(FVerticalScrollBar);
  FreeAndNil(FHorizontalScrollBar);
end;

function TcxGridInplaceEditFormAreaScrollBars.GetContainerViewInfo: TcxGridInplaceEditFormContainerViewInfo;
begin
  Result := ViewInfo.ContainerViewInfo;
end;

{ TcxGridInplaceEditFormButtonViewInfo }

constructor TcxGridInplaceEditFormButtonViewInfo.Create(AButtonsPanelViewInfo: TcxGridInplaceEditFormButtonsPanelViewInfo);
begin
  inherited Create(AButtonsPanelViewInfo.GridViewInfo);
  FViewInfo := AButtonsPanelViewInfo;
end;

function TcxGridInplaceEditFormButtonViewInfo.DoCalculateHeight: Integer;
begin
  Result := TextHeightWithOffset + 2 * LookAndFeelPainter.ButtonTextOffset;
  Result := Max(Result, cxGridInplaceEditFormButtonMinHeight);
end;

function TcxGridInplaceEditFormButtonViewInfo.DoCalculateWidth: Integer;
begin
  Result := TextWidthWithOffset + 2 * LookAndFeelPainter.ButtonTextOffset;
  Result := Max(Result, cxGridInplaceEditFormButtonMinWidth);
end;

function TcxGridInplaceEditFormButtonViewInfo.GetHitTestClass: TcxCustomGridHitTestClass;
begin
  Result := TcxGridInplaceEditFormButtonHitTest;
end;

function TcxGridInplaceEditFormButtonViewInfo.GetPainterClass: TcxCustomGridCellPainterClass;
begin
  Result := TcxGridInplaceEditFormButtonPainter;
end;

procedure TcxGridInplaceEditFormButtonViewInfo.GetViewParams(var AParams: TcxViewParams);
begin
  GridView.Styles.GetViewParams(0, nil, nil, AParams);
end;

function TcxGridInplaceEditFormButtonViewInfo.GetGridView: TcxGridTableView;
begin
  Result := TcxGridTableView(inherited GridView);
end;

function TcxGridInplaceEditFormButtonViewInfo.GetInplaceEditFormAreaViewInfo: TcxGridInplaceEditFormAreaViewInfo;
begin
  Result := ButtonsPanelViewInfo.ViewInfo;
end;

{ TcxGridInplaceEditFormCancelButtonViewInfo }

procedure TcxGridInplaceEditFormCancelButtonViewInfo.Click;
begin
  ViewInfo.RecordViewInfo.CancelInplaceEditFormEditing;
end;

function TcxGridInplaceEditFormCancelButtonViewInfo.GetHitTestClass: TcxCustomGridHitTestClass;
begin
  Result := TcxGridInplaceEditFormCancelButtonHitTest;
end;

function TcxGridInplaceEditFormCancelButtonViewInfo.GetText: string;
begin
  Result := cxGetResourceString(@scxGridInplaceEditFormButtonCancel);
end;

function TcxGridInplaceEditFormCancelButtonViewInfo.GetFocused: Boolean;
begin
  Result := (ViewInfo.FocusedItemKind = fikCancelButton) and GridView.IsControlFocused;
end;

{ TcxGridInplaceEditFormUpdateButtonViewInfo }

procedure TcxGridInplaceEditFormUpdateButtonViewInfo.Click;
begin
  try
    ViewInfo.RecordViewInfo.UpdateInplaceEditFormEditing;
  except
    MouseCapture := False;
    raise;
  end;
end;

function TcxGridInplaceEditFormUpdateButtonViewInfo.GetEnabled: Boolean;
begin
  Result := ViewInfo.InplaceEditForm.IsUpdateButtonEnabled;
end;

function TcxGridInplaceEditFormUpdateButtonViewInfo.GetHitTestClass: TcxCustomGridHitTestClass;
begin
  Result := TcxGridInplaceEditFormUpdateButtonHitTest;
end;

function TcxGridInplaceEditFormUpdateButtonViewInfo.GetHotTrack: Boolean;
begin
  Result := Enabled;
end;

function TcxGridInplaceEditFormUpdateButtonViewInfo.GetText: string;
begin
  Result := cxGetResourceString(@scxGridInplaceEditFormButtonUpdate);
end;

function TcxGridInplaceEditFormUpdateButtonViewInfo.GetFocused: Boolean;
begin
  Result := (ViewInfo.FocusedItemKind = fikUpdateButton) and GridView.IsControlFocused;
end;

{ TcxGridInplaceEditFormButtonsPanelViewInfo }

constructor TcxGridInplaceEditFormButtonsPanelViewInfo.Create(AInplaceEditFormAreaViewInfo: TcxGridInplaceEditFormAreaViewInfo);
begin
  inherited Create(AInplaceEditFormAreaViewInfo.GridViewInfo);
  FViewInfo := AInplaceEditFormAreaViewInfo;
  FItems := TdxFastObjectList.Create;
  CreateButtons;
  Params.Color := ViewInfo.Params.Color;
end;

destructor TcxGridInplaceEditFormButtonsPanelViewInfo.Destroy;
begin
  DestroyButtons;
  FreeAndNil(FItems);
  inherited Destroy;
end;

procedure TcxGridInplaceEditFormButtonsPanelViewInfo.Calculate(ALeftBound, ATopBound: Integer;
  AWidth: Integer = -1; AHeight: Integer = -1);
begin
  inherited Calculate(ALeftBound, ATopBound, AWidth, AHeight);
  CalculateButtons;
end;

function TcxGridInplaceEditFormButtonsPanelViewInfo.GetHitTest(const P: TPoint): TcxCustomGridHitTest;
var
  AHitTest: TcxCustomGridHitTest;
  I: Integer;
begin
  Result := inherited GetHitTest(P);
  if (Result <> nil) and (Result.ClassType = GetHitTestClass) then
  begin
    for I := 0 to FItems.Count - 1 do
    begin
      AHitTest := Items[I].GetHitTest(P);
      if AHitTest <> nil then
      begin
        Result := AHitTest;
        Exit;
      end;
    end;
  end;
end;

function TcxGridInplaceEditFormButtonsPanelViewInfo.AddItem(
  AItemClass: TcxGridInplaceEditFormButtonViewInfoClass): TcxGridInplaceEditFormButtonViewInfo;
begin
  Result := AItemClass.Create(Self);
  FItems.Add(Result);
end;

function TcxGridInplaceEditFormButtonsPanelViewInfo.GetButtonRightIndent: Integer;
var
  AContainerRootOffset: TdxLayoutLookAndFeelOffsets;
begin
  AContainerRootOffset := ViewInfo.ContainerViewInfo.LayoutLookAndFeel.Offsets;
  Result := AContainerRootOffset.RootItemsAreaOffsetHorz + AContainerRootOffset.ItemOffset;
end;

procedure TcxGridInplaceEditFormButtonsPanelViewInfo.CalculateButtons;
var
  I: Integer;
  ALeft, ATop, AButtonWidth: Integer;
begin
  ALeft := Bounds.Right;
  AButtonWidth := GetMaxButtonWidth;
  SetButtonsWidth(AButtonWidth);
  for I := 0 to FItems.Count - 1 do
  begin
    if I = 0 then
      ALeft := ALeft - GetButtonRightIndent
    else
      ALeft := ALeft - cxGridInplaceEditFormButtonsOffset;
    ALeft := ALeft - Items[I].Width;
    ATop := Bounds.Top + cxGridInplaceEditFormButtonsTopOffset + GetSeparatorHeight;
    Items[I].Calculate(ALeft, ATop, Items[I].Width);
  end;
end;

function TcxGridInplaceEditFormButtonsPanelViewInfo.CalculateHeight: Integer;
var
  I, AItemHeight: Integer;
begin
  Result := 0;
  for I := 0 to FItems.Count - 1 do
    if Items[I].Visible then
    begin
      AItemHeight := Items[I].CalculateHeight;
      Result := Max(Result, AItemHeight);
    end;
  if Result <> 0 then
  begin
    Inc(Result, cxGridInplaceEditFormButtonsTopOffset);
    Inc(Result, cxGridInplaceEditFormButtonsBottomOffset);
    Inc(Result, GetSeparatorHeight);
  end;
end;

function TcxGridInplaceEditFormButtonsPanelViewInfo.CalculateWidth: Integer;
begin
  Result := ViewInfo.Width;
end;

function TcxGridInplaceEditFormButtonsPanelViewInfo.GetHeight: Integer;
begin
  Result := CalculateHeight;
end;

function TcxGridInplaceEditFormButtonsPanelViewInfo.GetHitTestClass: TcxCustomGridHitTestClass;
begin
  Result := TcxGridInplaceEditFormButtonsPanelHitTest;
end;

function TcxGridInplaceEditFormButtonsPanelViewInfo.GetPainterClass: TcxCustomGridCellPainterClass;
begin
  Result := TcxGridInplaceEditFormButtonsPanelPainter;
end;

function TcxGridInplaceEditFormButtonsPanelViewInfo.GetSeparatorBound: TRect;
begin
  Result := Bounds;
  Result.Left := Result.Left + GetButtonRightIndent;
  Result.Right := Result.Right - GetButtonRightIndent;
  Result.Bottom := Result.Top + GetSeparatorHeight;
end;

function TcxGridInplaceEditFormButtonsPanelViewInfo.GetSeparatorHeight: Integer;
begin
  Result := Form.Container.LayoutLookAndFeel.GetSeparatorItemMinWidth;
end;

procedure TcxGridInplaceEditFormButtonsPanelViewInfo.GetViewParams(var AParams: TcxViewParams);
begin
  GridView.Styles.GetViewParams(0, nil, nil, AParams);
end;

function TcxGridInplaceEditFormButtonsPanelViewInfo.GetWidth: Integer;
begin
  Result := CalculateWidth;
end;

procedure TcxGridInplaceEditFormButtonsPanelViewInfo.CreateButtons;
begin
  AddItem(TcxGridInplaceEditFormCancelButtonViewInfo);
  AddItem(TcxGridInplaceEditFormUpdateButtonViewInfo);
end;

procedure TcxGridInplaceEditFormButtonsPanelViewInfo.DestroyButtons;
begin
  FItems.Clear;
end;

function TcxGridInplaceEditFormButtonsPanelViewInfo.GetButtonCount: Integer;
begin
  Result := FItems.Count;
end;

function TcxGridInplaceEditFormButtonsPanelViewInfo.GetGridView: TcxGridTableView;
begin
  Result := TcxGridTableView(inherited GridView);
end;

function TcxGridInplaceEditFormButtonsPanelViewInfo.GetForm: TcxGridTableViewInplaceEditForm;
begin
  Result := ViewInfo.InplaceEditForm;
end;

function TcxGridInplaceEditFormButtonsPanelViewInfo.GetItem(
  Index: Integer): TcxGridInplaceEditFormButtonViewInfo;
begin
  Result := TcxGridInplaceEditFormButtonViewInfo(FItems[Index]);
end;

function TcxGridInplaceEditFormButtonsPanelViewInfo.GetMaxButtonWidth: Integer;
var
  I: Integer;
begin
  Result := 0;
  for I := 0 to FItems.Count - 1 do
    Result := Max(Result, Items[I].CalculateWidth);
end;

procedure TcxGridInplaceEditFormButtonsPanelViewInfo.SetButtonsWidth(AValue: Integer);
var
  I: Integer;
begin
  for I := 0 to FItems.Count - 1 do
    Items[I].Width :=  AValue;
end;

{ TcxGridInplaceEditFormAreaViewInfoCacheInfo }

procedure TcxGridInplaceEditFormAreaViewInfoCacheInfo.SetHeight(
  const Value: Integer);
begin
  FHeight := Value;
  FIsHeightAssigned := True;
end;

procedure TcxGridInplaceEditFormAreaViewInfoCacheInfo.SetRestHeight(
  const Value: Integer);
begin
  FRestHeight := Value;
  FIsRestHeightAssigned := True;
end;

{ TcxGridInplaceEditFormAreaViewInfo }

constructor TcxGridInplaceEditFormAreaViewInfo.Create(
  AGridViewInfo: TcxCustomGridViewInfo; ARecordViewInfo: TcxGridDataRowViewInfo);
begin
  inherited Create(AGridViewInfo);
  FCacheInfo := TcxGridInplaceEditFormAreaViewInfoCacheInfo.Create;
  FRecordViewInfo := ARecordViewInfo;
  ContainerViewInfo.Calculate;
  CreateGridItemViewInfos;
  CreateButtonsPanel;
  CreateScrollBars;
end;

destructor TcxGridInplaceEditFormAreaViewInfo.Destroy;
begin
  DestroyScrollBars;
  DestroyButtonsPanel;
  DestroyGridItemViewInfos;
  FreeAndNil(FCacheInfo);
  inherited Destroy;
end;

procedure TcxGridInplaceEditFormAreaViewInfo.BeforeRecalculation;
var
  I: Integer;
begin
  inherited BeforeRecalculation;
  for I := 0 to GridItemViewInfos.Count - 1 do
    GridItemViewInfos[I].BeforeRecalculation;
end;

procedure TcxGridInplaceEditFormAreaViewInfo.Calculate(ALeftBound, ATopBound: Integer; AWidth: Integer = -1;
  AHeight: Integer = -1);
var
  AOffsetX, AOffsetY: Integer;
begin
  inherited Calculate(ALeftBound, ATopBound, AWidth, AHeight);
  ButtonsPanelViewInfo.Calculate(Bounds.Left,
    Bounds.Bottom - ButtonsPanelViewInfo.CalculateHeight);
  ScrollBars.Calculate;
  AOffsetX := ScrollBars.GetContainerOffset.X;
  AOffsetY := ScrollBars.GetContainerOffset.Y;
  ContainerViewInfo.Offset := Point(AOffsetX, AOffsetY);
  CalculateLayoutContainerViewInfo;
end;

function TcxGridInplaceEditFormAreaViewInfo.FindCellViewInfo(
  AGridItem: TcxCustomGridTableItem): TcxGridTableDataCellViewInfo;
begin
  Result := GridItemViewInfos.FindCellViewInfo(AGridItem);
end;

function TcxGridInplaceEditFormAreaViewInfo.GetBoundsForEdit(const ADataCellBounds: TRect): TRect;
begin
  Result := ADataCellBounds;
  if Result.Right > (Bounds.Left + DataWidth) then
    Result.Right := Bounds.Left + DataWidth;
  if Result.Bottom > (Bounds.Top + DataHeight) then
    Result.Bottom := Bounds.Top + DataHeight;
end;

function TcxGridInplaceEditFormAreaViewInfo.GetHitTest(const P: TPoint): TcxCustomGridHitTest;
var
  AHitTest: TcxCustomGridHitTest;
begin
  Result := inherited GetHitTest(P);
  if (Result <> nil) and (Result.ClassType = GetHitTestClass) then
  begin
    AHitTest := ScrollBars.GetHitTest(P);
    if AHitTest <> nil then
    begin
      Result := AHitTest;
      Exit;
    end;
    AHitTest := ButtonsPanelViewInfo.GetHitTest(P);
    if AHitTest <> nil then
    begin
      Result := AHitTest;
      Exit;
    end;
    if IsPointInLayoutContainer(P) then
    begin
      AHitTest := GetGridItemHitTest(P);
      if AHitTest <> nil then
      begin
        Result := AHitTest;
        Exit;
      end;
    end;
  end;
end;

procedure TcxGridInplaceEditFormAreaViewInfo.InvalidateUpdateButtonInfo;
begin
  ButtonsPanelViewInfo.Items[1].Invalidate;
end;

procedure TcxGridInplaceEditFormAreaViewInfo.MakeDataCellVisible(
  ACellViewInfo: TcxGridInplaceEditFormDataCellViewInfo);

  function GetLeftOffset: Integer;
  var
    ALayoutItemBounds: TRect;
  begin
    Result := 0;
    ALayoutItemBounds := ACellViewInfo.LayoutItemViewInfoBounds;
    if ALayoutItemBounds.Left < Bounds.Left then
      Result := Bounds.Left - ALayoutItemBounds.Left
    else
      if ALayoutItemBounds.Right > (Bounds.Left + DataWidth) then
        if DataWidth > cxRectWidth(ALayoutItemBounds) then
          Result := Bounds.Left + DataWidth - ALayoutItemBounds.Right
        else
          Result := Bounds.Left - ALayoutItemBounds.Left;
  end;

  function GetTopOffset: Integer;
  var
    ALayoutItemBounds: TRect;
  begin
    Result := 0;
    ALayoutItemBounds := ACellViewInfo.LayoutItemViewInfoBounds;
    if ALayoutItemBounds.Top < Bounds.Top then
      Result := Bounds.Top - ALayoutItemBounds.Top
    else
      if ALayoutItemBounds.Bottom > (Bounds.Top + DataHeight) then
        if DataHeight > cxRectHeight(ALayoutItemBounds) then
          Result := Bounds.Top + DataHeight - ALayoutItemBounds.Bottom
        else
          Result := Bounds.Top - ALayoutItemBounds.Top;
  end;

var
  AOffsetX, AOffsetY: Integer;
begin
  AOffsetX := GetLeftOffset;
  AOffsetY := GetTopOffset;
  if (AOffsetX <> 0) or (AOffsetY <> 0) then
    DoOffset(AOffsetX, AOffsetY);
end;

procedure TcxGridInplaceEditFormAreaViewInfo.MakeItemVisible(AItem: TcxCustomGridTableItem);
var
  ADataCell: TcxGridTableDataCellViewInfo;
begin
  ADataCell := FindCellViewInfo(AItem);
  if ADataCell <> nil then
    MakeDataCellVisible(TcxGridInplaceEditFormDataCellViewInfo(ADataCell));
end;

function TcxGridInplaceEditFormAreaViewInfo.MouseDown(AHitTest: TcxCustomGridHitTest; AButton: TMouseButton;
  AShift: TShiftState): Boolean;
begin
  Result := inherited MouseDown(AHitTest, AButton, AShift);
  if IsPointInLayoutContainer(AHitTest.Pos) then
    ContainerViewInfo.MouseDown(AButton, AShift, AHitTest.Pos.X, AHitTest.Pos.Y);
end;

function TcxGridInplaceEditFormAreaViewInfo.MouseMove(AHitTest: TcxCustomGridHitTest; AShift: TShiftState): Boolean;
begin
  Result := inherited MouseMove(AHitTest, AShift);
  if IsPointInLayoutContainer(AHitTest.Pos) then
    ContainerViewInfo.MouseMove(AShift, AHitTest.Pos.X, AHitTest.Pos.Y);
end;

function TcxGridInplaceEditFormAreaViewInfo.MouseUp(AHitTest: TcxCustomGridHitTest; AButton: TMouseButton;
  AShift: TShiftState): Boolean;
begin
  Result := inherited MouseUp(AHitTest, AButton, AShift);
  if IsPointInLayoutContainer(AHitTest.Pos) then
  begin
    ContainerViewInfo.MouseUp(AButton, AShift, AHitTest.Pos.X, AHitTest.Pos.Y);
    CheckFocusedItem;
  end;
end;

function TcxGridInplaceEditFormAreaViewInfo.CalculateAvailableHeight: Integer;
begin
  Result := cxRectHeight(RecordViewInfo.RecordsViewInfo.ContentBounds);
  if not GridView.OptionsBehavior.NeedHideCurrentRow then
  begin
    Dec(Result, RecordViewInfo.CellHeight);
    if RecordViewInfo.HasPreview then
      Dec(Result, RecordViewInfo.PreviewViewInfo.Height);
  end;
end;

function TcxGridInplaceEditFormAreaViewInfo.CalculateHeight: Integer;
var
  AAvailableHeight: Integer;
  ARequiredHeight: Integer;
begin
  AAvailableHeight := AvailableHeight;
  if ContainerViewInfo.ItemsViewInfo.AlignVert <> avClient then
  begin
    ARequiredHeight := ContainerViewInfo.ContentHeight;
    Inc(ARequiredHeight, ScrollBars.GetHorizontalScrollBarHeight);
    Inc(ARequiredHeight, ButtonsPanelViewInfo.Height);
    Result := Min(ARequiredHeight, AAvailableHeight);
  end
  else
    Result := AAvailableHeight;
  if Result < 0 then Result := 0;
end;

procedure TcxGridInplaceEditFormAreaViewInfo.CalculateLayoutContainerViewInfo;
begin
  ContainerViewInfo.Calculate;
  CheckFocusedItem;
end;

function TcxGridInplaceEditFormAreaViewInfo.CalculateWidth: Integer;
var
  AIndent: Integer;
  AIndicatorWidth: Integer;
begin
  AIndicatorWidth := 0;
  if GridViewInfo.IndicatorViewInfo.Visible then
    AIndicatorWidth := GridView.OptionsView.IndicatorWidth;
  if RecordViewInfo.DataIndent > (GridViewInfo.Bounds.Left + AIndicatorWidth) then
  begin
    AIndent := RecordViewInfo.DataIndent - GridViewInfo.Bounds.Left;
    Dec(AIndent, AIndicatorWidth);
  end
  else
    AIndent := 0;
  Result := GridViewInfo.ClientWidth - AIndent;
  if Result < 0 then Result := 0;
end;

procedure TcxGridInplaceEditFormAreaViewInfo.CheckFocusedItem;
var
  I: Integer;
  AGridItemViewInfo: TcxGridTableViewInplaceEditFormDataCellViewInfo;
begin
  if RecordViewInfo.GridViewInfo.CalculateDown then
    for I := 0 to GridItemViewInfos.Count - 1 do
    begin
      AGridItemViewInfo := TcxGridTableViewInplaceEditFormDataCellViewInfo(GridItemViewInfos.Items[I]);
      if AGridItemViewInfo.Focused then
      begin
        InplaceEditForm.CheckFocusedItem(AGridItemViewInfo);
        Break;
      end;
    end;
end;

function TcxGridInplaceEditFormAreaViewInfo.FocusedItemKind: TcxGridFocusedItemKind;
begin
  Result := RecordViewInfo.FocusedItemKind;
end;

function TcxGridInplaceEditFormAreaViewInfo.GetAvailableHeight: Integer;
begin
  if CacheInfo.IsRestHeightAssigned then
    Result := CacheInfo.RestHeight
  else
  begin
    Result := CalculateAvailableHeight;
    CacheInfo.RestHeight := Result;
  end;
end;

function TcxGridInplaceEditFormAreaViewInfo.GetBoundsForGridItem(AGridItem: TcxCustomGridTableItem): TRect;
var
  AItemViewInfo: TcxGridTableDataCellViewInfo;
begin
  AItemViewInfo := FindCellViewInfo(AGridItem);
  if AItemViewInfo <> nil then
    Result := AItemViewInfo.ContentBounds
  else
    Result := cxEmptyRect;
end;

function TcxGridInplaceEditFormAreaViewInfo.GetDataHeight: Integer;

  function GetHorizontalScrollBarHeight: Integer;
  begin
    if ScrollBars = nil then
      Result := GetScrollBarSize.cx
    else
      Result := ScrollBars.GetHorizontalScrollBarHeight;
  end;

var
  AAvailableHeight: Integer;
  AContentHeight: Integer;
  AHScrollBarHeight: Integer;
  ARequiredHeight: Integer;
begin
  AAvailableHeight := AvailableHeight;
  if GridView.EditForm.DefaultStretch = fsNone then
    AContentHeight := ContainerViewInfo.ContentHeight
  else
    AContentHeight := InplaceEditForm.MinHeight;
  if IsNeedHorizontalScrollBar then
    AHScrollBarHeight := GetHorizontalScrollBarHeight
  else
    AHScrollBarHeight := 0;
  ARequiredHeight := AContentHeight + ButtonsPanelViewInfo.Height + AHScrollBarHeight;
  if (GridView.EditForm.DefaultStretch in [fsVertical, fsClient]) or
    (ARequiredHeight > AAvailableHeight) then
  begin
    Result := AAvailableHeight;
    Dec(Result, ButtonsPanelViewInfo.Height);
    Dec(Result, AHScrollBarHeight);
  end
  else
    Result := AContentHeight;
  if Result < 0 then Result := 0;
end;

function TcxGridInplaceEditFormAreaViewInfo.GetDataWidth: Integer;

  function GetVerticalScrollBarWidth: Integer;
  begin
    if ScrollBars <> nil then
      Result := ScrollBars.GetVerticalScrollBarWidth
    else
      Result := GetScrollBarSize.cy;
  end;

var
  AVScrollBarWidth: Integer;
begin
  Result := Width;
  AVScrollBarWidth := 0;
  if IsNeedVerticalScrollBar then
    AVScrollBarWidth := GetVerticalScrollBarWidth;
  Dec(Result, AVScrollBarWidth);
  if Result < 0 then Result := 0;
end;

function TcxGridInplaceEditFormAreaViewInfo.GetGridItemHitTest(
  const P: TPoint): TcxCustomGridHitTest;
begin
  Result := GridItemViewInfos.GetHitTest(P);
end;

function TcxGridInplaceEditFormAreaViewInfo.GetHeight: Integer;
begin
  if FCacheInfo.IsHeightAssigned then
    Result := FCacheInfo.Height
  else
  begin
    Result := CalculateHeight;
    FCacheInfo.Height := Result;
  end;
end;

function TcxGridInplaceEditFormAreaViewInfo.GetInplaceEditFormClientBounds: TRect;
begin
  Result := Rect(Bounds.Left, Bounds.Top, Bounds.Left + DataWidth, Bounds.Top + DataHeight);
end;

procedure TcxGridInplaceEditFormAreaViewInfo.GetViewParams(var AParams: TcxViewParams);
begin
  AParams.Color := ContainerViewInfo.ItemsViewInfo.Color;
end;

function TcxGridInplaceEditFormAreaViewInfo.GetWidth: Integer;
begin
  Result := CalculateWidth;
end;

function TcxGridInplaceEditFormAreaViewInfo.IsPointInLayoutContainer(const P: TPoint): Boolean;
var
  ARect: TRect;
  ARight, ABottom: Integer;
begin
  ARight := Min(Bounds.Left + DataWidth, ContainerViewInfo.ContentBounds.Right);
  ABottom := Min(Bounds.Top + DataHeight, ContainerViewInfo.ContentBounds.Bottom);
  ARect := Rect(Bounds.Left, Bounds.Top, ARight, ABottom);
  Result := PtInRect(ARect, P);
end;

procedure TcxGridInplaceEditFormAreaViewInfo.Offset(DX, DY: Integer);

  procedure CheckScrollBarsPosition;
  begin
    ScrollBars.CheckPosition;
  end;

begin
  ContainerViewInfo.Offset := Point(ContainerViewInfo.Offset.X + DX,
    ContainerViewInfo.Offset.Y + DY);
  CalculateLayoutContainerViewInfo;
  CheckScrollBarsPosition;
  Invalidate;
end;

procedure TcxGridInplaceEditFormAreaViewInfo.UpdateOnScroll;
begin
  Recalculate;
  InplaceEditForm.ValidateEditVisibility;
  Invalidate;
end;

function TcxGridInplaceEditFormAreaViewInfo.GetGridItemViewInfoClass: TcxGridTableDataCellViewInfoClass;
begin
  Result := TcxGridTableViewInplaceEditFormDataCellViewInfo;
end;

function TcxGridInplaceEditFormAreaViewInfo.GetHitTestClass: TcxCustomGridHitTestClass;
begin
  Result := TcxGridInplaceEditFormAreaHitTest;
end;

function TcxGridInplaceEditFormAreaViewInfo.GetButtonsPanelViewInfoClass: TcxGridInplaceEditFormButtonsPanelViewInfoClass;
begin
  Result := TcxGridInplaceEditFormButtonsPanelViewInfo;
end;

function TcxGridInplaceEditFormAreaViewInfo.GetScrollBarsClass: TcxGridInplaceEditFormAreaScrollBarsClass;
begin
  Result := TcxGridInplaceEditFormAreaScrollBars;
end;

function TcxGridInplaceEditFormAreaViewInfo.GetPainterClass: TcxCustomGridCellPainterClass;
begin
  Result := TcxGridInplaceEditFormAreaPainter;
end;

procedure TcxGridInplaceEditFormAreaViewInfo.CreateButtonsPanel;
begin
  FButtonsPanelViewInfo := GetButtonsPanelViewInfoClass.Create(Self);
end;

procedure TcxGridInplaceEditFormAreaViewInfo.CreateGridItemViewInfos;
var
  I: Integer;
  AGridItemViewInfo: TcxGridTableDataCellViewInfo;
  AGridViewItem: TcxCustomGridTableItem;
begin
  FGridItemViewInfos := TcxGridInplaceEditFormContainerGridItemViewInfos.Create;
  for I := 0 to InplaceEditForm.Container.AbsoluteItemCount - 1 do
  begin
    if not (InplaceEditForm.Container.AbsoluteItems[I] is TcxGridInplaceEditFormLayoutItem) or
      (InplaceEditForm.Container.AbsoluteItems[I].Parent = nil) then
      Continue;
    AGridViewItem := TcxGridInplaceEditFormLayoutItem(InplaceEditForm.Container.AbsoluteItems[I]).GridViewItem;
    AGridItemViewInfo := GetGridItemViewInfoClass.Create(RecordViewInfo, AGridViewItem);
    FGridItemViewInfos.Add(AGridItemViewInfo);
  end;
end;

procedure TcxGridInplaceEditFormAreaViewInfo.CreateScrollBars;
begin
  FScrollBars := GetScrollBarsClass.Create(Self);
end;

procedure TcxGridInplaceEditFormAreaViewInfo.DestroyButtonsPanel;
begin
  FreeAndNil(FButtonsPanelViewInfo);
end;

procedure TcxGridInplaceEditFormAreaViewInfo.DestroyGridItemViewInfos;
begin
  FreeAndNil(FGridItemViewInfos);
end;

procedure TcxGridInplaceEditFormAreaViewInfo.DestroyScrollBars;
begin
  FreeAndNil(FScrollBars);
end;

function TcxGridInplaceEditFormAreaViewInfo.GetContainerViewInfo: TcxGridInplaceEditFormContainerViewInfo;
begin
  Result := InplaceEditForm.Container.ViewInfo;
end;

function TcxGridInplaceEditFormAreaViewInfo.GetGridRecord: TcxGridDataRow;
begin
  Result := RecordViewInfo.GridRecord;
end;

function TcxGridInplaceEditFormAreaViewInfo.GetGridView: TcxGridTableView;
begin
  Result := TcxGridTableView(inherited GridView);
end;

function TcxGridInplaceEditFormAreaViewInfo.GetGridViewInfo: TcxGridTableViewInfo;
begin
  Result := TcxGridTableViewInfo(inherited GridViewInfo);
end;

function TcxGridInplaceEditFormAreaViewInfo.GetInplaceEditForm: TcxGridTableViewInplaceEditForm;
begin
  Result := RecordViewInfo.InplaceEditForm;
end;

function TcxGridInplaceEditFormAreaViewInfo.GetScrollBarOwner: TWinControl;
begin
  Result := Control;
end;

function TcxGridInplaceEditFormAreaViewInfo.GetLookAndFeel: TcxLookAndFeel;
begin
  Result := GridView.LookAndFeel;
end;

{ TcxGridDataRowViewInfo }

constructor TcxGridDataRowViewInfo.Create(ARecordsViewInfo: TcxCustomGridRecordsViewInfo;
  ARecord: TcxCustomGridRecord);
begin
  inherited;
  CreateViewInfos;
end;

destructor TcxGridDataRowViewInfo.Destroy;
begin
  DestroyViewInfos;
  inherited;
end;

function TcxGridDataRowViewInfo.FindCellViewInfoInTableRow(AItem: TcxCustomGridTableItem): TcxGridTableDataCellViewInfo;
begin
  if AItem.VisibleIndex = -1 then
    Result := nil
  else
    if GridView.IsControlLocked and not (GridViewInfo.IsCalculating or GridView.InDataControlFocusing) then
      Result := nil
    else
      Result := InternalCellViewInfos[AItem.VisibleIndex];
end;

function TcxGridDataRowViewInfo.FindCellViewInfoOnInplaceEditForm(
  AItem: TcxCustomGridTableItem): TcxGridTableDataCellViewInfo;
begin
  Result := InplaceEditFormAreaViewInfo.FindCellViewInfo(AItem);
end;

function TcxGridDataRowViewInfo.GetCellViewInfo(Index: Integer): TcxGridDataCellViewInfo;
begin
  Result := InternalCellViewInfos[Index];
  if Result = nil then
  begin
    Result := GetCellViewInfoClass(Index).Create(Self, GridView.VisibleColumns[Index]);
    FCellViewInfos[Index] := Result;
  end;
end;

function TcxGridDataRowViewInfo.GetCellViewInfoCount: Integer;
begin
  Result := FCellViewInfos.Count;
end;

function TcxGridDataRowViewInfo.GetGridRecord: TcxGridDataRow;
begin
  Result := TcxGridDataRow(inherited GridRecord);
end;

function TcxGridDataRowViewInfo.GetHasInplaceEditFormArea: Boolean;
begin
  Result := FInplaceEditFormAreaViewInfo <> nil; 
end;

function TcxGridDataRowViewInfo.GetHasPreview: Boolean;
begin
  Result := FPreviewViewInfo <> nil;
end;

function TcxGridDataRowViewInfo.GetInplaceEditForm: TcxGridTableViewInplaceEditForm;
begin
  Result := TcxGridTableViewAccess.GetInplaceEditForm(GridView);
end;

function TcxGridDataRowViewInfo.GetInternalCellViewInfo(Index: Integer): TcxGridDataCellViewInfo;
begin
  if (Index < 0) or (Index >= FCellViewInfos.Count) then
    Result := nil
  else
    Result := TcxGridDataCellViewInfo(FCellViewInfos[Index]);
end;

procedure TcxGridDataRowViewInfo.CreateViewInfos;
begin
  FCellViewInfos := TList.Create;
  FCellViewInfos.Count := GridViewInfo.HeaderViewInfo.Count;
  FCellsAreaViewInfo := GetCellsAreaViewInfoClass.Create(Self);
  if ShowPreview then
    FPreviewViewInfo := GetPreviewViewInfoClass.Create(Self, GridView.Preview.Column);
  if ShowInplaceEditFormArea then
  begin
    FInplaceEditFormAreaViewInfo := GetInplaceEditFormAreaViewInfoClass.Create(GridViewInfo, Self);
    CacheItem.IsHeightAssigned := False;
  end;
end;

procedure TcxGridDataRowViewInfo.DestroyViewInfos;
var
  I: Integer;
begin
  FreeAndNil(FInplaceEditFormAreaViewInfo);
  FPreviewViewInfo.Free;
  FCellsAreaViewInfo.Free;
  for I := 0 to CellViewInfoCount - 1 do
    InternalCellViewInfos[I].Free;
  FCellViewInfos.Free;
end;

procedure TcxGridDataRowViewInfo.AfterRowsViewInfoCalculate;
var
  I: Integer;
  ACellViewInfo: TcxGridDataCellViewInfo;
begin
  inherited;
  for I := 0 to CellViewInfoCount - 1 do
  begin
    ACellViewInfo := InternalCellViewInfos[I];
    if (ACellViewInfo <> nil) and ACellViewInfo.Calculated then
      ACellViewInfo.AfterRowsViewInfoCalculate;
  end;
end;

procedure TcxGridDataRowViewInfo.CalculateExpandButtonBounds(var ABounds: TRect);
begin
  if GridView.IsInplaceEditFormMode and
    not (not GridRecord.IsData or GridView.IsMaster) then
    ABounds := cxEmptyRect
  else
    inherited CalculateExpandButtonBounds(ABounds);
end;


function TcxGridDataRowViewInfo.AdjustToIntegralBottomBound(var ABound: Integer): Boolean;
var
  ABottomBound: Integer;
begin
  if HasPreview then
  begin
    if PreviewViewInfo.Preview.Place = ppTop then
    begin
      ABottomBound := PreviewViewInfo.ContentBounds.Bottom;
      Result := ABottomBound >= ABound;
      if not Result then
        ABottomBound := CellsAreaBounds.Bottom - 1;
      Result := ABottomBound >= ABound;
    end
    else
    begin
      ABottomBound := CellsAreaBounds.Bottom - 1;
      Result := ABottomBound >= ABound;
      if not Result then
        ABottomBound := PreviewViewInfo.ContentBounds.Bottom;
      Result := ABottomBound >= ABound;
    end;
    if Result then
      ABound := ABottomBound;
  end
  else
    Result := inherited AdjustToIntegralBottomBound(ABound);
end;

procedure TcxGridDataRowViewInfo.AfterRowsViewInfoOffset;
var
  I: Integer;
  ACellViewInfo: TcxGridDataCellViewInfo;
begin
  inherited;
  for I := 0 to CellViewInfoCount - 1 do
  begin
    ACellViewInfo := InternalCellViewInfos[I];
    if ACellViewInfo <> nil then
      ACellViewInfo.AfterRowsViewInfoOffset;
  end;
end;

procedure TcxGridDataRowViewInfo.ApplyMergedCellsBounds(var R: TRect;
  AItem: TcxCustomGridTableItem);
var
  I: Integer;

  procedure ProcessCell(ACellViewInfo: TcxGridDataCellViewInfo);
  begin
    if (ACellViewInfo <> nil) and ACellViewInfo.IsMerged then
      with ACellViewInfo.MergingCell do
      begin
        if Bounds.Top < R.Top then
          R.Top := Bounds.Top;
        if Bounds.Bottom > R.Bottom then
          R.Bottom := Bounds.Bottom;
      end;
  end;

begin
  if AItem = nil then
    for I := 0 to CellViewInfoCount - 1 do
      ProcessCell(InternalCellViewInfos[I])
  else
    ProcessCell(InternalCellViewInfos[AItem.VisibleIndex]);
end;

procedure TcxGridDataRowViewInfo.ApplyMergingCellsBounds(var R: TRect);
var
  I: Integer;
  ACellViewInfo: TcxGridDataCellViewInfo;
begin
  for I := 0 to CellViewInfoCount - 1 do
  begin
    ACellViewInfo := InternalCellViewInfos[I];
    if (ACellViewInfo <> nil) and
      ACellViewInfo.IsMerging and (ACellViewInfo.Bounds.Bottom > R.Bottom) then
      R.Bottom := ACellViewInfo.Bounds.Bottom;
  end;
end;

procedure TcxGridDataRowViewInfo.CalculateCellViewInfo(AIndex: Integer);
begin
  CellViewInfos[AIndex].Calculate(GetCellLeftBound(AIndex), GetCellTopBound(AIndex),
    -1, GetCellHeight(AIndex));
end;

procedure TcxGridDataRowViewInfo.CalculateInplaceEditForm;
var
  ATopPos, ALeftPos: Integer;
begin
  ALeftPos := GetInplaceEditFormLeftPosition;
  ATopPos := GetInplaceEditFormTopPosition;
  InplaceEditFormAreaViewInfo.Calculate(ALeftPos, ATopPos);
end;

procedure TcxGridDataRowViewInfo.CalculateTableRowEmptyAreaBounds;
begin
  FEmptyAreaBounds := Rect(GetTableRowEmptyAreaLeft, CellsAreaBounds.Top,
    InplaceEditFormAreaViewInfo.Bounds.Right, InplaceEditFormAreaViewInfo.Bounds.Top);
end;

function TcxGridDataRowViewInfo.CalculateMultilineEditMinHeight: Integer;
begin
  Result := CalculateDataAutoHeight(False);
  Dec(Result, RecordsViewInfo.GetCellHeight(Result) - Result);
  Dec(Result, 2 * cxGridEditOffset);
end;

function TcxGridDataRowViewInfo.CalculateDataAutoHeight(ACheckEditingCell: Boolean = True): Integer;
var
  I, AHeight: Integer;
  ADataCellViewInfo: TcxGridDataCellViewInfo;
begin
  Result := 0;
  for I := 0 to CellViewInfoCount - 1 do
  begin
    ADataCellViewInfo := CellViewInfos[I];
    if not ACheckEditingCell and (ADataCellViewInfo.Item = GridView.Controller.EditingItem) then
      Continue;
    AHeight := ADataCellViewInfo.CalculateHeight;
    if AHeight > Result then Result := AHeight;
  end;
  Result := RecordsViewInfo.GetCellHeight(Result);
  if not TcxCustomGridTableViewAccess.IsGetCellHeightAssigned(GridView) then
    with RecordsViewInfo do
      if Result < DataRowHeight then Result := DataRowHeight;
end;

function TcxGridDataRowViewInfo.CalculateHeight: Integer;
begin
  if not IsNeedHideTableRowCells then
  begin
    if AutoHeight then
      Result := CalculateDataAutoHeight
    else
      Result := RecordsViewInfo.DataRowHeight;
    if HasPreview then
      Inc(Result, PreviewViewInfo.Height);
  end
  else
    Result := 0;
  if HasInplaceEditFormArea then
    Inc(Result, InplaceEditFormAreaViewInfo.Height);
  Inc(Result, inherited CalculateHeight);
end;

function TcxGridDataRowViewInfo.CanDelayedDrawDataCellBorders: Boolean;
begin
  Result := True;
end;

function TcxGridDataRowViewInfo.CanSize: Boolean;
begin
  Result := RecordsViewInfo.CanDataRowSize;
end;

procedure TcxGridDataRowViewInfo.CheckRowHeight(var AValue: Integer);
begin
  Dec(AValue, NonBaseHeight);
  inherited;
  GridView.OptionsView.CheckDataRowHeight(AValue);
  Inc(AValue, NonBaseHeight);
end;

procedure TcxGridDataRowViewInfo.DoToggleExpanded;
begin
  if ((GridView.MasterRowDblClickAction = dcaSwitchExpandedState) and GridView.IsMaster)
    or not GridRecord.IsData then
    inherited DoToggleExpanded
  else
    GridRecord.ToggleEditFormVisibility;
end;

function TcxGridDataRowViewInfo.FindCellViewInfo(AItem: TcxCustomGridTableItem): TcxGridTableDataCellViewInfo;
begin
  if HasInplaceEditFormArea then
    Result := FindCellViewInfoOnInplaceEditForm(AItem)
  else
    Result := FindCellViewInfoInTableRow(AItem);
end;

function TcxGridDataRowViewInfo.GetAutoHeight: Boolean;
begin
  Result := RecordsViewInfo.AutoDataRecordHeight and not ShowInplaceEditFormArea;
end;

function TcxGridDataRowViewInfo.GetBackgroundBitmapBounds: TRect;
begin
  Result := inherited GetBackgroundBitmapBounds;
  if HasPreview and (FPreviewViewInfo.BackgroundBitmap <> nil) then
    with CellsAreaBounds do
    begin
      Result.Top := Top;
      Result.Bottom := Bottom;
    end;
end;

function TcxGridDataRowViewInfo.GetBaseHeight: Integer;
begin
  Result := inherited GetBaseHeight;
  if not IsNeedHideTableRowCells and HasPreview then
    Dec(Result, PreviewViewInfo.Height);
  if HasInplaceEditFormArea then
    Dec(Result, InplaceEditFormAreaViewInfo.Height);
end;

function TcxGridDataRowViewInfo.GetInplaceEditFormLeftPosition: Integer;
var
  AIndicatorWidth: Integer;
begin
  AIndicatorWidth := 0;
  if GridViewInfo.IndicatorViewInfo.Visible then
    AIndicatorWidth := GridView.OptionsView.IndicatorWidth;
  if (DataIndent - AIndicatorWidth) < GridViewInfo.Bounds.Left then
  begin
    Result := GridViewInfo.Bounds.Left;
    Inc(Result, AIndicatorWidth);
  end
  else
    Result := DataIndent;
end;

function TcxGridDataRowViewInfo.GetInplaceEditFormTopPosition: Integer;
begin
  if IsNeedHideTableRowCells then
    Result := Bounds.Top
  else
    if HasPreview and (PreviewViewInfo.Preview.Place = ppBottom) then
      Result := PreviewViewInfo.Bounds.Bottom
    else
      Result := FCellsAreaBounds.Bottom;
end;

function TcxGridDataRowViewInfo.GetPainterClass: TcxCustomGridCellPainterClass;
begin
  Result := TcxGridDataRowPainter;
end;

procedure TcxGridDataRowViewInfo.GetViewParams(var AParams: TcxViewParams);
begin
  GridView.Styles.GetViewParams(0, nil, nil, AParams);
end;

function TcxGridDataRowViewInfo.IsNeedHideTableRowCells: Boolean;
begin
  Result := HasInplaceEditFormArea and GridView.OptionsBehavior.NeedHideCurrentRow;
end;

function TcxGridDataRowViewInfo.NeedToggleExpandRecord(AHitTest: TcxCustomGridHitTest;
  AButton: TMouseButton; AShift: TShiftState): Boolean;
begin
  Result := inherited NeedToggleExpandRecord(AHitTest, AButton, AShift);
  if Result and HasInplaceEditFormArea then
    Result := not  (AHitTest.ViewInfo is TcxGridInplaceEditFormDataCellViewInfo);
end;

procedure TcxGridDataRowViewInfo.Offset(DX, DY: Integer);

  procedure OffsetCells;
  var
    I: Integer;
    ACellViewInfo: TcxGridDataCellViewInfo;
  begin
    for I := 0 to CellViewInfoCount - 1 do
    begin
      ACellViewInfo := InternalCellViewInfos[I];
      if IsCellVisible(I) then
        if (ACellViewInfo = nil) or not ACellViewInfo.Calculated then
          CalculateCellViewInfo(I)
        else
          ACellViewInfo.DoOffset(DX, DY)
      else
        if (DX <> 0) and (ACellViewInfo <> nil) then
        begin
          ACellViewInfo.Free;
          FCellViewInfos[I] := nil;
        end;
    end;
  end;

begin
  inherited;
  OffsetRect(FCellsAreaBounds, DX, DY);
  OffsetCells;
  FCellsAreaViewInfo.DoOffset(DX, DY);
  if FPreviewViewInfo <> nil then
    FPreviewViewInfo.DoOffset(DX, DY);
  if HasInplaceEditFormArea then
  begin
    CalculateInplaceEditForm;
    CalculateTableRowEmptyAreaBounds;
  end;
end;

procedure TcxGridDataRowViewInfo.SetRowHeight(Value: Integer);
begin
  if RowHeight <> Value then
    GridView.OptionsView.DataRowHeight := Value - NonBaseHeight;
end;

function TcxGridDataRowViewInfo.GetCellHeight(AIndex: Integer): Integer;
begin
  if CellViewInfos[AIndex].IsMerging then
    Result := InternalCellViewInfos[AIndex].Height
  else
    Result := GridViewInfo.GetCellHeight(AIndex, FCellHeight);
end;

function TcxGridDataRowViewInfo.GetCellHeightValue: Integer;
begin
  if AutoHeight then
  begin
    Result := DataHeight;
    if HasPreview then
      Dec(Result, PreviewViewInfo.Height);
    if HasInplaceEditFormArea then
      Dec(Result, InplaceEditFormAreaViewInfo.Height);
  end
  else
    Result := RecordsViewInfo.RowHeight;
end;

function TcxGridDataRowViewInfo.GetCellLeftBound(AIndex: Integer): Integer;
begin
  Result := GridViewInfo.HeaderViewInfo[AIndex].DataOffset;
end;

function TcxGridDataRowViewInfo.GetCellTopBound(AIndex: Integer): Integer;
begin
  Result := FCellsAreaBounds.Top + GridViewInfo.GetCellTopOffset(AIndex, FCellHeight);
end;

function TcxGridDataRowViewInfo.GetCellsAreaBounds: TRect;
begin
  with Result do
  begin
    Left := DataIndent;
    Right := Left + DataWidth;
    Top := Bounds.Top;
    Bottom := Top + DataHeight;
    if HasPreview then
      if PreviewViewInfo.Preview.Place = ppTop then
        Inc(Top, PreviewViewInfo.Height)
      else
        Dec(Bottom, PreviewViewInfo.Height);
    if HasInplaceEditFormArea then
      Dec(Bottom, InplaceEditFormAreaViewInfo.Height);
  end;
end;

function TcxGridDataRowViewInfo.GetCellsAreaViewInfoClass: TcxGridDataRowCellsAreaViewInfoClass;
begin
  Result := TcxGridDataRowCellsAreaViewInfoClass(RecordsViewInfo.GetDataRowCellsAreaViewInfoClass);
end;

function TcxGridDataRowViewInfo.GetCellViewInfoClass(AIndex: Integer): TcxGridDataCellViewInfoClass;
begin
  Result := TcxGridDataCellViewInfo;
end;

function TcxGridDataRowViewInfo.GetCellWidth(AIndex: Integer): Integer;
begin
  Result := GridViewInfo.HeaderViewInfo[AIndex].RealWidth;
end;

function TcxGridDataRowViewInfo.GetDataWidth: Integer;
begin
  if HasInplaceEditFormArea then
    Result := RecordsViewInfo.RowWidth
  else
    Result := inherited GetDataWidth;
end;

function TcxGridDataRowViewInfo.GetInplaceEditFormAreaViewInfoClass: TcxGridInplaceEditFormAreaViewInfoClass;
begin
  Result := TcxGridInplaceEditFormAreaViewInfo;
end;

function TcxGridDataRowViewInfo.GetPreviewViewInfoClass: TcxGridPreviewCellViewInfoClass;
begin
  Result := TcxGridPreviewCellViewInfo;
end;

function TcxGridDataRowViewInfo.GetShowInplaceEditFormContainer: Boolean;
begin
  Result := GridRecord.EditFormVisible;
end;

function TcxGridDataRowViewInfo.GetShowPreview: Boolean;
begin
  Result := GridView.Preview.Active;
end;

function TcxGridDataRowViewInfo.GetTableRowEmptyAreaLeft: Integer;
begin
  Result := Bounds.Left + DataWidth;
end;

function TcxGridDataRowViewInfo.GetWidth: Integer;
begin
  if HasInplaceEditFormArea then
    Result := Max(GridViewInfo.Bounds.Right - Bounds.Left, RecordsViewInfo.RowWidth)
  else
    Result := inherited GetWidth;
end;

function TcxGridDataRowViewInfo.IsCellVisible(AIndex: Integer): Boolean;
begin
  Result := GridViewInfo.HeaderViewInfo[AIndex].Visible;
end;

function TcxGridDataRowViewInfo.HasFocusRect: Boolean;
begin
  if HasInplaceEditFormArea then
    Result := False
  else
    Result := inherited HasFocusRect;
end;

procedure TcxGridDataRowViewInfo.BeforeCellRecalculation(ACell: TcxGridTableCellViewInfo);
begin
//do nothing
end;

procedure TcxGridDataRowViewInfo.BeforeRecalculation;
var
  I: Integer;
begin
  inherited;
  FCellsAreaViewInfo.BeforeRecalculation;
  for I := 0 to CellViewInfoCount - 1 do
    if InternalCellViewInfos[I] <> nil then
      InternalCellViewInfos[I].BeforeRecalculation;
  if FPreviewViewInfo <> nil then
    FPreviewViewInfo.BeforeRecalculation;
  if HasInplaceEditFormArea then
    FInplaceEditFormAreaViewInfo.BeforeRecalculation;
end;

procedure TcxGridDataRowViewInfo.Calculate(ALeftBound, ATopBound: Integer;
  AWidth: Integer = -1; AHeight: Integer = -1);

  procedure CalculatePreview;

    function GetTopBound: Integer;
    begin
      if PreviewViewInfo.Preview.Place = ppTop then
        Result := ATopBound
      else
        Result := FCellsAreaBounds.Bottom;
    end;

  begin
    PreviewViewInfo.Calculate(FCellsAreaBounds.Left, GetTopBound);
  end;

var
  I: Integer;
begin
  inherited;
  if not IsNeedHideTableRowCells then
    FCellsAreaBounds := CellsAreaBounds;
  if HasPreview and not IsNeedHideTableRowCells then
    CalculatePreview;
  if HasInplaceEditFormArea then
    CalculateInplaceEditForm;
  FCellHeight := CellHeight;
  if not IsNeedHideTableRowCells then
    for I := 0 to CellViewInfoCount - 1 do
      if IsCellVisible(I) then CalculateCellViewInfo(I);
  if FCellsAreaViewInfo.Visible and not IsNeedHideTableRowCells then
    FCellsAreaViewInfo.Calculate(FCellsAreaBounds);
  if HasInplaceEditFormArea and not IsNeedHideTableRowCells then
    CalculateTableRowEmptyAreaBounds
  else
    FEmptyAreaBounds := cxEmptyRect;
end;

function TcxGridDataRowViewInfo.GetAreaBoundsForPainting: TRect;
begin
  Result := inherited GetAreaBoundsForPainting;
  ApplyMergingCellsBounds(Result);
end;

function TcxGridDataRowViewInfo.GetBoundsForInvalidate(AItem: TcxCustomGridTableItem): TRect;
begin
  Result := inherited GetBoundsForInvalidate(AItem);
  if AItem = nil then
    ApplyMergingCellsBounds(Result);
  ApplyMergedCellsBounds(Result, AItem);
end;

function TcxGridDataRowViewInfo.GetBoundsForItem(AItem: TcxCustomGridTableItem): TRect;
begin
  if HasInplaceEditFormArea then
    Result := InplaceEditFormAreaViewInfo.GetBoundsForGridItem(AItem)
  else
    if (InternalCellViewInfos[AItem.VisibleIndex] = nil) then
      Result := inherited GetBoundsForItem(AItem)
    else
      Result := InternalCellViewInfos[AItem.VisibleIndex].Bounds;
end;

function TcxGridDataRowViewInfo.GetCellBorders(AIsRight, AIsBottom: Boolean): TcxBorders;
begin
  Result := GridViewInfo.GetCellBorders(AIsRight, AIsBottom);
end;

function TcxGridDataRowViewInfo.GetCellViewInfoByItem(AItem: TcxCustomGridTableItem): TcxGridTableDataCellViewInfo;
begin
  if TcxGridColumn(AItem).IsPreview then
    Result := FPreviewViewInfo
  else
    Result := FindCellViewInfo(AItem);
end;

function TcxGridDataRowViewInfo.GetHitTest(const P: TPoint): TcxCustomGridHitTest;
var
  I: Integer;
  ACellViewInfo: TcxGridDataCellViewInfo;
  AHitTest: TcxCustomGridHitTest;
begin
  Result := inherited GetHitTest(P);
  if (Result <> nil) and (Result.ClassType = GetHitTestClass) then
  begin
    for I := 0 to CellViewInfoCount - 1 do
    begin
      ACellViewInfo := InternalCellViewInfos[I];
      if ACellViewInfo <> nil then
      begin
        AHitTest := ACellViewInfo.GetHitTest(P);
        if AHitTest <> nil then
        begin
          Result := AHitTest;
          Exit;
        end;
      end;
    end;
    if HasPreview then
    begin
      AHitTest := FPreviewViewInfo.GetHitTest(P);
      if AHitTest <> nil then
      begin
        Result := AHitTest;
        Exit;
      end;
    end;
    if HasInplaceEditFormArea then
    begin
      AHitTest := InplaceEditFormAreaViewInfo.GetHitTest(P);
      if AHitTest <> nil then
      begin
        Result := AHitTest;
        Exit;
      end;
    end;
  end;
end;

function TcxGridDataRowViewInfo.GetInplaceEditFormClientBounds: TRect;
begin
  if HasInplaceEditFormArea then
    Result := InplaceEditFormAreaViewInfo.GetInplaceEditFormClientBounds
  else
    Result := cxEmptyRect;
end;

procedure TcxGridDataRowViewInfo.CancelInplaceEditFormEditing;
begin
  InplaceEditForm.CancelExecute;
end;

procedure TcxGridDataRowViewInfo.UpdateInplaceEditFormEditing;
begin
  InplaceEditForm.UpdateExecute;
end;

{ TcxGridNewItemRowViewInfo }

function TcxGridNewItemRowViewInfo.CalculateSelected: Boolean;
begin
  Result := GridRecord.Selected;
end;

function TcxGridNewItemRowViewInfo.CanDelayedDrawDataCellBorders: Boolean;
begin
  Result := False;
end;

function TcxGridNewItemRowViewInfo.CanShowDataCellHint: Boolean;
begin
  Result := inherited CanShowDataCellHint and Focused;
end;

function TcxGridNewItemRowViewInfo.GetAlignmentHorz: TAlignment;
begin
  Result := taCenter;
end;

function TcxGridNewItemRowViewInfo.GetAlignmentVert: TcxAlignmentVert;
begin
  Result := vaCenter;
end;

function TcxGridNewItemRowViewInfo.GetAutoHeight: Boolean;
begin
  Result := False;
end;

function TcxGridNewItemRowViewInfo.GetCellLeftBound(AIndex: Integer): Integer;
begin
  Result := GridViewInfo.HeaderViewInfo[AIndex].RealBounds.Left;
end;

function TcxGridNewItemRowViewInfo.GetCellWidth(AIndex: Integer): Integer;
begin
  Result := GridViewInfo.HeaderViewInfo[AIndex].Width;
end;

function TcxGridNewItemRowViewInfo.GetHeight: Integer;
begin
  if FHeight = 0 then
    FHeight := CalculateHeight;
  Result := FHeight;
end;

function TcxGridNewItemRowViewInfo.GetInfoText: string;
begin
  Result := Options.InfoText;
end;

function TcxGridNewItemRowViewInfo.GetOptions: TcxGridSpecialRowOptions;
begin
  Result := GridView.NewItemRow;
end;

function TcxGridNewItemRowViewInfo.GetSeparatorColor: TColor;
begin
  Result := Options.GetSeparatorColor;
end;

function TcxGridNewItemRowViewInfo.GetSeparatorWidth: Integer;
begin
  Result := Options.SeparatorWidth;
end;

function TcxGridNewItemRowViewInfo.GetShowInfoText: Boolean;
begin
  Result := not Focused;
end;

function TcxGridNewItemRowViewInfo.GetShowPreview: Boolean;
begin
  Result := False;
end;

function TcxGridNewItemRowViewInfo.GetStyleIndex: Integer;
begin
  Result := vsNewItemRowInfoText;
end;

function TcxGridNewItemRowViewInfo.GetText: string;
begin
  if ShowInfoText then
    Result := InfoText
  else
    Result := inherited GetText;
end;

function TcxGridNewItemRowViewInfo.GetTextAreaBounds: TRect;
begin
  Result := ContentBounds;
  with GridViewInfo.ClientBounds do
  begin
    Result.Left := Left;
    if Result.Right > Right then
      Result.Right := Right;
  end;
end;

procedure TcxGridNewItemRowViewInfo.GetViewParams(var AParams: TcxViewParams);
begin
  GridView.Styles.GetViewParams(GetStyleIndex, GridRecord, nil, AParams);
end;

function TcxGridNewItemRowViewInfo.HasFooters: Boolean;
begin
  Result := False;
end;

function TcxGridNewItemRowViewInfo.HasLastHorzGridLine: Boolean;
begin
  Result := False;
end;

function TcxGridNewItemRowViewInfo.GetPainterClass: TcxCustomGridCellPainterClass;
begin
  Result := TcxGridNewItemRowPainter;
end;

{ TcxGridFilterRowViewInfo }

function TcxGridFilterRowViewInfo.GetGridRecord: TcxGridFilterRow;
begin
  Result := TcxGridFilterRow(inherited GridRecord);
end;

function TcxGridFilterRowViewInfo.GetOptions: TcxGridSpecialRowOptions;
begin
  Result := GridView.FilterRow;
end;

function TcxGridFilterRowViewInfo.GetShowInfoText: Boolean;
begin
  Result := inherited GetShowInfoText and GridRecord.IsEmpty;
end;

function TcxGridFilterRowViewInfo.GetShowInplaceEditFormContainer: Boolean;
begin
  Result := False;
end;

function TcxGridFilterRowViewInfo.GetStyleIndex: Integer;
begin
  Result := vsFilterRowInfoText;
end;

{ TcxGridMasterDataRowDetailsSiteLeftTabsViewInfo }

function TcxGridMasterDataRowDetailsSiteLeftTabsViewInfo.GetBottomGridLineColor: TColor;
begin
  Result := SiteViewInfo.MasterGridViewInfo.GridLineColor;
end;

function TcxGridMasterDataRowDetailsSiteLeftTabsViewInfo.GetBottomGridLineWidth: Integer;
begin
  Result := SiteViewInfo.MasterGridViewInfo.GridLineWidth;
end;

function TcxGridMasterDataRowDetailsSiteLeftTabsViewInfo.GetSiteViewInfo: TcxGridDetailsSiteViewInfo;
begin
  Result := TcxGridDetailsSiteViewInfo(inherited SiteViewInfo);
end;

function TcxGridMasterDataRowDetailsSiteLeftTabsViewInfo.CalculateHeight: Integer;
begin
  Result := inherited CalculateHeight;
  if HasBottomGridLine then
    Inc(Result, BottomGridLineWidth);
end;

function TcxGridMasterDataRowDetailsSiteLeftTabsViewInfo.GetBoundsRect: TRect;
begin
  Result := inherited GetBoundsRect;
  if HasBottomGridLine then
    Dec(Result.Bottom, BottomGridLineWidth);
end;

function TcxGridMasterDataRowDetailsSiteLeftTabsViewInfo.GetBottomGridLineBounds: TRect;
begin
  Result := Bounds;
  Result.Top := Result.Bottom - BottomGridLineWidth;
end;

function TcxGridMasterDataRowDetailsSiteLeftTabsViewInfo.GetPainterClass: TcxCustomGridCellPainterClass;
begin
  Result := TcxGridMasterDataRowDetailsSiteTabsPainter;
end;

function TcxGridMasterDataRowDetailsSiteLeftTabsViewInfo.HasBottomGridLine: Boolean;
begin
  Result := bBottom in SiteViewInfo.MasterGridViewInfo.GetCellBorders(False, True);
end;

{ TcxGridDetailsSiteViewInfo }

constructor TcxGridDetailsSiteViewInfo.Create(AMasterDataRowViewInfo: TcxGridMasterDataRowViewInfo);
begin
  FMasterDataRowViewInfo := AMasterDataRowViewInfo;
  inherited Create(TcxGridLevel(FMasterDataRowViewInfo.GridView.Level));
  if CacheItem.IsDetailsSiteCachedInfoAssigned then
    SetCachedInfo(CacheItem.DetailsSiteCachedInfo);
end;

destructor TcxGridDetailsSiteViewInfo.Destroy;
begin
  if not MasterGridViewInfo.IsInternalUse and (CacheItem <> nil) then
  begin
    if CacheItem.IsDetailsSiteCachedInfoAssigned then
      raise EdxException.Create('CacheItem.IsDetailsSiteCachedInfoAssigned');  //!!!
    GetCachedInfo(CacheItem.DetailsSiteCachedInfo);
  end;
  inherited;
end;

function TcxGridDetailsSiteViewInfo.GetCacheItem: TcxGridMasterTableViewInfoCacheItem;
begin
  Result := FMasterDataRowViewInfo.CacheItem;
end;

function TcxGridDetailsSiteViewInfo.GetMasterGridView: TcxGridTableView;
begin
  Result := FMasterDataRowViewInfo.GridView;
end;

function TcxGridDetailsSiteViewInfo.GetMasterGridViewInfo: TcxGridTableViewInfo;
begin
  Result := FMasterDataRowViewInfo.GridViewInfo;
end;

procedure TcxGridDetailsSiteViewInfo.ControlFocusChanged;
begin
  if ActiveGridViewExists then
    ActiveGridView.Controller.ControlFocusChanged;
end;

function TcxGridDetailsSiteViewInfo.GetActiveGridView: TcxCustomGridView;
begin
  Result := FMasterDataRowViewInfo.GridRecord.ActiveDetailGridView;
  if Result <> nil then
    Result.CheckSynchronizationAssignNeeded;
end;

function TcxGridDetailsSiteViewInfo.GetActiveGridViewExists: Boolean;
var
  AMasterGridRecord: TcxGridMasterDataRow;
begin
  Result := inherited GetActiveGridViewExists;
  if not Result and not IsActiveGridViewDestroying and not (csDestroying in ComponentState) then
  begin
    AMasterGridRecord := FMasterDataRowViewInfo.GridRecord;
    Result :=
      (AMasterGridRecord <> nil) and AMasterGridRecord.ActiveDetailGridViewExists and
      not AMasterGridRecord.ActiveDetailGridView.IsDestroying;
  end;
end;

function TcxGridDetailsSiteViewInfo.GetActiveGridViewValue: TcxCustomGridView;
begin
  Result := inherited GetActiveGridViewValue;
  if (Result <> nil) and (Result.ViewInfo <> nil) then
    Result.ViewInfo.IsInternalUse := MasterGridViewInfo.IsInternalUse;
end;

function TcxGridDetailsSiteViewInfo.GetActiveLevel: TcxGridLevel;
begin
  Result := FMasterDataRowViewInfo.GridRecord.ActiveDetailLevel;
end;

function TcxGridDetailsSiteViewInfo.GetCanvas: TcxCanvas;
begin
  Result := MasterGridViewInfo.Canvas;
end;

function TcxGridDetailsSiteViewInfo.GetContainer: TcxControl;
begin
  Result := MasterGridViewInfo.Site;
end;

function TcxGridDetailsSiteViewInfo.GetDesignController: TcxCustomGridDesignController;
begin
  Result := MasterGridView.Controller.DesignController;
end;

function TcxGridDetailsSiteViewInfo.GetFullyVisible: Boolean;
begin
  if CacheItem.IsDetailsSiteFullyVisibleAssigned then
    Result := CacheItem.DetailsSiteFullyVisible
  else
  begin
    Result := inherited GetFullyVisible;
    CacheItem.DetailsSiteFullyVisible := Result;
  end;  
end;

function TcxGridDetailsSiteViewInfo.GetHeight: Integer;
begin
  if CacheItem.IsDetailsSiteHeightAssigned then
    Result := CacheItem.DetailsSiteHeight
  else
  begin
    Result := inherited GetHeight;
    CacheItem.DetailsSiteHeight := Result;
  end;
end;

function TcxGridDetailsSiteViewInfo.GetMasterRecord: TObject;
begin
  Result := MasterDataRowViewInfo.GridRecord;
end;

function TcxGridDetailsSiteViewInfo.GetMaxHeight: Integer;
begin
  if MasterGridView.UseRestHeightForDetails then
    Result := FMasterDataRowViewInfo.RestHeight
  else
    Result := cxMaxRectSize;
  TcxGridLevelAccess.CheckHeight(Level, Result);
end;

function TcxGridDetailsSiteViewInfo.GetMaxNormalHeight: Integer;
begin
  Result := inherited GetMaxNormalHeight;
  TcxGridLevelAccess.CheckHeight(Level, Result);
end;

function TcxGridDetailsSiteViewInfo.GetMaxWidth: Integer;
begin
  Result := MasterGridViewInfo.ClientWidth -
    FMasterDataRowViewInfo.LevelIndent -
    FMasterDataRowViewInfo.ExpandButtonCellViewInfo.BaseWidth;
end;

function TcxGridDetailsSiteViewInfo.GetNormalHeight: Integer;
begin
  if CacheItem.IsDetailsSiteNormalHeightAssigned then
    Result := CacheItem.DetailsSiteNormalHeight
  else
  begin
    Result := inherited GetNormalHeight;
    CacheItem.DetailsSiteNormalHeight := Result;
  end;
end;

function TcxGridDetailsSiteViewInfo.GetTabsViewInfoClass: TcxCustomGridDetailsSiteTabsViewInfoClass;
begin
  if TabsPosition = dtpLeft then
    Result := TcxGridMasterDataRowDetailsSiteLeftTabsViewInfo
  else
    Result := inherited GetTabsViewInfoClass;
end;

function TcxGridDetailsSiteViewInfo.GetVisible: Boolean;
begin
  Result := FMasterDataRowViewInfo.DetailsSiteVisible;
end;

function TcxGridDetailsSiteViewInfo.GetWidth: Integer;
begin
  if CacheItem.IsDetailsSiteWidthAssigned then
    Result := CacheItem.DetailsSiteWidth
  else
  begin
    Result := inherited GetWidth;
    CacheItem.DetailsSiteWidth := Result;
  end;
end;

procedure TcxGridDetailsSiteViewInfo.InitTabHitTest(AHitTest: TcxGridDetailsSiteTabHitTest);
begin
  AHitTest.Owner := MasterDataRowViewInfo.GridRecord;
end;

procedure TcxGridDetailsSiteViewInfo.ChangeActiveTab(ALevel: TcxGridLevel;
  AFocusView: Boolean = False);
var
  ARow: TcxGridMasterDataRow;
begin
  ARow := MasterDataRowViewInfo.GridRecord;
  ARow.ActiveDetailIndex := ALevel.Index;
  if AFocusView and (ARow.ActiveDetailGridView <> nil) then
    ARow.ActiveDetailGridView.Focused := True;
end;

function TcxGridDetailsSiteViewInfo.DetailHasData(ALevel: TcxGridLevel): Boolean;
begin
  Result := TcxGridMasterDataRow(MasterRecord).DetailGridViewHasData[ALevel.Index];
end;

function TcxGridDetailsSiteViewInfo.HasMaxHeight: Boolean;
begin
  Result := (Level.MaxDetailHeight <> 0) and (Height = Level.MaxDetailHeight);
end;

function TcxGridDetailsSiteViewInfo.SupportsTabAccelerators: Boolean;
begin
  Result := MasterDataRowViewInfo.SupportsTabAccelerators;
end;

{ TcxGridExpandButtonCellViewInfo }

function TcxGridExpandButtonCellViewInfo.GetRecordViewInfo: TcxGridMasterDataRowViewInfo;
begin
  Result := TcxGridMasterDataRowViewInfo(inherited RecordViewInfo);
end;

function TcxGridExpandButtonCellViewInfo.GetRightBorderRestSpaceBounds: TRect;
begin
  Result := inherited GetBorderBounds(bRight);
  Result.Bottom := BorderBounds[bRight].Top;
end;

function TcxGridExpandButtonCellViewInfo.CalculateHeight: Integer;
begin
  Result := RecordViewInfo.DataHeight;
  if RecordViewInfo.DetailsSiteVisible then
    Inc(Result, RecordViewInfo.DetailsAreaViewInfo.CalculateHeight);
end;

function TcxGridExpandButtonCellViewInfo.CalculateWidth: Integer;
begin
  Result := GridViewInfo.ClientBounds.Left + RecordViewInfo.LevelIndent +
    BaseWidth - RecordViewInfo.ContentIndent;
end;

function TcxGridExpandButtonCellViewInfo.GetBackgroundBitmap: TBitmap;
begin
  Result := RecordViewInfo.BackgroundBitmap;
end;

function TcxGridExpandButtonCellViewInfo.GetBaseWidth: Integer;
begin
  Result := GridViewInfo.LevelIndent;
end;

function TcxGridExpandButtonCellViewInfo.GetBorderBounds(AIndex: TcxBorder): TRect;
begin
  Result := inherited GetBorderBounds(AIndex);
  if AIndex = bRight then
  begin
    Inc(Result.Top, RecordViewInfo.DataHeight);
    if bBottom in Borders then
      Dec(Result.Top, BorderWidth[bBottom]);
  end;
end;

function TcxGridExpandButtonCellViewInfo.GetBorders: TcxBorders;
begin
  Result := GridViewInfo.GetCellBorders(False, True);
  if RecordViewInfo.Expanded and (bBottom in Result) then
    Include(Result, bRight)
  else
    Exclude(Result, bRight);
end;

function TcxGridExpandButtonCellViewInfo.GetPainterClass: TcxCustomGridCellPainterClass;
begin
  Result := TcxGridExpandButtonCellPainter;
end;

{ TcxGridDetailsAreaViewInfo }

function TcxGridDetailsAreaViewInfo.GetRecordViewInfo: TcxGridMasterDataRowViewInfo;
begin
  Result := TcxGridMasterDataRowViewInfo(inherited RecordViewInfo);
end;

function TcxGridDetailsAreaViewInfo.CalculateHeight: Integer;
begin
  Result := RecordViewInfo.DetailsSiteViewInfo.Height;
end;

function TcxGridDetailsAreaViewInfo.CalculateWidth: Integer;
begin
  Result := RecordViewInfo.DetailsSiteViewInfo.MaxWidth;
end;

function TcxGridDetailsAreaViewInfo.GetBorders: TcxBorders;
begin
  if GridLines in [glBoth, glHorizontal] then
    Result := [bBottom]
  else
    Result := [];
end;

{ TcxGridMasterDataRowViewInfo }

constructor TcxGridMasterDataRowViewInfo.Create(ARecordsViewInfo: TcxCustomGridRecordsViewInfo;
  ARecord: TcxCustomGridRecord);
begin
  inherited;
  if DetailsSiteVisible then
  begin
    FDetailsAreaViewInfo := GetDetailsAreaViewInfoClass.Create(Self);
    FDetailsSiteViewInfo := GetDetailsSiteViewInfoClass.Create(Self);
  end;
  FExpandButtonCellViewInfo := GetExpandButtonCellViewInfoClass.Create(Self);
end;

destructor TcxGridMasterDataRowViewInfo.Destroy;
begin
  FExpandButtonCellViewInfo.Free;
  FDetailsSiteViewInfo.Free;
  FDetailsAreaViewInfo.Free;
  inherited;
end;

function TcxGridMasterDataRowViewInfo.GetCacheItem: TcxGridMasterTableViewInfoCacheItem;
begin
  Result := TcxGridMasterTableViewInfoCacheItem(inherited CacheItem);
end;

function TcxGridMasterDataRowViewInfo.GetDetailsSiteIndentBounds: TRect;
begin
  with DetailsSiteViewInfo.Bounds do
    Result := Rect(ContentIndent, Top, Left, Bottom);
end;

function TcxGridMasterDataRowViewInfo.GetGridRecord: TcxGridMasterDataRow;
begin
  Result := TcxGridMasterDataRow(inherited GridRecord);
end;

procedure TcxGridMasterDataRowViewInfo.CalculateExpandButtonBounds(var ABounds: TRect);
begin
  ABounds := ExpandButtonAreaBounds;
  inherited;
end;

function TcxGridMasterDataRowViewInfo.CalculateHeight: Integer;
begin
  Result := inherited CalculateHeight;
  if DetailsSiteVisible then
  begin
    FRestHeight := CalculateRestHeight(Result);
    Inc(Result, DetailsSiteViewInfo.Height);
  end;
end;

function TcxGridMasterDataRowViewInfo.CalculateRestHeight(ARowHeight: Integer): Integer;
begin
  Result := RecordsViewInfo.GetRestHeight(Bounds.Top) - ARowHeight;
end;

procedure TcxGridMasterDataRowViewInfo.ControlFocusChanged;
begin
  inherited;
  if DetailsSiteVisible then
    DetailsSiteViewInfo.ControlFocusChanged;
end;

function TcxGridMasterDataRowViewInfo.GetDataHeight: Integer;
begin
  Result := inherited GetDataHeight;
  if DetailsSiteVisible then
    Dec(Result, DetailsSiteViewInfo.Height);
end;

function TcxGridMasterDataRowViewInfo.GetDataIndent: Integer;
begin
  Result := inherited GetDataIndent + GridViewInfo.LevelIndent;
end;

function TcxGridMasterDataRowViewInfo.GetDataWidth: Integer;
begin
  Result := inherited GetDataWidth - GridViewInfo.LevelIndent;
end;

function TcxGridMasterDataRowViewInfo.GetDetailsSiteVisible: Boolean;
begin
  Result := Expanded;
end;

function TcxGridMasterDataRowViewInfo.GetExpandButtonAreaBounds: TRect;
var
  AButtonHeight: Integer;
begin
  AButtonHeight := DataHeight;
  if HasInplaceEditFormArea then
    AButtonHeight := AButtonHeight -  cxRectHeight(InplaceEditFormAreaViewInfo.Bounds);
  Result := Rect(ContentIndent, Bounds.Top, DataIndent, Bounds.Top + AButtonHeight);
end;

function TcxGridMasterDataRowViewInfo.GetMaxHeight: Integer;
begin
  Result := inherited GetMaxHeight;
  if DetailsSiteVisible then
    if not DetailsSiteViewInfo.FullyVisible and not GridViewInfo.CalculateDown and
      (Index > 0) and not DetailsSiteViewInfo.HasMaxHeight then
      Result := cxMaxRectSize
    else
      Result := Result - DetailsSiteViewInfo.Height + DetailsSiteViewInfo.NormalHeight;
end;

function TcxGridMasterDataRowViewInfo.GetPixelScrollSize: Integer;
begin
  Result := MaxHeight;
end;

function TcxGridMasterDataRowViewInfo.GetTableRowEmptyAreaLeft: Integer;
begin
  Result := inherited GetTableRowEmptyAreaLeft + GridViewInfo.LevelIndent;
end;

function TcxGridMasterDataRowViewInfo.IsDetailVisible(ADetail: TcxCustomGridView): Boolean;
begin
  Result := DetailsSiteVisible and (DetailsSiteViewInfo <> nil) and
    (DetailsSiteViewInfo.ActiveLevel = ADetail.Level);
end;

function TcxGridMasterDataRowViewInfo.IsFullyVisible: Boolean;
begin
  Result := inherited IsFullyVisible and
    (not DetailsSiteVisible or DetailsSiteViewInfo.HasMaxHeight or DetailsSiteViewInfo.FullyVisible);
end;

procedure TcxGridMasterDataRowViewInfo.VisibilityChanged(AVisible: Boolean);
begin
  inherited;
  if DetailsSiteVisible then
    DetailsSiteViewInfo.VisibilityChanged(AVisible);
end;

function TcxGridMasterDataRowViewInfo.GetPainterClass: TcxCustomGridCellPainterClass;
begin
  Result := TcxGridMasterDataRowPainter;
end;

function TcxGridMasterDataRowViewInfo.GetDetailsAreaViewInfoClass: TcxGridDetailsAreaViewInfoClass;
begin
  Result := TcxGridDetailsAreaViewInfo;
end;

function TcxGridMasterDataRowViewInfo.GetDetailsSiteViewInfoClass: TcxGridDetailsSiteViewInfoClass;
begin
  Result := TcxGridDetailsSiteViewInfo;
end;

function TcxGridMasterDataRowViewInfo.GetExpandButtonCellViewInfoClass: TcxGridExpandButtonCellViewInfoClass;
begin
  Result := TcxGridExpandButtonCellViewInfo;
end;

procedure TcxGridMasterDataRowViewInfo.BeforeRecalculation;
begin
  inherited;
  if DetailsSiteVisible then
  begin
    FDetailsSiteViewInfo.BeforeRecalculation;
    FDetailsAreaViewInfo.BeforeRecalculation;
  end;
  FExpandButtonCellViewInfo.BeforeRecalculation;
end;

procedure TcxGridMasterDataRowViewInfo.Calculate(ALeftBound, ATopBound: Integer;
  AWidth: Integer = -1; AHeight: Integer = -1);
var
  ADetailLeftBound, ADetailTopBound: Integer;

  function NeedCalculateDetail: Boolean;
  begin
    Result := DetailsSiteVisible and
      (GridViewInfo.IsCalculating or
        DetailsSiteViewInfo.ActiveGridViewExists and
          (DetailsSiteViewInfo.ActiveGridView.Site.Left = cxInvisibleCoordinate));
  end;

begin
  inherited;
  ExpandButtonCellViewInfo.Calculate(ContentIndent, ATopBound);
  if NeedCalculateDetail then
  begin
    ADetailLeftBound := ExpandButtonCellViewInfo.Bounds.Right;
    ADetailTopBound := ATopBound + DataHeight;
    DetailsSiteViewInfo.Calculate(ADetailLeftBound, ADetailTopBound);
    DetailsAreaViewInfo.Calculate(ADetailLeftBound, ADetailTopBound);
  end;
end;

function TcxGridMasterDataRowViewInfo.GetHitTest(const P: TPoint): TcxCustomGridHitTest;
begin
  if DetailsSiteVisible and PtInRect(DetailsSiteViewInfo.Bounds, P) then
    Result := DetailsSiteViewInfo.GetHitTest(P)
  else
    Result := inherited GetHitTest(P);
end;

function TcxGridMasterDataRowViewInfo.ProcessDialogChar(ACharCode: Word): Boolean;
begin
  Result := inherited ProcessDialogChar(ACharCode);
  if not Result and DetailsSiteVisible then
    Result := DetailsSiteViewInfo.ProcessDialogChar(ACharCode);
end;

function TcxGridMasterDataRowViewInfo.SupportsTabAccelerators: Boolean;
begin
  Result := GridView.Controller.SupportsTabAccelerators(GridRecord);
end;

{ TcxGridGroupCellViewInfo }

function TcxGridGroupCellViewInfo.GetExpandedAreaBounds: TRect;
begin
  Result := inherited GetBorderBounds(bBottom);
  Result.Right := BorderBounds[bBottom].Left;
end;

function TcxGridGroupCellViewInfo.GetGridRecord: TcxGridGroupRow;
begin
  Result := TcxGridGroupRow(inherited GridRecord);
end;

function TcxGridGroupCellViewInfo.GetRecordViewInfo: TcxGridGroupRowViewInfo;
begin
  Result := TcxGridGroupRowViewInfo(inherited RecordViewInfo);
end;

function TcxGridGroupCellViewInfo.GetRowStyle: TcxGridGroupRowStyle;
begin
  Result := RecordViewInfo.RowStyle;
end;

function TcxGridGroupCellViewInfo.CalculateHeight: Integer;
begin
  Result := RecordViewInfo.DataHeight;
end;

function TcxGridGroupCellViewInfo.CalculateWidth: Integer;
begin
  with RecordViewInfo, Bounds do
    Result := Right - Left - LevelIndent;
end;

function TcxGridGroupCellViewInfo.CustomDraw(ACanvas: TcxCanvas): Boolean;
begin
  Result := inherited CustomDraw(ACanvas);
  if not Result then
    TcxGridTableViewAccess.DoCustomDrawGroupCell(GridView, ACanvas, Self, Result);
end;

function TcxGridGroupCellViewInfo.GetAlignmentVert: TcxAlignmentVert;
begin
  if RowStyle = grsStandard then
    Result := vaCenter
  else
    Result := vaBottom;
end;

function TcxGridGroupCellViewInfo.GetAlwaysSelected: Boolean;
begin
  Result := True;
end;

function TcxGridGroupCellViewInfo.GetBackgroundBitmap: TBitmap;
begin
  Result := RecordViewInfo.BackgroundBitmap;
end;

function TcxGridGroupCellViewInfo.GetBorderBounds(AIndex: TcxBorder): TRect;
begin
  Result := inherited GetBorderBounds(AIndex);
  if (AIndex = bBottom) and (RowStyle = grsStandard) and GridRecord.Expanded then
    Result.Left := RecordViewInfo.LevelIndentVertLineBounds[GridRecord.Level].Left;
end;

function TcxGridGroupCellViewInfo.GetBorderColor(AIndex: TcxBorder): TColor;
begin
  if (AIndex = bBottom) and (RowStyle = grsOffice11) then
    Result := GridView.LookAndFeelPainter.GridGroupRowStyleOffice11SeparatorColor
  else
    Result := GridViewInfo.GridLineColor;
end;

function TcxGridGroupCellViewInfo.GetBorders: TcxBorders;
begin
  Result := GridViewInfo.GetCellBorders(True, True);
  if RowStyle = grsOffice11 then
    Include(Result, bBottom);
end;

function TcxGridGroupCellViewInfo.GetBorderWidth(AIndex: TcxBorder): Integer;
begin
  if (AIndex = bBottom) and (RowStyle = grsOffice11) then
    Result := RecordViewInfo.RecordsViewInfo.GroupRowSeparatorWidth
  else
    Result := GridViewInfo.GridLineWidth;
end;

function TcxGridGroupCellViewInfo.GetPainterClass: TcxCustomGridCellPainterClass;
begin
  Result := TcxGridGroupCellPainter;
end;

function TcxGridGroupCellViewInfo.GetText: string;
begin
  Result := GridRecord.DisplayCaption;
  TcxCustomGridTableItemAccess.DoGetDisplayText(GridRecord.GroupedColumn, GridRecord, Result);
end;

function TcxGridGroupCellViewInfo.GetTextAreaBounds: TRect;
begin
  Result := inherited GetTextAreaBounds;
  if GridRecord.Expandable then
    Inc(Result.Left, GridViewInfo.LevelIndent);
end;

procedure TcxGridGroupCellViewInfo.GetViewParams(var AParams: TcxViewParams);
begin
  GridView.Styles.GetDataCellParams(GridRecord, nil, AParams, True, Self);
end;

function TcxGridGroupCellViewInfo.HasCustomDraw: Boolean;
begin
  Result := TcxGridTableViewAccess.HasCustomDrawGroupCell(GridView);
end;

function TcxGridGroupCellViewInfo.CanDrawSelected: Boolean;
begin
  Result := True;
end;

{ TcxGridGroupRowSpacerViewInfo }

constructor TcxGridGroupRowSpacerViewInfo.Create(ARowViewInfo: TcxGridGroupRowViewInfo;
  const AText: string);
begin
  inherited Create(ARowViewInfo.GridViewInfo);
  FRowViewInfo := ARowViewInfo;
  Text := AText;
end;

function TcxGridGroupRowSpacerViewInfo.GetGridView: TcxGridTableView;
begin
  Result := TcxGridTableView(inherited GridView);
end;

function TcxGridGroupRowSpacerViewInfo.CalculateWidth: Integer;
begin
  Result := TextWidth;
end;

function TcxGridGroupRowSpacerViewInfo.GetAlignmentVert: TcxAlignmentVert;
begin
  Result := FRowViewInfo.CellViewInfo.AlignmentVert;
end;

function TcxGridGroupRowSpacerViewInfo.GetText: string;
begin
  Result := Text;
end;

function TcxGridGroupRowSpacerViewInfo.GetTextAreaBounds: TRect;
begin
  Result := ContentBounds;
  InflateRect(Result, 0, -cxGridCellTextOffset);
end;

procedure TcxGridGroupRowSpacerViewInfo.GetViewParams(var AParams: TcxViewParams);
begin
  GridView.Styles.GetGroupSummaryCellParams(RowViewInfo.GridRecord, nil, AParams);
end;

function TcxGridGroupRowSpacerViewInfo.GetWidth: Integer;
begin
  CalculateParams;
  Result := inherited GetWidth;
end;

function TcxGridGroupRowSpacerViewInfo.HasBackground: Boolean;
begin
  Result := False;
end;

{ TcxGridGroupSummaryCellViewInfo }

constructor TcxGridGroupSummaryCellViewInfo.Create(ARowViewInfo: TcxGridGroupRowViewInfo;
  ASummaryItem: TcxDataSummaryItem; const AValue: Variant);
begin
  inherited Create(ARowViewInfo.GridViewInfo);
  FRowViewInfo := ARowViewInfo;
  FSummaryItem := ASummaryItem;
  FValue := AValue;
  FSeparatorViewInfo := FRowViewInfo.CreateSpacerViewInfo(GetSeparatorText);
  FTextWidth := -1;
end;

destructor TcxGridGroupSummaryCellViewInfo.Destroy;
begin
  FSeparatorViewInfo.Free;
  inherited;
end;

function TcxGridGroupSummaryCellViewInfo.GetColumn: TcxGridColumn;
begin
  Result := FSummaryItem.ItemLink as TcxGridColumn;
end;

function TcxGridGroupSummaryCellViewInfo.GetGridView: TcxGridTableView;
begin
  Result := TcxGridTableView(inherited GridView);
end;

procedure TcxGridGroupSummaryCellViewInfo.SetIsLast(Value: Boolean);
begin
  if FIsLast <> Value then
  begin
    FIsLast := Value;
    FSeparatorViewInfo.Text := GetSeparatorText;
  end;
end;

function TcxGridGroupSummaryCellViewInfo.CalculateWidth: Integer;
begin
  Result := TextWidth + FSeparatorViewInfo.Width;
end;

function TcxGridGroupSummaryCellViewInfo.CanAlignWithColumn: Boolean;
begin
  Result := Column <> nil;
end;

function TcxGridGroupSummaryCellViewInfo.CustomDraw(ACanvas: TcxCanvas): Boolean;
begin
  Result := inherited CustomDraw(ACanvas);
  if not Result then
  begin
    if Column <> nil then
      TcxGridColumnAccess.DoCustomDrawGroupSummaryCell(Column, ACanvas, Self, Result);
    if not Result then
      TcxGridTableViewAccess.DoCustomDrawGroupSummaryCell(GridView, ACanvas, Self, Result);
  end;
end;

function TcxGridGroupSummaryCellViewInfo.GetAlignmentHorz: TAlignment;
begin
  if CanAlignWithColumn and FRowViewInfo.SummaryCellAutoWidth then
    Result := Column.GroupSummaryAlignment
  else
    Result := taLeftJustify;
end;

function TcxGridGroupSummaryCellViewInfo.GetAlignmentVert: TcxAlignmentVert;
begin
  Result := FRowViewInfo.CellViewInfo.AlignmentVert;
end;

function TcxGridGroupSummaryCellViewInfo.GetDesignSelectionBounds: TRect;
begin
  Result := Bounds;
  InflateRect(Result, 0, -1);
end;

function TcxGridGroupSummaryCellViewInfo.GetHitTestClass: TcxCustomGridHitTestClass;
begin
  Result := TcxGridGroupSummaryHitTest;
end;

function TcxGridGroupSummaryCellViewInfo.GetIsDesignSelected: Boolean;
begin
  Result := GridView.IsDesigning and
    GridView.Controller.DesignController.IsObjectSelected(SummaryItem);
end;

function TcxGridGroupSummaryCellViewInfo.GetPainterClass: TcxCustomGridCellPainterClass;
begin
  Result := TcxGridGroupSummaryCellPainter;
end;

function TcxGridGroupSummaryCellViewInfo.GetSeparatorBounds: TRect;
begin
  Result := Bounds;
  Result.Left := Min(GetTextBounds(True, False).Right, TextAreaBounds.Right);
end;

function TcxGridGroupSummaryCellViewInfo.GetSeparatorText: string;
begin
  if IsLast then
    Result := ''
  else
    Result := (FSummaryItem.SummaryItems as TcxDataGroupSummaryItems).Separator + ' ';
end;

function TcxGridGroupSummaryCellViewInfo.GetShowEndEllipsis: Boolean;
begin
  Result := True;
end;

function TcxGridGroupSummaryCellViewInfo.GetText: string;
begin
  try
    Result := FSummaryItem.FormatValue(FValue, False);
  except
    Application.HandleException(Self);
  end;
end;

function TcxGridGroupSummaryCellViewInfo.GetTextAreaBounds: TRect;
begin
  Result := ContentBounds;
  InflateRect(Result, 0, -cxGridCellTextOffset);
  Dec(Result.Right, FSeparatorViewInfo.Width);
  if Result.Right < Result.Left then
    Result.Right := Result.Left;
end;

procedure TcxGridGroupSummaryCellViewInfo.GetViewParams(var AParams: TcxViewParams);
begin
  GridView.Styles.GetGroupSummaryCellParams(RowViewInfo.GridRecord, SummaryItem, AParams);
end;

function TcxGridGroupSummaryCellViewInfo.HasBackground: Boolean;
begin
  Result := False;
end;

function TcxGridGroupSummaryCellViewInfo.HasCustomDraw: Boolean;
begin
  Result := inherited HasCustomDraw or
    (Column <> nil) and TcxGridColumnAccess.HasCustomDrawGroupSummaryCell(Column) or
    TcxGridTableViewAccess.HasCustomDrawGroupSummaryCell(GridView);
end;

procedure TcxGridGroupSummaryCellViewInfo.InitHitTest(AHitTest: TcxCustomGridHitTest);
begin
  inherited;
  with TcxGridGroupSummaryHitTest(AHitTest) do
  begin
    if not GridView.IsDesigning then
      ViewInfo := Self.RowViewInfo;
    GridRecord := Self.RowViewInfo.GridRecord;
    SummaryItem := Self.SummaryItem;
  end;
end;

procedure TcxGridGroupSummaryCellViewInfo.Offset(DX, DY: Integer);
begin
  inherited;
  FSeparatorViewInfo.DoOffset(DX, DY);
end;

procedure TcxGridGroupSummaryCellViewInfo.BeforeRecalculation;
begin
  inherited;
  FSeparatorViewInfo.BeforeRecalculation;
end;

procedure TcxGridGroupSummaryCellViewInfo.Calculate(ALeftBound, ATopBound: Integer;
  AWidth: Integer = -1; AHeight: Integer = -1);
begin
  inherited;
  FSeparatorViewInfo.Calculate(GetSeparatorBounds);
end;

function TcxGridGroupSummaryCellViewInfo.MouseDown(AHitTest: TcxCustomGridHitTest;
  AButton: TMouseButton; AShift: TShiftState): Boolean;
begin
  Result := inherited MouseDown(AHitTest, AButton, AShift);
  if GridView.IsDesigning and (AButton = mbLeft) then
  begin
    GridView.Controller.DesignController.SelectObject(SummaryItem, not (ssShift in AShift));
    Result := True;
  end;
end;

{ TcxGridGroupRowViewInfo }

constructor TcxGridGroupRowViewInfo.Create(ARecordsViewInfo: TcxCustomGridRecordsViewInfo;
  ARecord: TcxCustomGridRecord); 
begin
  inherited;
  FCellViewInfo := TcxGridGroupCellViewInfo.Create(Self);
  FSummaryCellViewInfos := TList.Create;
  FSummaryCellsWithoutColumns := TList.Create;
  CreateSummaryCellViewInfos;
  GetColumnSummaryCellViewInfos(nil, FSummaryCellsWithoutColumns);
  CreateSpacerViewInfos;
end;

destructor TcxGridGroupRowViewInfo.Destroy;
begin
  DestroySpacerViewInfos;
  DestroySummaryCellViewInfos;
  FSummaryCellsWithoutColumns.Free;
  FSummaryCellViewInfos.Free;
  FCellViewInfo.Free;
  inherited;
end;

function TcxGridGroupRowViewInfo.GetGridRecord: TcxGridGroupRow;
begin
  Result := TcxGridGroupRow(inherited GridRecord);
end;

function TcxGridGroupRowViewInfo.GetRowStyle: TcxGridGroupRowStyle;
begin
  Result := GridView.OptionsView.GroupRowStyle;
end;

function TcxGridGroupRowViewInfo.GetSummaryCellLayout: TcxGridGroupSummaryLayout;
begin
  Result := GridView.OptionsView.GetGroupSummaryLayout;
end;

function TcxGridGroupRowViewInfo.GetSummaryCellViewInfo(Index: Integer): TcxGridGroupSummaryCellViewInfo;
begin
  Result := TcxGridGroupSummaryCellViewInfo(FSummaryCellViewInfos[Index]);
end;

function TcxGridGroupRowViewInfo.GetSummaryCellViewInfoCount: Integer;
begin
  Result := FSummaryCellViewInfos.Count;
end;

procedure TcxGridGroupRowViewInfo.CalculateExpandButtonBounds(var ABounds: TRect);
begin
  ABounds := FCellViewInfo.ContentBounds;
  if RowStyle = grsOffice11 then
    with ABounds do
      Top := Bottom - FCellViewInfo.TextHeightWithOffset;
  inherited;
end;

function TcxGridGroupRowViewInfo.CalculateHeight: Integer;
begin
  if AutoHeight then
  begin
    CellViewInfo.CalculateParams;
    Result := RecordsViewInfo.CalculateCustomGroupRowHeight(False, CellViewInfo.Params);
    dxAdjustToTouchableSize(Result);
  end
  else
    Result := RecordsViewInfo.GroupRowHeight;
  Inc(Result, inherited CalculateHeight);
end;

function TcxGridGroupRowViewInfo.CanSize: Boolean;
begin
  Result := GridView.OptionsCustomize.GroupRowSizing;
end;

procedure TcxGridGroupRowViewInfo.CheckRowHeight(var AValue: Integer);
begin
  Dec(AValue, NonBaseHeight);
  inherited;
  GridView.OptionsView.CheckGroupRowHeight(AValue);
  Inc(AValue, NonBaseHeight);
end;

function TcxGridGroupRowViewInfo.GetAutoHeight: Boolean;
begin
  Result := inherited GetAutoHeight and (GridView.OptionsView.GroupRowHeight = 0);
end;

function TcxGridGroupRowViewInfo.GetBackgroundBitmap: TBitmap;
begin
  Result := RecordsViewInfo.GroupBackgroundBitmap;
end;

function TcxGridGroupRowViewInfo.GetExpandButtonAreaBounds: TRect;
begin
  Result := FCellViewInfo.ContentBounds;
  Result.Right := Result.Left + GridViewInfo.LevelIndent;
end;

function TcxGridGroupRowViewInfo.GetFocusRectBounds: TRect;
begin
  Result := inherited GetFocusRectBounds;
  Result.Bottom := FCellViewInfo.ContentBounds.Bottom;
end;

function TcxGridGroupRowViewInfo.GetPainterClass: TcxCustomGridCellPainterClass;
begin
  Result := TcxGridGroupRowPainter;
end;

function TcxGridGroupRowViewInfo.GetSeparatorBounds: TRect;
begin
  Result := inherited GetSeparatorBounds;
  if Expanded then Inc(Result.Left, GridViewInfo.LevelIndent);
end;

function TcxGridGroupRowViewInfo.GetSeparatorIndentBounds: TRect;
begin
  Result := inherited GetSeparatorBounds;
  Result.Right := SeparatorBounds.Left;
end;

function TcxGridGroupRowViewInfo.GetShowSeparator: Boolean;
begin
  if RowStyle = grsStandard then
    Result := inherited GetShowSeparator
  else
    Result := False;
end;

function TcxGridGroupRowViewInfo.HasFocusRect: Boolean;
begin
  Result := True;
end;

function TcxGridGroupRowViewInfo.HasFooter(ALevel: Integer): Boolean;
begin
  Result := inherited HasFooter(ALevel) or
    (GridView.OptionsView.GroupFooters = gfAlwaysVisible) and
    (ALevel = 0) and not GridRecord.Expanded and
    TcxGridColumnAccess.CanShowGroupFooters(GridRecord.GroupedColumn);
end;

procedure TcxGridGroupRowViewInfo.Offset(DX, DY: Integer);
var
  I: Integer;
begin
  inherited;
  FCellViewInfo.DoOffset(DX, DY);
  if FSummaryBeginningSpacerViewInfo <> nil then
    FSummaryBeginningSpacerViewInfo.DoOffset(DX, DY);
  for I := 0 to SummaryCellViewInfoCount - 1 do
    SummaryCellViewInfos[I].DoOffset(DX, DY);
  if FSummaryEndingSpacerViewInfo <> nil then
    FSummaryEndingSpacerViewInfo.DoOffset(DX, DY);
end;

procedure TcxGridGroupRowViewInfo.SetRowHeight(Value: Integer);
begin
  if RowHeight <> Value then
    GridView.OptionsView.GroupRowHeight := Value - NonBaseHeight;
end;

function TcxGridGroupRowViewInfo.CreateSpacerViewInfo(const AText: string): TcxGridGroupRowSpacerViewInfo;
begin
  Result := GetSpacerViewInfoClass.Create(Self, AText);
end;

procedure TcxGridGroupRowViewInfo.CreateSpacerViewInfos;
begin
  if HasUnalignableSummaryCells then
  begin
    FSummaryBeginningSpacerViewInfo := CreateSpacerViewInfo(GetSummaryBeginningSpacerText);
    FSummaryEndingSpacerViewInfo := CreateSpacerViewInfo(GetSummaryEndingSpacerText);
  end;
end;

procedure TcxGridGroupRowViewInfo.DestroySpacerViewInfos;
begin
  FSummaryEndingSpacerViewInfo.Free;
  FSummaryBeginningSpacerViewInfo.Free;
end;

function TcxGridGroupRowViewInfo.GetSpacerViewInfoClass: TcxGridGroupRowSpacerViewInfoClass;
begin
  Result := TcxGridGroupRowSpacerViewInfo;
end;

function TcxGridGroupRowViewInfo.AddSummaryCellViewInfo(ASummaryItem: TcxDataSummaryItem;
  const AValue: Variant): TcxGridGroupSummaryCellViewInfo;
begin
  Result := GetSummaryCellViewInfoClass.Create(Self, ASummaryItem, AValue);
  FSummaryCellViewInfos.Add(Result);
end;

procedure TcxGridGroupRowViewInfo.CreateSummaryCellViewInfos;
var
  ASummaryItems: TcxDataSummaryItems;
  ASummaryValues: PVariant;
  I: Integer;
begin
  if not GridRecord.GetGroupSummaryInfo(ASummaryItems, ASummaryValues) then Exit;
  for I := 0 to ASummaryItems.Count - 1 do
    if ASummaryItems[I].Position = spGroup then
      AddSummaryCellViewInfo(ASummaryItems[I], ASummaryValues^[I]);
end;

procedure TcxGridGroupRowViewInfo.DestroySummaryCellViewInfos;
var
  I: Integer;
begin
  for I := 0 to SummaryCellViewInfoCount - 1 do
    SummaryCellViewInfos[I].Free;
end;

function TcxGridGroupRowViewInfo.GetSummaryCellViewInfoClass: TcxGridGroupSummaryCellViewInfoClass;
begin
  Result := TcxGridGroupSummaryCellViewInfo;
end;

procedure TcxGridGroupRowViewInfo.CalculateSummaryCells;
var
  I: Integer;
  AColumnHeaderViewInfo: TcxGridColumnHeaderViewInfo;
  ACellViewInfos: TList;
begin
  if SummaryCellViewInfoCount = 0 then Exit;

  if HasUnalignableSummaryCells then
  begin
    SummaryBeginningSpacerViewInfo.Calculate(GetSummaryBeginningSpacerBounds);
    TcxGridGroupSummaryCellViewInfo(UnalignableSummaryCells.Last).IsLast := True;
    CalculateSummaryCells(UnalignableSummaryCells, GetSummaryCellsAreaBounds(False),
      taLeftJustify, False);
    SummaryEndingSpacerViewInfo.Calculate(GetSummaryEndingSpacerBounds);
  end;

  if SummaryCellLayout <> gslStandard then
  begin
    ACellViewInfos := TList.Create;
    try
      for I := 0 to GridViewInfo.HeaderViewInfo.Count - 1 do
      begin
        AColumnHeaderViewInfo := GridViewInfo.HeaderViewInfo[I];
        GetColumnSummaryCellViewInfos(AColumnHeaderViewInfo.Column, ACellViewInfos);
        if ACellViewInfos.Count <> 0 then
        begin
          TcxGridGroupSummaryCellViewInfo(ACellViewInfos.Last).IsLast := True;
          CalculateSummaryCells(ACellViewInfos,
            GetColumnSummaryCellsAreaBounds(AColumnHeaderViewInfo),
            AColumnHeaderViewInfo.Column.GroupSummaryAlignment, SummaryCellAutoWidth);
        end;
      end;
    finally
      ACellViewInfos.Free;
    end;
  end;
end;

procedure TcxGridGroupRowViewInfo.CalculateSummaryCells(ACellViewInfos: TList;
  const AAreaBounds: TRect; AAlignment: TAlignment; AAutoWidth: Boolean);
var
  AWidths: TcxAutoWidthObject;
  ABounds: TRect;
  I: Integer;
begin
  AWidths := CalculateSummaryCellWidths(ACellViewInfos, AAreaBounds.Right - AAreaBounds.Left,
    AAutoWidth);
  try
    ABounds := GetSummaryCellsBounds(AAreaBounds, AWidths, AAlignment, AAutoWidth);
    for I := 0 to ACellViewInfos.Count - 1 do
    begin
      ABounds.Right := Min(ABounds.Left + AWidths[I].AutoWidth, AAreaBounds.Right);
      TcxGridGroupSummaryCellViewInfo(ACellViewInfos[I]).Calculate(ABounds);
      ABounds.Left := ABounds.Right;
    end;
  finally
    AWidths.Free;
  end;
end;

function TcxGridGroupRowViewInfo.CalculateSummaryCellWidths(ACellViewInfos: TList;
  AAvailableWidth: Integer; AAutoWidth: Boolean): TcxAutoWidthObject;
var
  I: Integer;
begin
  Result := TcxAutoWidthObject.Create(ACellViewInfos.Count);
  Result.AvailableWidth := AAvailableWidth;
  for I := 0 to ACellViewInfos.Count - 1 do
    with Result.AddItem do
    begin
      Width := TcxGridGroupSummaryCellViewInfo(ACellViewInfos[I]).CalculateWidth;
      MinWidth := Width;
      Fixed := False;
      if not AAutoWidth then AutoWidth := Width;
    end;
  if AAutoWidth then Result.Calculate;
end;

function TcxGridGroupRowViewInfo.GetColumnSummaryCellsAreaBounds(AColumnHeaderViewInfo: TcxGridColumnHeaderViewInfo): TRect;
begin
  Result := GetSummaryCellsAreaBounds(True);
  Result.Left := Max(Result.Left, AColumnHeaderViewInfo.DataOffset + cxGridCellTextOffset);
  Result.Right := Min(Result.Right, AColumnHeaderViewInfo.Bounds.Right - cxGridCellTextOffset);
end;

procedure TcxGridGroupRowViewInfo.GetColumnSummaryCellViewInfos(AColumn: TcxGridColumn; AList: TList);
var
  I: Integer;
begin
  AList.Clear;
  for I := 0 to SummaryCellViewInfoCount - 1 do
    if SummaryCellViewInfos[I].Column = AColumn then
      AList.Add(SummaryCellViewInfos[I]);
end;

function TcxGridGroupRowViewInfo.GetSummaryBeginningSpacerBounds: TRect;
begin
  Result := GetSummaryCellsAreaBounds(False);
  Result.Right := Result.Left;
  Dec(Result.Left, SummaryBeginningSpacerViewInfo.Width);
  Result.Right := Min(Result.Right, CellViewInfo.TextAreaBounds.Right);
end;

function TcxGridGroupRowViewInfo.GetSummaryEndingSpacerBounds: TRect;
begin
  Result := GetSummaryCellsAreaBounds(False);
  Result.Left := TcxGridGroupSummaryCellViewInfo(UnalignableSummaryCells.Last).Bounds.Right;
  Result.Right := Result.Left + SummaryEndingSpacerViewInfo.Width;
  Result.Right := Min(Result.Right, CellViewInfo.TextAreaBounds.Right);
end;

function TcxGridGroupRowViewInfo.GetSummaryBeginningSpacerText: string;
begin
  Result := ' ' + GridRecord.GroupSummaryItems.BeginText;
end;

function TcxGridGroupRowViewInfo.GetSummaryEndingSpacerText: string;
begin
  Result := GridRecord.GroupSummaryItems.EndText;
end;

function TcxGridGroupRowViewInfo.GetSummaryCellAutoWidth: Boolean;
begin
  Result := SummaryCellLayout = gslAlignWithColumnsAndDistribute;
end;

function TcxGridGroupRowViewInfo.GetSummaryCellsAreaBounds(AForAlignableCells: Boolean): TRect;
begin
  Result := CellViewInfo.ContentBounds;
  Result.Right := CellViewInfo.TextAreaBounds.Right;
  Result.Left := Min(CellViewInfo.GetTextBounds(True, False).Right, Result.Right);
  if AForAlignableCells then
  begin
    if HasUnalignableSummaryCells then
      Result.Left := SummaryEndingSpacerViewInfo.Bounds.Right;
    Inc(Result.Left, 2 * cxGridCellTextOffset);
  end
  else
  begin
    Inc(Result.Left, SummaryBeginningSpacerViewInfo.Width);
    Dec(Result.Right, SummaryEndingSpacerViewInfo.Width);
    if Result.Right < Result.Left then
      Result.Right := Result.Left;
  end;
end;

function TcxGridGroupRowViewInfo.GetSummaryCellsBounds(const AAreaBounds: TRect;
  AWidths: TcxAutoWidthObject; AAlignment: TAlignment; AAutoWidth: Boolean): TRect;
begin
  Result := AAreaBounds;
  if not AAutoWidth and (AWidths.Width < AWidths.AvailableWidth) then
    case AAlignment of
      taRightJustify:
        Result.Left := Result.Right - AWidths.Width;
      taCenter:
        Result.Left := (Result.Left + Result.Right - AWidths.Width) div 2;
    end;
end;

function TcxGridGroupRowViewInfo.GetUnalignableSummaryCells: TList;
begin
  if SummaryCellLayout = gslStandard then
    Result := SummaryCells
  else
    Result := SummaryCellsWithoutColumns;
end;

function TcxGridGroupRowViewInfo.HasUnalignableSummaryCells: Boolean;
begin
  Result := (SummaryCellLayout = gslStandard) or (SummaryCellsWithoutColumns.Count <> 0);
end;

procedure TcxGridGroupRowViewInfo.BeforeRecalculation;
var
  I: Integer;
begin
  inherited;
  FCellViewInfo.BeforeRecalculation;
  if FSummaryBeginningSpacerViewInfo <> nil then
    FSummaryBeginningSpacerViewInfo.BeforeRecalculation;
  for I := 0 to SummaryCellViewInfoCount - 1 do
    SummaryCellViewInfos[I].BeforeRecalculation;
  if FSummaryEndingSpacerViewInfo <> nil then
    FSummaryEndingSpacerViewInfo.BeforeRecalculation;  
end;

procedure TcxGridGroupRowViewInfo.Calculate(ALeftBound, ATopBound: Integer;
  AWidth: Integer = -1; AHeight: Integer = -1);
begin
  inherited;
  FCellViewInfo.Calculate(ALeftBound + LevelIndent, ATopBound);
  CalculateSummaryCells;
end;

function TcxGridGroupRowViewInfo.GetHitTest(const P: TPoint): TcxCustomGridHitTest;
var
  I: Integer;
  AHitTest: TcxCustomGridHitTest;
begin
  Result := inherited GetHitTest(P);
  if Result <> nil then
    for I := 0 to SummaryCellViewInfoCount - 1 do
    begin
      AHitTest := SummaryCellViewInfos[I].GetHitTest(P);
      if AHitTest <> nil then
      begin
        Result := AHitTest;
        Break;
      end;
    end;
end;

end.
