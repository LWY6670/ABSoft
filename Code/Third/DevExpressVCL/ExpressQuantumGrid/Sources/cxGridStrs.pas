{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressQuantumGrid                                       }
{                                                                    }
{           Copyright (c) 1998-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSQUANTUMGRID AND ALL            }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY. }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit cxGridStrs;

{$I cxVer.inc}

interface

resourcestring
  scxGridRecursiveLevels = '您不能创建递归层';

  scxGridDeletingFocusedConfirmationText = '删除数据吗?';
  scxGridDeletingSelectedConfirmationText =  '删除所有选定的记录吗?';

  scxGridNoDataInfoText = '<没有任何记录>';

  scxGridFilterRowInfoText = '单击此处定义一个过滤器';
  scxGridNewItemRowInfoText = '单击此处添加一新行';

  scxGridFindPanelClearButtonCaption = '清除';
  scxGridFindPanelFindButtonCaption = '查找';
  scxGridFindPanelInfoText = '输入文本搜索...';

  scxGridFilterIsEmpty = '<数据过滤条件为空>';

  scxGridCustomizationFormCaption = '定制';
  scxGridCustomizationFormColumnsPageCaption =  '列';
  scxGridGroupByBoxCaption = '把列标题拖放到此处使记录按此列进行分组';
  scxGridFilterApplyButtonCaption = 'Apply Filter';
  scxGridFilterCustomizeButtonCaption = '定制...';
  scxGridColumnsQuickCustomizationHint = '点击选择可视列';

  scxGridCustomizationFormBandsPageCaption = '区域';
  scxGridBandsQuickCustomizationHint = '点击选择可视区域';

  scxGridCustomizationFormRowsPageCaption ='行';

  scxGridConverterIntermediaryMissing = '缺少一个中间组件!'#13#10'请添加一个 %s 组件到窗体.';
  scxGridConverterNotExistGrid = 'cxGrid 不存在';
  scxGridConverterNotExistComponent = '组件不存在';
  scxImportErrorCaption = '导入错误';

  scxNotExistGridView = 'Grid 视图不存在';
  scxNotExistGridLevel =  '活动的 grid 层不存在';
  scxCantCreateExportOutputFile = '不能建立导出文件';

  cxSEditRepositoryExtLookupComboBoxItem = 'ExtLookupComboBox|一个使用QuantumGrid作为其下拉控件的高级查找';

  // date ranges

  scxGridYesterday = '昨天';
  scxGridToday = '今天';
  scxGridTomorrow = '明天';
  scxGridLast30Days = '30天前';
  scxGridLast14Days = '14天前';
  scxGridLast7Days = '7天前';
  scxGridNext7Days = '7天后';
  scxGridNext14Days = '14天后';
  scxGridNext30Days = '30天后';
  scxGridLastTwoWeeks = '两周前';
  scxGridLastWeek = '上周';
  scxGridThisWeek = '本周';
  scxGridNextWeek = '下一周';
  scxGridNextTwoWeeks = '两周后';
  scxGridLastMonth = '上个月';
  scxGridThisMonth = '本月';
  scxGridNextMonth = '下个月';
  scxGridLastYear = '去年';
  scxGridThisYear = '今年';
  scxGridNextYear = '下一年';
  scxGridPast = '过去';
  scxGridFuture = '将来';

  scxGridMonthFormat = 'mmmm yyyy';
  scxGridYearFormat = 'yyyy';

  // ChartView
  
  scxGridChartCategoriesDisplayText = '数据';

  scxGridChartValueHintFormat = '%s 对 %s 是 %s';  // series display text, category, value
  scxGridChartPercentValueTickMarkLabelFormat = '0%';

  scxGridChartToolBoxDataLevels = '数据层:';
  scxGridChartToolBoxDataLevelSelectValue = '选择值';
  scxGridChartToolBoxCustomizeButtonCaption = '自定义图表';

  scxGridChartNoneDiagramDisplayText = '无图表';
  scxGridChartColumnDiagramDisplayText = '直方图';
  scxGridChartBarDiagramDisplayText = '柱形图表';
  scxGridChartLineDiagramDisplayText = '线图表';
  scxGridChartAreaDiagramDisplayText = '面积图表';
  scxGridChartPieDiagramDisplayText = '圆形图';
  scxGridChartStackedBarDiagramDisplayText = '层叠条形图';
  scxGridChartStackedColumnDiagramDisplayText = '层叠条形图';
  scxGridChartStackedAreaDiagramDisplayText = '层叠区域图';

  scxGridChartCustomizationFormSeriesPageCaption = '系列';
  scxGridChartCustomizationFormSortBySeries = '排序通过:';
  scxGridChartCustomizationFormNoSortedSeries = '<空系列>';
  scxGridChartCustomizationFormDataGroupsPageCaption = '数据组';
  scxGridChartCustomizationFormOptionsPageCaption = '选项';

  scxGridChartLegend = '图例';
  scxGridChartLegendKeyBorder = '主边框';
  scxGridChartPosition = '位置';
  scxGridChartPositionDefault = '默认';
  scxGridChartPositionNone = '无';
  scxGridChartPositionLeft = '左';
  scxGridChartPositionTop = '上';
  scxGridChartPositionRight = '右';
  scxGridChartPositionBottom = '下';
  scxGridChartAlignment = '排列';
  scxGridChartAlignmentDefault = '默认';
  scxGridChartAlignmentStart = '开始';
  scxGridChartAlignmentCenter = '居中';
  scxGridChartAlignmentEnd = '结束';
  scxGridChartOrientation = '方向';
  scxGridChartOrientationDefault = '默认';
  scxGridChartOrientationHorizontal = '水平';
  scxGridChartOrientationVertical = '垂直';
  scxGridChartBorder = '边框';
  scxGridChartTitle = '标题';
  scxGridChartToolBox = '工具箱';
  scxGridChartDiagramSelector = '选择图表';
  scxGridChartOther = '其它';
  scxGridChartValueHints = '提示值';

  scxGridLayoutViewCustomizeFormOk = '确定';
  scxGridLayoutViewCustomizeFormCancel = '取消';
  scxGridLayoutViewCustomizeFormApply = '应用';
  scxGridLayoutViewCustomizeWarningDialogCaption = '警告';
  scxGridLayoutViewCustomizeWarningDialogMessage = '已更改布局。是否要保存更改？';
  scxGridLayoutViewCustomizeLayoutButtonCaption = '布局编辑器';
  scxGridLayoutViewCustomizeFormTemplateCard = '模板卡';
  scxGridLayoutViewCustomizeFormViewLayout = '视图布局';
  scxGridLayoutViewRecordCaptionDefaultMask = '第[RecordIndex]条,共[RecordCount]条]';

  scxGridLockedStateImageText = '请等待...';

  scxGridInplaceEditFormButtonCancel = '取消';
  scxGridInplaceEditFormButtonUpdate = '更新';
  scxGridInplaceEditFormSaveChangesQuery = '你的数据修改了. 是否保存修改?';

implementation

uses
  dxCore;

procedure AddcxGridResourceStringNames(AProduct: TdxProductResourceStrings);

  procedure InternalAdd(const AResourceStringName: string; AAdress: Pointer);
  begin
    AProduct.Add(AResourceStringName, AAdress);
  end;

begin
  InternalAdd('scxGridRecursiveLevels', @scxGridRecursiveLevels);
  InternalAdd('scxGridDeletingFocusedConfirmationText', @scxGridDeletingFocusedConfirmationText);
  InternalAdd('scxGridDeletingSelectedConfirmationText', @scxGridDeletingSelectedConfirmationText);
  InternalAdd('scxGridNoDataInfoText', @scxGridNoDataInfoText);
  InternalAdd('scxGridFilterRowInfoText', @scxGridFilterRowInfoText);
  InternalAdd('scxGridNewItemRowInfoText', @scxGridNewItemRowInfoText);
  InternalAdd('scxGridFindPanelClearButtonCaption', @scxGridFindPanelClearButtonCaption);
  InternalAdd('scxGridFindPanelFindButtonCaption', @scxGridFindPanelFindButtonCaption);
  InternalAdd('scxGridFindPanelInfoText', @scxGridFindPanelInfoText);
  InternalAdd('scxGridFilterIsEmpty', @scxGridFilterIsEmpty);
  InternalAdd('scxGridCustomizationFormCaption', @scxGridCustomizationFormCaption);
  InternalAdd('scxGridCustomizationFormColumnsPageCaption', @scxGridCustomizationFormColumnsPageCaption);
  InternalAdd('scxGridGroupByBoxCaption', @scxGridGroupByBoxCaption);
  InternalAdd('scxGridFilterApplyButtonCaption', @scxGridFilterApplyButtonCaption);
  InternalAdd('scxGridFilterCustomizeButtonCaption', @scxGridFilterCustomizeButtonCaption);
  InternalAdd('scxGridColumnsQuickCustomizationHint', @scxGridColumnsQuickCustomizationHint);
  InternalAdd('scxGridCustomizationFormBandsPageCaption', @scxGridCustomizationFormBandsPageCaption);
  InternalAdd('scxGridBandsQuickCustomizationHint', @scxGridBandsQuickCustomizationHint);
  InternalAdd('scxGridCustomizationFormRowsPageCaption', @scxGridCustomizationFormRowsPageCaption);
  InternalAdd('scxGridConverterIntermediaryMissing', @scxGridConverterIntermediaryMissing);
  InternalAdd('scxGridConverterNotExistGrid', @scxGridConverterNotExistGrid);
  InternalAdd('scxGridConverterNotExistComponent', @scxGridConverterNotExistComponent);
  InternalAdd('scxImportErrorCaption', @scxImportErrorCaption);
  InternalAdd('scxNotExistGridView', @scxNotExistGridView);
  InternalAdd('scxNotExistGridLevel', @scxNotExistGridLevel);
  InternalAdd('scxCantCreateExportOutputFile', @scxCantCreateExportOutputFile);
  InternalAdd('cxSEditRepositoryExtLookupComboBoxItem', @cxSEditRepositoryExtLookupComboBoxItem);
  InternalAdd('scxGridYesterday', @scxGridYesterday);
  InternalAdd('scxGridToday', @scxGridToday);
  InternalAdd('scxGridTomorrow', @scxGridTomorrow);
  InternalAdd('scxGridLast30Days', @scxGridLast30Days);
  InternalAdd('scxGridLast14Days', @scxGridLast14Days);
  InternalAdd('scxGridLast7Days', @scxGridLast7Days);
  InternalAdd('scxGridNext7Days', @scxGridNext7Days);
  InternalAdd('scxGridNext14Days', @scxGridNext14Days);
  InternalAdd('scxGridNext30Days', @scxGridNext30Days);
  InternalAdd('scxGridLastTwoWeeks', @scxGridLastTwoWeeks);
  InternalAdd('scxGridLastWeek', @scxGridLastWeek);
  InternalAdd('scxGridThisWeek', @scxGridThisWeek);
  InternalAdd('scxGridNextWeek', @scxGridNextWeek);
  InternalAdd('scxGridNextTwoWeeks', @scxGridNextTwoWeeks);
  InternalAdd('scxGridLastMonth', @scxGridLastMonth);
  InternalAdd('scxGridThisMonth', @scxGridThisMonth);
  InternalAdd('scxGridNextMonth', @scxGridNextMonth);
  InternalAdd('scxGridLastYear', @scxGridLastYear);
  InternalAdd('scxGridThisYear', @scxGridThisYear);
  InternalAdd('scxGridNextYear', @scxGridNextYear);
  InternalAdd('scxGridPast', @scxGridPast);
  InternalAdd('scxGridFuture', @scxGridFuture);
  InternalAdd('scxGridMonthFormat', @scxGridMonthFormat);
  InternalAdd('scxGridYearFormat', @scxGridYearFormat);
  InternalAdd('scxGridChartCategoriesDisplayText', @scxGridChartCategoriesDisplayText);
  InternalAdd('scxGridChartValueHintFormat', @scxGridChartValueHintFormat);
  InternalAdd('scxGridChartPercentValueTickMarkLabelFormat', @scxGridChartPercentValueTickMarkLabelFormat);
  InternalAdd('scxGridChartToolBoxDataLevels', @scxGridChartToolBoxDataLevels);
  InternalAdd('scxGridChartToolBoxDataLevelSelectValue', @scxGridChartToolBoxDataLevelSelectValue);
  InternalAdd('scxGridChartToolBoxCustomizeButtonCaption', @scxGridChartToolBoxCustomizeButtonCaption);
  InternalAdd('scxGridChartNoneDiagramDisplayText', @scxGridChartNoneDiagramDisplayText);
  InternalAdd('scxGridChartColumnDiagramDisplayText', @scxGridChartColumnDiagramDisplayText);
  InternalAdd('scxGridChartBarDiagramDisplayText', @scxGridChartBarDiagramDisplayText);
  InternalAdd('scxGridChartLineDiagramDisplayText', @scxGridChartLineDiagramDisplayText);
  InternalAdd('scxGridChartAreaDiagramDisplayText', @scxGridChartAreaDiagramDisplayText);
  InternalAdd('scxGridChartPieDiagramDisplayText', @scxGridChartPieDiagramDisplayText);
  InternalAdd('scxGridChartStackedBarDiagramDisplayText', @scxGridChartStackedBarDiagramDisplayText);
  InternalAdd('scxGridChartStackedColumnDiagramDisplayText', @scxGridChartStackedColumnDiagramDisplayText);
  InternalAdd('scxGridChartStackedAreaDiagramDisplayText', @scxGridChartStackedAreaDiagramDisplayText);
  InternalAdd('scxGridChartCustomizationFormSeriesPageCaption', @scxGridChartCustomizationFormSeriesPageCaption);
  InternalAdd('scxGridChartCustomizationFormSortBySeries', @scxGridChartCustomizationFormSortBySeries);
  InternalAdd('scxGridChartCustomizationFormNoSortedSeries', @scxGridChartCustomizationFormNoSortedSeries);
  InternalAdd('scxGridChartCustomizationFormDataGroupsPageCaption', @scxGridChartCustomizationFormDataGroupsPageCaption);
  InternalAdd('scxGridChartCustomizationFormOptionsPageCaption', @scxGridChartCustomizationFormOptionsPageCaption);
  InternalAdd('scxGridChartLegend', @scxGridChartLegend);
  InternalAdd('scxGridChartLegendKeyBorder', @scxGridChartLegendKeyBorder);
  InternalAdd('scxGridChartPosition', @scxGridChartPosition);
  InternalAdd('scxGridChartPositionDefault', @scxGridChartPositionDefault);
  InternalAdd('scxGridChartPositionNone', @scxGridChartPositionNone);
  InternalAdd('scxGridChartPositionLeft', @scxGridChartPositionLeft);
  InternalAdd('scxGridChartPositionTop', @scxGridChartPositionTop);
  InternalAdd('scxGridChartPositionRight', @scxGridChartPositionRight);
  InternalAdd('scxGridChartPositionBottom', @scxGridChartPositionBottom);
  InternalAdd('scxGridChartAlignment', @scxGridChartAlignment);
  InternalAdd('scxGridChartAlignmentDefault', @scxGridChartAlignmentDefault);
  InternalAdd('scxGridChartAlignmentStart', @scxGridChartAlignmentStart);
  InternalAdd('scxGridChartAlignmentCenter', @scxGridChartAlignmentCenter);
  InternalAdd('scxGridChartAlignmentEnd', @scxGridChartAlignmentEnd);
  InternalAdd('scxGridChartOrientation', @scxGridChartOrientation);
  InternalAdd('scxGridChartOrientationDefault', @scxGridChartOrientationDefault);
  InternalAdd('scxGridChartOrientationHorizontal', @scxGridChartOrientationHorizontal);
  InternalAdd('scxGridChartOrientationVertical', @scxGridChartOrientationVertical);
  InternalAdd('scxGridChartBorder', @scxGridChartBorder);
  InternalAdd('scxGridChartTitle', @scxGridChartTitle);
  InternalAdd('scxGridChartToolBox', @scxGridChartToolBox);
  InternalAdd('scxGridChartDiagramSelector', @scxGridChartDiagramSelector);
  InternalAdd('scxGridChartOther', @scxGridChartOther);
  InternalAdd('scxGridChartValueHints', @scxGridChartValueHints);

  InternalAdd('scxGridLayoutViewCustomizeFormOk', @scxGridLayoutViewCustomizeFormOk);
  InternalAdd('scxGridLayoutViewCustomizeFormCancel', @scxGridLayoutViewCustomizeFormCancel);
  InternalAdd('scxGridLayoutViewCustomizeFormApply', @scxGridLayoutViewCustomizeFormApply);
  InternalAdd('scxGridLayoutViewCustomizeWarningDialogCaption', @scxGridLayoutViewCustomizeWarningDialogCaption);
  InternalAdd('scxGridLayoutViewCustomizeWarningDialogMessage', @scxGridLayoutViewCustomizeWarningDialogMessage);
  InternalAdd('scxGridLayoutViewCustomizeLayoutButtonCaption', @scxGridLayoutViewCustomizeLayoutButtonCaption);
  InternalAdd('scxGridLayoutViewCustomizeFormTemplateCard', @scxGridLayoutViewCustomizeFormTemplateCard);
  InternalAdd('scxGridLayoutViewCustomizeFormViewLayout', @scxGridLayoutViewCustomizeFormViewLayout);
  InternalAdd('scxGridLayoutViewRecordCaptionDefaultMask', @scxGridLayoutViewRecordCaptionDefaultMask);

  InternalAdd('scxGridLockedStateImageText', @scxGridLockedStateImageText);

  InternalAdd('scxGridInplaceEditFormButtonCancel', @scxGridInplaceEditFormButtonCancel);
  InternalAdd('scxGridInplaceEditFormButtonUpdate', @scxGridInplaceEditFormButtonUpdate);
  InternalAdd('scxGridInplaceEditFormSaveChangesQuery', @scxGridInplaceEditFormSaveChangesQuery);
end;

initialization
  dxResourceStringsRepository.RegisterProduct('ExpressQuantumGrid', @AddcxGridResourceStringNames);

finalization
  dxResourceStringsRepository.UnRegisterProduct('ExpressQuantumGrid');

end.
