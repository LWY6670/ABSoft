{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressEditors                                           }
{                                                                    }
{           Copyright (c) 1998-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSEDITORS AND ALL                }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY. }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit cxGridPopupMenuConsts;

{$I cxVer.inc}

interface

uses
  cxClasses, cxCustomData, dxCore;

resourcestring
   cxSGridNone = '无';

  // Header popup menu item captions

  cxSGridSortColumnAsc = '升序排序';
  cxSGridSortColumnDesc = '降序排序';
  cxSGridClearSorting = '清空排序';
  cxSGridGroupByThisField = '按此字段分组';
  cxSGridRemoveThisGroupItem = '移除此分组';
  cxSGridGroupByBox = '按框分组';
  cxSGridAlignmentSubMenu = '排列';
  cxSGridAlignLeft = '左对齐';
  cxSGridAlignRight = '右对齐';
  cxSGridAlignCenter = '居中';
  cxSGridRemoveColumn = '删除此列';
  cxSGridFieldChooser = '字段选择器';
  cxSGridBestFit = '自适应';
  cxSGridBestFitAllColumns = '自适应(全部列)';
  cxSGridShowFooter = '页脚';
  cxSGridShowGroupFooter = '组页脚';

  // Footer popup menu item captions

 cxSGridNoneMenuItem = '无';
  cxSGridSumMenuItem = '总和';
  cxSGridMinMenuItem = '最小';
  cxSGridMaxMenuItem = '最大';
  cxSGridCountMenuItem = '数量';
  cxSGridAvgMenuItem = '平均';

  // Group row popup menu item captions
  cxSGridSortByGroupValues = '按分组值排序';
  cxSGridSortBySummaryCaption = '按分组汇总排序:';
  cxSGridSortBySummary = '%s 至 %s';

function GetSummaryName(ASummaryKind: TcxSummaryKind): TcxResourceStringID;

implementation

function GetSummaryName(ASummaryKind: TcxSummaryKind): TcxResourceStringID;
const
  SummaryNames: array[TcxSummaryKind] of TcxResourceStringID = (
    @cxSGridNoneMenuItem,
    @cxSGridSumMenuItem,
    @cxSGridMinMenuItem,
    @cxSGridMaxMenuItem,
    @cxSGridCountMenuItem,
    @cxSGridAvgMenuItem);
begin
  Result := SummaryNames[ASummaryKind];
end;

procedure AddcxGridPopupMenuResourceStringNames(AProduct: TdxProductResourceStrings);

  procedure InternalAdd(const AResourceStringName: string; AAdress: Pointer);
  begin
    AProduct.Add(AResourceStringName, AAdress);
  end;

begin
  InternalAdd('cxSGridNone', @cxSGridNone);
  InternalAdd('cxSGridSortColumnAsc', @cxSGridSortColumnAsc);
  InternalAdd('cxSGridSortColumnDesc', @cxSGridSortColumnDesc);
  InternalAdd('cxSGridClearSorting', @cxSGridClearSorting);
  InternalAdd('cxSGridGroupByThisField', @cxSGridGroupByThisField);
  InternalAdd('cxSGridRemoveThisGroupItem', @cxSGridRemoveThisGroupItem);
  InternalAdd('cxSGridGroupByBox', @cxSGridGroupByBox);
  InternalAdd('cxSGridAlignmentSubMenu', @cxSGridAlignmentSubMenu);
  InternalAdd('cxSGridAlignLeft', @cxSGridAlignLeft);
  InternalAdd('cxSGridAlignRight', @cxSGridAlignRight);
  InternalAdd('cxSGridAlignCenter', @cxSGridAlignCenter);
  InternalAdd('cxSGridRemoveColumn', @cxSGridRemoveColumn);
  InternalAdd('cxSGridFieldChooser', @cxSGridFieldChooser);
  InternalAdd('cxSGridBestFit', @cxSGridBestFit);
  InternalAdd('cxSGridBestFitAllColumns', @cxSGridBestFitAllColumns);
  InternalAdd('cxSGridShowFooter', @cxSGridShowFooter);
  InternalAdd('cxSGridShowGroupFooter', @cxSGridShowGroupFooter);
  InternalAdd('cxSGridNoneMenuItem', @cxSGridNoneMenuItem);
  InternalAdd('cxSGridSumMenuItem', @cxSGridSumMenuItem);
  InternalAdd('cxSGridMinMenuItem', @cxSGridMinMenuItem);
  InternalAdd('cxSGridMaxMenuItem', @cxSGridMaxMenuItem);
  InternalAdd('cxSGridCountMenuItem', @cxSGridCountMenuItem);
  InternalAdd('cxSGridAvgMenuItem', @cxSGridAvgMenuItem);
  InternalAdd('cxSGridSortByGroupValues', @cxSGridSortByGroupValues);
  InternalAdd('cxSGridSortBySummaryCaption', @cxSGridSortBySummaryCaption);
  InternalAdd('cxSGridSortBySummary', @cxSGridSortBySummary);
end;

initialization
  dxResourceStringsRepository.RegisterProduct('cxGridPopupMenu', @AddcxGridPopupMenuResourceStringNames);

finalization
  dxResourceStringsRepository.UnRegisterProduct('cxGridPopupMenu');

end.
