{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressExport                                            }
{                                                                    }
{           Copyright (c) 2001-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSEXPORT AND ALL                 }
{   ACCOMPANYING VCL AND CLX CONTROLS AS PART OF AN EXECUTABLE       }
{   PROGRAM ONLY.                                                    }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit cxExportStrs;

{$I cxVer.inc}

interface

resourcestring
  scxUnsupportedExport =  '不提供的输出类型: %1';
  scxStyleManagerKill = '样式管理器正在被使用或不能释放';
  scxStyleManagerCreate = '不能创建样式管理器';

  scxExportToHtml  = '输出到网页 (*.html)';
  scxExportToXml   = '输出到XML文档 (*.xml)';
  scxExportToText  = '输出到文本文件 (*.txt)';

  scxEmptyExportCache = '输出缓冲为空';
  scxIncorrectUnion = '不正确的单元组合';
  scxIllegalWidth = '非法的列宽';
  scxInvalidColumnRowCount = '无效的行数或列数';
  scxIllegalHeight = '非法的行高';
  scxInvalidColumnIndex = '列标 %d 超出范围';
  scxInvalidRowIndex = '行号 %d 超出范围';
  scxInvalidStyleIndex = '无效的样式索引 %d';

  scxExportToExcel = '输出到 MS Excel文件 (*.xls)';
  scxExportToXlsx  = '输出到 MS Excel 2007 (*.xlsx)';
  scxWorkbookWrite = '写 XLS 文件出错';
  scxInvalidCellDimension ='无效的单元维度';
  scxBoolTrue  = '真';
  scxBoolFalse = '假';

implementation

uses
  dxCore;

  procedure AddExpressExportResourceStringNames(AProduct: TdxProductResourceStrings);

  procedure InternalAdd(const AResourceStringName: string; AAdress: Pointer);
  begin
    AProduct.Add(AResourceStringName, AAdress);
  end;

begin
  InternalAdd('scxUnsupportedExport', @scxUnsupportedExport);
  InternalAdd('scxStyleManagerKill', @scxStyleManagerKill);
  InternalAdd('scxStyleManagerCreate', @scxStyleManagerCreate);
  InternalAdd('scxExportToHtml', @scxExportToHtml);
  InternalAdd('scxExportToXml', @scxExportToXml);
  InternalAdd('scxExportToText', @scxExportToText);
  InternalAdd('scxEmptyExportCache', @scxEmptyExportCache);
  InternalAdd('scxIncorrectUnion', @scxIncorrectUnion);
  InternalAdd('scxIllegalWidth', @scxIllegalWidth);
  InternalAdd('scxInvalidColumnRowCount', @scxInvalidColumnRowCount);
  InternalAdd('scxIllegalHeight', @scxIllegalHeight);
  InternalAdd('scxInvalidColumnIndex', @scxInvalidColumnIndex);
  InternalAdd('scxInvalidRowIndex', @scxInvalidRowIndex);
  InternalAdd('scxInvalidStyleIndex', @scxInvalidStyleIndex);
  InternalAdd('scxExportToExcel', @scxExportToExcel);
  InternalAdd('scxExportToXlsx', @scxExportToXlsx);
  InternalAdd('scxWorkbookWrite', @scxWorkbookWrite);
  InternalAdd('scxInvalidCellDimension', @scxInvalidCellDimension);
  InternalAdd('scxBoolTrue', @scxBoolTrue);
  InternalAdd('scxBoolFalse', @scxBoolFalse);
end;

initialization
  dxResourceStringsRepository.RegisterProduct('ExpressExport', @AddExpressExportResourceStringNames);

finalization
  dxResourceStringsRepository.UnRegisterProduct('ExpressExport');
  
end.
