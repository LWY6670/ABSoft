{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressSpellChecker                                      }
{                                                                    }
{           Copyright (c) 1998-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSSPELLCHECKER AND ALL           }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY. }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxSpellCheckerStrs;

{$I cxVer.inc}

interface

resourcestring

  sdxSpellCheckerMoreThanOne = '一个执行程序中只能运行一个TdxSpellChecker';
  sdxSpellCheckerFileFormatMismatch = '文件格式不匹配';
  sdxSpellCheckerISpellDictionary = 'ISpell';
  sdxSpellCheckerOpenOfficeDictionary = 'OpenOffice';
  sdxSpellCheckerUserDictionary = '用户';
  sdxSpellCheckerHunspellDictionary = 'Hunspell';

  sdxSpellCheckerApplylButton = '应用(&A)';
  sdxSpellCheckerOkButton = '确定(&O)';
  sdxSpellCheckerCancelButton = '取消';
  sdxSpellCheckerAddButton = '添加(&A)';
  sdxSpellCheckerChangeButton = '改变(&C)';
  sdxSpellCheckerEditButton = '编辑...';
  sdxSpellCheckerChangeAllButton = '改变所有(&L)';
  sdxSpellCheckerCloseButton = '关闭';
  sdxSpellCheckerDeleteButton = '删除(&D)';
  sdxSpellCheckerDeleteAllButton = '全部删除(&L)';
  sdxSpellCheckerIgnoreButton = '忽略(&I)';
  sdxSpellCheckerIgnoreAllButton = '全部忽略(&G)';
  sdxSpellCheckerOptionsButton = '选项(&O)...';
  sdxSpellCheckerSuggestButton = '建议(&S)';
  sdxSpellCheckerUndoButton = '撤销(&U)';
  sdxSpellCheckerUndoEditButton = '撤消编辑(&U)';
  sdxSpellCheckerUndoLastButton = '撤消上次操作(&U)';
  sdxSpellCheckerChangeTo = '改变为(&T):';
  sdxSpellCheckerRepeatedWord = '重复的单词:';
  sdxSpellCheckerSuggestions = '建议(&N):';
  sdxSpellCheckerNotInDictionary = '不在字典里:';
  sdxSpellCheckerNoSuggestions = '(不建议)';

  sdxSpellCheckerCustomDictionaryFormCaption = '自定义词典';
  sdxSpellCheckerSpellingFormCaption = '拼写检查';
  sdxSpellCheckerSpellingComplete = '拼写检查结束。';
  sdxSpellCheckerNoActiveDictionaries = '词典不可用';
  sdxSpellCheckerNotUseChangeAll = '更改所有选项不可用的 ' +
    '因为您已修改文本而不是真正的拼写错误 ' +
    '选择修改这句话，或撤消修改恢复原来的句子';
  sdxSpellCheckerConfirmUseUnknownWord = '您已选择在主要的或自定义词典中找不到的单词 ' +
    '。您要使用这个词并继续检查吗？';
  sdxSpellCheckerSelectionCheckIsFinished = '选择检查完毕。是否要继续检查文档的其余部分？';


  // Options
  sdxSpellCheckerSpellingOptionsFormCaption = '拼写选项';
  sdxSpellCheckerSpellingOptionsMainGroupBox = '拼写检查';
  sdxSpellCheckerSpellingLanguage = '语言:';
  sdxSpellCheckerSpellingOptionsInternationalDictionariesGroupBox = '国际字典';
  sdxSpellCheckerSpellingOptionsInternationalDictionariesText = '选择检查拼写时要使用的词典。';
  sdxSpellCheckerSpellingOptionsEditCustomDictionaryGroupBox = '编辑自定义词典';
  sdxSpellCheckerSpellingOptionsEditCustomDictionaryText = '从自定义词典中添加、修改或删除单词。';
  sdxSpellCheckerSpellingOptionsGeneralOptionsGroupBox = '常规选项';
  sdxSpellCheckerIgnoreUpperCaseWords = '忽略大写的单词';
  sdxSpellCheckerIgnoreMixedCaseWords = '忽略的单词 MiXeDcASe';
  sdxSpellCheckerIgnoreWordsWithNumbers = '忽略带数字的单词';
  sdxSpellCheckerIgnoreRepeatedWords = '忽略重复的单词';
  sdxSpellCheckerIgnoreEmails = '忽略电子邮件';
  sdxSpellCheckerIgnoreUrls = '忽略网站';

  // AutoCorrect
  sdxSpellCheckerAutoCorrect = '自动更正';
  sdxSpellCheckerAutoCorrectReplacementExistMessageFormat = '自动校正条目 %s 已存在。是否要重新定义它？';
  sdxSpellCheckerAutoCorrectOptionsFormCaption = '自动更正选项';
  sdxSpellCheckerExceptionsButton = '异常(&E)';
  sdxSpellCheckerActive = '激活(&A)';
  sdxSpellCheckerAutoInclude = '自动包含(&U)';
  sdxSpellCheckerAutoCorrectCapitalize = '大写';
  sdxSpellCheckerAutoCorrectCorrectSentenceCaps = '句首字母大写(&S)';
  sdxSpellCheckerAutoCorrectCorrectInitialCaps = '校正连续两个大写字母(&I)';
  sdxSpellCheckerAutoCorrectCorrectCapsLock = '校正意外 CAPS LOCK 键用法(&L)';
  sdxSpellCheckerAutoCorrectDisableCapsLock = '禁用Caps Lock键';
  sdxSpellCheckerAutoCorrectAutomaticallyUseSuggestions = '自动使用拼写检查器提供的建议';
  sdxSpellCheckerAutoCorrectReplaceTextAsYouType = '替换文本';
  sdxSpellCheckerAutoCorrectReplace = '替换(&R):';
  sdxSpellCheckerAutoCorrectWith = '和(&W):';
  sdxSpellCheckerReplaceButton = '替换(&A)';
  sdxSpellCheckerAutoCorrectExceptionsFormCaption = '异常';
  sdxSpellCheckerFirstLetterExceptions = '缩写 (没有后续的大写)';
  sdxSpellCheckerInitialCapsExceptions = '具有两个字母连续大写的单词';
implementation

uses
  dxCore;

procedure AddExpressSpellCheckerResourceStringNames(AProduct: TdxProductResourceStrings);

  procedure InternalAdd(const AResourceStringName: string; AAdress: Pointer);
  begin
    AProduct.Add(AResourceStringName, AAdress);
  end;

begin
  InternalAdd('sdxSpellCheckerMoreThanOne', @sdxSpellCheckerMoreThanOne);
  InternalAdd('sdxSpellCheckerFileFormatMismatch', @sdxSpellCheckerFileFormatMismatch);
  InternalAdd('sdxSpellCheckerISpellDictionary', @sdxSpellCheckerISpellDictionary);
  InternalAdd('sdxSpellCheckerOpenOfficeDictionary', @sdxSpellCheckerOpenOfficeDictionary);
  InternalAdd('sdxSpellCheckerUserDictionary', @sdxSpellCheckerUserDictionary);
  InternalAdd('sdxSpellCheckerHunspellDictionary', @sdxSpellCheckerHunspellDictionary);
  InternalAdd('sdxSpellCheckerApplylButton', @sdxSpellCheckerApplylButton);
  InternalAdd('sdxSpellCheckerOkButton', @sdxSpellCheckerOkButton);
  InternalAdd('sdxSpellCheckerCancelButton', @sdxSpellCheckerCancelButton);
  InternalAdd('sdxSpellCheckerAddButton', @sdxSpellCheckerAddButton);
  InternalAdd('sdxSpellCheckerChangeButton', @sdxSpellCheckerChangeButton);
  InternalAdd('sdxSpellCheckerEditButton', @sdxSpellCheckerEditButton);
  InternalAdd('sdxSpellCheckerChangeAllButton', @sdxSpellCheckerChangeAllButton);
  InternalAdd('sdxSpellCheckerCloseButton', @sdxSpellCheckerCloseButton);
  InternalAdd('sdxSpellCheckerDeleteButton', @sdxSpellCheckerDeleteButton);
  InternalAdd('sdxSpellCheckerDeleteAllButton', @sdxSpellCheckerDeleteAllButton);
  InternalAdd('sdxSpellCheckerIgnoreButton', @sdxSpellCheckerIgnoreButton);
  InternalAdd('sdxSpellCheckerIgnoreAllButton', @sdxSpellCheckerIgnoreAllButton);
  InternalAdd('sdxSpellCheckerOptionsButton', @sdxSpellCheckerOptionsButton);
  InternalAdd('sdxSpellCheckerSuggestButton', @sdxSpellCheckerSuggestButton);
  InternalAdd('sdxSpellCheckerUndoButton', @sdxSpellCheckerUndoButton);
  InternalAdd('sdxSpellCheckerUndoEditButton', @sdxSpellCheckerUndoEditButton);
  InternalAdd('sdxSpellCheckerUndoLastButton', @sdxSpellCheckerUndoLastButton);
  InternalAdd('sdxSpellCheckerChangeTo', @sdxSpellCheckerChangeTo);
  InternalAdd('sdxSpellCheckerRepeatedWord', @sdxSpellCheckerRepeatedWord);
  InternalAdd('sdxSpellCheckerSuggestions', @sdxSpellCheckerSuggestions);
  InternalAdd('sdxSpellCheckerNotInDictionary', @sdxSpellCheckerNotInDictionary);
  InternalAdd('sdxSpellCheckerNoSuggestions', @sdxSpellCheckerNoSuggestions);
  InternalAdd('sdxSpellCheckerCustomDictionaryFormCaption', @sdxSpellCheckerCustomDictionaryFormCaption);
  InternalAdd('sdxSpellCheckerSpellingFormCaption', @sdxSpellCheckerSpellingFormCaption);
  InternalAdd('sdxSpellCheckerSpellingComplete', @sdxSpellCheckerSpellingComplete);
  InternalAdd('sdxSpellCheckerNoActiveDictionaries', @sdxSpellCheckerNoActiveDictionaries);
  InternalAdd('sdxSpellCheckerNotUseChangeAll', @sdxSpellCheckerNotUseChangeAll);
  InternalAdd('sdxSpellCheckerConfirmUseUnknownWord', @sdxSpellCheckerConfirmUseUnknownWord);
  InternalAdd('sdxSpellCheckerSelectionCheckIsFinished', @sdxSpellCheckerSelectionCheckIsFinished);
  InternalAdd('sdxSpellCheckerSpellingOptionsFormCaption', @sdxSpellCheckerSpellingOptionsFormCaption);
  InternalAdd('sdxSpellCheckerSpellingOptionsMainGroupBox', @sdxSpellCheckerSpellingOptionsMainGroupBox);
  InternalAdd('sdxSpellCheckerSpellingLanguage', @sdxSpellCheckerSpellingLanguage);
  InternalAdd('sdxSpellCheckerSpellingOptionsInternationalDictionariesGroupBox', @sdxSpellCheckerSpellingOptionsInternationalDictionariesGroupBox);
  InternalAdd('sdxSpellCheckerSpellingOptionsInternationalDictionariesText', @sdxSpellCheckerSpellingOptionsInternationalDictionariesText);
  InternalAdd('sdxSpellCheckerSpellingOptionsEditCustomDictionaryGroupBox', @sdxSpellCheckerSpellingOptionsEditCustomDictionaryGroupBox);
  InternalAdd('sdxSpellCheckerSpellingOptionsEditCustomDictionaryText', @sdxSpellCheckerSpellingOptionsEditCustomDictionaryText);
  InternalAdd('sdxSpellCheckerSpellingOptionsGeneralOptionsGroupBox', @sdxSpellCheckerSpellingOptionsGeneralOptionsGroupBox);
  InternalAdd('sdxSpellCheckerIgnoreUpperCaseWords', @sdxSpellCheckerIgnoreUpperCaseWords);
  InternalAdd('sdxSpellCheckerIgnoreMixedCaseWords', @sdxSpellCheckerIgnoreMixedCaseWords);
  InternalAdd('sdxSpellCheckerIgnoreWordsWithNumbers', @sdxSpellCheckerIgnoreWordsWithNumbers);
  InternalAdd('sdxSpellCheckerIgnoreRepeatedWords', @sdxSpellCheckerIgnoreRepeatedWords);
  InternalAdd('sdxSpellCheckerIgnoreEmails', @sdxSpellCheckerIgnoreEmails);
  InternalAdd('sdxSpellCheckerIgnoreUrls', @sdxSpellCheckerIgnoreUrls);
  InternalAdd('sdxSpellCheckerAutoCorrect', @sdxSpellCheckerAutoCorrect);
  InternalAdd('sdxSpellCheckerAutoCorrectOptionsFormCaption', @sdxSpellCheckerAutoCorrectOptionsFormCaption);
  InternalAdd('sdxSpellCheckerExceptionsButton', @sdxSpellCheckerExceptionsButton);
  InternalAdd('sdxSpellCheckerActive', @sdxSpellCheckerActive);
  InternalAdd('sdxSpellCheckerAutoInclude', @sdxSpellCheckerAutoInclude);
  InternalAdd('sdxSpellCheckerAutoCorrectReplacementExistMessageFormat', @sdxSpellCheckerAutoCorrectReplacementExistMessageFormat);
  InternalAdd('sdxSpellCheckerAutoCorrectCapitalize', @sdxSpellCheckerAutoCorrectCapitalize);
  InternalAdd('sdxSpellCheckerAutoCorrectCorrectSentenceCaps', @sdxSpellCheckerAutoCorrectCorrectSentenceCaps);
  InternalAdd('sdxSpellCheckerAutoCorrectCorrectInitialCaps', @sdxSpellCheckerAutoCorrectCorrectInitialCaps);
  InternalAdd('sdxSpellCheckerAutoCorrectCorrectCapsLock', @sdxSpellCheckerAutoCorrectCorrectCapsLock);
  InternalAdd('sdxSpellCheckerAutoCorrectDisableCapsLock', @sdxSpellCheckerAutoCorrectDisableCapsLock);
  InternalAdd('sdxSpellCheckerAutoCorrectReplaceTextAsYouType', @sdxSpellCheckerAutoCorrectReplaceTextAsYouType);
  InternalAdd('sdxSpellCheckerAutoCorrectAutomaticallyUseSuggestions', @sdxSpellCheckerAutoCorrectAutomaticallyUseSuggestions);
  InternalAdd('sdxSpellCheckerAutoCorrectReplace', @sdxSpellCheckerAutoCorrectReplace);
  InternalAdd('sdxSpellCheckerAutoCorrectWith', @sdxSpellCheckerAutoCorrectWith);
  InternalAdd('sdxSpellCheckerReplaceButton', @sdxSpellCheckerReplaceButton);
  InternalAdd('sdxSpellCheckerAutoCorrectExceptionsFormCaption', @sdxSpellCheckerAutoCorrectExceptionsFormCaption);
  InternalAdd('sdxSpellCheckerFirstLetterExceptions', @sdxSpellCheckerFirstLetterExceptions);
  InternalAdd('sdxSpellCheckerInitialCapsExceptions', @sdxSpellCheckerInitialCapsExceptions);
end;

initialization
  dxResourceStringsRepository.RegisterProduct('ExpressSpellChecker', @AddExpressSpellCheckerResourceStringNames);

finalization
  dxResourceStringsRepository.UnRegisterProduct('ExpressSpellChecker');

end.
