{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressPivotGrid                                         }
{                                                                    }
{           Copyright (c) 2005-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSPIVOTGRID AND ALL ACCOMPANYING }
{   VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY.              }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit cxPivotGridStrs;

{$I cxVer.inc}

interface

uses
  cxCustomPivotGrid;

resourcestring

  scxDataField  = '数据';
  //
  scxDropFilterFields = '将过滤字段拖至此处';
  scxDropDataItems    = '将数据字段拖至此处';
  scxDropRowFields    = '将行字段拖至此处';
  scxDropColumnFields = '将列字段拖至此处';
  scxGrandTotal       = '总计';
  scxNoDataToDisplay  = '<无可显示的数据>';

  // field list strings
  scxAddTo            = '添加到';
  scxDragItems        = '拖动项目到 PivotGrid';
  scxFieldListCaption = 'PivotGrid 字段列表';
  scxMeasureGroups    = '度量值组';

  scxRowArea    = '行区域';
  scxColumnArea = '列区域';
  scxFilterArea = '过滤区域';
  scxDataArea   = '数据区域';

  // group strings 
  scxGroupTotal      = '%s 总计';
  scxGroupCount      = '%s 计数值';
  scxGroupSum        = '%s 小计';
  scxGroupMin        = '%s 最小值';
  scxGroupMax        = '%s 最大值';
  scxGroupAverage    = '%s 平均值';
  scxGroupStdDev     = '%s 标准偏差值';
  scxGroupStdDevP    = '%s 总体标准偏差';
  scxGroupVariance   = '%s 变量';
  scxGroupVarianceP  = '%s 变量指针';
  scxGroupCustom     = '%s 自定义';

  scxOthers           = '其他的';


  // filter strings
  scxPivotGridShowAll = '(全部显示)';
  scxPivotGridOk      = '确定';
  scxPivotGridCancel  = '取消';

  // intervals

  scxQuarterFormat = '季度 %d';

  // pivot grid exception

  scxFieldNotADataField = '字段必须在数据区域中！';
  scxInvalidLayout  = '无效的布局！';
  scxNotImplemented =  '尚未实现！';

  // pivot grid pupup menu items
  scxSeparator       = '-';

  scxHide            = '隐藏';
  scxOrder           = '顺序';
  scxMoveToBeginning = '移至开头';
  scxMoveToEnd       = '移至尾部';
  scxMoveToLeft      = '向左移动';
  scxMoveToRight     = '向右移动';
  //
 
  scxExpand          = '展开';
  scxCollapse        = '折叠';
  scxExpandAll       = '全部展开';
  scxCollapseAll     = '全部折叠';
  // misc. commands
  scxShowCustomization = '显示字段列表';
  scxHideCustomization = '隐藏字段列表';
  // prefilter commands
  scxShowPrefilterDialog = '显示预过滤对话框';

  scxSortGroupByThisColumn = 'Sort "%s" by This Column';
  scxSortGroupByThisRow    = 'Sort "%s" by This Row';
  scxRemoveAllSorting      = 'Remove All Sorting';


  // prefilter
  scxPrefilterIsEmpty = '<预过滤器为空>';
  scxPrefilterCustomizeButtonCaption = '预过滤器...';


  // connection designer
  scxConnectUsing   = '连接使用';
  scxAnalysisServer = '分析服务器';
  scxCubeFile       = 'Cube 文件';
  scxFile           = '文件';
  scxServer         = '服务器';
  scxDatabase       = '数据库';
  scxCube           = 'Cube';
  scxMeasures       = '措施';
  scxKPIs           = 'KPIs';
  scxSets           = 'Sets';


  scxUnsupportedProviderVersion = '不支持的数据提供程序版本： %d';
  scxInvalidCubeName = '无效的%sCube名称。';  

  scxInvalidProviderVersion = '提供程序版本不匹配的 OLAP 数据源版本';

  // Advanced Customization Form
  scxUpdate = '更新';
  scxDeferLayoutUpdate = '推迟布局更新';
  scxAdvancedCustomizationFormMainCaption = '将字段拖到数据透视表格';
  scxAdvancedCustomizationFormFieldsCaption = '在下面的区域之间拖动字段:';
  scxAdvancedCustomizationFormFilterAreaCaption = '过滤区域';
  scxAdvancedCustomizationFormColumnAreaCaption = '列区域';
  scxAdvancedCustomizationFormRowAreaCaption = '行区域';
  scxAdvancedCustomizationFormDataAreaCaption = '数据区域';
  scxAdvancedCustomizationFormBottomPanelOnly1by4 = '仅区域部分 (1 by 4)';
  scxAdvancedCustomizationFormBottomPanelOnly2by2 = '仅区域部分 (2 by 2)';
  scxAdvancedCustomizationFormStackedDefault = '字段和区域部分层叠';
  scxAdvancedCustomizationFormStackedSideBySide = '字段和区域部分并排';
  scxAdvancedCustomizationFormTopPanelOnly = '仅字段部分';
  // KPI hints
  scxKPIStatusBad = 'Bad';
  scxKPIStatusNeutral = 'Neutral';
  scxKPIStatusGood = 'Good';
  scxKPITrendGoingDown = 'Going Down';
  scxKPITrendNoChange = '不改变';
  scxKPITrendGoingUp = 'Going Up';

const
  scxCustomization: array[Boolean] of Pointer =
    (@scxShowCustomization, @scxHideCustomization);
  scxExpandCollapse: array[Boolean] of Pointer =
    (@scxExpand, @scxCollapse);

  TotalDescriptions: array[TcxPivotGridSummaryType] of Pointer =
    (@scxGroupCount, @scxGroupSum, @scxGroupMin, @scxGroupMax, @scxGroupAverage,
     @scxGroupStdDev, @scxGroupStdDevP, @scxGroupVariance, @scxGroupVarianceP, @scxGroupCustom);

  scxOLAPFilterCaption = 'OLAP过滤器';
  scxOLAPTotalCaption = 'OLAP Grand Total';

implementation

uses
  dxCore;

procedure AddExpressPivotGridResourceStringNames(AProduct: TdxProductResourceStrings);

  procedure InternalAdd(const AResourceStringName: string; AAdress: Pointer);
  begin
    AProduct.Add(AResourceStringName, AAdress);
  end;

begin
  InternalAdd('scxDataField', @scxDataField);
  InternalAdd('scxDropFilterFields', @scxDropFilterFields);
  InternalAdd('scxDropDataItems', @scxDropDataItems);
  InternalAdd('scxDropRowFields', @scxDropRowFields);
  InternalAdd('scxDropColumnFields', @scxDropColumnFields);
  InternalAdd('scxGrandTotal', @scxGrandTotal);
  InternalAdd('scxNoDataToDisplay', @scxNoDataToDisplay);
  InternalAdd('scxAddTo', @scxAddTo);
  InternalAdd('scxDragItems', @scxDragItems);
  InternalAdd('scxFieldListCaption', @scxFieldListCaption);
  InternalAdd('scxMeasureGroups', @scxMeasureGroups);
  InternalAdd('scxRowArea', @scxRowArea);
  InternalAdd('scxColumnArea', @scxColumnArea);
  InternalAdd('scxFilterArea', @scxFilterArea);
  InternalAdd('scxDataArea', @scxDataArea);
  InternalAdd('scxGroupTotal', @scxGroupTotal);
  InternalAdd('scxGroupCount', @scxGroupCount);
  InternalAdd('scxGroupSum', @scxGroupSum);
  InternalAdd('scxGroupMin', @scxGroupMin);
  InternalAdd('scxGroupMax', @scxGroupMax);
  InternalAdd('scxGroupAverage', @scxGroupAverage);
  InternalAdd('scxGroupStdDev', @scxGroupStdDev);
  InternalAdd('scxGroupStdDevP', @scxGroupStdDevP);
  InternalAdd('scxGroupVariance', @scxGroupVariance);
  InternalAdd('scxGroupVarianceP', @scxGroupVarianceP);
  InternalAdd('scxGroupCustom', @scxGroupCustom);
  InternalAdd('scxOthers', @scxOthers);
  InternalAdd('scxPivotGridShowAll', @scxPivotGridShowAll);
  InternalAdd('scxPivotGridOk', @scxPivotGridOk);
  InternalAdd('scxPivotGridCancel', @scxPivotGridCancel);
  InternalAdd('scxQuarterFormat', @scxQuarterFormat);
  InternalAdd('scxFieldNotADataField', @scxFieldNotADataField);
  InternalAdd('scxInvalidLayout', @scxInvalidLayout);
  InternalAdd('scxNotImplemented', @scxNotImplemented);
  InternalAdd('scxSeparator', @scxSeparator);
  InternalAdd('scxHide', @scxHide);
  InternalAdd('scxOrder', @scxOrder);
  InternalAdd('scxMoveToBeginning', @scxMoveToBeginning);
  InternalAdd('scxMoveToEnd', @scxMoveToEnd);
  InternalAdd('scxMoveToLeft', @scxMoveToLeft);
  InternalAdd('scxMoveToRight', @scxMoveToRight);
  InternalAdd('scxExpand', @scxExpand);
  InternalAdd('scxCollapse', @scxCollapse);
  InternalAdd('scxExpandAll', @scxExpandAll);
  InternalAdd('scxCollapseAll', @scxCollapseAll);
  InternalAdd('scxShowCustomization', @scxShowCustomization);
  InternalAdd('scxHideCustomization', @scxHideCustomization);
  InternalAdd('scxShowPrefilterDialog', @scxShowPrefilterDialog);
  InternalAdd('scxSortGroupByThisColumn', @scxSortGroupByThisColumn);
  InternalAdd('scxSortGroupByThisRow', @scxSortGroupByThisRow);
  InternalAdd('scxRemoveAllSorting', @scxRemoveAllSorting);
  InternalAdd('scxPrefilterIsEmpty', @scxPrefilterIsEmpty);
  InternalAdd('scxPrefilterCustomizeButtonCaption', @scxPrefilterCustomizeButtonCaption);
  InternalAdd('scxConnectUsing', @scxConnectUsing);
  InternalAdd('scxAnalysisServer', @scxAnalysisServer);
  InternalAdd('scxCubeFile', @scxCubeFile);
  InternalAdd('scxFile', @scxFile);
  InternalAdd('scxServer', @scxServer);
  InternalAdd('scxDatabase', @scxDatabase);
  InternalAdd('scxCube', @scxCube);
  InternalAdd('scxMeasures', @scxMeasures);
  InternalAdd('scxKPIs', @scxKPIs);
  InternalAdd('scxSets', @scxSets);
  InternalAdd('scxUnsupportedProviderVersion', @scxUnsupportedProviderVersion);
  InternalAdd('scxInvalidCubeName', @scxInvalidCubeName);
  InternalAdd('scxInvalidProviderVersion', @scxInvalidProviderVersion);
  InternalAdd('scxUpdate', @scxUpdate);
  InternalAdd('scxDeferLayoutUpdate', @scxDeferLayoutUpdate);
  InternalAdd('scxAdvancedCustomizationFormMainCaption', @scxAdvancedCustomizationFormMainCaption);
  InternalAdd('scxAdvancedCustomizationFormFieldsCaption', @scxAdvancedCustomizationFormFieldsCaption);
  InternalAdd('scxAdvancedCustomizationFormFilterAreaCaption', @scxAdvancedCustomizationFormFilterAreaCaption);
  InternalAdd('scxAdvancedCustomizationFormColumnAreaCaption', @scxAdvancedCustomizationFormColumnAreaCaption);
  InternalAdd('scxAdvancedCustomizationFormRowAreaCaption', @scxAdvancedCustomizationFormRowAreaCaption);
  InternalAdd('scxAdvancedCustomizationFormDataAreaCaption', @scxAdvancedCustomizationFormDataAreaCaption);
  InternalAdd('scxAdvancedCustomizationFormBottomPanelOnly1by4', @scxAdvancedCustomizationFormBottomPanelOnly1by4);
  InternalAdd('scxAdvancedCustomizationFormBottomPanelOnly2by2', @scxAdvancedCustomizationFormBottomPanelOnly2by2);
  InternalAdd('scxAdvancedCustomizationFormStackedDefault', @scxAdvancedCustomizationFormStackedDefault);
  InternalAdd('scxAdvancedCustomizationFormStackedSideBySide', @scxAdvancedCustomizationFormStackedSideBySide);
  InternalAdd('scxAdvancedCustomizationFormTopPanelOnly', @scxAdvancedCustomizationFormTopPanelOnly);
  InternalAdd('scxKPIStatusBad', @scxKPIStatusBad);
  InternalAdd('scxKPIStatusNeutral', @scxKPIStatusNeutral);
  InternalAdd('scxKPIStatusGood', @scxKPIStatusGood);
  InternalAdd('scxKPITrendGoingDown', @scxKPITrendGoingDown);
  InternalAdd('scxKPITrendNoChange', @scxKPITrendNoChange);
  InternalAdd('scxKPITrendGoingUp', @scxKPITrendGoingUp);

end;

initialization
  dxResourceStringsRepository.RegisterProduct('ExpressPivotGrid', @AddExpressPivotGridResourceStringNames);

finalization
  dxResourceStringsRepository.UnRegisterProduct('ExpressPivotGrid');

end.
