{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressScheduler                                         }
{                                                                    }
{           Copyright (c) 2003-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSSCHEDULER AND ALL ACCOMPANYING }
{   VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY.              }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit cxSchedulerStrs;

{$I cxVer.inc}

interface

resourcestring
  scxUntitledEvent = '无标题事件';

  scxVertical   = '垂直';
  scxHorizontal = '水平';
  scxTimeGrid   = '时间格';

  scxMinute  = '分钟';
  scxMinutes = '分钟';
  scxHour    = '小时';
  scxHours   = '小时';
  scxOneDay  = '一天';

  // Navigation buttons
  scxNextAppointment = '下一个事件';
  scxPrevAppointment = '上一个事件'; 

  // dialogs constants
  scxDeleteRecurringEventDescription = '是重复事件。您希望仅删除本次或所有的事件？';
  scxEditRecurringEventDescription   = '是重复事件。您希望仅打开本次或所有的事件？';

  scxGoToDateDialogCaption     = '转到日期';
  scxDeleteTypeDialogCaption   = '确认删除';
  scxDeleteTypeOccurrenceLabel = '删除本次';
  scxDeleteTypeSeriesLabel     = '删除序列';
  scxEditTypeDialogCaption     = '打开重复项目';
  scxEditTypeOccurrenceLabel   = '打开事件';
  scxEditTypeSeriesLabel       = '打开序列';

  scxExitConfirmation  = '保存修改吗？';
  scxDeleteConfirmation= '项目已改变，确定要删除吗？';
  scxWrongTimeBounds   = '结束日期早于起始日期。';
  scxWrongPattern      = '周期模式不合法。';
  scxReplaceOccurrenceDate = '某些月份不足%s天，对于这些月份，事件将被安排在该月的最后一天。';
  scxInvalidRecurrenceDuration = '事件持续时间必须小于重复间隔。 ' +
    '缩短持续时间，或更改对话框中的定期模式。';
  scxConfirmLostExceptions = '与此定期事件相关联的任何例外将会丢失。确定吗？';
  scxInvalidNumber      = '您必须输入一个有效的数字。';
  scxShedulerEditorFormNotRegistered = 'There is no registered editor form';
  scxNoAvailableFreeTime = '没有空余时间。';


  scxCannotRescheduleOccurrence = 'Cannot reschedule an occurrence of the recurring appointment "%s" if it skips over a later occurrence of the same appointment.';
  scxTwoOccurrencesPerDay = 'Two occurrences of "%s" cannot occur on the same day.';
  scxActionClose       = '关闭';
  scxEvent             = '事件';
  scxUntitled          = '无标题';

  scxNoneEvent         = '普通事件';
  scxRecurrenceEvent   = '重复事件';
  scxExceptionEvent    = '例外事件';
  scxOccurenceEvent    = '触发事件';

  scxAdd               = '添加(&A)';
  scxEdit              = '编辑(&E)';
  scxDelete            = '删除(&D)';
  scxRecurrence        = '重复(&R)';

  scxDate              = '日期(&D):';
  scxShowIn            = '显示在(&S):';
  scxDayCalendar       = '日历';
  scxWeekCalendar      = '周历';
  scxMonthCalendar     = '月历';
  scxWorkWeekCalendar  = '工作周历';


  scxEventsConflict    = '与另一个在您的日程里安排的事件冲突。';
  scxResource          = '资源';
  scxSubject           = '主题(&S):';
  scxLocation          = '位置(&L):';
  scxLabel             = '标签(&B):';
  scxStartTime         = '起始时间(&T):';
  scxEndTime           = '结束时间(&E):';
  scxAllDayEvent       = '全天事件(&A)';
  scxRecurrenceLabel   = '重复:';
  scxLabelAs           = 'Label As:';

  scxReminder          = '提醒:';
  scxShowTimeAs        = '时间显示为(&W):';

  scxShowAs            = 'Show As:';

 scxSuffixMinute      = '分钟';
  scxSuffixMinutes     = '分钟';
  scxSuffixHour        = '小时';
  scxSuffixHours       = '小时';
  scxSuffixDay         = '天';
  scxSuffixDays        = '天';
  scxSuffixWeek        = '周';
  scxSuffixWeeks       = '周';


  scxBusy              = '忙';
  scxFree              = '释放';
  scxTentative         = '暂定';
  scxOutOfOffice       = '外出';

  scxRecurrenceCaption = '事件周期';
  scxRecurrenceHolidayCaption  = 'Holiday recurrence';

  scxEventTime         = ' 事件时间 ';
  scxRecurrencePattern = ' 循环模式 ';
  scxRangeOfRecurrence = ' 重复范围 ';
  scxStart             = '开始(&S):';
  scxStart1            = '开始(&T):';
  scxEnd               = '结束(&E):';
  scxDuration          = '持续时间(&U):';
  // Pattern
  scxDaily             = '日(&D)';
  scxWeekly            = '周(&W)';
  scxQuarterly         = '季度(&Q)';
  scxMonthly           = '月(&M)';
  scxYearly            = '年(&Y)';
  // daily
   scxEvery             = '每(&V)';
  scxEveryWeekDay      = '每周(&K)';
  scxDays              = '天';
  // weekly
  scxWeeksOn           = '周后的:';
  scxRecurEvery        = '重复间隔为(&U)';
  //monthly
  scxOfEvery           = '每';
  scxMonths            = '月';
  // yearly
  scxThe               = '在(&H)';
  scxOf                = '共';

  // Task links

  scxTaskComplete                   = '任务完成(&M):';
  scxTaskStatus                     = '任务状态';
  scxTaskDependencyEditorCaption    = '任务依赖';
  scxTaskWrongTimeBounds            = '你要输入一个在 %s - %s 之间的新日期。';
  scxFinishToFinishLong   = '完成至完成 (FF)';
  scxFinishToStartLong    = '完成至开始 (FS)';
  scxFrom                 = '自:';
  scxStartToFinishLong    = '开始至完成 (SF)';
  scxStartToStartLong     = '开始至开始 (SS)';
  scxTo                   = '到:';
  scxType                 = '类型(&T):';

  // other
   scxFirst             = '第一';
  scxSecond            = '第二';
  scxThird             = '第三';
  scxFourth            = '第四';
  scxLast              = '最后';
  scxDay               = '天(&A)';
  scxDay1              = '天';
  scxWeekday           = '周日';
  scxWeekendday        = '周末';
  scxNoEndDate         = '无结束日期(&N)';
  scxEndAfter          = '重复(&F):';
  scxEndBy             = '结束(&B):';
  scxOccurences        = '次后结束';

  // buttons
  scxAdd1              = '添加';
  scxAdd1Hint          = '添加(Ins)';
  scxEditDotted        = '编辑...';
  scxApply             = '应用(&A)';
  scxFindAvailableTime = '查找空闲时间';
  scxOk                = '确定(&O)';
  scxSaveAndClose      = '保存并关闭';
  scxSaveAndCloseHint  = '保存并关闭';
  scxSave              = '保存';
  scxCancel            = '取消(&C)';
  scxClose             = '关闭(&C)';
  scxDown              = '下(&D)';
  scxDelete1           = '删除';
  scxDelete1Hint       = '删除(Del)';
  scxEdit1             = '修改';
  scxImport            = '导入(&I)';
  scxExport            = '导出(&E)';
  scxRemoveRecur       = '删除周期(&R)';
  scxSelectAll         = '全选(&A)';
  scxSelectNone        = '取消选择(&N)';
  scxImportHint        = '输入';
  scxExportHint        = '输出';  
  scxUp                = '上(&U)';
  //
  scxResourceLayoutCaption = '周期编辑';

 // popup menu resources
  scxpmNewEvent          = '新事件(&N)';
  scxpmNewAllDayEvent    = '新全天事件(&E)';
  scxpmNewRecurringEvent = '新周期事件(&R)';
  scxpmToday             = '今天(&O)';
  scxpmGotoThisDay       = '转到这天(&D)';
  scxpmGoToDate          = '转到日期(&T)...';
  scxpmResourcesLayout   = '周期编辑...';

  // for event
  scxpmOpen              = '打开(&O)';
  scxpmEditSeries        = '编辑序列(&R)';
  scxpmShowTimeAs        = '时间显示为(&H)';
  scxpmDelete            = '删除(&D)';
  scxpmFree              = '闲(&F)';
  scxpmTentative         = '暂定(&T)';
  scxpmBusy              = '忙(&B)';
  scxpmOutOfOffice       = '外出(&O)';
  scxpmLabel             = '标签(&L)';

  // event label captions
 scxEventLabelNone      = '无';
  scxEventLabel0         = '重要';
  scxEventLabel1         = '商业';
  scxEventLabel2         = '个人';
  scxEventLabel3         = '休假';
  scxEventLabel4         = '必须参加';
  scxEventLabel5         = '必须转发';
  scxEventLabel6         = '务必准备';
  scxEventLabel7         = '生日';
  scxEventLabel8         = '纪念';
  scxEventLabel9         = '电话';
  // for time ruler menu items
 scxpmTimeZone          = '改变时区(&G)';
  scxpm60Minutes         = '60 分(&0)';
  scxpm30Minutes         = '30 分(&3)';
  scxpm15Minutes         = '15 分(&1)';
  scxpm10Minutes         = '10 分(&M)';
  scxpm6Minutes          = '6 分(&6)';
  scxpm5Minutes          = '5 分(&5)';


  // for year view scale menu items
   scxpmFullYear          = '全年(&F)';
  scxpmHalfYear          = '半年(&H)';
  scxpmQuarter           = '季度(&Q)';
  // year view scales
  scxFullYear            = '全年';
  scxHalfYear            = '半年';
  scxQuarter             = '季度';
  scxHalfYearShort       = 'H';
  scxQuarterShort        = 'Q';

  //navigator hints
  scxFirstButtonHint     = '第一个资源';
  scxPrevPageButtonHint  = '上一页';
  scxPrevButtonHint      = '前一资源';
  scxNextButtonHint      = '下一个资源';
  scxNextPageButtonHint  = '下一页';
  scxLastButtonHint      = '最后的资源';
  scxShowMoreResourcesButtonHint  = '显示更多资源';
  scxShowFewerResourcesButtonHint = '显示更少资源';

  //for reminder
  scxrCaptionReminder  = '提醒';
  scxrCaptionReminders = '%d 个提醒';
  scxrDismissButton    = '解散(&D)';
  scxrDismissAllButton = '解散所有(&A)';
  scxrDueIn            = '预期在';
  scxrOpenItemButton   = '打开项目(&O)';
  scxrSnoozeButton     = '稍后(&S)';
  scxrSubject          = '主题';
  scxrSnoozeLabel      = '稍后(&S)';
  scxrSelected         = '选择 %d 个提醒';
  scxrStartTime        = '起始时间： %s';


  // time
  scxTime0m     = '0 分';
  scxTime5m     = '5 分';
  scxTime10m    = '10 分';
  scxTime15m    = '15 分';
  scxTime20m    = '20 分';
  scxTime30m    = '30 分';
  scxTime1h     = '1 小时';
  scxTime2h     = '2 小时';
  scxTime3h     = '3 小时';
  scxTime4h     = '4 小时';
  scxTime5h     = '5 小时';
  scxTime6h     = '6 小时';
  scxTime7h     = '7 小时';
  scxTime8h     = '8 小时';
  scxTime9h     = '9 小时';
  scxTime10h    = '10 小时';
  scxTime11h    = '11 小时';
  scxTime12h    = '12 小时';
  scxTime18h    = '18 小时';
  scxTime1d     = '1 天';
  scxTime2d     = '2 天';
  scxTime3d     = '3 天';
  scxTime4d     = '4 天';
  scxTime1w     = '1 周';
  scxTime2w     = '2 周';
  // advance time
  scxAdvance0h  = '离开始还有1小时';
  scxAdvance5m  = '离开始还有5分钟';
  scxAdvance10m = '离开始还有10分钟';
  scxAdvance15m = '离开始还有15分钟';

  // for export

  secxSetDateRangeCaption = '设置日期范围';
  secxSetDateRangeText = '导出并创建单个的约会或任务之间 ' +
    '发生的事件:';
  secxSetDateRangeAnd = '并且';
  secxTrue = '真';
  secxFalse = '假';
  secxExportStorageInvalid = '没有指派存储';


  // card field names

  secxYes         = '是';
  secxNo          = '否';
  secxSubject     = '主题';
  secxLocation    = '位置';
  secxDescription = '描述';
  secxAllDay      = '全天';
  secxStart       = '开始';
  secxFinish      = '完成';
  secxState       = '状态';
  secxReminder    = '提醒';

  // table fields

  secxStartDate          = '起始日期';
  secxStartTime          = '起始时间';
  secxEndDate            = '结束日期';
  secxEndTime            = '结束时间';
  secxAlldayevent        = '全天事件';
  secxReminderonoff      = '提醒开关';
  secxReminderDate       = '提醒日期';
  secxReminderTime       = '提醒时间';
  secxCategories         = '分类';
  secxShowtimeas         = '时间显示';

  // storage
  scxRequiredFieldsNeeded = '必要字段'#13#10'%s未分配值！';
  scxInvalidFieldName = '非法字段名';
  scxInvalidCustomField = '非法的自定义字段';


  // Event fields
  scxAllDayEventField = '全天事件'; 
  scxIDField = '编号';
  scxActualFinishField = '实际完成';
  scxActualStartField = '实际开始';
  scxCaptionField = '标题';
  scxEnabledField = '启用';
  scxEventTypeField = '类型';
  scxFinishField = '完成';
  scxLabelField = '标签';
  scxLocationField = '位置';
  scxMessageField = '消息';
  scxParentIDField = '父组件ID';
  scxGroupIDField = '分组编号';
  scxRecurrenceField = '循环模式';
  scxRecurrenceIndexField = '重复索引';
  scxReminderDateField = '提醒日期';
  scxReminderField = '提醒';
  scxReminderMinutesBeforeStartField = '在开始之前的提醒时间 （分钟）';
  scxResourceField = '资源';
  scxStartField = '开始';
  scxStateField = '状态';
  scxTaskCompleteField = '任务完成';
  scxTaskIndexField = '任务索引'; 
  scxTaskLinksField = '任务链接';
  scxTaskStatusField = '任务状态';
  scxActionRecurrence  = 'Recurrence';
  // status  
  scxNotStarted = '未开始';
  scxInProgress = '正在进行';
  scxComplete   = '完成';
  scxWaiting    = '正在等待';
  scxDeferred   = '推迟';


  // Event task relations

 scxFinishToStart  = '完成至开始';
  scxStartToStart   = '开始至开始';
  scxFinishToFinish = '完成至完成';
  scxStartToFinish  = '开始到完成 ';

  scxFinishToStartShort  = 'FS';
  scxStartToStartShort   = 'SS';
  scxFinishToFinishShort = 'FF';
  scxStartToFinishShort  = 'SF';

  scxGanttEventHint = '任务: %s'#13#10'完成: %d %%'#13#10'开始: %s'#13#10'完成: %s';
  scxLinkHint = '任务链接: %s (%s)'#13#10'从: %s'#13#10'到: %s';


  //

  scxCompleteDisplayFormat = '0 %';

  scxNone      = '普通事件';
  scxPattern   = '循环模式';
  scxOccurrence= '普通事件';
  scxException = '例外事件';
  scxCustom    = '自定义事件';

  // Holidays

   scxHolidaysEditorCaption                = '节假日编辑器';
  scxLocationsGroupBox                    = '本地';
  scxHolidaysGroupBox                     = '节假日';
  scxAddedHolidaysGroupBox                = '已添加的节假日';
  scxLocationName                         = '名称:';
  scxHolidaysLocationEditorCaption        = '本地编辑器';
  scxHolidayName                          = '名称:';
  scxHolidayDate                          = '日期:';
  scxHolidaysLocationHolidayEditorCaption = '节假日编辑器';
  scxOutlookFormatMismatch                = '节假日格式不匹配';
  scxHolidayDisplayFormat                 = '%s (%s)';
  scxAddedHolidayDisplayFormat            = '%s - %s (%s)';

const
  sRangeNames: array[0..4] of Pointer =
    (@scxFirst, @scxSecond, @scxThird, @scxFourth, @scxLast);
  sDayNames: array [0..9] of string =
    (scxDay, scxWeekday, scxWeekendday, '', '', '', '', '', '', '');
  sEventLabelCaptions: array[0..10] of Pointer = (
    @scxEventLabelNone, @scxEventLabel0, @scxEventLabel1, @scxEventLabel2,
    @scxEventLabel3, @scxEventLabel4, @scxEventLabel5, @scxEventLabel6,
    @scxEventLabel7, @scxEventLabel8, @scxEventLabel9);
  sEventRelations: array[0..3] of Pointer = (
    @scxFinishToStart, @scxStartToStart, @scxFinishToFinish, @scxStartToFinish);
  sEventRelationsShort: array[0..3] of Pointer = (
    @scxFinishToStartShort, @scxStartToStartShort, @scxFinishToFinishShort,
    @scxStartToFinishShort);
  sEventTaskStatus: array[0..4] of Pointer = 
    (@scxNotStarted, @scxInProgress, @scxComplete, @scxWaiting, @scxDeferred);

procedure cxSchedulerInitStrings;

implementation

uses
  SysUtils, dxCore, cxFormats;

procedure cxSchedulerInitStrings;
var
  I: Integer;
begin
  for I := 1 to 7 do
    sDayNames[2 + I] := dxFormatSettings.LongDayNames[I];
end;

procedure AddcxSchedulerResourceStringNames(AProduct: TdxProductResourceStrings);

  procedure InternalAdd(const AResourceStringName: string; AAdress: Pointer);
  begin
    AProduct.Add(AResourceStringName, AAdress);
  end;

begin
  InternalAdd('scxUntitledEvent', @scxUntitledEvent);
  InternalAdd('scxVertical', @scxVertical);
  InternalAdd('scxHorizontal', @scxHorizontal);
  InternalAdd('scxTimeGrid', @scxTimeGrid);
  InternalAdd('scxMinute', @scxMinute);
  InternalAdd('scxMinutes', @scxMinutes);
  InternalAdd('scxHour', @scxHour);
  InternalAdd('scxHours', @scxHours);
  InternalAdd('scxOneDay', @scxOneDay);
  InternalAdd('scxNextAppointment', @scxNextAppointment);
  InternalAdd('scxPrevAppointment', @scxPrevAppointment);
  InternalAdd('scxDeleteRecurringEventDescription', @scxDeleteRecurringEventDescription);
  InternalAdd('scxEditRecurringEventDescription', @scxEditRecurringEventDescription);
  InternalAdd('scxGoToDateDialogCaption', @scxGoToDateDialogCaption);
  InternalAdd('scxDeleteTypeDialogCaption', @scxDeleteTypeDialogCaption);
  InternalAdd('scxDeleteTypeOccurrenceLabel', @scxDeleteTypeOccurrenceLabel);
  InternalAdd('scxDeleteTypeSeriesLabel', @scxDeleteTypeSeriesLabel);
  InternalAdd('scxEditTypeDialogCaption', @scxEditTypeDialogCaption);
  InternalAdd('scxEditTypeOccurrenceLabel', @scxEditTypeOccurrenceLabel);
  InternalAdd('scxEditTypeSeriesLabel', @scxEditTypeSeriesLabel);
  InternalAdd('scxExitConfirmation', @scxExitConfirmation);
  InternalAdd('scxDeleteConfirmation', @scxDeleteConfirmation);
  InternalAdd('scxWrongTimeBounds', @scxWrongTimeBounds);
  InternalAdd('scxWrongPattern', @scxWrongPattern);
  InternalAdd('scxReplaceOccurrenceDate', @scxReplaceOccurrenceDate);
  InternalAdd('scxInvalidRecurrenceDuration', @scxInvalidRecurrenceDuration);
  InternalAdd('scxConfirmLostExceptions', @scxConfirmLostExceptions);
  InternalAdd('scxInvalidNumber', @scxInvalidNumber);
  InternalAdd('scxShedulerEditorFormNotRegistered', @scxShedulerEditorFormNotRegistered);
  InternalAdd('scxNoAvailableFreeTime', @scxNoAvailableFreeTime);
  InternalAdd('scxCannotRescheduleOccurrence', @scxCannotRescheduleOccurrence);
  InternalAdd('scxTwoOccurrencesPerDay', @scxTwoOccurrencesPerDay);
  InternalAdd('scxEvent', @scxEvent);
  InternalAdd('scxUntitled', @scxUntitled);
  InternalAdd('scxNoneEvent', @scxNoneEvent);
  InternalAdd('scxRecurrenceEvent', @scxRecurrenceEvent);
  InternalAdd('scxExceptionEvent', @scxExceptionEvent);
  InternalAdd('scxOccurenceEvent', @scxOccurenceEvent);
  InternalAdd('scxAdd', @scxAdd);
  InternalAdd('scxEdit', @scxEdit);
  InternalAdd('scxDelete', @scxDelete);
  InternalAdd('scxRecurrence', @scxRecurrence);
  InternalAdd('scxActionRecurrence', @scxActionRecurrence);
  InternalAdd('scxDate', @scxDate);
  InternalAdd('scxShowIn', @scxShowIn);
  InternalAdd('scxDayCalendar', @scxDayCalendar);
  InternalAdd('scxWeekCalendar', @scxWeekCalendar);
  InternalAdd('scxMonthCalendar', @scxMonthCalendar);
  InternalAdd('scxWorkWeekCalendar', @scxWorkWeekCalendar);
  InternalAdd('scxEventsConflict', @scxEventsConflict);
  InternalAdd('scxResource', @scxResource);
  InternalAdd('scxSubject', @scxSubject);
  InternalAdd('scxLocation', @scxLocation);
  InternalAdd('scxLabelAs', @scxLabelAs);
  InternalAdd('scxLabel', @scxLabel);
  InternalAdd('scxStartTime', @scxStartTime);
  InternalAdd('scxEndTime', @scxEndTime);
  InternalAdd('scxAllDayEvent', @scxAllDayEvent);
  InternalAdd('scxRecurrenceLabel', @scxRecurrenceLabel);
  InternalAdd('scxReminder', @scxReminder);
  InternalAdd('scxShowTimeAs', @scxShowTimeAs);
  InternalAdd('scxShowAs', @scxShowAs);
  InternalAdd('scxSuffixMinute', @scxSuffixMinute);
  InternalAdd('scxSuffixMinutes', @scxSuffixMinutes);
  InternalAdd('scxSuffixHour', @scxSuffixHour);
  InternalAdd('scxSuffixHours', @scxSuffixHours);
  InternalAdd('scxSuffixDay', @scxSuffixDay);
  InternalAdd('scxSuffixDays', @scxSuffixDays);
  InternalAdd('scxSuffixWeek', @scxSuffixWeek);
  InternalAdd('scxSuffixWeeks', @scxSuffixWeeks);
  InternalAdd('scxBusy', @scxBusy);
  InternalAdd('scxFree', @scxFree);
  InternalAdd('scxTentative', @scxTentative);
  InternalAdd('scxOutOfOffice', @scxOutOfOffice);
  InternalAdd('scxRecurrenceCaption', @scxRecurrenceCaption);
  InternalAdd('scxRecurrenceHolidayCaption', @scxRecurrenceHolidayCaption);
  InternalAdd('scxEventTime', @scxEventTime);
  InternalAdd('scxRecurrencePattern', @scxRecurrencePattern);
  InternalAdd('scxRangeOfRecurrence', @scxRangeOfRecurrence);
  InternalAdd('scxStart', @scxStart);
  InternalAdd('scxStart1', @scxStart1);
  InternalAdd('scxEnd', @scxEnd);
  InternalAdd('scxDuration', @scxDuration);
  InternalAdd('scxDaily', @scxDaily);
  InternalAdd('scxWeekly', @scxWeekly);
  InternalAdd('scxQuarterly', @scxQuarterly);
  InternalAdd('scxMonthly', @scxMonthly);
  InternalAdd('scxYearly', @scxYearly);
  InternalAdd('scxEvery', @scxEvery);
  InternalAdd('scxEveryWeekDay', @scxEveryWeekDay);
  InternalAdd('scxDays', @scxDays);
  InternalAdd('scxWeeksOn', @scxWeeksOn);
  InternalAdd('scxRecurEvery', @scxRecurEvery);
  InternalAdd('scxOfEvery', @scxOfEvery);
  InternalAdd('scxMonths', @scxMonths);
  InternalAdd('scxThe', @scxThe);
  InternalAdd('scxOf', @scxOf);
  InternalAdd('scxTaskComplete', @scxTaskComplete);
  InternalAdd('scxTaskStatus', @scxTaskStatus);
  InternalAdd('scxTaskDependencyEditorCaption', @scxTaskDependencyEditorCaption);
  InternalAdd('scxTaskWrongTimeBounds', @scxTaskWrongTimeBounds);
  InternalAdd('scxFinishToFinishLong', @scxFinishToFinishLong);
  InternalAdd('scxFinishToStartLong', @scxFinishToStartLong);
  InternalAdd('scxFrom', @scxFrom);
  InternalAdd('scxStartToFinishLong', @scxStartToFinishLong);
  InternalAdd('scxStartToStartLong', @scxStartToStartLong);
  InternalAdd('scxTo', @scxTo);
  InternalAdd('scxType', @scxType);
  InternalAdd('scxFirst', @scxFirst);
  InternalAdd('scxSecond', @scxSecond);
  InternalAdd('scxThird', @scxThird);
  InternalAdd('scxFourth', @scxFourth);
  InternalAdd('scxLast', @scxLast);
  InternalAdd('scxDay', @scxDay);
  InternalAdd('scxDay1', @scxDay1);
  InternalAdd('scxWeekday', @scxWeekday);
  InternalAdd('scxWeekendday', @scxWeekendday);
  InternalAdd('scxNoEndDate', @scxNoEndDate);
  InternalAdd('scxEndAfter', @scxEndAfter);
  InternalAdd('scxEndBy', @scxEndBy);
  InternalAdd('scxOccurences', @scxOccurences);
  InternalAdd('scxAdd1', @scxAdd1);
  InternalAdd('scxAdd1Hint', @scxAdd1Hint);
  InternalAdd('scxEditDotted', @scxEditDotted);
  InternalAdd('scxApply', @scxApply);
  InternalAdd('scxFindAvailableTime', @scxFindAvailableTime);
  InternalAdd('scxOk', @scxOk);
  InternalAdd('scxSaveAndClose', @scxSaveAndClose);
  InternalAdd('scxSaveAndCloseHint', @scxSaveAndCloseHint);
  InternalAdd('scxSave', @scxSave);
  InternalAdd('scxCancel', @scxCancel);
  InternalAdd('scxClose', @scxClose);
  InternalAdd('scxActionClose', @scxActionClose);
  InternalAdd('scxDown', @scxDown);
  InternalAdd('scxDelete1', @scxDelete1);
  InternalAdd('scxDelete1Hint', @scxDelete1Hint);
  InternalAdd('scxEdit1', @scxEdit1);
  InternalAdd('scxImport', @scxImport);
  InternalAdd('scxExport', @scxExport);
  InternalAdd('scxImportHint', @scxImportHint);
  InternalAdd('scxExportHint', @scxExportHint);
  InternalAdd('scxRemoveRecur', @scxRemoveRecur);
  InternalAdd('scxSelectAll', @scxSelectAll);
  InternalAdd('scxSelectNone', @scxSelectNone);
  InternalAdd('scxUp', @scxUp);
  InternalAdd('scxResourceLayoutCaption', @scxResourceLayoutCaption);
  InternalAdd('scxpmNewEvent', @scxpmNewEvent);
  InternalAdd('scxpmNewAllDayEvent', @scxpmNewAllDayEvent);
  InternalAdd('scxpmNewRecurringEvent', @scxpmNewRecurringEvent);
  InternalAdd('scxpmToday', @scxpmToday);
  InternalAdd('scxpmGotoThisDay', @scxpmGotoThisDay);
  InternalAdd('scxpmGoToDate', @scxpmGoToDate);
  InternalAdd('scxpmResourcesLayout', @scxpmResourcesLayout);
  InternalAdd('scxpmOpen', @scxpmOpen);
  InternalAdd('scxpmEditSeries', @scxpmEditSeries);
  InternalAdd('scxpmShowTimeAs', @scxpmShowTimeAs);
  InternalAdd('scxpmDelete', @scxpmDelete);
  InternalAdd('scxpmFree', @scxpmFree);
  InternalAdd('scxpmTentative', @scxpmTentative);
  InternalAdd('scxpmBusy', @scxpmBusy);
  InternalAdd('scxpmOutOfOffice', @scxpmOutOfOffice);
  InternalAdd('scxpmLabel', @scxpmLabel);
  InternalAdd('scxEventLabelNone', @scxEventLabelNone);
  InternalAdd('scxEventLabel0', @scxEventLabel0);
  InternalAdd('scxEventLabel1', @scxEventLabel1);
  InternalAdd('scxEventLabel2', @scxEventLabel2);
  InternalAdd('scxEventLabel3', @scxEventLabel3);
  InternalAdd('scxEventLabel4', @scxEventLabel4);
  InternalAdd('scxEventLabel5', @scxEventLabel5);
  InternalAdd('scxEventLabel6', @scxEventLabel6);
  InternalAdd('scxEventLabel7', @scxEventLabel7);
  InternalAdd('scxEventLabel8', @scxEventLabel8);
  InternalAdd('scxEventLabel9', @scxEventLabel9);
  InternalAdd('scxpmTimeZone', @scxpmTimeZone);
  InternalAdd('scxpm60Minutes', @scxpm60Minutes);
  InternalAdd('scxpm30Minutes', @scxpm30Minutes);
  InternalAdd('scxpm15Minutes', @scxpm15Minutes);
  InternalAdd('scxpm10Minutes', @scxpm10Minutes);
  InternalAdd('scxpm6Minutes', @scxpm6Minutes);
  InternalAdd('scxpm5Minutes', @scxpm5Minutes);
  InternalAdd('scxpmFullYear', @scxpmFullYear);
  InternalAdd('scxpmHalfYear', @scxpmHalfYear);
  InternalAdd('scxpmQuarter', @scxpmQuarter);
  InternalAdd('scxFullYear', @scxFullYear);
  InternalAdd('scxHalfYear', @scxHalfYear);
  InternalAdd('scxQuarter', @scxQuarter);
  InternalAdd('scxHalfYearShort', @scxHalfYearShort);
  InternalAdd('scxQuarterShort', @scxQuarterShort);
  InternalAdd('scxFirstButtonHint', @scxFirstButtonHint);
  InternalAdd('scxPrevPageButtonHint', @scxPrevPageButtonHint);
  InternalAdd('scxPrevButtonHint', @scxPrevButtonHint);
  InternalAdd('scxNextButtonHint', @scxNextButtonHint);
  InternalAdd('scxNextPageButtonHint', @scxNextPageButtonHint);
  InternalAdd('scxLastButtonHint', @scxLastButtonHint);
  InternalAdd('scxShowMoreResourcesButtonHint', @scxShowMoreResourcesButtonHint);
  InternalAdd('scxShowFewerResourcesButtonHint', @scxShowFewerResourcesButtonHint);
  InternalAdd('scxrCaptionReminder', @scxrCaptionReminder);
  InternalAdd('scxrCaptionReminders', @scxrCaptionReminders);
  InternalAdd('scxrDismissButton', @scxrDismissButton);
  InternalAdd('scxrDismissAllButton', @scxrDismissAllButton);
  InternalAdd('scxrDueIn', @scxrDueIn);
  InternalAdd('scxrOpenItemButton', @scxrOpenItemButton);
  InternalAdd('scxrSnoozeButton', @scxrSnoozeButton);
  InternalAdd('scxrSubject', @scxrSubject);
  InternalAdd('scxrSnoozeLabel', @scxrSnoozeLabel);
  InternalAdd('scxrSelected', @scxrSelected);
  InternalAdd('scxrStartTime', @scxrStartTime);
  InternalAdd('scxTime0m', @scxTime0m);
  InternalAdd('scxTime5m', @scxTime5m);
  InternalAdd('scxTime10m', @scxTime10m);
  InternalAdd('scxTime15m', @scxTime15m);
  InternalAdd('scxTime20m', @scxTime20m);
  InternalAdd('scxTime30m', @scxTime30m);
  InternalAdd('scxTime1h', @scxTime1h);
  InternalAdd('scxTime2h', @scxTime2h);
  InternalAdd('scxTime3h', @scxTime3h);
  InternalAdd('scxTime4h', @scxTime4h);
  InternalAdd('scxTime5h', @scxTime5h);
  InternalAdd('scxTime6h', @scxTime6h);
  InternalAdd('scxTime7h', @scxTime7h);
  InternalAdd('scxTime8h', @scxTime8h);
  InternalAdd('scxTime9h', @scxTime9h);
  InternalAdd('scxTime10h', @scxTime10h);
  InternalAdd('scxTime11h', @scxTime11h);
  InternalAdd('scxTime12h', @scxTime12h);
  InternalAdd('scxTime18h', @scxTime18h);
  InternalAdd('scxTime1d', @scxTime1d);
  InternalAdd('scxTime2d', @scxTime2d);
  InternalAdd('scxTime3d', @scxTime3d);
  InternalAdd('scxTime4d', @scxTime4d);
  InternalAdd('scxTime1w', @scxTime1w);
  InternalAdd('scxTime2w', @scxTime2w);
  InternalAdd('scxAdvance0h', @scxAdvance0h);
  InternalAdd('scxAdvance5m', @scxAdvance5m);
  InternalAdd('scxAdvance10m', @scxAdvance10m);
  InternalAdd('scxAdvance15m', @scxAdvance15m);
  InternalAdd('secxExportStorageInvalid', @secxExportStorageInvalid);
  InternalAdd('secxYes', @secxYes);
  InternalAdd('secxNo', @secxNo);
  InternalAdd('secxSubject', @secxSubject);
  InternalAdd('secxLocation', @secxLocation);
  InternalAdd('secxDescription', @secxDescription);
  InternalAdd('secxAllDay', @secxAllDay);
  InternalAdd('secxStart', @secxStart);
  InternalAdd('secxFinish', @secxFinish);
  InternalAdd('secxState', @secxState);
  InternalAdd('secxReminder', @secxReminder);
  InternalAdd('secxStartDate', @secxStartDate);
  InternalAdd('secxStartTime', @secxStartTime);
  InternalAdd('secxEndDate', @secxEndDate);
  InternalAdd('secxEndTime', @secxEndTime);
  InternalAdd('secxAlldayevent', @secxAlldayevent);
  InternalAdd('secxReminderonoff', @secxReminderonoff);
  InternalAdd('secxReminderDate', @secxReminderDate);
  InternalAdd('secxReminderTime', @secxReminderTime);

  InternalAdd('secxTrue', @secxTrue);
  InternalAdd('secxFalse', @secxFalse);
  InternalAdd('secxSetDateRangeCaption', @secxSetDateRangeCaption);
  InternalAdd('secxSetDateRangeText', @secxSetDateRangeText);
  InternalAdd('secxSetDateRangeAnd', @secxSetDateRangeAnd);

  InternalAdd('secxCategories', @secxCategories);
  InternalAdd('secxShowtimeas', @secxShowtimeas);
  InternalAdd('scxRequiredFieldsNeeded', @scxRequiredFieldsNeeded);
  InternalAdd('scxInvalidFieldName', @scxInvalidFieldName);
  InternalAdd('scxInvalidCustomField', @scxInvalidCustomField);
  InternalAdd('scxAllDayEventField', @scxAllDayEventField);
  InternalAdd('scxIDField', @scxIDField);
  InternalAdd('scxActualFinishField', @scxActualFinishField);
  InternalAdd('scxActualStartField', @scxActualStartField);
  InternalAdd('scxCaptionField', @scxCaptionField);
  InternalAdd('scxEnabledField', @scxEnabledField);
  InternalAdd('scxEventTypeField', @scxEventTypeField);
  InternalAdd('scxFinishField', @scxFinishField);
  InternalAdd('scxLabelField', @scxLabelField);
  InternalAdd('scxLocationField', @scxLocationField);
  InternalAdd('scxMessageField', @scxMessageField);
  InternalAdd('scxParentIDField', @scxParentIDField);
  InternalAdd('scxRecurrenceField', @scxRecurrenceField);
  InternalAdd('scxRecurrenceIndexField', @scxRecurrenceIndexField);
  InternalAdd('scxReminderDateField', @scxReminderDateField);
  InternalAdd('scxReminderField', @scxReminderField);
  InternalAdd('scxReminderMinutesBeforeStartField', @scxReminderMinutesBeforeStartField);
  InternalAdd('scxResourceField', @scxResourceField);
  InternalAdd('scxStartField', @scxStartField);
  InternalAdd('scxStateField', @scxStateField);
  InternalAdd('scxTaskCompleteField', @scxTaskCompleteField);
  InternalAdd('scxTaskIndexField', @scxTaskIndexField);
  InternalAdd('scxTaskLinksField', @scxTaskLinksField);
  InternalAdd('scxTaskStatusField', @scxTaskStatusField);
  InternalAdd('scxNotStarted', @scxNotStarted);
  InternalAdd('scxInProgress', @scxInProgress);
  InternalAdd('scxComplete', @scxComplete);
  InternalAdd('scxWaiting', @scxWaiting);
  InternalAdd('scxDeferred', @scxDeferred);
  InternalAdd('scxFinishToStart', @scxFinishToStart);
  InternalAdd('scxStartToStart', @scxStartToStart);
  InternalAdd('scxFinishToFinish', @scxFinishToFinish);
  InternalAdd('scxStartToFinish', @scxStartToFinish);
  InternalAdd('scxFinishToStartShort', @scxFinishToStartShort);
  InternalAdd('scxStartToStartShort', @scxStartToStartShort);
  InternalAdd('scxFinishToFinishShort', @scxFinishToFinishShort);
  InternalAdd('scxStartToFinishShort', @scxStartToFinishShort);
  InternalAdd('scxGanttEventHint', @scxGanttEventHint);
  InternalAdd('scxLinkHint', @scxLinkHint);
  InternalAdd('scxCompleteDisplayFormat', @scxCompleteDisplayFormat);
  InternalAdd('scxNone', @scxNone);
  InternalAdd('scxPattern', @scxPattern);
  InternalAdd('scxOccurrence', @scxOccurrence);
  InternalAdd('scxException', @scxException);
  InternalAdd('scxCustom', @scxCustom);
  InternalAdd('scxHolidaysEditorCaption', @scxHolidaysEditorCaption);
  InternalAdd('scxLocationsGroupBox', @scxLocationsGroupBox);
  InternalAdd('scxHolidaysGroupBox', @scxHolidaysGroupBox);
  InternalAdd('scxAddedHolidaysGroupBox', @scxAddedHolidaysGroupBox);
  InternalAdd('scxLocationName', @scxLocationName);
  InternalAdd('scxHolidaysLocationEditorCaption', @scxHolidaysLocationEditorCaption);
  InternalAdd('scxHolidayName', @scxHolidayName);
  InternalAdd('scxHolidayDate', @scxHolidayDate);
  InternalAdd('scxHolidaysLocationHolidayEditorCaption', @scxHolidaysLocationHolidayEditorCaption);
  InternalAdd('scxOutlookFormatMismatch', @scxOutlookFormatMismatch);
  InternalAdd('scxHolidayDisplayFormat', @scxHolidayDisplayFormat);
  InternalAdd('scxAddedHolidayDisplayFormat', @scxAddedHolidayDisplayFormat);
  InternalAdd('scxGroupIDField', @scxGroupIDField);
end;

initialization
  cxSchedulerInitStrings;
  dxResourceStringsRepository.RegisterProduct('ExpressScheduler', @AddcxSchedulerResourceStringNames);

finalization
  dxResourceStringsRepository.UnRegisterProduct('ExpressScheduler');

end.
