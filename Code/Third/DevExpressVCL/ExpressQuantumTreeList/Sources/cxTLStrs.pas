{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressQuantumTreeList                                   }
{                                                                    }
{           Copyright (c) 1998-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSQUANTUMTREELIST AND ALL        }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY. }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit cxTLStrs;

{$I cxVer.inc}

interface

uses
  cxClasses, dxCore;

resourcestring

  scxTLPrefixName = 'TcxTreeList';

   scxIndexOutOfBounds    = '指针%d超界';
  scxInvalidStreamFormat = '流格式无效';
  scxMultiSelectRequired = '必须多选';
  // designer constants

  scxDesignerCaption     = 'TreeList设计器编辑 - ';
  scxBands               = '带区...';
  scxColumns             = '列...';
  scxCreateAllItems      = '创建全部列';
  scxDeleteAllItems      = '删除全部列';
  scxItems = '项编辑器...';

  // customization form
                                      
  scxCustomizeCaption = '自定义';
  scxColumnsCaption   = '   列   ';
  scxBandsCaption     = '    带区    ';

  // Summary menu item captions
  scxTreeListNoneMenuItem = '无';
  scxTreeListSumMenuItem = '总和';
  scxTreeListMinMenuItem = '最小';
  scxTreeListMaxMenuItem = '最大';
  scxTreeListCountMenuItem = '数量';
  scxTreeListAvgMenuItem = '平均';
  scxTreeListAllNodesMenuItem = '所有节点';
  // Column Header menu item captions
  scxTreeListSortAscendingMenuItem = '升序排序(&S)';
  scxTreeListSortDescendingMenuItem = '降序排序(&O)';
  scxTreeListClearSortingMenuItem = '清空排序(&C)';
  scxTreeListFooterMenuItem = '页脚(&F)';
  scxTreeListGroupFootersMenuItem = '分组页脚(&G)';
  scxTreeListGroupFootersInvisibleMenuItem = '隐藏(&H)';
  scxTreeListGroupFootersVisibleWhenExpandedMenuItem = '展开时可见(&V)';
  scxTreeListGroupFootersAlwaysVisibleMenuItem = '始终可见(&A)';
  scxTreeListRemoveThisColumnMenuItem = '删除此列(&E)';
  scxTreeListFieldChooserMenuItem = '字段选择器(&I)';
  scxTreeListHorizontalAlignmentMenuItem = '水平对齐(&R)';
  scxTreeListHorizontalAlignmentLeftMenuItem = '左(&L)';
  scxTreeListHorizontalAlignmentCenterMenuItem = '居中(&N)';
  scxTreeListHorizontalAlignmentRightMenuItem = '右(&R)';
  scxTreeListVerticalAlignmentMenuItem = '垂直对齐(&T)';
  scxTreeListVerticalAlignmentBottomMenuItem = '下(&B)';
  scxTreeListVerticalAlignmentCenterMenuItem = '居中(&N)';
  scxTreeListVerticalAlignmentTopMenuItem = '上(&T)';
  scxTreeListBestFitMenuItem = '最适合(&B)';
  scxTreeListBestFitAllColumnsMenuItem = '自适应(全部列)(&T)';

  scxOperationNotSupported = '不支持此操作';

  scxTreeListDeletingFocusedConfirmationText = '删除记录？';
  scxTreeListDeletingConfirmationCaption = '确认';

  scxExportNotVisibleControl = '组件必须显示输出';
  function scxStr(AResString: Pointer): string;

implementation

function scxStr(AResString: Pointer): string;
begin
  Result := cxGetResourceString(AResString);
end;

procedure AddExpressQuantumTreeListResourceStringNames(AProduct: TdxProductResourceStrings);

  procedure InternalAdd(const AResourceStringName: string; AAdress: Pointer);
  begin
    AProduct.Add(AResourceStringName, AAdress);
  end;

begin
  InternalAdd('scxIndexOutOfBounds', @scxIndexOutOfBounds);
  InternalAdd('scxInvalidStreamFormat', @scxInvalidStreamFormat);
  InternalAdd('scxMultiSelectRequired', @scxMultiSelectRequired);
  InternalAdd('scxTLPrefixName', @scxTLPrefixName);
  InternalAdd('scxDesignerCaption', @scxDesignerCaption);
  InternalAdd('scxBands', @scxBands);
  InternalAdd('scxColumns', @scxColumns);
  InternalAdd('scxCreateAllItems', @scxCreateAllItems);
  InternalAdd('scxDeleteAllItems', @scxDeleteAllItems);
  InternalAdd('scxItems', @scxItems);
  InternalAdd('scxCustomizeCaption', @scxCustomizeCaption);
  InternalAdd('scxColumnsCaption', @scxColumnsCaption);
  InternalAdd('scxBandsCaption', @scxBandsCaption);
  InternalAdd('scxTreeListNoneMenuItem', @scxTreeListNoneMenuItem);
  InternalAdd('scxTreeListSumMenuItem', @scxTreeListSumMenuItem);
  InternalAdd('scxTreeListMinMenuItem', @scxTreeListMinMenuItem);
  InternalAdd('scxTreeListMaxMenuItem', @scxTreeListMaxMenuItem);
  InternalAdd('scxTreeListCountMenuItem', @scxTreeListCountMenuItem);
  InternalAdd('scxTreeListAvgMenuItem', @scxTreeListAvgMenuItem);
  InternalAdd('scxTreeListAllNodesMenuItem', @scxTreeListAllNodesMenuItem);
  InternalAdd('scxTreeListSortAscendingMenuItem', @scxTreeListSortAscendingMenuItem);
  InternalAdd('scxTreeListSortDescendingMenuItem', @scxTreeListSortDescendingMenuItem);
  InternalAdd('scxTreeListClearSortingMenuItem', @scxTreeListClearSortingMenuItem);
  InternalAdd('scxTreeListFooterMenuItem', @scxTreeListFooterMenuItem);
  InternalAdd('scxTreeListGroupFootersMenuItem', @scxTreeListGroupFootersMenuItem);
  InternalAdd('scxTreeListGroupFootersInvisibleMenuItem', @scxTreeListGroupFootersInvisibleMenuItem);
  InternalAdd('scxTreeListGroupFootersVisibleWhenExpandedMenuItem', @scxTreeListGroupFootersVisibleWhenExpandedMenuItem);
  InternalAdd('scxTreeListGroupFootersAlwaysVisibleMenuItem', @scxTreeListGroupFootersAlwaysVisibleMenuItem);
  InternalAdd('scxTreeListRemoveThisColumnMenuItem', @scxTreeListRemoveThisColumnMenuItem);
  InternalAdd('scxTreeListFieldChooserMenuItem', @scxTreeListFieldChooserMenuItem);
  InternalAdd('scxTreeListHorizontalAlignmentMenuItem', @scxTreeListHorizontalAlignmentMenuItem);
  InternalAdd('scxTreeListHorizontalAlignmentLeftMenuItem', @scxTreeListHorizontalAlignmentLeftMenuItem);
  InternalAdd('scxTreeListHorizontalAlignmentCenterMenuItem', @scxTreeListHorizontalAlignmentCenterMenuItem);
  InternalAdd('scxTreeListHorizontalAlignmentRightMenuItem', @scxTreeListHorizontalAlignmentRightMenuItem);
  InternalAdd('scxTreeListVerticalAlignmentMenuItem', @scxTreeListVerticalAlignmentMenuItem);
  InternalAdd('scxTreeListVerticalAlignmentBottomMenuItem', @scxTreeListVerticalAlignmentBottomMenuItem);
  InternalAdd('scxTreeListVerticalAlignmentCenterMenuItem', @scxTreeListVerticalAlignmentCenterMenuItem);
  InternalAdd('scxTreeListVerticalAlignmentTopMenuItem', @scxTreeListVerticalAlignmentTopMenuItem);
  InternalAdd('scxTreeListBestFitMenuItem', @scxTreeListBestFitMenuItem);
  InternalAdd('scxTreeListBestFitAllColumnsMenuItem', @scxTreeListBestFitAllColumnsMenuItem);
  InternalAdd('scxOperationNotSupported', @scxOperationNotSupported);
  InternalAdd('scxTreeListDeletingFocusedConfirmationText', @scxTreeListDeletingFocusedConfirmationText);
  InternalAdd('scxTreeListDeletingConfirmationCaption', @scxTreeListDeletingConfirmationCaption);
  InternalAdd('scxExportNotVisibleControl', @scxExportNotVisibleControl);
end;

initialization
  scxStr(@scxBands);
  dxResourceStringsRepository.RegisterProduct('ExpressQuantumTreeList', @AddExpressQuantumTreeListResourceStringNames);

finalization
  dxResourceStringsRepository.UnRegisterProduct('ExpressQuantumTreeList');

end.
