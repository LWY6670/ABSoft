{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.DocumentModel.MergedProperties;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  dxRichEdit.Utils.Types;

type

  { TdxMergedProperties }

  TdxMergedProperties<TInfo: IdxCloneable<TInfo>; TOptions: IdxCloneable<TOptions>> = class
  private
    FInfo: TInfo;
    FOptions: TOptions;
  public
    constructor Create(AInfo: TInfo; AOptions: TOptions); virtual;
    destructor Destroy; override;

    property Info: TInfo read FInfo;
    property Options: TOptions read FOptions;
  end;

  { TdxPropertiesMergerBase }

  TdxPropertiesMergerBase<
    TInfo: IdxCloneable<TInfo>;
    TOptions: IdxCloneable<TOptions>;
    TMergedProperties: TdxMergedProperties<TInfo, TOptions>> = class
  private
    FMergedProperties: TMergedProperties;
    function GetOwnInfo: TInfo;
    function GetOwnOptions: TOptions;
  protected
    procedure MergeCore(const AInfo: TInfo; const AOptions: TOptions); virtual; abstract;
    property OwnInfo: TInfo read GetOwnInfo;
    property OwnOptions: TOptions read GetOwnOptions;
  public
    constructor Create(const AProperties: TMergedProperties);
    procedure Merge(const AProperties: TMergedProperties);
    property MergedProperties: TMergedProperties read FMergedProperties;
  end;

implementation

uses
  SysUtils;

{ TdxMergedProperties<TInfo, TOptions> }

constructor TdxMergedProperties<TInfo, TOptions>.Create(AInfo: TInfo;
  AOptions: TOptions);
begin
  inherited Create;
  FInfo := AInfo.Clone;
  FOptions := AOptions.Clone;
end;

destructor TdxMergedProperties<TInfo, TOptions>.Destroy;
begin
  FreeAndNil(FOptions);
  FreeAndNil(FInfo);
  inherited Destroy;
end;

{ TdxPropertiesMergerBase<TInfo, TOptions, TMergedProperties> }

constructor TdxPropertiesMergerBase<TInfo, TOptions, TMergedProperties>.Create(const AProperties: TMergedProperties);
begin
  inherited Create;
  FMergedProperties := AProperties;
end;

function TdxPropertiesMergerBase<TInfo, TOptions, TMergedProperties>.GetOwnInfo: TInfo;
begin
  Result := MergedProperties.Info;
end;

function TdxPropertiesMergerBase<TInfo, TOptions, TMergedProperties>.GetOwnOptions: TOptions;
begin
  Result := MergedProperties.Options;
end;

procedure TdxPropertiesMergerBase<TInfo, TOptions, TMergedProperties>.Merge(const AProperties: TMergedProperties);
begin
  MergeCore(AProperties.Info, AProperties.Options);
end;

end.
