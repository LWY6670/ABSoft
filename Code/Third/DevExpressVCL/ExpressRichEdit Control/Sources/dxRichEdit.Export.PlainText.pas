{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Export.PlainText;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Types, Classes, SysUtils, Graphics, Windows, Generics.Collections, dxRichEdit.Options,
  dxRichEdit.Utils.Characters, dxRichEdit.Utils.ChunkedStringBuilder, dxRichEdit.DocumentModel.Styles, dxRichEdit.Platform.Font,
  dxRichEdit.DocumentModel.PieceTable, dxRichEdit.DocumentModel.Exporter, dxRichEdit.DocumentModel.CharacterFormatting,
  dxRichEdit.DocumentModel.ParagraphFormatting, dxRichEdit.DocumentModel.UnitConverter, dxRichEdit.DocumentModel.Section, dxRichEdit.DocumentModel.TabFormatting;

type
  TdxPlainTextExporter = class(TdxDocumentModelExporter, IdxRichEditExporter)
  strict private
    FSb: TStringBuilder;
    FNeedDestroyOptions: Boolean;
    FOptions: TdxPlainTextDocumentExporterOptions;
    function CalculateActualContentLength: Integer;
  protected
    procedure ExportDocument; override;
    procedure ExportTextRun(ARun: TdxTextRun); override;

    function ShouldExportHiddenText: Boolean; override;

    //IdxRichEditExporter
    function IdxRichEditExporter.Export = RichEditExport;
    function RichEditExport: string;

    property Content: TStringBuilder read FSb;
  public
    constructor Create(ADocumentModel: TdxDocumentModel; AOptions: TdxPlainTextDocumentExporterOptions);
    destructor Destroy; override;

    function ExportSaveMemory: TStringBuilder;
    property NeedDestroyOptions: Boolean read FNeedDestroyOptions write FNeedDestroyOptions;
  end;

implementation

uses
  dxCore;

{ TdxPlainTextExporter }

function TdxPlainTextExporter.CalculateActualContentLength: Integer;
var
  ALength: Integer;
  ALastChar: Char;
  ATrail: string;
begin
  ALength := FSb.Length;
  if ALength <= 0 then
    Result := 0
  else
    if ALength = 1 then
    begin
      ALastChar := FSb[ALength - 1];
      if dxCharInSet(ALastChar, [#13, #10]) then
        Result := 0
      else
        Result := 1;
    end
    else
    begin
      ATrail := FSb.ToString(ALength - 2, 2);
      if (ATrail = #10#13) or (ATrail = #13#10) then
        Result := ALength - 2
      else
        if dxCharInSet(ATrail[1], [#13, #10]) then
          Result := ALength - 1
        else
          Result := ALength;
    end;
end;

constructor TdxPlainTextExporter.Create(ADocumentModel: TdxDocumentModel;
  AOptions: TdxPlainTextDocumentExporterOptions);
begin
  inherited Create(ADocumentModel);
  FSb := TStringBuilder.Create;
  FOptions := AOptions;
end;

destructor TdxPlainTextExporter.Destroy;
begin
  if NeedDestroyOptions then
    FreeAndNil(FOptions);
  FreeAndNil(FSb);
  inherited;
end;

procedure TdxPlainTextExporter.ExportDocument;
begin
  inherited ExportDocument;
end;

function TdxPlainTextExporter.ExportSaveMemory: TStringBuilder;
begin
  FSb.Clear;
  inherited Export;

  FSb.Length := CalculateActualContentLength;
  Result := FSb;
end;

procedure TdxPlainTextExporter.ExportTextRun(ARun: TdxTextRun);
var
  AText: string;
begin
  AText := ARun.GetPlainText(PieceTable.TextBuffer);
  if ARun.AllCaps then
    AText := UpperCase(AText);

  AText := StringReplace(AText, #13#10, #10, [rfReplaceAll, rfIgnoreCase]);
  AText := StringReplace(AText, #10, #13#10, [rfReplaceAll, rfIgnoreCase]);
  FSb.Append(AText);
end;

function TdxPlainTextExporter.RichEditExport: string;
begin
  Result := ExportSaveMemory.ToString;
end;

function TdxPlainTextExporter.ShouldExportHiddenText: Boolean;
begin
  Result := FOptions.ExportHiddenText;
end;

end.
