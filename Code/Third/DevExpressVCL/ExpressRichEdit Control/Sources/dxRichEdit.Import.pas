{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Import;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Types, Classes, SysUtils, dxCoreClasses, dxRichEdit.DocumentModel.Core, dxRichEdit.DocumentModel.PieceTable,
  dxRichEdit.DocumentModel.UnitConverter, dxRichEdit.Options;

type
  { TdxFastObjectStack }

  TdxFastObjectStack = class(TdxFastObjectList)
  public
    function Peek: TObject;
    function Pop: TObject;
    procedure Push(AObject: TObject);
  end;

  { TdxRichEditImportBookmarkInfoCore }

  TdxRichEditImportBookmarkInfoCore = class
  private
    FFinish: Integer;
    FStart: Integer;
  public
    constructor Create;
    function Validate(APieceTable: TdxPieceTable): Boolean; virtual;

    property Finish: Integer read FFinish write FFinish;
    property Start: Integer read FStart write FStart;
  end;

  { TdxRichEditImportBookmarkInfo }

  TdxRichEditImportBookmarkInfo = class(TdxRichEditImportBookmarkInfoCore)
  private
    FName: string;
  public
    function Validate(APieceTable: TdxPieceTable): Boolean; override;

    property Name: string read FName write FName;
  end;

  TdxRichEditImportBookmarkInfos = class(TdxFastObjectList)
  private
    function GetItem(Index: Integer): TdxRichEditImportBookmarkInfo;
    procedure SetItem(Index: Integer; const Value: TdxRichEditImportBookmarkInfo);
  public
    property Items[Index: Integer]: TdxRichEditImportBookmarkInfo read GetItem write SetItem; default;
  end;

  { ImportRangePermissionInfo }

  TdxRichEditImportRangePermissionInfo = class(TdxRichEditImportBookmarkInfoCore)
  private
  public

  end;

  TdxRichEditImportRangePermissionInfos = class(TdxFastObjectList)
  private
    function GetItem(Index: Integer): TdxRichEditImportRangePermissionInfo;
    procedure SetItem(Index: Integer; const Value: TdxRichEditImportRangePermissionInfo);
  public
    property Items[Index: Integer]: TdxRichEditImportRangePermissionInfo read GetItem write SetItem; default;
  end;

  { TdxRichEditImportPieceTableInfoBase }

  TdxRichEditImportPieceTableInfoBase = class
  private
    FBoomarks: TdxRichEditImportBookmarkInfos;
    FPieceTable: TdxPieceTable;
    FRangePermissions: TdxRichEditImportRangePermissionInfos;
  public
    constructor Create(APieceTable: TdxPieceTable);
    destructor Destroy; override;

    property Boomarks: TdxRichEditImportBookmarkInfos read FBoomarks;
    property PieceTable: TdxPieceTable read FPieceTable;
    property RangePermissions: TdxRichEditImportRangePermissionInfos read FRangePermissions;
  end;

  TdxRichEditDocumentModelImporter = class
  private
    FDocumentModel: TdxDocumentModel;
    FOptions: TdxDocumentImporterOptions;
    function GetUnitConverter: TdxDocumentModelUnitConverter;
  protected
    procedure ImportCore(AStream: TStream); virtual; abstract;
  public
    constructor Create(ADocumentModel: TdxDocumentModel; AOptions: TdxDocumentImporterOptions); virtual;

    procedure Import(AStream: TStream); overload;
    procedure Import(const AFileName: string); overload;

    property DocumentModel: TdxDocumentModel read FDocumentModel;
    property Options: TdxDocumentImporterOptions read FOptions;
    property UnitConverter: TdxDocumentModelUnitConverter read  GetUnitConverter;
  end;

implementation

uses
  RTLConsts, Math;

{ TdxFastObjectStack }

function TdxFastObjectStack.Peek: TObject;
begin
  Result := Last;
end;

function TdxFastObjectStack.Pop: TObject;
begin
  Result := Last;
  Extract(Result);
end;

procedure TdxFastObjectStack.Push(AObject: TObject);
begin
  Add(AObject);
end;

{ TdxRichEditImportBookmarkInfoCore }

constructor TdxRichEditImportBookmarkInfoCore.Create;
begin
  inherited Create;
  FStart := -1;
  FFinish := -1;
end;

function TdxRichEditImportBookmarkInfoCore.Validate(APieceTable: TdxPieceTable): Boolean;
var
  ATemp, AMax: Integer;
begin
  Result := (Start >= 0) and (Finish >= 0);
  if Result then
  begin
    if Start > Finish then
    begin
      ATemp := Start;
      FStart := FFinish;
      FFinish := ATemp;
    end;
    AMax := APieceTable.DocumentEndLogPosition + 1;
    Start := Min(Start, AMax);
    Finish := Min(Finish, AMax);
  end;
end;

{ TdxRichEditImportBookmarkInfo }

function TdxRichEditImportBookmarkInfo.Validate(APieceTable: TdxPieceTable): Boolean;
begin
  Result := (Name <> '') and (APieceTable.Bookmarks.FindByName(Name) = nil) and inherited Validate(APieceTable);
end;

{ TdxRichEditImportBookmarkInfos }

function TdxRichEditImportBookmarkInfos.GetItem(Index: Integer): TdxRichEditImportBookmarkInfo;
begin
  Result := TdxRichEditImportBookmarkInfo(inherited Items[Index]);
end;

procedure TdxRichEditImportBookmarkInfos.SetItem(Index: Integer; const Value: TdxRichEditImportBookmarkInfo);
begin
  inherited Items[Index] := Value;
end;

{ TdxRichEditImportRangePermissionInfos }

function TdxRichEditImportRangePermissionInfos.GetItem(Index: Integer): TdxRichEditImportRangePermissionInfo;
begin
  Result := TdxRichEditImportRangePermissionInfo(inherited Items[Index]);
end;

procedure TdxRichEditImportRangePermissionInfos.SetItem(Index: Integer; const Value: TdxRichEditImportRangePermissionInfo);
begin
  inherited Items[Index] := Value;
end;

{ TdxRichEditImportPieceTableInfoBase }

constructor TdxRichEditImportPieceTableInfoBase.Create(APieceTable: TdxPieceTable);
begin
  inherited Create;
  FPieceTable := APieceTable;
  FBoomarks := TdxRichEditImportBookmarkInfos.Create;
  FRangePermissions := TdxRichEditImportRangePermissionInfos.Create;
end;

destructor TdxRichEditImportPieceTableInfoBase.Destroy;
begin
  FreeAndNil(FRangePermissions);
  FreeAndNil(FBoomarks);
  inherited;
end;

{ TdxRichEditDocumentModelImporter }

constructor TdxRichEditDocumentModelImporter.Create(ADocumentModel: TdxDocumentModel;
  AOptions: TdxDocumentImporterOptions);
begin
  inherited Create;
  FDocumentModel := ADocumentModel;
  FOptions := AOptions;
end;

procedure TdxRichEditDocumentModelImporter.Import(AStream: TStream);
begin
  if AStream = nil then
    Exit;
  ImportCore(AStream);
end;

procedure TdxRichEditDocumentModelImporter.Import(const AFileName: string);
var
  AStream: TStream;
begin
  AStream := TFileStream.Create(AFileName, fmShareDenyNone);
  try
    AStream.Position := 0;
    Import(AStream);
  finally
    AStream.Free;
  end;
end;

function TdxRichEditDocumentModelImporter.GetUnitConverter: TdxDocumentModelUnitConverter;
begin
  Result := DocumentModel.UnitConverter;
end;

end.
