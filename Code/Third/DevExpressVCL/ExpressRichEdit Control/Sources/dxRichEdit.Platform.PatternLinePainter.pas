{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Platform.PatternLinePainter;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Windows, Forms, SysUtils, dxCore, Generics.Collections, dxCoreClasses, cxGeometry, Graphics, Types,
  dxRichEdit.DocumentModel.CharacterFormatting, dxRichEdit.DocumentModel.UnitConverter, dxRichEdit.DocumentLayout.UnitConverter,
  dxRichEdit.DocumentModel.PatternLine, cxGraphics;

type
  TdxRectangleF = TdxRectF;  
  TdxColor = TColor;
  TdxGraphics = TcxCanvas;

  PIntArray = ^TIntArray;
  TIntArray = array[0..0] of Integer;

  IdxPatternLinePaintingSupport = interface
  ['{CA9A0CED-1580-49FB-AF27-1912870E7895}']
    function GetPen(AColor: TColor; AThickness: Double): TPen;
    procedure ReleasePen(APen: TPen);
    procedure DrawLine(APen: TPen; X1, Y1, X2, Y2: Double); 
    procedure DrawLines(APen: TPen; const APoints: array of TPointFloat); 
  end;

  { TdxPatternLinePainterParameters }

  TdxPatternLinePainterParameters = class abstract
  private
    FDotPattern: TSingleDynArray; 
    FDashPattern: TSingleDynArray; 
    FDashSmallGapPattern: TSingleDynArray; 
    FDashDotPattern: TSingleDynArray; 
    FDashDotDotPattern: TSingleDynArray; 
    FLongDashPattern: TSingleDynArray; 
    FPixelPenWidth: Single; 
    FPixelStep: Single; 
  protected
    function CreatePattern(const APattern: array of Single; ADpiX: Single): TSingleDynArray; 
    function PixelsToUnits(AValue, ADpi: Single): Single; virtual; abstract; 
  public
    destructor Destroy; override;
    procedure Initialize(ADpiX: Single); virtual; 

    property DotPattern: TSingleDynArray read FDotPattern; 
    property DashPattern: TSingleDynArray read FDashPattern; 
    property DashSmallGapPattern: TSingleDynArray read FDashSmallGapPattern; 
    property DashDotPattern: TSingleDynArray read FDashDotPattern; 
    property DashDotDotPattern: TSingleDynArray read FDashDotDotPattern; 
    property LongDashPattern: TSingleDynArray read FLongDashPattern; 
    property PixelPenWidth: Single read FPixelPenWidth; 
    property PixelStep: Single read FPixelStep; 
  end;

  { TdxRichEditPatternLinePainterParameters }

  TdxRichEditPatternLinePainterParameters = class(TdxPatternLinePainterParameters)
  private
    FUnitConverter: TdxDocumentLayoutUnitConverter; 
    FSectionStartPattern: TSingleDynArray; 
    FPageBreakPattern: TSingleDynArray; 
    FColumnBreakPattern: TSingleDynArray; 
  protected
    function PixelsToUnits(AValue, ADpi: Single): Single; override; 
  public
    constructor Create(AUnitConverter: TdxDocumentLayoutUnitConverter); 
    destructor Destroy; override;
    procedure Initialize(ADpiX: Single); override; 

    property SectionStartPattern: TSingleDynArray read FSectionStartPattern; 
    property PageBreakPattern: TSingleDynArray read FPageBreakPattern; 
    property ColumnBreakPattern: TSingleDynArray read FColumnBreakPattern; 
  end;


  { TdxPatternLinePainterParameters }

  TdxPatternLinePainter = class abstract
  private
    FPainter: IdxPatternLinePaintingSupport;
    FUnitConverter: TdxDocumentLayoutUnitConverter;
    function UnitsToPixels(AVal, ADpi: Double): Double; virtual; 
    function PixelsToUnits(AVal, ADpi: Double): Double; virtual; 
    function RoundToPixels(AVal, ADpi: Double): Double; virtual; 
    function GetDashDotPattern: TSingleDynArray;
    function GetDotPattern: TSingleDynArray;
    function GetDashPattern: TSingleDynArray;
    function GetDashSmallGapPattern: TSingleDynArray;
    function GetDashDotDotPattern: TSingleDynArray;
    function GetLongDashPattern: TSingleDynArray;
    function GetPixelPenWidth: Single;
    function GetPixelStep: Single;
  protected
    function GetParameters: TdxPatternLinePainterParameters; virtual; abstract;
    function GetPixelGraphics: TdxGraphics; virtual; abstract;

    property DotPattern: TSingleDynArray read GetDotPattern; 
    property DashPattern: TSingleDynArray read GetDashPattern; 
    property DashSmallGapPattern: TSingleDynArray read GetDashSmallGapPattern; 
    property DashDotPattern: TSingleDynArray read GetDashDotPattern; 
    property DashDotDotPattern: TSingleDynArray read GetDashDotDotPattern; 
    property LongDashPattern: TSingleDynArray read GetLongDashPattern; 
    property PixelPenWidth: Single read GetPixelPenWidth; 
    property PixelStep: Single  read GetPixelStep; 
    property Parameters: TdxPatternLinePainterParameters read GetParameters; 
    property PixelGraphics: TdxGraphics read GetPixelGraphics; 
  public
    constructor Create(APainter: IdxPatternLinePaintingSupport; AUnitConverter: TdxDocumentLayoutUnitConverter); 
    function RotateBounds(const ABounds: TdxRectangleF): TdxRectangleF; virtual; 
    function RotatePoint(APointF: TPointFloat): TPointFloat; virtual; 
    procedure DrawLine(APen: TPen; ABounds: TdxRectangleF); virtual; 
    procedure DrawSolidLine(ABounds: TdxRectangleF; AColor: TColor); 
    procedure DrawDoubleSolidLine(ABounds: TdxRectangleF; AColor: TColor); 

    procedure DrawPatternLine(ABounds: TdxRectangleF; AColor: TColor; const APattern: TSingleDynArray); 
    procedure DrawWaveUnderline(ABounds: TdxRectangleF; AColor: TColor; APenWidth: Single); overload; 
    procedure DrawWaveUnderline(ABounds: TdxRectangleF; APen: TPen; AStep: Single); overload; 
    procedure DrawWaveUnderline(ABounds: TdxRectangleF; APen: TPen); overload; 
    function MakeFixedWidthPattern(const APattern: TSingleDynArray; AThickness: Single): TSingleDynArray; 
    function MakeBoundsAtLeast2PixelsHigh(ABounds: TdxRectangleF): TdxRectangleF; virtual; 

    property Painter: IdxPatternLinePaintingSupport read FPainter;
    property UnitConverter: TdxDocumentLayoutUnitConverter read FUnitConverter;
  end;

  { TdxRichEditPatternLinePainter }

  TdxRichEditPatternLinePainter = class(TdxPatternLinePainter) 
  private
    FPixelGraphics: TdxGraphics; 
    FParameters: TdxPatternLinePainterParameters;
  protected
    function GetParameters: TdxPatternLinePainterParameters; override;
    procedure PrepareParameters;
    procedure InitializeParameters(AParameters: TdxPatternLinePainterParameters); virtual; abstract;
    class function CreatePixelGraphics: TdxGraphics;
    function GetPixelGraphics: TdxGraphics; override;
  public
    constructor Create(APainter: IdxPatternLinePaintingSupport; AUnitConverter: TdxDocumentLayoutUnitConverter);
    destructor Destroy; override;
    //IUnderlinePainter
    procedure DrawUnderline(AUnderline: TdxUnderlineSingle; ABounds: TdxRectangleF; AColor: TColor); overload;
    procedure DrawUnderline(AUnderline: TdxUnderlineDotted; ABounds: TdxRectangleF; AColor: TColor); overload;
    procedure DrawUnderline(AUnderline: TdxUnderlineDashed; ABounds: TdxRectangleF; AColor: TColor); overload;
    procedure DrawUnderline(AUnderline: TdxUnderlineDashSmallGap; ABounds: TdxRectangleF; AColor: TdxColor); overload;
    procedure DrawUnderline(AUnderline: TdxUnderlineDashDotted; ABounds: TdxRectangleF; AColor: TColor); overload;
    procedure DrawUnderline(AUnderline: TdxUnderlineDashDotDotted; ABounds: TdxRectangleF; AColor: TColor); overload;
    procedure DrawUnderline(AUnderline: TdxUnderlineDouble; ABounds: TdxRectangleF; AColor: TColor); overload;
    procedure DrawUnderline(AUnderline: TdxUnderlineHeavyWave; ABounds: TdxRectangleF; AColor: TColor); overload;
    procedure DrawUnderline(AUnderline: TdxUnderlineLongDashed; ABounds: TdxRectangleF; AColor: TColor); overload;
    procedure DrawUnderline(AUnderline: TdxUnderlineThickSingle; ABounds: TdxRectangleF; AColor: TColor); overload;
    procedure DrawUnderline(AUnderline: TdxUnderlineThickDotted; ABounds: TdxRectangleF; AColor: TColor); overload;
    procedure DrawUnderline(AUnderline: TdxUnderlineThickDashed; ABounds: TdxRectangleF; AColor: TColor); overload;
    procedure DrawUnderline(AUnderline: TdxUnderlineThickDashDotted; ABounds: TdxRectangleF; AColor: TColor); overload;
    procedure DrawUnderline(AUnderline: TdxUnderlineThickDashDotDotted; ABounds: TdxRectangleF; AColor: TColor); overload;
    procedure DrawUnderline(AUnderline: TdxUnderlineThickLongDashed; ABounds: TdxRectangleF; AColor: TColor); overload;
    procedure DrawUnderline(AUnderline: TdxUnderlineDoubleWave; ABounds: TdxRectangleF; AColor: TColor); overload;
    procedure DrawUnderline(AUnderline: TdxUnderlineWave; ABounds: TdxRectangleF; AColor: TColor); overload;
  //IStrikeoutPainter
    procedure DrawStrikeout(AStrikeout: TdxStrikeoutSingle; ABounds: TdxRectangleF; AColor: TColor); overload;
    procedure DrawStrikeout(AStrikeout: TdxStrikeoutDouble; ABounds: TdxRectangleF; AColor: TColor); overload;
  end;

  { TdxRichEditHorizontalPatternLinePainter }

  TdxRichEditHorizontalPatternLinePainter = class(TdxRichEditPatternLinePainter)
  protected
    procedure InitializeParameters(AParameters: TdxPatternLinePainterParameters); override;
  end;

implementation

uses
  Math;

{ TdxPatternLinePainterParameters }

function TdxPatternLinePainterParameters.CreatePattern(const APattern: array of Single; ADpiX: Single): TSingleDynArray;
var
  I: Integer;
begin
  SetLength(Result, Length(APattern));
  for I := 0 to Length(APattern) - 1 do
    Result[I] := PixelsToUnits(APattern[I], ADpiX) / 5;
end;

destructor TdxPatternLinePainterParameters.Destroy;
begin
  SetLength(FDotPattern, 0);
  SetLength(FDashPattern, 0);
  SetLength(FDashSmallGapPattern, 0);
  SetLength(FDashDotPattern, 0);
  SetLength(FDashDotDotPattern, 0);
  SetLength(FLongDashPattern, 0);
  inherited Destroy;
end;

procedure TdxPatternLinePainterParameters.Initialize(ADpiX: Single);
const
  ADotPattern: array [0..1] of Single = (10, 10);
  ADashPattern: array [0..1] of Single = (40, 20);
  ADashSmallGapPattern: array [0..1] of Single = (40, 10);
  ADashDotPattern: array [0..3] of Single = (40, 13, 13, 13);
  ADashDotDotPattern: array [0..5] of Single = (30, 10, 10, 10, 10, 10);
  ALongDashPattern: array [0..1] of Single = (80, 40);
begin
  FDotPattern := CreatePattern(ADotPattern, ADpiX); 
  FDashPattern := CreatePattern(ADashPattern, ADpiX); 
  FDashSmallGapPattern := CreatePattern(ADashSmallGapPattern, ADpiX); 
  FDashDotPattern := CreatePattern(ADashDotPattern, ADpiX); 
  FDashDotDotPattern := CreatePattern(ADashDotDotPattern, ADpiX); 
  FLongDashPattern := CreatePattern(ALongDashPattern, ADpiX); 
  FPixelPenWidth := PixelsToUnits(1, ADpiX); 
  FPixelStep := PixelsToUnits(2, ADpiX); 
end;

{ TdxRichEditPatternLinePainterParameters }

constructor TdxRichEditPatternLinePainterParameters.Create(AUnitConverter: TdxDocumentLayoutUnitConverter);
begin
  inherited Create;
  Assert(AUnitConverter <> nil, 'AUnitConverter'); 
  FUnitConverter := AUnitConverter;
end;

destructor TdxRichEditPatternLinePainterParameters.Destroy;
begin
  SetLength(FSectionStartPattern, 0);
  SetLength(FPageBreakPattern, 0);
  SetLength(FColumnBreakPattern, 0);
  inherited Destroy;
end;

procedure TdxRichEditPatternLinePainterParameters.Initialize(ADpiX: Single);
const
  ASectionStartPattern: array [0..1] of Single = (10, 10);
  APageBreakPattern: array [0..1] of Single = (10, 10);
  AColumnBreakPattern: array [0..1] of Single = (10, 20);
begin
  FSectionStartPattern := CreatePattern(ASectionStartPattern, ADpiX);
  FPageBreakPattern := CreatePattern(APageBreakPattern, ADpiX);
  FColumnBreakPattern := CreatePattern(AColumnBreakPattern, ADpiX);
  inherited Initialize(ADpiX);
end;

function TdxRichEditPatternLinePainterParameters.PixelsToUnits(AValue, ADpi: Single): Single;
begin
  Result := FUnitConverter.PixelsToLayoutUnitsF(AValue, ADpi);
end;

{ TdxRichEditPatternLinePainter }

constructor TdxRichEditPatternLinePainter.Create(APainter: IdxPatternLinePaintingSupport;
  AUnitConverter: TdxDocumentLayoutUnitConverter);
begin
  inherited Create(APainter, AUnitConverter);
  FPixelGraphics := CreatePixelGraphics;
  PrepareParameters;
end;

destructor TdxRichEditPatternLinePainter.Destroy;
begin
  FreeAndNil(FParameters);


  DeleteDC(FPixelGraphics.Handle);
  FPixelGraphics.Canvas.Handle := 0;
  FPixelGraphics.Canvas.Free;
  FreeAndNil(FPixelGraphics);

  inherited Destroy;
end;

procedure TdxRichEditPatternLinePainter.DrawStrikeout(AStrikeout: TdxStrikeoutSingle; ABounds: TdxRectangleF;
  AColor: TColor);
begin
  DrawSolidLine(ABounds, AColor);
end;

class function TdxRichEditPatternLinePainter.CreatePixelGraphics: TdxGraphics;
begin
  Result := TcxCanvas.Create(TCanvas.Create);
  Result.Canvas.Handle := CreateCompatibleDC(0);
end;

procedure TdxRichEditPatternLinePainter.DrawStrikeout(AStrikeout: TdxStrikeoutDouble; ABounds: TdxRectangleF;
  AColor: TColor);
begin
  DrawDoubleSolidLine(ABounds, AColor);
end;

procedure TdxRichEditPatternLinePainter.DrawUnderline(AUnderline: TdxUnderlineDotted; ABounds: TdxRectangleF;
  AColor: TColor);
begin
  DrawPatternLine(ABounds, AColor, DotPattern);
end;

procedure TdxRichEditPatternLinePainter.DrawUnderline(AUnderline: TdxUnderlineSingle; ABounds: TdxRectangleF;
  AColor: TColor);
begin
  DrawSolidLine(ABounds, AColor);
end;

procedure TdxRichEditPatternLinePainter.DrawUnderline(AUnderline: TdxUnderlineDouble; ABounds: TdxRectangleF; AColor: TColor);
begin
  DrawDoubleSolidLine(ABounds, AColor);
end;

procedure TdxRichEditPatternLinePainter.DrawUnderline(AUnderline: TdxUnderlineThickDashDotted; ABounds: TdxRectangleF; AColor: TColor);
begin
  DrawPatternLine(ABounds, AColor, DashDotPattern);
end;

procedure TdxRichEditPatternLinePainter.DrawUnderline(AUnderline: TdxUnderlineThickDashed; ABounds: TdxRectangleF;
  AColor: TColor);
begin
  DrawPatternLine(ABounds, AColor, DashPattern);
end;

procedure TdxRichEditPatternLinePainter.DrawUnderline(AUnderline: TdxUnderlineThickDotted; ABounds: TdxRectangleF;
  AColor: TColor);
begin
  DrawPatternLine(ABounds, AColor, DotPattern);
end;

procedure TdxRichEditPatternLinePainter.DrawUnderline(AUnderline: TdxUnderlineThickSingle; ABounds: TdxRectangleF;
  AColor: TColor);
begin
  DrawSolidLine(ABounds, AColor);
end;

procedure TdxRichEditPatternLinePainter.DrawUnderline(AUnderline: TdxUnderlineLongDashed; ABounds: TdxRectangleF;
  AColor: TColor);
begin
  DrawPatternLine(ABounds, AColor, LongDashPattern);
end;

procedure TdxRichEditPatternLinePainter.DrawUnderline(AUnderline: TdxUnderlineHeavyWave; ABounds: TdxRectangleF;
  AColor: TColor);
begin
  ABounds := MakeBoundsAtLeast2PixelsHigh(ABounds);
  DrawWaveUnderline(ABounds, AColor, 2 * PixelPenWidth);
end;

procedure TdxRichEditPatternLinePainter.DrawUnderline(AUnderline: TdxUnderlineDashDotDotted; ABounds: TdxRectangleF;
  AColor: TColor);
begin
  DrawPatternLine(ABounds, AColor, DashDotDotPattern);
end;

procedure TdxRichEditPatternLinePainter.DrawUnderline(AUnderline: TdxUnderlineDashDotted; ABounds: TdxRectangleF;
  AColor: TColor);
begin
  DrawPatternLine(ABounds, AColor, DashDotPattern);
end;

procedure TdxRichEditPatternLinePainter.DrawUnderline(AUnderline: TdxUnderlineDashSmallGap; ABounds: TdxRectangleF;
  AColor: TdxColor);
begin
  DrawPatternLine(ABounds, AColor, DashSmallGapPattern);
end;

procedure TdxRichEditPatternLinePainter.DrawUnderline(AUnderline: TdxUnderlineDashed; ABounds: TdxRectangleF;
  AColor: TColor);
begin
  DrawPatternLine(ABounds, AColor, DashPattern);
end;

function TdxRichEditPatternLinePainter.GetParameters: TdxPatternLinePainterParameters;
begin
  Result := FParameters;
end;

function TdxRichEditPatternLinePainter.GetPixelGraphics: TdxGraphics;
begin
  Result := FPixelGraphics;
end;

procedure TdxRichEditPatternLinePainter.PrepareParameters;
var
  ARichEditParameters: TdxRichEditPatternLinePainterParameters;
begin
  ARichEditParameters := TdxRichEditPatternLinePainterParameters.Create(UnitConverter);
  FParameters := ARichEditParameters;
  InitializeParameters(ARichEditParameters);
end;

procedure TdxRichEditPatternLinePainter.DrawUnderline(AUnderline: TdxUnderlineThickDashDotDotted;
  ABounds: TdxRectangleF; AColor: TColor);
begin
  DrawPatternLine(ABounds, AColor, DashDotDotPattern);
end;

procedure TdxRichEditPatternLinePainter.DrawUnderline(AUnderline: TdxUnderlineThickLongDashed; ABounds: TdxRectangleF;
  AColor: TColor);
begin
  DrawPatternLine(ABounds, AColor, LongDashPattern);
end;

procedure TdxRichEditPatternLinePainter.DrawUnderline(AUnderline: TdxUnderlineDoubleWave; ABounds: TdxRectangleF;
  AColor: TColor);
var
  AThickness: Single;
  ATopBounds: TdxRectangleF;
  ABottomBounds: TdxRectangleF;
begin
  ABounds := RotateBounds(ABounds);

  AThickness := RoundToPixels(cxRectHeight(ABounds) / 2, Screen.PixelsPerInch); 
  if AThickness <= PixelsToUnits(1, Screen.PixelsPerInch) then 
    AThickness := PixelsToUnits(1, Screen.PixelsPerInch); 

  ATopBounds := cxRectFBounds(ABounds.Left, ABounds.Top, ABounds.Width, AThickness);
  ATopBounds := MakeBoundsAtLeast2PixelsHigh(ATopBounds);
  ABottomBounds := cxRectFBounds(ABounds.Left, ATopBounds.Bottom, ABounds.Width, AThickness);
  ABottomBounds := MakeBoundsAtLeast2PixelsHigh(ABottomBounds);

  DrawWaveUnderline(RotateBounds(ATopBounds), AColor, 0);
  DrawWaveUnderline(RotateBounds(ABottomBounds), AColor, 0);

end;

procedure TdxRichEditPatternLinePainter.DrawUnderline(AUnderline: TdxUnderlineWave; ABounds: TdxRectangleF;
  AColor: TColor);
begin
  ABounds := MakeBoundsAtLeast2PixelsHigh(RotateBounds(ABounds));
  DrawWaveUnderline(RotateBounds(ABounds), AColor, 0);
end;

{ TdxPatternLinePainter }

constructor TdxPatternLinePainter.Create(APainter: IdxPatternLinePaintingSupport;
  AUnitConverter: TdxDocumentLayoutUnitConverter);
begin
  inherited Create;
  Assert(APainter <> nil);
  FPainter := APainter;
  FUnitConverter := AUnitConverter;
end;

procedure TdxPatternLinePainter.DrawDoubleSolidLine(ABounds: TdxRectangleF; AColor: TColor);
var
  AThickness, AHalfThickness, ADistance: Double;
  ATopBounds, ABottomBounds: TdxRectangleF;
begin
  ABounds := RotateBounds(ABounds);
  AThickness := RoundToPixels(ABounds.Height / 4, UnitConverter.Dpi);
  if AThickness <= PixelsToUnits(1, UnitConverter.Dpi) then
    AThickness := PixelsToUnits(1, UnitConverter.Dpi);

  AHalfThickness := AThickness / 2;

  ATopBounds := cxRectFBounds(ABounds.Left, ABounds.Top + AHalfThickness, ABounds.Width, AThickness);
  ABottomBounds := cxRectFBounds(ABounds.Left, ABounds.Bottom - AHalfThickness, ABounds.Width, AThickness);

  ADistance := RoundToPixels(ABottomBounds.Top - ATopBounds.Bottom, UnitConverter.Dpi);
  if ADistance <= PixelsToUnits(1, UnitConverter.Dpi) then
    ADistance := PixelsToUnits(1, UnitConverter.Dpi);
  ABottomBounds := cxRectSetTop(ABottomBounds, ATopBounds.Bottom + Trunc(ADistance));

  DrawSolidLine(RotateBounds(ATopBounds), AColor);
  DrawSolidLine(RotateBounds(ABottomBounds), AColor);
end;

procedure TdxPatternLinePainter.DrawPatternLine(ABounds: TdxRectangleF; AColor: TColor; const APattern: TSingleDynArray);
var
  APenPattern: PIntArray;
  APen: TPen;
  I, APatternLength: Integer;
  ALogBrush: TLOGBRUSH;
  AFixedPattern: TSingleDynArray;
begin
  ABounds := RotateBounds(ABounds);
  AFixedPattern := MakeFixedWidthPattern(APattern, ABounds.Height);
  try
    APatternLength := Length(AFixedPattern);
    APenPattern := AllocMem(APatternLength * SizeOf(Integer));
    try
      for I := 0 to APatternLength - 1 do
        APenPattern[I] := Trunc(AFixedPattern[I]);
      APen := TPen.Create;
      try
        ALogBrush.lbStyle := BS_SOLID;
        ALogBrush.lbColor := AColor;
        ALogBrush.lbHatch := 0;
        APen.Handle := ExtCreatePen(PS_GEOMETRIC or PS_USERSTYLE or PS_ENDCAP_FLAT, Trunc(cxRectHeight(ABounds)),
          ALogBrush, Length(AFixedPattern), APenPattern);
        APen.Color := AColor;
        DrawLine(APen, RotateBounds(ABounds));
      finally
        APen.Free;
      end;
    finally
      FreeMem(APenPattern);
    end;
  finally
   SetLength(AFixedPattern, 0);
  end;
end;

function TdxPatternLinePainter.RotateBounds(const ABounds: TdxRectangleF): TdxRectangleF;
begin
  Result := ABounds;
end;

function TdxPatternLinePainter.RotatePoint(APointF: TPointFloat): TPointFloat;
begin
  Result := APointF;
end;

procedure TdxPatternLinePainter.DrawLine(APen: TPen; ABounds: TdxRectangleF);
begin
  APen.Width := Trunc(cxRectHeight(ABounds)); 
  Painter.DrawLine(APen, ABounds.Left, ABounds.Top, ABounds.Right, ABounds.Top);
end;

procedure TdxPatternLinePainter.DrawSolidLine(ABounds: TdxRectangleF; AColor: TColor);
var
  APen: TPen;
begin
  APen := FPainter.GetPen(AColor, Min(cxRectWidth(ABounds), cxRectHeight(ABounds)));
  try
    DrawLine(APen, ABounds);
  finally
    FPainter.ReleasePen(APen);
  end;
end;

procedure TdxPatternLinePainter.DrawWaveUnderline(ABounds: TdxRectangleF; APen: TPen; AStep: Single);

  function PointFloat(X, Y: Single): TPointFloat;
  begin
    Result.x := X;
    Result.y := Y;
  end;

var
  ACount: Integer;
  X, Y, ASum: Single;
  APoints: array of TPointFloat;
  I: Integer;
begin
  ABounds := RotateBounds(ABounds);
  ACount := Math.Ceil(ABounds.Width / AStep); 
  if ACount <= 1 then
    Exit;

  X := ABounds.Left;
  Y := ABounds.Top;
  ASum := Y + ABounds.Bottom;
  SetLength(APoints, ACount); 
  try
    for I := 0 to ACount - 1 do
    begin
      APoints[I] := RotatePoint(PointFloat(X, Y));
      Y := ASum - Y;
      X := X + AStep;
    end;
    Painter.DrawLines(APen, APoints);
  finally
    SetLength(APoints, 0);
  end;
end;

procedure TdxPatternLinePainter.DrawWaveUnderline(ABounds: TdxRectangleF; APen: TPen);
var
  AStep: Single;
begin
  ABounds := RotateBounds(ABounds);
  AStep := RoundToPixels(ABounds.Height, Screen.PixelsPerInch); 
  AStep := Math.Max(PixelStep, AStep);

  DrawWaveUnderline(RotateBounds(ABounds), APen, AStep);
end;

procedure TdxPatternLinePainter.DrawWaveUnderline(ABounds: TdxRectangleF; AColor: TColor; APenWidth: Single);
var
  APen: TPen;
begin
  APen := FPainter.GetPen(AColor, APenWidth);
  try
    DrawWaveUnderline(ABounds, APen);
  finally
    FPainter.ReleasePen(APen);
  end;
end;

function TdxPatternLinePainter.GetDashDotDotPattern: TSingleDynArray;
begin
  Result := Parameters.DashDotDotPattern;
end;

function TdxPatternLinePainter.GetDashDotPattern: TSingleDynArray;
begin
  Result := Parameters.DashDotPattern;
end;

function TdxPatternLinePainter.GetDashPattern: TSingleDynArray;
begin
  Result := Parameters.DashPattern;
end;

function TdxPatternLinePainter.GetDashSmallGapPattern: TSingleDynArray;
begin
  Result := Parameters.DashSmallGapPattern;
end;

function TdxPatternLinePainter.GetDotPattern: TSingleDynArray;
begin
  Result := Parameters.DotPattern;
end;

function TdxPatternLinePainter.GetLongDashPattern: TSingleDynArray;
begin
  Result := Parameters.LongDashPattern;
end;

function TdxPatternLinePainter.GetPixelPenWidth: Single;
begin
  Result := Parameters.PixelPenWidth;
end;

function TdxPatternLinePainter.GetPixelStep: Single;
begin
  Result := Parameters.PixelStep;
end;

function TdxPatternLinePainter.MakeBoundsAtLeast2PixelsHigh(ABounds: TdxRectangleF): TdxRectangleF;
var
  AHeight: Single;
begin
  AHeight := RoundToPixels(ABounds.Height, Screen.PixelsPerInch); 
  if AHeight <= PixelsToUnits(2, Screen.PixelsPerInch) then 
  begin
    AHeight := PixelsToUnits(2, Screen.PixelsPerInch); 
    ABounds := cxRectFBounds(ABounds.Left, ABounds.Top, ABounds.Width, AHeight);
  end;
  Result := ABounds;
end;

function TdxPatternLinePainter.MakeFixedWidthPattern(const APattern: TSingleDynArray; AThickness: Single): TSingleDynArray;
var
  ACount: Integer;
  I: Integer;
begin
  ACount := Length(APattern);
  SetLength(Result, ACount);
  for I := 0 to ACount - 1 do
    Result[I] := APattern[I] / AThickness;
end;

function TdxPatternLinePainter.PixelsToUnits(AVal, ADpi: Double): Double;
begin
  Result := UnitConverter.PixelsToLayoutUnitsF(AVal, ADpi);
end;

function TdxPatternLinePainter.RoundToPixels(AVal, ADpi: Double): Double;
var
  APixelVal: Double;
begin
  APixelVal := UnitsToPixels(AVal, ADpi);
  APixelVal := Round(APixelVal);
  Result := PixelsToUnits(APixelVal, ADpi);
end;

function TdxPatternLinePainter.UnitsToPixels(AVal, ADpi: Double): Double;
begin
  Result := UnitConverter.LayoutUnitsToPixelsF(AVal, ADpi);
end;

{ TdxRichEditHorizontalPatternLinePainter }

procedure TdxRichEditHorizontalPatternLinePainter.InitializeParameters(AParameters: TdxPatternLinePainterParameters);
begin
  AParameters.Initialize(Screen.PixelsPerInch); 
end;

end.
