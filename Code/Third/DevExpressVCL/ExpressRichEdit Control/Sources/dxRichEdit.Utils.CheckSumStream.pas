{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Utils.CheckSumStream;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  SysUtils, Classes, Generics.Collections;

type
  { IdxCheckSumCalculator }

  IdxCheckSumCalculator<T> = interface
  ['{3B690C6C-57EE-4074-B47A-364D48EBBFFB}']
    function InitialCheckSumValue: T;
    function UpdateCheckSum(AValue: T; const ABuffer; ACount: Integer): T;
    function GetFinalCheckSum(AValue: T): T;
  end;

  { TdxCheckSumStream }

  TdxCheckSumStream<T> = class(TStream)
  strict private
    FStream: TStream;
    FCheckSumCalculator: IdxCheckSumCalculator<T>;
    FReadCheckSum: T;
    FWriteCheckSum: T;
  protected
    function GetSize: Int64; override;
    procedure SetSize(const NewSize: Int64); override;

    function GetReadCheckSum: T;
    function GetWriteCheckSum: T;
  public
    constructor Create(AStream: TStream; const ACheckSumCalculator: IdxCheckSumCalculator<T>);

    procedure ResetCheckSum;

    function Read(var Buffer; Count: Longint): Longint; override;
    function Seek(const Offset: Int64; Origin: TSeekOrigin): Int64; override;
    function Write(const Buffer; Count: Longint): Longint; override;

    procedure WriteByte(Value: Byte);

    property Stream: TStream read FStream;
    property ReadCheckSum: T read GetReadCheckSum;
    property WriteCheckSum: T read GetWriteCheckSum;

  end;

  { TdxCrc32Stream }

  TdxCrc32Stream = class(TdxCheckSumStream<Cardinal>)
  public
    constructor Create(AStream: TStream); reintroduce;
  end;

  { TdxCrc32CheckSumCalculator }

  TdxCrc32CheckSumCalculator = class(TInterfacedObject, IdxCheckSumCalculator<Cardinal>)
  protected
    //IdxCheckSumCalculator
    function InitialCheckSumValue: Cardinal;
    function UpdateCheckSum(AValue: Cardinal; const ABuffer; ACount: Integer): Cardinal;
    function GetFinalCheckSum(AValue: Cardinal): Cardinal;
  end;

  { TdxCrc32CheckSum }

  TdxCrc32CheckSum = class
  private
    class var FTable: TArray<Cardinal>;
    class constructor Initialize;
    class destructor Finalize;
  protected
    class property Table: TArray<Cardinal> read FTable;
  public
    class function Update(ACheckSum: Cardinal; const ABuffer; ACount: Integer): Cardinal;
  end;


implementation

uses
  Math;

{ TdxCheckSumStream<T> }

constructor TdxCheckSumStream<T>.Create(AStream: TStream;
  const ACheckSumCalculator: IdxCheckSumCalculator<T>);
begin
  inherited Create;
  FStream := AStream;
  FCheckSumCalculator := ACheckSumCalculator;
  ResetCheckSum;
end;

procedure TdxCheckSumStream<T>.ResetCheckSum;
begin
  FReadCheckSum := FCheckSumCalculator.InitialCheckSumValue;
  FWriteCheckSum := FCheckSumCalculator.InitialCheckSumValue;
end;

function TdxCheckSumStream<T>.Seek(const Offset: Int64;
  Origin: TSeekOrigin): Int64;
begin
  Result := Stream.Seek(Offset, Origin);
end;

procedure TdxCheckSumStream<T>.SetSize(const NewSize: Int64);
begin
  Stream.Size := NewSize;
end;

function TdxCheckSumStream<T>.GetReadCheckSum: T;
begin
  Result := FCheckSumCalculator.GetFinalCheckSum(FReadCheckSum);
end;

function TdxCheckSumStream<T>.GetSize: Int64;
begin
  Result := Stream.Size;
end;

function TdxCheckSumStream<T>.GetWriteCheckSum: T;
begin
  Result := FCheckSumCalculator.GetFinalCheckSum(FWriteCheckSum);
end;

function TdxCheckSumStream<T>.Read(var Buffer; Count: Longint): Longint;
var
  AOffset: Integer;
  ACount: Integer;
begin
  ACount := Count;
  Result := Stream.Read(Buffer, Count);
  FReadCheckSum := FCheckSumCalculator.UpdateCheckSum(FReadCheckSum, Buffer, ACount);
end;

function TdxCheckSumStream<T>.Write(const Buffer; Count: Longint): Longint;
var
  AOffset: Integer;
  ACount: Integer;
begin
  ACount := Count;
  Result := Stream.Write(Buffer, Count);
  FWriteCheckSum := FCheckSumCalculator.UpdateCheckSum(FWriteCheckSum, Buffer, Result);
end;

procedure TdxCheckSumStream<T>.WriteByte(Value: Byte);
begin
  Write(Value, 1);
end;

{ TdxCrc32Stream }

constructor TdxCrc32Stream.Create(AStream: TStream);
begin
  inherited Create(AStream, TdxCrc32CheckSumCalculator.Create);
end;

{ TdxCrc32CheckSumCalculator }

function TdxCrc32CheckSumCalculator.GetFinalCheckSum(
  AValue: Cardinal): Cardinal;
begin
  Result := AValue xor $FFFFFFFF;
end;

function TdxCrc32CheckSumCalculator.InitialCheckSumValue: Cardinal;
begin
  Result := $FFFFFFFF;
end;

function TdxCrc32CheckSumCalculator.UpdateCheckSum(AValue: Cardinal;
  const ABuffer; ACount: Integer): Cardinal;
begin
  Result := TdxCrc32CheckSum.Update(AValue, ABuffer, ACount);
end;

{ TdxCrc32CheckSum }

class constructor TdxCrc32CheckSum.Initialize;
var
  N, C: Cardinal;
  K: Integer;
begin
  SetLength(FTable, 256);
  for N := 0 to 255 do
  begin
    C := N;
    for K := 0 to 7 do
    begin
      if C and 1 <> 0 then
        C := $EDB88320 xor (C shr 1)
      else
        C := C shr 1;
    end;
    FTable[N] := C;
  end;
end;

class destructor TdxCrc32CheckSum.Finalize;
begin
  SetLength(FTable, 0);
end;

class function TdxCrc32CheckSum.Update(ACheckSum: Cardinal;
  const ABuffer; ACount: Integer): Cardinal;
var
  I: Integer;
  AValue: Byte;
begin
  Result := ACheckSum;
  for I := 0 to ACount - 1 do
  begin
    System.Move(Pointer(Longint(@ABuffer) + I)^, Pointer(@AValue)^, 1);
    Result := (Result shr 8) xor Table[(Result xor AValue) and $FF];
  end;
end;

end.
