{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.LayoutEngine.BoxMeasurer; 

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Windows, Classes, Types, SysUtils, Graphics,
  dxCore, cxGeometry, dxCoreClasses, dxGDIPlusAPI, dxGDIPlusClasses,
  dxRichEdit.Platform.Font, dxRichEdit.DocumentModel.PieceTable;


type
  PdxIntegerArray = ^TdxIntegerArray;
  TdxIntegerArray = array [0..0] of Integer;

  PdxWordArray = ^TdxWordArray;
  TdxWordArray = array [0..0] of Integer;

  IntPtr = PINT;

  { TdxGdiTextViewInfo }

  TdxGdiTextViewInfo = class(TdxTextViewInfo)
  private
    FGlyphCount: Integer;
    FGlyphs: PdxWordArray;
    FCharacterWidths: PdxIntegerArray;
  public
    destructor Destroy; override;

    property Glyphs: PdxWordArray read FGlyphs write FGlyphs;
    property GlyphCount: Integer read FGlyphCount write FGlyphCount;
    property CharacterWidths: PdxIntegerArray read FCharacterWidths write FCharacterWidths;
  end;

  { TdxGdiBoxMeasurer }

  TdxGdiBoxMeasurer = class(TdxBoxMeasurer)
  private
    FCaretPosBufferSize: Integer;
    FCaretPosBuffer: Pointer;
    FGraphics: TCanvas;
  protected
    procedure AdjustCharacterBoundsForLigature(const ACharacterBounds: TRects; AFrom, ATo: Integer);
    procedure EstimateCaretPositionsForLigatures(const ACharacterBounds: TRects);
    function CalculateCharactersBounds(ACaret: PINT; ALength: Integer; const ABounds: TRect): TRects;
    function MeasureCharactersWithGetCharacterPlacementSlow(AHdc: THandle; const AText: string; var AGcpResults: TGCPResultsW): Integer;
  public
    constructor Create(ADocumentModel: TdxDocumentModel; AGraphics: TCanvas);
    destructor Destroy; override;

    function GetHdc: HDC;
    property Graphics: TCanvas read FGraphics;

    function MeasureCharactersBounds(const AText: string; AFontInfo: TdxFontInfo; const ABounds: TRect): TRects; override;
    function MeasureWithGetCharacterPlacementSlow(AHdc: THandle; const AText: string; var AGcpResults: TGCPResultsW): Integer;
    function CreateTextViewInfoCore(AHdc: HDC; const AText: string): TdxGdiTextViewInfo; virtual; 
    function CreateTextViewInfo(ABoxInfo: TdxBoxInfo; const AText: string; AFontInfo: TdxFontInfo): TdxTextViewInfo; override; 
    function GetCaretPosBuffer(AItemsCount: Integer): Pointer; 
    function SnapToPixels(AValue: Integer; ADpi: Single): Integer; 
  end;

  { TdxRectangleUtils }

  TdxRectangleUtils = class
  public
    class function SplitHorizontally(const ABounds: TRect; ACellCount: Integer): TRects; static;
    class function BoundingRectangle(const APoints: array of TPoint): TRect; overload; static;
    class function BoundingRectangle(const ABounds: TRect; ATransform: TdxTransformMatrix): TRect; overload; static;
    class function CenterPoint(const ARectangle: TRect): TPoint; static;
  end;

implementation

uses
  dxRichEdit.Platform.Win.Font, Math, dxTypeHelpers;

{ TdxGdiTextViewInfo }

destructor TdxGdiTextViewInfo.Destroy;
begin
  FreeMem(FGlyphs);
  FreeMem(FCharacterWidths);
  inherited Destroy;
end;

{ TdxGdiBoxMeasurer }

constructor TdxGdiBoxMeasurer.Create(ADocumentModel: TdxDocumentModel; AGraphics: TCanvas);
const
  dxInitialBufferItemCount = 64;
begin
  inherited Create(ADocumentModel);
  FGraphics := AGraphics;
  GetCaretPosBuffer(dxInitialBufferItemCount);
end;

destructor TdxGdiBoxMeasurer.Destroy;
begin
  FreeMem(FCaretPosBuffer);
  inherited Destroy;
end;

function TdxGdiBoxMeasurer.CreateTextViewInfo(ABoxInfo: TdxBoxInfo; const AText: string;
  AFontInfo: TdxFontInfo): TdxTextViewInfo;
var
  AHdc: HDC;
  AGdiPlusFontInfo: TdxGdiFontInfo absolute AFontInfo;
begin
  AHdc := GetHdc;
  SelectObject(AHdc, AGdiPlusFontInfo.GdiFontHandle);
  Result := CreateTextViewInfoCore(AHdc, AText);
end;

function TdxGdiBoxMeasurer.CreateTextViewInfoCore(AHdc: HDC; const AText: string): TdxGdiTextViewInfo;
var
  AGcpResults: TGCPResultsW;
  ASize: Cardinal;
  ALength, AWidth, AHeight: Integer; 
begin
  ALength := Length(AText);
  cxZeroMemory(@AGcpResults, SizeOf(TGCPResults));
  AGcpResults.lStructSize := SizeOf(TGCPResults);

  AGcpResults.lpCaretPos := GetCaretPosBuffer(ALength);
  GetMem(AGcpResults.lpDx, SizeOf(Integer) * ALength);
  GetMem(AGcpResults.lpGlyphs, SizeOf(Word) * ALength);
  if ALength > 0 then
    PWord(AGcpResults.lpGlyphs)^ := 0;
  AGcpResults.nGlyphs := ALength;

  ASize := GetCharacterPlacement(AHdc, PChar(AText), ALength, 0, AGcpResults, GCP_USEKERNING or GCP_LIGATE);
  if (ASize = 0) and (ALength > 0) then
    ASize := MeasureWithGetCharacterPlacementSlow(AHdc, AText, AGcpResults);
  AWidth := LongRec(ASize).Lo;
  AHeight := LongRec(ASize).Hi;


  Result := TdxGdiTextViewInfo.Create;
  Result.Size := cxSize(AWidth, AHeight);
  Result.Glyphs := Pointer(AGcpResults.lpGlyphs);
  Result.GlyphCount := AGcpResults.nGlyphs;
  Result.CharacterWidths := Pointer(AGcpResults.lpDx);
end;

function TdxGdiBoxMeasurer.GetCaretPosBuffer(AItemsCount: Integer): Pointer;
var
  ASize: Integer;
begin
  ASize := SizeOf(Integer) * AItemsCount;
  if ASize > FCaretPosBufferSize then
  begin
    ReallocMem(FCaretPosBuffer, ASize);
    FCaretPosBufferSize := ASize;
  end;
  Result := FCaretPosBuffer;
end;

function TdxGdiBoxMeasurer.GetHdc: HDC;
begin
  Result := FGraphics.Handle;
end;

function TdxGdiBoxMeasurer.MeasureCharactersBounds(const AText: string; AFontInfo: TdxFontInfo; const ABounds: TRect): TRects;
var
  AGcpResults: TGCPResultsW;
  ASize: Cardinal;
  ACaretPos: Integer;
  ACaret: Pointer;
  ALength: Integer;
  ADC: HDC;
  AOldFont: THandle;
  AGdiFontInfo: TdxGdiFontInfo absolute AFontInfo;
begin
  ADC := CreateCompatibleDC(0);
  ALength := Length(AText);
  AOldFont := SelectObject(ADC, AGdiFontInfo.GdiFontHandle);
  try
    ACaretPos := SizeOf(Integer) * ALength;
    GetMem(ACaret, ACaretPos);
    try
      AGcpResults.lStructSize := SizeOf(GCP_RESULTS);
      AGcpResults.lpOutString := nil;
      AGcpResults.lpOrder := nil;
      AGcpResults.lpDx := nil;
      AGcpResults.lpCaretPos := ACaret;
      AGcpResults.lpClass := nil;
      AGcpResults.lpGlyphs := nil;
      AGcpResults.nGlyphs := ALength;
      AGcpResults.nMaxFit := 0;
      ASize := GetCharacterPlacementW(ADC, PChar(AText), ALength, 0, AGcpResults, GCP_USEKERNING or GCP_LIGATE);
      if (ASize = 0) and (ALength > 0) then
        MeasureCharactersWithGetCharacterPlacementSlow(ADC, AText, AGcpResults);
      Result := CalculateCharactersBounds(AGcpResults.lpCaretPos, ALength, ABounds);
    finally
      FreeMem(ACaret);
    end;
  finally
    SelectObject(ADC, AOldFont);
    DeleteDC(ADC);
  end;
end;

function TdxGdiBoxMeasurer.MeasureWithGetCharacterPlacementSlow(AHdc: THandle; const AText: string; var AGcpResults: TGCPResultsW): Integer;
var
  I, AAdd, AStep, ALength: Integer;
begin
  Result := 0;
  ALength := Length(AText);
  AStep := Max(1, ALength div 2);
  AAdd := AStep;
  I := 0;
  while I < 3 do
  begin
    ReallocMem(AGcpResults.lpDx, SizeOf(Integer) * (ALength + AAdd));
    ReallocMem(AGcpResults.lpGlyphs, SizeOf(Word) * (ALength + AAdd));
    AGcpResults.nGlyphs := ALength + AAdd;
    if ALength > 0 then
      PWord(AGcpResults.lpGlyphs)^ := 0;
    Result := GetCharacterPlacement(AHdc, PWideChar(AText), ALength, 0, AGcpResults, GCP_USEKERNING or GCP_LIGATE);
    if Result <> 0 then
      Exit;
    Inc(I);
  end;
end;

function TdxGdiBoxMeasurer.SnapToPixels(AValue: Integer; ADpi: Single): Integer;
begin
  Result := DocumentModel.LayoutUnitConverter.SnapToPixels(AValue, ADpi);
end;

function TdxGdiBoxMeasurer.CalculateCharactersBounds(ACaret: PINT; ALength: Integer; const ABounds: TRect): TRects;
var
  I, APrevPos, ANextPos: Integer;
  ANeedProcessLigatures: Boolean;
begin
  ANeedProcessLigatures := False;
  SetLength(Result, ALength);
  APrevPos := ACaret^;
  for I := 0 to ALength - 2 do
  begin
    Inc(ACaret);
    ANextPos := ACaret^;
    Result[I].InitSize(ABounds.Left + APrevPos, ABounds.Top, ANextPos - APrevPos, ABounds.Height);
    ANeedProcessLigatures := (APrevPos = ANextPos) or ANeedProcessLigatures;
    APrevPos := ANextPos;
  end;
  if ALength > 0 then
    Result[ALength - 1].InitSize(ABounds.Left + APrevPos, ABounds.Top, ABounds.Width - APrevPos, ABounds.Height);
  if ANeedProcessLigatures then
    EstimateCaretPositionsForLigatures(Result);
end;

function TdxGdiBoxMeasurer.MeasureCharactersWithGetCharacterPlacementSlow(AHdc: THandle; const AText: string; var AGcpResults: TGCPResultsW): Integer;
var
  I, AAdd, AStep, ALength: Integer;
begin
  Result := 0;
  ALength := Length(AText);
  AStep := Max(1, ALength div 2);
  AAdd := AStep;
  I := 0;
  while I < 3 do
  begin
    ReallocMem(AGcpResults.lpCaretPos, SizeOf(Integer) * (ALength + AAdd));
    AGcpResults.nGlyphs := ALength + AAdd;
    Result := GetCharacterPlacement(AHdc, PChar(AText), ALength, 0, AGcpResults, GCP_USEKERNING or GCP_LIGATE);
    if Result <> 0 then
      Exit;
    Inc(I);
  end;
end;

procedure TdxGdiBoxMeasurer.EstimateCaretPositionsForLigatures(const ACharacterBounds: TRects);
var
  ABounds: TRect;
  I, AFrom, ACount: Integer;
begin
  ACount := Length(ACharacterBounds);
  AFrom := MaxInt;
  for I := 0 to ACount - 1 do
  begin
    ABounds := ACharacterBounds[I];
    if (ABounds.Right - ABounds.Left) = 0 then
    begin
      if AFrom = MaxInt then
        AFrom := I;
    end
    else
      if AFrom < I then
      begin
        AdjustCharacterBoundsForLigature(ACharacterBounds, AFrom, I);
        AFrom := MaxInt;
      end;
  end;
end;

procedure TdxGdiBoxMeasurer.AdjustCharacterBoundsForLigature(const ACharacterBounds: TRects;
  AFrom, ATo: Integer);
var
  I, ACount: Integer;
  ABounds: TRects;
begin
  ACount := ATo - AFrom + 1;
  ABounds := TdxRectangleUtils.SplitHorizontally(ACharacterBounds[ATo], ACount);
  for I := 0 to ACount - 1 do
    ACharacterBounds[I + AFrom] := ABounds[I];
end;

class function TdxRectangleUtils.BoundingRectangle(const APoints: array of TPoint): TRect;
var
  I, ACount, AMaxX, AMaxY, AMinX, AMinY: Integer;
begin
  ACount := Length(APoints);
  if ACount = 0 then
    Exit(cxNullRect);
  AMinX := APoints[0].X;
  AMaxX := AMinX;
  AMinY := APoints[0].Y;
  AMaxY := AMinY;
  for I := 1 to ACount - 1 do
  begin
    AMinX := Math.Min(APoints[I].X, AMinX);
    AMinY := Math.Min(APoints[I].Y, AMinY);
    AMaxX := Math.Max(APoints[I].X, AMaxX);
    AMaxY := Math.Max(APoints[I].Y, AMaxY);
  end;
  Result := Rect(AMinX, AMinY, AMaxX, AMaxY);
end;

class function TdxRectangleUtils.BoundingRectangle(const ABounds: TRect; ATransform: TdxTransformMatrix): TRect;
var
  APt0, APt1, APt2, APt3: TPoint;
begin
  APt0 := ATransform.TransformPoint(Point(ABounds.Right, ABounds.Bottom));
  APt1 := ATransform.TransformPoint(Point(ABounds.Right, ABounds.Top));
  APt2 := ATransform.TransformPoint(Point(ABounds.Left, ABounds.Bottom));
  APt3 := ATransform.TransformPoint(ABounds.Location);

  Result := TdxRectangleUtils.BoundingRectangle([APt0, APt1, APt2, APt3]);
end;

class function TdxRectangleUtils.CenterPoint(const ARectangle: TRect): TPoint;
begin
  Result := Point(Trunc((ARectangle.Right + ARectangle.Left) / 2), Trunc((ARectangle.Bottom + ARectangle.Top) / 2));
end;

class function TdxRectangleUtils.SplitHorizontally(const ABounds: TRect; ACellCount: Integer): TRects;
var
  I, AWidth, ARemainder, AColumnWidth, AColumnsAreaWidth, AHeight, AOffset: Integer;
begin
  if ACellCount <= 0 then
  begin
    SetLength(Result, 1);
    Result[0] := ABounds;
    Exit;
  end;
  SetLength(Result, ACellCount);
  AOffset := ABounds.Left;
  AHeight := ABounds.Height;
  AColumnsAreaWidth := ABounds.Width;
  AColumnWidth := AColumnsAreaWidth div ACellCount;
  ARemainder := AColumnsAreaWidth - (AColumnWidth * ACellCount);
  for I := 0 to ACellCount - 1 do
  begin
    AWidth := AColumnWidth;
    if ARemainder > 0 then
      Inc(AWidth);
    Result[I].InitSize(AOffset, ABounds.Top, AWidth, AHeight);
    Inc(AOffset, AWidth);
    Dec(ARemainder)
  end;
end;

end.
