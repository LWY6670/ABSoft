{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Import.Rtf.DestinationListLevel;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Types, Classes, SysUtils, Graphics, Windows, Math, dxCoreClasses, dxRichEdit.Import.Rtf,
  Generics.Collections, dxRichEdit.Import.Rtf.DestinationPieceTable, dxRichEdit.DocumentModel.PieceTable, dxRichEdit.DocumentModel.CharacterFormatting,
  dxRichEdit.DocumentModel.ParagraphFormatting, dxRichEdit.DocumentModel.Section;

type
  TdxListLevelNumbersDestination = class(TdxStringValueDestination);

  { TdxListLevelTextDestination }

  TdxListLevelTextDestination = class(TdxStringValueDestination)
  private
    FLevelTemplateId: Integer;
    //handlers
    class procedure ListLevelTemplateIdKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
  protected
    procedure InitializeClone(AClone: TdxRichEditRtfDestinationBase); override;
    procedure PopulateKeywordTable(ATable: TdxKeywordTranslatorTable); override;

    property LevelTemplateId: Integer read FLevelTemplateId;
  end;

  { TdxListLevelDestination }

  TdxListLevelDestination = class(TdxRichEditRtfDestinationBase)
  public class var
    NumberingFormats: TList<TdxNumberingFormat>;
  strict private class var
    class constructor Initialize;
    class destructor Finalize;
    class function CreateNumberingFormatList: TList<TdxNumberingFormat>; static;
  private
    FLevel: TdxRtfListLevel;

    procedure ApplyListLevelCharacterProperties;
    procedure ApplyListLevelParagraphProperties;
    procedure TryToHandleFinishOfListLevelNumbersDestination(ANestedDestination: TdxRichEditRtfDestinationBase);
    procedure TryToHandleFinishOfListLevelTextDestination(ANestedDestination: TdxRichEditRtfDestinationBase);

    //Handlers
    class procedure ListLevelAlignmentKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure ListLevelFollowKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure ListLevelIndentKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure ListLevelLegalKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure ListLevelNoRestartKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure ListLevelNumberingFormatKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure ListLevelNumbersKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure ListLevelOldKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure ListLevelPictureKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure ListLevelPictureNoSizeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure ListLevelPrevKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure ListLevelPrevspaceKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure ListLevelSpaceKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure ListLevelStartAtKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure ListLevelTentativeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure ListLevelTextKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure ParagraphStyleKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    procedure SetLevel(const Value: TdxRtfListLevel);
  protected
    function GetControlCharHT: TdxControlCharTranslatorTable; override;
    procedure InitializeClone(AClone: TdxRichEditRtfDestinationBase); override;
    procedure PopulateKeywordTable(ATable: TdxKeywordTranslatorTable); override;
    procedure ProcessControlCharCore(AChar: Char); override;
    procedure ProcessCharCore(AChar: Char); override;
    function ProcessKeywordCore(const AKeyword: string;
      AParameterValue: Integer; AHasParameter: Boolean): Boolean; override;
  public
    constructor Create(AImporter: TdxRichEditDocumentModelRtfImporter); override;
    procedure BeforePopRtfState; override;
    procedure NestedGroupFinished(ADestination: TdxRichEditRtfDestinationBase); override;

    property Level: TdxRtfListLevel read FLevel;
  end;

  TdxListOverrideLevelDestination = class(TdxRichEditRtfDestinationBase)
  private
    FOverrideLevel: TdxRtfListOverrideLevel;
    //Handlers
    class procedure ListOverrideFormatKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure ListOverrideListLevelKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure ListOverrideStartAtKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure ListOverrideStartAtValueKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    procedure SetOverrideLevel(const Value: TdxRtfListOverrideLevel);
  protected
    function GetControlCharHT: TdxControlCharTranslatorTable; override;
    procedure PopulateKeywordTable(ATable: TdxKeywordTranslatorTable); override;
    procedure InitializeClone(AClone: TdxRichEditRtfDestinationBase); override;
  public
    constructor Create(AImporter: TdxRichEditDocumentModelRtfImporter); override;

    property OverrideLevel: TdxRtfListOverrideLevel read FOverrideLevel;
  end;

implementation

uses
  dxRichEdit.DocumentModel.Core, dxRichEdit.Utils.BatchUpdateHelper, RTLConsts, 
  dxRichEdit.Platform.Font, dxRichEdit.Utils.Characters, dxRichEdit.Import.Rtf.DestinationDefault,
  dxRichEdit.DocumentModel.Numbering;

{ TdxListLevelTextDestination }

procedure TdxListLevelTextDestination.InitializeClone(AClone: TdxRichEditRtfDestinationBase);
begin
  inherited InitializeClone(AClone);
  TdxListLevelTextDestination(AClone).FLevelTemplateId := LevelTemplateId;
end;

procedure TdxListLevelTextDestination.PopulateKeywordTable(ATable: TdxKeywordTranslatorTable);
begin
  ATable.Add('leveltemplateid', ListLevelTemplateIdKeywordHandler);
end;

class procedure TdxListLevelTextDestination.ListLevelTemplateIdKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  TdxListLevelTextDestination(AImporter.Destination).FLevelTemplateId := IfThen(AHasParameter, AParameterValue, 0);
end;

{ TdxListLevelDestination }

constructor TdxListLevelDestination.Create(AImporter: TdxRichEditDocumentModelRtfImporter);
begin
  inherited Create(AImporter);
  FLevel := TdxRtfListLevel.Create(Importer.DocumentModel);
end;

class constructor TdxListLevelDestination.Initialize;
begin
  NumberingFormats := CreateNumberingFormatList;
end;

procedure TdxListLevelDestination.InitializeClone(
  AClone: TdxRichEditRtfDestinationBase);
begin
  inherited InitializeClone(AClone);
  TdxListLevelDestination(AClone).SetLevel(Level);
end;

class destructor TdxListLevelDestination.Finalize;
begin
  FreeAndNil(NumberingFormats);
end;

class function TdxListLevelDestination.CreateNumberingFormatList: TList<TdxNumberingFormat>;
begin
  Result := TList<TdxNumberingFormat>.Create;
  Result.Add(TdxNumberingFormat.Decimal);
  Result.Add(TdxNumberingFormat.UpperRoman);
  Result.Add(TdxNumberingFormat.LowerRoman);
  Result.Add(TdxNumberingFormat.UpperLetter);
  Result.Add(TdxNumberingFormat.LowerLetter);
  Result.Add(TdxNumberingFormat.Ordinal);
  Result.Add(TdxNumberingFormat.CardinalText);
  Result.Add(TdxNumberingFormat.OrdinalText);
  Result.Add(TdxNumberingFormat.None); //*
  Result.Add(TdxNumberingFormat.None);//*
  Result.Add(TdxNumberingFormat.None); //*
  Result.Add(TdxNumberingFormat.None);//*
  Result.Add(TdxNumberingFormat.AIUEOHiragana);
  Result.Add(TdxNumberingFormat.Iroha);
  Result.Add(TdxNumberingFormat.None);//*
  Result.Add(TdxNumberingFormat.None);//*
  Result.Add(TdxNumberingFormat.None);//*
  Result.Add(TdxNumberingFormat.None);//*
  Result.Add(TdxNumberingFormat.DecimalEnclosedCircle);
  Result.Add(TdxNumberingFormat.None);//*
  Result.Add(TdxNumberingFormat.AIUEOFullWidthHiragana);
  Result.Add(TdxNumberingFormat.IrohaFullWidth);
  Result.Add(TdxNumberingFormat.DecimalZero);
  Result.Add(TdxNumberingFormat.Bullet);
  Result.Add(TdxNumberingFormat.Ganada);
  Result.Add(TdxNumberingFormat.Chosung);
  Result.Add(TdxNumberingFormat.None);//*
  Result.Add(TdxNumberingFormat.None);//*
  Result.Add(TdxNumberingFormat.None);//*
  Result.Add(TdxNumberingFormat.None);//*
  Result.Add(TdxNumberingFormat.None);//*
  Result.Add(TdxNumberingFormat.None);//*
  Result.Add(TdxNumberingFormat.None);//*
  Result.Add(TdxNumberingFormat.None);//*
  Result.Add(TdxNumberingFormat.None);//*
  Result.Add(TdxNumberingFormat.None);//*
  Result.Add(TdxNumberingFormat.None);//*
  Result.Add(TdxNumberingFormat.None);//*
  Result.Add(TdxNumberingFormat.KoreanCounting);
  Result.Add(TdxNumberingFormat.KoreanDigital);
  Result.Add(TdxNumberingFormat.KoreanDigital2);
  Result.Add(TdxNumberingFormat.KoreanLegal);
  Result.Add(TdxNumberingFormat.Hebrew1);
  Result.Add(TdxNumberingFormat.ArabicAlpha);
  Result.Add(TdxNumberingFormat.Hebrew2);
  Result.Add(TdxNumberingFormat.ArabicAbjad);
  Result.Add(TdxNumberingFormat.HindiVowels);
  Result.Add(TdxNumberingFormat.HindiConsonants);
  Result.Add(TdxNumberingFormat.HindiNumbers);
  Result.Add(TdxNumberingFormat.HindiDescriptive);
  Result.Add(TdxNumberingFormat.ThaiLetters);
  Result.Add(TdxNumberingFormat.ThaiNumbers);
  Result.Add(TdxNumberingFormat.ThaiDescriptive);
  Result.Add(TdxNumberingFormat.VietnameseDescriptive);
  Result.Add(TdxNumberingFormat.NumberInDash);
  Result.Add(TdxNumberingFormat.RussianLower);
  Result.Add(TdxNumberingFormat.RussianUpper);
  Result.Add(TdxNumberingFormat.None);//*
  Result.Add(TdxNumberingFormat.None);//*
  Result.Add(TdxNumberingFormat.None);//*
  Result.Add(TdxNumberingFormat.None);//*
  Result.Add(TdxNumberingFormat.None);//*
  Result.Add(TdxNumberingFormat.None);//*
  Result.Add(TdxNumberingFormat.None);//*
  Result.Add(TdxNumberingFormat.None);//*
  Result.Add(TdxNumberingFormat.None);//*
end;

procedure TdxListLevelDestination.BeforePopRtfState;
begin
	ApplyListLevelParagraphProperties;
  ApplyListLevelCharacterProperties;
end;

procedure TdxListLevelDestination.NestedGroupFinished(ADestination: TdxRichEditRtfDestinationBase);
begin
  TryToHandleFinishOfListLevelTextDestination(ADestination);
  TryToHandleFinishOfListLevelNumbersDestination(ADestination);
end;

procedure TdxListLevelDestination.SetLevel(const Value: TdxRtfListLevel);
begin
  FLevel.Free;
  FLevel := Value;
end;

function TdxListLevelDestination.GetControlCharHT: TdxControlCharTranslatorTable;
begin
  Result := nil;
end;

procedure TdxListLevelDestination.PopulateKeywordTable(ATable: TdxKeywordTranslatorTable);
begin
  ATable.Add('levelstartat', ListLevelStartAtKeywordHandler);
  ATable.Add('lvltentative', ListLevelTentativeKeywordHandler);
  ATable.Add('levelnfc', ListLevelNumberingFormatKeywordHandler);
  ATable.Add('leveljc', ListLevelAlignmentKeywordHandler);
  ATable.Add('levelnfcn', ListLevelNumberingFormatKeywordHandler);
  ATable.Add('leveljcn', ListLevelAlignmentKeywordHandler);
  ATable.Add('levelold', ListLevelOldKeywordHandler);
  ATable.Add('levelprev', ListLevelPrevKeywordHandler);
  ATable.Add('levelprevspace', ListLevelPrevspaceKeywordHandler);
  ATable.Add('levelindent', ListLevelIndentKeywordHandler);
  ATable.Add('levelspace', ListLevelSpaceKeywordHandler);
  ATable.Add('leveltext', ListLevelTextKeywordHandler);
  ATable.Add('levelnumbers', ListLevelNumbersKeywordHandler);
  ATable.Add('levelfollow', ListLevelFollowKeywordHandler);
  ATable.Add('levellegal', ListLevelLegalKeywordHandler);
  ATable.Add('levelnorestart', ListLevelNoRestartKeywordHandler);
  ATable.Add('levelpicture', ListLevelPictureKeywordHandler);
  ATable.Add('levelpicturenosize', ListLevelPictureNoSizeKeywordHandler);
  ATable.Add('s', ParagraphStyleKeywordHandler);
  TdxDefaultDestination.AddParagraphPropertiesKeywords(ATable);
  TdxDefaultDestination.AddCharacterPropertiesKeywords(ATable);
end;

procedure TdxListLevelDestination.ProcessCharCore(AChar: Char);
begin
//do nothing
end;

procedure TdxListLevelDestination.ProcessControlCharCore(AChar: Char);
begin
//do nothing
end;

function TdxListLevelDestination.ProcessKeywordCore(const AKeyword: string;
  AParameterValue: Integer; AHasParameter: Boolean): Boolean;
var
  ATranslator: TdxTranslateKeywordHandler;
begin
  if KeywordHT.TryGetValue(AKeyword, ATranslator) then
  begin
    ATranslator(Importer, AParameterValue, AHasParameter);
    Result := True;
  end
  else
    Result := False;
end;

procedure TdxListLevelDestination.ApplyListLevelCharacterProperties;
var
  ADestination: TdxListLevelDestination;
  AFormatting: TdxCharacterFormattingBase;
  AParentCharacterProperties: TdxMergedCharacterProperties;
begin
  ADestination := TdxListLevelDestination(Importer.Destination);
  AFormatting := Importer.DocumentModel.DefaultCharacterProperties.Info;
  AParentCharacterProperties := TdxMergedCharacterProperties.Create(AFormatting.Info, AFormatting.Options);
  try
    Importer.ApplyCharacterProperties(ADestination.Level.CharacterProperties, Importer.Position.CharacterFormatting.Info, AParentCharacterProperties);
  finally
    AParentCharacterProperties.Free;
  end;
end;

procedure TdxListLevelDestination.ApplyListLevelParagraphProperties;
var
  ADestination: TdxListLevelDestination;
  AFormatting: TdxParagraphFormattingBase;
  AParentParagraphProperties: TdxMergedParagraphProperties;
begin
  ADestination := TdxListLevelDestination(Importer.Destination);
  AFormatting := Importer.DocumentModel.DefaultParagraphProperties.Info;
  AParentParagraphProperties := TdxMergedParagraphProperties.Create(AFormatting.Info, AFormatting.Options);
  try
    Importer.ApplyParagraphProperties(ADestination.Level.ParagraphProperties, Importer.Position.ParagraphFormattingInfo,
      AParentParagraphProperties);
  finally
    AParentParagraphProperties.Free;
  end;
end;

procedure TdxListLevelDestination.TryToHandleFinishOfListLevelNumbersDestination(ANestedDestination: TdxRichEditRtfDestinationBase);
var
  ACurrentDestination: TdxListLevelDestination;
  ADestination: TdxListLevelNumbersDestination;
begin
  if ANestedDestination is TdxListLevelNumbersDestination then
  begin
    ACurrentDestination := TdxListLevelDestination(Importer.Destination);
    ADestination := TdxListLevelNumbersDestination(ANestedDestination);
    ACurrentDestination.Level.Number := ADestination.Value;
  end;
end;

procedure TdxListLevelDestination.TryToHandleFinishOfListLevelTextDestination(ANestedDestination: TdxRichEditRtfDestinationBase);
var
  ACurrentDestination: TdxListLevelDestination;
  ADestination: TdxListLevelTextDestination;
begin
  if ANestedDestination is TdxListLevelTextDestination then
  begin
    ACurrentDestination := TdxListLevelDestination(Importer.Destination);
    ADestination := TdxListLevelTextDestination(ANestedDestination);
    ACurrentDestination.Level.Text := ADestination.Value;
    ACurrentDestination.Level.ListLevelProperties.TemplateCode := ADestination.LevelTemplateId;
  end;
end;

class procedure TdxListLevelDestination.ListLevelAlignmentKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  if AHasParameter then
    TdxListLevelDestination(AImporter.Destination).Level.ListLevelProperties.Alignment := TdxListNumberAlignment(AParameterValue);
end;

class procedure TdxListLevelDestination.ListLevelFollowKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
var
  ADestination: TdxListLevelDestination;
begin
  if AHasParameter then
  begin
    ADestination := TdxListLevelDestination(AImporter.Destination);
    case AParameterValue of
      0: ADestination.Level.ListLevelProperties.Separator := TdxCharacters.TabMark;
      1: ADestination.Level.ListLevelProperties.Separator := TdxCharacters.Space;
    else
      ADestination.Level.ListLevelProperties.Separator := #0; 
    end;
  end;
end;

class procedure TdxListLevelDestination.ListLevelIndentKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  if AHasParameter then
    TdxListLevelDestination(AImporter.Destination).Level.ListLevelProperties.LegacyIndent :=
      AImporter.UnitConverter.TwipsToModelUnits(AParameterValue);
end;

class procedure TdxListLevelDestination.ListLevelLegalKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  if AHasParameter then
    TdxListLevelDestination(AImporter.Destination).Level.ListLevelProperties.ConvertPreviousLevelNumberingToDecimal := AParameterValue <> 0;
end;

class procedure TdxListLevelDestination.ListLevelNoRestartKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  if AHasParameter then
    TdxListLevelDestination(AImporter.Destination).Level.ListLevelProperties.SuppressRestart := AParameterValue <> 0;
end;

class procedure TdxListLevelDestination.ListLevelNumberingFormatKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
var
  ADestination: TdxListLevelDestination;
begin
  if AHasParameter and (AParameterValue >=0) and (AParameterValue < NumberingFormats.Count) then
  begin
    ADestination := TdxListLevelDestination(AImporter.Destination);
    ADestination.Level.ListLevelProperties.Format := NumberingFormats[AParameterValue];
  end;
end;

class procedure TdxListLevelDestination.ListLevelNumbersKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.Destination := TdxListLevelNumbersDestination.Create(AImporter);
end;

class procedure TdxListLevelDestination.ListLevelOldKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  if not AHasParameter or (AParameterValue <> 0) then
    TdxListLevelDestination(AImporter.Destination).Level.ListLevelProperties.Legacy := True;
end;

class procedure TdxListLevelDestination.ListLevelPictureKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented')
end;

class procedure TdxListLevelDestination.ListLevelPictureNoSizeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented')
end;

class procedure TdxListLevelDestination.ListLevelPrevKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented')
end;

class procedure TdxListLevelDestination.ListLevelPrevspaceKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented')
end;

class procedure TdxListLevelDestination.ListLevelSpaceKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
var
  ADestination: TdxListLevelDestination;
begin
  if AHasParameter then
  begin
    ADestination := TdxListLevelDestination(AImporter.Destination);
    ADestination.Level.ListLevelProperties.LegacySpace := AImporter.UnitConverter.TwipsToModelUnits(AParameterValue);
  end;
end;

class procedure TdxListLevelDestination.ListLevelStartAtKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  if AHasParameter then
    TdxListLevelDestination(AImporter.Destination).Level.ListLevelProperties.Start := AParameterValue;
end;

class procedure TdxListLevelDestination.ListLevelTentativeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
//do nothing
end;

class procedure TdxListLevelDestination.ListLevelTextKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.Destination := TdxListLevelTextDestination.Create(AImporter)
end;

class procedure TdxListLevelDestination.ParagraphStyleKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
var
  ADestination: TdxListLevelDestination;
begin
  ADestination := TdxListLevelDestination(AImporter.Destination);
  if AImporter.ParagraphStyleCollectionIndex.ContainsKey(AParameterValue) then
    ADestination.Level.ParagraphStyleIndex := AImporter.ParagraphStyleCollectionIndex[AParameterValue];
end;

{ TdxListOverrideLevelDestination }

constructor TdxListOverrideLevelDestination.Create(AImporter: TdxRichEditDocumentModelRtfImporter);
begin
  inherited Create(AImporter);
  FOverrideLevel := TdxRtfListOverrideLevel.Create(Importer.DocumentModel);
end;

function TdxListOverrideLevelDestination.GetControlCharHT: TdxControlCharTranslatorTable;
begin
  Result := nil;
end;

procedure TdxListOverrideLevelDestination.InitializeClone(
  AClone: TdxRichEditRtfDestinationBase);
begin
  inherited InitializeClone(AClone);
  TdxListOverrideLevelDestination(AClone).SetOverrideLevel(OverrideLevel);
end;

procedure TdxListOverrideLevelDestination.PopulateKeywordTable(ATable: TdxKeywordTranslatorTable);
begin
  ATable.Add('listoverrideformat', ListOverrideFormatKeywordHandler);
  ATable.Add('listoverridestartat', ListOverrideStartAtKeywordHandler);
  ATable.Add('levelstartat', ListOverrideStartAtValueKeywordHandler);
  ATable.Add('listlevel', ListOverrideListLevelKeywordHandler);
end;

procedure TdxListOverrideLevelDestination.SetOverrideLevel(
  const Value: TdxRtfListOverrideLevel);
begin
  FOverrideLevel.Free;
  FOverrideLevel := Value;
end;

class procedure TdxListOverrideLevelDestination.ListOverrideFormatKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  TdxListOverrideLevelDestination(AImporter.Destination).OverrideLevel.OverrideFormat := True;
end;

class procedure TdxListOverrideLevelDestination.ListOverrideListLevelKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
var
  ADestination: TdxListOverrideLevelDestination;
  ANewDestination: TdxListLevelDestination;
begin
  ADestination := TdxListOverrideLevelDestination(AImporter.Destination);
  ANewDestination := TdxListLevelDestination.Create(AImporter);
  AImporter.Destination := ANewDestination;
  ADestination.OverrideLevel.Level := ANewDestination.Level;
end;

class procedure TdxListOverrideLevelDestination.ListOverrideStartAtKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  TdxListOverrideLevelDestination(AImporter.Destination).OverrideLevel.OverrideStartAt := True;
end;

class procedure TdxListOverrideLevelDestination.ListOverrideStartAtValueKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  if AHasParameter then
    TdxListOverrideLevelDestination(AImporter.Destination).OverrideLevel.StartAt := AParameterValue;
end;

end.
