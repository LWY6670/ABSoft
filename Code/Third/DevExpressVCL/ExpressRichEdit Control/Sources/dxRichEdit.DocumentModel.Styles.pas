{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.DocumentModel.Styles;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  SysUtils, dxCore, dxCoreClasses, cxClasses, dxRichEdit.Utils.BatchUpdateHelper, Generics.Collections, Classes,
  dxRichEdit.DocumentModel.ParagraphFormatting, dxRichEdit.DocumentModel.CharacterFormatting,
  dxRichEdit.DocumentModel.History.IndexChangedHistoryItem, dxRichEdit.Platform.Font, dxRichEdit.DocumentModel.Core,
  Generics.Defaults, dxRichEdit.DocumentModel.TabFormatting, dxRichEdit.DocumentModel.IndexBasedObject;

type
  { TdxStyleBase }

  TdxStyleType = (ParagraphStyle, CharacterStyle, TableStyle, NumberingListStyle, TableCellStyle);

  TdxResetFormattingCacheType = (
    Character = 1,
    Paragraph = 2,
    All
  );

  IdxStyle = interface
  ['{DFA740C2-A0BA-442A-8330-8F345400F073}']
    function GetStyleName: string;
    function GetType: TdxStyleType;
    function GetDeleted: Boolean;
    function GetHidden: Boolean;
    function GetSemihidden: Boolean;

    procedure ResetCachedIndices(AResetFormattingCacheType: TdxResetFormattingCacheType);

    property Deleted: Boolean read GetDeleted;
    property Hidden: Boolean read GetHidden;
    property Semihidden: Boolean read GetSemihidden;
    property StyleName: string read GetStyleName;
    property StyleType: TdxStyleType read GetType;
  end;

  { TdxCustomStyle }

  TdxCustomStyleObject = class(TcxIUnknownObject)
  private
    FDocumentModel: TdxCustomDocumentModel;
    function GetPieceTable: TObject;
  protected
    property PieceTable: TObject read GetPieceTable;
  public
    constructor Create(ADocumentModel: TdxCustomDocumentModel); virtual;

    property DocumentModel: TdxCustomDocumentModel read FDocumentModel;
  end;

  { TdxStyleBase }

  TdxStyleBase = class(TdxCustomStyleObject, IdxStyle, IdxBatchUpdateable, IdxBatchUpdateHandler)
  private
    FDeleted: Boolean;
    FDocumentModel: TObject;
    FHidden: Boolean;
    FLocalizedStyleName: string;
    FBatchUpdateHelper: TdxBatchUpdateHelper;
    FParentStyle: TdxStyleBase;
    FPrimary: Boolean;
    FSemihidden: Boolean;
    FStyleName: string;
    procedure SetParentStyle(const Value: TdxStyleBase);
    procedure SetStyleName(const Value: string);
  protected
    function CalculateLocalizedName(const AStyleName: string): string;
    procedure MergePropertiesWithParent; virtual; abstract;
    procedure NotifyStyleChanged;
    procedure NotifyStyleChangedCore;
    procedure OnLastEndUpdateCore;
    procedure OnParentDeleting;
    procedure SetStyleNameCore(const ANewStyleName: string);
    //IdxStyle
    function GetStyleName: string;
    function GetType: TdxStyleType; virtual; abstract;
    function GetDeleted: Boolean;
    function GetHidden: Boolean;
    function GetSemihidden: Boolean;
    procedure ResetCachedIndices(AResetFormattingCacheType: TdxResetFormattingCacheType); virtual;

    function IsParentValid(AParent: TdxStyleBase): Boolean;

    property DeletedCore: Boolean read FDeleted write FDeleted;
    property SemihiddenCore: Boolean read FSemihidden write FSemihidden;
  public
    constructor Create(ADocumentModel: TdxCustomDocumentModel;
      AParentStyle: TdxStyleBase; const AStyleName: string = ''); reintroduce; virtual;
    destructor Destroy; override;
    procedure SetParentStyleCore(const ANewStyle: TdxStyleBase);

    function Copy(ATargetModel: TdxCustomDocumentModel): Integer; virtual; abstract;
    procedure CopyProperties(ASource: TdxStyleBase); virtual; abstract;

    //IdxBatchUpdateable
    function GetIsUpdateLocked: Boolean;
    function GetBatchUpdateHelper: TdxBatchUpdateHelper;
    procedure BeginUpdate;
    procedure EndUpdate;
    procedure CancelUpdate;

    //IdxBatchUpdateHandler
    procedure OnFirstBeginUpdate;
    procedure OnBeginUpdate;
    procedure OnEndUpdate;
    procedure OnLastEndUpdate;
    procedure OnCancelUpdate;
    procedure OnLastCancelUpdate;

    property IsUpdateLocked: Boolean read GetIsUpdateLocked;
    property BatchUpdateHelper: TdxBatchUpdateHelper read GetBatchUpdateHelper;
    property Parent: TdxStyleBase read FParentStyle write SetParentStyle;
    property Primary: Boolean read FPrimary write FPrimary;
    property StyleName: string read FStyleName write SetStyleName;
    property Deleted: Boolean read FDeleted;
    property Hidden: Boolean read FHidden write FHidden;
    property Semihidden: Boolean read FSemihidden write FSemihidden;
    property StyleType: TdxStyleType read GetType;
  end;

  { TdxStyleCollectionBase }

  TdxStyleCollectionBase = class(TdxCustomStyleObject)
  private
    FItems: TObjectList<TdxStyleBase>;
    FOnCollectionChanged: TNotifyEvent;
    procedure AddDeletedStyle(AItem: TdxStyleBase);
    function GetCount: Integer;
    function GetDefaultItem: TdxStyleBase;
    function GetDefaultItemIndex: Integer;
    function GetItem(Index: Integer): TdxStyleBase;
    function MustAddParent(AItem: TdxStyleBase): Boolean;
  protected
    procedure AddCore(AItem: TdxStyleBase);
    procedure AddDeletedStyleCore(AItem: TdxStyleBase);
    function AddNewStyle(AItem: TdxStyleBase): Integer;
    function CanDeleteStyle(AItem: TdxStyleBase): Boolean; virtual;
    function CreateDefaultItem: TdxStyleBase; virtual; abstract;
    procedure DeleteCore(AItem: TdxStyleBase);
    procedure NotifyChildrenParentDeleting(AItem: TdxStyleBase);
    procedure NotifyDocumentStyleDeleting(AItem: TdxStyleBase); virtual;
    procedure NotifyPieceTableStyleDeleting(APieceTable: TObject; AStyle: TdxStyleBase); virtual; abstract;
    procedure RaiseCollectionChanged;
    procedure RemoveLastStyle;
    procedure ResetItemCachedIndices(AStyle: TdxStyleBase; AResetFromattingCacheType: TdxResetFormattingCacheType);
  public
    constructor Create(ADocumentModel: TdxCustomDocumentModel); override;
    destructor Destroy; override;

    function Add(AItem: TdxStyleBase): Integer;
    procedure Clear;
    function IndexOf(AItem: TdxStyleBase): Integer;
    procedure ResetCachedIndices(AResetFromattingCacheType: TdxResetFormattingCacheType);

    function GetStyleIndexByName(const AStyleName: string): Integer;
    function GetStyleByName(const AStyleName: string): TdxStyleBase;

    property Count: Integer read GetCount;
    property DefaultItemIndex: Integer read GetDefaultItemIndex;
    property DefaultItem: TdxStyleBase read GetDefaultItem;
    property Self[Index: Integer]: TdxStyleBase read GetItem; default;
    property Items: TObjectList<TdxStyleBase> read FItems;
    property OnCollectionChanged: TNotifyEvent read FOnCollectionChanged write FOnCollectionChanged;
  end;

  { IdxTabPropertiesContainer }

  IdxTabPropertiesContainer = interface
  ['{CD8694E9-15B5-4722-BCF3-DB42E93F7772}']
    function GetDocumentModel: TdxCustomDocumentModel;

    function CreateTabPropertiesChangedHistoryItem: TdxIndexChangedHistoryItemCore<TdxDocumentModelChangeActions>;
    procedure OnTabPropertiesChanged;

    property DocumentModel: TdxCustomDocumentModel read GetDocumentModel; 
  end;

  { TdxParagraphPropertiesBasedStyle }

  TdxParagraphPropertiesBasedStyle = class(TdxStyleBase, IdxParagraphPropertiesContainer, IdxTabPropertiesContainer)
  private
    FParagraphProperties: TdxParagraphProperties;
    FTabs: TdxTabProperties;
    function GetPieceTable: TObject;
  protected
    function GetParentStyle: TdxParagraphPropertiesBasedStyle;
    procedure SetParentStyle(const Value: TdxParagraphPropertiesBasedStyle);
    function CreateParagraphPropertiesChangedHistoryItem: TdxIndexChangedHistoryItemCore<TdxDocumentModelChangeActions>;
    procedure OnParagraphPropertiesChanged;
    function GetParagraphProperties: TdxParagraphProperties; overload;
    function GetDocumentModel: TdxCustomDocumentModel;
    function CreateTabPropertiesChangedHistoryItem: TdxIndexChangedHistoryItemCore<TdxDocumentModelChangeActions>;
    procedure OnTabPropertiesChanged;

    procedure MergePropertiesWithParent; override;
  public
    constructor Create(ADocumentModel: TdxCustomDocumentModel; AParent: TdxParagraphPropertiesBasedStyle;
      const AStyleName: string = ''); reintroduce; virtual;
    destructor Destroy; override;

    procedure CopyProperties(ASource: TdxStyleBase); override;
    function GetParagraphProperties(AMask: TdxUsedParagraphFormattingOption): TdxParagraphProperties; overload; virtual;
    function GetMergedParagraphProperties: TdxMergedParagraphProperties; virtual;
    function GetMergedWithDefaultParagraphProperties: TdxMergedParagraphProperties; virtual;
    function GetTabs: TdxTabFormattingInfo;

    property Tabs: TdxTabProperties read FTabs;
    property ParagraphProperties: TdxParagraphProperties read GetParagraphProperties;
    property Parent: TdxParagraphPropertiesBasedStyle read GetParentStyle write SetParentStyle;
    property PieceTable: TObject read GetPieceTable;
  end;

  TdxCharacterStyle = class;

  TdxParagraphStyle = class(TdxParagraphPropertiesBasedStyle, IdxParagraphPropertiesContainer,
    IdxCharacterPropertiesContainer)
  private
    FAutoUpdate: Boolean;
    FCharacterProperties: TdxCharacterProperties;
    FListLevelIndex: Integer;
    FNextParagraphStyle: TdxParagraphStyle;
    FNumberingListIndex: TdxNumberingListIndex;
    FParagraphProperties: TdxParagraphProperties;
    FTabs: TdxTabProperties;
    function GetLinkedStyle: TdxCharacterStyle;
    function GetParent: TdxParagraphStyle;
    procedure SetNextParagraphStyle(Value: TdxParagraphStyle);
    procedure SetParent(const Value: TdxParagraphStyle);
    procedure SetAutoUpdate(const Value: Boolean);
  protected
    //IdxCharacterPropertiesContainer and IdxParagraphPropertiesContainer
    function GetPieceTable: TObject;
    function CreateCharacterPropertiesChangedHistoryItem: TdxIndexChangedHistoryItemCore<TdxDocumentModelChangeActions>;
    function CreateParagraphPropertiesChangedHistoryItem: TdxIndexChangedHistoryItemCore<TdxDocumentModelChangeActions>;
    function GetType: TdxStyleType; override;
    procedure MergePropertiesWithParent; override;
    procedure OnCharacterPropertiesChanged;
    procedure OnParagraphPropertiesChanged;

    function CopyFrom(ATargetModel: TdxCustomDocumentModel): TdxParagraphStyle; virtual;
    procedure CopyTo(AStyle: TdxParagraphStyle);
    procedure SetAutoUpdateCore(AAutoUpdate: Boolean);

    procedure SubscribeCharacterPropertiesEvents; virtual;
    procedure SubscribeParagraphPropertiesEvents; virtual;
    procedure OnObtainAffectedRange(ASender: TObject; E: TdxObtainAffectedRangeEventArgs);
  public
    constructor Create(ADocumentModel: TdxCustomDocumentModel; AParentStyle: TdxParagraphStyle = nil; const AStyleName: string = ''); reintroduce;
    destructor Destroy; override;

    function Copy(ATargetModel: TdxCustomDocumentModel): Integer; override;
    procedure CopyProperties(ASource: TdxStyleBase); override;
    function GetListLevelIndex: Integer;
    function GetMergedCharacterProperties: TdxMergedCharacterProperties;
    function GetMergedWithDefaultCharacterProperties: TdxMergedCharacterProperties; virtual;
    function GetNumberingListIndex: TdxNumberingListIndex; virtual; 
    function GetOwnListLevelIndex: Integer;
    function GetOwnNumberingListIndex: Integer;
    function GetTabs: TdxTabFormattingInfo;
    function HasLinkedStyle: Boolean;
    procedure SetNumberingListIndex(ANumberingListIndex: TdxNumberingListIndex); virtual;
    procedure SetNumberingListLevelIndex(AListLevelIndex: Integer); virtual;

    property AutoUpdate: Boolean read FAutoUpdate write SetAutoUpdate;
    property CharacterProperties: TdxCharacterProperties read FCharacterProperties;
    property LinkedStyle: TdxCharacterStyle read GetLinkedStyle;
    property NextParagraphStyle: TdxParagraphStyle read FNextParagraphStyle write SetNextParagraphStyle;
    property ParagraphProperties: TdxParagraphProperties read FParagraphProperties;
    property Parent: TdxParagraphStyle read GetParent write SetParent;
    property Tabs: TdxTabProperties read FTabs;
  end;

  TdxParagraphStyleCollection = class(TdxStyleCollectionBase)
  const
    DefaultParagraphStyleName = 'Normal';
    EmptyParagraphStyleIndex = 0;
  private
    function GetItem(Index: Integer): TdxParagraphStyle;
  protected
    function CreateDefaultItem: TdxStyleBase; override;
    procedure NotifyDocumentStyleDeleting(AParagraphStyle: TdxStyleBase); override; 
    procedure NotifyPieceTableStyleDeleting(APieceTable: TObject; AParagraphStyle: TdxStyleBase); override; 
  public

    property Items[Index: Integer]: TdxParagraphStyle read GetItem; default;
  end;

  TdxCharacterStyle = class(TdxStyleBase, IdxCharacterPropertiesContainer)
  private
    FCharacterProperties: TdxCharacterProperties;
    procedure OnCharacterPropertiesChangedCore;

    function GetHasLinkedStyle: Boolean;
    function GetLinkedStyle: TdxParagraphStyle;
    function GetParent: TdxCharacterStyle;
    procedure SetParent(const Value: TdxCharacterStyle);
  protected
    //IdxCharacterPropertiesContainer
    function GetPieceTable: TObject;
    function CopyFrom(ATargetModel: TdxCustomDocumentModel): TdxCharacterStyle; virtual;
    procedure CopyTo(AStyle: TdxCharacterStyle);
    function CreateCharacterPropertiesChangedHistoryItem: TdxIndexChangedHistoryItemCore<TdxDocumentModelChangeActions>;
    function GetType: TdxStyleType; override;
    procedure MergePropertiesWithParent; override;
    procedure OnCharacterPropertiesChanged;
  public
    constructor Create(ADocumentModel: TdxCustomDocumentModel; AParentStyle: TdxStyleBase; const AStyleName: string = ''); override;
    destructor Destroy; override;

    function GetMergedWithDefaultCharacterProperties: TdxMergedCharacterProperties; virtual;
    function Copy(ATargetModel: TdxCustomDocumentModel): Integer; override;
    procedure CopyProperties(ASource: TdxStyleBase); override;
    function GetMergedCharacterProperties: TdxMergedCharacterProperties;

    property CharacterProperties: TdxCharacterProperties read FCharacterProperties;
    property HasLinkedStyle: Boolean read GetHasLinkedStyle;
    property LinkedStyle: TdxParagraphStyle read GetLinkedStyle;
    property Parent: TdxCharacterStyle read GetParent write SetParent;
  end;

  { TdxCharacterStyleCollection }

  TdxCharacterStyleCollection = class(TdxStyleCollectionBase)
  const
    EmptyCharacterStyleIndex = 0;
    DefaultCharacterStyleName = 'Default Paragraph Font';
    LineNumberingStyleName = 'Line Number';
    HyperlinkStyleName = 'Hyperlink';
  private
    function GetItem(Index: Integer): TdxCharacterStyle;
  protected
    function CreateDefaultItem: TdxStyleBase; override;
    procedure NotifyPieceTableStyleDeleting(APieceTable: TObject; AStyle: TdxStyleBase); override;
  public
    property Items[Index: Integer]: TdxCharacterStyle read GetItem; default;
  end;

  { TdxStyleLinkManager }

  TdxStyleLinkManager = class
  private
    FDocumentModel: TObject;
    FParagraphStyleToCharacterStyleLinks: TDictionary<TdxParagraphStyle, TdxCharacterStyle>;
    FCharacterStyleToParagraphStyleLinks: TDictionary<TdxCharacterStyle, TdxParagraphStyle>;
  public
    constructor Create(ADocumentModel: TdxCustomDocumentModel);
    destructor Destroy; override;

    procedure CreateLink(AParagraphStyle: TdxParagraphStyle; ACharacterStyle: TdxCharacterStyle);
    procedure CreateLinkCore(AParagraphStyle: TdxParagraphStyle; ACharacterStyle: TdxCharacterStyle);
    procedure DeleteLinkCore(AParagraphStyle: TdxParagraphStyle; ACharacterStyle: TdxCharacterStyle);

    function GetLinkedParagraphStyle(ACharacterStyle: TdxCharacterStyle): TdxParagraphStyle;
    function GetLinkedCharacterStyle(AParagraphStyle: TdxParagraphStyle): TdxCharacterStyle;
    function HasLinkedParagraphStyle(ACharacterStyle: TdxCharacterStyle): Boolean;
    function HasLinkedCharacterStyle(AParagraphStyle: TdxParagraphStyle): Boolean;

    property DocumentModel: TObject read FDocumentModel;
  end;

  { TdxStyleTopologicalComparer }

  TdxStyleTopologicalComparer<T: TdxStyleBase> = class(TInterfacedObject, IComparer<T>)
  public
    //IComparer
    function Compare(const Left, Right: T): Integer;
  end;

implementation

uses
  RTLConsts, 
  dxRichEdit.DocumentModel.PieceTable, dxRichEdit.DocumentModel.History.Style;

type
  TdxStyleBaseHelper = class helper for TdxCustomStyleObject
  private
    function GetDocumentModel: TdxDocumentModel;
    function GetPieceTable: TdxPieceTable;
  public
    property DocumentModel: TdxDocumentModel read GetDocumentModel;
    property PieceTable: TdxPieceTable read GetPieceTable;
  end;

  { TdxStyleHistoryItem }

  TdxStyleHistoryItem = class(TdxRichEditHistoryItem)
  private
    FStyle: TdxStyleBase;
  public
    constructor Create(AStyle: TdxStyleBase); reintroduce; virtual;
    property Style: TdxStyleBase read FStyle;
  end;

  TdxAddStyleHistoryItem = class(TdxStyleHistoryItem)
  private
    FCanRedo: Boolean;
    FOwner: TdxStyleCollectionBase;
    FOldDeleted: Boolean;
  protected
    procedure UndoCore; override;
    procedure RedoCore; override;
  public
    constructor Create(AOwner: TdxStyleCollectionBase; AStyle: TdxStyleBase); reintroduce;
    destructor Destroy; override;

    property Owner: TdxStyleCollectionBase read FOwner;
    property OldDeleted: Boolean read FOldDeleted write FOldDeleted;
  end;

  TdxAddDeletedStyleHistoryItem = class(TdxStyleHistoryItem)
  private
    FOwner: TdxStyleCollectionBase;
  protected
    procedure UndoCore; override;
    procedure RedoCore; override;
  public
    constructor Create(AOwner: TdxStyleCollectionBase; AStyle: TdxStyleBase); reintroduce;
  end;

  TdxChangeStyleNameHistoryItem = class(TdxRichEditHistoryItem)
  private
    FStyle: TdxStyleBase;
    FOldStyleName: string;
    FNewStyleName: string;
  protected
    procedure UndoCore; override;
    procedure RedoCore; override;
  public
    constructor Create(AStyle: TdxStyleBase; const AOldStyleName, ANewStyleName: string); reintroduce;
    property Style: TdxStyleBase read FStyle;
    property OldStyleName: string read FOldStyleName;
    property NewStyleName: string read FNewStyleName;
  end;

{ TdxStyleBaseHelper }

function TdxStyleBaseHelper.GetDocumentModel: TdxDocumentModel;
begin
  Result := TdxDocumentModel(FDocumentModel);
end;

function TdxStyleBaseHelper.GetPieceTable: TdxPieceTable;
begin
  Result := TdxPieceTable(inherited PieceTable);
end;

{ TdxStyleHistoryItem }

constructor TdxStyleHistoryItem.Create(AStyle: TdxStyleBase);
begin
  inherited Create(TdxDocumentModel(AStyle.DocumentModel).MainPieceTable);
  FStyle := AStyle;
end;

{ TdxAddStyleHistoryItem }

constructor TdxAddStyleHistoryItem.Create(AOwner: TdxStyleCollectionBase; AStyle: TdxStyleBase);
begin
  inherited Create(AStyle);
  FOwner := AOwner;
end;

destructor TdxAddStyleHistoryItem.Destroy;
begin
  if FCanRedo then
    FreeAndNil(FStyle);
  inherited Destroy;
end;

procedure TdxAddStyleHistoryItem.UndoCore;
begin
  Assert(Owner.Count > 0);
  Assert(Owner.Items[Owner.Count - 1] = Style);
  FOwner.Items.OwnsObjects := False;
  try
    FOwner.RemoveLastStyle;
  finally
    FOwner.Items.OwnsObjects := True;
  end;
  Style.DeletedCore := OldDeleted;
  FCanRedo := True;
end;

procedure TdxAddStyleHistoryItem.RedoCore;
begin
  OldDeleted := Style.Deleted;
  Owner.AddCore(Style);
  Style.DeletedCore := False;
  FCanRedo := False;
end;

{ TdxAddDeletedStyleHistoryItem }

constructor TdxAddDeletedStyleHistoryItem.Create(AOwner: TdxStyleCollectionBase; AStyle: TdxStyleBase);
begin
  inherited Create(AStyle);
  FOwner := AOwner;
end;

procedure TdxAddDeletedStyleHistoryItem.UndoCore;
begin
  FOwner.DeleteCore(Style);
end;

procedure TdxAddDeletedStyleHistoryItem.RedoCore;
begin
  FOwner.AddDeletedStyleCore(Style);
end;

{ TdxChangeStyleNameHistoryItem }

constructor TdxChangeStyleNameHistoryItem.Create(AStyle: TdxStyleBase; const AOldStyleName, ANewStyleName: string);
begin
  inherited Create(TdxDocumentModel(AStyle.DocumentModel).MainPieceTable);
  FStyle := AStyle;
  FOldStyleName := AOldStyleName;
  FNewStyleName := ANewStyleName;
end;

procedure TdxChangeStyleNameHistoryItem.UndoCore;
begin
  Style.SetStyleNameCore(OldStyleName);
end;

procedure TdxChangeStyleNameHistoryItem.RedoCore;
begin
  Style.SetStyleNameCore(NewStyleName);
end;

{ TdxCustomStyle }

constructor TdxCustomStyleObject.Create(ADocumentModel: TdxCustomDocumentModel);
begin
  inherited Create;
  Assert(ADocumentModel is TdxDocumentModel);
  FDocumentModel := ADocumentModel;
end;

function TdxCustomStyleObject.GetPieceTable: TObject;
begin
  Result := TdxDocumentModel(FDocumentModel).MainPieceTable;
end;

{ TdxStyleBase }

constructor TdxStyleBase.Create(ADocumentModel: TdxCustomDocumentModel;
  AParentStyle: TdxStyleBase;
  const AStyleName: string = '');
begin
  inherited Create(ADocumentModel);
  FBatchUpdateHelper := TdxBatchUpdateHelper.Create(Self);
  FDocumentModel := ADocumentModel;
  FParentStyle := AParentStyle;
  SetStyleNameCore(AStyleName);
end;

destructor TdxStyleBase.Destroy;
begin
  FreeAndNil(FBatchUpdateHelper);
  inherited Destroy;
end;

function TdxStyleBase.GetIsUpdateLocked: Boolean;
begin
  Result := FBatchUpdateHelper.IsUpdateLocked;
end;

function TdxStyleBase.GetBatchUpdateHelper: TdxBatchUpdateHelper;
begin
  Result := FBatchUpdateHelper;
end;

procedure TdxStyleBase.BeginUpdate;
begin
  FBatchUpdateHelper.BeginUpdate;
end;

procedure TdxStyleBase.EndUpdate;
begin
  FBatchUpdateHelper.EndUpdate;
end;

procedure TdxStyleBase.CancelUpdate;
begin
  FBatchUpdateHelper.CancelUpdate;
end;

procedure TdxStyleBase.OnFirstBeginUpdate;
begin
  DocumentModel.BeginUpdate;
end;

procedure TdxStyleBase.OnBeginUpdate;
begin
end;

procedure TdxStyleBase.OnEndUpdate;
begin
end;

procedure TdxStyleBase.OnLastEndUpdate;
begin
  OnLastEndUpdateCore;
  DocumentModel.EndUpdate;
end;

procedure TdxStyleBase.OnCancelUpdate;
begin
end;

procedure TdxStyleBase.OnLastCancelUpdate;
begin
  OnLastEndUpdateCore;
  DocumentModel.EndUpdate;
end;

function TdxStyleBase.CalculateLocalizedName(const AStyleName: string): string;
begin
  Result := AStyleName;
end;

procedure TdxStyleBase.NotifyStyleChanged;
begin
  if not IsUpdateLocked then
    NotifyStyleChangedCore;
end;

procedure TdxStyleBase.NotifyStyleChangedCore;
var
  Actions: TdxDocumentModelChangeActions;
begin
  DocumentModel.ResetDocumentFormattingCaches(TdxResetFormattingCacheType.All);
  Actions := TdxParagraphFormattingChangeActionsCalculator.CalculateChangeActions(TdxParagraphFormattingChangeType.ParagraphStyle);
  DocumentModel.ApplyChangesCore(PieceTable, Actions, 0, PieceTable.Runs.Count - 1);
end;

procedure TdxStyleBase.OnLastEndUpdateCore;
begin
  NotifyStyleChangedCore;
end;

procedure TdxStyleBase.OnParentDeleting;
begin
  Assert(Parent <> nil);
  BeginUpdate;
  try
    MergePropertiesWithParent;
    Parent := Parent.Parent;
  finally
    EndUpdate;
  end;
end;

procedure TdxStyleBase.SetStyleNameCore(const ANewStyleName: string);
begin
  FStyleName := ANewStyleName;
  FLocalizedStyleName := CalculateLocalizedName(ANewStyleName);
end;

function TdxStyleBase.GetStyleName: string;
begin
  Result := FStyleName;
end;

function TdxStyleBase.GetDeleted: Boolean;
begin
  Result := FDeleted;
end;

function TdxStyleBase.GetHidden: Boolean;
begin
  Result := FHidden;
end;

function TdxStyleBase.GetSemihidden: Boolean;
begin
  Result := FSemihidden;
end;

procedure TdxStyleBase.ResetCachedIndices(AResetFormattingCacheType: TdxResetFormattingCacheType);
begin
end;

function TdxStyleBase.IsParentValid(AParent: TdxStyleBase): Boolean;
begin
  Result := (Parent = nil) or Parent.IsParentValid(AParent);
end;

procedure TdxStyleBase.SetParentStyle(const Value: TdxStyleBase);
var
  AItem: TdxChangeParentStyleHistoryItem<TdxStyleBase>;
begin
  if Parent = Value then
    Exit;
  if not IsParentValid(Value) then
  begin
    if (DocumentModel.DeferredChanges = nil) or not DocumentModel.DeferredChanges.IsSetContentMode then
      Assert(False) 
    else
      Exit;
  end;

  AItem := TdxChangeParentStyleHistoryItem<TdxStyleBase>.Create(Self, Parent, Value);
  DocumentModel.History.Add(AItem);
  AItem.Execute;
end;

procedure TdxStyleBase.SetParentStyleCore(const ANewStyle: TdxStyleBase);
begin
  if IsUpdateLocked then
  begin
    FParentStyle := ANewStyle;
    Exit;
  end;

  DocumentModel.BeginUpdate;
  try
    FParentStyle := ANewStyle;
    NotifyStyleChangedCore;
  finally
    DocumentModel.EndUpdate;
  end;
end;

procedure TdxStyleBase.SetStyleName(const Value: string);
var
  AHistoryItem: TdxChangeStyleNameHistoryItem;
begin
  if FStyleName <> Value then
  begin
    AHistoryItem := TdxChangeStyleNameHistoryItem.Create(Self, FStyleName, Value);
    DocumentModel.History.Add(AHistoryItem);
    AHistoryItem.Execute;
  end;
end;

{ TdxStyleCollectionBase }

constructor TdxStyleCollectionBase.Create(ADocumentModel: TdxCustomDocumentModel);
var
  ADefaultItem: TdxStyleBase;
begin
  inherited Create(ADocumentModel);
  FItems := TObjectList<TdxStyleBase>.Create;
  ADefaultItem := CreateDefaultItem;
  if ADefaultItem <> nil then
    FItems.Add(ADefaultItem);
end;

destructor TdxStyleCollectionBase.Destroy;
begin
  FreeAndNil(FItems);
  inherited Destroy;
end;

function TdxStyleCollectionBase.Add(AItem: TdxStyleBase): Integer;
begin
  Assert(AItem.DocumentModel = DocumentModel);
  DocumentModel.History.BeginTransaction;
  try
    if MustAddParent(AItem) then
      Add(AItem.Parent);
    Result := FItems.IndexOf(AItem);
    if Result >= 0 then
    begin
      if AItem.Deleted then
        AddDeletedStyle(AItem);
    end
    else
      Result := AddNewStyle(AItem);
  finally
    DocumentModel.History.EndTransaction;
  end;
end;

procedure TdxStyleCollectionBase.Clear;
begin
  FItems.Clear;
end;

function TdxStyleCollectionBase.GetStyleIndexByName(const AStyleName: string): Integer;
var
  I: Integer;
begin
  Result := -1;
  for I := 0 to Count - 1 do
  begin
    if Items[I].StyleName = AStyleName then
    begin
      Result := I;
      Break;
    end;
  end;
end;

function TdxStyleCollectionBase.GetStyleByName(const AStyleName: string): TdxStyleBase;
var
  AIndex: Integer;
begin
  AIndex := GetStyleIndexByName(AStyleName);
  if AIndex >= 0 then
    Result := Items[AIndex]
  else
    Result := nil;
end;

function TdxStyleCollectionBase.IndexOf(AItem: TdxStyleBase): Integer;
begin
  Result := FItems.IndexOf(AItem);
end;

procedure TdxStyleCollectionBase.ResetCachedIndices(AResetFromattingCacheType: TdxResetFormattingCacheType);
var
  I: Integer;
begin
  for I := 0 to Items.Count - 1 do
    ResetItemCachedIndices(Items[I], AResetFromattingCacheType); 
end;

procedure TdxStyleCollectionBase.AddCore(AItem: TdxStyleBase);
begin
  Assert(not FItems.Contains(AItem));
  FItems.Add(AItem);
  RaiseCollectionChanged;
end;

procedure TdxStyleCollectionBase.AddDeletedStyleCore(AItem: TdxStyleBase);
begin
  AItem.DeletedCore := False;
  RaiseCollectionChanged;
end;

function TdxStyleCollectionBase.AddNewStyle(AItem: TdxStyleBase): Integer;
var
  AHistoryItem: TdxAddStyleHistoryItem;
begin
  AHistoryItem := TdxAddStyleHistoryItem.Create(Self, AItem);
  DocumentModel.History.Add(AHistoryItem);
  AHistoryItem.Execute;
  Result := FItems.Count - 1;
end;

function TdxStyleCollectionBase.CanDeleteStyle(AItem: TdxStyleBase): Boolean;
begin
  Result := AItem <> DefaultItem;
end;

procedure TdxStyleCollectionBase.DeleteCore(AItem: TdxStyleBase);
begin
  Assert(CanDeleteStyle(AItem));
  NotifyChildrenParentDeleting(AItem);
  NotifyDocumentStyleDeleting(AItem);
  AItem.DeletedCore := True;
  RaiseCollectionChanged;
end;

procedure TdxStyleCollectionBase.NotifyChildrenParentDeleting(AItem: TdxStyleBase);
var
  I: Integer;
  AStyle: TdxStyleBase;
begin
  for I := 0 to Count - 1 do
  begin
    AStyle := Items[I];
    if AStyle.Parent = TObject(AItem) then
      AStyle.OnParentDeleting;
  end;
end;

procedure TdxStyleCollectionBase.NotifyDocumentStyleDeleting(AItem: TdxStyleBase);
var
  AList: TList<TdxPieceTable>;
  I: Integer;
begin
  AList := DocumentModel.GetPieceTables(True);
  for I := 0 to AList.Count - 1 do
    NotifyPieceTableStyleDeleting(AList[I], AItem);
end;

procedure TdxStyleCollectionBase.RaiseCollectionChanged;
begin
  dxCallNotify(OnCollectionChanged, Self);
end;

procedure TdxStyleCollectionBase.RemoveLastStyle;
begin
  Assert(FItems.Count > 0);
  FItems.Delete(FItems.Count - 1);
  RaiseCollectionChanged;
end;

procedure TdxStyleCollectionBase.ResetItemCachedIndices(AStyle: TdxStyleBase;
  AResetFromattingCacheType: TdxResetFormattingCacheType);
begin
  AStyle.ResetCachedIndices(AResetFromattingCacheType);
end;

procedure TdxStyleCollectionBase.AddDeletedStyle(AItem: TdxStyleBase);
var
  AHistoryItem: TdxAddDeletedStyleHistoryItem;
begin
  AHistoryItem := TdxAddDeletedStyleHistoryItem.Create(Self, AItem);
  DocumentModel.History.Add(AHistoryItem);
  AHistoryItem.Execute;
end;

function TdxStyleCollectionBase.GetCount: Integer;
begin
  Result := FItems.Count;
end;

function TdxStyleCollectionBase.GetDefaultItem: TdxStyleBase;
begin
  Result := FItems[DefaultItemIndex];
end;

function TdxStyleCollectionBase.GetDefaultItemIndex: Integer;
begin
  Result := 0;
end;

function TdxStyleCollectionBase.GetItem(Index: Integer): TdxStyleBase;
begin
  Result := FItems[Index];
end;

function TdxStyleCollectionBase.MustAddParent(AItem: TdxStyleBase): Boolean;
var
  AParent: TdxStyleBase;
begin
  AParent := AItem.Parent;
  Result := (AParent <> nil) and
    (not FItems.Contains(AParent) or AParent.Deleted);
end;

{ TdxParagraphPropertiesBasedStyle }

constructor TdxParagraphPropertiesBasedStyle.Create(ADocumentModel: TdxCustomDocumentModel;
  AParent: TdxParagraphPropertiesBasedStyle; const AStyleName: string = '');
begin
  inherited Create(ADocumentModel, AParent, AStyleName);
  FParagraphProperties := TdxParagraphProperties.Create(Self);
  FTabs := TdxTabProperties.Create(Self);
end;

destructor TdxParagraphPropertiesBasedStyle.Destroy;
begin
  FTabs.Free;
  FParagraphProperties.Free;
  inherited Destroy;
end;

function TdxParagraphPropertiesBasedStyle.GetDocumentModel: TdxCustomDocumentModel;
begin
  Result := inherited DocumentModel;
end;

function TdxParagraphPropertiesBasedStyle.GetMergedParagraphProperties: TdxMergedParagraphProperties;
var
  AMerger: TdxParagraphPropertiesMerger;
  AMergedParagraphProperties: TdxMergedParagraphProperties;
begin
  Assert(not Deleted);
  AMerger := TdxParagraphPropertiesMerger.Create(ParagraphProperties);
  try
    if Parent <> nil then
    begin
      AMergedParagraphProperties := Parent.GetMergedParagraphProperties;
      try
        AMerger.Merge(AMergedParagraphProperties);
      finally
        AMergedParagraphProperties.Free;
      end;
    end;
    Result := AMerger.MergedProperties;
  finally
    AMerger.Free;
  end;
end;

function TdxParagraphPropertiesBasedStyle.GetMergedWithDefaultParagraphProperties: TdxMergedParagraphProperties;
var
  AMerger: TdxParagraphPropertiesMerger;
begin
  Assert(not Deleted); 
  AMerger := TdxParagraphPropertiesMerger.Create(GetMergedParagraphProperties);
  try
    AMerger.Merge(DocumentModel.DefaultParagraphProperties);
    Result := AMerger.MergedProperties;
  finally
    AMerger.Free;
  end;
end;

function TdxParagraphPropertiesBasedStyle.GetParagraphProperties(AMask: TdxUsedParagraphFormattingOption): TdxParagraphProperties;
begin
  if Deleted then
    raise Exception.Create(''); 
  if ParagraphProperties.UseVal(AMask) then
    Exit(ParagraphProperties);
  if Parent <> nil then
    Result := Parent.GetParagraphProperties(AMask)
  else
    Result := nil;
end;

function TdxParagraphPropertiesBasedStyle.GetParagraphProperties: TdxParagraphProperties;
begin
  Result := FParagraphProperties;
end;

function TdxParagraphPropertiesBasedStyle.GetParentStyle: TdxParagraphPropertiesBasedStyle;
begin
  Result := TdxParagraphPropertiesBasedStyle(inherited Parent);
end;

function TdxParagraphPropertiesBasedStyle.GetPieceTable: TObject;
begin
  Result := inherited PieceTable;
end;

function TdxParagraphPropertiesBasedStyle.GetTabs: TdxTabFormattingInfo;
begin
  if Parent = nil then
    Result := Tabs.GetTabs
  else
    Result := TdxTabFormattingInfo.Merge(Tabs.GetTabs, Parent.GetTabs);
end;

procedure TdxParagraphPropertiesBasedStyle.CopyProperties(ASource: TdxStyleBase);
begin
  Assert(ASource is TdxParagraphPropertiesBasedStyle);
  ParagraphProperties.CopyFrom(TdxParagraphPropertiesBasedStyle(ASource).ParagraphProperties.Info);
end;

function TdxParagraphPropertiesBasedStyle.CreateParagraphPropertiesChangedHistoryItem: TdxIndexChangedHistoryItemCore<TdxDocumentModelChangeActions>;
begin
  Result := TdxIndexChangedHistoryItem<TdxDocumentModelChangeActions>.Create(DocumentModel.MainPieceTable, ParagraphProperties);
end;

function TdxParagraphPropertiesBasedStyle.CreateTabPropertiesChangedHistoryItem: TdxIndexChangedHistoryItemCore<TdxDocumentModelChangeActions>;
begin
  Result := TdxIndexChangedHistoryItem<TdxDocumentModelChangeActions>.Create(DocumentModel.MainPieceTable, Tabs);
end;

procedure TdxParagraphPropertiesBasedStyle.MergePropertiesWithParent;
begin
  ParagraphProperties.Merge(Parent.ParagraphProperties);
end;

procedure TdxParagraphPropertiesBasedStyle.OnParagraphPropertiesChanged;
begin
  BeginUpdate;
  try
    NotifyStyleChanged;
  finally
    EndUpdate;
  end;
end;

procedure TdxParagraphPropertiesBasedStyle.OnTabPropertiesChanged;
begin
end;

procedure TdxParagraphPropertiesBasedStyle.SetParentStyle(const Value: TdxParagraphPropertiesBasedStyle);
begin
  inherited Parent := Value;
end;

{ TdxParagraphStyle }

constructor TdxParagraphStyle.Create(ADocumentModel: TdxCustomDocumentModel; AParentStyle: TdxParagraphStyle = nil;
  const AStyleName: string = '');
begin
  inherited Create(ADocumentModel, AParentStyle, AStyleName);
  FNumberingListIndex := NumberingListIndexListIndexNotSetted;
  FCharacterProperties := TdxCharacterProperties.Create(Self);
  FParagraphProperties := TdxParagraphProperties.Create(Self);
  FTabs := TdxTabProperties.Create(Self);
  SubscribeCharacterPropertiesEvents;
  SubscribeParagraphPropertiesEvents;
end;

destructor TdxParagraphStyle.Destroy;
begin
  FreeAndNil(FTabs);
  FreeAndNil(FParagraphProperties);
  FreeAndNil(FCharacterProperties);
  inherited Destroy;
end;

function TdxParagraphStyle.Copy(ATargetModel: TdxCustomDocumentModel): Integer;
var
  I: Integer;
  ACopy: TdxParagraphStyle;
begin
  for I := 0 to TdxDocumentModel(ATargetModel).ParagraphStyles.Count - 1 do
  begin
    if StyleName = TdxDocumentModel(ATargetModel).ParagraphStyles[I].StyleName then
    begin
      Result := I;
      Exit;
    end;
  end;
  ACopy := CopyFrom(ATargetModel);
  ACopy.CopyProperties(Self);
  Result := TdxDocumentModel(ATargetModel).ParagraphStyles.AddNewStyle(ACopy);
  if NextParagraphStyle <> nil then
    ACopy.NextParagraphStyle := TdxDocumentModel(ATargetModel).ParagraphStyles[NextParagraphStyle.Copy(ATargetModel)];
end;

function TdxParagraphStyle.CopyFrom(ATargetModel: TdxCustomDocumentModel): TdxParagraphStyle;
begin
  Result := TdxParagraphStyle.Create(ATargetModel);
  CopyTo(Result);
end;

procedure TdxParagraphStyle.CopyProperties(ASource: TdxStyleBase);
var
  AParagraphStyle: TdxParagraphStyle absolute ASource;
begin
  Assert(ASource is TdxParagraphStyle);
  CharacterProperties.CopyFrom(AParagraphStyle.CharacterProperties.Info);
  ParagraphProperties.CopyFrom(AParagraphStyle.ParagraphProperties.Info);
end;

procedure TdxParagraphStyle.CopyTo(AStyle: TdxParagraphStyle);
var
  AMergedWithDefaultParagraphProperties, AStyleMergedWithDefaultParagraphProperties: TdxMergedParagraphProperties;
  AStyleMergedWithDefaultCharacterProperties, AMergedWithDefaultCharacterProperties: TdxMergedCharacterProperties;
begin
  AStyle.ParagraphProperties.CopyFrom(ParagraphProperties.Info);
  AStyle.CharacterProperties.CopyFrom(FCharacterProperties.Info);
  AStyle.AutoUpdate := AutoUpdate;
  AStyle.Tabs.CopyFrom(Tabs.Info);
  AStyle.StyleName := StyleName;
  if GetOwnNumberingListIndex >= 0 then
    AStyle.SetNumberingListIndex(DocumentModel.GetNumberingListIndex(AStyle.DocumentModel, GetOwnNumberingListIndex));

  if GetOwnListLevelIndex >= 0 then
    AStyle.SetNumberingListLevelIndex(GetOwnListLevelIndex);

  if Parent <> nil then
    AStyle.Parent := AStyle.DocumentModel.ParagraphStyles[Parent.Copy(AStyle.DocumentModel)];

  AStyleMergedWithDefaultParagraphProperties := AStyle.GetMergedWithDefaultParagraphProperties;
  try
    AMergedWithDefaultParagraphProperties := GetMergedWithDefaultParagraphProperties;
    try
      ParagraphProperties.ApplyPropertiesDiff(AStyle.ParagraphProperties,
        AStyleMergedWithDefaultParagraphProperties.Info,
        AMergedWithDefaultParagraphProperties.Info);
    finally
      AMergedWithDefaultParagraphProperties.Free;
    end;
  finally
    AStyleMergedWithDefaultParagraphProperties.Free;
  end;
  AStyleMergedWithDefaultCharacterProperties := AStyle.GetMergedWithDefaultCharacterProperties;
  try
    AMergedWithDefaultCharacterProperties := GetMergedWithDefaultCharacterProperties;
    try
      CharacterProperties.ApplyPropertiesDiff(AStyle.CharacterProperties,
        AStyleMergedWithDefaultCharacterProperties.Info,
        AMergedWithDefaultCharacterProperties.Info);
    finally
      AMergedWithDefaultCharacterProperties.Free;
    end;
  finally
    AStyleMergedWithDefaultCharacterProperties.Free;
  end;
end;

function TdxParagraphStyle.GetListLevelIndex: Integer;
begin
  if FNumberingListIndex >= 0 then
    Result := GetOwnListLevelIndex
  else
    if (FNumberingListIndex = NumberingListIndexNoNumberingList) or (Parent = nil) then
      Result := 0
    else
      Result := Parent.GetListLevelIndex;
end;

function TdxParagraphStyle.GetMergedCharacterProperties: TdxMergedCharacterProperties;
var
  AMerger: TdxCharacterPropertiesMerger;
  AParentProperties: TdxMergedCharacterProperties;
begin
  Assert(not Deleted);
  AMerger := TdxCharacterPropertiesMerger.Create(CharacterProperties);
  try
    if Parent <> nil then
    begin
      AParentProperties := Parent.GetMergedCharacterProperties;
      try
        AMerger.Merge(AParentProperties);
      finally
        AParentProperties.Free;
      end;
    end;
    Result := AMerger.MergedProperties;
  finally
    AMerger.Free;
  end;
end;

function TdxParagraphStyle.GetMergedWithDefaultCharacterProperties: TdxMergedCharacterProperties;
var
  AMerger: TdxCharacterPropertiesMerger;
begin
  Assert(not Deleted); 
  AMerger := TdxCharacterPropertiesMerger.Create(GetMergedCharacterProperties);
  try
    AMerger.Merge(DocumentModel.DefaultCharacterProperties);
    Result := AMerger.MergedProperties;
  finally
    AMerger.Free;
  end;
end;

function TdxParagraphStyle.GetNumberingListIndex: TdxNumberingListIndex;
begin
  if (FNumberingListIndex >= NumberingListIndexMinValue) or
    (FNumberingListIndex = NumberingListIndexNoNumberingList) or (Parent = nil) then
    Result := FNumberingListIndex
  else
    Result := Parent.GetNumberingListIndex;
end;

function TdxParagraphStyle.GetOwnListLevelIndex: Integer;
begin
  Result := FListLevelIndex;
end;

function TdxParagraphStyle.GetOwnNumberingListIndex: Integer;
begin
  Result := FNumberingListIndex;
end;

function TdxParagraphStyle.HasLinkedStyle: Boolean;
begin
  Result := DocumentModel.StyleLinkManager.HasLinkedCharacterStyle(Self);
end;

procedure TdxParagraphStyle.SetNumberingListIndex(ANumberingListIndex: TdxNumberingListIndex);
begin
  FNumberingListIndex := ANumberingListIndex;
end;

procedure TdxParagraphStyle.SetNumberingListLevelIndex(AListLevelIndex: Integer);
begin
  FListLevelIndex := AListLevelIndex;
end;

function TdxParagraphStyle.GetPieceTable: TObject;
begin
  Result := PieceTable;
end;

function TdxParagraphStyle.CreateCharacterPropertiesChangedHistoryItem: TdxIndexChangedHistoryItemCore<TdxDocumentModelChangeActions>;
begin
  Result := TdxIndexChangedHistoryItem<TdxDocumentModelChangeActions>.Create(TdxPieceTable(PieceTable), CharacterProperties);
end;

function TdxParagraphStyle.CreateParagraphPropertiesChangedHistoryItem: TdxIndexChangedHistoryItemCore<TdxDocumentModelChangeActions>;
begin
  Result := TdxIndexChangedHistoryItem<TdxDocumentModelChangeActions>.Create(TdxPieceTable(PieceTable), ParagraphProperties);
end;

function TdxParagraphStyle.GetTabs: TdxTabFormattingInfo;
var
  ATabs, AParentTabs: TdxTabFormattingInfo;
begin
  if Parent = nil then
    Result := Tabs.GetTabs
  else
  begin
    ATabs := Tabs.GetTabs;
    try
      AParentTabs := Parent.GetTabs;
      try
        Result := TdxTabFormattingInfo.Merge(ATabs, AParentTabs);
      finally
        AParentTabs.Free;
      end;
    finally
      ATabs.Free;
    end;
  end;
end;

function TdxParagraphStyle.GetType: TdxStyleType;
begin
  Result := TdxStyleType.ParagraphStyle;
end;

procedure TdxParagraphStyle.MergePropertiesWithParent;
begin
  ParagraphProperties.Merge(Parent.ParagraphProperties);
end;

procedure TdxParagraphStyle.OnCharacterPropertiesChanged;
begin
  BeginUpdate;
  try
    NotifyStyleChanged;
    if HasLinkedStyle then
      LinkedStyle.CharacterProperties.CopyFrom(CharacterProperties);
  finally
    EndUpdate;
  end;
end;

procedure TdxParagraphStyle.OnObtainAffectedRange(ASender: TObject; E: TdxObtainAffectedRangeEventArgs);
begin
  E.Start := 0;
  E.&End := MaxInt;
end;

procedure TdxParagraphStyle.OnParagraphPropertiesChanged;
begin
  BeginUpdate;
  try
    NotifyStyleChanged;
  finally
    EndUpdate;
  end;
end;

function TdxParagraphStyle.GetLinkedStyle: TdxCharacterStyle;
begin
  Result := DocumentModel.StyleLinkManager.GetLinkedCharacterStyle(Self);
end;

function TdxParagraphStyle.GetParent: TdxParagraphStyle;
begin
  Result := TdxParagraphStyle(inherited Parent);
end;

procedure TdxParagraphStyle.SetAutoUpdate(const Value: Boolean);
var
  AItem: TdxParagraphStyleChangeAutoUpdatePropertyHistoryItem;
begin
  if AutoUpdate = Value then
    Exit;
  AItem := TdxParagraphStyleChangeAutoUpdatePropertyHistoryItem.Create(Self);
  DocumentModel.History.Add(AItem);
  AItem.Execute;
end;

procedure TdxParagraphStyle.SetAutoUpdateCore(AAutoUpdate: Boolean);
begin
  FAutoUpdate := AAutoUpdate;
end;

procedure TdxParagraphStyle.SetNextParagraphStyle(Value: TdxParagraphStyle);
begin
  FNextParagraphStyle := Value;
end;

procedure TdxParagraphStyle.SetParent(const Value: TdxParagraphStyle);
begin
  inherited Parent := Value;
end;

procedure TdxParagraphStyle.SubscribeCharacterPropertiesEvents;
begin
  CharacterProperties.OnObtainAffectedRange.Add(OnObtainAffectedRange);
end;

procedure TdxParagraphStyle.SubscribeParagraphPropertiesEvents;
begin
  ParagraphProperties.OnObtainAffectedRange.Add(OnObtainAffectedRange);
end;

{ TdxParagraphStyleCollection }

function TdxParagraphStyleCollection.CreateDefaultItem: TdxStyleBase;
begin
  Result := TdxParagraphStyle.Create(FDocumentModel, nil, DefaultParagraphStyleName);
end;

function TdxParagraphStyleCollection.GetItem(Index: Integer): TdxParagraphStyle;
begin
  Result := TdxParagraphStyle(inherited Items[Index]);
end;

procedure TdxParagraphStyleCollection.NotifyDocumentStyleDeleting(AParagraphStyle: TdxStyleBase);
var
  I: Integer;
  AStyle: TdxParagraphStyle absolute AParagraphStyle;
begin
  inherited NotifyDocumentStyleDeleting(AStyle);
  for I := 0 to Count - 1 do
    if Self[I].NextParagraphStyle = AStyle then
      Self[i].NextParagraphStyle := nil;
end;

procedure TdxParagraphStyleCollection.NotifyPieceTableStyleDeleting(APieceTable: TObject; AParagraphStyle: TdxStyleBase);
var
  I: Integer;
  AParagraphs: TdxParagraphCollection;
  AStyle: TdxParagraphStyle absolute AParagraphStyle;
begin
  Assert(APieceTable is TdxPieceTable);
  AParagraphs := TdxPieceTable(APieceTable).Paragraphs;
  for I := 0 to AParagraphs.Count - 1 do
    if AParagraphs[I].ParagraphStyle = AStyle then
      AParagraphs[I].ParagraphStyleIndex := DefaultItemIndex;
end;

{ TdxCharacterStyle }

constructor TdxCharacterStyle.Create(ADocumentModel: TdxCustomDocumentModel;
  AParentStyle: TdxStyleBase; const AStyleName: string = '');
begin
  inherited Create(ADocumentModel, AParentStyle, AStyleName);
  FCharacterProperties := TdxCharacterProperties.Create(Self);
end;

destructor TdxCharacterStyle.Destroy;
begin
  FreeAndNil(FCharacterProperties);
  inherited Destroy;
end;

function TdxCharacterStyle.Copy(ATargetModel: TdxCustomDocumentModel): Integer;
var
  ATargetStyles: TdxCharacterStyleCollection;
  I: Integer;
  AStyle: TdxCharacterStyle;
begin
  ATargetStyles := TdxDocumentModel(ATargetModel).CharacterStyles;
  for I := 0 to ATargetStyles.Count - 1 do
    if StyleName = ATargetStyles[I].StyleName then
      Exit(I);

  AStyle := TdxCharacterStyle.Create(ATargetModel, nil);
  AStyle.CopyProperties(Self);
  Result := ATargetStyles.AddNewStyle(AStyle);

end;

procedure TdxCharacterStyle.CopyProperties(ASource: TdxStyleBase);
begin
  Assert(ASource is TdxCharacterStyle);
  CharacterProperties.CopyFrom(TdxCharacterStyle(ASource).CharacterProperties.Info);
end;

function TdxCharacterStyle.GetMergedCharacterProperties: TdxMergedCharacterProperties;
var
  AMerger: TdxCharacterPropertiesMerger;
  AParentProperties: TdxMergedCharacterProperties;
begin
  Assert(not Deleted);
  AMerger := TdxCharacterPropertiesMerger.Create(CharacterProperties);
  try
    if Parent <> nil then
    begin
      AParentProperties := Parent.GetMergedCharacterProperties;
      try
        AMerger.Merge(AParentProperties);
      finally
        AParentProperties.Free;
      end;
    end;
    Result := AMerger.MergedProperties;
  finally
    AMerger.Free;
  end;
end;

function TdxCharacterStyle.GetMergedWithDefaultCharacterProperties: TdxMergedCharacterProperties;
var
  AMerger: TdxCharacterPropertiesMerger;
begin
  Assert(not Deleted); 
  AMerger := TdxCharacterPropertiesMerger.Create(GetMergedCharacterProperties);
  try
    AMerger.Merge(DocumentModel.DefaultCharacterProperties);
    Result := AMerger.MergedProperties;
  finally
    AMerger.Free;
  end;
end;

function TdxCharacterStyle.GetParent: TdxCharacterStyle;
begin
  Result := TdxCharacterStyle(inherited Parent);
end;

function TdxCharacterStyle.GetPieceTable: TObject;
begin
  Result := PieceTable;
end;

function TdxCharacterStyle.CopyFrom(ATargetModel: TdxCustomDocumentModel): TdxCharacterStyle;
begin
  Result := TdxCharacterStyle.Create(ATargetModel, nil);
  CopyTo(Result);
end;

procedure TdxCharacterStyle.CopyTo(AStyle: TdxCharacterStyle);
begin
  AStyle.CharacterProperties.CopyFrom(CharacterProperties.Info);
  AStyle.StyleName := StyleName;
  if Parent <> nil then
    AStyle.Parent := AStyle.DocumentModel.CharacterStyles[Parent.Copy(AStyle.DocumentModel)];
  CharacterProperties.ApplyPropertiesDiff(AStyle.CharacterProperties, AStyle.GetMergedWithDefaultCharacterProperties.Info, GetMergedWithDefaultCharacterProperties.Info);
end;

function TdxCharacterStyle.CreateCharacterPropertiesChangedHistoryItem: TdxIndexChangedHistoryItemCore<TdxDocumentModelChangeActions>;
begin
  Result := TdxIndexChangedHistoryItem<TdxDocumentModelChangeActions>.Create(TdxPieceTable(PieceTable), CharacterProperties);
end;
function TdxCharacterStyle.GetType: TdxStyleType;
begin
  Result := TdxStyleType.CharacterStyle;
end;

procedure TdxCharacterStyle.MergePropertiesWithParent;
begin
  Assert(Parent <> nil);
  CharacterProperties.Merge(Parent.CharacterProperties);
end;

procedure TdxCharacterStyle.OnCharacterPropertiesChanged;
begin
  OnCharacterPropertiesChangedCore;
end;

procedure TdxCharacterStyle.OnCharacterPropertiesChangedCore;
begin
  BeginUpdate;
  try
    NotifyStyleChanged;
    if HasLinkedStyle then
      LinkedStyle.CharacterProperties.CopyFrom(CharacterProperties);
  finally
    EndUpdate;
  end;
end;

procedure TdxCharacterStyle.SetParent(const Value: TdxCharacterStyle);
begin
  inherited Parent := Value;
end;

function TdxCharacterStyle.GetHasLinkedStyle: Boolean;
begin
  Result := DocumentModel.StyleLinkManager.HasLinkedParagraphStyle(Self);
end;

function TdxCharacterStyle.GetLinkedStyle: TdxParagraphStyle;
begin
  Result := DocumentModel.StyleLinkManager.GetLinkedParagraphStyle(Self);
end;

{ TdxCharacterStyleCollection }

function TdxCharacterStyleCollection.CreateDefaultItem: TdxStyleBase;
begin
  Result := TdxCharacterStyle.Create(DocumentModel, nil, DefaultCharacterStyleName);
  Result.SemihiddenCore := True;
end;

function TdxCharacterStyleCollection.GetItem(Index: Integer): TdxCharacterStyle;
begin
  Result := TdxCharacterStyle(inherited Items[Index]);
end;

procedure TdxCharacterStyleCollection.NotifyPieceTableStyleDeleting(
  APieceTable: TObject; AStyle: TdxStyleBase);
var
  ARuns: TdxTextRunCollection;
  I, ACount: TdxRunIndex;
begin
  Assert(APieceTable is TdxPieceTable);
  ARuns := TdxPieceTable(APieceTable).Runs;
  ACount := ARuns.Count;
  for I := 0 to ACount - 1 do 
  begin
    if ARuns[I].CharacterStyle = AStyle then
      ARuns[i].CharacterStyleIndex := DefaultItemIndex;
  end;
end;

{ TdxStyleLinkManager }

constructor TdxStyleLinkManager.Create(ADocumentModel: TdxCustomDocumentModel);
begin
  inherited Create;
  FDocumentModel := ADocumentModel;
  FParagraphStyleToCharacterStyleLinks := TDictionary<TdxParagraphStyle, TdxCharacterStyle>.Create;
  FCharacterStyleToParagraphStyleLinks := TDictionary<TdxCharacterStyle, TdxParagraphStyle>.Create;
end;

destructor TdxStyleLinkManager.Destroy;
begin
  FreeAndNil(FParagraphStyleToCharacterStyleLinks);
  FreeAndNil(FCharacterStyleToParagraphStyleLinks);
  inherited Destroy;
end;

procedure TdxStyleLinkManager.CreateLink(AParagraphStyle: TdxParagraphStyle; ACharacterStyle: TdxCharacterStyle);
begin
  if AParagraphStyle.HasLinkedStyle or ACharacterStyle.HasLinkedStyle then
    Exit;

  if AParagraphStyle.Deleted or ACharacterStyle.Deleted then
  begin
    NotImplemented;
  end;

  TdxDocumentModel(DocumentModel).BeginUpdate;
  try
  finally
    TdxDocumentModel(DocumentModel).EndUpdate;
  end;
  NotImplemented;
end;

procedure TdxStyleLinkManager.CreateLinkCore(AParagraphStyle: TdxParagraphStyle; ACharacterStyle: TdxCharacterStyle);
begin
  if not FParagraphStyleToCharacterStyleLinks.ContainsKey(AParagraphStyle) then
    FParagraphStyleToCharacterStyleLinks.Add(AParagraphStyle, ACharacterStyle)
  else
    FParagraphStyleToCharacterStyleLinks[AParagraphStyle] := ACharacterStyle;

  if not FCharacterStyleToParagraphStyleLinks.ContainsKey(ACharacterStyle) then
    FCharacterStyleToParagraphStyleLinks.Add(ACharacterStyle, AParagraphStyle)
  else
    FCharacterStyleToParagraphStyleLinks[ACharacterStyle] := AParagraphStyle;
end;

procedure TdxStyleLinkManager.DeleteLinkCore(AParagraphStyle: TdxParagraphStyle; ACharacterStyle: TdxCharacterStyle);
begin
  FParagraphStyleToCharacterStyleLinks.Remove(AParagraphStyle);
  FCharacterStyleToParagraphStyleLinks.Remove(ACharacterStyle);
end;

function TdxStyleLinkManager.GetLinkedParagraphStyle(ACharacterStyle: TdxCharacterStyle): TdxParagraphStyle;
begin
  if not FCharacterStyleToParagraphStyleLinks.TryGetValue(ACharacterStyle, Result) then
    Result := nil;
end;

function TdxStyleLinkManager.GetLinkedCharacterStyle(AParagraphStyle: TdxParagraphStyle): TdxCharacterStyle;
begin
  if not FParagraphStyleToCharacterStyleLinks.TryGetValue(AParagraphStyle, Result) then
    Result := nil;
end;

function TdxStyleLinkManager.HasLinkedParagraphStyle(ACharacterStyle: TdxCharacterStyle): Boolean;
begin
  Result := FCharacterStyleToParagraphStyleLinks.ContainsKey(ACharacterStyle);
end;

function TdxStyleLinkManager.HasLinkedCharacterStyle(AParagraphStyle: TdxParagraphStyle): Boolean;
begin
  Result := FParagraphStyleToCharacterStyleLinks.ContainsKey(AParagraphStyle);
end;

{ TdxStyleTopologicalComparer<T> }

function TdxStyleTopologicalComparer<T>.Compare(const Left, Right: T): Integer;
begin
  if Left.Parent = TdxStyleBase(Right) then
    Result := 1
  else
    if Right.Parent = TdxStyleBase(Left) then
      Result := -1
    else
      Result := 0;
end;

end.
