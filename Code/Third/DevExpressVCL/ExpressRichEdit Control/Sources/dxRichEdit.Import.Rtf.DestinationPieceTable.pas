{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Import.Rtf.DestinationPieceTable;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Types, Classes, SysUtils, dxCoreClasses, dxRichEdit.Import.Rtf, dxRichEdit.Import, dxRichEdit.Options,
  dxRichEdit.DocumentModel.ParagraphFormatting, dxRichEdit.DocumentModel.TabFormatting,
  dxRichEdit.DocumentModel.CharacterFormatting, dxRichEdit.DocumentModel.PieceTable,
  dxRichEdit.DocumentModel.Borders;

type

  { TdxCustomMergedParagraphPropertiesCachedResult }

  TdxCustomMergedParagraphPropertiesCachedResult = class(TdxParagraphMergedParagraphPropertiesCachedResult) 
  private
    FInitialParagraphPropertiesIndex: Integer; 
    FFinalParagraphPropertiesIndex: Integer; 
  public
    constructor Create;

    property InitialParagraphPropertiesIndex: Integer read FInitialParagraphPropertiesIndex write FInitialParagraphPropertiesIndex; 
    property FinalParagraphPropertiesIndex: Integer read FFinalParagraphPropertiesIndex write FFinalParagraphPropertiesIndex; 
  end;

  { TdxCustomMergedCharacterPropertiesCachedResult }

  TdxCustomMergedCharacterPropertiesCachedResult = class(TdxRunMergedCharacterPropertiesCachedResult)
  strict private
    FInitialRunCharacterPropertiesIndex: Integer;
    FFinalRunCharacterPropertiesIndex: Integer;
  public
    constructor Create; override;
    property InitialRunCharacterPropertiesIndex: Integer read FInitialRunCharacterPropertiesIndex
      write FInitialRunCharacterPropertiesIndex;
    property FinalRunCharacterPropertiesIndex: Integer read FFinalRunCharacterPropertiesIndex
      write FFinalRunCharacterPropertiesIndex;
  end;

  { TdxDestinationPieceTable }

  TdxDestinationPieceTable = class(TdxRichEditRtfDestinationBase)
  const
    HighestCyrillic = 1279;
    LowLatinExtendedAdditional = 7680;
    HighLatinExtendedAdditional = 7929;
    LowGeneralPunctuation = 8192;
    HighGeneralPunctuation = 8303;
    LowCurrencySymbols = 8352;
    HighCurrencySymbols = 8367;
    LowLetterlikeSymbols = 8448;
    HighLetterlikeSymbols = 8506;
    LowNumberForms = 8531;
    HighNumberForms = 8579;
    LowMathematicalOperations = 8704;
    HighMathematicalOperations = 8945;
  private
    procedure AppendDoubleByteChar(AChar: Char);
    function GetDocumentModel: TdxDocumentModel;
    function GetNormalizedNumberingListIndex(AOwnNumberingListIndex, AParentNumberingListIndex: TdxNumberingListIndex): TdxNumberingListIndex; 
    class procedure InsertSpace(AImporter: TdxRichEditDocumentModelRtfImporter); static;
    function IsDoubleByteChar(AChar: Char): Boolean;
    procedure NormalizeProperties;
    procedure NormalizeParagraphsProperties; 
    procedure NormalizeParagraphProperties(AParagraph: TdxParagraph; ACachedResult: TdxCustomMergedParagraphPropertiesCachedResult); 
    procedure NormalizeRunProperties(ARun: TdxTextRunBase; ACachedResult: TdxCustomMergedCharacterPropertiesCachedResult);
    procedure NormalizeRunsProperties;

    class procedure AssignWidthUnitInfo(AUnitInfo: TdxWidthUnit; Value: Integer); static;
    class function ShouldApplyParagraphStyle(AImporter: TdxRichEditDocumentModelRtfImporter): Boolean; static;
    class function GetWidthUnitType(AParameterValue: Integer): TdxWidthUnitType; static;

    class procedure AlignLeftKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure AlignCenterKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure AlignRightKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure AlignJustifyKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure AssociatedFontNameKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure BackColorKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure BarTabKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure BoldKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure BookmarkEndKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure BookmarkStartKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure BulletKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure CapsKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure CharacterStyleIndexHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure DocumentVariableKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure DoubleByteCharactersKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure DoubleStrikeoutKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure DxCustomRunDataKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure EmDashKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure EmSpaceKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure EnDashKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure EnSpaceKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure FieldStartKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure FirstLineIndentKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure FontNameKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure FontSizeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure ForeColorKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure HiddenTextKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure ItalicKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure LeftDoubleQuoteKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure LeftIndentKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure LeftSingleQuoteKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure LineBreakKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure ListLevelHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure ListOverrideHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure LineSpacingTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure LineSpacingMultiplierKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure ListTextHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure MailMergeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure NonShapePictureKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure NoSuperAndSubScriptKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure OldParagraphNumberingKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure OldParagraphNumberingTextAfterKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure OldParagraphNumberingTextBeforeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure ParagraphStyleIndexHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure ParKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure PictureKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure PlainKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure QmSpaceKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure RangePermissionEndKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure RangePermissionStartKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure ResetParagraphPropertiesKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure RightDoubleQuoteKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure ParCharHandler(AImporter: TdxRichEditDocumentModelRtfImporter; var AChar: Char); static;
    class procedure RightIndentKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure RightSingleQuoteKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure SectionLevelNumberingKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure ShapeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure ShapePictureKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure StrikeoutKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure SpacingAfterKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure SpacingBeforeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure SubscriptKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure SuperscriptKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure TabCenterKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure TabDecimalKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure TabKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure TabLeaderDotsKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure TabLeaderEqualSignKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure TabLeaderHyphensKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure TabLeaderMiddleDotsKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure TabLeaderThickLineKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure TabLeaderUnderlineKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure TabPositionKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure TabRightKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure UnderlineColorKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure UnderlineDashDotDottedKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure UnderlineDashDottedKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure UnderlineDashedKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure UnderlineDottedKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure UnderlineDoubleKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure UnderlineDoubleWaveKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure UnderlineHeavyWaveKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure UnderlineKeywordHandleCore(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean; AType: TdxUnderlineType); static;
    class procedure UnderlineLongDashedKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure UnderlineNoneKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure UnderlineSingleKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure UnderlineThickDashDotDottedKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure UnderlineThickDashDottedKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure UnderlineThickDashedKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure UnderlineThickDottedKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure UnderlineThickLongDashedKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure UnderlineThickSingleKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure UnderlineWaveKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure UnderlineWordsOnlyKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure UnicodeCountKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure WidowOrphanControlOffKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure WidowOrphanControlOnKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;

    class procedure InTableParagraphKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure RowKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure CellKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure NestedCellKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure NestedRowKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure NestedTablePropertiesKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure NoNestedTablesKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure ItapKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;

    class procedure TableRowDefaultsKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure TableStyleKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure CellxKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure CellPreferredWidthKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure WidthUnitTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure FirstHorizontalMergedCellKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure NextHorizontalMergedCellKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure FirstVerticalMergedCellKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure NextVerticalMergedCellKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure CellFitTextKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure CellNoWrapKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure CellVerticalAlignmentTopKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure CellVerticalAlignmentCenterKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure CellVerticalAlignmentBottomKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure CellHideMarkKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure CellBottomCellMarginKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure CellLeftCellMarginKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure CellRightCellMarginKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure CellTopCellMarginKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure CellBottomCellMarginUnitTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure CellLeftCellMarginUnitTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure CellRightCellMarginUnitTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure CellTopCellMarginUnitTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure CellTextTopAlignmentKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure CellTextCenterVerticalAlignmentKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure CellTextBottomAlignmentKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure CellLeftToRightTopToBottomTextDirectionKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure CellTopToBottomRightToLeftTextDirectionKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure CellBottomToTopLeftToRightTextDirectionKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure CellLeftToRightTopToBottomVerticalTextDirectionKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure CellTopToBottomRightToLeftVerticalTextDirectionKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure CellBackgroundColorHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;

    class procedure NoTableBorderKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure BottomCellBorderKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure TopCellBorderKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure LeftCellBorderKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure RightCellBorderKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure UpperLeftToLowerRightBorderKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure UpperRightToLowerLeftBorderKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;

    class procedure RowLeftKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure RowHeaderKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure RowHeightKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure RowKeepKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure TableRightAlignmentKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure TableLeftAlignmentKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure TableCenterAlignmentKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure WidthBeforeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure WidthBeforeUnitTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure WidthAfterKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure WidthAfterUnitTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;

    class procedure SpaceBetweenCellsKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure TableBottomCellMarginKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure TableLeftCellMarginKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure TableRightCellMarginKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure TableTopCellMarginKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure TableBottomCellMarginUnitTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure TableLeftCellMarginUnitTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure TableRightCellMarginUnitTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure TableTopCellMarginUnitTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;

    class procedure TableBottomCellSpacingKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure TableLeftCellSpacingKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure TableRightCellSpacingKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure TableTopCellSpacingKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure TableBottomCellSpacingUnitTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure TableLeftCellSpacingUnitTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure TableRightCellSpacingUnitTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure TableTopCellSpacingUnitTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;

    class procedure TablePreferredWidthKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure TablePreferredWidthUnitTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure TableIndentKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure TableIndentUnitTypeHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure TableOverlapKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;

    class procedure TableLeftFromTextKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure TableRightFromTextKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure TableTopFromTextKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure TableBottomFromTextKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure ColHorizontalAnchorKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure MarginHorizontalAnchorKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure PageHorizontalAnchorKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure MarginVerticalAnchorKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure ParagraphVerticalAnchorKewordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure PageVerticalAnchorKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure TableHorizontalPositionKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure TableVerticalPositionKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure CenterTableHorizontalAlignKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure InsideTableHorizontalAlignKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure LeftTableHorizontalAlignKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure OutsideTableHorizontalAlignKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure RightTableHorizontalAlignKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure BottomTableVerticalAlignKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure CenterTableVerticalAlignKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure InlineTableVerticalAlignKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure InsideTableVerticalAlignKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure OutsideTableVerticalAlignKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure TopTableVerticalAlignKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure TableAutoFitKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;

    class procedure RowBandSizeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure ColumnBandSizeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;

    class procedure TopTableBorderKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure LeftTableBorderKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure BottomTableBorderKewordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure RightTableBorderKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure HorizontalTableBorderKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure VerticalTableBorderKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;

    class procedure ApplyFirstRowConditionalFormattingKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure ApplyLastRowConditionalFormattingKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure ApplyFirstColumnContitionalFormattingKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure ApplyLastColumnConditionalFormattingKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure DoNotApplyRowBandingConditionalFormattingKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure DoNotApplyColumnBandingConditionalFormattingKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;

    class procedure NoBorderKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure BorderWidthKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure BorderColorKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure FrameBorderKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure BorderSpaceKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure SingleThicknessBorderTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure DoubleThicknessBorderTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure ShadowedBorderTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure DoubleBorderTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure DottedBorderTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure DashedBorderTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure HairlineBorderTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure SmallDashedBorderTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure DotDashedBorderTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure DotDotDashedBorderTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure InsetBorderTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure NoneBorderTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure OutsetBorderTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure TripletBorderTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure SmallThickThinBorderTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure SmallThinThickBorderTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure SmallThinThickThinBorderTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure MediumThickThinBorderTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure MediumThinThickBorderTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure MediumThinThickThinBorderTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure LargeThickThinBorderTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure LargeThinThickBorderTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure LargeThinThickThinBorderTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure WavyBorderTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure DoubleWavyBorderTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure StripedBorderTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure EmbossedBorderTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure EngravedBorderTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure BorderArtIndexHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
  protected
    function CanAppendText: Boolean; override;
    procedure FixLastParagraph; virtual;
    procedure InsertImage(AImporter: TdxRichEditDocumentModelRtfImporter; AImageInfo: TdxRtfImageInfo);
    procedure PopulateControlCharTable(ATable: TdxControlCharTranslatorTable); override;
    procedure PopulateKeywordTable(ATable: TdxKeywordTranslatorTable); override;
    procedure ProcessCharCore(AChar: Char); override;
    procedure ProcessTextCore(const AText: string); override;

    procedure AddCommonCharacterKeywords(ATable: TdxKeywordTranslatorTable);
    procedure AddCommonParagraphKeywords(ATable: TdxKeywordTranslatorTable);
    procedure AddCommonSymbolsAndObjectsKeywords(ATable: TdxKeywordTranslatorTable);
    procedure AddCommonTabKeywords(ATable: TdxKeywordTranslatorTable);
    procedure AddCommonNumberingListsKeywords(ATable: TdxKeywordTranslatorTable);
    procedure AppendTableKeywords(ATable: TdxKeywordTranslatorTable);
    procedure AppendTablePropertiesKeywords(ATable: TdxKeywordTranslatorTable);

    property DocumentModel: TdxDocumentModel read GetDocumentModel;
  public
    constructor Create(AImporter: TdxRichEditDocumentModelRtfImporter); override;
    destructor Destroy; override;

    class procedure AddCharacterPropertiesKeywords(ATable: TdxKeywordTranslatorTable); static;
    class procedure AddParagraphPropertiesKeywords(ATable: TdxKeywordTranslatorTable); static;

    procedure NestedGroupFinished(ADestination: TdxRichEditRtfDestinationBase); override;

    procedure FinalizePieceTableCreation; virtual;
  end;

  { TdxStringValueDestination }

  TdxStringValueDestination = class(TdxRichEditRtfDestinationBase)
  private
    FValue: string;
  protected
    function GetValue: string; virtual;
    procedure PopulateControlCharTable(ATable: TdxControlCharTranslatorTable); override;
    procedure PopulateKeywordTable(ATable: TdxKeywordTranslatorTable); override;
    procedure ProcessCharCore(AChar: Char); override;
  public
    property Value: string read GetValue;
  end;

  TdxBookmarkDestinationBase = class(TdxStringValueDestination);
  TdxBookmarkStartDestination = class(TdxBookmarkDestinationBase);
  TdxBookmarkEndDestination = class(TdxBookmarkDestinationBase);

implementation

uses
  dxRichEdit.Utils.BatchUpdateHelper, RTLConsts,
  Math, Graphics, dxRichEdit.Import.Rtf.DestinationSkip, dxRichEdit.Import.Rtf.DestinationOldParagraphNumbering,
  dxRichEdit.Import.Rtf.DestinationColorTable, Generics.Collections, dxRichEdit.Utils.PredefinedFontSizeCollection, dxRichEdit.Utils.Characters,
  dxRichEdit.Import.Rtf.DestinationPicture, dxRichEdit.DocumentModel.Core, dxRichEdit.Platform.Font,
  dxRichEdit.DocumentModel.Numbering;

{ TdxCustomMergedParagraphPropertiesCachedResult }

constructor TdxCustomMergedParagraphPropertiesCachedResult.Create;
begin
  inherited Create;
  FInitialParagraphPropertiesIndex := -1;
  FFinalParagraphPropertiesIndex := -1;
end;

{ TdxCustomMergedCharacterPropertiesCachedResult }

constructor TdxCustomMergedCharacterPropertiesCachedResult.Create;
begin
  inherited Create;
  FInitialRunCharacterPropertiesIndex := -1;
  FFinalRunCharacterPropertiesIndex := -1;
end;

{ TdxDestinationPieceTable }

constructor TdxDestinationPieceTable.Create(
  AImporter: TdxRichEditDocumentModelRtfImporter);
begin
  inherited;
end;

destructor TdxDestinationPieceTable.Destroy;
begin
  inherited;
end;

procedure TdxDestinationPieceTable.NestedGroupFinished(ADestination: TdxRichEditRtfDestinationBase);
var
  AOldListIndex: TdxNumberingListIndex;
  ALevelNumber: Integer;
  AListLevel: IdxListLevel;
  APictureDestination: TdxPictureDestination;
  AImageInfo: TdxRtfImageInfo;
begin
  if ADestination is TdxPictureDestination then
  begin
    APictureDestination := TdxPictureDestination(ADestination);
    AImageInfo := APictureDestination.GetImageInfo;
    try
      InsertImage(Importer, AImageInfo);
    finally
      AImageInfo.Free;
    end;
  end;

  AOldListIndex := IfThen(Importer.Position.CurrentOldSimpleList, Importer.Position.CurrentOldSimpleListIndex,
    Importer.Position.CurrentOldMultiLevelListIndex);
  if AOldListIndex >= NumberingListIndexMinValue then
  begin
    ALevelNumber := IfThen(Importer.Position.CurrentOldSimpleList, 0, Importer.Position.CurrentOldListLevelNumber);
    if ADestination is TdxTextBeforeDestination then
    begin
      Supports(PieceTable.DocumentModel.NumberingLists[AOldListIndex].Levels[ALevelNumber], IdxListLevel, AListLevel);
      AListLevel.ListLevelProperties.DisplayFormatString := TdxTextBeforeDestination(ADestination).Value + AListLevel.ListLevelProperties.DisplayFormatString
    end
    else
      if ADestination is TdxTextAfterDestination then
      begin
        Supports(PieceTable.DocumentModel.NumberingLists[AOldListIndex].Levels[ALevelNumber], IdxListLevel, AListLevel);
        AListLevel.ListLevelProperties.DisplayFormatString := AListLevel.ListLevelProperties.DisplayFormatString + TdxTextAfterDestination(ADestination).Value;
      end;
  end;
end;

procedure TdxDestinationPieceTable.FinalizePieceTableCreation;
begin
  Assert(PieceTable = Importer.PieceTable);
  Assert(PieceTable = Importer.PieceTableInfo.PieceTable);
  FixLastParagraph;
  Importer.LinkParagraphStylesWithNumberingLists;
  NormalizeProperties;
end;

function TdxDestinationPieceTable.CanAppendText: Boolean;
begin
  Result := True;
end;

procedure TdxDestinationPieceTable.FixLastParagraph;
begin
  if PieceTable.ShouldFixLastParagraph then
    PieceTable.FixLastParagraphCore
  else
    Importer.ApplyFormattingToLastParagraph;
  PieceTable.FixTables;
end;

procedure TdxDestinationPieceTable.PopulateControlCharTable(ATable: TdxControlCharTranslatorTable);
begin
  ATable.Add(#10, ParCharHandler);
  ATable.Add(#13, ParCharHandler);
  ATable.Add('\', EscapedCharHandler);
  ATable.Add('{', EscapedCharHandler);
  ATable.Add('}', EscapedCharHandler);
end;

procedure TdxDestinationPieceTable.PopulateKeywordTable(ATable: TdxKeywordTranslatorTable);
begin
  AddCommonCharacterKeywords(ATable);
  AddCommonParagraphKeywords(ATable);
  AddCommonSymbolsAndObjectsKeywords(ATable);
  AddCommonTabKeywords(ATable);
  AddCommonNumberingListsKeywords(ATable);
  AppendTableKeywords(ATable);
end;

procedure TdxDestinationPieceTable.ProcessCharCore(AChar: Char);
var
  APos: TdxRtfInputPosition;
begin
  APos := Importer.Position;
  if Length(APos.DoubleByteCharactersFontName) = 0 then
    PieceTable.AppendText(APos, AChar)
  else
    AppendDoubleByteChar(AChar);
end;

procedure TdxDestinationPieceTable.ProcessTextCore(const AText: string);
var
  APos: TdxRtfInputPosition;
  I: Integer;
begin
  APos := Importer.Position;
  if Length(APos.DoubleByteCharactersFontName) = 0 then
    PieceTable.AppendText(APos, AText)
  else
    for I := 1 to Length(AText) do
      AppendDoubleByteChar(AText[I]);
end;

procedure TdxDestinationPieceTable.AddCommonCharacterKeywords(ATable: TdxKeywordTranslatorTable);
begin
  ATable.Add('plain', PlainKeywordHandler);
  ATable.Add('cs', CharacterStyleIndexHandler);
  AddCharacterPropertiesKeywords(ATable);
end;

class procedure TdxDestinationPieceTable.AddCharacterPropertiesKeywords(ATable: TdxKeywordTranslatorTable);
begin
  ATable.Add('b', BoldKeywordHandler);
  ATable.Add('i', ItalicKeywordHandler);
  ATable.Add('ul', UnderlineSingleKeywordHandler);
  ATable.Add('uld', UnderlineDottedKeywordHandler);
  ATable.Add('uldash', UnderlineDashedKeywordHandler);
  ATable.Add('uldashd', UnderlineDashDottedKeywordHandler);
  ATable.Add('uldashdd', UnderlineDashDotDottedKeywordHandler);
  ATable.Add('uldb', UnderlineDoubleKeywordHandler);
  ATable.Add('ulhwave', UnderlineHeavyWaveKeywordHandler);
  ATable.Add('ulldash', UnderlineLongDashedKeywordHandler);
  ATable.Add('ulth', UnderlineThickSingleKeywordHandler);
  ATable.Add('ulthd', UnderlineThickDottedKeywordHandler);
  ATable.Add('ulthdash', UnderlineThickDashedKeywordHandler);
  ATable.Add('ulthdashd', UnderlineThickDashDottedKeywordHandler);
  ATable.Add('ulthdashdd', UnderlineThickDashDotDottedKeywordHandler);
  ATable.Add('ulthldash', UnderlineThickLongDashedKeywordHandler);
  ATable.Add('ululdbwave', UnderlineDoubleWaveKeywordHandler);
  ATable.Add('ulwave', UnderlineWaveKeywordHandler);
  ATable.Add('ulw', UnderlineWordsOnlyKeywordHandler);
  ATable.Add('ulnone', UnderlineNoneKeywordHandler);
  ATable.Add('ulc', UnderlineColorKeywordHandler);
  ATable.Add('strike', StrikeoutKeywordHandler);
  ATable.Add('striked', DoubleStrikeoutKeywordHandler);
  ATable.Add('sub', SubscriptKeywordHandler);
  ATable.Add('super', SuperscriptKeywordHandler);
  ATable.Add('nosupersub', NoSuperAndSubScriptKeywordHandler);
  ATable.Add('caps', CapsKeywordHandler);
  ATable.Add('v', HiddenTextKeywordHandler);
  ATable.Add('fs', FontSizeKeywordHandler);
  ATable.Add('f', FontNameKeywordHandler);
  ATable.Add('af', AssociatedFontNameKeywordHandler);
  ATable.Add('cf', ForeColorKeywordHandler);
  ATable.Add('cb', BackColorKeywordHandler);
  ATable.Add('highlight', BackColorKeywordHandler);
  ATable.Add('chcbpat', BackColorKeywordHandler);
  ATable.Add('dbch', DoubleByteCharactersKeywordHandler);
end;

procedure TdxDestinationPieceTable.AddCommonParagraphKeywords(ATable: TdxKeywordTranslatorTable);
begin
  ATable.Add('par', ParKeywordHandler);
  ATable.Add('pard', ResetParagraphPropertiesKeywordHandler);
  ATable.Add('s', ParagraphStyleIndexHandler);
  AddParagraphPropertiesKeywords(ATable);
end;

procedure TdxDestinationPieceTable.AddCommonSymbolsAndObjectsKeywords(ATable: TdxKeywordTranslatorTable);
begin
  ATable.Add('pict', PictureKeywordHandler);
  ATable.Add('shp', ShapeKeywordHandler);
  ATable.Add('shppict', ShapePictureKeywordHandler);
  ATable.Add('nonshppict', NonShapePictureKeywordHandler);
  ATable.Add('line', LineBreakKeywordHandler);
  ATable.Add('uc', UnicodeCountKeywordHandler);
  ATable.Add('u', UnicodeKeywordHandler);
  ATable.Add('tab', TabKeywordHandler);
  ATable.Add('emdash', EmDashKeywordHandler);
  ATable.Add('endash', EnDashKeywordHandler);
  ATable.Add('lquote', LeftSingleQuoteKeywordHandler);
  ATable.Add('rquote', RightSingleQuoteKeywordHandler);
  ATable.Add('ldblquote', LeftDoubleQuoteKeywordHandler);
  ATable.Add('rdblquote', RightDoubleQuoteKeywordHandler);
  ATable.Add('bullet', BulletKeywordHandler);
  ATable.Add('emspace', EmSpaceKeywordHandler);
  ATable.Add('enspace', EnSpaceKeywordHandler);
  ATable.Add('qmspace', QmSpaceKeywordHandler);
  ATable.Add('field', FieldStartKeywordHandler);
  ATable.Add('mailmerge', MailMergeKeywordHandler);
  ATable.Add('bkmkstart', BookmarkStartKeywordHandler);
  ATable.Add('bkmkend', BookmarkEndKeywordHandler);
  ATable.Add('protstart', RangePermissionStartKeywordHandler);
  ATable.Add('protend', RangePermissionEndKeywordHandler);
  ATable.Add('docvar', DocumentVariableKeywordHandler);
  ATable.Add('dxcustomrundata', DxCustomRunDataKeywordHandler);
end;

procedure TdxDestinationPieceTable.AddCommonTabKeywords(ATable: TdxKeywordTranslatorTable);
begin
  ATable.Add('tqr', TabRightKeywordHandler);
  ATable.Add('tqc', TabCenterKeywordHandler);
  ATable.Add('tqdec', TabDecimalKeywordHandler);
  ATable.Add('tldot', TabLeaderDotsKeywordHandler);
  ATable.Add('tlmdot', TabLeaderMiddleDotsKeywordHandler);
  ATable.Add('tlhyph', TabLeaderHyphensKeywordHandler);
  ATable.Add('tlul', TabLeaderUnderlineKeywordHandler);
  ATable.Add('tlth', TabLeaderThickLineKeywordHandler);
  ATable.Add('tleq', TabLeaderEqualSignKeywordHandler);
  ATable.Add('tx', TabPositionKeywordHandler);
  ATable.Add('tb', BarTabKeywordHandler);
end;

procedure TdxDestinationPieceTable.AddCommonNumberingListsKeywords(ATable: TdxKeywordTranslatorTable);
begin
  ATable.Add('ls', ListOverrideHandler);
  ATable.Add('ilvl', ListLevelHandler);
  ATable.Add('listtext', ListTextHandler);
  ATable.Add('pntext', ListTextHandler);
  ATable.Add('pnseclvl', SectionLevelNumberingKeywordHandler);
  ATable.Add('pn', OldParagraphNumberingKeywordHandler);
  ATable.Add('pntxta', OldParagraphNumberingTextAfterKeywordHandler);
  ATable.Add('pntxtb', OldParagraphNumberingTextBeforeKeywordHandler);
end;

class procedure TdxDestinationPieceTable.AddParagraphPropertiesKeywords(ATable: TdxKeywordTranslatorTable);
begin
  ATable.Add('ql', AlignLeftKeywordHandler);
  ATable.Add('qc', AlignCenterKeywordHandler);
  ATable.Add('qr', AlignRightKeywordHandler);
  ATable.Add('qd', AlignJustifyKeywordHandler);
  ATable.Add('qj', AlignJustifyKeywordHandler);
  ATable.Add('li', LeftIndentKeywordHandler);
  ATable.Add('lin', LeftIndentKeywordHandler);
  ATable.Add('ri', RightIndentKeywordHandler);
  ATable.Add('fi', FirstLineIndentKeywordHandler);
  ATable.Add('rin', RightIndentKeywordHandler);
  ATable.Add('sb', SpacingBeforeKeywordHandler);
  ATable.Add('sa', SpacingAfterKeywordHandler);
  ATable.Add('sl', LineSpacingTypeKeywordHandler);
  ATable.Add('slmult', LineSpacingMultiplierKeywordHandler);
  ATable.Add('widctlpar', WidowOrphanControlOnKeywordHandler);
  ATable.Add('nowidctlpar', WidowOrphanControlOffKeywordHandler);
end;

procedure TdxDestinationPieceTable.AppendTableKeywords(ATable: TdxKeywordTranslatorTable);
begin
  ATable.Add('intbl', InTableParagraphKeywordHandler);
  ATable.Add('row', RowKeywordHandler);
  ATable.Add('cell', CellKeywordHandler);
  ATable.Add('nestcell', NestedCellKeywordHandler);
  ATable.Add('nestrow', NestedRowKeywordHandler);
  ATable.Add('nesttableprops', NestedTablePropertiesKeywordHandler);
  ATable.Add('nonesttables', NoNestedTablesKeywordHandler);
  ATable.Add('itap', ItapKeywordHandler);
  AppendTablePropertiesKeywords(ATable);
end;

procedure TdxDestinationPieceTable.AppendTablePropertiesKeywords(ATable: TdxKeywordTranslatorTable);
begin
  ATable.Add('trowd', TableRowDefaultsKeywordHandler);
  ATable.Add('ts', TableStyleKeywordHandler);
  ATable.Add('cellx', CellxKeywordHandler);
  ATable.Add('clwWidth', CellPreferredWidthKeywordHandler);
  ATable.Add('clftsWidth', WidthUnitTypeKeywordHandler);
  ATable.Add('clmgf', FirstHorizontalMergedCellKeywordHandler);
  ATable.Add('clmrg', NextHorizontalMergedCellKeywordHandler);
  ATable.Add('clvmgf', FirstVerticalMergedCellKeywordHandler);
  ATable.Add('clvmrg', NextVerticalMergedCellKeywordHandler);
  ATable.Add('clFitText', CellFitTextKeywordHandler);
  ATable.Add('clNoWrap', CellNoWrapKeywordHandler);
  ATable.Add('tsvertalt', CellVerticalAlignmentTopKeywordHandler);
  ATable.Add('tsvertalc', CellVerticalAlignmentCenterKeywordHandler);
  ATable.Add('tsvertalb', CellVerticalAlignmentBottomKeywordHandler);
  ATable.Add('clhidemark', CellHideMarkKeywordHandler);
  ATable.Add('clpadb', CellBottomCellMarginKeywordHandler);
  ATable.Add('clpadl', CellLeftCellMarginKeywordHandler);
  ATable.Add('clpadr', CellRightCellMarginKeywordHandler);
  ATable.Add('clpadt', CellTopCellMarginKeywordHandler);
  ATable.Add('clpadfb', CellBottomCellMarginUnitTypeKeywordHandler);
  ATable.Add('clpadfl', CellLeftCellMarginUnitTypeKeywordHandler);
  ATable.Add('clpadfr', CellRightCellMarginUnitTypeKeywordHandler);
  ATable.Add('clpadft', CellTopCellMarginUnitTypeKeywordHandler);
  ATable.Add('clvertalt', CellTextTopAlignmentKeywordHandler);
  ATable.Add('clvertalc', CellTextCenterVerticalAlignmentKeywordHandler);
  ATable.Add('clvertalb', CellTextBottomAlignmentKeywordHandler);
  ATable.Add('cltxlrtb', CellLeftToRightTopToBottomTextDirectionKeywordHandler);
  ATable.Add('cltxtbrl', CellTopToBottomRightToLeftTextDirectionKeywordHandler);
  ATable.Add('cltxbtlr', CellBottomToTopLeftToRightTextDirectionKeywordHandler);
  ATable.Add('cltxlrtbv', CellLeftToRightTopToBottomVerticalTextDirectionKeywordHandler);
  ATable.Add('cltxtbrlv', CellTopToBottomRightToLeftVerticalTextDirectionKeywordHandler);
  ATable.Add('clcbpat', CellBackgroundColorHandler);

  ATable.Add('brdrtbl', NoTableBorderKeywordHandler);
  ATable.Add('clbrdrb', BottomCellBorderKeywordHandler);
  ATable.Add('clbrdrt', TopCellBorderKeywordHandler);
  ATable.Add('clbrdrl', LeftCellBorderKeywordHandler);
  ATable.Add('clbrdrr', RightCellBorderKeywordHandler);
  ATable.Add('cldglu', UpperLeftToLowerRightBorderKeywordHandler);
  ATable.Add('cldgll', UpperRightToLowerLeftBorderKeywordHandler);

  ATable.Add('trleft', RowLeftKeywordHandler);
  ATable.Add('trhdr', RowHeaderKeywordHandler);
  ATable.Add('trrh', RowHeightKeywordHandler);
  ATable.Add('trkeep', RowKeepKeywordHandler);
  ATable.Add('trqr', TableRightAlignmentKeywordHandler);
  ATable.Add('trql', TableLeftAlignmentKeywordHandler);
  ATable.Add('trqc', TableCenterAlignmentKeywordHandler);
  ATable.Add('trwWidthB', WidthBeforeKeywordHandler);
  ATable.Add('trftsWidthB', WidthBeforeUnitTypeKeywordHandler);
  ATable.Add('trwWidthA', WidthAfterKeywordHandler);
  ATable.Add('trftsWidthA', WidthAfterUnitTypeKeywordHandler);

  ATable.Add('trgaph', SpaceBetweenCellsKeywordHandler);
  ATable.Add('trpaddb', TableBottomCellMarginKeywordHandler);
  ATable.Add('trpaddl', TableLeftCellMarginKeywordHandler);
  ATable.Add('trpaddr', TableRightCellMarginKeywordHandler);
  ATable.Add('trpaddt', TableTopCellMarginKeywordHandler);
  ATable.Add('trpaddfb', TableBottomCellMarginUnitTypeKeywordHandler);
  ATable.Add('trpaddfl', TableLeftCellMarginUnitTypeKeywordHandler);
  ATable.Add('trpaddfr', TableRightCellMarginUnitTypeKeywordHandler);
  ATable.Add('trpaddft', TableTopCellMarginUnitTypeKeywordHandler);

  ATable.Add('trspdb', TableBottomCellSpacingKeywordHandler);
  ATable.Add('trspdl', TableLeftCellSpacingKeywordHandler);
  ATable.Add('trspdr', TableRightCellSpacingKeywordHandler);
  ATable.Add('trspdt', TableTopCellSpacingKeywordHandler);
  ATable.Add('trspdfb', TableBottomCellSpacingUnitTypeKeywordHandler);
  ATable.Add('trspdfl', TableLeftCellSpacingUnitTypeKeywordHandler);
  ATable.Add('trspdfr', TableRightCellSpacingUnitTypeKeywordHandler);
  ATable.Add('trspdft', TableTopCellSpacingUnitTypeKeywordHandler);

  ATable.Add('trwWidth', TablePreferredWidthKeywordHandler);
  ATable.Add('trftsWidth', TablePreferredWidthUnitTypeKeywordHandler);
  ATable.Add('tblind', TableIndentKeywordHandler);
  ATable.Add('tblindtype', TableIndentUnitTypeHandler);
  ATable.Add('tabsnoovrlp', TableOverlapKeywordHandler);

  ATable.Add('tdfrmtxtLeft', TableLeftFromTextKeywordHandler);
  ATable.Add('tdfrmtxtRight', TableRightFromTextKeywordHandler);
  ATable.Add('tdfrmtxtTop', TableTopFromTextKeywordHandler);
  ATable.Add('tdfrmtxtBottom', TableBottomFromTextKeywordHandler);
  ATable.Add('tphcol', ColHorizontalAnchorKeywordHandler);
  ATable.Add('tphmrg', MarginHorizontalAnchorKeywordHandler);
  ATable.Add('tphpg', PageHorizontalAnchorKeywordHandler);
  ATable.Add('tpvmrg', MarginVerticalAnchorKeywordHandler);
  ATable.Add('tpvpara', ParagraphVerticalAnchorKewordHandler);
  ATable.Add('tpvpg', PageVerticalAnchorKeywordHandler);
  ATable.Add('tposx', TableHorizontalPositionKeywordHandler);
  ATable.Add('tposnegx', TableHorizontalPositionKeywordHandler);
  ATable.Add('tposy', TableVerticalPositionKeywordHandler);
  ATable.Add('tposnegy', TableVerticalPositionKeywordHandler);
  ATable.Add('tposxc', CenterTableHorizontalAlignKeywordHandler);
  ATable.Add('tposxi', InsideTableHorizontalAlignKeywordHandler);
  ATable.Add('tposxl', LeftTableHorizontalAlignKeywordHandler);
  ATable.Add('tposxo', OutsideTableHorizontalAlignKeywordHandler);
  ATable.Add('tposxr', RightTableHorizontalAlignKeywordHandler);
  ATable.Add('tposyb', BottomTableVerticalAlignKeywordHandler);
  ATable.Add('tposyc', CenterTableVerticalAlignKeywordHandler);
  ATable.Add('tposyil', InlineTableVerticalAlignKeywordHandler);
  ATable.Add('tposyin', InsideTableVerticalAlignKeywordHandler);
  ATable.Add('tposyout', OutsideTableVerticalAlignKeywordHandler);
  ATable.Add('tposyt', TopTableVerticalAlignKeywordHandler);
  ATable.Add('trautofit', TableAutoFitKeywordHandler);

  ATable.Add('tscbandsh', RowBandSizeKeywordHandler);
  ATable.Add('tscbandsv', ColumnBandSizeKeywordHandler);
  ATable.Add('tscellcbpat', CellBackgroundColorHandler);
  ATable.Add('tsbrdrt', TopCellBorderKeywordHandler);
  ATable.Add('tsbrdrl', LeftCellBorderKeywordHandler);
  ATable.Add('tsbrdrb', BottomCellBorderKeywordHandler);
  ATable.Add('tsbrdrr', RightCellBorderKeywordHandler);
  ATable.Add('tsnowrap', CellNoWrapKeywordHandler);
  ATable.Add('tscellpaddb', TableBottomCellMarginKeywordHandler);
  ATable.Add('tscellpaddl', TableLeftCellMarginKeywordHandler);
  ATable.Add('tscellpaddr', TableRightCellMarginKeywordHandler);
  ATable.Add('tscellpaddt', TableTopCellMarginKeywordHandler);
  ATable.Add('tscellpaddfb', TableBottomCellMarginUnitTypeKeywordHandler);
  ATable.Add('tscellpaddfl', TableLeftCellMarginUnitTypeKeywordHandler);
  ATable.Add('tscellpaddfr', TableRightCellMarginUnitTypeKeywordHandler);
  ATable.Add('tscellpaddft', TableTopCellMarginUnitTypeKeywordHandler);
  ATable.Add('tsbrdrdgl', UpperLeftToLowerRightBorderKeywordHandler);
  ATable.Add('tsbrdrdgr', UpperRightToLowerLeftBorderKeywordHandler);
  ATable.Add('tsrowd', TableRowDefaultsKeywordHandler);


  ATable.Add('trbrdrt', TopTableBorderKeywordHandler);
  ATable.Add('trbrdrl', LeftTableBorderKeywordHandler);
  ATable.Add('trbrdrb', BottomTableBorderKewordHandler);
  ATable.Add('trbrdrr', RightTableBorderKeywordHandler);
  ATable.Add('trbrdrh', HorizontalTableBorderKeywordHandler);
  ATable.Add('trbrdrv', VerticalTableBorderKeywordHandler);

  ATable.Add('tbllkhdrrows', ApplyFirstRowConditionalFormattingKeywordHandler);
  ATable.Add('tbllklastrow', ApplyLastRowConditionalFormattingKeywordHandler);
  ATable.Add('tbllkhdrcols', ApplyFirstColumnContitionalFormattingKeywordHandler);
  ATable.Add('tbllklastcol', ApplyLastColumnConditionalFormattingKeywordHandler);
  ATable.Add('tbllknorowband', DoNotApplyRowBandingConditionalFormattingKeywordHandler);
  ATable.Add('tbllknocolband', DoNotApplyColumnBandingConditionalFormattingKeywordHandler);

  ATable.Add('brdrnil', NoBorderKeywordHandler);
  ATable.Add('brdrw', BorderWidthKeywordHandler);
  ATable.Add('brdrcf', BorderColorKeywordHandler);
  ATable.Add('brdrframe', FrameBorderKeywordHandler);
  ATable.Add('brsp', BorderSpaceKeywordHandler);
  ATable.Add('brdrs', SingleThicknessBorderTypeKeywordHandler);
  ATable.Add('brdrth', DoubleThicknessBorderTypeKeywordHandler);
  ATable.Add('brdrsh', ShadowedBorderTypeKeywordHandler);
  ATable.Add('brdrdb', DoubleBorderTypeKeywordHandler);
  ATable.Add('brdrdot', DottedBorderTypeKeywordHandler);
  ATable.Add('brdrdash', DashedBorderTypeKeywordHandler);
  ATable.Add('brdrhair', HairlineBorderTypeKeywordHandler);
  ATable.Add('brdrdashsm', SmallDashedBorderTypeKeywordHandler);
  ATable.Add('brdrdashd', DotDashedBorderTypeKeywordHandler);
  ATable.Add('brdrdashdd', DotDotDashedBorderTypeKeywordHandler);
  ATable.Add('brdrdashdot', DotDashedBorderTypeKeywordHandler);
  ATable.Add('brdrdashdotdot', DotDotDashedBorderTypeKeywordHandler);
  ATable.Add('brdrinset', InsetBorderTypeKeywordHandler);
  ATable.Add('brdrnone', NoneBorderTypeKeywordHandler);
  ATable.Add('brdroutset', OutsetBorderTypeKeywordHandler);
  ATable.Add('brdrtriple', TripletBorderTypeKeywordHandler);
  ATable.Add('brdrtnthsg', SmallThickThinBorderTypeKeywordHandler);
  ATable.Add('brdrthtnsg', SmallThinThickBorderTypeKeywordHandler);
  ATable.Add('brdrtnthtnsg', SmallThinThickThinBorderTypeKeywordHandler);
  ATable.Add('brdrtnthmg', MediumThickThinBorderTypeKeywordHandler);
  ATable.Add('brdrthtnmg', MediumThinThickBorderTypeKeywordHandler);
  ATable.Add('brdrtnthtnmg', MediumThinThickThinBorderTypeKeywordHandler);
  ATable.Add('brdrtnthlg', LargeThickThinBorderTypeKeywordHandler);
  ATable.Add('brdrthtnlg', LargeThinThickBorderTypeKeywordHandler);
  ATable.Add('brdrtnthtnlg', LargeThinThickThinBorderTypeKeywordHandler);
  ATable.Add('brdrwavy', WavyBorderTypeKeywordHandler);
  ATable.Add('brdrwavydb', DoubleWavyBorderTypeKeywordHandler);
  ATable.Add('brdrdashdotstr', StripedBorderTypeKeywordHandler);
  ATable.Add('brdremboss', EmbossedBorderTypeKeywordHandler);
  ATable.Add('brdrengrave', EngravedBorderTypeKeywordHandler);
  ATable.Add('brdrart', BorderArtIndexHandler);
end;

procedure TdxDestinationPieceTable.AppendDoubleByteChar(AChar: Char);
var
  APos: TdxRtfInputPosition;
  AOldFontName: string;
begin
  APos := Importer.Position;
  AOldFontName := APos.CharacterFormatting.FontName;
  if IsDoubleByteChar(AChar) then
    APos.CharacterFormatting.FontName := APos.DoubleByteCharactersFontName;
  PieceTable.AppendText(APos, AChar);
  APos.CharacterFormatting.FontName := AOldFontName;
end;

function TdxDestinationPieceTable.GetDocumentModel: TdxDocumentModel;
begin
  Result := PieceTable.DocumentModel;
end;

function TdxDestinationPieceTable.GetNormalizedNumberingListIndex(AOwnNumberingListIndex, AParentNumberingListIndex: TdxNumberingListIndex): TdxNumberingListIndex;
begin
  Result := AOwnNumberingListIndex;
  if AOwnNumberingListIndex = AParentNumberingListIndex then
    Result := NumberingListIndexListIndexNotSetted
  else
    if (AOwnNumberingListIndex = NumberingListIndexNoNumberingList) or (AOwnNumberingListIndex = NumberingListIndexListIndexNotSetted) then
    begin
      if AParentNumberingListIndex >= NumberingListIndexMinValue then
        Result := NumberingListIndexNoNumberingList
      else
        Result := NumberingListIndexListIndexNotSetted;
    end;
end;

class function TdxDestinationPieceTable.GetWidthUnitType(
  AParameterValue: Integer): TdxWidthUnitType;
begin
  case AParameterValue of
    0: Result := TdxWidthUnitType.Nil;
    1: Result := TdxWidthUnitType.Auto;
    2: Result := TdxWidthUnitType.FiftiethsOfPercent;
    3: Result := TdxWidthUnitType.ModelUnits;
  else
    Result := TdxWidthUnitType.Nil;
  end;
end;

type
  TdxInlinePictureRunAccess = class(TdxInlinePictureRun);

procedure TdxDestinationPieceTable.InsertImage(
  AImporter: TdxRichEditDocumentModelRtfImporter; AImageInfo: TdxRtfImageInfo);
var
  ATable: TdxPieceTable;
  AScaleX, AScaleY: Integer;
  ARuns: TdxTextRunCollection;
  ARun: TdxInlinePictureRunAccess;
begin
  if not TdxDocumentFormatsHelper.ShouldInsertPicture(AImporter.DocumentModel) or
    (AImageInfo = nil) or (AImageInfo.RtfImage = nil) then
  begin
    InsertSpace(AImporter);
    Exit;
  end;
  ATable := AImporter.PieceTable;

  AScaleX := Max(1, AImageInfo.ScaleX);
  AScaleY := Max(1, AImageInfo.ScaleY);
  ATable.AppendImage(AImporter.Position, AImageInfo.RtfImage, AScaleX, AScaleY);
  ARuns := ATable.Runs;
  ARun := TdxInlinePictureRunAccess(ARuns[ARuns.Count - 2]);
  ARun.SetOriginalSize(AImageInfo.SizeInModelUnits);
  ARun.Properties.PseudoInline := AImageInfo.PseudoInline;
end;

class procedure TdxDestinationPieceTable.InsertSpace(AImporter: TdxRichEditDocumentModelRtfImporter);
begin
  AImporter.PieceTable.InsertTextCore(AImporter.Position, ' ');
end;

function TdxDestinationPieceTable.IsDoubleByteChar(AChar: Char): Boolean;
var
  AOrd: Word absolute AChar;
begin
  Result := (AOrd > HighestCyrillic) and
    ((AOrd < LowLatinExtendedAdditional) or (AOrd > HighLatinExtendedAdditional)) and
    ((AOrd < LowGeneralPunctuation) or (AOrd > HighGeneralPunctuation)) and
    ((AOrd < LowCurrencySymbols) or (AOrd > HighCurrencySymbols)) and
    ((AOrd < LowLetterlikeSymbols) or (AOrd > HighLetterlikeSymbols)) and
    ((AOrd < LowNumberForms) or (AOrd > HighNumberForms)) and
    ((AOrd < LowMathematicalOperations) or (AOrd > HighMathematicalOperations));
end;

procedure TdxDestinationPieceTable.NormalizeProperties;
begin
  NormalizeParagraphsProperties;
  NormalizeRunsProperties;
end;

procedure TdxDestinationPieceTable.NormalizeParagraphsProperties;
var
  ACachedResult: TdxCustomMergedParagraphPropertiesCachedResult;
  AParagraphs: TdxParagraphCollection;
  ACount: TdxParagraphIndex;
  I: Integer;
begin
  ACachedResult := TdxCustomMergedParagraphPropertiesCachedResult.Create;
  try
    AParagraphs := PieceTable.Paragraphs;
    ACount := AParagraphs.Count;
    for I := 0 to ACount - 1 do
      NormalizeParagraphProperties(AParagraphs[I], ACachedResult);
  finally
    FreeAndNil(ACachedResult);
  end;
end;

procedure TdxDestinationPieceTable.NormalizeParagraphProperties(AParagraph: TdxParagraph; ACachedResult: TdxCustomMergedParagraphPropertiesCachedResult);
var
  AProperties: TdxParagraphProperties;
  AParentProperties: TdxMergedParagraphProperties;
begin
  AProperties := AParagraph.ParagraphProperties;
  if (ACachedResult.InitialParagraphPropertiesIndex = AProperties.Index) and AParagraph.TryUseParentMergedCachedResult(ACachedResult) then
    AProperties.SetIndexInitial(ACachedResult.FinalParagraphPropertiesIndex)
  else
  begin
    ACachedResult.InitialParagraphPropertiesIndex := AProperties.Index;
    AParentProperties := AParagraph.GetParentMergedParagraphProperties;
    ACachedResult.MergedParagraphProperties := AParentProperties;
    Importer.ApplyParagraphProperties(AProperties, AProperties.Info.Info, AParentProperties);
    ACachedResult.FinalParagraphPropertiesIndex := AProperties.Index;
  end;
  if AParagraph.GetListLevelIndex = 0 then
    AParagraph.SetNumberingListIndex(GetNormalizedNumberingListIndex(AParagraph.GetOwnNumberingListIndex, AParagraph.ParagraphStyle.GetNumberingListIndex));
end;

procedure TdxDestinationPieceTable.NormalizeRunProperties(ARun: TdxTextRunBase; ACachedResult: TdxCustomMergedCharacterPropertiesCachedResult);
var
  AProperties: TdxCharacterProperties;
  AParentProperties: TdxMergedCharacterProperties;
begin
  AProperties := ARun.CharacterProperties;
  if (ACachedResult.InitialRunCharacterPropertiesIndex = AProperties.Index) and ARun.TryUseParentMergedCachedResult(ACachedResult) then
  begin
    AProperties.SetIndexInitial(ACachedResult.FinalRunCharacterPropertiesIndex);
    Exit;
  end;
  ACachedResult.InitialRunCharacterPropertiesIndex := AProperties.Index;
  AParentProperties := ARun.GetParentMergedCharacterProperties;
  ACachedResult.MergedCharacterProperties := AParentProperties;
  Importer.ApplyCharacterProperties(AProperties, AProperties.Info.Info, AParentProperties);
  ACachedResult.FinalRunCharacterPropertiesIndex := AProperties.Index;
end;

procedure TdxDestinationPieceTable.NormalizeRunsProperties;
var
  ACachedResults: TDictionary<TClass, TdxCustomMergedCharacterPropertiesCachedResult>;
  ACachedResult: TdxCustomMergedCharacterPropertiesCachedResult;
  ARuns: TdxTextRunCollection;
  ARun: TdxTextRunBase;
  I, ACount: TdxRunIndex;
begin
  ACachedResults := TObjectDictionary<TClass, TdxCustomMergedCharacterPropertiesCachedResult>.Create([doOwnsValues]);
  try
    ARuns := PieceTable.Runs;
    ACount := ARuns.Count;
    for I := 0 to ACount - 1 do
    begin
      ARun := ARuns[I];
      if not ACachedResults.TryGetValue(ARun.ClassType, ACachedResult) then
      begin
        ACachedResult := TdxCustomMergedCharacterPropertiesCachedResult.Create;
        ACachedResults.Add(ARun.ClassType, ACachedResult);
      end;
      NormalizeRunProperties(ARun, ACachedResult);
    end;
  finally
    ACachedResults.Free;
  end;
end;

class procedure TdxDestinationPieceTable.AssignWidthUnitInfo(AUnitInfo: TdxWidthUnit; Value: Integer);
begin
  if Value = 3 then
    AUnitInfo.&Type := TdxWidthUnitType.ModelUnits
  else
    if Value = 0 then
      AUnitInfo.&Type := TdxWidthUnitType.Nil;
end;

class function TdxDestinationPieceTable.ShouldApplyParagraphStyle(AImporter: TdxRichEditDocumentModelRtfImporter): Boolean;
begin
  Result := AImporter.DocumentModel.DocumentCapabilities.ParagraphStyleAllowed;
end;

class procedure TdxDestinationPieceTable.AlignLeftKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  if ShouldApplyParagraphStyle(AImporter) then
    AImporter.Position.ParagraphFormattingInfo.Alignment := TdxParagraphAlignment.Left;
end;

class procedure TdxDestinationPieceTable.AlignCenterKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  if ShouldApplyParagraphStyle(AImporter) then
    AImporter.Position.ParagraphFormattingInfo.Alignment := TdxParagraphAlignment.Center;
end;

class procedure TdxDestinationPieceTable.AlignRightKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  if ShouldApplyParagraphStyle(AImporter) then
    AImporter.Position.ParagraphFormattingInfo.Alignment := TdxParagraphAlignment.Right;
end;

class procedure TdxDestinationPieceTable.AlignJustifyKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  if ShouldApplyParagraphStyle(AImporter) then
    AImporter.Position.ParagraphFormattingInfo.Alignment := TdxParagraphAlignment.Justify;
end;

class procedure TdxDestinationPieceTable.AssociatedFontNameKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  if AImporter.Position.IsDoubleByteCharactersFont then
    FontNameKeywordHandler(AImporter, AParameterValue, AHasParameter);
end;

class procedure TdxDestinationPieceTable.BackColorKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
var
  AProperties: TdxRtfDocumentProperties;
  AColor: TColor;
begin
  if not AHasParameter then
    AParameterValue := 0;
  AProperties := AImporter.DocumentProperties;
  AColor := AProperties.Colors[AParameterValue];
  if AColor = 0 then
    AColor := clNone;
  AImporter.Position.CharacterFormatting.BackColor := AColor;
end;

class procedure TdxDestinationPieceTable.BarTabKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
var
  AInfo: TdxRtfParagraphFormattingInfo;
begin
  AInfo := AImporter.Position.ParagraphFormattingInfo;
  AInfo.TabAlignment := TdxTabInfo.DefaultAlignment;
  AInfo.TabLeader := TdxTabInfo.DefaultLeader;
end;

class procedure TdxDestinationPieceTable.BoldKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.Position.CharacterFormatting.FontBold := not AHasParameter or (AParameterValue <> 0);
end;

class procedure TdxDestinationPieceTable.BookmarkEndKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.Destination := TdxBookmarkEndDestination.Create(AImporter);
end;

class procedure TdxDestinationPieceTable.BookmarkStartKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.Destination := TdxBookmarkStartDestination.Create(AImporter);
end;

class procedure TdxDestinationPieceTable.BulletKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.ParseCharWithoutDecoding(TdxCharacters.Bullet);
end;

class procedure TdxDestinationPieceTable.CapsKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.Position.CharacterFormatting.AllCaps := not AHasParameter or (AParameterValue > 0);
end;

class procedure TdxDestinationPieceTable.CharacterStyleIndexHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  if AImporter.DocumentModel.DocumentCapabilities.CharacterStyleAllowed then
    AImporter.Position.CharacterStyleIndex := AImporter.GetCharacterStyleIndex(AParameterValue);
end;

class procedure TdxDestinationPieceTable.DocumentVariableKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.DoubleByteCharactersKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.Position.IsDoubleByteCharactersFont := True;
end;

class procedure TdxDestinationPieceTable.DoubleStrikeoutKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
const
  AValueMap: array[Boolean] of TdxStrikeoutType = (TdxStrikeoutType.None, TdxStrikeoutType.Double);
begin
  AImporter.Position.CharacterFormatting.FontStrikeoutType := AValueMap[not AHasParameter or (AParameterValue > 0)];
end;

class procedure TdxDestinationPieceTable.DxCustomRunDataKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.EmDashKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.ParseCharWithoutDecoding(TdxCharacters.EmDash);
end;

class procedure TdxDestinationPieceTable.EmSpaceKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.ParseCharWithoutDecoding(TdxCharacters.EmSpace);
end;

class procedure TdxDestinationPieceTable.EnDashKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.ParseCharWithoutDecoding(TdxCharacters.EnSpace);
end;

class procedure TdxDestinationPieceTable.EnSpaceKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.ParseCharWithoutDecoding(TdxCharacters.EmSpace);
end;

class procedure TdxDestinationPieceTable.FieldStartKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.FirstLineIndentKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
var
  AInfo: TdxParagraphFormattingInfo;
  AIndent: Integer;
begin
  AInfo := AImporter.Position.ParagraphFormattingInfo;
  AIndent := AImporter.UnitConverter.TwipsToModelUnits(AParameterValue);
  if not ShouldApplyParagraphStyle(AImporter) or (AIndent = 0) then
  begin
    AInfo.FirstLineIndentType := TdxParagraphFirstLineIndent.None;
    AInfo.FirstLineIndent := 0;
  end
  else
  begin
    if AIndent > 0 then
    begin
      AInfo.FirstLineIndentType := TdxParagraphFirstLineIndent.Indented;
      AInfo.FirstLineIndent := AIndent;
    end
    else
      if AIndent < 0 then
      begin
        AInfo.FirstLineIndentType := TdxParagraphFirstLineIndent.Hanging;
        AInfo.FirstLineIndent := -AIndent;
      end;
  end;
end;

class procedure TdxDestinationPieceTable.FontNameKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  if not AHasParameter then
    AParameterValue := AImporter.DocumentProperties.DefaultFontNumber;
  AImporter.SetFont(AImporter.DocumentProperties.Fonts.GetRtfFontInfoById(AParameterValue));
end;

class procedure TdxDestinationPieceTable.FontSizeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  if not AHasParameter then
    AParameterValue := 24;
  AImporter.Position.CharacterFormatting.DoubleFontSize := Max(TdxPredefinedFontSizeCollection.MinFontSize, AParameterValue);
end;

class procedure TdxDestinationPieceTable.ForeColorKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
var
  AProperties: TdxRtfDocumentProperties;
  AForeColor: TColor;
begin
  if not AHasParameter then
    AParameterValue := 0;
  AProperties := AImporter.DocumentProperties;
  AForeColor := AProperties.Colors[AParameterValue];
  if TdxColorTableDestination.AutoColor = AForeColor then
    AForeColor := clBlack;
  AImporter.Position.CharacterFormatting.ForeColor := AForeColor;
end;

class procedure TdxDestinationPieceTable.HiddenTextKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.Position.CharacterFormatting.Hidden := not AHasParameter or (AParameterValue <> 0);
end;

class procedure TdxDestinationPieceTable.ItalicKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.Position.CharacterFormatting.FontItalic := not AHasParameter or (AParameterValue <> 0);
end;

class procedure TdxDestinationPieceTable.LeftDoubleQuoteKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.ParseCharWithoutDecoding(TdxCharacters.LeftDoubleQuote);
end;

class procedure TdxDestinationPieceTable.LeftIndentKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  if ShouldApplyParagraphStyle(AImporter) then
    AImporter.Position.ParagraphFormattingInfo.LeftIndent := AImporter.UnitConverter.TwipsToModelUnits(AParameterValue);
end;

class procedure TdxDestinationPieceTable.LeftSingleQuoteKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.ParseCharWithoutDecoding(TdxCharacters.LeftSingleQuote);
end;

class procedure TdxDestinationPieceTable.LineBreakKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  case AImporter.Options.LineBreakSubstitute of
    TdxLineBreakSubstitute.Space:
      AImporter.ParseCharWithoutDecoding(TdxCharacters.Space);
    TdxLineBreakSubstitute.Paragraph:
      ParKeywordHandler(AImporter, 0, False);
  else
    AImporter.ParseCharWithoutDecoding(TdxCharacters.LineBreak);
  end;
end;

class procedure TdxDestinationPieceTable.ListLevelHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.Position.ParagraphFormattingInfo.ListLevelIndex := AParameterValue;
end;

class procedure TdxDestinationPieceTable.ListOverrideHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
var
  AIndex: TdxNumberingListIndex;
begin
  if AImporter.ListOverrideIndexToNumberingListIndexMap.TryGetValue(AParameterValue, AIndex) then
    AImporter.Position.ParagraphFormattingInfo.NumberingListIndex := AIndex;
end;

class procedure TdxDestinationPieceTable.LineSpacingTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  if ShouldApplyParagraphStyle(AImporter) then
    AImporter.Position.ParagraphFormattingInfo.RtfLineSpacingType := AParameterValue;
end;

class procedure TdxDestinationPieceTable.LineSpacingMultiplierKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  if ShouldApplyParagraphStyle(AImporter) then
    AImporter.Position.ParagraphFormattingInfo.RtfLineSpacingMultiplier := AParameterValue;
end;

class procedure TdxDestinationPieceTable.ListTextHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.Destination := TdxSkipDestination.Create(AImporter);
end;

class procedure TdxDestinationPieceTable.MailMergeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.NonShapePictureKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.NoSuperAndSubScriptKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.Position.CharacterFormatting.Script := TdxCharacterFormattingScript.Normal;
end;

class procedure TdxDestinationPieceTable.OldParagraphNumberingKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.Destination := TdxDestinationOldParagraphNumbering.Create(AImporter);
end;

class procedure TdxDestinationPieceTable.OldParagraphNumberingTextAfterKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.Destination := TdxTextAfterDestination.Create(AImporter);
end;

class procedure TdxDestinationPieceTable.OldParagraphNumberingTextBeforeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.Destination := TdxTextBeforeDestination.Create(AImporter);
end;

class procedure TdxDestinationPieceTable.ParagraphStyleIndexHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  if ShouldApplyParagraphStyle(AImporter) then
    AImporter.Position.ParagraphFormattingInfo.StyleIndex := AImporter.GetParagraphStyleIndex(AParameterValue);
end;

class procedure TdxDestinationPieceTable.ParKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  if AImporter.DocumentModel.DocumentCapabilities.ParagraphsAllowed then
    AImporter.InsertParagraph
  else
    InsertSpace(AImporter);
end;

class procedure TdxDestinationPieceTable.PictureKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.Destination := TdxPictureDestination.Create(AImporter);
end;

class procedure TdxDestinationPieceTable.PlainKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
var
  ADefaultFormatting: TdxCharacterFormattingInfo;
  ACurrentPositionFormatting: TdxCharacterFormattingBase;
begin
  ADefaultFormatting := AImporter.DocumentModel.Cache.CharacterFormattingInfoCache.DefaultItem;
  ACurrentPositionFormatting := AImporter.Position.CharacterFormatting;
  ACurrentPositionFormatting.FontName := AImporter.DocumentProperties.Fonts.GetRtfFontInfoById(AImporter.DocumentProperties.DefaultFontNumber).Name;
  ACurrentPositionFormatting.DoubleFontSize := ADefaultFormatting.DoubleFontSize;
  ACurrentPositionFormatting.FontBold := ADefaultFormatting.FontBold;
  ACurrentPositionFormatting.FontItalic := ADefaultFormatting.FontItalic;
  ACurrentPositionFormatting.FontStrikeoutType := ADefaultFormatting.FontStrikeoutType;
  ACurrentPositionFormatting.FontUnderlineType := ADefaultFormatting.FontUnderlineType;
  ACurrentPositionFormatting.AllCaps := ADefaultFormatting.AllCaps;
  ACurrentPositionFormatting.Hidden := ADefaultFormatting.Hidden;
  ACurrentPositionFormatting.UnderlineWordsOnly := ADefaultFormatting.UnderlineWordsOnly;
  ACurrentPositionFormatting.StrikeoutWordsOnly := ADefaultFormatting.StrikeoutWordsOnly;
  ACurrentPositionFormatting.ForeColor := ADefaultFormatting.ForeColor;
  ACurrentPositionFormatting.BackColor := ADefaultFormatting.BackColor;
  ACurrentPositionFormatting.StrikeoutColor := ADefaultFormatting.StrikeoutColor;
  ACurrentPositionFormatting.UnderlineColor := ADefaultFormatting.UnderlineColor;
  ACurrentPositionFormatting.Script := ADefaultFormatting.Script;
  AImporter.SetCodePage(AImporter.DocumentProperties.DefaultCodePage);
end;

class procedure TdxDestinationPieceTable.QmSpaceKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.ParseCharWithoutDecoding(TdxCharacters.QmSpace);
end;

class procedure TdxDestinationPieceTable.RangePermissionEndKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.RangePermissionStartKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.ResetParagraphPropertiesKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
var
  AInfo: TdxRtfParagraphFormattingInfo;
begin
  if ShouldApplyParagraphStyle(AImporter) then
  begin
    AInfo := TdxRtfParagraphFormattingInfo.Create;
    try
      AImporter.Position.ParagraphFormattingInfo := AInfo;
    finally
      AInfo.Free;
    end;
  end;
end;

class procedure TdxDestinationPieceTable.RightDoubleQuoteKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.ParseCharWithoutDecoding(TdxCharacters.RightDoubleQuote);
end;

class procedure TdxDestinationPieceTable.ParCharHandler(AImporter: TdxRichEditDocumentModelRtfImporter; var AChar: Char);
begin
  AImporter.FlushDecoder;
  AImporter.TableReader.OnEndParagraph;
  AImporter.InsertParagraph;
end;

class procedure TdxDestinationPieceTable.RightIndentKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  if ShouldApplyParagraphStyle(AImporter) then
    AImporter.Position.ParagraphFormattingInfo.RightIndent := AImporter.UnitConverter.TwipsToModelUnits(AParameterValue);
end;

class procedure TdxDestinationPieceTable.RightSingleQuoteKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.ParseCharWithoutDecoding(TdxCharacters.RightSingleQuote);
end;

class procedure TdxDestinationPieceTable.SectionLevelNumberingKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.Destination := TdxSkipDestination.Create(AImporter);
end;

class procedure TdxDestinationPieceTable.ShapeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.ShapePictureKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
end;

class procedure TdxDestinationPieceTable.StrikeoutKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
const
  AValueMap: array[Boolean] of TdxStrikeoutType = (TdxStrikeoutType.None, TdxStrikeoutType.Single);
begin
  AImporter.Position.CharacterFormatting.FontStrikeoutType := AValueMap[not AHasParameter or (AParameterValue > 0)];
end;

class procedure TdxDestinationPieceTable.SpacingAfterKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  if ShouldApplyParagraphStyle(AImporter) then
    AImporter.Position.ParagraphFormattingInfo.SpacingAfter := AImporter.UnitConverter.TwipsToModelUnits(AParameterValue);
end;

class procedure TdxDestinationPieceTable.SpacingBeforeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  if ShouldApplyParagraphStyle(AImporter) then
    AImporter.Position.ParagraphFormattingInfo.SpacingBefore := AImporter.UnitConverter.TwipsToModelUnits(AParameterValue);
end;

class procedure TdxDestinationPieceTable.SubscriptKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
const
  AValueMap: array[Boolean] of TdxCharacterFormattingScript = (TdxCharacterFormattingScript.Normal, TdxCharacterFormattingScript.Subscript);
begin
  AImporter.Position.CharacterFormatting.Script := AValueMap[not AHasParameter or (AParameterValue <> 0)];
end;

class procedure TdxDestinationPieceTable.SuperscriptKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
const
  AValueMap: array[Boolean] of TdxCharacterFormattingScript = (TdxCharacterFormattingScript.Normal, TdxCharacterFormattingScript.Superscript);
begin
  AImporter.Position.CharacterFormatting.Script := AValueMap[not AHasParameter or (AParameterValue <> 0)];
end;

class procedure TdxDestinationPieceTable.TabCenterKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  if ShouldApplyParagraphStyle(AImporter) then
    AImporter.Position.ParagraphFormattingInfo.TabAlignment := TdxTabAlignmentType.Center;
end;

class procedure TdxDestinationPieceTable.TabDecimalKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  if ShouldApplyParagraphStyle(AImporter) then
    AImporter.Position.ParagraphFormattingInfo.TabAlignment := TdxTabAlignmentType.Decimal;
end;

class procedure TdxDestinationPieceTable.TabKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  if AImporter.DocumentModel.DocumentCapabilities.TabSymbolAllowed then
    AImporter.ParseCharWithoutDecoding(TdxCharacters.TabMark)
  else
	  AImporter.ParseCharWithoutDecoding(' ');
end;

class procedure TdxDestinationPieceTable.TabLeaderDotsKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.TabLeaderEqualSignKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.TabLeaderHyphensKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.TabLeaderMiddleDotsKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.TabLeaderThickLineKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.TabLeaderUnderlineKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.TabPositionKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
var
  AInfo: TdxRtfParagraphFormattingInfo;
begin
  AInfo := AImporter.Position.ParagraphFormattingInfo;
  if AHasParameter then
    AInfo.Tabs.Add(TdxTabInfo.Create(AImporter.UnitConverter.TwipsToModelUnits(AParameterValue), AInfo.TabAlignment, AInfo.TabLeader));
  AInfo.TabAlignment := TdxTabAlignmentType.Left;
  AInfo.TabLeader := TdxTabLeaderType.None;
end;

class procedure TdxDestinationPieceTable.TabRightKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  if ShouldApplyParagraphStyle(AImporter) then
    AImporter.Position.ParagraphFormattingInfo.TabAlignment := TdxTabAlignmentType.Right;
end;

class procedure TdxDestinationPieceTable.UnderlineColorKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
var
  AProperties: TdxRtfDocumentProperties;
begin
  if not AHasParameter then
    AParameterValue := 0;
  AProperties := AImporter.DocumentProperties;
  AImporter.Position.CharacterFormatting.UnderlineColor := AProperties.Colors[AParameterValue];
end;

class procedure TdxDestinationPieceTable.UnderlineDashDotDottedKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  UnderlineKeywordHandleCore(AImporter, AParameterValue, AHasParameter, TdxUnderlineType.DashDotDotted);
end;

class procedure TdxDestinationPieceTable.UnderlineDashDottedKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  UnderlineKeywordHandleCore(AImporter, AParameterValue, AHasParameter, TdxUnderlineType.DashDotted);
end;

class procedure TdxDestinationPieceTable.UnderlineDashedKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  UnderlineKeywordHandleCore(AImporter, AParameterValue, AHasParameter, TdxUnderlineType.Dashed);
end;

class procedure TdxDestinationPieceTable.UnderlineDottedKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  UnderlineKeywordHandleCore(AImporter, AParameterValue, AHasParameter, TdxUnderlineType.Dotted);
end;

class procedure TdxDestinationPieceTable.UnderlineDoubleKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  UnderlineKeywordHandleCore(AImporter, AParameterValue, AHasParameter, TdxUnderlineType.Double);
end;

class procedure TdxDestinationPieceTable.UnderlineDoubleWaveKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  UnderlineKeywordHandleCore(AImporter, AParameterValue, AHasParameter, TdxUnderlineType.DoubleWave);
end;

class procedure TdxDestinationPieceTable.UnderlineHeavyWaveKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  UnderlineKeywordHandleCore(AImporter, AParameterValue, AHasParameter, TdxUnderlineType.HeavyWave);
end;

class procedure TdxDestinationPieceTable.UnderlineKeywordHandleCore(AImporter: TdxRichEditDocumentModelRtfImporter;
  AParameterValue: Integer; AHasParameter: Boolean; AType: TdxUnderlineType);
begin
  if AHasParameter and (AParameterValue = 0) then
    AType := TdxUnderlineType.None;
  AImporter.Position.CharacterFormatting.FontUnderlineType := AType;
end;

class procedure TdxDestinationPieceTable.UnderlineLongDashedKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  UnderlineKeywordHandleCore(AImporter, AParameterValue, AHasParameter, TdxUnderlineType.LongDashed);
end;

class procedure TdxDestinationPieceTable.UnderlineNoneKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  UnderlineKeywordHandleCore(AImporter, AParameterValue, AHasParameter, TdxUnderlineType.None);
end;

class procedure TdxDestinationPieceTable.UnderlineSingleKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  UnderlineKeywordHandleCore(AImporter, AParameterValue, AHasParameter, TdxUnderlineType.Single);
end;

class procedure TdxDestinationPieceTable.UnderlineThickDashDotDottedKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  UnderlineKeywordHandleCore(AImporter, AParameterValue, AHasParameter, TdxUnderlineType.ThickDashDotDotted);
end;

class procedure TdxDestinationPieceTable.UnderlineThickDashDottedKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  UnderlineKeywordHandleCore(AImporter, AParameterValue, AHasParameter, TdxUnderlineType.ThickDashDotted);
end;

class procedure TdxDestinationPieceTable.UnderlineThickDashedKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  UnderlineKeywordHandleCore(AImporter, AParameterValue, AHasParameter, TdxUnderlineType.ThickDashed);
end;

class procedure TdxDestinationPieceTable.UnderlineThickDottedKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  UnderlineKeywordHandleCore(AImporter, AParameterValue, AHasParameter, TdxUnderlineType.ThickDotted);
end;

class procedure TdxDestinationPieceTable.UnderlineThickLongDashedKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  UnderlineKeywordHandleCore(AImporter, AParameterValue, AHasParameter, TdxUnderlineType.ThickLongDashed);
end;

class procedure TdxDestinationPieceTable.UnderlineThickSingleKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  UnderlineKeywordHandleCore(AImporter, AParameterValue, AHasParameter, TdxUnderlineType.ThickSingle);
end;

class procedure TdxDestinationPieceTable.UnderlineWaveKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  UnderlineKeywordHandleCore(AImporter, AParameterValue, AHasParameter, TdxUnderlineType.Wave);
end;

class procedure TdxDestinationPieceTable.UnderlineWordsOnlyKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
var
  AValue: Boolean;
begin
  if AHasParameter then
    AValue := AParameterValue <> 0
  else
     AValue := True;
  if AValue then
    AImporter.Position.CharacterFormatting.FontUnderlineType := TdxUnderlineType.Single
  else
    AImporter.Position.CharacterFormatting.FontUnderlineType := TdxUnderlineType.None;
  AImporter.Position.CharacterFormatting.UnderlineWordsOnly := AValue;
end;

class procedure TdxDestinationPieceTable.UnicodeCountKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.Position.RtfFormattingInfo.UnicodeCharacterByteCount := AParameterValue;
end;

class procedure TdxDestinationPieceTable.WidowOrphanControlOffKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  if ShouldApplyParagraphStyle(AImporter) then
    AImporter.Position.ParagraphFormattingInfo.WidowOrphanControl := False;
end;

class procedure TdxDestinationPieceTable.WidowOrphanControlOnKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  if ShouldApplyParagraphStyle(AImporter) then
    AImporter.Position.ParagraphFormattingInfo.WidowOrphanControl := True;
end;

class procedure TdxDestinationPieceTable.InTableParagraphKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.Position.ParagraphFormattingInfo.InTableParagraph := True;
end;

class procedure TdxDestinationPieceTable.RowKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.TableReader.OnEndRow;
end;

class procedure TdxDestinationPieceTable.CellKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.TableReader.OnEndCell;
  AImporter.InsertParagraph;
end;

class procedure TdxDestinationPieceTable.NestedCellKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.TableReader.OnEndNestedCell;
  AImporter.InsertParagraph;
end;

class procedure TdxDestinationPieceTable.NestedRowKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.TableReader.OnEndNestedRow;
end;

class procedure TdxDestinationPieceTable.NestedTablePropertiesKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin

  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.NoNestedTablesKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.ItapKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  if not AHasParameter or (AParameterValue < 0) then
    Exit;
  AImporter.Position.ParagraphFormattingInfo.NestingLevel := AParameterValue;
end;

class procedure TdxDestinationPieceTable.TableRowDefaultsKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.TableReader.OnTableRowDefaults;
end;

class procedure TdxDestinationPieceTable.TableStyleKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.CellxKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.CellPreferredWidthKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.WidthUnitTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.FirstHorizontalMergedCellKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.NextHorizontalMergedCellKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.FirstVerticalMergedCellKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.NextVerticalMergedCellKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.CellFitTextKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.CellNoWrapKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.CellVerticalAlignmentTopKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.TableReader.CellProperties.VerticalAlignment := TdxVerticalAlignment.Top;
end;

class procedure TdxDestinationPieceTable.CellVerticalAlignmentCenterKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.TableReader.CellProperties.VerticalAlignment := TdxVerticalAlignment.Center;
end;

class procedure TdxDestinationPieceTable.CellVerticalAlignmentBottomKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.TableReader.CellProperties.VerticalAlignment := TdxVerticalAlignment.Bottom;
end;

class procedure TdxDestinationPieceTable.CellHideMarkKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.CellBottomCellMarginKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.CellLeftCellMarginKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.CellRightCellMarginKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.CellTopCellMarginKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.CellBottomCellMarginUnitTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.CellLeftCellMarginUnitTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.CellRightCellMarginUnitTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.CellTopCellMarginUnitTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.CellTextTopAlignmentKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.CellTextCenterVerticalAlignmentKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.CellTextBottomAlignmentKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.CellLeftToRightTopToBottomTextDirectionKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.CellTopToBottomRightToLeftTextDirectionKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.CellBottomToTopLeftToRightTextDirectionKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.CellLeftToRightTopToBottomVerticalTextDirectionKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.CellTopToBottomRightToLeftVerticalTextDirectionKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.CellBackgroundColorHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.NoTableBorderKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.BottomCellBorderKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.TableReader.ProcessedBorder := AImporter.TableReader.CellProperties.Borders.BottomBorder;
end;

class procedure TdxDestinationPieceTable.TopCellBorderKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.TableReader.ProcessedBorder := AImporter.TableReader.CellProperties.Borders.TopBorder;
end;

class procedure TdxDestinationPieceTable.LeftCellBorderKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.TableReader.ProcessedBorder := AImporter.TableReader.CellProperties.Borders.LeftBorder;
end;

class procedure TdxDestinationPieceTable.RightCellBorderKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.TableReader.ProcessedBorder := AImporter.TableReader.CellProperties.Borders.RightBorder;
end;

class procedure TdxDestinationPieceTable.UpperLeftToLowerRightBorderKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.TableReader.ProcessedBorder := AImporter.TableReader.CellProperties.Borders.TopLeftDiagonalBorder;
end;

class procedure TdxDestinationPieceTable.UpperRightToLowerLeftBorderKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.TableReader.ProcessedBorder := AImporter.TableReader.CellProperties.Borders.TopRightDiagonalBorder;
end;

class procedure TdxDestinationPieceTable.RowLeftKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.RowHeaderKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.RowHeightKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.RowKeepKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.TableRightAlignmentKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.TableLeftAlignmentKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.TableCenterAlignmentKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.WidthBeforeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.WidthBeforeUnitTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.TableReader.RowProperties.WidthBefore.&Type := GetWidthUnitType(AParameterValue);
end;

class procedure TdxDestinationPieceTable.WidthAfterKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.WidthAfterUnitTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.SpaceBetweenCellsKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.TableBottomCellMarginKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  if (AParameterValue < 0) or not AHasParameter then
    Exit;
  AImporter.TableReader.TableProperties.CellMargins.Bottom.Value := AParameterValue;
end;

class procedure TdxDestinationPieceTable.TableLeftCellMarginKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  if (AParameterValue < 0) or not AHasParameter then
    Exit;
  AImporter.TableReader.TableProperties.CellMargins.Left.Value := AParameterValue;
end;

class procedure TdxDestinationPieceTable.TableRightCellMarginKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  if (AParameterValue < 0) or not AHasParameter then
    Exit;
  AImporter.TableReader.TableProperties.CellMargins.Right.Value := AParameterValue;
end;

class procedure TdxDestinationPieceTable.TableTopCellMarginKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  if (AParameterValue < 0) or not AHasParameter then
    Exit;
  AImporter.TableReader.TableProperties.CellMargins.Top.Value := AParameterValue;
end;

class procedure TdxDestinationPieceTable.TableBottomCellMarginUnitTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  if not AHasParameter then
    Exit;
  AssignWidthUnitInfo(AImporter.TableReader.TableProperties.CellMargins.Bottom, AParameterValue);
end;

class procedure TdxDestinationPieceTable.TableLeftCellMarginUnitTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  if not AHasParameter then
    Exit;
  AssignWidthUnitInfo(AImporter.TableReader.TableProperties.CellMargins.Left, AParameterValue);
end;

class procedure TdxDestinationPieceTable.TableRightCellMarginUnitTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  if not AHasParameter then
    Exit;
  AssignWidthUnitInfo(AImporter.TableReader.TableProperties.CellMargins.Right, AParameterValue);
end;

class procedure TdxDestinationPieceTable.TableTopCellMarginUnitTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  if not AHasParameter then
    Exit;
  AssignWidthUnitInfo(AImporter.TableReader.TableProperties.CellMargins.Top, AParameterValue);
end;

class procedure TdxDestinationPieceTable.TableBottomCellSpacingKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.TableLeftCellSpacingKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.TableRightCellSpacingKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.TableTopCellSpacingKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.TableBottomCellSpacingUnitTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.TableLeftCellSpacingUnitTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.TableRightCellSpacingUnitTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.TableTopCellSpacingUnitTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.TablePreferredWidthKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.TablePreferredWidthUnitTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.TableIndentKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  if not AHasParameter then
    Exit;
  AImporter.TableReader.TableProperties.TableIndent.Value := AParameterValue;
end;

class procedure TdxDestinationPieceTable.TableIndentUnitTypeHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  if not AHasParameter then
    Exit;
  AImporter.TableReader.TableProperties.TableIndent.&Type := GetWidthUnitType(AParameterValue);
end;

class procedure TdxDestinationPieceTable.TableOverlapKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.TableLeftFromTextKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.TableRightFromTextKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.TableTopFromTextKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.TableBottomFromTextKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.ColHorizontalAnchorKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.MarginHorizontalAnchorKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.PageHorizontalAnchorKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.MarginVerticalAnchorKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.ParagraphVerticalAnchorKewordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.PageVerticalAnchorKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.TableHorizontalPositionKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.TableVerticalPositionKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.CenterTableHorizontalAlignKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.InsideTableHorizontalAlignKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.LeftTableHorizontalAlignKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.OutsideTableHorizontalAlignKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.RightTableHorizontalAlignKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.BottomTableVerticalAlignKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.CenterTableVerticalAlignKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.InlineTableVerticalAlignKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.InsideTableVerticalAlignKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.OutsideTableVerticalAlignKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.TopTableVerticalAlignKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.TableAutoFitKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.RowBandSizeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.ColumnBandSizeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.TopTableBorderKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.LeftTableBorderKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.BottomTableBorderKewordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.RightTableBorderKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.HorizontalTableBorderKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.VerticalTableBorderKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.ApplyFirstRowConditionalFormattingKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.ApplyLastRowConditionalFormattingKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.ApplyFirstColumnContitionalFormattingKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.ApplyLastColumnConditionalFormattingKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.DoNotApplyRowBandingConditionalFormattingKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.DoNotApplyColumnBandingConditionalFormattingKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.NoBorderKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.BorderWidthKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.BorderColorKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.FrameBorderKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.BorderSpaceKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.SingleThicknessBorderTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.DoubleThicknessBorderTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.ShadowedBorderTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.DoubleBorderTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.DottedBorderTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.DashedBorderTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.HairlineBorderTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.SmallDashedBorderTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.DotDashedBorderTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.DotDotDashedBorderTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.InsetBorderTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.NoneBorderTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.OutsetBorderTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.TripletBorderTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.SmallThickThinBorderTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.SmallThinThickBorderTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.SmallThinThickThinBorderTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.MediumThickThinBorderTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.MediumThinThickBorderTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.MediumThinThickThinBorderTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.LargeThickThinBorderTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.LargeThinThickBorderTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.LargeThinThickThinBorderTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.WavyBorderTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.DoubleWavyBorderTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.StripedBorderTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.EmbossedBorderTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.EngravedBorderTypeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

class procedure TdxDestinationPieceTable.BorderArtIndexHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  Assert(False, 'not implemented');
end;

{ TdxStringValueDestination }

procedure TdxStringValueDestination.PopulateControlCharTable(ATable: TdxControlCharTranslatorTable);
begin
  inherited PopulateControlCharTable(ATable);
  ATable.Add('''', SwitchToHexCharHandler);
  ATable.Add('\', EscapedCharHandler);
end;

procedure TdxStringValueDestination.PopulateKeywordTable(ATable: TdxKeywordTranslatorTable);
begin
  inherited PopulateKeywordTable(ATable);
  ATable.Add('u', UnicodeKeywordHandler);
end;

procedure TdxStringValueDestination.ProcessCharCore(AChar: Char);
begin
  FValue := FValue + AChar;
end;

function TdxStringValueDestination.GetValue: string;
var
  APos: Integer;
begin
  APos := Pos(';', FValue);
  if APos > 0 then
    Result := Copy(FValue, 1, APos - 1)
  else
    Result := FValue;
end;

end.
