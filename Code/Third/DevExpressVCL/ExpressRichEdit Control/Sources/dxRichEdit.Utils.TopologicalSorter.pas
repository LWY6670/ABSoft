{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Utils.TopologicalSorter;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  SysUtils, Generics.Defaults, Generics.Collections;

type
  TdxTopologicalSorterIsDependOnDelegate = function (ALeft, ARight: Integer): Boolean of object;

  TdxTopologicalSorter<T> = class
  private
    type
      PNode = ^TNode;
      TNode = record
      private
        FOneBasedSuccessorIndex: Integer;
        FNext: PNode;
        FIsValid: Boolean;
      public
        constructor Create(AOneBasedSuccessorIndex: Integer; ANext: PNode);
        property OneBasedSuccessorIndex: Integer read FOneBasedSuccessorIndex;
        property Next: PNode read FNext;
        property IsValid: Boolean read FIsValid;
      end;

      TDefaultDependencyCalculator = record
      private
        FSourceObjects: TList<T>;
        FComparer: IComparer<T>;
      public
        constructor Create(ASourceObjects: TList<T>; AComparer: IComparer<T>);

        function IsDependOn(Left, Right: Integer): Boolean;
      end;
  strict private
    FQLink: TArray<Integer>;
    FNodes: TArray<TNode>;
    FSuccess: Boolean;
  protected
    procedure AppendRelation(ASuccessorIndex, APredecessorIndex: Integer);
    procedure Initialize(N: Integer);
    procedure CalculateRelations(ASourceObjects: TList<T>; AIsDependOn: TdxTopologicalSorterIsDependOnDelegate);
    function CreateVirtualNoPredecessorsItemList: Integer;
    function ProcessNodes(ALastNoPredecessorItemIndex: Integer; ASourceObjects: TList<T>): TList<T>;
    function RemoveRelation(ANode: PNode; var ALastNoPredecessorItemIndex: Integer): PNode;

    function DoSort(ASourceObjects: TList<T>; AComparer: IComparer<T>): TList<T>; overload;
    function DoSort(ASourceObjects: TList<T>; AIsDependOn: TdxTopologicalSorterIsDependOnDelegate): TList<T>; overload;

    property Nodes: TArray<TNode> read FNodes;
    property QLink: TArray<Integer> read FQLink;
  public
    class function Sort(ASourceObjects: TList<T>; AComparer: IComparer<T>): TList<T>;

    property Success: Boolean read FSuccess;

  end;

implementation

{ TdxTopologicalSorter<T>.TNode }

constructor TdxTopologicalSorter<T>.TNode.Create(
  AOneBasedSuccessorIndex: Integer; ANext: PNode);
begin
  FOneBasedSuccessorIndex := AOneBasedSuccessorIndex;
  if ANext.IsValid then
    FNext := ANext
  else
    FNext := nil;
  FIsValid := True;
end;

{ TdxTopologicalSorter<T>.TDefaultDependencyCalculator }

constructor TdxTopologicalSorter<T>.TDefaultDependencyCalculator.Create(
  ASourceObjects: TList<T>; AComparer: IComparer<T>);
begin
  FSourceObjects := ASourceObjects;
  FComparer := AComparer;
end;

function TdxTopologicalSorter<T>.TDefaultDependencyCalculator.IsDependOn(Left,
  Right: Integer): Boolean;
begin
  Result := FComparer.Compare(FSourceObjects[Left], FSourceObjects[Right]) > 0;
end;

{ TdxTopologicalSorter<T> }

function TdxTopologicalSorter<T>.DoSort(ASourceObjects: TList<T>;
  AComparer: IComparer<T>): TList<T>;
var
  ADependencyCalculator: TDefaultDependencyCalculator;
  AInnerComparer: IComparer<T>;
begin
  if AComparer <> nil then
    AInnerComparer := AComparer
  else
    AInnerComparer := TComparer<T>.Default;
  ADependencyCalculator := TDefaultDependencyCalculator.Create(ASourceObjects, AInnerComparer);
  Result := DoSort(ASourceObjects, ADependencyCalculator.IsDependOn);
end;

procedure TdxTopologicalSorter<T>.AppendRelation(ASuccessorIndex,
  APredecessorIndex: Integer);
var
  AOneBasedPredecessorIndex: Integer;
  AOneBasedSuccessorIndex: Integer;
begin
  AOneBasedPredecessorIndex := APredecessorIndex + 1;
  AOneBasedSuccessorIndex := ASuccessorIndex + 1;
  QLink[AOneBasedSuccessorIndex] := QLink[AOneBasedSuccessorIndex] + 1;
  Nodes[AOneBasedPredecessorIndex] := TNode.Create(AOneBasedSuccessorIndex, @Nodes[AOneBasedPredecessorIndex]);
end;

procedure TdxTopologicalSorter<T>.CalculateRelations(ASourceObjects: TList<T>;
  AIsDependOn: TdxTopologicalSorterIsDependOnDelegate);
var
  N: Integer;
  Y, X: Integer;
begin
  N := ASourceObjects.Count;
  for Y := 0 to N - 1 do
  begin
    for X := 0 to N - 1 do
      if AIsDependOn(Y, X) then
        AppendRelation(Y, X);
  end;
end;

function TdxTopologicalSorter<T>.CreateVirtualNoPredecessorsItemList: Integer;
var
  N: Integer;
  I: Integer;
begin
  Result := 0;
  N := Length(QLink);
  for I := 1 to N - 1 do
  begin
    if QLink[I] = 0 then
    begin
      QLink[Result] := I;
      Result := I;
    end;
  end;
end;

procedure TdxTopologicalSorter<T>.Initialize(N: Integer);
begin
  SetLength(FQLink, N + 1);
  SetLength(FNodes, N + 1);
end;

function TdxTopologicalSorter<T>.ProcessNodes(
  ALastNoPredecessorItemIndex: Integer; ASourceObjects: TList<T>): TList<T>;
var
  N: Integer;
  AItemsLeft: Integer;
  F: Integer;
  ANode: PNode;
begin
  N := ASourceObjects.Count;
  AItemsLeft := N;

  F := QLink[0];
  Result := TList<T>.Create;
  while F > 0 do
  begin
    Result.Add(ASourceObjects[F - 1]);
    Dec(AItemsLeft);

    ANode := @Nodes[F];
    while (ANode <> nil) and ANode.IsValid do
      ANode := RemoveRelation(ANode, ALastNoPredecessorItemIndex);
    F := QLink[F];
  end;
  FSuccess := AItemsLeft = 0;
end;

function TdxTopologicalSorter<T>.RemoveRelation(ANode: PNode;
  var ALastNoPredecessorItemIndex: Integer): PNode;
var
  AIndex: Integer;
begin
  AIndex := ANode.OneBasedSuccessorIndex;
  Dec(QLink[AIndex]);

  if QLink[AIndex] = 0 then
  begin
    QLink[ALastNoPredecessorItemIndex] := AIndex;
    ALastNoPredecessorItemIndex := AIndex;
  end;
  Result := ANode.Next;
end;

class function TdxTopologicalSorter<T>.Sort(ASourceObjects: TList<T>;
  AComparer: IComparer<T>): TList<T>;
var
  ASorter: TdxTopologicalSorter<T>;
begin
  ASorter := TdxTopologicalSorter<T>.Create;
  try
    Result := ASorter.DoSort(ASourceObjects, AComparer);
  finally
    ASorter.Free;
  end;
end;

function TdxTopologicalSorter<T>.DoSort(ASourceObjects: TList<T>;
  AIsDependOn: TdxTopologicalSorterIsDependOnDelegate): TList<T>;
var
  N: Integer;
  I: Integer;
  ALastNoPredecessorItemIndex: Integer;
begin
  N := ASourceObjects.Count;
  if N < 2 then
  begin
    FSuccess := True;
    Result := TList<T>.Create;
    for I := 0 to N - 1 do
      Result.Add(ASourceObjects[I]);
  end
  else
  begin
    Initialize(N);
    CalculateRelations(Result, AIsDependOn);

    ALastNoPredecessorItemIndex := CreateVirtualNoPredecessorsItemList;
    Result := ProcessNodes(ALastNoPredecessorItemIndex, ASourceObjects);
    if Result.Count = 0 then
    begin
      for I := 0 to N - 1 do
        Result.Add(ASourceObjects[I]);
    end;
  end;
end;

end.
