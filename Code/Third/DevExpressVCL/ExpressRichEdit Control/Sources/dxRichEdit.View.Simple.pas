{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.View.Simple;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface


uses
  SysUtils, Types, Generics.Collections, Controls, SyncObjs, Classes, Graphics, ActiveX,
  cxGraphics, dxCoreClasses, cxControls,
  dxRichEdit.Utils.Types,
  dxRichEdit.Utils.BatchUpdateHelper,
  dxRichEdit.Utils.DataObject,
  dxRichEdit.Utils.BackgroundThreadUIUpdater,
  dxRichEdit.Utils.PredefinedFontSizeCollection,
  dxRichEdit.DocumentModel.PieceTable,
  dxRichEdit.DocumentModel.Selection,
  dxRichEdit.DocumentLayout,
  dxRichEdit.DocumentLayout.Position,
  dxRichEdit.LayoutEngine.Formatter,
  dxRichEdit.LayoutEngine.DocumentFormatter,
  dxRichEdit.Control.HitTest,
  dxRichEdit.Control.HotZones,
  dxRichEdit.View.Core,
  dxRichEdit.View.ViewInfo,
  dxRichEdit.View.PageViewInfoGenerator,
  dxRichEdit.Platform.Win.Painter;

type
  TdxSimpleView = class;
  TdxSimpleViewPageController = class;
  TdxSimpleViewDocumentFormattingController = class;

  { TdxSimpleViewColumnsBoundsCalculator }

  TdxSimpleViewColumnsBoundsCalculator = class(TdxColumnsBoundsCalculator)
  protected
    procedure PopulateColumnsBounds(const AResult: TList<TRect>; const ABounds: TRect; const AColumnInfoCollection: TdxColumnInfoCollection); override;
    procedure PopulateEqualWidthColumnsBounds(const AResult: TList<TRect>; const ABounds: TRect; AColumnCount, ASpaceBetweenColumns: Integer); override;
  end;

  { TdxSimpleViewColumnController }

  TdxSimpleViewColumnController = class(TdxColumnController)
  protected
    function GetShouldZeroSpacingBeforeWhenMoveRowToNextColumn: Boolean; override;
  public
    function CalculateColumnBoundsCore(AColumnIndex: Integer): TRect; override;
    function CreateColumnBoundsCalculator: TdxColumnsBoundsCalculator; override;
  end;

  { TdxSimplePageBoundsCalculator }

  TdxSimplePageBoundsCalculator = class(TdxPageBoundsCalculator)
  private
    FController: TdxSimpleViewPageController;
  protected
    function CalculatePageBounds(ASection: TdxSection): TRect; override;
    function CalculatePageClientBoundsCore(APageWidth, APageHeight, AMarginLeft, AMarginTop, AMarginRight, AMarginBottom: Integer): TRect; override; 
  public
    constructor Create(AController: TdxSimpleViewPageController);
  end;

  { TdxSimpleViewCurrentHorizontalPositionController }

  TdxSimpleViewCurrentHorizontalPositionController = class(TdxCurrentHorizontalPositionController)
  protected
    function CanFitBoxToCurrentRow(const ABoxSize: TSize): Boolean; override;
  end;

  { TdxSimpleViewBoxHitTestCalculator }

  TdxSimpleViewBoxHitTestCalculator = class(TdxBoxHitTestCalculator)
  public
    procedure ProcessColumnCollection(ACollection: TdxColumnCollection); override;
  end;

  { TdxSimpleViewPageController }

  TdxSimpleViewPageController = class(TdxNonPrintViewPageControllerBase)
  strict private
    FPageSize: TSize;
    FMinPageWidth: Integer;
  protected
    function GetMaxWidth(AAreas: TdxPageAreaCollection): Integer; overload; virtual;
    function GetMaxWidth(ARows: TdxRowCollection): Integer; overload; virtual;

    property PageSize: TSize read FPageSize;
  public
    constructor Create(ADocumentLayout: TdxDocumentLayout);
    function CreatePageBoundsCalculator: TdxPageBoundsCalculator; override;
    function CreateHitTestCalculator(ARequest: TdxRichEditHitTestRequest; AResult: TdxRichEditHitTestResult): TdxBoxHitTestCalculator; override;
    procedure FinalizePagePrimaryFormatting(APage: TdxPage; ASkipAddingFloatingObjects: Boolean); override;

    property MinPageWidth: Integer read FMinPageWidth write FMinPageWidth;
    property VirtualPageHeight: Integer read FPageSize.cy write FPageSize.cy;
    property VirtualPageWidth: Integer read FPageSize.cx write FPageSize.cx;
  end;

  { TdxSimpleViewRowsControllerBase }

  TdxSimpleViewRowsControllerBase = class abstract (TdxRowsController)
  protected
  public
  end;

  { TdxSimpleViewRowsController }

  TdxSimpleViewRowsController = class(TdxSimpleViewRowsControllerBase)
  strict private
    FController: TdxSimpleViewDocumentFormattingController;
  strict protected
    function CreateCurrentHorizontalPosition: TdxCurrentHorizontalPositionController; overload; override;
    function CreateCurrentHorizontalPosition(APosition: Integer): TdxCurrentHorizontalPositionController; overload; override;
  public
    constructor Create(AController: TdxSimpleViewDocumentFormattingController; APieceTable: TdxPieceTable;
      AColumnController: TdxColumnController; AMatchHorizontalTableIndentsToTextEdge: Boolean);
    property Controller: TdxSimpleViewDocumentFormattingController read FController;
  end;

  { TdxMaxWidthCalculator }

  TdxMaxWidthCalculator = class
  strict private
    function GetLastNonLineBreakBox(ARow: TdxRow): TdxBox;
  public
    function GetMaxWidth(APage: TdxPage): Integer; overload;
    function GetMaxWidth(AAreas: TdxPageAreaCollection): Integer; overload;
    function GetMaxWidth(ARows: TdxRowCollection): Integer; overload;
  end;

  { TdxSimpleViewPageViewInfoGenerator }

  TdxSimpleViewPageViewInfoGenerator = class(TdxPageViewInfoGenerator)
  strict private
    function GetView: TdxSimpleView;
  protected
    function GetHorizontalPageGap: Integer; override;
    function GetVerticalPageGap: Integer; override;
    procedure SetHorizontalPageGap(Value: Integer); override;
    procedure SetVerticalPageGap(Value: Integer); override;
    function CreateEmptyClone: TdxPageGeneratorLayoutManager; override;
  public
    procedure Reset(AStrategy: TdxPageGenerationStrategyType); override;
    function ProcessPage(APage: TdxPage; APageIndex: Integer): TdxProcessPageResult; override;
    function CalculatePageLogicalTotalWidth(APage: TdxPage): Integer; override;
    function CanFitPageToPageRow(APage: TdxPage; ARow: TdxPageViewInfoRow): Boolean; override;
    procedure CalculateWidthParameters; override;

    property View: TdxSimpleView read GetView;
  end;

  { TdxSimpleViewDocumentFormattingController }

  TdxSimpleViewDocumentFormattingController = class(TdxDocumentFormattingController)
  strict private
    FView: TdxSimpleView;
  public
    constructor Create(AView: TdxSimpleView; ADocumentLayout: TdxDocumentLayout; APieceTable: TdxPieceTable);
    function CreatePageController: TdxPageController; override;
    function CreateColumnController: TdxColumnController; override;
    function CreateRowController: TdxRowsController; override;

    property View: TdxSimpleView read FView;
  end;

  { TdxSimpleView }

  TdxSimpleView = class(TdxRichEditView)
  strict private const
    DefaultPadding: TRect = (Left: 15; Top: 4; Right: 4; Bottom: 0);
  strict private
    FHidePartiallyVisibleRow: Boolean;
    FWordWrap: Boolean;
    FInternalWordWrap: Boolean;
    procedure ControlResizeDelegate;
    procedure SetInternalWordWrap(Value: Boolean);
    procedure SetWordWrap(Value: Boolean);
  protected
    function CreatePadding: TdxRichEditControlPadding; override;
    function ShouldUpdatePageWidth: Boolean; virtual;
    procedure UpdatePageWidthCore(AController: TdxDocumentFormattingController); virtual;
    procedure UpdatePageWidth; virtual;
    procedure ResetPadding; virtual;
    function GetActualPadding: TRect; override;

    property HidePartiallyVisibleRow: Boolean read FHidePartiallyVisibleRow write FHidePartiallyVisibleRow; 
    property InternalWordWrap: Boolean read FInternalWordWrap write SetInternalWordWrap;
    property WordWrap: Boolean read FWordWrap write SetWordWrap default True; 
  public
    constructor Create(AControl: IdxRichEditControl); override;
    procedure Assign(Source: TPersistent); override;

    function CalcBestSize(AFixedWidth: Boolean): TSize; override;
    function CalculatePageContentClipBounds(APage: TdxPageViewInfo): TRect; override;
    function CreateDocumentFormattingController: TdxDocumentFormattingController; override;
    function CreatePageViewInfoGenerator: TdxPageViewInfoGenerator; override;
    procedure EnsureCaretVisibleOnResize; override;
    procedure OnResizeCore; override;
    procedure OnZoomFactorChangedCore; override;
    procedure PerformZoomFactorChanged; override;
    function CreateDocumentLayoutExporter(APainter: TdxPainter; AAdapter: TdxGraphicsDocumentLayoutExporterAdapter;
      APageViewInfo: TdxPageViewInfo; ABounds: TRect): TdxDocumentLayoutExporter; override;
    procedure Visit(AVisitor: IdxRichEditViewVisitor); override;
    function GetPageViewInfoRowFromPoint(const APoint: TPoint; AStrictSearch: Boolean): TdxPageViewInfoRow; override;
    function DefaultHitTestPageAccuracy: TdxHitTestAccuracy; override;
    function PerformStrictPageViewInfoHitTest(APageViewInfo: TdxPageViewInfo; const APt: TPoint): Boolean; override;
    procedure UpdateHorizontalScrollbar; override;
    procedure OnResize(const ABounds: TRect; AEnsureCaretVisibleOnResize: Boolean); override;
  published
    property BackColor;
    property Padding;
  end;

implementation

uses
  Math, dxCore, dxTypeHelpers, cxGeometry, dxRichEdit.DocumentModel.Core;

{ TdxSimpleViewColumnsBoundsCalculator }

procedure TdxSimpleViewColumnsBoundsCalculator.PopulateColumnsBounds(const AResult: TList<TRect>; const ABounds: TRect; const AColumnInfoCollection: TdxColumnInfoCollection);
begin
  AResult.Add(ABounds);
end;

procedure TdxSimpleViewColumnsBoundsCalculator.PopulateEqualWidthColumnsBounds(const AResult: TList<TRect>;
  const ABounds: TRect; AColumnCount, ASpaceBetweenColumns: Integer);
begin
  AResult.Add(ABounds);
end;

{ TdxSimpleViewRowsController }

constructor TdxSimpleViewRowsController.Create(AController: TdxSimpleViewDocumentFormattingController; APieceTable: TdxPieceTable;
  AColumnController: TdxColumnController; AMatchHorizontalTableIndentsToTextEdge: Boolean);
begin
  inherited Create(APieceTable, AColumnController, AMatchHorizontalTableIndentsToTextEdge);
  FController := AController;
end;

function TdxSimpleViewRowsController.CreateCurrentHorizontalPosition: TdxCurrentHorizontalPositionController;
begin
  Result := TdxSimpleViewCurrentHorizontalPositionController.Create(Self);
end;

function TdxSimpleViewRowsController.CreateCurrentHorizontalPosition(APosition: Integer): TdxCurrentHorizontalPositionController;
begin
  Result := TdxSimpleViewCurrentHorizontalPositionController.Create(Self, APosition);
end;

{ TdxMaxWidthCalculator }

function TdxMaxWidthCalculator.GetMaxWidth(APage: TdxPage): Integer;
var
  I: Integer;
begin
  Result := GetMaxWidth(APage.Areas);
  if APage.FloatingObjects <> nil then
    for I := 0 to APage.FloatingObjects.Count - 1 do
      Result := Max(APage.FloatingObjects[I].Bounds.Right, Result);
end;

function TdxMaxWidthCalculator.GetMaxWidth(AAreas: TdxPageAreaCollection): Integer;
var
  ARows: TdxRowCollection;
  I: Integer;
begin
  Result := MinInt;
  for I := 0 to AAreas.Count - 1 do
  begin
    ARows := AAreas[I].Columns.First.Rows;
    Result := Max(Result, GetMaxWidth(ARows));
  end;
end;

function TdxMaxWidthCalculator.GetMaxWidth(ARows: TdxRowCollection): Integer;
var
  ALastBox: TdxBox;
  I: Integer;
begin
  Result := 0;
  for I := 0 to ARows.Count - 1 do
  begin
    ALastBox := GetLastNonLineBreakBox(ARows[I]);
    if ALastBox <> nil then
      Result := Max(Result, ALastBox.Bounds.Right);
  end;
end;

function TdxMaxWidthCalculator.GetLastNonLineBreakBox(ARow: TdxRow): TdxBox;
var
  I: Integer;
begin
  for I := ARow.Boxes.Count - 1 downto 0 do
    if not ARow.Boxes[I].IsLineBreak then
      Exit(ARow.Boxes[I]);
  Result := nil;
end;

{ TdxSimpleViewPageViewInfoGenerator }

function TdxSimpleViewPageViewInfoGenerator.GetHorizontalPageGap: Integer;
begin
  Result := 0;
end;

function TdxSimpleViewPageViewInfoGenerator.GetVerticalPageGap: Integer;
begin
  Result := 0;
end;

function TdxSimpleViewPageViewInfoGenerator.GetView: TdxSimpleView;
begin
  Result := TdxSimpleView(FView);
end;

procedure TdxSimpleViewPageViewInfoGenerator.SetHorizontalPageGap(Value: Integer);
begin
end;

procedure TdxSimpleViewPageViewInfoGenerator.SetVerticalPageGap(Value: Integer);
begin
end;

function TdxSimpleViewPageViewInfoGenerator.CanFitPageToPageRow(APage: TdxPage; ARow: TdxPageViewInfoRow): Boolean;
begin
  Result := False;
end;

function TdxSimpleViewPageViewInfoGenerator.CreateEmptyClone: TdxPageGeneratorLayoutManager;
begin
  Result := TdxSimpleViewPageViewInfoGenerator.Create(FView, FViewInfo);
end;

procedure TdxSimpleViewPageViewInfoGenerator.Reset(AStrategy: TdxPageGenerationStrategyType);
begin
  TotalWidth := MinInt64;
  VisibleWidth := MinInt64;
  inherited Reset(AStrategy);
end;

procedure TdxSimpleViewPageViewInfoGenerator.CalculateWidthParameters;
var
  AActualTotalWidth: Int64;
begin
  if TotalWidth = MinInt64 then
    TotalWidth := 100;
  if VisibleWidth = MinInt64 then
    VisibleWidth := 100;
  AActualTotalWidth := TotalWidth;
  inherited CalculateWidthParameters;
  TotalWidth := AActualTotalWidth;
end;

function TdxSimpleViewPageViewInfoGenerator.ProcessPage(APage: TdxPage; APageIndex: Integer): TdxProcessPageResult;
begin
  Result := inherited ProcessPage(APage, APageIndex);
  TotalWidth := Max(TotalWidth, CalculatePageLogicalTotalWidth(APage));
end;

function TdxSimpleViewPageViewInfoGenerator.CalculatePageLogicalTotalWidth(APage: TdxPage): Integer;
var
  AMaxWidth: Integer;
  AMaxWidthCalculator: TdxMaxWidthCalculator;
begin
  Result := inherited CalculatePageLogicalTotalWidth(APage);
  if View.InternalWordWrap then
    Exit;
  AMaxWidthCalculator := TdxMaxWidthCalculator.Create;
  try
    AMaxWidth := AMaxWidthCalculator.GetMaxWidth(APage);
  finally
    AMaxWidthCalculator.Free;
  end;
  Result := Max(Result, AMaxWidth);
end;

{ TdxSimpleViewDocumentFormattingController }

constructor TdxSimpleViewDocumentFormattingController.Create(AView: TdxSimpleView;
  ADocumentLayout: TdxDocumentLayout; APieceTable: TdxPieceTable);
begin
  inherited Create(ADocumentLayout, APieceTable, nil);
  FView := AView;
end;

function TdxSimpleViewDocumentFormattingController.CreateColumnController: TdxColumnController;
begin
  Result := TdxSimpleViewColumnController.Create(PageAreaController);
end;

function TdxSimpleViewDocumentFormattingController.CreatePageController: TdxPageController;
begin
  Result := TdxSimpleViewPageController.Create(DocumentLayout);
end;

function TdxSimpleViewDocumentFormattingController.CreateRowController: TdxRowsController;
begin
  Result := TdxSimpleViewRowsController.Create(Self, PieceTable, ColumnController, False); 
end;

{ TdxSimpleViewColumnController }

function TdxSimpleViewColumnController.CalculateColumnBoundsCore(AColumnIndex: Integer): TRect;
begin
  Result := ColumnsBounds[0];
end;

function TdxSimpleViewColumnController.CreateColumnBoundsCalculator: TdxColumnsBoundsCalculator;
begin
  Result := TdxSimpleViewColumnsBoundsCalculator.Create(PageAreaController.PageController.DocumentLayout.DocumentModel.ToDocumentLayoutUnitConverter);
end;

function TdxSimpleViewColumnController.GetShouldZeroSpacingBeforeWhenMoveRowToNextColumn: Boolean;
begin
  Result := False;
end;

{ TdxSimpleViewPageController }

constructor TdxSimpleViewPageController.Create(ADocumentLayout: TdxDocumentLayout);
begin
  inherited Create(ADocumentLayout);
  VirtualPageHeight := ADocumentLayout.UnitConverter.DocumentsToLayoutUnits(1200); // 4 inch
  VirtualPageWidth := ADocumentLayout.UnitConverter.DocumentsToLayoutUnits(900); // 3 inch
  FMinPageWidth := VirtualPageWidth;
end;

function TdxSimpleViewPageController.CreatePageBoundsCalculator: TdxPageBoundsCalculator;
begin
  Result := TdxSimplePageBoundsCalculator.Create(Self);
end;

function TdxSimpleViewPageController.CreateHitTestCalculator(ARequest: TdxRichEditHitTestRequest;
  AResult: TdxRichEditHitTestResult): TdxBoxHitTestCalculator;
begin
  Result := TdxSimpleViewBoxHitTestCalculator.Create(ARequest, AResult);
end;

procedure TdxSimpleViewPageController.FinalizePagePrimaryFormatting(APage: TdxPage;
  ASkipAddingFloatingObjects: Boolean);
var
  ALastPageArea: TdxPageArea;
  AColumn: TdxColumn;
  APageBounds, APageAreaBounds, AColumnBounds: TRect;
  AHeight: Integer;
begin
  ALastPageArea := APage.Areas.Last;
  AColumn := ALastPageArea.Columns.First;
  APageBounds := APage.Bounds;
  APageAreaBounds := ALastPageArea.Bounds;
  AColumnBounds := AColumn.Bounds;

  AHeight := GetColumnBottom(AColumn);
  APageBounds.Height := AHeight;
  APageAreaBounds.Height := Max(APageAreaBounds.Height, AHeight);
  AColumnBounds.Height := Max(AColumnBounds.Height, AHeight);

  APage.Bounds := APageBounds;
  APage.ClientBounds := TRect.CreateSize(0, 0, APageBounds.Width, APageBounds.Height); 
  ALastPageArea.Bounds := APageAreaBounds;
  AColumn.Bounds := AColumnBounds;
  inherited FinalizePagePrimaryFormatting(APage, ASkipAddingFloatingObjects);
end;

function TdxSimpleViewPageController.GetMaxWidth(AAreas: TdxPageAreaCollection): Integer;
var
  I: Integer;
  ARows: TdxRowCollection;
begin
  Result := MinInt;
  for I := 0 to AAreas.Count - 1 do
  begin
    ARows := AAreas[I].Columns.First.Rows;
    Result := Max(Result, GetMaxWidth(ARows));
  end;
end;

function TdxSimpleViewPageController.GetMaxWidth(ARows: TdxRowCollection): Integer;
var
  I: Integer;
  ALastBox: TdxBox;
begin
  Result := MinInt;
  for I := 0 to ARows.Count - 1 do
  begin
    ALastBox := ARows[I].Boxes.Last;
    if ALastBox <> nil then
      Result := Max(Result, ALastBox.Bounds.Right);
  end;
end;


{ TdxSimplePageBoundsCalculator }

constructor TdxSimplePageBoundsCalculator.Create(AController: TdxSimpleViewPageController);
begin
  inherited Create(AController.DocumentLayout.DocumentModel.ToDocumentLayoutUnitConverter);
  FController := AController;
end;

function TdxSimplePageBoundsCalculator.CalculatePageBounds(ASection: TdxSection): TRect;
begin
  Result := CalculatePageClientBounds(ASection);
end;

function TdxSimplePageBoundsCalculator.CalculatePageClientBoundsCore(APageWidth, APageHeight, AMarginLeft, AMarginTop,
  AMarginRight, AMarginBottom: Integer): TRect;
begin
  Result.InitSize(cxNullPoint, FController.PageSize.cx, FController.PageSize.cy);
end;

{ TdxSimpleViewCurrentHorizontalPositionController }

function TdxSimpleViewCurrentHorizontalPositionController.CanFitBoxToCurrentRow(const ABoxSize: TSize): Boolean;
begin
  if TdxSimpleViewRowsController(RowsController).Controller.View.InternalWordWrap then
    Result := inherited CanFitBoxToCurrentRow(ABoxSize)
  else
    Result := True;
end;

{ TdxSimpleViewBoxHitTestCalculator }

procedure TdxSimpleViewBoxHitTestCalculator.ProcessColumnCollection(ACollection: TdxColumnCollection);
var
  AStrictHitTest: Boolean;
begin
  AStrictHitTest := (HitTestRequest.Accuracy and ExactColumn) <> 0;
  FastHitTestAssumingArrangedVertically(ACollection, AStrictHitTest);
end;

{ TdxSimpleView }

constructor TdxSimpleView.Create(AControl: IdxRichEditControl);
begin
  inherited Create(AControl);
  FWordWrap := True;
  FInternalWordWrap := True;
end;

procedure TdxSimpleView.Assign(Source: TPersistent);
begin
  if Source is TdxSimpleView then
  begin
    Padding := TdxSimpleView(Source).Padding;
    WordWrap := TdxSimpleView(Source).WordWrap;
  end;
end;

function TdxSimpleView.CalcBestSize(AFixedWidth: Boolean): TSize;
var
  AOldInternalWordWrap: Boolean;
  AMaxWidthCalculator: TdxMaxWidthCalculator;
  I, ACount, AWidth, AHeight: Integer;
begin
  Control.BeginUpdate;
  try
    AOldInternalWordWrap := InternalWordWrap;
    if not AFixedWidth then
      InternalWordWrap := False;
    BeginDocumentRendering;

    AMaxWidthCalculator := TdxMaxWidthCalculator.Create;
    try
      AWidth := AMaxWidthCalculator.GetMaxWidth(DocumentLayout.Pages[0]);
      AHeight := DocumentLayout.Pages[0].Bounds.Height;
      ACount := DocumentLayout.Pages.Count;
      for I := 1 to ACount - 1 do
      begin
        AWidth := Max(AMaxWidthCalculator.GetMaxWidth(DocumentLayout.Pages[I]), AWidth);
        Inc(AHeight, DocumentLayout.Pages[I].Bounds.Height);
      end;

      EndDocumentRendering;
      InternalWordWrap := AOldInternalWordWrap;
    finally
      AMaxWidthCalculator.Free;
    end;
  finally
    Control.EndUpdate;
  end;
  Result := DocumentLayout.UnitConverter.LayoutUnitsToPixels(TSize.Create(AWidth, AHeight),
    DocumentModel.DpiX, DocumentModel.DpiY);
end;

function TdxSimpleView.CalculatePageContentClipBounds(APage: TdxPageViewInfo): TRect;
begin
  Result := inherited CalculatePageContentClipBounds(APage);
  Result.Left := Min(Result.Left, 0);
  Result.Width := MaxInt div 2;
end;

function TdxSimpleView.CreateDocumentFormattingController: TdxDocumentFormattingController;
begin
  Result := TdxSimpleViewDocumentFormattingController.Create(Self, DocumentLayout, DocumentModel.MainPieceTable);
  UpdatePageWidthCore(Result);
end;

function TdxSimpleView.CreatePageViewInfoGenerator: TdxPageViewInfoGenerator;
begin
  Result := TdxSimpleViewPageViewInfoGenerator.Create(Self, ViewInfo);
end;

function TdxSimpleView.GetActualPadding: TRect;
begin
  Result := Padding.Value;
end;

function TdxSimpleView.GetPageViewInfoRowFromPoint(const APoint: TPoint; AStrictSearch: Boolean): TdxPageViewInfoRow;
begin
  Result := inherited GetPageViewInfoRowFromPoint(APoint, False);
end;

procedure TdxSimpleView.OnResize(const ABounds: TRect; AEnsureCaretVisibleOnResize: Boolean);
begin
  inherited OnResize(ABounds, AEnsureCaretVisibleOnResize);
  if AEnsureCaretVisibleOnResize then
    EnsureCaretVisible;
end;

procedure TdxSimpleView.OnResizeCore;
begin
  inherited OnResizeCore;
  if ShouldUpdatePageWidth then
    UpdatePageWidth;
end;

procedure TdxSimpleView.OnZoomFactorChangedCore;
begin
  inherited OnZoomFactorChangedCore;
  UpdatePageWidth;
end;

function TdxSimpleView.PerformStrictPageViewInfoHitTest(APageViewInfo: TdxPageViewInfo; const APt: TPoint): Boolean;
var
  ABounds: TRect;
begin
  ABounds := APageViewInfo.ClientBounds;
  Result := (ABounds.Left <= APt.X) and (APt.X <= ABounds.Right);
end;

procedure TdxSimpleView.PerformZoomFactorChanged;
begin
  inherited PerformZoomFactorChanged;
  EnsureCaretVisible;
end;

function TdxSimpleView.CreateDocumentLayoutExporter(APainter: TdxPainter; AAdapter: TdxGraphicsDocumentLayoutExporterAdapter;
  APageViewInfo: TdxPageViewInfo; ABounds: TRect): TdxDocumentLayoutExporter;
var
  AExporter: TdxScreenOptimizedGraphicsDocumentLayoutExporter;
begin
  if AllowDisplayLineNumbers then
    AExporter := TdxScreenOptimizedGraphicsDocumentLayoutExporter.Create(DocumentModel, APainter, AAdapter, ABounds)
  else
    AExporter := TdxScreenOptimizedGraphicsDocumentLayoutExporterNoLineNumbers.Create(DocumentModel, APainter, AAdapter, ABounds);
  AExporter.VisibleBounds := CalculateVisiblePageBounds(ABounds, APageViewInfo);
  AExporter.HidePartiallyVisibleRow := HidePartiallyVisibleRow;
  Result := AExporter;
end;

procedure TdxSimpleView.ResetPadding;
begin
  Padding.Reset;
end;

function TdxSimpleView.DefaultHitTestPageAccuracy: TdxHitTestAccuracy;
begin
  Result := NearestPage;
end;

function TdxSimpleView.CreatePadding: TdxRichEditControlPadding;
begin
  Result := TdxRichEditControlPadding.Create(Self, DefaultPadding);
end;

procedure TdxSimpleView.EnsureCaretVisibleOnResize;
begin
  if (Bounds.Height > 0) and (Bounds.Width > 0) then
    EnsureCaretVisible();
end;

function TdxSimpleView.ShouldUpdatePageWidth: Boolean;
var
  APageController: TdxSimpleViewPageController;
begin
  APageController := FormattingController.PageController as TdxSimpleViewPageController;
  Result := APageController.VirtualPageWidth <> Math.Max(DocumentLayout.UnitConverter.DocumentsToLayoutUnits(4), Trunc(Bounds.Width / ZoomFactor));
end;

procedure TdxSimpleView.UpdateHorizontalScrollbar;

var
  APrevVisibility: Boolean;
begin
  APrevVisibility := HorizontalScrollController.IsScrollPossible;
  inherited UpdateHorizontalScrollbar;
  if APrevVisibility <> HorizontalScrollController.IsScrollPossible then
  begin
    Control.UpdateUIFromBackgroundThread(ControlResizeDelegate);
  end;
end;

procedure TdxSimpleView.ControlResizeDelegate;
begin
  Control.OnResizeCore;
end;

procedure TdxSimpleView.UpdatePageWidth;
var
  APieceTable: TdxPieceTable;
  AChangeActions: TdxDocumentModelChangeActions;
begin
  DocumentModel.BeginUpdate;
  try
    UpdatePageWidthCore(FormattingController);
    AChangeActions := [TdxDocumentModelChangeAction.ResetAllPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSelectionLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout,
      TdxDocumentModelChangeAction.ResetSpellingCheck,
      TdxDocumentModelChangeAction.ForceResetHorizontalRuler,
      TdxDocumentModelChangeAction.ForceResetVerticalRuler];
    APieceTable := DocumentModel.ActivePieceTable;
    APieceTable.ApplyChangesCore(AChangeActions, 0, MaxInt); 
  finally
    DocumentModel.EndUpdate;
  end;
end;

procedure TdxSimpleView.UpdatePageWidthCore(AController: TdxDocumentFormattingController);
var
  APageController: TdxSimpleViewPageController;
begin
  APageController := AController.PageController as TdxSimpleViewPageController;
  APageController.VirtualPageWidth := Math.Max(DocumentLayout.UnitConverter.DocumentsToLayoutUnits(4), Trunc(Bounds.Width / ZoomFactor));
  AController.Reset(False);
end;

procedure TdxSimpleView.Visit(AVisitor: IdxRichEditViewVisitor);
begin
  AVisitor.Visit(Self);
end;

procedure TdxSimpleView.SetInternalWordWrap(Value: Boolean);
var
  AChangeActions: TdxDocumentModelChangeActions;
begin
  DocumentModel.BeginUpdate;
  try
    FInternalWordWrap := Value;
    AChangeActions :=
      [TdxDocumentModelChangeAction.ResetAllPrimaryLayout,
       TdxDocumentModelChangeAction.ResetSelectionLayout,
       TdxDocumentModelChangeAction.ResetSecondaryLayout,
       TdxDocumentModelChangeAction.ResetSpellingCheck,
       TdxDocumentModelChangeAction.ForceResetHorizontalRuler,
       TdxDocumentModelChangeAction.ForceResetVerticalRuler,
       TdxDocumentModelChangeAction.Redraw];
    DocumentModel.ActivePieceTable.ApplyChangesCore(AChangeActions, 0, MaxInt); 
  finally
    DocumentModel.EndUpdate;
  end;
end;

procedure TdxSimpleView.SetWordWrap(Value: Boolean);
begin
  FWordWrap := Value;
  InternalWordWrap := Value;
end;


end.
