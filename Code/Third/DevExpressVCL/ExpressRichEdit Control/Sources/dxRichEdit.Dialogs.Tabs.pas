{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Dialogs.Tabs;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, Dialogs, Menus,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxLayoutContainer, dxLayoutControl, dxLayoutControlAdapters, dxLayoutcxEditAdapters, cxContainer, cxEdit,
  cxLabel, cxMaskEdit, cxSpinEdit, cxListBox, cxTextEdit, StdCtrls, cxButtons, cxGroupBox, cxRadioGroup,
  dxRichEdit.DocumentModel.TabFormatting, dxRichEdit.Dialog.CustomDialog, dxLayoutLookAndFeels, cxClasses,
  dxRichEdit.LayoutEngine.Formatter, dxMeasurementUnitEdit, Generics.Collections;

const
  MinimumAllowableTabStopPostionDistance = 4;

type
  TdxRichEditTabsDialogForm = class(TdxRichEditCustomDialogForm)
    btnCancel: TcxButton;
    btnClear: TcxButton;
    btnClearAll: TcxButton;
    btnOk: TcxButton;
    btnSet: TcxButton;
    dxLayoutControl1: TdxLayoutControl;
    dxLayoutControl1Group_Root: TdxLayoutGroup;
    dxLayoutControl1Group1: TdxLayoutAutoCreatedGroup;
    dxLayoutControl1Group10: TdxLayoutAutoCreatedGroup;
    dxLayoutControl1Group2: TdxLayoutGroup;
    dxLayoutControl1Group3: TdxLayoutGroup;
    dxLayoutControl1Group4: TdxLayoutGroup;
    dxLayoutControl1Group5: TdxLayoutAutoCreatedGroup;
    dxLayoutControl1Group6: TdxLayoutGroup;
    dxLayoutControl1Group7: TdxLayoutAutoCreatedGroup;
    dxLayoutControl1Group8: TdxLayoutAutoCreatedGroup;
    dxLayoutControl1Group9: TdxLayoutGroup;
    dxLayoutControl1Item1: TdxLayoutItem;
    dxLayoutControl1Item10: TdxLayoutItem;
    dxLayoutControl1Item12: TdxLayoutItem;
    dxLayoutControl1Item13: TdxLayoutItem;
    dxLayoutControl1Item14: TdxLayoutItem;
    dxLayoutControl1Item15: TdxLayoutItem;
    dxLayoutControl1Item16: TdxLayoutItem;
    dxLayoutControl1Item17: TdxLayoutItem;
    dxLayoutControl1Item18: TdxLayoutItem;
    dxLayoutControl1Item19: TdxLayoutItem;
    dxLayoutControl1Item2: TdxLayoutItem;
    dxLayoutControl1Item20: TdxLayoutItem;
    dxLayoutControl1Item21: TdxLayoutItem;
    dxLayoutControl1Item22: TdxLayoutItem;
    dxLayoutControl1Item3: TdxLayoutItem;
    dxLayoutControl1Item4: TdxLayoutItem;
    dxLayoutControl1Item5: TdxLayoutItem;
    dxLayoutControl1Item6: TdxLayoutItem;
    dxLayoutControl1Item7: TdxLayoutItem;
    dxLayoutControl1Item8: TdxLayoutItem;
    dxLayoutControl1Item9: TdxLayoutItem;
    dxLayoutCxLookAndFeel1: TdxLayoutCxLookAndFeel;
    dxLayoutLookAndFeelList1: TdxLayoutLookAndFeelList;
    lblAlignment: TcxLabel;
    lblLeader: TcxLabel;
    lblRemoveTabStops: TcxLabel;
    lblTabStopsToBeCleared: TcxLabel;
    lbTabStopPosition: TcxListBox;
    lciDefaultTabStops: TdxLayoutItem;
    lciTabStopPosition: TdxLayoutItem;
    lcMainGroup_Root: TdxLayoutGroup;
    rbCenter: TcxRadioButton;
    rbDecimal: TcxRadioButton;
    rbDots: TcxRadioButton;
    rbEqualSign: TcxRadioButton;
    rbHyphens: TcxRadioButton;
    rbLeft: TcxRadioButton;
    rbMiddleDots: TcxRadioButton;
    rbNone: TcxRadioButton;
    rbRight: TcxRadioButton;
    rbThickLine: TcxRadioButton;
    rbUnderline: TcxRadioButton;
    seDefaultTabStops: TdxMeasurementUnitEdit;
    teTabStopPosition: TcxTextEdit;
    procedure btnSetClick(Sender: TObject);
    procedure btnClearClick(Sender: TObject);
    procedure btnClearAllClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure seDefaultTabStopsPropertiesChange(Sender: TObject);
    procedure teTabStopPositionPropertiesChange(Sender: TObject);
    procedure OnAlignmentSelected(Sender: TObject);
    procedure OnLeaderSelected(Sender: TObject);
    procedure OnTabStopPositions(Sender: TObject);
  private
    FController: TdxTabsController;
    FRemovedTabStops: TList<Integer>;
    function FindTabStopWithClosePosition(ANewTabStopPosition: Integer): TdxTabInfo;
    function GetIsSetAndClearButtonDisabled: Boolean;
    function GetTabAlignmentType: TdxTabAlignmentType;
    function GetTabLeaderType: TdxTabLeaderType;
    function GetTabStopPosition: Integer;
    function GetTabStopPositionText: string;
    function PositionToText(APosition: integer): string;
    procedure RemoveTabStop(AItemIndex: Integer; ATabFormattingInfo: TdxTabFormattingInfo);
    procedure SetTabAlignmentType(const Value: TdxTabAlignmentType);
    procedure SetTabLeaderType(const Value: TdxTabLeaderType);
    procedure SetTabStopPositionText(const Value: string);
    procedure SetTabStopPosition(const Value: Integer);
    procedure SubscribeControlsEvents;
    function TextToPosition(const AValue: string): Integer;
    function TabInfoPositionAsText(ATabInfo: TdxTabInfo): string;
    procedure UnsubscribeControlsEvents;
  protected
    IsClearAllHappend: Boolean;
    procedure ApplyLocalization; override;
    function GetEditedTabInfo: TdxTabInfo;
    procedure InitializeForm; override;
    procedure UpdateForm(AProc: TProc = nil);
    procedure UpdateLblRemoteTabs;
    procedure UpdateSetAndClearButtons;
    procedure UpdatePosition;
    procedure UpdateRgAlignment;
    procedure UpdateRgLeader;
    procedure UpdateTabInfoAlignmentAndLeader(ATabInfo: TdxTabInfo);
    procedure UpdateTabStopPositions;
    property IsSetAndClearButtonDisabled: Boolean read GetIsSetAndClearButtonDisabled;
    property TabAlignmentType: TdxTabAlignmentType read GetTabAlignmentType write SetTabAlignmentType;
    property TabLeaderType: TdxTabLeaderType read GetTabLeaderType write SetTabLeaderType;
  public
    destructor Destroy; override;
    property Controller: TdxTabsController read FController write FController;
    property TabStopPosition: Integer read GetTabStopPosition write SetTabStopPosition;
    property TabStopPositionText: string read GetTabStopPositionText write SetTabStopPositionText;
  end;

var
  dxRichEditTabsDialogForm: TdxRichEditTabsDialogForm;

implementation

uses
  StrUtils, dxCore, dxRichEdit.DialogStrs;

{$R *.dfm}

{ TdxRichEditTabsDialogForm }

procedure TdxRichEditTabsDialogForm.OnTabStopPositions(Sender: TObject);
begin
  UpdateForm;
end;

procedure TdxRichEditTabsDialogForm.OnAlignmentSelected(Sender: TObject);
var
  ATabInfo: TdxTabInfo;
begin
  ATabInfo := GetEditedTabInfo;
  if Assigned(ATabInfo) then
		UpdateTabInfoAlignmentAndLeader(ATabInfo);
end;

procedure TdxRichEditTabsDialogForm.OnLeaderSelected(Sender: TObject);
var
  ATabInfo: TdxTabInfo;
begin
  ATabInfo := GetEditedTabInfo;
  if Assigned(ATabInfo) then
    UpdateTabInfoAlignmentAndLeader(ATabInfo);
end;

procedure TdxRichEditTabsDialogForm.seDefaultTabStopsPropertiesChange(Sender: TObject);
begin
end;

function TdxRichEditTabsDialogForm.FindTabStopWithClosePosition(ANewTabStopPosition: Integer): TdxTabInfo;
var
  I: Integer;
  ADelta: Integer;
begin
  for I := 0 to Controller.Tabs.Count - 1 do
  begin
    Result := Controller.Tabs[I];
    ADelta := Abs(Result.Position - ANewTabStopPosition);
    if ADelta <= MinimumAllowableTabStopPostionDistance then
      Exit;
  end;
  Result := nil;
end;

function TdxRichEditTabsDialogForm.GetIsSetAndClearButtonDisabled: Boolean;
begin
  Result := Trim(teTabStopPosition.EditingText) = '';
end;

function TdxRichEditTabsDialogForm.GetTabAlignmentType: TdxTabAlignmentType;
begin
  if rbLeft.Checked then
    Result := TdxTabAlignmentType.Left
  else
    if rbCenter.Checked then
      Result := TdxTabAlignmentType.Center
    else
      if rbRight.Checked then
        Result := TdxTabAlignmentType.Right
      else
        if rbDecimal.Checked then
          Result := TdxTabAlignmentType.Decimal
        else
          Result := TdxTabAlignmentType.Left;
end;

function TdxRichEditTabsDialogForm.GetTabLeaderType: TdxTabLeaderType;
begin
  if rbNone.Checked then
    Result := TdxTabLeaderType.None
  else
    if rbDots.Checked then
      Result := TdxTabLeaderType.Dots
    else
      if rbMiddleDots.Checked then
        Result := TdxTabLeaderType.MiddleDots
      else
        if rbHyphens.Checked then
          Result := TdxTabLeaderType.Hyphens
        else
          if rbUnderline.Checked then
            Result := TdxTabLeaderType.Underline
          else
            if rbThickLine.Checked then
              Result := TdxTabLeaderType.ThickLine
            else
              if rbEqualSign.Checked then
                Result := TdxTabLeaderType.EqualSign
              else
                Result := TdxTabLeaderType.None;
end;

function TdxRichEditTabsDialogForm.GetTabStopPosition: Integer;
begin
  Result := TextToPosition(TabStopPositionText);
end;

function TdxRichEditTabsDialogForm.GetTabStopPositionText: string;
begin
  Result := teTabStopPosition.Text;
end;

function TdxRichEditTabsDialogForm.PositionToText(APosition: integer): string;
begin
  Result := IntToStr(APosition);;
end;

procedure TdxRichEditTabsDialogForm.RemoveTabStop(AItemIndex: Integer; ATabFormattingInfo: TdxTabFormattingInfo);
var
  ATabInfo: TdxTabInfo;
  APosition: Integer;
begin
  ATabInfo := ATabFormattingInfo[AItemIndex];
  ATabFormattingInfo.Remove(ATabInfo);
  APosition := ATabInfo.Position;
  if not FRemovedTabStops.Contains(APosition) then
  begin
    FRemovedTabStops.Add(APosition);
    FRemovedTabStops.Sort;
  end;
end;

procedure TdxRichEditTabsDialogForm.SetTabAlignmentType(const Value: TdxTabAlignmentType);
begin
  case Value of
    TdxTabAlignmentType.Left:
      rbLeft.Checked := True;
    TdxTabAlignmentType.Center:
      rbCenter.Checked := True;
    TdxTabAlignmentType.Right:
      rbRight.Checked := True;
    TdxTabAlignmentType.Decimal:
      rbDecimal.Checked := True;
  end;
end;

procedure TdxRichEditTabsDialogForm.SetTabLeaderType(const Value: TdxTabLeaderType);
begin
  case Value of
    TdxTabLeaderType.None:
      rbNone.Checked := True;
    TdxTabLeaderType.Dots:
      rbDots.Checked := True;
    TdxTabLeaderType.MiddleDots:
      rbMiddleDots.Checked := True;
    TdxTabLeaderType.Hyphens:
      rbHyphens.Checked := True;
    TdxTabLeaderType.Underline:
      rbUnderline.Checked := True;
    TdxTabLeaderType.ThickLine:
      rbThickLine.Checked := True;
    TdxTabLeaderType.EqualSign:
      rbEqualSign.Checked := True;
  end;
end;

procedure TdxRichEditTabsDialogForm.teTabStopPositionPropertiesChange(Sender: TObject);
begin
  UpdateSetAndClearButtons;
end;

procedure TdxRichEditTabsDialogForm.btnClearAllClick(Sender: TObject);
begin
	Controller.Tabs.Clear;
  IsClearAllHappend := true;
	UpdateForm;
end;

procedure TdxRichEditTabsDialogForm.btnClearClick(Sender: TObject);
var
  ACurrentItemIndex: Integer;
  ALastTabIndex: Integer;
  ATabFormattingInfo: TdxTabFormattingInfo;
begin
	ACurrentItemIndex := lbTabStopPosition.ItemIndex;
	if ACurrentItemIndex < 0 then
    Exit;
  ATabFormattingInfo := Controller.Tabs;
  RemoveTabStop(ACurrentItemIndex, ATabFormattingInfo);
  UpdateTabStopPositions;
	ALastTabIndex := ATabFormattingInfo.Count - 1;
  if ACurrentItemIndex > ALastTabIndex then
    ACurrentItemIndex := ALastTabIndex;
	UpdateForm(procedure
    begin
  		if ALastTabIndex > -1 then
			  TabStopPosition := ATabFormattingInfo[ACurrentItemIndex].Position;
    end);
end;

procedure TdxRichEditTabsDialogForm.btnSetClick(Sender: TObject);
var
  AItem: TdxTabInfo;
begin
  AItem := FindTabStopWithClosePosition(TabStopPosition);
  if AItem = nil then
  begin
    AItem := TdxTabInfo.Create(TabStopPosition, TabAlignmentType, TabLeaderType);
    Controller.Tabs.Add(AItem);
  end;
  UpdateTabStopPositions;
  lbTabStopPosition.ItemIndex := lbTabStopPosition.Items.IndexOfObject(AItem);
	UpdateForm();
end;

procedure TdxRichEditTabsDialogForm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
end;

procedure TdxRichEditTabsDialogForm.FormCreate(Sender: TObject);
begin
  inherited;
  FRemovedTabStops := TList<Integer>.Create;
end;

procedure TdxRichEditTabsDialogForm.SetTabStopPositionText(const Value: string);
begin
  teTabStopPosition.Text := Value;
end;

procedure TdxRichEditTabsDialogForm.SetTabStopPosition(const Value: Integer);
begin
  TabStopPositionText := PositionToText(Value);
end;

procedure TdxRichEditTabsDialogForm.SubscribeControlsEvents;
begin

end;

function TdxRichEditTabsDialogForm.TextToPosition(const AValue: string): Integer;
begin
  Result := StrToInt(AValue);
end;

function TdxRichEditTabsDialogForm.TabInfoPositionAsText(ATabInfo: TdxTabInfo): string;
begin
  Result := IntToStr(ATabInfo.Position);
end;

procedure TdxRichEditTabsDialogForm.UnsubscribeControlsEvents;
begin

end;

procedure TdxRichEditTabsDialogForm.ApplyLocalization;
begin
  Caption := cxGetResourceString(@sdxRichEditTabsDialogForm);
  btnOk.Caption := cxGetResourceString(@sdxRichEditTabsDialogButtonOk);
  btnCancel.Caption := cxGetResourceString(@sdxRichEditTabsDialogButtonCancel);
  lblTabStopsToBeCleared.Caption := cxGetResourceString(@sdxRichEditTabsDialogTabStopsToBeCleared);
  lblAlignment.Caption := cxGetResourceString(@sdxRichEditTabsDialogAlignment);
  lblLeader.Caption := cxGetResourceString(@sdxRichEditTabsDialogLeader);
  btnSet.Caption := cxGetResourceString(@sdxRichEditTabsDialogButtonSet);
  btnClear.Caption := cxGetResourceString(@sdxRichEditTabsDialogButtonClear);
  btnClearAll.Caption := cxGetResourceString(@sdxRichEditTabsDialogButtonClearAll);
  rbLeft.Caption := cxGetResourceString(@sdxRichEditTabsDialogLeft);
  rbDecimal.Caption := cxGetResourceString(@sdxRichEditTabsDialogDecimal);
  rbCenter.Caption := cxGetResourceString(@sdxRichEditTabsDialogCenter);
  rbHyphens.Caption := cxGetResourceString(@sdxRichEditTabsDialogHyphens);
  rbRight.Caption := cxGetResourceString(@sdxRichEditTabsDialogRight);
  rbNone.Caption := cxGetResourceString(@sdxRichEditTabsDialogNone);
  rbDots.Caption := cxGetResourceString(@sdxRichEditTabsDialogDots);
  rbMiddleDots.Caption := cxGetResourceString(@sdxRichEditTabsDialogMiddleDots);
  rbUnderline.Caption := cxGetResourceString(@sdxRichEditTabsDialogUnderline);
  rbThickLine.Caption := cxGetResourceString(@sdxRichEditTabsDialogThickLine);
  rbEqualSign.Caption := cxGetResourceString(@sdxRichEditTabsDialogEqualSign);
  lciTabStopPosition.CaptionOptions.Text := cxGetResourceString(@sdxRichEditTabsDialogTabStopPosition);
  lciDefaultTabStops.CaptionOptions.Text := cxGetResourceString(@sdxRichEditTabsDialogDefaultTabStops);
end;

function TdxRichEditTabsDialogForm.GetEditedTabInfo: TdxTabInfo;
var
  AIndex: Integer;
begin
  Result := nil;
  if Controller.Tabs.Count >= 0 then
  begin
    AIndex := lbTabStopPosition.ItemIndex;
    if AIndex >= 0 then
      Result := Controller.Tabs[AIndex];
  end;
end;

procedure TdxRichEditTabsDialogForm.InitializeForm;
begin
end;

procedure TdxRichEditTabsDialogForm.UpdateForm(AProc: TProc);
begin
  UnsubscribeControlsEvents;
  try
    UpdatePosition();
    UpdateSetAndClearButtons();
    UpdateRgAlignment();
    UpdateRgLeader();
    UpdateLblRemoteTabs();
    if Assigned(AProc) then
      AProc;
  finally
    SubscribeControlsEvents;
  end;
end;

procedure TdxRichEditTabsDialogForm.UpdateLblRemoteTabs;
var
  AStringBuilder: TStringBuilder;
  AItem: Integer;
  AListSeparator: Char;
begin
  if isClearAllHappend then
    lblRemoveTabStops.Caption := cxGetResourceString(@sdxRichEditTabForm_All)
  else
    begin
      AStringBuilder := TStringBuilder.Create;
      try
        AListSeparator := ListSeparator;
        for AItem in FRemovedTabStops do
        begin
          if AStringBuilder.Length > 0 then
            AStringBuilder.Append(AListSeparator).Append(' ');
          AStringBuilder.Append(PositionToText(AItem));
        end;
        lblRemoveTabStops.Caption := AStringBuilder.ToString;
      finally
        AStringBuilder.Free;
      end;
    end;
end;

procedure TdxRichEditTabsDialogForm.UpdateSetAndClearButtons;
var
  IsEnabled: Boolean;
begin
  IsEnabled := not IsSetAndClearButtonDisabled;
  btnSet.Enabled := IsEnabled;
  btnClear.Enabled := IsEnabled;
end;

procedure TdxRichEditTabsDialogForm.UpdatePosition;
var
  ATabInfo: TdxTabInfo;
begin
  ATabInfo := GetEditedTabInfo;
  if ATabInfo = nil then
    TabStopPositionText := ''
  else
    TabStopPosition := ATabInfo.Position;
end;

procedure TdxRichEditTabsDialogForm.UpdateRgAlignment;
var
  ATabInfo: TdxTabInfo;
begin
  ATabInfo := GetEditedTabInfo;
  if ATabInfo = nil then
    TabAlignmentType := TdxTabAlignmentType.Left
  else
    TabAlignmentType := ATabInfo.Alignment;
end;

procedure TdxRichEditTabsDialogForm.UpdateRgLeader;
var
  ATabInfo: TdxTabInfo;
begin
  ATabInfo := GetEditedTabInfo;
  if ATabInfo = nil then
    TabLeaderType := TdxTabLeaderType.None
  else
    TabLeaderType := ATabInfo.Leader;
end;

procedure TdxRichEditTabsDialogForm.UpdateTabInfoAlignmentAndLeader(ATabInfo: TdxTabInfo);
var
  ATabFormattingInfo: TdxTabFormattingInfo;
  ANewTabInfo: TdxTabInfo;
begin
  ATabFormattingInfo := Controller.Tabs;
	ATabFormattingInfo.Remove(ATabInfo);
  ANewTabInfo := TdxTabInfo.Create(ATabInfo.Position, TabAlignmentType, TabLeaderType);
  ATabFormattingInfo.Add(ANewTabInfo);
end;

procedure TdxRichEditTabsDialogForm.UpdateTabStopPositions;
var
  I: Integer;
  ATabInfo: TdxTabInfo;
begin
  lbTabStopPosition.Items.BeginUpdate;
  try
    lbTabStopPosition.Items.Clear;
    for I := 0 to Controller.Tabs.Count - 1 do
    begin
      ATabInfo := Controller.Tabs[I];
      lbTabStopPosition.Items.AddObject(TabInfoPositionAsText(ATabInfo), ATabInfo);
    end;
  finally
    lbTabStopPosition.Items.EndUpdate;
  end;
end;

destructor TdxRichEditTabsDialogForm.Destroy;
begin
  FRemovedTabStops.Free;
  inherited;
end;

end.
