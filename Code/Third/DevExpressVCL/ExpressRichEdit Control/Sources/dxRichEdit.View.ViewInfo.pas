{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.View.ViewInfo;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Types, SysUtils, Generics.Collections, dxTypeHelpers, dxRichEdit.DocumentModel.PieceTable;

type
  TdxOwnedPageViewInfoList = class;

  { TdxPageViewInfo }

  TdxPageViewInfo = class
  strict private
    FBounds: TRect;
    FClientBounds: TRect;
    FIndex: Integer;
    FPage: TdxPage;
  public
    constructor Create(APage: TdxPage; AOwner: TdxOwnedPageViewInfoList);

    property Bounds: TRect read FBounds write FBounds;
    property ClientBounds: TRect read FClientBounds write FClientBounds;
    property Index: Integer read FIndex write FIndex;
    property Page: TdxPage read FPage;
  end;

  { TdxOwnedPageViewInfoList }

  TdxOwnedPageViewInfoList = class(TObjectList<TdxPageViewInfo>);

  { TdxFirstPageAnchor }

  TdxFirstPageAnchor = class abstract;

  { TdxRunningHeightFirstPageAnchor }

  TdxRunningHeightFirstPageAnchor = class(TdxFirstPageAnchor)
  strict private
    FTopInvisibleHeight: Int64;
  public
    property TopInvisibleHeight: Int64 read FTopInvisibleHeight write FTopInvisibleHeight;
  end;

  { TdxFirstPageOffsetFirstPageAnchor }

  TdxFirstPageOffsetFirstPageAnchor = class(TdxFirstPageAnchor)
  strict private
    FPageIndex: Integer;
    FVerticalOffset: Integer;
  public
    property PageIndex: Integer read FPageIndex write FPageIndex;
    property VerticalOffset: Integer read FVerticalOffset write FVerticalOffset;
  end;

  { TdxPageViewInfoCollection }

  TdxPageViewInfoCollection = class(TList<TdxPageViewInfo>)
  public
    procedure Clear; virtual;
    procedure ExportTo(AExporter: IDocumentLayoutExporter);

    function First: TdxPageViewInfo;
    function Last: TdxPageViewInfo;
  end;

  TdxRichEditViewPageViewInfoCollection = class(TdxPageViewInfoCollection)
  strict private
    FDocumentLayout: TdxDocumentLayout;
    FIsClearing: Boolean;
  protected
    procedure Notify(const Value: TdxPageViewInfo; Action: TCollectionNotification); override;
    procedure UpdateDocumentLayoutVisiblePages;
    procedure ResetDocumentLayoutVisiblePages;

    property IsClearing: Boolean read FIsClearing;
  public
    constructor Create(ADocumentLayout: TdxDocumentLayout);
    destructor Destroy; override;
    procedure Clear; override;

    property DocumentLayout: TdxDocumentLayout read FDocumentLayout;
  end;

implementation


{ TdxPageViewInfo }

constructor TdxPageViewInfo.Create(APage: TdxPage; AOwner: TdxOwnedPageViewInfoList);
begin
  inherited Create;
  Assert(APage <> nil, 'page = nil');
  FPage := APage;
  AOwner.Add(Self);
end;


{ TdxPageViewInfoCollection }

procedure TdxPageViewInfoCollection.Clear;
begin
  inherited Clear;
end;

procedure TdxPageViewInfoCollection.ExportTo(AExporter: IDocumentLayoutExporter);
var
  APageViewInfo: TdxPageViewInfo;
begin
  for APageViewInfo in Self do
    APageViewInfo.Page.ExportTo(AExporter);
end;

function TdxPageViewInfoCollection.First: TdxPageViewInfo;
begin
  if Count > 0 then
    Result := inherited First
  else
    Result := nil;
end;

function TdxPageViewInfoCollection.Last: TdxPageViewInfo;
begin
  if Count > 0 then
    Result := inherited Last
  else
    Result := nil;
end;

{ TdxRichEditViewPageViewInfoCollection }

constructor TdxRichEditViewPageViewInfoCollection.Create(ADocumentLayout: TdxDocumentLayout);
begin
  inherited Create;
  Assert(ADocumentLayout <> nil, 'documentLayout = nil');
  FDocumentLayout := ADocumentLayout;
end;

destructor TdxRichEditViewPageViewInfoCollection.Destroy;
begin
  FIsClearing := True;
  inherited Destroy;
end;

procedure TdxRichEditViewPageViewInfoCollection.Clear;
begin
  FIsClearing := True;
  try
    inherited Clear;
  finally
    FIsClearing := False;
    ResetDocumentLayoutVisiblePages;
  end;
end;

procedure TdxRichEditViewPageViewInfoCollection.Notify(
  const Value: TdxPageViewInfo; Action: TCollectionNotification);
begin
  inherited;
  case Action of
    cnAdded:
      UpdateDocumentLayoutVisiblePages;
    cnRemoved:
      if not IsClearing then
        UpdateDocumentLayoutVisiblePages;
  end;
end;

procedure TdxRichEditViewPageViewInfoCollection.UpdateDocumentLayoutVisiblePages;
begin
  FDocumentLayout.FirstVisiblePageIndex := First.Index;
  FDocumentLayout.LastVisiblePageIndex := Last.Index;
end;

procedure TdxRichEditViewPageViewInfoCollection.ResetDocumentLayoutVisiblePages;
begin
  FDocumentLayout.FirstVisiblePageIndex := -1;
  FDocumentLayout.LastVisiblePageIndex := -1;
end;

end.

