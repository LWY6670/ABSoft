object dxRichEditTabsDialogForm: TdxRichEditTabsDialogForm
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Tabs'
  ClientHeight = 360
  ClientWidth = 381
  Color = clBtnFace
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object dxLayoutControl1: TdxLayoutControl
    Left = 0
    Top = 0
    Width = 381
    Height = 360
    Align = alClient
    TabOrder = 0
    object btnOk: TcxButton
      Left = 215
      Top = 325
      Width = 75
      Height = 25
      Caption = 'OK'
      ModalResult = 1
      TabOrder = 21
    end
    object btnCancel: TcxButton
      Left = 296
      Top = 325
      Width = 75
      Height = 25
      Cancel = True
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 22
    end
    object teTabStopPosition: TcxTextEdit
      Left = 10
      Top = 28
      Properties.OnChange = teTabStopPositionPropertiesChange
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 0
      Width = 182
    end
    object lbTabStopPosition: TcxListBox
      Left = 10
      Top = 47
      Width = 182
      Height = 74
      ItemHeight = 13
      Style.TransparentBorder = False
      TabOrder = 1
      OnClick = OnTabStopPositions
    end
    object seDefaultTabStops: TdxMeasurementUnitEdit
      Left = 198
      Top = 28
      Properties.AutoValue = Null
      Properties.HasAutoValue = False
      Properties.Nullstring = 'Auto'
      Properties.OnChange = seDefaultTabStopsPropertiesChange
      Properties.OnMeasurementUnitChange = MetricsUnitChange
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      TabOrder = 2
      Value = 1.000000000000000000
      Width = 173
    end
    object lblTabStopsToBeCleared: TcxLabel
      Left = 198
      Top = 55
      Caption = 'Tab stops to be cleared:'
      Style.HotTrack = False
      Transparent = True
    end
    object lblRemoveTabStops: TcxLabel
      Left = 214
      Top = 78
      AutoSize = False
      Style.HotTrack = False
      Properties.WordWrap = True
      Transparent = True
      Height = 43
      Width = 157
    end
    object lblAlignment: TcxLabel
      Left = 10
      Top = 127
      AutoSize = False
      Caption = 'Alignment'
      Style.HotTrack = False
      Properties.LineOptions.Visible = True
      Transparent = True
      Height = 17
      Width = 361
    end
    object lblLeader: TcxLabel
      Left = 10
      Top = 196
      AutoSize = False
      Caption = 'Leader'
      Style.HotTrack = False
      Properties.LineOptions.Visible = True
      Transparent = True
      Height = 17
      Width = 361
    end
    object btnSet: TcxButton
      Left = 134
      Top = 294
      Width = 75
      Height = 25
      Caption = '&Set'
      TabOrder = 18
      OnClick = btnSetClick
    end
    object btnClear: TcxButton
      Left = 215
      Top = 294
      Width = 75
      Height = 25
      Caption = 'Cl&ear'
      TabOrder = 19
      OnClick = btnClearClick
    end
    object btnClearAll: TcxButton
      Left = 296
      Top = 294
      Width = 75
      Height = 25
      Caption = 'Clear &All'
      TabOrder = 20
      OnClick = btnClearAllClick
    end
    object rbLeft: TcxRadioButton
      Left = 18
      Top = 150
      Width = 113
      Height = 17
      Caption = '&Left'
      Checked = True
      TabOrder = 6
      TabStop = True
      OnClick = OnAlignmentSelected
      GroupIndex = 1
    end
    object rbDecimal: TcxRadioButton
      Left = 18
      Top = 173
      Width = 113
      Height = 17
      Caption = '&Decimal'
      TabOrder = 9
      OnClick = OnAlignmentSelected
      GroupIndex = 1
    end
    object rbCenter: TcxRadioButton
      Left = 137
      Top = 150
      Width = 113
      Height = 17
      Caption = '&Center'
      TabOrder = 7
      OnClick = OnAlignmentSelected
      GroupIndex = 1
    end
    object rbHyphens: TcxRadioButton
      Left = 18
      Top = 242
      Width = 113
      Height = 17
      Caption = '&Hyphens'
      TabOrder = 14
      OnClick = OnLeaderSelected
      GroupIndex = 2
    end
    object rbRight: TcxRadioButton
      Left = 256
      Top = 150
      Width = 113
      Height = 17
      Caption = '&Right'
      TabOrder = 8
      OnClick = OnAlignmentSelected
      GroupIndex = 1
    end
    object rbNone: TcxRadioButton
      Left = 18
      Top = 219
      Width = 113
      Height = 17
      Caption = '&(None)'
      Checked = True
      TabOrder = 11
      TabStop = True
      OnClick = OnLeaderSelected
      GroupIndex = 2
    end
    object rbDots: TcxRadioButton
      Left = 137
      Top = 219
      Width = 113
      Height = 17
      Caption = '&Dots'
      TabOrder = 12
      OnClick = OnLeaderSelected
      GroupIndex = 2
    end
    object rbMiddleDots: TcxRadioButton
      Left = 256
      Top = 219
      Width = 113
      Height = 17
      Caption = '&MiddleDots'
      TabOrder = 13
      OnClick = OnLeaderSelected
      GroupIndex = 2
    end
    object rbUnderline: TcxRadioButton
      Left = 137
      Top = 242
      Width = 113
      Height = 17
      Caption = '&Underline'
      TabOrder = 15
      OnClick = OnLeaderSelected
      GroupIndex = 2
    end
    object rbThickLine: TcxRadioButton
      Left = 256
      Top = 242
      Width = 113
      Height = 17
      Caption = '&ThickLine'
      TabOrder = 16
      OnClick = OnLeaderSelected
      GroupIndex = 2
    end
    object rbEqualSign: TcxRadioButton
      Left = 18
      Top = 265
      Width = 353
      Height = 17
      Caption = '&EqualSign'
      TabOrder = 17
      OnClick = OnLeaderSelected
      GroupIndex = 2
    end
    object dxLayoutControl1Group_Root: TdxLayoutGroup
      AlignHorz = ahClient
      AlignVert = avClient
      CaptionOptions.Visible = False
      ButtonOptions.Buttons = <>
      Hidden = True
      ShowBorder = False
      Index = -1
    end
    object lcMainGroup_Root: TdxLayoutGroup
      Parent = dxLayoutControl1Group_Root
      AlignHorz = ahClient
      AlignVert = avClient
      CaptionOptions.Text = 'New Group'
      ButtonOptions.Buttons = <>
      ShowBorder = False
      Index = 0
    end
    object dxLayoutControl1Item1: TdxLayoutItem
      Parent = dxLayoutControl1Group1
      AlignHorz = ahClient
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnOk
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object dxLayoutControl1Item2: TdxLayoutItem
      Parent = dxLayoutControl1Group1
      CaptionOptions.Text = 'cxButton2'
      CaptionOptions.Visible = False
      Control = btnCancel
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object dxLayoutControl1Group1: TdxLayoutAutoCreatedGroup
      Parent = dxLayoutControl1Group_Root
      AlignHorz = ahRight
      LayoutDirection = ldHorizontal
      Index = 2
      AutoCreated = True
    end
    object dxLayoutControl1Group2: TdxLayoutGroup
      Parent = lcMainGroup_Root
      CaptionOptions.Text = 'New Group'
      ButtonOptions.Buttons = <>
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 0
    end
    object dxLayoutControl1Group3: TdxLayoutGroup
      Parent = dxLayoutControl1Group2
      AlignHorz = ahClient
      CaptionOptions.Text = 'New Group'
      LayoutLookAndFeel = dxLayoutCxLookAndFeel1
      ButtonOptions.Buttons = <>
      ShowBorder = False
      Index = 0
    end
    object dxLayoutControl1Group4: TdxLayoutGroup
      Parent = dxLayoutControl1Group2
      AlignHorz = ahClient
      CaptionOptions.Text = 'New Group'
      ButtonOptions.Buttons = <>
      ShowBorder = False
      Index = 1
    end
    object lciTabStopPosition: TdxLayoutItem
      Parent = dxLayoutControl1Group3
      CaptionOptions.Text = '&Tab stop position:'
      CaptionOptions.Layout = clTop
      Control = teTabStopPosition
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object dxLayoutControl1Item3: TdxLayoutItem
      Parent = dxLayoutControl1Group3
      CaptionOptions.Text = 'cxListBox1'
      CaptionOptions.Visible = False
      CaptionOptions.Layout = clTop
      Control = lbTabStopPosition
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciDefaultTabStops: TdxLayoutItem
      Parent = dxLayoutControl1Group4
      CaptionOptions.Text = 'De&fault tab stops:'
      CaptionOptions.Layout = clTop
      Control = seDefaultTabStops
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object dxLayoutControl1Item6: TdxLayoutItem
      Parent = dxLayoutControl1Group4
      CaptionOptions.Text = 'cxLabel1'
      CaptionOptions.Visible = False
      CaptionOptions.Layout = clTop
      Control = lblTabStopsToBeCleared
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object dxLayoutControl1Item7: TdxLayoutItem
      Parent = dxLayoutControl1Group4
      AlignVert = avClient
      CaptionOptions.Text = 'cxLabel2'
      CaptionOptions.Visible = False
      CaptionOptions.Layout = clTop
      Offsets.Left = 16
      Control = lblRemoveTabStops
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object dxLayoutControl1Item8: TdxLayoutItem
      Parent = lcMainGroup_Root
      CaptionOptions.Text = 'cxLabel3'
      CaptionOptions.Visible = False
      Control = lblAlignment
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object dxLayoutControl1Item10: TdxLayoutItem
      Parent = lcMainGroup_Root
      CaptionOptions.Text = 'cxLabel4'
      CaptionOptions.Visible = False
      Control = lblLeader
      ControlOptions.ShowBorder = False
      Index = 3
    end
    object dxLayoutControl1Item13: TdxLayoutItem
      Parent = dxLayoutControl1Group5
      AlignHorz = ahClient
      CaptionOptions.Text = 'cxButton4'
      CaptionOptions.Visible = False
      Control = btnSet
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object dxLayoutControl1Item12: TdxLayoutItem
      Parent = dxLayoutControl1Group5
      CaptionOptions.Text = 'cxButton3'
      CaptionOptions.Visible = False
      Control = btnClear
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object dxLayoutControl1Group5: TdxLayoutAutoCreatedGroup
      Parent = dxLayoutControl1Group_Root
      AlignHorz = ahRight
      LayoutDirection = ldHorizontal
      Index = 1
      AutoCreated = True
    end
    object dxLayoutControl1Item14: TdxLayoutItem
      Parent = dxLayoutControl1Group5
      CaptionOptions.Text = 'cxButton5'
      CaptionOptions.Visible = False
      Control = btnClearAll
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object dxLayoutControl1Group6: TdxLayoutGroup
      Parent = lcMainGroup_Root
      AlignHorz = ahClient
      CaptionOptions.Text = 'New Group'
      Offsets.Left = 8
      ButtonOptions.Buttons = <>
      ShowBorder = False
      Index = 2
    end
    object dxLayoutControl1Item4: TdxLayoutItem
      Parent = dxLayoutControl1Group8
      AlignHorz = ahLeft
      CaptionOptions.Text = 'cxRadioButton1'
      CaptionOptions.Visible = False
      Control = rbLeft
      ControlOptions.AutoColor = True
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object dxLayoutControl1Item16: TdxLayoutItem
      Parent = dxLayoutControl1Group6
      AlignHorz = ahLeft
      CaptionOptions.Text = 'cxRadioButton4'
      CaptionOptions.Visible = False
      Control = rbDecimal
      ControlOptions.AutoColor = True
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object dxLayoutControl1Item5: TdxLayoutItem
      Parent = dxLayoutControl1Group8
      AlignHorz = ahLeft
      CaptionOptions.Visible = False
      Control = rbCenter
      ControlOptions.AutoColor = True
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object dxLayoutControl1Group8: TdxLayoutAutoCreatedGroup
      Parent = dxLayoutControl1Group6
      AlignHorz = ahLeft
      LayoutDirection = ldHorizontal
      Index = 0
      AutoCreated = True
    end
    object dxLayoutControl1Group9: TdxLayoutGroup
      Parent = lcMainGroup_Root
      AlignHorz = ahClient
      CaptionOptions.Text = 'New Group'
      Offsets.Left = 8
      ButtonOptions.Buttons = <>
      ShowBorder = False
      Index = 4
    end
    object dxLayoutControl1Item19: TdxLayoutItem
      Parent = dxLayoutControl1Group10
      AlignHorz = ahLeft
      CaptionOptions.Text = 'cxRadioButton4'
      CaptionOptions.Visible = False
      Control = rbHyphens
      ControlOptions.AutoColor = True
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object dxLayoutControl1Item15: TdxLayoutItem
      Parent = dxLayoutControl1Group8
      AlignHorz = ahLeft
      CaptionOptions.Text = 'cxRadioButton3'
      CaptionOptions.Visible = False
      Control = rbRight
      ControlOptions.AutoColor = True
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object dxLayoutControl1Item9: TdxLayoutItem
      Parent = dxLayoutControl1Group7
      AlignHorz = ahLeft
      CaptionOptions.Text = 'cxRadioButton1'
      CaptionOptions.Visible = False
      Control = rbNone
      ControlOptions.AutoColor = True
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object dxLayoutControl1Item17: TdxLayoutItem
      Parent = dxLayoutControl1Group7
      AlignHorz = ahLeft
      CaptionOptions.Text = 'cxRadioButton2'
      CaptionOptions.Visible = False
      Control = rbDots
      ControlOptions.AutoColor = True
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object dxLayoutControl1Group7: TdxLayoutAutoCreatedGroup
      Parent = dxLayoutControl1Group9
      LayoutDirection = ldHorizontal
      Index = 0
      AutoCreated = True
    end
    object dxLayoutControl1Item18: TdxLayoutItem
      Parent = dxLayoutControl1Group7
      AlignHorz = ahLeft
      CaptionOptions.Text = 'cxRadioButton3'
      CaptionOptions.Visible = False
      Control = rbMiddleDots
      ControlOptions.AutoColor = True
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object dxLayoutControl1Item20: TdxLayoutItem
      Parent = dxLayoutControl1Group10
      AlignHorz = ahLeft
      CaptionOptions.Text = 'cxRadioButton5'
      CaptionOptions.Visible = False
      Control = rbUnderline
      ControlOptions.AutoColor = True
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object dxLayoutControl1Group10: TdxLayoutAutoCreatedGroup
      Parent = dxLayoutControl1Group9
      LayoutDirection = ldHorizontal
      Index = 1
      AutoCreated = True
    end
    object dxLayoutControl1Item21: TdxLayoutItem
      Parent = dxLayoutControl1Group10
      AlignHorz = ahLeft
      CaptionOptions.Text = 'cxRadioButton6'
      CaptionOptions.Visible = False
      Control = rbThickLine
      ControlOptions.AutoColor = True
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object dxLayoutControl1Item22: TdxLayoutItem
      Parent = dxLayoutControl1Group9
      CaptionOptions.Text = 'cxRadioButton7'
      CaptionOptions.Visible = False
      Control = rbEqualSign
      ControlOptions.AutoColor = True
      ControlOptions.ShowBorder = False
      Index = 2
    end
  end
  object dxLayoutLookAndFeelList1: TdxLayoutLookAndFeelList
    object dxLayoutCxLookAndFeel1: TdxLayoutCxLookAndFeel
      Offsets.ItemOffset = 0
    end
  end
end
