{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Dialogs.Utils;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Classes, SysUtils, Windows, Generics.Collections, cxSpinEdit;

type
  TdxRichEditMeasurementUnits = (muDefault, muInches, muMillimeters);

  TCustomSpinEditUnitsMeasurementUnitsClass = class of TCustomSpinEditMeasurementUnitsHelper;
  TCustomSpinEditMeasurementUnitsHelper = class
  private
    FDefaultDisplayFormat: string;
    FSpinEdit: TcxCustomSpinEdit;
  protected
    procedure SetSpinEditProperties(ASpinEdit: TcxCustomSpinEdit); virtual; abstract;
  public
    constructor Create(ASpinEdit: TcxCustomSpinEdit);
    procedure BeforeDestruction; override;
    class function GetInnerMeasurementUnits: TdxRichEditMeasurementUnits;
    class function GetInnerMeasurementUnitsName: string;
  end;

  TSpinEditUnitsMeasureManager = class
  private
    FItems: TObjectDictionary<TcxCustomSpinEdit, TCustomSpinEditMeasurementUnitsHelper>;
    FIsChanged: boolean;
  public
    constructor Create;
    destructor Destroy; override;
    function SetUnitsMeasureHelper(ASpinEdit: TcxCustomSpinEdit;
      AUnitsMeasureHelperClass: TCustomSpinEditUnitsMeasurementUnitsClass): TCustomSpinEditMeasurementUnitsHelper;
    procedure Reset(ASpinEdit: TcxCustomSpinEdit);
    property IsChanged: boolean read FIsChanged;
  end;

  TSpinEditLinearMeasureHelper = class(TCustomSpinEditMeasurementUnitsHelper)
  protected
    procedure SetSpinEditProperties(ASpinEdit: TcxCustomSpinEdit); override;
  end;

  TSpinEditLinesMeasureHelper = class(TCustomSpinEditMeasurementUnitsHelper)
  protected
    procedure SetSpinEditProperties(ASpinEdit: TcxCustomSpinEdit); override;
  end;

  TSpinEditPointsMeasureHelper = class(TCustomSpinEditMeasurementUnitsHelper)
  protected
    procedure SetSpinEditProperties(ASpinEdit: TcxCustomSpinEdit); override;
  end;

  TSpinEditNumbersMeasureHelper = class(TCustomSpinEditMeasurementUnitsHelper)
  protected
    procedure SetSpinEditProperties(ASpinEdit: TcxCustomSpinEdit); override;
  end;

var
  MeasurementUnits: TdxRichEditMeasurementUnits;

implementation

uses
  cxClasses, dxCore, dxRichEdit.DialogStrs;

const
  DisplayFormatMap: array[TcxSpinEditValueType] of string = ('0 ', '0.00 ');

function GetDefaultMeasurementUnits: TdxRichEditMeasurementUnits;
const
  MeasurementUintsMap: array[Boolean] of TdxRichEditMeasurementUnits = (muInches, muMillimeters);
begin
  Result := MeasurementUintsMap[GetLocaleChar(LOCALE_USER_DEFAULT, LOCALE_IMEASURE, '0') = '0'];
end;


{ TSpinEditUnitsMeasureManager }

constructor TSpinEditUnitsMeasureManager.Create;
begin
  FItems := TObjectDictionary<TcxCustomSpinEdit, TCustomSpinEditMeasurementUnitsHelper>.Create([doOwnsValues]);
end;

destructor TSpinEditUnitsMeasureManager.Destroy;
begin
  FItems.Free;
  inherited;
end;

procedure TSpinEditUnitsMeasureManager.Reset(ASpinEdit: TcxCustomSpinEdit);
begin
  FIsChanged := True;
  try
    FItems.Remove(ASpinEdit);
  finally
    FIsChanged := False;
  end;
end;

function TSpinEditUnitsMeasureManager.SetUnitsMeasureHelper(ASpinEdit: TcxCustomSpinEdit;
  AUnitsMeasureHelperClass: TCustomSpinEditUnitsMeasurementUnitsClass): TCustomSpinEditMeasurementUnitsHelper;
begin
  FIsChanged := True;
  try
    Result := AUnitsMeasureHelperClass.Create(ASpinEdit);
    FItems.AddOrSetValue(ASpinEdit, Result);
  finally
    FIsChanged := False;
  end;
end;

{ TCustomSpinEditUnitsMeasureHelper }

procedure TCustomSpinEditMeasurementUnitsHelper.BeforeDestruction;
begin
  FSpinEdit.Properties.DisplayFormat := FDefaultDisplayFormat;
end;

constructor TCustomSpinEditMeasurementUnitsHelper.Create(ASpinEdit: TcxCustomSpinEdit);
begin
  inherited Create;
  FSpinEdit := ASpinEdit;
  FDefaultDisplayFormat := ASpinEdit.Properties.DisplayFormat;
  SetSpinEditProperties(ASpinEdit);
end;

class function TCustomSpinEditMeasurementUnitsHelper.GetInnerMeasurementUnits: TdxRichEditMeasurementUnits;
begin
  if MeasurementUnits = muDefault then
    Result := GetDefaultMeasurementUnits
  else
    Result := MeasurementUnits;
end;

class function TCustomSpinEditMeasurementUnitsHelper.GetInnerMeasurementUnitsName: string;
begin
  case GetInnerMeasurementUnits of
    muInches:
      Result := cxGetResourceString(@sdxRichEditUnitsInches);
    muMillimeters:
      Result := cxGetResourceString(@sdxRichEditUnitsMillimeters);
    else
      Result := '';
  end;
end;

{ TSpinEditLinearMeasureHelper }

procedure TSpinEditLinearMeasureHelper.SetSpinEditProperties(ASpinEdit: TcxCustomSpinEdit);
begin
  ASpinEdit.Properties.ValueType := vtFloat;
  ASpinEdit.Properties.DisplayFormat := DisplayFormatMap[ASpinEdit.Properties.ValueType] + GetInnerMeasurementUnitsName;
//  ASpinEdit.Properties.UseDisplayFormatWhenEditing := True;
end;

{ TSpinEditLinesMeasureHelper }

procedure TSpinEditLinesMeasureHelper.SetSpinEditProperties(ASpinEdit: TcxCustomSpinEdit);
begin

end;

{ TSpinEditPointsMeasureHelper }

procedure TSpinEditPointsMeasureHelper.SetSpinEditProperties(ASpinEdit: TcxCustomSpinEdit);
begin
  ASpinEdit.Properties.ValueType := vtFloat;
  ASpinEdit.Properties.DisplayFormat := '0.### ' + cxGetResourceString(@sdxRichEditUnitsPoints);
end;

{ TSpinEditNumbersMeasureHelper }

procedure TSpinEditNumbersMeasureHelper.SetSpinEditProperties(ASpinEdit: TcxCustomSpinEdit);
begin

end;

end.
