{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.DocumentModel.Borders;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Types, Graphics, SysUtils,
  dxCoreClasses,
  dxRichEdit.DocumentModel.Core,
  dxRichEdit.Utils.Types, dxRichEdit.Utils.Colors, dxRichEdit.DocumentModel.IndexBasedObject,
  dxRichEdit.DocumentModel.UnitConverter;

type
  TdxBorderLineStyle = (
    &Nil = -1,
    None = 0,
    Single = 1,
    Thick = 2,
    Double = 3,
    Dotted = 4,
    Dashed = 5,
    DotDash = 6,
    DotDotDash = 7,
    Triple = 8,
    ThinThickSmallGap = 9,
    ThickThinSmallGap = 10,
    ThinThickThinSmallGap = 11,
    ThinThickMediumGap = 12,
    ThickThinMediumGap = 13,
    ThinThickThinMediumGap = 14,
    ThinThickLargeGap = 15,
    ThickThinLargeGap = 16,
    ThinThickThinLargeGap = 17,
    Wave = 18,
    DoubleWave = 19,
    DashSmallGap = 20,
    DashDotStroked = 21,
    ThreeDEmboss = 22,
    ThreeDEngrave = 23,
    Outset = 24,
    Inset = 25,
    //art borders
    Apples,
    ArchedScallops,
    BabyPacifier,
    BabyRattle,
    Balloons3Colors,
    BalloonsHotAir,
    BasicBlackDashes,
    BasicBlackDots,
    BasicBlackSquares,
    BasicThinLines,
    BasicWhiteDashes,
    BasicWhiteDots,
    BasicWhiteSquares,
    BasicWideInline,
    BasicWideMidline,
    BasicWideOutline,
    Bats,
    Birds,
    BirdsFlight,
    Cabins,
    CakeSlice,
    CandyCorn,
    CelticKnotwork,
    CertificateBanner,
    ChainLink,
    ChampagneBottle,
    CheckedBarBlack,
    CheckedBarColor,
    Checkered,
    ChristmasTree,
    CirclesLines,
    CirclesRectangles,
    ClassicalWave,
    Clocks,
    Compass,
    Confetti,
    ConfettiGrays,
    ConfettiOutline,
    ConfettiStreamers,
    ConfettiWhite,
    CornerTriangles,
    CouponCutoutDashes,
    CouponCutoutDots,
    CrazyMaze,
    CreaturesButterfly,
    CreaturesFish,
    CreaturesInsects,
    CreaturesLadyBug,
    CrossStitch,
    Cup,
    DecoArch,
    DecoArchColor,
    DecoBlocks,
    DiamondsGray,
    DoubleD,
    DoubleDiamonds,
    Earth1,
    Earth2,
    EclipsingSquares1,
    EclipsingSquares2,
    EggsBlack,
    Fans,
    Film,
    Firecrackers,
    FlowersBlockPrint,
    FlowersDaisies,
    FlowersModern1,
    FlowersModern2,
    FlowersPansy,
    FlowersRedRose,
    FlowersRoses,
    FlowersTeacup,
    FlowersTiny,
    Gems,
    GingerbreadMan,
    Gradient,
    Handmade1,
    Handmade2,
    HeartBalloon,
    HeartGray,
    Hearts,
    HeebieJeebies,
    Holly,
    HouseFunky,
    Hypnotic,
    IceCreamCones,
    LightBulb,
    Lightning1,
    Lightning2,
    MapleLeaf,
    MapleMuffins,
    MapPins,
    Marquee,
    MarqueeToothed,
    Moons,
    Mosaic,
    MusicNotes,
    Northwest,
    Ovals,
    Packages,
    PalmsBlack,
    PalmsColor,
    PaperClips,
    Papyrus,
    PartyFavor,
    PartyGlass,
    Pencils,
    People,
    PeopleHats,
    PeopleWaving,
    Poinsettias,
    PostageStamp,
    Pumpkin1,
    PushPinNote1,
    PushPinNote2,
    Pyramids,
    PyramidsAbove,
    Quadrants,
    Rings,
    Safari,
    Sawtooth,
    SawtoothGray,
    ScaredCat,
    Seattle,
    ShadowedSquares,
    SharksTeeth,
    ShorebirdTracks,
    Skyrocket,
    SnowflakeFancy,
    Snowflakes,
    Sombrero,
    Southwest,
    Stars,
    Stars3d,
    StarsBlack,
    StarsShadowed,
    StarsTop,
    Sun,
    Swirligig,
    TornPaper,
    TornPaperBlack,
    Trees,
    TriangleParty,
    Triangles,
    Tribal1,
    Tribal2,
    Tribal3,
    Tribal4,
    Tribal5,
    Tribal6,
    TwistedLines1,
    TwistedLines2,
    Vine,
    Waveline,
    WeavingAngles,
    WeavingBraid,
    WeavingRibbon,
    WeavingStrips,
    WhiteFlowers,
    Woodwork,
    XIllusions,
    ZanyTriangles,
    ZigZag,
    ZigZagStitch,
    Disabled = $7FFFFFFF 
  );

  TdxBorderChangeType = (
    None = 0,
    Style,
    Color,
    Width,
    Offset,
    Frame,
    Shadow,
    BatchUpdate
  );

  TdxProperties = (
    //CellMargins,
    TopMargin,
    LeftMargin,
    RightMargin,
    BottomMargin,
    CellSpacing,
    PreferredWidth,
    TableIndent,
    TableLayout,
    TableLook,
    TableStyleColumnBandSize,
    TableStyleRowBandSize,
    IsTableOverlap,
    AvoidDoubleBorders,
    //TableBorders,
    LeftBorder,
    RightBorder,
    TopBorder,
    BottomBorder,
    InsideHorizontalBorder,
    InsideVerticalBorder,
    TopLeftDiagonalBorder,
    TopRightDiagonalBorder,
    HideCellMark,
    NoWrap,
    FitText,
    TextDirection,
    VerticalAlignment,
    ColumnSpan,
    VerticalMerging,
    CellConditionalFormatting,
    RowHeight,
    WidthBefore,
    WidthAfter,
    Header,
    TableRowAlignment,
    TableRowConditionalFormatting,
    GridAfter,
    GridBefore,
    CantSplit,
    TableFloatingPosition,
    BackgroundColor,
    CellGeneralSettings,
    Borders,
    TableAlignment
  );

  { IdxPropertiesContainer }

  IdxPropertiesContainer = interface
  ['{37AD9DE4-820F-4EBD-8BD1-142F885D9116}']
    procedure BeginChanging(AChangedProperty: TdxProperties);
    procedure EndChanging;
    procedure BeginPropertiesUpdate;
    procedure EndPropertiesUpdate;
    procedure RaiseObtainAffectedRange(const AArgs: TdxObtainAffectedRangeEventArgs);
    procedure ResetPropertyUse(AProperty: TdxProperties);
    procedure ApplyChanges(const AChangeActions: TdxDocumentModelChangeActions);
  end;

  { TdxBorderInfo }

  TdxBorderInfo = class(TcxIUnknownObject, IdxCloneable<TdxBorderInfo>, IdxSupportsCopyFrom<TdxBorderInfo>,
    IdxSupportsSizeOf)
  private
    FStyle: TdxBorderLineStyle;
    FColor: TColor;
    FWidth: Integer;
    FOffset: Integer;
    FFrame: Boolean;
    FShadow: Boolean;
  public
    function Equals(AObject: TObject): Boolean; override;
    function GetHashCode: Integer; override;
    procedure CopyFrom(const AInfo: TdxBorderInfo);
    function Clone: TdxBorderInfo;
    property Style: TdxBorderLineStyle read FStyle write FStyle;
    property Color: TColor read FColor write FColor;
    property Width: Integer read FWidth write FWidth;
    property Offset: Integer read FOffset write FOffset;
    property Frame: Boolean read FFrame write FFrame;
    property Shadow: Boolean read FShadow write FShadow;
  end;

  { TdxBorderInfoCache }

  TdxBorderInfoCache = class(TdxUniqueItemsCache<TdxBorderInfo>)
  protected
    function CreateDefaultItem(const AUnitConverter: IdxDocumentModelUnitConverter): TdxBorderInfo; override;
  end;

  { TdxBorderChangeActionsCalculator }

  TdxBorderChangeActionsCalculator = class
  public
    class function CalculateChangeActions(AChange: TdxBorderChangeType): TdxDocumentModelChangeActions;
  end;

  { TdxBorderBase }

  TdxBorderBase = class abstract(TdxRichEditIndexBasedObject<TdxBorderInfo>)
  private
    FOwner: IdxPropertiesContainer;
    function GetColor: TColor;
    function GetFrame: Boolean;
    function GetOffset: Integer;
    function GetShadow: Boolean;
    function GetStyle: TdxBorderLineStyle;
    function GetWidth: Integer;
    procedure SetColor(const Value: TColor);
    procedure SetFrame(const Value: Boolean);
    procedure SetOffset(const Value: Integer);
    procedure SetShadow(const Value: Boolean);
    procedure SetStyle(const Value: TdxBorderLineStyle);
    procedure SetWidth(const Value: Integer);
  protected
    function SetStyleCore(const AInfo: TdxBorderInfo; const AValue: Integer): TdxDocumentModelChangeActions;
    function SetColorCore(const AInfo: TdxBorderInfo; const AValue: Integer): TdxDocumentModelChangeActions;
    function SetWidthCore(const AInfo: TdxBorderInfo; const AValue: Integer): TdxDocumentModelChangeActions;
    function SetOffsetCore(const AInfo: TdxBorderInfo; const AValue: Integer): TdxDocumentModelChangeActions;
    function SetFrameCore(const AInfo: TdxBorderInfo; const AValue: Boolean): TdxDocumentModelChangeActions;
    function SetShadowCore(const AInfo: TdxBorderInfo; const AValue: Boolean): TdxDocumentModelChangeActions;
    function GetCache(const ADocumentModel: TdxCustomDocumentModel): TdxUniqueItemsCache<TdxBorderInfo>; override;
    procedure OnBeginAssign; override;
    procedure OnEndAssign; override;
    function GetBatchUpdateChangeActions: TdxDocumentModelChangeActions; override;
    procedure RaiseObtainAffectedRange(const AArgs: TdxObtainAffectedRangeEventArgs); override;
    procedure ApplyChanges(const AChangeActions: TdxDocumentModelChangeActions); override;
    function GetProperty: TdxProperties; virtual; abstract;

    property Owner: IdxPropertiesContainer read FOwner;
  public
    constructor Create(APieceTable: TObject; AOwner: IdxPropertiesContainer); reintroduce;
    procedure ResetBorder;

    property &Property: TdxProperties read GetProperty;
    property Style: TdxBorderLineStyle read GetStyle write SetStyle;
    property Color: TColor read GetColor write SetColor;
    property Width: Integer read GetWidth write SetWidth;
    property Offset: Integer read GetOffset write SetOffset;
    property Frame: Boolean read GetFrame write SetFrame;
    property Shadow: Boolean read GetShadow write SetShadow;
  end;

  { TdxInsideHorizontalBorder }

  TdxInsideHorizontalBorder = class(TdxBorderBase)
  protected
    function GetProperty: TdxProperties; override;
  end;

  { TdxInsideVerticalBorder }

  TdxInsideVerticalBorder = class(TdxBorderBase)
  protected
    function GetProperty: TdxProperties; override;
  end;

  { TdxLeftBorder }

  TdxLeftBorder = class(TdxBorderBase)
  protected
    function GetProperty: TdxProperties; override;
  end;

  { TdxRightBorder }

  TdxRightBorder = class(TdxBorderBase)
  protected
    function GetProperty: TdxProperties; override;
  end;

  { TdxTopBorder }

  TdxTopBorder = class(TdxBorderBase)
  protected
    function GetProperty: TdxProperties; override;
  end;

  { TdxBottomBorder }

  TdxBottomBorder = class(TdxBorderBase)
  protected
    function GetProperty: TdxProperties; override;
  end;

  TdxWidthUnitType = (
    &Nil = 0,               
    Auto = 1,
    FiftiethsOfPercent = 2, 
    ModelUnits = 3
  );

  { TdxWidthUnitInfo }

  TdxWidthUnitInfo = class(TcxIUnknownObject, IdxCloneable<TdxWidthUnitInfo>,
    IdxSupportsCopyFrom<TdxWidthUnitInfo>, IdxSupportsSizeOf)
  private
    FType: TdxWidthUnitType;
    FValue: Integer;
  public
    constructor Create(AType: TdxWidthUnitType; AValue: Integer); overload;

    procedure CopyFrom(const AInfo: TdxWidthUnitInfo);
    function Clone: TdxWidthUnitInfo;
    function Equals(AObject: TObject): Boolean; override;
    function GetHashCode: Integer; override;
    property &Type: TdxWidthUnitType read FType write FType;
    property Value: Integer read FValue write FValue;
  end;

  { TdxWidthUnit }

  TdxWidthUnit = class abstract(TdxRichEditIndexBasedObject<TdxWidthUnitInfo>)
  private
    FOwner: IdxPropertiesContainer;
    function GetType: TdxWidthUnitType;
    procedure SetType(const AValue: TdxWidthUnitType);
    function GetValue: Integer;
    procedure SetValue(const AValue: Integer);
  protected
    function SetTypeCore(const AUnit: TdxWidthUnitInfo;
      const AValue: TdxWidthUnitType): TdxDocumentModelChangeActions; virtual;
    function SetValueCore(const AUnit: TdxWidthUnitInfo;
      const AValue: Integer): TdxDocumentModelChangeActions; virtual;
    function GetCache(const ADocumentModel: TdxCustomDocumentModel): TdxUniqueItemsCache<TdxWidthUnitInfo>; override;
    procedure OnEndAssign; override;
    function GetBatchUpdateChangeActions: TdxDocumentModelChangeActions; override;
    procedure RaiseObtainAffectedRange(const AArgs: TdxObtainAffectedRangeEventArgs); override;
    procedure ApplyChanges(const AChangeActions: TdxDocumentModelChangeActions); override;

    property Owner: IdxPropertiesContainer read FOwner;
  public
    constructor Create(APieceTable: TObject; const AOwner: IdxPropertiesContainer); reintroduce;

    property &Type: TdxWidthUnitType read GetType write SetType;
    property Value: Integer read GetValue write SetValue;
  end;

  { TdxWidthUnitInfoCache }

  TdxWidthUnitInfoCache = class(TdxUniqueItemsCache<TdxWidthUnitInfo>)
  protected
    function CreateDefaultItem(const AUnitConverter: IdxDocumentModelUnitConverter): TdxWidthUnitInfo; override;
  end;

  TdxHeightUnitType = (
    Minimum,
    Auto,
    Exact
  );

  { TdxHeightUnitInfo }

  TdxHeightUnitInfo = class(TcxIUnknownObject, IdxCloneable<TdxHeightUnitInfo>,
    IdxSupportsCopyFrom<TdxHeightUnitInfo>, IdxSupportsSizeOf)
  private
    FType: TdxHeightUnitType;
    FVal: Integer;
  public
    constructor Create(AValue: Integer; AType: TdxHeightUnitType); overload;
    function Equals(AObject: TObject): Boolean; override;
    function GetHashCode: Integer; override;

    procedure CopyFrom(const AInfo: TdxHeightUnitInfo);
    function Clone: TdxHeightUnitInfo;
    property &Type: TdxHeightUnitType read FType write FType; 
    property Value: Integer read FVal write FVal; 
  end;

  { TdxHeightUnit }

  TdxHeightUnit = class(TdxRichEditIndexBasedObject<TdxHeightUnitInfo>)
  private
    FOwner: IdxPropertiesContainer;
    function GetType: TdxHeightUnitType;
    procedure SetType(const Value: TdxHeightUnitType);
    function GetValue: Integer;
    procedure SetValue(const Value: Integer);
  protected
    function SetTypeCore(const AUnit: TdxHeightUnitInfo;
      const AValue: Integer): TdxDocumentModelChangeActions;
    function SetValueCore(const AUnit: TdxHeightUnitInfo;
      const AValue: Integer): TdxDocumentModelChangeActions;
    function GetCache(const ADocumentModel: TdxCustomDocumentModel): TdxUniqueItemsCache<TdxHeightUnitInfo>; override;
    procedure OnEndAssign; override;
    function GetBatchUpdateChangeActions: TdxDocumentModelChangeActions; override;
    procedure RaiseObtainAffectedRange(const AArgs: TdxObtainAffectedRangeEventArgs); override;
    procedure ApplyChanges(const AChangeActions: TdxDocumentModelChangeActions); override;

    property Owner: IdxPropertiesContainer read FOwner;
  public
    constructor Create(APieceTable: TObject; AOwner: IdxPropertiesContainer); reintroduce;

    property &Type: TdxHeightUnitType read GetType write SetType;
    property Value: Integer read GetValue write SetValue;
  end;

  { TdxHeightUnitInfoCache }

  TdxHeightUnitInfoCache = class(TdxUniqueItemsCache<TdxHeightUnitInfo>)
  protected
    function CreateDefaultItem(const AUnitConverter: IdxDocumentModelUnitConverter): TdxHeightUnitInfo; override;
  end;

  { TdxMarginUnitBase }

  TdxMarginUnitBase = class abstract (TdxWidthUnit)
  protected
    function GetProperty: TdxProperties; virtual; abstract;
    procedure OnBeginAssign; override;
    function GetBatchUpdateChangeActions: TdxDocumentModelChangeActions; override;
    function SetTypeCore(const AUnit: TdxWidthUnitInfo; const AValue: TdxWidthUnitType): TdxDocumentModelChangeActions; override;
    function SetValueCore(const AUnit: TdxWidthUnitInfo; const AValue: Integer): TdxDocumentModelChangeActions; override;

    property &Property: TdxProperties read GetProperty;
  end;

  { TdxPreferredWidth }

  TdxPreferredWidth = class(TdxWidthUnit)
  protected
    procedure OnBeginAssign; override;
    function GetBatchUpdateChangeActions: TdxDocumentModelChangeActions; override;
    function SetTypeCore(const AUnit: TdxWidthUnitInfo;
      const AValue: TdxWidthUnitType): TdxDocumentModelChangeActions; override;
    function SetValueCore(const AUnit: TdxWidthUnitInfo;
      const AValue: Integer): TdxDocumentModelChangeActions; override;
  end;

  { TdxTopLeftDiagonalBorder }

  TdxTopLeftDiagonalBorder = class(TdxBorderBase)
  protected
    function GetProperty: TdxProperties; override;
  end;

  { TdxTopRightDiagonalBorder }

  TdxTopRightDiagonalBorder = class(TdxBorderBase)
  protected
    function GetProperty: TdxProperties; override;
  end;

implementation

uses
  Classes, RTLConsts, dxRichEdit.Utils.BatchUpdateHelper, 
  Math, dxRichEdit.DocumentModel.PieceTable;

{ TdxBorderInfo }

procedure TdxBorderInfo.CopyFrom(const AInfo: TdxBorderInfo);
begin
  Style := AInfo.Style;
  Color := AInfo.Color;
  Width := AInfo.Width;
  Frame := AInfo.Frame;
  Shadow := AInfo.Shadow;
  Offset := AInfo.Offset;
end;

function TdxBorderInfo.Clone: TdxBorderInfo;
begin
  Result := TdxBorderInfo.Create;
  Result.CopyFrom(Self);
end;

function TdxBorderInfo.Equals(AObject: TObject): Boolean;
var
  AInfo: TdxBorderInfo absolute AObject;
begin
  if AObject is TdxBorderInfo then
  begin
    Result := (Style = AInfo.Style) and (Color = AInfo.Color) and (Width = AInfo.Width) and (Frame = AInfo.Frame) and
      (Shadow = AInfo.Shadow) and (Offset = AInfo.Offset);
  end
  else
    Result := False;
end;

function TdxBorderInfo.GetHashCode: Integer;
begin
  Result := ((Ord(Style) shl 20) or (Width shl 15) or (Offset shl 10) or
    (IfThen(Frame, 1, 0) shl 5) or (IfThen(Shadow, 1, 0) shl 4)) xor Color;
end;

{ TdxBorderInfoCache }

function TdxBorderInfoCache.CreateDefaultItem(const AUnitConverter: IdxDocumentModelUnitConverter): TdxBorderInfo;
begin
  Result := TdxBorderInfo.Create;
  Result.Style := TdxBorderLineStyle.&Nil;
  Result.Color := TdxColor.Black;
  Result.Width := 0; 
end;

{ TdxBorderBase }

procedure TdxBorderBase.ApplyChanges(const AChangeActions: TdxDocumentModelChangeActions);
begin
  if Owner <> nil then
    Owner.ApplyChanges(AChangeActions)
  else
    inherited ApplyChanges(AChangeActions);
end;

constructor TdxBorderBase.Create(APieceTable: TObject; AOwner: IdxPropertiesContainer);
begin
  inherited Create(APieceTable as TdxPieceTable);
  Assert(APieceTable is TdxPieceTable);
  FOwner := AOwner;
end;

function TdxBorderBase.GetBatchUpdateChangeActions: TdxDocumentModelChangeActions;
begin
  Result := TdxBorderChangeActionsCalculator.CalculateChangeActions(TdxBorderChangeType.BatchUpdate);
end;

function TdxBorderBase.GetCache(const ADocumentModel: TdxCustomDocumentModel): TdxUniqueItemsCache<TdxBorderInfo>;
begin
  Result := TdxDocumentModel(ADocumentModel).Cache.BorderInfoCache;
end;

function TdxBorderBase.GetColor: TColor;
begin
  Result := Info.Color;
end;

function TdxBorderBase.GetFrame: Boolean;
begin
  Result := Info.Frame;
end;

function TdxBorderBase.GetOffset: Integer;
begin
  Result := Info.Offset;
end;

function TdxBorderBase.GetShadow: Boolean;
begin
  Result := Info.Shadow;
end;

function TdxBorderBase.GetStyle: TdxBorderLineStyle;
begin
  Result := Info.Style;
end;

function TdxBorderBase.GetWidth: Integer;
begin
  Result := Info.Width;
end;

procedure TdxBorderBase.OnBeginAssign;
begin
  inherited OnBeginAssign;
  if Owner <> nil then
    Owner.BeginChanging(&Property);
end;

procedure TdxBorderBase.OnEndAssign;
begin
  if Owner <> nil then
    Owner.EndChanging;
  inherited OnEndAssign;
end;

procedure TdxBorderBase.RaiseObtainAffectedRange(const AArgs: TdxObtainAffectedRangeEventArgs);
begin
  if Owner <> nil then
    Owner.RaiseObtainAffectedRange(AArgs);
end;

procedure TdxBorderBase.ResetBorder;
begin
  Owner.ResetPropertyUse(&Property);
end;

procedure TdxBorderBase.SetColor(const Value: TColor);
begin
  if Value = Info.Color then
    NotifyFakeAssign
  else
    SetPropertyValue<Integer>(SetColorCore, Ord(Value));
end;

function TdxBorderBase.SetColorCore(const AInfo: TdxBorderInfo; const AValue: Integer): TdxDocumentModelChangeActions;
begin
  AInfo.Color := AValue;
  Result := TdxBorderChangeActionsCalculator.CalculateChangeActions(TdxBorderChangeType.Color);
end;

procedure TdxBorderBase.SetFrame(const Value: Boolean);
begin
  if Value = Info.Frame then
    NotifyFakeAssign
  else
    SetPropertyValue<Boolean>(SetFrameCore, Value);
end;

function TdxBorderBase.SetFrameCore(const AInfo: TdxBorderInfo; const AValue: Boolean): TdxDocumentModelChangeActions;
begin
  AInfo.Frame := AValue;
  Result := TdxBorderChangeActionsCalculator.CalculateChangeActions(TdxBorderChangeType.Frame);
end;

procedure TdxBorderBase.SetOffset(const Value: Integer);
begin
  if Value < 0 then
    raise Exception.Create('Error Message'); 
  if Value = Info.Offset then
    NotifyFakeAssign
  else
    SetPropertyValue<Integer>(SetOffsetCore, Value);
end;

function TdxBorderBase.SetOffsetCore(const AInfo: TdxBorderInfo; const AValue: Integer): TdxDocumentModelChangeActions;
begin
  AInfo.Offset := AValue;
  Result := TdxBorderChangeActionsCalculator.CalculateChangeActions(TdxBorderChangeType.Offset);
end;

procedure TdxBorderBase.SetShadow(const Value: Boolean);
begin
  if Value = Info.Shadow then
    NotifyFakeAssign
  else
    SetPropertyValue<Boolean>(SetShadowCore, Value);
end;

function TdxBorderBase.SetShadowCore(const AInfo: TdxBorderInfo; const AValue: Boolean): TdxDocumentModelChangeActions;
begin
  AInfo.Shadow := AValue;
  Result := TdxBorderChangeActionsCalculator.CalculateChangeActions(TdxBorderChangeType.Shadow);
end;

procedure TdxBorderBase.SetStyle(const Value: TdxBorderLineStyle);
begin
  if Value = Info.Style then
    NotifyFakeAssign
  else
    SetPropertyValue<Integer>(SetStyleCore, Ord(Value));
end;

function TdxBorderBase.SetStyleCore(const AInfo: TdxBorderInfo;
  const AValue: Integer): TdxDocumentModelChangeActions;
begin
  AInfo.Style := TdxBorderLineStyle(AValue);
  Result := TdxBorderChangeActionsCalculator.CalculateChangeActions(TdxBorderChangeType.Style);
end;

procedure TdxBorderBase.SetWidth(const Value: Integer);
begin
  if Value < 0 then
    raise Exception.Create('Error Message'); 
  if Value = Info.Width then
    NotifyFakeAssign
  else
    SetPropertyValue<Integer>(SetWidthCore, Value);
end;

function TdxBorderBase.SetWidthCore(const AInfo: TdxBorderInfo; const AValue: Integer): TdxDocumentModelChangeActions;
begin
  AInfo.Width := AValue;
  Result := TdxBorderChangeActionsCalculator.CalculateChangeActions(TdxBorderChangeType.Width);
end;

{ TdxWidthUnit }

procedure TdxWidthUnit.ApplyChanges(const AChangeActions: TdxDocumentModelChangeActions);
begin
  if Owner <> nil then
    Owner.ApplyChanges(AChangeActions)
  else
    inherited ApplyChanges(AChangeActions);
end;

constructor TdxWidthUnit.Create(APieceTable: TObject; const AOwner: IdxPropertiesContainer);
begin
  inherited Create(APieceTable as TdxPieceTable);
  Assert(AOwner <> nil);
  FOwner := AOwner;
end;

function TdxWidthUnit.GetBatchUpdateChangeActions: TdxDocumentModelChangeActions;
begin
  Result := [];
end;

function TdxWidthUnit.GetCache(const ADocumentModel: TdxCustomDocumentModel): TdxUniqueItemsCache<TdxWidthUnitInfo>;
begin
  Result := TdxDocumentModel(DocumentModel).Cache.UnitInfoCache;
end;

function TdxWidthUnit.GetType: TdxWidthUnitType;
begin
  Result := Info.&Type;
end;

function TdxWidthUnit.GetValue: Integer;
begin
  Result := Info.Value;
end;

procedure TdxWidthUnit.OnEndAssign;
begin
  Owner.EndChanging;
  inherited OnEndAssign;
end;

procedure TdxWidthUnit.RaiseObtainAffectedRange(const AArgs: TdxObtainAffectedRangeEventArgs);
begin
  if Owner <> nil then
    Owner.RaiseObtainAffectedRange(AArgs);
end;

procedure TdxWidthUnit.SetType(const AValue: TdxWidthUnitType);
begin
  if AValue = Info.&Type then
  begin
    NotifyFakeAssign;
    Exit;
  end;
  SetPropertyValue<TdxWidthUnitType>(SetTypeCore, AValue);
end;

function TdxWidthUnit.SetTypeCore(const AUnit: TdxWidthUnitInfo; const AValue: TdxWidthUnitType): TdxDocumentModelChangeActions;
begin
  AUnit.&Type := AValue;
  Result := [];
end;

procedure TdxWidthUnit.SetValue(const AValue: Integer);
begin
  if AValue = Info.Value then
  begin
    NotifyFakeAssign;
    Exit;
  end;
  SetPropertyValue<Integer>(SetValueCore, AValue);
end;

function TdxWidthUnit.SetValueCore(const AUnit: TdxWidthUnitInfo; const AValue: Integer): TdxDocumentModelChangeActions;
begin
  AUnit.Value := AValue;
  Result := [];
end;

{ TdxWidthUnitInfo }

constructor TdxWidthUnitInfo.Create(AType: TdxWidthUnitType; AValue: Integer);
begin
  inherited Create;
  FType := AType;
  FValue := AValue;
end;

procedure TdxWidthUnitInfo.CopyFrom(const AInfo: TdxWidthUnitInfo);
begin
  FType := AInfo.&Type;
  FValue := AInfo.Value;
end;

function TdxWidthUnitInfo.Clone: TdxWidthUnitInfo;
begin
  Result := TdxWidthUnitInfo.Create;
  Result.CopyFrom(Self);
end;

function TdxWidthUnitInfo.Equals(AObject: TObject): Boolean;
var
  AInfo: TdxWidthUnitInfo;
begin
  if AObject is TdxWidthUnitInfo then
  begin
    AInfo := TdxWidthUnitInfo(AObject);
    Result := (FType = AInfo.&Type) and (FValue = AInfo.Value);
  end
  else
    Result := False;
end;

function TdxWidthUnitInfo.GetHashCode: Integer;
begin
  Result := (Ord(FType) shl 3) or FValue;
end;

{ TdxWidthUnitInfoCache }

function TdxWidthUnitInfoCache.CreateDefaultItem(const AUnitConverter: IdxDocumentModelUnitConverter): TdxWidthUnitInfo;
begin
  Result := TdxWidthUnitInfo.Create;
end;

{ TdxMarginUnitBase }

function TdxMarginUnitBase.GetBatchUpdateChangeActions: TdxDocumentModelChangeActions;
begin
  Result := [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
    TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetRuler,
    TdxDocumentModelChangeAction.RaiseContentChanged, TdxDocumentModelChangeAction.ResetSelectionLayout,
    TdxDocumentModelChangeAction.ResetRuler];
end;

procedure TdxMarginUnitBase.OnBeginAssign;
begin
  inherited OnBeginAssign;
  Owner.BeginChanging(&Property);
end;

function TdxMarginUnitBase.SetTypeCore(const AUnit: TdxWidthUnitInfo;
  const AValue: TdxWidthUnitType): TdxDocumentModelChangeActions;
begin
  Result := inherited SetTypeCore(AUnit, AValue) + [TdxDocumentModelChangeAction.Redraw,
    TdxDocumentModelChangeAction.ResetPrimaryLayout, TdxDocumentModelChangeAction.ResetSecondaryLayout,
    TdxDocumentModelChangeAction.ResetRuler, TdxDocumentModelChangeAction.RaiseContentChanged,
    TdxDocumentModelChangeAction.ResetSelectionLayout, TdxDocumentModelChangeAction.ResetRuler];
end;

function TdxMarginUnitBase.SetValueCore(const AUnit: TdxWidthUnitInfo; const AValue: Integer): TdxDocumentModelChangeActions;
begin
  Result := inherited SetValueCore(AUnit, AValue) + [TdxDocumentModelChangeAction.Redraw,
    TdxDocumentModelChangeAction.ResetPrimaryLayout, TdxDocumentModelChangeAction.ResetSecondaryLayout,
    TdxDocumentModelChangeAction.ResetRuler, TdxDocumentModelChangeAction.RaiseContentChanged,
    TdxDocumentModelChangeAction.ResetSelectionLayout, TdxDocumentModelChangeAction.ResetRuler];
end;

{ TdxHeightUnitInfo }

procedure TdxHeightUnitInfo.CopyFrom(const AInfo: TdxHeightUnitInfo);
begin
  &Type := AInfo.&Type;
  Value := AInfo.Value;
end;

function TdxHeightUnitInfo.Clone: TdxHeightUnitInfo;
begin
  Result := TdxHeightUnitInfo.Create;
  Result.CopyFrom(Self);
end;

constructor TdxHeightUnitInfo.Create(AValue: Integer; AType: TdxHeightUnitType);
begin
  inherited Create;
  FVal := AValue;
  FType := AType;
end;

function TdxHeightUnitInfo.Equals(AObject: TObject): Boolean;
var
  AInfo: TdxHeightUnitInfo;
begin
  if AObject is TdxHeightUnitInfo then
  begin
    AInfo := TdxHeightUnitInfo(AObject);
    Result := (&Type = AInfo.&Type) and (Value = AInfo.Value);
  end
  else
    Result := False;
end;

function TdxHeightUnitInfo.GetHashCode: Integer;
begin
  Result := (Ord(&Type) shl 3) or Value;
end;

{ TdxHeightUnit }

procedure TdxHeightUnit.ApplyChanges(const AChangeActions: TdxDocumentModelChangeActions);
begin
  if Owner <> nil then
    Owner.ApplyChanges(AChangeActions)
  else
    inherited ApplyChanges(AChangeActions);
end;

constructor TdxHeightUnit.Create(APieceTable: TObject; AOwner: IdxPropertiesContainer);
begin
  inherited Create(APieceTable as TdxPieceTable);
  Assert(AOwner <> nil); 
  FOwner := AOwner;
end;

function TdxHeightUnit.GetBatchUpdateChangeActions: TdxDocumentModelChangeActions;
begin
  Result := [];
end;

function TdxHeightUnit.GetCache(const ADocumentModel: TdxCustomDocumentModel): TdxUniqueItemsCache<TdxHeightUnitInfo>;
begin
  Result := TdxDocumentModel(ADocumentModel).Cache.HeightUnitInfoCache;
end;

function TdxHeightUnit.GetType: TdxHeightUnitType;
begin
  Result := Info.&Type;
end;

function TdxHeightUnit.GetValue: Integer;
begin
  Result := Info.Value;
end;

procedure TdxHeightUnit.OnEndAssign;
begin
  Owner.EndChanging;
  inherited OnEndAssign;
end;

procedure TdxHeightUnit.RaiseObtainAffectedRange(const AArgs: TdxObtainAffectedRangeEventArgs);
begin
  if Owner <> nil then
    Owner.RaiseObtainAffectedRange(AArgs);
end;

procedure TdxHeightUnit.SetType(const Value: TdxHeightUnitType);
begin
  if Value = Info.&Type then
    NotifyFakeAssign
  else
    SetPropertyValue<Integer>(SetTypeCore, Ord(Value));
end;

function TdxHeightUnit.SetTypeCore(const AUnit: TdxHeightUnitInfo; const AValue: Integer): TdxDocumentModelChangeActions;
begin
  AUnit.&Type := TdxHeightUnitType(AValue);
  Result := GetBatchUpdateChangeActions;
end;

procedure TdxHeightUnit.SetValue(const Value: Integer);
begin
  if Value < 0 then
    Assert(False); 
  if Value = Info.Value then
    NotifyFakeAssign
  else
    SetPropertyValue<Integer>(SetValueCore, Value);
end;

function TdxHeightUnit.SetValueCore(const AUnit: TdxHeightUnitInfo; const AValue: Integer): TdxDocumentModelChangeActions;
begin
  AUnit.Value := AValue;
  Result := GetBatchUpdateChangeActions;
end;

{ TdxHeightUnitInfoCache }

function TdxHeightUnitInfoCache.CreateDefaultItem(const AUnitConverter: IdxDocumentModelUnitConverter): TdxHeightUnitInfo;
begin
  Result := TdxHeightUnitInfo.Create;
  Result.&Type := TdxHeightUnitType.Auto;
  Result.Value := 0;
end;

{ TdxBorderChangeActionsCalculator }

class function TdxBorderChangeActionsCalculator.CalculateChangeActions(
  AChange: TdxBorderChangeType): TdxDocumentModelChangeActions;
const
  ABorderChangeActionsTable: array[TdxBorderChangeType] of TdxDocumentModelChangeActions = (
    [], 
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout], 
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetSecondaryLayout], 
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout], 
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout], 
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout], 
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout], 
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout] 
  );
begin
  Result := ABorderChangeActionsTable[AChange];
end;

{ TdxPreferredWidth }

function TdxPreferredWidth.GetBatchUpdateChangeActions: TdxDocumentModelChangeActions;
begin
  Result := [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
    TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetRuler,
    TdxDocumentModelChangeAction.RaiseContentChanged, TdxDocumentModelChangeAction.ResetSelectionLayout,
    TdxDocumentModelChangeAction.ResetRuler];
end;

procedure TdxPreferredWidth.OnBeginAssign;
begin
  inherited OnBeginAssign;
  Owner.BeginChanging(TdxProperties.PreferredWidth);
end;

function TdxPreferredWidth.SetTypeCore(const AUnit: TdxWidthUnitInfo;
  const AValue: TdxWidthUnitType): TdxDocumentModelChangeActions;
begin
  Result := inherited SetTypeCore(AUnit, AValue) + [TdxDocumentModelChangeAction.Redraw,
    TdxDocumentModelChangeAction.ResetPrimaryLayout, TdxDocumentModelChangeAction.ResetSecondaryLayout,
    TdxDocumentModelChangeAction.ResetRuler, TdxDocumentModelChangeAction.RaiseContentChanged,
    TdxDocumentModelChangeAction.ResetSelectionLayout, TdxDocumentModelChangeAction.ResetRuler];
end;

function TdxPreferredWidth.SetValueCore(const AUnit: TdxWidthUnitInfo;
  const AValue: Integer): TdxDocumentModelChangeActions;
begin
  Result := inherited SetValueCore(AUnit, AValue) + [TdxDocumentModelChangeAction.Redraw,
    TdxDocumentModelChangeAction.ResetPrimaryLayout, TdxDocumentModelChangeAction.ResetSecondaryLayout,
    TdxDocumentModelChangeAction.ResetRuler, TdxDocumentModelChangeAction.RaiseContentChanged,
    TdxDocumentModelChangeAction.ResetSelectionLayout, TdxDocumentModelChangeAction.ResetRuler];
end;

{ TdxInsideHorizontalBorder }

function TdxInsideHorizontalBorder.GetProperty: TdxProperties;
begin
  Result := TdxProperties.InsideHorizontalBorder;
end;

{ TdxInsideVerticalBorder }

function TdxInsideVerticalBorder.GetProperty: TdxProperties;
begin
  Result := TdxProperties.InsideVerticalBorder;
end;

{ TdxLeftBorder }

function TdxLeftBorder.GetProperty: TdxProperties;
begin
  Result := TdxProperties.LeftBorder;
end;

{ TdxRightBorder }

function TdxRightBorder.GetProperty: TdxProperties;
begin
  Result := TdxProperties.RightBorder;
end;

{ TdxTopBorder }

function TdxTopBorder.GetProperty: TdxProperties;
begin
  Result := TdxProperties.TopBorder;
end;

{ TdxBottomBorder }

function TdxBottomBorder.GetProperty: TdxProperties;
begin
  Result := TdxProperties.BottomBorder;
end;

{ TdxTopLeftDiagonalBorder }

function TdxTopLeftDiagonalBorder.GetProperty: TdxProperties;
begin
  Result := TdxProperties.TopLeftDiagonalBorder;
end;

{ TdxTopRightDiagonalBorder }

function TdxTopRightDiagonalBorder.GetProperty: TdxProperties;
begin
  Result := TdxProperties.TopRightDiagonalBorder;
end;

end.
