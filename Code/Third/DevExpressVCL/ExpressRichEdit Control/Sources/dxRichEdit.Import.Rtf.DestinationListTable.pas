{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Import.Rtf.DestinationListTable;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Types, Classes, SysUtils, Graphics, Windows, dxCoreClasses, dxRichEdit.Import.Rtf,
  Generics.Collections, dxRichEdit.Import.Rtf.DestinationPieceTable, dxRichEdit.Import.Rtf.DestinationListLevel;

type
  TdxListNameDestination = class(TdxStringValueDestination);
  TdxListStyleNameDestination = class(TdxStringValueDestination);

  TdxListTableDestination = class(TdxRichEditRtfDestinationBase)
  private
    FCurrentList: TdxRtfNumberingList;
    procedure TryToHandleFinishOfListLevelDestination(ANestedDestination: TdxRichEditRtfDestinationBase);
    procedure TryToHandleFinishOfListNameDestination(ANestedDestination: TdxRichEditRtfDestinationBase);
    procedure TryToHandleFinishOfListStyleNameDestination(ANestedDestination: TdxRichEditRtfDestinationBase);
    //Handlers
    class procedure ListHybridKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure ListIdKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure ListKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure ListLevelKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure ListNameKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure ListRestartAtEachSectionKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure ListSimpleKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure ListStyleIdKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure ListStyleNameKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure ListTemplateIdKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
  protected
    function CanProcessDefaultKeyword: Boolean; override;
    function GetControlCharHT: TdxControlCharTranslatorTable; override;
    procedure InitializeClone(AClone: TdxRichEditRtfDestinationBase); override;
    procedure ProcessCharCore(AChar: Char); override;
    procedure PopulateKeywordTable(ATable: TdxKeywordTranslatorTable); override;
  public
    constructor Create(AImporter: TdxRichEditDocumentModelRtfImporter); override;
    destructor Destroy; override;

    procedure NestedGroupFinished(ADestination: TdxRichEditRtfDestinationBase); override;

    property CurrentList: TdxRtfNumberingList read FCurrentList write FCurrentList;
  end;

  { TdxListOverrideTableDestination }

  TdxListOverrideTableDestination = class(TdxRichEditRtfDestinationBase)
  private
    FCurrentOverride: TdxRtfNumberingListOverride;
    //Handlers
    class procedure ListOverrideCountKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure ListOverrideIdKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure ListOverrideKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure ListOverrideLevelKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure ListOverrideListIdKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
  protected
    procedure PopulateKeywordTable(ATable: TdxKeywordTranslatorTable); override;
    function GetControlCharHT: TdxControlCharTranslatorTable; override;

    property CurrentOverride: TdxRtfNumberingListOverride read FCurrentOverride write FCurrentOverride;
  end;

implementation

uses
  dxRichEdit.Import.Rtf.DestinationSkip;

{ TdxListTableDestination }

constructor TdxListTableDestination.Create(AImporter: TdxRichEditDocumentModelRtfImporter);
begin
  inherited Create(AImporter);
end;

destructor TdxListTableDestination.Destroy;
begin
  inherited Destroy;
end;

procedure TdxListTableDestination.NestedGroupFinished(ADestination: TdxRichEditRtfDestinationBase);
begin
  TryToHandleFinishOfListNameDestination(ADestination);
  TryToHandleFinishOfListStyleNameDestination(ADestination);
  TryToHandleFinishOfListLevelDestination(ADestination);
end;

function TdxListTableDestination.CanProcessDefaultKeyword: Boolean;
begin
  Result := False;
end;

function TdxListTableDestination.GetControlCharHT: TdxControlCharTranslatorTable;
begin
  Result := nil;
end;

procedure TdxListTableDestination.InitializeClone(AClone: TdxRichEditRtfDestinationBase);
begin
  TdxListTableDestination(AClone).CurrentList := CurrentList;
end;

procedure TdxListTableDestination.ProcessCharCore(AChar: Char);
begin
//do nothing
end;

procedure TdxListTableDestination.TryToHandleFinishOfListLevelDestination(ANestedDestination: TdxRichEditRtfDestinationBase);
var
  ACurrentDestination: TdxListTableDestination;
  ADestination: TdxListLevelDestination;
begin
  if ANestedDestination is TdxListLevelDestination then
  begin
    ACurrentDestination := TdxListTableDestination(Importer.Destination);
    ADestination := TdxListLevelDestination(ANestedDestination);
    ACurrentDestination.CurrentList.Levels.Add(ADestination.Level);
  end;
end;

procedure TdxListTableDestination.TryToHandleFinishOfListNameDestination(ANestedDestination: TdxRichEditRtfDestinationBase);
var
  ACurrentDestination: TdxListTableDestination;
  ADestination: TdxListNameDestination;
begin
  if ANestedDestination is TdxListNameDestination then
  begin
    ACurrentDestination := TdxListTableDestination(Importer.Destination);
    ADestination := TdxListNameDestination(ANestedDestination);
    ACurrentDestination.CurrentList.Name := ADestination.Value;
  end;
end;

procedure TdxListTableDestination.TryToHandleFinishOfListStyleNameDestination(ANestedDestination: TdxRichEditRtfDestinationBase);
var
  ACurrentDestination: TdxListTableDestination;
  ADestination: TdxListStyleNameDestination;
begin
  if ANestedDestination is TdxListStyleNameDestination then
  begin
    ACurrentDestination := TdxListTableDestination(Importer.Destination);
    ADestination := TdxListStyleNameDestination(ANestedDestination);
    ACurrentDestination.CurrentList.StyleName := ADestination.Value;
  end;
end;

procedure TdxListTableDestination.PopulateKeywordTable(
  ATable: TdxKeywordTranslatorTable);
begin
  ATable.Add('list', ListKeywordHandler);
  ATable.Add('listid', ListIdKeywordHandler);
  ATable.Add('listtemplateid', ListTemplateIdKeywordHandler);
  ATable.Add('liststyleid', ListStyleIdKeywordHandler);
  ATable.Add('liststylename', ListStyleNameKeywordHandler);
  ATable.Add('listname', ListNameKeywordHandler);
  ATable.Add('listhybrid', ListHybridKeywordHandler);
  ATable.Add('listrestarthdn', ListRestartAtEachSectionKeywordHandler);
  ATable.Add('listsimple', ListSimpleKeywordHandler);
  ATable.Add('listlevel', ListLevelKeywordHandler);
end;

class procedure TdxListTableDestination.ListHybridKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
var
  ADestination: TdxListTableDestination;
begin
  ADestination := TdxListTableDestination(AImporter.Destination);
  ADestination.CurrentList.NumberingListType := TdxRtfNumberingListType.Hybrid;
end;

class procedure TdxListTableDestination.ListIdKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
var
  ADestination: TdxListTableDestination;
begin
  if AHasParameter then
  begin
    ADestination := TdxListTableDestination(AImporter.Destination);
    ADestination.CurrentList.ID := AParameterValue;
  end;
end;

class procedure TdxListTableDestination.ListKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
var
  ADestination: TdxListTableDestination;
begin
  ADestination := TdxListTableDestination(AImporter.Destination);
  ADestination.CurrentList := TdxRtfNumberingList.Create;
  AImporter.DocumentProperties.ListTable.Add(ADestination.CurrentList);
end;

class procedure TdxListTableDestination.ListLevelKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
begin
  AImporter.Destination := TdxListLevelDestination.Create(AImporter);
end;

class procedure TdxListTableDestination.ListNameKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
begin
  AImporter.Destination := TdxListNameDestination.Create(AImporter);
end;

class procedure TdxListTableDestination.ListRestartAtEachSectionKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
begin
//do nothing
end;

class procedure TdxListTableDestination.ListSimpleKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
begin
//do nothing
end;

class procedure TdxListTableDestination.ListStyleIdKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
var
  ADestination: TdxListTableDestination;
begin
  if AHasParameter then
  begin
    ADestination := TdxListTableDestination(AImporter.Destination);
    ADestination.CurrentList.ParentStyleID := AParameterValue;
  end;
end;

class procedure TdxListTableDestination.ListStyleNameKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
begin
  AImporter.Destination := TdxListStyleNameDestination.Create(AImporter);
end;

class procedure TdxListTableDestination.ListTemplateIdKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
begin
//do nothing
end;

{ TdxListOverrideTableDestination }

function TdxListOverrideTableDestination.GetControlCharHT: TdxControlCharTranslatorTable;
begin
  Result := nil;
end;

procedure TdxListOverrideTableDestination.PopulateKeywordTable(
  ATable: TdxKeywordTranslatorTable);
begin
  ATable.Add('listoverride', ListOverrideKeywordHandler);
  ATable.Add('listid', ListOverrideListIdKeywordHandler);
  ATable.Add('listoverridecount', ListOverrideCountKeywordHandler);
  ATable.Add('ls', ListOverrideIdKeywordHandler);
  ATable.Add('lfolevel', ListOverrideLevelKeywordHandler);
end;

class procedure TdxListOverrideTableDestination.ListOverrideCountKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
//do nothing
end;

class procedure TdxListOverrideTableDestination.ListOverrideIdKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
var
  ADestination: TdxListOverrideTableDestination;
begin
  if AHasParameter then
  begin
    ADestination := TdxListOverrideTableDestination(AImporter.Destination);
    if (ADestination.CurrentOverride <> nil) then
      ADestination.CurrentOverride.ID := AParameterValue;
  end;
end;

class procedure TdxListOverrideTableDestination.ListOverrideKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
var
  ADestination: TdxListOverrideTableDestination;
begin
  ADestination := TdxListOverrideTableDestination(AImporter.Destination);
  ADestination.CurrentOverride := TdxRtfNumberingListOverride.Create;
  AImporter.DocumentProperties.ListOverrideTable.Add(ADestination.CurrentOverride);
end;

class procedure TdxListOverrideTableDestination.ListOverrideLevelKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
var
  ADestination: TdxListOverrideTableDestination;
  ANewDestination: TdxListOverrideLevelDestination;
begin
  ADestination := TdxListOverrideTableDestination(AImporter.Destination);
  if ADestination.CurrentOverride <> nil then
  begin
    ANewDestination := TdxListOverrideLevelDestination.Create(AImporter);
    ADestination.CurrentOverride.Levels.Add(ANewDestination.OverrideLevel);
    AImporter.Destination := ANewDestination;
  end
  else
    AImporter.Destination := TdxSkipDestination.Create(AImporter);
end;

class procedure TdxListOverrideTableDestination.ListOverrideListIdKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
var
  ADestination: TdxListOverrideTableDestination;
begin
  if AHasParameter then
  begin
    ADestination := TdxListOverrideTableDestination(AImporter.Destination);
    if ADestination.CurrentOverride <> nil then
      ADestination.CurrentOverride.ListId := AParameterValue;
  end;
end;

end.
