{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.DocumentModel.History.Paragraph;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  dxRichEdit.DocumentModel.PieceTable, dxRichEdit.DocumentModel.Core, Generics.Collections,
  dxRichEdit.DocumentModel.SectionRange;

type

  { TdxRemoveLastParagraphHistoryItem }

  TdxRemoveLastParagraphHistoryItem = class(TdxParagraphBaseHistoryItem)
  private
    FParagraph: TdxParagraph;
    FRun: TdxTextRunBase;
    FOriginalParagraphCount: Integer;
  protected
    procedure RedoCore; override;
    procedure UndoCore; override;
  public
    destructor Destroy; override;

    property OriginalParagraphCount: Integer read FOriginalParagraphCount write FOriginalParagraphCount;
  end;

  { TdxParagraphsDeletedHistoryItem }

  TdxParagraphsDeletedHistoryItem = class(TdxParagraphBaseHistoryItem)
  strict private
    FDeletedParagraphsCount: Integer;
    FDeletedParagraphs: TObjectList<TdxParagraph>;
		FNotificationIds: TList<Integer>;
  protected
    procedure RedoCore; override;
    procedure UndoCore; override;
  public
    constructor Create(APieceTable: TdxPieceTable);
    destructor Destroy; override;

    property DeletedParagraphsCount: Integer read FDeletedParagraphsCount write FDeletedParagraphsCount;
  end;

  { TdxFixLastParagraphOfLastSectionHistoryItem }

  TdxFixLastParagraphOfLastSectionHistoryItem = class(TdxParagraphBaseHistoryItem)
  private
    FSection: TdxSection;
    FLastRun: TdxSectionRun;
    FOriginalParagraphCount: Integer;
  protected
    procedure RedoCore; override;
    procedure UndoCore; override;
  public
    property OriginalParagraphCount: Integer read FOriginalParagraphCount write FOriginalParagraphCount;
  end;

  { TdxParagraphInsertedBaseHistoryItem }

  TdxParagraphInsertedBaseHistoryItem = class(TdxParagraphBaseHistoryItem)
  private
    FLogPosition: TdxDocumentLogPosition;
    FParagraphMarkRunIndex: TdxRunIndex;
    FNotificationId: Integer;
    FNewParagraph: TdxParagraph;
    class procedure ChangeRangesParagraph(ARunIndex: TdxRunIndex; ARuns: TdxTextRunCollection; AOldParagraph, ANewParagraph: TdxParagraph); static;
  protected
    procedure UndoCore; override;
    procedure RedoCore; override;
  public
    constructor Create(APieceTable: TdxPieceTable);
    destructor Destroy; override;
    procedure Execute; override;

    property ParagraphMarkRunIndex: TdxRunIndex read FParagraphMarkRunIndex write FParagraphMarkRunIndex;
    property LogPosition: TdxDocumentLogPosition read FLogPosition write FLogPosition;
  end;

implementation

uses
  SysUtils, dxRichEdit.DocumentModel.Commands;

{ TdxRemoveLastParagraphHistoryItem }

destructor TdxRemoveLastParagraphHistoryItem.Destroy;
begin
  FreeAndNil(FParagraph);
  inherited Destroy;
end;

procedure TdxRemoveLastParagraphHistoryItem.RedoCore;
var
  ARunIndex: TdxRunIndex;
  AParagraphIndex: TdxParagraphIndex;
begin
  AParagraphIndex := OriginalParagraphCount - 1;
  FParagraph := PieceTable.Paragraphs[AParagraphIndex];
  PieceTable.Paragraphs.RemoveAt(AParagraphIndex); 
  ARunIndex := PieceTable.Runs.Count - 1;
  FRun := PieceTable.Runs[ARunIndex];
  PieceTable.Runs.Delete(ARunIndex);
end;

procedure TdxRemoveLastParagraphHistoryItem.UndoCore;
begin
  PieceTable.Runs.Add(FRun);
  PieceTable.Paragraphs.Add(FParagraph);
  FParagraph := nil;
end;

{ TdxParagraphsDeletedHistoryItem }

constructor TdxParagraphsDeletedHistoryItem.Create(APieceTable: TdxPieceTable);
begin
  inherited Create(APieceTable);
  FDeletedParagraphsCount := -1;
end;

destructor TdxParagraphsDeletedHistoryItem.Destroy;
begin
  FreeAndNil(FDeletedParagraphs);
  FreeAndNil(FNotificationIds);
  inherited Destroy;
end;

procedure TdxParagraphsDeletedHistoryItem.RedoCore;
var
  I: Integer;
  ACount: TdxParagraphIndex;
  AParagraphs: TdxParagraphCollection;
  AParagraph: TdxParagraph;
  AFirstRunIndex, ALastRunIndex: TdxRunIndex;
  ALogPosition: TdxDocumentLogPosition;
begin
  FDeletedParagraphs.Free;
  FDeletedParagraphs := TObjectList<TdxParagraph>.Create;
  if FNotificationIds = nil then
  begin
    FNotificationIds := TList<Integer>.Create;
    for I := 0 to DeletedParagraphsCount - 1 do
      FNotificationIds.Add(DocumentModel.History.GetNotificationId);
  end;
  AParagraphs := PieceTable.Paragraphs;
  ACount := ParagraphIndex + DeletedParagraphsCount;
  for I := ACount - 1 downto ParagraphIndex do
  begin
    AParagraph := AParagraphs[I];
    FDeletedParagraphs.Add(AParagraph);
    AFirstRunIndex := AParagraph.FirstRunIndex;
    ALastRunIndex := AParagraph.LastRunIndex;
    ALogPosition := AParagraph.LogPosition;
    TdxDocumentModelStructureChangedNotifier.NotifyParagraphRemoved(PieceTable, PieceTable, SectionIndex, I,
      AFirstRunIndex, FNotificationIds[I - ParagraphIndex]);
    AParagraphs.RemoveAt(I);
    AParagraph.AfterRemove(AFirstRunIndex, ALastRunIndex, ALogPosition);
    TdxNumberingListNotifier.NotifyParagraphRemoved(DocumentModel, AParagraph.GetOwnNumberingListIndex);
  end;
end;

procedure TdxParagraphsDeletedHistoryItem.UndoCore;
var
  AParagraphs: TdxParagraphCollection;
  ACount, I: TdxParagraphIndex;
  AParagraph: TdxParagraph;
begin
  AParagraphs := PieceTable.Paragraphs;
  ACount := ParagraphIndex + DeletedParagraphsCount;
  FDeletedParagraphs.OwnsObjects := False;
  for I := ParagraphIndex to ACount - 1 do
  begin
    AParagraph := FDeletedParagraphs[ACount - I - 1];
    AParagraphs.Insert(I, AParagraph);
    AParagraph.AfterUndoRemove;
    TdxDocumentModelStructureChangedNotifier.NotifyParagraphInserted(PieceTable, PieceTable, SectionIndex,
      I, AParagraphs[I].FirstRunIndex, Cell, False, I, FNotificationIds[I - ParagraphIndex]);
    TdxNumberingListNotifier.NotifyParagraphAdded(DocumentModel, AParagraph.GetOwnNumberingListIndex);
  end;
end;

{ TdxFixLastParagraphOfLastSectionHistoryItem }

procedure TdxFixLastParagraphOfLastSectionHistoryItem.RedoCore;
var
  ARuns: TdxTextRunCollection;
  ASections: TdxSectionCollection;
  ASectionIndex: TdxSectionIndex;
begin
  ASections := DocumentModel.Sections;
  FSection := ASections.Last;
  if FSection.FirstParagraphIndex = OriginalParagraphCount - 1 then
  begin
    FSection.UnsubscribeHeadersFootersEvents;
    ASectionIndex := ASections.Count - 1;
    ASections.Delete(ASectionIndex);
  end
  else
    FSection.LastParagraphIndex := FSection.LastParagraphIndex - 1;
  ARuns := PieceTable.Runs;
  if ARuns.Last is TdxSectionRun then
  begin
    FLastRun := TdxSectionRun(ARuns.Last);
    DocumentModel.UnsafeEditor.ReplaceSectionRunWithParagraphRun(PieceTable, FLastRun, ARuns.Count - 1);
  end;
end;

procedure TdxFixLastParagraphOfLastSectionHistoryItem.UndoCore;
begin
  if FSection.FirstParagraphIndex = OriginalParagraphCount - 1 then
  begin
    FSection.SubscribeHeadersFootersEvents;
    DocumentModel.Sections.Add(FSection);
  end
  else
    FSection.LastParagraphIndex := FSection.LastParagraphIndex + 1;
  if FLastRun <> nil then
    DocumentModel.UnsafeEditor.ReplaceParagraphRunWithSectionRun(PieceTable, FLastRun, PieceTable.Runs.Count - 1);
end;

{ TdxParagraphInsertedBaseHistoryItem }

constructor TdxParagraphInsertedBaseHistoryItem.Create(APieceTable: TdxPieceTable);
begin
  inherited Create(APieceTable);
  FLogPosition := -1;
  FParagraphMarkRunIndex := -1;
end;

destructor TdxParagraphInsertedBaseHistoryItem.Destroy;
begin
  FreeAndNil(FNewParagraph);
  inherited Destroy;
end;

procedure TdxParagraphInsertedBaseHistoryItem.Execute;
begin
  Assert(FNewParagraph = nil);
  FNewParagraph := TdxParagraph.Create(PieceTable);
  inherited Execute;
end;

procedure TdxParagraphInsertedBaseHistoryItem.RedoCore;
var
  AParagraph: TdxParagraph;
  ASectionIndex: TdxSectionIndex;
  AParagraphLastRunIndex: TdxRunIndex;
  AParagraphFirstRunIndex: TdxRunIndex;
  AParagraphLogPosition: TdxDocumentLogPosition;
begin
  AParagraph := PieceTable.Paragraphs[ParagraphIndex];
  ASectionIndex := PieceTable.LookupSectionIndexByParagraphIndex(ParagraphIndex);
  Assert((ASectionIndex >= 0) or ((ASectionIndex = dxRunIndexDontCare) and (PieceTable.IsNote or PieceTable.IsTextBox or PieceTable.IsComment)));
  FNewParagraph.SetParagraphStyleIndexCore(AParagraph.ParagraphStyleIndex);
  AParagraphLastRunIndex := ParagraphMarkRunIndex;
  AParagraphFirstRunIndex := AParagraph.FirstRunIndex;
  AParagraphLogPosition := AParagraph.LogPosition;
  FNewParagraph.Length := LogPosition - AParagraphLogPosition + 1;
  AParagraph.SetRelativeFirstRunIndex(FParagraphMarkRunIndex + 1);
  AParagraph.Length := AParagraph.Length - FNewParagraph.Length;
  AParagraph.ShiftLogPosition(FNewParagraph.Length);
  PieceTable.Paragraphs.Insert(ParagraphIndex, FNewParagraph);
  FNewParagraph.SetRelativeFirstRunIndex(AParagraphFirstRunIndex);
  FNewParagraph.SetRelativeLastRunIndex(AParagraphLastRunIndex);
  FNewParagraph.SetRelativeLogPosition(AParagraphLogPosition);
  ChangeRangesParagraph(AParagraphLastRunIndex, PieceTable.Runs, AParagraph, FNewParagraph);
  if FNotificationId = TdxNotificationIdGenerator.EmptyId then
    FNotificationId := DocumentModel.History.GetNotificationId;
  TdxDocumentModelStructureChangedNotifier.NotifyParagraphInserted(PieceTable, PieceTable, ASectionIndex, ParagraphIndex, ParagraphMarkRunIndex + 1, Cell, false, ParagraphIndex, FNotificationId);
  FNewParagraph.InheritStyleAndFormattingFromCore(AParagraph);
  FNewParagraph := nil;
end;

procedure TdxParagraphInsertedBaseHistoryItem.UndoCore;
var
  AParagraph: TdxParagraph;
  ASectionIndex: TdxSectionIndex;
  ARunIndex: TdxRunIndex;
begin
  Assert(FNewParagraph = nil);
  AParagraph := PieceTable.Paragraphs[ParagraphIndex + 1];
  ASectionIndex := PieceTable.LookupSectionIndexByParagraphIndex(ParagraphIndex);
  Assert(ASectionIndex >= 0);
  FNewParagraph := PieceTable.Paragraphs[ParagraphIndex];
  ARunIndex := AParagraph.FirstRunIndex;
  AParagraph.SetRelativeFirstRunIndex(FNewParagraph.FirstRunIndex);
  AParagraph.Length := AParagraph.Length + FNewParagraph.Length;
  AParagraph.SetRelativeLogPosition(FNewParagraph.LogPosition);
  ChangeRangesParagraph(FNewParagraph.LastRunIndex, PieceTable.Runs, FNewParagraph, AParagraph);
  TdxDocumentModelStructureChangedNotifier.NotifyParagraphRemoved(PieceTable, PieceTable, ASectionIndex, ParagraphIndex, ARunIndex, FNotificationId);
  PieceTable.Paragraphs.RemoveAt(ParagraphIndex);
end;

class procedure TdxParagraphInsertedBaseHistoryItem.ChangeRangesParagraph(ARunIndex: TdxRunIndex; ARuns: TdxTextRunCollection;
  AOldParagraph, ANewParagraph: TdxParagraph);
var
  I: Integer;
  ARange: TdxTextRunBase;
begin
  for I := ARunIndex downto 0 do
  begin
    ARange := ARuns[I];
    if ARange.Paragraph <> AOldParagraph then
      Break;
    ARange.Paragraph := ANewParagraph;
  end;
end;

end.
