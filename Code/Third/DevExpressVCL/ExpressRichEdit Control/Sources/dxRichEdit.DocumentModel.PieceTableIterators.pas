{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.DocumentModel.PieceTableIterators;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Types, SysUtils, Generics.Collections,
  dxRichEdit.DocumentModel.PieceTable, dxRichEdit.Platform.Font;

type
  { TdxPieceTableIterator }

  TdxPieceTableIterator = class abstract
  strict private
    FPieceTable: TdxPieceTable;
  private
  protected
    procedure MoveBackCore(var APos: TdxDocumentModelPosition); virtual; abstract;
    procedure MoveForwardCore(var APos: TdxDocumentModelPosition); virtual; abstract;
    procedure UpdateModelPositionByLogPosition(var APos: TdxDocumentModelPosition);
    property PieceTable: TdxPieceTable read FPieceTable;
  public
    constructor Create(APieceTable: TdxPieceTable); virtual;

    function MoveBack(const APos: TdxDocumentModelPosition): TdxDocumentModelPosition; virtual;
    function MoveForward(const APos: TdxDocumentModelPosition): TdxDocumentModelPosition; virtual;

    function IsEndOfDocument(const APos: TdxDocumentModelPosition): Boolean; virtual;
    function IsNewElement(ACurrentPosition: TdxDocumentModelPosition): Boolean; virtual;
    function IsStartOfDocument(const APos: TdxDocumentModelPosition): Boolean; virtual;

  end;

  { TdxCharactersDocumentModelIterator }

  TdxCharactersDocumentModelIterator = class(TdxPieceTableIterator)
  protected
    procedure MoveBackCore(var APos: TdxDocumentModelPosition); override;
    procedure MoveForwardCore(var APos: TdxDocumentModelPosition); override;

    procedure SkipForward(var APos: TdxDocumentModelPosition); virtual;
    procedure SkipBackward(var APos: TdxDocumentModelPosition); virtual;
  end;

  { TdxVisibleCharactersStopAtFieldsDocumentModelIterator }

  TdxVisibleCharactersStopAtFieldsDocumentModelIterator = class(TdxPieceTableIterator)
  end;

  { TdxWordsDocumentModelIteratorBase }

  TdxWordsDocumentModelIteratorBase = class abstract(TdxPieceTableIterator)
  strict private
    FCachedRunIndex: TdxRunIndex;
    FCachedRunText: string;
  public
    constructor Create(APieceTable: TdxPieceTable); override;

    function GetCharacter(APos: TdxDocumentModelPosition): Char;
    procedure SkipForward(AIterator: TdxPieceTableIterator; var APos: TdxDocumentModelPosition;
      APredicate: TdxPredicate<Char>);
    procedure SkipBackward(AIterator: TdxPieceTableIterator; var APos: TdxDocumentModelPosition;
      APredicate: TdxPredicate<Char>);
  end;

  { TdxWordsDocumentModelIterator }

  TdxWordsDocumentModelIterator = class(TdxWordsDocumentModelIteratorBase)
  strict private
    class var FNonWordSymbols: TList<Char>;
    class var FSpaces: TList<Char>;
    class var FSpecialSymbols: TList<Char>;
    class var FWordsDelimiter: TList<Char>;
    class constructor Initialize;
    class destructor Finalize;
    class function CreateSpacesList: TList<Char>;
    class function CreateSpecialSymbolsList: TList<Char>;
    class function CreateWordDelimiters: TList<Char>;
    class function CreateNonWordSymbols: TList<Char>;
    class procedure PopulateSpacesList(AList: TList<Char>);
    class procedure PopulateSpecialSymbolsList(AList: TList<Char>);
    class procedure PopulateWordDelimiters(AList: TList<Char>);
  protected
    procedure MoveBackCore(var APos: TdxDocumentModelPosition); override;
    procedure MoveForwardCore(var APos: TdxDocumentModelPosition); override;

    class property NonWordSymbols: TList<Char> read FNonWordSymbols;
    class property Spaces: TList<Char> read FSpaces;
    class property SpecialSymbols: TList<Char> read FSpecialSymbols;
    class property WordsDelimiter: TList<Char> read FWordsDelimiter;
  public
    function IsWordsDelimiter(const AItem: Char): Boolean;
    function IsNotNonWordsSymbols(const AItem: Char): Boolean;
    function IsSpace(const AItem: Char): Boolean;

    function IsInsideWord(ACurrentPosition: TdxDocumentModelPosition): Boolean;

  end;

  { TdxVisibleWordsIterator }

  TdxVisibleWordsIterator = class(TdxWordsDocumentModelIterator)
  end;

  { TdxParagraphsDocumentModelIterator }

  TdxParagraphsDocumentModelIterator = class(TdxPieceTableIterator)
  protected
    procedure MoveForwardCore(var APos: TdxDocumentModelPosition); override;
    procedure MoveBackCore(var APos: TdxDocumentModelPosition); override;
  end;

  { TdxDocumentsDocumentModelIterator }

  TdxDocumentsDocumentModelIterator = class(TdxPieceTableIterator)
  end;


implementation

uses
  Math, dxRichEdit.Utils.Characters, dxRichEdit.DocumentModel.Core;

{ TdxPieceTableIterator }

constructor TdxPieceTableIterator.Create(APieceTable: TdxPieceTable);
begin
  inherited Create;
  FPieceTable := APieceTable;
end;

function TdxPieceTableIterator.IsEndOfDocument(
  const APos: TdxDocumentModelPosition): Boolean;
begin
  Result := APos.LogPosition >= PieceTable.DocumentEndLogPosition;
end;

function TdxPieceTableIterator.IsNewElement(
  ACurrentPosition: TdxDocumentModelPosition): Boolean;
var
  APos: TdxDocumentModelPosition;
begin
  APos := ACurrentPosition.Clone;
  APos := MoveBack(APos);
  APos := MoveForward(APos);
  Result := APos = ACurrentPosition;
end;

function TdxPieceTableIterator.IsStartOfDocument(
  const APos: TdxDocumentModelPosition): Boolean;
begin
  Result := APos.LogPosition = PieceTable.DocumentStartLogPosition;
end;

function TdxPieceTableIterator.MoveBack(
  const APos: TdxDocumentModelPosition): TdxDocumentModelPosition;
begin
  Result := APos.Clone;
  if IsStartOfDocument(Result) then
    Exit;
  MoveBackCore(Result);
end;

function TdxPieceTableIterator.MoveForward(
  const APos: TdxDocumentModelPosition): TdxDocumentModelPosition;
begin
  Result := APos.Clone;
  if IsEndOfDocument(Result) then
    Exit;
  MoveForwardCore(Result);
end;

procedure TdxPieceTableIterator.UpdateModelPositionByLogPosition(
  var APos: TdxDocumentModelPosition);
var
  AParagraph: TdxParagraph;
  ALogPosition: TdxDocumentLogPosition;
  ARunStart: TdxDocumentLogPosition;
begin
  AParagraph := PieceTable.Paragraphs[APos.ParagraphIndex];
  ALogPosition := APos.LogPosition;
  if (ALogPosition >= APos.RunStartLogPosition) and (ALogPosition < APos.RunEndLogPosition) then
    Exit;
  while (ALogPosition > AParagraph.EndLogPosition) or (ALogPosition < AParagraph.LogPosition) do
  begin
    if ALogPosition < AParagraph.LogPosition then
      APos.ParagraphIndex := APos.ParagraphIndex - 1
    else
      APos.ParagraphIndex := APos.ParagraphIndex + 1;
    AParagraph := PieceTable.Paragraphs[APos.ParagraphIndex];
  end;
  APos.RunIndex := AParagraph.FirstRunIndex;
  ARunStart := AParagraph.LogPosition;
  while (ALogPosition >= ARunStart + PieceTable.Runs[APos.RunIndex].Length) do
  begin
    ARunStart := ARunStart + PieceTable.Runs[APos.RunIndex].Length;
    APos.RunIndex := APos.RunIndex + 1;
  end;
  APos.RunStartLogPosition := ARunStart;
end;

{ TdxWordsDocumentModelIteratorBase }

constructor TdxWordsDocumentModelIteratorBase.Create(
  APieceTable: TdxPieceTable);
begin
  inherited Create(APieceTable);
  FCachedRunIndex := -1;
end;

function TdxWordsDocumentModelIteratorBase.GetCharacter(
  APos: TdxDocumentModelPosition): Char;
var
  AIndex: Integer;
begin
  if APos.RunIndex <> FCachedRunIndex then
  begin
    FCachedRunIndex := APos.RunIndex;
    FCachedRunText := PieceTable.GetRunNonEmptyText(FCachedRunIndex);
  end;
  AIndex := Min(APos.LogPosition, APos.PieceTable.DocumentEndLogPosition) - APos.RunStartLogPosition;
  if AIndex = Length(FCachedRunText) then
  begin
    if AIndex > 0 then
      Dec(AIndex);
  end;
  Result := FCachedRunText[AIndex + 1]
end;

procedure TdxWordsDocumentModelIteratorBase.SkipBackward(
  AIterator: TdxPieceTableIterator; var APos: TdxDocumentModelPosition;
  APredicate: TdxPredicate<Char>);
begin
  while not IsStartOfDocument(APos) and APredicate(GetCharacter(APos)) do
    AIterator.MoveBackCore(APos);
end;

procedure TdxWordsDocumentModelIteratorBase.SkipForward(
  AIterator: TdxPieceTableIterator; var APos: TdxDocumentModelPosition;
  APredicate: TdxPredicate<Char>);
begin
  while not IsEndOfDocument(APos) and APredicate(GetCharacter(APos)) do
    AIterator.MoveForwardCore(APos);
end;

{ TdxWordsDocumentModelIterator }

class function TdxWordsDocumentModelIterator.CreateNonWordSymbols: TList<Char>;
begin
  Result := TList<Char>.Create;
  PopulateSpacesList(Result);
  PopulateSpecialSymbolsList(Result);
  PopulateWordDelimiters(Result);
end;

class function TdxWordsDocumentModelIterator.CreateSpacesList: TList<Char>;
begin
  Result := TList<Char>.Create;
  PopulateSpacesList(Result);
end;

class function TdxWordsDocumentModelIterator.CreateSpecialSymbolsList: TList<Char>;
begin
  Result := TList<Char>.Create;
  PopulateSpecialSymbolsList(Result);
end;

class function TdxWordsDocumentModelIterator.CreateWordDelimiters: TList<Char>;
begin
  Result := TList<Char>.Create;
  PopulateWordDelimiters(Result);
end;

class destructor TdxWordsDocumentModelIterator.Finalize;
begin
  FreeAndNil(FNonWordSymbols);
  FreeAndNil(FSpaces);
  FreeAndNil(FSpecialSymbols);
  FreeAndNil(FWordsDelimiter);
end;

class constructor TdxWordsDocumentModelIterator.Initialize;
begin
  FNonWordSymbols := CreateNonWordSymbols;
  FSpaces := CreateSpacesList;
  FSpecialSymbols := CreateSpecialSymbolsList;
  FWordsDelimiter := CreateWordDelimiters;
end;

function TdxWordsDocumentModelIterator.IsInsideWord(
  ACurrentPosition: TdxDocumentModelPosition): Boolean;
begin
  Assert(ACurrentPosition.IsValid);
  Result := not NonWordSymbols.Contains(GetCharacter(ACurrentPosition)) and
    not IsNewElement(ACurrentPosition);
end;

function TdxWordsDocumentModelIterator.IsNotNonWordsSymbols(
  const AItem: Char): Boolean;
begin
  Result := not NonWordSymbols.Contains(AItem);
end;

function TdxWordsDocumentModelIterator.IsSpace(const AItem: Char): Boolean;
begin
  Result := Spaces.Contains(AItem);
end;

function TdxWordsDocumentModelIterator.IsWordsDelimiter(
  const AItem: Char): Boolean;
begin
  Result := WordsDelimiter.Contains(AItem);
end;

procedure TdxWordsDocumentModelIterator.MoveBackCore(
  var APos: TdxDocumentModelPosition);
var
  AIterator: TdxCharactersDocumentModelIterator;
begin
  AIterator := TdxCharactersDocumentModelIterator.Create(PieceTable);
  try
    AIterator.MoveBackCore(APos);
    if SpecialSymbols.Contains(GetCharacter(APos)) then
      Exit;
    SkipBackward(AIterator, APos, IsSpace);
    if SpecialSymbols.Contains(GetCharacter(APos)) then
    begin
      if not IsStartOfDocument(APos) then
        AIterator.MoveForwardCore(APos);
      Exit;
    end;
    if WordsDelimiter.Contains(GetCharacter(APos)) then
    begin
      SkipBackward(AIterator, APos, IsWordsDelimiter);
      if not IsStartOfDocument(APos) or not WordsDelimiter.Contains(GetCharacter(APos)) then
        AIterator.MoveForwardCore(APos);
    end
    else
    begin
      SkipBackward(AIterator, APos, IsNotNonWordsSymbols);
      if not IsStartOfDocument(APos) or NonWordSymbols.Contains(GetCharacter(APos)) then
        AIterator.MoveForwardCore(APos);
    end;
  finally
    AIterator.Free;
  end;
end;

procedure TdxWordsDocumentModelIterator.MoveForwardCore(
  var APos: TdxDocumentModelPosition);
var
  AIterator: TdxCharactersDocumentModelIterator;
begin
  AIterator := TdxCharactersDocumentModelIterator.Create(PieceTable);
  try
    if SpecialSymbols.Contains(GetCharacter(APos)) then
    begin
      AIterator.MoveForwardCore(APos);
      Exit;
    end;
    if WordsDelimiter.Contains(GetCharacter(APos)) then
      SkipForward(AIterator, APos, IsWordsDelimiter)
    else
      SkipForward(AIterator, APos, IsNotNonWordsSymbols);
    SkipForward(AIterator, APos, IsSpace);
  finally
    AIterator.Free;
  end;
end;

class procedure TdxWordsDocumentModelIterator.PopulateSpacesList(
  AList: TList<Char>);
begin
  AList.Add(TdxCharacters.Space);
  AList.Add(TdxCharacters.NonBreakingSpace);
  AList.Add(TdxCharacters.EnSpace);
  AList.Add(TdxCharacters.EmSpace);
  AList.Add(TdxCharacters.QmSpace);
end;

class procedure TdxWordsDocumentModelIterator.PopulateSpecialSymbolsList(
  AList: TList<Char>);
begin
  AList.Add(TdxCharacters.PageBreak);
  AList.Add(TdxCharacters.LineBreak);
  AList.Add(TdxCharacters.ColumnBreak);
  AList.Add(TdxCharacters.SectionMark);
  AList.Add(TdxCharacters.ParagraphMark);
  AList.Add(TdxCharacters.TabMark);
end;

class procedure TdxWordsDocumentModelIterator.PopulateWordDelimiters(
  AList: TList<Char>);
begin
  AList.Add(TdxCharacters.Dot);
  AList.Add(',');
  AList.Add('!');
  AList.Add('@');
  AList.Add('#');
  AList.Add('$');
  AList.Add('%');
  AList.Add('^');
  AList.Add('&');
  AList.Add('*');
  AList.Add('(');
  AList.Add(')');
  AList.Add(TdxCharacters.Dash);
  AList.Add(TdxCharacters.EmDash);
  AList.Add(TdxCharacters.EnDash);
  AList.Add(TdxCharacters.Hyphen);
  AList.Add(TdxCharacters.Underscore);
  AList.Add('=');
  AList.Add('+');
  AList.Add('[');
  AList.Add(']');
  AList.Add('{');
  AList.Add('}');
  AList.Add('\');
  AList.Add('|');
  AList.Add(';');
  AList.Add(':');
  AList.Add('''');
  AList.Add('"');
  AList.Add('<');
  AList.Add('>');
  AList.Add('/');
  AList.Add('?');
  AList.Add('`');
  AList.Add('~');
  AList.Add(TdxCharacters.TrademarkSymbol);
  AList.Add(TdxCharacters.CopyrightSymbol);
  AList.Add(TdxCharacters.RegisteredTrademarkSymbol);
  AList.Add(TdxCharacters.Ellipsis);
  AList.Add(TdxCharacters.LeftDoubleQuote);
  AList.Add(TdxCharacters.LeftSingleQuote);
  AList.Add(TdxCharacters.RightDoubleQuote);
  AList.Add(TdxCharacters.OpeningDoubleQuotationMark);
  AList.Add(TdxCharacters.OpeningSingleQuotationMark);
  AList.Add(TdxCharacters.ClosingDoubleQuotationMark);
  AList.Add(TdxCharacters.ClosingSingleQuotationMark);
end;

{ TdxCharactersDocumentModelIterator }

procedure TdxCharactersDocumentModelIterator.MoveBackCore(
  var APos: TdxDocumentModelPosition);
begin
  SkipBackward(APos);
  UpdateModelPositionByLogPosition(APos);
end;

procedure TdxCharactersDocumentModelIterator.MoveForwardCore(
  var APos: TdxDocumentModelPosition);
begin
  SkipForward(APos);
  UpdateModelPositionByLogPosition(APos);
end;

procedure TdxCharactersDocumentModelIterator.SkipBackward(
  var APos: TdxDocumentModelPosition);
begin
  APos.LogPosition := APos.LogPosition - 1;
end;

procedure TdxCharactersDocumentModelIterator.SkipForward(
  var APos: TdxDocumentModelPosition);
begin
  APos.LogPosition := APos.LogPosition + 1;
end;

{ TdxParagraphsDocumentModelIterator }

procedure TdxParagraphsDocumentModelIterator.MoveBackCore(
  var APos: TdxDocumentModelPosition);
var
  AParagraph: TdxParagraph;
begin
  AParagraph := PieceTable.Paragraphs[APos.ParagraphIndex];
  if (APos.LogPosition = AParagraph.LogPosition) and (APos.ParagraphIndex > 0) then
    APos.ParagraphIndex := APos.ParagraphIndex - 1;
  AParagraph := PieceTable.Paragraphs[APos.ParagraphIndex];
  APos.LogPosition := AParagraph.LogPosition;
  APos.RunStartLogPosition := APos.LogPosition;
  APos.RunIndex := AParagraph.FirstRunIndex;
end;

procedure TdxParagraphsDocumentModelIterator.MoveForwardCore(
  var APos: TdxDocumentModelPosition);
var
  ALastParagraphIndex: TdxParagraphIndex;
  AParagraph: TdxParagraph;
  ALastParagraph: TdxParagraph;
begin
  ALastParagraphIndex := PieceTable.Paragraphs.Last.Index;
  if APos.ParagraphIndex > ALastParagraphIndex then
    Exit;
  APos.ParagraphIndex := APos.ParagraphIndex + 1;
  if APos.ParagraphIndex <= ALastParagraphIndex then
  begin
    AParagraph := PieceTable.Paragraphs[APos.ParagraphIndex];
    APos.LogPosition := AParagraph.LogPosition;
    APos.RunIndex := AParagraph.FirstRunIndex;
  end
  else
  begin
    ALastParagraph := PieceTable.Paragraphs.Last;
    APos.LogPosition := ALastParagraph.EndLogPosition + 1;
    APos.RunIndex := ALastParagraph.LastRunIndex + 1;
  end;
  APos.RunStartLogPosition := APos.LogPosition;
end;

end.
