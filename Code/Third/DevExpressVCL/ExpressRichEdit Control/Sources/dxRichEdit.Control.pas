{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Control;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Types, Windows, Messages, Classes, SysUtils, Graphics, Generics.Collections, Controls,
  StdCtrls, Forms, ActiveX, dxCoreClasses, cxGeometry, cxLookAndFeels, cxGraphics, cxControls, cxClasses,

  dxRichEdit.Utils.DataObject,
  dxRichEdit.Utils.BatchUpdateHelper,
  dxRichEdit.Utils.BackgroundThreadUIUpdater,
  dxRichEdit.InnerControl,
  dxRichEdit.DocumentModel.Core,
  dxRichEdit.DocumentModel.PieceTable,
  dxRichEdit.DocumentModel.Selection,
  dxRichEdit.DocumentLayout.UnitConverter,
  dxRichEdit.Control.DragAndDrop.Types,
  dxRichEdit.Control.HotZones,
  dxRichEdit.View.Core,
  dxRichEdit.View.ViewInfo,
  dxRichEdit.View.Simple,
  dxRichEdit.Platform.Win.Painter,
  dxRichEdit.Platform.Win.Control,
  dxRichEdit.Platform.Win.Scroll,
  dxRichEdit.Platform.Font,
  dxMessages,
  dxRichEdit.DocumentModel.ParagraphFormatting;

type
  TdxCustomRichEditControl = class;

  TdxCustomMark = class(TObject);
  TdxRichEditHoverPainters = class(TObject);
  IdxDecoratorPainter = interface(IUnknown)
  end;

  TdxCustomMarkVisualInfoCollection = class;

  IdxCustomMarkExporter = interface
  ['{49117034-859E-435A-B1AA-60B5DDBF60D8}']
    function GetCustomMarkVisualInfoCollection: TdxCustomMarkVisualInfoCollection;
    procedure ExportCustomMarkBox(ACustomMark: TdxCustomMark; const ABounds: TRect);
    property CustomMarkVisualInfoCollection: TdxCustomMarkVisualInfoCollection read GetCustomMarkVisualInfoCollection;
  end;

  { TdxCustomMarkVisualInfo }

  TdxCustomMarkVisualInfo = class
  private
    FBounds: TRect;
    FCustomMark: TdxCustomMark;
    function GetUserData: TObject; 
  protected
    property CustomMark: TdxCustomMark  read FCustomMark;
  public
    constructor Create(ACustomMark: TdxCustomMark; ABounds: TRect);

    property UserData: TObject read GetUserData; 
    property Bounds: TRect read FBounds write FBounds;
  end;

  { TdxCustomMarkVisualInfoCollection }

  TdxCustomMarkVisualInfoCollection = class(TList<TdxCustomMarkVisualInfo>); 

  { TdxRichEditPaintersBase<TdxLayoutItem, TdxPainter> }

  TdxRichEditPaintersBase<TLayoutItem, TPainter> = class
  private
    FDefaultPainter: TPainter;
    FPainters: TObjectDictionary<TClass, TPainter>;
  protected
    property DefaultPainter: TPainter read FDefaultPainter;
    property Painters: TObjectDictionary<TClass, TPainter> read FPainters;
  public
    constructor Create;
    destructor Destroy; override;

    procedure AddDefault(APainter: TPainter);
    procedure Add(AType: TClass; APainter: TPainter);
    procedure RemoveDefault;
    procedure Remove(AType: TClass);
    procedure Clear;
    function Get(AType: TClass): TPainter;
  end;

  { TdxOfficeSelectionPainter }

  TdxOfficeSelectionPainter = class abstract (TcxIUnknownObject)
  strict private
    FCanvas: TcxCanvas;
  protected
    procedure FillRectangle(const R: TRect); virtual; abstract;
    procedure DrawRectangle(const R: TRect); virtual; abstract;
    procedure DrawLine(const AFrom, ATo: TPoint); virtual; abstract;
  public
    constructor Create(ACanvas: TcxCanvas); virtual;

    property Canvas: TcxCanvas read FCanvas;

    function TryPushRotationTransform(ACenter: TPoint; const AAngleInDegrees: Double): Boolean;
    procedure PushRotationTransform(ACenter: TPoint; const AAngleInDegrees: Double);
    procedure PopTransform; 
  end;

  { IdxRectangularObjectHotZoneVisitor }

  IdxRectangularObjectHotZoneVisitor = interface(IdxHotZoneVisitor)
  end;

  { TdxRichEditSelectionPainter }

  TdxRichEditSelectionPainter = class(TdxOfficeSelectionPainter, IdxSelectionPainter, IdxRectangularObjectHotZoneVisitor, IdxHotZoneVisitor) 
  const
    SelectionColor = $D39396;
    SelectionAlpha = 128;
  public
    procedure Draw(AViewInfo: TdxRowSelectionLayoutBase); overload; virtual; 
    procedure Draw(AViewInfo: TdxRectangularObjectSelectionLayout); overload; virtual; 
    procedure Draw(AViewInfo: TdxFloatingObjectAnchorSelectionLayout); overload; virtual; 
    procedure DrawHotZone(AHotZone: IdxHotZone); virtual; 
  end;

  { TdxSemitransparentSelectionPainter }

  TdxSemitransparentSelectionPainter = class(TdxRichEditSelectionPainter)
  protected
    procedure FillRectangle(const R: TRect); override;
    procedure DrawRectangle(const R: TRect); override;
    procedure DrawLine(const AFrom, ATo: TPoint); override;
  end;

  TdxRichEditSelectionPainters = class(TdxRichEditPaintersBase<IdxSelectionLayoutItem, TdxRichEditSelectionPainter>);

  { TdxHdcOriginModifier }

  TdxHdcOriginModifier = class
  type
    TMode = (Replace, Combine);
  private
    FCanvas: TcxCanvas;
    FOldOrigin: TPoint;
  protected
    procedure RestoreHDC; virtual;
  public
    constructor Create(ACanvas: TcxCanvas; const ANewOrigin: TPoint; AZoomFactor: Single; AMode: TMode = TMode.Replace);
    destructor Destroy; override;

    procedure SetHDCOrigin(const ANewOrigin: TPoint; AZoomFactor: Single; AMode: TMode); virtual;
  end;

  { TdxHdcZoomModifier }

  TdxHdcZoomModifier = class
  private
    FCanvas: TcxCanvas;
    FOldWindowExtent: TSize;
  protected
    procedure RestoreHDC; virtual;
  public
    constructor Create(ACanvas: TcxCanvas; AZoomFactor: Single);
    destructor Destroy; override;

    procedure ZoomHDC(AZoomFactor: Single); virtual;
  end;

  { TdxRichEditViewPainter }

  TdxRichEditViewPainter = class
  strict private
    FView: TdxRichEditView;
    FControl: TdxCustomRichEditControl;
    FSelectionPainters: TdxRichEditSelectionPainters;
    FHoverPainters: TdxRichEditHoverPainters;
    FDecoratorPainters: TList<IdxDecoratorPainter>;
    FCanvas: TcxCanvas;
    FZoomModifier: TdxHdcZoomModifier;
    FPhysicalLeftInvisibleWidth: Integer;
    FMinReadableTextHeight: Integer;
    FOriginModifier: TdxHdcOriginModifier;
    FOldClipBounds: TRect;

    FSaveDCIndex: Integer;

    function GetDocumentModel: TdxDocumentModel;
    function GetLookAndFeel: TcxLookAndFeel;
  protected
    procedure BeginDraw(ACanvas: TcxCanvas); virtual;
    procedure EndDraw; virtual;
    procedure AddSelectionPainters(ACanvas: TcxCanvas);
    procedure CacheInitialize(ACanvas: TcxCanvas);
    procedure CacheDispose;
    function BeginDrawPagesContent: TdxTransformMatrix; virtual;
    procedure EndDrawPagesContent; virtual;

    property DecoratorPainters: TList<IdxDecoratorPainter> read FDecoratorPainters;
    property SelectionPainters: TdxRichEditSelectionPainters read FSelectionPainters;
    property HoverPainters: TdxRichEditHoverPainters read FHoverPainters;
    property Control: TdxCustomRichEditControl read FControl;
    property DocumentModel: TdxDocumentModel read GetDocumentModel;
    property Graphics: TcxCanvas read FCanvas; 
  public
    constructor Create(AView: TdxRichEditView); virtual;
    destructor Destroy; override;

    procedure AddDecoratorPainter(APainter: IdxDecoratorPainter);

    property View: TdxRichEditView read FView;
    property LookAndFeel: TcxLookAndFeel read GetLookAndFeel;

    procedure BeginDrawPageContent(APage: TdxPageViewInfo; ATransform: TdxTransformMatrix); virtual; 
    procedure ClipPageContent(APage: TdxPageViewInfo; APainter: TdxPainter); virtual; 
    procedure RestoreOldClipBounds(APainter: TdxPainter); virtual; 

    procedure EndDrawPageContent(APage: TdxPageViewInfo; ATransform: TdxTransformMatrix); virtual; 
    procedure BeginDrawInPixels(ACanvas: TcxCanvas); virtual; 
    procedure EndDrawInPixels; virtual; 

    procedure DrawPageContent(APage: TdxPageViewInfo; APainter: TdxPainter); virtual; 
    function GetPageBounds(APage: TdxPageViewInfo): TRect; virtual; 
    procedure DrawPagesContent(ATransform: TdxTransformMatrix); 
    procedure DrawBoxesInPixels(ACustomMarkExporter: IdxCustomMarkExporter); virtual; 
    procedure Draw(ACanvas: TcxCanvas; ACustomMarkExporter: IdxCustomMarkExporter); virtual; 
    procedure ApplyGraphicsTransform(ACanvas: TcxCanvas; ACustomMarkVisualInfoCollection: TdxCustomMarkVisualInfoCollection); virtual; 
    procedure DrawPageSelection(AGr: TdxGraphics; APage: TdxPageViewInfo); virtual; 
    procedure DrawPageSelectionCore(ASelection: IdxSelectionLayoutItem); 
    procedure DrawHover(APainter: TdxPainter); virtual; 
    procedure DrawEmptyPages(ACanvas: TcxCanvas); virtual; 
    procedure DrawEmptyPage(ACanvas: TcxCanvas; APage: TdxPageViewInfo); virtual; abstract; 
  end;

  { TdxSimpleViewPainter }

  TdxSimpleViewPainter = class(TdxRichEditViewPainter) 
  public
    procedure DrawEmptyPages(ACanvas: TcxCanvas); override; 
    procedure DrawEmptyPage(ACanvas: TcxCanvas; APage: TdxPageViewInfo); override; 
  end;

  { TdxRichEditControlPainter }

  DrawDelegate = reference to procedure (ACanvas: TcxCanvas; ACustomMarkExporter: IdxCustomMarkExporter);
  DrawAtPageDelegate = procedure (ACanvas: TcxCanvas) of object;

  TdxRichEditControlPainter = class
  strict private
    FControl: TdxCustomRichEditControl;
    FDeferredDraws: TList<DrawDelegate>;
    function GetLookAndFeel: TcxLookAndFeel;
  protected
    function GetCaret: TdxCaret; virtual;
    function GetDragCaret: TdxDragCaret; virtual;
    function GetActiveView: TdxRichEditView; virtual;

    procedure DrawCaret(ADrawCaret: DrawDelegate); overload;
    procedure DrawCaretCore(ACanvas: TcxCanvas; ACustomMarkExporter: IdxCustomMarkExporter); overload; virtual;
    procedure DrawDragCaretCore(ACanvas: TcxCanvas; ACustomMarkExporter: IdxCustomMarkExporter); overload; virtual;
    procedure DrawCaretCore(ACaret: TdxCaret; ACanvas: TcxCanvas; ACustomMarkExporter: IdxCustomMarkExporter); overload; virtual;
    function GetActualBounds(ACanvas: TcxCanvas; APage: TdxPageViewInfo; const ABounds: TRect): TRect; virtual;
    procedure DrawReversibleCore(APage: TdxPageViewInfo; ADraw: DrawDelegate); virtual;
    procedure DrawReversibleFrameCore(ACanvas: TcxCanvas; const ABounds: TRect; APage: TdxPageViewInfo); virtual;
    procedure DrawReversibleHorizontalLineCore(ACanvas: TcxCanvas; Y: Integer; APage: TdxPageViewInfo); virtual;
    procedure DrawReversibleVerticalLineCore(ACanvas: TcxCanvas; X: Integer; APage: TdxPageViewInfo); virtual;
    procedure PerformRenderingInUnits(ACanvas: TcxCanvas; ACustomMarkExporter: IdxCustomMarkExporter; ADraw: DrawDelegate); virtual;
    procedure PerformRenderingInPixels(ACanvas: TcxCanvas; ACustomMarkExporter: IdxCustomMarkExporter; ADraw: DrawDelegate); virtual;
    procedure RenderDocument(ACanvas: TcxCanvas; ACustomMarkExporter: IdxCustomMarkExporter); virtual;
    procedure RenderDocumentDecorators(ACanvas: TcxCanvas; ACustomMarkExporter: IdxCustomMarkExporter); virtual;

    property DeferredDraws: TList<DrawDelegate> read FDeferredDraws;
  public
    constructor Create(AControl: TdxCustomRichEditControl); virtual;
    destructor Destroy; override;

    procedure Draw(ACanvas: TcxCanvas; ACustomMarkExporter: IdxCustomMarkExporter); virtual;
    procedure DrawCaret; overload; virtual;
    procedure DrawDragCaret; overload; virtual;
    procedure DrawReversibleHorizontalLine(Y: Integer; APage: TdxPageViewInfo); virtual;
    procedure DrawReversibleVerticalLine(X: Integer; APage: TdxPageViewInfo); virtual;
    procedure RegisterDeferredDraw(ADraw: DrawDelegate);
    procedure DeferredDraw(APage: TdxPageViewInfo; ADrawAtPage: DrawAtPageDelegate);
    procedure DeferredDrawReversibleFrame(const ABounds: TRect; APage: TdxPageViewInfo);

    property Control: TdxCustomRichEditControl read FControl;
    property LookAndFeel: TcxLookAndFeel read GetLookAndFeel;
    property Caret: TdxCaret read GetCaret;
    property DragCaret: TdxDragCaret read GetDragCaret;
    property ActiveView: TdxRichEditView read GetActiveView;
  end;

  { TdxEmptyRichEditControlPainter }

  TdxEmptyRichEditControlPainter = class(TdxRichEditControlPainter) 
  public
    procedure Draw(ACanvas: TcxCanvas; ACustomMarkExporter: IdxCustomMarkExporter); override; 
    procedure DrawCaret; override; 
  end;

  TdxRichEditViewBackgroundPainter = class 
  private
    FView: TdxRichEditView;
  protected
    function GetActualPageBackColor: TColor; virtual;
  public
    constructor Create(AView: TdxRichEditView);
    procedure Draw(ACanvas: TcxCanvas; const ABounds: TRect); virtual;

    property ActualPageBackColor: TColor read GetActualPageBackColor;
    property View: TdxRichEditView read FView;
  end;

  { TdxCustomMarkExporter }

  TdxCustomMarkExporter = class(TcxIUnknownObject, IdxCustomMarkExporter) 
  private
    FVisualInfoCollection: TdxCustomMarkVisualInfoCollection;
    function GetCustomMarkVisualInfoCollection: TdxCustomMarkVisualInfoCollection;
  public
    constructor Create; 
    destructor Destroy; override;

    procedure ExportCustomMarkBox(ACustomMark: TdxCustomMark; const ABounds: TRect); virtual; 

    property CustomMarkVisualInfoCollection: TdxCustomMarkVisualInfoCollection read GetCustomMarkVisualInfoCollection; 
  end;

  { TdxRichEditViewRepository }

  TdxRichEditViewRepository = class(TdxRichEditCustomViewRepository)
  private
    function GetSimpleView: TdxSimpleView;
    procedure SetSimpleView(const Value: TdxSimpleView);
  protected
    procedure CreateViews; override;
  published
    property Simple: TdxSimpleView read GetSimpleView write SetSimpleView;
  end;

  { TdxCustomRichEditControl }

  TdxAutoSizeMode = (None, Horizontal, Vertical, Both);

  TdxCustomRichEditControl = class(TdxVCLControl,
    IdxInnerRichEditControlOwner,
    IdxRichEditControl,
    IdxBatchUpdateable,
    IdxInnerRichEditDocumentServerOwner,
    IDropTarget,
    IDropSource)
  strict private
    FDragObject: IDataObject;
  protected 
    procedure CreateDragCaret;
    procedure DestroyDragCaret;
    procedure DrawDragCaret;
    function DoDragDrop(const AData: IDataObject; AllowedEffects: TdxDragDropEffects): TdxDragDropEffects;
    function GetDragCaret: TdxDragCaret;
    function GetOvertype: Boolean;
    procedure SetOvertype(const Value: Boolean);
    procedure UpdateControlAutoSize;
    procedure ShowParagraphForm(AParagraphProperties: TdxMergedParagraphProperties; ACallback: TdxShowParagraphFormCallback; ACallbackData: TObject); virtual;

    function IDropTarget.DragEnter = OleDragEnter;
    function OleDragEnter(const dataObj: IDataObject; grfKeyState: Longint; pt: TPoint; var dwEffect: Longint): HResult; stdcall;
    function IDropTarget.DragOver = OleDragOver;
    function OleDragOver(grfKeyState: Longint; pt: TPoint; var dwEffect: Longint): HResult; stdcall;
    function IDropTarget.DragLeave = OleDragLeave;
    function OleDragLeave: HResult; stdcall;
    function IDropTarget.Drop = OleDrop;
    function OleDrop(const dataObj: IDataObject; grfKeyState: Longint; pt: TPoint; var dwEffect: Longint): HResult; stdcall;

    function IDropSource.QueryContinueDrag = OleQueryContinueDrag;
    function OleQueryContinueDrag(fEscapePressed: BOOL;
      grfKeyState: Longint): HResult; stdcall;
    function IDropSource.GiveFeedback = OleGiveFeedback;
    function OleGiveFeedback(dwEffect: Longint): HResult; stdcall;

    function CalcBestSize(AWidth, AHeight: Integer; AFixedWidth: Boolean): TSize;
    function CalcViewBestSize(AFixedWidth: Boolean): TSize; virtual;
    function CalculateActualViewBounds(const APreviousViewBounds: TRect): TRect;
    function CalculateHorizontalScrollbarHeight: Integer;
    function CalculateVerticalScrollbarWidth: Integer;

    procedure HideCaret;
    procedure ShowCaret;
    procedure OnViewPaddingChanged;
    function GetCursor: TCursor;
    function GetInnerControl: IdxInnerControl;
    function GetRichEditControl: TObject;
    function CreateRichEditViewVerticalScrollController(ARichEditView: TdxRichEditView): TdxRichEditViewVerticalScrollController;
    function CreateRichEditViewHorizontalScrollController(ARichEditView: TdxRichEditView): TdxRichEditViewHorizontalScrollController;
    function CreatePlatformSpecificScrollBarAdapter: IdxPlatformSpecificScrollBarAdapter;
    procedure RedrawEnsureSecondaryFormattingComplete(Action: TdxRefreshAction); overload;
    procedure SetCursor(Value: TCursor);
    procedure UpdateUIFromBackgroundThread(AMethod: TdxAction);
    function UseStandardDragDropMode: Boolean;

    procedure ActivateViewPlatformSpecific(AView: TdxRichEditView); virtual;
    function CreateViewRepository: TdxRichEditCustomViewRepository; virtual;
    procedure IdxInnerRichEditControlOwner.OnResize = BoundsChanged;
    procedure OnResizeCore; overload;
    procedure OnResizeCore(AEnsureCaretVisibleOnResize: Boolean); overload;
    procedure OnZoomFactorChangingPlatformSpecific;
    procedure IdxInnerRichEditControlOwner.OnActiveViewBackColorChanged = OnBackColorChanged;

    procedure CustomRefresh;
    procedure Redraw; overload;
    procedure Redraw(AAfterEndUpdate: Boolean); overload; virtual;
    procedure Redraw(AAction: TdxRefreshAction); overload;
    procedure RedrawAfterEndUpdate;
    procedure RedrawEnsureSecondaryFormattingComplete; overload; virtual;

    procedure RaiseDeferredEvents(AChangeActions: TdxDocumentModelChangeActions);
    procedure ApplyChangesCorePlatformSpecific(AChangeActions: TdxDocumentModelChangeActions); virtual;
    function CreateMeasurementAndDrawingStrategy(ADocumentModel: TdxDocumentModel): TdxMeasurementAndDrawingStrategy; virtual;
  strict private
    class var FSystemCaretBitmap: TBitmap;
    class constructor Initialize;
    class destructor Finalize;
  strict private
    FAutoSizeMode: TdxAutoSizeMode;
    FCaret: TdxCaret;
    FCaretTimer: TcxTimer;
    FDragCaret: TdxDragCaret;
    FInnerControl: TdxInnerRichEditControl;
    FIsInsideRefresh: Boolean;
    FInsideResize: Boolean;
    FIsUpdateAutoSizeMode: Boolean;
    FLastSize: TSize;
    FContentSize: TSize;
    FKeyboardController: TdxRichEditKeyboardCustomController;
    FMouseController: TdxRichEditMouseCustomController;
    FControlCreated: Boolean;
    FWantTabs: Boolean;
    FWantReturns: Boolean;

    FPainter: TdxRichEditControlPainter;
    FViewPainter: TdxRichEditViewPainter;
    FBackgroundPainter: TdxRichEditViewBackgroundPainter;

    FHorizontalScrollBar: TdxOfficeScrollbar;
    FVerticalScrollBar: TdxOfficeScrollbar;
    function CalculateInitialClientBounds: TRect;
    function GetActiveView: TdxRichEditView;
    function GetBackgroundThreadUIUpdater: TdxBackgroundThreadUIUpdater;
    function GetLayoutUnit: TdxDocumentLayoutUnit;
    function GetDocumentModel: TdxDocumentModel;
    function GetViews: TdxRichEditViewRepository;
    procedure SetAutoSizeMode(const Value: TdxAutoSizeMode);
    procedure SetLayoutUnit(const Value: TdxDocumentLayoutUnit);
    procedure SetViews(const Value: TdxRichEditViewRepository);
  private
    FLastValidClientRectangle: TRect;
    FSizeGripBounds: TRect;
    FClientBounds: TRect;
    FViewBounds: TRect;
    procedure CaretTimerHandler(Sender: TObject);
    function GetControlDeferredChanges: TdxRichEditControlDeferredChanges;
    function GetMeasurementAndDrawingStrategy: TdxMeasurementAndDrawingStrategy;
    function GetDpiX: Single;
    function GetDpiY: Single;
    function GetOptions: TdxRichEditDocumentServerOptions;
    procedure SetOptions(const Value: TdxRichEditDocumentServerOptions);
    function GetModified: Boolean;
    procedure SetModified(const Value: Boolean);
    procedure SetWantReturns(const Value: Boolean);
    procedure SetWantTabs(const Value: Boolean);
    procedure CMNCSizeChanged(var Message: TMessage); message DXM_NCSIZECHANGED; 
    procedure WMGetDlgCode(var Message: TWMGetDlgCode); message WM_GETDLGCODE;
  protected
    procedure ApplyFontAndForeColor; virtual;
    procedure OnBackColorChanged; virtual;

    function CreateInnerControl: TdxInnerRichEditControl; virtual;
    function CreateKeyboardController: TdxRichEditKeyboardCustomController; virtual;
    function CreateMouseController: TdxRichEditMouseCustomController; virtual;

    procedure CreateViewPainter(AView: TdxRichEditView); virtual;
    procedure CreateBackgroundPainter(AView: TdxRichEditView); virtual;
    procedure RecreateBackgroundPainter;

    procedure BeginInitialize;
    procedure EndInitialize;
    procedure EndInitializeCommon;
    procedure DisposeCommon; virtual;
    procedure ForceHandleLookAndFeelChanged; virtual;
    function GetPixelPhysicalBounds(APageViewInfo: TdxPageViewInfo; ALogicalBounds: TRect): TRect;
    procedure SubscribeInnerControlEvents; virtual;
    procedure UnsubscribeInnerControlEvents; virtual;
    function DefaultLayoutUnit: TdxDocumentLayoutUnit; virtual;
    function IsUpdateLocked: Boolean;
    procedure PerformDeferredUIUpdates(ADeferredUpdater: TdxDeferredBackgroundThreadUIUpdater); virtual;

    procedure ResizeView(AEnsureCaretVisibleOnResize: Boolean); virtual;

    function CreateVerticalScrollBar: IdxOfficeScrollbar; virtual;
    function CreateHorizontalScrollBar: IdxOfficeScrollbar; virtual;
    function CreateScrollBarCore(AScrollBar: TdxOfficeScrollbar): IdxOfficeScrollbar; virtual;
    procedure UpdateScrollbarsVisibility; virtual;
    function CalculateVerticalScrollbarVisibility: Boolean; virtual;
    function CalculateHorizontalScrollbarVisibility: Boolean; virtual;
    procedure UpdateVerticalScrollBar(AAvoidJump: Boolean); virtual;

    function GetVerticalScrollbarBounds(AWidth, AOffset, AHorizontalScrollbarHeight: Integer): TRect; virtual;
    function PerformResize(const AInitialClientBounds: TRect; AEnsureCaretVisibleOnResize: Boolean): Boolean; virtual;
    function CalculateViewPixelBounds(AClientBounds: TRect): TRect; virtual;
    function CalculateViewBounds(AClientBounds: TRect): TRect; virtual;

    procedure CreateWnd; override;
    procedure DestroyWnd; override;

    function GetClientBounds: TRect; override;

    procedure DoPaint; override;
    procedure BoundsChanged; override;
    procedure EraseBackground(ACanvas: TcxCanvas; const ARect: TRect); override;
    function HasBackground: Boolean; override;
    function GetSizeGripBounds: TRect; override;
    procedure InitControl; override;

    procedure FocusEnter; override;
    procedure FocusLeave; override;

    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X: Integer;
      Y: Integer); override;
    procedure MouseMove(Shift: TShiftState; X: Integer; Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X: Integer; Y: Integer); override;
    function InternalMouseWheel(Shift: TShiftState; WheelDelta: Integer; MousePos: TPoint): Boolean; override;

    procedure AddKeyboardService(const AService: IdxKeyboardHandlerService);
    procedure RemoveKeyboardService(const AService: IdxKeyboardHandlerService);
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure KeyPress(var Key: Char); override;
    procedure KeyUp(var Key: Word; Shift: TShiftState); override;

    class function CreateSystemCaretBitmap: TcxBitmap;
    procedure EnsureSystemCaretCreated; virtual;
    procedure HideCaretCore;
    function ShouldShowCaret: Boolean;
    procedure ShowCaretCore;
    procedure ShowSystemCaret; virtual;
    procedure HideSystemCaret; virtual;
    procedure StartCaretBlinking;
    procedure StopCaretBlinking;
    procedure StartCaretTimer;
    procedure StopCaretTimer;

    function GetIsUpdateLocked: Boolean;
    function GetBatchUpdateHelper: TdxBatchUpdateHelper;
    procedure BeginUpdate;
    procedure CancelUpdate;
    procedure EndUpdate;

    property KeyboardController: TdxRichEditKeyboardCustomController read FKeyboardController;
    property MouseController: TdxRichEditMouseCustomController read FMouseController;
    property MeasurementAndDrawingStrategy: TdxMeasurementAndDrawingStrategy read GetMeasurementAndDrawingStrategy;
    property InnerControl: TdxInnerRichEditControl read FInnerControl;
    property Painter: TdxRichEditControlPainter read FPainter;
    property BackgroundPainter: TdxRichEditViewBackgroundPainter read FBackgroundPainter;
    property ViewPainter: TdxRichEditViewPainter read FViewPainter;
    property DpiX: Single read GetDpiX;
    property DpiY: Single read GetDpiY;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure SetBounds(ALeft, ATop, AWidth, AHeight: Integer); override;

    procedure SaveDocument(const AFileName: string);
    procedure LoadDocument(const AFileName: string);

    property ActiveView: TdxRichEditView read GetActiveView;
    property AutoSizeMode: TdxAutoSizeMode read FAutoSizeMode write SetAutoSizeMode default TdxAutoSizeMode.None;
    property Caret: TdxCaret read FCaret;
    property DocumentModel: TdxDocumentModel read GetDocumentModel;
    property DragCaret: TdxDragCaret read FDragCaret;
    property LayoutUnit: TdxDocumentLayoutUnit read GetLayoutUnit write SetLayoutUnit;
    property DocumentModelModified: Boolean read GetModified write SetModified;
    property BackgroundThreadUIUpdater: TdxBackgroundThreadUIUpdater read GetBackgroundThreadUIUpdater;
    property ControlDeferredChanges: TdxRichEditControlDeferredChanges read GetControlDeferredChanges;
    property Views: TdxRichEditViewRepository read GetViews write SetViews;
    property ViewBounds: TRect read FViewBounds;
    property WantReturns: Boolean read FWantReturns write SetWantReturns default True;
    property WantTabs: Boolean read FWantTabs write SetWantTabs default True;
    property Options: TdxRichEditDocumentServerOptions read GetOptions write SetOptions;

    property HorizontalScrollBar: TdxOfficeScrollbar read FHorizontalScrollBar;
    property VerticalScrollBar: TdxOfficeScrollbar read FVerticalScrollBar;
  end;

  { TdxRichEditControl }

  TdxRichEditControl = class(TdxCustomRichEditControl)
  published
    property Align;
    property Anchors;
    property AutoSizeMode;
    property BorderStyle default cxcbsDefault;
    property Color;
    property DragMode;
    property Enabled;
    property Options;
    property PopupMenu;
    property Views;
    property TabOrder;
    property TabStop default True;
    property Visible;

    property OnContextPopup;
  end;

  { TdxGdiPlusMeasurementAndDrawingStrategyBase }

  TdxGdiPlusMeasurementAndDrawingStrategyBase = class(TdxMeasurementAndDrawingStrategy) 
  end;

  { TdxGdiMeasurementAndDrawingStrategyBase }

  TdxGdiMeasurementAndDrawingStrategyBase = class(TdxGdiPlusMeasurementAndDrawingStrategyBase) 
  end;

  { TdxGdiMeasurementAndDrawingStrategy }

  TdxGdiMeasurementAndDrawingStrategy = class(TdxGdiMeasurementAndDrawingStrategyBase)
  private
    FHdcMeasureGraphics: TCanvas;
    FDC: THandle;
  public
    destructor Destroy; override;

		procedure Initialize; override;
    function CreateBoxMeasurer: TdxBoxMeasurer; override; 
    function CreateDocumentPainter(ACanvas: TcxCanvas): TObject; override; 
    function CreateFontCacheManager: TdxFontCacheManager; override; 
  end;

implementation

uses
  Math, ShellAPI, ComObj, ShlObj, dxTypeHelpers, dxCore,
  dxRichEdit.DocumentLayout,
  dxRichEdit.LayoutEngine.BoxMeasurer,
  dxRichEdit.Control.Mouse,
  dxRichEdit.Control.Keyboard,
  dxRichEdit.Platform.Win.FontCache,
  dxRichEdit.DocumentModel.DocumentProperties,
  dxRichEdit.Utils.Colors, 
  cxScrollBar,
  dxRichEdit.FormController,
  dxRichEdit.Dialogs.Paragraph;

{ TdxHdcOriginModifier }

constructor TdxHdcOriginModifier.Create(ACanvas: TcxCanvas; const ANewOrigin: TPoint;
  AZoomFactor: Single; AMode: TMode = TMode.Replace);
begin
  FCanvas := ACanvas;
  SetHDCOrigin(ANewOrigin, AZoomFactor, AMode);
end;

destructor TdxHdcOriginModifier.Destroy;
begin
  RestoreHDC;
  inherited Destroy;
end;

procedure TdxHdcOriginModifier.SetHDCOrigin(const ANewOrigin: TPoint; AZoomFactor: Single; AMode: TMode);
var
  P: TPoint;
  ANewX, ANewY: Integer;
begin
  if FCanvas.HandleAllocated then
  begin
    GetWindowOrgEx(FCanvas.Handle, FOldOrigin);
    ANewX := -Round(ANewOrigin.X / AZoomFactor);
    ANewY := -Round(ANewOrigin.Y / AZoomFactor);
    if AMode = TMode.Combine then
    begin
      Inc(ANewX, Round(FOldOrigin.X / AZoomFactor));
      Inc(ANewY, Round(FOldOrigin.Y / AZoomFactor));
    end;
    SetWindowOrgEx(FCanvas.Handle, ANewX, ANewY, @P);
  end;
end;

procedure TdxHdcOriginModifier.RestoreHDC;
var
  P: TPoint;
begin
  if FCanvas.HandleAllocated then
    SetWindowOrgEx(FCanvas.Handle, FOldOrigin.X, FOldOrigin.Y, @P);
end;

{ TdxHdcZoomModifier }

constructor TdxHdcZoomModifier.Create(ACanvas: TcxCanvas; AZoomFactor: Single);
begin
  FCanvas := ACanvas;
  ZoomHDC(AZoomFactor);
end;

destructor TdxHdcZoomModifier.Destroy;
begin
  RestoreHDC;
  inherited Destroy;
end;

procedure TdxHdcZoomModifier.ZoomHDC(AZoomFactor: Single);
var
  S: TSize;
begin
  if FCanvas.HandleAllocated then
  begin
    GetWindowExtEx(FCanvas.Handle, FOldWindowExtent);
    SetWindowExtEx(FCanvas.Handle, Round(FOldWindowExtent.cx / AZoomFactor), Round(FOldWindowExtent.cy / AZoomFactor), @S);
  end;
end;

procedure TdxHdcZoomModifier.RestoreHDC;
var
  S: TSize;
begin
  if FCanvas.HandleAllocated then
    SetWindowExtEx(FCanvas.Handle, FOldWindowExtent.Width, FOldWindowExtent.Height, @S);
end;

type
  TdxInnerRichEditControlAccess = class(TdxInnerRichEditControl);

{ RichEditViewPainter }

constructor TdxRichEditViewPainter.Create(AView: TdxRichEditView);
begin
  FView := AView;
  FControl := TdxCustomRichEditControl(AView.Control.GetRichEditControl);
  FDecoratorPainters := TList<IdxDecoratorPainter>.Create;
  FSelectionPainters := TdxRichEditSelectionPainters.Create;
  FHoverPainters := TdxRichEditHoverPainters.Create;
end;

destructor TdxRichEditViewPainter.Destroy;
begin
  FreeAndNil(FHoverPainters);
  FreeAndNil(FSelectionPainters);
  FreeAndNil(FDecoratorPainters);
  inherited Destroy;
end;

procedure TdxRichEditViewPainter.Draw(ACanvas: TcxCanvas; ACustomMarkExporter: IdxCustomMarkExporter);
var
  AOldTransform: TdxTransformMatrix;
begin
  BeginDraw(ACanvas);
  DrawEmptyPages(ACanvas);
  AOldTransform := BeginDrawPagesContent;
  DrawPagesContent(AOldTransform);
  EndDrawPagesContent;

  EndDraw;

  BeginDrawInPixels(ACanvas);
  DrawBoxesInPixels(ACustomMarkExporter);
  ApplyGraphicsTransform(ACanvas, ACustomMarkExporter.CustomMarkVisualInfoCollection);
  EndDrawInPixels;
end;

procedure TdxRichEditViewPainter.DrawBoxesInPixels(ACustomMarkExporter: IdxCustomMarkExporter);
begin
end;

procedure TdxRichEditViewPainter.DrawEmptyPages(ACanvas: TcxCanvas);
var
  I: Integer;
begin
  for I := 0 to View.PageViewInfos.Count - 1 do
    DrawEmptyPage(ACanvas, View.PageViewInfos[I]);
end;

procedure TdxRichEditViewPainter.DrawHover(APainter: TdxPainter);
begin
end;

procedure TdxRichEditViewPainter.DrawPageContent(APage: TdxPageViewInfo; APainter: TdxPainter);
var
  AExporter: TdxDocumentLayoutExporter;
  AAdapter: TdxGraphicsDocumentLayoutExporterAdapter;
begin
  AAdapter := TdxWinFormsGraphicsDocumentLayoutExporterAdapter.Create;
  try
    AExporter := View.CreateDocumentLayoutExporter(APainter, AAdapter, APage, GetPageBounds(APage));
    try
      AExporter.ShowWhitespace := FControl.DocumentModel.FormattingMarkVisibilityOptions.ShowHiddenText;
      AExporter.MinReadableTextHeight := FMinReadableTextHeight;
      AExporter.CurrentBackColor := Control.BackgroundPainter.ActualPageBackColor;
      Assert(APage.Page.SecondaryFormattingComplete);
      APage.Page.ExportTo(AExporter);
    finally
      AExporter.Free;
    end;
  finally
    AAdapter.Free;
  end;
end;

procedure TdxRichEditViewPainter.DrawPagesContent(ATransform: TdxTransformMatrix);
var
  I: Integer;
  APainter: TdxPainter;
  APageViewInfo: TdxPageViewInfo;
begin
  APainter := TdxGdiPainter.Create(Graphics);
  try
    for I := 0 to View.PageViewInfos.Count - 1 do
    begin
      APageViewInfo := View.PageViewInfos[I];
      BeginDrawPageContent(APageViewInfo, ATransform);

      ClipPageContent(APageViewInfo, APainter);

      DrawPageContent(APageViewInfo, APainter);

      RestoreOldClipBounds(APainter);

      DrawPageSelection(Graphics, APageViewInfo);
      DrawHover(APainter);
      EndDrawPageContent(APageViewInfo, ATransform);
    end;
  finally
    APainter.Free;
  end;
end;

procedure TdxRichEditViewPainter.DrawPageSelection(AGr: TdxGraphics; APage: TdxPageViewInfo);
var
  I: Integer;
  ASelections: TdxPageSelectionLayoutsCollection;
begin
  ASelections := View.SelectionLayout.GetPageSelection(APage.Page);
  try
    if ASelections = nil then
      Exit;
    for I := 0 to ASelections.Count - 1 do
      DrawPageSelectionCore(ASelections[I]);
  finally
    ASelections.Free;
  end;
end;

procedure TdxRichEditViewPainter.DrawPageSelectionCore(ASelection: IdxSelectionLayoutItem);
begin
  if ASelection <> nil then
    ASelection.Draw(SelectionPainters.Get(ASelection.GetType));
end;

procedure TdxRichEditViewPainter.AddDecoratorPainter(APainter: IdxDecoratorPainter);
begin
  DecoratorPainters.Add(APainter);
end;


function TdxRichEditViewPainter.GetDocumentModel: TdxDocumentModel;
begin
  Result := Control.InnerControl.DocumentModel;
end;

function TdxRichEditViewPainter.GetLookAndFeel: TcxLookAndFeel;
begin
  Result := Control.LookAndFeel;
end;

function TdxRichEditViewPainter.GetPageBounds(APage: TdxPageViewInfo): TRect;
begin
  Result := APage.Page.Bounds;
  Result.Right := MaxInt;
end;

procedure TdxRichEditViewPainter.BeginDraw(ACanvas: TcxCanvas);
begin
  CacheInitialize(ACanvas);
  FMinReadableTextHeight := Round(DocumentModel.LayoutUnitConverter.PixelsToLayoutUnits(6, Graphics.DpiY) / View.ZoomFactor);
  FPhysicalLeftInvisibleWidth := View.HorizontalScrollController.GetPhysicalLeftInvisibleWidth;
  AddSelectionPainters(ACanvas);
end;

procedure TdxRichEditViewPainter.BeginDrawInPixels(ACanvas: TcxCanvas);
begin
  FSaveDCIndex := SaveDC(ACanvas.Handle);
  FCanvas := ACanvas;
end;

procedure TdxRichEditViewPainter.EndDrawInPixels;
begin
  RestoreDC(Graphics.Handle, FSaveDCIndex);
end;

procedure TdxRichEditViewPainter.AddSelectionPainters(ACanvas: TcxCanvas);
begin
  SelectionPainters.AddDefault(TdxSemitransparentSelectionPainter.Create(ACanvas));
end;

procedure TdxRichEditViewPainter.ApplyGraphicsTransform(ACanvas: TcxCanvas;
  ACustomMarkVisualInfoCollection: TdxCustomMarkVisualInfoCollection);
begin
end;

procedure TdxRichEditViewPainter.CacheInitialize(ACanvas: TcxCanvas);
begin
  FCanvas := ACanvas;
end;

procedure TdxRichEditViewPainter.ClipPageContent(APage: TdxPageViewInfo; APainter: TdxPainter);
var
  AClipBounds: TRect;
  ANewClipBounds: TRect;
begin
  AClipBounds := View.CalculatePageContentClipBounds(APage);
  case GetClipBox(TdxGdiPainter(APainter).Graphics.Handle, FOldClipBounds) of
    NULLREGION:;
  end;
  ANewClipBounds := AClipBounds;
  ANewClipBounds.IntersectsWith(FOldClipBounds);

  Graphics.IntersectClipRect(ANewClipBounds);                          
end;

procedure TdxRichEditViewPainter.RestoreOldClipBounds(APainter: TdxPainter);
begin
  Graphics.SetClipRegion(TcxRegion.Create(FOldClipBounds), roSet);
end;

procedure TdxRichEditViewPainter.EndDraw;
begin
  CacheDispose;
end;

procedure TdxRichEditViewPainter.CacheDispose;
begin
  FCanvas := nil;
end;

procedure TdxRichEditViewPainter.BeginDrawPageContent(APage: TdxPageViewInfo; ATransform: TdxTransformMatrix);
var
  AOrigin: TPoint;
  AMeasurer: TdxGdiBoxMeasurer;
begin
  AMeasurer := Control.InnerControl.Measurer as TdxGdiBoxMeasurer;
  AOrigin := APage.ClientBounds.Location;
  AOrigin.X := AOrigin.X - FPhysicalLeftInvisibleWidth;
  AOrigin.X := AMeasurer.SnapToPixels(AOrigin.X, Graphics.DpiX);
  FOriginModifier := TdxHdcOriginModifier.Create(Graphics, AOrigin, View.ZoomFactor, TdxHdcOriginModifier.TMode.Combine);
end;

procedure TdxRichEditViewPainter.EndDrawPageContent(APage: TdxPageViewInfo; ATransform: TdxTransformMatrix);
begin
  FreeAndNil(FOriginModifier);
end;

function TdxRichEditViewPainter.BeginDrawPagesContent: TdxTransformMatrix;
begin
  FZoomModifier := TdxHdcZoomModifier.Create(Graphics, View.ZoomFactor);
  Result := nil
end;

procedure TdxRichEditViewPainter.EndDrawPagesContent;
begin
  FreeAndNil(FZoomModifier);
end;


{ TdxRichEditControlPainter }

constructor TdxRichEditControlPainter.Create(AControl: TdxCustomRichEditControl);
begin
  inherited Create;
  FControl := AControl;
  FDeferredDraws := TList<DrawDelegate>.Create;
end;

destructor TdxRichEditControlPainter.Destroy;
begin
  FreeAndNil(FDeferredDraws);
  inherited Destroy;
end;

function TdxRichEditControlPainter.GetCaret: TdxCaret;
begin
  Result := Control.Caret;
end;

function TdxRichEditControlPainter.GetDragCaret: TdxDragCaret;
begin
  Result := Control.DragCaret;
end;

function TdxRichEditControlPainter.GetActiveView: TdxRichEditView;
begin
  Result := Control.ActiveView;
end;

function TdxRichEditControlPainter.GetLookAndFeel: TcxLookAndFeel;
begin
  Result := Control.LookAndFeel;
end;

procedure TdxRichEditControlPainter.Draw(ACanvas: TcxCanvas; ACustomMarkExporter: IdxCustomMarkExporter);
var
  I, ACount: Integer;
begin
  if Control.InnerControl = nil then
    Exit;
  Control.InnerControl.BeginDocumentRendering;
  try
    ACanvas.SaveClipRegion;
    ACanvas.IntersectClipRect(Control.ClientBounds);
    try
      PerformRenderingInUnits(ACanvas, ACustomMarkExporter, RenderDocument);
      PerformRenderingInPixels(ACanvas, ACustomMarkExporter, RenderDocumentDecorators);
      ACount := FDeferredDraws.Count;
      if ACount > 0 then
      begin
        for I := 0 to ACount - 1 do
          PerformRenderingInUnits(ACanvas, nil, FDeferredDraws[I]);
        FDeferredDraws.Clear;
      end;
    finally
      ACanvas.RestoreClipRegion;
    end;
  finally
    Control.InnerControl.EndDocumentRendering;
  end;
end;

procedure TdxRichEditControlPainter.DrawCaret;
begin
  DrawCaret(DrawCaretCore);
end;

procedure TdxRichEditControlPainter.DrawDragCaret;
begin
  DrawCaret(DrawDragCaretCore);
end;

procedure TdxRichEditControlPainter.DrawCaret(ADrawCaret: DrawDelegate);
var
  ADC: HDC;
  AInnerCanvas: TCanvas;
  ACanvas: TcxCanvas;
  AClientBounds: TRect;
begin
  AInnerCanvas := TCanvas.Create;
  try
    ACanvas := TcxCanvas.Create(AInnerCanvas);
    try
      ADC := GetWindowDC(Control.Handle);
      try
        AInnerCanvas.Handle := ADC;
        AClientBounds := Control.ClientBounds;
        IntersectClipRect(ADC, AClientBounds.Left, AClientBounds.Top, AClientBounds.Right, AClientBounds.Bottom);
        PerformRenderingInPixels(ACanvas, nil, ADrawCaret); 
      finally
        ReleaseDC(Control.Handle, ADC);
      end;
    finally
      ACanvas.Free;
    end;
  finally
    AInnerCanvas.Free;
  end;
end;

procedure TdxRichEditControlPainter.RegisterDeferredDraw(ADraw: DrawDelegate);
begin
  FDeferredDraws.Add(ADraw);
end;

procedure TdxRichEditControlPainter.DeferredDraw(APage: TdxPageViewInfo; ADrawAtPage: DrawAtPageDelegate);
var
  ADraw: DrawDelegate;
begin
  ADraw := procedure (ACanvas: TcxCanvas; ACustomMarkExporter: IdxCustomMarkExporter)
    begin
    end;
  RegisterDeferredDraw(ADraw);
  NotImplemented;
end;

procedure TdxRichEditControlPainter.DeferredDrawReversibleFrame(const ABounds: TRect; APage: TdxPageViewInfo);
var
  ADraw: DrawDelegate;
begin
  ADraw := procedure (ACanvas: TcxCanvas; ACustomMarkExporter: IdxCustomMarkExporter)
    begin
    end;
  RegisterDeferredDraw(ADraw);
  NotImplemented;
end;

procedure TdxRichEditControlPainter.DrawReversibleHorizontalLine(Y: Integer; APage: TdxPageViewInfo);
var
  ADraw: DrawDelegate;
begin
  ADraw := procedure (ACanvas: TcxCanvas; ACustomMarkExporter: IdxCustomMarkExporter)
    begin
      DrawReversibleHorizontalLineCore(ACanvas, Y, APage);
    end;
  DrawReversibleCore(APage, ADraw);
end;

procedure TdxRichEditControlPainter.DrawReversibleVerticalLine(X: Integer; APage: TdxPageViewInfo);
var
  ADraw: DrawDelegate;
begin
  ADraw := procedure (ACanvas: TcxCanvas; ACustomMarkExporter: IdxCustomMarkExporter)
    begin
      DrawReversibleVerticalLineCore(ACanvas, X, APage);
    end;
  DrawReversibleCore(APage, ADraw);
  NotImplemented;
end;

procedure TdxRichEditControlPainter.DrawReversibleCore(APage: TdxPageViewInfo; ADraw: DrawDelegate);
var
  ACanvas: TcxCanvas;
begin
  ACanvas := Control.Canvas;
  ACanvas.SaveClipRegion;
  ACanvas.SetClipRegion(TcxRegion.Create(Control.ClientBounds), roSet);
  try
    PerformRenderingInUnits(ACanvas, nil, ADraw);
  finally
    ACanvas.RestoreClipRegion;
  end;
end;

procedure TdxRichEditControlPainter.PerformRenderingInUnits(ACanvas: TcxCanvas; ACustomMarkExporter: IdxCustomMarkExporter; ADraw: DrawDelegate);
var
  ANewOrg: TPoint;
  APrevOrg: TPoint;
  AViewBounds: TRect;
begin
  AViewBounds := Control.ViewBounds;
  APrevOrg := ACanvas.WindowOrg;
  ANewOrg := cxPointInvert(AViewBounds.TopLeft);
  ACanvas.WindowOrg := ANewOrg;
  try
      ADraw(ACanvas, ACustomMarkExporter);
  finally
    ACanvas.WindowOrg := APrevOrg;
  end;
end;

procedure TdxRichEditControlPainter.PerformRenderingInPixels(ACanvas: TcxCanvas; ACustomMarkExporter: IdxCustomMarkExporter; ADraw: DrawDelegate);
begin
  ACanvas.SetClipRegion(TcxRegion.Create(Control.ClientBounds), roSet);
  ADraw(ACanvas, ACustomMarkExporter);
end;

procedure TdxRichEditControlPainter.RenderDocument(ACanvas: TcxCanvas; ACustomMarkExporter: IdxCustomMarkExporter);
begin
  ActiveView.SelectionLayout.Update;
  Control.ViewPainter.Draw(ACanvas, ACustomMarkExporter);
end;

procedure TdxRichEditControlPainter.RenderDocumentDecorators(ACanvas: TcxCanvas; ACustomMarkExporter: IdxCustomMarkExporter);
begin
  if not Caret.IsHidden then
    DrawCaretCore(ACanvas, ACustomMarkExporter);
  if (DragCaret <> nil) and not DragCaret.IsHidden then
    DrawDragCaretCore(ACanvas, ACustomMarkExporter);
end;

procedure TdxRichEditControlPainter.DrawCaretCore(ACanvas: TcxCanvas; ACustomMarkExporter: IdxCustomMarkExporter);
begin
  DrawCaretCore(Caret, ACanvas, ACustomMarkExporter);
end;

procedure TdxRichEditControlPainter.DrawDragCaretCore(ACanvas: TcxCanvas; ACustomMarkExporter: IdxCustomMarkExporter);
begin
  DrawCaretCore(DragCaret, ACanvas, ACustomMarkExporter);
end;

procedure TdxRichEditControlPainter.DrawCaretCore(ACaret: TdxCaret; ACanvas: TcxCanvas; ACustomMarkExporter: IdxCustomMarkExporter);
var
  APosition: TdxCaretPosition;
  AViewBounds, ALogicalBounds: TRect;
begin
  if not ACaret.ShouldDrawCaret(ActiveView.DocumentModel) then
    Exit;
  APosition := ACaret.GetCaretPosition(ActiveView);
  if not APosition.Update(TdxDocumentLayoutDetailsLevel.Character) then
    Exit;
  AViewBounds := ActiveView.CreateLogicalRectangle(APosition.PageViewInfo, ActiveView.Bounds);
  ALogicalBounds := APosition.CalculateCaretBounds;
  ALogicalBounds.Intersect(AViewBounds);
  ACaret.Bounds := Control.GetPixelPhysicalBounds(APosition.PageViewInfo, ALogicalBounds);
  if (ACaret.Bounds.Width <= 0) or (ACaret.Bounds.Height <= 0) then
    Exit;
  ACaret.Draw(ACanvas);
end;

function TdxRichEditControlPainter.GetActualBounds(ACanvas: TcxCanvas; APage: TdxPageViewInfo; const ABounds: TRect): TRect;
begin
  Result := ActiveView.CreatePhysicalRectangleFast(APage, ABounds);
  Result := ActiveView.DocumentLayout.UnitConverter.LayoutUnitsToPixels(Result, ACanvas.DpiX, ACanvas.DpiY);
end;

procedure TdxRichEditControlPainter.DrawReversibleFrameCore(ACanvas: TcxCanvas; const ABounds: TRect; APage: TdxPageViewInfo);
begin
  if not ActiveView.CaretPosition.Update(TdxDocumentLayoutDetailsLevel.Character) then
    Exit; 
  NotImplemented;
end;

procedure TdxRichEditControlPainter.DrawReversibleHorizontalLineCore(ACanvas: TcxCanvas; Y: Integer; APage: TdxPageViewInfo);
begin
  NotImplemented;
end;

procedure TdxRichEditControlPainter.DrawReversibleVerticalLineCore(ACanvas: TcxCanvas; X: Integer; APage: TdxPageViewInfo);
begin
  NotImplemented;
end;

{ TdxEmptyRichEditControlPainter }

procedure TdxEmptyRichEditControlPainter.Draw(ACanvas: TcxCanvas; ACustomMarkExporter: IdxCustomMarkExporter);
begin
end;

procedure TdxEmptyRichEditControlPainter.DrawCaret;
begin
end;

{ TdxRichEditViewRepository }

procedure TdxRichEditViewRepository.CreateViews;
begin
  AddView(TdxSimpleView.Create(RichEditControl));
end;

function TdxRichEditViewRepository.GetSimpleView: TdxSimpleView;
begin
  Result := Views[0] as TdxSimpleView;
end;

procedure TdxRichEditViewRepository.SetSimpleView(const Value: TdxSimpleView);
begin
  (Views[0] as TdxSimpleView).Assign(Value);
end;

{ TdxCustomRichEditControl }

constructor TdxCustomRichEditControl.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  Width := 300;
  Height := 200;
  FWantTabs := True;
  FWantReturns := True;
  Keys := [kAll, kArrows, kChars, kTab];
  BorderStyle := cxcbsDefault;
  FAutoSizeMode := TdxAutoSizeMode.None;
  FInnerControl := CreateInnerControl;
  FMouseController := CreateMouseController;
  FKeyboardController := CreateKeyboardController;
  BeginInitialize;
  LayoutUnit := DefaultLayoutUnit;
  FCaretTimer := TcxTimer.Create(nil);
  EndInitialize;
end;

destructor TdxCustomRichEditControl.Destroy;
begin
  StopCaretTimer;

  DisposeCommon;

//  LockScrollBars;
  FreeAndNil(FBackgroundPainter);
  FreeAndNil(FPainter);
  FreeAndNil(FViewPainter);
  FreeAndNil(FKeyboardController);
  FreeAndNil(FMouseController);
  FreeAndNil(FCaretTimer);
  FreeAndNil(FCaret);
  DestroyCaret; 
  inherited Destroy;
end;

class constructor TdxCustomRichEditControl.Initialize;
begin
  FSystemCaretBitmap := CreateSystemCaretBitmap;
end;

class destructor TdxCustomRichEditControl.Finalize;
begin
  FreeAndNil(FSystemCaretBitmap); 
end;

function TdxCustomRichEditControl.CalculateActualViewBounds(const APreviousViewBounds: TRect): TRect;
begin
  Result := CalculateViewBounds(ClientBounds);
end;

function TdxCustomRichEditControl.CalculateInitialClientBounds: TRect;
var
  AForm: TCustomForm;
begin
  AForm := GetParentForm(Self);
  if AForm <> nil then
  begin
    if AForm.WindowState <> wsMinimized then
      FLastValidClientRectangle := ClientRect;
  end
  else
    FLastValidClientRectangle := ClientRect;
  Result := FLastValidClientRectangle;
  InflateRect(Result, -BorderSize, -BorderSize);
end;

function TdxCustomRichEditControl.CalculateHorizontalScrollbarHeight: Integer;
begin
  if CalculateHorizontalScrollbarVisibility then
    Result := GetScrollBarSize.cy
  else
    Result := 0;
end;

function TdxCustomRichEditControl.CalculateVerticalScrollbarWidth: Integer;
begin
  case Options.VerticalScrollbar.Visibility of
    TdxRichEditScrollbarVisibility.Hidden:
      Result := 0;
    else
      Result := GetScrollBarSize.cx;
  end;
end;

function TdxCustomRichEditControl.CalculateViewBounds(AClientBounds: TRect): TRect;
var
  AViewPixelBounds: TRect;
begin
  AViewPixelBounds := CalculateViewPixelBounds(AClientBounds);
  Result := DocumentModel.LayoutUnitConverter.PixelsToLayoutUnits(AViewPixelBounds, DpiX, DpiY);
end;

function TdxCustomRichEditControl.CalculateViewPixelBounds(AClientBounds: TRect): TRect;
var
  AViewPadding: TRect;
begin
  Result := ClientBounds;
  AViewPadding := ActiveView.ActualPadding;
  Result.Inflate(-AViewPadding.Left, -AViewPadding.Top, -AViewPadding.Right, -AViewPadding.Bottom);
end;

procedure TdxCustomRichEditControl.SetBounds(ALeft, ATop, AWidth, AHeight: Integer);
var
  ABestSize: TSize;
begin
  if (AutoSizeMode <> TdxAutoSizeMode.None) and not FIsUpdateAutoSizeMode and not FInsideResize then
  begin
    ABestSize := CalcBestSize(AWidth, AHeight, False);
    AWidth := ABestSize.Width;
    AHeight := ABestSize.Height;
  end;
  inherited SetBounds(ALeft, ATop, AWidth, AHeight);
end;

procedure TdxCustomRichEditControl.CreateWnd;
begin
  inherited CreateWnd;
  OleCheck(RegisterDragDrop(Handle, Self));
end;

procedure TdxCustomRichEditControl.DestroyWnd;
begin
  if InnerControl <> nil then
  begin
    InnerControl.BackgroundThreadUIUpdater.Free;
    InnerControl.BackgroundThreadUIUpdater := TdxDeferredBackgroundThreadUIUpdater.Create;
  end;

  RevokeDragDrop(Handle);
  inherited DestroyWnd;
end;

function TdxCustomRichEditControl.GetClientBounds: TRect;
begin
  Result := FClientBounds;
end;

procedure TdxCustomRichEditControl.DoPaint;
var
  ACustomMarkExporter: TdxCustomMarkExporter;
begin
  if not FSizeGripBounds.IsEmpty then
  begin
    Canvas.FillRect(FSizeGripBounds, LookAndFeel.Painter.DefaultSizeGripAreaColor);
    Canvas.ExcludeClipRect(FSizeGripBounds);
  end;
  inherited DoPaint;
  if Assigned(Painter) and not IsUpdateLocked then
  begin
    ACustomMarkExporter := TdxCustomMarkExporter.Create;
    try
      Painter.Draw(Canvas, ACustomMarkExporter);
    finally
      ACustomMarkExporter.Free;
    end;
  end;
end;

procedure TdxCustomRichEditControl.CreateDragCaret;
begin
  FDragCaret := TdxDragCaret.Create(ActiveView);
end;

procedure TdxCustomRichEditControl.DestroyDragCaret;
begin
  if (DragCaret <> nil) and not DragCaret.IsHidden then
    DrawDragCaret;
  FreeAndNil(FDragCaret);
end;

function TdxCustomRichEditControl.GetDragCaret: TdxDragCaret;
begin
  Result := FDragCaret;
end;

function TdxCustomRichEditControl.GetOvertype: Boolean;
begin
  Result := InnerControl.Overtype;
end;

procedure TdxCustomRichEditControl.SetOvertype(const Value: Boolean);
begin
  InnerControl.Overtype := Value;
end;

procedure TdxCustomRichEditControl.UpdateControlAutoSize;
var
  ANewSize: TSize;
begin
  if AutoSizeMode = TdxAutoSizeMode.None then
    Exit;
  if not FIsUpdateAutoSizeMode and not FInsideResize then
  begin
    FIsUpdateAutoSizeMode := True;
    try
      ANewSize := CalcBestSize(Width, Height, (Width = Constraints.MaxWidth) and (Constraints.MaxWidth <> 0));
      FContentSize := ANewSize;
      if not ANewSize.IsEqual(FLastSize) then
      begin
        SetBounds(Left, Top, ANewSize.cx, ANewSize.cy);
        FLastSize := ANewSize;
      end;
    finally
      FIsUpdateAutoSizeMode := False;
    end;
  end;
end;

type
  TdxParagraphFormControllerAccess = class(TdxParagraphFormController);
  TdxParagraphFormControllerParametersAccess = class(TdxParagraphFormControllerParameters);

procedure TdxCustomRichEditControl.ShowParagraphForm(AParagraphProperties: TdxMergedParagraphProperties;
  ACallback: TdxShowParagraphFormCallback; ACallbackData: TObject);
var
  AForm: TdxRichEditParagraphDialogForm; 
  AArgs: TdxParagraphFormShowingEventArgs;
  AControllerParameters: TdxParagraphFormControllerParameters;
  AResult: TModalResult; 
begin
  AControllerParameters := TdxParagraphFormControllerParameters.Create(Self, AParagraphProperties, DocumentModel.UnitConverter);
  try
    AArgs := TdxParagraphFormShowingEventArgs.Create(AControllerParameters);
    try
      if not AArgs.Handled then
      begin
        AForm := TdxRichEditParagraphDialogForm.Create(Self); 
        try
          AForm.Initialize(AControllerParameters);
          AResult := AForm.ShowModal;
          if AResult = mrOk then
            ACallback(TdxParagraphFormControllerAccess(AForm.Controller).SourceProperties, ACallbackData);
        finally
          AForm.Free;
        end;
      end
      else
      begin
        AResult := AArgs.DialogResult;
        if AResult = mrOk then
          ACallback(TdxParagraphFormControllerParametersAccess(AControllerParameters).ParagraphProperties, ACallbackData);
      end;
    finally
      AArgs.Free;
    end;
  finally
    AControllerParameters.Free;
  end;
end;

function TdxCustomRichEditControl.OleDragEnter(const dataObj: IDataObject;
  grfKeyState: Integer; pt: TPoint; var dwEffect: Integer): HResult;
var
  Args: TdxDragEventArgs;
  P: TPoint;
begin
  if not (IsDesigning or IsDestroying) and Enabled then
  begin
    FDragObject := dataObj;
    P := ScreenToClient(pt);
    Args := TdxDragEventArgs.CreateFormOle(dataObj, grfKeyState, P, dwEffect);
    MouseController.DoDragEnter(@Args);
    dwEffect := dxDragDropEffectsToOleDragDropEffects(Args.Effect);
    Result := S_OK;
  end
  else
  begin
    dwEffect := DROPEFFECT_NONE;
    Result := E_UNEXPECTED;
  end;
end;

function TdxCustomRichEditControl.OleDragLeave: HResult;
begin
  FDragObject := nil;
  MouseController.DoDragLeave;
  Result := S_OK;
end;

function TdxCustomRichEditControl.OleDragOver(grfKeyState: Integer; pt: TPoint;
  var dwEffect: Integer): HResult;
var
  Args: TdxDragEventArgs;
  P: TPoint;
begin
  if not (IsDesigning or IsDestroying) and Enabled then
  begin
    P := ScreenToClient(pt);
    Args := TdxDragEventArgs.CreateFormOle(FDragObject, grfKeyState, P, dwEffect);
    MouseController.DoDragOver(@Args);
    dwEffect := dxDragDropEffectsToOleDragDropEffects(Args.Effect);
    Result := S_OK;
  end
  else
  begin
    dwEffect := DROPEFFECT_NONE;
    Result := E_UNEXPECTED
  end;
end;

function TdxCustomRichEditControl.OleDrop(const dataObj: IDataObject; grfKeyState: Integer; pt: TPoint;
  var dwEffect: Integer): HResult;
var
  Args: TdxDragEventArgs;
  P: TPoint;
begin
  if not (IsDesigning or IsDestroying) and Enabled then
  begin
    P := ScreenToClient(pt);
    Args := TdxDragEventArgs.CreateFormOle(dataObj, grfKeyState, P, dwEffect);
    MouseController.DoDragDrop(@Args);
    dwEffect := dxDragDropEffectsToOleDragDropEffects(Args.Effect);
    Result := S_OK;
    Application.BringToFront;
    SetFocus;
  end
  else
  begin
    dwEffect := DROPEFFECT_NONE;
    Result := E_UNEXPECTED;
  end;
  FDragObject := nil;
end;

function TdxCustomRichEditControl.OleQueryContinueDrag(fEscapePressed: BOOL; grfKeyState: Longint): HResult;
var
  Args: TdxQueryContinueDragEventArgs;
begin
  Args := TdxQueryContinueDragEventArgs.Create(fEscapePressed, grfKeyState);
  MouseController.QueryContinueDrag(@Args);
  case Args.Action of
    TdxDragAction.Cancel:
      Result := DRAGDROP_S_CANCEL;
    TdxDragAction.Drop:
      Result := DRAGDROP_S_DROP;
  else
    Result := S_OK;
  end;
end;

function TdxCustomRichEditControl.OleGiveFeedback(dwEffect: Longint): HResult;
var
  Args: TdxGiveFeedbackEventArgs;
begin
  Args := TdxGiveFeedbackEventArgs.Create(dwEffect);
  MouseController.GiveFeedback(@Args);
  if Args.UseDefaultCursors then
    Result := DRAGDROP_S_USEDEFAULTCURSORS
  else
    Result := S_OK;
end;

procedure TdxCustomRichEditControl.DrawDragCaret;
begin
  Painter.DrawDragCaret;
end;

function TdxCustomRichEditControl.DoDragDrop(const AData: IDataObject; AllowedEffects: TdxDragDropEffects): TdxDragDropEffects;
var
  AAllowedEffects, AEffect: Longint;
begin
  Result := [];
  AAllowedEffects := dxDragDropEffectsToOleDragDropEffects(AllowedEffects);
  try
    ActiveX.DoDragDrop(AData, Self, AAllowedEffects, AEffect);
  except

    on E: Exception do

      raise;
  end;
  Result := dxOleDragDropEffectsToDragDropEffects(AEffect);
end;

function TdxCustomRichEditControl.CalcBestSize(AWidth, AHeight: Integer; AFixedWidth: Boolean): TSize;
var
  ABestSize: TSize;
begin
  ABestSize := CalcViewBestSize(AFixedWidth);
  Inc(ABestSize.cx, CalculateVerticalScrollbarWidth + BorderSize * 2);
  Inc(ABestSize.cy, CalculateHorizontalScrollbarHeight + BorderSize * 2);
  case AutoSizeMode of
    TdxAutoSizeMode.Vertical:
      Result.Init(AWidth, ABestSize.cy);
    TdxAutoSizeMode.Horizontal:
      Result.Init(ABestSize.cx, AHeight);
    TdxAutoSizeMode.Both:
      Result.Init(ABestSize.cx, ABestSize.cy);
    else
      Result.Init(0, 0);
  end;
end;

function TdxCustomRichEditControl.CalcViewBestSize(AFixedWidth: Boolean): TSize;
var
  AViewPadding: TRect;
begin
  Result := ActiveView.CalcBestSize(AFixedWidth);
  AViewPadding := ActiveView.ActualPadding;
  Inc(Result.cx, AViewPadding.Left + AViewPadding.Right);
  Inc(Result.cy, AViewPadding.Top + AViewPadding.Bottom);
end;

procedure TdxCustomRichEditControl.EraseBackground(ACanvas: TcxCanvas; const ARect: TRect);
begin
  if BackgroundPainter <> nil then
    BackgroundPainter.Draw(ACanvas, ClientBounds);
end;

function TdxCustomRichEditControl.HasBackground: Boolean;
begin
  Result := True;
end;

function TdxCustomRichEditControl.GetSizeGripBounds: TRect;
begin
  Result := FSizeGripBounds;
end;

procedure TdxCustomRichEditControl.ActivateViewPlatformSpecific(AView: TdxRichEditView);
begin
  CreateBackgroundPainter(AView);
  CreateViewPainter(AView);
end;

function TdxCustomRichEditControl.CreateViewRepository: TdxRichEditCustomViewRepository;
begin
  Result := TdxRichEditViewRepository.Create(Self);
end;

procedure TdxCustomRichEditControl.ApplyChangesCorePlatformSpecific(AChangeActions: TdxDocumentModelChangeActions);
begin
  if TdxDocumentModelChangeAction.Redraw in AChangeActions then
    RedrawEnsureSecondaryFormattingComplete;
end;

procedure TdxCustomRichEditControl.BeginInitialize;
begin
  InnerControl.BeginInitialize;
end;

function TdxCustomRichEditControl.GetIsUpdateLocked: Boolean;
begin
  Result := (InnerControl <> nil) and InnerControl.GetIsUpdateLocked;
end;

function TdxCustomRichEditControl.GetBatchUpdateHelper: TdxBatchUpdateHelper;
begin
  if InnerControl <> nil then
    Result := InnerControl.GetBatchUpdateHelper
  else
    Result := nil;
end;

procedure TdxCustomRichEditControl.BeginUpdate;
begin
  if InnerControl <> nil then
    InnerControl.BeginUpdate;
end;

procedure TdxCustomRichEditControl.CancelUpdate;
begin
  if InnerControl <> nil then
    InnerControl.CancelUpdate;
end;

procedure TdxCustomRichEditControl.ApplyFontAndForeColor;
begin
  if InnerControl <> nil then
    InnerControl.ApplyFontAndForeColor;
end;

procedure TdxCustomRichEditControl.OnBackColorChanged;
begin
  Redraw(False);
end;


function TdxCustomRichEditControl.CreateInnerControl: TdxInnerRichEditControl;
begin
  Result := TdxInnerRichEditControl.Create(Self);
end;

function TdxCustomRichEditControl.CreateKeyboardController: TdxRichEditKeyboardCustomController;
begin
  Result := TdxRichEditKeyboardController.Create(Self);
end;

function TdxCustomRichEditControl.CreateMouseController: TdxRichEditMouseCustomController;
begin
  Result := TdxRichEditMouseController.Create(Self);
end;

function TdxCustomRichEditControl.CreateMeasurementAndDrawingStrategy(
  ADocumentModel: TdxDocumentModel): TdxMeasurementAndDrawingStrategy;
begin
  Result := TdxGdiMeasurementAndDrawingStrategy.Create(ADocumentModel);
end;

function TdxCustomRichEditControl.CreatePlatformSpecificScrollBarAdapter: IdxPlatformSpecificScrollBarAdapter;
begin
  Result := TdxWinFormsScrollBarAdapter.Create;
end;

procedure TdxCustomRichEditControl.RedrawEnsureSecondaryFormattingComplete(Action: TdxRefreshAction);
begin
  RedrawEnsureSecondaryFormattingComplete;
  if not IsUpdateLocked and (Action = TdxRefreshAction.Transforms) then
    Refresh;
end;

function TdxCustomRichEditControl.CreateRichEditViewHorizontalScrollController(
  ARichEditView: TdxRichEditView): TdxRichEditViewHorizontalScrollController;
begin
  Result := TdxWinFormsRichEditViewHorizontalScrollController.Create(ARichEditView);
end;

procedure TdxCustomRichEditControl.CreateViewPainter(AView: TdxRichEditView);
begin
  FViewPainter := TdxSimpleViewPainter.Create(AView); 
end;

procedure TdxCustomRichEditControl.CreateBackgroundPainter(AView: TdxRichEditView);
begin
  FBackgroundPainter := TdxRichEditViewBackgroundPainter.Create(Views.Simple);
end;

procedure TdxCustomRichEditControl.RecreateBackgroundPainter;
begin
  FreeAndNil(FBackgroundPainter);
  CreateBackgroundPainter(Views.Simple);
end;

function TdxCustomRichEditControl.CreateRichEditViewVerticalScrollController(
  ARichEditView: TdxRichEditView): TdxRichEditViewVerticalScrollController;
begin
  Result := TdxWinFormsRichEditViewVerticalScrollController.Create(ARichEditView); 
end;

function TdxCustomRichEditControl.DefaultLayoutUnit: TdxDocumentLayoutUnit;
begin
  Result := TdxDocumentLayoutUnit.Pixel;
end;

procedure TdxCustomRichEditControl.EndInitialize;
begin
  FPainter := TdxRichEditControlPainter.Create(Self);
  EndInitializeCommon;
  FCaret := TdxCaret.Create;
end;

procedure TdxCustomRichEditControl.EndInitializeCommon;
begin
  SubscribeInnerControlEvents;
  InnerControl.EndInitialize;
end;

procedure TdxCustomRichEditControl.DisposeCommon;
begin
  UnsubscribeInnerControlEvents;

  FInnerControl.Free; 
  FInnerControl := nil;
end;

procedure TdxCustomRichEditControl.ForceHandleLookAndFeelChanged;
begin
  OnResizeCore(True);
end;

function TdxCustomRichEditControl.GetPixelPhysicalBounds(APageViewInfo: TdxPageViewInfo; ALogicalBounds: TRect): TRect;
var
  APhysicalBounds: TRect;
begin
  if APageViewInfo = nil then
    Exit(cxNullRect);
  APhysicalBounds := ActiveView.CreatePhysicalRectangle(APageViewInfo, ALogicalBounds);
  APhysicalBounds := cxRectOffset(APhysicalBounds, ViewBounds.Left, ViewBounds.Top);
  Result := ActiveView.DocumentLayout.UnitConverter.LayoutUnitsToPixels(APhysicalBounds, DpiX, DpiY);
end;

procedure TdxCustomRichEditControl.EndUpdate;
begin
  if InnerControl <> nil then
    InnerControl.EndUpdate;
end;

function TdxCustomRichEditControl.GetInnerControl: IdxInnerControl;
begin
  Result := InnerControl;
end;

function TdxCustomRichEditControl.GetRichEditControl: TObject;
begin
  Result := Self;
end;

function TdxCustomRichEditControl.GetActiveView: TdxRichEditView;
begin
  Result := InnerControl.ActiveView;
end;

function TdxCustomRichEditControl.GetBackgroundThreadUIUpdater: TdxBackgroundThreadUIUpdater;
begin
  if InnerControl <> nil then
    Result := InnerControl.BackgroundThreadUIUpdater
  else
    Result := nil;
end;

procedure TdxCustomRichEditControl.CaretTimerHandler(Sender: TObject);
begin
  if IsUpdateLocked then
    Exit;
  FCaret.IsHidden := not Caret.IsHidden;
  Painter.DrawCaret;
end;

function TdxCustomRichEditControl.GetControlDeferredChanges: TdxRichEditControlDeferredChanges;
begin
  if InnerControl <> nil then
    Result := InnerControl.ControlDeferredChanges
  else
    Result := nil;
end;

function TdxCustomRichEditControl.GetCursor: TCursor;
begin
  Result := Cursor;
end;

function TdxCustomRichEditControl.GetDocumentModel: TdxDocumentModel;
begin
  if InnerControl <> nil then
    Result := InnerControl.DocumentModel
  else
    Result := nil;
end;

function TdxCustomRichEditControl.GetViews: TdxRichEditViewRepository;
begin
  if InnerControl <> nil then
    Result := TdxRichEditViewRepository(InnerControl.Views)
  else
    Result := nil;
end;

function TdxCustomRichEditControl.GetDpiX: Single;
begin
  Result := DocumentModel.DpiX;
end;

function TdxCustomRichEditControl.GetDpiY: Single;
begin
  Result := DocumentModel.DpiY;
end;

function TdxCustomRichEditControl.GetLayoutUnit: TdxDocumentLayoutUnit;
begin
  if InnerControl <> nil then
    Result := InnerControl.LayoutUnit
  else
    Result := DefaultLayoutUnit;
end;

function TdxCustomRichEditControl.GetMeasurementAndDrawingStrategy: TdxMeasurementAndDrawingStrategy;
begin
  if InnerControl <> nil then
    Result := InnerControl.MeasurementAndDrawingStrategy
  else
    Result := nil;
end;

function TdxCustomRichEditControl.GetModified: Boolean;
begin
  if InnerControl <> nil then
    Result := InnerControl.Modified
  else
    Result := False;
end;

function TdxCustomRichEditControl.GetOptions: TdxRichEditDocumentServerOptions;
begin
  Result := TdxInnerRichEditControlAccess(InnerControl).Options;
end;

procedure TdxCustomRichEditControl.HideCaret;
begin
  if Focused then
    HideCaretCore;
end;

class function TdxCustomRichEditControl.CreateSystemCaretBitmap: TcxBitmap;
begin
  Result := TcxBitmap.CreateSize(16, 16);
  Result.Canvas.Brush.Color := clBlack;
  Result.Canvas.FillRect(Result.ClientRect);
end;

procedure TdxCustomRichEditControl.EnsureSystemCaretCreated;
begin
  if not HandleAllocated or not Focused then
    Exit;
  DestroyCaret;
    CreateCaret(Handle, FSystemCaretBitmap.Handle, 0, 0);
end;

procedure TdxCustomRichEditControl.HideCaretCore;
begin
  if IsUpdateLocked then
    Exit;
  EnsureSystemCaretCreated;
  if not Caret.IsHidden then
  begin
    HideSystemCaret;
    Painter.DrawCaret;
    Caret.IsHidden := True;
  end;
end;

procedure TdxCustomRichEditControl.InitControl;
var
  ADeferredUpdater: TdxDeferredBackgroundThreadUIUpdater;
begin
  FControlCreated := True;
  inherited InitControl;
  BeginUpdate;
  ApplyFontAndForeColor;
  ForceHandleLookAndFeelChanged;
  ADeferredUpdater := BackgroundThreadUIUpdater as TdxDeferredBackgroundThreadUIUpdater;
  try
    InnerControl.BackgroundThreadUIUpdater := TdxBeginInvokeBackgroundThreadUIUpdater.Create;
    PerformDeferredUIUpdates(ADeferredUpdater);
  finally
    FreeAndNil(ADeferredUpdater);
  end;


  FHorizontalScrollBar.Parent := Self;
  FHorizontalScrollBar.HandleNeeded;
  FVerticalScrollBar.Parent := Self;
  FVerticalScrollBar.HandleNeeded;

  EndUpdate;
end;

function TdxCustomRichEditControl.IsUpdateLocked: Boolean;
begin
  Result := FControlCreated and (InnerControl <> nil) and InnerControl.IsUpdateLocked;
end;

procedure TdxCustomRichEditControl.PerformDeferredUIUpdates(ADeferredUpdater: TdxDeferredBackgroundThreadUIUpdater);
var
  ACount, I: Integer;
  ADeferredUpdates: TList<TdxAction>;
begin
  ADeferredUpdates := ADeferredUpdater.Updates;
  ACount := ADeferredUpdates.Count;
  for I := 0 to ACount - 1 do
    BackgroundThreadUIUpdater.UpdateUI(ADeferredUpdates[I]);
end;

procedure TdxCustomRichEditControl.KeyDown(var Key: Word; Shift: TShiftState);
begin
  inherited KeyDown(Key, Shift);
  if (Key = VK_TAB) and not WantTabs then
    Key := 0;
  if (Key = VK_RETURN) and not WantReturns then
    Key := 0;
  if Key <> 0 then
    KeyboardController.KeyDown(Key, Shift);
end;

procedure TdxCustomRichEditControl.KeyPress(var Key: Char);
begin
  inherited KeyPress(Key);
  KeyboardController.KeyPress(Key);
end;

procedure TdxCustomRichEditControl.KeyUp(var Key: Word; Shift: TShiftState);
begin
  inherited KeyUp(Key, Shift);
  KeyboardController.KeyUp(Key, Shift);
end;

procedure TdxCustomRichEditControl.LoadDocument(const AFileName: string);
begin
  if InnerControl <> nil then
    InnerControl.LoadDocument(AFileName);
end;

procedure TdxCustomRichEditControl.SaveDocument(const AFileName: string);
begin
  if InnerControl <> nil then
    InnerControl.SaveDocument(AFileName);
end;

procedure TdxCustomRichEditControl.OnResizeCore;
begin
  OnResizeCore(True);
end;

procedure TdxCustomRichEditControl.OnZoomFactorChangingPlatformSpecific;
begin
end;

procedure TdxCustomRichEditControl.OnResizeCore(AEnsureCaretVisibleOnResize: Boolean);
var
  AInitialClientBounds: TRect;
  AInitialWidth, AInitialHeight: Integer;
begin
  Assert(not DocumentModel.IsUpdateLocked);
  AInitialWidth := Width;
  AInitialHeight := Height;
  AInitialClientBounds := CalculateInitialClientBounds;
  while True do
  begin
    UpdateScrollbarsVisibility;
    if not PerformResize(AInitialClientBounds, AEnsureCaretVisibleOnResize) then
      Break;
    if (AInitialWidth <> Width) or (AInitialHeight <> Height) then
    begin
      AInitialWidth := Width;
      AInitialHeight := Height;
      AInitialClientBounds := CalculateInitialClientBounds;
    end;
  end;
  UpdateVerticalScrollBar(False);
end;

procedure TdxCustomRichEditControl.OnViewPaddingChanged;
begin
  Assert(False, 'Not implemented')
end;

procedure TdxCustomRichEditControl.UpdateVerticalScrollBar(AAvoidJump: Boolean);
begin
  if InnerControl <> nil then
    InnerControl.UpdateVerticalScrollBar(AAvoidJump);
end;

function TdxCustomRichEditControl.GetVerticalScrollbarBounds(AWidth, AOffset, AHorizontalScrollbarHeight: Integer): TRect;
begin
  Result.InitSize(FClientBounds.Right - AWidth, FClientBounds.Top + AOffset, AWidth, FClientBounds.Height - AOffset - AHorizontalScrollbarHeight);
end;

procedure TdxCustomRichEditControl.ResizeView(AEnsureCaretVisibleOnResize: Boolean);
var
  ANormalizedViewBounds: TRect;
begin
  FViewBounds := CalculateViewBounds(ClientBounds);
  ANormalizedViewBounds := FViewBounds;
  ANormalizedViewBounds.Offset(-FViewBounds.Left, -FViewBounds.Top);
  ActiveView.OnResize(ANormalizedViewBounds, AEnsureCaretVisibleOnResize);
end;

function TdxCustomRichEditControl.PerformResize(const AInitialClientBounds: TRect; AEnsureCaretVisibleOnResize: Boolean): Boolean;
var
  AVerticalScrollbarWidth, AHorizontalScrollbarHeight, AVerticalScrollbarHeight: Integer;
begin
  Assert(not DocumentModel.IsUpdateLocked);
  FClientBounds := AInitialClientBounds;
  AVerticalScrollbarWidth := CalculateVerticalScrollbarWidth;
  AHorizontalScrollbarHeight := CalculateHorizontalScrollbarHeight;

  if HorizontalScrollbar.IsOverlapScrollBar then
    AVerticalScrollbarHeight := 0
  else
    AVerticalScrollbarHeight := AHorizontalScrollbarHeight;
  VerticalScrollbar.BoundsRect := GetVerticalScrollbarBounds(AVerticalScrollbarWidth, 0, AVerticalScrollbarHeight);

  HorizontalScrollbar.BoundsRect := TRect.CreateSize(FClientBounds.Left, FClientBounds.Bottom - AHorizontalScrollbarHeight, FClientBounds.Width - AVerticalScrollbarWidth, AHorizontalScrollbarHeight);

  if not HorizontalScrollbar.IsOverlapScrollBar then
  begin
    Dec(FClientBounds.Right, AVerticalScrollbarWidth);
    Dec(FClientBounds.Bottom, AHorizontalScrollbarHeight);
    FSizeGripBounds.Init(
      HorizontalScrollbar.BoundsRect.Right, VerticalScrollbar.BoundsRect.Bottom,
      FClientBounds.Right + AVerticalScrollbarWidth, FClientBounds.Bottom + AHorizontalScrollbarHeight);
  end
  else
    FSizeGripBounds.Empty;



  ResizeView(AEnsureCaretVisibleonResize);
  Result := AHorizontalScrollbarHeight <> CalculateHorizontalScrollbarHeight;
end;

procedure TdxCustomRichEditControl.RaiseDeferredEvents(AChangeActions: TdxDocumentModelChangeActions);
begin
end;

procedure TdxCustomRichEditControl.CustomRefresh;
begin
  FIsInsideRefresh := True;
  try
    UpdateControlAutoSize;
    Repaint;
  finally
    FIsInsideRefresh := False;
  end;
end;

procedure TdxCustomRichEditControl.Redraw;
begin
  Redraw(False);
end;

procedure TdxCustomRichEditControl.Redraw(AAfterEndUpdate: Boolean);
begin
  if IsUpdateLocked then
    ControlDeferredChanges.Redraw := True
  else
    if not AAfterEndUpdate then
      CustomRefresh
    else
      if HandleAllocated and not FIsInsideRefresh then
        RedrawAfterEndUpdate;
end;

procedure TdxCustomRichEditControl.Redraw(AAction: TdxRefreshAction);
begin
  Redraw(False);
end;

procedure TdxCustomRichEditControl.RedrawAfterEndUpdate;
begin
  Repaint;
end;

procedure TdxCustomRichEditControl.RedrawEnsureSecondaryFormattingComplete;
begin
  if IsUpdateLocked then
  begin
    InnerControl.BeginDocumentRendering;
    InnerControl.EndDocumentRendering;
    ControlDeferredChanges.Redraw := True;
  end
  else
    Repaint;
end;

procedure TdxCustomRichEditControl.BoundsChanged;
var
  AOldPainter: TdxRichEditControlPainter;
  ANestedResize: Boolean;
  ABestSize: TSize;
begin
  if not HandleAllocated then
    Exit;
  ANestedResize := FInsideResize;
  FInsideResize := True;
  BeginUpdate;
  try
    AOldPainter := FPainter;
    FPainter := TdxEmptyRichEditControlPainter.Create(Self);
    try
      inherited BoundsChanged;
      OnResizeCore(False);
      if not ANestedResize and ((AutoSizeMode = TdxAutoSizeMode.Both) or (AutoSizeMode = TdxAutoSizeMode.Vertical)) then
      begin
        ABestSize := CalcBestSize(Width, Height, True);
        SetBounds(Left, Top, ABestSize.cx, ABestSize.cy);
      end;
    finally
      FreeAndNil(FPainter);
      FPainter := AOldPainter;
    end;
  finally
    EndUpdate;
    FInsideResize := False;
  end;
end;


procedure TdxCustomRichEditControl.FocusEnter;
begin
  inherited DoEnter;
  if ShouldShowCaret then
    StartCaretBlinking;
  InnerControl.OnUpdateUI;
end;

procedure TdxCustomRichEditControl.FocusLeave;
begin
  if IsDestroying then
    Exit;
  if not ShouldShowCaret then
  begin
    StopCaretBlinking;
    Windows.DestroyCaret;
  end;
  InnerControl.OnUpdateUI;
end;

procedure TdxCustomRichEditControl.MouseDown(Button: TMouseButton;
  Shift: TShiftState; X: Integer; Y: Integer);
begin
  inherited MouseDown(Button, Shift, X, Y);
  if Button = mbMiddle then
    DoScrolling
  else
    MouseController.MouseDown(Button, Shift, X, Y);
end;

procedure TdxCustomRichEditControl.MouseMove(Shift: TShiftState;
  X: Integer; Y: Integer);
begin
  inherited MouseMove(Shift, X, Y);
  MouseController.MouseMove(Shift, X, Y);
end;

procedure TdxCustomRichEditControl.MouseUp(Button: TMouseButton;
  Shift: TShiftState; X: Integer; Y: Integer);
begin
  inherited MouseUp(Button, Shift, X, Y);
  MouseController.MouseUp(Button, Shift, X, Y);
end;

function TdxCustomRichEditControl.InternalMouseWheel(Shift: TShiftState; WheelDelta: Integer;
  MousePos: TPoint): Boolean;
begin
  Result := MouseController.MouseWheel(Shift, WheelDelta, MousePos);
end;

procedure TdxCustomRichEditControl.AddKeyboardService(const AService: IdxKeyboardHandlerService);
begin
  FKeyboardController.AddHandler(AService);
end;

procedure TdxCustomRichEditControl.RemoveKeyboardService(const AService: IdxKeyboardHandlerService);
begin
  FKeyboardController.RemoveHandler(AService);
end;

procedure TdxCustomRichEditControl.SetCursor(Value: TCursor);
begin
//  if DragAndDropState <> ddsNone then
//    DragCursor := Value
//  else
    Cursor := Value;
end;

procedure TdxCustomRichEditControl.UpdateUIFromBackgroundThread(AMethod: TdxAction);
begin
  BackgroundThreadUIUpdater.UpdateUI(AMethod);
end;

procedure TdxCustomRichEditControl.SetAutoSizeMode(const Value: TdxAutoSizeMode);
begin
  if FAutoSizeMode <> Value then
  begin
    FAutoSizeMode := Value;
    SetBounds(Left, Top, Width, Height);
  end;
end;

procedure TdxCustomRichEditControl.SetLayoutUnit(const Value: TdxDocumentLayoutUnit);
begin
  if InnerControl <> nil then
    InnerControl.LayoutUnit := Value;
end;

procedure TdxCustomRichEditControl.SetModified(const Value: Boolean);
begin
  if InnerControl <> nil then
    InnerControl.Modified := Value;
end;

procedure TdxCustomRichEditControl.SetViews(const Value: TdxRichEditViewRepository);
begin
  if Views <> nil then
    Views.Assign(Value);
end;

procedure TdxCustomRichEditControl.SetWantReturns(const Value: Boolean);
begin
  if FWantReturns <> Value then
  begin
    FWantReturns := Value;
    Changed;
  end;
end;

procedure TdxCustomRichEditControl.SetWantTabs(const Value: Boolean);
begin
  if FWantTabs <> Value then
  begin
    FWantTabs := Value;
    Changed;
  end;
end;

procedure TdxCustomRichEditControl.CMNCSizeChanged(var Message: TMessage);
var
  ANewHeight, ANewWidth: Integer;
begin
  if not HandleAllocated then
    Exit;
  ANewHeight := GetSystemMetrics(SM_CYHSCROLL);
  ANewWidth  := GetSystemMetrics(SM_CXVSCROLL);
  if (FHorizontalScrollBar.Height <> ANewHeight) or (FVerticalScrollBar.Width <> ANewWidth) then
  begin
    FHorizontalScrollBar.Height := ANewHeight;
    FVerticalScrollBar.Width := ANewWidth;
    BoundsChanged;
  end;
end;

procedure TdxCustomRichEditControl.WMGetDlgCode(var Message: TWMGetDlgCode);
begin
  inherited;
  if FWantTabs then
    Message.Result := Message.Result or DLGC_WANTTAB
  else
    Message.Result := Message.Result and not DLGC_WANTTAB;
  if not FWantReturns then
    Message.Result := Message.Result and not DLGC_WANTALLKEYS;
end;

procedure TdxCustomRichEditControl.SetOptions(const Value: TdxRichEditDocumentServerOptions);
begin
  TdxInnerRichEditControlAccess(InnerControl).Options.Assign(Value);
end;

function TdxCustomRichEditControl.ShouldShowCaret: Boolean;
begin
  Result := Enabled and Focused;
end;

procedure TdxCustomRichEditControl.StartCaretBlinking;
begin
  StopCaretTimer;
  StartCaretTimer;
  ShowCaretCore;
end;

procedure TdxCustomRichEditControl.StopCaretBlinking;
begin
  StopCaretTimer;
  HideCaretCore;
end;

procedure TdxCustomRichEditControl.StartCaretTimer;
begin
  if FCaretTimer.Enabled then
    FCaretTimer.Enabled := False;
  FCaretTimer.Interval := GetCaretBlinkTime;
  FCaretTimer.OnTimer := CaretTimerHandler;
  FCaretTimer.Enabled := True;
end;

procedure TdxCustomRichEditControl.StopCaretTimer;
begin
  FCaretTimer.Enabled := False;
end;

procedure TdxCustomRichEditControl.ShowCaret;
begin
  if ShouldShowCaret then
    ShowCaretCore;
end;

procedure TdxCustomRichEditControl.ShowCaretCore;
begin
  if IsUpdateLocked then
    Exit;
  StopCaretTimer;
  EnsureSystemCaretCreated;
  if FCaret.IsHidden then
  begin
    ShowSystemCaret;
    Painter.DrawCaret;
    FCaret.IsHidden := False;
  end;
  FCaretTimer.Enabled := True;
end;

procedure TdxCustomRichEditControl.ShowSystemCaret;
var
  ACaretBounds: TRect;
begin
  if not HandleAllocated then
    Exit;
  ACaretBounds := ActiveView.GetCursorBounds;
  if Bounds.Contains(ACaretBounds.Location) then
  begin
    Windows.SetCaretPos(ACaretBounds.Left, ACaretBounds.Top);
    Windows.ShowCaret(Handle);
  end
  else
  begin
    Windows.HideCaret(Handle);
    Windows.DestroyCaret;
  end;
end;

procedure TdxCustomRichEditControl.HideSystemCaret;
var
  ACaretBounds: TRect;
begin
  if not HandleAllocated then
    Exit;
  ACaretBounds := ActiveView.GetCursorBounds;
  if Bounds.Contains(ACaretBounds.Location) then
  begin
    Windows.SetCaretPos(ACaretBounds.Left, ACaretBounds.Top);
    Windows.HideCaret(Handle);
  end
  else
  begin
    Windows.HideCaret(Handle);
    Windows.DestroyCaret;
  end;
end;

procedure TdxCustomRichEditControl.SubscribeInnerControlEvents;
begin
end;

procedure TdxCustomRichEditControl.UnsubscribeInnerControlEvents;
begin
end;


function TdxCustomRichEditControl.CreateVerticalScrollBar: IdxOfficeScrollbar;
begin
  FVerticalScrollBar := TdxOfficeScrollbar.Create(nil);
  FVerticalScrollBar.Kind := sbVertical;
  Result := CreateScrollBarCore(FVerticalScrollBar);
end;

function TdxCustomRichEditControl.CreateHorizontalScrollBar: IdxOfficeScrollbar;
begin
  FHorizontalScrollBar := TdxOfficeScrollbar.Create(nil);
  FHorizontalScrollBar.Kind := sbHorizontal;
  Result := CreateScrollBarCore(FHorizontalScrollBar);
end;

function TdxCustomRichEditControl.CreateScrollBarCore(AScrollBar: TdxOfficeScrollbar): IdxOfficeScrollbar;
begin
  AScrollBar.Enabled := False;
  AScrollBar.LookAndFeel.MasterLookAndFeel := LookAndFeel;
  Result := AScrollBar;
end;

procedure TdxCustomRichEditControl.UpdateScrollbarsVisibility;
begin
  FVerticalScrollbar.Visible := CalculateVerticalScrollbarVisibility;
  FHorizontalScrollbar.Visible := CalculateHorizontalScrollbarVisibility;
  if FHorizontalScrollbar.Visible{ActualVisible} then
    FHorizontalScrollbar.Enabled := ActiveView.HorizontalScrollController.IsScrollPossible;
  if FVerticalScrollbar.Visible{ActualVisible} then
    FVerticalScrollbar.Enabled := ActiveView.VerticalScrollController.IsScrollPossible;
end;

function TdxCustomRichEditControl.CalculateVerticalScrollbarVisibility: Boolean;
begin
  case Options.VerticalScrollbar.Visibility of
    TdxRichEditScrollbarVisibility.Hidden:
      Result := False;
  else
    Result := True;
  end;
end;

function TdxCustomRichEditControl.CalculateHorizontalScrollbarVisibility: Boolean;
var
  AIsScrollPossible: Boolean;
begin
  case Options.HorizontalScrollbar.Visibility of
    TdxRichEditScrollbarVisibility.Visible:
      Result := True;
    TdxRichEditScrollbarVisibility.Hidden:
      Result := False;
    else
    begin
      AIsScrollPossible := ActiveView.HorizontalScrollController.IsScrollPossible;
      if AIsScrollPossible then
        Exit(True);
      ActiveView.PageViewInfoGenerator.CalculateMaxPageWidth;
      Result := ActiveView.PageViewInfoGenerator.MaxPageWidth > ActiveView.PageViewInfoGenerator.ViewPortBounds.Width + 1;
    end;
  end;
end;

function TdxCustomRichEditControl.UseStandardDragDropMode: Boolean;
begin
  Result := True;
end;

{ TdxGdiMeasurementAndDrawingStrategy }

function TdxGdiMeasurementAndDrawingStrategy.CreateDocumentPainter(
  ACanvas: TcxCanvas): TObject;
begin
  Result := TdxRichEditGdiPainter.Create(ACanvas, TdxGdiBoxMeasurer(Measurer));
end;

function TdxGdiMeasurementAndDrawingStrategy.CreateFontCacheManager: TdxFontCacheManager;
begin
  Result := TdxGdiFontCacheManager.Create(DocumentModel.LayoutUnitConverter);
end;

destructor TdxGdiMeasurementAndDrawingStrategy.Destroy;
begin
  FHdcMeasureGraphics.Free;
  DeleteDC(FDC);
  inherited Destroy;
end;

function TdxGdiMeasurementAndDrawingStrategy.CreateBoxMeasurer: TdxBoxMeasurer;
begin
  Result := TdxGdiBoxMeasurer.Create(DocumentModel, FHdcMeasureGraphics);
end;

procedure TdxGdiMeasurementAndDrawingStrategy.Initialize;
begin
  FHdcMeasureGraphics := TCanvas.Create;
  FDC := CreateCompatibleDC(0);
  FHdcMeasureGraphics.Handle := FDC;
  inherited Initialize;
end;

{ TdxSimpleViewPainter }

procedure TdxSimpleViewPainter.DrawEmptyPage(ACanvas: TcxCanvas; APage: TdxPageViewInfo);
begin
end;

procedure TdxSimpleViewPainter.DrawEmptyPages(ACanvas: TcxCanvas);
begin
end;

{ TdxRichEditPaintersBase }

constructor TdxRichEditPaintersBase<TLayoutItem, TPainter>.Create;
begin
  inherited Create;
  FPainters := TObjectDictionary<TClass, TPainter>.Create([doOwnsValues]);
end;

destructor TdxRichEditPaintersBase<TLayoutItem, TPainter>.Destroy;
begin
  FreeAndNil(FPainters);
  FreeAndNil(FDefaultPainter);
  inherited Destroy;
end;

procedure TdxRichEditPaintersBase<TLayoutItem, TPainter>.AddDefault(APainter: TPainter);
begin
  FreeAndNil(FDefaultPainter);
  FDefaultPainter := APainter;
end;

procedure TdxRichEditPaintersBase<TLayoutItem, TPainter>.Add(AType: TClass; APainter: TPainter);
begin
  Painters.AddOrSetValue(AType, APainter);
end;

procedure TdxRichEditPaintersBase<TLayoutItem, TPainter>.RemoveDefault;
begin
  Assert(False);
end;

procedure TdxRichEditPaintersBase<TLayoutItem, TPainter>.Remove(AType: TClass);
begin
  Painters.Remove(AType);
end;

procedure TdxRichEditPaintersBase<TLayoutItem, TPainter>.Clear;
begin
  RemoveDefault;
  Painters.Clear;
end;

function TdxRichEditPaintersBase<TLayoutItem, TPainter>.Get(AType: TClass): TPainter;
begin
  if not Painters.TryGetValue(AType, Result) then
    Result := DefaultPainter;
end;

{ TdxOfficeSelectionPainter }

constructor TdxOfficeSelectionPainter.Create(ACanvas: TcxCanvas);
begin
  inherited Create;
  FCanvas := ACanvas;
end;

procedure TdxOfficeSelectionPainter.PopTransform;
begin
  NotImplemented;
end;

procedure TdxOfficeSelectionPainter.PushRotationTransform(ACenter: TPoint; const AAngleInDegrees: Double);
begin
  NotImplemented;
end;

function TdxOfficeSelectionPainter.TryPushRotationTransform(ACenter: TPoint; const AAngleInDegrees: Double): Boolean;
var
  ANeedApplyTransform: Boolean;
begin
  ANeedApplyTransform := ((AAngleInDegrees / 360) - Ceil(AAngleInDegrees / 360)) <> 0; 
  if not ANeedApplyTransform then
    Exit(False);
  PushRotationTransform(ACenter, AAngleInDegrees);
  Result := True;
end;

{ TdxRichEditSelectionPainter }

procedure TdxRichEditSelectionPainter.Draw(AViewInfo: TdxRowSelectionLayoutBase);
begin
  FillRectangle(AViewInfo.Bounds);
end;

procedure TdxRichEditSelectionPainter.Draw(AViewInfo: TdxRectangularObjectSelectionLayout);
var
  I: Integer;
  ABounds: TRect;
  ACenter: TPoint;
  ATransformApplied: Boolean;
  AHotZones: TdxHotZoneCollection;
begin
  ABounds := AViewInfo.Box.ActualSizeBounds;
  ACenter := TdxRectangleUtils.CenterPoint(ABounds);
  ATransformApplied := TryPushRotationTransform(ACenter,
    TdxRichEditView(AViewInfo.View).DocumentModel.GetBoxEffectiveRotationAngleInDegrees(AViewInfo.Box));
  try
    if AViewInfo.Resizeable then
      DrawRectangle(ABounds)
    else
      FillRectangle(ABounds);
    AHotZones := TdxRichEditView(AViewInfo.View).SelectionLayout.LastDocumentSelectionLayout.HotZones;
    for I := 0 to AHotZones.Count - 1 do 
      DrawHotZone(AHotZones[I]);
  finally
    if ATransformApplied then
      PopTransform;
  end;
end;

procedure TdxRichEditSelectionPainter.Draw(AViewInfo: TdxFloatingObjectAnchorSelectionLayout);
begin
  NotImplemented;
end;

procedure TdxRichEditSelectionPainter.DrawHotZone(AHotZone: IdxHotZone);
begin
  AHotZone.Accept(Self);
end;

{ TdxCustomMarkExporter }

constructor TdxCustomMarkExporter.Create;
begin
  inherited Create;
  FVisualInfoCollection := TdxCustomMarkVisualInfoCollection.Create;
end;

destructor TdxCustomMarkExporter.Destroy;
begin
  FreeAndNil(FVisualInfoCollection);
  inherited Destroy;
end;

procedure TdxCustomMarkExporter.ExportCustomMarkBox(ACustomMark: TdxCustomMark; const ABounds: TRect);
begin
  FVisualInfoCollection.Add(TdxCustomMarkVisualInfo.Create(ACustomMark, ABounds));
end;

function TdxCustomMarkExporter.GetCustomMarkVisualInfoCollection: TdxCustomMarkVisualInfoCollection;
begin
  Result := FVisualInfoCollection;
end;

{ TdxCustomMarkVisualInfo }

constructor TdxCustomMarkVisualInfo.Create(ACustomMark: TdxCustomMark; ABounds: TRect);
begin
  inherited Create;
  Assert(ACustomMark <> nil); 
  FBounds := ABounds;
  FCustomMark := ACustomMark;
end;

function TdxCustomMarkVisualInfo.GetUserData: TObject;
begin
  Result := NotImplemented;
end;

{ TdxSemitransparentSelectionPainter }

procedure TdxSemitransparentSelectionPainter.DrawLine(const AFrom, ATo: TPoint);
begin
Assert(False);
end;

procedure TdxSemitransparentSelectionPainter.DrawRectangle(const R: TRect);
begin
Assert(False);
end;

procedure TdxSemitransparentSelectionPainter.FillRectangle(const R: TRect);
var
  ABitmap: TcxBitmap32;
begin
  ABitmap := TcxBitmap32.CreateSize(R);
  try
    ABitmap.cxCanvas.FillRect(ABitmap.ClientRect, SelectionColor);
    SystemAlphaBlend(Canvas.Handle, ABitmap.cxCanvas.Handle, R, ABitmap.ClientRect, 100, False);
  finally
    ABitmap.Free;
  end;
end;


{ TdxRichEditViewBackgroundPainter }

constructor TdxRichEditViewBackgroundPainter.Create(AView: TdxRichEditView);
begin
  inherited Create;
  FView := AView;
end;

procedure TdxRichEditViewBackgroundPainter.Draw(ACanvas: TcxCanvas; const ABounds: TRect);
begin
  ACanvas.FillRect(ABounds, ActualPageBackColor);
end;

function TdxRichEditViewBackgroundPainter.GetActualPageBackColor: TColor;
var
  ADocumentProperties: TdxDocumentProperties;
  APageBackColor: TColor;
begin
  ADocumentProperties := View.DocumentModel.DocumentProperties;
  APageBackColor := ADocumentProperties.PageBackColor;
  if ADocumentProperties.DisplayBackgroundShape and (APageBackColor <> TdxColor.Empty) then
    Result := APageBackColor
  else
    Result := View.ActualBackColor;
end;

end.
