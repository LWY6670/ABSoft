{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.DocumentModel.Commands;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Types, Classes, SysUtils, Graphics,
{$IFDEF DELPHI14}
  Generics.Collections,
{$ENDIF}
  dxCore, dxCoreClasses, dxRichEdit.DocumentModel.PieceTable, dxRichEdit.DocumentModel.ParagraphFormatting,
  dxRichEdit.DocumentModel.Styles, dxRichEdit.Utils.OfficeImage, dxRichEdit.Utils.WidthsContentInfo,
  dxRichEdit.DocumentModel.Core, dxRichEdit.Utils.DataObject, dxRichEdit.Options,
  dxRichEdit.DocumentModel.Borders, dxRichEdit.DocumentModel.TableFormatting,
  dxRichEdit.Export.Rtf;

type
  TdxCopyBookmarksOperation = TObject;
  TdxTableCellCollection = TObject;
  TdxSelectedCellsIntervalInRow = TObject;

  TdxSyntaxHighlightHistoryItem = class;
  TdxPieceTableInsertContentConvertedToDocumentModelCommand = class;

  { TdxDocumentModelCommand }

  TdxDocumentModelCommand = class abstract
  private
    FDocumentModel: TdxDocumentModel;
    FTransaction: TdxHistoryTransaction;
    function GetPieceTable: TdxPieceTable;
  protected
    procedure BeginExecute; virtual;
    procedure EndExecute; virtual;

    procedure ExecuteCore; virtual; abstract;
    procedure CalculateExecutionParameters; virtual; abstract;
    procedure CalculateApplyChangesParameters; virtual; abstract;
    procedure ApplyChanges; virtual; abstract;
  public
    constructor Create(ADocumentModel: TdxDocumentModel);
    procedure Execute;

    property DocumentModel: TdxDocumentModel read FDocumentModel;
    property PieceTable: TdxPieceTable read GetPieceTable;
    property Transaction: TdxHistoryTransaction read FTransaction;
  end;

  { TdxDocumentModelInsertObjectCommand }

  TdxDocumentModelInsertObjectCommand = class abstract (TdxDocumentModelCommand)
  private
    FParagraphIndex: TdxParagraphIndex;
    FApplyChangesParagraphIndex: TdxParagraphIndex;
  protected
    function GetChangeType: TdxDocumentModelChangeType; virtual; abstract;
    procedure CalculateExecutionParameters; override;
    procedure CalculateApplyChangesParameters; override;
    procedure ApplyChanges; override;

    function CalculateInsertionParagraphIndex: TdxParagraphIndex; virtual; abstract;

    property ChangeType: TdxDocumentModelChangeType read GetChangeType;
  public
    property ParagraphIndex: TdxParagraphIndex read FParagraphIndex;
  end;

  { TdxDocumentModelInsertObjectAtLogPositionCommand }

  TdxDocumentModelInsertObjectAtLogPositionCommand = class abstract (TdxDocumentModelInsertObjectCommand)
  private
    FForceVisible: Boolean;
    FLogPosition: TdxDocumentLogPosition;
  protected
    function CalculateInsertionParagraphIndex: TdxParagraphIndex; override;
  public
    constructor Create(ADocumentModel: TdxDocumentModel; ALogPosition: TdxDocumentLogPosition; AForceVisible: Boolean);

    property LogPosition: TdxDocumentLogPosition read FLogPosition;
    property ForceVisible: Boolean read FForceVisible;
  end;

  { TdxDocumentModelInsertObjectAtInputPositionCommand }

  TdxDocumentModelInsertObjectAtInputPositionCommand = class abstract(TdxDocumentModelInsertObjectCommand)
  private
    FPosition: TdxInputPosition;
  protected
    function CalculateInsertionParagraphIndex: TdxParagraphIndex; override;
  public
    constructor Create(ADocumentModel: TdxDocumentModel; APosition: TdxInputPosition);

    property Position: TdxInputPosition read FPosition;
  end;

  { TdxPieceTableCommand }

  TdxPieceTableCommand = class abstract
  private
    FPieceTable: TdxPieceTable;
    FTransaction: TdxHistoryTransaction;
    FResult: TObject;
    function GetDocumentModel: TdxDocumentModel;
  protected
    procedure BeginExecute; virtual;
    procedure EndExecute; virtual;

		procedure ExecuteCore; virtual; abstract;
    procedure CalculateExecutionParameters; virtual; abstract;
    procedure CalculateApplyChangesParameters; virtual; abstract;
    procedure ApplyChanges; virtual; abstract;
  public
    constructor Create(APieceTable: TdxPieceTable); virtual;
    procedure Execute; virtual;

    property PieceTable: TdxPieceTable read FPieceTable;
    property DocumentModel: TdxDocumentModel read GetDocumentModel;
    property Transaction: TdxHistoryTransaction read FTransaction;
    property &Result: TObject read FResult write FResult;
  end;

  { TdxPieceTablePasteContentCommand }

  TdxPieceTablePasteContentCommand = class abstract(TdxPieceTableCommand)
  private
    FPasteSource: IdxPasteSource;
  protected
    function GetFormat: TdxDocumentFormat; virtual; abstract;
  public
    constructor Create(APieceTable: TdxPieceTable); override;

    function IsDataAvailable: Boolean; virtual; abstract;

    property Format: TdxDocumentFormat read GetFormat;
    property PasteSource: IdxPasteSource read FPasteSource write FPasteSource;
  end;

  { TdxPieceTablePasteContentConvertedToDocumentModelCommandBase }

  TdxPieceTablePasteContentConvertedToDocumentModelCommandBase = class abstract(TdxPieceTablePasteContentCommand)
  private
    FCopyBetweenInternalModels: Boolean;
    FForceInsertFloatingObjectAtParagraphStart: Boolean;
    FInsertOptions: TdxInsertOptions;
    FPasteFromIE: Boolean;
  protected
    procedure CalculateExecutionParameters; override;
    procedure ExecuteCore; override;
    procedure CalculateApplyChangesParameters; override;
    procedure ApplyChanges; override;

    function CreateSourceDocumentModel(const ASizeCollection: string): TdxDocumentModel; virtual; abstract;
    function CreatePasteDocumentModelCommand(APos: TdxDocumentLogPosition; ASource: TdxDocumentModel;
      ASuppressFieldsUpdate, APasteFromIE: Boolean): TdxPieceTableInsertContentConvertedToDocumentModelCommand; virtual;
    function GetAdditionalContentString: string; virtual;
    function GetDocumentModelSingleFloatingObjectAnchorRun(AModel: TdxDocumentModel): TdxFloatingObjectAnchorRun;
    procedure OffsetNewlyInsertedFloatingObjectIfNeed(ANewRun: TdxFloatingObjectAnchorRun; AParagraph: TdxParagraph);
    procedure OffsetNewlyInsertedFloatingObject(ANewRun: TdxFloatingObjectAnchorRun; ARun: TdxFloatingObjectAnchorRun);
    procedure PasteContent(ASource: TdxDocumentModel; APos: TdxDocumentLogPosition; const ASizeCollection: string); virtual;
    procedure PasteDocumentModel(APos: TdxDocumentLogPosition; ASource: TdxDocumentModel; APasteFromIE: Boolean; ASuppressFieldsUpdate: Boolean = False);
    function PreprocessSingleFloatingObjectInsertion(ARun: TdxFloatingObjectAnchorRun): TdxParagraph;
    procedure SetSuppressStoreImageSize(ADocumentModel: TdxDocumentModel; const ASizeCollection: string); virtual;
    function ShouldOffsetNewRun(ANewRun: TdxFloatingObjectAnchorRun; ARun: TdxFloatingObjectAnchorRun): Boolean;
    function SuppressCopySectionProperties(ASource: TdxDocumentModel): Boolean; virtual;
  public
    constructor Create(APieceTable: TdxPieceTable); overload; override;
    constructor Create(APieceTable: TdxPieceTable; AInsertOptions: TdxInsertOptions); reintroduce; overload;

    property CopyBetweenInternalModels: Boolean read FCopyBetweenInternalModels write FCopyBetweenInternalModels;
    property ForceInsertFloatingObjectAtParagraphStart: Boolean read FForceInsertFloatingObjectAtParagraphStart write FForceInsertFloatingObjectAtParagraphStart;
    property InsertOptions: TdxInsertOptions read FInsertOptions;
    property PasteFromIE: Boolean read FPasteFromIE write FPasteFromIE;
  end;

  { TdxPieceTablePasteTextContentConvertedToDocumentModelCommandBase }

  TdxPieceTablePasteTextContentConvertedToDocumentModelCommandBase = class abstract(TdxPieceTablePasteContentConvertedToDocumentModelCommandBase)
  protected
    function CreateSourceDocumentModel(const ASizeCollection: string): TdxDocumentModel; override;
    function CreateDocumentModelFromContentString(AContent: TdxClipboardStringContent; const ASizeCollection: string): TdxDocumentModel; virtual;
    function GetContent: TdxClipboardStringContent; virtual; abstract;
    procedure PopulateDocumentModelFromContentStringCore(ADocumentModel: TdxDocumentModel; AContent: TdxClipboardStringContent; const ASizeCollection: string); virtual; abstract;
    procedure PasteContent(AContent: TdxClipboardStringContent;
      APos: TdxDocumentLogPosition; const ASizeCollection: string); reintroduce; overload; virtual;
  end;

  { TdxPieceTablePasteRtfTextCommand }

  TdxPieceTablePasteRtfTextCommand = class(TdxPieceTablePasteTextContentConvertedToDocumentModelCommandBase)
  private
    function GetIsPasteFromIe: Boolean;
  protected
    function GetAdditionalContentString: string; override;
    function GetFormat: TdxDocumentFormat; override;
    function GetContent: TdxClipboardStringContent; override;
    procedure PopulateDocumentModelFromContentStringCore(ADocumentModel: TdxDocumentModel;
      AContent: TdxClipboardStringContent; const ASizeCollection: string); override;
    function PrepareInputStream(const AStr: string): TStream;
  public
    function IsDataAvailable: Boolean; override;
  end;

  { TdxPieceTableInsertObjectCommand }

  TdxPieceTableInsertObjectCommand = class abstract (TdxPieceTableCommand)
  private
    FParagraphIndex: TdxParagraphIndex;
    FApplyChangesParagraphIndex: TdxParagraphIndex;
  protected
    function GetChangeType: TdxDocumentModelChangeType; virtual; abstract;
    procedure CalculateExecutionParameters; override;
    procedure CalculateApplyChangesParameters; override;
    procedure ApplyChanges; override;
    function CalculateInsertionParagraphIndex: TdxParagraphIndex; virtual; abstract;
  public
    property ParagraphIndex: TdxParagraphIndex read FParagraphIndex;
    property ChangeType: TdxDocumentModelChangeType read GetChangeType;
  end;

  { TdxPieceTableInsertObjectAtLogPositionCommand }

  TdxPieceTableInsertObjectAtLogPositionCommand = class abstract (TdxPieceTableInsertObjectCommand)
  private
    FForceVisible: Boolean;
    FLogPosition: TdxDocumentLogPosition;
  protected
    function CalculateInsertionParagraphIndex: TdxParagraphIndex; override;
  public
    constructor Create(APieceTable: TdxPieceTable; ALogPosition: TdxDocumentLogPosition; AForceVisible: Boolean); reintroduce;

    property ForceVisible: Boolean read FForceVisible;
    property LogPosition: TdxDocumentLogPosition read FLogPosition;
  end;

  { TdxPieceTableInsertObjectAtInputPositionCommand }

  TdxPieceTableInsertObjectAtInputPositionCommand = class abstract (TdxPieceTableInsertObjectCommand)
  private
    FForceVisible: Boolean;
    FPosition: TdxInputPosition;
  protected
    function CalculateInsertionParagraphIndex: TdxParagraphIndex; override;
  public
    constructor Create(APieceTable: TdxPieceTable; APosition: TdxInputPosition; AForceVisible: Boolean); reintroduce;

    property Position: TdxInputPosition read FPosition;
    property ForceVisible: Boolean read FForceVisible;
  end;

  { TdxPieceTableInsertTextAtLogPositionCommand }

  TdxPieceTableInsertTextAtLogPositionCommand = class(TdxPieceTableInsertObjectAtLogPositionCommand)
  private
    FText: string;
  protected
    function GetChangeType: TdxDocumentModelChangeType; override;
    procedure ExecuteCore; override;
  public
    constructor Create(APieceTable: TdxPieceTable; ALogPosition: TdxDocumentLogPosition; const AText: string; AForceVisible: Boolean);

    property Text: string read FText;
  end;

  { TdxPieceTableInsertPlainTextAtLogPositionCommand }

  TdxPieceTableInsertPlainTextAtLogPositionCommand = class(TdxPieceTableInsertTextAtLogPositionCommand)
  protected
    procedure ExecuteCore; override;
    procedure InsertPlainTextCore(APosition: TdxDocumentLogPosition; const AText: string); virtual;
    function GetNextLine(const ASource: string; var AIndex: Integer): string;
    class function ShouldAddParagraph(const ASource: string; var AIndex: Integer): Boolean; static;
  end;

  { TdxPieceTableInsertTextAtInputPositionCommand }

  TdxPieceTableInsertTextAtInputPositionCommand = class(TdxPieceTableInsertObjectAtInputPositionCommand)
  private
    FText: string;
  protected
    procedure ExecuteCore; override;
    function GetChangeType: TdxDocumentModelChangeType; override;
  public
    constructor Create(APieceTable: TdxPieceTable; AInputPosition: TdxInputPosition; const AText: string; AForceVisible: Boolean);

    property Text: string read FText;
  end;

  { TdxPieceTableInsertPlainTextAtInputPositionCommand }

  TdxPieceTableInsertPlainTextAtInputPositionCommand = class(TdxPieceTableInsertObjectAtInputPositionCommand)
  private
    FText: string;
  protected
    function GetChangeType: TdxDocumentModelChangeType; override;
    procedure ExecuteCore; override;
    procedure InsertPlainTextCore(APosition: TdxInputPosition; const AText: string); virtual;
  public
    constructor Create(APieceTable: TdxPieceTable; AInputPosition: TdxInputPosition; const AText: string; AForceVisible: Boolean);

    property Text: string read FText;
  end;

  { TdxPieceTableInsertParagraphAtLogPositionCommand }

  TdxPieceTableInsertParagraphAtLogPositionCommand = class(TdxPieceTableInsertObjectAtLogPositionCommand)
  protected
    function GetChangeType: TdxDocumentModelChangeType; override;
    procedure ExecuteCore; override;
  end;

  { TdxPieceTableInsertInlinePictureAtLogPositionCommand }

  TdxPieceTableInsertInlinePictureAtLogPositionCommand = class(TdxPieceTableInsertObjectAtLogPositionCommand)
  private
    FScaleX: Integer;
    FScaleY: Integer;
    FImage: TdxOfficeImageReference;
  protected
    procedure ExecuteCore; override;
    function GetChangeType: TdxDocumentModelChangeType; override;
  public
    constructor Create(APieceTable: TdxPieceTable; ALogPosition: TdxDocumentLogPosition;
      AImage: TdxOfficeImageReference; AScaleX, AScaleY: Integer; AForceVisible: Boolean);
    destructor Destroy; override;

    property Image: TdxOfficeImageReference read FImage;
    property ScaleX: Integer read FScaleX;
    property ScaleY: Integer read FScaleY;
  end;

  { TdxPieceTableInsertCustomRunAtLogPositionCommand }

  TdxPieceTableInsertCustomRunAtLogPositionCommand = class(TdxPieceTableInsertObjectAtLogPositionCommand)
  private
    FCustomRunObject: ICustomRunObject;
  protected
    function GetChangeType: TdxDocumentModelChangeType; override;
    procedure ExecuteCore; override;
  public
    constructor Create(APieceTable: TdxPieceTable; ALogPosition: TdxDocumentLogPosition; const ACustomRunObject: ICustomRunObject; AForceVisible: Boolean);

    property CustomRunObject: ICustomRunObject read FCustomRunObject;
  end;

  { TdxPieceTableInsertInlineCustomObjectAtLogPositionCommand }

  TdxPieceTableInsertInlineCustomObjectAtLogPositionCommand = class(TdxPieceTableInsertObjectAtLogPositionCommand)
  private
    FCustomObject: IInlineCustomObject;
    FScaleX: Integer;
    FScaleY: Integer;
  protected
    function GetChangeType: TdxDocumentModelChangeType; override;
    procedure ExecuteCore; override;
  public
    constructor Create(APieceTable: TdxPieceTable; ALogPosition: TdxDocumentLogPosition; const ACustomObject: IInlineCustomObject; AScaleX, AScaleY: Integer; AForceVisible: Boolean);

    property CustomObject: IInlineCustomObject read FCustomObject;
    property ScaleX: Integer read FScaleX;
    property ScaleY: Integer read FScaleY;
  end;

  { TdxPieceTableInsertFloatingObjectAnchorAtLogPositionCommand }

  TdxPieceTableInsertFloatingObjectAnchorAtLogPositionCommand = class(TdxPieceTableInsertObjectAtLogPositionCommand)
  protected
    function GetChangeType: TdxDocumentModelChangeType; override;
    procedure ExecuteCore; override;
  end;

  { TdxPieceTableInsertContentConvertedToDocumentModelCommand }

  TdxPieceTableInsertContentConvertedToDocumentModelCommand = class(TdxPieceTableInsertObjectAtLogPositionCommand)
  private
    FPasteFromIE: Boolean;
    FSuppressParentFieldsUpdate: Boolean;
    FCopyLastParagraph: Boolean;
    FInsertOptions: TdxInsertOptions;
    FSuppressFieldsUpdate: Boolean;
    FSuppressCopySectionProperties: Boolean;
    FCopyBetweenInternalModels: Boolean;
    FSourceModel: TdxDocumentModel;
    FIsMergingTableCell: Boolean;
  protected
    function GetChangeType: TdxDocumentModelChangeType; override;
    procedure ExecuteCore; override;

    property SourceModel: TdxDocumentModel read FSourceModel;
    property IsMergingTableCell: Boolean read FIsMergingTableCell write FIsMergingTableCell;
  public
    constructor Create(ATargetPieceTable: TdxPieceTable; ASourceModel: TdxDocumentModel;
      ALogPosition: TdxDocumentLogPosition; AInsertOptions: TdxInsertOptions; AForceVisible: Boolean); overload;
    constructor Create(ATargetPieceTable: TdxPieceTable; ASourceModel: TdxDocumentModel;
      ALogPosition: TdxDocumentLogPosition; AForceVisible: Boolean); overload;
    procedure CopyDocumentModelContent(ACopyManager: TdxDocumentModelCopyManager; ALength: Integer);
    function GetDocumentLength(ADocumentModel: TdxDocumentModel): Integer;

    property InsertOptions: TdxInsertOptions read FInsertOptions;
    property SuppressParentFieldsUpdate: Boolean read FSuppressParentFieldsUpdate write FSuppressParentFieldsUpdate;
    property SuppressFieldsUpdate: Boolean read FSuppressFieldsUpdate write FSuppressFieldsUpdate;
    property CopyBetweenInternalModels: Boolean read FCopyBetweenInternalModels write FCopyBetweenInternalModels;
    property CopyLastParagraph: Boolean read FCopyLastParagraph write FCopyLastParagraph;
    property PasteFromIE: Boolean read FPasteFromIE write FPasteFromIE;
    property SuppressCopySectionProperties: Boolean read FSuppressCopySectionProperties write FSuppressCopySectionProperties;
  end;

  { TdxDocumentModelCopyCommand }

  TdxDocumentModelCopyCommand = class(TdxPieceTableCommand)
  private
    FTableCopyFromNestedLevel: Integer;
    FAllowCopyWholeFieldResult: Boolean;
    FFixLastParagraph: Boolean;
    FSuppressFieldsUpdate: Boolean;
    FTargetModel: TdxDocumentModel;
    FOptions: TdxDocumentModelCopyOptions;
  protected
    procedure ExecuteCore; override;
    function CreateCopyBookmarkOperation(ACopyManager: TdxDocumentModelCopyManager): TdxCopyBookmarksOperation; virtual;
    procedure NormalizeTables(APieceTable: TdxPieceTable); 
    class procedure ReplaceDefaultProperties(ATargetModel: TdxDocumentModel; ASourceModel: TdxDocumentModel); static;
    class procedure ReplaceDefaultStyles(ATargetModel, ASourceModel: TdxDocumentModel); static;
    class procedure CopyStyles(ATargetModel, ASourceModel: TdxDocumentModel); static;
    procedure CopyDocumentVariables(ATargetModel, ASourceModel: TdxDocumentModel);
    class procedure ReplaceStylesCore(ATargetStyles, ASourceStyles: TdxStyleCollectionBase); static;
    class procedure CopyStylesCore(ATargetStyles, ASourceStyles: TdxStyleCollectionBase); static;
    procedure CalculateExecutionParameters; override;
    procedure ApplyChanges; override;
    procedure CalculateApplyChangesParameters; override;

    property TargetModel: TdxDocumentModel read FTargetModel;
  public
    constructor Create(ASourcePieceTable: TdxPieceTable; ATargetModel: TdxDocumentModel; AOptions: TdxDocumentModelCopyOptions); reintroduce;

    property FixLastParagraph: Boolean read FFixLastParagraph write FFixLastParagraph;
    property TableCopyFromNestedLevel: Integer read FTableCopyFromNestedLevel write FTableCopyFromNestedLevel;
    property AllowCopyWholeFieldResult: Boolean read FAllowCopyWholeFieldResult write FAllowCopyWholeFieldResult;
    property SuppressFieldsUpdate: Boolean read FSuppressFieldsUpdate write FSuppressFieldsUpdate;
  end;

  { TdxPieceTableCreateFieldCommand }

  TdxPieceTableCreateFieldCommand = class(TdxPieceTableInsertObjectAtLogPositionCommand)
  private
    FLength: Integer;
    FInsertedField: TdxField;
  protected
    procedure ExecuteCore; override;
    function GetChangeType: TdxDocumentModelChangeType; override;
  public
    constructor Create(APieceTable: TdxPieceTable; ALogPosition: TdxDocumentLogPosition; ALength: Integer; AForceVisible: Boolean);

    property Length: Integer read FLength;
    property InsertedField: TdxField read FInsertedField;
  end;

  { TdxPieceTableCreateFieldWithResultCommand }

  TdxPieceTableCreateFieldWithResultCommand = class(TdxPieceTableInsertObjectCommand)
  private
    FInsertedField: TdxField;
    FStartCode: TdxDocumentLogPosition;
    FEndCode: TdxDocumentLogPosition;
    FResultLength: Integer;
    FForceVisible: Boolean;
  protected
    function GetChangeType: TdxDocumentModelChangeType; override;
    function CalculateInsertionParagraphIndex: TdxParagraphIndex; override;
    procedure ExecuteCore; override;
    function InsertStartCodeRun: TdxRunIndex; virtual;
    function InsertEndCodeRun(AEndCodePosition: TdxDocumentLogPosition): TdxRunIndex; virtual;
    function InsertEndResultRun(AEndResultPosition: TdxDocumentLogPosition): TdxRunIndex; virtual;
  public
    constructor Create(APieceTable: TdxPieceTable; AStartCode, AEndCode: TdxDocumentLogPosition;
      AResultLength: Integer; AForceVisible: Boolean); reintroduce;

    property InsertedField: TdxField read FInsertedField;
  end;

  { TdxPieceTableInsertSeparatorAtLogPositionCommand }

  TdxPieceTableInsertSeparatorAtLogPositionCommand = class(TdxPieceTableInsertObjectAtLogPositionCommand)
  protected
    function GetChangeType: TdxDocumentModelChangeType; override;
    procedure ExecuteCore; override;
  end;

  { TdxPieceTableInsertDataContainerAtLogPositionCommand }

  TdxPieceTableInsertDataContainerAtLogPositionCommand = class(TdxPieceTableInsertObjectAtLogPositionCommand)
  private
    FDataContainer: IDataContainer;
  protected
    function GetChangeType: TdxDocumentModelChangeType; override;
    procedure ExecuteCore; override;
  public
    constructor Create(APieceTable: TdxPieceTable; ALogPosition: TdxDocumentLogPosition;
      ADataContainer: IDataContainer; AForceVisible: Boolean);

    property DataContainer: IDataContainer read FDataContainer;
  end;

  { TdxPieceTableDeleteFieldWithoutResultCommand }

  TdxPieceTableDeleteFieldWithoutResultCommand = class(TdxPieceTableCommand)
  private
    FField: TdxField;
    FRunIndex: TdxRunIndex;
  protected
    procedure ExecuteCore; override;
    procedure CalculateExecutionParameters; override;
    procedure CalculateApplyChangesParameters; override;
    procedure ApplyChanges; override;
  public
    constructor Create(APieceTable: TdxPieceTable; AField: TdxField); reintroduce;
  end;

  { TdxPieceTableDeleteTextCommand }

  TdxPieceTableDeleteTextCommand = class(TdxPieceTableCommand)
  private
    FLength: Integer;
    FForceRemoveInnerFields: Boolean;
    FLogPosition: TdxDocumentLogPosition;
    FBackspacePressed: Boolean;
    FDocumentLastParagraphSelected: Boolean;
    FAllowPartiallyDeletingField: Boolean;
    FLeaveFieldIfResultIsRemoved: Boolean;
    FParagraphIndex: TdxParagraphIndex;
    FSectionBreakDeleted: Boolean;
  protected
    procedure CalculateExecutionParameters; override;
    procedure ExecuteCore; override;
    function CreateDeleteContentOperation: TdxDeleteContentOperation; virtual;
    procedure CalculateApplyChangesParameters; override;
    procedure ApplyChanges; override;
  public
    constructor Create(APieceTable: TdxPieceTable; ALogPosition: TdxDocumentLogPosition; ALength: Integer); reintroduce;

    property DocumentLastParagraphSelected: Boolean read FDocumentLastParagraphSelected write FDocumentLastParagraphSelected;
    property LeaveFieldIfResultIsRemoved: Boolean read FLeaveFieldIfResultIsRemoved write FLeaveFieldIfResultIsRemoved;
    property LogPosition: TdxDocumentLogPosition read FLogPosition;
    property Length: Integer read FLength;
    property AllowPartiallyDeletingField: Boolean read FAllowPartiallyDeletingField write FAllowPartiallyDeletingField;
    property ForceRemoveInnerFields: Boolean read FForceRemoveInnerFields write FForceRemoveInnerFields;
    property BackspacePressed: Boolean read FBackspacePressed write FBackspacePressed;
  end;

  { TdxDocumentModelInsertSectionAtLogPositionCommand }

  TdxDocumentModelInsertSectionAtLogPositionCommand = class(TdxDocumentModelInsertObjectAtLogPositionCommand)
  protected
    function GetChangeType: TdxDocumentModelChangeType; override;
    procedure ExecuteCore; override;
  end;

  { TdxPieceTableTableBaseCommand }

  TdxPieceTableTableBaseCommand = class abstract(TdxPieceTableCommand)
  protected
    procedure CalculateExecutionParameters; override;
    procedure CalculateApplyChangesParameters; override;
    procedure ApplyChanges; override;
  end;

  { TdxPieceTableTableDocumentServerOwnerCommand }

  TdxPieceTableTableDocumentServerOwnerCommand = class abstract(TdxPieceTableTableBaseCommand)
  private
    FServer: IdxInnerRichEditDocumentServerOwner;
  public
    constructor Create(APieceTable: TdxPieceTable; AServer: IdxInnerRichEditDocumentServerOwner); reintroduce;

    property DocumentServer: IdxInnerRichEditDocumentServerOwner read FServer;
  end;

  { TdxPieceTableCreateEmptyTableCommand }

  TdxPieceTableCreateEmptyTableCommand = class(TdxPieceTableTableBaseCommand)
  private
    FInsertedTable: TdxTable;
    FSourceCell: TdxTableCell;
  protected
    procedure ExecuteCore; override;
  public
    constructor Create(APieceTable: TdxPieceTable; ASourceCell: TdxTableCell); reintroduce;
    procedure Execute; override;

    property NewTable: TdxTable read FInsertedTable;
  end;

  { TdxPieceTableCreateRowEmptyCommand }

  TdxPieceTableCreateRowEmptyCommand = class(TdxPieceTableTableBaseCommand)
  private
    FInsertedTableRow: TdxTableRow;
    FTable: TdxTable;
    FIndex: Integer;
  protected
    procedure ExecuteCore; override;
  public
    constructor Create(APieceTable: TdxPieceTable; ATable: TdxTable; AIndex: Integer); reintroduce;
    procedure Execute; override;

    property InsertedRow: TdxTableRow read FInsertedTableRow;
  end;

  { TdxPieceTableCreateCellEmptyCommand }

  TdxPieceTableCreateCellEmptyCommand = class(TdxPieceTableTableBaseCommand)
  private
    FInsertedIndex: Integer;
    FStartParagraphIndex: TdxParagraphIndex;
    FRow: TdxTableRow;
    FEndParagraphIndex: TdxParagraphIndex;
    FInsertedTableCell: TdxTableCell;
  protected
    procedure ExecuteCore; override;
  public
    constructor Create(APieceTable: TdxPieceTable; ARow: TdxTableRow; AInsertedIndex: Integer;
      AStart, AEnd: TdxParagraphIndex); reintroduce;

    property InsertedCell: TdxTableCell read FInsertedTableCell;
    property StartParagraphIndex: TdxParagraphIndex  read FStartParagraphIndex;
    property EndParagraphIndex: TdxParagraphIndex read FEndParagraphIndex;
    property Row: TdxTableRow read FRow;
    property InsertedIndex: Integer read FInsertedIndex;
  end;

  { TdxPieceTableInsertTableRowCommand }

  TdxPieceTableInsertTableRowCommand = class abstract(TdxPieceTableCommand)
  private
    FPatternRow: TdxTableRow;
    FPositionToParagraphsInsert: TdxDocumentLogPosition;
    FNewRowStartParagraphIndex: TdxParagraphIndex;
    FNewRowIndex: Integer;
    FForceVisible: Boolean;
  protected
    procedure ExecuteCore; override;
    procedure CorrentVerticalMerging(ARow: TdxTableRow; I: Integer;
      ASourceCell: TdxTableCell; ATargetCell: TdxTableCell); virtual; abstract;
    procedure CopyPropertiesFromPatternCell(ASource, ATarget: TdxTableCell); virtual; abstract;
    procedure CopyCharacterAndParagraphFormattingFromPatternCell(ASource, ATarget: TdxTableCell); virtual;
    procedure InsertParagraphs(ALogPosition: TdxDocumentLogPosition; ACells: TdxTableCellCollection);
    procedure CalculateApplyChangesParameters; override;
    procedure ApplyChanges; override;
    procedure AfterParagraphsInserted; virtual; abstract;
    function GetNextRow(ARow: TdxTableRow): TdxTableRow; virtual; abstract;

    property NewRowStartParagraphIndex: TdxParagraphIndex read FNewRowStartParagraphIndex
      write FNewRowStartParagraphIndex;
    property PositionToParagraphsInsert: TdxDocumentLogPosition read FPositionToParagraphsInsert
      write FPositionToParagraphsInsert;
    property NewRowIndex: Integer read FNewRowIndex write FNewRowIndex;
  public
    constructor Create(APieceTable: TdxPieceTable; APatternRow: TdxTableRow; AForceVisible: Boolean); reintroduce;

    property PatternRow: TdxTableRow read FPatternRow;
  end;

  { TdxPieceTableInsertTableRowBelowCommand }

  TdxPieceTableInsertTableRowBelowCommand = class(TdxPieceTableInsertTableRowCommand)
  protected
    procedure CalculateExecutionParameters; override;
    procedure AfterParagraphsInserted; override;
    function GetNextRow(ARow: TdxTableRow): TdxTableRow; override;
    procedure CorrentVerticalMerging(ACreatedRow: TdxTableRow; I: Integer; ASourceCell, ATargetCell: TdxTableCell); override;
    procedure CopyPropertiesFromPatternCell(ASourceCell, ATargetCell: TdxTableCell); override;
  end;

  { TdxPieceTableInsertTableRowAboveCommand }

  TdxPieceTableInsertTableRowAboveCommand = class(TdxPieceTableInsertTableRowCommand)
  protected
    procedure CalculateExecutionParameters; override;
    procedure AfterParagraphsInserted; override;
    function GetNextRow(ARow: TdxTableRow): TdxTableRow; override;
    procedure CorrentVerticalMerging(ARow: TdxTableRow; I: Integer; ASourceCell, ATargetCell: TdxTableCell); override;
    procedure CopyPropertiesFromPatternCell(ASourceCell, ATargetCell: TdxTableCell); override;
  end;

  { TdxPieceTableMergeTableCellsCommandBase }

  TdxPieceTableMergeTableCellsCommandBase = class abstract(TdxPieceTableTableBaseCommand)
  private
    FCell: TdxTableCell;
    FNeedDeleteNextTableCell: Boolean;
    FCopyManager: TdxDocumentModelCopyManager;
    FSuppressNormalizeTableRows: Boolean;
    FCopyDocumentModel: TdxDocumentModel;
    function GetCopyPieceTable: TdxPieceTable;
  protected
    procedure ExecuteCore; override;
    function CalculateNextCell: TdxTableCell; virtual; abstract;
    procedure UpdateProperties(ANextCell: TdxTableCell); virtual; abstract;
    function CalculateSelectionRange(ACell: TdxTableCell): TdxSelectionRange; virtual;
    function IsEmptyCell(ACell: TdxTableCell): Boolean; virtual;
    procedure CopyToCopyPieceTable(ACopyingRange: TdxSelectionRange); virtual;
    procedure DeleteTableCellWithContent(ANextCell: TdxTableCell; ADeletingRange: TdxSelectionRange); virtual;
    procedure CopyToPieceTable; virtual;
    procedure FixParagraphsInPatternCell(ANeedDeleteFirstParagraphInCell, ANeedDeleteLastParagraphInCell: Boolean); virtual;

    property CopyPieceTable: TdxPieceTable read GetCopyPieceTable;
    property CopyManager: TdxDocumentModelCopyManager read FCopyManager write FCopyManager;
    property NeedDeleteNextTableCell: Boolean read FNeedDeleteNextTableCell write FNeedDeleteNextTableCell;
    property PatternCell: TdxTableCell read FCell write FCell;
    property SuppressNormalizeTableRows: Boolean read FSuppressNormalizeTableRows write FSuppressNormalizeTableRows;
  public
		constructor Create(APieceTable: TdxPieceTable; ACell: TdxTableCell); reintroduce;
  end;

  { TdxPieceTableMergeTwoTableCellsHorizontallyCommand }

  TdxPieceTableMergeTwoTableCellsHorizontallyCommand = class(TdxPieceTableMergeTableCellsCommandBase)
  protected
    function CalculateNextCell: TdxTableCell; override;
    procedure UpdateProperties(ANextCell: TdxTableCell); override;
    procedure DeleteTableCellWithContent(ANextCell: TdxTableCell; ADeletingRange: TdxSelectionRange); override;
  end;

  { TdxPieceTableMergeTwoTableCellsVerticallyCommand }

  TdxPieceTableMergeTwoTableCellsVerticallyCommand = class(TdxPieceTableMergeTableCellsCommandBase)
  protected
    function CalculateNextCell: TdxTableCell; override;
    procedure UpdateProperties(ANextCell: TdxTableCell); override;
  end;

  { TdxPieceTableMergeTableCellsHorizontallyCommand }

  TdxPieceTableMergeTableCellsHorizontallyCommand = class(TdxPieceTableTableBaseCommand)
  private
    FCell: TdxTableCell;
    FCount: Integer;
  protected
    function CalculateCellIndex(ARow: TdxTableRow; AStartCellIndex, AColumnSpan: Integer): Integer; virtual;
    procedure ExecuteCore; override;
  public
    constructor Create(APieceTable: TdxPieceTable; ACell: TdxTableCell; ACount: Integer); reintroduce;
  end;

  { TdxPieceTableMergeTableCellsVerticallyCommand }

  TdxPieceTableMergeTableCellsVerticallyCommand = class(TdxPieceTableTableBaseCommand)
  private
    FCell: TdxTableCell;
    FCount: Integer;
  protected
    procedure ExecuteCore; override;
  public
    constructor Create(APieceTable: TdxPieceTable; ACell: TdxTableCell; ACount: Integer); reintroduce;
  end;

  { TdxPieceTableDeleteTableCellWithNestedTablesCommand }

  TdxPieceTableDeleteTableCellWithNestedTablesCommand = class(TdxPieceTableTableBaseCommand)
  private
    FTableIndex: Integer;
    FRowIndex: Integer;
    FCellIndex: Integer;
  protected
    procedure ExecuteCore; override;
  public
    constructor Create(APieceTable: TdxPieceTable; ATableIndex, ARowIndex, ACellIndex: Integer); reintroduce;
  end;

  { TdxPieceTableSplitTableCommand }

  TdxPieceTableSplitTableCommand = class(TdxPieceTableTableBaseCommand)
  private
    FCopyManager: TdxDocumentModelCopyManager;
    FCopyDocumentModel: TdxDocumentModel;
    FTableIndex: Integer;
    FRowIndex: Integer;
    FForceVisible: Boolean;
    function GetCopyPieceTable: TdxPieceTable;
    function GetTable: TdxTable;
    function GetParagraphs: TdxParagraphCollection;
  protected
    procedure ExecuteCore; override;
    procedure InsertParagraphBeforeTable; virtual;
    function CalculateSelectionRange: TdxSelectionRange; virtual;
    procedure CopyToCopyPieceTable(ACopyingRange: TdxSelectionRange); virtual;
    procedure DeleteContent(ADeletingRange: TdxSelectionRange); virtual;
    procedure CopyToPieceTable; virtual;
    procedure InsertParagraphWithDefaultProperties(APosition: TdxDocumentLogPosition); virtual;

    property CopyPieceTable: TdxPieceTable read GetCopyPieceTable;
    property CopyManager: TdxDocumentModelCopyManager read FCopyManager write FCopyManager;
    property Table: TdxTable read  GetTable;
    property Paragraphs: TdxParagraphCollection read GetParagraphs;
  public
    constructor Create(APieceTable: TdxPieceTable; ATableIndex, ARowIndex: Integer; AForceVisible: Boolean); reintroduce;
  end;

  { TdxPieceTablePieceTableInsertColumnBase }

  TdxPieceTablePieceTableInsertColumnBase = class abstract(TdxPieceTableTableBaseCommand)
  private
    FForceVisible: Boolean;
    FPatternCell: TdxTableCell;
  protected
    procedure ExecuteCore; override;
    procedure InsertColumnToTheLeft(ACurrentRow: TdxTableRow; ACurrentCell: TdxTableCell); virtual;
    procedure ChangeStartParagraphIndexInCells(ACells: TdxTableCellList);
    procedure InsertColumnToTheRight(ACurrentRow: TdxTableRow; ACurrentCell: TdxTableCell); virtual;
    function InsertColumnCore(ARow: TdxTableRow; AInsertedIndex: Integer; AStart, AEnd: TdxParagraphIndex): TdxTableCell; virtual;
    function GetColumnIndex: Integer; virtual; abstract;
    function GetCurrentCell(AColumnIndex: Integer; ACurrentRow: TdxTableRow): TdxTableCell; virtual; abstract;
    procedure Modify(ACurrentRow: TdxTableRow; ACurrentCell: TdxTableCell); virtual; abstract;

    property ForceVisible: Boolean read FForceVisible;
  public
    constructor Create(APieceTable: TdxPieceTable; APatternCell: TdxTableCell; AForceVisible: Boolean); reintroduce;

    property PatternCell: TdxTableCell read FPatternCell;
  end;

  { TdxPieceTableInsertColumnToTheLeft }

  TdxPieceTableInsertColumnToTheLeft = class(TdxPieceTablePieceTableInsertColumnBase)
  protected
    function GetColumnIndex: Integer; override;
    function GetCurrentCell(AColumnIndex: Integer; ACurrentRow: TdxTableRow): TdxTableCell; override;
    procedure Modify(ACurrentRow: TdxTableRow; ACurrentCell: TdxTableCell); override;
  end;

  TdxPieceTableInsertColumnToTheRight = class(TdxPieceTablePieceTableInsertColumnBase)
  protected
    function GetColumnIndex: Integer; override;
    function GetCurrentCell(AColumnIndex: Integer; ACurrentRow: TdxTableRow): TdxTableCell; override;
    procedure Modify(ACurrentRow: TdxTableRow; ACurrentCell: TdxTableCell); override;
  end;

  { TdxPieceTableDeleteTableColumnsCommand }

  TdxPieceTableDeleteTableColumnsCommand = class(TdxPieceTableTableDocumentServerOwnerCommand)
  private
    FSelectedCells: TdxSelectedCellsCollection;
    FEndColumnIndex: Integer;
    FStartColumnIndex: Integer;
  protected
    procedure ExecuteCore; override;
    procedure DeleteCellsWithContent(ACells: TdxTableCellList);
    function IsSelectedEntireRow(ACells: TdxTableCellList): Boolean;
    procedure CalculateExecutionParameters; override;
    function GetStartColumnIndex(AFirstCell: TdxTableCell): Integer;
    function GetEndColumnIndex(ALastCell: TdxTableCell): Integer;
    procedure NormalizeCellVerticalMerging(ATable: TdxTable); virtual;
    function isAllCellsVerticalMergingContinue(ACells: TdxTableCellCollection): Boolean; virtual;

    property StartColumnIndex: Integer read FStartColumnIndex write FStartColumnIndex;
    property EndColumnIndex: Integer read FEndColumnIndex write FEndColumnIndex;
  public
    constructor Create(APieceTable: TdxPieceTable; ASelectedCells: TdxSelectedCellsCollection; AServer: IdxInnerRichEditDocumentServerOwner);
  end;

  { TdxPieceTableDeleteTableCellsWithShiftToTheUpCommand }

  TdxPieceTableDeleteTableCellsWithShiftToTheUpCommand = class(TdxPieceTableTableBaseCommand)
  private
    FSelectedCells: TdxSelectedCellsCollection;
  protected
    procedure ExecuteCore; override;
    procedure DeleteSelectedCells(ASelectedCells: TdxSelectedCellsIntervalInRow);
  public
    constructor Create(APieceTable: TdxPieceTable; ASelectedCells: TdxSelectedCellsCollection); reintroduce;
  end;

  { TdxPieceTableDeleteTableCellWithShiftToTheUpCoreCommand }

  TdxPieceTableDeleteTableCellWithShiftToTheUpCoreCommand = class(TdxPieceTableTableBaseCommand)
  private
    FCell: TdxTableCell;
  protected
    procedure ExecuteCore; override;
  public
    constructor Create(APieceTable: TdxPieceTable; ACell: TdxTableCell); reintroduce;
  end;

  { TdxPieceTableDeleteOneTableCellWithShiftToTheUpCommand }

  TdxPieceTableDeleteOneTableCellWithShiftToTheUpCommand = class(TdxPieceTableMergeTwoTableCellsVerticallyCommand)
  private
    FRunInfo: TdxRunInfo;
  protected
    procedure ExecuteCore; override;
    procedure FixParagraphsInPatternCell(ANeedDeleteFirstParagraphInCell,
      ANeedDeleteLastParagraphInCell: Boolean); override;
    procedure DeleteContentInCell; virtual;
    procedure UpdateProperties(ANextCell: TdxTableCell); override;
  public
    constructor Create(APieceTable: TdxPieceTable; ACell: TdxTableCell);
    destructor Destroy; override;
  end;

  { TdxPieceTableDeleteTableCellWithContentCommand }

  TdxPieceTableDeleteTableCellWithContentCommand = class(TdxPieceTableTableDocumentServerOwnerCommand)
  private
    FDeletedCell: TdxTableCell;
    FCanNormalizeCellVerticalMerging: Boolean;
  protected
    procedure ExecuteCore; override;
  public
    constructor Create(APieceTable: TdxPieceTable; ADeletedCell: TdxTableCell;
      AServer: IdxInnerRichEditDocumentServerOwner);

    property CanNormalizeCellVerticalMerging: Boolean read FCanNormalizeCellVerticalMerging
      write FCanNormalizeCellVerticalMerging;
  end;

  { TdxPieceTableInsertTableCellsBase }

  TdxPieceTableInsertTableCellsBase = class abstract(TdxPieceTableTableDocumentServerOwnerCommand)
  private
    FPatternCell: TdxTableCell;
    FCanCopyProperties: Boolean;
    FCanNormalizeTable: Boolean;
    FForceVisible: Boolean;
    FCanNormalizeVerticalMerging: Boolean;
  protected
    function Modify(ACell: TdxTableCell): TdxTableCell; virtual; abstract;
    procedure ExecuteCore; override;
    procedure NormalizeTableGridAfter(ATable: TdxTable); virtual;

    property CanNormalizeTable: Boolean read FCanNormalizeTable write FCanNormalizeTable;
    property CanCopyProperties: Boolean read FCanCopyProperties write FCanCopyProperties;
    property CanNormalizeVerticalMerging: Boolean read FCanNormalizeVerticalMerging
      write FCanNormalizeVerticalMerging;
    property ForceVisible: Boolean read FForceVisible;
  public
    constructor Create(APieceTable: TdxPieceTable; APatternCell: TdxTableCell;
      AForceVisible: Boolean; AServer: IdxInnerRichEditDocumentServerOwner);

		property PatternCell: TdxTableCell read FPatternCell;
  end;

  { TdxPieceTableInsertTableCellToTheLeft }

  TdxPieceTableInsertTableCellToTheLeft = class(TdxPieceTableInsertTableCellsBase)
  protected
    function Modify(ACell: TdxTableCell): TdxTableCell; override;
    procedure ChangeStartParagraphIndexInCells(ACells: TdxTableCellList);
  end;

  { TdxPieceTableInsertTableCellToTheRight }

  TdxPieceTableInsertTableCellToTheRight = class(TdxPieceTableInsertTableCellsBase)
  protected
    function Modify(ACell: TdxTableCell): TdxTableCell; override;
  end;

  { TdxPieceTableInsertTableCellWithShiftToTheDownCommand }

  TdxPieceTableInsertTableCellWithShiftToTheDownCommand = class(TdxPieceTableTableDocumentServerOwnerCommand)
  private
    FPatternCell: TdxTableCell;
    FForceVisible: Boolean;
  protected
    procedure ExecuteCore; override;
    procedure InsertTableCells(APatternCell: TdxTableCell; AInsertedCellsCount: Integer;
      AWidth: TdxPreferredWidth); virtual;
    procedure DeleteContentInPatternCell; virtual;
  public
    constructor Create(APieceTable: TdxPieceTable; APatternCell: TdxTableCell;
      AForceVisible: Boolean; AServer: IdxInnerRichEditDocumentServerOwner);
  end;

  { TdxPieceTableInsertTableCellWithShiftToTheDownCoreCommand }

  TdxPieceTableInsertTableCellWithShiftToTheDownCoreCommand = class(TdxPieceTableMergeTableCellsCommandBase)
  protected
    function CalculateNextCell: TdxTableCell; override;
    procedure UpdateProperties(ANextCell: TdxTableCell); override;
    procedure DeleteTableCellWithContent(ANextCell: TdxTableCell; ADeletingRange: TdxSelectionRange); override;
    procedure FixParagraphsInPatternCell(ANeedDeleteFirstParagraphInCell, ANeedDeleteLastParagraphInCell: Boolean); override;
  end;

  { TdxPieceTableSplitTableCellsHorizontally }

  TdxPieceTableSplitTableCellsHorizontally = class(TdxPieceTableTableDocumentServerOwnerCommand)
  private
    FPatternCell: TdxTableCell;
    FPartsCount: Integer;
    FForceVisible: Boolean;
  protected
    procedure ExecuteCore; override;
    procedure NormalizeTableCellsWidth(AOldWidth: Integer);
  public
    constructor Create(APieceTable: TdxPieceTable; APatternCell: TdxTableCell;
      APartsCount: Integer; AForceVisible: Boolean; AServer: IdxInnerRichEditDocumentServerOwner);
  end;

  { TdxPieceTableSplitTableCellsVertically }

  TdxPieceTableSplitTableCellsVertically = class(TdxPieceTableTableBaseCommand)
  private
    FPatternCell: TdxTableCell;
    FPartsCount: Integer;
    FColumnsCount: Integer;
    FForceVisible: Boolean;
  protected
    procedure ExecuteCore; override;
    procedure InsertRows(ARow: TdxTableRow); virtual;
    procedure SplitMergedCellsVertically(ACellsInRow: TdxTableCellCollection); virtual;
    function GetSelectedCellsEndIndex: Integer; virtual;
    procedure SplitMergedCellsVerticallyCore(ARestartCell: TdxTableCell); virtual;
  public
    constructor Create(APieceTable: TdxPieceTable; APatternCell: TdxTableCell;
      APartsCount, AColumnsCount: Integer; AForceVisible: Boolean); reintroduce;
  end;

  { TdxJoinTableWidthsCalculator }

  TdxJoinTableWidthsCalculator = class(TdxRtfTableWidthsCalculator)
  protected
    function GetMaxWidth(const AContentWidths: TdxWidthsContentInfo;
      AHorizontalMargins, ABordersWidth, ASpacing: TdxModelUnit): TdxLayoutUnit; override;
    function GetMinWidth(const AContentWidths: TdxWidthsContentInfo;
      AHorizontalMargins, ABordersWidth, ASpacing: TdxModelUnit): TdxLayoutUnit; override;
  end;

  { TdxPieceTableJoinSeveralTablesCommand }

  TdxPieceTableJoinSeveralTablesCommand = class(TdxPieceTableTableBaseCommand) 
  private

  protected
    procedure ExecuteCore; override;
    function CalculateWidthConsiderGrid(AGrid: TdxTableGrid;
      AStartIndex, AEndIndex: Integer): Integer; virtual;
  public
  end;

  { TdxPieceTableJoinTablesCommand }

  TdxPieceTableJoinTablesCommand = class(TdxPieceTableJoinSeveralTablesCommand)
  public
    constructor Create(APieceTable: TdxPieceTable; ATopTable, ABottomTable: TdxTable); reintroduce;
  end;

  { TdxNormalizeTableGridHelperBase }

  TdxNormalizeTableGridHelperBase = class abstract
  private
  protected
  public
  end;


  { TdxRichEditFieldHistoryItem }

  TdxRichEditFieldHistoryItem = class abstract(TdxRichEditHistoryItem)
  end;

  { TdxAddFieldHistoryItem }

  TdxAddFieldHistoryItem = class(TdxRichEditFieldHistoryItem)
  private
    FCodeStartRunIndex: TdxRunIndex;
    FResultEndRunIndex: TdxRunIndex;
    FCodeEndRunIndex: TdxRunIndex;
    FInsertedFieldIndex: Integer;
  protected
    procedure RedoCore; override;
    procedure UndoCore; override;
  public
    constructor Create(APieceTable: TdxPieceTable);

    property CodeStartRunIndex: TdxRunIndex read FCodeStartRunIndex write FCodeStartRunIndex;
    property CodeEndRunIndex: TdxRunIndex read FCodeEndRunIndex write FCodeEndRunIndex;
    property ResultEndRunIndex: TdxRunIndex read FResultEndRunIndex write FResultEndRunIndex;
    property InsertedFieldIndex: Integer read FInsertedFieldIndex;
  end;

  { TdxRichEditCompositeHistoryItem }

  TdxRichEditCompositeHistoryItem = class(TdxCompositeHistoryItem)
  private
    FSyntaxHighlightTransaction: TdxSyntaxHighlightHistoryItem;
  protected
    procedure UndoCore; override;
    procedure RedoCore; override;
  public
    constructor Create(ADocumentModel: TdxDocumentModel); reintroduce;

    property SyntaxHighlightTransaction: TdxSyntaxHighlightHistoryItem
      read FSyntaxHighlightTransaction write FSyntaxHighlightTransaction;
  end;

  { TdxSyntaxHighlightHistoryItem }

  TdxSyntaxHighlightHistoryItem = class(TdxRichEditCompositeHistoryItem)
  private
    FParent: TdxRichEditCompositeHistoryItem;
    FStartTransactionLevel: Integer;
  public
    constructor Create(ADocumentModel: TdxDocumentModel;
      AParent: TdxRichEditCompositeHistoryItem; AStartTransactionLevel: Integer);

    property Parent: TdxRichEditCompositeHistoryItem read FParent;
    property StartTransactionLevel: Integer read FStartTransactionLevel;
  end;

  { TdxTextRunAppendTextHistoryItem }

  TdxTextRunAppendTextHistoryItem = class(TdxTextRunBaseHistoryItem)
  private
    FTextLength: Integer;
    FLogPosition: TdxDocumentLogPosition;
    FHistoryItem: TdxTextRunInsertedHistoryItem;
  protected
    procedure UndoCore; override;
    procedure RedoCore; override;
  public
    property TextLength: Integer read FTextLength write FTextLength;
    property LogPosition: TdxDocumentLogPosition read FLogPosition write FLogPosition;
  end;

  { TdxRichEditDocumentHistory }

  TdxRichEditDocumentHistory = class(TdxDocumentHistory)
  private
    function GetDocumentModel: TdxDocumentModel;
  protected
    procedure OnEndUndoCore; override;
    function CommitAsSingleItemCore(ASingleItem: TdxHistoryItem): TdxHistoryItem; override;
    function CreateCompositeHistoryItem: TdxCompositeHistoryItem; override;
    procedure UndoCore; override;
    procedure RedoCore; override;
  public
    function Add(AItem: TdxHistoryItem): TdxHistoryItem; override;
    procedure AddRangeTextAppendedHistoryItem(AItem: TdxTextRunAppendTextHistoryItem); virtual;

    property DocumentModel: TdxDocumentModel read GetDocumentModel;
  end;

  { TdxParagraphListHistoryItemBase }

  TdxParagraphListHistoryItemBase = class abstract(TdxParagraphBaseHistoryItem)
  private
    FListLevelIndex: Integer;
    FNumberingListIndex: TdxNumberingListIndex;
  public
    property NumberingListIndex: TdxNumberingListIndex read FNumberingListIndex write FNumberingListIndex;
    property ListLevelIndex: Integer read FListLevelIndex write FListLevelIndex;
  end;

  { TdxAddParagraphToListHistoryItem }

  TdxAddParagraphToListHistoryItem = class(TdxParagraphListHistoryItemBase)
  private
    FOldOwnNumberingListIndex: TdxNumberingListIndex;
  protected
    procedure RedoCore; override;
    procedure UndoCore; override;
  public
    constructor Create(APieceTable: TdxPieceTable);

    property OldOwnNumberingListIndex: TdxNumberingListIndex read FOldOwnNumberingListIndex
      write FOldOwnNumberingListIndex;
  end;

  { TdxMergeParagraphsHistoryItem }

  TdxMergeParagraphsHistoryItem = class(TdxParagraphBaseHistoryItem)
  strict private
    FStartParagraph: TdxParagraph;
    FEndParagraph: TdxParagraph;
    FIndex: Integer;
    FEndParagraphIndex: TdxParagraphIndex;
    FParagraphStyleIndex: Integer;
    FUseFirstParagraphStyle: Boolean;
    FNotificationId: Integer;
  private
    function └StartParagraph: TdxParagraph;
  protected
    procedure RedoCore; override;
    procedure UndoCore; override;
  public
    property StartParagraph: TdxParagraph read └StartParagraph write FStartParagraph;
    property EndParagraph: TdxParagraph read FEndParagraph write FEndParagraph;
    property UseFirstParagraphStyle: Boolean read FUseFirstParagraphStyle write FUseFirstParagraphStyle;
  end;

  { TdxTextRunSplitHistoryItem }

  TdxTextRunSplitHistoryItem = class(TdxTextRunBaseHistoryItem)
  private
    FSplitOffset: Integer;
  protected
    procedure UndoCore; override;
    procedure RedoCore; override;
  public
    constructor Create(APieceTable: TdxPieceTable);

    property SplitOffset: Integer read FSplitOffset write FSplitOffset;
  end;

  { TdxNumberingListNotifier }

  TdxNumberingListNotifier = class
  public
    class procedure NotifyParagraphAdded(ADocumentModel: TdxDocumentModel; AIndex: TdxNumberingListIndex); static;
    class procedure NotifyParagraphRemoved(ADocumentModel: TdxDocumentModel; AIndex: TdxNumberingListIndex); static;
  end;

  { TdxRemoveParagraphFromListHistoryItem }

  TdxRemoveParagraphFromListHistoryItem = class(TdxParagraphListHistoryItemBase)
  protected
    procedure RedoCore; override;
    procedure UndoCore; override;
  end;

implementation

uses
  dxRichEdit.Utils.BatchUpdateHelper, RTLConsts, 
  Math, dxRichEdit.LayoutEngine.Formatter, dxRichEdit.DocumentModel.TextRange, dxRichEdit.Import.Rtf;

{ TdxDocumentModelCommand }

procedure TdxDocumentModelCommand.BeginExecute;
begin
  DocumentModel.BeginUpdate;
  FTransaction := TdxHistoryTransaction.Create(DocumentModel.History);
  CalculateExecutionParameters;
  CalculateApplyChangesParameters;
end;

constructor TdxDocumentModelCommand.Create(ADocumentModel: TdxDocumentModel);
begin
  inherited Create;
  Assert(ADocumentModel <> nil);
  FDocumentModel := ADocumentModel;
end;

procedure TdxDocumentModelCommand.EndExecute;
begin
  ApplyChanges;
  FreeAndNil(FTransaction);
  DocumentModel.EndUpdate;
end;

procedure TdxDocumentModelCommand.Execute;
begin
  BeginExecute;
  ExecuteCore;
  EndExecute;
end;

function TdxDocumentModelCommand.GetPieceTable: TdxPieceTable;
begin
  Result := FDocumentModel.MainPieceTable;
end;

{ TdxDocumentModelInsertObjectCommand }

procedure TdxDocumentModelInsertObjectCommand.ApplyChanges;
var
  AParagraph: TdxParagraph;
begin
  AParagraph := PieceTable.Paragraphs[FApplyChangesParagraphIndex];
  PieceTable.ApplyChanges(ChangeType, AParagraph.FirstRunIndex, AParagraph.LastRunIndex);
end;

procedure TdxDocumentModelInsertObjectCommand.CalculateApplyChangesParameters;
var
  ASectionIndex: TdxSectionIndex;
  ASection: TdxSection;
  AParagraph: TdxParagraph;
begin
  ASectionIndex := PieceTable.LookupSectionIndexByParagraphIndex(ParagraphIndex);
  ASection := DocumentModel.Sections[ASectionIndex];
  if (ASection.LastParagraphIndex = ParagraphIndex) and (ParagraphIndex > 0) then
    FApplyChangesParagraphIndex := ParagraphIndex - 1
  else
    FApplyChangesParagraphIndex := ParagraphIndex;
  AParagraph := PieceTable.Paragraphs[FApplyChangesParagraphIndex];
  PieceTable.ApplyChanges(ChangeType, AParagraph.FirstRunIndex, AParagraph.LastRunIndex + 1);
end;

procedure TdxDocumentModelInsertObjectCommand.CalculateExecutionParameters;
begin
  FParagraphIndex := CalculateInsertionParagraphIndex;
end;

{ TdxDocumentModelInsertObjectAtLogPositionCommand }

function TdxDocumentModelInsertObjectAtLogPositionCommand.CalculateInsertionParagraphIndex: TdxParagraphIndex;
begin
  Result := PieceTable.FindParagraphIndex(LogPosition);
end;

constructor TdxDocumentModelInsertObjectAtLogPositionCommand.Create(ADocumentModel: TdxDocumentModel;
  ALogPosition: TdxDocumentLogPosition; AForceVisible: Boolean);
begin
  inherited Create(ADocumentModel);
  if ALogPosition < 0 then
    raise Exception.Create('Error Message'); 
  FLogPosition := ALogPosition;
  FForceVisible := AForceVisible;
end;

{ TdxDocumentModelInsertObjectAtInputPositionCommand }

function TdxDocumentModelInsertObjectAtInputPositionCommand.CalculateInsertionParagraphIndex: TdxParagraphIndex;
begin
  Result := Position.ParagraphIndex;
end;

constructor TdxDocumentModelInsertObjectAtInputPositionCommand.Create(ADocumentModel: TdxDocumentModel;
  APosition: TdxInputPosition);
begin
  inherited Create(ADocumentModel);
  if APosition.LogPosition < 0 then
    raise Exception.Create('Error Message'); 
  FPosition := APosition;
end;

{ TdxPieceTableCommand }

procedure TdxPieceTableCommand.BeginExecute;
begin
  DocumentModel.BeginUpdate;
  FTransaction := TdxHistoryTransaction.Create(DocumentModel.History);
  CalculateExecutionParameters;
  CalculateApplyChangesParameters;
end;

constructor TdxPieceTableCommand.Create(APieceTable: TdxPieceTable);
begin
  inherited Create;
  Assert(APieceTable <> nil);
  FPieceTable := APieceTable;
end;

procedure TdxPieceTableCommand.EndExecute;
begin
  ApplyChanges;
  FreeAndNil(FTransaction);
  DocumentModel.EndUpdate;
end;

procedure TdxPieceTableCommand.Execute;
begin
  BeginExecute;
  try
    ExecuteCore;
  finally
    EndExecute;
  end;
end;

function TdxPieceTableCommand.GetDocumentModel: TdxDocumentModel;
begin
  Result := FPieceTable.DocumentModel;
end;

{ TdxPieceTableInsertObjectCommand }

procedure TdxPieceTableInsertObjectCommand.ApplyChanges;
var
  AFirstParagraph, ALastParagraph: TdxParagraph;
begin
  AFirstParagraph := PieceTable.Paragraphs[FApplyChangesParagraphIndex];
  ALastParagraph := PieceTable.Paragraphs[ParagraphIndex];
  PieceTable.ApplyChanges(ChangeType, AFirstParagraph.FirstRunIndex, ALastParagraph.LastRunIndex);
end;

procedure TdxPieceTableInsertObjectCommand.CalculateApplyChangesParameters;
var
  ASection: TdxSection;
  ASectionIndex: TdxSectionIndex;
  AFirstParagraph, ALastParagraph: TdxParagraph;
begin
  ASectionIndex := PieceTable.LookupSectionIndexByParagraphIndex(ParagraphIndex);
  if ASectionIndex > 0 then
  begin
    ASection := DocumentModel.Sections[ASectionIndex];
    if (ASection.LastParagraphIndex = ParagraphIndex) and (ParagraphIndex > 0) then
      FApplyChangesParagraphIndex := ParagraphIndex - 1
    else
      FApplyChangesParagraphIndex := ParagraphIndex;
  end
  else
    FApplyChangesParagraphIndex := ParagraphIndex;
  AFirstParagraph := PieceTable.Paragraphs[FApplyChangesParagraphIndex];
  ALastParagraph := PieceTable.Paragraphs[ParagraphIndex];
  PieceTable.ApplyChanges(ChangeType, AFirstParagraph.FirstRunIndex, ALastParagraph.LastRunIndex + 1);
end;

procedure TdxPieceTableInsertObjectCommand.CalculateExecutionParameters;
begin
  FParagraphIndex := CalculateInsertionParagraphIndex;
end;

{ TdxPieceTableInsertObjectAtLogPositionCommand }

function TdxPieceTableInsertObjectAtLogPositionCommand.CalculateInsertionParagraphIndex: TdxParagraphIndex;
begin
  Result := PieceTable.FindParagraphIndex(LogPosition);
end;

constructor TdxPieceTableInsertObjectAtLogPositionCommand.Create(APieceTable: TdxPieceTable;
  ALogPosition: TdxDocumentLogPosition; AForceVisible: Boolean);
begin
  inherited Create(APieceTable);
  if ALogPosition < 0 then
    raise Exception.Create('Error Message'); 
  FLogPosition := ALogPosition;
  FForceVisible := AForceVisible;
end;

{ TdxPieceTableInsertObjectAtInputPositionCommand }

function TdxPieceTableInsertObjectAtInputPositionCommand.CalculateInsertionParagraphIndex: TdxParagraphIndex;
begin
  Result := Position.ParagraphIndex;
end;

constructor TdxPieceTableInsertObjectAtInputPositionCommand.Create(APieceTable: TdxPieceTable;
  APosition: TdxInputPosition; AForceVisible: Boolean);
begin
  inherited Create(APieceTable);
  if APosition.LogPosition < 0 then
    raise Exception.Create('Error Message'); 
  FPosition := APosition;
  FForceVisible := AForceVisible;
end;

{ TdxPieceTableInsertTextAtLogPositionCommand }

constructor TdxPieceTableInsertTextAtLogPositionCommand.Create(APieceTable: TdxPieceTable;
  ALogPosition: TdxDocumentLogPosition; const AText: string; AForceVisible: Boolean);
begin
  inherited Create(APieceTable, ALogPosition, AForceVisible);
  if AText = '' then
    raise Exception.Create('Error Message'); 
  FText := AText;
end;

procedure TdxPieceTableInsertTextAtLogPositionCommand.ExecuteCore;
begin
  PieceTable.InsertTextCore(ParagraphIndex, LogPosition, FText, ForceVisible);
end;

function TdxPieceTableInsertTextAtLogPositionCommand.GetChangeType: TdxDocumentModelChangeType;
begin
  Result := TdxDocumentModelChangeType.InsertText;
end;

{ TdxPieceTableInsertPlainTextAtLogPositionCommand }

procedure TdxPieceTableInsertPlainTextAtLogPositionCommand.ExecuteCore;
begin
  InsertPlainTextCore(LogPosition, Text);
end;

function TdxPieceTableInsertPlainTextAtLogPositionCommand.GetNextLine(const ASource: string;
  var AIndex: Integer): string;
var
  AStrings: TStringBuilder;
  ACount: Integer;
begin
  AStrings := TStringBuilder.Create;
  try
    ACount := Length(ASource);
    while (AIndex <= ACount) and (ASource[AIndex] <> #10) and (ASource[AIndex] <> #13) do
    begin
      AStrings.Append(ASource[AIndex]);
      Inc(AIndex);
    end;
    Result := AStrings.ToString;
  finally
    AStrings.Free;
  end;
end;

procedure TdxPieceTableInsertPlainTextAtLogPositionCommand.InsertPlainTextCore(APosition: TdxDocumentLogPosition;
  const AText: string);
var
  ACount, AIndex: Integer;
  ARunFormattingSource: TdxTextRunBase;
  ALastInsertedRun: TdxTextRunBase;
  ALine: string;
begin
  ACount := Length(AText);
  AIndex := 1;
  ARunFormattingSource := nil;
  while AIndex <= ACount do
  begin
    ALine := GetNextLine(AText, AIndex);
    if ALine <> '' then
    begin
      PieceTable.InsertText(APosition, ALine, ForceVisible);
      ALastInsertedRun := PieceTable.LastInsertedRunInfo.Run;
      if ARunFormattingSource <> nil then
        ALastInsertedRun.ApplyFormatting(ARunFormattingSource.CharacterProperties.Info.Info,
          ARunFormattingSource.CharacterProperties.Info.Options, ARunFormattingSource.CharacterStyleIndex,
          ForceVisible)
      else
        ARunFormattingSource := ALastInsertedRun;
      APosition := APosition + Length(ALine);
    end;
    if ShouldAddParagraph(AText, AIndex) then
    begin
      PieceTable.InsertParagraph(APosition, ForceVisible);
      Inc(APosition);
    end;
  end;
end;

class function TdxPieceTableInsertPlainTextAtLogPositionCommand.ShouldAddParagraph(const ASource: string; var AIndex: Integer): Boolean;
var
  ACount: Integer;
begin
  ACount := Length(ASource);
  Result := AIndex <= ACount;
  if Result then
  begin
    if ASource[AIndex] = #13 then
      Inc(AIndex);
    if (AIndex <= ACount) and (ASource[AIndex] = #10) then
      Inc(AIndex);
  end;
end;

{ TdxPieceTableInsertTextAtInputPositionCommand }

constructor TdxPieceTableInsertTextAtInputPositionCommand.Create(APieceTable: TdxPieceTable;
  AInputPosition: TdxInputPosition; const AText: string; AForceVisible: Boolean);
begin
  inherited Create(APieceTable, AInputPosition, AForceVisible);
  if AText = '' then 
    raise Exception.Create('Error Message'); 
  FText := AText;
end;

procedure TdxPieceTableInsertTextAtInputPositionCommand.ExecuteCore;
begin
  PieceTable.InsertTextCore(Position, FText, ForceVisible);
end;

function TdxPieceTableInsertTextAtInputPositionCommand.GetChangeType: TdxDocumentModelChangeType;
begin
  Result := TdxDocumentModelChangeType.InsertText;
end;

{ TdxPieceTableInsertPlainTextAtInputPositionCommand }

constructor TdxPieceTableInsertPlainTextAtInputPositionCommand.Create(APieceTable: TdxPieceTable;
  AInputPosition: TdxInputPosition; const AText: string; AForceVisible: Boolean);
begin
  inherited Create(APieceTable, AInputPosition, AForceVisible);
  if AText = '' then
    raise Exception.Create('Error Message'); 
  FText := AText;
end;

procedure TdxPieceTableInsertPlainTextAtInputPositionCommand.ExecuteCore;
begin
  InsertPlainTextCore(Position, Text);
end;

function TdxPieceTableInsertPlainTextAtInputPositionCommand.GetChangeType: TdxDocumentModelChangeType;
begin
  Result := TdxDocumentModelChangeType.InsertText;
end;

procedure TdxPieceTableInsertPlainTextAtInputPositionCommand.InsertPlainTextCore(APosition: TdxInputPosition;
  const AText: string);
begin
  NotImplemented;
end;

{ TdxPieceTableInsertParagraphAtLogPositionCommand }

procedure TdxPieceTableInsertParagraphAtLogPositionCommand.ExecuteCore;
begin
  PieceTable.InsertParagraphCore(ParagraphIndex, LogPosition, ForceVisible);
  PieceTable.ApplyNumberingToInsertedParagraph(ParagraphIndex);
end;

function TdxPieceTableInsertParagraphAtLogPositionCommand.GetChangeType: TdxDocumentModelChangeType;
begin
  Result := TdxDocumentModelChangeType.InsertParagraph;
end;

{ TdxPieceTableInsertInlinePictureAtLogPositionCommand }

constructor TdxPieceTableInsertInlinePictureAtLogPositionCommand.Create(APieceTable: TdxPieceTable;
  ALogPosition: TdxDocumentLogPosition; AImage: TdxOfficeImageReference;
  AScaleX, AScaleY: Integer; AForceVisible: Boolean);
begin
  inherited Create(APieceTable, ALogPosition, AForceVisible);
  Assert(AImage <> nil);
  Assert(AScaleX > 0);
  Assert(AScaleY > 0);
  FImage := AImage.Clone(DocumentModel.ImageCache);
  FScaleX := AScaleX;
  FScaleY := AScaleY;
end;

destructor TdxPieceTableInsertInlinePictureAtLogPositionCommand.Destroy;
begin
  FreeAndNil(FImage);
  inherited Destroy;
end;

procedure TdxPieceTableInsertInlinePictureAtLogPositionCommand.ExecuteCore;
begin
  &Result := PieceTable.InsertInlineImageCore(ParagraphIndex, LogPosition, Image, ScaleX, ScaleY, ForceVisible);
end;

function TdxPieceTableInsertInlinePictureAtLogPositionCommand.GetChangeType: TdxDocumentModelChangeType;
begin
  Result := TdxDocumentModelChangeType.InsertInlinePicture;
end;

{ TdxPieceTableInsertCustomRunAtLogPositionCommand }

constructor TdxPieceTableInsertCustomRunAtLogPositionCommand.Create(APieceTable: TdxPieceTable;
  ALogPosition: TdxDocumentLogPosition; const ACustomRunObject: ICustomRunObject; AForceVisible: Boolean);
begin
  inherited Create(APieceTable, ALogPosition, AForceVisible);
  Assert(ACustomRunObject <> nil);
  FCustomRunObject := ACustomRunObject;
end;

procedure TdxPieceTableInsertCustomRunAtLogPositionCommand.ExecuteCore;
begin
  PieceTable.InsertCustomRunCore(ParagraphIndex, LogPosition, CustomRunObject, ForceVisible);
end;

function TdxPieceTableInsertCustomRunAtLogPositionCommand.GetChangeType: TdxDocumentModelChangeType;
begin
  Result := TdxDocumentModelChangeType.InsertInlineCustomObject;
end;

{ TdxPieceTableInsertInlineCustomObjectAtLogPositionCommand }

constructor TdxPieceTableInsertInlineCustomObjectAtLogPositionCommand.Create(APieceTable: TdxPieceTable;
  ALogPosition: TdxDocumentLogPosition; const ACustomObject: IInlineCustomObject; AScaleX, AScaleY: Integer;
  AForceVisible: Boolean);
begin
  inherited Create(APieceTable, ALogPosition, AForceVisible);
  Assert(ACustomObject <> nil);
  Assert(AScaleX > 0);
  Assert(AScaleY > 0);
  FCustomObject := ACustomObject;
  FScaleX := AScaleX;
  FScaleY := AScaleY;
end;

procedure TdxPieceTableInsertInlineCustomObjectAtLogPositionCommand.ExecuteCore;
begin
  PieceTable.InsertInlineCustomObjectCore(ParagraphIndex, LogPosition, CustomObject, ScaleX, ScaleY, ForceVisible);
end;

function TdxPieceTableInsertInlineCustomObjectAtLogPositionCommand.GetChangeType: TdxDocumentModelChangeType;
begin
  Result := TdxDocumentModelChangeType.InsertInlineCustomObject;
end;

{ TdxPieceTableInsertFloatingObjectAnchorAtLogPositionCommand }

procedure TdxPieceTableInsertFloatingObjectAnchorAtLogPositionCommand.ExecuteCore;
begin
  PieceTable.InsertFloatingObjectAnchorCore(ParagraphIndex, LogPosition, ForceVisible);
end;

function TdxPieceTableInsertFloatingObjectAnchorAtLogPositionCommand.GetChangeType: TdxDocumentModelChangeType;
begin
  Result := TdxDocumentModelChangeType.InsertFloatingObjectAnchor;
end;

{ TdxPieceTableInsertContentConvertedToDocumentModelCommand }

constructor TdxPieceTableInsertContentConvertedToDocumentModelCommand.Create(ATargetPieceTable: TdxPieceTable;
  ASourceModel: TdxDocumentModel; ALogPosition: TdxDocumentLogPosition; AInsertOptions: TdxInsertOptions;
  AForceVisible: Boolean);
begin
  inherited Create(ATargetPieceTable, ALogPosition, AForceVisible);
  Assert(ASourceModel <> nil);
  FSourceModel := ASourceModel;
  FInsertOptions := AInsertOptions;
end;

constructor TdxPieceTableInsertContentConvertedToDocumentModelCommand.Create(ATargetPieceTable: TdxPieceTable;
  ASourceModel: TdxDocumentModel; ALogPosition: TdxDocumentLogPosition; AForceVisible: Boolean);
begin
  Create(ATargetPieceTable, ASourceModel, ALogPosition, TdxInsertOptions.MatchDestinationFormatting, AForceVisible);
end;

procedure TdxPieceTableInsertContentConvertedToDocumentModelCommand.CopyDocumentModelContent(
  ACopyManager: TdxDocumentModelCopyManager; ALength: Integer);
var
  AOperation: TdxCopySectionOperation;
begin
  AOperation := SourceModel.CreateCopySectionOperation(ACopyManager);
  try
    AOperation.SuppressParentFieldsUpdate := SuppressParentFieldsUpdate;
    AOperation.SuppressFieldsUpdate := SuppressFieldsUpdate;
    AOperation.IsMergingTableCell := IsMergingTableCell;
    AOperation.SuppressCopySectionProperties := SuppressCopySectionProperties;
    if FPasteFromIE then
      AOperation.UpdateFieldOperationType := TdxUpdateFieldOperationType.PasteFromIE;
    AOperation.Execute(0, ALength - IfThen(FCopyLastParagraph, 0, 1), True);
  finally
    AOperation.Free;
  end;
end;

procedure TdxPieceTableInsertContentConvertedToDocumentModelCommand.ExecuteCore;
var
  ALength: Integer;
  ATargetPieceTable: TdxPieceTable;
  ACopyManager: TdxDocumentModelCopyManager;
  ARunIndex: TdxRunIndex;
  ARangeStartLogPosition: TdxDocumentLogPosition;
begin
  ALength := GetDocumentLength(SourceModel);
  SourceModel.Selection.Start := 0;
  if SourceModel.MailMergeOptions.KeepLastParagraph then
    FCopyLastParagraph := True;
  SourceModel.Selection.&End := ALength - IfThen(FCopyLastParagraph, 0, 1);
  ATargetPieceTable := PieceTable;
  ACopyManager := ATargetPieceTable.GetCopyManager(SourceModel.MainPieceTable, InsertOptions);
  try
    ACopyManager.IsInserted := True;
    ACopyManager.TargetPosition.LogPosition := LogPosition;
    ACopyManager.TargetPosition.ParagraphIndex := ATargetPieceTable.FindParagraphIndex(LogPosition);
    ARangeStartLogPosition := ATargetPieceTable.FindRunStartLogPosition(ATargetPieceTable.Paragraphs[ACopyManager.TargetPosition.ParagraphIndex], LogPosition, ARunIndex);
    if ARangeStartLogPosition <> LogPosition then
    begin
      ATargetPieceTable.SplitTextRun(ACopyManager.TargetPosition.ParagraphIndex, ARunIndex, LogPosition - ARangeStartLogPosition);
      ARangeStartLogPosition := LogPosition;
      Inc(ARunIndex);
    end;
    ACopyManager.TargetPosition.RunStartLogPosition := ARangeStartLogPosition;
    ACopyManager.TargetPosition.RunIndex := ARunIndex;
    ACopyManager.CopyAdditionalInfo(CopyBetweenInternalModels);
    CopyDocumentModelContent(ACopyManager, ALength);
  finally
    ACopyManager.Free;
  end;
end;

function TdxPieceTableInsertContentConvertedToDocumentModelCommand.GetChangeType: TdxDocumentModelChangeType;
begin
  Result := TdxDocumentModelChangeType.None;
end;

function TdxPieceTableInsertContentConvertedToDocumentModelCommand.GetDocumentLength(
  ADocumentModel: TdxDocumentModel): Integer;
var
  ALastParagraph: TdxParagraph;
begin
  ALastParagraph := ADocumentModel.MainPieceTable.Paragraphs.Last;
  Result := ALastParagraph.LogPosition + ALastParagraph.Length;
end;

{ TdxDocumentModelCopyCommand }

constructor TdxDocumentModelCopyCommand.Create(ASourcePieceTable: TdxPieceTable; ATargetModel: TdxDocumentModel;
  AOptions: TdxDocumentModelCopyOptions);
begin
  inherited Create(ASourcePieceTable);
  Assert(ATargetModel <> nil);
  Assert(AOptions <> nil);
  FTargetModel := ATargetModel;
  FOptions := AOptions;
  FTableCopyFromNestedLevel := -1;
end;

procedure TdxDocumentModelCopyCommand.ApplyChanges;
begin
end;

procedure TdxDocumentModelCopyCommand.CalculateApplyChangesParameters;
begin
end;

procedure TdxDocumentModelCopyCommand.CalculateExecutionParameters;
begin
end;

procedure TdxDocumentModelCopyCommand.CopyDocumentVariables(ATargetModel, ASourceModel: TdxDocumentModel);
begin
  NotImplemented;
end;

class procedure TdxDocumentModelCopyCommand.CopyStyles(ATargetModel, ASourceModel: TdxDocumentModel);
begin
  ATargetModel.BeginUpdate;
  CopyStylesCore(ATargetModel.CharacterStyles, ASourceModel.CharacterStyles);
  CopyStylesCore(ATargetModel.ParagraphStyles, ASourceModel.ParagraphStyles);
  CopyStylesCore(ATargetModel.NumberingListStyles, ASourceModel.NumberingListStyles);
  ATargetModel.EndUpdate;
end;

function TdxDocumentModelCopyCommand.CreateCopyBookmarkOperation(
  ACopyManager: TdxDocumentModelCopyManager): TdxCopyBookmarksOperation;
begin
  Result := NotImplemented;
end;

procedure TdxDocumentModelCopyCommand.ExecuteCore;
var
  ACopyManager: TdxDocumentModelCopyManager;
  AOperation: TdxCopySectionOperation;
  ASelectionRanges: TdxSelectionRangeCollection;
  ATargetBookmarksPositions: TList<TdxDocumentModelPosition>;
  I: Integer;
  ACurrentRange: TdxSelectionRange;
begin
  if FOptions.DefaultPropertiesCopyOptions = TdxDefaultPropertiesCopyOptions.Always then
    ReplaceDefaultProperties(TargetModel, DocumentModel);
  ReplaceDefaultStyles(TargetModel, DocumentModel);
  if FOptions.CopyDocumentVariables then
    CopyDocumentVariables(TargetModel, DocumentModel);
  TargetModel.DeleteDefaultNumberingList(TargetModel.NumberingLists);

  ACopyManager := TdxDocumentModelCopyManager.Create(PieceTable, TargetModel.MainPieceTable,
    FOptions.ParagraphNumerationCopyOptions, FOptions.FormattingCopyOptions);
  try
    AOperation := DocumentModel.CreateCopySectionOperation(ACopyManager);
    try
      AOperation.ShouldCopyBookmarks := False;
      AOperation.SuppressFieldsUpdate := SuppressFieldsUpdate;
      AOperation.AllowCopyWholeFieldResult := AllowCopyWholeFieldResult;
      ASelectionRanges := FOptions.SelectionRanges;
      ATargetBookmarksPositions := TList<TdxDocumentModelPosition>.Create;
      try
  			TargetModel.ActivePieceTable.SuppressTableIntegrityCheck := True;
			  for I := 0 to ASelectionRanges.Count -1 do
        begin
          ACurrentRange := ASelectionRanges[I];
          ATargetBookmarksPositions.Add(ACopyManager.TargetPosition.Clone);
          AOperation.Execute(ACurrentRange.From, ACurrentRange.Length, False);
        end;
        NormalizeTables(TargetModel.ActivePieceTable);
        TargetModel.ActivePieceTable.SuppressTableIntegrityCheck := False;
        for I := 0 to ASelectionRanges.Count - 1 do
        begin
        end;
        if FixLastParagraph then
				  TargetModel.MainPieceTable.FixLastParagraph;
      finally
        ATargetBookmarksPositions.Free;
      end;
    finally
      AOperation.Free;
    end;
  finally
    ACopyManager.Free;
  end;
end;

procedure TdxDocumentModelCopyCommand.NormalizeTables(APieceTable: TdxPieceTable);
var
  ATables: TdxTableCollection;
  ACurrentTable: TdxTable;
  ARow: TdxTableRow;
  I, AMaxCells, ARowIndex, ARowsCount, ACount, ANewCellsCount, ACells: Integer;
begin
  ATables := APieceTable.Tables;
  ACount := ATables.Count;
  APieceTable.DocumentModel.BeginUpdate;
  try
    for I := 0 to ACount - 1 do
    begin
      ACurrentTable := ATables[I];
      ACurrentTable.Normalize;
      ACurrentTable.NormalizeCellColumnSpans;
      ACurrentTable.NormalizeTableCellVerticalMerging;
      AMaxCells := ACurrentTable.FindTotalColumnsCountInTable;
      ARowsCount := ACurrentTable.Rows.Count;
      for ARowIndex := 0 to ARowsCount - 1 do
      begin
        ARow := ACurrentTable.Rows[ARowIndex];
        ACells := ACurrentTable.GetTotalCellsInRowConsiderGrid(ARow);
        if ACells < AMaxCells then
        begin
          ANewCellsCount := AMaxCells - ACells;
          ARow.LastCell.ColumnSpan := ARow.LastCell.ColumnSpan + ANewCellsCount;
        end;
      end;
    end;
  finally
    APieceTable.DocumentModel.EndUpdate;
  end;
end;

class procedure TdxDocumentModelCopyCommand.ReplaceDefaultProperties(ATargetModel, ASourceModel: TdxDocumentModel);
begin
  ATargetModel.DefaultCharacterProperties.Info.CopyFrom(ASourceModel.DefaultCharacterProperties.Info);
  ATargetModel.DefaultParagraphProperties.Info.CopyFrom(ASourceModel.DefaultParagraphProperties.Info);
end;

class procedure TdxDocumentModelCopyCommand.ReplaceDefaultStyles(ATargetModel, ASourceModel: TdxDocumentModel);
begin
  ReplaceStylesCore(ATargetModel.CharacterStyles, ASourceModel.CharacterStyles);
  ReplaceStylesCore(ATargetModel.ParagraphStyles, ASourceModel.ParagraphStyles);
  ReplaceStylesCore(ATargetModel.NumberingListStyles, ASourceModel.NumberingListStyles);
end;

class procedure TdxDocumentModelCopyCommand.ReplaceStylesCore(ATargetStyles, ASourceStyles: TdxStyleCollectionBase);
var
  I: Integer;
  ATargetStyle, ASourceStyle: TdxStyleBase;
begin
  for I := 0 to ATargetStyles.Count - 1 do
  begin
    ATargetStyle := ATargetStyles[I];
    ASourceStyle := ASourceStyles.GetStyleByName(ATargetStyle.StyleName);
    if ASourceStyle <> nil then
      ATargetStyle.CopyProperties(ASourceStyle);
  end;
end;

class procedure TdxDocumentModelCopyCommand.CopyStylesCore(ATargetStyles, ASourceStyles: TdxStyleCollectionBase);
var
  I: Integer;
  ATargetStyle, ASourceStyle: TdxStyleBase;
begin
  for I := 0 to ASourceStyles.Count - 1 do
  begin
    ASourceStyle := ASourceStyles[I];
    ATargetStyle := ATargetStyles.GetStyleByName(ASourceStyle.StyleName);
    if ATargetStyle <> nil then
      ATargetStyle.CopyProperties(ASourceStyle)
    else
      ASourceStyle.Copy(ATargetStyles.DocumentModel);
  end;
end;

{ TdxPieceTableCreateFieldCommand }

constructor TdxPieceTableCreateFieldCommand.Create(APieceTable: TdxPieceTable; ALogPosition: TdxDocumentLogPosition;
  ALength: Integer; AForceVisible: Boolean);
begin
  inherited Create(APieceTable, ALogPosition, AForceVisible);
  Assert(ALength >= 0);
  FLength := ALength;
end;

procedure TdxPieceTableCreateFieldCommand.ExecuteCore;
begin
  NotImplemented;
end;

function TdxPieceTableCreateFieldCommand.GetChangeType: TdxDocumentModelChangeType;
begin
  Result := TdxDocumentModelChangeType.InsertText;
end;

{ TdxPieceTableCreateFieldWithResultCommand }

function TdxPieceTableCreateFieldWithResultCommand.CalculateInsertionParagraphIndex: TdxParagraphIndex;
begin
  Result := PieceTable.FindParagraphIndex(FStartCode);
end;

constructor TdxPieceTableCreateFieldWithResultCommand.Create(APieceTable: TdxPieceTable; AStartCode,
  AEndCode: TdxDocumentLogPosition; AResultLength: Integer; AForceVisible: Boolean);
begin
  inherited Create(APieceTable);
  FStartCode := AStartCode;
  FEndCode := AEndCode;
  FResultLength := AResultLength;
  FForceVisible := AForceVisible;
end;

procedure TdxPieceTableCreateFieldWithResultCommand.ExecuteCore;
begin
  NotImplemented;
end;

function TdxPieceTableCreateFieldWithResultCommand.GetChangeType: TdxDocumentModelChangeType;
begin
  Result := TdxDocumentModelChangeType.InsertText;
end;

function TdxPieceTableCreateFieldWithResultCommand.InsertEndCodeRun(
  AEndCodePosition: TdxDocumentLogPosition): TdxRunIndex;
var
  AEndCodeParagraphIndex: TdxParagraphIndex;
begin
  AEndCodeParagraphIndex := PieceTable.FindParagraphIndex(AEndCodePosition);
  Result := PieceTable.InsertFieldCodeEndRunCore(AEndCodeParagraphIndex, AEndCodePosition, FForceVisible);
end;

function TdxPieceTableCreateFieldWithResultCommand.InsertEndResultRun(
  AEndResultPosition: TdxDocumentLogPosition): TdxRunIndex;
var
  AEndResultParagraphIndex: TdxParagraphIndex;
begin
  AEndResultParagraphIndex := PieceTable.FindParagraphIndex(AEndResultPosition);
  Result := PieceTable.InsertFieldResultEndRunCore(AEndResultParagraphIndex, AEndResultPosition, FForceVisible);
end;

function TdxPieceTableCreateFieldWithResultCommand.InsertStartCodeRun: TdxRunIndex;
var
  AStartCodeParagraphIndex: TdxParagraphIndex;
begin
  AStartCodeParagraphIndex := ParagraphIndex;
  Result := PieceTable.InsertFieldCodeStartRunCore(AStartCodeParagraphIndex, FStartCode, FForceVisible);
end;

{ TdxPieceTableInsertSeparatorAtLogPositionCommand }

procedure TdxPieceTableInsertSeparatorAtLogPositionCommand.ExecuteCore;
begin
  PieceTable.InsertSeparatorTextRunCore(ParagraphIndex, LogPosition);
end;

function TdxPieceTableInsertSeparatorAtLogPositionCommand.GetChangeType: TdxDocumentModelChangeType;
begin
  Result := TdxDocumentModelChangeType.InsertText;
end;

{ TdxPieceTableInsertDataContainerAtLogPositionCommand }

constructor TdxPieceTableInsertDataContainerAtLogPositionCommand.Create(APieceTable: TdxPieceTable;
  ALogPosition: TdxDocumentLogPosition; ADataContainer: IDataContainer; AForceVisible: Boolean);
begin
  inherited Create(APieceTable, ALogPosition, AForceVisible);
  Assert(ADataContainer <> nil);
  FDataContainer := ADataContainer;
end;

procedure TdxPieceTableInsertDataContainerAtLogPositionCommand.ExecuteCore;
begin
  PieceTable.InsertDataContainerRunCore(ParagraphIndex, LogPosition, DataContainer, ForceVisible);
end;

function TdxPieceTableInsertDataContainerAtLogPositionCommand.GetChangeType: TdxDocumentModelChangeType;
begin
  Result := TdxDocumentModelChangeType.InsertText;
end;

{ TdxPieceTableDeleteFieldWithoutResultCommand }

procedure TdxPieceTableDeleteFieldWithoutResultCommand.ApplyChanges;
begin
  PieceTable.ApplyChanges(TdxDocumentModelChangeType.DeleteContent, FRunIndex, MaxInt);
end;

procedure TdxPieceTableDeleteFieldWithoutResultCommand.CalculateApplyChangesParameters;
begin
  NotImplemented;
end;

procedure TdxPieceTableDeleteFieldWithoutResultCommand.CalculateExecutionParameters;
begin
end;

constructor TdxPieceTableDeleteFieldWithoutResultCommand.Create(APieceTable: TdxPieceTable; AField: TdxField);
begin
  inherited Create(APieceTable);
  Assert(AField <> nil);
  FField := AField;
end;

procedure TdxPieceTableDeleteFieldWithoutResultCommand.ExecuteCore;
begin
  NotImplemented;
end;

{ TdxPieceTableDeleteTextCommand }

procedure TdxPieceTableDeleteTextCommand.ApplyChanges;
var
  ASectionIndex: TdxSectionIndex;
  ARunIndex: TdxRunIndex;
begin
  if FSectionBreakDeleted then
  begin
    ASectionIndex := DocumentModel.FindSectionIndex(PieceTable.Paragraphs[FParagraphIndex].LogPosition);
    FParagraphIndex := DocumentModel.Sections[ASectionIndex].FirstParagraphIndex;
  end;
  FParagraphIndex := Min(FParagraphIndex, PieceTable.Paragraphs.Count - 1);
  ARunIndex := PieceTable.Paragraphs[FParagraphIndex].FirstRunIndex;
  PieceTable.ApplyChanges(TdxDocumentModelChangeType.DeleteContent, ARunIndex, MaxInt);
end;

procedure TdxPieceTableDeleteTextCommand.CalculateApplyChangesParameters;
begin
  FParagraphIndex := PieceTable.FindParagraphIndex(LogPosition);
end;

procedure TdxPieceTableDeleteTextCommand.CalculateExecutionParameters;
begin
end;

constructor TdxPieceTableDeleteTextCommand.Create(APieceTable: TdxPieceTable; ALogPosition: TdxDocumentLogPosition;
  ALength: Integer);
begin
  inherited Create(APieceTable);
  if ALogPosition < 0 then
    raise Exception.Create('Error Message'); 
  Assert(ALength >= 0);
  FLogPosition := ALogPosition;
  FLength := ALength;
end;

function TdxPieceTableDeleteTextCommand.CreateDeleteContentOperation: TdxDeleteContentOperation;
begin
  Result := PieceTable.CreateDeleteContentOperation;
end;

procedure TdxPieceTableDeleteTextCommand.ExecuteCore;
var
  AOperation: TdxDeleteContentOperation;
begin
  AOperation := CreateDeleteContentOperation;
  try
    AOperation.AllowPartiallyDeletingField := AllowPartiallyDeletingField;
    AOperation.LeaveFieldIfResultIsRemoved := LeaveFieldIfResultIsRemoved;
    AOperation.ForceRemoveInnerFields := ForceRemoveInnerFields;
    AOperation.BackspacePressed := BackspacePressed;
    FSectionBreakDeleted := AOperation.Execute(LogPosition, Length, DocumentLastParagraphSelected);
  finally
    AOperation.Free;
  end;
end;

{ TdxDocumentModelInsertSectionAtLogPositionCommand }

procedure TdxDocumentModelInsertSectionAtLogPositionCommand.ExecuteCore;
begin
  NotImplemented;
end;

function TdxDocumentModelInsertSectionAtLogPositionCommand.GetChangeType: TdxDocumentModelChangeType;
begin
  Result := TdxDocumentModelChangeType.InsertSection;
end;

{ TdxPieceTableTableBaseCommand }

procedure TdxPieceTableTableBaseCommand.ApplyChanges;
begin
end;

procedure TdxPieceTableTableBaseCommand.CalculateApplyChangesParameters;
begin
end;

procedure TdxPieceTableTableBaseCommand.CalculateExecutionParameters;
begin
end;

{ TdxPieceTableTableDocumentServerOwnerCommand }

constructor TdxPieceTableTableDocumentServerOwnerCommand.Create(APieceTable: TdxPieceTable;
  AServer: IdxInnerRichEditDocumentServerOwner);
begin
  inherited Create(APieceTable);
  Assert(AServer <> nil);
  FServer := AServer;
end;

{ TdxPieceTableCreateEmptyTableCommand }

constructor TdxPieceTableCreateEmptyTableCommand.Create(APieceTable: TdxPieceTable; ASourceCell: TdxTableCell);
begin
  inherited Create(APieceTable);
  FSourceCell := ASourceCell;
end;

procedure TdxPieceTableCreateEmptyTableCommand.Execute;
begin
  NotImplemented;
end;

procedure TdxPieceTableCreateEmptyTableCommand.ExecuteCore;
begin
  NotImplemented;
end;

{ TdxPieceTableCreateRowEmptyCommand }

constructor TdxPieceTableCreateRowEmptyCommand.Create(APieceTable: TdxPieceTable; ATable: TdxTable; AIndex: Integer);
begin
  inherited Create(APieceTable);
  Assert(ATable <> nil);
  Assert(AIndex >= 0);
  FTable := ATable;
  FIndex := AIndex;
end;

procedure TdxPieceTableCreateRowEmptyCommand.Execute;
begin
  NotImplemented;
end;

procedure TdxPieceTableCreateRowEmptyCommand.ExecuteCore;
begin
  NotImplemented;
end;

{ TdxPieceTableCreateCellEmptyCommand }

constructor TdxPieceTableCreateCellEmptyCommand.Create(APieceTable: TdxPieceTable; ARow: TdxTableRow;
  AInsertedIndex: Integer; AStart, AEnd: TdxParagraphIndex);
begin
  inherited Create(APieceTable);
  Assert(ARow <> nil);
  Assert(AStart >= 0);
  Assert(AEnd >= 0);
  FRow := ARow;
  FStartParagraphIndex := AStart;
  FEndParagraphIndex := AEnd;
  FInsertedIndex := AInsertedIndex;
end;

procedure TdxPieceTableCreateCellEmptyCommand.ExecuteCore;
begin
  NotImplemented;
end;

{ TdxPieceTableInsertTableRowCommand }

constructor TdxPieceTableInsertTableRowCommand.Create(APieceTable: TdxPieceTable; APatternRow: TdxTableRow;
  AForceVisible: Boolean);
begin
  inherited Create(APieceTable);
  Assert(APatternRow <> nil);
  FPatternRow := APatternRow;
  FForceVisible := AForceVisible;
end;

procedure TdxPieceTableInsertTableRowCommand.ExecuteCore;
begin
  NotImplemented;
end;

procedure TdxPieceTableInsertTableRowCommand.InsertParagraphs(ALogPosition: TdxDocumentLogPosition;
  ACells: TdxTableCellCollection);
begin
  NotImplemented;
end;

procedure TdxPieceTableInsertTableRowCommand.ApplyChanges;
begin
end;

procedure TdxPieceTableInsertTableRowCommand.CalculateApplyChangesParameters;
begin
end;

procedure TdxPieceTableInsertTableRowCommand.CopyCharacterAndParagraphFormattingFromPatternCell(ASource,
  ATarget: TdxTableCell);
begin
  NotImplemented;
end;

{ TdxPieceTableInsertTableRowBelowCommand }

procedure TdxPieceTableInsertTableRowBelowCommand.AfterParagraphsInserted;
begin
  NotImplemented;
end;

procedure TdxPieceTableInsertTableRowBelowCommand.CalculateExecutionParameters;
begin
  NotImplemented;
end;

procedure TdxPieceTableInsertTableRowBelowCommand.CopyPropertiesFromPatternCell(ASourceCell, ATargetCell: TdxTableCell);
begin
  NotImplemented;
end;

procedure TdxPieceTableInsertTableRowBelowCommand.CorrentVerticalMerging(ACreatedRow: TdxTableRow; I: Integer;
  ASourceCell, ATargetCell: TdxTableCell);
begin
  NotImplemented;
end;

function TdxPieceTableInsertTableRowBelowCommand.GetNextRow(ARow: TdxTableRow): TdxTableRow;
begin
  Result := NotImplemented;
end;

{ TdxPieceTableInsertTableRowAboveCommand }

procedure TdxPieceTableInsertTableRowAboveCommand.AfterParagraphsInserted;
begin
  NotImplemented;
end;

procedure TdxPieceTableInsertTableRowAboveCommand.CalculateExecutionParameters;
begin
  NotImplemented;
end;

procedure TdxPieceTableInsertTableRowAboveCommand.CopyPropertiesFromPatternCell(ASourceCell, ATargetCell: TdxTableCell);
begin
  NotImplemented;
end;

procedure TdxPieceTableInsertTableRowAboveCommand.CorrentVerticalMerging(ARow: TdxTableRow; I: Integer; ASourceCell,
  ATargetCell: TdxTableCell);
begin
  NotImplemented;
end;

function TdxPieceTableInsertTableRowAboveCommand.GetNextRow(ARow: TdxTableRow): TdxTableRow;
begin
  Result := NotImplemented;
end;

{ TdxPieceTableMergeTableCellsCommandBase }

function TdxPieceTableMergeTableCellsCommandBase.CalculateSelectionRange(ACell: TdxTableCell): TdxSelectionRange;
begin
  Result := NotImplemented;
end;

procedure TdxPieceTableMergeTableCellsCommandBase.CopyToCopyPieceTable(ACopyingRange: TdxSelectionRange);
begin
  NotImplemented;
end;

procedure TdxPieceTableMergeTableCellsCommandBase.CopyToPieceTable;
begin
  NotImplemented;
end;

constructor TdxPieceTableMergeTableCellsCommandBase.Create(APieceTable: TdxPieceTable; ACell: TdxTableCell);
begin
  inherited Create(APieceTable);
  Assert(ACell <> nil);
  FCell := ACell;
end;

procedure TdxPieceTableMergeTableCellsCommandBase.DeleteTableCellWithContent(ANextCell: TdxTableCell;
  ADeletingRange: TdxSelectionRange);
begin
  NotImplemented;
end;

procedure TdxPieceTableMergeTableCellsCommandBase.ExecuteCore;
begin
  NotImplemented;
end;

procedure TdxPieceTableMergeTableCellsCommandBase.FixParagraphsInPatternCell(ANeedDeleteFirstParagraphInCell,
  ANeedDeleteLastParagraphInCell: Boolean);
begin
  NotImplemented;
end;

function TdxPieceTableMergeTableCellsCommandBase.GetCopyPieceTable: TdxPieceTable;
begin
  Result := FCopyDocumentModel.ActivePieceTable;
end;

function TdxPieceTableMergeTableCellsCommandBase.IsEmptyCell(ACell: TdxTableCell): Boolean;
begin
  Result := Boolean(NotImplemented);
end;

{ TdxPieceTableMergeTwoTableCellsHorizontallyCommand }

function TdxPieceTableMergeTwoTableCellsHorizontallyCommand.CalculateNextCell: TdxTableCell;
begin
  Result := NotImplemented;
end;

procedure TdxPieceTableMergeTwoTableCellsHorizontallyCommand.DeleteTableCellWithContent(ANextCell: TdxTableCell;
  ADeletingRange: TdxSelectionRange);
begin
  NeedDeleteNextTableCell := True;
  inherited DeleteTableCellWithContent(ANextCell, ADeletingRange);
end;

procedure TdxPieceTableMergeTwoTableCellsHorizontallyCommand.UpdateProperties(ANextCell: TdxTableCell);
begin
  NotImplemented;
end;

{ TdxPieceTableMergeTwoTableCellsVerticallyCommand }

function TdxPieceTableMergeTwoTableCellsVerticallyCommand.CalculateNextCell: TdxTableCell;
begin
  Result := NotImplemented;
end;

procedure TdxPieceTableMergeTwoTableCellsVerticallyCommand.UpdateProperties(ANextCell: TdxTableCell);
begin
  NotImplemented;
end;

{ TdxPieceTableMergeTableCellsHorizontallyCommand }

function TdxPieceTableMergeTableCellsHorizontallyCommand.CalculateCellIndex(ARow: TdxTableRow; AStartCellIndex,
  AColumnSpan: Integer): Integer;
begin
  Result := Integer(NotImplemented);
end;

constructor TdxPieceTableMergeTableCellsHorizontallyCommand.Create(APieceTable: TdxPieceTable; ACell: TdxTableCell;
  ACount: Integer);
begin
  inherited Create(APieceTable);
  Assert(ACell <> nil);
  Assert(ACount > 0);
  FCell := ACell;
  FCount := ACount;
end;

procedure TdxPieceTableMergeTableCellsHorizontallyCommand.ExecuteCore;
begin
  NotImplemented;
end;

{ TdxPieceTableMergeTableCellsVerticallyCommand }

constructor TdxPieceTableMergeTableCellsVerticallyCommand.Create(APieceTable: TdxPieceTable; ACell: TdxTableCell;
  ACount: Integer);
begin
  inherited Create(APieceTable);
  Assert(ACell <> nil);
  Assert(ACount > 0);
  FCell := ACell;
  FCount := ACount;
end;

procedure TdxPieceTableMergeTableCellsVerticallyCommand.ExecuteCore;
begin
  NotImplemented;
end;

{ TdxPieceTableDeleteTableCellWithNestedTablesCommand }

constructor TdxPieceTableDeleteTableCellWithNestedTablesCommand.Create(APieceTable: TdxPieceTable; ATableIndex,
  ARowIndex, ACellIndex: Integer);
begin
  inherited Create(APieceTable);
  Assert(ATableIndex >= 0);
  Assert(ARowIndex >= 0);
  Assert(ACellIndex >= 0);
  FTableIndex := ATableIndex;
  FRowIndex := ARowIndex;
  FCellIndex := ACellIndex;
end;

procedure TdxPieceTableDeleteTableCellWithNestedTablesCommand.ExecuteCore;
begin
  NotImplemented;
end;

{ TdxPieceTableSplitTableCommand }

function TdxPieceTableSplitTableCommand.CalculateSelectionRange: TdxSelectionRange;
begin
  Result := NotImplemented;
end;

procedure TdxPieceTableSplitTableCommand.CopyToCopyPieceTable(ACopyingRange: TdxSelectionRange);
begin
  NotImplemented;
end;

procedure TdxPieceTableSplitTableCommand.CopyToPieceTable;
begin
  NotImplemented;
end;

constructor TdxPieceTableSplitTableCommand.Create(APieceTable: TdxPieceTable; ATableIndex, ARowIndex: Integer;
  AForceVisible: Boolean);
begin
  inherited Create(APieceTable);
  Assert(ATableIndex >= 0);
  Assert(ARowIndex >= 0);
  FTableIndex := ATableIndex;
  FRowIndex := ARowIndex;
  FForceVisible := AForceVisible;
end;

procedure TdxPieceTableSplitTableCommand.DeleteContent(ADeletingRange: TdxSelectionRange);
begin
  NotImplemented;
end;

procedure TdxPieceTableSplitTableCommand.ExecuteCore;
begin
  NotImplemented;
end;

function TdxPieceTableSplitTableCommand.GetCopyPieceTable: TdxPieceTable;
begin
  Result := FCopyDocumentModel.ActivePieceTable;
end;

function TdxPieceTableSplitTableCommand.GetParagraphs: TdxParagraphCollection;
begin
  Result := PieceTable.Paragraphs;
end;

function TdxPieceTableSplitTableCommand.GetTable: TdxTable;
begin
  Result := NotImplemented;
end;

procedure TdxPieceTableSplitTableCommand.InsertParagraphBeforeTable;
begin
  NotImplemented;
end;

procedure TdxPieceTableSplitTableCommand.InsertParagraphWithDefaultProperties(APosition: TdxDocumentLogPosition);
begin
  NotImplemented;
end;

{ TdxPieceTablePieceTableInsertColumnBase }

procedure TdxPieceTablePieceTableInsertColumnBase.ChangeStartParagraphIndexInCells(ACells: TdxTableCellList);
begin
  NotImplemented;
end;

constructor TdxPieceTablePieceTableInsertColumnBase.Create(APieceTable: TdxPieceTable; APatternCell: TdxTableCell;
  AForceVisible: Boolean);
begin
  inherited Create(APieceTable);
  FPatternCell := APatternCell;
  FForceVisible := AForceVisible;
end;

procedure TdxPieceTablePieceTableInsertColumnBase.ExecuteCore;
begin
  NotImplemented;
end;

function TdxPieceTablePieceTableInsertColumnBase.InsertColumnCore(ARow: TdxTableRow; AInsertedIndex: Integer; AStart,
  AEnd: TdxParagraphIndex): TdxTableCell;
begin
  Result := PieceTable.CreateTableCellCore(ARow, AInsertedIndex, AStart, AEnd);
end;

procedure TdxPieceTablePieceTableInsertColumnBase.InsertColumnToTheLeft(ACurrentRow: TdxTableRow;
  ACurrentCell: TdxTableCell);
begin
  NotImplemented;
end;

procedure TdxPieceTablePieceTableInsertColumnBase.InsertColumnToTheRight(ACurrentRow: TdxTableRow;
  ACurrentCell: TdxTableCell);
begin
  NotImplemented;
end;

{ TdxPieceTableInsertColumnToTheLeft }

function TdxPieceTableInsertColumnToTheLeft.GetColumnIndex: Integer;
begin
  Result := Integer(NotImplemented);
end;

function TdxPieceTableInsertColumnToTheLeft.GetCurrentCell(AColumnIndex: Integer;
  ACurrentRow: TdxTableRow): TdxTableCell;
begin
  Result := NotImplemented;
end;

procedure TdxPieceTableInsertColumnToTheLeft.Modify(ACurrentRow: TdxTableRow; ACurrentCell: TdxTableCell);
begin
  NotImplemented;
end;

{ TdxPieceTableInsertColumnToTheRight }

function TdxPieceTableInsertColumnToTheRight.GetColumnIndex: Integer;
begin
  Result := Integer(NotImplemented);
end;

function TdxPieceTableInsertColumnToTheRight.GetCurrentCell(AColumnIndex: Integer;
  ACurrentRow: TdxTableRow): TdxTableCell;
begin
  Result := NotImplemented;
end;

procedure TdxPieceTableInsertColumnToTheRight.Modify(ACurrentRow: TdxTableRow; ACurrentCell: TdxTableCell);
begin
  NotImplemented;
end;

{ TdxPieceTableDeleteTableColumnsCommand }

procedure TdxPieceTableDeleteTableColumnsCommand.CalculateExecutionParameters;
begin
  NotImplemented;
end;

constructor TdxPieceTableDeleteTableColumnsCommand.Create(APieceTable: TdxPieceTable;
  ASelectedCells: TdxSelectedCellsCollection; AServer: IdxInnerRichEditDocumentServerOwner);
begin
  inherited Create(APieceTable, AServer);
  Assert(ASelectedCells <> nil);
  FSelectedCells := ASelectedCells;
end;

procedure TdxPieceTableDeleteTableColumnsCommand.DeleteCellsWithContent(ACells: TdxTableCellList);
begin
  NotImplemented;
end;

procedure TdxPieceTableDeleteTableColumnsCommand.ExecuteCore;
begin
  NotImplemented;
end;

function TdxPieceTableDeleteTableColumnsCommand.GetEndColumnIndex(ALastCell: TdxTableCell): Integer;
begin
  Result := TdxTableCellVerticalBorderCalculator.GetStartColumnIndex(ALastCell, False) + ALastCell.ColumnSpan - 1;
end;

function TdxPieceTableDeleteTableColumnsCommand.GetStartColumnIndex(AFirstCell: TdxTableCell): Integer;
begin
  Result := TdxTableCellVerticalBorderCalculator.GetStartColumnIndex(AFirstCell, False);
end;

function TdxPieceTableDeleteTableColumnsCommand.isAllCellsVerticalMergingContinue(
  ACells: TdxTableCellCollection): Boolean;
begin
  Result := Boolean(NotImplemented);
end;

function TdxPieceTableDeleteTableColumnsCommand.IsSelectedEntireRow(ACells: TdxTableCellList): Boolean;
begin
  Result := Boolean(NotImplemented);
end;

procedure TdxPieceTableDeleteTableColumnsCommand.NormalizeCellVerticalMerging(ATable: TdxTable);
begin
  NotImplemented;
end;

{ TdxPieceTableDeleteTableCellsWithShiftToTheUpCommand }

constructor TdxPieceTableDeleteTableCellsWithShiftToTheUpCommand.Create(APieceTable: TdxPieceTable;
  ASelectedCells: TdxSelectedCellsCollection);
begin
  inherited Create(APieceTable);
  Assert(ASelectedCells <> nil);
  FSelectedCells := ASelectedCells;
end;

procedure TdxPieceTableDeleteTableCellsWithShiftToTheUpCommand.DeleteSelectedCells(
  ASelectedCells: TdxSelectedCellsIntervalInRow);
begin
  NotImplemented;
end;

procedure TdxPieceTableDeleteTableCellsWithShiftToTheUpCommand.ExecuteCore;
begin
  NotImplemented;
end;

{ TdxPieceTableDeleteTableCellWithShiftToTheUpCoreCommand }

constructor TdxPieceTableDeleteTableCellWithShiftToTheUpCoreCommand.Create(APieceTable: TdxPieceTable;
  ACell: TdxTableCell);
begin
  inherited Create(APieceTable);
  FCell := ACell;
end;

procedure TdxPieceTableDeleteTableCellWithShiftToTheUpCoreCommand.ExecuteCore;
begin
  NotImplemented;
end;

{ TdxPieceTableDeleteOneTableCellWithShiftToTheUpCommand }

constructor TdxPieceTableDeleteOneTableCellWithShiftToTheUpCommand.Create(APieceTable: TdxPieceTable;
  ACell: TdxTableCell);
begin
  inherited Create(APieceTable, ACell);
  FRunInfo := PieceTable.GetRunInfoByTableCell(PatternCell);
end;

destructor TdxPieceTableDeleteOneTableCellWithShiftToTheUpCommand.Destroy;
begin
  FreeAndNil(FRunInfo);
  inherited Destroy;
end;

procedure TdxPieceTableDeleteOneTableCellWithShiftToTheUpCommand.DeleteContentInCell;
var
  AStartLogPosition: TdxDocumentLogPosition;
begin
  AStartLogPosition := FRunInfo.Start.LogPosition;
  PieceTable.DeleteContent(AStartLogPosition, FRunInfo.&End.LogPosition - AStartLogPosition + 1, False);
end;

procedure TdxPieceTableDeleteOneTableCellWithShiftToTheUpCommand.ExecuteCore;
begin
  if PatternCell.Row.IsLastRowInTable then
    DeleteContentInCell
  else
    inherited ExecuteCore;
end;

procedure TdxPieceTableDeleteOneTableCellWithShiftToTheUpCommand.FixParagraphsInPatternCell(
  ANeedDeleteFirstParagraphInCell, ANeedDeleteLastParagraphInCell: Boolean);
begin
  DeleteContentInCell;
end;

procedure TdxPieceTableDeleteOneTableCellWithShiftToTheUpCommand.UpdateProperties(ANextCell: TdxTableCell);
begin
end;

{ TdxPieceTableDeleteTableCellWithContentCommand }

constructor TdxPieceTableDeleteTableCellWithContentCommand.Create(APieceTable: TdxPieceTable;
  ADeletedCell: TdxTableCell; AServer: IdxInnerRichEditDocumentServerOwner);
begin
  inherited Create(APieceTable, AServer);
  FDeletedCell := ADeletedCell;
  FCanNormalizeCellVerticalMerging := True;
end;

procedure TdxPieceTableDeleteTableCellWithContentCommand.ExecuteCore;
begin
  NotImplemented;
end;

{ TdxPieceTableInsertTableCellsBase }

constructor TdxPieceTableInsertTableCellsBase.Create(APieceTable: TdxPieceTable; APatternCell: TdxTableCell;
  AForceVisible: Boolean; AServer: IdxInnerRichEditDocumentServerOwner);
begin
  inherited Create(APieceTable, AServer);
  Assert(APatternCell <> nil);
  FPatternCell := APatternCell;
  FCanNormalizeTable := True;
  FCanCopyProperties := True;
  FCanNormalizeVerticalMerging := True;
  FForceVisible := AForceVisible;
end;

procedure TdxPieceTableInsertTableCellsBase.ExecuteCore;
begin
  NotImplemented;
end;

procedure TdxPieceTableInsertTableCellsBase.NormalizeTableGridAfter(ATable: TdxTable);
begin
  NotImplemented;
end;

{ TdxPieceTableInsertTableCellToTheLeft }

procedure TdxPieceTableInsertTableCellToTheLeft.ChangeStartParagraphIndexInCells(ACells: TdxTableCellList);
begin
  NotImplemented;
end;

function TdxPieceTableInsertTableCellToTheLeft.Modify(ACell: TdxTableCell): TdxTableCell;
begin
  Result := NotImplemented;
end;

{ TdxPieceTableInsertTableCellToTheRight }

function TdxPieceTableInsertTableCellToTheRight.Modify(ACell: TdxTableCell): TdxTableCell;
begin
  Result := NotImplemented;
end;

{ TdxPieceTableInsertTableCellWithShiftToTheDownCommand }

constructor TdxPieceTableInsertTableCellWithShiftToTheDownCommand.Create(APieceTable: TdxPieceTable;
  APatternCell: TdxTableCell; AForceVisible: Boolean; AServer: IdxInnerRichEditDocumentServerOwner);
begin
  inherited Create(APieceTable, AServer);
  Assert(APatternCell <> nil);
  FPatternCell := APatternCell;
  FForceVisible := FForceVisible;
end;

procedure TdxPieceTableInsertTableCellWithShiftToTheDownCommand.DeleteContentInPatternCell;
begin
  NotImplemented;
end;

procedure TdxPieceTableInsertTableCellWithShiftToTheDownCommand.ExecuteCore;
begin
  NotImplemented;
end;

procedure TdxPieceTableInsertTableCellWithShiftToTheDownCommand.InsertTableCells(APatternCell: TdxTableCell;
  AInsertedCellsCount: Integer; AWidth: TdxPreferredWidth);
begin
  NotImplemented;
end;

{ TdxPieceTableInsertTableCellWithShiftToTheDownCoreCommand }

function TdxPieceTableInsertTableCellWithShiftToTheDownCoreCommand.CalculateNextCell: TdxTableCell;
begin
  Result := PatternCell.Table.Rows[PatternCell.Row.IndexInTable - 1].Cells[PatternCell.IndexInRow];
end;

procedure TdxPieceTableInsertTableCellWithShiftToTheDownCoreCommand.DeleteTableCellWithContent(ANextCell: TdxTableCell;
  ADeletingRange: TdxSelectionRange);
var
  ASelectionRangeForPatternCell: TdxSelectionRange;
begin
  ASelectionRangeForPatternCell := CalculateSelectionRange(PatternCell);
  PieceTable.DeleteContent(ASelectionRangeForPatternCell.Start, ASelectionRangeForPatternCell.Length, False);
end;

procedure TdxPieceTableInsertTableCellWithShiftToTheDownCoreCommand.FixParagraphsInPatternCell(
  ANeedDeleteFirstParagraphInCell, ANeedDeleteLastParagraphInCell: Boolean);
begin
  inherited FixParagraphsInPatternCell(True, False);
end;

procedure TdxPieceTableInsertTableCellWithShiftToTheDownCoreCommand.UpdateProperties(ANextCell: TdxTableCell);
begin
  NotImplemented;
end;

{ TdxPieceTableSplitTableCellsHorizontally }

constructor TdxPieceTableSplitTableCellsHorizontally.Create(APieceTable: TdxPieceTable; APatternCell: TdxTableCell;
  APartsCount: Integer; AForceVisible: Boolean; AServer: IdxInnerRichEditDocumentServerOwner);
begin
  inherited Create(APieceTable, AServer);
  FPatternCell := APatternCell;
  FPartsCount := APartsCount;
  FForceVisible := AForceVisible;
end;

procedure TdxPieceTableSplitTableCellsHorizontally.ExecuteCore;
begin
  NotImplemented;
end;

procedure TdxPieceTableSplitTableCellsHorizontally.NormalizeTableCellsWidth(AOldWidth: Integer);
begin
  NotImplemented;
end;

{ TdxPieceTableSplitTableCellsVertically }

constructor TdxPieceTableSplitTableCellsVertically.Create(APieceTable: TdxPieceTable; APatternCell: TdxTableCell;
  APartsCount, AColumnsCount: Integer; AForceVisible: Boolean);
begin
  inherited Create(APieceTable);
  Assert(APatternCell <> nil);
  FPatternCell := APatternCell;
  FPartsCount := APartsCount;
  FColumnsCount := AColumnsCount;
  FForceVisible := AForceVisible;
end;

procedure TdxPieceTableSplitTableCellsVertically.ExecuteCore;
begin
  NotImplemented;
end;

function TdxPieceTableSplitTableCellsVertically.GetSelectedCellsEndIndex: Integer;
begin
  Result := Integer(NotImplemented);
end;

procedure TdxPieceTableSplitTableCellsVertically.InsertRows(ARow: TdxTableRow);
begin
  NotImplemented;
end;

procedure TdxPieceTableSplitTableCellsVertically.SplitMergedCellsVertically(ACellsInRow: TdxTableCellCollection);
begin
  NotImplemented;
end;

procedure TdxPieceTableSplitTableCellsVertically.SplitMergedCellsVerticallyCore(ARestartCell: TdxTableCell);
var
  AMergedCells: TList<TdxTableCell>;
  I, AColumnIndex, AMergedCellsCount, ATotalRowsCount: Integer;
begin
  AColumnIndex := TdxTableCellVerticalBorderCalculator.GetStartColumnIndex(ARestartCell, False);
  AMergedCells := TdxTableCellVerticalBorderCalculator.GetVerticalSpanCells(ARestartCell, AColumnIndex, False);
  AMergedCellsCount := AMergedCells.Count;
  if AMergedCellsCount = FPartsCount then
  begin
    for I := 0 to AMergedCellsCount - 1 do
      AMergedCells[I].VerticalMerging := TdxMergingState.None;
    Exit;
  end;
  ATotalRowsCount := AMergedCellsCount div FPartsCount;
  for I := 0 to AMergedCellsCount - 1 do
    if I mod ATotalRowsCount = 0 then
      AMergedCells[I].VerticalMerging := TdxMergingState.Restart;
end;

{ TdxJoinTableWidthsCalculator }

function TdxJoinTableWidthsCalculator.GetMaxWidth(const AContentWidths: TdxWidthsContentInfo; AHorizontalMargins,
  ABordersWidth, ASpacing: TdxModelUnit): TdxLayoutUnit;
begin
  Result := AContentWidths.MaxWidth;
end;

function TdxJoinTableWidthsCalculator.GetMinWidth(const AContentWidths: TdxWidthsContentInfo; AHorizontalMargins,
  ABordersWidth, ASpacing: TdxModelUnit): TdxLayoutUnit;
begin
  Result := AContentWidths.MinWidth;
end;

{ TdxPieceTableJoinSeveralTablesCommand }


function TdxPieceTableJoinSeveralTablesCommand.CalculateWidthConsiderGrid(AGrid: TdxTableGrid; AStartIndex,
  AEndIndex: Integer): Integer;
var
  I: Integer;
begin
  Result := 0;
  for I := AStartIndex to AEndIndex  - 1 do
    Inc(Result, AGrid.Columns[I].Width);
end;

procedure TdxPieceTableJoinSeveralTablesCommand.ExecuteCore;
begin
  NotImplemented;
end;


{ TdxPieceTableJoinTablesCommand }

constructor TdxPieceTableJoinTablesCommand.Create(APieceTable: TdxPieceTable; ATopTable, ABottomTable: TdxTable);
begin
  Assert(ATopTable <> nil);
  Assert(ABottomTable <> nil);
end;

{ TdxAddFieldHistoryItem }

constructor TdxAddFieldHistoryItem.Create(APieceTable: TdxPieceTable);
begin
  inherited Create(APieceTable);
  FInsertedFieldIndex := -1;
end;

procedure TdxAddFieldHistoryItem.RedoCore;
begin
  NotImplemented;
end;

procedure TdxAddFieldHistoryItem.UndoCore;
begin
  NotImplemented;
end;

{ TdxSyntaxHighlightHistoryItem }

constructor TdxSyntaxHighlightHistoryItem.Create(ADocumentModel: TdxDocumentModel;
  AParent: TdxRichEditCompositeHistoryItem; AStartTransactionLevel: Integer);
begin
  inherited Create(ADocumentModel);
  FParent := AParent;
  FStartTransactionLevel := AStartTransactionLevel;
end;

{ TdxRichEditCompositeHistoryItem }

constructor TdxRichEditCompositeHistoryItem.Create(ADocumentModel: TdxDocumentModel);
begin
  inherited Create(ADocumentModel.MainPieceTable);
end;

procedure TdxRichEditCompositeHistoryItem.RedoCore;
begin
  inherited RedoCore;
  if FSyntaxHighlightTransaction <> nil then
    FSyntaxHighlightTransaction.Redo;
end;

procedure TdxRichEditCompositeHistoryItem.UndoCore;
begin
  if FSyntaxHighlightTransaction <> nil then
    FSyntaxHighlightTransaction.Undo;
  inherited UndoCore;
end;

{ TdxRichEditDocumentHistory }

function TdxRichEditDocumentHistory.Add(AItem: TdxHistoryItem): TdxHistoryItem;
begin
  if TransactionLevel <> 0  then
  begin
    if Transaction is TdxRichEditCompositeHistoryItem then
      Assert(TdxRichEditCompositeHistoryItem(Transaction).SyntaxHighlightTransaction = nil);
  end;
  Result := inherited Add(AItem);
end;

procedure TdxRichEditDocumentHistory.AddRangeTextAppendedHistoryItem(AItem: TdxTextRunAppendTextHistoryItem);
begin
  if TransactionLevel <> 0 then
    Transaction.AddItem(AItem)
  else
    SetModifiedTextAppended(True);
end;

function TdxRichEditDocumentHistory.CommitAsSingleItemCore(ASingleItem: TdxHistoryItem): TdxHistoryItem;
var
  ATransaction: TdxRichEditCompositeHistoryItem;
begin
  if Transaction is TdxRichEditCompositeHistoryItem then
  begin
    ATransaction := TdxRichEditCompositeHistoryItem(Transaction);
    if ATransaction.SyntaxHighlightTransaction <> nil then
      Exit(nil);
  end;
  if ASingleItem is TdxTextRunAppendTextHistoryItem then
  begin
    Transaction.Items.Delete(0); 
    SetModifiedTextAppended(False);
    Result := Transaction;
  end
  else
    Result := inherited CommitAsSingleItemCore(ASingleItem);
end;

function TdxRichEditDocumentHistory.CreateCompositeHistoryItem: TdxCompositeHistoryItem;
begin
  Result := TdxRichEditCompositeHistoryItem.Create(DocumentModel);
end;

function TdxRichEditDocumentHistory.GetDocumentModel: TdxDocumentModel;
begin
  Result := TdxDocumentModel(inherited DocumentModel);
end;

procedure TdxRichEditDocumentHistory.OnEndUndoCore;
begin
  if not DocumentModel.ValidateActivePieceTable then
    DocumentModel.SetActivePieceTable(DocumentModel.MainPieceTable, nil);
end;

procedure TdxRichEditDocumentHistory.RedoCore;
begin
  DocumentModel.DeferredChanges.SuppressSyntaxHighlight := True;
  inherited RedoCore;
end;

procedure TdxRichEditDocumentHistory.UndoCore;
begin
  DocumentModel.DeferredChanges.SuppressSyntaxHighlight := True;
  inherited UndoCore;
end;

{ TdxTextRunAppendTextHistoryItem }

procedure TdxTextRunAppendTextHistoryItem.RedoCore;
var
  ALastInsertedRunInfo: TdxLastInsertedRunInfo;
begin
  ALastInsertedRunInfo := PieceTable.LastInsertedRunInfo;
  Assert(ALastInsertedRunInfo <> nil);
  Assert(ALastInsertedRunInfo.Run <> nil);
  Assert(ALastInsertedRunInfo.HistoryItem <> nil);
  Assert(ALastInsertedRunInfo.RunIndex = RunIndex);
  FHistoryItem := ALastInsertedRunInfo.HistoryItem;
  ALastInsertedRunInfo.LogPosition := LogPosition + TextLength;
  ALastInsertedRunInfo.Run.Length := ALastInsertedRunInfo.Run.Length + TextLength;
  ALastInsertedRunInfo.HistoryItem.NewLength := ALastInsertedRunInfo.HistoryItem.NewLength + TextLength;
  DocumentModel.History.RaiseOperationCompleted;
  TdxDocumentModelStructureChangedNotifier.NotifyRunMerged(PieceTable, PieceTable, ParagraphIndex, RunIndex, TextLength);
end;

procedure TdxTextRunAppendTextHistoryItem.UndoCore;
var
  ALastInsertedRunInfo: TdxLastInsertedRunInfo;
begin
  ALastInsertedRunInfo := PieceTable.LastInsertedRunInfo;
  ALastInsertedRunInfo.RunIndex := RunIndex;
  ALastInsertedRunInfo.Run := PieceTable.Runs[RunIndex] as TdxTextRun;
  ALastInsertedRunInfo.HistoryItem := FHistoryItem;
  ALastInsertedRunInfo.LogPosition := FLogPosition;
  ALastInsertedRunInfo.Run.Length := ALastInsertedRunInfo.Run.Length - TextLength; 
  ALastInsertedRunInfo.HistoryItem.NewLength := ALastInsertedRunInfo.HistoryItem.NewLength - TextLength; 
  DocumentModel.History.RaiseOperationCompleted;
  TdxDocumentModelStructureChangedNotifier.NotifyRunUnmerged(PieceTable, PieceTable, ParagraphIndex, RunIndex, -TextLength);
end;

{ TdxAddParagraphToListHistoryItem }

constructor TdxAddParagraphToListHistoryItem.Create(APieceTable: TdxPieceTable);
begin
  inherited Create(APieceTable);
  FOldOwnNumberingListIndex := NumberingListIndexListIndexNotSetted; 
end;

procedure TdxAddParagraphToListHistoryItem.RedoCore;
var
  AParagraph: TdxParagraph;
begin
  AParagraph := PieceTable.Paragraphs[ParagraphIndex];
  OldOwnNumberingListIndex := AParagraph.GetOwnNumberingListIndex;
  AParagraph.SetNumberingListIndex(NumberingListIndex);
  AParagraph.SetListLevelIndex(ListLevelIndex);
  TdxNumberingListNotifier.NotifyParagraphAdded(DocumentModel, NumberingListIndex);
  TdxNumberingListNotifier.NotifyParagraphRemoved(DocumentModel, OldOwnNumberingListIndex);
end;

procedure TdxAddParagraphToListHistoryItem.UndoCore;
var
  AParagraph: TdxParagraph;
begin
  AParagraph := PieceTable.Paragraphs[ParagraphIndex];
  AParagraph.ResetNumberingListIndex(OldOwnNumberingListIndex);
  AParagraph.ResetListLevelIndex;
  TdxNumberingListNotifier.NotifyParagraphRemoved(DocumentModel, NumberingListIndex);
  TdxNumberingListNotifier.NotifyParagraphAdded(DocumentModel, OldOwnNumberingListIndex);
end;

{ TdxNumberingListNotifier }

class procedure TdxNumberingListNotifier.NotifyParagraphAdded(ADocumentModel: TdxDocumentModel;
  AIndex: TdxNumberingListIndex);
begin
  if AIndex >= NumberingListIndexMinValue then
    ADocumentModel.NumberingLists[AIndex].OnParagraphAdded;
end;

class procedure TdxNumberingListNotifier.NotifyParagraphRemoved(ADocumentModel: TdxDocumentModel;
  AIndex: TdxNumberingListIndex);
begin
  if AIndex >= NumberingListIndexMinValue then
    ADocumentModel.NumberingLists[AIndex].OnParagraphRemoved;
end;

{ TdxTextRunSplitHistoryItem }

constructor TdxTextRunSplitHistoryItem.Create(APieceTable: TdxPieceTable);
begin
  inherited Create(APieceTable);
  FSplitOffset := -1; 
end;

procedure TdxTextRunSplitHistoryItem.RedoCore;
var
  ARuns: TdxTextRunCollection;
  ARun, ATailRun: TdxTextRun;
begin
  ARuns := PieceTable.Runs;
  ARun := ARuns[RunIndex] as TdxTextRun;
  ATailRun := ARun.CreateRun(ARun.Paragraph, ARun.StartIndex + SplitOffset, ARun.Length - SplitOffset);
  ARuns.Insert(ATailRun, RunIndex + 1);
  ARun.Length := SplitOffset;
  ATailRun.InheritStyleAndFormattingFromCore(ARun, False);
  TdxDocumentModelStructureChangedNotifier.NotifyRunSplit(PieceTable, PieceTable, ParagraphIndex, RunIndex, SplitOffset);
  PieceTable.ApplyChanges(TdxDocumentModelChangeType.SplitRun, RunIndex, RunIndex + 1);
  DocumentModel.ResetMerging;
end;

procedure TdxTextRunSplitHistoryItem.UndoCore;
var
  ARuns: TdxTextRunCollection;
  ARun: TdxTextRunBase;
  ATailRun: TdxTextRunBase;
begin
  ARuns := PieceTable.Runs;
  ARun := ARuns[RunIndex];
  ATailRun := ARuns.Extract(RunIndex + 1);
  ARun.Length := ARun.Length + ATailRun.Length;
  try
    TdxDocumentModelStructureChangedNotifier.NotifyRunJoined(PieceTable, PieceTable, ParagraphIndex, RunIndex, SplitOffset, ATailRun.Length);
    PieceTable.ApplyChanges(TdxDocumentModelChangeType.JoinRun, RunIndex, RunIndex);
    DocumentModel.ResetMerging;
  finally
    FreeAndNil(ATailRun);
  end;
end;

{ TdxRemoveParagraphFromListHistoryItem }

procedure TdxRemoveParagraphFromListHistoryItem.RedoCore;
var
  AParagraph: TdxParagraph;
  ANewValue: TdxNumberingListIndex;
begin
  AParagraph := PieceTable.Paragraphs[ParagraphIndex];
  NumberingListIndex := AParagraph.GetOwnNumberingListIndex;
  ListLevelIndex := AParagraph.GetOwnListLevelIndex;
  ANewValue := NumberingListIndexListIndexNotSetted;
  if not AParagraph.IsInNonStyleList then
    ANewValue := NumberingListIndexNoNumberingList;
  AParagraph.ResetNumberingListIndex(ANewValue);
  AParagraph.ResetListLevelIndex;
  TdxNumberingListNotifier.NotifyParagraphRemoved(DocumentModel, NumberingListIndex);
end;

procedure TdxRemoveParagraphFromListHistoryItem.UndoCore;
var
  AParagraph: TdxParagraph;
begin
  AParagraph := PieceTable.Paragraphs[ParagraphIndex];
  AParagraph.SetNumberingListIndex(NumberingListIndex);
  AParagraph.SetListLevelIndex(ListLevelIndex);
  TdxNumberingListNotifier.NotifyParagraphAdded(DocumentModel, NumberingListIndex);
end;

{ TdxMergeParagraphsHistoryItem }

procedure TdxMergeParagraphsHistoryItem.RedoCore;
var
  ARuns: TdxTextRunCollection;
  I: TdxRunIndex;
  ASectionIndex: TdxSectionIndex;
  AFirstRunIndex, ALastRunIndex: TdxRunIndex;
  ALogPosition: TdxDocumentLogPosition;
begin
  ARuns := PieceTable.Runs;
  FIndex := EndParagraph.LastRunIndex + 1 - EndParagraph.FirstRunIndex;
  for I := EndParagraph.FirstRunIndex to EndParagraph.LastRunIndex do
  begin
    ARuns[I].Paragraph := StartParagraph;
    StartParagraph.Length := StartParagraph.Length + ARuns[I].Length;
  end;
  if not UseFirstParagraphStyle then
  begin
    FParagraphStyleIndex := StartParagraph.ParagraphStyleIndex;
    StartParagraph.SetParagraphStyleIndexCore(EndParagraph.ParagraphStyleIndex);
    for I := StartParagraph.FirstRunIndex to StartParagraph.LastRunIndex do
      ARuns[I].ResetMergedCharacterFormattingIndex;
  end;
  StartParagraph.SetRelativeLastRunIndex(EndParagraph.LastRunIndex);
  ASectionIndex := PieceTable.LookupSectionIndexByParagraphIndex(EndParagraph.Index);
  if FNotificationId = TdxNotificationIdGenerator.EmptyId then
    FNotificationId := DocumentModel.History.GetNotificationId;
  TdxDocumentModelStructureChangedNotifier.NotifyParagraphMerged(PieceTable, PieceTable,
    ASectionIndex, EndParagraph.Index, EndParagraph.FirstRunIndex, FNotificationId);
  FEndParagraphIndex := EndParagraph.Index;
  AFirstRunIndex := EndParagraph.FirstRunIndex;
  ALastRunIndex := EndParagraph.LastRunIndex;
  ALogPosition := EndParagraph.LogPosition;
  PieceTable.Paragraphs.RemoveAt(FEndParagraphIndex);
  EndParagraph.AfterRemove(AFirstRunIndex, ALastRunIndex, ALogPosition);
end;

procedure TdxMergeParagraphsHistoryItem.UndoCore;
var
  ARuns: TdxTextRunCollection;
  I: TdxRunIndex;
  ASectionIndex: TdxSectionIndex;
begin
  ARuns := PieceTable.Runs;
  PieceTable.Paragraphs.Insert(FEndParagraphIndex, EndParagraph);
  EndParagraph.AfterUndoRemove;
  ASectionIndex := PieceTable.LookupSectionIndexByParagraphIndex(EndParagraph.Index - 1);
  TdxDocumentModelStructureChangedNotifier.NotifyParagraphInserted(PieceTable, PieceTable, ASectionIndex, EndParagraph.Index, EndParagraph.FirstRunIndex, Cell, true, EndParagraph.Index, FNotificationId);
  if not UseFirstParagraphStyle then
  begin
    EndParagraph.SetParagraphStyleIndexCore(StartParagraph.ParagraphStyleIndex);
    StartParagraph.SetParagraphStyleIndexCore(FParagraphStyleIndex);
    for I := StartParagraph.FirstRunIndex to StartParagraph.LastRunIndex do
      ARuns[I].ResetMergedCharacterFormattingIndex;
  end;
  I := StartParagraph.LastRunIndex;
  while I > StartParagraph.LastRunIndex - FIndex do
  begin
    ARuns[I].Paragraph := EndParagraph;
    StartParagraph.Length := StartParagraph.Length - ARuns[I].Length;
    Dec(I);
  end;
  StartParagraph.SetRelativeLastRunIndex(StartParagraph.LastRunIndex - FIndex);
end;

function TdxMergeParagraphsHistoryItem.└StartParagraph: TdxParagraph;
begin
  Result := FStartParagraph;
end;

{ TdxPieceTablePasteContentCommand }

constructor TdxPieceTablePasteContentCommand.Create(APieceTable: TdxPieceTable);
begin
  inherited Create(APieceTable);
  FPasteSource := TdxEmptyPasteSource.Create;
end;

{ TdxPieceTablePasteContentConvertedToDocumentModelCommandBase }

constructor TdxPieceTablePasteContentConvertedToDocumentModelCommandBase.Create(
  APieceTable: TdxPieceTable);
begin
  inherited Create(APieceTable);
  FInsertOptions := DocumentModel.CopyPasteOptions.InsertOptions;
end;

procedure TdxPieceTablePasteContentConvertedToDocumentModelCommandBase.ApplyChanges;
begin
end;

procedure TdxPieceTablePasteContentConvertedToDocumentModelCommandBase.CalculateApplyChangesParameters;
begin
end;

procedure TdxPieceTablePasteContentConvertedToDocumentModelCommandBase.CalculateExecutionParameters;
begin
end;

constructor TdxPieceTablePasteContentConvertedToDocumentModelCommandBase.Create(
  APieceTable: TdxPieceTable; AInsertOptions: TdxInsertOptions);
begin
  inherited Create(APieceTable);
  FInsertOptions := AInsertOptions;
end;

function TdxPieceTablePasteContentConvertedToDocumentModelCommandBase.CreatePasteDocumentModelCommand(
  APos: TdxDocumentLogPosition; ASource: TdxDocumentModel;
  ASuppressFieldsUpdate,
  APasteFromIE: Boolean): TdxPieceTableInsertContentConvertedToDocumentModelCommand;
begin
  Result := TdxPieceTableInsertContentConvertedToDocumentModelCommand.Create(PieceTable, ASource, APos,
    InsertOptions, False);
  Result.SuppressFieldsUpdate := ASuppressFieldsUpdate;
  Result.CopyBetweenInternalModels := CopyBetweenInternalModels;
  Result.PasteFromIE := APasteFromIE;
  Result.SuppressCopySectionProperties := SuppressCopySectionProperties(ASource);
end;

procedure TdxPieceTablePasteContentConvertedToDocumentModelCommandBase.ExecuteCore;
var
  ASizeCollection: string;
  ASource: TdxDocumentModel;
begin
  ASizeCollection := GetAdditionalContentString;
  ASource := CreateSourceDocumentModel(ASizeCollection);
  try
    PasteContent(ASource, DocumentModel.Selection.&End, ASizeCollection);
  finally
    ASource.Free;
  end;
end;

function TdxPieceTablePasteContentConvertedToDocumentModelCommandBase.GetAdditionalContentString: string;
begin
  Result := '';
end;

function TdxPieceTablePasteContentConvertedToDocumentModelCommandBase.GetDocumentModelSingleFloatingObjectAnchorRun(AModel: TdxDocumentModel): TdxFloatingObjectAnchorRun;
var
  APieceTable: TdxPieceTable;
begin
  APieceTable := AModel.MainPieceTable;
  if (APieceTable.Runs.Count = 2) and (APieceTable.Runs[0] is TdxFloatingObjectAnchorRun) then
    Result := TdxFloatingObjectAnchorRun(APieceTable.Runs[0])
  else
    Result := nil;
end;

procedure TdxPieceTablePasteContentConvertedToDocumentModelCommandBase.OffsetNewlyInsertedFloatingObjectIfNeed(
  ANewRun: TdxFloatingObjectAnchorRun; AParagraph: TdxParagraph);
var
  ALastRunIndex, I: TdxRunIndex;
  ARuns: TdxTextRunCollection;
  ARun: TdxFloatingObjectAnchorRun;
begin
  ALastRunIndex := AParagraph.LastRunIndex;
  ARuns := AParagraph.PieceTable.Runs;
  I := AParagraph.FirstRunIndex;
  while I < ALastRunIndex do
  begin
    if ARuns[I] is TdxFloatingObjectAnchorRun then
    begin
      ARun := TdxFloatingObjectAnchorRun(ARuns[I]);
      if ShouldOffsetNewRun(ANewRun, ARun) then
      begin
        OffsetNewlyInsertedFloatingObject(ANewRun, ARun);
        I := AParagraph.FirstRunIndex - 1; 
      end
      else
        Inc(I);
    end
    else
      Break;
  end;
end;

procedure TdxPieceTablePasteContentConvertedToDocumentModelCommandBase.OffsetNewlyInsertedFloatingObject(ANewRun: TdxFloatingObjectAnchorRun; ARun: TdxFloatingObjectAnchorRun);
var
  AProperties: TdxFloatingObjectProperties;
  AOffset: TPoint;
  AShift: Integer;
begin
  AProperties := ANewRun.FloatingObjectProperties;
  AOffset := AProperties.Offset;
  AShift := DocumentModel.UnitConverter.DocumentsToModelUnits(50);
  Inc(AOffset.X, AShift);
  Inc(AOffset.Y, AShift);
  AProperties.Offset := AOffset;
  AProperties.ZOrder := Math.Max(AProperties.ZOrder, ARun.FloatingObjectProperties.ZOrder + 1);
end;

procedure TdxPieceTablePasteContentConvertedToDocumentModelCommandBase.PasteContent(
  ASource: TdxDocumentModel; APos: TdxDocumentLogPosition;
  const ASizeCollection: string);
var
  AAnchorRunParagraph: TdxParagraph;
  AAnchorRun: TdxFloatingObjectAnchorRun;
begin
  if ASource = nil then
    Exit;

  if FForceInsertFloatingObjectAtParagraphStart then
  begin
    AAnchorRun := GetDocumentModelSingleFloatingObjectAnchorRun(ASource);
    if AAnchorRun <> nil then
    begin
      AAnchorRunParagraph := PreprocessSingleFloatingObjectInsertion(AAnchorRun);
      APos := AAnchorRunParagraph.LogPosition;
    end;
  end;
  ASource.PreprocessContentBeforeInsert(PieceTable, APos);
  PasteDocumentModel(APos, ASource, PasteFromIE);
end;

procedure TdxPieceTablePasteContentConvertedToDocumentModelCommandBase.PasteDocumentModel(APos: TdxDocumentLogPosition;
  ASource: TdxDocumentModel; APasteFromIE: Boolean; ASuppressFieldsUpdate: Boolean = False);
var
  ACommand: TdxPieceTableInsertContentConvertedToDocumentModelCommand;
begin
  ACommand := CreatePasteDocumentModelCommand(APos, ASource, ASuppressFieldsUpdate, APasteFromIE);
  try
    ACommand.Execute;
  finally
    ACommand.Free;
  end;
end;

function TdxPieceTablePasteContentConvertedToDocumentModelCommandBase.PreprocessSingleFloatingObjectInsertion(
  ARun: TdxFloatingObjectAnchorRun): TdxParagraph;
var
  AModelPosition: TdxDocumentModelPosition;
  APieceTable: TdxPieceTable;

begin
  AModelPosition := DocumentModel.Selection.Interval.&End;
  APieceTable := AModelPosition.PieceTable;
  Result := APieceTable.Paragraphs[AModelPosition.ParagraphIndex];
  OffsetNewlyInsertedFloatingObjectIfNeed(ARun, Result);
end;

procedure TdxPieceTablePasteContentConvertedToDocumentModelCommandBase.SetSuppressStoreImageSize(
  ADocumentModel: TdxDocumentModel; const ASizeCollection: string);
var
  ARuns: TdxTextRunCollection;
  I: Integer;
begin
  if ASizeCollection <> '' then
  begin
    ARuns := PieceTable.Runs;
    for I := 0 to ARuns.Count - 1 do
    begin
      NotImplemented;
    end;
  end;
end;

function TdxPieceTablePasteContentConvertedToDocumentModelCommandBase.ShouldOffsetNewRun(
  ANewRun, ARun: TdxFloatingObjectAnchorRun): Boolean;
var
  ANewFloatingObjectProperties: TdxFloatingObjectProperties;
  AFloatingObjectProperties: TdxFloatingObjectProperties;
begin
  ANewFloatingObjectProperties := ANewRun.FloatingObjectProperties;
  AFloatingObjectProperties := ARun.FloatingObjectProperties;
  Result := PointsEqual(ANewFloatingObjectProperties.Offset, AFloatingObjectProperties.Offset) and
    (ANewFloatingObjectProperties.HorizontalPositionType = AFloatingObjectProperties.HorizontalPositionType) and
    (ANewFloatingObjectProperties.VerticalPositionType = AFloatingObjectProperties.VerticalPositionType);
end;

function TdxPieceTablePasteContentConvertedToDocumentModelCommandBase.SuppressCopySectionProperties(
  ASource: TdxDocumentModel): Boolean;
var
  AParagraphs: TdxParagraphCollection;
  ALastParagraph: TdxParagraph;
  AShouldPreserveTableProperties: Boolean;
begin
  AParagraphs := ASource.MainPieceTable.Paragraphs;
  ALastParagraph := AParagraphs.Last;
  AShouldPreserveTableProperties := (AParagraphs.Count > 1) and (AParagraphs[AParagraphs.Count - 2].GetCell <> nil);
  Result := not ALastParagraph.IsEmpty or
    AShouldPreserveTableProperties or DocumentModel.CopyPasteOptions.MaintainDocumentSectionSettings;
end;

{ TdxPieceTablePasteRtfTextCommand }

function TdxPieceTablePasteRtfTextCommand.GetAdditionalContentString: string;
begin
  Result := PasteSource.GetDataAsText(TdxOfficeDataFormats.SuppressStoreImageSize);
end;

function TdxPieceTablePasteRtfTextCommand.GetContent: TdxClipboardStringContent;
var
  AContent: string;
begin
  AContent := PasteSource.GetDataAsText(TdxOfficeDataFormats.Rtf);
  Result := TdxClipboardStringContent.Create(AContent);
end;

function TdxPieceTablePasteRtfTextCommand.GetFormat: TdxDocumentFormat;
begin
  Result := TdxDocumentFormat.Rtf;
end;

function TdxPieceTablePasteRtfTextCommand.GetIsPasteFromIe: Boolean;
begin
  Result := PasteSource.ContainsData(TdxOfficeDataFormats.MsSourceUrl);
end;

function TdxPieceTablePasteRtfTextCommand.IsDataAvailable: Boolean;
begin
  Result := PasteSource.ContainsData(TdxOfficeDataFormats.Rtf);
end;

procedure TdxPieceTablePasteRtfTextCommand.PopulateDocumentModelFromContentStringCore(
  ADocumentModel: TdxDocumentModel; AContent: TdxClipboardStringContent;
  const ASizeCollection: string);
var
  AOptions: TdxRtfDocumentImporterOptions;
  AImporter: TdxRichEditDocumentModelRtfImporter;
  AStream: TStream;
begin
  ADocumentModel.DeleteDefaultNumberingList(ADocumentModel.NumberingLists);
  AOptions := TdxRtfDocumentImporterOptions.Create;
  try
    AOptions.SuppressLastParagraphDelete := True;
    AOptions.CopySingleCellAsText := ADocumentModel.BehaviorOptions.PasteSingleCellAsText;
    AOptions.LineBreakSubstitute := ADocumentModel.BehaviorOptions.PasteLineBreakSubstitution;
    AOptions.PasteFromIE := GetIsPasteFromIe;
    PasteFromIE := AOptions.PasteFromIE;
    AImporter := TdxRichEditDocumentModelRtfImporter.Create(ADocumentModel, AOptions);
    try
      AStream := PrepareInputStream(AContent.StringContent);
      try
        AImporter.Import(AStream);
      finally
        AStream.Free;
      end;
    finally
      AImporter.Free;
    end;
    SetSuppressStoreImageSize(ADocumentModel, ASizeCollection);
  finally
    AOptions.Free;
  end;
end;

function TdxPieceTablePasteRtfTextCommand.PrepareInputStream(
  const AStr: string): TStream;
begin
  Result := TStringStream.Create(AStr);
end;

{ TdxPieceTablePasteTextContentConvertedToDocumentModelCommandBase }

function TdxPieceTablePasteTextContentConvertedToDocumentModelCommandBase.CreateDocumentModelFromContentString(
  AContent: TdxClipboardStringContent;
  const ASizeCollection: string): TdxDocumentModel;
begin
  if (AContent = nil) or (AContent.StringContent = '') then
    Result := nil
  else
  begin
    Result := PieceTable.DocumentModel.CreateNew;
    Result.IntermediateModel := True;
    if not PieceTable.IsMain then
    begin
      Result.DocumentCapabilities.HeadersFooters := TdxDocumentCapability.Disabled;
      Result.DocumentCapabilities.Sections := TdxDocumentCapability.Disabled;
    end;
    PopulateDocumentModelFromContentStringCore(Result, AContent, ASizeCollection);
  end;
end;

function TdxPieceTablePasteTextContentConvertedToDocumentModelCommandBase.CreateSourceDocumentModel(
  const ASizeCollection: string): TdxDocumentModel;
var
  AContent: TdxClipboardStringContent;
begin
  AContent := GetContent;
  try
    Result := CreateDocumentModelFromContentString(AContent, ASizeCollection);
  finally
    AContent.Free;
  end;
end;

procedure TdxPieceTablePasteTextContentConvertedToDocumentModelCommandBase.PasteContent(
  AContent: TdxClipboardStringContent; APos: TdxDocumentLogPosition;
  const ASizeCollection: string);
var
  ASource: TdxDocumentModel;
begin
  ASource := CreateDocumentModelFromContentString(AContent, ASizeCollection);
  try
    PasteContent(ASource, APos, ASizeCollection);
  finally
    ASource.Free;
  end;
end;

end.
