{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.DocumentModel.History.Table;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  dxCore, dxRichEdit.DocumentModel.PieceTable, dxRichEdit.DocumentModel.Core, dxRichEdit.View.Core,
  dxRichEdit.DocumentModel.History.IndexChangedHistoryItem;

type


  { TdxCreateTableHistoryItem }

  TdxCreateTableHistoryItem = class(TdxRichEditHistoryItem)
  private
    FFirstParagraphIndex: TdxParagraphIndex;
    FRowCount: Integer;
    FCellCount: Integer;
    FTable: TdxTable;
  protected
    procedure RedoCore; override;
    procedure UndoCore; override;
  public
    constructor Create(APieceTable: TdxPieceTable; AFirstParagraphIndex: TdxParagraphIndex; ARowCount, ACellCount: Integer);
    procedure Execute; override;

    property FirstParagraphIndex: TdxParagraphIndex read FFirstParagraphIndex;
    property RowCount: Integer read FRowCount;
    property CellCount: Integer read FCellCount;
  end;


  { TdxTableCellPropertiesChangedHistoryItem }

  TdxTableCellPropertiesChangedHistoryItem = class(TdxIndexChangedHistoryItemCore<TdxDocumentModelChangeActions>)
  private
    FFirstCellParagraphIndex: TdxParagraphIndex;
    FNestedLevel: Integer;
    function GetPieceTable: TdxPieceTable;
  public
    constructor Create(APieceTable: TdxPieceTable; AFirstCellParagraphIndex: TdxParagraphIndex; ANestedLevel: Integer); reintroduce;
    function GetObject: IdxIndexBasedObject<TdxDocumentModelChangeActions>; override;

    property FirstCellParagraphIndex: TdxParagraphIndex read FFirstCellParagraphIndex;
    property PieceTable: TdxPieceTable read GetPieceTable;
  end;

implementation

uses
  Math, SysUtils, dxRichEdit.DocumentModel.Styles;


{ TdxCreateTableHistoryItem }

constructor TdxCreateTableHistoryItem.Create(APieceTable: TdxPieceTable;
  AFirstParagraphIndex: TdxParagraphIndex; ARowCount, ACellCount: Integer);
begin
  inherited Create(APieceTable);
  FFirstParagraphIndex := AFirstParagraphIndex;
  FRowCount := ARowCount;
  FCellCount := ACellCount;
end;

procedure TdxCreateTableHistoryItem.Execute;
begin
  FTable := PieceTable.TableCellsManager.CreateTableCore(FirstParagraphIndex, RowCount, CellCount);
end;

procedure TdxCreateTableHistoryItem.RedoCore;
begin
  PieceTable.TableCellsManager.InsertTable(FTable);
end;

procedure TdxCreateTableHistoryItem.UndoCore;
var
  ASelection: TdxSelection;
begin
  PieceTable.TableCellsManager.RemoveTable(FTable);
  ASelection := DocumentModel.Selection;
  ASelection.SetStartCell(ASelection.NormalizedStart);
end;


{ TdxTableCellPropertiesChangedHistoryItem }

constructor TdxTableCellPropertiesChangedHistoryItem.Create(APieceTable: TdxPieceTable;
  AFirstCellParagraphIndex: TdxParagraphIndex; ANestedLevel: Integer);
begin
  inherited Create(APieceTable);
  Assert(AFirstCellParagraphIndex >= 0); 
  FFirstCellParagraphIndex := AFirstCellParagraphIndex;
  FNestedLevel := ANestedLevel;
end;

function TdxTableCellPropertiesChangedHistoryItem.GetObject: IdxIndexBasedObject<TdxDocumentModelChangeActions>;
var
  ACell: TdxTableCell;
begin
  ACell := PieceTable.Paragraphs[FirstCellParagraphIndex].GetCell;
  while ACell.Table.NestedLevel <> FNestedLevel do
    ACell := ACell.Table.ParentCell;
  Result := ACell.Properties;
end;

function TdxTableCellPropertiesChangedHistoryItem.GetPieceTable: TdxPieceTable;
begin
Result := TdxPieceTable(DocumentModelPart);
end;

end.
