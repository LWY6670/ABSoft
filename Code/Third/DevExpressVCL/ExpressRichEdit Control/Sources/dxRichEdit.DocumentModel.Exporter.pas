{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.DocumentModel.Exporter;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Types, Classes, SysUtils, dxRichEdit.Utils.GenericsHelpers, Generics.Collections, Generics.Defaults,
  dxRichEdit.DocumentModel.PieceTable, dxRichEdit.DocumentModel.TextRange, dxCoreClasses, dxRichEdit.DocumentModel.ParagraphRange,
  dxRichEdit.DocumentModel.Numbering, dxRichEdit.DocumentModel.Core;

type
  TdxVisitableDocumentIntervalBoundaryCollection = class;
  TdxVisitableDocumentIntervalBoundary = class;
  TdxDocumentModelExporter = class;

  { TdxPieceTableNumberingListCountersManager }

  TdxPieceTableNumberingListCountersManager = class
  private
    FCalculators: TDictionary<TdxAbstractNumberingList, TdxNumberingListCountersCalculator>;
  public
    procedure BeginCalculateCounters;
    procedure EndCalculateCounters;
    function CalculateNextCounters(ACurrentParagraph: TdxParagraph): TIntegerDynArray;
  end;

  { TdxVisitableDocumentIntervalBoundaryIterator }

  TdxVisitableDocumentIntervalBoundaryIterator = class
  strict private
    type
      TComparer = class(TComparer<TdxVisitableDocumentIntervalBoundary>)
      public
        //IComparer
        function Compare(const Left, Right: TdxVisitableDocumentIntervalBoundary): Integer; override;
      end;
    class var
      FComparer: TComparer;
    class constructor Initialize;
    class destructor Finalize;
  private
    FCurrentIndex: Integer;
    FPieceTable: TdxPieceTable;
    FVisitableDocumentIntervalBoundaries: TdxVisitableDocumentIntervalBoundaryCollection;
    FIncludeHiddenIntervals: Boolean;
    function GetCurrent: TdxVisitableDocumentIntervalBoundary;
  protected
    procedure InitializeBoundaries;
    procedure PopulateBoundaries;
    procedure PopulateBoundariesCore; overload; virtual;
    procedure PopulateBoundariesCore<T: TdxVisitableDocumentInterval>(AIntervals: TList<T>); overload;
    property Boundaries: TdxVisitableDocumentIntervalBoundaryCollection  read FVisitableDocumentIntervalBoundaries;
  public
    constructor Create(APieceTable: TdxPieceTable; AIncludeHiddenIntervals: Boolean = True);
    destructor Destroy; override;

    function IsDone: Boolean;
    procedure MoveNext;
    procedure Reset; virtual;

    property Current: TdxVisitableDocumentIntervalBoundary read GetCurrent;
    property PieceTable: TdxPieceTable read FPieceTable;
  end;

  { TdxBookmarkBoundaryOrder }

  TdxBookmarkBoundaryOrder = (
    Start = 0,
    &End = 1
  );

  { TdxVisitableDocumentIntervalBoundary }

  TdxVisitableDocumentIntervalBoundary = class abstract
  private
    FInterval: TdxVisitableDocumentInterval;
    FIntervalIndex: Integer;
  protected
    function CreateBox: TdxVisitableDocumentIntervalBox; virtual; abstract;
    function GetOrder: TdxBookmarkBoundaryOrder; virtual; abstract;
    function GetPosition: TdxDocumentModelPosition; virtual; abstract;
  public
    constructor Create(AInterval: TdxVisitableDocumentInterval);

    procedure Export(AExporter: TdxDocumentModelExporter); virtual; abstract;

    property IntervalIndex: Integer read FIntervalIndex write FIntervalIndex;
    property Order: TdxBookmarkBoundaryOrder read GetOrder;
    property Position: TdxDocumentModelPosition read GetPosition;
    property VisitableInterval: TdxVisitableDocumentInterval read FInterval;
  end;

  { TdxVisitableDocumentIntervalBoundaryCollection }

  TdxVisitableDocumentIntervalBoundaryCollection = class(TList<TdxVisitableDocumentIntervalBoundary>);

  { TdxVisitableDocumentIntervalBasedObjectBoundaryIterator }

  TdxVisitableDocumentIntervalBasedObjectBoundaryIterator = class(TdxVisitableDocumentIntervalBoundaryIterator)
  protected
    procedure PopulateBoundariesCore; override;
  end;

  { TdxDocumentModelExporter }

  TdxExportPieceTableDelegate = procedure of object;

  TdxDocumentModelExporter = class abstract(TInterfacedObject, IdxDocumentModelExporter)
  strict private
    FCurrentSection: TdxSection;
    FFieldLevel: Integer;
    FDocumentModel: TdxDocumentModel;
    FExportedParagraphCount: Integer;
    FPieceTable: TdxPieceTable;
    FPieceTableNumberingListCounters: TdxPieceTableNumberingListCountersManager;
    FVisitableDocumentIntervalsIteratorStack: TStack<TdxVisitableDocumentIntervalBoundaryIterator>;
  private
    function GetVisitableDocumentIntervalsIterator: TdxVisitableDocumentIntervalBoundaryIterator;
    function GetPieceTableNumberingListCounters: TdxPieceTableNumberingListCountersManager;
  protected
    procedure ExportDocument; virtual;
    procedure ExportFloatingObjectAnchorRun(ARun: TdxFloatingObjectAnchorRun); virtual;
    procedure ExportSectionFiltered(ASection: TdxSection);
    procedure ExportTextRun(ARun: TdxTextRun); virtual;
    procedure ExportSection(ASection: TdxSection); virtual;
    procedure ExportSectionHeadersFooters(ASection: TdxSection);
    procedure ExportSectionHeadersFootersCore(ASection: TdxSection);
    procedure ExportParagraphs(AFrom, ATo: TdxParagraphIndex);
    function ExportParagraphFiltered(AParagraph: TdxParagraph): TdxParagraphIndex;
    function ExportParagraph(AParagraph: TdxParagraph): TdxParagraphIndex; virtual;
    procedure ExportParagraphRuns(AParagraph: TdxParagraph);
    procedure ExportRun(I: TdxRunIndex);
    procedure ExportParagraphRun(ARun: TdxParagraphRun);
    procedure ExportInlinePictureRun(ARun: TdxInlinePictureRun); virtual;

    function GetNumberingListText(AParagraph: TdxParagraph): string; virtual;

    function ShouldExportRun(ARun: TdxTextRunBase): Boolean;
    function ShouldExportHiddenText: Boolean; virtual;
    function ShouldExportSection(ASection: TdxSection): Boolean;
    function ShouldSplitRuns: Boolean;
    function ShouldCalculateFootNoteAndEndNoteNumbers: Boolean;
    function ShouldExportParagraph(AParagraph: TdxParagraph): Boolean;
    function ShouldUseCustomSaveTableMethod: Boolean; virtual;

    procedure TryToExportBookmarks(ARunIndex: TdxRunIndex; ARunOffset: Integer);

    procedure InvalidateParagraphsBoxes;
    function PrepareModelForExport(ADocumentModel: TdxDocumentModel): TdxDocumentModel;
    procedure PerformExportPieceTable(APieceTable: TdxPieceTable; APieceTableExporter: TdxExportPieceTableDelegate);
    procedure SplitRuns;
    procedure PopVisitableDocumentIntervalBoundaryIterator;
    procedure PushVisitableDocumentIntervalBoundaryIterator;

    // IdxDocumentModelExporter
    procedure Export(ARun: TdxTextRunBase); overload;

    property ExportedParagraphCount: Integer read FExportedParagraphCount write FExportedParagraphCount;
    property FieldLevel: Integer read FFieldLevel;
    property PieceTableNumberingListCounters: TdxPieceTableNumberingListCountersManager read GetPieceTableNumberingListCounters;
    property VisitableDocumentIntervalsIterator: TdxVisitableDocumentIntervalBoundaryIterator read GetVisitableDocumentIntervalsIterator;
  public
    constructor Create(ADocumentModel: TdxDocumentModel);
    destructor Destroy; override;

    procedure Export; overload; virtual;

    property DocumentModel: TdxDocumentModel read FDocumentModel;
    property PieceTable: TdxPieceTable read FPieceTable;
  end;

implementation

{ TdxDocumentModelExporter }

constructor TdxDocumentModelExporter.Create(ADocumentModel: TdxDocumentModel);
begin
  inherited Create;
  FDocumentModel := PrepareModelForExport(ADocumentModel);
  FPieceTable := FDocumentModel.MainPieceTable;
  FVisitableDocumentIntervalsIteratorStack := TStack<TdxVisitableDocumentIntervalBoundaryIterator>.Create;
end;

destructor TdxDocumentModelExporter.Destroy;
begin
  FreeAndNil(FPieceTableNumberingListCounters);
  FreeAndNil(FVisitableDocumentIntervalsIteratorStack);
  inherited Destroy;
end;

procedure TdxDocumentModelExporter.Export(ARun: TdxTextRunBase);
begin
  if not ShouldExportRun(ARun) then
    Exit;

  if ARun is TdxTextRun then
    ExportTextRun(TdxTextRun(ARun))
  else
  if ARun is TdxParagraphRun then
    ExportParagraphRun(TdxParagraphRun(ARun))
  else
  if ARun is TdxFloatingObjectAnchorRun then
    ExportFloatingObjectAnchorRun(TdxFloatingObjectAnchorRun(ARun))
  else
  if ARun is TdxInlinePictureRun then
    ExportInlinePictureRun(TdxInlinePictureRun(ARun))
end;

procedure TdxDocumentModelExporter.Export;
begin
  try
    PerformExportPieceTable(DocumentModel.MainPieceTable, ExportDocument);
  finally
  end;
end;

procedure TdxDocumentModelExporter.ExportDocument;
begin
  TdxListHelper.ForEach<TdxSection>(DocumentModel.Sections, ExportSectionFiltered);
  TryToExportBookmarks(PieceTable.Runs.Count - 1, 1);
end;

procedure TdxDocumentModelExporter.ExportFloatingObjectAnchorRun(ARun: TdxFloatingObjectAnchorRun);
begin
end;

function TdxDocumentModelExporter.ExportParagraph(
  AParagraph: TdxParagraph): TdxParagraphIndex;
begin
  ExportParagraphRuns(AParagraph);
  Result := AParagraph.Index;
end;

function TdxDocumentModelExporter.ExportParagraphFiltered(
  AParagraph: TdxParagraph): TdxParagraphIndex;
var
  AParagraphCell: TdxTableCell;
begin
  Inc(FExportedParagraphCount);
  if not ShouldExportParagraph(AParagraph) then
    Result := AParagraph.Index
  else
  begin
    AParagraphCell := AParagraph.GetCell;
    if ShouldUseCustomSaveTableMethod or (AParagraphCell = nil) or
        not DocumentModel.DocumentCapabilities.TablesAllowed then
      Result := ExportParagraph(AParagraph)
    else
      Result := Integer(NotImplemented);
  end;
end;

procedure TdxDocumentModelExporter.ExportParagraphRun(ARun: TdxParagraphRun);
begin
// do nothing
end;

procedure TdxDocumentModelExporter.ExportInlinePictureRun(ARun: TdxInlinePictureRun);
begin
// do nothing
end;

function TdxDocumentModelExporter.GetNumberingListText(AParagraph: TdxParagraph): string;
begin
  Result := AParagraph.GetNumberingListText(FPieceTableNumberingListCounters.CalculateNextCounters(AParagraph));
end;

procedure TdxDocumentModelExporter.ExportParagraphRuns(
  AParagraph: TdxParagraph);
var
  ALastRunIndex: TdxRunIndex;
  I: Integer;
begin
  ALastRunIndex := AParagraph.LastRunIndex;
  for I := AParagraph.FirstRunIndex to ALastRunIndex do
  begin
    TryToExportBookmarks(I, 0);
    ExportRun(I);
  end;
end;

procedure TdxDocumentModelExporter.ExportParagraphs(AFrom,
  ATo: TdxParagraphIndex);
var
  I: TdxParagraphIndex;
begin
  I := AFrom;
  while I <= ATo do
  begin
    I := ExportParagraphFiltered(PieceTable.Paragraphs[I]);
    Inc(I);
  end;
end;

procedure TdxDocumentModelExporter.ExportRun(I: TdxRunIndex);
begin
  PieceTable.Runs[I].Export(Self);
end;

procedure TdxDocumentModelExporter.ExportSection(ASection: TdxSection);
begin
  FCurrentSection := ASection;

  if ShouldCalculateFootNoteAndEndNoteNumbers then
  begin
  end;
  ExportSectionHeadersFooters(ASection);
  ExportParagraphs(ASection.FirstParagraphIndex, ASection.LastParagraphIndex);
end;

procedure TdxDocumentModelExporter.ExportSectionFiltered(ASection: TdxSection);
begin
  if ShouldExportSection(ASection) then
    ExportSection(ASection)
  else
  begin
    Assert(False);
  end;
end;

procedure TdxDocumentModelExporter.ExportSectionHeadersFooters(
  ASection: TdxSection);
begin
  ExportSectionHeadersFootersCore(ASection);
end;

procedure TdxDocumentModelExporter.ExportSectionHeadersFootersCore(
  ASection: TdxSection);
begin
  if not DocumentModel.DocumentCapabilities.HeadersFootersAllowed then
    Exit;
end;

procedure TdxDocumentModelExporter.ExportTextRun(ARun: TdxTextRun);
begin
// do noting
end;

function TdxDocumentModelExporter.GetPieceTableNumberingListCounters: TdxPieceTableNumberingListCountersManager;
begin
  if FPieceTableNumberingListCounters = nil then
    FPieceTableNumberingListCounters := TdxPieceTableNumberingListCountersManager.Create;
  Result := FPieceTableNumberingListCounters;
end;

function TdxDocumentModelExporter.GetVisitableDocumentIntervalsIterator: TdxVisitableDocumentIntervalBoundaryIterator;
begin
  if FVisitableDocumentIntervalsIteratorStack.Count > 0 then
    Result := FVisitableDocumentIntervalsIteratorStack.Peek
  else
    Result := nil;
end;

procedure TdxDocumentModelExporter.InvalidateParagraphsBoxes;
var
  AParagraph: TdxParagraph;
  I: Integer;
begin
  for I := 0 to FPieceTable.Paragraphs.Count - 1 do
  begin
    AParagraph := FPieceTable.Paragraphs[I];
    AParagraph.BoxCollection.InvalidateBoxes;
  end;
end;

procedure TdxDocumentModelExporter.PerformExportPieceTable(
  APieceTable: TdxPieceTable; APieceTableExporter: TdxExportPieceTableDelegate);
var
  AOriginalPieceTable: TdxPieceTable;
  AOriginalPieceTableNumberingListCounters: TdxPieceTableNumberingListCountersManager;
  ATransaction: TdxCompositeHistoryItem;
  ATransactionItemCountBeforeExecute: Integer;
  I: Integer;
begin
  AOriginalPieceTable := FPieceTable;
  AOriginalPieceTableNumberingListCounters := PieceTableNumberingListCounters;
  FPieceTable := APieceTable;
  FPieceTableNumberingListCounters := TdxPieceTableNumberingListCountersManager.Create;
  FPieceTableNumberingListCounters.BeginCalculateCounters;
  try
    PushVisitableDocumentIntervalBoundaryIterator;
    if not ShouldSplitRuns then
      APieceTableExporter
    else
    begin
      DocumentModel.BeginUpdate;
      try
        ATransaction := DocumentModel.History.Transaction;
        ATransactionItemCountBeforeExecute := ATransaction.Items.Count;
        SplitRuns;
        APieceTableExporter;
        //Undo transaction
        if not DocumentModel.ModelForExport then
        begin
          for I := ATransaction.Items.Count - 1 downto ATransactionItemCountBeforeExecute do
          begin
            ATransaction.Items[I].Undo;
            ATransaction.Items.Delete(I);
          end;
        end;
        InvalidateParagraphsBoxes;
      finally
        DocumentModel.EndUpdate;
      end;
    end;
  finally
    PopVisitableDocumentIntervalBoundaryIterator;
    FPieceTableNumberingListCounters.EndCalculateCounters;
    if AOriginalPieceTable = nil then
      FPieceTable := DocumentModel.MainPieceTable
    else
      FPieceTable := AOriginalPieceTable;

    FreeAndNil(FPieceTableNumberingListCounters);
    FPieceTableNumberingListCounters := AOriginalPieceTableNumberingListCounters;
  end;
end;

procedure TdxDocumentModelExporter.PopVisitableDocumentIntervalBoundaryIterator;
begin
  Assert(FVisitableDocumentIntervalsIteratorStack.Count > 0);
  FVisitableDocumentIntervalsIteratorStack.Pop.Free;
end;

function TdxDocumentModelExporter.PrepareModelForExport(
  ADocumentModel: TdxDocumentModel): TdxDocumentModel;
begin
  Result := ADocumentModel;
end;

procedure TdxDocumentModelExporter.PushVisitableDocumentIntervalBoundaryIterator;
begin
  FVisitableDocumentIntervalsIteratorStack.Push(TdxVisitableDocumentIntervalBasedObjectBoundaryIterator.Create(PieceTable));
end;

function TdxDocumentModelExporter.ShouldExportRun(
  ARun: TdxTextRunBase): Boolean;
begin
  Result := not (ARun is TdxInlinePictureRun) or not TdxInlinePictureRun(ARun).Image.SuppressStore;

  Result := Result and ShouldExportHiddenText;
  if not Result then
  begin
    Result := not ARun.Hidden;
  end;
end;

function TdxDocumentModelExporter.ShouldExportSection(
  ASection: TdxSection): Boolean;
begin
  Result := ShouldExportHiddenText or not ASection.IsHidden;
end;

function TdxDocumentModelExporter.ShouldSplitRuns: Boolean;
begin
  Result := VisitableDocumentIntervalsIterator.Boundaries.Count > 0;
end;

function TdxDocumentModelExporter.ShouldUseCustomSaveTableMethod: Boolean;
begin
  Result := False;
end;

procedure TdxDocumentModelExporter.SplitRuns;
begin
  NotImplemented;
end;

procedure TdxDocumentModelExporter.TryToExportBookmarks(ARunIndex: TdxRunIndex;
  ARunOffset: Integer);
var
  ABoundary: TdxVisitableDocumentIntervalBoundary;
begin
  if VisitableDocumentIntervalsIterator = nil then
    Exit;

  while not VisitableDocumentIntervalsIterator.IsDone do
  begin
    ABoundary := VisitableDocumentIntervalsIterator.Current;
    if (ABoundary.Position.RunIndex <> ARunIndex) or (ABoundary.Position.RunOffset <> ARunOffset) then
      Break;
    ABoundary.Export(Self);

    VisitableDocumentIntervalsIterator.MoveNext;
  end;
end;

function TdxDocumentModelExporter.ShouldCalculateFootNoteAndEndNoteNumbers: Boolean;
begin
  Result := False;
end;

function TdxDocumentModelExporter.ShouldExportHiddenText: Boolean;
begin
  Result := False;
end;

function TdxDocumentModelExporter.ShouldExportParagraph(
  AParagraph: TdxParagraph): Boolean;
begin
  Result := ShouldExportHiddenText or not AParagraph.IsHidden;
end;

{ TdxPieceTableNumberingListCountersManager }

procedure TdxPieceTableNumberingListCountersManager.BeginCalculateCounters;
begin
  FCalculators := TDictionary<TdxAbstractNumberingList, TdxNumberingListCountersCalculator>.Create;
end;

function TdxPieceTableNumberingListCountersManager.CalculateNextCounters(
  ACurrentParagraph: TdxParagraph): TIntegerDynArray;
var
  AbstractNumberingList: TdxAbstractNumberingList;
  ACalculator: TdxNumberingListCountersCalculator;
begin
  AbstractNumberingList := ACurrentParagraph.GetAbstractNumberingList;
  if AbstractNumberingList <> nil then
  begin
    if not FCalculators.TryGetValue(AbstractNumberingList, ACalculator) then
    begin
      ACalculator := TdxNumberingListCountersCalculator.Create(AbstractNumberingList);
      ACalculator.BeginCalculateCounters;
      FCalculators.Add(AbstractNumberingList, ACalculator);
    end;
    Result := ACalculator.CalculateNextCounters(ACurrentParagraph);
  end;
end;

procedure TdxPieceTableNumberingListCountersManager.EndCalculateCounters;
var
  AKey: TdxAbstractNumberingList;
begin
  for AKey in FCalculators.Keys do
    FCalculators[AKey].EndCalculateCounters;
  FreeAndNil(FCalculators);
end;

{ TdxVisitableDocumentIntervalBoundaryIterator.TComparer }

function TdxVisitableDocumentIntervalBoundaryIterator.TComparer.Compare(const Left,
  Right: TdxVisitableDocumentIntervalBoundary): Integer;
begin
  Result := Left.Position.LogPosition - Right.Position.LogPosition;
  if Result = 0 then
  begin
    Result := Left.IntervalIndex - Right.IntervalIndex;
    if Result = 0 then
      Result := Ord(Left.Order) - Ord(Right.Order);
  end;
end;

{ TdxVisitableDocumentIntervalBoundaryIterator }

constructor TdxVisitableDocumentIntervalBoundaryIterator.Create(
  APieceTable: TdxPieceTable; AIncludeHiddenIntervals: Boolean);
begin
  inherited Create;
  FPieceTable := APieceTable;
  FIncludeHiddenIntervals := AIncludeHiddenIntervals;
  FVisitableDocumentIntervalBoundaries := TdxVisitableDocumentIntervalBoundaryCollection.Create;
  InitializeBoundaries;
end;

destructor TdxVisitableDocumentIntervalBoundaryIterator.Destroy;
begin
  FreeAndNil(FVisitableDocumentIntervalBoundaries);
  inherited Destroy;
end;

class constructor TdxVisitableDocumentIntervalBoundaryIterator.Initialize;
begin
  FComparer := TComparer.Create;
end;

class destructor TdxVisitableDocumentIntervalBoundaryIterator.Finalize;
begin
  FComparer.Free;
end;

function TdxVisitableDocumentIntervalBoundaryIterator.GetCurrent: TdxVisitableDocumentIntervalBoundary;
begin
  Result := FVisitableDocumentIntervalBoundaries[FCurrentIndex];
end;

procedure TdxVisitableDocumentIntervalBoundaryIterator.InitializeBoundaries;
begin
  PopulateBoundaries;
  FVisitableDocumentIntervalBoundaries.Sort(FComparer);
end;

function TdxVisitableDocumentIntervalBoundaryIterator.IsDone: Boolean;
begin
  Result := FCurrentIndex >= FVisitableDocumentIntervalBoundaries.Count;
end;

procedure TdxVisitableDocumentIntervalBoundaryIterator.MoveNext;
begin
  Inc(FCurrentIndex);
end;

procedure TdxVisitableDocumentIntervalBoundaryIterator.Reset;
begin
  FCurrentIndex := 0;
end;

procedure TdxVisitableDocumentIntervalBoundaryIterator.PopulateBoundaries;
begin
  FVisitableDocumentIntervalBoundaries.Clear;
  PopulateBoundariesCore;
end;

procedure TdxVisitableDocumentIntervalBoundaryIterator.PopulateBoundariesCore;
begin
end;

procedure TdxVisitableDocumentIntervalBoundaryIterator.PopulateBoundariesCore<T>(
  AIntervals: TList<T>);
begin
  NotImplemented;
end;

{ TdxVisitableDocumentIntervalBoundary }

constructor TdxVisitableDocumentIntervalBoundary.Create(
  AInterval: TdxVisitableDocumentInterval);
begin
  inherited Create;
  FIntervalIndex := -1;
  FInterval := AInterval;
end;

{ TdxVisitableDocumentIntervalBasedObjectBoundaryIterator }

procedure TdxVisitableDocumentIntervalBasedObjectBoundaryIterator.PopulateBoundariesCore;
begin
  // use both, Bookmarks, RangePermissions and FloatingObjectAnchors
  inherited PopulateBoundariesCore;
end;

end.
