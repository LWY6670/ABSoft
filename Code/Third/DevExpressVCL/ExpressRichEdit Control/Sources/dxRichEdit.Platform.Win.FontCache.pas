{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Platform.Win.FontCache;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Windows, Graphics, Classes, cxClasses, dxRichEdit.Platform.Font, dxRichEdit.Platform.Win.Font,
  dxRichEdit.DocumentLayout.UnitConverter, dxRichEdit.Utils.UnicodeRangeInfo, Generics.Collections,
  SyncObjs;

type
  { TdxGdiFontCache }

  TdxGdiFontCache = class(TdxFontCache)
  private
    type
      TRegularFontLoader = class(TcxThread)
      private type
        TCallbackData = record
          Fonts: TStringList;
          Thread: TRegularFontLoader;
        end;
        PCallbackData = ^TCallbackData;
      private
        class function EnumFontInfoProc(var ALogFont: TLogFontW; ATextMetric: PTextMetricW;
          AFontType: Integer; AData: Pointer): Integer; stdcall; static;
        function CreateFontCharacterSet(ADC: HDC; const AFontName: string;
          AUnitConverter: TdxDocumentLayoutUnitConverter): TdxFontCharacterSet;
        function CanContinue: Boolean;
      protected
        procedure Execute; override;
      end;
  private
    class var
      FLoader: TRegularFontLoader;
      FNameToCharacterMapPopulatedEvent: TSimpleEvent;
      FSystemRegularFonts: TStringList;
      FUnicodeRangeInfo: TdxUnicodeRangeInfo;
    class constructor Initialize;
    class destructor Finalize;
    function GetMeasurer: TdxGdiFontInfoMeasurer; inline;
  protected
    function CreateOverrideFontSubstitutes: TDictionary<string, string>; virtual;
    function CreateFontInfoMeasurer(AUnitConverter: TdxDocumentLayoutUnitConverter): TdxFontInfoMeasurer; override;
    function CreateFontInfoCore(const AFontName: string; ADoubleFontSize: Integer; const AFontStyle: TFontStyles): TdxFontInfo; override;
    function GetFontCharacterRanges(AFontInfo: TdxFontInfo): TdxFontCharacterRangeArray; override;

    class property NameToCharacterMapPopulatedEvent: TSimpleEvent read FNameToCharacterMapPopulatedEvent write FNameToCharacterMapPopulatedEvent;
    class property SystemRegularFonts: TStringList read FSystemRegularFonts;
    class property UnicodeRangeInfo: TdxUnicodeRangeInfo read FUnicodeRangeInfo;
  public
    function CreateFontCharacterSet(const AFontName: string): TdxFontCharacterSet;
    function GetFontCharacterSet(const AFontName: string): TdxFontCharacterSet; override;
    function FindSubstituteFont(const ASourceFontName: string; ACharacter: Char; var AFontCharacterSet: TdxFontCharacterSet): string; override;
    function ShouldUseDefaultFontToDrawInvisibleCharacter(AFontInfo: TdxFontInfo; ACharacter: Char): Boolean; override;

    property Measurer: TdxGdiFontInfoMeasurer read GetMeasurer;
    class procedure PopulateNameToCharacterMap;
  end;

  { TdxGdiFontCacheManager }

  TdxGdiFontCacheManager = class(TdxFontCacheManager)
  public
    function CreateFontCache: TdxFontCache; override;
  end;

implementation

uses
  SysUtils, dxCore, Forms, Character, ComObj, ActiveX, dxRichEdit.DocumentModel.Core;

const
  CLASS_CMultiLanguage: TGUID = '{275C23E2-3747-11D0-9FEA-00AA003F8646}';

type
  // *********************************************************************//
  // Interface: IMLangCodePages
  // Flags:     (0)
  // GUID:      {359F3443-BD4A-11D0-B188-00AA0038C969}
  // *********************************************************************//
  IMLangCodePages = interface(IUnknown)
    ['{359F3443-BD4A-11D0-B188-00AA0038C969}']
    function GetCharCodePages(const chSrc: WideChar; out pdwCodePages: DWORD): HResult; stdcall;
    function GetStrCodePages(const pszSrc: PWideChar; const cchSrc: ULONG; dwPriorityCodePages: DWORD; out pdwCodePages: DWORD; out pcchCodePages: ULONG): HResult; stdcall;
    function CodePageToCodePages(const uCodePage: SYSUINT; out pdwCodePages: LongWord): HResult; stdcall;
    function CodePagesToCodePage(const dwCodePages: LongWord; const uDefaultCodePage: SYSUINT; out puCodePage: SYSUINT): HResult; stdcall;
  end;

  // *********************************************************************//
  // Interface: IMLangFontLink
  // Flags:     (0)
  // GUID:      {359F3441-BD4A-11D0-B188-00AA0038C969}
  // *********************************************************************//
  IMLangFontLink = interface(IMLangCodePages)
    ['{359F3441-BD4A-11D0-B188-00AA0038C969}']
    function GetFontCodePages(const hDC: THandle; const hFont: THandle; out pdwCodePages: LongWord): HResult; stdcall;
    function MapFont(const hDC: THandle; const dwCodePages: LongWord; hSrcFont: THandle; out phDestFont: THandle): HResult; stdcall;
    function ReleaseFont(const hFont: THandle): HResult; stdcall;
    function ResetFontMapping: HResult; stdcall;
  end;

var
  FontLink: IMLangFontLink;

{ TdxGdiFontCache }

class constructor TdxGdiFontCache.Initialize;
begin
  FNameToCharacterSetMap := TObjectDictionary<string, TdxFontCharacterSet>.Create([doOwnsValues], 512);
  FSystemRegularFonts := TStringList.Create;
  FUnicodeRangeInfo := TdxUnicodeRangeInfo.Create;

  FLoader := TRegularFontLoader.Create(False);
  PopulateNameToCharacterMap;
end;

class destructor TdxGdiFontCache.Finalize;
begin
  FLoader.Terminate;
  FLoader.WaitFor;
  FLoader.Free;
  FUnicodeRangeInfo.Free;
  FSystemRegularFonts.Free;
  FNameToCharacterSetMap.Free;
end;

function TdxGdiFontCache.CreateOverrideFontSubstitutes: TDictionary<string, string>;
begin
  Result := TDictionary<string, string>.Create;
end;

function TdxGdiFontCache.CreateFontInfoMeasurer(AUnitConverter: TdxDocumentLayoutUnitConverter): TdxFontInfoMeasurer;
begin
  Result := TdxGdiFontInfoMeasurer.Create(AUnitConverter);
end;

function TdxGdiFontCache.CreateFontInfoCore(const AFontName: string; ADoubleFontSize: Integer; const AFontStyle: TFontStyles): TdxFontInfo;
begin
  Result := TdxGdiFontInfo.Create(Measurer, AFontName, ADoubleFontSize, AFontStyle);
end;

function TdxGdiFontCache.GetFontCharacterRanges(AFontInfo: TdxFontInfo): TdxFontCharacterRangeArray;
var
  AGdiFontInfo: TdxGdiFontInfo absolute AFontInfo;
begin
  Result := AGdiFontInfo.GetFontUnicodeRanges(Measurer.MeasureGraphics.Handle, AGdiFontInfo.GdiFontHandle);
end;

function TdxGdiFontCache.ShouldUseDefaultFontToDrawInvisibleCharacter(AFontInfo: TdxFontInfo; ACharacter: Char): Boolean;
var
  AGdiFontInfo: TdxGdiFontInfo absolute AFontInfo;
begin
  Result := AGdiFontInfo.CanDrawCharacter(UnicodeRangeInfo, Measurer.MeasureGraphics, ACharacter);
end;

class procedure TdxGdiFontCache.PopulateNameToCharacterMap;
begin
  FNameToCharacterMapPopulatedEvent := TSimpleEvent.Create(nil, True, False, '');
  NameToCharacterMapPopulatedEvent.WaitFor(INFINITE);
  FreeAndNil(FNameToCharacterMapPopulatedEvent);
end;

function TdxGdiFontCache.GetFontCharacterSet(const AFontName: string): TdxFontCharacterSet;
begin
  System.TMonitor.Enter(NameToCharacterSetMap);
  try
    if not NameToCharacterSetMap.TryGetValue(AFontName, Result) then
    begin
      Result := CreateFontCharacterSet(AFontName);
      if Result <> nil then
        NameToCharacterSetMap.Add(AFontName, Result);
    end;
  finally
    System.TMonitor.Exit(NameToCharacterSetMap);
  end;
end;

function TdxGdiFontCache.FindSubstituteFont(const ASourceFontName: string; ACharacter: Char; var AFontCharacterSet: TdxFontCharacterSet): string;
var
  AMinDistance, ADistance: Integer;
  ASourceCharacterSet, ATestFontCharacterSet: TdxFontCharacterSet;
  AFontCharacterSetPair: TPair<string, TdxFontCharacterSet>;
  AUpperFontName: string;
  ACharCodePages, ACodePage: DWORD;
begin
  Result := ASourceFontName;
  System.TMonitor.Enter(NameToCharacterSetMap);
  try
  {$IFDEF DELPHI18}
    AUpperFontName := ASourceFontName.ToUpper;
  {$ELSE}
    AUpperFontName := ToUpper(ASourceFontName);
  {$ENDIF}
    if SystemRegularFonts.IndexOf(AUpperFontName) < 0 then
    begin
      if FontLink.GetCharCodePages(ACharacter, ACharCodePages) = S_OK then
        if FontLink.CodePagesToCodePage(ACharCodePages, CP_ACP, ACodePage) = S_OK then
        begin
          AUpperFontName := '';
          case ACodePage of
            1250..1259: AUpperFontName := 'TIMES NEW ROMAN'; 
            932: AUpperFontName := 'MS MINCHO';              
            949: AUpperFontName := 'BATANG';                 
            936: AUpperFontName := 'MS SONG';                
            950: AUpperFontName := 'NEW MINGLIU';            
            874: AUpperFontName := 'TAHOMA';                 
          end;
          if (AUpperFontName <> '') and (SystemRegularFonts.IndexOf(AUpperFontName) >= 0) then
          begin
            AFontCharacterSet := NameToCharacterSetMap.Items[AUpperFontName];
            if AFontCharacterSet.ContainsChar(ACharacter) then
              Exit(AUpperFontName);
          end;
        end;
    end;
    AMinDistance := MaxInt;
    ASourceCharacterSet := NameToCharacterSetMap[ASourceFontName];
    for AFontCharacterSetPair in NameToCharacterSetMap do
    begin
      ATestFontCharacterSet := AFontCharacterSetPair.Value;
      if ATestFontCharacterSet.ContainsChar(ACharacter) then
      begin
        ADistance := TdxFontCharacterSet.CalculatePanoseDistance(ASourceCharacterSet, ATestFontCharacterSet);
        if ADistance < AMinDistance then
        begin
          AMinDistance := ADistance;
          Result := AFontCharacterSetPair.Key;
        end;
      end;
    end;
  finally
    System.TMonitor.Exit(NameToCharacterSetMap);
  end;
end;

function TdxGdiFontCache.GetMeasurer: TdxGdiFontInfoMeasurer;
begin
  Result := TdxGdiFontInfoMeasurer(inherited Measurer);
end;

function TdxGdiFontCache.CreateFontCharacterSet(const AFontName: string): TdxFontCharacterSet;
var
  AFontInfo: TdxGdiFontInfo;
  ACharacterRanges: TdxFontCharacterRangeArray;
  AcharacterRangesCount: Integer;
begin
  AFontInfo := TdxGdiFontInfo.Create(Measurer, AFontName, 24, []);
  try
    ACharacterRanges := GetFontCharacterRanges(AFontInfo);
    ACharacterRangesCount := Length(ACharacterRanges);
    SetLength(ACharacterRanges, ACharacterRangesCount + 2);
    ACharacterRanges[AcharacterRangesCount].CopyFrom(0, 255);       
    Inc(AcharacterRangesCount);
    ACharacterRanges[AcharacterRangesCount].CopyFrom(61440, 61695); 
    Result := TdxFontCharacterSet.Create(ACharacterRanges, AFontInfo.Panose);
  finally
    AFontInfo.Free;
    ACharacterRanges := nil;
  end;
end;

{ TdxGdiFontCache.TRegularFontLoader }

class function TdxGdiFontCache.TRegularFontLoader.EnumFontInfoProc(
  var ALogFont: TLogFontW; ATextMetric: PTextMetricW; AFontType: Integer;
  AData: Pointer): Integer;

  function IsRegularFont: Boolean;
  begin
    Result := (AFontType = TRUETYPE_FONTTYPE) and (ATextMetric.tmItalic = 0) and (ATextMetric.tmUnderlined = 0) and
     (ATextMetric.tmStruckOut = 0) and (ATextMetric.tmWeight = FW_REGULAR);
  end;

var
  ATemp: string;
  AFonts: TStringList;
  ACallbackData: PCallbackData absolute AData;
  ACount: Integer;
begin
  if IsRegularFont then
  begin
    AFonts := ACallbackData.Fonts;
  {$IFDEF DELPHI18}
    ATemp  := string(PChar(@ALogFont.lfFaceName)).ToUpper;
  {$ELSE}
    ATemp  := ToUpper(PChar(@ALogFont.lfFaceName));
  {$ENDIF}
    if (ATemp <> '') and (ATemp[1] <> '@') then
    begin
      ACount := AFonts.Count;
      if ACount = 0 then
        AFonts.Add(ATemp)
      else
        if AFonts[ACount - 1] <> ATemp then
          AFonts.Add(ATemp);
    end;
  end;
  Result := Ord(ACallbackData.Thread.CanContinue);
end;

procedure TdxGdiFontCache.TRegularFontLoader.Execute;
var
  ALogFont: TLogFontW;
  ADC: HDC;
  I: Integer;
  AFontCharacterSet: TdxFontCharacterSet;
  AFonts: TStringList absolute TdxGdiFontCache.SystemRegularFonts;
  AUnitConverter: TdxDocumentLayoutUnitConverter;
  ACallbackData: TCallbackData;
begin
  AFonts := TdxGdiFontCache.SystemRegularFonts;
  ADC := CreateCompatibleDC(0);
  try
    AFonts.BeginUpdate;
    try
      ACallbackData.Fonts := AFonts;
      ACallbackData.Thread := Self;
      FillChar(ALogFont, SizeOf(ALogFont), 0);
      ALogFont.lfCharset := DEFAULT_CHARSET;
      EnumFontFamiliesExW(ADC, ALogFont, @EnumFontInfoProc, TdxNativeInt(@ACallbackData), 0);
      AFonts.Sorted := True;
    finally
      AFonts.EndUpdate;
    end;
    if CanContinue then
    begin
      AUnitConverter := TdxDocumentLayoutUnitConverter.CreateConverter(TdxDocumentLayoutUnit.Pixel, TdxCustomDocumentModel.Dpi);
      try
        for I := 0 to AFonts.Count - 1 do
        begin
          AFontCharacterSet := CreateFontCharacterSet(ADC, AFonts[I], AUnitConverter);
          if AFontCharacterSet <> nil then
            TdxGdiFontCache.NameToCharacterSetMap.Add(AFonts[I], AFontCharacterSet);
          if not CanContinue then
            Break;
        end;
      finally
        AUnitConverter.Free;
      end;
    end;
  finally
    DeleteDC(ADC);
  end;
  TdxGdiFontCache.NameToCharacterMapPopulatedEvent.SetEvent;
end;

function TdxGdiFontCache.TRegularFontLoader.CanContinue: Boolean;
begin
  Result := not Terminated and ((Application = nil) or not Application.Terminated);
end;

function TdxGdiFontCache.TRegularFontLoader.CreateFontCharacterSet(ADC: HDC; const AFontName: string;
  AUnitConverter: TdxDocumentLayoutUnitConverter): TdxFontCharacterSet;
var
  AFont, AOldFont: HFONT;
  ACharacterRanges: TdxFontCharacterRangeArray;
  ACharacterRangesCount: Integer;
  AOutlineTextMetrics: TOutlineTextmetricW;
begin
  AFont := TdxGdiFontInfoMeasurer.CreateFont(AFontName, 24, [], AUnitConverter);
  AOldFont := SelectObject(ADC, AFont);
  try
    ACharacterRanges := TdxGdiFontInfo.GetFontUnicodeRanges(ADC, 0); 
    ACharacterRangesCount := Length(ACharacterRanges);
    SetLength(ACharacterRanges, ACharacterRangesCount + 2);
    ACharacterRanges[ACharacterRangesCount].CopyFrom(0, 255);       
    Inc(AcharacterRangesCount);
    ACharacterRanges[ACharacterRangesCount].CopyFrom(61440, 61695); 

    AOutlineTextMetrics.otmSize := SizeOf(AOutlineTextMetrics);
    if Windows.GetOutlineTextMetricsW(ADC, SizeOf(AOutlineTextMetrics), @AOutlineTextMetrics) <> 0 then
      Result := TdxFontCharacterSet.Create(ACharacterRanges, AOutlineTextMetrics.otmPanoseNumber)
    else
      Result := nil;
  finally
    SelectObject(ADC, AOldFont);
    DeleteObject(AFont);
    ACharacterRanges := nil;
  end;
end;

{ TdxGdiFontCacheManager }

function TdxGdiFontCacheManager.CreateFontCache: TdxFontCache;
begin
  Result := TdxGdiFontCache.Create(UnitConverter);
end;

initialization
  CoInitialize(nil);
  FontLink := CreateComObject(CLASS_CMultiLanguage) as IMLangFontLink;

finalization
  if FontLink <> nil then
    FontLink.ResetFontMapping;
  FontLink := nil;
  CoUninitialize;

end.
