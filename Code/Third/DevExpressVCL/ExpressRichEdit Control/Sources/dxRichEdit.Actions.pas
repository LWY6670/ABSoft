{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Actions;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Types, SysUtils, ActnList, Classes, Generics.Collections, dxRichEdit.Control, dxRichEdit.Commands;

type

  TdxControlCommandClass = class of TdxControlCommand;

  { TdxRichEditControlAction }

  TdxRichEditControlAction = class(TAction)
  private
    FControl: TdxCustomRichEditControl;
    procedure SetControl(Value: TdxCustomRichEditControl);
  protected
    function DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand; virtual; abstract;
    procedure DoExecute(ACommand: TdxCommand); virtual;
    function CreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand;
    function GetState(ACommand: TdxCommand): IdxCommandUIState;
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure CopyFrom(const AState: IdxCommandUIState); virtual;
    procedure CopyTo(const AState: IdxCommandUIState); virtual;

    procedure DoUpdateState; virtual;
    procedure Reset;
    procedure UpdateState;
  public
    destructor Destroy; override;
    procedure ExecuteTarget(Target: TObject); override;
    function HandlesTarget(Target: TObject): Boolean; override;
    procedure UpdateTarget(Target: TObject); override;
    property Control: TdxCustomRichEditControl read FControl write SetControl;
  end;

  { TdxRichEditControlToggleFontBold }

  TdxRichEditControlToggleFontBold = class(TdxRichEditControlAction)
  protected
    function DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand; override;
  end;

  { TdxRichEditControlToggleFontItalic }

  TdxRichEditControlToggleFontItalic = class(TdxRichEditControlAction)
  protected
    function DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand; override;
  end;

  { TdxRichEditControlToggleFontUnderline }

  TdxRichEditControlToggleFontUnderline = class(TdxRichEditControlAction)
  protected
    function DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand; override;
  end;

  { TdxRichEditControlToggleFontDoubleUnderline }

  TdxRichEditControlToggleFontDoubleUnderline = class(TdxRichEditControlAction)
  protected
    function DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand; override;
  end;

  { TdxRichEditControlToggleFontStrikeout }

  TdxRichEditControlToggleFontStrikeout = class(TdxRichEditControlAction)
  protected
    function DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand; override;
  end;

  { TdxRichEditControlToggleFontDoubleStrikeout }

  TdxRichEditControlToggleFontDoubleStrikeout = class(TdxRichEditControlAction)
  protected
    function DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand; override;
  end;

  { TdxRichEditControlToggleParagraphAlignmentLeft }

  TdxRichEditControlToggleParagraphAlignmentLeft = class(TdxRichEditControlAction)
  protected
    function DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand; override;
  end;

  { TdxRichEditControlToggleParagraphAlignmentCenter }

  TdxRichEditControlToggleParagraphAlignmentCenter = class(TdxRichEditControlAction)
  protected
    function DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand; override;
  end;

  { TdxRichEditControlToggleParagraphAlignmentRight }

  TdxRichEditControlToggleParagraphAlignmentRight = class(TdxRichEditControlAction)
  protected
    function DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand; override;
  end;

  { TdxRichEditControlToggleParagraphAlignmentJustify }

  TdxRichEditControlToggleParagraphAlignmentJustify = class(TdxRichEditControlAction)
  protected
    function DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand; override;
  end;

  { TdxRichEditControlCopySelection }

  TdxRichEditControlCopySelection = class(TdxRichEditControlAction)
  protected
    function DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand; override;
  end;

  { TdxRichEditControlPasteSelection }

  TdxRichEditControlPasteSelection = class(TdxRichEditControlAction)
  protected
    function DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand; override;
  end;

  { TdxRichEditControlCutSelection }

  TdxRichEditControlCutSelection = class(TdxRichEditControlAction)
  protected
    function DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand; override;
  end;

  { TdxRichEditControlSetSingleParagraphSpacing }

  TdxRichEditControlSetSingleParagraphSpacing = class(TdxRichEditControlAction)
  protected
    function DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand; override;
  end;

  { TdxRichEditControlSetDoubleParagraphSpacing }

  TdxRichEditControlSetDoubleParagraphSpacing = class(TdxRichEditControlAction)
  protected
    function DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand; override;
  end;

  { TdxRichEditControlSetSesquialteralParagraphSpacing }

  TdxRichEditControlSetSesquialteralParagraphSpacing = class(TdxRichEditControlAction)
  protected
    function DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand; override;
  end;

  { TdxRichEditControlUndo }

  TdxRichEditControlUndo = class(TdxRichEditControlAction)
  protected
    function DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand; override;
  end;

  { TdxRichEditControlRedo }

  TdxRichEditControlRedo = class(TdxRichEditControlAction)
  protected
    function DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand; override;
  end;

  { TdxRichEditControlIncreaseFontSize }

  TdxRichEditControlIncreaseFontSize = class(TdxRichEditControlAction)
  protected
    function DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand; override;
  end;

  { TdxRichEditControlDecreaseFontSize }

  TdxRichEditControlDecreaseFontSize = class(TdxRichEditControlAction)
  protected
    function DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand; override;
  end;

  { TdxRichEditControlToggleFontSuperscript }

  TdxRichEditControlToggleFontSuperscript = class(TdxRichEditControlAction)
  protected
    function DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand; override;
  end;

  { TdxRichEditControlToggleFontSuperscript }

  TdxRichEditControlToggleFontSubscript = class(TdxRichEditControlAction)
  protected
    function DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand; override;
  end;

  { TdxRichEditControlToggleBulletedList }

  TdxRichEditControlToggleBulletedList = class(TdxRichEditControlAction)
  protected
    function DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand; override;
  end;

  { TdxRichEditControlToggleSimpleNumberingList }

  TdxRichEditControlToggleSimpleNumberingList = class(TdxRichEditControlAction)
  protected
    function DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand; override;
  end;

  { TdxToggleMultiLevelListCommand }

  TdxRichEditControlToggleMultiLevelList = class(TdxRichEditControlAction)
  protected
    function DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand; override;
  end;

  { TdxRichEditControlSelectAll }

  TdxRichEditControlSelectAll = class(TdxRichEditControlAction)
  protected
    function DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand; override;
  end;

  { TdxRichEditControlToggleShowWhitespace }

  TdxRichEditControlToggleShowWhitespace = class(TdxRichEditControlAction)
  protected
    function DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand; override;
  end;

  { TdxRichEditControlIncrementIndent }

  TdxRichEditControlIncrementIndent = class(TdxRichEditControlAction)
  protected
    function DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand; override;
  end;

  { TdxRichEditControlDecrementIndent }

  TdxRichEditControlDecrementIndent = class(TdxRichEditControlAction)
  protected
    function DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand; override;
  end;

  { TdxRichEditControlShowParagraphForm }

  TdxRichEditControlShowParagraphForm = class(TdxRichEditControlAction)
  protected
    function DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand; override;
  end;

  { TdxRichEditControlValueAction }

  TdxRichEditControlValueAction<T> = class abstract(TdxRichEditControlAction)
  private
    FLockCount: Integer;
    FValue: T;
    procedure SetValue(const AValue: T);
  protected
    procedure CopyFrom(const AState: IdxCommandUIState); override;
    procedure CopyTo(const AState: IdxCommandUIState); override;
    procedure DoExecute(ACommand: TdxCommand); override;

    class function IsValueEquals(const AValue1, AValue2: T): Boolean; virtual; abstract;
  public
    function Execute: Boolean; override;

    procedure BeginUpdate;
    procedure EndUpdate;
    function IsLocked: Boolean;

    property Value: T read FValue write SetValue;
  end;

  { TdxRichEditControlChangeFontName }

  TdxRichEditControlChangeFontName = class(TdxRichEditControlValueAction<string>)
  protected
    function DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand; override;
    class function IsValueEquals(const AValue1, AValue2: string): Boolean; override;
  end;

  { TdxRichEditControlChangeFontSize }

  TdxRichEditControlChangeFontSize = class(TdxRichEditControlValueAction<Single>)
  protected
    function DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand; override;
    class function IsValueEquals(const AValue1, AValue2: Single): Boolean; override;
  end;

implementation

uses
  dxRichEdit.Commands.ChangeProperties, dxRichEdit.Commands.CopyAndPaste, dxRichEdit.Commands.Numbering,
  dxRichEdit.Commands.Selection;

{ TdxRichEditControlAction }

destructor TdxRichEditControlAction.Destroy;
begin
  Control := nil;
  inherited Destroy;
end;

procedure TdxRichEditControlAction.ExecuteTarget(Target: TObject);
var
  ACommand: TdxCommand;
begin
  if not HandlesTarget(Target) then
    Exit;

  ACommand := CreateCommand(Control);
  try
    DoExecute(ACommand);
  finally
    ACommand.Free;
  end;
end;

function TdxRichEditControlAction.GetState(ACommand: TdxCommand): IdxCommandUIState;
begin
  Result := ACommand.CreateDefaultCommandUIState;
  CopyTo(Result);
end;

function TdxRichEditControlAction.HandlesTarget(Target: TObject): Boolean;
begin
  Result := (Control <> nil) or ((Control <> nil) and (Target = Control) or
    (Control = nil) and (Target is TdxCustomRichEditControl)) and TdxCustomRichEditControl(Target).Focused;
end;

procedure TdxRichEditControlAction.UpdateTarget(Target: TObject);
begin
  if Target is TdxCustomRichEditControl then
    Control := TdxCustomRichEditControl(Target);
  UpdateState;
end;

procedure TdxRichEditControlAction.DoExecute(ACommand: TdxCommand);
begin
  ACommand.Execute;
end;

function TdxRichEditControlAction.CreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand;
begin
  Result := DoCreateCommand(ATarget);
  Result.CommandSourceType := TdxCommandSourceType.Menu;
end;

procedure TdxRichEditControlAction.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if (Operation = opRemove) and (AComponent = Control) then
    FControl := nil;
end;

procedure TdxRichEditControlAction.CopyFrom(const AState: IdxCommandUIState);
begin
  Enabled := AState.Enabled;
  Checked := AState.Checked;
  Visible := AState.Visible;
end;

procedure TdxRichEditControlAction.CopyTo(const AState: IdxCommandUIState);
begin
  AState.Enabled := Enabled;
  AState.Checked := Checked;
  AState.Visible := Visible;
end;

procedure TdxRichEditControlAction.DoUpdateState;
var
  ACommand: TdxCommand;
  AState: IdxCommandUIState;
begin
  ACommand := CreateCommand(Control);
  try
    AState := GetState(ACommand);
    ACommand.UpdateUIState(AState);
  finally
    ACommand.Free;
  end;
  CopyFrom(AState);
end;

procedure TdxRichEditControlAction.Reset;
begin
  Enabled := False;
  Checked := False;
end;

procedure TdxRichEditControlAction.UpdateState;
begin
  if Control <> nil then
    DoUpdateState
  else
    Reset;
end;

procedure TdxRichEditControlAction.SetControl(Value: TdxCustomRichEditControl);
begin
  if FControl <> Value then
  begin
    if FControl <> nil then
      FControl.RemoveFreeNotification(Self);
    FControl := Value;
    if FControl <> nil then
      Value.FreeNotification(Self);
    UpdateState;
  end;
end;

{ TdxRichEditControlToggleFontBold }

function TdxRichEditControlToggleFontBold.DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand;
begin
  Result := TdxToggleFontBoldCommand.Create(ATarget);
end;

{ TdxRichEditControlToggleFontItalic }

function TdxRichEditControlToggleFontItalic.DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand;
begin
  Result := TdxToggleFontItalicCommand.Create(ATarget);
end;

{ TdxRichEditControlToggleFontUnderline }

function TdxRichEditControlToggleFontUnderline.DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand;
begin
  Result := TdxToggleFontUnderlineCommand.Create(ATarget);
end;

{ TdxRichEditControlToggleFontDoubleUnderline }

function TdxRichEditControlToggleFontDoubleUnderline.DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand;
begin
  Result := TdxToggleFontDoubleUnderlineCommand.Create(ATarget);
end;

{ TdxRichEditControlToggleFontStrikeout }

function TdxRichEditControlToggleFontStrikeout.DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand;
begin
  Result := TdxToggleFontStrikeoutCommand.Create(ATarget);
end;

{ TdxRichEditControlToggleFontDoubleStrikeout }

function TdxRichEditControlToggleFontDoubleStrikeout.DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand;
begin
  Result := TdxToggleFontDoubleStrikeoutCommand.Create(ATarget);
end;

{ TdxRichEditControlToggleParagraphAlignmentLeft }

function TdxRichEditControlToggleParagraphAlignmentLeft.DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand;
begin
  Result := TdxToggleParagraphAlignmentLeftCommand.Create(ATarget);
end;

{ TdxRichEditControlToggleParagraphAlignmentCenter }

function TdxRichEditControlToggleParagraphAlignmentCenter.DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand;
begin
  Result := TdxToggleParagraphAlignmentCenterCommand.Create(ATarget);
end;

{ TdxRichEditControlToggleParagraphAlignmentRight }

function TdxRichEditControlToggleParagraphAlignmentRight.DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand;
begin
  Result := TdxToggleParagraphAlignmentRightCommand.Create(ATarget);
end;

{ TdxRichEditControlToggleParagraphAlignmentJustify }

function TdxRichEditControlToggleParagraphAlignmentJustify.DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand;
begin
  Result := TdxToggleParagraphAlignmentJustifyCommand.Create(ATarget);
end;

{ TdxRichEditControlCopySelection }

function TdxRichEditControlCopySelection.DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand;
begin
  Result := TdxCopySelectionCommand.Create(ATarget);
end;

{ TdxRichEditControlPasteSelection }

function TdxRichEditControlPasteSelection.DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand;
begin
  Result := TdxPasteSelectionCommand.Create(ATarget);
end;

{ TdxRichEditControlCutSelection }

function TdxRichEditControlCutSelection.DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand;
begin
  Result := TdxCutSelectionCommand.Create(ATarget);
end;

{ TdxRichEditControlSetSingleParagraphSpacing }

function TdxRichEditControlSetSingleParagraphSpacing.DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand;
begin
  Result := TdxSetSingleParagraphSpacingCommand.Create(ATarget);
end;

{ TdxRichEditControlSetDoubleParagraphSpacing }

function TdxRichEditControlSetDoubleParagraphSpacing.DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand;
begin
  Result := TdxSetDoubleParagraphSpacingCommand.Create(ATarget);
end;

{ TdxRichEditControlSetSesquialteralParagraphSpacing }

function TdxRichEditControlSetSesquialteralParagraphSpacing.DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand;
begin
  Result := TdxSetSesquialteralParagraphSpacingCommand.Create(ATarget);
end;

{ TdxRichEditControlUndo }

function TdxRichEditControlUndo.DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand;
begin
  Result := TdxUndoCommand.Create(ATarget);
end;

{ TdxRichEditControlRedo }

function TdxRichEditControlRedo.DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand;
begin
  Result := TdxRedoCommand.Create(ATarget);
end;

{ TdxRichEditControlIncreaseFontSize }

function TdxRichEditControlIncreaseFontSize.DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand;
begin
  Result := TdxIncreaseFontSizeCommand.Create(ATarget);
end;

{ TdxRichEditControlDecreaseFontSize }

function TdxRichEditControlDecreaseFontSize.DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand;
begin
  Result := TdxDecreaseFontSizeCommand.Create(ATarget);
end;

{ TdxRichEditControlDecreaseToggleFontSuperscript }

function TdxRichEditControlToggleFontSuperscript.DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand;
begin
  Result := TdxToggleFontSuperscriptCommand.Create(ATarget);
end;

{ TdxRichEditControlToggleFontSubscript }

function TdxRichEditControlToggleFontSubscript.DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand;
begin
  Result := TdxToggleFontSubscriptCommand.Create(ATarget);
end;

{ TdxRichEditControlInsertBulletList }

function TdxRichEditControlToggleBulletedList.DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand;
begin
  Result := TdxToggleBulletedListCommand.Create(ATarget);
end;

{ TdxRichEditControlToggleSimpleNumberingList }

function TdxRichEditControlToggleSimpleNumberingList.DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand;
begin
  Result := TdxToggleSimpleNumberingListCommand.Create(ATarget);
end;

{ TdxRichEditControlToggleMultiLevelList }

function TdxRichEditControlToggleMultiLevelList.DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand;
begin
  Result := TdxToggleMultiLevelListCommand.Create(ATarget);
end;

{ TdxRichEditControlSelectAll }

function TdxRichEditControlSelectAll.DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand;
begin
  Result := TdxSelectAllCommand.Create(ATarget);
end;

{ TdxRichEditControlToggleShowWhitespace }

function TdxRichEditControlToggleShowWhitespace.DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand;
begin
  Result := TdxToggleShowWhitespaceCommand.Create(ATarget);
end;

{ TdxRichEditControlIncrementIndent }

function TdxRichEditControlIncrementIndent.DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand;
begin
  Result := TdxIncrementIndentCommand.Create(ATarget);
end;

{ TdxRichEditControlDecrementIndent }

function TdxRichEditControlDecrementIndent.DoCreateCommand(ATarget: TdxCustomRichEditControl): TdxCommand;
begin
  Result := TdxDecrementIndentCommand.Create(ATarget);
end;

{ TdxRichEditControlShowParagraphForm }

function TdxRichEditControlShowParagraphForm.DoCreateCommand(
  ATarget: TdxCustomRichEditControl): TdxCommand;
begin
  Result := TdxShowParagraphFormCommand.Create(ATarget);
end;

{ TdxRichEditControlValueAction<T> }

procedure TdxRichEditControlValueAction<T>.BeginUpdate;
begin
  Inc(FLockCount);
end;

procedure TdxRichEditControlValueAction<T>.CopyFrom(
  const AState: IdxCommandUIState);
var
  AValueState: IdxValueBasedCommandUIState<T>;
begin
  if IsLocked then
    Exit;
  inherited CopyFrom(AState);
  if Supports(AState, IdxValueBasedCommandUIState<T>, AValueState) then
    FValue := AValueState.Value;
end;

procedure TdxRichEditControlValueAction<T>.CopyTo(
  const AState: IdxCommandUIState);
var
  AValueState: IdxValueBasedCommandUIState<T>;
begin
  inherited CopyTo(AState);
  if Supports(AState, IdxValueBasedCommandUIState<T>, AValueState) then
    AValueState.Value := FValue;
end;

procedure TdxRichEditControlValueAction<T>.DoExecute(ACommand: TdxCommand);
var
  AState: IdxCommandUIState;
begin
  AState := GetState(ACommand);
  ACommand.ForceExecute(AState);
end;

procedure TdxRichEditControlValueAction<T>.EndUpdate;
begin
  Dec(FLockCount);
end;

function TdxRichEditControlValueAction<T>.Execute: Boolean;
begin
  BeginUpdate;
  try
    inherited Execute;
  finally
    EndUpdate;
  end;
end;

function TdxRichEditControlValueAction<T>.IsLocked: Boolean;
begin
  Result := FLockCount > 0;
end;

procedure TdxRichEditControlValueAction<T>.SetValue(const AValue: T);
begin
 if not IsValueEquals(FValue, AValue) then
  begin
    FValue := AValue;
    Execute;
  end;
end;

{ TdxRichEditControlChangeFontName }

function TdxRichEditControlChangeFontName.DoCreateCommand(
  ATarget: TdxCustomRichEditControl): TdxCommand;
begin
  Result := TdxChangeFontNameCommand.Create(ATarget);
end;

class function TdxRichEditControlChangeFontName.IsValueEquals(const AValue1,
  AValue2: string): Boolean;
begin
  Result := AValue1 = AValue2;
end;

{ TdxRichEditControlChangeFontSize }

function TdxRichEditControlChangeFontSize.DoCreateCommand(
  ATarget: TdxCustomRichEditControl): TdxCommand;
begin
  Result := TdxChangeFontSizeCommand.Create(ATarget);
end;

class function TdxRichEditControlChangeFontSize.IsValueEquals(const AValue1,
  AValue2: Single): Boolean;
begin
  Result := AValue1 = AValue2;
end;

end.
