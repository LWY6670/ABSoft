{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.DocumentModel.Selection;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Types, SysUtils, Generics.Collections,
  dxCoreClasses,
  dxRichEdit.DocumentLayout.Position, dxRichEdit.DocumentModel.PieceTable, dxRichEdit.DocumentLayout.UnitConverter;

type
  TdxRectangularObjectSelectionLayout = class;
  TdxRowSelectionLayoutBase = class;
  TdxFloatingObjectAnchorSelectionLayout = class;

  { IdxSelectionPainter }

  IdxSelectionPainter = interface
  ['{5E9AA6D1-9F74-4BD6-834C-3A89C283C95C}']
    procedure Draw(AViewInfo: TdxRectangularObjectSelectionLayout); overload;
    procedure Draw(AViewInfo: TdxRowSelectionLayoutBase); overload;
    procedure Draw(AViewInfo: TdxFloatingObjectAnchorSelectionLayout); overload;
  end;

  { IdxSelectionLayoutItem }

  IdxSelectionLayoutItem = interface
  ['{0CC8B2CD-CD1B-4E94-A415-22C7F3611A14}']
    procedure Draw(const ASelectionPainter: IdxSelectionPainter);
    function Update: Boolean; 
    function HitTest(ALogPosition: TdxDocumentLogPosition; const ALogicalPoint: TPoint): Boolean;
    function GetType: TClass;
  end;

  TdxPageSelectionLayoutsCollection = class(TList<IdxSelectionLayoutItem>);

  { TdxTableStructureBySelectionCalculator}

  TdxTableStructureBySelectionCalculator = class
  private
    FPieceTable: TdxPieceTable;
  public
    constructor Create(APieceTable: TdxPieceTable); virtual;

    function SetStartCell(APos: TdxDocumentLogPosition): IdxSelectedTableStructureBase;

    property ActivePieceTable: TdxPieceTable read FPieceTable;
  end;

  { TdxStartSelectedCellInTable }

  TdxStartSelectedCellInTable = class(TInterfacedObject, IdxSelectedTableStructureBase)
  private
    FFirstSelectedCell: TdxTableCell;
		FOriginalStartLogPostition: TdxDocumentLogPosition;
  protected
    function GetOriginalStartLogPosition: TdxDocumentLogPosition;
    function GetFirstSelectedCell: TdxTableCell;
    function GetIsNotEmpty: Boolean;
    function GetSelectedOnlyOneCell: Boolean;
    function GetRowsCount: Integer;
    procedure SetFirstSelectedCell(AStartCell: TdxTableCell; APos: TdxDocumentLogPosition);
  public
    constructor Create; overload;
    constructor Create(AFirstSelectedCell: TdxTableCell; AOriginalStartLogPosition: TdxDocumentLogPosition); overload;
    constructor Create(AOld: IdxSelectedTableStructureBase); overload;

    property FirstSelectedCell: TdxTableCell read FFirstSelectedCell;
		property OriginalStartLogPostition: TdxDocumentLogPosition read FOriginalStartLogPostition;
  end;

  { TdxRowSelectionLayoutBase }

  TdxRowSelectionLayoutBase = class abstract(TInterfacedObject, IdxSelectionLayoutItem)
  strict private
    FBounds: TRect;
    FPage: TdxPage;
  protected
    procedure Draw(const ASelectionPainter: IdxSelectionPainter);
    function GetType: TClass;
    function HitTest(ALogPosition: TdxDocumentLogPosition; const ALogicalPoint: TPoint): Boolean;
    function Update: Boolean; virtual; abstract;
  public
    constructor Create(APage: TdxPage);

    property Bounds: TRect read FBounds write FBounds;
    property Page: TdxPage read FPage;
  end;

  { TdxRowSelectionLayout }

  TdxRowSelectionLayout = class(TdxRowSelectionLayoutBase)
  strict private
    FEnd: TdxDocumentLayoutPosition;
    FStart: TdxDocumentLayoutPosition;
  protected
    function Update: Boolean; override;
  public
    constructor Create(AStart, AEnd: TdxDocumentLayoutPosition; APage: TdxPage);
    destructor Destroy; override;

    property &End: TdxDocumentLayoutPosition read FEnd;
    property Start: TdxDocumentLayoutPosition read FStart;
  end;

  { TdxEntireRowSelectionLayout }

  TdxEntireRowSelectionLayout = class(TdxRowSelectionLayoutBase)
  strict private
    FEnd: TdxDocumentLayoutPosition;
    FStart: TdxDocumentLayoutPosition;
  protected
    function Update: Boolean; override;
  public
    constructor Create(AStart, AEnd: TdxDocumentLayoutPosition; APage: TdxPage);
    destructor Destroy; override;

    property &End: TdxDocumentLayoutPosition read FEnd;
    property Start: TdxDocumentLayoutPosition read FStart;
  end;

  { TdxRectangularObjectSelectionLayoutBase }

  TdxRectangularObjectSelectionLayoutBase = class abstract(TcxIUnknownObject, IdxSelectionLayoutItem)
  private
    FView: TObject;
    FBox: TdxBox;
    FLogStart: TdxDocumentLogPosition;
    FLogEnd: TdxDocumentLogPosition;
    FPieceTable: TdxPieceTable;
    FUnitConverter: TdxDocumentLayoutUnitConverter;
  protected
    procedure Draw(const ASelectionPainter: IdxSelectionPainter); virtual; abstract;
    function Update: Boolean; virtual; abstract;
    function HitTest(ALogPosition: TdxDocumentLogPosition; const ALogicalPoint: TPoint): Boolean; virtual; abstract;
    function GetType: TClass;

  public
    constructor Create(AView: TObject; ABox: TdxBox; AStart: TdxDocumentLogPosition; APieceTable: TdxPieceTable); 

    property LogStart: TdxDocumentLogPosition read FLogStart;
    property LogEnd: TdxDocumentLogPosition read FLogEnd;
    property Box: TdxBox read FBox;
    property View: TObject read FView;
    property PieceTable: TdxPieceTable read FPieceTable;
    property UnitConverter: TdxDocumentLayoutUnitConverter read FUnitConverter;
  end;

  { TdxRectangularObjectSelectionLayout }

  TdxRectangularObjectSelectionLayout = class(TdxRectangularObjectSelectionLayoutBase)
  private
    FHitTestTransform: TdxTransformMatrix;
    FAnchor: TdxFloatingObjectAnchorSelectionLayout;
  protected
    function GetResizeable: Boolean; virtual;
  public
    property HitTestTransform: TdxTransformMatrix read FHitTestTransform write FHitTestTransform; 
    property Anchor: TdxFloatingObjectAnchorSelectionLayout read FAnchor write FAnchor;
    property Resizeable: Boolean read GetResizeable;

    procedure Draw(const ASelectionPainter: IdxSelectionPainter); override;
    function Update: Boolean; override;
    function HitTest(ALogPosition: TdxDocumentLogPosition; const ALogicalPoint: TPoint): Boolean; override;
  end;

  { TdxFloatingObjectAnchorSelectionLayout }

  TdxFloatingObjectAnchorSelectionLayout = class(TdxRectangularObjectSelectionLayoutBase)
  private
    FAnchorBounds: TRect;
  public
    procedure Draw(const ASelectionPainter: IdxSelectionPainter); override;
    function Update: Boolean; override;
    function HitTest(ALogPosition: TdxDocumentLogPosition; const ALogicalPoint: TPoint): Boolean; override;

    property AnchorBounds: TRect read FAnchorBounds write FAnchorBounds;
  end;

implementation

uses
  dxTypeHelpers, dxRichEdit.View.Core;

type

  { TdxRectangularObjectSelectionLayoutBaseHelper }

  TdxRectangularObjectSelectionLayoutBaseHelper = class helper for TdxRectangularObjectSelectionLayoutBase
  private
    function GetView: TdxRichEditView;
  public
    property View: TdxRichEditView read GetView;
  end;

  function TdxRectangularObjectSelectionLayoutBaseHelper.GetView: TdxRichEditView;
  begin
    Result := TdxRichEditView(inherited View);
  end;

{ TdxTableStructureBySelectionCalculator }

constructor TdxTableStructureBySelectionCalculator.Create(
  APieceTable: TdxPieceTable);
begin
  inherited Create;
  FPieceTable := APieceTable;
end;

function TdxTableStructureBySelectionCalculator.SetStartCell(
  APos: TdxDocumentLogPosition): IdxSelectedTableStructureBase;
var
  AStartCell: TdxTableCell;
begin
  AStartCell := ActivePieceTable.FindParagraph(APos).GetCell;
  if AStartCell = nil then
    Result := TdxStartSelectedCellInTable.Create(nil, APos)
  else
  begin
    Assert(False, 'not implemented');
  end;
end;

{ TdxStartSelectedCellInTable }

constructor TdxStartSelectedCellInTable.Create;
begin
  inherited Create;
end;

constructor TdxStartSelectedCellInTable.Create(AFirstSelectedCell: TdxTableCell;
  AOriginalStartLogPosition: TdxDocumentLogPosition);
begin
  Create;
  FFirstSelectedCell := AFirstSelectedCell;
  FOriginalStartLogPostition := AOriginalStartLogPosition;
end;

constructor TdxStartSelectedCellInTable.Create(AOld: IdxSelectedTableStructureBase);
begin
  Create(AOld.FirstSelectedCell, AOld.OriginalStartLogPosition);
end;

function TdxStartSelectedCellInTable.GetFirstSelectedCell: TdxTableCell;
begin
  Result := FFirstSelectedCell;
end;

function TdxStartSelectedCellInTable.GetIsNotEmpty: Boolean;
begin
  Result := FFirstSelectedCell <> nil;
end;

function TdxStartSelectedCellInTable.GetOriginalStartLogPosition: TdxDocumentLogPosition;
begin
  Result := FOriginalStartLogPostition;
end;

function TdxStartSelectedCellInTable.GetRowsCount: Integer;
begin
  if FirstSelectedCell = nil then
    Result := 0
  else
    Result := 1;
end;

function TdxStartSelectedCellInTable.GetSelectedOnlyOneCell: Boolean;
begin
  Result := True;
end;

procedure TdxStartSelectedCellInTable.SetFirstSelectedCell(
  AStartCell: TdxTableCell; APos: TdxDocumentLogPosition);
begin
  FFirstSelectedCell := AStartCell;
  FOriginalStartLogPostition := APos;
end;

{ TdxRowSelectionLayoutBase }

constructor TdxRowSelectionLayoutBase.Create(APage: TdxPage);
begin
  inherited Create;
  FPage := APage;
end;

procedure TdxRowSelectionLayoutBase.Draw(
  const ASelectionPainter: IdxSelectionPainter);
begin
  ASelectionPainter.Draw(Self);
end;

function TdxRowSelectionLayoutBase.GetType: TClass;
begin
  Result := ClassType;
end;

function TdxRowSelectionLayoutBase.HitTest(ALogPosition: TdxDocumentLogPosition;
  const ALogicalPoint: TPoint): Boolean;
begin
  Result := Bounds.Contains(ALogicalPoint);
end;

{ TdxRectangularObjectSelectionLayoutBase }

constructor TdxRectangularObjectSelectionLayoutBase.Create(AView: TObject; ABox: TdxBox; AStart: TdxDocumentLogPosition;
  APieceTable: TdxPieceTable);
begin
  inherited Create;
  Assert(AView <> nil);
  Assert(ABox <> nil);
  FUnitConverter := TdxRichEditView(AView).DocumentModel.LayoutUnitConverter;
  FView := AView;
  FBox := ABox;
  FLogStart := AStart;
  FLogEnd := AStart + 1;
  FPieceTable := APieceTable;
end;

function TdxRectangularObjectSelectionLayoutBase.GetType: TClass;
begin
  Result := ClassType;
end;

{ TdxRowSelectionLayout }

constructor TdxRowSelectionLayout.Create(AStart,
  AEnd: TdxDocumentLayoutPosition; APage: TdxPage);
begin
  inherited Create(APage);
  Assert(AStart.IsValid(TdxDocumentLayoutDetailsLevel.Character));
  Assert(AEnd.IsValid(TdxDocumentLayoutDetailsLevel.Character));
  FStart := AStart.Clone;
  FEnd := AEnd.Clone;
end;

destructor TdxRowSelectionLayout.Destroy;
begin
  FreeAndNil(FStart);
  FreeAndNil(FEnd);
  inherited Destroy;
end;

function TdxRowSelectionLayout.Update: Boolean;
var
  AStartCharacterBounds, AEndCharacterBounds: TRect;
begin
  AStartCharacterBounds := Start.Character.Bounds;
  AEndCharacterBounds := &End.Character.Bounds;
  Bounds := Rect(AStartCharacterBounds.Left, AStartCharacterBounds.Top, AEndCharacterBounds.Right, AEndCharacterBounds.Bottom);
  Result := True;
end;

{ TdxEntireRowSelectionLayout }

constructor TdxEntireRowSelectionLayout.Create(AStart,
  AEnd: TdxDocumentLayoutPosition; APage: TdxPage);
begin
  inherited Create(APage);
  Assert(AStart.IsValid(TdxDocumentLayoutDetailsLevel.Row));
  Assert(AEnd.IsValid(TdxDocumentLayoutDetailsLevel.Row));
  FStart := AStart.Clone;
  FEnd := AEnd.Clone;
end;

destructor TdxEntireRowSelectionLayout.Destroy;
begin
  FreeAndNil(FStart);
  FreeAndNil(FEnd);
  inherited Destroy;
end;

function TdxEntireRowSelectionLayout.Update: Boolean;
var
  AStartCharacterBounds, AEndCharacterBounds: TRect;
  ARowBoxes: TdxBoxCollection;
  ARowBounds: TRect;
begin
  ARowBoxes := Start.Row.Boxes;
  AStartCharacterBounds := ARowBoxes.First.Bounds;
  AEndCharacterBounds := ARowBoxes.Last.Bounds;
  ARowBounds := Start.Row.Bounds;
  Bounds := Rect(AStartCharacterBounds.Left, ARowBounds.Top, AEndCharacterBounds.Right, ARowBounds.Bottom);
  Result := True;
end;

{ TdxRectangularObjectSelectionLayout }

procedure TdxRectangularObjectSelectionLayout.Draw(const ASelectionPainter: IdxSelectionPainter);
begin
  ASelectionPainter.Draw(Self);
  if FAnchor <> nil then
    FAnchor.Draw(ASelectionPainter);
end;

function TdxRectangularObjectSelectionLayout.GetResizeable: Boolean;
begin
  Result := False;
end;

function TdxRectangularObjectSelectionLayout.HitTest(ALogPosition: TdxDocumentLogPosition;
  const ALogicalPoint: TPoint): Boolean;
begin
  raise Exception.Create(''); 
end;

function TdxRectangularObjectSelectionLayout.Update: Boolean;
begin
  FAnchor := nil;
  Result := True;
end;

{ TdxFloatingObjectAnchorSelectionLayout }

procedure TdxFloatingObjectAnchorSelectionLayout.Draw(const ASelectionPainter: IdxSelectionPainter);
begin
  ASelectionPainter.Draw(Self);
end;

function TdxFloatingObjectAnchorSelectionLayout.HitTest(ALogPosition: TdxDocumentLogPosition;
  const ALogicalPoint: TPoint): Boolean;
begin
  Result := AnchorBounds.Contains(ALogicalPoint);
end;

function TdxFloatingObjectAnchorSelectionLayout.Update: Boolean;
begin
  Result := True;
end;

end.
