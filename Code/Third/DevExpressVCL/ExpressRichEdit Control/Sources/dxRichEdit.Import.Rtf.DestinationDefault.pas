{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Import.Rtf.DestinationDefault;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Types, Classes, SysUtils, dxCoreClasses, dxRichEdit.Import.Rtf,
  dxRichEdit.Import.Rtf.DestinationPieceTable;

type
  TdxDefaultDestination = class(TdxDestinationPieceTable)
  private
    procedure ApplyStyleLinks;

    procedure AddDocumentProtectionKeywords(ATable: TdxKeywordTranslatorTable);
    procedure AddDocumentPropertiesKeywords(ATable: TdxKeywordTranslatorTable);
    procedure AddSectionKeywords(ATable: TdxKeywordTranslatorTable);
    procedure AddFootNoteAndEndNoteKeywords(ATable: TdxKeywordTranslatorTable);
    procedure AddCommentKeywords(ATable: TdxKeywordTranslatorTable);

    //Handlers
    class procedure AnnotationProtectionHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure BottomMarginKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure ColumnBreakKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure CommentAnnotationKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure CommentAuthorKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure CommentEndPositionKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure CommentIdKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure CommentStartPositionKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure DefaultCharacterPropertiesKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure DefaultParagraphPropertiesKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure DefaultTabKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure DeffKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure DisplayBackgroundShapeHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure DocumentProtectionLevelHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure EndNoteNumberingChicagoHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure EndNoteNumberingChosungHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure EndNoteNumberingDecimalEnclosedCircleHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure EndNoteNumberingDecimalFullWidthHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure EndNoteNumberingDecimalHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure EndNoteNumberingGanadaHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure EndNoteNumberingLowerCaseLetterHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure EndNoteNumberingLowerCaseRomanHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure EndNoteNumberingRestartContinuousHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure EndNoteNumberingRestartEachSectionHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure EndNoteNumberingStartHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure EndNoteNumberingUpperCaseLetterHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure EndNoteNumberingUpperCaseRomanHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure EndNotePlacementBelowTextHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure EndNotePlacementEndOfDocumentHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure EndNotePlacementEndOfSectionHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure EndNotePlacementPageBottomHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure EnforceDocumentProtectionHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure FooterForFirstPageKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure FooterForLeftPagesKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure FooterForRightPagesKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure FooterKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure FootNoteKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure FootNoteNumberingChicagoHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure FootNoteNumberingChosungHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure FootNoteNumberingDecimalEnclosedCircleHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure FootNoteNumberingDecimalFullWidthHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure FootNoteNumberingDecimalHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure FootNoteNumberingGanadaHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure FootNoteNumberingLowerCaseLetterHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure FootNoteNumberingLowerCaseRomanHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure FootNoteNumberingRestartContinuousHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure FootNoteNumberingRestartEachPageHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure FootNoteNumberingRestartEachSectionHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure FootNoteNumberingStartHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure FootNoteNumberingUpperCaseLetterHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure FootNoteNumberingUpperCaseRomanHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure FootNotePlacementBelowTextHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure FootNotePlacementEndOfDocumentHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure FootNotePlacementEndOfSectionHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure FootNotePlacementPageBottomHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure GutterAtRightHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure GutterKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure HeaderForFirstPageKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure HeaderForLeftPagesKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure HeaderForRightPagesKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure HeaderKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure InfoKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure LandscapeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure LeftMarginKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure LineNumberingContinuousHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure LineNumberingDistanceHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure LineNumberingRestartHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure LineNumberingRestartOnEachPageHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure LineNumberingStartingLineNumberHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure LineNumberingStepHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure ListOverrideTableKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure ListTableKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure NewSectionHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure OnlyAllowEditingOfFormFieldsHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure PageBackgroundHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure PageBreakKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure PageFacingHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure PageNumberingArabicAbjadHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure PageNumberingArabicAlphaHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure PageNumberingChapterHeaderStyleHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure PageNumberingChapterSeparatorColonHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure PageNumberingChapterSeparatorEmDashHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure PageNumberingChapterSeparatorEnDashHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure PageNumberingChapterSeparatorHyphenHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure PageNumberingChapterSeparatorPeriodHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure PageNumberingChosungHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure PageNumberingDecimalEnclosedCircleHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure PageNumberingDecimalFullWidthHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure PageNumberingDecimalHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure PageNumberingGanadaHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure PageNumberingHindiConsonantsHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure PageNumberingHindiDescriptiveHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure PageNumberingHindiNumbersHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure PageNumberingHindiVowelsHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure PageNumberingLowerLetterHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure PageNumberingLowerRomanHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure PageNumberingStartHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure PageNumberingThaiDescriptiveHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure PageNumberingThaiLettersHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure PageNumberingThaiNumbersHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure PageNumberingUpperLetterHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure PageNumberingUpperRomanHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure PageNumberingVietnameseDescriptiveHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure PaperHeightKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure PaperSizeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure PaperWidthKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure ReadOnlyProtectionHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure RightMarginKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure RtfKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionBottomMarginHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionBreakColumnHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionBreakEvenPageHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionBreakNoneHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionBreakOddPageHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionBreakPageHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionColumnCountHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionColumnSpaceHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionCurrentColumnIndexHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionCurrentColumnSpaceHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionCurrentColumnWidthHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionDefaultHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionDifferentFirstPageHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionDrawVerticalSeparatorHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionEndNoteNumberingChicagoHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionEndNoteNumberingChosungHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionEndNoteNumberingDecimalEnclosedCircleHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionEndNoteNumberingDecimalFullWidthHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionEndNoteNumberingDecimalHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionEndNoteNumberingGanadaHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionEndNoteNumberingLowerCaseLetterHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionEndNoteNumberingLowerCaseRomanHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionEndNoteNumberingRestartContinuousHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionEndNoteNumberingRestartEachSectionHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionEndNoteNumberingStartHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionEndNoteNumberingUpperCaseLetterHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionEndNoteNumberingUpperCaseRomanHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionFirstPagePaperSourceHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionFooterOffsetHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionFootNoteNumberingChicagoHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionFootNoteNumberingChosungHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionFootNoteNumberingDecimalEnclosedCircleHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionFootNoteNumberingDecimalFullWidthHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionFootNoteNumberingDecimalHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionFootNoteNumberingGanadaHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionFootNoteNumberingLowerCaseLetterHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionFootNoteNumberingLowerCaseRomanHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionFootNoteNumberingRestartContinuousHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionFootNoteNumberingRestartEachPageHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionFootNoteNumberingRestartEachSectionHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionFootNoteNumberingStartHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionFootNoteNumberingUpperCaseLetterHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionFootNoteNumberingUpperCaseRomanHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionFootNotePlacementBelowTextHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionFootNotePlacementPageBottomHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionGutterHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionHeaderOffsetHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionLandscapeHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionLeftMarginHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionOtherPagePaperSourceHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionPageNumberingContinuousHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionPageNumberingRestartHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionPageNumberingStartHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionPaperHeightHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionPaperWidthHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionRightMarginHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionTextFlowHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SectionTopMarginHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure StyleSheetKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure TopMarginKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure VerticalTextAlignmentBottomHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure VerticalTextAlignmentCenterHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure VerticalTextAlignmentJustifyHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure VerticalTextAlignmentTopHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
  protected
    procedure FixLastParagraph; override;
    procedure PopulateKeywordTable(ATable: TdxKeywordTranslatorTable); override;
  public
    procedure NestedGroupFinished(ADestination: TdxRichEditRtfDestinationBase); override;
  end;

implementation

uses
  dxRichEdit.Import.Rtf.DestinationInfo, dxRichEdit.Import.Rtf.DestinationStyleSheet, dxRichEdit.Import.Rtf.DestinationListTable,
  dxRichEdit.DocumentModel.Section, dxRichEdit.Import.Rtf.DestinationsDefaultPropertes, dxRichEdit.DocumentModel.PieceTable,
  dxRichEdit.DocumentModel.Styles, dxRichEdit.Utils.Characters, dxRichEdit.DocumentModel.Core, dxRichEdit.Import.Rtf.ListConverter,
  dxRichEdit.Platform.Font;

{ TdxDefaultDestination }

procedure TdxDefaultDestination.NestedGroupFinished(ADestination: TdxRichEditRtfDestinationBase);
var
  AConverter: TdxRtfListConverter;
begin
  inherited NestedGroupFinished(ADestination);

  if ADestination is TdxListTableDestination then
    Importer.DocumentProperties.ListTableComplete := True;

  if ADestination is TdxListOverrideTableDestination then
    Importer.DocumentProperties.ListOverrideTableComplete := True;

  if Importer.DocumentProperties.ListOverrideTableComplete and Importer.DocumentProperties.ListTableComplete then
  begin
    AConverter := TdxRtfListConverter.Create(Importer);
    try
      AConverter.Convert(Importer.DocumentProperties.ListTable, Importer.DocumentProperties.ListOverrideTable);
    finally
      AConverter.Free;
    end;
    Importer.DocumentProperties.ListTableComplete := False;
    Importer.DocumentProperties.ListOverrideTableComplete := False;
  end;

  if ADestination is TdxStyleSheetDestination then
    ApplyStyleLinks;

  if ADestination is TdxDestinationPieceTable then
    if ADestination.PieceTable <> PieceTable then
      TdxDestinationPieceTable(ADestination).FinalizePieceTableCreation;
end;

procedure TdxDefaultDestination.FixLastParagraph;
begin
  Importer.ApplySectionFormatting(True);
  if not Importer.Options.SuppressLastParagraphDelete and PieceTable.ShouldFixLastParagraph then
    PieceTable.FixLastParagraphCore
  else
    Importer.ApplyFormattingToLastParagraph;

  if not PieceTable.DocumentModel.DocumentCapabilities.ParagraphsAllowed then
    PieceTable.UnsafeRemoveLastSpaceSymbolRun;

  PieceTable.FixTables;
end;

procedure TdxDefaultDestination.PopulateKeywordTable(ATable: TdxKeywordTranslatorTable);
begin
  ATable.Add('rtf', RtfKeywordHandler);
  ATable.Add('deff', DeffKeywordHandler);
  ATable.Add('info', InfoKeywordHandler);
  ATable.Add('stylesheet', StyleSheetKeywordHandler);
  ATable.Add('defchp', DefaultCharacterPropertiesKeywordHandler);
  ATable.Add('defpap', DefaultParagraphPropertiesKeywordHandler);
  AddCommonCharacterKeywords(ATable);
  AddCommonParagraphKeywords(ATable);
  AddCommonSymbolsAndObjectsKeywords(ATable);
  ATable.Add('page', PageBreakKeywordHandler);
  ATable.Add('column', ColumnBreakKeywordHandler);
  AddCommonTabKeywords(ATable);
  ATable.Add('deftab', DefaultTabKeywordHandler);
  AddDocumentProtectionKeywords(ATable);
  AddCommonNumberingListsKeywords(ATable);
  ATable.Add('listtable', ListTableKeywordHandler);
  ATable.Add('listoverridetable', ListOverrideTableKeywordHandler);
  ATable.Add('background', PageBackgroundHandler);
  AppendTableKeywords(ATable);
  AddDocumentPropertiesKeywords(ATable);
  AddSectionKeywords(ATable);
  AddFootNoteAndEndNoteKeywords(ATable);
  AddCommentKeywords(ATable);
end;

procedure TdxDefaultDestination.ApplyStyleLinks;
var
  AStyleLinks: TdxIntegerDictionary;
  ADocumentModel: TdxDocumentModel;
  ACharacterStyles: TdxCharacterStyleCollection;
  AParagraphStyles: TdxParagraphStyleCollection;
  ARtfParagraphStyleIndex: Integer;
  ARtfCharacterStyleIndex: Integer;
  AParagraphStyleIndex, ACharacterStyleIndex: Integer;
  ACharacterStyle: TdxCharacterStyle;
  AParagraphStyle: TdxParagraphStyle;
  ARtfNextStyleIndex, ANextStyleIndex: Integer;
begin
  AStyleLinks := Importer.LinkParagraphStyleIndexToCharacterStyleIndex;
  ADocumentModel := Importer.DocumentModel;
  ACharacterStyles := ADocumentModel.CharacterStyles;
  AParagraphStyles := ADocumentModel.ParagraphStyles;
  for ARtfParagraphStyleIndex in AStyleLinks.Keys do
  begin
    ARtfCharacterStyleIndex := AStyleLinks[ARtfParagraphStyleIndex];
    if Importer.ParagraphStyleCollectionIndex.TryGetValue(ARtfParagraphStyleIndex, AParagraphStyleIndex) and
      Importer.CharacterStyleCollectionIndex.TryGetValue(ARtfCharacterStyleIndex, ACharacterStyleIndex) then
    begin
      ACharacterStyle := ACharacterStyles[ACharacterStyleIndex];
      AParagraphStyle := AParagraphStyles[AParagraphStyleIndex];
      ADocumentModel.StyleLinkManager.CreateLinkCore(AParagraphStyle, ACharacterStyle);
    end;
  end;
  AStyleLinks := Importer.NextParagraphStyleIndexTable;
  for ARtfParagraphStyleIndex in AStyleLinks.Keys do
  begin
    ARtfNextStyleIndex := AStyleLinks[ARtfParagraphStyleIndex];
    if Importer.ParagraphStyleCollectionIndex.TryGetValue(ARtfParagraphStyleIndex, AParagraphStyleIndex) then
    begin
      if Importer.ParagraphStyleCollectionIndex.TryGetValue(ARtfNextStyleIndex, ANextStyleIndex) then
        AParagraphStyles[AParagraphStyleIndex].NextParagraphStyle := AParagraphStyles[ANextStyleIndex];
    end;
  end;
end;

procedure TdxDefaultDestination.AddDocumentProtectionKeywords(ATable: TdxKeywordTranslatorTable);
begin
  ATable.Add('enforceprot', EnforceDocumentProtectionHandler);
  ATable.Add('annotprot', AnnotationProtectionHandler);
  ATable.Add('readprot', ReadOnlyProtectionHandler);
  ATable.Add('protlevel', DocumentProtectionLevelHandler);
end;

procedure TdxDefaultDestination.AddDocumentPropertiesKeywords(ATable: TdxKeywordTranslatorTable);
begin
  ATable.Add('paperw', PaperWidthKeywordHandler);
  ATable.Add('paperh', PaperHeightKeywordHandler);
  ATable.Add('psz', PaperSizeKeywordHandler);
  ATable.Add('landscape', LandscapeKeywordHandler);
  ATable.Add('gutter', GutterKeywordHandler);
  ATable.Add('margl', LeftMarginKeywordHandler);
  ATable.Add('margr', RightMarginKeywordHandler);
  ATable.Add('margt', TopMarginKeywordHandler);
  ATable.Add('margb', BottomMarginKeywordHandler);
  ATable.Add('rtlgutter', GutterAtRightHandler);
  ATable.Add('pgnstart', PageNumberingStartHandler);
  ATable.Add('facingp', PageFacingHandler);
  ATable.Add('endnotes', FootNotePlacementEndOfSectionHandler);
  ATable.Add('enddoc', FootNotePlacementEndOfDocumentHandler);
  ATable.Add('ftntj', FootNotePlacementBelowTextHandler);
  ATable.Add('ftnbj', FootNotePlacementPageBottomHandler);
  ATable.Add('ftnstart', FootNoteNumberingStartHandler);
  ATable.Add('ftnrstpg', FootNoteNumberingRestartEachPageHandler);
  ATable.Add('ftnrestart', FootNoteNumberingRestartEachSectionHandler);
  ATable.Add('ftnrstcont', FootNoteNumberingRestartContinuousHandler);
  ATable.Add('ftnnar', FootNoteNumberingDecimalHandler);
  ATable.Add('ftnnalc', FootNoteNumberingLowerCaseLetterHandler);
  ATable.Add('ftnnauc', FootNoteNumberingUpperCaseLetterHandler);
  ATable.Add('ftnnrlc', FootNoteNumberingLowerCaseRomanHandler);
  ATable.Add('ftnnruc', FootNoteNumberingUpperCaseRomanHandler);
  ATable.Add('ftnnchi', FootNoteNumberingChicagoHandler);
  ATable.Add('ftnnchosung', FootNoteNumberingChosungHandler);
  ATable.Add('ftnncnum', FootNoteNumberingDecimalEnclosedCircleHandler);
  ATable.Add('ftnndbar', FootNoteNumberingDecimalFullWidthHandler);
  ATable.Add('ftnnganada', FootNoteNumberingGanadaHandler);
  ATable.Add('aendnotes', EndNotePlacementEndOfSectionHandler);
  ATable.Add('aenddoc', EndNotePlacementEndOfDocumentHandler);
  ATable.Add('aftntj', EndNotePlacementBelowTextHandler);
  ATable.Add('aftnbj', EndNotePlacementPageBottomHandler);
  ATable.Add('aftnstart', EndNoteNumberingStartHandler);
  ATable.Add('aftnrestart', EndNoteNumberingRestartEachSectionHandler);
  ATable.Add('aftnrstcont', EndNoteNumberingRestartContinuousHandler);
  ATable.Add('aftnnar', EndNoteNumberingDecimalHandler);
  ATable.Add('aftnnalc', EndNoteNumberingLowerCaseLetterHandler);
  ATable.Add('aftnnauc', EndNoteNumberingUpperCaseLetterHandler);
  ATable.Add('aftnnrlc', EndNoteNumberingLowerCaseRomanHandler);
  ATable.Add('aftnnruc', EndNoteNumberingUpperCaseRomanHandler);
  ATable.Add('aftnnchi', EndNoteNumberingChicagoHandler);
  ATable.Add('aftnnchosung', EndNoteNumberingChosungHandler);
  ATable.Add('aftnncnum', EndNoteNumberingDecimalEnclosedCircleHandler);
  ATable.Add('aftnndbar', EndNoteNumberingDecimalFullWidthHandler);
  ATable.Add('aftnnganada', EndNoteNumberingGanadaHandler);
  ATable.Add('viewbksp', DisplayBackgroundShapeHandler);
end;

procedure TdxDefaultDestination.AddSectionKeywords(ATable: TdxKeywordTranslatorTable);
begin
  ATable.Add('pgndec', PageNumberingDecimalHandler);
  ATable.Add('pgnucrm', PageNumberingUpperRomanHandler);
  ATable.Add('pgnlcrm', PageNumberingLowerRomanHandler);
  ATable.Add('pgnucltr', PageNumberingUpperLetterHandler);
  ATable.Add('pgnlcltr', PageNumberingLowerLetterHandler);
  ATable.Add('pgnbidia', PageNumberingArabicAbjadHandler);
  ATable.Add('pgnbidib', PageNumberingArabicAlphaHandler);
  ATable.Add('pgnchosung', PageNumberingChosungHandler);
  ATable.Add('pgncnum', PageNumberingDecimalEnclosedCircleHandler);
  ATable.Add('pgndecd', PageNumberingDecimalFullWidthHandler);
  ATable.Add('pgnganada', PageNumberingGanadaHandler);
  ATable.Add('pgnhindia', PageNumberingHindiVowelsHandler);
  ATable.Add('pgnhindib', PageNumberingHindiConsonantsHandler);
  ATable.Add('pgnhindic', PageNumberingHindiNumbersHandler);
  ATable.Add('pgnhindid', PageNumberingHindiDescriptiveHandler);
  ATable.Add('pgnthaia', PageNumberingThaiLettersHandler);
  ATable.Add('pgnthaib', PageNumberingThaiNumbersHandler);
  ATable.Add('pgnthaic', PageNumberingThaiDescriptiveHandler);
  ATable.Add('pgnvieta', PageNumberingVietnameseDescriptiveHandler);
  ATable.Add('pgnhn', PageNumberingChapterHeaderStyleHandler);
  ATable.Add('pgnhnsh', PageNumberingChapterSeparatorHyphenHandler);
  ATable.Add('pgnhnsp', PageNumberingChapterSeparatorPeriodHandler);
  ATable.Add('pgnhnsc', PageNumberingChapterSeparatorColonHandler);
  ATable.Add('pgnhnsm', PageNumberingChapterSeparatorEmDashHandler);
  ATable.Add('pgnhnsn', PageNumberingChapterSeparatorEnDashHandler);
  ATable.Add('vertal', VerticalTextAlignmentBottomHandler);
  ATable.Add('vertalb', VerticalTextAlignmentBottomHandler);
  ATable.Add('vertalt', VerticalTextAlignmentTopHandler);
  ATable.Add('vertalc', VerticalTextAlignmentCenterHandler);
  ATable.Add('vertalj', VerticalTextAlignmentJustifyHandler);
  ATable.Add('pgnstarts', SectionPageNumberingStartHandler);
  ATable.Add('pgncont', SectionPageNumberingContinuousHandler);
  ATable.Add('pgnrestart', SectionPageNumberingRestartHandler);
  ATable.Add('stextflow', SectionTextFlowHandler);
  ATable.Add('linemod', LineNumberingStepHandler);
  ATable.Add('linex', LineNumberingDistanceHandler);
  ATable.Add('linestarts', LineNumberingStartingLineNumberHandler);
  ATable.Add('linerestart', LineNumberingRestartHandler);
  ATable.Add('lineppage', LineNumberingRestartOnEachPageHandler);
  ATable.Add('linecont', LineNumberingContinuousHandler);
  ATable.Add('pgwsxn', SectionPaperWidthHandler);
  ATable.Add('pghsxn', SectionPaperHeightHandler);
  ATable.Add('lndscpsxn', SectionLandscapeHandler);
  ATable.Add('marglsxn', SectionLeftMarginHandler);
  ATable.Add('margrsxn', SectionRightMarginHandler);
  ATable.Add('margtsxn', SectionTopMarginHandler);
  ATable.Add('margbsxn', SectionBottomMarginHandler);
  ATable.Add('guttersxn', SectionGutterHandler);
  ATable.Add('headery', SectionHeaderOffsetHandler);
  ATable.Add('footery', SectionFooterOffsetHandler);
  ATable.Add('binfsxn', SectionFirstPagePaperSourceHandler);
  ATable.Add('binsxn', SectionOtherPagePaperSourceHandler);
  ATable.Add('sectunlocked', OnlyAllowEditingOfFormFieldsHandler);
  ATable.Add('cols', SectionColumnCountHandler);
  ATable.Add('colsx', SectionColumnSpaceHandler);
  ATable.Add('colno', SectionCurrentColumnIndexHandler);
  ATable.Add('colsr', SectionCurrentColumnSpaceHandler);
  ATable.Add('colw', SectionCurrentColumnWidthHandler);
  ATable.Add('linebetcol', SectionDrawVerticalSeparatorHandler);
  ATable.Add('sbknone', SectionBreakNoneHandler);
  ATable.Add('sbkcol', SectionBreakColumnHandler);
  ATable.Add('sbkpage', SectionBreakPageHandler);
  ATable.Add('sbkeven', SectionBreakEvenPageHandler);
  ATable.Add('sbkodd', SectionBreakOddPageHandler);
  ATable.Add('sectd', SectionDefaultHandler);
  ATable.Add('sect', NewSectionHandler);
  ATable.Add('titlepg', SectionDifferentFirstPageHandler);
  ATable.Add('header', HeaderKeywordHandler);
  ATable.Add('headerl', HeaderForLeftPagesKeywordHandler);
  ATable.Add('headerr', HeaderForRightPagesKeywordHandler);
  ATable.Add('headerf', HeaderForFirstPageKeywordHandler);
  ATable.Add('footer', FooterKeywordHandler);
  ATable.Add('footerl', FooterForLeftPagesKeywordHandler);
  ATable.Add('footerr', FooterForRightPagesKeywordHandler);
  ATable.Add('footerf', FooterForFirstPageKeywordHandler);
  ATable.Add('sftntj', SectionFootNotePlacementBelowTextHandler);
  ATable.Add('sftnbj', SectionFootNotePlacementPageBottomHandler);
  ATable.Add('sftnstart', SectionFootNoteNumberingStartHandler);
  ATable.Add('sftnrstpg', SectionFootNoteNumberingRestartEachPageHandler);
  ATable.Add('sftnrestart', SectionFootNoteNumberingRestartEachSectionHandler);
  ATable.Add('sftnrstcont', SectionFootNoteNumberingRestartContinuousHandler);
  ATable.Add('sftnnar', SectionFootNoteNumberingDecimalHandler);
  ATable.Add('sftnnalc', SectionFootNoteNumberingLowerCaseLetterHandler);
  ATable.Add('sftnnauc', SectionFootNoteNumberingUpperCaseLetterHandler);
  ATable.Add('sftnnrlc', SectionFootNoteNumberingLowerCaseRomanHandler);
  ATable.Add('sftnnruc', SectionFootNoteNumberingUpperCaseRomanHandler);
  ATable.Add('sftnnchi', SectionFootNoteNumberingChicagoHandler);
  ATable.Add('sftnnchosung', SectionFootNoteNumberingChosungHandler);
  ATable.Add('sftnncnum', SectionFootNoteNumberingDecimalEnclosedCircleHandler);
  ATable.Add('sftnndbar', SectionFootNoteNumberingDecimalFullWidthHandler);
  ATable.Add('sftnnganada', SectionFootNoteNumberingGanadaHandler);
  ATable.Add('saftnstart', SectionEndNoteNumberingStartHandler);
  ATable.Add('saftnrestart', SectionEndNoteNumberingRestartEachSectionHandler);
  ATable.Add('saftnrstcont', SectionEndNoteNumberingRestartContinuousHandler);
  ATable.Add('saftnnar', SectionEndNoteNumberingDecimalHandler);
  ATable.Add('saftnnalc', SectionEndNoteNumberingLowerCaseLetterHandler);
  ATable.Add('saftnnauc', SectionEndNoteNumberingUpperCaseLetterHandler);
  ATable.Add('saftnnrlc', SectionEndNoteNumberingLowerCaseRomanHandler);
  ATable.Add('saftnnruc', SectionEndNoteNumberingUpperCaseRomanHandler);
  ATable.Add('saftnnchi', SectionEndNoteNumberingChicagoHandler);
  ATable.Add('saftnnchosung', SectionEndNoteNumberingChosungHandler);
  ATable.Add('saftnncnum', SectionEndNoteNumberingDecimalEnclosedCircleHandler);
  ATable.Add('saftnndbar', SectionEndNoteNumberingDecimalFullWidthHandler);
  ATable.Add('saftnnganada', SectionEndNoteNumberingGanadaHandler);
end;

procedure TdxDefaultDestination.AddFootNoteAndEndNoteKeywords(ATable: TdxKeywordTranslatorTable);
begin
  ATable.Add('footnote', FootNoteKeywordHandler);
end;

procedure TdxDefaultDestination.AddCommentKeywords(ATable: TdxKeywordTranslatorTable);
begin
  ATable.Add('atrfstart', CommentStartPositionKeywordHandler);
  ATable.Add('atrfend', CommentEndPositionKeywordHandler);
  ATable.Add('atnid', CommentIdKeywordHandler);
  ATable.Add('atnauthor', CommentAuthorKeywordHandler);
  ATable.Add('annotation', CommentAnnotationKeywordHandler);
end;

class procedure TdxDefaultDestination.AnnotationProtectionHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.BottomMarginKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  if not AHasParameter then
    AParameterValue := 1440;
  AParameterValue := AImporter.UnitConverter.TwipsToModelUnits(AParameterValue);
  AImporter.DocumentProperties.DefaultSectionProperties.Margins.Bottom := AParameterValue;
  AImporter.Position.SectionFormattingInfo.Margins.Bottom := AParameterValue;
end;

class procedure TdxDefaultDestination.ColumnBreakKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.ParseCharWithoutDecoding(TdxCharacters.ColumnBreak);
end;

class procedure TdxDefaultDestination.CommentAnnotationKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.CommentAuthorKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.CommentEndPositionKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.CommentIdKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.CommentStartPositionKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.DefaultCharacterPropertiesKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.Destination := TdxDefaultCharacterPropertiesDestination.Create(AImporter);
end;

class procedure TdxDefaultDestination.DefaultParagraphPropertiesKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.Destination := TdxDefaultParagraphPropertiesDestination.Create(AImporter);
end;

class procedure TdxDefaultDestination.DefaultTabKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  if not AHasParameter or (AParameterValue <= 0) then
    AParameterValue := 720;
  AImporter.DocumentModel.DocumentProperties.DefaultTabWidth := AImporter.UnitConverter.TwipsToModelUnits(AParameterValue);
end;

class procedure TdxDefaultDestination.DeffKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.DocumentProperties.DefaultFontNumber := AParameterValue;
end;

class procedure TdxDefaultDestination.DisplayBackgroundShapeHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.DocumentProtectionLevelHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.EndNoteNumberingChicagoHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.EndNoteNumberingChosungHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.EndNoteNumberingDecimalEnclosedCircleHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.EndNoteNumberingDecimalFullWidthHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.EndNoteNumberingDecimalHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.EndNoteNumberingGanadaHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.EndNoteNumberingLowerCaseLetterHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.EndNoteNumberingLowerCaseRomanHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin

end;

class procedure TdxDefaultDestination.EndNoteNumberingRestartContinuousHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin

end;

class procedure TdxDefaultDestination.EndNoteNumberingRestartEachSectionHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.EndNoteNumberingStartHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin

end;

class procedure TdxDefaultDestination.EndNoteNumberingUpperCaseLetterHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.EndNoteNumberingUpperCaseRomanHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.EndNotePlacementBelowTextHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.EndNotePlacementEndOfDocumentHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin

end;

class procedure TdxDefaultDestination.EndNotePlacementEndOfSectionHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.EndNotePlacementPageBottomHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.EnforceDocumentProtectionHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.FooterForFirstPageKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.FooterForLeftPagesKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.FooterForRightPagesKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.FooterKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.FootNoteKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.FootNoteNumberingChicagoHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.FootNoteNumberingChosungHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.FootNoteNumberingDecimalEnclosedCircleHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.FootNoteNumberingDecimalFullWidthHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.FootNoteNumberingDecimalHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin

end;

class procedure TdxDefaultDestination.FootNoteNumberingGanadaHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.FootNoteNumberingLowerCaseLetterHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.FootNoteNumberingLowerCaseRomanHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.FootNoteNumberingRestartContinuousHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin

end;

class procedure TdxDefaultDestination.FootNoteNumberingRestartEachPageHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.FootNoteNumberingRestartEachSectionHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.FootNoteNumberingStartHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
end;

class procedure TdxDefaultDestination.FootNoteNumberingUpperCaseLetterHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.FootNoteNumberingUpperCaseRomanHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.FootNotePlacementBelowTextHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.FootNotePlacementEndOfDocumentHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.FootNotePlacementEndOfSectionHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.FootNotePlacementPageBottomHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
end;

class procedure TdxDefaultDestination.GutterAtRightHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.DocumentProperties.DefaultSectionProperties.Margins.GutterAlignment := TdxSectionGutterAlignment.Right;
  AImporter.Position.SectionFormattingInfo.Margins.GutterAlignment := TdxSectionGutterAlignment.Right;
end;

class procedure TdxDefaultDestination.GutterKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
var
  AValue: Integer;
begin
  if not AHasParameter then
    AParameterValue := 0;
  AValue := AImporter.UnitConverter.TwipsToModelUnits(AParameterValue);
  AImporter.DocumentProperties.DefaultSectionProperties.Margins.Gutter := AValue;
  AImporter.Position.SectionFormattingInfo.Margins.Gutter := AValue;
end;

class procedure TdxDefaultDestination.HeaderForFirstPageKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.HeaderForLeftPagesKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.HeaderForRightPagesKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.HeaderKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.InfoKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.Destination := TdxInfoDestination.Create(AImporter);
end;

class procedure TdxDefaultDestination.LandscapeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.DocumentProperties.DefaultSectionProperties.Page.Landscape := True;
  AImporter.Position.SectionFormattingInfo.Page.Landscape := True;
end;

class procedure TdxDefaultDestination.LeftMarginKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  if not AHasParameter then
    AParameterValue := 1800;
  AParameterValue := AImporter.UnitConverter.TwipsToModelUnits(AParameterValue);
  AImporter.DocumentProperties.DefaultSectionProperties.Margins.Left := AParameterValue;
  AImporter.Position.SectionFormattingInfo.Margins.Left := AParameterValue;
end;

class procedure TdxDefaultDestination.LineNumberingContinuousHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.LineNumberingDistanceHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  if not AHasParameter then
    AParameterValue := 360;
  AImporter.Position.SectionFormattingInfo.LineNumbering.Distance := AImporter.UnitConverter.TwipsToModelUnits(AParameterValue);
end;

class procedure TdxDefaultDestination.LineNumberingRestartHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.LineNumberingRestartOnEachPageHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.LineNumberingStartingLineNumberHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.LineNumberingStepHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.ListOverrideTableKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.Destination := TdxListOverrideTableDestination.Create(AImporter);
end;

class procedure TdxDefaultDestination.ListTableKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.Destination := TdxListTableDestination.Create(AImporter);
end;

class procedure TdxDefaultDestination.NewSectionHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.OnlyAllowEditingOfFormFieldsHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.Position.SectionFormattingInfo.GeneralSectionInfo.OnlyAllowEditingOfFormFields := True;
end;

class procedure TdxDefaultDestination.PageBackgroundHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  NotImplemented();
end;

class procedure TdxDefaultDestination.PageBreakKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.ParseCharWithoutDecoding(TdxCharacters.PageBreak);
end;

class procedure TdxDefaultDestination.PageFacingHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.PageNumberingArabicAbjadHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.PageNumberingArabicAlphaHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.PageNumberingChapterHeaderStyleHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.PageNumberingChapterSeparatorColonHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.PageNumberingChapterSeparatorEmDashHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.PageNumberingChapterSeparatorEnDashHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.PageNumberingChapterSeparatorHyphenHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.PageNumberingChapterSeparatorPeriodHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.PageNumberingChosungHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.PageNumberingDecimalEnclosedCircleHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.PageNumberingDecimalFullWidthHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.PageNumberingDecimalHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.Position.SectionFormattingInfo.PageNumbering.NumberingFormat := TdxNumberingFormat.Decimal;
end;

class procedure TdxDefaultDestination.PageNumberingGanadaHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.PageNumberingHindiConsonantsHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.PageNumberingHindiDescriptiveHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.PageNumberingHindiNumbersHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.PageNumberingHindiVowelsHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.PageNumberingLowerLetterHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.PageNumberingLowerRomanHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.PageNumberingStartHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.PageNumberingThaiDescriptiveHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.PageNumberingThaiLettersHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.PageNumberingThaiNumbersHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.PageNumberingUpperLetterHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.PageNumberingUpperRomanHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.PageNumberingVietnameseDescriptiveHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.PaperHeightKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
var
  AValue: Integer;
begin
  if not AHasParameter then
    AParameterValue := 15840;
  AValue := AImporter.UnitConverter.TwipsToModelUnits(AParameterValue);
  AImporter.DocumentProperties.DefaultSectionProperties.Page.Height := AValue;
  AImporter.Position.SectionFormattingInfo.Page.Height := AValue;
end;

class procedure TdxDefaultDestination.PaperSizeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
var
  AValue: TdxPaperKind;
begin
  if AHasParameter then
  begin
    AValue := TdxPaperKind(AParameterValue);
    AImporter.DocumentProperties.DefaultSectionProperties.Page.PaperKind := AValue;
    AImporter.Position.SectionFormattingInfo.Page.PaperKind := AValue;
  end;
end;

class procedure TdxDefaultDestination.PaperWidthKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
var
  AValue: Integer;
begin
  if not AHasParameter then
    AParameterValue := 12240;
  AValue := AImporter.UnitConverter.TwipsToModelUnits(AParameterValue);
  AImporter.DocumentProperties.DefaultSectionProperties.Page.Width := AValue;
  AImporter.Position.SectionFormattingInfo.Page.Width := AValue;
end;

class procedure TdxDefaultDestination.ReadOnlyProtectionHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.RightMarginKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  if not AHasParameter then
    AParameterValue := 1800;
  AParameterValue := AImporter.UnitConverter.TwipsToModelUnits(AParameterValue);
  AImporter.DocumentProperties.DefaultSectionProperties.Margins.Right := AParameterValue;
  AImporter.Position.SectionFormattingInfo.Margins.Right := AParameterValue;
end;

class procedure TdxDefaultDestination.RtfKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
//do nothing
end;

class procedure TdxDefaultDestination.SectionBottomMarginHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.Position.SectionFormattingInfo.Margins.Bottom := AImporter.UnitConverter.TwipsToModelUnits(AParameterValue);
end;

class procedure TdxDefaultDestination.SectionBreakColumnHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.SectionBreakEvenPageHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.SectionBreakNoneHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.Position.SectionFormattingInfo.GeneralSectionInfo.StartType := TdxSectionStartType.Continuous;
end;

class procedure TdxDefaultDestination.SectionBreakOddPageHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.SectionBreakPageHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.Position.SectionFormattingInfo.GeneralSectionInfo.StartType := TdxSectionStartType.NextPage;
end;

class procedure TdxDefaultDestination.SectionColumnCountHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.SectionColumnSpaceHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.SectionCurrentColumnIndexHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.SectionCurrentColumnSpaceHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.SectionCurrentColumnWidthHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.SectionDefaultHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.Position.SectionFormattingInfo.Assign(AImporter.DocumentProperties.DefaultSectionProperties);
end;

class procedure TdxDefaultDestination.SectionDifferentFirstPageHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.SectionDrawVerticalSeparatorHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.SectionEndNoteNumberingChicagoHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.SectionEndNoteNumberingChosungHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.SectionEndNoteNumberingDecimalEnclosedCircleHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.SectionEndNoteNumberingDecimalFullWidthHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.SectionEndNoteNumberingDecimalHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.SectionEndNoteNumberingGanadaHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.SectionEndNoteNumberingLowerCaseLetterHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.SectionEndNoteNumberingLowerCaseRomanHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.SectionEndNoteNumberingRestartContinuousHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.SectionEndNoteNumberingRestartEachSectionHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.SectionEndNoteNumberingStartHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.SectionEndNoteNumberingUpperCaseLetterHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.SectionEndNoteNumberingUpperCaseRomanHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.SectionFirstPagePaperSourceHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.SectionFooterOffsetHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  if not AHasParameter then
    AParameterValue := 720;
  AImporter.Position.SectionFormattingInfo.Margins.FooterOffset := AImporter.UnitConverter.TwipsToModelUnits(AParameterValue);
end;

class procedure TdxDefaultDestination.SectionFootNoteNumberingChicagoHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.SectionFootNoteNumberingChosungHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.SectionFootNoteNumberingDecimalEnclosedCircleHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.SectionFootNoteNumberingDecimalFullWidthHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.SectionFootNoteNumberingDecimalHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.SectionFootNoteNumberingGanadaHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.SectionFootNoteNumberingLowerCaseLetterHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.SectionFootNoteNumberingLowerCaseRomanHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.SectionFootNoteNumberingRestartContinuousHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.SectionFootNoteNumberingRestartEachPageHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.SectionFootNoteNumberingRestartEachSectionHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.SectionFootNoteNumberingStartHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.SectionFootNoteNumberingUpperCaseLetterHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.SectionFootNoteNumberingUpperCaseRomanHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.SectionFootNotePlacementBelowTextHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.SectionFootNotePlacementPageBottomHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
end;

class procedure TdxDefaultDestination.SectionGutterHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.Position.SectionFormattingInfo.Margins.Gutter := AImporter.UnitConverter.TwipsToModelUnits(AParameterValue);
end;

class procedure TdxDefaultDestination.SectionHeaderOffsetHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  if not AHasParameter then
    AParameterValue := 720;
  AImporter.Position.SectionFormattingInfo.Margins.HeaderOffset := AImporter.UnitConverter.TwipsToModelUnits(AParameterValue);
end;

class procedure TdxDefaultDestination.SectionLandscapeHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.Position.SectionFormattingInfo.Page.Landscape := True;
end;

class procedure TdxDefaultDestination.SectionLeftMarginHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.Position.SectionFormattingInfo.Margins.Left := AImporter.UnitConverter.TwipsToModelUnits(AParameterValue);
end;

class procedure TdxDefaultDestination.SectionOtherPagePaperSourceHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.Position.SectionFormattingInfo.GeneralSectionInfo.OtherPagePaperSource := AParameterValue;
end;

class procedure TdxDefaultDestination.SectionPageNumberingContinuousHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.Position.SectionFormattingInfo.RestartPageNumbering := False;
end;

class procedure TdxDefaultDestination.SectionPageNumberingRestartHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.Position.SectionFormattingInfo.RestartPageNumbering := True;
end;

class procedure TdxDefaultDestination.SectionPageNumberingStartHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  if not AHasParameter then
    AParameterValue := 1;
  AImporter.Position.SectionFormattingInfo.PageNumbering.StartingPageNumber := AParameterValue;
end;

class procedure TdxDefaultDestination.SectionPaperHeightHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.Position.SectionFormattingInfo.Page.Height := AImporter.UnitConverter.TwipsToModelUnits(AParameterValue);
end;

class procedure TdxDefaultDestination.SectionPaperWidthHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.Position.SectionFormattingInfo.Page.Width := AImporter.UnitConverter.TwipsToModelUnits(AParameterValue);
end;

class procedure TdxDefaultDestination.SectionRightMarginHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.Position.SectionFormattingInfo.Margins.Right := AImporter.UnitConverter.TwipsToModelUnits(AParameterValue);
end;

class procedure TdxDefaultDestination.SectionTextFlowHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
var
  AFlow: TdxTextDirection;
begin
  if not AHasParameter then
    AParameterValue := 0;
  AFlow := TdxTextDirection(AParameterValue);
  AImporter.Position.SectionFormattingInfo.GeneralSectionInfo.TextDirection := AFlow;
end;

class procedure TdxDefaultDestination.SectionTopMarginHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.Position.SectionFormattingInfo.Margins.Top := AImporter.UnitConverter.TwipsToModelUnits(AParameterValue);
end;

class procedure TdxDefaultDestination.StyleSheetKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.Destination := TdxStyleSheetDestination.Create(AImporter);
end;

class procedure TdxDefaultDestination.TopMarginKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  if not AHasParameter then
    AParameterValue := 1440;
  AParameterValue := AImporter.UnitConverter.TwipsToModelUnits(AParameterValue);
  AImporter.DocumentProperties.DefaultSectionProperties.Margins.Top := AParameterValue;
  AImporter.Position.SectionFormattingInfo.Margins.Top := AParameterValue;
end;

class procedure TdxDefaultDestination.VerticalTextAlignmentBottomHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.VerticalTextAlignmentCenterHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.VerticalTextAlignmentJustifyHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

class procedure TdxDefaultDestination.VerticalTextAlignmentTopHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
Assert(False, 'not implemented in TdxDefaultDestination');
end;

end.
