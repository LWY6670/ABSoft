{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.DocumentModel.History.Style;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  dxRichEdit.DocumentModel.PieceTable, dxRichEdit.DocumentModel.Core, Generics.Collections,
  dxRichEdit.DocumentModel.Styles;

type
  { TdxChangeParagraphStyleIndexHistoryItem }

  TdxChangeParagraphStyleIndexHistoryItem = class(TdxRichEditHistoryItem)
  private
    FOldIndex: Integer;
    FNewIndex: Integer;
    FParagraphIndex: TdxParagraphIndex;
  protected
    procedure UndoCore; override;
    procedure RedoCore; override;
  public
    constructor Create(APieceTable: TdxPieceTable; AParagraphIndex: TdxParagraphIndex;
      AOldStyleIndex, ANewStyleIndex: Integer);

    property OldIndex: Integer read FOldIndex;
    property NewIndex: Integer read FNewIndex;
    property ParagraphIndex: TdxParagraphIndex read FParagraphIndex;
  end;

  { TdxParagraphStyleChangeAutoUpdatePropertyHistoryItem }

  TdxParagraphStyleChangeAutoUpdatePropertyHistoryItem = class(TdxRichEditHistoryItem)
  private
    FStyle: TdxParagraphStyle;
  protected
    procedure UndoCore; override;
    procedure RedoCore; override;
  public
    constructor Create(AStyle: TdxParagraphStyle);

    property Style: TdxParagraphStyle read FStyle;
  end;

  { TdxChangeParentStyleHistoryItem }

  TdxChangeParentStyleHistoryItem<T: TdxStyleBase> = class(TdxRichEditHistoryItem)
  private
    FStyle: TdxStyleBase;
    FOldParentStyle: T;
    FNewParentStyle: T;
  protected
    procedure UndoCore; override;
    procedure RedoCore; override;
  public
    constructor Create(AStyle: TdxStyleBase; AOldParentStyle, ANewParentStyle: T);
    property OldParentStyle: T read FOldParentStyle;
    property NewParentStyle: T read FNewParentStyle;
    property Style: TdxStyleBase read FStyle;
  end;

implementation

uses
  SysUtils, dxRichEdit.DocumentModel.Commands;

type
  TdxParagraphStyleAccess = class(TdxParagraphStyle);

{ TdxChangeParagraphStyleIndexHistoryItem }

constructor TdxChangeParagraphStyleIndexHistoryItem.Create(APieceTable: TdxPieceTable; AParagraphIndex: TdxParagraphIndex;
  AOldStyleIndex, ANewStyleIndex: Integer);
begin
  inherited Create(APieceTable);
  FOldIndex := AOldStyleIndex;
  FNewIndex := ANewStyleIndex;
  FParagraphIndex := AParagraphIndex;
end;

procedure TdxChangeParagraphStyleIndexHistoryItem.UndoCore;
begin
  PieceTable.Paragraphs[ParagraphIndex].SetParagraphStyleIndexCore(OldIndex);
end;

procedure TdxChangeParagraphStyleIndexHistoryItem.RedoCore;
begin
  PieceTable.Paragraphs[ParagraphIndex].SetParagraphStyleIndexCore(NewIndex);
end;

{ TdxParagraphStyleChangeAutoUpdatePropertyHistoryItem }

constructor TdxParagraphStyleChangeAutoUpdatePropertyHistoryItem.Create(AStyle: TdxParagraphStyle);
begin
  inherited Create(TdxDocumentModel(AStyle.DocumentModel).MainPieceTable);
  FStyle := AStyle;
end;

procedure TdxParagraphStyleChangeAutoUpdatePropertyHistoryItem.RedoCore;
begin
  TdxParagraphStyleAccess(Style).SetAutoUpdateCore(not Style.AutoUpdate);
end;

procedure TdxParagraphStyleChangeAutoUpdatePropertyHistoryItem.UndoCore;
begin
  TdxParagraphStyleAccess(Style).SetAutoUpdateCore(not Style.AutoUpdate);
end;

{ TdxChangeParentStyleHistoryItem<T> }

constructor TdxChangeParentStyleHistoryItem<T>.Create(AStyle: TdxStyleBase; AOldParentStyle, ANewParentStyle: T);
begin
  inherited Create(TdxDocumentModel(AStyle.DocumentModel).MainPieceTable);
  FStyle := AStyle;
  FOldParentStyle := AOldParentStyle;
  FNewParentStyle := ANewParentStyle;
end;

procedure TdxChangeParentStyleHistoryItem<T>.RedoCore;
begin
  Style.SetParentStyleCore(NewParentStyle);
end;

procedure TdxChangeParentStyleHistoryItem<T>.UndoCore;
begin
  Style.SetParentStyleCore(OldParentStyle);
end;

end.
