{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.DocumentLayout.Position;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  dxCoreClasses, cxClasses, dxRichEdit.Utils.Types, dxRichEdit.DocumentModel.PieceTable, Generics.Collections;

type

  { TdxDocumentLayoutPosition }

  TdxDocumentLayoutPosition = class(TcxIUnknownObject, IdxCloneable<TdxDocumentLayoutPosition>, IdxSupportsCopyFrom<TdxDocumentLayoutPosition>)
  type
    TdxIsIntersectedWithPrevBoxDelegate = reference to function (ABox: TdxBox): Boolean;
  strict private
    FDocumentLayout: TdxDocumentLayout; 
    FPieceTable: TdxPieceTable; 
    FLogPosition: TdxDocumentLogPosition;
    FDetailsLevel: TdxDocumentLayoutDetailsLevel; 
    FPage: TdxPage; 
    FFloatingObjectBoxPage: TdxPage; 
    FPageArea: TdxPageArea; 
    FColumn: TdxColumn; 
    FRow: TdxRow; 
    FBox: TdxBox; 
    FCharacter: TdxCharacterBox; 
    FTableRow: TdxTableRowViewInfoBase; 
    FTableCell: TdxTableCellViewInfo; 
    FLeftOffset: Integer; 
    FRightOffset: Integer; 
    function GetDocumentModel: TdxDocumentModel;
    procedure SetCharacter(const Value: TdxCharacterBox); 
  protected
    function GetPieceTable: TdxPieceTable; virtual;
    procedure EnsureFormattingComplete; virtual; 
    procedure EnsurePageSecondaryFormattingComplete(APage: TdxPage); virtual; 
    function UpdateCore(APages: TdxPageCollection; ADetailsLevel: TdxDocumentLayoutDetailsLevel): TdxDocumentLayoutDetailsLevel; virtual; 
    function UpdatePageRecursive(APages: TdxPageCollection; AStartIndex: Integer;
      ADetailsLevel: TdxDocumentLayoutDetailsLevel): TdxDocumentLayoutDetailsLevel; virtual; 
    function LookupPage(APages: TdxPageCollection; AStartIndex: Integer): TdxPage; virtual; 
    function UpdatePageAreaRecursive(APage: TdxPage; AStartIndex: Integer;
      ADetailsLevel: TdxDocumentLayoutDetailsLevel): TdxDocumentLayoutDetailsLevel; virtual; 
    procedure CheckPageAreaPieceTable(APageArea: TdxPageArea); virtual; 
    function LookupPageArea(APage: TdxPage; AStartIndex: Integer): TdxPageArea; virtual; 
    function UpdateColumnRecursive(AColumns: TdxColumnCollection; AStartIndex: Integer;
      ADetailsLevel: TdxDocumentLayoutDetailsLevel): TdxDocumentLayoutDetailsLevel; virtual; 
    function UpdateRowRecursive(ARows: TdxRowCollection; AStartIndex: Integer; ADetailsLevel: TdxDocumentLayoutDetailsLevel): TdxDocumentLayoutDetailsLevel; virtual; 
    function UpdateBoxRecursive(ARow: TdxRow; AStartIndex: Integer; ADetailsLevel: TdxDocumentLayoutDetailsLevel): TdxDocumentLayoutDetailsLevel; virtual; 
    function UpdateCharacterBoxRecursive(ARow: TdxRow; AStartIndex: Integer;
      ADetailsLevel: TdxDocumentLayoutDetailsLevel): TdxDocumentLayoutDetailsLevel; virtual; 
    function CreateEmptyClone: TdxDocumentLayoutPosition; virtual; 
    function GetPageIndex(APages: TdxPageCollection; AStartIndex, AEndIndex: Integer): Integer; overload; virtual; 
    function GetPageIndex(APages: TdxPageCollection): Integer; overload; virtual; 
    function GetPageAreaIndex(AAreas: TdxPageAreaCollection; AStartIndex, AEndIndex: Integer): Integer; overload; virtual; 
    function GetPageAreaIndex(AAreas: TdxPageAreaCollection): Integer; overload; virtual; 
    function GetColumnIndex(AColumns: TdxColumnCollection; AStartIndex, AEndIndex: Integer): Integer; overload; virtual; 
    function GetColumnIndex(AColumns: TdxColumnCollection): Integer; overload; virtual; 

    function FindBoxIndex(ACollection: TdxBoxCollectionBase; AExactComparable: TdxBoxComparable;
      AIsIntersect: TdxIsIntersectedWithPrevBoxDelegate;
      AStartIndex, AEndIndex: Integer): Integer; overload;
    function FindBoxIndex(ACollection: TdxBoxCollectionBase; AComparable, AExactComparable: TdxBoxComparable;
      AIsIntersect: TdxIsIntersectedWithPrevBoxDelegate;
      AStartIndex, AEndIndex: Integer): Integer; overload;

    function IsIntersectedWithPrevArea(AArea: TdxBox): Boolean;
    function IsIntersectedWithPrevColumn(AColumn: TdxBox): Boolean;
    function IsIntersectedWithPrevPage(APage: TdxBox): Boolean;

    function GetRowIndex(ARows: TdxRowCollection; AStartIndex, AEndIndex: Integer): Integer; overload; virtual; 
    function GetRowIndex(ARows: TdxRowCollection): Integer; overload; virtual; 
    function GetBoxIndex(ABoxes: TdxBoxCollection; AStartIndex, AEndIndex: Integer): Integer; overload; virtual; 
    function GetBoxIndex(ABoxes: TdxBoxCollection): Integer; overload; virtual; 
    function GetCharIndex(ACharacters: TdxCharacterBoxCollection; AStartIndex, AEndIndex: Integer): Integer; overload; virtual; 
    function GetCharIndex(ACharacters: TdxCharacterBoxCollection): Integer; overload; virtual; 
    function GetLogPosition: TdxDocumentLogPosition; virtual;
    function GetUseLastAvailablePosition: Boolean; virtual;
    procedure SetUseLastAvailablePosition(const Value: Boolean); virtual;

    property UseLastAvailablePosition: Boolean read GetUseLastAvailablePosition write SetUseLastAvailablePosition; 
  public
    constructor Create(ADocumentLayout: TdxDocumentLayout; APieceTable: TdxPieceTable; ALogPosition: TdxDocumentLogPosition); 
    destructor Destroy; override;

    procedure SetLogPosition(AValue: TdxDocumentLogPosition); 
    procedure IncreaseDetailsLevel(ADetailsLevel: TdxDocumentLayoutDetailsLevel); 
    procedure Invalidate; virtual; 
    function IsValid(ADetailsLevel: TdxDocumentLayoutDetailsLevel): Boolean; virtual; 
    function UpdateCellRecursive(AColumn: TdxColumn; ADetailsLevel: TdxDocumentLayoutDetailsLevel): TdxDocumentLayoutDetailsLevel; virtual; 
    function Update(APages: TdxPageCollection; ADetailsLevel: TdxDocumentLayoutDetailsLevel): Boolean; overload; virtual; 
    function Update(APages: TdxPageCollection; ADetailsLevel: TdxDocumentLayoutDetailsLevel;
      AUseLastAvailablePosition: Boolean): Boolean; overload; virtual; 

    //IdxCloneable
    function Clone: TdxDocumentLayoutPosition;
    //IdxSupportsCopyFrom
    procedure CopyFrom(const Source: TdxDocumentLayoutPosition);

    property DocumentLayout: TdxDocumentLayout read FDocumentLayout; 
    property DocumentModel: TdxDocumentModel read GetDocumentModel; 
    property PieceTable: TdxPieceTable read GetPieceTable; 
    property DetailsLevel: TdxDocumentLayoutDetailsLevel read FDetailsLevel; 
    property Page: TdxPage read FPage write FPage; 
    property FloatingObjectBoxPage: TdxPage read FFloatingObjectBoxPage write FFloatingObjectBoxPage; 
    property LeftOffset: Integer read FLeftOffset write FLeftOffset; 
    property RightOffset: Integer read FRightOffset write FRightOffset; 
    property LogPosition: TdxDocumentLogPosition read GetLogPosition; 
    property PageArea: TdxPageArea read FPageArea write FPageArea; 
    property Column: TdxColumn read FColumn write FColumn; 
    property Row: TdxRow read FRow write FRow; 
    property Box: TdxBox read FBox write FBox; 
    property Character: TdxCharacterBox read FCharacter write SetCharacter; 
    property TableRow: TdxTableRowViewInfoBase read FTableRow write FTableRow; 
    property TableCell: TdxTableCellViewInfo read FTableCell write FTableCell; 
  end;
  TdxDocumentLayoutPositionClass = class of TdxDocumentLayoutPosition;

  { TdxTextBoxDocumentLayoutPosition }

  TdxTextBoxDocumentLayoutPosition = class(TdxDocumentLayoutPosition)
  private
    FAnchorPieceTable: TdxPieceTable;
    FPreferredPageIndex: Integer;
    FUseLastAvailablePosition: Boolean;
  protected
    function CreateEmptyClone: TdxDocumentLayoutPosition; override;
    function GetPageIndex(APages: TdxPageCollection; AStartIndex, AEndIndex: Integer): Integer; override;
    function GetPieceTable: TdxPieceTable; override;
    procedure CheckPageAreaPieceTable(APageArea: TdxPageArea); override;
    function LookupPageArea(APage: TdxPage; AStartIndex: Integer): TdxPageArea; overload; override;
    function LookupPageArea(AFloatingObjects: TList<TdxFloatingObjectBox>; ARun: TdxFloatingObjectAnchorRun): TdxPageArea; reintroduce; overload; virtual;

    function GetUseLastAvailablePosition: Boolean; override;
    procedure SetUseLastAvailablePosition(const Value: Boolean); override;

    property AnchorPieceTable: TdxPieceTable read FAnchorPieceTable;
  public
    constructor Create(ADocumentLayout: TdxDocumentLayout; ATextBoxContentType: TdxTextBoxContentType;
      ALogPosition: TdxDocumentLogPosition; APreferredPageIndex: Integer);

    property PreferredPageIndex: Integer read FPreferredPageIndex write FPreferredPageIndex;
  end;

implementation

uses
  SysUtils, dxRichEdit.DocumentModel.Core;

{ TdxDocumentLayoutPosition }

procedure TdxDocumentLayoutPosition.CheckPageAreaPieceTable(APageArea: TdxPageArea);
begin
  Assert(APageArea.PieceTable = FPieceTable);
end;

constructor TdxDocumentLayoutPosition.Create(ADocumentLayout: TdxDocumentLayout; APieceTable: TdxPieceTable; ALogPosition: TdxDocumentLogPosition);
begin
  inherited Create;
  FDetailsLevel := TdxDocumentLayoutDetailsLevel.None;
  Assert(ADocumentLayout <> nil);
  Assert(APieceTable <> nil);
  Assert(ADocumentLayout.DocumentModel = APieceTable.DocumentModel);
  FDocumentLayout := ADocumentLayout;
  FPieceTable := APieceTable;
  FLogPosition := ALogPosition;
end;

destructor TdxDocumentLayoutPosition.Destroy;
begin
  FreeAndNil(FCharacter);
  inherited Destroy;
end;

function TdxDocumentLayoutPosition.CreateEmptyClone: TdxDocumentLayoutPosition;
begin
  Result := TdxDocumentLayoutPosition.Create(DocumentLayout, PieceTable, LogPosition);
end;

procedure TdxDocumentLayoutPosition.EnsureFormattingComplete;
begin
end;

procedure TdxDocumentLayoutPosition.EnsurePageSecondaryFormattingComplete(APage: TdxPage);
begin
end;

function TdxDocumentLayoutPosition.GetColumnIndex(AColumns: TdxColumnCollection; AStartIndex,
  AEndIndex: Integer): Integer;
var
  AComparable: TdxBoxComparable;
begin
  AComparable := TdxExactColumnAndLogPositionComparable.Create(PieceTable, LogPosition);
  try
    Result := FindBoxIndex(AColumns, AComparable, IsIntersectedWithPrevColumn, AStartIndex, AEndIndex);
  finally
    AComparable.Free;
  end;
end;

function TdxDocumentLayoutPosition.GetBoxIndex(ABoxes: TdxBoxCollection; AStartIndex, AEndIndex: Integer): Integer;
var
  AComparable: TdxBoxAndLogPositionComparable;
begin
  AComparable := TdxBoxAndLogPositionComparable.Create(PieceTable, LogPosition);
  try
    Result := ABoxes.BinarySearch(AComparable, AStartIndex, AEndIndex);
  finally
    AComparable.Free;
  end;
  if Result < 0 then
    Result := not Result;
end;

function TdxDocumentLayoutPosition.GetBoxIndex(ABoxes: TdxBoxCollection): Integer;
begin
  Result := GetBoxIndex(ABoxes, 0, ABoxes.Count - 1);
end;

function TdxDocumentLayoutPosition.GetCharIndex(ACharacters: TdxCharacterBoxCollection; AStartIndex,
  AEndIndex: Integer): Integer;
var
  AComparable: TdxBoxComparable;
begin
  AComparable := TdxBoxAndLogPositionComparable.Create(PieceTable, LogPosition);
  try
    Result := ACharacters.BinarySearch(AComparable, AStartIndex, AEndIndex);
  finally
    AComparable.Free;
  end;
  if Result < 0 then
    Result := not Result;
end;

function TdxDocumentLayoutPosition.GetCharIndex(ACharacters: TdxCharacterBoxCollection): Integer;
begin
  Result := GetCharIndex(ACharacters, 0, ACharacters.Count - 1);
end;

function TdxDocumentLayoutPosition.GetColumnIndex(AColumns: TdxColumnCollection): Integer;
begin
  Result := GetColumnIndex(AColumns, 0, AColumns.Count - 1);
end;

function TdxDocumentLayoutPosition.FindBoxIndex(ACollection: TdxBoxCollectionBase;
  AExactComparable: TdxBoxComparable;
  AIsIntersect: TdxIsIntersectedWithPrevBoxDelegate;
  AStartIndex, AEndIndex: Integer): Integer;
var
  AComparable: TdxBoxComparable;
begin
  AComparable := TdxBoxStartAndLogPositionComparable.Create(PieceTable, LogPosition);
  try
    Result := FindBoxIndex(ACollection, AExactComparable, AComparable, AIsIntersect, AStartIndex, AEndIndex)
  finally
    AComparable.Free;
  end;
end;

function TdxDocumentLayoutPosition.FindBoxIndex(ACollection: TdxBoxCollectionBase;
  AComparable, AExactComparable: TdxBoxComparable;
  AIsIntersect: TdxIsIntersectedWithPrevBoxDelegate;
  AStartIndex, AEndIndex: Integer): Integer;
var
  ABoxIndex: Integer;
  ALastBoxIndex: Integer;
  AStartBoxIndex: Integer;
begin
  Result := -1;
  ABoxIndex := ACollection.BinarySearch(AComparable, AStartIndex, AEndIndex);
  if ABoxIndex >= 0 then
    Result := ABoxIndex
  else
  begin
    ALastBoxIndex := not ABoxIndex - 1;
    if ALastBoxIndex < 0 then
    begin
      if ACollection.Count > 0 then
        Result := 0;
    end
    else
    begin
      AStartBoxIndex := ALastBoxIndex;
      while (AStartBoxIndex > 0) and AIsIntersect(ACollection[AStartBoxIndex]) do
        Dec(AStartBoxIndex);
			ABoxIndex := ACollection.BinarySearch(AExactComparable, AStartBoxIndex, ALastBoxIndex);
      if ABoxIndex >= 0 then
        Result := ABoxIndex
      else
      begin
        ABoxIndex := not ABoxIndex;
        if ABoxIndex < ACollection.Count then
          Result := ABoxIndex
        else
        begin
          if UseLastAvailablePosition then
            Result := ACollection.Count - 1;
        end;
      end;
    end;
  end;
end;

function TdxDocumentLayoutPosition.GetDocumentModel: TdxDocumentModel;
begin
  Result := DocumentLayout.DocumentModel;
end;

function TdxDocumentLayoutPosition.GetLogPosition: TdxDocumentLogPosition;
begin
  Result := FLogPosition;
end;

function TdxDocumentLayoutPosition.GetPageAreaIndex(AAreas: TdxPageAreaCollection; AStartIndex,
  AEndIndex: Integer): Integer;
var
  AComparable: TdxBoxComparable;
begin
  AComparable := TdxExactPageAreaAndLogPositionComparable.Create(PieceTable, LogPosition);
  try
    Result := FindBoxIndex(AAreas,
      AComparable, IsIntersectedWithPrevArea,
      AStartIndex, AEndIndex);
  finally
    AComparable.Free;
  end;
end;

function TdxDocumentLayoutPosition.GetPageAreaIndex(AAreas: TdxPageAreaCollection): Integer;
begin
  Result := GetPageAreaIndex(AAreas, 0, AAreas.Count - 1);
end;

function TdxDocumentLayoutPosition.GetPageIndex(APages: TdxPageCollection): Integer;
begin
  Result := GetPageIndex(APages, 0, APages.Count - 1);
end;

function TdxDocumentLayoutPosition.GetPieceTable: TdxPieceTable;
begin
  Result := FPieceTable;
end;

function TdxDocumentLayoutPosition.GetRowIndex(ARows: TdxRowCollection): Integer;
begin
  Result := GetRowIndex(ARows, 0, ARows.Count - 1);
end;

function TdxDocumentLayoutPosition.GetRowIndex(ARows: TdxRowCollection; AStartIndex, AEndIndex: Integer): Integer;
var
  AComparable: TdxBoxComparable;
begin
  AComparable := TdxBoxAndLogPositionComparable.Create(PieceTable, LogPosition);
  try
  Result := ARows.BinarySearch(AComparable, AStartIndex, AEndIndex);
  finally
    AComparable.Free;
  end;
  if Result < 0 then
    Result := not Result;
end;

function TdxDocumentLayoutPosition.GetPageIndex(APages: TdxPageCollection; AStartIndex, AEndIndex: Integer): Integer;
var
  AComparable: TdxBoxComparable;
begin
  AComparable := TdxExactPageAndLogPositionComparable.Create(PieceTable, LogPosition);
  try
    Result := FindBoxIndex(APages, AComparable, IsIntersectedWithPrevPage, AStartIndex, AEndIndex);
  finally
    AComparable.Free;
  end;
end;

function TdxDocumentLayoutPosition.GetUseLastAvailablePosition: Boolean;
begin
  Result := False;
end;

procedure TdxDocumentLayoutPosition.IncreaseDetailsLevel(ADetailsLevel: TdxDocumentLayoutDetailsLevel);
begin
  if FDetailsLevel < ADetailsLevel then
    FDetailsLevel := ADetailsLevel;
end;

procedure TdxDocumentLayoutPosition.Invalidate;
begin
  FDetailsLevel := TdxDocumentLayoutDetailsLevel.None;
end;

function TdxDocumentLayoutPosition.IsIntersectedWithPrevArea(AArea: TdxBox): Boolean;
begin
  Assert(AArea is TdxPageArea);
  Result := IsIntersectedWithPrevColumn(TdxPageArea(AArea).Columns.First);
end;

function TdxDocumentLayoutPosition.IsIntersectedWithPrevColumn(AColumn: TdxBox): Boolean;
begin
  Assert(AColumn is TdxColumn);
  Result := TdxColumn(AColumn).IsIntersectedWithPrevColumn;
end;

function TdxDocumentLayoutPosition.IsIntersectedWithPrevPage(APage: TdxBox): Boolean;
begin
  Assert(APage is TdxPage);
  Result := IsIntersectedWithPrevArea(TdxPage(APage).Areas.First);
end;

function TdxDocumentLayoutPosition.IsValid(ADetailsLevel: TdxDocumentLayoutDetailsLevel): Boolean;
begin
  Result := ADetailsLevel <= DetailsLevel;
end;

function TdxDocumentLayoutPosition.LookupPage(APages: TdxPageCollection; AStartIndex: Integer): TdxPage;
var
  APageIndex: Integer;
begin
  Result :=  nil;
  APageIndex := GetPageIndex(APages, AStartIndex, APages.Count - 1);
  if APageIndex >= 0 then
    Result := APages[APageIndex]
end;

function TdxDocumentLayoutPosition.LookupPageArea(APage: TdxPage; AStartIndex: Integer): TdxPageArea;
var
  Areas: TdxPageAreaCollection;
  APageAreaIndex: Integer;
begin
  if (APage.Header <> nil) and (APage.Header.PieceTable = PieceTable) then
    Result := APage.Header
  else
  begin
    if (APage.Footer <> nil) and (APage.Footer.PieceTable = PieceTable) then
      Result := APage.Footer
    else
    begin
      Areas := APage.Areas;
      APageAreaIndex := GetPageAreaIndex(Areas, AStartIndex, Areas.Count - 1);
      if APageAreaIndex >= 0 then
        Result := Areas[APageAreaIndex]
      else
        Result := nil;
    end;
  end;
end;

procedure TdxDocumentLayoutPosition.SetCharacter(const Value: TdxCharacterBox);
begin
  if Character <> Value then
  begin
    FCharacter.Free;
    if Value <> nil then
      FCharacter := Value.Clone
    else
      FCharacter := nil;
  end;
end;

procedure TdxDocumentLayoutPosition.SetLogPosition(AValue: TdxDocumentLogPosition);
begin
  FLogPosition := AValue;
end;

procedure TdxDocumentLayoutPosition.SetUseLastAvailablePosition(const Value: Boolean);
begin
end;

function TdxDocumentLayoutPosition.Update(APages: TdxPageCollection; ADetailsLevel: TdxDocumentLayoutDetailsLevel;
  AUseLastAvailablePosition: Boolean): Boolean;
begin
  UseLastAvailablePosition := AUseLastAvailablePosition;
  if not IsValid(ADetailsLevel) then
  begin
    FDetailsLevel := UpdateCore(APages, ADetailsLevel);
    Result := IsValid(ADetailsLevel);
  end
  else
    Result := True;
end;

function TdxDocumentLayoutPosition.Update(APages: TdxPageCollection;
  ADetailsLevel: TdxDocumentLayoutDetailsLevel): Boolean;
begin
  Result := Update(APages, ADetailsLevel, False);
end;

function TdxDocumentLayoutPosition.UpdateBoxRecursive(ARow: TdxRow; AStartIndex: Integer;
  ADetailsLevel: TdxDocumentLayoutDetailsLevel): TdxDocumentLayoutDetailsLevel;
var
  ABoxIndex: Integer;
  ABoxes: TdxBoxCollection;
begin
  if not IsValid(TdxDocumentLayoutDetailsLevel.Box) then
  begin
    ABoxes := ARow.Boxes;
    ABoxIndex := GetBoxIndex(ABoxes, AStartIndex, ABoxes.Count - 1);
    if ABoxIndex >= ABoxes.Count then
    begin
      if UseLastAvailablePosition then
        ABoxIndex := ABoxes.Count - 1
      else
        Exit(TdxDocumentLayoutDetailsLevel.Row);
    end;
    FBox := ABoxes[ABoxIndex];
  end;
  if ADetailsLevel <= TdxDocumentLayoutDetailsLevel.Box then
    Result := TdxDocumentLayoutDetailsLevel.Box
  else
    Result := UpdateCharacterBoxRecursive(ARow, 0, ADetailsLevel);
end;

function TdxDocumentLayoutPosition.UpdateCellRecursive(AColumn: TdxColumn;
  ADetailsLevel: TdxDocumentLayoutDetailsLevel): TdxDocumentLayoutDetailsLevel;
var
  ARows: TdxRowCollection;
  AColumnTables: TdxTableViewInfoCollection;
begin
  if not IsValid(TdxDocumentLayoutDetailsLevel.TableCell) then
  begin
    AColumnTables := AColumn.InnerTables;
    if AColumnTables <> nil then
    begin
      Assert(False, 'not implemented');
    end
    else
      FTableCell := nil;
  end;
  if ADetailsLevel <= TdxDocumentLayoutDetailsLevel.TableCell then
    Exit(TdxDocumentLayoutDetailsLevel.TableCell);
  if TableCell <> nil then
    ARows := TableCell.GetRows(AColumn)
  else
    ARows := AColumn.Rows;
  Result := UpdateRowRecursive(ARows, 0, ADetailsLevel);
end;

function TdxDocumentLayoutPosition.Clone: TdxDocumentLayoutPosition;
begin
  Result := TdxDocumentLayoutPositionClass(ClassType).Create(DocumentLayout, PieceTable, LogPosition);
  Result.CopyFrom(Self);
end;

procedure TdxDocumentLayoutPosition.CopyFrom(const Source: TdxDocumentLayoutPosition);
begin
  FDetailsLevel := Source.DetailsLevel;
  FLogPosition := Source.LogPosition;
  Page := Source.Page;
  PageArea := Source.PageArea;
  Column := Source.Column;
  Row := Source.Row;
  Box := Source.Box;
  Character := Source.Character;
  TableRow := Source.TableRow;
  TableCell := Source.TableCell;
  LeftOffset := Source.LeftOffset;
  RightOffset := Source.RightOffset;
end;

function TdxDocumentLayoutPosition.UpdateCharacterBoxRecursive(ARow: TdxRow; AStartIndex: Integer;
  ADetailsLevel: TdxDocumentLayoutDetailsLevel): TdxDocumentLayoutDetailsLevel;
var
  ACharIndex: Integer;
  ADetailRow: TdxDetailRow;
  ACharacters: TdxCharacterBoxCollection;
begin
  if not IsValid(TdxDocumentLayoutDetailsLevel.Character) then
  begin
    ADetailRow := DocumentLayout.CreateDetailRowForBox(ARow, Box);
    try
      ACharacters := ADetailRow.Characters;
      ACharIndex := GetCharIndex(ACharacters, AStartIndex, ACharacters.Count - 1);
      if ACharIndex >= ADetailRow.Characters.Count then
      begin
        if UseLastAvailablePosition then
          ACharIndex := ADetailRow.Characters.Count - 1
        else
          Exit(TdxDocumentLayoutDetailsLevel.Box);
      end;
      Character := ADetailRow.Characters[ACharIndex];
    finally
      ADetailRow.Free;
    end;
  end;
  Result := TdxDocumentLayoutDetailsLevel.Character;
end;

function TdxDocumentLayoutPosition.UpdateColumnRecursive(AColumns: TdxColumnCollection; AStartIndex: Integer;
  ADetailsLevel: TdxDocumentLayoutDetailsLevel): TdxDocumentLayoutDetailsLevel;
var
  AColumnIndex: Integer;
begin
  if not IsValid(TdxDocumentLayoutDetailsLevel.Column) then
  begin
    AColumnIndex := GetColumnIndex(AColumns, AStartIndex, AColumns.Count - 1);
    if AColumnIndex < 0 then
      Exit(TdxDocumentLayoutDetailsLevel.PageArea);
    FColumn := AColumns[AColumnIndex];
  end;
  if ADetailsLevel <= TdxDocumentLayoutDetailsLevel.Column then
    Result := TdxDocumentLayoutDetailsLevel.Column
  else
    Result := UpdateCellRecursive(FColumn, ADetailsLevel);
end;

function TdxDocumentLayoutPosition.UpdateCore(APages: TdxPageCollection;
  ADetailsLevel: TdxDocumentLayoutDetailsLevel): TdxDocumentLayoutDetailsLevel;
begin
  if ADetailsLevel < TdxDocumentLayoutDetailsLevel.Page then
    Exit(TdxDocumentLayoutDetailsLevel.None);
  EnsureFormattingComplete;
  Result := UpdatePageRecursive(APages, 0, ADetailsLevel);
end;

function TdxDocumentLayoutPosition.UpdatePageAreaRecursive(APage: TdxPage; AStartIndex: Integer;
  ADetailsLevel: TdxDocumentLayoutDetailsLevel): TdxDocumentLayoutDetailsLevel;
begin
  if not IsValid(TdxDocumentLayoutDetailsLevel.PageArea) then
  begin
    FPageArea := LookupPageArea(APage, AStartIndex);
    if FPageArea = nil then
      Exit(TdxDocumentLayoutDetailsLevel.Page);
    CheckPageAreaPieceTable(FPageArea);
  end;
  if ADetailsLevel <= TdxDocumentLayoutDetailsLevel.PageArea then
    Result := TdxDocumentLayoutDetailsLevel.PageArea
  else
    Result := UpdateColumnRecursive(FPageArea.Columns, 0, ADetailsLevel);
end;

function TdxDocumentLayoutPosition.UpdatePageRecursive(APages: TdxPageCollection; AStartIndex: Integer;
  ADetailsLevel: TdxDocumentLayoutDetailsLevel): TdxDocumentLayoutDetailsLevel;
var
  APage: TdxPage;
begin
  if not IsValid(TdxDocumentLayoutDetailsLevel.Page) then
  begin
    APage := LookupPage(APages, AStartIndex);
    if APage = nil then
      Exit(TdxDocumentLayoutDetailsLevel.None)
    else
      FPage := APage;
  end;
  if ADetailsLevel <= TdxDocumentLayoutDetailsLevel.Page then
    Result := TdxDocumentLayoutDetailsLevel.Page
  else
    Result := UpdatePageAreaRecursive(FPage, 0, ADetailsLevel);
end;

function TdxDocumentLayoutPosition.UpdateRowRecursive(ARows: TdxRowCollection; AStartIndex: Integer;
  ADetailsLevel: TdxDocumentLayoutDetailsLevel): TdxDocumentLayoutDetailsLevel;
var
  ARowIndex: Integer;
begin
  if not IsValid(TdxDocumentLayoutDetailsLevel.Row) then
  begin
    EnsurePageSecondaryFormattingComplete(Page);
    ARowIndex := GetRowIndex(ARows, AStartIndex, ARows.Count - 1);
    if ARowIndex >= ARows.Count then
    begin
      if UseLastAvailablePosition then
        ARowIndex := ARows.Count - 1
      else
        Exit(TdxDocumentLayoutDetailsLevel.Column);
    end;
    if ARowIndex < 0 then
      Exit(TdxDocumentLayoutDetailsLevel.Column);
    FRow := ARows[ARowIndex];
    Assert(FRow.Paragraph.PieceTable = PieceTable);
  end;
  if ADetailsLevel <= TdxDocumentLayoutDetailsLevel.Row then
    Result := TdxDocumentLayoutDetailsLevel.Row
  else
    Result := UpdateBoxRecursive(FRow, 0, ADetailsLevel);
end;

{ TdxTextBoxDocumentLayoutPosition }

procedure TdxTextBoxDocumentLayoutPosition.CheckPageAreaPieceTable(APageArea: TdxPageArea);
begin
end;

constructor TdxTextBoxDocumentLayoutPosition.Create(ADocumentLayout: TdxDocumentLayout;
  ATextBoxContentType: TdxTextBoxContentType; ALogPosition: TdxDocumentLogPosition; APreferredPageIndex: Integer);
begin
  inherited Create(ADocumentLayout, ATextBoxContentType.PieceTable, ALogPosition);
  FPreferredPageIndex := APreferredPageIndex;
end;

function TdxTextBoxDocumentLayoutPosition.CreateEmptyClone: TdxDocumentLayoutPosition;
begin
  Result := TdxTextBoxDocumentLayoutPosition.Create(DocumentLayout, TdxTextBoxContentType(PieceTable.ContentType),
    LogPosition, FPreferredPageIndex);
end;

function TdxTextBoxDocumentLayoutPosition.GetPageIndex(APages: TdxPageCollection; AStartIndex,
  AEndIndex: Integer): Integer;
var
  APieceTable: TdxPieceTable;
  AAnchorLogPosition: TdxDocumentLogPosition;
  ATextBoxPieceTable: TdxTextBoxContentType;
  AExactPageAndLogPositionComparable: TdxExactPageAndLogPositionComparable;
  ABoxStartAndLogPositionComparable: TdxBoxStartAndLogPositionComparable;
begin
  if LogPosition > PieceTable.DocumentEndLogPosition then
    Exit(-1);

  ATextBoxPieceTable := TdxTextBoxContentType(PieceTable.ContentType);
  APieceTable := ATextBoxPieceTable.AnchorRun.PieceTable;
  if APieceTable.IsHeaderFooter then
    Exit(FPreferredPageIndex);

  FAnchorPieceTable := APieceTable;
  try
    AAnchorLogPosition := FAnchorPieceTable.GetRunLogPosition(ATextBoxPieceTable.AnchorRun);
    ABoxStartAndLogPositionComparable := TdxBoxStartAndLogPositionComparable.Create(FAnchorPieceTable, AAnchorLogPosition);
    try
      AExactPageAndLogPositionComparable := TdxExactPageAndLogPositionComparable.Create(FAnchorPieceTable, AAnchorLogPosition);
      try
        Result := FindBoxIndex(APages, ABoxStartAndLogPositionComparable, AExactPageAndLogPositionComparable,
          function (ABox: TdxBox): Boolean 
          begin
            Result := TdxPage(ABox).Areas.First.Columns.First.IsIntersectedWithPrevColumn;
          end, AStartIndex, AEndIndex);
      finally
        AExactPageAndLogPositionComparable.Free;
      end;
    finally
      ABoxStartAndLogPositionComparable.Free;
    end;
  finally
    FAnchorPieceTable := nil;
  end;
end;

function TdxTextBoxDocumentLayoutPosition.GetPieceTable: TdxPieceTable;
begin
  if FAnchorPieceTable <> nil then
    Result :=  FAnchorPieceTable
  else
    Result :=  inherited GetPieceTable;
end;

function TdxTextBoxDocumentLayoutPosition.GetUseLastAvailablePosition: Boolean;
begin
  Result := FUseLastAvailablePosition;
end;

function TdxTextBoxDocumentLayoutPosition.LookupPageArea(AFloatingObjects: TList<TdxFloatingObjectBox>;
  ARun: TdxFloatingObjectAnchorRun): TdxPageArea;
var
  I: Integer;
begin
  if AFloatingObjects = nil then
    Exit(nil);
  for I := 0 to AFloatingObjects.Count - 1 do
    if AFloatingObjects[I].GetFloatingObjectRun = ARun then
      Exit(AFloatingObjects[I].DocumentLayout.Pages.First.Areas.First);
  Result := nil;
end;

procedure TdxTextBoxDocumentLayoutPosition.SetUseLastAvailablePosition(const Value: Boolean);
begin
  FUseLastAvailablePosition := Value;
end;

function TdxTextBoxDocumentLayoutPosition.LookupPageArea(APage: TdxPage; AStartIndex: Integer): TdxPageArea;
var
  AAnchorRun: TdxFloatingObjectAnchorRun;
  ATextBoxPieceTable: TdxTextBoxContentType;
begin
  ATextBoxPieceTable := TdxTextBoxContentType(PieceTable.ContentType);
  AAnchorRun := ATextBoxPieceTable.AnchorRun;

  Result := LookupPageArea(APage.InnerFloatingObjects, AAnchorRun);
  if Result <> nil then
    Exit;
  Result := LookupPageArea(APage.InnerForegroundFloatingObjects, AAnchorRun);
  if Result <> nil then
    Exit;
  Result := LookupPageArea(APage.InnerBackgroundFloatingObjects, AAnchorRun);
end;

end.
