{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Control.Keyboard;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Types, SysUtils, Classes, Generics.Collections, Controls,
  dxCoreClasses, dxRichEdit.DocumentModel.PieceTable, dxRichEdit.View.Core, dxRichEdit.Commands.IDs,
  dxRichEdit.Commands, dxRichEdit.Commands.Insert;

type
  TdxRichEditKeyboardController = class;

  TdxRichEditKeyboardCustomHandler = class(TInterfacedObject, IdxKeyboardHandlerService)
  strict private
    FCommandTable: TDictionary<TdxRichEditCommandId, TdxRichEditCommandClass>;
    FController: TdxRichEditKeyboardController;
    FKeyHandlerIdTable: TDictionary<TShortCut, TdxRichEditCommandId>;
  private
    function GetControl: IdxRichEditControl;
    function IsValidChar(const AChar: Char): Boolean;
  protected
    procedure PopulateCommandTable; virtual;

    function ArgsToShortCut(const Args: TdxKeyEventArgs): TShortCut;
    function CharToShortCut(AValue: Char; const AShiftState: TShiftState): TShortCut;
    function KeysToShortCut(AKey: Word; const AShiftState: TShiftState): TShortCut;

    procedure RegisterCommand(ACommandClass: TdxRichEditCommandClass);
    procedure RegisterKeyCommand(AKey: Word; const AShiftState: TShiftState; ACommandClass: TdxRichEditCommandClass); overload;
    procedure RegisterKeyCommand(AKey: Char; const AShiftState: TShiftState; ACommandClass: TdxRichEditCommandClass); overload;
    procedure RegisterKeyCommand(AShortCut: TShortCut; ACommandClass: TdxRichEditCommandClass); overload;
    procedure UnregisterCommand(ACommandClass: TdxRichEditCommandClass);
    procedure UnregisterKeyCommand(AKey: Word; const AShiftState: TShiftState); overload;
    procedure UnregisterKeyCommand(AShortCut: TShortCut); overload;

    function CreateHandlerCommand(AHandlerId: TdxRichEditCommandId): TdxRichEditCommand; virtual;
    function ExecuteCommand(ACommand: TdxRichEditCommand; AShortCut: TShortCut): Boolean; virtual;
    procedure ExecuteCommandCore(ACommand: TdxRichEditCommand; AShortCut: TShortCut);
    function GetKeyHandler(const Args: TdxKeyEventArgs): TdxRichEditCommand; overload; virtual;
    function GetKeyHandler(AChar: Char; const AShiftState: TShiftState): TdxRichEditCommand; overload;
    function GetKeyHandlerCore(AShortCut: TShortCut): TdxRichEditCommand; virtual;
    function GetKeyHandlerId(AShortCut: TShortCut): TdxRichEditCommandId; virtual;

    function HandleKey(const Args: TdxKeyEventArgs): Boolean; virtual;
    function HandleKeyPress(AChar: Char; const AShiftState: TShiftState): Boolean; overload; virtual;

    property CommandTable: TDictionary<TdxRichEditCommandId, TdxRichEditCommandClass> read FCommandTable;
    property Control: IdxRichEditControl read GetControl;
    property KeyHandlerIdTable: TDictionary<TShortCut, TdxRichEditCommandId> read FKeyHandlerIdTable;
  public
    constructor Create(AController: TdxRichEditKeyboardController);
    destructor Destroy; override;

    function HandleKeyDown(const Args: TdxKeyEventArgs): Boolean;
    function HandleKeyUp(const Args: TdxKeyEventArgs): Boolean;
    function HandleKeyPress(const Args: TdxKeyPressEventArgs): Boolean; overload;

    property Controller: TdxRichEditKeyboardController read FController;
  end;

  { TdxRichEditKeyboardDefaultHandler }

  TdxRichEditKeyboardDefaultHandler = class(TdxRichEditKeyboardCustomHandler)
  private
    FText: string;
    function CreateFlushPendingTextCommand: TdxInsertTextCommand;
    procedure FlushPendingTextInput;
    procedure PerformFlushPendingTextInput(const S: string);
    procedure FlushPendingTextInputCore(const S: string);
    procedure TryChangeCursor;
  protected
    function GetKeyHandler(const Args: TdxKeyEventArgs): TdxRichEditCommand; override;
    function HandleKey(const Args: TdxKeyEventArgs): Boolean; override;
    function HandleKeyPress(AChar: Char; const AShiftState: TShiftState): Boolean; override;
    procedure PopulateCommandTable; override;
  end;

  { TdxRichEditKeyboardController }

  TdxRichEditKeyboardController = class(TdxRichEditKeyboardCustomController)
  strict private
    FRichControl: IdxRichEditControl;
    FDefaultHandler: IdxKeyboardHandlerService;
  private
    function GetActiveView: TdxRichEditView;
    function GetDocumentModel: TdxDocumentModel;
    function GetInnerControl: IdxInnerControl;
  protected
    function CreateDefaultHandler: IdxKeyboardHandlerService; virtual;

    property ActiveView: TdxRichEditView read GetActiveView;
    property DocumentModel: TdxDocumentModel read GetDocumentModel;
    property InnerControl: IdxInnerControl read GetInnerControl;
    property RichControl: IdxRichEditControl read FRichControl;
  public
    constructor Create(ARichControl: IdxRichEditControl); reintroduce; virtual;
    destructor Destroy; override;
  end;

implementation

uses
  Windows, dxCore,
  dxRichEdit.Control.Cursors, dxRichEdit.Commands.Keyboard, dxRichEdit.Platform.Font,
  dxRichEdit.Commands.Delete, dxRichEdit.Commands.Selection,
  dxRichEdit.Commands.CopyAndPaste, dxRichEdit.Commands.ChangeProperties,
  dxRichEdit.Commands.Save, dxRichEdit.Commands.Numbering, 
  dxRichEdit.Commands.Tab;

{ TdxRichEditKeyboardDefaultHandler }

function TdxRichEditKeyboardCustomHandler.ArgsToShortCut(const Args: TdxKeyEventArgs): TShortCut;
begin
  Result := KeysToShortCut(Args.KeyData, Args.ShiftState);
end;

function TdxRichEditKeyboardCustomHandler.CharToShortCut(AValue: Char;
  const AShiftState: TShiftState): TShortCut;
begin
  Result := KeysToShortCut(Ord(UpCase(AValue)), AShiftState);
end;

constructor TdxRichEditKeyboardCustomHandler.Create(
  AController: TdxRichEditKeyboardController);
begin
  inherited Create;
  FController := AController;
  FCommandTable := TDictionary<TdxRichEditCommandId, TdxRichEditCommandClass>.Create;
  FKeyHandlerIdTable := TDictionary<TShortCut, TdxRichEditCommandId>.Create;
  PopulateCommandTable;
end;

destructor TdxRichEditKeyboardCustomHandler.Destroy;
begin
  FreeAndNil(FKeyHandlerIdTable);
  FreeAndNil(FCommandTable);
  inherited;
end;

function TdxRichEditKeyboardCustomHandler.ExecuteCommand(
  ACommand: TdxRichEditCommand; AShortCut: TShortCut): Boolean;
begin
  Result := ACommand <> nil;
  if Result then
    ExecuteCommandCore(ACommand, AShortCut);
end;

procedure TdxRichEditKeyboardCustomHandler.ExecuteCommandCore(
  ACommand: TdxRichEditCommand; AShortCut: TShortCut);
begin
  ACommand.CommandSourceType := TdxCommandSourceType.Keyboard;
  ACommand.Execute;
end;

function TdxRichEditKeyboardCustomHandler.HandleKeyDown(
  const Args: TdxKeyEventArgs): Boolean;
begin
  Result := HandleKey(Args);
end;

function TdxRichEditKeyboardCustomHandler.HandleKeyPress(AChar: Char;
  const AShiftState: TShiftState): Boolean;
var
  ACommand: TdxRichEditCommand;
begin
  Result := False;
  ACommand := GetKeyHandler(AChar, AShiftState);
  if ACommand <> nil then
  try
    Result := ExecuteCommand(ACommand, CharToShortCut(AChar, AShiftState));
  finally
    ACommand.Free;
  end;
end;

function TdxRichEditKeyboardCustomHandler.HandleKeyPress(
  const Args: TdxKeyPressEventArgs): Boolean;
begin
  Result := IsValidChar(Args.Key) and HandleKeyPress(Args.Key, Args.Shift);
end;

Function TdxRichEditKeyboardCustomHandler.HandleKeyUp(
  const Args: TdxKeyEventArgs): Boolean;
begin
  Result := False;
end;

procedure TdxRichEditKeyboardCustomHandler.PopulateCommandTable;
begin
end;

procedure TdxRichEditKeyboardCustomHandler.RegisterCommand(ACommandClass: TdxRichEditCommandClass);
begin
  CommandTable.AddOrSetValue(ACommandClass.Id, ACommandClass);
end;

procedure TdxRichEditKeyboardCustomHandler.RegisterKeyCommand(
  AShortCut: TShortCut; ACommandClass: TdxRichEditCommandClass);
begin
  KeyHandlerIdTable.Add(AShortCut, ACommandClass.Id);
  RegisterCommand(ACommandClass);
end;

procedure TdxRichEditKeyboardCustomHandler.UnregisterCommand(ACommandClass: TdxRichEditCommandClass);
begin
  CommandTable.Remove(ACommandClass.Id);
end;

procedure TdxRichEditKeyboardCustomHandler.RegisterKeyCommand(AKey: Word;
  const AShiftState: TShiftState; ACommandClass: TdxRichEditCommandClass);
begin
  RegisterKeyCommand(KeysToShortCut(AKey, AShiftState), ACommandClass);
end;

procedure TdxRichEditKeyboardCustomHandler.RegisterKeyCommand(AKey: Char;
  const AShiftState: TShiftState; ACommandClass: TdxRichEditCommandClass);
begin
  RegisterKeyCommand(CharToShortCut(AKey, AShiftState), ACommandClass);
end;

procedure TdxRichEditKeyboardCustomHandler.UnregisterKeyCommand(AKey: Word;
  const AShiftState: TShiftState);
begin
  UnregisterKeyCommand(KeysToShortCut(AKey, AShiftState));
end;

function TdxRichEditKeyboardCustomHandler.CreateHandlerCommand(AHandlerId: TdxRichEditCommandId): TdxRichEditCommand;
begin
  Assert(AHandlerId <> TdxRichEditCommandId.None);
  Result := CommandTable[AHandlerId].Create(Controller.RichControl);
end;

function TdxRichEditKeyboardCustomHandler.GetControl: IdxRichEditControl;
begin
  Result := Controller.RichControl;
end;

function TdxRichEditKeyboardCustomHandler.IsValidChar(const AChar: Char): Boolean;
begin
  Result := Ord(AChar) >= 32;
end;

function TdxRichEditKeyboardCustomHandler.KeysToShortCut(AKey: Word; const AShiftState: TShiftState): TShortCut;
begin
  if HiByte(AKey) <> 0 then
    Result := 0
  else
  begin
    Result := AKey;
    if ssShift in AShiftState then
      Inc(Result, scShift);
    if ssCtrl in AShiftState then
      Inc(Result, scCtrl);
    if ssAlt in AShiftState then
      Inc(Result, scAlt);
  end;
end;

function TdxRichEditKeyboardCustomHandler.GetKeyHandler(const Args: TdxKeyEventArgs): TdxRichEditCommand;
begin
  Result := GetKeyHandlerCore(ArgsToShortCut(Args));
end;

function TdxRichEditKeyboardCustomHandler.GetKeyHandler(AChar: Char;
  const AShiftState: TShiftState): TdxRichEditCommand;
begin
  if dxCharInSet(AChar, ['A'..'Z']) then
    Result := GetKeyHandlerCore(CharToShortCut(AChar, AShiftState))
  else
    Result := nil;
end;

function TdxRichEditKeyboardCustomHandler.GetKeyHandlerCore(AShortCut: TShortCut): TdxRichEditCommand;
var
  AHandlerId: TdxRichEditCommandId;
begin
  AHandlerId := GetKeyHandlerId(AShortCut);
  if AHandlerId <> TdxRichEditCommandId.None then
    Result := CreateHandlerCommand(AHandlerId)
  else
    Result := nil;
end;

function TdxRichEditKeyboardCustomHandler.GetKeyHandlerId(AShortCut: TShortCut): TdxRichEditCommandId;
begin
  Result := TdxRichEditCommandId.None;
  if KeyHandlerIdTable.ContainsKey(AShortCut) then
    Result := KeyHandlerIdTable[AShortCut];
end;

function TdxRichEditKeyboardCustomHandler.HandleKey(const Args: TdxKeyEventArgs): Boolean;
var
  ACommand: TdxRichEditCommand;
begin
  Result := False;
  ACommand := GetKeyHandler(Args);
  if ACommand <> nil then
  try
    Result := ExecuteCommand(ACommand, ArgsToShortCut(Args));
  finally
    ACommand.Free;
  end;
end;

{ TdxRichEditKeyboardDefaultHandler }

procedure TdxRichEditKeyboardDefaultHandler.FlushPendingTextInput;
var
  APendingInput: string;
begin
  if FText = '' then
    Exit;
  APendingInput := FText;
  FText := '';
  PerformFlushPendingTextInput(APendingInput);
end;

procedure TdxRichEditKeyboardDefaultHandler.FlushPendingTextInputCore(
  const S: string);
var
  ACommand: TdxInsertTextCommand;
begin
  ACommand := CreateFlushPendingTextCommand;
  try
    ACommand.Text := S;
    ACommand.CommandSourceType := TdxCommandSourceType.Keyboard;
    ACommand.Execute;
  finally
    ACommand.Free;
  end;
end;

function TdxRichEditKeyboardDefaultHandler.CreateFlushPendingTextCommand: TdxInsertTextCommand;
begin
  if Control.Overtype then
    Result := TdxOvertypeTextCommand.Create(Control)
  else
    Result := TdxInsertTextCommand.Create(Control);
end;


function TdxRichEditKeyboardDefaultHandler.GetKeyHandler(
  const Args: TdxKeyEventArgs): TdxRichEditCommand;
begin
  Result := inherited GetKeyHandler(Args);
  if Result <> nil then
    FlushPendingTextInput;
end;

function TdxRichEditKeyboardDefaultHandler.HandleKey(const Args: TdxKeyEventArgs): Boolean;
begin
  Result := inherited HandleKey(Args);
  if Result then
    Exit;
  TryChangeCursor;
  Result := False;
end;

function TdxRichEditKeyboardDefaultHandler.HandleKeyPress(AChar: Char;
  const AShiftState: TShiftState): Boolean;
begin
  Result := inherited HandleKeyPress(AChar, AShiftState);
  if not Result then
  begin
    FText := FText + AChar;
    FlushPendingTextInput;
    Result := True;
  end;
end;

procedure TdxRichEditKeyboardDefaultHandler.PerformFlushPendingTextInput(
  const S: string);
begin
  if Length(S) = 1 then
    FlushPendingTextInputCore(S)
  else
  begin
    Assert(False);
  end;
end;

procedure TdxRichEditKeyboardDefaultHandler.PopulateCommandTable;
begin
  inherited PopulateCommandTable;
  RegisterKeyCommand('8', [ssShift, ssCtrl], TdxToggleShowWhitespaceCommand);
  RegisterKeyCommand(VK_LEFT, [], TdxPreviousCharacterCommand);
  RegisterKeyCommand(VK_RIGHT, [], TdxNextCharacterCommand);
  RegisterKeyCommand(VK_LEFT, [ssShift], TdxExtendPreviousCharacterCommand);
  RegisterKeyCommand(VK_RIGHT, [ssShift], TdxExtendNextCharacterCommand);
  RegisterKeyCommand(VK_LEFT, [ssCtrl], TdxPreviousWordCommand);
  RegisterKeyCommand(VK_RIGHT, [ssCtrl], TdxNextWordCommand);
  RegisterKeyCommand(VK_LEFT, [ssCtrl, ssShift], TdxExtendPreviousWordCommand);
  RegisterKeyCommand(VK_RIGHT, [ssShift, ssCtrl], TdxExtendNextWordCommand);
  RegisterKeyCommand(VK_HOME, [], TdxStartOfLineCommand);
  RegisterKeyCommand(VK_END, [], TdxEndOfLineCommand);
  RegisterKeyCommand(VK_HOME, [ssShift], TdxExtendStartOfLineCommand);
  RegisterKeyCommand(VK_END, [ssShift], TdxExtendEndOfLineCommand);
  RegisterKeyCommand(VK_UP, [], TdxPreviousLineCommand);
  RegisterKeyCommand(VK_DOWN, [], TdxNextLineCommand);
  RegisterKeyCommand(VK_UP, [ssShift], TdxExtendPreviousLineCommand);
  RegisterKeyCommand(VK_DOWN, [ssShift], TdxExtendNextLineCommand);
  RegisterKeyCommand(VK_UP, [ssCtrl], TdxPreviousParagraphCommand);
  RegisterKeyCommand(VK_DOWN, [ssCtrl], TdxNextParagraphCommand);
  RegisterKeyCommand(VK_UP, [ssCtrl, ssShift], TdxExtendPreviousParagraphCommand);
  RegisterKeyCommand(VK_DOWN, [ssCtrl, ssShift], TdxExtendNextParagraphCommand);
  RegisterKeyCommand(VK_PRIOR, [ssCtrl], TdxPreviousPageCommand);
  RegisterKeyCommand(VK_NEXT, [ssCtrl], TdxNextPageCommand);
  RegisterKeyCommand(VK_PRIOR, [ssCtrl, ssShift], TdxExtendPreviousPageCommand);
  RegisterKeyCommand(VK_NEXT, [ssCtrl, ssShift], TdxExtendNextPageCommand);
  RegisterKeyCommand(VK_PRIOR, [], TdxPreviousScreenCommand);
  RegisterKeyCommand(VK_NEXT, [], TdxNextScreenCommand);
  RegisterKeyCommand(VK_PRIOR, [ssShift], TdxExtendPreviousScreenCommand);
  RegisterKeyCommand(VK_NEXT, [ssShift], TdxExtendNextScreenCommand);
  RegisterKeyCommand(VK_HOME, [ssCtrl], TdxStartOfDocumentCommand);
  RegisterKeyCommand(VK_END, [ssCtrl], TdxEndOfDocumentCommand);
  RegisterKeyCommand(VK_HOME, [ssCtrl, ssShift], TdxExtendStartOfDocumentCommand);
  RegisterKeyCommand(VK_END, [ssCtrl, ssShift], TdxExtendEndOfDocumentCommand);
  RegisterKeyCommand('A', [ssCtrl], TdxSelectAllCommand);
  RegisterKeyCommand(VK_NUMPAD5, [ssCtrl], TdxSelectAllCommand);
  RegisterKeyCommand(VK_CLEAR, [ssCtrl], TdxSelectAllCommand);
  RegisterKeyCommand('Z', [ssCtrl], TdxUndoCommand);
  RegisterKeyCommand('Y', [ssCtrl], TdxRedoCommand);
  RegisterKeyCommand(VK_BACK, [ssAlt], TdxUndoCommand);
  RegisterKeyCommand(VK_BACK, [ssAlt, ssShift], TdxRedoCommand);
  RegisterKeyCommand(VK_RETURN, [], TdxEnterKeyCommand);
  RegisterKeyCommand(VK_RETURN, [ssShift], TdxInsertLineBreakCommand);
  RegisterKeyCommand(VK_INSERT, [], TdxToggleOvertypeCommand);
  RegisterKeyCommand(VK_TAB, [], TdxTabKeyCommand);
  RegisterKeyCommand(VK_TAB, [ssShift], TdxShiftTabKeyCommand);
  RegisterKeyCommand(VK_TAB, [ssCtrl], TdxInsertTabCommand);
  RegisterKeyCommand(VK_RETURN, [ssCtrl], TdxInsertPageBreakCommand);
  RegisterKeyCommand('C', [ssCtrl, ssAlt], TdxInsertCopyrightSymbolCommand);
  RegisterKeyCommand('R', [ssCtrl, ssAlt], TdxInsertRegisteredTrademarkSymbolCommand);
  RegisterKeyCommand('T', [ssCtrl, ssAlt], TdxInsertTrademarkSymbolCommand);
  RegisterKeyCommand(VK_OEM_PERIOD, [ssCtrl, ssAlt], TdxInsertEllipsisCommand);
  RegisterKeyCommand('B', [ssCtrl], TdxToggleFontBoldCommand);
  RegisterKeyCommand('I', [ssCtrl], TdxToggleFontItalicCommand);
  RegisterKeyCommand('U', [ssCtrl], TdxToggleFontUnderlineCommand);
  RegisterKeyCommand('D', [ssCtrl, ssShift], TdxToggleFontDoubleUnderlineCommand);
  RegisterKeyCommand(VK_OEM_PERIOD, [ssCtrl, ssShift], TdxIncreaseFontSizeCommand);
  RegisterKeyCommand(VK_OEM_COMMA, [ssCtrl, ssShift], TdxDecreaseFontSizeCommand);
  RegisterKeyCommand(VK_OEM_6, [ssCtrl], TdxIncrementFontSizeCommand);
  RegisterKeyCommand(VK_OEM_4, [ssCtrl], TdxDecrementFontSizeCommand);
  RegisterKeyCommand(VK_OEM_PLUS, [ssCtrl, ssShift], TdxToggleFontSuperscriptCommand);
  RegisterKeyCommand(VK_OEM_PLUS, [ssCtrl], TdxToggleFontSubscriptCommand);

  RegisterKeyCommand(VK_F12, [], TdxSaveDocumentAsCommand);

  RegisterKeyCommand('L', [ssCtrl], TdxToggleParagraphAlignmentLeftCommand);
  RegisterKeyCommand('E', [ssCtrl], TdxToggleParagraphAlignmentCenterCommand);
  RegisterKeyCommand('R', [ssCtrl], TdxToggleParagraphAlignmentRightCommand);
  RegisterKeyCommand('J', [ssCtrl], TdxToggleParagraphAlignmentJustifyCommand);

  RegisterKeyCommand('1', [ssCtrl], TdxSetSingleParagraphSpacingCommand);
  RegisterKeyCommand('2', [ssCtrl], TdxSetDoubleParagraphSpacingCommand);
  RegisterKeyCommand('3', [ssCtrl], TdxSetSesquialteralParagraphSpacingCommand);
  RegisterKeyCommand(VK_DELETE, [], TdxDeleteCommand);
  RegisterKeyCommand(VK_BACK, [], TdxBackSpaceKeyCommand);
  RegisterKeyCommand(VK_BACK, [ssShift], TdxBackSpaceKeyCommand);
  RegisterKeyCommand(VK_DELETE, [ssCtrl], TdxDeleteWordCommand);
  RegisterKeyCommand(VK_BACK, [ssCtrl], TdxDeleteWordBackCommand);
  RegisterKeyCommand('C', [ssCtrl], TdxCopySelectionCommand);
  RegisterKeyCommand(VK_INSERT, [ssCtrl], TdxCopySelectionCommand);
  RegisterKeyCommand('V', [ssCtrl], TdxPasteSelectionCommand);
  RegisterKeyCommand(VK_INSERT, [ssShift], TdxPasteSelectionCommand);
  RegisterKeyCommand('X', [ssCtrl], TdxCutSelectionCommand);
  RegisterKeyCommand(VK_DELETE, [ssShift], TdxCutSelectionCommand);

  RegisterKeyCommand('I', [ssAlt], TdxIncrementNumerationFromParagraphCommand);
  RegisterKeyCommand('I', [ssAlt, ssCtrl], TdxDecrementNumerationFromParagraphCommand);



  RegisterCommand(TdxShowParagraphFormCommand);
  RegisterCommand(TdxChangeFontSizeCommand);
end;


procedure TdxRichEditKeyboardDefaultHandler.TryChangeCursor;
begin
end;

{ TdxRichEditKeyboardController }

constructor TdxRichEditKeyboardController.Create(
  ARichControl: IdxRichEditControl);
begin
  inherited Create;
  FRichControl := ARichControl;
  FDefaultHandler := CreateDefaultHandler;
  Handlers.Add(FDefaultHandler);
end;

function TdxRichEditKeyboardController.CreateDefaultHandler: IdxKeyboardHandlerService;
begin
  Result := TdxRichEditKeyboardDefaultHandler.Create(Self);
end;

destructor TdxRichEditKeyboardController.Destroy;
begin
  Handlers.Remove(FDefaultHandler);
  FDefaultHandler := nil;
  inherited Destroy;
end;

function TdxRichEditKeyboardController.GetActiveView: TdxRichEditView;
begin
  Result := InnerControl.ActiveView;
end;

function TdxRichEditKeyboardController.GetDocumentModel: TdxDocumentModel;
begin
  Result := InnerControl.DocumentModel;

end;

function TdxRichEditKeyboardController.GetInnerControl: IdxInnerControl;
begin
  Result := RichControl.InnerControl;
end;

procedure TdxRichEditKeyboardCustomHandler.UnregisterKeyCommand(
  AShortCut: TShortCut);
var
  AId: TdxRichEditCommandId;
begin
  if KeyHandlerIdTable.TryGetValue(AShortCut, AId) then
  begin
    CommandTable.Remove(AId);
    KeyHandlerIdTable.Remove(AShortCut);
  end;
end;

end.
