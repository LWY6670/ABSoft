{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Export.Rtf;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Types, Classes, SysUtils, Graphics, Windows, Generics.Collections, dxRichEdit.Options,
  dxRichEdit.DocumentModel.Core, dxRichEdit.Utils.Characters, dxRichEdit.Utils.ChunkedStringBuilder, dxRichEdit.DocumentModel.Styles,
  dxRichEdit.Platform.Font, cxGeometry, dxRichEdit.DocumentModel.PieceTable, dxRichEdit.DocumentModel.Exporter,
  dxRichEdit.DocumentModel.CharacterFormatting, dxRichEdit.Utils.OfficeImage, dxRichEdit.DocumentModel.ParagraphFormatting,
  dxRichEdit.DocumentModel.UnitConverter, dxRichEdit.DocumentModel.Section, dxRichEdit.DocumentModel.TabFormatting,
  dxRichEdit.DocumentModel.Numbering, dxRichEdit.Export.Rtf.Keywords, dxRichEdit.DocumentModel.TableStyles,
  dxRichEdit.DocumentModel.TableCalculator;

type
  TdxRtfBuilder = class;
  TdxRtfContentExporter = class;

  IdxRtfExportHelper = interface
  ['{BB5A4345-2772-4F4C-8791-CA1DBCD5A4EA}']
    function GetDefaultCharacterProperties: string;
    function GetDefaultFontIndex: Integer;
    function GetDefaultParagraphProperties: string;
    function GetFontCharsetTable: TDictionary<Integer, Integer>;
    function GetListOverrideCollectionIndex: TDictionary<Integer, Integer>;

    function BlendColor(AColor: TColor): TColor;
    function GetCharacterStylesCollectionIndex: TDictionary<string, Integer>;
    function GetColorCollection: TList<TColor>;
    function GetColorIndex(AColor: TColor): Integer;
    function GetFontNameIndex(const AName: string): Integer;
    function GetFontNamesCollection: TList<string>;
    function GetListCollection: TDictionary<Integer, string>;
    function GetListOverrideCollection: TList<string>;
    function GetParagraphStylesCollectionIndex: TDictionary<string, Integer>;
    function GetStylesCollection: TList<string>;
    function GetSupportStyle: Boolean;
    function GetUserCollection: TList<string>;

    procedure SetDefaultCharacterProperties(const Value: string);
    procedure SetDefaultParagraphProperties(const Value: string);

    property CharacterStylesCollectionIndex: TDictionary<string, Integer> read GetCharacterStylesCollectionIndex;
    property ColorCollection: TList<TColor> read GetColorCollection;
    property DefaultCharacterProperties: string read GetDefaultCharacterProperties write SetDefaultCharacterProperties;
    property DefaultFontIndex: Integer read GetDefaultFontIndex;
    property DefaultParagraphProperties: string read GetDefaultParagraphProperties write SetDefaultParagraphProperties;
    property FontCharsetTable: TDictionary<Integer, Integer> read GetFontCharsetTable;
    property FontNamesCollection: TList<string> read GetFontNamesCollection;
    property ListCollection: TDictionary<Integer, string> read GetListCollection;
    property ListOverrideCollection: TList<string> read GetListOverrideCollection;
    property ListOverrideCollectionIndex: TDictionary<Integer, Integer> read GetListOverrideCollectionIndex;
    property ParagraphStylesCollectionIndex: TDictionary<string, Integer> read GetParagraphStylesCollectionIndex;
    property StylesCollection: TList<string> read GetStylesCollection;
    property SupportStyle: Boolean read GetSupportStyle;
    property UserCollection: TList<string> read GetUserCollection;
  end;

  { TdxRtfTableWidthsCalculator }

  TdxRtfTableWidthsCalculator = class(TdxTableWidthsCalculatorBase)
  end;


  { TdxRtfExportHelper }

  TdxRtfExportHelper = class(TInterfacedObject, IdxRtfExportHelper)
  private
    FDefaultFontName: string;
    FDefaultFontIndex: Integer;
    FFontNamesCollection: TList<string>;
    FListCollection: TDictionary<Integer, string>;
    FListOverrideCollectionIndex: TDictionary<Integer, Integer>;
    FListOverrideCollection: TList<string>;
    FColorCollection: TList<TColor>;
    FParagraphStylesCollectionIndex: TDictionary<string, Integer>;
    FCharacterStylesCollectionIndex: TDictionary<string, Integer>;
    FTableStylesCollectionIndex: TDictionary<string, Integer>;
    FFontCharsetTable: TDictionary<Integer, Integer>;
    FStylesCollection: TList<string>;
    FUserCollection: TList<string>;
    FDefaultCharacterProperties: string;
    FDefaultParagraphProperties: string;
    function GetListOverrideCollectionIndex: TDictionary<Integer, Integer>;
  protected
    //IdxRtfExportHelper
    function BlendColor(AColor: TColor): TColor;
    function GetCharacterStylesCollectionIndex: TDictionary<string, Integer>;
    function GetColorCollection: TList<TColor>;
    function GetColorIndex(AColor: TColor): Integer;
    function GetDefaultCharacterProperties: string;
    function GetDefaultFontIndex: Integer;
    function GetDefaultParagraphProperties: string;
    function GetFontCharsetTable: TDictionary<Integer, Integer>;
    function GetFontNamesCollection: TList<string>;
    function GetFontNameIndex(const AName: string): Integer;
    function GetListCollection: TDictionary<Integer, string>;
    function GetListOverrideCollection: TList<string>;
    function GetParagraphStylesCollectionIndex: TDictionary<string, Integer>;
    function GetStylesCollection: TList<string>;
    function GetSupportStyle: Boolean;
    function GetUserCollection: TList<string>;
    procedure SetDefaultCharacterProperties(const Value: string);
    procedure SetDefaultParagraphProperties(const Value: string);
  public
    constructor Create;
    destructor Destroy; override;

    property ColorCollection: TList<TColor> read FColorCollection;
    property FontCharsetTable: TDictionary<Integer, Integer> read GetFontCharsetTable;
    property FontNamesCollection: TList<string> read FFontNamesCollection;
    property ListCollection: TDictionary<Integer, string> read FListCollection;
    property ListOverrideCollectionIndex: TDictionary<Integer, Integer> read GetListOverrideCollectionIndex;
    property ListOverrideCollection: TList<string> read GetListOverrideCollection;
    property ParagraphStylesCollectionIndex: TDictionary<string, Integer> read GetParagraphStylesCollectionIndex;
    property CharacterStylesCollectionIndex: TDictionary<string, Integer> read GetCharacterStylesCollectionIndex;
    property StylesCollection: TList<string> read GetStylesCollection;
    property UserCollection: TList<string> read FUserCollection;
    property SupportStyle: Boolean read GetSupportStyle;
    property DefaultCharacterProperties: string read GetDefaultCharacterProperties write SetDefaultCharacterProperties;
    property DefaultParagraphProperties: string read GetDefaultParagraphProperties write SetDefaultParagraphProperties;
    property DefaultFontIndex: Integer read FDefaultFontIndex;
  end;

  { TdxRtfPropertiesExporter }

  TdxRtfPropertiesExporter = class abstract
  private
    FDocumentModel: TdxDocumentModel;
    FRtfBuilder: TdxRtfBuilder;
    FRtfExportHelper: IdxRtfExportHelper;
    function GetUnitConverter: TdxDocumentModelUnitConverter;
  public
    constructor Create(ADocumentModel: TdxDocumentModel; ARtfExportHelper: IdxRtfExportHelper;
      ARtfBuilder: TdxRtfBuilder);

    property RtfExportHelper: IdxRtfExportHelper read FRtfExportHelper;
    property RtfBuilder: TdxRtfBuilder read FRtfBuilder;
    property DocumentModel: TdxDocumentModel read FDocumentModel;
    property UnitConverter: TdxDocumentModelUnitConverter read GetUnitConverter;
  end;

  TdxRtfParagraphPropertiesExporter = class(TdxRtfPropertiesExporter)
  const
    DefaultParagraphFirstLineIndent = 0;
    DefaultParagraphLeftIndent = 0;
    DefaultParagraphRightIndent = 0;
    DefaultSuppressHyphenation = False;
    DefaultPageBreakBefore = False;
    DefaultBeforeAutoSpacing = False;
    DefaultAfterAutoSpacing = False;
    DefaultKeepWithNext = False;
    DefaultKeepLinesTogether = False;
    DefaultWidowOrphanControl = True;
    DoubleIntervalRtfLineSpacingValue = 480;
    SesquialteralIntervalRtfLineSpacingValue = 360;
    SingleIntervalRtfLineSpacingValue = 240;

    AtLeastLineSpacingMultiple = 0;
    ExactlyLineSpacingMultiple = 0;
    MultipleLineSpacing = 1;

    DefaultParagraphSpacingBefore = 0;
    DefaultParagraphSpacingAfter = 0;
  private
    function CalcRtfFirstLineIndent(AFirstLineIndentType: TdxParagraphFirstLineIndent;  AFirstLineIndent: Integer): Integer;
    function CalcRtfLeftIndent(AFirstLineIndentType: TdxParagraphFirstLineIndent; AFirstLineIndent, ALeftIndent: Integer): Integer;
    function CalcRtfRightIndent(ARightIndent: Integer): Integer;
  protected
    procedure ExportParagraphNumberingProperties(AParagraph: TdxParagraph);

    procedure WriteParagraphAlignment(Alignment: TdxParagraphAlignment);
    procedure WriteParagraphIndents(AMergedParagraphProperties: TdxMergedParagraphProperties);
    procedure WriteParagraphSuppressHyphenation(AParagraphSuppressHyphenation: Boolean);
    procedure WriteParagraphSuppressLineNumbers(AParagraphSuppressLineNumbers: Boolean);
    procedure WriteParagraphContextualSpacing(Value: Boolean);
    procedure WriteParagraphPageBreakBefore(Value: Boolean);
    procedure WriteParagraphBeforeAutoSpacing(Value: Boolean);
    procedure WriteParagraphAfterAutoSpacing(Value: Boolean);
    procedure WriteParagraphKeepWithNext(Value: Boolean);
    procedure WriteParagraphKeepLinesTogether(Value: Boolean);
    procedure WriteParagraphWidowOrphanControl(Value: Boolean);
    procedure WriteParagraphOutlineLevel(AOutlineLevel: Integer);
    procedure WriteParagraphBackColor(Value: TColor);
    procedure WriteParagraphLineSpacing(AParagraphLineSpacingType: TdxParagraphLineSpacing; AParagraphLineSpacing: Single);
    procedure WriteParagraphSpacingBefore(ASpacingBefore: Integer);
    procedure WriteParagraphSpacingAfter(ASpacingAfter: Integer);
    procedure WriteRtfLineSpacing(ARtfLineSpacingValue, ARtfLineSpacingMultiple: Integer);
    procedure WriteParagraphListIndex(AIndex: TdxNumberingListIndex);
    procedure WriteParagraphStyle(AParagraphStyle: TdxParagraphStyle);
    procedure WriteParagraphGroupPropertiesId(AParagraph: TdxParagraph);
    procedure WriteParagraphTabs(ATabFormattingInfo: TdxTabFormattingInfo);
    procedure WriteParagraphTab(ATabInfo: TdxTabInfo);
    procedure WriteTabKind(AlignmentType: TdxTabAlignmentType);
    procedure WriteTabLeader(ALeaderType: TdxTabLeaderType);
    procedure WriteTabPosition(APosition: Integer);
  public
    procedure ExportParagraphPropertiesCore(AProperties: TdxMergedParagraphProperties;
      ACheckDefaultAlignment: Boolean = False);
    procedure ExportParagraphProperties(AParagraph: TdxParagraph; ATableNestingLevel: Integer);
  end;

  { TdxRtfCharacterPropertiesExporter }

  TdxRtfCharacterPropertiesExporter = class(TdxRtfPropertiesExporter)
  const
    DefaultRtfFontSize = 24;
  private
    class var FDefaultUnderlineTypes: TDictionary<TdxUnderlineType, string>;
    FOptions: TdxRtfDocumentExporterOptions;
    class constructor Initialize;
    class destructor Finalize;
  protected
    procedure ExportCharacterProperties(ACharacterProperties: TdxMergedCharacterProperties; ACheckDefaultColor: Boolean = False);
    procedure ExportCharacterPropertiesCore(ACharacterProperties: TdxMergedCharacterProperties);
    procedure ExportParagraphCharacterProperties(ACharacterProperties: TdxMergedCharacterProperties);

    procedure RegisterFontCharset(AInfo: TdxCharacterFormattingInfo; AFontNameIndex: Integer);
    procedure WriteBackgroundColor(ABackColor: TColor);
    procedure WriteFontSize(ARtfFontSize: Integer);
    function WriteFontName(AFontName: string): Integer;
    procedure WriteFontUnderline(AUnderlineType: TdxUnderlineType);
    procedure WriteForegroundColor(AForeColor: TColor; ACheckDefaultColor: Boolean = False);
    procedure WriteForegroundColorCore(AForeColor: TColor; ACheckDefaultColor: Boolean = False);
    procedure WriteUnderlineColor(AUnderlineColor: TColor);

    class property DefaultUnderlineTypes: TDictionary<TdxUnderlineType, string> read FDefaultUnderlineTypes;
  public
    constructor Create(ADocumentModel: TdxDocumentModel; ARtfExportHelper: IdxRtfExportHelper;
      ARtfBuilder: TdxRtfBuilder; AOptions: TdxRtfDocumentExporterOptions);
  end;

  { TdxRtfStyleExporter }

  TdxRtfStyleExporter = class
  strict private
    FRtfExportHelper: IdxRtfExportHelper;
    FRtfBuilder: TdxRtfBuilder;
    FCharacterPropertiesExporter: TdxRtfCharacterPropertiesExporter;
    FParagraphPropertiesExporter: TdxRtfParagraphPropertiesExporter;
    FDocumentModel: TdxDocumentModel;
    function ObtainStyleIndex(AStyle: IdxStyle; ACollection: TDictionary<string, Integer>): Integer;
    function ObtainCharacterStyleIndex(AStyle: TdxCharacterStyle): Integer;
    function ObtainParagraphStyleIndex(AStyle: TdxParagraphStyle): Integer;
  protected
    procedure ExportCharacterProperties(ACharacterProperties: TdxMergedCharacterProperties);
    procedure ExportCharacterStyle(AStyle: TdxCharacterStyle);
    procedure ExportParagraphStyle(AStyle: TdxParagraphStyle; I: Integer);
    procedure ExportParagraphStyles(AParagraphStyles: TdxParagraphStyleCollection);
    procedure ExportParagraphProperties(AParagraphProperties: TdxParagraphProperties; AMergedParagraphProperties: TdxMergedParagraphProperties);    procedure ExportCharacterStyles(ACharacterStyles: TdxCharacterStyleCollection);
    function GetListId(AIndex: TdxNumberingListIndex): Integer;
    function GetNextFreeStyleIndex: Integer;
    procedure WriteStyleName(const AName: string);
  public
    constructor Create(ADocumentModel: TdxDocumentModel; AExporter: TdxRtfContentExporter;
      ARtfExportHelper: IdxRtfExportHelper; AOptions: TdxRtfDocumentExporterOptions);
    destructor Destroy; override;

    procedure ExportStyleSheet(AParagraphStyles: TdxParagraphStyleCollection;
      ACharacterStyles: TdxCharacterStyleCollection; ATableStyles: TdxTableStyleCollection);

    property RtfBuilder: TdxRtfBuilder read FRtfBuilder;
    property RtfExportHelper: IdxRtfExportHelper read FRtfExportHelper;
    property CharacterExporter: TdxRtfCharacterPropertiesExporter read FCharacterPropertiesExporter;
    property DocumentModel: TdxDocumentModel read FDocumentModel;
  end;

  { TdxRtfNumberingListExporter }

  TdxRtfNumberingListExporter = class
  strict private
    FRtfExporter: TdxRtfContentExporter;
    FRtfBuilder: TdxRtfBuilder;
    FText: string;
    FNumber: string;
    FCharacterPropertiesExporter: TdxRtfCharacterPropertiesExporter;
    FParagraphPropertiesExporter: TdxRtfParagraphPropertiesExporter;
    FTextLength: Integer;
  private
    function GetAbstractNumberingLists(ANumberingLists: TdxNumberingListCollection; AStartIndex: TdxNumberingListIndex;
      ACount: Integer): TdxAbstractNumberingListCollection;
  protected
    procedure ExportAbstractNumberingList(AList: TdxAbstractNumberingList); virtual;
    procedure ExportListLevels(AListLevelCollection: TdxListLevelCollection); virtual;
    procedure ExportListLevel(AListLevel: IdxListLevel);
    function GetNumberingListFormat(ANumberingFormat: TdxNumberingFormat): Integer;
    function GetListLevelSeparator(ASeparator: Char): Integer;
    procedure ExportListLevelTextAndNumber(const ADisplayFormatString: string; ALevelTemplateId: Integer); virtual;
    procedure GetTextAndNumber(const ADisplayFormatString: string);
    function AddLevelNumber(const ADisplayFormatString: string; I: Integer): Integer;
    function AddLevelNumberCore(const ADisplayFormatString: string; I: Integer): Integer;
    class function DoubleBrackets(const ADisplayFormatString: string; I: Integer): Boolean;
    function AddChar(ACh: Char; I: Integer): Integer;
    function AddEscapedChar(ACh: Char; I: Integer): Integer;
    procedure ExportListLevelCharacterAndParagraphProperties(AListLevel: IdxListLevel);
    procedure ExportAbstractListLevelParagraphStyleIndex(AAbstractListLevel: TdxListLevel);
    procedure ExportListOverride(ANumberingList: TdxNumberingList); virtual;
    procedure WriteListOverrideId(ANumberingList: TdxNumberingList); virtual;
    function GetListOverrideCount(ANumberingList: TdxNumberingList): Integer;
    function IsOverrideLevel(AListLevel: TdxAbstractListLevel): Boolean;
    procedure ExportListOverrideLevels(ANumberingList: TdxNumberingList);
    procedure ExportListOverrideLevel(ALevel: TdxAbstractListLevel);

    property RtfBuilder: TdxRtfBuilder read FRtfBuilder;
    property RtfExporter: TdxRtfContentExporter read FRtfExporter;
  public
    constructor Create(ARtfExporter: TdxRtfContentExporter);
    destructor Destroy; override;

    procedure Export(ANumberingLists: TdxNumberingListCollection; AStartIndex, ACount: Integer);
    procedure ExportNumberingListTable(AAbstractNumberingLists: TdxAbstractNumberingListCollection); virtual;
    procedure ExportAbstractNumberingLists(AAbstractNumberingLists: TdxAbstractNumberingListCollection); virtual;
    procedure ExportListOverrideTable(ANumberingLists: TdxNumberingListCollection; AStartIndex: TdxNumberingListIndex; ACount: Integer); overload; virtual;
    procedure ExportListOverrideTable(ANumberingLists: TdxNumberingListCollection); overload; virtual;

    property Text: string read FText write FText;
    property Number: string read FNumber write FNumber;
    property TextLength: Integer read FTextLength write FTextLength;
  end;

  { TdxRtfContentExporter }

  TdxRtfContentExporter = class(TdxDocumentModelExporter)
  strict private
    FLastParagraphRunNotSelected: Boolean;
    FRtfExportHelper: IdxRtfExportHelper;
    FRtfBuilder: TdxRtfBuilder;
    FParagraphPropertiesExporter: TdxRtfParagraphPropertiesExporter;
    FCharacterPropertiesExporter: TdxRtfCharacterPropertiesExporter;
    FOptions: TdxRtfDocumentExporterOptions;
    FGetMergedCharacterPropertiesCachedResult: TdxRunMergedCharacterPropertiesCachedResult;
    function CalculateFirstExportedNumberingListsIndex(ANumberingLists: TdxNumberingListCollection): Integer;
    function CalculateFirstExportedNumberingListsIndexForStyles(AParagraphStyleCollection: TdxParagraphStyleCollection): Integer;
    function GetUnitConverter: TdxDocumentModelUnitConverter;
    function IsLastParagraph(AParagraph: TdxParagraph): Boolean;
    procedure StartNewSection(ASection: TdxSection);
  protected
    procedure ExportSection(ASection: TdxSection); override;
    function ShouldExportHiddenText: Boolean; override;

    function CreateNumberingListExporter(AExporter: TdxRtfContentExporter): TdxRtfNumberingListExporter;

    function CreateRtfBuilder: TdxRtfBuilder; virtual;
    function CreateParagraphPropertiesExporter: TdxRtfParagraphPropertiesExporter; virtual;
    function CreateStyleExporter: TdxRtfStyleExporter;
    procedure EnsureValidFirstColor(AColor: TColor);
    procedure FinishParagraph(AParagraph: TdxParagraph);
    procedure PopulateUserTable;
    procedure StartNewParagraph(AParagraph: TdxParagraph; ATableNestingLevel: Integer);
    procedure StartNewInnerParagraph(AParagraph: TdxParagraph; ATableNestingLevel: Integer);
    function ShouldWriteRunCharacterStyle(ARun: TdxTextRunBase): Boolean;
    function SuppressExportLastParagraph: Boolean;
    procedure WriteAlternativeText(AParagraph: TdxParagraph);
    procedure WriteText(const AText: string);
    procedure WriteSectionBreakType(ABreakType: TdxSectionStartType);
    procedure WriteChapterSeparator(ASeparator: Char);
    procedure WritePageNumberingFormat(ANumberingFormat: TdxNumberingFormat);
    procedure WriteAlternativeAndPlainText(AParagraph: TdxParagraph);
    procedure WriteRunCharacterStyle(ARun: TdxTextRunBase);
    procedure WriteCharacterStyle(ACharacterStyle: TdxCharacterStyle);

    procedure ExportNumberingListTable;
    procedure ExportDefaultCharacterProperties;
    procedure ExportDefaultParagraphProperties;
    procedure ExportInlinePictureRun(ARun: TdxInlinePictureRun); override;
    procedure ExportStyleTable;
    procedure ExportDocumentProperties;
    procedure ExportSectionProperties(ASection: TdxSection);
    procedure ExportSectionMargins(AMargins: TdxSectionMargins);
    procedure ExportSectionPage(APage: TdxSectionPage);
    procedure ExportSectionGeneralSettings(ASettings: TdxSectionGeneralSettings);
    procedure ExportSectionPageNumbering(APageNumbering: TdxSectionPageNumbering);
    procedure ExportSectionLineNumbering(ALineNumbering: TdxSectionLineNumbering);
    procedure ExportSectionColumns(AColumns: TdxSectionColumns);
    procedure ExportSectionFootNote(ANote: TdxSectionFootNote);
    procedure ExportSectionEndNote(ANote: TdxSectionFootNote);
    procedure ExportSectionColumnsDetails(AColumns: TdxColumnInfoCollection);
    procedure ExportSectionColumn(AColumn: TdxColumnInfo; AColumnIndex: Integer);
    function ExportParagraph(AParagraph: TdxParagraph): TdxParagraphIndex; override;
    procedure ExportSingleParagraph(AParagraph: TdxParagraph);
    procedure ExportParagraphCore(AParagraph: TdxParagraph; ATableNestingLevel: Integer;
      ACondTypes: Integer; ATableStyleIndex: Integer);
    procedure ExportParagraphTableStyleProperties(ACondTypes: Integer;
      ATableStyleIndex: Integer);
    procedure ExportParagraphCharacterProperties(AParagraph: TdxParagraph);
    procedure ExportTextRun(ARun: TdxTextRun); override;
    procedure ExportFormattingFlags;

    property RtfExportHelper: IdxRtfExportHelper read FRtfExportHelper;
  public
    constructor Create(ADocumentModel: TdxDocumentModel; AOptions: TdxRtfDocumentExporterOptions;
      ARtfExportHelper: IdxRtfExportHelper);
    destructor Destroy; override;

    procedure Export; override;

    property LastParagraphRunNotSelected: Boolean read FLastParagraphRunNotSelected write FLastParagraphRunNotSelected;
    property Options: TdxRtfDocumentExporterOptions read FOptions;
    property RtfBuilder: TdxRtfBuilder read FRtfBuilder;
    property UnitConverter: TdxDocumentModelUnitConverter read GetUnitConverter;
  end;

  { TdxRtfBuilder }

  TdxRtfBuilder = class
  private
    FByteToHexString: array[0..255] of string;
    FEncoding: TEncoding;
    FHexMode: Boolean;
    FRowLength: Integer;
    FRtfContent: TdxChunkedStringBuilder;
    FIsPreviousWriteCommand: Boolean;
    FUnicodeTextBuilder: TStringBuilder;
    FSpecialMarks: TDictionary<Char, string>;
    FSpecialPCDataMarks: TDictionary<Char, string>;
    procedure AddHexToMarkTable(ATable: TDictionary<Char, string>; ACh: Char);
    function IsSpecialSymbol(ACh: Char): Boolean;
  protected
    procedure AppendUnicodeCompatibleCharCore(ACode: Integer; ACh: Char);
    function CreateSpecialMarksTable: TDictionary<Char, string>;
    function CreateSpecialPCDataMarksTable: TDictionary<Char, string>;
    procedure IncreaseRowLength(ADelta: Integer);
    function GetUnicodeCompatibleString(ACh: Char): string;
    function GetUnicodeCompatibleStringDirect(const AText: string): string;

    procedure WriteTextDirectUnsafe(AText: TdxChunkedStringBuilder);
    procedure WriteTextDirect(const AText: string; AMakeStringUnicodeCompatible: Boolean = False);
    procedure WriteTextCore(const AText: string; ASpecialMarks: TDictionary<Char, string>);
    procedure WriteStreamAsHex(AStream: TStream);
    procedure WriteStreamAsHexCore(AStream: TStream);
    procedure WriteByteArrayAsHex(ABuffer: TBytes; AOffset: Integer = 0; ALength: Integer = -1);
    procedure WriteMemoryStreamAsHex(AStream: TBytesStream);

    property Encoding: TEncoding read FEncoding;
  public
    constructor Create; overload;
    constructor Create(AEncoding: TEncoding); overload; virtual;
    destructor Destroy; override;

    procedure Clear;

    procedure CloseGroup;
    procedure OpenGroup;

    procedure WriteChar(ACh: Char);
    procedure WriteCommand(const ACommand: string); overload;
    procedure WriteCommand(const ACommand: string; AParam: Integer); overload;
    procedure WriteCommand(const ACommand, AParam: string); overload;
    procedure WriteText(const AText: string);
    procedure WritePCData(const AText: string);

    function RowLengthBound: Integer; virtual;

    property RtfContent: TdxChunkedStringBuilder read FRtfContent;
  end;

  { TdxDBCSRtfBuilder }

  TdxDBCSRtfBuilder = class(TdxRtfBuilder)
  public
    constructor Create(AEncoding: TEncoding); override;
  end;

  { TdxRtfExporter}

  TdxRtfExporter = class(TInterfacedObject, IdxRichEditExporter)
  strict private
    FContentExporter: TdxRtfContentExporter;
    FRtfBuilder: TdxRtfBuilder;
    FRtfExportHelper: IdxRtfExportHelper;
  private
    function GetLastParagraphRunNotSelected: Boolean;
    procedure SetLastParagraphRunNotSelected(const Value: Boolean);
  protected
    function CreateContentExporter(ADocumentModel: TdxDocumentModel; AOptions: TdxRtfDocumentExporterOptions;
      ARtfExportHelper: IdxRtfExportHelper): TdxRtfContentExporter; virtual;

    function CalculateFontNameEncoding(const AFontName: string; AFontIndex: Integer): TEncoding;
    function EscapeFontName(const AFontName: string; AEncoding: TEncoding): string;

    procedure ExportCore;

    procedure ExportColorTable;
    procedure ExportColorTableEntry(AColor: TColor);
    procedure ExportDefaultProperties;
    procedure ExportFontTable;
    procedure ExportListTable;
    procedure ExportListOverrideTable;
    procedure ExportStyleTable;
    procedure ExportParagraphGroupProperties;
    procedure ExportUsersTable;
    procedure ExportDocumentVariables;
    procedure ExportFontTableEntry(const AFontName: string; AFontIndex: Integer);
    procedure ExportDefaultCharacterProperties;
    procedure ExportDefaultParagraphProperties;

    property ContentExporter: TdxRtfContentExporter read FContentExporter;
    property RtfBuilder: TdxRtfBuilder read FRtfBuilder;
    property RtfExportHelper: IdxRtfExportHelper read FRtfExportHelper;
  public
    constructor Create(ADocumentModel: TdxDocumentModel; AOptions: TdxRtfDocumentExporterOptions);
    destructor Destroy; override;

    // IdxRichEditExporter
    function Export: string;

    function ExportSaveMemory: TdxChunkedStringBuilder;

    property LastParagraphRunNotSelected: Boolean read GetLastParagraphRunNotSelected write SetLastParagraphRunNotSelected;
  end;

implementation

uses
{$IFDEF DELPHI16}
  System.UITypes,
{$ENDIF}
  RTLConsts, dxRichEdit.Utils.BatchUpdateHelper, 
  Math, StrUtils, dxCore, dxCoreClasses, dxCoreGraphics,
  dxRichEdit.Utils.TopologicalSorter,
  dxRichEdit.Utils.Encoding,
  dxRichEdit.Utils.GenericsHelpers,
  dxRichEdit.Utils.Colors,
  dxRichEdit.Import.Rtf.DestinationListLevel,
  dxRichEdit.DocumentModel.SectionRange;

type
  { TdxRtfPictureExportStrategy }

  TdxRtfPictureExportStrategy = class abstract
  protected
    procedure ExportShapePicturePrefix(ARtfBuilder: TdxRtfBuilder); virtual; abstract;
    procedure ExportShapePicturePostfix(ARtfBuilder: TdxRtfBuilder); virtual; abstract;
    procedure ExportNonShapePicturePrefix(ARtfBuilder: TdxRtfBuilder); virtual; abstract;
    procedure ExportNonShapePicturePostfix(ARtfBuilder: TdxRtfBuilder); virtual; abstract;
  public
    procedure Export(ARtfBuilder: TdxRtfBuilder; ARun: IdxPictureContainerRun; ADuplicateObjectAsMetafile: Boolean);
  end;

  { TdxRtfFloatingObjectPictureExportStrategy }

  TdxRtfFloatingObjectPictureExportStrategy = class(TdxRtfPictureExportStrategy)
  end;

  { TdxRtfInlinePictureExportStrategy }

  TdxRtfInlinePictureExportStrategy = class(TdxRtfPictureExportStrategy)
  protected
    procedure ExportShapePicturePrefix(ARtfBuilder: TdxRtfBuilder); override;
    procedure ExportShapePicturePostfix(ARtfBuilder: TdxRtfBuilder); override;
    procedure ExportNonShapePicturePrefix(ARtfBuilder: TdxRtfBuilder); override;
    procedure ExportNonShapePicturePostfix(ARtfBuilder: TdxRtfBuilder); override;
  end;

  { TdxRtfPictureExporter }

  TdxRtfPictureExporter = class abstract
  private
    FRtfBuilder: TdxRtfBuilder;
    FRun: IdxPictureContainerRun;
    function GetImage: TdxOfficeImage;
    function GetUnitConverter: TdxDocumentModelUnitConverter;
  protected
    function GetPictureSize: TSize; virtual; abstract;
    function GetDesiredPictureSize: TSize; virtual; abstract;
    function GetPictureScale: TdxSizeF; virtual; abstract;
    function RtfPictureType: string; virtual; abstract;
    function GetImageBytesStream: TStream; virtual; abstract;

    procedure WritePictureHeader;

    property UnitConverter: TdxDocumentModelUnitConverter read GetUnitConverter;
    property Image: TdxOfficeImage read GetImage;
    property RtfBuilder: TdxRtfBuilder read FRtfBuilder;
    property Run: IdxPictureContainerRun read FRun;
  public
    constructor Create(ARtfBuilder: TdxRtfBuilder; ARun: IdxPictureContainerRun);
    class function CreateRtfPictureExporter(ARtfBuilder: TdxRtfBuilder; ARun: IdxPictureContainerRun;
      AImageFormat: TdxOfficeImageFormat): TdxRtfPictureExporter;
    procedure Export;
  end;

  { TdxRtfMetafilePictureExporter }

  TdxRtfMetafilePictureExporter = class abstract(TdxRtfPictureExporter)
  protected
    function GetDesiredPictureSize: TSize; override;
    function GetPictureScale: TdxSizeF; override;
    function GetPictureSize: TSize; override;
  end;

  { TdxRtfWmfPictureExporter }

  TdxRtfWmfPictureExporter = class(TdxRtfMetafilePictureExporter)
  protected
    function RtfPictureType: string; override;
    function GetImageBytesStream: TStream; override;
  end;

  { TdxRtfEmfPictureExporter }

  TdxRtfEmfPictureExporter = class(TdxRtfMetafilePictureExporter)
  protected
    function RtfPictureType: string; override;
    function GetImageBytesStream: TStream; override;
  end;

  { TdxRtfBitmapPictureExporter }

  TdxRtfBitmapPictureExporter = class abstract(TdxRtfPictureExporter)
  protected
    function GetImageBytesStream: TStream; override;
    function GetDesiredPictureSize: tagSIZE; override;
    function GetPictureScale: TdxSizeF; override;
    function GetPictureSize: tagSIZE; override;

    function GetImageFormat: TdxOfficeImageFormat; virtual; abstract;

    property ImageFormat: TdxOfficeImageFormat read GetImageFormat;
  end;

  { TdxRtfJpegPictureExporter }

  TdxRtfJpegPictureExporter = class(TdxRtfBitmapPictureExporter)
  protected
    function RtfPictureType: string; override;
    function GetImageFormat: TdxOfficeImageFormat; override;
  end;

  { TdxRtfPngPictureExporter }

  TdxRtfPngPictureExporter = class(TdxRtfBitmapPictureExporter)
  protected
    function RtfPictureType: string; override;
    function GetImageFormat: TdxOfficeImageFormat; override;
  end;

{ TdxRtfPictureExportStrategy }

procedure TdxRtfPictureExportStrategy.Export(ARtfBuilder: TdxRtfBuilder; ARun: IdxPictureContainerRun;
  ADuplicateObjectAsMetafile: Boolean);
var
  AFormat: TdxOfficeImageFormat;
  AExporter: TdxRtfPictureExporter;
  AWmfExporter: TdxRtfPictureExporter;
begin
  AFormat := ARun.PictureContent.Image.RawFormat;
  if AFormat <> TdxOfficeImageFormat.Wmf then
  begin
    ExportShapePicturePrefix(ARtfBuilder);
    AExporter := TdxRtfPictureExporter.CreateRtfPictureExporter(ARtfBuilder, ARun, AFormat);
    try
      AExporter.Export;
      ExportShapePicturePostfix(ARtfBuilder);
      if ADuplicateObjectAsMetafile and ARun.PictureContent.Image.CanGetImageBytes(TdxOfficeImageFormat.Wmf) then
      begin
        AWMfExporter := TdxRtfPictureExporter.CreateRtfPictureExporter(ARtfBuilder, ARun, TdxOfficeImageFormat.Wmf);
        try
          if AWmfExporter <> nil then
          begin
            ExportNonShapePicturePrefix(ARtfBuilder);
            AWmfExporter.Export;
            ExportNonShapePicturePostfix(ARtfBuilder);
          end;
        finally
          AWMfExporter.Free;
        end;
      end;
    finally
      AExporter.Free;
    end;
  end
  else
  begin
    AWmfExporter := TdxRtfPictureExporter.CreateRtfPictureExporter(ARtfBuilder, ARun, TdxOfficeImageFormat.Wmf);
    try
      if AWmfExporter <> nil then
        AWmfExporter.Export;
    finally
      AWmfExporter.Free;
    end;
  end;
end;

{ TdxRtfInlinePictureExportStrategy }

procedure TdxRtfInlinePictureExportStrategy.ExportShapePicturePrefix(ARtfBuilder: TdxRtfBuilder);
begin
  ARtfBuilder.OpenGroup;
  ARtfBuilder.WriteCommand(TdxRtfExportSR.ShapePicture);
end;

procedure TdxRtfInlinePictureExportStrategy.ExportShapePicturePostfix(ARtfBuilder: TdxRtfBuilder);
begin
  ARtfBuilder.CloseGroup;
end;

procedure TdxRtfInlinePictureExportStrategy.ExportNonShapePicturePrefix(ARtfBuilder: TdxRtfBuilder);
begin
  ARtfBuilder.OpenGroup;
  ARtfBuilder.WriteCommand(TdxRtfExportSR.NonShapePicture);
end;

procedure TdxRtfInlinePictureExportStrategy.ExportNonShapePicturePostfix(ARtfBuilder: TdxRtfBuilder);
begin
  ARtfBuilder.CloseGroup;
end;

{ TdxRtfPictureExporter }

constructor TdxRtfPictureExporter.Create(ARtfBuilder: TdxRtfBuilder;
  ARun: IdxPictureContainerRun);
begin
  inherited Create;
  FRtfBuilder := ARtfBuilder;
  FRun := ARun;
end;

class function TdxRtfPictureExporter.CreateRtfPictureExporter(ARtfBuilder: TdxRtfBuilder;
  ARun: IdxPictureContainerRun; AImageFormat: TdxOfficeImageFormat): TdxRtfPictureExporter;
begin
  if not ARun.PictureContent.Image.IsExportSupported(AImageFormat) then
    Result := nil
  else
  if AImageFormat = TdxOfficeImageFormat.Wmf then
    Result := TdxRtfWmfPictureExporter.Create(ARtfBuilder, ARun)
  else
  if AImageFormat = TdxOfficeImageFormat.Emf then
    Result := TdxRtfEmfPictureExporter.Create(ARtfBuilder, ARun)
  else
  if AImageFormat = TdxOfficeImageFormat.Jpeg then
    Result := TdxRtfJpegPictureExporter.Create(ARtfBuilder, ARun)
  else
    Result := TdxRtfPngPictureExporter.Create(ARtfBuilder, ARun);
end;

procedure TdxRtfPictureExporter.Export;
var
  AStream: TStream;
begin
  RtfBuilder.OpenGroup;
  WritePictureHeader;
  try
    AStream := GetImageBytesStream;
    try
      RtfBuilder.WriteStreamAsHex(AStream);
    finally
      AStream.Free;
    end;
  finally
    RtfBuilder.CloseGroup;
  end;
end;

function TdxRtfPictureExporter.GetImage: TdxOfficeImage;
begin
  Result := Run.PictureContent.Image.Image;
end;

function TdxRtfPictureExporter.GetUnitConverter: TdxDocumentModelUnitConverter;
begin
  Result := Run.PictureContent.Paragraph.DocumentModel.UnitConverter;
end;

procedure TdxRtfPictureExporter.WritePictureHeader;
var
  APirctureSize, ADesiredPictureSize: TSize;
  APictureScale: TdxSizeF;
begin
  APirctureSize := GetPictureSize;
  ADesiredPictureSize := GetDesiredPictureSize;
  APictureScale := GetPictureScale;
  while (ADesiredPictureSize.cx > $7FFF) or (ADesiredPictureSize.cy > $7FFF) do//T117205
  begin
    ADesiredPictureSize.cx := ADesiredPictureSize.cx div 2;
    ADesiredPictureSize.cy := ADesiredPictureSize.cy div 2;
    APictureScale.cx := APictureScale.cx * 2;
    APictureScale.cy := APictureScale.cy * 2;
    APirctureSize.cx := APirctureSize.cx div 2;
    APirctureSize.cy := APirctureSize.cy div 2;
  end;

  RtfBuilder.WriteCommand(TdxRtfExportSR.Picture);
  RtfBuilder.WriteCommand(RtfPictureType);

  RtfBuilder.WriteCommand(TdxRtfExportSR.PictureWidth, Math.Max(APirctureSize.cx, 1));
  RtfBuilder.WriteCommand(TdxRtfExportSR.PictureHeight, Math.Max(APirctureSize.cy, 1));

  RtfBuilder.WriteCommand(TdxRtfExportSR.PictureDesiredWidth, Math.Max(ADesiredPictureSize.cx, 1));
  RtfBuilder.WriteCommand(TdxRtfExportSR.PictureDesiredHeight, Math.Max(ADesiredPictureSize.cy, 1));

  RtfBuilder.WriteCommand(TdxRtfExportSR.PictureScaleX, Math.Max(Round(APictureScale.cx), 1));
  RtfBuilder.WriteCommand(TdxRtfExportSR.PictureScaleY, Math.Max(Round(APictureScale.cy), 1));

  if Image.Uri <> '' then
  begin
    RtfBuilder.OpenGroup;
    try
      RtfBuilder.WriteCommand(TdxRtfExportSR.DxImageUri);
      RtfBuilder.WriteText(Image.Uri);
    finally
      RtfBuilder.CloseGroup;
    end;
  end;
end;

{ TdxRtfMetafilePictureExporter }

function TdxRtfMetafilePictureExporter.GetDesiredPictureSize: TSize;
begin
  Result := UnitConverter.ModelUnitsToTwips(Run.PictureContent.OriginalSize);
end;

function TdxRtfMetafilePictureExporter.GetPictureScale: TdxSizeF;
begin
  Result := dxSizeF(Run.ScaleX, Run.ScaleY);
end;

function TdxRtfMetafilePictureExporter.GetPictureSize: TSize;
begin
  Result := UnitConverter.ModelUnitsToHundredthsOfMillimeter(Run.PictureContent.OriginalSize);
end;

{ TdxRtfWmfPictureExporter }

function TdxRtfWmfPictureExporter.GetImageBytesStream: TStream;
begin
  Result := Image.GetImageBytesStream(TdxOfficeImageFormat.Wmf)
end;

function TdxRtfWmfPictureExporter.RtfPictureType: string;
begin
  Result := '\wmetafile8';
end;

{ TdxRtfEmfPictureExporter }

function TdxRtfEmfPictureExporter.GetImageBytesStream: TStream;
begin
  Result := Image.GetImageBytesStream(TdxOfficeImageFormat.Emf);
end;

function TdxRtfEmfPictureExporter.RtfPictureType: string;
begin
  Result := '\emfblip';
end;

{ TdxRtfBitmapPictureExporter }

function TdxRtfBitmapPictureExporter.GetImageBytesStream: TStream;
begin
  Result := Image.GetImageBytesStream(ImageFormat);
end;

function TdxRtfBitmapPictureExporter.GetDesiredPictureSize: tagSIZE;
begin
  Result := Image.SizeInTwips;
end;

function TdxRtfBitmapPictureExporter.GetPictureScale: TdxSizeF;
begin
  Result := dxSizeF(Run.ScaleX, Run.ScaleY);
end;

function TdxRtfBitmapPictureExporter.GetPictureSize: tagSIZE;
begin
  Result := Image.SizeInHundredthsOfMillimeter;
end;

{ TdxRtfJpegPictureExporter }

function TdxRtfJpegPictureExporter.RtfPictureType: string;
begin
  Result := '\jpegblip';
end;

function TdxRtfJpegPictureExporter.GetImageFormat: TdxOfficeImageFormat;
begin
  Result := TdxOfficeImageFormat.Jpeg;
end;

{ TdxRtfPngPictureExporter }

function TdxRtfPngPictureExporter.RtfPictureType: string;
begin
  Result := '\pngblip';
end;

function TdxRtfPngPictureExporter.GetImageFormat: TdxOfficeImageFormat;
begin
  Result := TdxOfficeImageFormat.Png;
end;

{ TdxRtfExporter }

function TdxRtfExporter.CalculateFontNameEncoding(const AFontName: string;
  AFontIndex: Integer): TEncoding;
var
  AFontCharset: Integer;
  ACodePage: Integer;
begin
  // 1. Try ASCII encoding
  if TdxEncoding.CanBeLoselesslyEncoded(AFontName, TdxEncoding.ASCII) then
    Result := TdxEncoding.ASCII
  else
  begin
    // 2. Try to use font charset
    if not RtfExportHelper.FontCharsetTable.TryGetValue(AFontIndex, AFontCharset) then
      AFontCharset := -1;

    if AFontCharset >= 0 then
    begin
      ACodePage := TdxEncoding.CodePageFromCharset(AFontCharset);
      Result := TdxEncoding.GetEncodingFromCodePage(ACodePage);
      if TdxEncoding.CanBeLoselesslyEncoded(AFontName, Result) then
        Exit;
    end;

    // 3. Try to use default encoding
    Result := ContentExporter.Options.ActualEncoding;
    if not TdxEncoding.CanBeLoselesslyEncoded(AFontName, Result) then
      Result := TdxEncoding.GetEncodingFromString(AFontName);
  end;
end;

constructor TdxRtfExporter.Create(ADocumentModel: TdxDocumentModel;
  AOptions: TdxRtfDocumentExporterOptions);
begin
  inherited Create;
  FRtfExportHelper := TdxRtfExportHelper.Create;
  FContentExporter := CreateContentExporter(ADocumentModel, AOptions, RtfExportHelper);
  FRtfBuilder := ContentExporter.CreateRtfBuilder;
end;

function TdxRtfExporter.CreateContentExporter(ADocumentModel: TdxDocumentModel;
  AOptions: TdxRtfDocumentExporterOptions;
  ARtfExportHelper: IdxRtfExportHelper): TdxRtfContentExporter;
begin
  Result := TdxRtfContentExporter.Create(ADocumentModel, AOptions, ARtfExportHelper);
end;

destructor TdxRtfExporter.Destroy;
begin
  FreeAndNil(FRtfBuilder);
  FreeAndNil(FContentExporter);
  FRtfExportHelper := nil;
  inherited Destroy;
end;

function TdxRtfExporter.EscapeFontName(const AFontName: string;
  AEncoding: TEncoding): string;
begin
  NotImplemented;
  Result := '';
end;

function TdxRtfExporter.Export: string;
begin
  ExportCore;
  Result := RtfBuilder.RtfContent.ToString;
end;

procedure TdxRtfExporter.ExportColorTable;
begin
  RtfBuilder.OpenGroup;
  RtfBuilder.WriteCommand(TdxRtfExportSR.ColorTable);
  TdxListHelper.ForEach<TColor>(RtfExportHelper.ColorCollection, ExportColorTableEntry);
  RtfBuilder.CloseGroup;
end;

procedure TdxRtfExporter.ExportColorTableEntry(AColor: TColor);
begin
  if AColor <> 0 then
  begin
    AColor := ColorToRGB(AColor);
    RtfBuilder.WriteCommand(TdxRtfExportSR.ColorRed, GetRValue(AColor));
    RtfBuilder.WriteCommand(TdxRtfExportSR.ColorGreen, GetGValue(AColor));
    RtfBuilder.WriteCommand(TdxRtfExportSR.ColorBlue, GetBValue(AColor));
  end;
  RtfBuilder.WriteTextDirect(';');
end;

procedure TdxRtfExporter.ExportCore;
var
  AContent: TdxChunkedStringBuilder;
begin
  ContentExporter.Export;
  AContent := ContentExporter.RtfBuilder.RtfContent;

  RtfBuilder.OpenGroup;
  RtfBuilder.WriteCommand(TdxRtfExportSR.RtfSignature);
  RtfBuilder.WriteCommand(TdxRtfExportSR.DefaultFontIndex, RtfExportHelper.DefaultFontIndex);

  ExportFontTable;
  ExportColorTable;
  ExportDefaultProperties;
  ExportListTable;
  ExportListOverrideTable;
  ExportStyleTable;
  ExportParagraphGroupProperties;
  ExportUsersTable;
  ExportDocumentVariables;

  RtfBuilder.WriteTextDirectUnsafe(AContent);
  RtfBuilder.CloseGroup;
end;

procedure TdxRtfExporter.ExportDefaultCharacterProperties;
begin
  if Length(RtfExportHelper.DefaultCharacterProperties) = 0 then
    Exit;
  RtfBuilder.OpenGroup;
  RtfBuilder.WriteCommand(TdxRtfExportSR.DefaultCharacterProperties);
  RtfBuilder.WriteTextDirect(RtfExportHelper.DefaultCharacterProperties);
  RtfBuilder.CloseGroup;
end;

procedure TdxRtfExporter.ExportDefaultParagraphProperties;
begin
  if Length(RtfExportHelper.DefaultParagraphProperties) = 0 then
    Exit;
  RtfBuilder.OpenGroup;
  RtfBuilder.WriteCommand(TdxRtfExportSR.DefaultParagraphProperties);
  RtfBuilder.WriteTextDirect(RtfExportHelper.DefaultParagraphProperties);
  RtfBuilder.CloseGroup;
end;

procedure TdxRtfExporter.ExportDefaultProperties;
begin
  ExportDefaultCharacterProperties;
  ExportDefaultParagraphProperties;
end;

procedure TdxRtfExporter.ExportDocumentVariables;
var
  AVariables: TdxDocumentVariableCollection;
  AName: string;
begin
  AVariables := ContentExporter.DocumentModel.Variables;
  if AVariables.Count = 0 then
    Exit;

  for AName in AVariables.GetVariableNames do
  begin
    RtfBuilder.OpenGroup;
    RtfBuilder.WriteCommand(TdxRtfExportSR.DocumentVariable);

    RtfBuilder.OpenGroup;
    RtfBuilder.WriteText(AName);
    RtfBuilder.CloseGroup;

    RtfBuilder.OpenGroup;
    RtfBuilder.WritePCData(AVariables[AName].ToString);
    RtfBuilder.CloseGroup;

    RtfBuilder.CloseGroup;
  end;
end;

procedure TdxRtfExporter.ExportFontTable;
var
  AFontNames: TList<string>;
  I: Integer;
begin
  RtfBuilder.OpenGroup;
  RtfBuilder.WriteCommand(TdxRtfExportSR.FontTable);

  AFontNames := RtfExportHelper.FontNamesCollection;
  for I := 0 to AFontNames.Count - 1 do
    ExportFontTableEntry(AFontNames[I], I);

  RtfBuilder.CloseGroup;
end;

procedure TdxRtfExporter.ExportFontTableEntry(const AFontName: string;
  AFontIndex: Integer);
var
  AEncoding: TEncoding;
  AFontCharset: Integer;
begin
  RtfBuilder.OpenGroup;
  RtfBuilder.WriteCommand(TdxRtfExportSR.FontNumber, AFontIndex);

  AEncoding := CalculateFontNameEncoding(AFontName, AFontIndex);
  if AEncoding <> nil then
  begin
    AFontCharset := TdxEncoding.CharsetFromCodePage(AEncoding.CodePage);
    if AFontCharset = 0 then
    if not RtfExportHelper.FontCharsetTable.TryGetValue(AFontIndex, AFontCharset) then
      AFontCharset := 0;
    if AFontCharset > 1 then 
      RtfBuilder.WriteCommand(TdxRtfExportSR.FontCharset, AFontCharset);
  end
  else
    AEncoding := FContentExporter.Options.ActualEncoding;

  if (AEncoding = ContentExporter.Options.ActualEncoding) or TdxEncoding.CanBeLoselesslyEncoded(AFontName, TEncoding.ASCII) then
    RtfBuilder.WriteTextDirect(AFontName)
  else
    RtfBuilder.WriteTextDirect(EscapeFontName(AFontName, AEncoding));
  RtfBuilder.WriteTextDirect(';');
  RtfBuilder.CloseGroup;
end;

procedure TdxRtfExporter.ExportListOverrideTable;
var
  AOverrides: TList<string>;
  AValue: string;
begin
  RtfBuilder.OpenGroup;
  RtfBuilder.WriteCommand(TdxRtfExportSR.ListOverrideTable);

  AOverrides := RtfExportHelper.ListOverrideCollection;
  for AValue in AOverrides do
    RtfBuilder.WriteTextDirect(AValue);
  RtfBuilder.CloseGroup;
end;

procedure TdxRtfExporter.ExportListTable;
var
  AListCollection: TDictionary<Integer, string>;
  AValue: string;
begin
  AListCollection := RtfExportHelper.ListCollection;
  if AListCollection.Count = 0 then
    Exit;
  RtfBuilder.OpenGroup;
  RtfBuilder.WriteCommand(TdxRtfExportSR.NumberingListTable);
  for AValue in AListCollection.Values do
    RtfBuilder.WriteTextDirect(AValue);
  RtfBuilder.CloseGroup;
end;

procedure TdxRtfExporter.ExportParagraphGroupProperties;
begin
end;

function TdxRtfExporter.ExportSaveMemory: TdxChunkedStringBuilder;
begin
  ExportCore;
  Result := TdxChunkedStringBuilder.Create;
  Result.Append(RtfBuilder.RtfContent);
end;

procedure TdxRtfExporter.ExportStyleTable;
var
  AStyles: TList<string>;
  AValue: string;
begin
  RtfBuilder.OpenGroup;
  RtfBuilder.WriteCommand(TdxRtfExportSR.StyleTable);

  AStyles := RtfExportHelper.StylesCollection;
  for AValue in AStyles do
    RtfBuilder.WriteTextDirect(AValue);
  RtfBuilder.CloseGroup;
end;

procedure TdxRtfExporter.ExportUsersTable;
var
  AUsers: TList<string>;
  AValue: string;
begin
  AUsers := RtfExportHelper.UserCollection;
  if AUsers.Count = 0 then
    Exit;

  RtfBuilder.OpenGroup;
  RtfBuilder.WriteCommand(TdxRtfExportSR.UserTable);
  for AValue in AUsers do
  begin
    RtfBuilder.OpenGroup;
    RtfBuilder.WriteText(AValue);
    RtfBuilder.CloseGroup;
  end;
  RtfBuilder.CloseGroup;
end;

function TdxRtfExporter.GetLastParagraphRunNotSelected: Boolean;
begin
  Result := ContentExporter.LastParagraphRunNotSelected;
end;

procedure TdxRtfExporter.SetLastParagraphRunNotSelected(const Value: Boolean);
begin
  ContentExporter.LastParagraphRunNotSelected := Value;
end;

{ TdxRtfContentExporter }

function TdxRtfContentExporter.CalculateFirstExportedNumberingListsIndex(
  ANumberingLists: TdxNumberingListCollection): Integer;
var
  AFirstIndex: Integer;
  ALastIndex: Integer;
begin
  AFirstIndex := 0;
  ALastIndex := ANumberingLists.Count - 1;
  while AFirstIndex <= ALastIndex do
  begin
    if not ANumberingLists[AFirstIndex].CanRemove then
      Break;
    Inc(AFirstIndex);
  end;
  Result := AFirstIndex;
end;

function TdxRtfContentExporter.CalculateFirstExportedNumberingListsIndexForStyles(
  AParagraphStyleCollection: TdxParagraphStyleCollection): Integer;
var
  I: Integer;
  AStyle: TdxParagraphStyle;
  AStyleListIndex: Integer;
begin
  Result := MaxInt;
  for I := 0 to AParagraphStyleCollection.Count - 1 do
  begin
    AStyle := AParagraphStyleCollection[I];
    AStyleListIndex := AStyle.GetOwnNumberingListIndex;
    if AStyleListIndex >= 0 then
      Result := Min(Result, AStyleListIndex);
  end;
end;

constructor TdxRtfContentExporter.Create(ADocumentModel: TdxDocumentModel;
  AOptions: TdxRtfDocumentExporterOptions;
  ARtfExportHelper: IdxRtfExportHelper);
begin
  inherited Create(ADocumentModel);
  FRtfExportHelper := ARtfExportHelper;
  FOptions := AOptions;
  FRtfBuilder := CreateRtfBuilder;
  FParagraphPropertiesExporter := CreateParagraphPropertiesExporter;
  FCharacterPropertiesExporter := TdxRtfCharacterPropertiesExporter.Create(ADocumentModel, ARtfExportHelper, RtfBuilder, AOptions);
  FGetMergedCharacterPropertiesCachedResult := TdxRunMergedCharacterPropertiesCachedResult.Create;
end;

destructor TdxRtfContentExporter.Destroy;
begin
  FRtfExportHelper := nil;
  FreeAndNil(FGetMergedCharacterPropertiesCachedResult);
  FreeAndNil(FCharacterPropertiesExporter);
  FreeAndNil(FParagraphPropertiesExporter);
  FreeAndNil(FRtfBuilder);
  inherited Destroy;
end;

procedure TdxRtfContentExporter.EnsureValidFirstColor(AColor: TColor);
begin
//do nothing
end;

procedure TdxRtfContentExporter.Export;
begin
  if FOptions.ListExportFormat = TdxRtfNumberingListExportFormat.RtfFormat then
    ExportNumberingListTable;
  ExportDefaultCharacterProperties;
  ExportDefaultParagraphProperties;
  ExportStyleTable;
  PopulateUserTable;
  FRtfBuilder.Clear;
  ExportDocumentProperties;
  if FOptions.WrapContentInGroup then
    FRtfBuilder.OpenGroup;
  inherited Export;
  if FOptions.WrapContentInGroup then
    FRtfBuilder.CloseGroup;
end;

procedure TdxRtfContentExporter.ExportDefaultCharacterProperties;
var
  ACharacterPropertiesExporter: TdxRtfCharacterPropertiesExporter;
  ACharacterFormatting: TdxCharacterFormattingBase;
  AMergedCharacterProperties: TdxMergedCharacterProperties;
begin
  if RtfExportHelper.DefaultCharacterProperties <> '' then
    Exit;
  RtfBuilder.Clear;
  ACharacterPropertiesExporter := TdxRtfCharacterPropertiesExporter.Create(DocumentModel,
    RtfExportHelper, RtfBuilder, FOptions);
  try
    ACharacterFormatting := DocumentModel.DefaultCharacterProperties.Info;
    if (ACharacterFormatting.ForeColor <> clNone) and
        (ACharacterFormatting.ForeColor <> clBlack) then
      RtfExportHelper.GetColorIndex(clBlack);

    EnsureValidFirstColor(ACharacterFormatting.ForeColor);
    AMergedCharacterProperties :=  TdxMergedCharacterProperties.Create(ACharacterFormatting.Info, ACharacterFormatting.Options);
    try
      ACharacterPropertiesExporter.ExportCharacterProperties(AMergedCharacterProperties, True);
      RtfExportHelper.DefaultCharacterProperties := RtfBuilder.RtfContent.ToString;
    finally
      AMergedCharacterProperties.Free;
    end;
  finally
    ACharacterPropertiesExporter.Free;
  end;
end;

procedure TdxRtfContentExporter.ExportDefaultParagraphProperties;
var
  AParagraphPropertiesExporter: TdxRtfParagraphPropertiesExporter;
  AParagraphFormatting: TdxParagraphFormattingBase;
  AMergedParagraphProperties: TdxMergedParagraphProperties;
begin
  if RtfExportHelper.DefaultParagraphProperties <> '' then
    Exit;
  RtfBuilder.Clear;
  AParagraphPropertiesExporter := TdxRtfParagraphPropertiesExporter.Create(DocumentModel, RtfExportHelper, RtfBuilder);
  try
    AParagraphFormatting := DocumentModel.DefaultParagraphProperties.Info;
    AMergedParagraphProperties := TdxMergedParagraphProperties.Create(AParagraphFormatting.Info,
      AParagraphFormatting.Options);
    try
      AParagraphPropertiesExporter.ExportParagraphPropertiesCore(AMergedParagraphProperties, True);//true to prevent store emtpry default properties
      RtfExportHelper.DefaultParagraphProperties := RtfBuilder.RtfContent.ToString;
    finally
      AMergedParagraphProperties.Free;
    end;
  finally
    AParagraphPropertiesExporter.Free;
  end;
end;

procedure TdxRtfContentExporter.ExportDocumentProperties;
begin
  ExportFormattingFlags;

end;

procedure TdxRtfContentExporter.ExportFormattingFlags;
begin
  RtfBuilder.WriteCommand(TdxRtfExportSR.NoUICompatible);
  RtfBuilder.WriteCommand(TdxRtfExportSR.ShapeDoNotLay);
  RtfBuilder.WriteCommand(TdxRtfExportSR.HtmlAutoSpacing);
end;

procedure TdxRtfContentExporter.ExportInlinePictureRun(
  ARun: TdxInlinePictureRun);
var
  ACharacterProperties: TdxMergedCharacterProperties;
  AShouldExportCharacterProperties: Boolean;
  AExportStrategy: TdxRtfInlinePictureExportStrategy;
begin
  ACharacterProperties := ARun.GetMergedCharacterProperties(FGetMergedCharacterPropertiesCachedResult);
  AShouldExportCharacterProperties := TdxInlinePictureRun.ShouldExportInlinePictureRunCharacterProperties(ACharacterProperties.Info, ACharacterProperties.Options);
  if AShouldExportCharacterProperties then
  begin
    RtfBuilder.OpenGroup;
    FCharacterPropertiesExporter.ExportCharacterProperties(ACharacterProperties);//B181502
  end;
  AExportStrategy := TdxRtfInlinePictureExportStrategy.Create;
  try
    AExportStrategy.Export(FRtfBuilder, ARun, FOptions.Compatibility.DuplicateObjectAsMetafile);
  finally
    AExportStrategy.Free;
  end;
  if AShouldExportCharacterProperties then
    RtfBuilder.CloseGroup;
end;

procedure TdxRtfContentExporter.ExportNumberingListTable;
var
  AListExporter: TdxRtfNumberingListExporter;
  ANumberingLists: TdxNumberingListCollection;
  AStartIndex: Integer;
  ACount: Integer;
begin
  AListExporter := CreateNumberingListExporter(Self);
  try
    ANumberingLists := DocumentModel.NumberingLists;
    AStartIndex := CalculateFirstExportedNumberingListsIndex(ANumberingLists);
    AStartIndex := Min(AStartIndex,
      CalculateFirstExportedNumberingListsIndexForStyles(DocumentModel.ParagraphStyles));
    ACount := ANumberingLists.Count - AStartIndex;
    AListExporter.Export(ANumberingLists, AStartIndex, ACount);
  finally
    AListExporter.Free;
  end;
end;

function TdxRtfContentExporter.ExportParagraph(
  AParagraph: TdxParagraph): TdxParagraphIndex;
var
  ATablesAllowed: Boolean;
  AParagraphCell: TdxTableCell;
begin
  ATablesAllowed := DocumentModel.DocumentCapabilities.TablesAllowed;
  if ATablesAllowed then
    AParagraphCell := AParagraph.GetCell
  else
    AParagraphCell := nil;

  if AParagraphCell = nil then
  begin
    ExportSingleParagraph(AParagraph);
    Result := AParagraph.Index;
  end
  else
  begin
    Result := -1;
    NotImplemented;
  end;
end;

procedure TdxRtfContentExporter.ExportParagraphCharacterProperties(
  AParagraph: TdxParagraph);
var
  ARun: TdxTextRunBase;
  AProperties: TdxMergedCharacterProperties;
begin
  ARun := PieceTable.Runs[AParagraph.LastRunIndex];
  AProperties := ARun.GetMergedCharacterProperties(FGetMergedCharacterPropertiesCachedResult);
  FCharacterPropertiesExporter.ExportParagraphCharacterProperties(AProperties);
end;

procedure TdxRtfContentExporter.ExportParagraphCore(AParagraph: TdxParagraph;
  ATableNestingLevel: Integer;
  ACondTypes: Integer;
  ATableStyleIndex: Integer);
begin
  StartNewParagraph(AParagraph, ATableNestingLevel);
  ExportParagraphTableStyleProperties(ACondTypes, ATableStyleIndex);
  ExportParagraphRuns(AParagraph);
  ExportParagraphCharacterProperties(AParagraph);
end;

procedure TdxRtfContentExporter.ExportParagraphTableStyleProperties(
  ACondTypes: Integer;
  ATableStyleIndex: Integer);
begin
  if ATableStyleIndex < 0 then
    Exit;
  NotImplemented;
end;

procedure TdxRtfContentExporter.ExportSection(ASection: TdxSection);
begin
  StartNewSection(ASection);
  inherited ExportSection(ASection);
end;

procedure TdxRtfContentExporter.ExportSectionColumn(AColumn: TdxColumnInfo;
  AColumnIndex: Integer);
begin
  RtfBuilder.WriteCommand(TdxRtfExportSR.SectionColumnNumber, AColumnIndex + 1);
  RtfBuilder.WriteCommand(TdxRtfExportSR.SectionColumnWidth, UnitConverter.ModelUnitsToTwips(AColumn.Width));
  RtfBuilder.WriteCommand(TdxRtfExportSR.SectionColumnSpace, UnitConverter.ModelUnitsToTwips(AColumn.Space));
end;

procedure TdxRtfContentExporter.ExportSectionColumns(
  AColumns: TdxSectionColumns);
var
  ADefaultColumns: TdxColumnsInfo;
  AColumnsForExport: TdxColumnInfoCollection;
begin
  ADefaultColumns := PieceTable.DocumentModel.Cache.ColumnsInfoCache.DefaultItem;
  if AColumns.EqualWidthColumns then
  begin
    if AColumns.ColumnCount <> ADefaultColumns.ColumnCount then
      RtfBuilder.WriteCommand(TdxRtfExportSR.SectionColumnsCount, AColumns.ColumnCount);
    if AColumns.Space <> ADefaultColumns.Space then
      RtfBuilder.WriteCommand(TdxRtfExportSR.SectionSpaceBetweenColumns, UnitConverter.ModelUnitsToTwips(AColumns.Space));
  end
  else
  begin
    AColumnsForExport := AColumns.GetColumns;
    try
      ExportSectionColumnsDetails(AColumnsForExport);
    finally
      AColumnsForExport.Free;
    end;
  end;

  if AColumns.DrawVerticalSeparator then
    RtfBuilder.WriteCommand(TdxRtfExportSR.SectionColumnsDrawVerticalSeparator);
end;

procedure TdxRtfContentExporter.ExportSectionColumnsDetails(
  AColumns: TdxColumnInfoCollection);
var
  ACount: Integer;
  I: Integer;
begin
  ACount := AColumns.Count;
  if ACount > 0 then
    RtfBuilder.WriteCommand(TdxRtfExportSR.SectionColumnsCount, ACount);
  for I := 0 to ACount - 1 do
    ExportSectionColumn(AColumns[I], I);
end;

procedure TdxRtfContentExporter.ExportSectionEndNote(ANote: TdxSectionFootNote);
begin
end;

procedure TdxRtfContentExporter.ExportSectionFootNote(
  ANote: TdxSectionFootNote);
begin
end;

procedure TdxRtfContentExporter.ExportSectionGeneralSettings(
  ASettings: TdxSectionGeneralSettings);
begin
end;

procedure TdxRtfContentExporter.ExportSectionLineNumbering(
  ALineNumbering: TdxSectionLineNumbering);
var
  ADefaultLineNumbering: TdxLineNumberingInfo;
begin
  ADefaultLineNumbering := PieceTable.DocumentModel.Cache.LineNumberingInfoCache.DefaultItem;
  if ALineNumbering.Distance <> ADefaultLineNumbering.Distance then
    RtfBuilder.WriteCommand(TdxRtfExportSR.SectionLineNumberingDistance, UnitConverter.ModelUnitsToTwips(ALineNumbering.Distance));
  if ALineNumbering.Step <> ADefaultLineNumbering.Step then
    RtfBuilder.WriteCommand(TdxRtfExportSR.SectionLineNumberingStep, ALineNumbering.Step);

  if ALineNumbering.NumberingRestartType <> TdxLineNumberingRestart.NewPage then
  begin
    RtfBuilder.WriteCommand(TdxRtfExportSR.SectionLineNumberingStartingLineNumber, ALineNumbering.StartingLineNumber);
    if ALineNumbering.NumberingRestartType = TdxLineNumberingRestart.Continuous then
      RtfBuilder.WriteCommand(TdxRtfExportSR.SectionLineNumberingContinuous)
    else
      RtfBuilder.WriteCommand(TdxRtfExportSR.SectionLineNumberingRestartNewSection);
  end;
end;

procedure TdxRtfContentExporter.ExportSectionMargins(
  AMargins: TdxSectionMargins);
var
  ADefaultMargins: TdxMarginsInfo;
begin
  ADefaultMargins := PieceTable.DocumentModel.Cache.MarginsInfoCache.DefaultItem;
  if AMargins.Left <> ADefaultMargins.Left then
    RtfBuilder.WriteCommand(TdxRtfExportSR.SectionMarginsLeft, UnitConverter.ModelUnitsToTwips(AMargins.Left));
  if AMargins.Right <> ADefaultMargins.Right then
    RtfBuilder.WriteCommand(TdxRtfExportSR.SectionMarginsRight, UnitConverter.ModelUnitsToTwips(AMargins.Right));
  if AMargins.Top <> ADefaultMargins.Top then
    RtfBuilder.WriteCommand(TdxRtfExportSR.SectionMarginsTop, UnitConverter.ModelUnitsToTwips(AMargins.Top));
  if AMargins.Bottom <> ADefaultMargins.Bottom then
    RtfBuilder.WriteCommand(TdxRtfExportSR.SectionMarginsBottom, UnitConverter.ModelUnitsToTwips(AMargins.Bottom));
  if AMargins.HeaderOffset <> ADefaultMargins.HeaderOffset then
    RtfBuilder.WriteCommand(TdxRtfExportSR.SectionMarginsHeaderOffset, UnitConverter.ModelUnitsToTwips(AMargins.HeaderOffset));
  if AMargins.FooterOffset <> ADefaultMargins.FooterOffset then
    RtfBuilder.WriteCommand(TdxRtfExportSR.SectionMarginsFooterOffset, UnitConverter.ModelUnitsToTwips(AMargins.FooterOffset));
  if AMargins.Gutter <> ADefaultMargins.Gutter then
    RtfBuilder.WriteCommand(TdxRtfExportSR.SectionMarginsGutter, UnitConverter.ModelUnitsToTwips(AMargins.Gutter));
end;

procedure TdxRtfContentExporter.ExportSectionPage(APage: TdxSectionPage);
var
  ADefaultPage: TdxPageInfo;
begin
  ADefaultPage := PieceTable.DocumentModel.Cache.PageInfoCache.DefaultItem;
  if APage.Width <> ADefaultPage.Width then
    RtfBuilder.WriteCommand(TdxRtfExportSR.SectionPageWidth, UnitConverter.ModelUnitsToTwips(APage.Width));
  if APage.Height <> ADefaultPage.Height then
    RtfBuilder.WriteCommand(TdxRtfExportSR.SectionPageHeight, UnitConverter.ModelUnitsToTwips(APage.Height));
  if APage.Landscape then
    RtfBuilder.WriteCommand(TdxRtfExportSR.SectionPageLandscape);
  if (APage.PaperKind <> ADefaultPage.PaperKind) and (APage.PaperKind <> TdxPaperKind.Custom) then
    RtfBuilder.WriteCommand(TdxRtfExportSR.PaperKind, Ord(APage.PaperKind));
end;

procedure TdxRtfContentExporter.ExportSectionPageNumbering(
  APageNumbering: TdxSectionPageNumbering);
var
  ADefaultPageNumbering: TdxPageNumberingInfo;
begin
  ADefaultPageNumbering := PieceTable.DocumentModel.Cache.PageNumberingInfoCache.DefaultItem;
  if APageNumbering.ChapterSeparator <> ADefaultPageNumbering.ChapterSeparator then
    WriteChapterSeparator(APageNumbering.ChapterSeparator);
  if APageNumbering.ChapterHeaderStyle <> ADefaultPageNumbering.ChapterHeaderStyle then
    RtfBuilder.WriteCommand(TdxRtfExportSR.SectionChapterHeaderStyle, APageNumbering.ChapterHeaderStyle);

  if APageNumbering.StartingPageNumber <> ADefaultPageNumbering.StartingPageNumber then
  begin
    RtfBuilder.WriteCommand(TdxRtfExportSR.SectionPageNumberingStart, APageNumbering.StartingPageNumber);
    RtfBuilder.WriteCommand(TdxRtfExportSR.SectionPageNumberingRestart);
  end;

  if APageNumbering.NumberingFormat <> ADefaultPageNumbering.NumberingFormat then
    WritePageNumberingFormat(APageNumbering.NumberingFormat);
end;

procedure TdxRtfContentExporter.ExportSectionProperties(ASection: TdxSection);
begin
  ExportSectionMargins(ASection.Margins);
  ExportSectionPage(ASection.Page);
  ExportSectionGeneralSettings(ASection.GeneralSettings);
  ExportSectionPageNumbering(ASection.PageNumbering);
  ExportSectionLineNumbering(ASection.LineNumbering);
  ExportSectionColumns(ASection.Columns);
  ExportSectionFootNote(ASection.FootNote);
  ExportSectionEndNote(ASection.EndNote);
end;

procedure TdxRtfContentExporter.ExportSingleParagraph(AParagraph: TdxParagraph);
begin
  ExportParagraphCore(AParagraph, 0, 0, -1);
  if not (IsLastParagraph(AParagraph) and SuppressExportLastParagraph) then
    FinishParagraph(AParagraph);
end;

procedure TdxRtfContentExporter.ExportStyleTable;
var
  ADocumentModel: TdxDocumentModel;
  AStyleExporter: TdxRtfStyleExporter;
begin
  ADocumentModel := PieceTable.DocumentModel;
  AStyleExporter := CreateStyleExporter;
  try
    AStyleExporter.ExportStyleSheet(ADocumentModel.ParagraphStyles, ADocumentModel.CharacterStyles,
      ADocumentModel.TableStyles);
  finally
    AStyleExporter.Free;
  end;
end;

procedure TdxRtfContentExporter.ExportTextRun(ARun: TdxTextRun);
var
  AText: string;
  AProperties: TdxMergedCharacterProperties;
begin
  RtfBuilder.OpenGroup;
  AProperties := ARun.GetMergedCharacterProperties(FGetMergedCharacterPropertiesCachedResult);
  try
    FCharacterPropertiesExporter.ExportCharacterProperties(AProperties);
  finally
  end;
  WriteRunCharacterStyle(ARun);

  AText := ARun.GetPlainText(PieceTable.TextBuffer);
  WriteText(AText);
  RtfBuilder.CloseGroup;
end;

procedure TdxRtfContentExporter.FinishParagraph(AParagraph: TdxParagraph);
begin
  if (PieceTable.Runs[AParagraph.LastRunIndex] is TdxSectionRun) and
      DocumentModel.DocumentCapabilities.SectionsAllowed then
    RtfBuilder.WriteCommand(TdxRtfExportSR.SectionEndMark)
  else
    RtfBuilder.WriteCommand(TdxRtfExportSR.EndOfParagraph);
end;

function TdxRtfContentExporter.GetUnitConverter: TdxDocumentModelUnitConverter;
begin
  Result := DocumentModel.UnitConverter;
end;

function TdxRtfContentExporter.IsLastParagraph(
  AParagraph: TdxParagraph): Boolean;
begin
  Result := PieceTable.Paragraphs.Last = AParagraph;
end;

procedure TdxRtfContentExporter.PopulateUserTable;
begin
  if RtfExportHelper = nil then
    Exit;
end;

function TdxRtfContentExporter.ShouldExportHiddenText: Boolean;
begin
  Result := True;
end;

function TdxRtfContentExporter.ShouldWriteRunCharacterStyle(
  ARun: TdxTextRunBase): Boolean;
var
  AParagraphStyle: TdxParagraphStyle;
begin
  //TODO: Delete "&& RtfExportHelper.SupportStyle"
  if (ARun.CharacterStyleIndex = -1) or
      not RtfExportHelper.SupportStyle then
    Result := False
  else
  begin
    if ARun.Paragraph.ParagraphStyleIndex = -1 then
      Result := True
    else
    begin
      AParagraphStyle := ARun.Paragraph.ParagraphStyle;
      Result := not (AParagraphStyle.HasLinkedStyle and (AParagraphStyle.LinkedStyle = ARun.CharacterStyle));
    end;
  end;
end;

procedure TdxRtfContentExporter.StartNewInnerParagraph(AParagraph: TdxParagraph;
  ATableNestingLevel: Integer);
begin
  RtfBuilder.WriteCommand(TdxRtfExportSR.ResetCharacterFormatting);
  FParagraphPropertiesExporter.ExportParagraphProperties(AParagraph, ATableNestingLevel);
end;

procedure TdxRtfContentExporter.StartNewParagraph(AParagraph: TdxParagraph;
  ATableNestingLevel: Integer);
begin
  if AParagraph.IsInList then
    WriteAlternativeAndPlainText(AParagraph);
  RtfBuilder.WriteCommand(TdxRtfExportSR.ResetParagraphProperties);
  StartNewInnerParagraph(AParagraph, ATableNestingLevel);
end;

procedure TdxRtfContentExporter.StartNewSection(ASection: TdxSection);
begin
  if not DocumentModel.DocumentCapabilities.SectionsAllowed then
    Exit;
  RtfBuilder.WriteCommand(TdxRtfExportSR.ResetSectionProperties);
  ExportSectionProperties(ASection);
end;

function TdxRtfContentExporter.SuppressExportLastParagraph: Boolean;
begin
  Result := (Options.ExportFinalParagraphMark = TdxExportFinalParagraphMark.Never) or
    ((Options.ExportFinalParagraphMark = TdxExportFinalParagraphMark.SelectedOnly) and FLastParagraphRunNotSelected);
end;

procedure TdxRtfContentExporter.WriteAlternativeAndPlainText(
  AParagraph: TdxParagraph);
begin
  WriteAlternativeText(AParagraph);
end;

procedure TdxRtfContentExporter.WriteAlternativeText(AParagraph: TdxParagraph);
var
  ASeparator: string;
begin
  RtfBuilder.OpenGroup;
  RtfBuilder.WriteCommand(TdxRtfExportSR.AlternativeText);
  RtfBuilder.WriteCommand(TdxRtfExportSR.ResetParagraphProperties);
  WriteText(GetNumberingListText(AParagraph));
  ASeparator := AParagraph.GetListLevelSeparator;
  if ASeparator <> '' then
    WriteText(ASeparator);
  RtfBuilder.CloseGroup;
end;

procedure TdxRtfContentExporter.WriteChapterSeparator(ASeparator: Char);
begin
  NotImplemented;
end;

procedure TdxRtfContentExporter.WriteCharacterStyle(
  ACharacterStyle: TdxCharacterStyle);
var
  AStyleCollection: TDictionary<string, Integer>;
  AStyleName: string;
begin
  AStyleCollection := RtfExportHelper.CharacterStylesCollectionIndex;
  AStyleName := ACharacterStyle.StyleName;
  if AStyleCollection.ContainsKey(AStyleName) then
    RtfBuilder.WriteCommand(TdxRtfExportSR.CharacterStyleIndex, AStyleCollection[AStyleName]);
end;

procedure TdxRtfContentExporter.WritePageNumberingFormat(
  ANumberingFormat: TdxNumberingFormat);
begin
  NotImplemented;
end;

procedure TdxRtfContentExporter.WriteRunCharacterStyle(ARun: TdxTextRunBase);
begin
  if ShouldWriteRunCharacterStyle(ARun) then
    WriteCharacterStyle(ARun.CharacterStyle);
end;

procedure TdxRtfContentExporter.WriteSectionBreakType(
  ABreakType: TdxSectionStartType);
begin
  NotImplemented;
end;

procedure TdxRtfContentExporter.WriteText(const AText: string);
begin
  RtfBuilder.WriteText(AText);
end;

function TdxRtfContentExporter.CreateNumberingListExporter(
  AExporter: TdxRtfContentExporter): TdxRtfNumberingListExporter;
begin
  Result := TdxRtfNumberingListExporter.Create(AExporter);
end;

function TdxRtfContentExporter.CreateParagraphPropertiesExporter: TdxRtfParagraphPropertiesExporter;
begin
  Result := TdxRtfParagraphPropertiesExporter.Create(DocumentModel, RtfExportHelper, RtfBuilder)
end;

function TdxRtfContentExporter.CreateRtfBuilder: TdxRtfBuilder;
var
  AEncoding: TEncoding;
  AIsSingleByte: Boolean;
begin
  AEncoding := Options.ActualEncoding;
  AIsSingleByte := AEncoding.IsSingleByte;
  if AIsSingleByte then
    Result := TdxRtfBuilder.Create(AEncoding)
  else
    Result := TdxDBCSRtfBuilder.Create(AEncoding);
end;

function TdxRtfContentExporter.CreateStyleExporter: TdxRtfStyleExporter;
begin
  Result := TdxRtfStyleExporter.Create(PieceTable.DocumentModel, Self, RtfExportHelper, Options);
end;

{ TdxRtfPropertiesExporter }

constructor TdxRtfPropertiesExporter.Create(ADocumentModel: TdxDocumentModel;
  ARtfExportHelper: IdxRtfExportHelper; ARtfBuilder: TdxRtfBuilder);
begin
  inherited Create;
  FDocumentModel := ADocumentModel;
  FRtfExportHelper := ARtfExportHelper;
  FRtfBuilder := ARtfBuilder;
end;

function TdxRtfPropertiesExporter.GetUnitConverter: TdxDocumentModelUnitConverter;
begin
  Result := DocumentModel.UnitConverter;
end;

{ TdxRtfCharacterPropertiesExporter }

constructor TdxRtfCharacterPropertiesExporter.Create(
  ADocumentModel: TdxDocumentModel; ARtfExportHelper: IdxRtfExportHelper;
  ARtfBuilder: TdxRtfBuilder; AOptions: TdxRtfDocumentExporterOptions);
begin
  inherited Create(ADocumentModel, ARtfExportHelper, ARtfBuilder);
  FOptions := AOptions;
end;

class constructor TdxRtfCharacterPropertiesExporter.Initialize;
begin
  FDefaultUnderlineTypes := TDictionary<TdxUnderlineType, string>.Create;
  FDefaultUnderlineTypes.Add(TdxUnderlineType.None, '');
  FDefaultUnderlineTypes.Add(TdxUnderlineType.Single, TdxRtfExportSR.FontUnderline);
  FDefaultUnderlineTypes.Add(TdxUnderlineType.Dotted, TdxRtfExportSR.FontUnderlineDotted);
  FDefaultUnderlineTypes.Add(TdxUnderlineType.Dashed, TdxRtfExportSR.FontUnderlineDashed);
  FDefaultUnderlineTypes.Add(TdxUnderlineType.DashDotted, TdxRtfExportSR.FontUnderlineDashDotted);
  FDefaultUnderlineTypes.Add(TdxUnderlineType.DashDotDotted, TdxRtfExportSR.FontUnderlineDashDotDotted);
  FDefaultUnderlineTypes.Add(TdxUnderlineType.Double, TdxRtfExportSR.FontUnderlineDouble);
  FDefaultUnderlineTypes.Add(TdxUnderlineType.HeavyWave, TdxRtfExportSR.FontUnderlineHeavyWave);
  FDefaultUnderlineTypes.Add(TdxUnderlineType.LongDashed, TdxRtfExportSR.FontUnderlineLongDashed);
  FDefaultUnderlineTypes.Add(TdxUnderlineType.ThickSingle, TdxRtfExportSR.FontUnderlineThickSingle);
  FDefaultUnderlineTypes.Add(TdxUnderlineType.ThickDotted, TdxRtfExportSR.FontUnderlineThickDotted);
  FDefaultUnderlineTypes.Add(TdxUnderlineType.ThickDashed, TdxRtfExportSR.FontUnderlineThickDashed);
  FDefaultUnderlineTypes.Add(TdxUnderlineType.ThickDashDotted, TdxRtfExportSR.FontUnderlineThickDashDotted);
  FDefaultUnderlineTypes.Add(TdxUnderlineType.ThickDashDotDotted, TdxRtfExportSR.FontUnderlineThickDashDotDotted);
  FDefaultUnderlineTypes.Add(TdxUnderlineType.ThickLongDashed, TdxRtfExportSR.FontUnderlineThickLongDashed);
  FDefaultUnderlineTypes.Add(TdxUnderlineType.DoubleWave, TdxRtfExportSR.FontUnderlineDoubleWave);
  FDefaultUnderlineTypes.Add(TdxUnderlineType.Wave, TdxRtfExportSR.FontUnderlineWave);
end;

class destructor TdxRtfCharacterPropertiesExporter.Finalize;
begin
  FreeAndNil(FDefaultUnderlineTypes);
end;

procedure TdxRtfCharacterPropertiesExporter.ExportCharacterProperties(
  ACharacterProperties: TdxMergedCharacterProperties;
  ACheckDefaultColor: Boolean);
var
  AInfo: TdxCharacterFormattingInfo;
begin
  ExportCharacterPropertiesCore(ACharacterProperties);
  AInfo := ACharacterProperties.Info;
  WriteForegroundColor(AInfo.ForeColor, ACheckDefaultColor);
  if AInfo.BackColor <> clNone then
    WriteBackgroundColor(AInfo.BackColor);

  if AInfo.UnderlineColor <> clNone then
    WriteUnderlineColor(AInfo.UnderlineColor);
end;

procedure TdxRtfCharacterPropertiesExporter.ExportCharacterPropertiesCore(
  ACharacterProperties: TdxMergedCharacterProperties);
var
  AInfo: TdxCharacterFormattingInfo;
  AFontNameIndex: Integer;
begin
  AInfo := ACharacterProperties.Info;

  if AInfo.AllCaps then
    RtfBuilder.WriteCommand(TdxRtfExportSR.AllCapitals);
  if AInfo.Hidden then
    RtfBuilder.WriteCommand(TdxRtfExportSR.HiddenText);
  if AInfo.FontBold then
    RtfBuilder.WriteCommand(TdxRtfExportSR.FontBold);
  if AInfo.FontItalic then
    RtfBuilder.WriteCommand(TdxRtfExportSR.FontItalic);
  if AInfo.FontStrikeoutType <> TdxStrikeoutType.None then
  begin
    if AInfo.FontStrikeoutType = TdxStrikeoutType.Single then
      RtfBuilder.WriteCommand(TdxRtfExportSR.FontStrikeout)
    else
      RtfBuilder.WriteCommand(TdxRtfExportSR.FontDoubleStrikeout);
  end;
  if AInfo.Script <> TdxCharacterFormattingScript.Normal then
  begin
    if AInfo.Script = TdxCharacterFormattingScript.Subscript then
      RtfBuilder.WriteCommand(TdxRtfExportSR.RunSubScript)
    else
      RtfBuilder.WriteCommand(TdxRtfExportSR.RunSuperScript);
  end;
  if AInfo.UnderlineWordsOnly and (AInfo.FontUnderlineType = TdxUnderlineType.Single) then
    RtfBuilder.WriteCommand(TdxRtfExportSR.FontUnderlineWordsOnly)
  else
  begin
    if AInfo.FontUnderlineType <> TdxUnderlineType.None then
      WriteFontUnderline(AInfo.FontUnderlineType);
  end;
  AFontNameIndex := WriteFontName(AInfo.FontName);
  if AFontNameIndex >= 0 then
    RegisterFontCharset(AInfo, AFontNameIndex);
  WriteFontSize(AInfo.DoubleFontSize);
end;

procedure TdxRtfCharacterPropertiesExporter.ExportParagraphCharacterProperties(
  ACharacterProperties: TdxMergedCharacterProperties);
var
  AForeColor: TColor;
begin
  ExportCharacterPropertiesCore(ACharacterProperties);
  AForeColor := ACharacterProperties.Info.ForeColor;
  if AForeColor <> clNone then
    WriteForegroundColorCore(AForeColor);
end;

procedure TdxRtfCharacterPropertiesExporter.RegisterFontCharset(
  AInfo: TdxCharacterFormattingInfo; AFontNameIndex: Integer);
var
  ACharset: Integer;
  AFontInfoIndex: Integer;
  AFontInfo: TdxFontInfo;
  AFontStyle: TFontStyles;
begin
  if not RtfExportHelper.FontCharsetTable.TryGetValue(AFontNameIndex, ACharset) then
  begin
    AFontStyle := [];
    if AInfo.FontBold then
      Include(AFontStyle, fsBold);
    if AInfo.FontItalic then
      Include(AFontStyle, fsItalic);

    AFontInfoIndex := DocumentModel.FontCache.CalcFontIndex(AInfo.FontName,
      AInfo.DoubleFontSize, AFontStyle, AInfo.Script);
    AFontInfo := DocumentModel.FontCache[AFontInfoIndex];
    RtfExportHelper.FontCharsetTable.Add(AFontNameIndex, AFontInfo.Charset);
  end;
end;

procedure TdxRtfCharacterPropertiesExporter.WriteBackgroundColor(
  ABackColor: TColor);
var
  AIndex: Integer;
  AMode: TdxRtfRunBackColorExportMode;
begin
  if ABackColor <> clNone then
    ABackColor := RtfExportHelper.BlendColor(ABackColor);
  AIndex := RtfExportHelper.GetColorIndex(ABackColor);
  AMode := FOptions.Compatibility.BackColorExportMode;
  if AMode = TdxRtfRunBackColorExportMode.Chcbpat then
    RtfBuilder.WriteCommand(TdxRtfExportSR.RunBackgroundColor, AIndex)
  else
    if AMode = TdxRtfRunBackColorExportMode.Highlight then
      RtfBuilder.WriteCommand(TdxRtfExportSR.RunBackgroundColor2, AIndex)
    else
    begin
      RtfBuilder.WriteCommand(TdxRtfExportSR.RunBackgroundColor2, AIndex);
      RtfBuilder.WriteCommand(TdxRtfExportSR.RunBackgroundColor, AIndex);
    end;
end;

function TdxRtfCharacterPropertiesExporter.WriteFontName(
  AFontName: string): Integer;
begin
  Result := RtfExportHelper.GetFontNameIndex(AFontName);
  if Result = RtfExportHelper.DefaultFontIndex then
    Result := -1
  else
    RtfBuilder.WriteCommand(TdxRtfExportSR.FontNumber, Result);
end;

procedure TdxRtfCharacterPropertiesExporter.WriteFontSize(
  ARtfFontSize: Integer);
begin
  if ARtfFontSize = DefaultRtfFontSize then
    Exit;
  RtfBuilder.WriteCommand(TdxRtfExportSR.FontSize, ARtfFontSize);
end;

procedure TdxRtfCharacterPropertiesExporter.WriteFontUnderline(
  AUnderlineType: TdxUnderlineType);
var
  AKeyword: string;
begin
  if AUnderlineType = TdxUnderlineType.None then
    Exit;

  if not FDefaultUnderlineTypes.TryGetValue(AUnderlineType, AKeyword) or (AKeyword = '') then
    AKeyword := FDefaultUnderlineTypes[TdxUnderlineType.Single];
  RtfBuilder.WriteCommand(AKeyword);
end;

procedure TdxRtfCharacterPropertiesExporter.WriteForegroundColor(
  AForeColor: TColor; ACheckDefaultColor: Boolean = False);
begin
  WriteForegroundColorCore(AForeColor, ACheckDefaultColor);
end;

procedure TdxRtfCharacterPropertiesExporter.WriteForegroundColorCore(
  AForeColor: TColor; ACheckDefaultColor: Boolean = False);
var
  AIndex: Integer;
begin
  if AForeColor <> clNone then
    AForeColor := RtfExportHelper.BlendColor(AForeColor);
  AIndex := RtfExportHelper.GetColorIndex(AForeColor);
  if not ACheckDefaultColor or (AIndex <> 0) then
    RtfBuilder.WriteCommand(TdxRtfExportSR.RunForegroundColor, AIndex);
end;

procedure TdxRtfCharacterPropertiesExporter.WriteUnderlineColor(
  AUnderlineColor: TColor);
var
  AIndex: Integer;
begin
  AUnderlineColor := RtfExportHelper.BlendColor(AUnderlineColor);
  AIndex := RtfExportHelper.GetColorIndex(AUnderlineColor);
  RtfBuilder.WriteCommand(TdxRtfExportSR.RunUnderlineColor, AIndex);
end;

{ TdxRtfBuilder }

constructor TdxRtfBuilder.Create;
var
  I: Integer;
begin
  inherited Create;
  for I := 0 to 255 do
    FByteToHexString[I] := LowerCase(IntToHex(I, 2));
  FSpecialMarks := CreateSpecialMarksTable;
  FSpecialPCDataMarks := CreateSpecialPCDataMarksTable;
end;

constructor TdxRtfBuilder.Create(AEncoding: TEncoding);
begin
  Create;
  FEncoding := AEncoding;
  FRtfContent := TdxChunkedStringBuilder.Create;
  FUnicodeTextBuilder := TStringBuilder.Create;
end;

destructor TdxRtfBuilder.Destroy;
begin
  FreeAndNil(FRtfContent);
  FreeAndNil(FUnicodeTextBuilder);
  FreeAndNil(FSpecialPCDataMarks);
  FreeAndNil(FSpecialMarks);
  inherited;
end;

function TdxRtfBuilder.GetUnicodeCompatibleString(ACh: Char): string;
var
  ACode: Integer;
begin
  FUnicodeTextBuilder.Length := 0;
  ACode := Ord(ACh);
  if (ACode >= 0) and (ACode <= 127) then
  begin
    if IsSpecialSymbol(ACh) then
      FUnicodeTextBuilder.Append('\');
    FUnicodeTextBuilder.Append(ACh);
  end
  else
    AppendUnicodeCompatibleCharCore(ACode, ACh);
  Result := FUnicodeTextBuilder.ToString;
end;

function TdxRtfBuilder.GetUnicodeCompatibleStringDirect(
  const AText: string): string;
var
  ACh: Char;
  I, ALength, ACode: Integer;
begin
  FUnicodeTextBuilder.Length := 0;
  ALength := Length(AText);
  for I := 1 to ALength do
  begin
    ACh := AText[I];
    ACode := ShortInt(ACh);
    if (ACode >= 0) and (ACode <= 127) then
      FUnicodeTextBuilder.Append(ACh)
    else
      AppendUnicodeCompatibleCharCore(ACode, ACh);
  end;
  Result := FUnicodeTextBuilder.ToString;
end;

function TdxRtfBuilder.CreateSpecialMarksTable: TDictionary<Char, string>;
begin
  Result := TDictionary<Char, string>.Create;
  Result.Add(TdxCharacters.EmSpace, '\u8195\''3f');
  Result.Add(TdxCharacters.EnSpace, '\u8194\''3f');
  Result.Add(TdxCharacters.Hyphen, EmptyStr);
  Result.Add(TdxCharacters.LineBreak, '\line ');
  Result.Add(TdxCharacters.PageBreak, '\page ');
  Result.Add(TdxCharacters.ColumnBreak, '\column ');
  Result.Add(TdxCharacters.NonBreakingSpace, '\~');
  Result.Add(TdxCharacters.QmSpace, '\u8201\''3f');
  Result.Add(TdxCharacters.TabMark, '\tab ');
end;

function TdxRtfBuilder.CreateSpecialPCDataMarksTable: TDictionary<Char, string>;
var
  I: Integer;
begin
  Result := TDictionary<Char, string>.Create;
  for I := 0 to 30 do
    AddHexToMarkTable(Result, Char(I));
  AddHexToMarkTable(result, '\');
  AddHexToMarkTable(result, '{');
  AddHexToMarkTable(result, '}');
end;

procedure TdxRtfBuilder.IncreaseRowLength(ADelta: Integer);
var
  ACLRF: string;
begin
  FRowLength := FRowLength + ADelta;
  if (FRowLength >= RowLengthBound) and FHexMode then
  begin
    ACLRF := TdxRtfExportSR.CLRF;
    RtfContent.Append(@ACLRF);
    FIsPreviousWriteCommand := False;
    FRowLength := 0;
  end;
end;

function TdxRtfBuilder.IsSpecialSymbol(ACh: Char): Boolean;
begin
  Result := dxCharInSet(ACh, ['{', '}', '\', '%']);
end;

procedure TdxRtfBuilder.OpenGroup;
begin
  RtfContent.Append(TdxRtfExportSR.OpenGroup);
  FIsPreviousWriteCommand := False;
  IncreaseRowLength(Length(TdxRtfExportSR.OpenGroup));
end;

function TdxRtfBuilder.RowLengthBound: Integer;
begin
  Result := 200;
end;

procedure TdxRtfBuilder.AppendUnicodeCompatibleCharCore(ACode: Integer;
  ACh: Char);
var
  ABytes: TBytes;
begin
  ABytes := Encoding.GetBytes(ACh);
  FUnicodeTextBuilder.AppendFormat('\u%d\''%s', [ACode, FByteToHexString[ABytes[0]]]);
end;

procedure TdxRtfBuilder.Clear;
begin
  FRtfContent.Clear;
end;

procedure TdxRtfBuilder.CloseGroup;
begin
  RtfContent.Append(TdxRtfExportSR.CloseGroup);
  FIsPreviousWriteCommand := False;
  IncreaseRowLength(Length(TdxRtfExportSR.CloseGroup));
end;

procedure TdxRtfBuilder.WriteCommand(const ACommand: string);
begin
  RtfContent.Append(@ACommand);
  FIsPreviousWriteCommand := True;
  IncreaseRowLength(Length(ACommand));
end;

procedure TdxRtfBuilder.WriteCommand(const ACommand: string; AParam: Integer);
begin
  RtfContent.Append(@ACommand);
  RtfContent.Append(AParam);
  FIsPreviousWriteCommand := true;
  IncreaseRowLength(Length(ACommand));
end;

procedure TdxRtfBuilder.WriteByteArrayAsHex(ABuffer: TBytes; AOffset: Integer = 0;
  ALength: Integer = -1);
var
  ACount: Integer;
  I: Integer;
  AValue: Byte;
begin
  if ALength = -1 then
    ALength := Length(ABuffer);

  if FIsPreviousWriteCommand then
    FRtfContent.Append(TdxRtfExportSR.Space);
  FHexMode := True;
  ACount := AOffset + ALength;
  for I := AOffset to ACount - 1 do
  begin
    AValue := ABuffer[I];
    IncreaseRowLength(2);
    RtfContent.Append(@FByteToHexString[AValue]);
  end;
  FIsPreviousWriteCommand := True;
  FHexMode := False;
end;

procedure TdxRtfBuilder.WriteChar(ACh: Char);
var
  AStr: string;
begin
  if FIsPreviousWriteCommand then
    RtfContent.Append(TdxRtfExportSR.Space);
  AStr := GetUnicodeCompatibleString(ACh);
  RtfContent.Append(@AStr);
  FIsPreviousWriteCommand := False;
  IncreaseRowLength(Length(AStr));
end;

procedure TdxRtfBuilder.WriteCommand(const ACommand, AParam: string);
begin
  FRtfContent.Append(@ACommand);
  FRtfContent.Append(@AParam);
  FIsPreviousWriteCommand := True;
  IncreaseRowLength(Length(ACommand));
end;

procedure TdxRtfBuilder.WriteMemoryStreamAsHex(AStream: TBytesStream);
begin
  if AStream.Size > 0 then
    WriteByteArrayAsHex(AStream.Bytes, 0, AStream.Size)
  else
    WriteStreamAsHexCore(AStream);
end;

procedure TdxRtfBuilder.WritePCData(const AText: string);
begin
  WriteTextCore(AText, FSpecialPCDataMarks);
end;

procedure TdxRtfBuilder.WriteText(const AText: string);
begin
  WriteTextCore(AText, FSpecialMarks);
end;

procedure TdxRtfBuilder.WriteTextCore(const AText: string;
  ASpecialMarks: TDictionary<Char, string>);
var
  I: Integer;
  ACh: Char;
  ASpecialMark: string;
begin
  for I := 1 to Length(AText) do
  begin
    ACh := AText[I];
    if ASpecialMarks.TryGetValue(ACh, ASpecialMark) then
      WriteTextDirect(ASpecialMark)
    else
      WriteChar(ACh);
  end;
end;

procedure TdxRtfBuilder.WriteStreamAsHex(AStream: TStream);
begin
  if AStream is TBytesStream then
    WriteMemoryStreamAsHex(TBytesStream(AStream))
  else
    WriteStreamAsHexCore(AStream);
end;

procedure TdxRtfBuilder.WriteStreamAsHexCore(AStream: TStream);
var
  ABuffer: TBytes;
  ABufferLength, ALength: Integer;
begin
  SetLength(ABuffer, 4096);
  try
    ABufferLength := 4096;
    ALength := AStream.Size - AStream.Position;
    while ALength >= ABufferLength do
    begin
      AStream.Read(ABuffer, ABufferLength);
      Dec(ALength, ABufferLength);
      WriteByteArrayAsHex(ABuffer);
      FIsPreviousWriteCommand := False;
    end;
    if ALength > 0 then
    begin
      AStream.Read(ABuffer, ALength);
      WriteByteArrayAsHex(ABuffer, 0, ALength);
    end;
    FIsPreviousWriteCommand := True;
  finally
    SetLength(ABuffer, 0);
  end;
end;

procedure TdxRtfBuilder.WriteTextDirect(const AText: string;
  AMakeStringUnicodeCompatible: Boolean);
var
  S: string;
begin
  if FIsPreviousWriteCommand then
    RtfContent.Append(TdxRtfExportSR.Space);
  if AMakeStringUnicodeCompatible then
    S := GetUnicodeCompatibleStringDirect(AText)
  else
    S := AText;
  RtfContent.Append(@S);
  FIsPreviousWriteCommand := False;
  IncreaseRowLength(Length(AText));
end;

procedure TdxRtfBuilder.WriteTextDirectUnsafe(AText: TdxChunkedStringBuilder);
var
  ATextLength: Integer;
begin
  if FIsPreviousWriteCommand then
    RtfContent.Append(TdxRtfExportSR.Space);
  ATextLength := AText.Length;
  RtfContent.Append(AText);
  FIsPreviousWriteCommand := False;
  IncreaseRowLength(ATextLength);
end;

procedure TdxRtfBuilder.AddHexToMarkTable(ATable: TDictionary<Char, string>;
  ACh: Char);
begin
  ATable.Add(ACh, Format('\''%s', [IntToHex(Ord(ACh), 2)]));
end;

{ TdxDBCSRtfBuilder }

constructor TdxDBCSRtfBuilder.Create(AEncoding: TEncoding);
begin
  inherited Create(AEncoding);
  Assert(not Encoding.IsSingleByte);
end;

{ TdxRtfExportHelper }

function TdxRtfExportHelper.BlendColor(AColor: TColor): TColor;
begin
  Result := TdxColor.Blend(AColor, TdxColor.White);
end;

constructor TdxRtfExportHelper.Create;
begin
  inherited Create;
  FColorCollection := TList<TColor>.Create;
  FFontNamesCollection := TList<string>.Create;
  FListCollection := TDictionary<Integer, string>.Create;
  FListOverrideCollectionIndex := TDictionary<Integer, Integer>.Create;
  FListOverrideCollection := TList<string>.Create;
  FDefaultFontName := TdxRichEditControlCompatibility.DefaultFontName;
  FDefaultFontIndex := GetFontNameIndex(FDefaultFontName);
  FParagraphStylesCollectionIndex := TDictionary<string, Integer>.Create;
  FCharacterStylesCollectionIndex := TDictionary<string, Integer>.Create;
  FTableStylesCollectionIndex := TDictionary<string, Integer>.Create;
  FFontCharsetTable := TDictionary<Integer, Integer>.Create;
  FStylesCollection := TList<string>.Create;
  FUserCollection := TList<string>.Create;
end;

destructor TdxRtfExportHelper.Destroy;
begin
  FreeAndNil(FColorCollection);
  FreeAndNil(FFontNamesCollection);
  FreeAndNil(FListCollection);
  FreeAndNil(FListOverrideCollectionIndex);
  FreeAndNil(FListOverrideCollection);
  FreeAndNil(FParagraphStylesCollectionIndex);
  FreeAndNil(FCharacterStylesCollectionIndex);
  FreeAndNil(FTableStylesCollectionIndex);
  FreeAndNil(FFontCharsetTable);
  FreeAndNil(FStylesCollection);
  FreeAndNil(FUserCollection);
  inherited;
end;

function TdxRtfExportHelper.GetCharacterStylesCollectionIndex: TDictionary<string, Integer>;
begin
  Result := FCharacterStylesCollectionIndex;
end;

function TdxRtfExportHelper.GetColorCollection: TList<TColor>;
begin
  Result := FColorCollection;
end;

function TdxRtfExportHelper.GetColorIndex(AColor: TColor): Integer;
begin
  if AColor = clNone then
    Result := 0
  else
  begin
    Result := FColorCollection.IndexOf(AColor);
    if Result < 0 then
      Result := FColorCollection.Add(AColor);
  end;
end;

function TdxRtfExportHelper.GetDefaultCharacterProperties: string;
begin
  Result := FDefaultCharacterProperties;
end;

function TdxRtfExportHelper.GetDefaultFontIndex: Integer;
begin
  Result := FDefaultFontIndex;
end;

function TdxRtfExportHelper.GetDefaultParagraphProperties: string;
begin
  Result := FDefaultParagraphProperties;
end;

function TdxRtfExportHelper.GetFontCharsetTable: TDictionary<Integer, Integer>;
begin
  Result := FFontCharsetTable;
end;

function TdxRtfExportHelper.GetFontNamesCollection: TList<string>;
begin
  Result := FFontNamesCollection;
end;

function TdxRtfExportHelper.GetFontNameIndex(const AName: string): Integer;
begin
  Result := FFontNamesCollection.IndexOf(AName);
  if Result < 0 then
    Result := FFontNamesCollection.Add(AName);
end;

function TdxRtfExportHelper.GetListCollection: TDictionary<Integer, string>;
begin
  Result := FListCollection;
end;

function TdxRtfExportHelper.GetListOverrideCollection: TList<string>;
begin
  Result := FListOverrideCollection;
end;

function TdxRtfExportHelper.GetListOverrideCollectionIndex: TDictionary<Integer, Integer>;
begin
  Result := FListOverrideCollectionIndex;
end;

function TdxRtfExportHelper.GetParagraphStylesCollectionIndex: TDictionary<string, Integer>;
begin
  Result := FParagraphStylesCollectionIndex;
end;

function TdxRtfExportHelper.GetStylesCollection: TList<string>;
begin
  Result := FStylesCollection;
end;

function TdxRtfExportHelper.GetSupportStyle: Boolean;
begin
  Result := True;
end;

function TdxRtfExportHelper.GetUserCollection: TList<string>;
begin
  Result := FUserCollection;
end;

procedure TdxRtfExportHelper.SetDefaultCharacterProperties(const Value: string);
begin
  FDefaultCharacterProperties := Value;
end;

procedure TdxRtfExportHelper.SetDefaultParagraphProperties(const Value: string);
begin
  FDefaultParagraphProperties := Value;
end;

{ TdxRtfNumberingListExporter }

constructor TdxRtfNumberingListExporter.Create(
  ARtfExporter: TdxRtfContentExporter);
begin
  inherited Create;
  FRtfExporter := ARtfExporter;
  FRtfBuilder := FRtfExporter.CreateRtfBuilder;
  FCharacterPropertiesExporter := TdxRtfCharacterPropertiesExporter.Create(FRtfExporter.DocumentModel,
    FRtfExporter.RtfExportHelper, FRtfBuilder, FRtfExporter.Options);
  FParagraphPropertiesExporter := TdxRtfParagraphPropertiesExporter.Create(FRtfExporter.DocumentModel,
    FRtfExporter.RtfExportHelper, FRtfBuilder);
end;

destructor TdxRtfNumberingListExporter.Destroy;
begin
  FreeAndNil(FRtfBuilder);
  FreeAndNil(FCharacterPropertiesExporter);
  FreeAndNil(FParagraphPropertiesExporter);
  inherited Destroy;
end;

function TdxRtfNumberingListExporter.AddChar(ACh: Char; I: Integer): Integer;
begin
  Text := Text + ACh;
  Inc(FTextLength);
  Result := I + 1;
end;

function TdxRtfNumberingListExporter.AddEscapedChar(ACh: Char; I: Integer): Integer;
var
  S: string;
begin
  S := '\''' + RtfBuilder.FByteToHexString[Ord(ACh) mod 256];
  Text := Text + S ;
  Inc(FTextLength);
  Result := I + 1;
end;

function TdxRtfNumberingListExporter.AddLevelNumber(const ADisplayFormatString: string; I: Integer): Integer;
begin
  if DoubleBrackets(ADisplayFormatString, I) then
    Exit(AddEscapedChar(ADisplayFormatString[I], I));
  if ADisplayFormatString[I] = '\' then
    Exit(AddEscapedChar(ADisplayFormatString[I], I));

  Result := AddLevelNumberCore(ADisplayFormatString, I);
end;

function TdxRtfNumberingListExporter.AddLevelNumberCore(const ADisplayFormatString: string; I: Integer): Integer;

  function GetEndIndex(AStartIndex: Integer; out AEndIndex, AItemIndex: Integer): Boolean;
  var
    I: Integer;
    AChar: Char;
  begin
    AEndIndex := AStartIndex + 1;
    AItemIndex := -1;
    for I := AStartIndex + 1 to Length(ADisplayFormatString) do
    begin
      AChar := ADisplayFormatString[I];
      if (AChar >= '0') and (AChar <= '9') then
      begin
        if AItemIndex = -1 then
          AItemIndex := 0;
        AItemIndex := AItemIndex * 10 + (Ord(AChar) - Ord('0'));
      end
      else
      begin
        if AChar = ':' then
          AEndIndex := I + 1
        else
          AItemIndex := -1;
        Break;
      end;
    end;
    AChar := ADisplayFormatString[AEndIndex];
    Result := (AChar = 'S') or (AChar = 's');
  end;

var
  AEndIndex, AValue, ALength: Integer;
begin
  if GetEndIndex(I, AEndIndex, AValue) then
  begin
    Assert(AValue >= 0);
    if AValue < 0 then
      AValue := 0; 
    case AValue of
      0..9:
        ALength := 1;
      10..99:
        ALength := 2;
      else
        ALength := Length(IntToStr(AValue));
    end;
    Text := Format('%s\''%.2d', [Text, AValue]);         
    Inc(FTextLength, ALength);
    Number := Format('%s\''%.2d', [Number, TextLength]); 
    Result := AEndIndex + 1;
  end
  else
    Result := I + 1;
end;

class function TdxRtfNumberingListExporter.DoubleBrackets(const ADisplayFormatString: string; I: Integer): Boolean;
begin
  Result := (ADisplayFormatString[I] = '%') and (ADisplayFormatString[I + 1] = '%');
end;

procedure TdxRtfNumberingListExporter.Export(
  ANumberingLists: TdxNumberingListCollection; AStartIndex, ACount: Integer);
var
  AAbstractNumberingLists: TdxAbstractNumberingListCollection;
begin
  if ANumberingLists.Count <= 0 then
    Exit;
  AAbstractNumberingLists := GetAbstractNumberingLists(ANumberingLists, AStartIndex, ACount);
  ExportNumberingListTable(AAbstractNumberingLists);
  ExportListOverrideTable(ANumberingLists, AStartIndex, ACount);
end;

procedure TdxRtfNumberingListExporter.ExportAbstractListLevelParagraphStyleIndex(AAbstractListLevel: TdxListLevel);
var
  AParagraphStyleIndex: Integer;
  AParagraphStyles: TdxParagraphStyleCollection;
begin
  AParagraphStyles := FRtfExporter.DocumentModel.ParagraphStyles;
  AParagraphStyleIndex := AAbstractListLevel.ParagraphStyleIndex;
  if (AParagraphStyleIndex < 0) or (AParagraphStyleIndex >= AParagraphStyles.Count) then
    Exit;
  NotImplemented;
end;

procedure TdxRtfNumberingListExporter.ExportAbstractNumberingList(AList: TdxAbstractNumberingList);
begin
  if AList.Id = -2 then
    Exit;

  FRtfBuilder.OpenGroup();
  FRtfBuilder.WriteCommand(TdxRtfExportSR.NumberingList);
  FRtfBuilder.WriteCommand(TdxRtfExportSR.NumberingListTemplateId, -1);
  if TdxNumberingListHelper.IsHybridList(AList) then
    FRtfBuilder.WriteCommand(TdxRtfExportSR.NumberingListHybrid);
  ExportListLevels(AList.Levels);
  FRtfBuilder.OpenGroup;
  FRtfBuilder.WriteCommand(TdxRtfExportSR.NumberingListName);
  FRtfBuilder.CloseGroup;
  FRtfBuilder.WriteCommand(TdxRtfExportSR.NumberingListId, AList.Id);
  FRtfBuilder.CloseGroup;
  if not FRtfExporter.RtfExportHelper.ListCollection.ContainsKey(AList.Id) then
    FRtfExporter.RtfExportHelper.ListCollection.Add(AList.Id, FRtfBuilder.RtfContent.ToString);
end;

procedure TdxRtfNumberingListExporter.ExportAbstractNumberingLists(
  AAbstractNumberingLists: TdxAbstractNumberingListCollection);
var
  I: Integer;
  ACount: TdxAbstractNumberingListIndex;
begin
  ACount := AAbstractNumberingLists.Count;
  for I := 0 to ACount - 1 do
  begin
    FRtfBuilder.Clear;
    ExportAbstractNumberingList(AAbstractNumberingLists[I]);
  end;
end;

procedure TdxRtfNumberingListExporter.ExportListLevel(AListLevel: IdxListLevel);
var
  ANumberingListFormat: Integer;
begin
  ANumberingListFormat := GetNumberingListFormat(AListLevel.ListLevelProperties.Format);
  FRtfBuilder.OpenGroup;
  FRtfBuilder.WriteCommand(TdxRtfExportSR.ListLevel);
  FRtfBuilder.WriteCommand(TdxRtfExportSR.ListLevelNumberingFormat, ANumberingListFormat);
  FRtfBuilder.WriteCommand(TdxRtfExportSR.ListLevelNumberingFormatN, ANumberingListFormat);
  FRtfBuilder.WriteCommand(TdxRtfExportSR.ListLevelAlignment, Ord(AListLevel.ListLevelProperties.Alignment));
  FRtfBuilder.WriteCommand(TdxRtfExportSR.ListLevelAlignmentN, Ord(AListLevel.ListLevelProperties.Alignment));
  FRtfBuilder.WriteCommand(TdxRtfExportSR.ListLevelFollow, GetListLevelSeparator(AListLevel.ListLevelProperties.Separator));
  FRtfBuilder.WriteCommand(TdxRtfExportSR.ListLevelStart, AListLevel.ListLevelProperties.Start);
  if AListLevel.ListLevelProperties.Legacy then
  begin
    FRtfBuilder.WriteCommand(TdxRtfExportSR.ListLevelLegacy);
    FRtfBuilder.WriteCommand(TdxRtfExportSR.ListLevelLegacySpace,
      AListLevel.DocumentModel.UnitConverter.ModelUnitsToTwips(AListLevel.ListLevelProperties.LegacySpace));
    FRtfBuilder.WriteCommand(TdxRtfExportSR.ListLevelLegacyIndent,
      AListLevel.DocumentModel.UnitConverter.ModelUnitsToTwips(AListLevel.ListLevelProperties.LegacyIndent));
  end;
  ExportListLevelTextAndNumber(AListLevel.ListLevelProperties.DisplayFormatString, AListLevel.ListLevelProperties.TemplateCode);

  if AListLevel.ListLevelProperties.ConvertPreviousLevelNumberingToDecimal then
    FRtfBuilder.WriteCommand(TdxRtfExportSR.ListLevelLegal, 1)
  else
    FRtfBuilder.WriteCommand(TdxRtfExportSR.ListLevelLegal, 0);

  if AListLevel.ListLevelProperties.SuppressRestart then
    FRtfBuilder.WriteCommand(TdxRtfExportSR.ListLevelNoRestart, 1)
  else
    FRtfBuilder.WriteCommand(TdxRtfExportSR.ListLevelNoRestart, 0);

  ExportListLevelCharacterAndParagraphProperties(AListLevel);
  FRtfBuilder.CloseGroup;
end;

procedure TdxRtfNumberingListExporter.ExportListLevelCharacterAndParagraphProperties(AListLevel: IdxListLevel);
var
  AMerger: TdxCharacterPropertiesMerger;
  AParagraphMerger: TdxParagraphPropertiesMerger;
begin
  AMerger := TdxCharacterPropertiesMerger.Create(AListLevel.CharacterProperties);
  try
    AMerger.Merge(FRtfExporter.DocumentModel.DefaultCharacterProperties);
    FCharacterPropertiesExporter.ExportCharacterProperties(AMerger.MergedProperties);
  finally
    AMerger.Free;
  end;
  if AListLevel is TdxListLevel then
    ExportAbstractListLevelParagraphStyleIndex(TdxListLevel(AListLevel));
  AParagraphMerger := TdxParagraphPropertiesMerger.Create(AListLevel.ParagraphProperties);
  try
    AParagraphMerger.Merge(FRtfExporter.DocumentModel.DefaultParagraphProperties);
    FParagraphPropertiesExporter.WriteParagraphIndents(AParagraphMerger.MergedProperties);
  finally
    AParagraphMerger.Free;
  end;
end;

procedure TdxRtfNumberingListExporter.ExportListLevels(AListLevelCollection: TdxListLevelCollection);
var
  I: Integer;
begin
  for I := 0 to AListLevelCollection.Count - 1 do
    ExportListLevel(AListLevelCollection[I]);
end;

procedure TdxRtfNumberingListExporter.ExportListLevelTextAndNumber(const ADisplayFormatString: string;
  ALevelTemplateId: Integer);
begin
  FText := EmptyStr;
  FNumber := EmptyStr;
  GetTextAndNumber(ADisplayFormatString);
  FRtfBuilder.OpenGroup;
  FRtfBuilder.WriteCommand(TdxRtfExportSR.ListLevelText);
  if ALevelTemplateId <> 0 then
    FRtfBuilder.WriteCommand(TdxRtfExportSR.LevelTemplateId, ALevelTemplateId);
  FRtfBuilder.WriteTextDirect(Format('\''%.2d', [FTextLength]));
  FRtfBuilder.WriteTextDirect(FText, True);
  FRtfBuilder.WriteChar(';');
  FRtfBuilder.CloseGroup;
  FRtfBuilder.OpenGroup;
  FRtfBuilder.WriteCommand(TdxRtfExportSR.ListLevelNumbers, FNumber + ';');
  FRtfBuilder.CloseGroup;
end;

procedure TdxRtfNumberingListExporter.ExportListOverride(ANumberingList: TdxNumberingList);
var
  AListOverrideCount, AIndex: Integer;
begin
  FRtfBuilder.OpenGroup;
  FRtfBuilder.WriteCommand(TdxRtfExportSR.ListOverride);
  FRtfBuilder.WriteCommand(TdxRtfExportSR.NumberingListId, ANumberingList.AbstractNumberingList.Id);
  AListOverrideCount := GetListOverrideCount(ANumberingList);
  FRtfBuilder.WriteCommand(TdxRtfExportSR.ListOverrideCount, AListOverrideCount);
  WriteListOverrideId(ANumberingList);
  ExportListOverrideLevels(ANumberingList);
  FRtfBuilder.CloseGroup;

  AIndex := FRtfExporter.RtfExportHelper.ListOverrideCollection.Count;
  FRtfExporter.RtfExportHelper.ListOverrideCollection.Add(FRtfBuilder.RtfContent.ToString);
  if FRtfExporter.RtfExportHelper.ListOverrideCollectionIndex.ContainsKey(ANumberingList.Id) then
    FRtfExporter.RtfExportHelper.ListOverrideCollectionIndex.Add(ANumberingList.Id, AIndex);
end;

procedure TdxRtfNumberingListExporter.ExportListOverrideLevel(ALevel: TdxAbstractListLevel);
begin
  FRtfBuilder.OpenGroup;
  FRtfBuilder.WriteCommand(TdxRtfExportSR.ListOverrideLevel);
  if ALevel.OverrideStart then
    FRtfBuilder.WriteCommand(TdxRtfExportSR.ListOverrideStart);
  if ALevel is TdxOverrideListLevel then
  begin
    FRtfBuilder.WriteCommand(TdxRtfExportSR.ListOverrideFormat);
    ExportListLevel(ALevel);
  end
  else
    FRtfBuilder.WriteCommand(TdxRtfExportSR.ListLevelStart, ALevel.NewStart);
  FRtfBuilder.CloseGroup;
end;

procedure TdxRtfNumberingListExporter.ExportListOverrideLevels(ANumberingList: TdxNumberingList);
var
  I: Integer;
  ALevels: TdxListLevelCollection;
begin
  ALevels := ANumberingList.Levels;
  for I := 0 to ALevels.Count - 1 do
    if IsOverrideLevel(ALevels[I]) then
      ExportListOverrideLevel(ALevels[I]);
end;

procedure TdxRtfNumberingListExporter.ExportListOverrideTable(ANumberingLists: TdxNumberingListCollection;
  AStartIndex: TdxNumberingListIndex; ACount: Integer);
var
  I: Integer;
  ALastIndex: TdxNumberingListIndex;
begin
  if ANumberingLists.Count <= 0 then
    Exit;

  ALastIndex := AStartIndex + (ACount - 1);
  for I := AStartIndex to ALastIndex do
  begin
    FRtfBuilder.Clear;
    ExportListOverride(ANumberingLists[I]);
  end;
end;

procedure TdxRtfNumberingListExporter.ExportListOverrideTable(ANumberingLists: TdxNumberingListCollection);
var
  I, ACount: TdxNumberingListIndex;
begin
  if ANumberingLists.Count <= 0 then
    Exit;

  ACount := ANumberingLists.Count;
  for I := 0 to ACount - 1 do
  begin
    RtfBuilder.Clear;
    ExportListOverride(ANumberingLists[I]);
  end;
end;

procedure TdxRtfNumberingListExporter.ExportNumberingListTable(
  AAbstractNumberingLists: TdxAbstractNumberingListCollection);
begin
  if AAbstractNumberingLists.Count > 0 then
  begin
    FRtfBuilder.Clear;
    ExportAbstractNumberingLists(AAbstractNumberingLists);
  end;
end;

function TdxRtfNumberingListExporter.GetAbstractNumberingLists(ANumberingLists: TdxNumberingListCollection;
  AStartIndex: TdxNumberingListIndex; ACount: Integer): TdxAbstractNumberingListCollection;
var
  I: Integer;
  AList: TdxAbstractNumberingList;
  ALastIndex: TdxNumberingListIndex;
begin
  Result := TdxAbstractNumberingListCollection.Create;
  ALastIndex := AStartIndex + (ACount - 1);
  for I := AStartIndex to ALastIndex do
  begin
    AList := ANumberingLists[I].AbstractNumberingList;
    if Result.IndexOf(AList) < 0 then
      Result.Add(AList);
  end;
end;

function TdxRtfNumberingListExporter.GetListLevelSeparator(ASeparator: Char): Integer;
begin
  case ASeparator of
    TdxCharacters.TabMark:
      Result := 0;
    ' ':
      Result := 1;
    else
      Result := 2;
  end;
end;

function TdxRtfNumberingListExporter.GetListOverrideCount(ANumberingList: TdxNumberingList): Integer;
var
  I: Integer;
  ALevels: TdxListLevelCollection;
begin
  ALevels := ANumberingList.Levels;
  Result := 0;
  for I := 0 to ALevels.Count - 1 do
    if IsOverrideLevel(ALevels[I]) then
      Inc(Result);
end;

function TdxRtfNumberingListExporter.GetNumberingListFormat(ANumberingFormat: TdxNumberingFormat): Integer;
begin
  Result := TdxListLevelDestination.NumberingFormats.IndexOf(ANumberingFormat);
  if Result < 0 then
    Result := 0;
end;

procedure TdxRtfNumberingListExporter.GetTextAndNumber(const ADisplayFormatString: string);
var
  ACh: Char;
  I, ACount: Integer;
begin
  TextLength := 0;
  ACount := Length(ADisplayFormatString);
  I := 1;
  while I <= ACount do
  begin
    ACh := ADisplayFormatString[I];
    if not RtfBuilder.IsSpecialSymbol(ACh) then
      I := AddChar(ACh, I)
    else
      I := AddLevelNumber(ADisplayFormatString, I);
  end;
end;

function TdxRtfNumberingListExporter.IsOverrideLevel(AListLevel: TdxAbstractListLevel): Boolean;
begin
  if AListLevel is TdxOverrideListLevel then
    Exit(True);
  Result := AListLevel.OverrideStart;
end;

procedure TdxRtfNumberingListExporter.WriteListOverrideId(ANumberingList: TdxNumberingList);
begin
  FRtfBuilder.WriteCommand(TdxRtfExportSR.ListIndex, ANumberingList.Id);
end;

{ TdxRtfParagraphPropertiesExporter }

procedure TdxRtfParagraphPropertiesExporter.WriteParagraphKeepLinesTogether(
  Value: Boolean);
begin
  if Value <> DefaultKeepLinesTogether then
    RtfBuilder.WriteCommand(TdxRtfExportSR.KeepLinesTogether, IfThen(Value, 1, 0));
end;

function TdxRtfParagraphPropertiesExporter.CalcRtfFirstLineIndent(
  AFirstLineIndentType: TdxParagraphFirstLineIndent;
  AFirstLineIndent: Integer): Integer;
begin
  case AFirstLineIndentType of
    TdxParagraphFirstLineIndent.Indented: Result := UnitConverter.ModelUnitsToTwips(AFirstLineIndent);
    TdxParagraphFirstLineIndent.Hanging: Result := -UnitConverter.ModelUnitsToTwips(AFirstLineIndent);
  else
    Result := 0;
  end;
end;

function TdxRtfParagraphPropertiesExporter.CalcRtfLeftIndent(
  AFirstLineIndentType: TdxParagraphFirstLineIndent; AFirstLineIndent,
  ALeftIndent: Integer): Integer;
begin
  Result := UnitConverter.ModelUnitsToTwips(ALeftIndent);
end;

function TdxRtfParagraphPropertiesExporter.CalcRtfRightIndent(
  ARightIndent: Integer): Integer;
begin
  Result := UnitConverter.ModelUnitsToTwips(ARightIndent);
end;

procedure TdxRtfParagraphPropertiesExporter.ExportParagraphNumberingProperties(
  AParagraph: TdxParagraph);
begin
  if not AParagraph.ShouldExportNumbering then
    Exit;
  RtfBuilder.WriteCommand(TdxRtfExportSR.LevelIndex, AParagraph.GetListLevelIndex);
  WriteParagraphListIndex(AParagraph.GetNumberingListIndex);
end;

procedure TdxRtfParagraphPropertiesExporter.ExportParagraphProperties(
  AParagraph: TdxParagraph; ATableNestingLevel: Integer);
var
  AProperties: TdxMergedParagraphProperties;
  ATabs: TdxTabFormattingInfo;
begin
  if AParagraph.IsInList then
    ExportParagraphNumberingProperties(AParagraph);
  if RtfExportHelper.SupportStyle and (AParagraph.ParagraphStyleIndex <> TdxParagraphStyleCollection.EmptyParagraphStyleIndex) then
    WriteParagraphStyle(AParagraph.ParagraphStyle);
  WriteParagraphAlignment(AParagraph.Alignment);
  WriteParagraphGroupPropertiesId(AParagraph);
  AProperties := AParagraph.GetMergedParagraphProperties;
  try
    WriteParagraphIndents(AProperties);
  finally
    AProperties.Free;
  end;
  WriteParagraphSuppressHyphenation(AParagraph.SuppressHyphenation);
  WriteParagraphSuppressLineNumbers(AParagraph.SuppressLineNumbers);
  WriteParagraphContextualSpacing(AParagraph.ContextualSpacing);
  WriteParagraphPageBreakBefore(AParagraph.PageBreakBefore);
  WriteParagraphBeforeAutoSpacing(AParagraph.BeforeAutoSpacing);
  WriteParagraphAfterAutoSpacing(AParagraph.AfterAutoSpacing);
  WriteParagraphKeepWithNext(AParagraph.KeepWithNext);
  WriteParagraphKeepLinesTogether(AParagraph.KeepLinesTogether);
  WriteParagraphWidowOrphanControl(AParagraph.WidowOrphanControl);
  WriteParagraphOutlineLevel(AParagraph.OutlineLevel);
  WriteParagraphBackColor(AParagraph.BackColor);
  WriteParagraphLineSpacing(AParagraph.LineSpacingType, AParagraph.LineSpacing);
  WriteParagraphSpacingBefore(AParagraph.SpacingBefore);
  WriteParagraphSpacingAfter(AParagraph.SpacingAfter);
  ATabs := AParagraph.GetTabs;
  try
    WriteParagraphTabs(ATabs);
  finally
    ATabs.Free;
  end;
end;

procedure TdxRtfParagraphPropertiesExporter.ExportParagraphPropertiesCore(
  AProperties: TdxMergedParagraphProperties; ACheckDefaultAlignment: Boolean);
var
  AInfo: TdxParagraphFormattingInfo;
begin
  AInfo := AProperties.Info;
  if not ACheckDefaultAlignment or (AInfo.Alignment <> TdxParagraphAlignment.Left) then
    WriteParagraphAlignment(AInfo.Alignment);
  WriteParagraphIndents(AProperties);
  WriteParagraphSuppressHyphenation(AInfo.SuppressHyphenation);
  WriteParagraphSuppressLineNumbers(AInfo.SuppressLineNumbers);
  WriteParagraphContextualSpacing(AInfo.ContextualSpacing);
  WriteParagraphPageBreakBefore(AInfo.PageBreakBefore);
  WriteParagraphBeforeAutoSpacing(AInfo.BeforeAutoSpacing);
  WriteParagraphAfterAutoSpacing(AInfo.AfterAutoSpacing);
  WriteParagraphKeepWithNext(AInfo.KeepWithNext);
  WriteParagraphKeepLinesTogether(AInfo.KeepLinesTogether);
  WriteParagraphWidowOrphanControl(AInfo.WidowOrphanControl);
  WriteParagraphOutlineLevel(AInfo.OutlineLevel);
  WriteParagraphBackColor(AInfo.BackColor);
  WriteParagraphLineSpacing(AInfo.LineSpacingType, AInfo.LineSpacing);
  WriteParagraphSpacingBefore(AInfo.SpacingBefore);
  WriteParagraphSpacingAfter(AInfo.SpacingAfter);
end;

procedure TdxRtfParagraphPropertiesExporter.WriteParagraphAfterAutoSpacing(
  Value: Boolean);
begin
  if Value <> DefaultAfterAutoSpacing then
    RtfBuilder.WriteCommand(TdxRtfExportSR.AfterAutoSpacing, IfThen(Value, 1, 0));
end;

procedure TdxRtfParagraphPropertiesExporter.WriteParagraphAlignment(
  Alignment: TdxParagraphAlignment);
begin
  case Alignment of
    TdxParagraphAlignment.Left: RtfBuilder.WriteCommand(TdxRtfExportSR.LeftAlignment);
    TdxParagraphAlignment.Center: RtfBuilder.WriteCommand(TdxRtfExportSR.CenterAlignment);
    TdxParagraphAlignment.Justify: RtfBuilder.WriteCommand(TdxRtfExportSR.JustifyAlignment);
    TdxParagraphAlignment.Right: RtfBuilder.WriteCommand(TdxRtfExportSR.RightAlignment);
  end;
end;

procedure TdxRtfParagraphPropertiesExporter.WriteParagraphBackColor(
  Value: TColor);
var
  AIndex: Integer;
begin
  if Value = clNone then
    Exit;
  AIndex := RtfExportHelper.GetColorIndex(Value);
  RtfBuilder.WriteCommand(TdxRtfExportSR.ParagraphBackgroundColor, AIndex);
end;

procedure TdxRtfParagraphPropertiesExporter.WriteParagraphBeforeAutoSpacing(
  Value: Boolean);
begin
  if Value <> DefaultBeforeAutoSpacing then
    RtfBuilder.WriteCommand(TdxRtfExportSR.BeforeAutoSpacing, IfThen(Value, 1 , 0));
end;

procedure TdxRtfParagraphPropertiesExporter.WriteParagraphContextualSpacing(
  Value: Boolean);
begin
  if Value then
    RtfBuilder.WriteCommand(TdxRtfExportSR.ContextualSpacing);
end;

procedure TdxRtfParagraphPropertiesExporter.WriteParagraphGroupPropertiesId(
  AParagraph: TdxParagraph);
begin
end;

procedure TdxRtfParagraphPropertiesExporter.WriteParagraphIndents(
  AMergedParagraphProperties: TdxMergedParagraphProperties);
var
  AParagraphPropertiesInfo: TdxParagraphFormattingInfo;
  AFirstLineIndent: Integer;
  ALeftIndent: Integer;
  ARightIndent: Integer;
begin
  AParagraphPropertiesInfo := AMergedParagraphProperties.Info;

  AFirstLineIndent := CalcRtfFirstLineIndent(AParagraphPropertiesInfo.FirstLineIndentType,
    AParagraphPropertiesInfo.FirstLineIndent);
  if AFirstLineIndent <> DefaultParagraphFirstLineIndent then
    RtfBuilder.WriteCommand(TdxRtfExportSR.FirstLineIndentInTwips, AFirstLineIndent);

  ALeftIndent := CalcRtfLeftIndent(AParagraphPropertiesInfo.FirstLineIndentType,
    AParagraphPropertiesInfo.FirstLineIndent,
    AParagraphPropertiesInfo.LeftIndent);
  if ALeftIndent <> DefaultParagraphLeftIndent then
  begin
    RtfBuilder.WriteCommand(TdxRtfExportSR.LeftIndentInTwips, ALeftIndent);
    RtfBuilder.WriteCommand(TdxRtfExportSR.LeftIndentInTwips_Lin, ALeftIndent);
  end;
  ARightIndent := CalcRtfRightIndent(AParagraphPropertiesInfo.RightIndent);
  if ARightIndent <> DefaultParagraphRightIndent then
  begin
    RtfBuilder.WriteCommand(TdxRtfExportSR.RightIndentInTwips, ARightIndent);
    RtfBuilder.WriteCommand(TdxRtfExportSR.RightIndentInTwips_Rin, ARightIndent);
  end;
end;

procedure TdxRtfParagraphPropertiesExporter.WriteParagraphKeepWithNext(
  Value: Boolean);
begin
  if Value <> DefaultKeepWithNext then
    RtfBuilder.WriteCommand(TdxRtfExportSR.KeepWithNext, IfThen(Value, 1, 0));
end;

procedure TdxRtfParagraphPropertiesExporter.WriteParagraphLineSpacing(
  AParagraphLineSpacingType: TdxParagraphLineSpacing;
  AParagraphLineSpacing: Single);
begin
  case AParagraphLineSpacingType of
    TdxParagraphLineSpacing.AtLeast:
      WriteRtfLineSpacing(UnitConverter.ModelUnitsToTwips(Trunc(AParagraphLineSpacing)), AtLeastLineSpacingMultiple);
    TdxParagraphLineSpacing.Exactly:
      WriteRtfLineSpacing(-UnitConverter.ModelUnitsToTwips(Trunc(AParagraphLineSpacing)), ExactlyLineSpacingMultiple);
    TdxParagraphLineSpacing.Double:
      WriteRtfLineSpacing(DoubleIntervalRtfLineSpacingValue, MultipleLineSpacing);
    TdxParagraphLineSpacing.Sesquialteral:
      WriteRtfLineSpacing(SesquialteralIntervalRtfLineSpacingValue, MultipleLineSpacing);
    TdxParagraphLineSpacing.Multiple:
      WriteRtfLineSpacing(Trunc(AParagraphLineSpacing * SingleIntervalRtfLineSpacingValue), MultipleLineSpacing);
  end;
end;

procedure TdxRtfParagraphPropertiesExporter.WriteParagraphListIndex(
  AIndex: TdxNumberingListIndex);
begin
  RtfBuilder.WriteCommand(TdxRtfExportSR.ListIndex, DocumentModel.NumberingLists[AIndex].Id);
end;

procedure TdxRtfParagraphPropertiesExporter.WriteParagraphOutlineLevel(
  AOutlineLevel: Integer);
begin
  if (AOutlineLevel < 0) or (AOutlineLevel >= 10) then
    Exit;

  if AOutlineLevel > 0 then
    RtfBuilder.WriteCommand(TdxRtfExportSR.OutlineLevel, AOutlineLevel - 1);
end;

procedure TdxRtfParagraphPropertiesExporter.WriteParagraphPageBreakBefore(
  Value: Boolean);
begin
  if Value <> DefaultPageBreakBefore then
    RtfBuilder.WriteCommand(TdxRtfExportSR.PageBreakBefore, IfThen(Value, 1 , 0));
end;

procedure TdxRtfParagraphPropertiesExporter.WriteParagraphSpacingAfter(
  ASpacingAfter: Integer);
begin
  if ASpacingAfter <> DefaultParagraphSpacingAfter then
    RtfBuilder.WriteCommand(TdxRtfExportSR.SpaceAfter, UnitConverter.ModelUnitsToTwips(ASpacingAfter));
end;

procedure TdxRtfParagraphPropertiesExporter.WriteParagraphSpacingBefore(
  ASpacingBefore: Integer);
begin
  if ASpacingBefore <> DefaultParagraphSpacingBefore then
    RtfBuilder.WriteCommand(TdxRtfExportSR.SpaceBefore, UnitConverter.ModelUnitsToTwips(ASpacingBefore));
end;

procedure TdxRtfParagraphPropertiesExporter.WriteParagraphStyle(
  AParagraphStyle: TdxParagraphStyle);
var
  AStyleName: string;
  AStyleCollection: TDictionary<string, Integer>;
begin
  AStyleName := AParagraphStyle.StyleName;
  AStyleCollection := RtfExportHelper.ParagraphStylesCollectionIndex;
  if AStyleCollection.ContainsKey(AStyleName) then
    RtfBuilder.WriteCommand(TdxRtfExportSR.ParagraphStyle, AStyleCollection[AStyleName]);
end;

procedure TdxRtfParagraphPropertiesExporter.WriteParagraphSuppressHyphenation(
  AParagraphSuppressHyphenation: Boolean);
begin
  if AParagraphSuppressHyphenation <> DefaultSuppressHyphenation then
    RtfBuilder.WriteCommand(TdxRtfExportSR.AutomaticParagraphHyphenation, IfThen(AParagraphSuppressHyphenation, 0, 1));
end;

procedure TdxRtfParagraphPropertiesExporter.WriteParagraphSuppressLineNumbers(
  AParagraphSuppressLineNumbers: Boolean);
begin
  if AParagraphSuppressLineNumbers then
    RtfBuilder.WriteCommand(TdxRtfExportSR.SuppressLineNumbering);
end;

procedure TdxRtfParagraphPropertiesExporter.WriteParagraphTab(
  ATabInfo: TdxTabInfo);
begin
  WriteTabKind(ATabInfo.Alignment);
  WriteTabLeader(ATabInfo.Leader);
  WriteTabPosition(ATabInfo.Position);
end;

procedure TdxRtfParagraphPropertiesExporter.WriteParagraphTabs(
  ATabFormattingInfo: TdxTabFormattingInfo);
var
  ACount: Integer;
  I: Integer;
begin
  ACount := ATabFormattingInfo.Count;
  for I := 0 to ACount - 1 do
    WriteParagraphTab(ATabFormattingInfo[I]);
end;

procedure TdxRtfParagraphPropertiesExporter.WriteParagraphWidowOrphanControl(
  Value: Boolean);
begin
  if Value = DefaultWidowOrphanControl then
    Exit;
  if Value then
    RtfBuilder.WriteCommand(TdxRtfExportSR.WidowOrphanControlOn)
  else
    RtfBuilder.WriteCommand(TdxRtfExportSR.WidowOrphanControlOff);
end;

procedure TdxRtfParagraphPropertiesExporter.WriteRtfLineSpacing(
  ARtfLineSpacingValue, ARtfLineSpacingMultiple: Integer);
begin
  RtfBuilder.WriteCommand(TdxRtfExportSR.RtfLineSpacingValue, ARtfLineSpacingValue);
  RtfBuilder.WriteCommand(TdxRtfExportSR.RtfLineSpacingMultiple, ARtfLineSpacingMultiple);
end;

procedure TdxRtfParagraphPropertiesExporter.WriteTabKind(
  AlignmentType: TdxTabAlignmentType);
begin
  case AlignmentType of
    TdxTabAlignmentType.Center:
      RtfBuilder.WriteCommand(TdxRtfExportSR.CenteredTab);
    TdxTabAlignmentType.Right:
      RtfBuilder.WriteCommand(TdxRtfExportSR.FlushRightTab);
    TdxTabAlignmentType.Decimal:
      RtfBuilder.WriteCommand(TdxRtfExportSR.DecimalTab);
  end;
end;

procedure TdxRtfParagraphPropertiesExporter.WriteTabLeader(
  ALeaderType: TdxTabLeaderType);
begin
  case ALeaderType of
    TdxTabLeaderType.Dots:
      RtfBuilder.WriteCommand(TdxRtfExportSR.TabLeaderDots);
    TdxTabLeaderType.EqualSign:
      RtfBuilder.WriteCommand(TdxRtfExportSR.TabLeaderEqualSign);
    TdxTabLeaderType.Hyphens:
      RtfBuilder.WriteCommand(TdxRtfExportSR.TabLeaderHyphens);
    TdxTabLeaderType.MiddleDots:
      RtfBuilder.WriteCommand(TdxRtfExportSR.TabLeaderMiddleDots);
    TdxTabLeaderType.ThickLine:
      RtfBuilder.WriteCommand(TdxRtfExportSR.TabLeaderThickLine);
    TdxTabLeaderType.Underline:
      RtfBuilder.WriteCommand(TdxRtfExportSR.TabLeaderUnderline);
  end;
end;

procedure TdxRtfParagraphPropertiesExporter.WriteTabPosition(
  APosition: Integer);
begin
  RtfBuilder.WriteCommand(TdxRtfExportSR.TabPosition, UnitConverter.ModelUnitsToTwips(APosition));
end;

{ TdxRtfStyleExporter }

constructor TdxRtfStyleExporter.Create(ADocumentModel: TdxDocumentModel;
  AExporter: TdxRtfContentExporter; ARtfExportHelper: IdxRtfExportHelper;
  AOptions: TdxRtfDocumentExporterOptions);
begin
  inherited Create;
  FDocumentModel := ADocumentModel;
  FRtfExportHelper := ARtfExportHelper;
  FRtfBuilder := AExporter.CreateRtfBuilder;
  FCharacterPropertiesExporter := TdxRtfCharacterPropertiesExporter.Create(ADocumentModel,
    RtfExportHelper, RtfBuilder, AOptions);
  FParagraphPropertiesExporter := TdxRtfParagraphPropertiesExporter.Create(ADocumentModel,
    RtfExportHelper, RtfBuilder);
end;

destructor TdxRtfStyleExporter.Destroy;
begin
  FreeAndNil(FRtfBuilder);
  FreeAndNil(FCharacterPropertiesExporter);
  FreeAndNil(FParagraphPropertiesExporter);
  inherited Destroy;
end;

procedure TdxRtfStyleExporter.ExportCharacterProperties(
  ACharacterProperties: TdxMergedCharacterProperties);
var
  AMerger: TdxCharacterPropertiesMerger;
begin
  AMerger := TdxCharacterPropertiesMerger.Create(ACharacterProperties);
  try
    AMerger.Merge(DocumentModel.DefaultCharacterProperties);
    CharacterExporter.ExportCharacterProperties(AMerger.MergedProperties, True);
  finally
    AMerger.Free;
  end;
end;

procedure TdxRtfStyleExporter.ExportCharacterStyle(AStyle: TdxCharacterStyle);
var
  AStyleIndex: Integer;
  AParentStyleIndex: Integer;
  ALinkedStyleIndex: Integer;
  AMergedCharacterProperties: TdxMergedCharacterProperties;
begin
  if AStyle.Deleted or RtfExportHelper.CharacterStylesCollectionIndex.ContainsKey(AStyle.StyleName) then
    Exit;

  AStyleIndex := GetNextFreeStyleIndex;
  RtfExportHelper.CharacterStylesCollectionIndex.Add(AStyle.StyleName, AStyleIndex);
  RtfBuilder.OpenGroup;
  RtfBuilder.WriteCommand(TdxRtfExportSR.CharacterStyle, AStyleIndex);
  AParentStyleIndex := ObtainCharacterStyleIndex(AStyle.Parent);
  if AParentStyleIndex >= 0 then
    RtfBuilder.WriteCommand(TdxRtfExportSR.ParentStyle, AParentStyleIndex);
  if AStyle.HasLinkedStyle then
  begin
    ALinkedStyleIndex := ObtainParagraphStyleIndex(AStyle.LinkedStyle);
    if ALinkedStyleIndex >= 0 then
      RtfBuilder.WriteCommand(TdxRtfExportSR.LinkedStyle, ALinkedStyleIndex);
  end;

  if AStyle.Primary then
    RtfBuilder.WriteCommand(TdxRtfExportSR.QuickFormatStyle);

  AMergedCharacterProperties := AStyle.GetMergedCharacterProperties;
  try
    ExportCharacterProperties(AMergedCharacterProperties);
  finally
    AMergedCharacterProperties.Free;
  end;
  WriteStyleName(AStyle.StyleName);
  RtfBuilder.CloseGroup;
end;

procedure TdxRtfStyleExporter.ExportCharacterStyles(
  ACharacterStyles: TdxCharacterStyleCollection);
var
  ACount: Integer;
  I: Integer;
begin
  RtfBuilder.Clear;
  ACount := ACharacterStyles.Count;

  for I := 0 to ACount - 1 do
    ExportCharacterStyle(ACharacterStyles[I]);

  RtfExportHelper.StylesCollection.Add(RtfBuilder.RtfContent.ToString);
end;

procedure TdxRtfStyleExporter.ExportParagraphProperties(
  AParagraphProperties: TdxParagraphProperties;
  AMergedParagraphProperties: TdxMergedParagraphProperties);
var
  AMerger: TdxParagraphPropertiesMerger;
  AInfo: TdxParagraphFormattingInfo;
begin
  AMerger := TdxParagraphPropertiesMerger.Create(AMergedParagraphProperties);
  try
    AMerger.Merge(DocumentModel.DefaultParagraphProperties);
    AInfo := AMerger.MergedProperties.Info;
    FParagraphPropertiesExporter.WriteParagraphAlignment(AInfo.Alignment);
    FParagraphPropertiesExporter.WriteParagraphIndents(AMergedParagraphProperties);
    FParagraphPropertiesExporter.WriteParagraphSuppressHyphenation(AInfo.SuppressHyphenation);
    FParagraphPropertiesExporter.WriteParagraphSuppressLineNumbers(AInfo.SuppressLineNumbers);
    FParagraphPropertiesExporter.WriteParagraphContextualSpacing(AInfo.ContextualSpacing);
    FParagraphPropertiesExporter.WriteParagraphPageBreakBefore(AInfo.PageBreakBefore);
    FParagraphPropertiesExporter.WriteParagraphOutlineLevel(AInfo.OutlineLevel);
    FParagraphPropertiesExporter.WriteParagraphBackColor(AInfo.BackColor);
    FParagraphPropertiesExporter.WriteParagraphLineSpacing(AInfo.LineSpacingType, AInfo.LineSpacing);
    FParagraphPropertiesExporter.WriteParagraphSpacingBefore(AParagraphProperties.SpacingBefore);
    FParagraphPropertiesExporter.WriteParagraphSpacingAfter(AParagraphProperties.SpacingAfter);
  finally
    AMerger.Free;
  end;
end;

procedure TdxRtfStyleExporter.ExportParagraphStyle(AStyle: TdxParagraphStyle;
  I: Integer);
var
  AStyleIndex: Integer;
  AParentStyleIndex: Integer;
  ALinkedStyleIndex: Integer;
  ANextStyleIndex: Integer;
  AListLevelIndex: Integer;
  AMergedParagraphProperties: TdxMergedParagraphProperties;
  AMergedCharacterProperties: TdxMergedCharacterProperties;
begin
  AStyleIndex := ObtainParagraphStyleIndex(AStyle);
  if AStyleIndex < 0 then
    Exit;

  RtfBuilder.OpenGroup;
  if I > 0 then
  begin
    RtfBuilder.WriteCommand(TdxRtfExportSR.ParagraphStyle, AStyleIndex);
    AParentStyleIndex := ObtainParagraphStyleIndex(AStyle.Parent);
    if AParentStyleIndex >= 0 then
      RtfBuilder.WriteCommand(TdxRtfExportSR.ParentStyle, AParentStyleIndex);
  end;
  if AStyle.HasLinkedStyle then
  begin
    ALinkedStyleIndex := ObtainCharacterStyleIndex(AStyle.LinkedStyle);
    if ALinkedStyleIndex >= 0 then
      RtfBuilder.WriteCommand(TdxRtfExportSR.LinkedStyle, ALinkedStyleIndex);
  end;
  if AStyle.NextParagraphStyle <> nil then
  begin
    ANextStyleIndex := ObtainParagraphStyleIndex(AStyle.NextParagraphStyle);
    if ANextStyleIndex >= 0 then
      RtfBuilder.WriteCommand(TdxRtfExportSR.NextStyle, ANextStyleIndex);
  end;

  if AStyle.Primary then
    RtfBuilder.WriteCommand(TdxRtfExportSR.QuickFormatStyle);

  AMergedParagraphProperties := AStyle.GetMergedParagraphProperties;
  try
    ExportParagraphProperties(AStyle.ParagraphProperties, AMergedParagraphProperties);
  finally
    AMergedParagraphProperties.Free;
  end;

  AMergedCharacterProperties := AStyle.GetMergedCharacterProperties;
  try
    ExportCharacterProperties(AMergedCharacterProperties);
  finally
    AMergedCharacterProperties.Free;
  end;

  if AStyle.GetNumberingListIndex >= 0 then
  begin
    RtfBuilder.WriteCommand(TdxRtfExportSR.ListIndex, GetListId(AStyle.GetNumberingListIndex));
    AListLevelIndex := AStyle.GetListLevelIndex;
    if AListLevelIndex > 0 then
      RtfBuilder.WriteCommand(TdxRtfExportSR.LevelIndex, AListLevelIndex);
  end;
  WriteStyleName(AStyle.StyleName);
  RtfBuilder.CloseGroup;
end;

procedure TdxRtfStyleExporter.ExportParagraphStyles(
  AParagraphStyles: TdxParagraphStyleCollection);
var
  AStyles: TList<TdxParagraphStyle>;
  AList: TList<TdxParagraphStyle>;
  I: Integer;
  AStylesToWrite: TList<TdxParagraphStyle>;
  ACount: Integer;
  AStyle: TdxParagraphStyle;
  AStyleIndex: Integer;
begin
  RtfBuilder.Clear;
  AList := TList<TdxParagraphStyle>.Create;
  try
    for I := 0 to AParagraphStyles.Count - 1 do
      AList.Add(AParagraphStyles[I]);
    AStyles := TdxTopologicalSorter<TdxParagraphStyle>.Sort(AList, TdxStyleTopologicalComparer<TdxParagraphStyle>.Create);
    try
      AStylesToWrite := TList<TdxParagraphStyle>.Create;
      try
        ACount := AStyles.Count;
        for I := 0 to ACount - 1 do
        begin
          AStyle := AStyles[I];
          if not AStyle.Deleted and not RtfExportHelper.ParagraphStylesCollectionIndex.ContainsKey(AStyle.StyleName) then
          begin
            AStylesToWrite.Add(AStyle);
            AStyleIndex := GetNextFreeStyleIndex;
            RtfExportHelper.ParagraphStylesCollectionIndex.Add(AStyle.StyleName, AStyleIndex);
          end;
        end;
        ACount := AStylesToWrite.Count;
        for I := 0 to ACount - 1 do
          ExportParagraphStyle(AStylesToWrite[I], I);
        RtfExportHelper.StylesCollection.Add(RtfBuilder.RtfContent.ToString);
      finally
        AStylesToWrite.Free;
      end;
    finally
      AStyles.Free;
    end;
  finally
    AList.Free;
  end;
end;

procedure TdxRtfStyleExporter.ExportStyleSheet(
  AParagraphStyles: TdxParagraphStyleCollection;
  ACharacterStyles: TdxCharacterStyleCollection;
  ATableStyles: TdxTableStyleCollection);
begin
  if AParagraphStyles.Count > 0 then
    ExportParagraphStyles(AParagraphStyles);
  if ACharacterStyles.Count > 0 then
    ExportCharacterStyles(ACharacterStyles);
end;

function TdxRtfStyleExporter.GetListId(AIndex: TdxNumberingListIndex): Integer;
begin
  Result := DocumentModel.NumberingLists[AIndex].Id;
end;

function TdxRtfStyleExporter.GetNextFreeStyleIndex: Integer;
begin
  Result := 0;
  while RtfExportHelper.CharacterStylesCollectionIndex.ContainsValue(Result) or
      RtfExportHelper.ParagraphStylesCollectionIndex.ContainsValue(Result) 
do
    Inc(Result);
end;

function TdxRtfStyleExporter.ObtainCharacterStyleIndex(
  AStyle: TdxCharacterStyle): Integer;
begin
  Result := ObtainStyleIndex(AStyle, RtfExportHelper.CharacterStylesCollectionIndex);
end;

function TdxRtfStyleExporter.ObtainParagraphStyleIndex(
  AStyle: TdxParagraphStyle): Integer;
begin
  Result := ObtainStyleIndex(AStyle, RtfExportHelper.ParagraphStylesCollectionIndex);
end;

function TdxRtfStyleExporter.ObtainStyleIndex(AStyle: IdxStyle;
  ACollection: TDictionary<string, Integer>): Integer;
begin
  if AStyle = nil then
    Result := -1
  else
  begin
    if not ACollection.TryGetValue(AStyle.StyleName, Result) then
      Result := -1;
  end;
end;

procedure TdxRtfStyleExporter.WriteStyleName(const AName: string);
var
  ACount: Integer;
  I: Integer;
begin
  ACount := Length(AName);
  for I := 1 to ACount do
    RtfBuilder.WriteChar(AName[I]);
  RtfBuilder.WriteChar(';');
end;

end.
