inherited NumberingListDialogForm: TNumberingListDialogForm
  Caption = 'Bullets and Numbering'
  ClientHeight = 333
  ClientWidth = 576
  PixelsPerInch = 96
  TextHeight = 13
  object dxLayoutControl1: TdxLayoutControl
    Left = 0
    Top = 0
    Width = 576
    Height = 333
    Align = alClient
    TabOrder = 0
    object lbBulleted: TcxListBox
      Left = 24
      Top = 44
      Width = 121
      Height = 97
      ItemHeight = 13
      TabOrder = 0
    end
    object lbNumbered: TcxListBox
      Left = 10000
      Top = 10000
      Width = 121
      Height = 97
      ItemHeight = 13
      TabOrder = 1
      Visible = False
    end
    object btnCustomize: TcxButton
      Left = 314
      Top = 298
      Width = 90
      Height = 25
      Caption = 'Customize...'
      TabOrder = 4
    end
    object btnOk: TcxButton
      Left = 410
      Top = 298
      Width = 75
      Height = 25
      Caption = 'OK'
      ModalResult = 1
      TabOrder = 5
    end
    object btnCancel: TcxButton
      Left = 491
      Top = 298
      Width = 75
      Height = 25
      Cancel = True
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 6
    end
    object rbRestartNumbering: TcxRadioButton
      Left = 10
      Top = 302
      Width = 127
      Height = 17
      Caption = 'Restart numbering'
      TabOrder = 2
    end
    object rbContinuePreviousList: TcxRadioButton
      Left = 143
      Top = 302
      Width = 136
      Height = 17
      Caption = 'Continue previous list'
      TabOrder = 3
    end
    object lcMainGroup_Root: TdxLayoutGroup
      AlignHorz = ahClient
      AlignVert = avClient
      CaptionOptions.Visible = False
      ButtonOptions.Buttons = <>
      Hidden = True
      ShowBorder = False
      Index = -1
    end
    object dxLayoutControl1Group2: TdxLayoutGroup
      Parent = lcMainGroup_Root
      AlignHorz = ahClient
      CaptionOptions.Text = 'New Group'
      CaptionOptions.Visible = False
      ButtonOptions.Buttons = <>
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 1
    end
    object dxLayoutControl1Group1: TdxLayoutGroup
      Parent = lcMainGroup_Root
      AlignVert = avClient
      CaptionOptions.Text = 'New Group'
      ButtonOptions.Buttons = <>
      LayoutDirection = ldTabbed
      ShowBorder = False
      Index = 0
    end
    object lcgBulleted: TdxLayoutGroup
      Parent = dxLayoutControl1Group1
      CaptionOptions.Text = '&Bulleted'
      ButtonOptions.Buttons = <>
      Index = 0
    end
    object lcgNumbered: TdxLayoutGroup
      Parent = dxLayoutControl1Group1
      CaptionOptions.Text = '&Numbered'
      ButtonOptions.Buttons = <>
      Index = 1
    end
    object lcgOutlineNumbered: TdxLayoutGroup
      Parent = dxLayoutControl1Group1
      CaptionOptions.Text = 'O&utline Numbered'
      ButtonOptions.Buttons = <>
      Index = 2
    end
    object dxLayoutControl1Item5: TdxLayoutItem
      Parent = lcgBulleted
      AlignHorz = ahClient
      AlignVert = avClient
      CaptionOptions.Text = 'cxListBox1'
      CaptionOptions.Visible = False
      Control = lbBulleted
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object dxLayoutControl1Item6: TdxLayoutItem
      Parent = lcgNumbered
      AlignVert = avClient
      CaptionOptions.Text = 'cxListBox1'
      CaptionOptions.Visible = False
      Control = lbNumbered
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object dxLayoutControl1Group4: TdxLayoutGroup
      Parent = dxLayoutControl1Group2
      AlignHorz = ahRight
      AlignVert = avCenter
      CaptionOptions.Text = 'New Group'
      ButtonOptions.Buttons = <>
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 1
    end
    object dxLayoutControl1Item2: TdxLayoutItem
      Parent = dxLayoutControl1Group4
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Offsets.Top = 4
      Control = btnCustomize
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object dxLayoutControl1Item3: TdxLayoutItem
      Parent = dxLayoutControl1Group4
      CaptionOptions.Text = 'cxButton2'
      CaptionOptions.Visible = False
      Offsets.Top = 4
      Control = btnOk
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object dxLayoutControl1Item4: TdxLayoutItem
      Parent = dxLayoutControl1Group4
      CaptionOptions.Text = 'cxButton3'
      CaptionOptions.Visible = False
      Offsets.Top = 4
      Control = btnCancel
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object dxLayoutControl1Item8: TdxLayoutItem
      Parent = dxLayoutControl1Group3
      AlignVert = avCenter
      CaptionOptions.Text = 'cxRadioButton1'
      CaptionOptions.Visible = False
      Control = rbRestartNumbering
      ControlOptions.AutoColor = True
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object dxLayoutControl1Item7: TdxLayoutItem
      Parent = dxLayoutControl1Group3
      AlignVert = avCenter
      CaptionOptions.Text = 'cxRadioButton2'
      CaptionOptions.Visible = False
      Control = rbContinuePreviousList
      ControlOptions.AutoColor = True
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object dxLayoutControl1Group3: TdxLayoutGroup
      Parent = dxLayoutControl1Group2
      AlignVert = avCenter
      CaptionOptions.AlignHorz = taCenter
      CaptionOptions.AlignVert = tavCenter
      CaptionOptions.Text = 'New Group'
      Offsets.Top = 4
      ButtonOptions.Buttons = <>
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 0
    end
  end
end
