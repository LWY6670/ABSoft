{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Options;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Math, SysUtils, Classes, Generics.Collections, dxRichEdit.DocumentModel.Core, dxRichEdit.Utils.Types,
  dxRichEdit.Utils.Characters;

type
  TdxNumberingOptions = class;

  { TdxRichEditNotificationOptions }

  TdxRichEditNotificationOptions = class(TInterfacedPersistent)
  private
    FHasChanges: Boolean;
    FLockCount: Integer;
    FOnChanged: TNotifyEvent;
  protected
    procedure Changed;
    procedure DoChanged; virtual;
    procedure DoReset; virtual;
    function IsLocked: Boolean; virtual;
    procedure Initialize; virtual;
  public
    constructor Create; virtual;
    procedure Assign(Source: TPersistent); override;

    procedure BeginUpdate;
    procedure CancelUpdate;
    procedure EndUpdate;

    procedure Reset;

    property OnChanged: TNotifyEvent read FOnChanged write FOnChanged;
  end;

  { TdxDocumentCapabilitiesOptions }

  TdxDocumentCapability = (Default, Disabled, Enabled, Hidden);

  TdxDocumentCapabilitiesOptions = class(TdxRichEditNotificationOptions)
  private
    FBookmarks: TdxDocumentCapability;
    FCharacterFormatting: TdxDocumentCapability;
    FCharacterStyle: TdxDocumentCapability;
    FEndNotes: TdxDocumentCapability;
    FFloatingObjects: TdxDocumentCapability;
    FFootNotes: TdxDocumentCapability;
    FHeadersFooters: TdxDocumentCapability;
    FHyperlinks: TdxDocumentCapability;
    FInlinePictures: TdxDocumentCapability;
		FParagraphFormatting: TdxDocumentCapability;
    FParagraphs: TdxDocumentCapability;
    FParagraphStyle: TdxDocumentCapability;
    FParagraphTabs: TdxDocumentCapability;
    FSections: TdxDocumentCapability;
    FTableCellStyle: TdxDocumentCapability;
    FTables: TdxDocumentCapability;
    FTableStyle: TdxDocumentCapability;
    FTabSymbol: TdxDocumentCapability;
    FUndo: TdxDocumentCapability;
    FNumbering: TdxNumberingOptions;
    procedure SetBookmarks(const Value: TdxDocumentCapability);
    procedure SetCharacterFormatting(const Value: TdxDocumentCapability);
    procedure SetCharacterStyle(const Value: TdxDocumentCapability);
    procedure SetEndNotes(const Value: TdxDocumentCapability);
    procedure SetFloatingObjects(const Value: TdxDocumentCapability);
    procedure SetFootNotes(const Value: TdxDocumentCapability);
    procedure SetHeadersFooters(const Value: TdxDocumentCapability);
    procedure SetHyperlinks(const Value: TdxDocumentCapability);
    procedure SetInlinePictures(const Value: TdxDocumentCapability);
		procedure SetParagraphFormatting(const Value: TdxDocumentCapability);
    procedure SetParagraphs(const Value: TdxDocumentCapability);
    procedure SetParagraphStyle(const Value: TdxDocumentCapability);
    procedure SetParagraphTabs(const Value: TdxDocumentCapability);
    procedure SetSections(const Value: TdxDocumentCapability);
    procedure SetTableCellStyle(const Value: TdxDocumentCapability);
    procedure SetTables(const Value: TdxDocumentCapability);
    procedure SetTableStyle(const Value: TdxDocumentCapability);
    procedure SetTabSymbol(const Value: TdxDocumentCapability);
    procedure SetUndo(const Value: TdxDocumentCapability);
    procedure SetNumbering(const Value: TdxNumberingOptions);
  protected
    procedure Initialize; override;
    procedure DoReset; override;
    function IsAllowed(const Value: TdxDocumentCapability): Boolean;
    procedure InnerOptionsChangeHandler(Sender: TObject); 
  public
    destructor Destroy; override;

    procedure Assign(Source: TPersistent); override;

    function BookmarksAllowed: Boolean; virtual;
    function CharacterFormattingAllowed: Boolean; virtual;
    function CharacterStyleAllowed: Boolean; virtual;
    function EndNotesAllowed: Boolean; virtual;
    function FloatingObjectsAllowed: Boolean; virtual;
    function FootNotesAllowed: Boolean; virtual;
    function HeadersFootersAllowed: Boolean; virtual;
    function HyperlinksAllowed: Boolean; virtual;
    function InlinePicturesAllowed: Boolean; virtual;
		function ParagraphFormattingAllowed: Boolean; virtual;
    function ParagraphsAllowed: Boolean; virtual;
    function ParagraphStyleAllowed: Boolean; virtual;
    function ParagraphTabsAllowed: Boolean; virtual;
    function SectionsAllowed: Boolean; virtual;
    function TableCellStyleAllowed: Boolean; virtual;
    function TablesAllowed: Boolean; virtual;
    function TableStyleAllowed: Boolean; virtual;
    function TabSymbolAllowed: Boolean; virtual;
    function UndoAllowed: Boolean; virtual;
  published
    property Bookmarks: TdxDocumentCapability read FBookmarks write SetBookmarks default TdxDocumentCapability.Default;
    property CharacterFormatting: TdxDocumentCapability read FCharacterFormatting write SetCharacterFormatting default TdxDocumentCapability.Default;
    property CharacterStyle: TdxDocumentCapability read FCharacterStyle write SetCharacterStyle default TdxDocumentCapability.Default;
    property EndNotes: TdxDocumentCapability read FEndNotes write SetEndNotes default TdxDocumentCapability.Default;
    property FloatingObjects: TdxDocumentCapability read FFloatingObjects write SetFloatingObjects default TdxDocumentCapability.Default;
    property FootNotes: TdxDocumentCapability read FFootNotes write SetFootNotes default TdxDocumentCapability.Default;
    property HeadersFooters: TdxDocumentCapability read FHeadersFooters write SetHeadersFooters default TdxDocumentCapability.Default;
    property Hyperlinks: TdxDocumentCapability read FHyperlinks write SetHyperlinks default TdxDocumentCapability.Default;
    property InlinePictures: TdxDocumentCapability read FInlinePictures write SetInlinePictures default TdxDocumentCapability.Default;
    property ParagraphFormatting: TdxDocumentCapability read FParagraphFormatting write SetParagraphFormatting default TdxDocumentCapability.Default;
    property Paragraphs: TdxDocumentCapability read FParagraphs write SetParagraphs default TdxDocumentCapability.Default;
    property ParagraphStyle: TdxDocumentCapability read FParagraphStyle write SetParagraphStyle default TdxDocumentCapability.Default;
    property ParagraphTabs: TdxDocumentCapability read FParagraphTabs write SetParagraphTabs default TdxDocumentCapability.Default;
    property Sections: TdxDocumentCapability read FSections write SetSections default TdxDocumentCapability.Default;
    property TableCellStyle: TdxDocumentCapability read FTableCellStyle write SetTableCellStyle default TdxDocumentCapability.Default;
    property Tables: TdxDocumentCapability read FTables write SetTables default TdxDocumentCapability.Default;
    property TableStyle: TdxDocumentCapability read FTableStyle write SetTableStyle default TdxDocumentCapability.Default;
    property TabSymbol: TdxDocumentCapability read FTabSymbol write SetTabSymbol default TdxDocumentCapability.Default;
    property Undo: TdxDocumentCapability read FUndo write SetUndo default TdxDocumentCapability.Default;
    property Numbering: TdxNumberingOptions read FNumbering write SetNumbering;
  end;
  TdxDocumentCapabilitiesOptionsClass = class of TdxDocumentCapabilitiesOptions;

  { TdxRichEditBehaviorOptions }

  TdxRichEditBaseValueSource = (
    Auto = 0,
    Document,
    Control
  );

  TdxLineBreakSubstitute = (
    None,
    Paragraph,
    Space
  );

  TdxPageBreakInsertMode = (
    NewLine,
    CurrentLine
  );

  TdxRichEditBehaviorOptions = class(TdxRichEditNotificationOptions)
  const
    DefaultMaxZoomFactor = 5.00; 
    DefaultMinZoomFactor = 0.10; 
  strict private
    FAllowCopy: TdxDocumentCapability;
    FAllowPaste: TdxDocumentCapability;
    FAllowDrag: TdxDocumentCapability;
    FAllowDrop: TdxDocumentCapability;
    FAllowCut: TdxDocumentCapability;
    FAllowPrinting: TdxDocumentCapability;
    FAllowZooming: TdxDocumentCapability;
    FAllowSaveAs: TdxDocumentCapability;
    FAllowSave: TdxDocumentCapability;
    FAllowCreateNew: TdxDocumentCapability;
    FAllowOpen: TdxDocumentCapability;
    FAllowShowPopupMenu: TdxDocumentCapability;
    FAllowOfficeScrolling: TdxDocumentCapability;
    FAllowTouch: TdxDocumentCapability;
    FPasteSingleCellAsText: Boolean;
    FPasteLineBreakSubstitution: TdxLineBreakSubstitute;
    FMinZoomFactor: Single;
    FMaxZoomFactor: Single;
    FFontSource: TdxRichEditBaseValueSource;
    FForeColorSource: TdxRichEditBaseValueSource;

    FTabMarker: string;
    FUseFontSubstitution: Boolean;
    FOvertypeAllowed: Boolean;
    FPageBreakInsertMode: TdxPageBreakInsertMode;
  private
    function GetDragAllowed: Boolean;
    function GetDropAllowed: Boolean;
    function GetCopyAllowed: Boolean;
    function GetPasteAllowed: Boolean;
    function GetCutAllowed: Boolean;
    function GetPrintingAllowed: Boolean;
    function GetSaveAllowed: Boolean;
    function GetSaveAsAllowed: Boolean;
    function GetCreateNewAllowed: Boolean;
    function GetOpenAllowed: Boolean;
    function GetZoomingAllowed: Boolean;
    function GetShowPopupMenuAllowed: Boolean;
    function GetOfficeScrollingAllowed: Boolean;
    function GetTouchAllowed: Boolean;
    function IsMinZoomFactorStored: Boolean;
    function IsMaxZoomFactorStored: Boolean;
    function IsTabMarkerStored: Boolean;
    procedure SetCopy(const Value: TdxDocumentCapability); inline;
    procedure SetPaste(const Value: TdxDocumentCapability); inline;
    procedure SetCreateNew(const Value: TdxDocumentCapability); inline;
    procedure SetCut(const Value: TdxDocumentCapability); inline;
    procedure SetDrag(const Value: TdxDocumentCapability); inline;
    procedure SetDrop(const Value: TdxDocumentCapability); inline;
    procedure SetFontSource(const Value: TdxRichEditBaseValueSource); inline;
    procedure SetForeColorSource(const Value: TdxRichEditBaseValueSource); inline;
    procedure SetMaxZoomFactor(Value: Single);
    procedure SetMinZoomFactor(Value: Single);
    procedure SetOfficeScrolling(const Value: TdxDocumentCapability); inline;
    procedure SetOpen(const Value: TdxDocumentCapability); inline;
    procedure SetOvertypeAllowed(const Value: Boolean); inline;
    procedure SetPageBreakInsertMode(const Value: TdxPageBreakInsertMode); inline;
    procedure SetPasteSingleCellAsText(const Value: Boolean); inline;
    procedure SetPasteLineBreakSubstitution(const Value: TdxLineBreakSubstitute); inline;
    procedure SetPrinting(const Value: TdxDocumentCapability); inline;
    procedure SetSave(const Value: TdxDocumentCapability); inline;
    procedure SetSaveAs(const Value: TdxDocumentCapability); inline;
    procedure SetShowPopupMenu(const Value: TdxDocumentCapability); inline;
    procedure SetTabMarker(const Value: string); inline;
    procedure SetTouch(const Value: TdxDocumentCapability); inline;
    procedure SetUseFontSubstitution(const Value: Boolean); inline;
    procedure SetZooming(const Value: TdxDocumentCapability); inline;
  protected
    function IsAllowed(AOption: TdxDocumentCapability): Boolean; 
    procedure DoReset; override; 

    function ShouldSerializePrinting: Boolean; virtual;
    procedure ResetPrinting; virtual; 

    function ShouldSerializeSaveAs: Boolean; virtual;
    procedure ResetSaveAs; virtual;

    function ShouldSerializeSave: Boolean; virtual;
    procedure ResetSave; virtual;

    function ShouldSerializeZooming: Boolean; virtual;
    procedure ResetZooming; virtual;

    function ShouldSerializeCreateNew: Boolean; virtual;
    procedure ResetCreateNew; virtual;

    function ShouldSerializeOpen: Boolean; virtual;
    procedure ResetOpen; virtual;
  public
    constructor Create; override;
    procedure Assign(Source: TPersistent); override; 

    property DragAllowed: Boolean read GetDragAllowed;
    property DropAllowed: Boolean read GetDropAllowed;

    property CopyAllowed: Boolean read GetCopyAllowed;
    property PasteAllowed: Boolean read GetPasteAllowed;
    property CutAllowed: Boolean read GetCutAllowed;
    property PrintingAllowed: Boolean read GetPrintingAllowed;
    property SaveAllowed: Boolean read GetSaveAllowed;
    property SaveAsAllowed: Boolean read GetSaveAsAllowed;
    property CreateNewAllowed: Boolean read GetCreateNewAllowed;
    property OpenAllowed: Boolean read GetOpenAllowed;

    property ZoomingAllowed: Boolean read GetZoomingAllowed;
    property ShowPopupMenuAllowed: Boolean read GetShowPopupMenuAllowed;
    property OfficeScrollingAllowed: Boolean read GetOfficeScrollingAllowed;
    property TouchAllowed: Boolean read GetTouchAllowed;
  published
    property Copy: TdxDocumentCapability read FAllowCopy write SetCopy default TdxDocumentCapability.Default;
    property CreateNew: TdxDocumentCapability read FAllowCreateNew write SetCreateNew default TdxDocumentCapability.Default;
    property Cut: TdxDocumentCapability read FAllowCut write SetCut default TdxDocumentCapability.Default;
    property Drag: TdxDocumentCapability read FAllowDrag write SetDrag default TdxDocumentCapability.Default;
    property Drop: TdxDocumentCapability read FAllowDrop write SetDrop default TdxDocumentCapability.Default;
    property FontSource: TdxRichEditBaseValueSource read FFontSource write SetFontSource default TdxRichEditBaseValueSource.Auto;
    property ForeColorSource: TdxRichEditBaseValueSource read FForeColorSource write SetForeColorSource default TdxRichEditBaseValueSource.Auto;
    property MaxZoomFactor: Single read FMaxZoomFactor write SetMaxZoomFactor stored IsMaxZoomFactorStored;
    property MinZoomFactor: Single read FMinZoomFactor write SetMinZoomFactor stored IsMinZoomFactorStored;
    property OfficeScrolling: TdxDocumentCapability read FAllowOfficeScrolling write SetOfficeScrolling default TdxDocumentCapability.Default;
    property Open: TdxDocumentCapability read FAllowOpen write SetOpen default TdxDocumentCapability.Default;
    property OvertypeAllowed: Boolean read FOvertypeAllowed write SetOvertypeAllowed default True;
    property PageBreakInsertMode: TdxPageBreakInsertMode read FPageBreakInsertMode write SetPageBreakInsertMode default TdxPageBreakInsertMode.NewLine;
    property Paste: TdxDocumentCapability read FAllowPaste write SetPaste default TdxDocumentCapability.Default;
    property PasteLineBreakSubstitution: TdxLineBreakSubstitute read FPasteLineBreakSubstitution write SetPasteLineBreakSubstitution default TdxLineBreakSubstitute.None;
    property PasteSingleCellAsText: Boolean read FPasteSingleCellAsText write SetPasteSingleCellAsText default False;
    property Printing: TdxDocumentCapability read FAllowPrinting write SetPrinting default TdxDocumentCapability.Default;
    property Save: TdxDocumentCapability read FAllowSave write SetSave default TdxDocumentCapability.Default;
    property SaveAs: TdxDocumentCapability read FAllowSaveAs write SetSaveAs default TdxDocumentCapability.Default;
    property ShowPopupMenu: TdxDocumentCapability read FAllowShowPopupMenu write SetShowPopupMenu default TdxDocumentCapability.Default;
    property TabMarker: string read FTabMarker write SetTabMarker stored IsTabMarkerStored;
    property Touch: TdxDocumentCapability read FAllowTouch write SetTouch default TdxDocumentCapability.Default;
    property UseFontSubstitution: Boolean read FUseFontSubstitution write SetUseFontSubstitution default True;
    property Zooming: TdxDocumentCapability read FAllowZooming write SetZooming default TdxDocumentCapability.Default;
  end;

  { TdxRichEditEditingOptions }

  TdxRichEditEditingOptions = class(TdxRichEditNotificationOptions)
  strict private
    FMergeParagraphsContent: Boolean;
    FMergeUseFirstParagraphStyle: Boolean;
  private
    procedure SetMergeParagraphsContent(const Value: Boolean);
    procedure SetMergeUseFirstParagraphStyle(const Value: Boolean);
  protected
    procedure DoReset; override;
  public
    procedure Assign(Source: TPersistent); override;
  published
    property MergeParagraphsContent: Boolean read FMergeParagraphsContent write SetMergeParagraphsContent default True;
    property MergeUseFirstParagraphStyle: Boolean read FMergeUseFirstParagraphStyle write SetMergeUseFirstParagraphStyle default True;
  end;

  { TdxRichEditMailMergeOptions }

  TdxRichEditMailMergeOptions = class(TdxRichEditNotificationOptions)
  private
    FDataSource: TObject;
    FDataMember: string;
    FViewMergedData: Boolean;
    FActiveRecord: Integer;
    FKeepLastParagraph: Boolean;
    procedure SetDataSource(const Value: TObject);
    procedure SetDataMember(const Value: string);
    procedure SetViewMergedData(const Value: Boolean);
    procedure SetActiveRecord(const Value: Integer);
  protected
    procedure DoReset; override; 
  public

    property DataSource: TObject read FDataSource write SetDataSource;
    property DataMember: string read FDataMember write SetDataMember;
    property ViewMergedData: Boolean read FViewMergedData write SetViewMergedData;
    property ActiveRecord: Integer read FActiveRecord write SetActiveRecord;
    property KeepLastParagraph: Boolean read FKeepLastParagraph write FKeepLastParagraph;
  end;

  { IdxImporterOptions }

  IdxImporterOptions = interface
  ['{9D793690-1FF1-4157-8F00-615A7AAA8064}']
  end;

  { TdxFieldUpdateOnLoadOptions }

  TdxFieldUpdateOnLoadOptions = class
  private
    FUpdateDateField: Boolean;
    FUpdateTimeField: Boolean;
  public
    constructor Create(AUpdateDateField, AUpdateTimeField: Boolean);

    property UpdateDateField: Boolean read FUpdateDateField;
    property UpdateTimeField: Boolean read FUpdateTimeField;
  end;

  { TdxUpdateFieldOptions }

  TdxUpdateFieldOptions = class(TdxRichEditNotificationOptions)
  const
    FDefaultDate = True;
    FDefaultTime = True;
  private
    FDate: Boolean;
    FTime: Boolean;
    procedure SetDate(const Value: Boolean);
    procedure SetTime(const Value: Boolean);
    function GetNativeOptions: TdxFieldUpdateOnLoadOptions;
  protected
    procedure DoReset; override; 
  public
    constructor Create; override;

    procedure CopyFrom(ASource: TdxUpdateFieldOptions); virtual;

    property Date: Boolean read FDate write SetDate;
    property Time: Boolean read FTime write SetTime;
    property NativeOptions: TdxFieldUpdateOnLoadOptions read GetNativeOptions;
  end;

  { TdxDocumentImporterOptions }

  TdxDocumentImporterOptions = class abstract(TdxRichEditNotificationOptions, IdxImporterOptions)
  private
    FActualEncoding: TEncoding;
    FSourceUri: string;
    FLineBreakSubstitute: TdxLineBreakSubstitute;
    FUpdateField: TdxUpdateFieldOptions;
    procedure CreateUpdateFieldOptions;
    procedure OnInnerOptionsChanged(ASender: TObject);
  protected
    function GetActualEncoding: TEncoding; virtual;
    procedure SetActualEncoding(const Value: TEncoding); virtual;
    function GetSourceUri: string; virtual;
    procedure SetSourceUri(const Value: string); virtual;

    function ShouldSerializeSourceUri: Boolean; virtual;
    procedure DoReset; override;

    property ActualEncoding: TEncoding read GetActualEncoding write SetActualEncoding;
  public
    constructor Create; override;
    destructor Destroy; override;

    procedure CopyFrom(const AValue: IdxImporterOptions); virtual;
    function Format: TdxDocumentFormat; virtual; abstract; 

    property LineBreakSubstitute: TdxLineBreakSubstitute read FLineBreakSubstitute write FLineBreakSubstitute;
    property SourceUri: string read GetSourceUri write SetSourceUri;
    property UpdateField: TdxUpdateFieldOptions read FUpdateField;
  end;

  { TdxRtfDocumentImporterOptions }

  TdxRtfDocumentImporterOptions = class(TdxDocumentImporterOptions)
  private
    FSuppressLastParagraphDelete: Boolean;
    FCopySingleCellAsText: Boolean;
    FPasteFromIe: Boolean;
  protected
    procedure SetActualEncoding(const Value: TEncoding); override;
    procedure DoReset; override; 
  public
    procedure CopyFrom(const AValue: IdxImporterOptions); override;
    function Format: TdxDocumentFormat; override; 

    property SuppressLastParagraphDelete: Boolean read FSuppressLastParagraphDelete write FSuppressLastParagraphDelete;
    property CopySingleCellAsText: Boolean read FCopySingleCellAsText write FCopySingleCellAsText;
    property PasteFromIE: Boolean read FPasteFromIe write FPasteFromIe;
  end;

  { TdxPlainTextDocumentImporterOptions }

  TdxPlainTextDocumentImporterOptions = class(TdxDocumentImporterOptions)
  private
    FAutoDetectEncoding: Boolean;
    procedure SetAutoDetectEncoding(const Value: Boolean);
  protected
    procedure DoReset; override;
  public
    procedure CopyFrom(const AValue: IdxImporterOptions); override;
    function Format: TdxDocumentFormat; override;
    property AutoDetectEncoding: Boolean read FAutoDetectEncoding write SetAutoDetectEncoding;
  end;

  { TdxHtmlDocumentImporterOptions }

  TdxHtmlDocumentImporterOptions = class(TdxDocumentImporterOptions)
  protected
  public
    function Format: TdxDocumentFormat; override; 
  end;

  { TdxMhtDocumentImporterOptions }

  TdxMhtDocumentImporterOptions = class(TdxHtmlDocumentImporterOptions)
  public
    function Format: TdxDocumentFormat; override; 
  end;

  { TdxOpenXmlDocumentImporterOptions }

  TdxOpenXmlDocumentImporterOptions = class(TdxDocumentImporterOptions)
  public
    function Format: TdxDocumentFormat; override; 
  end;

  { TdxOpenDocumentImporterOptions }

  TdxOpenDocumentImporterOptions = class(TdxDocumentImporterOptions)
  public
    function Format: TdxDocumentFormat; override; 
  end;

  { TdxWordMLDocumentImporterOptions }

  TdxWordMLDocumentImporterOptions = class(TdxDocumentImporterOptions)
  protected
    procedure DoReset; override; 
  public
    function Format: TdxDocumentFormat; override; 
 end;

  { TdxRichEditDocumentImportOptions }

  TdxRichEditDocumentImportOptions = class(TdxRichEditNotificationOptions)
  strict private
    class var FFallbackFormatValue: TdxDocumentFormat; 
    class constructor Initialize;
  private
    FRtf: TdxRtfDocumentImporterOptions;
    FPlainText: TdxPlainTextDocumentImporterOptions;
    FHtml: TdxHtmlDocumentImporterOptions;
    FMht: TdxMhtDocumentImporterOptions;
    FOpenXml: TdxOpenXmlDocumentImporterOptions;
    FOpenDocument: TdxOpenDocumentImporterOptions;
    FWordML: TdxWordMLDocumentImporterOptions;
    FFallbackFormat: TdxDocumentFormat;

    FOptionsTable: TDictionary<TdxDocumentFormat, TdxDocumentImporterOptions>;
    procedure SetFallbackFormat(const Value: TdxDocumentFormat);
  protected
    function ShouldSerializeFallbackFormat: Boolean; virtual;
    procedure ResetFallbackFormat; virtual;
    procedure RegisterOptions(AOptions: TdxDocumentImporterOptions);
    procedure DoReset; override; 
    procedure CopyFrom(AOptions: TdxRichEditDocumentImportOptions); virtual;
    function CreateRtfOptions: TdxRtfDocumentImporterOptions; virtual;
    function CreatePlainTextOptions: TdxPlainTextDocumentImporterOptions; virtual;
    function CreateHtmlOptions: TdxHtmlDocumentImporterOptions; virtual;
    function CreateMhtOptions: TdxMhtDocumentImporterOptions; virtual;
    function CreateOpenXmlOptions: TdxOpenXmlDocumentImporterOptions; virtual;
    function CreateOpenDocumentOptions: TdxOpenDocumentImporterOptions; virtual;
    function CreateWordMLOptions: TdxWordMLDocumentImporterOptions; virtual;

    property OptionsTable: TDictionary<TdxDocumentFormat, TdxDocumentImporterOptions> read FOptionsTable;
  public
    constructor Create; override;
    destructor Destroy; override;

    function GetOptions(AFormat: TdxDocumentFormat): TdxDocumentImporterOptions; virtual;

    property Rtf: TdxRtfDocumentImporterOptions read FRtf;
    property PlainText: TdxPlainTextDocumentImporterOptions read FPlainText;
    property Html: TdxHtmlDocumentImporterOptions read FHtml;
    property Mht: TdxMhtDocumentImporterOptions read FMht;
    property OpenXml: TdxOpenXmlDocumentImporterOptions read FOpenXml;
    property OpenDocument: TdxOpenDocumentImporterOptions read FOpenDocument;
    property WordML: TdxWordMLDocumentImporterOptions read FWordML;

    property FallbackFormat: TdxDocumentFormat read FFallbackFormat write SetFallbackFormat;
  end;

  { IdxExporterOptions }

  IdxExporterOptions = interface
  ['{BA8AE00B-59C4-40D2-B58D-3AE2753DBE81}']
  end;

  { TdxDocumentExporterOptions }

  TdxDocumentExporterOptions = class abstract(TdxRichEditNotificationOptions, IdxExporterOptions)
  private
    FActualEncoding: TEncoding;
    FTargetUri: string;
  protected
    function ShouldSerializeTargetUri: Boolean; virtual;
    procedure DoReset; override; 
    procedure SetActualEncoding(const Value: TEncoding); virtual;
  public
    procedure CopyFrom(const AValue: IdxExporterOptions); virtual;
    function Format: TdxDocumentFormat; virtual; abstract;

    property ActualEncoding: TEncoding read FActualEncoding write SetActualEncoding;
    property TargetUri: string read FTargetUri write FTargetUri;
  end;

  { TdxRtfRunBackColorExportMode }

  TdxRtfRunBackColorExportMode = (
    Chcbpat = 0,
    Highlight,
    Both
  );

  { TdxRtfDocumentExporterCompatibilityOptions }

  TdxRtfDocumentExporterCompatibilityOptions = class(TdxRichEditNotificationOptions, IdxSupportsCopyFrom<TdxRtfDocumentExporterCompatibilityOptions>) 
  private
    FDuplicateObjectAsMetafile: Boolean;
    FBackColorExportMode: TdxRtfRunBackColorExportMode;
    procedure SetDuplicateObjectAsMetafile(const Value: Boolean);
    procedure SetBackColorExportMode(const Value: TdxRtfRunBackColorExportMode);
  protected
    function ShouldSerializeDuplicateObjectAsMetafile: Boolean; virtual;
    procedure ResetDuplicateObjectAsMetafile; virtual;
    function ShouldSerializeBackColorExportMode: Boolean; virtual;
    procedure ResetBackColorExportMode; virtual;
    procedure DoReset; override; 
  public
    procedure CopyFrom(const AValue: TdxRtfDocumentExporterCompatibilityOptions); reintroduce; virtual;

    property DuplicateObjectAsMetafile: Boolean read FDuplicateObjectAsMetafile write SetDuplicateObjectAsMetafile;
    property BackColorExportMode: TdxRtfRunBackColorExportMode read FBackColorExportMode write SetBackColorExportMode;
  end;

  { TdxExportFinalParagraphMark }

  TdxExportFinalParagraphMark = (
    Always,
    Never,
    SelectedOnly
  );

  { TdxRtfNumberingListExportFormat }

  TdxRtfNumberingListExportFormat = (
    PlainTextFormat,
    RtfFormat
  );

  { TdxRtfDocumentExporterOptions }

  TdxRtfDocumentExporterOptions = class(TdxDocumentExporterOptions)
  private
    FWrapContentInGroup: Boolean;
    FCompatibility: TdxRtfDocumentExporterCompatibilityOptions;
    FExportFinalParagraphMark: TdxExportFinalParagraphMark;
    FListExportFormat: TdxRtfNumberingListExportFormat;
    procedure SetWrapContentInGroup(const Value: Boolean);
    procedure SetListExportFormat(const Value: TdxRtfNumberingListExportFormat);
    procedure SetExportFinalParagraphMark(const Value: TdxExportFinalParagraphMark);
  protected
    function ShouldSerializeWrapContentInGroup: Boolean; virtual;
    procedure ResetWrapContentInGroup; virtual;
    function ShouldSerializeListExportFormat: Boolean; virtual;
    procedure ResetListExportFormat; virtual;
    function ShouldSerializeExportFinalParagraphMark: Boolean; virtual;
    procedure ResetExportFinalParagraphMark; virtual;
    procedure DoReset; override; 
    function CreateCompatibilityOptions: TdxRtfDocumentExporterCompatibilityOptions; virtual;
    procedure SetActualEncoding(const Value: TEncoding); override; 
  public
    constructor Create; override;
    destructor Destroy; override;

    procedure CopyFrom(const Value: IdxExporterOptions); override;
    function Format: TdxDocumentFormat; override; 

    property WrapContentInGroup: Boolean read FWrapContentInGroup write SetWrapContentInGroup;
    property ListExportFormat: TdxRtfNumberingListExportFormat read FListExportFormat write SetListExportFormat;
    property ExportFinalParagraphMark: TdxExportFinalParagraphMark read FExportFinalParagraphMark write SetExportFinalParagraphMark;
    property Compatibility: TdxRtfDocumentExporterCompatibilityOptions read FCompatibility;
  end;

  { TdxPlainTextDocumentExporterOptions }

  TdxPlainTextDocumentExporterOptions = class(TdxDocumentExporterOptions)
  strict private
    FExportHiddenText: Boolean;
    FExportBulletsAndNumbering: Boolean;
    FFieldCodeStartMarker: string;
    FFieldCodeEndMarker: string;
    FFieldResultEndMarker: string;
    FFootNoteNumberStringFormat: string;
    FEndNoteNumberStringFormat: string;
    FFootNoteSeparator: string;
    FEndNoteSeparator: string;
  public
    constructor Create; override;

    function Format: TdxDocumentFormat; override; 

    property ExportHiddenText: Boolean read FExportHiddenText write FExportHiddenText;
    property ExportBulletsAndNumbering: Boolean read FExportBulletsAndNumbering write FExportBulletsAndNumbering;
    property FieldCodeStartMarker: string read FFieldCodeStartMarker write FFieldCodeStartMarker;
    property FieldCodeEndMarker: string read FFieldCodeEndMarker write FFieldCodeEndMarker;
    property FieldResultEndMarker: string read FFieldResultEndMarker write FFieldResultEndMarker;
    property FootNoteNumberStringFormat: string read FFootNoteNumberStringFormat write FFootNoteNumberStringFormat;
    property EndNoteNumberStringFormat: string read FEndNoteNumberStringFormat write FEndNoteNumberStringFormat;
    property FootNoteSeparator: string read FFootNoteSeparator write FFootNoteSeparator;
    property EndNoteSeparator: string read FEndNoteSeparator write FEndNoteSeparator;
  end;

  { TdxHtmlDocumentExporterOptions }

  TdxHtmlDocumentExporterOptions = class(TdxDocumentExporterOptions)
  private
    function GetEncoding: TEncoding;
    procedure SetEncoding(const Value: TEncoding);
  public
    property Encoding: TEncoding read GetEncoding write SetEncoding;
  public
    function Format: TdxDocumentFormat; override; 
  end;

  { TdxMhtDocumentExporterOptions }

  TdxMhtDocumentExporterOptions = class(TdxHtmlDocumentExporterOptions)
  public
    function Format: TdxDocumentFormat; override; 
  end;

  { TdxOpenXmlDocumentExporterOptions }

  TdxOpenXmlDocumentExporterOptions = class(TdxDocumentExporterOptions)
  public
    function Format: TdxDocumentFormat; override; 
  end;

  { TdxOpenDocumentExporterOptions }

  TdxOpenDocumentExporterOptions = class(TdxDocumentExporterOptions)
  public
    function Format: TdxDocumentFormat; override; 
  end;

  { TdxWordMLDocumentExporterOptions }

  TdxWordMLDocumentExporterOptions = class(TdxDocumentExporterOptions)
  public
    function Format: TdxDocumentFormat; override; 
  end;

  { TdxDocDocumentExporterOptions }

  TdxDocDocumentExporterOptions = class(TdxDocumentExporterOptions)
  public
    function Format: TdxDocumentFormat; override; 
  end;

  { TdxRichEditDocumentExportOptions }

  TdxRichEditDocumentExportOptions = class(TdxRichEditNotificationOptions)
  private
    FRtf: TdxRtfDocumentExporterOptions;
    FPlainText: TdxPlainTextDocumentExporterOptions;
    FHtml: TdxHtmlDocumentExporterOptions;
    FMht: TdxMhtDocumentExporterOptions;
    FOpenXml: TdxOpenXmlDocumentExporterOptions;
    FOpenDocument: TdxOpenDocumentExporterOptions;
    FWordML: TdxWordMLDocumentExporterOptions;
    FDoc: TdxDocDocumentExporterOptions;
    FOptionsTable: TDictionary<TdxDocumentFormat, TdxDocumentExporterOptions>;
  protected
    procedure RegisterOptions(AOptions: TdxDocumentExporterOptions); virtual;
    procedure DoReset; override; 
    procedure CopyFrom(AOptions: TdxRichEditDocumentExportOptions); virtual;

    function CreateRtfOptions: TdxRtfDocumentExporterOptions; virtual;
    function CreatePlainTextOptions: TdxPlainTextDocumentExporterOptions; virtual;
    function CreateHtmlOptions: TdxHtmlDocumentExporterOptions; virtual;
    function CreateMhtOptions: TdxMhtDocumentExporterOptions; virtual;
    function CreateOpenXmlOptions: TdxOpenXmlDocumentExporterOptions; virtual;
    function CreateOpenDocumentOptions: TdxOpenDocumentExporterOptions; virtual;
    function CreateWordMLOptions: TdxWordMLDocumentExporterOptions; virtual;
    function CreateDocOptions: TdxDocDocumentExporterOptions; virtual;

    property OptionsTable: TDictionary<TdxDocumentFormat, TdxDocumentExporterOptions> read FOptionsTable;
  public
    constructor Create; override;
    destructor Destroy; override;

    function GetOptions(AFormat: TdxDocumentFormat): TdxDocumentExporterOptions; virtual;

    property Rtf: TdxRtfDocumentExporterOptions read FRtf;
    property PlainText: TdxPlainTextDocumentExporterOptions read FPlainText;
    property Html: TdxHtmlDocumentExporterOptions read FHtml;
    property Mht: TdxMhtDocumentExporterOptions read FMht;
    property OpenXml: TdxOpenXmlDocumentExporterOptions read FOpenXml;
    property OpenDocument: TdxOpenDocumentExporterOptions read FOpenDocument;
    property WordML: TdxWordMLDocumentExporterOptions read FWordML;
    property Doc: TdxDocDocumentExporterOptions read FDoc;
  end;

  { TdxCopyPasteOptions }

  TdxInsertOptions = (KeepSourceFormatting, MatchDestinationFormatting);

  TdxCopyPasteOptions = class(TdxRichEditNotificationOptions)
  const
    DefaultInsertOptions = TdxInsertOptions.MatchDestinationFormatting;
  private
    FInsertOptions: TdxInsertOptions;
    FMaintainDocumentSectionSettings: Boolean;
    procedure SetInsertOptions(const Value: TdxInsertOptions);
    procedure SetMaintainDocumentSectionSettings(const Value: Boolean);
  protected
    procedure DoReset; override; 
  public
    property InsertOptions: TdxInsertOptions read FInsertOptions write SetInsertOptions;
    property MaintainDocumentSectionSettings: Boolean read FMaintainDocumentSectionSettings write SetMaintainDocumentSectionSettings;
  end;

  { TdxRichEditFormattingMarkVisibility }

  TdxRichEditFormattingMarkVisibility = (
    Auto,
    Visible,
    Hidden
  );

  { TdxFormattingMarkVisibilityOptions }

  TdxFormattingMarkVisibilityOptions = class(TdxRichEditNotificationOptions)
  const
    DefaultParagraphMarkVisibility = TdxRichEditFormattingMarkVisibility.Auto;
    DefaultTabCharacterVisibility = TdxRichEditFormattingMarkVisibility.Auto;
    DefaultSpaceVisibility = TdxRichEditFormattingMarkVisibility.Auto;
    DefaultHiddenTextVisibility = TdxRichEditFormattingMarkVisibility.Auto;
    DefaultSeparatorVisibility = TdxRichEditFormattingMarkVisibility.Auto;
  private
    FParagraphMark: TdxRichEditFormattingMarkVisibility;
    FTabCharacter: TdxRichEditFormattingMarkVisibility;
    FSpace: TdxRichEditFormattingMarkVisibility;
    FSeparator: TdxRichEditFormattingMarkVisibility;
    FHiddenText: TdxRichEditFormattingMarkVisibility;
    FShowHiddenText: Boolean;
    procedure SetShowHiddenText(const Value: Boolean);
    procedure SetParagraphMark(const Value: TdxRichEditFormattingMarkVisibility);
    procedure SetTabCharacter(const Value: TdxRichEditFormattingMarkVisibility);
    procedure SetSpace(const Value: TdxRichEditFormattingMarkVisibility);
    procedure SetSeparator(const Value: TdxRichEditFormattingMarkVisibility);
    procedure SetHiddenText(const Value: TdxRichEditFormattingMarkVisibility);
  protected
    procedure DoReset; override; 
  published

    property ParagraphMark: TdxRichEditFormattingMarkVisibility read FParagraphMark write SetParagraphMark;
    property TabCharacter: TdxRichEditFormattingMarkVisibility read FTabCharacter write SetTabCharacter;
    property ShowHiddenText: Boolean read FShowHiddenText write SetShowHiddenText;
    property Space: TdxRichEditFormattingMarkVisibility read FSpace write SetSpace;
    property Separator: TdxRichEditFormattingMarkVisibility read FSeparator write SetSeparator;
    property HiddenText: TdxRichEditFormattingMarkVisibility read FHiddenText write SetHiddenText;
  end;

  { TdxNumberingOptions }

  TdxNumberingOptions = class(TdxRichEditNotificationOptions)
  private
    FBulleted: TdxDocumentCapability;
    FSimple: TdxDocumentCapability;
    FMultiLevel: TdxDocumentCapability;
    procedure SetBulleted(const Value: TdxDocumentCapability);
    procedure SetSimple(const Value: TdxDocumentCapability);
    procedure SetMultiLevel(const Value: TdxDocumentCapability);
    function IsAllowed(AOption: TdxDocumentCapability): Boolean;
    function GetBulletedAllowed: Boolean;
    function GetSimpleAllowed: Boolean;
    function GetMultiLevelAllowed: Boolean;
  protected
    procedure DoReset; override; 
  public
    procedure Assign(Source: TPersistent); override;

    property BulletedAllowed: Boolean read GetBulletedAllowed;
    property SimpleAllowed: Boolean read GetSimpleAllowed;
    property MultiLevelAllowed: Boolean read GetMultiLevelAllowed;
 published
    property Bulleted: TdxDocumentCapability read FBulleted write SetBulleted default TdxDocumentCapability.Default;
    property Simple: TdxDocumentCapability read FSimple write SetSimple default TdxDocumentCapability.Default;
    property MultiLevel: TdxDocumentCapability read FMultiLevel write SetMultiLevel default TdxDocumentCapability.Default;
  end;

implementation

uses
  dxCore;

{ TdxRichEditNotificationOptions }

constructor TdxRichEditNotificationOptions.Create;
begin
  inherited Create;
  Initialize;
  DoReset;
end;

procedure TdxRichEditNotificationOptions.Assign(Source: TPersistent);
begin
  if Source = nil then
    Reset
  else
    if Source is TdxRichEditNotificationOptions then
    begin
    end;
end;

procedure TdxRichEditNotificationOptions.BeginUpdate;
begin
  Inc(FLockCount);
end;

procedure TdxRichEditNotificationOptions.CancelUpdate;
begin
  Dec(FLockCount);
end;

procedure TdxRichEditNotificationOptions.EndUpdate;
begin
  Dec(FLockCount);
  Changed;
end;

procedure TdxRichEditNotificationOptions.Reset;
begin
  BeginUpdate;
  try
    DoReset;
  finally
    EndUpdate;
  end;
end;

procedure TdxRichEditNotificationOptions.Changed;
begin
  FHasChanges := True;
  if not IsLocked then
  begin
    if FHasChanges then
      DoChanged;
    FHasChanges := False;
  end;
end;

procedure TdxRichEditNotificationOptions.DoChanged;
begin
  dxCallNotify(FOnChanged, Self);
end;

procedure TdxRichEditNotificationOptions.DoReset;
begin
end;

procedure TdxRichEditNotificationOptions.Initialize;
begin
end;

function TdxRichEditNotificationOptions.IsLocked: Boolean;
begin
  Result := FLockCount > 0;
end;

{ TdxDocumentCapabilitiesOptions }

destructor TdxDocumentCapabilitiesOptions.Destroy;
begin
  FNumbering.Free;
  inherited Destroy;
end;

procedure TdxDocumentCapabilitiesOptions.Assign(Source: TPersistent);
var
  ASource: TdxDocumentCapabilitiesOptions;
begin
  BeginUpdate;
  try
    if Source is TdxDocumentCapabilitiesOptions then
    begin
      ASource := TdxDocumentCapabilitiesOptions(Source);
      Bookmarks := ASource.Bookmarks;
      CharacterFormatting := ASource.CharacterFormatting;
      CharacterStyle := ASource.CharacterStyle;
      EndNotes := ASource.EndNotes;
      FloatingObjects := ASource.FloatingObjects;
      FootNotes := ASource.FootNotes;
      HeadersFooters := ASource.HeadersFooters;
      Hyperlinks := ASource.Hyperlinks;
      InlinePictures := ASource.InlinePictures;
      ParagraphFormatting := ASource.ParagraphFormatting;
      Paragraphs := ASource.Paragraphs;
      ParagraphStyle := ASource.ParagraphStyle;
      ParagraphTabs := ASource.ParagraphTabs;
      Sections := ASource.Sections;
      TableCellStyle := ASource.TableCellStyle;
      Tables := ASource.Tables;
      TableStyle := ASource.TableStyle;
      TabSymbol := ASource.TabSymbol;
      Undo := ASource.Undo;
      Numbering.Assign(ASource.Numbering);
    end;
    inherited Assign(Source);
  finally
    EndUpdate;
  end;
end;

function TdxDocumentCapabilitiesOptions.BookmarksAllowed: Boolean;
begin
  Result := IsAllowed(Bookmarks);
end;

function TdxDocumentCapabilitiesOptions.CharacterFormattingAllowed: Boolean;
begin
  Result := IsAllowed(CharacterFormatting);
end;

function TdxDocumentCapabilitiesOptions.CharacterStyleAllowed: Boolean;
begin
  Result := IsAllowed(CharacterStyle);
end;

function TdxDocumentCapabilitiesOptions.EndNotesAllowed: Boolean;
begin
  Result := IsAllowed(EndNotes);
end;

function TdxDocumentCapabilitiesOptions.FloatingObjectsAllowed: Boolean;
begin
  Result := IsAllowed(FloatingObjects);
end;

function TdxDocumentCapabilitiesOptions.FootNotesAllowed: Boolean;
begin
  Result := IsAllowed(FootNotes);
end;

function TdxDocumentCapabilitiesOptions.HeadersFootersAllowed: Boolean;
begin
  Result := IsAllowed(HeadersFooters);
end;

function TdxDocumentCapabilitiesOptions.HyperlinksAllowed: Boolean;
begin
  Result := IsAllowed(Hyperlinks);
end;

procedure TdxDocumentCapabilitiesOptions.Initialize;
begin
  inherited Initialize;
  FNumbering := TdxNumberingOptions.Create;
  FNumbering.OnChanged := InnerOptionsChangeHandler;
end;

function TdxDocumentCapabilitiesOptions.InlinePicturesAllowed: Boolean;
begin
  Result := IsAllowed(InlinePictures);
end;

procedure TdxDocumentCapabilitiesOptions.InnerOptionsChangeHandler(Sender: TObject);
begin
  DoChanged;
end;

function TdxDocumentCapabilitiesOptions.ParagraphFormattingAllowed: Boolean;
begin
  Result := IsAllowed(ParagraphFormatting);
end;

function TdxDocumentCapabilitiesOptions.ParagraphsAllowed: Boolean;
begin
  Result := IsAllowed(Paragraphs);
end;

function TdxDocumentCapabilitiesOptions.ParagraphStyleAllowed: Boolean;
begin
  Result := IsAllowed(ParagraphStyle);
end;

function TdxDocumentCapabilitiesOptions.ParagraphTabsAllowed: Boolean;
begin
  Result := IsAllowed(ParagraphTabs);
end;

function TdxDocumentCapabilitiesOptions.SectionsAllowed: Boolean;
begin
  Result := IsAllowed(Sections);
end;

function TdxDocumentCapabilitiesOptions.TableCellStyleAllowed: Boolean;
begin
  Result := IsAllowed(TableCellStyle);
end;

function TdxDocumentCapabilitiesOptions.TablesAllowed: Boolean;
begin
  Result := IsAllowed(Tables);
end;

function TdxDocumentCapabilitiesOptions.TableStyleAllowed: Boolean;
begin
  Result := IsAllowed(TableStyle);
end;

function TdxDocumentCapabilitiesOptions.TabSymbolAllowed: Boolean;
begin
  Result := IsAllowed(TabSymbol);
end;

function TdxDocumentCapabilitiesOptions.UndoAllowed: Boolean;
begin
  Result := IsAllowed(Undo);
end;

procedure TdxDocumentCapabilitiesOptions.DoReset;
begin
  Bookmarks := TdxDocumentCapability.Default;
  CharacterFormatting := TdxDocumentCapability.Default;
  CharacterStyle := TdxDocumentCapability.Default;
  EndNotes := TdxDocumentCapability.Default;
  FloatingObjects := TdxDocumentCapability.Default;
  FootNotes := TdxDocumentCapability.Default;
  HeadersFooters := TdxDocumentCapability.Default;
  Hyperlinks := TdxDocumentCapability.Default;
  InlinePictures := TdxDocumentCapability.Default;
  ParagraphFormatting := TdxDocumentCapability.Default;
  Paragraphs := TdxDocumentCapability.Default;
  ParagraphStyle := TdxDocumentCapability.Default;
  ParagraphTabs := TdxDocumentCapability.Default;
  Sections := TdxDocumentCapability.Default;
  TableCellStyle := TdxDocumentCapability.Default;
  Tables := TdxDocumentCapability.Default;
  TableStyle := TdxDocumentCapability.Default;
  TabSymbol := TdxDocumentCapability.Default;
  Undo := TdxDocumentCapability.Default;
  Numbering.DoReset;
end;

function TdxDocumentCapabilitiesOptions.IsAllowed(const Value: TdxDocumentCapability): Boolean;
begin
  Result := Value in [TdxDocumentCapability.Default, TdxDocumentCapability.Enabled];
end;

procedure TdxDocumentCapabilitiesOptions.SetBookmarks(const Value: TdxDocumentCapability);
begin
  if FBookmarks <> Value then
  begin
    FBookmarks := Value;
    Changed;
  end;
end;

procedure TdxDocumentCapabilitiesOptions.SetCharacterFormatting(const Value: TdxDocumentCapability);
begin
  if FCharacterFormatting <> Value then
  begin
    FCharacterFormatting := Value;
    Changed;
  end;
end;

procedure TdxDocumentCapabilitiesOptions.SetCharacterStyle(const Value: TdxDocumentCapability);
begin
  if FCharacterStyle <> Value then
  begin
    FCharacterStyle := Value;
    Changed;
  end;
end;

procedure TdxDocumentCapabilitiesOptions.SetEndNotes(const Value: TdxDocumentCapability);
begin
  if FEndNotes <> Value then
  begin
    FEndNotes := Value;
    Changed;
  end;
end;

procedure TdxDocumentCapabilitiesOptions.SetFloatingObjects(const Value: TdxDocumentCapability);
begin
  if FFloatingObjects <> Value then
  begin
    FFloatingObjects := Value;
    Changed;
  end;
end;

procedure TdxDocumentCapabilitiesOptions.SetFootNotes(const Value: TdxDocumentCapability);
begin
  if FFootNotes <> Value then
  begin
    FFootNotes := Value;
    Changed;
  end;
end;

procedure TdxDocumentCapabilitiesOptions.SetHeadersFooters(const Value: TdxDocumentCapability);
begin
  if FHeadersFooters <> Value then
  begin
    FHeadersFooters := Value;
    Changed;
  end;
end;

procedure TdxDocumentCapabilitiesOptions.SetHyperlinks(const Value: TdxDocumentCapability);
begin
  if FHyperlinks <> Value then
  begin
    FHyperlinks := Value;
    Changed;
  end;
end;

procedure TdxDocumentCapabilitiesOptions.SetInlinePictures(const Value: TdxDocumentCapability);
begin
  if FInlinePictures <> Value then
  begin
    FInlinePictures := Value;
    Changed;
  end;
end;

procedure TdxDocumentCapabilitiesOptions.SetParagraphFormatting(const Value: TdxDocumentCapability);
begin
  if FParagraphFormatting <> Value then
  begin
    FParagraphFormatting := Value;
    Changed;
  end;
end;

procedure TdxDocumentCapabilitiesOptions.SetParagraphs(const Value: TdxDocumentCapability);
begin
  if FParagraphs <> Value then
  begin
    FParagraphs := Value;
    Changed;
  end;
end;

procedure TdxDocumentCapabilitiesOptions.SetParagraphStyle(const Value: TdxDocumentCapability);
begin
  if FParagraphStyle <> Value then
  begin
    FParagraphStyle := Value;
    Changed;
  end;
end;

procedure TdxDocumentCapabilitiesOptions.SetParagraphTabs(const Value: TdxDocumentCapability);
begin
  if FParagraphTabs <> Value then
  begin
    FParagraphTabs := Value;
    Changed;
  end;
end;

procedure TdxDocumentCapabilitiesOptions.SetSections(const Value: TdxDocumentCapability);
begin
  if FSections <> Value then
  begin
    FSections := Value;
    Changed;
  end;
end;

procedure TdxDocumentCapabilitiesOptions.SetTableCellStyle(const Value: TdxDocumentCapability);
begin
  if FTableCellStyle <> Value then
  begin
    FTableCellStyle := Value;
    Changed;
  end;
end;

procedure TdxDocumentCapabilitiesOptions.SetTables(const Value: TdxDocumentCapability);
begin
  if FTables <> Value then
  begin
    FTables := Value;
    Changed;
  end;
end;

procedure TdxDocumentCapabilitiesOptions.SetTableStyle(const Value: TdxDocumentCapability);
begin
  if FTableStyle <> Value then
  begin
    FTableStyle := Value;
    Changed;
  end;
end;

procedure TdxDocumentCapabilitiesOptions.SetTabSymbol(const Value: TdxDocumentCapability);
begin
  if FTabSymbol <> Value then
  begin
    FTabSymbol := Value;
    Changed;
  end;
end;

procedure TdxDocumentCapabilitiesOptions.SetUndo(const Value: TdxDocumentCapability);
begin
  if FUndo <> Value then
  begin
    FUndo := Value;
    Changed;
  end;
end;

procedure TdxDocumentCapabilitiesOptions.SetNumbering(const Value: TdxNumberingOptions);
begin
  Numbering.Assign(Value);
end;

{ TdxRichEditBehaviorOptions }

constructor TdxRichEditBehaviorOptions.Create;
begin
  inherited Create;
  FMaxZoomFactor := DefaultMaxZoomFactor;
  FMinZoomFactor := DefaultMinZoomFactor;
  FTabMarker := TdxCharacters.TabMark;
end;

procedure TdxRichEditBehaviorOptions.Assign(Source: TPersistent);
var
  ASource: TdxRichEditBehaviorOptions;
begin
  BeginUpdate;
  try
    if Source is TdxRichEditBehaviorOptions then
    begin
      ASource := TdxRichEditBehaviorOptions(Source);
      Drag := ASource.Drag;
      Drop := ASource.Drop;
      Copy := ASource.Copy;
      Paste := ASource.Paste;
      Cut := ASource.Cut;
      Printing := ASource.Printing;
      Zooming := ASource.Zooming;
      SaveAs := ASource.SaveAs;
      Save := ASource.Save;
      CreateNew := ASource.CreateNew;
      Open := ASource.Open;
      ShowPopupMenu := ASource.ShowPopupMenu;

      MinZoomFactor := ASource.MinZoomFactor;
      MaxZoomFactor := ASource.MaxZoomFactor;

      FontSource := ASource.FontSource;
      ForeColorSource := ASource.ForeColorSource;
      PasteSingleCellAsText := ASource.PasteSingleCellAsText;
      TabMarker := ASource.TabMarker;
      UseFontSubstitution := ASource.UseFontSubstitution;
      OvertypeAllowed := ASource.OvertypeAllowed;
      PageBreakInsertMode := ASource.pageBreakInsertMode;
    end;
    inherited Assign(Source);
  finally
    EndUpdate;
  end;
end;

function TdxRichEditBehaviorOptions.IsAllowed(AOption: TdxDocumentCapability): Boolean;
begin
  Result := (AOption = TdxDocumentCapability.Default) or (AOption = TdxDocumentCapability.Enabled);
end;

procedure TdxRichEditBehaviorOptions.DoReset;
begin
  BeginUpdate;
  try
    Drag := TdxDocumentCapability.Default;
    Drop := TdxDocumentCapability.Default;
    Copy := TdxDocumentCapability.Default;
    Paste := TdxDocumentCapability.Default;
    Cut := TdxDocumentCapability.Default;
    ShowPopupMenu := TdxDocumentCapability.Default;

    PageBreakInsertMode := TdxPageBreakInsertMode.NewLine;
    MaxZoomFactor := DefaultMaxZoomFactor;
    MinZoomFactor := DefaultMinZoomFactor;

    FontSource := TdxRichEditBaseValueSource.Auto;
    ForeColorSource := TdxRichEditBaseValueSource.Auto;
    TabMarker := TdxCharacters.TabMark; 

    PasteSingleCellAsText := False;
    UseFontSubstitution := True;
    OvertypeAllowed := True;

    ResetPrinting;
    ResetZooming;
    ResetSaveAs;
    ResetSave;
    ResetCreateNew;
    ResetOpen;
  finally
    EndUpdate;
  end;
end;

function TdxRichEditBehaviorOptions.ShouldSerializePrinting: Boolean;
begin
  Result := Printing <> TdxDocumentCapability.Default;
end;

procedure TdxRichEditBehaviorOptions.ResetPrinting;
begin
  Printing := TdxDocumentCapability.Default;
end;

function TdxRichEditBehaviorOptions.ShouldSerializeSaveAs: Boolean;
begin
  Result := SaveAs <> TdxDocumentCapability.Default;
end;

procedure TdxRichEditBehaviorOptions.ResetSaveAs;
begin
  SaveAs := TdxDocumentCapability.Default;
end;

function TdxRichEditBehaviorOptions.ShouldSerializeSave: Boolean;
begin
  Result := Save <> TdxDocumentCapability.Default;
end;

procedure TdxRichEditBehaviorOptions.ResetSave;
begin
  Save := TdxDocumentCapability.Default;
end;

function TdxRichEditBehaviorOptions.ShouldSerializeZooming: Boolean;
begin
  Result := Zooming <> TdxDocumentCapability.Default;
end;

procedure TdxRichEditBehaviorOptions.ResetZooming;
begin
  Zooming := TdxDocumentCapability.Default;
end;

function TdxRichEditBehaviorOptions.ShouldSerializeCreateNew: Boolean;
begin
  Result := CreateNew <> TdxDocumentCapability.Default;
end;

procedure TdxRichEditBehaviorOptions.ResetCreateNew;
begin
  CreateNew := TdxDocumentCapability.Default;
end;

function TdxRichEditBehaviorOptions.ShouldSerializeOpen: Boolean;
begin
  Result := Open <> TdxDocumentCapability.Default;
end;

procedure TdxRichEditBehaviorOptions.ResetOpen;
begin
  Open := TdxDocumentCapability.Default;
end;

function TdxRichEditBehaviorOptions.GetDragAllowed: Boolean;
begin
  Result := IsAllowed(Drag);
end;

function TdxRichEditBehaviorOptions.GetDropAllowed: Boolean;
begin
  Result := IsAllowed(Drop);
end;

function TdxRichEditBehaviorOptions.GetCopyAllowed: Boolean;
begin
  Result := IsAllowed(Copy);
end;

function TdxRichEditBehaviorOptions.GetPasteAllowed: Boolean;
begin
  Result := IsAllowed(Paste);
end;

function TdxRichEditBehaviorOptions.GetCutAllowed: Boolean;
begin
  Result := IsAllowed(Cut);
end;

function TdxRichEditBehaviorOptions.GetPrintingAllowed: Boolean;
begin
  Result := IsAllowed(Printing);
end;

function TdxRichEditBehaviorOptions.GetSaveAllowed: Boolean;
begin
  Result := IsAllowed(Save);
end;

function TdxRichEditBehaviorOptions.GetSaveAsAllowed: Boolean;
begin
  Result := IsAllowed(SaveAs);
end;

function TdxRichEditBehaviorOptions.GetCreateNewAllowed: Boolean;
begin
  Result := IsAllowed(CreateNew);
end;

function TdxRichEditBehaviorOptions.GetOpenAllowed: Boolean;
begin
  Result := IsAllowed(Open);
end;

function TdxRichEditBehaviorOptions.GetZoomingAllowed: Boolean;
begin
  Result := IsAllowed(Zooming);
end;

function TdxRichEditBehaviorOptions.GetShowPopupMenuAllowed: Boolean;
begin
  Result := IsAllowed(ShowPopupMenu);
end;

function TdxRichEditBehaviorOptions.GetOfficeScrollingAllowed: Boolean;
begin
  Result := IsAllowed(OfficeScrolling);
end;

function TdxRichEditBehaviorOptions.GetTouchAllowed: Boolean;
begin
  Result := IsAllowed(Touch);
end;

function TdxRichEditBehaviorOptions.IsMinZoomFactorStored: Boolean;
var
  ADefaultMinZoomFactor: Single;
begin
  ADefaultMinZoomFactor := DefaultMinZoomFactor;
  Result := not SameValue(FMinZoomFactor, ADefaultMinZoomFactor);
end;

function TdxRichEditBehaviorOptions.IsMaxZoomFactorStored: Boolean;
var
  ADefaultMaxZoomFactor: Single;
begin
  ADefaultMaxZoomFactor := DefaultMaxZoomFactor;
  Result := not SameValue(FMaxZoomFactor, ADefaultMaxZoomFactor);
end;

function TdxRichEditBehaviorOptions.IsTabMarkerStored: Boolean;
begin
  Result := FTabMarker <> TdxCharacters.TabMark;
end;

procedure TdxRichEditBehaviorOptions.SetCopy(const Value: TdxDocumentCapability);
begin
  if FAllowCopy <> Value then
  begin
    FAllowCopy := Value;
    Changed;
  end;
end;

procedure TdxRichEditBehaviorOptions.SetPaste(const Value: TdxDocumentCapability);
begin
  if FAllowPaste <> Value then
  begin
    FAllowPaste := Value;
    Changed;
  end;
end;

procedure TdxRichEditBehaviorOptions.SetCreateNew(const Value: TdxDocumentCapability);
begin
  if FAllowCreateNew <> Value then
  begin
    FAllowCreateNew := Value;
    Changed;
  end;
end;

procedure TdxRichEditBehaviorOptions.SetCut(const Value: TdxDocumentCapability);
begin
  if FAllowCut <> Value then
  begin
    FAllowCut := Value;
    Changed;
  end;
end;

procedure TdxRichEditBehaviorOptions.SetDrag(const Value: TdxDocumentCapability);
begin
  if FAllowDrag <> Value then
  begin
    FAllowDrag := Value;
    Changed;
  end;
end;

procedure TdxRichEditBehaviorOptions.SetDrop(const Value: TdxDocumentCapability);
begin
  if FAllowDrop <> Value then
  begin
    FAllowDrop := Value;
    Changed;
  end;
end;

procedure TdxRichEditBehaviorOptions.SetFontSource(const Value: TdxRichEditBaseValueSource);
begin
  if FFontSource <> Value then
  begin
    FFontSource := Value;
    Changed;
  end;
end;

procedure TdxRichEditBehaviorOptions.SetForeColorSource(const Value: TdxRichEditBaseValueSource);
begin
  if FForeColorSource <> Value then
  begin
    FForeColorSource := Value;
    Changed;
  end;
end;

procedure TdxRichEditBehaviorOptions.SetMaxZoomFactor(Value: Single);
begin
  Value := Min(Max(FMinZoomFactor, Value), DefaultMaxZoomFactor);
  if not SameValue(FMaxZoomFactor, Value) then
  begin
    FMaxZoomFactor := Value;
    Changed;
  end;
end;

procedure TdxRichEditBehaviorOptions.SetMinZoomFactor(Value: Single);
begin
  Value := Max(Min(FMaxZoomFactor, Value), DefaultMinZoomFactor);
  if not SameValue(FMinZoomFactor, Value) then
  begin
    FMinZoomFactor := Value;
    Changed;
  end;
end;

procedure TdxRichEditBehaviorOptions.SetOfficeScrolling(const Value: TdxDocumentCapability);
begin
  if FAllowOfficeScrolling <> Value then
  begin
    FAllowOfficeScrolling := Value;
    Changed;
  end;
end;

procedure TdxRichEditBehaviorOptions.SetOpen(const Value: TdxDocumentCapability);
begin
  if FAllowOpen <> Value then
  begin
    FAllowOpen := Value;
    Changed;
  end;
end;

procedure TdxRichEditBehaviorOptions.SetOvertypeAllowed(const Value: Boolean);
begin
  if FOvertypeAllowed <> Value then
  begin
    FOvertypeAllowed := Value;
    Changed;
  end;
end;

procedure TdxRichEditBehaviorOptions.SetPageBreakInsertMode(const Value: TdxPageBreakInsertMode);
begin
  if FPageBreakInsertMode <> Value then
  begin
    FPageBreakInsertMode := Value;
    Changed;
  end;
end;

procedure TdxRichEditBehaviorOptions.SetPasteSingleCellAsText(const Value: Boolean);
begin
  if FPasteSingleCellAsText <> Value then
  begin
    FPasteSingleCellAsText := Value;
    Changed;
  end;
end;

procedure TdxRichEditBehaviorOptions.SetPasteLineBreakSubstitution(const Value: TdxLineBreakSubstitute);
begin
  if FPasteLineBreakSubstitution <> Value then
  begin
    FPasteLineBreakSubstitution := Value;
    Changed;
  end;
end;

procedure TdxRichEditBehaviorOptions.SetPrinting(const Value: TdxDocumentCapability);
begin
  if FAllowPrinting <> Value then
  begin
    FAllowPrinting := Value;
    Changed;
  end;
end;

procedure TdxRichEditBehaviorOptions.SetSave(const Value: TdxDocumentCapability);
begin
  if FAllowSave <> Value then
  begin
    FAllowSave := Value;
    Changed;
  end;
end;

procedure TdxRichEditBehaviorOptions.SetSaveAs(const Value: TdxDocumentCapability);
begin
  if FAllowSaveAs <> Value then
  begin
    FAllowSaveAs := Value;
    Changed;
  end;
end;

procedure TdxRichEditBehaviorOptions.SetShowPopupMenu(const Value: TdxDocumentCapability);
begin
  if FAllowShowPopupMenu <> Value then
  begin
    FAllowShowPopupMenu := Value;
    Changed;
  end;
end;

procedure TdxRichEditBehaviorOptions.SetTabMarker(const Value: string);
begin
  if FTabMarker <> Value then
  begin
    FTabMarker := Value;
    Changed;
  end;
end;

procedure TdxRichEditBehaviorOptions.SetTouch(const Value: TdxDocumentCapability);
begin
  if FAllowTouch <> Value then
  begin
    FAllowTouch := Value;
    Changed;
  end;
end;

procedure TdxRichEditBehaviorOptions.SetUseFontSubstitution(const Value: Boolean);
begin
  if FUseFontSubstitution <> Value then
  begin
    FUseFontSubstitution := Value;
    Changed;
  end;
end;

procedure TdxRichEditBehaviorOptions.SetZooming(const Value: TdxDocumentCapability);
begin
  if FAllowZooming <> Value then
  begin
    FAllowZooming := Value;
    Changed;
  end;
end;

{ TdxRichEditEditingOptions }

procedure TdxRichEditEditingOptions.Assign(Source: TPersistent);
var
  ASource: TdxRichEditEditingOptions;
begin
  BeginUpdate;
  try
    if Source is TdxRichEditEditingOptions then
    begin
      ASource := TdxRichEditEditingOptions(Source);
      MergeParagraphsContent := ASource.MergeParagraphsContent;
      MergeUseFirstParagraphStyle := ASource.MergeUseFirstParagraphStyle;
    end;
    inherited Assign(Source);
  finally
    EndUpdate;
  end;
end;

procedure TdxRichEditEditingOptions.DoReset;
begin
  inherited DoReset;
  FMergeParagraphsContent := True;
  FMergeUseFirstParagraphStyle := True;
end;

procedure TdxRichEditEditingOptions.SetMergeParagraphsContent(const Value: Boolean);
begin
  if FMergeParagraphsContent <> Value then
  begin
    FMergeParagraphsContent := Value;
    Changed;
  end;
end;

procedure TdxRichEditEditingOptions.SetMergeUseFirstParagraphStyle(const Value: Boolean);
begin
  if FMergeUseFirstParagraphStyle <> Value then
  begin
    FMergeUseFirstParagraphStyle := Value;
    Changed;
  end;
end;

{ TdxRichEditMailMergeOptions }

procedure TdxRichEditMailMergeOptions.DoReset;
begin
  DataSource := nil;
  DataMember := EmptyStr;
  ViewMergedData := False;
  ActiveRecord := 0;
end;

procedure TdxRichEditMailMergeOptions.SetActiveRecord(const Value: Integer);
begin
  if FActiveRecord = Value then
    Exit;
  FActiveRecord := Value;
  DoChanged;
end;

procedure TdxRichEditMailMergeOptions.SetDataMember(const Value: string);
begin
  if Value = FDataMember then
    Exit;
  FDataMember := Value;
  DoChanged;
end;

procedure TdxRichEditMailMergeOptions.SetDataSource(const Value: TObject);
begin
  if FDataSource = Value then
    Exit;
  FDataSource := Value;
  DoChanged;
end;

procedure TdxRichEditMailMergeOptions.SetViewMergedData(const Value: Boolean);
begin
  if Value = FViewMergedData then
    Exit;
  FViewMergedData := Value;
  DoChanged;
end;

{ TdxFieldUpdateOnLoadOptions }

constructor TdxFieldUpdateOnLoadOptions.Create(AUpdateDateField, AUpdateTimeField: Boolean);
begin
  inherited Create;
  FUpdateDateField := AUpdateDateField;
  FUpdateTimeField := AUpdateTimeField;
end;

{ TdxUpdateFieldOptions }

constructor TdxUpdateFieldOptions.Create;
begin
  inherited Create;
  FDate := FDefaultDate; 
  FTime := FDefaultTime; 
end;

procedure TdxUpdateFieldOptions.CopyFrom(ASource: TdxUpdateFieldOptions);
begin
  Date := ASource.Date;
  Time := ASource.Time;
end;

procedure TdxUpdateFieldOptions.DoReset;
begin
  Date := FDefaultDate;
  Time := FDefaultTime;
end;

function TdxUpdateFieldOptions.GetNativeOptions: TdxFieldUpdateOnLoadOptions;
begin
  Result := TdxFieldUpdateOnLoadOptions.Create(Date, Time);
end;

procedure TdxUpdateFieldOptions.SetDate(const Value: Boolean);
begin
  if Date = Value then
    Exit;
  FDate := Value;
  DoChanged; 
end;

procedure TdxUpdateFieldOptions.SetTime(const Value: Boolean);
begin
  if Time = Value then
    Exit;
  FTime := Value;
  DoChanged; 
end;

{ TdxDocumentImporterOptions }

constructor TdxDocumentImporterOptions.Create;
begin
  inherited Create;
  CreateUpdateFieldOptions;
end;

destructor TdxDocumentImporterOptions.Destroy;
begin
  FreeAndNil(FUpdateField);
  inherited Destroy;
end;

procedure TdxDocumentImporterOptions.CopyFrom(const AValue: IdxImporterOptions);
var
  AOptions: TdxDocumentImporterOptions;
begin
  if AValue is TdxDocumentImporterOptions then
  begin
    AOptions := TdxDocumentImporterOptions(AValue);
    ActualEncoding := AOptions.ActualEncoding;
    LineBreakSubstitute := AOptions.LineBreakSubstitute;
    UpdateField.CopyFrom(AOptions.UpdateField);
  end;
end;

procedure TdxDocumentImporterOptions.DoReset;
begin
  FActualEncoding := nil; 
  SourceUri := EmptyStr;
  LineBreakSubstitute := TdxLineBreakSubstitute.None;
  CreateUpdateFieldOptions;
  UpdateField.DoReset;
end;

procedure TdxDocumentImporterOptions.CreateUpdateFieldOptions;
begin
  if FUpdateField <> nil then
    Exit;
  FreeAndNil(FUpdateField);
  FUpdateField := TdxUpdateFieldOptions.Create;
  FUpdateField.OnChanged := OnInnerOptionsChanged; 
end;

function TdxDocumentImporterOptions.GetActualEncoding: TEncoding;
begin
  Result := FActualEncoding;
end;

function TdxDocumentImporterOptions.GetSourceUri: string;
begin
  Result := FSourceUri;
end;

procedure TdxDocumentImporterOptions.OnInnerOptionsChanged(ASender: TObject);
begin
  OnChanged(ASender);
end;

procedure TdxDocumentImporterOptions.SetActualEncoding(const Value: TEncoding);
begin
  if FActualEncoding = Value then
    Exit;
  FActualEncoding := Value;
  DoChanged; 
end;

procedure TdxDocumentImporterOptions.SetSourceUri(const Value: string);
begin
  FSourceUri := Value;
end;

function TdxDocumentImporterOptions.ShouldSerializeSourceUri: Boolean;
begin
  Result := False;
end;

{ TdxRtfDocumentImporterOptions }

procedure TdxRtfDocumentImporterOptions.CopyFrom(const AValue: IdxImporterOptions);
var
  AOptions: TdxRtfDocumentImporterOptions;
begin
  inherited CopyFrom(AValue);
  if AValue is TdxRtfDocumentImporterOptions then
  begin
    AOptions := TdxRtfDocumentImporterOptions(AValue);
    SuppressLastParagraphDelete := AOptions.SuppressLastParagraphDelete;
    CopySingleCellAsText := AOptions.CopySingleCellAsText;
    PasteFromIE := AOptions.PasteFromIE;
  end;
end;

function TdxRtfDocumentImporterOptions.Format: TdxDocumentFormat;
begin
  Result := TdxDocumentFormat.Rtf;
end;

procedure TdxRtfDocumentImporterOptions.DoReset;
begin
  inherited DoReset;
  SuppressLastParagraphDelete := False;
  CopySingleCellAsText := False;
  PasteFromIE := False;
end;

procedure TdxRtfDocumentImporterOptions.SetActualEncoding(const Value: TEncoding);
begin
end;

{ TdxPlainTextDocumentImporterOptions }

procedure TdxPlainTextDocumentImporterOptions.CopyFrom(const AValue: IdxImporterOptions);
var
  AOptions: TdxPlainTextDocumentImporterOptions;
begin
  inherited CopyFrom(AValue);
  if AValue is TdxPlainTextDocumentImporterOptions then
  begin
    AOptions := TdxPlainTextDocumentImporterOptions(AValue);
    AutoDetectEncoding := AOptions.AutoDetectEncoding;
  end;
end;

function TdxPlainTextDocumentImporterOptions.Format: TdxDocumentFormat;
begin
  Result := TdxDocumentFormat.PlainText;
end;

procedure TdxPlainTextDocumentImporterOptions.DoReset;
begin
  inherited DoReset;
  AutoDetectEncoding := True;
end;

procedure TdxPlainTextDocumentImporterOptions.SetAutoDetectEncoding(const Value: Boolean);
begin
  if FAutoDetectEncoding <> Value then
  begin
    FAutoDetectEncoding := Value;
    Changed;
  end;
end;

{ TdxHtmlDocumentImporterOptions }

function TdxHtmlDocumentImporterOptions.Format: TdxDocumentFormat;
begin
  Result := TdxDocumentFormat.Html;
end;

{ TdxMhtDocumentImporterOptions }

function TdxMhtDocumentImporterOptions.Format: TdxDocumentFormat;
begin
  Result := TdxDocumentFormat.Mht;
end;

{ TdxOpenXmlDocumentImporterOptions }

function TdxOpenXmlDocumentImporterOptions.Format: TdxDocumentFormat;
begin
  Result := TdxDocumentFormat.OpenXml;
end;

{ TdxOpenDocumentImporterOptions }

function TdxOpenDocumentImporterOptions.Format: TdxDocumentFormat;
begin
  Result := TdxDocumentFormat.OpenDocument;
end;

{ TdxWordMLDocumentImporterOptions }

procedure TdxWordMLDocumentImporterOptions.DoReset;
begin
  inherited DoReset;
  ActualEncoding := TEncoding.UTF8;  
end;

function TdxWordMLDocumentImporterOptions.Format: TdxDocumentFormat;
begin
  Result := TdxDocumentFormat.WordML;
end;

{ TdxRichEditDocumentImportOptions }

procedure TdxRichEditDocumentImportOptions.CopyFrom(AOptions: TdxRichEditDocumentImportOptions);
var
  AKey: TdxDocumentFormat;
  ASourceOptions: TdxDocumentImporterOptions;
begin
  for AKey in FOptionsTable.Keys do
  begin
    ASourceOptions := AOptions.GetOptions(AKey);
    if ASourceOptions <> nil then
      FOptionsTable[AKey].CopyFrom(ASourceOptions);
  end;
  FallbackFormat := AOptions.FallbackFormat;
end;

constructor TdxRichEditDocumentImportOptions.Create;
begin
  inherited Create;
  FRtf := CreateRtfOptions;
  FPlainText := CreatePlainTextOptions;
  FHtml := CreateHtmlOptions;
  FMht := CreateMhtOptions;
  FOpenXml := CreateOpenXmlOptions;
  FOpenDocument := CreateOpenDocumentOptions;
  FWordML := CreateWordMLOptions;

  FOptionsTable := TDictionary<TdxDocumentFormat, TdxDocumentImporterOptions>.Create;
  RegisterOptions(FRtf);
  RegisterOptions(FPlainText);
  RegisterOptions(FHtml);
  RegisterOptions(FMht);
  RegisterOptions(FOpenXml);
  RegisterOptions(FOpenDocument);
  RegisterOptions(FWordML);
end;

destructor TdxRichEditDocumentImportOptions.Destroy;
begin
  FreeAndNil(FWordML);
  FreeAndNil(FOpenDocument);
  FreeAndNil(FOpenXml);
  FreeAndNil(FMht);
  FreeAndNil(FHtml);
  FreeAndNil(FPlainText);
  FreeAndNil(FRtf);
  FreeAndNil(FOptionsTable);
  inherited Destroy;
end;

procedure TdxRichEditDocumentImportOptions.DoReset;
var
  AKey: TdxDocumentFormat;
begin
  if FOptionsTable <> nil then
    for AKey in FOptionsTable.Keys do
      FOptionsTable[AKey].Reset;
  FallbackFormat := FFallbackFormatValue;
end;

function TdxRichEditDocumentImportOptions.GetOptions(AFormat: TdxDocumentFormat): TdxDocumentImporterOptions;
begin
  if not FOptionsTable.TryGetValue(AFormat, Result) then
    Result := nil;
end;

class constructor TdxRichEditDocumentImportOptions.Initialize;
begin
  FFallbackFormatValue := TdxDocumentFormat.PlainText;
end;

procedure TdxRichEditDocumentImportOptions.RegisterOptions(AOptions: TdxDocumentImporterOptions);
begin
  FOptionsTable.Add(AOptions.Format, AOptions);
end;

procedure TdxRichEditDocumentImportOptions.ResetFallbackFormat;
begin
  FallbackFormat := FFallbackFormatValue;
end;

procedure TdxRichEditDocumentImportOptions.SetFallbackFormat(const Value: TdxDocumentFormat);
begin
  if FFallbackFormat = Value then
    Exit;
  FFallbackFormat := Value;
  DoChanged; 
end;

function TdxRichEditDocumentImportOptions.ShouldSerializeFallbackFormat: Boolean;
begin
  Result := FallbackFormat <> FFallbackFormatValue;
end;

function TdxRichEditDocumentImportOptions.CreateHtmlOptions: TdxHtmlDocumentImporterOptions;
begin
  Result := TdxHtmlDocumentImporterOptions.Create;
end;

function TdxRichEditDocumentImportOptions.CreateMhtOptions: TdxMhtDocumentImporterOptions;
begin
  Result := TdxMhtDocumentImporterOptions.Create;
end;

function TdxRichEditDocumentImportOptions.CreateOpenDocumentOptions: TdxOpenDocumentImporterOptions;
begin
  Result := TdxOpenDocumentImporterOptions.Create;
end;

function TdxRichEditDocumentImportOptions.CreateOpenXmlOptions: TdxOpenXmlDocumentImporterOptions;
begin
  Result := TdxOpenXmlDocumentImporterOptions.Create;
end;

function TdxRichEditDocumentImportOptions.CreatePlainTextOptions: TdxPlainTextDocumentImporterOptions;
begin
  Result := TdxPlainTextDocumentImporterOptions.Create;
end;

function TdxRichEditDocumentImportOptions.CreateRtfOptions: TdxRtfDocumentImporterOptions;
begin
  Result := TdxRtfDocumentImporterOptions.Create;
end;

function TdxRichEditDocumentImportOptions.CreateWordMLOptions: TdxWordMLDocumentImporterOptions;
begin
  Result := TdxWordMLDocumentImporterOptions.Create;
end;

{ TdxDocumentExporterOptions }

procedure TdxDocumentExporterOptions.CopyFrom(const AValue: IdxExporterOptions);
begin
  if AValue is TdxDocumentExporterOptions then
    ActualEncoding := TdxDocumentExporterOptions(AValue).ActualEncoding;
end;

procedure TdxDocumentExporterOptions.DoReset;
begin
  FActualEncoding := TEncoding.Default;
  TargetUri := EmptyStr;
end;

procedure TdxDocumentExporterOptions.SetActualEncoding(const Value: TEncoding);
begin
  if FActualEncoding = Value then
    Exit;
  FActualEncoding := Value;
  DoChanged; 
end;

function TdxDocumentExporterOptions.ShouldSerializeTargetUri: Boolean;
begin
  Result := False;
end;

{ TdxRtfDocumentExporterCompatibilityOptions }

procedure TdxRtfDocumentExporterCompatibilityOptions.CopyFrom(const AValue: TdxRtfDocumentExporterCompatibilityOptions);
begin
  DuplicateObjectAsMetafile := AValue.DuplicateObjectAsMetafile;
  BackColorExportMode := AValue.BackColorExportMode;
end;

procedure TdxRtfDocumentExporterCompatibilityOptions.DoReset;
begin
  ResetDuplicateObjectAsMetafile;
  ResetBackColorExportMode;
end;

procedure TdxRtfDocumentExporterCompatibilityOptions.ResetBackColorExportMode;
begin
  BackColorExportMode := TdxRtfRunBackColorExportMode.Chcbpat;
end;

procedure TdxRtfDocumentExporterCompatibilityOptions.ResetDuplicateObjectAsMetafile;
begin
  DuplicateObjectAsMetafile := False;
end;

procedure TdxRtfDocumentExporterCompatibilityOptions.SetBackColorExportMode(const Value: TdxRtfRunBackColorExportMode);
begin
  if FBackColorExportMode = Value then
    Exit;
  FBackColorExportMode := Value;
  DoChanged; 
end;

procedure TdxRtfDocumentExporterCompatibilityOptions.SetDuplicateObjectAsMetafile(const Value: Boolean);
begin
  if FDuplicateObjectAsMetafile = Value then
    Exit;
  FDuplicateObjectAsMetafile := Value;
  DoChanged; 
end;

function TdxRtfDocumentExporterCompatibilityOptions.ShouldSerializeBackColorExportMode: Boolean;
begin
  Result := BackColorExportMode <> TdxRtfRunBackColorExportMode.Chcbpat;
end;

function TdxRtfDocumentExporterCompatibilityOptions.ShouldSerializeDuplicateObjectAsMetafile: Boolean;
begin
  Result := DuplicateObjectAsMetafile <> False;
end;

{ TdxRtfDocumentExporterOptions }

constructor TdxRtfDocumentExporterOptions.Create;
begin
  inherited Create; 
  FCompatibility := CreateCompatibilityOptions;
end;

destructor TdxRtfDocumentExporterOptions.Destroy;
begin
  FreeAndNil(FCompatibility);
  inherited Destroy;
end;

procedure TdxRtfDocumentExporterOptions.CopyFrom(const Value: IdxExporterOptions);
var
  AOptions: TdxRtfDocumentExporterOptions;
begin
  inherited CopyFrom(Value);
  if Value is TdxRtfDocumentExporterOptions then
  begin
    AOptions := TdxRtfDocumentExporterOptions(Value);
    WrapContentInGroup := AOptions.WrapContentInGroup;
    ListExportFormat := AOptions.ListExportFormat;
    Compatibility.CopyFrom(AOptions.Compatibility);
    ExportFinalParagraphMark := AOptions.ExportFinalParagraphMark;
  end;
end;

function TdxRtfDocumentExporterOptions.CreateCompatibilityOptions: TdxRtfDocumentExporterCompatibilityOptions;
begin
  Result := TdxRtfDocumentExporterCompatibilityOptions.Create;
end;

procedure TdxRtfDocumentExporterOptions.DoReset;
begin
  inherited DoReset;
  ListExportFormat := TdxRtfNumberingListExportFormat.RtfFormat;
  WrapContentInGroup := False;
  ExportFinalParagraphMark := TdxExportFinalParagraphMark.Always;
  if FCompatibility <> nil then
    FCompatibility.Reset;
end;

function TdxRtfDocumentExporterOptions.Format: TdxDocumentFormat;
begin
  Result := TdxDocumentFormat.Rtf;
end;

procedure TdxRtfDocumentExporterOptions.ResetExportFinalParagraphMark;
begin
  ExportFinalParagraphMark := TdxExportFinalParagraphMark.Always;
end;

procedure TdxRtfDocumentExporterOptions.ResetListExportFormat;
begin
  ListExportFormat := TdxRtfNumberingListExportFormat.RtfFormat;
end;

procedure TdxRtfDocumentExporterOptions.ResetWrapContentInGroup;
begin
  WrapContentInGroup := False;
end;

procedure TdxRtfDocumentExporterOptions.SetActualEncoding(const Value: TEncoding);
begin
end;

procedure TdxRtfDocumentExporterOptions.SetExportFinalParagraphMark(const Value: TdxExportFinalParagraphMark);
begin
  if FExportFinalParagraphMark = Value then
    Exit;
  FExportFinalParagraphMark := Value;
  DoChanged; 
end;

procedure TdxRtfDocumentExporterOptions.SetListExportFormat(const Value: TdxRtfNumberingListExportFormat);
begin
  if FListExportFormat = Value then
    Exit;

  FListExportFormat := Value;
  DoChanged; 
end;

procedure TdxRtfDocumentExporterOptions.SetWrapContentInGroup(const Value: Boolean);
begin
  if FWrapContentInGroup = Value then
    Exit;
  FWrapContentInGroup := Value;
  DoChanged; 
end;

function TdxRtfDocumentExporterOptions.ShouldSerializeExportFinalParagraphMark: Boolean;
begin
  Result := ExportFinalParagraphMark <> TdxExportFinalParagraphMark.Always;
end;

function TdxRtfDocumentExporterOptions.ShouldSerializeListExportFormat: Boolean;
begin
  Result := ListExportFormat <> TdxRtfNumberingListExportFormat.RtfFormat;
end;

function TdxRtfDocumentExporterOptions.ShouldSerializeWrapContentInGroup: Boolean;
begin
  Result := WrapContentInGroup <> False;
end;

{ TdxPlainTextDocumentExporterOptions }

constructor TdxPlainTextDocumentExporterOptions.Create;
begin
  inherited Create;
  FExportBulletsAndNumbering := True;
end;

{ TdxPlainTextDocumentExporterOptions }

function TdxPlainTextDocumentExporterOptions.Format: TdxDocumentFormat;
begin
  Result := TdxDocumentFormat.PlainText;
end;

{ TdxHtmlDocumentExporterOptions }

function TdxHtmlDocumentExporterOptions.Format: TdxDocumentFormat;
begin
  Result := TdxDocumentFormat.Html;
end;

{ TdxMhtDocumentExporterOptions }

function TdxMhtDocumentExporterOptions.Format: TdxDocumentFormat;
begin
  Result := TdxDocumentFormat.Mht;
end;

{ TdxOpenXmlDocumentExporterOptions }

function TdxOpenXmlDocumentExporterOptions.Format: TdxDocumentFormat;
begin
  Result := TdxDocumentFormat.OpenXml;
end;

{ TdxOpenDocumentExporterOptions }

function TdxOpenDocumentExporterOptions.Format: TdxDocumentFormat;
begin
  Result := TdxDocumentFormat.OpenDocument;
end;

{ TdxWordMLDocumentExporterOptions }

function TdxWordMLDocumentExporterOptions.Format: TdxDocumentFormat;
begin
  Result := TdxDocumentFormat.WordML;
end;

{ TdxDocDocumentExporterOptions }

function TdxDocDocumentExporterOptions.Format: TdxDocumentFormat;
begin
  Result := TdxDocumentFormat.Doc;
end;

{ TdxRichEditDocumentExportOptions }

constructor TdxRichEditDocumentExportOptions.Create;
begin
  inherited Create;
  FRtf := CreateRtfOptions;
  FPlainText := CreatePlainTextOptions;
  FHtml := CreateHtmlOptions;
  FMht := CreateMhtOptions;
  FOpenXml := CreateOpenXmlOptions;
  FOpenDocument := CreateOpenDocumentOptions;
  FWordML := CreateWordMLOptions;
  FDoc := CreateDocOptions;

  FOptionsTable := TDictionary<TdxDocumentFormat, TdxDocumentExporterOptions>.Create;
  RegisterOptions(FRtf);
  RegisterOptions(FPlainText);
  RegisterOptions(FHtml);
  RegisterOptions(FMht);
  RegisterOptions(FOpenXml);
  RegisterOptions(FOpenDocument);
  RegisterOptions(FWordML);
  RegisterOptions(FDoc);
end;

destructor TdxRichEditDocumentExportOptions.Destroy;
begin
  FreeAndNil(FOptionsTable);
  FreeAndNil(FDoc);
  FreeAndNil(FWordML);
  FreeAndNil(FOpenDocument);
  FreeAndNil(FOpenXml);
  FreeAndNil(FMht);
  FreeAndNil(FHtml);
  FreeAndNil(FPlainText);
  FreeAndNil(FRtf);
  inherited Destroy;
end;

procedure TdxRichEditDocumentExportOptions.CopyFrom(AOptions: TdxRichEditDocumentExportOptions);
var
  AKey: TdxDocumentFormat;
  ASourceOptions: TdxDocumentExporterOptions;
begin
  for AKey in FOptionsTable.Keys do
  begin
    ASourceOptions := AOptions.GetOptions(AKey);
    if ASourceOptions <> nil then
      FOptionsTable[AKey].CopyFrom(ASourceOptions);
  end;
end;

function TdxRichEditDocumentExportOptions.CreateDocOptions: TdxDocDocumentExporterOptions;
begin
  Result := TdxDocDocumentExporterOptions.Create;
end;

function TdxRichEditDocumentExportOptions.CreateHtmlOptions: TdxHtmlDocumentExporterOptions;
begin
  Result := TdxHtmlDocumentExporterOptions.Create;
end;

function TdxRichEditDocumentExportOptions.CreateMhtOptions: TdxMhtDocumentExporterOptions;
begin
  Result := TdxMhtDocumentExporterOptions.Create;
end;

function TdxRichEditDocumentExportOptions.CreateOpenDocumentOptions: TdxOpenDocumentExporterOptions;
begin
  Result := TdxOpenDocumentExporterOptions.Create;
end;

function TdxRichEditDocumentExportOptions.CreateOpenXmlOptions: TdxOpenXmlDocumentExporterOptions;
begin
  Result := TdxOpenXmlDocumentExporterOptions.Create;
end;

function TdxRichEditDocumentExportOptions.CreatePlainTextOptions: TdxPlainTextDocumentExporterOptions;
begin
  Result := TdxPlainTextDocumentExporterOptions.Create;
end;

function TdxRichEditDocumentExportOptions.CreateRtfOptions: TdxRtfDocumentExporterOptions;
begin
  Result := TdxRtfDocumentExporterOptions.Create;
end;

function TdxRichEditDocumentExportOptions.CreateWordMLOptions: TdxWordMLDocumentExporterOptions;
begin
  Result := TdxWordMLDocumentExporterOptions.Create;
end;

procedure TdxRichEditDocumentExportOptions.DoReset;
var
  AKey: TdxDocumentFormat;
begin
  if FOptionsTable <> nil then
    for AKey in FOptionsTable.Keys do
      FOptionsTable[AKey].Reset;
end;

function TdxRichEditDocumentExportOptions.GetOptions(AFormat: TdxDocumentFormat): TdxDocumentExporterOptions;
begin
  if not FOptionsTable.TryGetValue(AFormat, Result) then
    Result := nil;
end;

procedure TdxRichEditDocumentExportOptions.RegisterOptions(AOptions: TdxDocumentExporterOptions);
begin
  FOptionsTable.Add(AOptions.Format, AOptions);
end;

{ TdxCopyPasteOptions }

procedure TdxCopyPasteOptions.DoReset;
begin
  inherited DoReset;
  InsertOptions := TdxInsertOptions.MatchDestinationFormatting;
  MaintainDocumentSectionSettings := False;
end;

procedure TdxCopyPasteOptions.SetInsertOptions(const Value: TdxInsertOptions);
begin
  if FInsertOptions <> Value then
  begin
    FInsertOptions := Value;
    Changed;
  end;
end;

procedure TdxCopyPasteOptions.SetMaintainDocumentSectionSettings(const Value: Boolean);
begin
  if FMaintainDocumentSectionSettings <> Value then
  begin
    FMaintainDocumentSectionSettings := Value;
    Changed;
  end;
end;

{ TdxFormattingMarkVisibilityOptions }

procedure TdxFormattingMarkVisibilityOptions.DoReset;
begin
  ParagraphMark := DefaultParagraphMarkVisibility;
  TabCharacter := DefaultTabCharacterVisibility;
  Space := DefaultSpaceVisibility;
  Separator := DefaultSeparatorVisibility;
  HiddenText := DefaultHiddenTextVisibility;

  ShowHiddenText := False;
end;

procedure TdxFormattingMarkVisibilityOptions.SetHiddenText(const Value: TdxRichEditFormattingMarkVisibility);
begin
  if HiddenText = Value then
    Exit;
  FHiddenText := Value;
  DoChanged; 
end;

procedure TdxFormattingMarkVisibilityOptions.SetParagraphMark(const Value: TdxRichEditFormattingMarkVisibility);
begin
  if ParagraphMark = Value then
    Exit;
  FParagraphMark := Value;
  DoChanged; 
end;

procedure TdxFormattingMarkVisibilityOptions.SetSeparator(const Value: TdxRichEditFormattingMarkVisibility);
begin
  if Separator = Value then
    Exit;
  FSeparator := Value;
  DoChanged; 
end;

procedure TdxFormattingMarkVisibilityOptions.SetShowHiddenText(const Value: Boolean);
begin
  if ShowHiddenText = Value then
    Exit;
  FShowHiddenText := Value;
  DoChanged; 
end;

procedure TdxFormattingMarkVisibilityOptions.SetSpace(const Value: TdxRichEditFormattingMarkVisibility);
begin
  if Space = Value then
    Exit;
  FSpace := Value;
  DoChanged; 
end;

procedure TdxFormattingMarkVisibilityOptions.SetTabCharacter(const Value: TdxRichEditFormattingMarkVisibility);
begin
  if TabCharacter = Value then
    Exit;
  FTabCharacter := Value;
  DoChanged; 
end;

{ TdxNumberingOptions }

procedure TdxNumberingOptions.Assign(Source: TPersistent);
var
  ASource: TdxNumberingOptions;
begin
  BeginUpdate;
  try
    if Source is TdxNumberingOptions then
    begin
      ASource := TdxNumberingOptions(Source);
      Bulleted := ASource.Bulleted;
      Simple := ASource.Simple;
      MultiLevel := ASource.MultiLevel;
    end;
    inherited Assign(Source);
  finally
    EndUpdate;
  end;
end;

procedure TdxNumberingOptions.DoReset;
begin
  FBulleted := TdxDocumentCapability.Default;
  FMultiLevel := TdxDocumentCapability.Default;
  FSimple := TdxDocumentCapability.Default;
end;

function TdxNumberingOptions.GetBulletedAllowed: Boolean;
begin
  Result := IsAllowed(Bulleted);
end;

function TdxNumberingOptions.GetMultiLevelAllowed: Boolean;
begin
  Result := IsAllowed(MultiLevel);
end;

function TdxNumberingOptions.GetSimpleAllowed: Boolean;
begin
  Result := IsAllowed(Simple);
end;

function TdxNumberingOptions.IsAllowed(AOption: TdxDocumentCapability): Boolean;
begin
  Result := (AOption = TdxDocumentCapability.Default) or (AOption = TdxDocumentCapability.Enabled);
end;

procedure TdxNumberingOptions.SetBulleted(const Value: TdxDocumentCapability);
begin
  if FBulleted = Value then
    Exit;
  FBulleted := Value;
  DoChanged; 
end;

procedure TdxNumberingOptions.SetMultiLevel(const Value: TdxDocumentCapability);
begin
  if FMultiLevel = Value then
    Exit;
  FMultiLevel := Value;
  DoChanged; 
end;

procedure TdxNumberingOptions.SetSimple(const Value: TdxDocumentCapability);
begin
  if FSimple = Value then
    Exit;
  FSimple := Value;
  DoChanged; 
end;

{ TdxHtmlDocumentExporterOptions }

function TdxHtmlDocumentExporterOptions.GetEncoding: TEncoding;
begin
  Result := ActualEncoding;
end;

procedure TdxHtmlDocumentExporterOptions.SetEncoding(const Value: TEncoding);
begin
  ActualEncoding := Value;
end;

end.
