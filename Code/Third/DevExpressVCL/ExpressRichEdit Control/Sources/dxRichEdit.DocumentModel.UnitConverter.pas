{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.DocumentModel.UnitConverter;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Types, dxCore, dxCoreClasses, dxRichEdit.DocumentLayout.UnitConverter, dxRichEdit.DocumentModel.UnitToLayoutUnitConverter;

type
  IdxDocumentModelUnitConverter = interface
    function TwipsToModelUnits(const AValue: Integer): Integer; overload;
    function TwipsToModelUnits(const AValue: TSize): TSize; overload;
    function MillimetersToModelUnitsF(const AValue: Single): Single;
    function PointsToModelUnits(const AValue: Integer): Integer;
    function PointsToModelUnitsF(const AValue: Single): Single;
    function PixelsToModelUnits(const AValue: Integer; const ADpi: Single): Integer; overload;
    function PixelsToModelUnits(const AValue: TSize; const ADpiX, ADpiY: Single): TSize; overload;
    function HundredthsOfInchToModelUnits(const AValue: Integer): Integer; overload;
    function HundredthsOfInchToModelUnits(const AValue: TSize): TSize; overload;
    function HundredthsOfMillimeterToModelUnits(const AValue: Integer): Integer; overload;
    function HundredthsOfMillimeterToModelUnits(const AValue: TSize): TSize; overload;
    function HundredthsOfMillimeterToModelUnitsRound(const AValue: Integer): Integer;
    function CentimetersToModelUnitsF(const AValue: Single): Single;
    function InchesToModelUnitsF(const AValue: Single): Single;
    function PicasToModelUnitsF(const AValue: Single): Single;
    function DocumentsToModelUnits(const AValue: Integer): Integer; overload;
    function DocumentsToModelUnits(const AValue: TSize): TSize; overload;
    function DocumentsToModelUnitsF(const AValue: Single): Single;
    function DegreeToModelUnits(const AValue: Single): Integer;
    function FDToModelUnits(const AValue: Integer): Integer;
    function ModelUnitsToTwips(const AValue: Integer): Integer; overload;
    function ModelUnitsToTwipsF(const AValue: Single): Single;
    function ModelUnitsToTwips(const AValue: TSize): TSize; overload;
    function ModelUnitsToHundredthsOfMillimeter(const AValue: TSize): TSize;
    function ModelUnitsToPointsF(const AValue: Single): Single;
    function ModelUnitsToPointsFRound(const AValue: Single): Single;
    function ModelUnitsToPixels(const AValue: Integer; const ADpi: Single): Integer;
    function ModelUnitsToPixelsF(const AValue: Single; const ADpi: Single): Single;
    function ModelUnitsToCentimetersF(const AValue: Single): Single;
    function ModelUnitsToInchesF(const AValue: Single): Single;
    function ModelUnitsToMillimetersF(const AValue: Single): Single;
    function ModelUnitsToDocumentsF(const AValue: Single): Single;
    function ModelUnitsToHundredthsOfInch(const AValue: Integer): Integer; overload;
    function ModelUnitsToHundredthsOfInch(const AValue: TSize): TSize; overload;
    function ModelUnitsToDegree(const AValue: Integer): Integer;
    function ModelUnitsToFD(const AValue: Integer): Integer;
  end;

  { TdxDocumentModelUnitConverter }

  TdxDocumentModelUnitConverter = class(TcxIUnknownObject, IdxDocumentModelUnitConverter)
  public
    function CreateConverterToLayoutUnits(const AUnit: TdxDocumentLayoutUnit; const ADpi: Single): TdxDocumentModelUnitToLayoutUnitConverter; virtual; abstract;
    function TwipsToModelUnits(const AValue: Integer): Integer; overload; virtual; abstract;
    function TwipsToModelUnits(const AValue: TSize): TSize; overload; virtual; abstract;
    function MillimetersToModelUnitsF(const AValue: Single): Single; virtual; abstract;
    function PointsToModelUnits(const AValue: Integer): Integer; virtual; abstract;
    function PointsToModelUnitsF(const AValue: Single): Single; virtual; abstract;
    function PixelsToModelUnits(const AValue: Integer; const ADpi: Single): Integer; overload; virtual; abstract;
    function EmuToModelUnits(const AValue: Integer): Integer; virtual; abstract;
    function EmuToModelUnitsL(const AValue: Int64): Int64; virtual; abstract;
    function EmuToModelUnitsF(const AValue: Integer): Single; virtual; abstract;
    function ModelUnitsToEmu(const AValue: Integer): Integer; virtual; abstract;
    function ModelUnitsToEmuL(const AValue: Int64): Int64; virtual; abstract;
    function ModelUnitsToEmuF(const AValue: Single): Integer; virtual; abstract;
    function PixelsToModelUnits(const AValue: TSize; const ADpiX, ADpiY: Single): TSize; overload; virtual; abstract;
    function HundredthsOfInchToModelUnits(const AValue: Integer): Integer; overload; virtual; abstract;
    function HundredthsOfInchToModelUnits(const AValue: TSize): TSize; overload; virtual; abstract;
    function HundredthsOfMillimeterToModelUnits(const AValue: Integer): Integer; overload; virtual; abstract;
    function HundredthsOfMillimeterToModelUnits(const AValue: TSize): TSize; overload; virtual; abstract;
    function HundredthsOfMillimeterToModelUnitsRound(const AValue: Integer): Integer; virtual; abstract;
    function CentimetersToModelUnitsF(const AValue: Single): Single; virtual; abstract;
    function InchesToModelUnitsF(const AValue: Single): Single; virtual; abstract;
    function PicasToModelUnitsF(const AValue: Single): Single; virtual; abstract;
    function DocumentsToModelUnits(const AValue: Integer): Integer; overload; virtual; abstract;
    function DocumentsToModelUnits(const AValue: TSize): TSize; overload; virtual; abstract;
    function DocumentsToModelUnitsF(const AValue: Single): Single; virtual; abstract;
    function AdjAngleToModelUnits(const AValue: Integer): Integer;
    function ModelUnitsToAdjAngle(const AValue: Integer): Integer;
    function DegreeToModelUnits(const AValue: Single): Integer;
    function FDToModelUnits(const AValue: Integer): Integer; virtual; abstract;
    function ModelUnitsToTwips(const AValue: Integer): Integer; overload; virtual; abstract;
    function ModelUnitsToTwipsF(const AValue: Single): Single; virtual; abstract;
    function ModelUnitsToTwips(const AValue: TSize): TSize; overload; virtual; abstract;
    function ModelUnitsToHundredthsOfMillimeter(const AValue: TSize): TSize; virtual; abstract;
    function ModelUnitsToPointsF(const AValue: Single): Single; virtual; abstract;
    function ModelUnitsToPointsFRound(const AValue: Single): Single; virtual; abstract;
    function ModelUnitsToPixels(const AValue: Integer; const ADpi: Single): Integer; virtual; abstract;
    function ModelUnitsToPixelsF(const AValue: Single; const ADpi: Single): Single; virtual; abstract;
    function ModelUnitsToCentimetersF(const AValue: Single): Single; virtual; abstract;
    function ModelUnitsToInchesF(const AValue: Single): Single; virtual; abstract;
    function ModelUnitsToMillimetersF(const AValue: Single): Single; virtual; abstract;
    function ModelUnitsToDocumentsF(const AValue: Single): Single; virtual; abstract;
    function ModelUnitsToHundredthsOfInch(const AValue: Integer): Integer; overload; virtual; abstract;
    function ModelUnitsToHundredthsOfInch(const AValue: TSize): TSize; overload; virtual; abstract;
    function ModelUnitsToDegree(const AValue: Integer): Integer;
    function ModelUnitsToDegreeF(const AValue: Integer): Single;
    function ModelUnitsToRadians(const AValue: Integer): Single;
    function ModelUnitsToFD(const AValue: Integer): Integer; virtual; abstract;
  end;

implementation

{ TdxDocumentModelUnitConverter }

function TdxDocumentModelUnitConverter.AdjAngleToModelUnits(const AValue: Integer): Integer;
begin
  Result := AValue;
end;

function TdxDocumentModelUnitConverter.ModelUnitsToAdjAngle(const AValue: Integer): Integer;
begin
  Result := AValue;
end;

function TdxDocumentModelUnitConverter.DegreeToModelUnits(const AValue: Single): Integer;
begin
  Result := Round(AValue * 60000);
end;

function TdxDocumentModelUnitConverter.ModelUnitsToDegree(const AValue: Integer): Integer;
begin
  Result := Trunc(AValue / 60000);
end;

function TdxDocumentModelUnitConverter.ModelUnitsToDegreeF(const AValue: Integer): Single;
begin
  Result := AValue / 60000;
end;

function TdxDocumentModelUnitConverter.ModelUnitsToRadians(const AValue: Integer): Single;
begin
  Result := PI * (AValue / 60000) / 180;
end;

end.
