{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Control.HotZones;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Types, Generics.Collections, Controls,
  dxRichEdit.DocumentModel.PieceTable, dxRichEdit.Control.HitTest;

type

  IdxHotZoneVisitor = interface
  ['{EFFD0EB4-AA22-43E5-BC88-1BBCD2AAAF62}']
  end;

  IdxHotZone = interface
  ['{20843961-4C3C-41AF-A6E0-746DAF8EB2FE}']
    function HitTest(const P: TPoint; ADpi, AZoomFactor: Double): Boolean;
    procedure Accept(AVisitor: IdxHotZoneVisitor);
  end;

  TdxHotZone = class abstract(TInterfacedObject, IdxHotZone)
  const
    HotZoneSize = 10;
  private
    FBounds: TRect;
    FExtendedBounds: TRect;
    FHitTestTransform: TdxTransformMatrix;
    function GetUseExtendedBounds: Boolean;
  protected
    procedure AcceptCore(AVisitor: IdxHotZoneVisitor); virtual; abstract;
  public
    function CalculateActualBounds(ADpiX, AZoomFactor: Double): TRect;

    function BeforeActivate(AController: TdxRichEditMouseCustomController; AResult: TdxRichEditHitTestResult): Boolean; virtual; abstract;
    procedure Activate(AController: TdxRichEditMouseCustomController; AResult: TdxRichEditHitTestResult); virtual; abstract;

    function Cursor: TCursor; virtual; abstract;

    //IdxHotZone
    function HitTest(const P: TPoint; ADpi, AZoomFactor: Double): Boolean;
    procedure Accept(AVisitor: IdxHotZoneVisitor);

    property Bounds: TRect read FBounds write FBounds;
    property HitTestTransform: TdxTransformMatrix read FHitTestTransform write FHitTestTransform;
    property ExtendedBounds: TRect read FExtendedBounds write FExtendedBounds;
    property UseExtendedBounds: Boolean read GetUseExtendedBounds;

  end;

  TdxHotZoneCollection = TObjectList<TdxHotZone>;

implementation

uses
  dxRichEdit.Utils.Units, dxTypeHelpers;

{ TdxHotZone }

function TdxHotZone.CalculateActualBounds(ADpiX, AZoomFactor: Double): TRect;
var
  AMinHotZoneSize: Integer;
begin
  AMinHotZoneSize := PixelsToDocuments(HotZoneSize, ADpiX * AZoomFactor);
  if UseExtendedBounds then
    Result := ExtendedBounds
  else
    Result := Bounds;
  if Result.Width < AMinHotZoneSize then
  begin
    Result.Left := Result.Left + (Result.Width - AMinHotZoneSize) div 2;
    Result.Width := AMinHotZoneSize;
  end;
  if Result.Height < AMinHotZoneSize then
  begin
    Result.Top := Result.Top + (Result.Height - AMinHotZoneSize) div 2;
    Result.Height := AMinHotZoneSize;
  end;
end;

function TdxHotZone.HitTest(const P: TPoint; ADpi, AZoomFactor: Double): Boolean;
var
  R: TRect;
  APoint: TPoint;
begin
  R := CalculateActualBounds(ADpi, AZoomFactor);
  APoint := P;
  if FHitTestTransform <> nil then
    APoint := FHitTestTransform.TransformPoint(APoint);
  Result := R.Contains(APoint);
end;

procedure TdxHotZone.Accept(AVisitor: IdxHotZoneVisitor);
begin
  AcceptCore(AVisitor);
end;

function TdxHotZone.GetUseExtendedBounds: Boolean;
begin
  Result := False;
  Assert(False, 'not implemented');
end;

end.
