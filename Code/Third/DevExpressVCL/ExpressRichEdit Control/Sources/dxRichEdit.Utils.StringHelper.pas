{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Utils.StringHelper;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Generics.Collections, Generics.Defaults;

type

  { TdxStringHelper }

  TdxStringHelper = class abstract
  private const
    FLastLowSpecial = #$001f; 
    FFirstHighSpecial = #$ffff; 
    FMaxDocFontNameLength = 42; 
  private
    class function ContainsSpecialSymbols(const AText: string): Boolean;
  public
    class function RemoveSpecialSymbols(const AText: string): string;
  end;

implementation

uses
  dxRichEdit.Utils.Characters;


{ TdxStringHelper }

class function TdxStringHelper.ContainsSpecialSymbols(const AText: string): Boolean;
var
  I: Integer;
  ACh: Char;
begin
  for I := 1 to Length(AText) do
  begin
    ACh := AText[I];
    if (ACh <= FLastLowSpecial) or (ACh >= FFirstHighSpecial) then
      if (ACh <> TdxCharacters.TabMark) and (ACh <> #$000A) and (ACh <> TdxCharacters.ParagraphMark) then 
        Exit(True);
  end;
  Result := False;
end;

class function TdxStringHelper.RemoveSpecialSymbols(const AText: string): string;
var
  I: Integer;
  ACh: Char;
begin
  if not ContainsSpecialSymbols(AText) then
    Exit(AText);

  Result := '';
  for I := 1 to Length(AText) do
  begin
    ACh := AText[I];
    if (ACh > FLastLowSpecial) and (ACh < FFirstHighSpecial) then
      Result := Result + ACh 
    else
      if (ACh = TdxCharacters.TabMark) or (ACh = #$000A) or (ACh = TdxCharacters.ParagraphMark) then 
        Result := Result + ACh 
  end;
end;

end.
