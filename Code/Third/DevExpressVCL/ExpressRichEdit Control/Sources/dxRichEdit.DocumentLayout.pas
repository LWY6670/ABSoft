{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.DocumentLayout;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Windows, Types, Classes, SysUtils, Graphics, Generics.Collections, dxCore, dxCoreClasses, cxGeometry,
  dxRichEdit.DocumentModel.PieceTable,
  dxRichEdit.Utils.OfficeImage,
  dxRichEdit.DocumentModel.Borders,
  dxRichEdit.DocumentModel.CharacterFormatting,
  dxRichEdit.DocumentModel.TabFormatting,
  dxRichEdit.DocumentModel.PatternLine,
  dxRichEdit.LayoutEngine.Formatter,
  dxRichEdit.LayoutEngine.BoxMeasurer,
  dxRichEdit.Platform.Win.Painter,
  dxRichEdit.Platform.PatternLinePainter,
  dxRichEdit.Options;

type

  TdxDocumentLayoutExporter = class(TcxIUnknownObject, IDocumentLayoutExporter) 
  private
    FColumnClipBounds: TdxRectF; 
    FPieceTable: TdxPieceTable;
    FMinReadableTextHeight: Integer;
    FCurrentBackColor: TColor;
    FCurrentRow: TdxRow;
    FCurrentCell: TdxTableCellViewInfo;
    FDocumentModel: TdxDocumentModel;
    FShowWhitespace: Boolean;
  protected
    function CalcRowContentBounds(ARow: TdxRow): TRect; virtual; 
    procedure SetCurrentCell(ACell: TdxTableCellViewInfo); virtual; 
    function GetBoxText(ABox: TdxBox): string; 
    function GetLastExportBoxInRowIndex(ARow: TdxRow): Integer; virtual;
    function GetShowWhitespace: Boolean; virtual;
    procedure SetShowWhitespace(const Value: Boolean); virtual;

    function GetPainter: TdxPainter; virtual; abstract;
    function ApplyClipBounds(const AClipBounds: TdxRectangleF): TdxRectangleF; 
    procedure RestoreClipBounds(const AClipBounds: TdxRectangleF); 
    function GetClipBounds: TdxRectangleF; virtual; 

    procedure SetClipBounds(const AClipBounds: TdxRectangleF); virtual; 
    function IntersectClipBounds(const AOldClipBounds, ABounds: TdxRectangleF): TdxRectangleF; 
    procedure FinishExport; virtual; 
    function IsValidBounds(const ABoxBounds: TRect): Boolean; virtual; 
    function GetDrawingBounds(const ABounds: TRect): TRect; 

    procedure ExportRows(AColumn: TdxColumn); virtual; 
    procedure ExportParagraphFrames(AColumn: TdxColumn); 
    procedure ExportParagraphFrame(AParagraphFrameBox: TdxParagraphFrameBox); 
    procedure SetCurrentRow(ARow: TdxRow); 
    procedure ExportRowCore; virtual;
    procedure ExportRowContentBoxes; virtual; 
    procedure ExportRowUnderlineBoxes; virtual; 
    procedure ExportRowStrikeoutBoxes; virtual; 
    procedure ExportRowErrorBoxes; overload; virtual; 
    procedure ExportRowErrorBoxes(ARow: TdxRow); overload; virtual; 
  public
    constructor Create(ADocumentModel: TdxDocumentModel);

    procedure ExportPageAreaCore(APage: TdxPage; APieceTable: TdxPieceTable; AAction: TdxExportPageAction); 
    procedure ExportPageHeader(APage: TdxPage); 
    procedure ExportPageMainAreas(APage: TdxPage); 
    procedure ExportPageFooter(APage: TdxPage); 
    procedure ExportFloatingObjects(AFloatingObjects: TList<TdxFloatingObjectBox>; APieceTable: TdxPieceTable); 
    procedure ExportPage(APage: TdxPage); 
    procedure ExportPageArea(APageArea: TdxPageArea); 
    procedure ExportColumn(AColumn: TdxColumn); 
    procedure ExportRow(ARow: TdxRow); virtual; 
    procedure ApplyCurrentRowTableCellClipping(ARow: TdxRow); 
    procedure ExportTextBox(ABox: TdxTextBox); virtual; 
    procedure ExportSpecialTextBox(ABox: TdxSpecialTextBox); 
    procedure ExportLayoutDependentTextBox(ABox: TdxLayoutDependentTextBox); virtual; 
    procedure ExportHyphenBox(ABox: TdxHyphenBox); virtual; 
    procedure ExportInlinePictureBox(ABox: TdxInlinePictureBox); virtual; 
    procedure ExportCustomRunBox(ABox: TdxCustomRunBox); virtual;
    procedure ExportFloatingObjectBox(ABox: TdxFloatingObjectBox); virtual; 
    procedure ExportFloatingObjectShape(TdxBox: TdxFloatingObjectBox; AShape: TdxShape); virtual; 
    procedure ExportFloatingObjectPicture(ABox: TdxFloatingObjectBox; APictureContent: TdxPictureFloatingObjectContent); virtual; 
    procedure ExportFloatingObjectTextBox(ABox: TdxFloatingObjectBox; ATextBoxContent: TdxTextBoxFloatingObjectContent; ATextBoxDocumentLayout: TdxDocumentLayout); virtual; 
    procedure ExportSpaceBox(ABox: ISpaceBox); virtual; 
    procedure ExportTabSpaceBox(ABox: TdxTabSpaceBox); virtual; 
    procedure ExportLineBreakBox(ABox: TdxLineBreakBox); virtual; 
    procedure ExportParagraphMarkBox(ABox: TdxParagraphMarkBox); virtual; 
    procedure ExportSectionMarkBox(ABox: TdxSectionMarkBox); virtual; 
    procedure ExportUnderlineBox(ARow: TdxRow; AUnderlineBox: TdxUnderlineBox); virtual; 
    procedure ExportStrikeoutBox(ARow: TdxRow; AStrikeoutBox: TdxUnderlineBox); virtual; 
    procedure ExportErrorBox(AErrorBox: TdxErrorBox); virtual; 
    procedure ExportBookmarkStartBox(ABox: TdxVisitableDocumentIntervalBox); virtual; 
    procedure ExportBookmarkEndBox(ABox: TdxVisitableDocumentIntervalBox); virtual; 
    procedure ExportCustomMarkBox(ABox: TdxCustomMarkBox); virtual; 
    procedure ExportSeparatorBox(ABox: TdxSeparatorBox); virtual; 
    procedure ExportPageBreakBox(ABox: TdxPageBreakBox); virtual; 
    procedure ExportColumnBreakBox(ABox: TdxColumnBreakBox); virtual; 
    procedure ExportNumberingListBox(ABox: TdxNumberingListBox); virtual; 
    procedure ExportLineNumberBox(ABox: TdxLineNumberBox); virtual; 
    procedure ExportTableBorder(ABorder: TdxTableBorderViewInfoBase; ACellBounds: TRect); virtual; 
    procedure ExportTableBorderCorner(ACorner: TdxCornerViewInfoBase; X, Y: Integer); virtual; 
    procedure ExportTableCell(ACell: TdxTableCellViewInfo); virtual; 
    procedure ExportTableRow(ARow: TdxTableRowViewInfoBase); virtual; 
    procedure ExportParagraphFrameBox(ABox: TdxParagraphFrameBox); virtual; 
    procedure ExportDataContainerRunBox(ABox: TdxDataContainerRunBox); virtual; 

    function IsAnchorVisible(AAnchor: IdxTableCellVerticalAnchor): Boolean; virtual; 
    function IsTableRowVisible(ARow: TdxTableRowViewInfoBase): Boolean; virtual; 

    property DocumentModel: TdxDocumentModel read FDocumentModel;
    property PieceTable: TdxPieceTable read FPieceTable write FPieceTable;
    property ShowWhitespace: Boolean read GetShowWhitespace write SetShowWhitespace; 
    property MinReadableTextHeight: Integer read FMinReadableTextHeight write FMinReadableTextHeight;
    property CurrentBackColor: TColor read FCurrentBackColor write FCurrentBackColor;

    property CurrentRow: TdxRow read FCurrentRow;
    property CurrentCell: TdxTableCellViewInfo read FCurrentCell;
    property Painter: TdxPainter read GetPainter;
  end;

  TdxBoundedDocumentLayoutExporter = class(TdxDocumentLayoutExporter, IdxUnderlinePainter, IdxStrikeoutPainter) 
  private
    FBounds: TRect;
    FVisibleBounds: TRect;
    FHorizontalLinePainter: TdxRichEditPatternLinePainter;
    FVerticalLinePainter: TdxRichEditPatternLinePainter;
    function GetOffset: TPoint;
  protected
    function CreateHorizontalLinePainter(ALinePaintingSupport: IdxPatternLinePaintingSupport): TdxRichEditPatternLinePainter; virtual; abstract; 
    function CreateVerticalLinePainter(ALinePaintingSupport: IdxPatternLinePaintingSupport ): TdxRichEditPatternLinePainter; virtual; abstract; 

    function IsValidBounds(const ABoxBounds: TRect): Boolean; override; 
    function GetDrawingBounds(const ABounds: TRect): TRect; 
    function GetTabLeaderCharacter(ALeaderType: TdxTabLeaderType): Char; 
    function GetTabLeaderCharacterWidth(ABox: TdxTabSpaceBox): Integer; 
    function ShouldCenterTabLeaderLineVertically(ABox: TdxTabSpaceBox): Boolean; 
    function GetTabLeaderUnderlineType(ABox: TdxTabSpaceBox): TdxUnderlineType; 
    procedure ExportRows(AColumn: TdxColumn); override; 
    procedure ExportRowBackground(ARow: TdxRow); 
    function ShouldExportRowBackground(ARow: TdxRow): Boolean; virtual; 
    function ShouldExportRow(ARow: TdxRow): Boolean; virtual; 
    function GetTabLeaderText(ABox: TdxTabSpaceBox; ATextBounds: TRect): string; 
    procedure ExportBackground; virtual; 
    procedure ExportTextHighlighting; 
    procedure ExportHighlighting(AHighlightAreas: TdxHighlightAreaCollection); 
    procedure ExportHighlightArea(AArea: TdxHighlightArea); virtual; abstract; 
    procedure DrawDoubleSolidLine(ABounds: TdxRectangleF; AColor: TColor); 
  public
    constructor Create(ADocumentModel: TdxDocumentModel; ABounds: TRect; ALinePaintingSupport: IdxPatternLinePaintingSupport); virtual; 
    destructor Destroy; override;

    procedure DrawUnderline(AUnderline: TdxUnderlineSingle; ABounds: TdxRectangleF; AColor: TColor); overload; 
    procedure DrawUnderline(AUnderline: TdxUnderlineDotted; ABounds: TdxRectangleF; AColor: TColor); overload; 
    procedure DrawUnderline(AUnderline: TdxUnderlineDashed; ABounds: TdxRectangleF; AColor: TColor); overload; 
    procedure DrawUnderline(AUnderline: TdxUnderlineDashSmallGap; ABounds: TdxRectangleF; AColor: TColor); overload; 
    procedure DrawUnderline(AUnderline: TdxUnderlineDashDotted; ABounds: TdxRectangleF; AColor: TColor); overload; 
    procedure DrawUnderline(AUnderline: TdxUnderlineDashDotDotted; ABounds: TdxRectangleF; AColor: TColor); overload; 
    procedure DrawUnderline(AUnderline: TdxUnderlineDouble; ABounds: TdxRectangleF; AColor: TColor); overload; 
    procedure DrawUnderline(AUnderline: TdxUnderlineHeavyWave; ABounds: TdxRectangleF; AColor: TColor); overload; 
    procedure DrawUnderline(AUnderline: TdxUnderlineLongDashed; ABounds: TdxRectangleF; AColor: TColor); overload; 
    procedure DrawUnderline(AUnderline: TdxUnderlineThickSingle; ABounds: TdxRectangleF; AColor: TColor); overload; 
    procedure DrawUnderline(AUnderline: TdxUnderlineThickDotted; ABounds: TdxRectangleF; AColor: TColor); overload; 
    procedure DrawUnderline(AUnderline: TdxUnderlineThickDashed; ABounds: TdxRectangleF; AColor: TColor); overload; 
    procedure DrawUnderline(AUnderline: TdxUnderlineThickDashDotted; ABounds: TdxRectangleF; AColor: TColor); overload; 
    procedure DrawUnderline(AUnderline: TdxUnderlineThickDashDotDotted; ABounds: TdxRectangleF; AColor: TColor); overload; 
    procedure DrawUnderline(AUnderline: TdxUnderlineThickLongDashed; ABounds: TdxRectangleF; AColor: TColor); overload; 
    procedure DrawUnderline(AUnderline: TdxUnderlineDoubleWave; ABounds: TdxRectangleF; AColor: TColor); overload; 
    procedure DrawUnderline(AUnderline: TdxUnderlineWave; ABounds: TdxRectangleF; AColor: TColor); overload; 
    procedure DrawStrikeout(AStrikeout: TdxStrikeoutSingle; ABounds: TdxRectangleF; AColor: TColor); overload; 
    procedure DrawStrikeout(AStrikeout: TdxStrikeoutDouble; ABounds: TdxRectangleF; AColor: TColor); overload; 

    function GetTableBorderLine(ABorderLineStyle: TdxBorderLineStyle): TdxUnderline; 
    function GetShapeOutlinePenWidth(ARun: TdxFloatingObjectAnchorRun; ABox: TdxFloatingObjectBox): Integer; 

    property Bounds: TRect read FBounds write FBounds;
    property VisibleBounds: TRect read FVisibleBounds write FVisibleBounds;
		property HorizontalLinePainter: TdxRichEditPatternLinePainter read FHorizontalLinePainter;
		property VerticalLinePainter: TdxRichEditPatternLinePainter read FVerticalLinePainter;
    property Offset: TPoint read GetOffset;
  end;

  TdxGraphicsDocumentLayoutExporter = class;

  { TdxGraphicsDocumentLayoutExporterWhitespaceStrategy }

  TdxGraphicsDocumentLayoutExporterWhitespaceStrategy = class 
  private
    FExporter: TdxGraphicsDocumentLayoutExporter; 
  public
    constructor Create(AExporter: TdxGraphicsDocumentLayoutExporter); 

    procedure ExportSpaceBox(ABox: ISpaceBox); virtual; abstract;  
    procedure ExportParagraphMarkBox(ABox: TdxParagraphMarkBox); virtual; abstract; 
    procedure ExportSectionMarkBox(ABox: TdxSectionMarkBox); virtual; abstract; 
    procedure ExportColumnBreakBox(ABox: TdxColumnBreakBox); virtual; abstract; 
    procedure ExportPageBreakBox(ABox: TdxPageBreakBox); virtual; abstract; 
    procedure ExportTabSpaceBox(ABox: TdxTabSpaceBox); virtual; abstract; 
    procedure ExportLineBreakBox(ABox: TdxLineBreakBox); virtual; abstract; 
    procedure ExportSeparatorBox(ABox: TdxSeparatorBox); virtual; abstract; 
    procedure ExportNumberingListBoxSeparator(ANumberingListBoxWithSeparator: TdxNumberingListBoxWithSeparator); virtual; abstract; 

    property Exporter: TdxGraphicsDocumentLayoutExporter read FExporter; 
  end;

  { TdxGraphicsDocumentLayoutExporterShowWhitespaceStrategy }

  TdxGraphicsDocumentLayoutExporterShowWhitespaceStrategy = class(TdxGraphicsDocumentLayoutExporterWhitespaceStrategy) 
  private
  public

    procedure ExportSpaceBox(ASpaceBox: ISpaceBox); override; 
    procedure ExportParagraphMarkBox(ABox: TdxParagraphMarkBox); override; 
    procedure ExportSectionMarkBox(ABox: TdxSectionMarkBox); override; 
    procedure ExportColumnBreakBox(ABox: TdxColumnBreakBox); override; 
    procedure ExportPageBreakBox(ABox: TdxPageBreakBox); override; 
    procedure ExportTabSpaceBox(ABox: TdxTabSpaceBox); override; 
    procedure ExportLineBreakBox(ABox: TdxLineBreakBox); override; 
    procedure ExportSeparatorBox(ABox: TdxSeparatorBox); override; 
    procedure ExportNumberingListBoxSeparator(ANumberingListBoxWithSeparator: TdxNumberingListBoxWithSeparator); override; 
  end;

  { TdxGraphicsDocumentLayoutExporterHideWhitespaceStrategy }

  TdxGraphicsDocumentLayoutExporterHideWhitespaceStrategy = class(TdxGraphicsDocumentLayoutExporterShowWhitespaceStrategy) 
  private
    function GetFormattingMarkVisibility: TdxFormattingMarkVisibilityOptions;
  public
    procedure ExportSpaceBox(ABox: ISpaceBox); override; 
    procedure ExportParagraphMarkBox(ABox: TdxParagraphMarkBox); override; 
    procedure ExportSectionMarkBox(ABox: TdxSectionMarkBox); override; 
    procedure ExportColumnBreakBox(ABox: TdxColumnBreakBox); override; 
    procedure ExportPageBreakBox(ABox: TdxPageBreakBox); override; 
    procedure ExportTabSpaceBox(ABox: TdxTabSpaceBox); override; 
    procedure ExportSeparatorBox(ABox: TdxSeparatorBox); override; 
    procedure ExportLineBreakBox(ABox: TdxLineBreakBox); override; 

    property FormattingMarkVisibility: TdxFormattingMarkVisibilityOptions read GetFormattingMarkVisibility; 
  end;

  { TdxGraphicsDocumentLayoutExporterAdapter }

  TdxGraphicsDocumentLayoutExporterAdapter = class 
  public
    procedure ExportUnderlineLineBox(AExporter: TdxGraphicsDocumentLayoutExporter; ALinePainter: IdxUnderlinePainter; ALineBox: TdxUnderlineBox; ALine: TdxUnderline; ALineColor: TColor); virtual; abstract; 
    procedure ExportStrikeoutLineBox(AExporter: TdxGraphicsDocumentLayoutExporter; ALinePainter: IdxStrikeoutPainter; ALineBox: TdxUnderlineBox; ALine: TdxStrikeout; ALineColor: TColor); virtual; abstract;
    procedure ExportInlinePictureBox(AExporter: TdxGraphicsDocumentLayoutExporter; ABox: TdxInlinePictureBox); virtual; abstract; 
    procedure ExportTabSpaceBoxCore(AExporter: TdxGraphicsDocumentLayoutExporter; ABox: TdxTabSpaceBox); virtual; abstract; 
    procedure ExportSeparatorBoxCore(AExporter: TdxGraphicsDocumentLayoutExporter; ABox: TdxSeparatorBox); virtual; abstract; 
    procedure ExportLineBreakBoxCore(AExporter: TdxGraphicsDocumentLayoutExporter; ABox: TdxLineBreakBox); virtual; abstract; 
    procedure ExportTabLeader(AExporter: TdxGraphicsDocumentLayoutExporter; ABox: TdxTabSpaceBox); virtual; abstract; 
    procedure ExportFloatingObjectPicture(AExporter: TdxGraphicsDocumentLayoutExporter; ABox: TdxFloatingObjectBox;
      APictureContent: TdxPictureFloatingObjectContent); virtual; abstract; 
  end;

  { TdxGraphicsDocumentLayoutExporter }

  TdxGraphicsDocumentLayoutExporter = class(TdxBoundedDocumentLayoutExporter)
  private
    FPainter: TdxPainter;
    FWhitespaceStrategy: TdxGraphicsDocumentLayoutExporterWhitespaceStrategy;
    FAdapter: TdxGraphicsDocumentLayoutExporterAdapter;
    FDrawInactivePieceTableWithDifferentColor: Boolean; 
    FHidePartiallyVisibleRow: Boolean;
  protected
    function GetPainter: TdxPainter; override;
    procedure SetShowWhitespace(const Value: Boolean); override;
    function ShouldExportRowBackground(ARow: TdxRow): Boolean; override; 
    function ShouldExportRow(ARow: TdxRow): Boolean; override; 
  public
    constructor Create(ADocumentModel: TdxDocumentModel; APainter: TdxPainter;
      AAdapter: TdxGraphicsDocumentLayoutExporterAdapter; ABounds: TRect;
      AHidePartiallyVisibleRow: Boolean = False); reintroduce;
    destructor Destroy; override;
    function CreateHorizontalLinePainter(ALinePaintingSupport: IdxPatternLinePaintingSupport): TdxRichEditPatternLinePainter; override; 
    function CreateVerticalLinePainter(ALinePaintingSupport: IdxPatternLinePaintingSupport): TdxRichEditPatternLinePainter; override; 
    function GetActualColor(AColor: TColor): TColor; 
    function GetEffectivePieceTable(APieceTable: TdxPieceTable): TdxPieceTable; 
    function ShouldGrayContent: Boolean; 
    procedure ExportRow(ARow: TdxRow); override; 
    function IsTableRowVisible(ARow: TdxTableRowViewInfoBase): Boolean; override; 
    function IsRowContainsVerticalMergingCell(ARow: TdxTableRow): Boolean; 
    function IsAnchorVisible(AAnchor: IdxTableCellVerticalAnchor): Boolean; override; 
    procedure ExportBackground; override; 
    procedure ExportFieldsHighlighting; 
    procedure ExportHighlightArea(AArea: TdxHighlightArea); override; 
    procedure ExportTextBox(ABox: TdxTextBox); override; 
    procedure ExportLayoutDependentTextBox(ABox: TdxLayoutDependentTextBox); override; 
    procedure ExportNumberingListBox(ABox: TdxNumberingListBox); override; 
    procedure ExportLineNumberBox(ABox: TdxLineNumberBox); override; 
    procedure ExportSpaceBox(ABox: ISpaceBox); override; 
    procedure ExportSeparatorBox(ABox: TdxSeparatorBox); override; 
    procedure ExportInlinePictureBox(ABox: TdxInlinePictureBox); override;
    procedure ExportFloatingObjectBox(ABox: TdxFloatingObjectBox); override; 
    procedure ExportFloatingObjectShape(ABox: TdxFloatingObjectBox; AShape: TdxShape); override; 
    procedure ExportFloatingObjectPicture(ABox: TdxFloatingObjectBox; APictureContent: TdxPictureFloatingObjectContent); override; 
    procedure ExportFloatingObjectTextBox(ABox: TdxFloatingObjectBox; ATextBoxContent: TdxTextBoxFloatingObjectContent; ATextBoxDocumentLayout: TdxDocumentLayout); override; 
    procedure ExportTextBoxCore(ABox: TdxBox); overload; 
    procedure ExportTextBoxCoreNoCheckBoundsValidity(ABox: TdxBox); 
    procedure ExportNumberingListBoxCore(ABox: TdxBox); 
    procedure ExportTextBoxCore(ABox: TdxBox; ATextBounds: TRect; const AText: string); overload; 
    procedure ExportSpaceBoxCore(ABox: TdxBox; ATextBounds: TRect; const AText: string); 
    procedure ExportHyphenBox(ABox: TdxHyphenBox); override; 
    procedure ExportTabSpaceBox(ABox: TdxTabSpaceBox); override; 
    procedure ExportTabLeaderAsCharacterSequence(ABox: TdxTabSpaceBox); 
    procedure ExportTabLeaderAsUnderline(ABox: TdxTabSpaceBox); 
    procedure ExportLineBreakBox(ABox: TdxLineBreakBox); override; 
    procedure ExportParagraphMarkBox(ABox: TdxParagraphMarkBox); override; 
    procedure ExportParagraphMarkBoxCore(ABox: TdxParagraphMarkBox); 
    procedure ExportSectionMarkBox(ABox: TdxSectionMarkBox); override; 
    procedure ExportSectionMarkBoxCore(ABox: TdxSectionMarkBox); 
    procedure ExportColumnBreakBox(ABox: TdxColumnBreakBox); override; 
    procedure ExportColumnBreakBoxCore(ABox: TdxColumnBreakBox); 
    procedure ExportPageBreakBox(ABox: TdxPageBreakBox); override; 
    procedure ExportPageBreakBoxCore(ABox: TdxPageBreakBox); 
    procedure ExportMarkBoxCore(ABox: TdxBox; AExporter: TdxMarkBoxExporterBase); 
    procedure ExportTableBorder(ABorder: TdxTableBorderViewInfoBase; ACellBounds: TRect); override; 
    procedure ExportTableBorderCorner(ACorner: TdxCornerViewInfoBase; X, Y: Integer); override; 
    procedure ExportTableCell(ACell: TdxTableCellViewInfo); override; 
    procedure ExportTableRow(ARow: TdxTableRowViewInfoBase); override; 
    procedure ExportParagraphFrameBox(ABox: TdxParagraphFrameBox); override; 
    procedure ExcludeCellBounds(ACells: TdxTableCellViewInfoCollection; ARowBounds: TRect); 
    function GetActualBackgroundColor(ARow: TdxTableRowViewInfoBase): TColor; 
    function GetBorderPainter(AViewInfo: TdxTableBorderViewInfoBase): TdxTableBorderPainter; overload;
    function GetBorderPainter(ACorner: TdxCornerViewInfoBase): TdxTableCornerPainter; overload;
    procedure ExportUnderlineBox(ARow: TdxRow; AUnderlineBox: TdxUnderlineBox); override; 
    procedure ExportStrikeoutBox(ARow: TdxRow; AStrikeoutBox: TdxUnderlineBox); override; 
    procedure ExportErrorBox(AErrorBox: TdxErrorBox); override; 
    function GetClipBounds: TdxRectangleF; override; 
    procedure SetClipBounds(const AClipBounds: TdxRectangleF); override; 

    property Adapter: TdxGraphicsDocumentLayoutExporterAdapter read FAdapter;
    property HidePartiallyVisibleRow: Boolean read FHidePartiallyVisibleRow write FHidePartiallyVisibleRow;
    property DrawInactivePieceTableWithDifferentColor: Boolean read FDrawInactivePieceTableWithDifferentColor write FDrawInactivePieceTableWithDifferentColor;
  end;

  { TdxScreenOptimizedGraphicsDocumentLayoutExporter }

  TdxScreenOptimizedGraphicsDocumentLayoutExporter = class(TdxGraphicsDocumentLayoutExporter) 
  strict private class var
    FHalftoneBrush: HBRUSH;
    class constructor Initialize;
    class destructor Finalize;
    class function CreateHalftoneBrush: HBRUSH;
  private
    FPixel: Integer;
  public
    constructor Create(ADocumentModel: TdxDocumentModel; APainter: TdxPainter;
      AAdapter: TdxGraphicsDocumentLayoutExporterAdapter; ABounds: TRect);
    procedure FinishExport; override; 
    function ShouldOptimizeBox(ABox: TdxBox): Boolean; virtual; 
    procedure ExportRowStrikeoutBoxes; override; 
    procedure ExportRowUnderlineBoxes; override; 
    procedure ExportRowErrorBoxes; override; 
    procedure ExportHyphenBox(ABox: TdxHyphenBox); override; 
    procedure ExportParagraphMarkBox(ABox: TdxParagraphMarkBox); override; 
    procedure ExportSpaceBox(ABox: ISpaceBox); override; 
    procedure ExportSeparatorBox(ABox: TdxSeparatorBox); override; 
    procedure ExportTextBox(ABox: TdxTextBox); override; 
    procedure ExportLayoutDependentTextBox(ABox: TdxLayoutDependentTextBox); override; 
    procedure ExportNumberingListBox(ABox: TdxNumberingListBox); override; 
    procedure ExportLineNumberBox(ABox: TdxLineNumberBox); override; 
    procedure ExportBoxOptimized(ABox: TdxBox); virtual; 
  end;

  { TdxScreenOptimizedGraphicsDocumentLayoutExporterNoLineNumbers }

  TdxScreenOptimizedGraphicsDocumentLayoutExporterNoLineNumbers = class(TdxScreenOptimizedGraphicsDocumentLayoutExporter) 
  public
    procedure ExportLineNumberBox(ABox: TdxLineNumberBox); override; 
  end;

  { TdxWinFormsGraphicsDocumentLayoutExporterAdapter }

  TdxWinFormsGraphicsDocumentLayoutExporterAdapter = class(TdxGraphicsDocumentLayoutExporterAdapter)  
  private
    function ExportLineBoxCore<T>(AExporter: TdxGraphicsDocumentLayoutExporter; ALineBox: TdxUnderlineBox; ALine: TdxPatternLine<T>): TdxRectangleF;
    procedure ExportInlinePictureBoxCore(AExporter: TdxGraphicsDocumentLayoutExporter; ABox: TdxInlinePictureBox;
      AImg: TdxOfficeImage; AImgBounds: TRect; ASize: TSize; ASizing: TdxImageSizeMode); 
  public
    procedure ExportUnderlineLineBox(AExporter: TdxGraphicsDocumentLayoutExporter; ALinePainter: IdxUnderlinePainter;
      ALineBox: TdxUnderlineBox; ALine: TdxUnderline; ALineColor: TColor); override;
    procedure ExportStrikeoutLineBox(AExporter: TdxGraphicsDocumentLayoutExporter; ALinePainter: IdxStrikeoutPainter;
      ALineBox: TdxUnderlineBox; ALine: TdxStrikeout; ALineColor: TColor); override;
    procedure ExportInlinePictureBox(AExporter: TdxGraphicsDocumentLayoutExporter; ABox: TdxInlinePictureBox); override; 
    procedure ExportFloatingObjectPicture(AExporter: TdxGraphicsDocumentLayoutExporter; ABox: TdxFloatingObjectBox;
      APictureContent: TdxPictureFloatingObjectContent); override; 
    procedure ExportTabSpaceBoxCore(AExporter: TdxGraphicsDocumentLayoutExporter; ABox: TdxTabSpaceBox); override; 
    procedure ExportSeparatorBoxCore(AExporter: TdxGraphicsDocumentLayoutExporter; ABox: TdxSeparatorBox); override; 
    procedure ExportSingleCharacterMarkBoxCore(AExporter: TdxGraphicsDocumentLayoutExporter; ABox: TdxSingleCharacterMarkBox); 
    procedure ExportLineBreakBoxCore(AExporter: TdxGraphicsDocumentLayoutExporter; ABox: TdxLineBreakBox); override; 
    procedure DrawLineBreakArrow(AExporter: TdxGraphicsDocumentLayoutExporter; const AGlyphBounds: TRect; AForeColor: TColor); virtual;
    procedure ExportTabLeader(AExporter: TdxGraphicsDocumentLayoutExporter; ABox: TdxTabSpaceBox); override; 
  end;

  { TdxCharacterBoxLevelDocumentLayoutExporter }

  TdxCharacterBoxLevelDocumentLayoutExporter = class(TdxDocumentLayoutExporter)
  private
    FChars: TdxCharacterBoxCollection;
    FMeasurer: TdxBoxMeasurer;
    FRowBounds: TRect;
    procedure AppendCharacterBox(AStartPos: TdxFormatterPosition;
      AOffset: Integer; const ABounds, ATightBounds: TRect);
    function CalculateCharacterBounds(ABox: TdxTextBox): TRects;
    function GetActualCharacterBounds(const ABounds: TRect): TRect;
    procedure ExportMultiCharacterBox(ABox: TdxBox; ACharacterBounds: TRects);
  protected
    function GetPainter: TdxPainter; override;
    procedure ExportRowCore; override;
    procedure ExportSingleCharacterBox(ABox: TdxBox);
  public
    constructor Create(ADocumentModel: TdxDocumentModel;
      AChars: TdxCharacterBoxCollection; AMeasurer: TdxBoxMeasurer);
    procedure ExportTextBox(ABox: TdxTextBox); override;
    procedure ExportLayoutDependentTextBox(ABox: TdxLayoutDependentTextBox);
      override;
    procedure ExportSpaceBox(ABox: ISpaceBox); override;
    procedure ExportHyphenBox(ABox: TdxHyphenBox); override;
    procedure ExportInlinePictureBox(ABox: TdxInlinePictureBox); override;
    procedure ExportTabSpaceBox(ABox: TdxTabSpaceBox); override;
    procedure ExportSeparatorBox(ABox: TdxSeparatorBox); override;
    procedure ExportDataContainerRunBox(ABox: TdxDataContainerRunBox); override;
    procedure ExportParagraphMarkBox(ABox: TdxParagraphMarkBox); override;
    procedure ExportRowBox(ARow: TdxRow; ABox: TdxBox); virtual;
    procedure ExportSectionMarkBox(ABox: TdxSectionMarkBox); override;
    procedure ExportLineBreakBox(ABox: TdxLineBreakBox); override;
    procedure ExportPageBreakBox(ABox: TdxPageBreakBox); override;
    procedure ExportColumnBreakBox(ABox: TdxColumnBreakBox); override;
    procedure ExportNumberingListBox(ABox: TdxNumberingListBox); override;
    procedure ExportCustomRunBox(ABox: TdxCustomRunBox); override;

    property Chars: TdxCharacterBoxCollection read FChars;
    property Measurer: TdxBoxMeasurer read FMeasurer;
  end;

implementation

uses
{$IFDEF DELPHI16}
  System.UITypes,
{$ENDIF}
  Math, StrUtils, dxCoreGraphics, dxTypeHelpers,
  dxRichEdit.Utils.Characters,
  dxRichEdit.Utils.Colors,
  dxRichEdit.DocumentModel.Core,
  dxRichEdit.DocumentModel.ParagraphRange,
  dxRichEdit.DocumentModel.TableFormatting,
  dxRichEdit.Platform.Font;

function RectSplitHorizontally(const R: TRect; ACount: Integer): TRects;
var
  AWidth: Integer;
  I: Integer;
  ALeft: Integer;
begin
  SetLength(Result, ACount);
  ALeft := R.Left;
  for I := 0 to ACount - 2 do
  begin
    AWidth := (R.Right - ALeft) div (ACount - I);
    Result[I] := Rect(ALeft, R.Top, ALeft + AWidth, R.Bottom);
    ALeft := ALeft + AWidth;
  end;
  Result[ACount - 1] := Rect(ALeft, R.Top, R.Right, R.Bottom);
end;

{ TdxDocumentLayoutExporter }

constructor TdxDocumentLayoutExporter.Create(ADocumentModel: TdxDocumentModel);
begin
  inherited Create;
  Assert(ADocumentModel <> nil);
  FDocumentModel := ADocumentModel;
  FCurrentBackColor := TdxColor.Empty;
  FPieceTable := ADocumentModel.MainPieceTable;
end;

function TdxDocumentLayoutExporter.ApplyClipBounds(const AClipBounds: TdxRectangleF): TdxRectangleF;
begin
  NotImplemented;
end;

procedure TdxDocumentLayoutExporter.ApplyCurrentRowTableCellClipping(ARow: TdxRow);
var
  ACell: TdxTableCellViewInfo;
  AClipBounds: TRect;
begin
  if ARow is TdxTableCellRow then
  begin
    ACell := TdxTableCellRow(ARow).CellViewInfo;
    if not (ACell = FCurrentCell) then
    begin
      AClipBounds := GetDrawingBounds(ACell.TableViewInfo.GetCellBounds(ACell));
      RestoreClipBounds(FColumnClipBounds);
      ApplyClipBounds(dxRectF(AClipBounds));
      FCurrentCell := ACell;
    end;
  end
  else
    if FCurrentCell <> nil then
    begin
      SetClipBounds(FColumnClipBounds);
      FCurrentCell := nil;
    end;
end;

function TdxDocumentLayoutExporter.CalcRowContentBounds(ARow: TdxRow): TRect;
begin
  Result := ARow.Bounds;
end;

procedure TdxDocumentLayoutExporter.ExportColumn(AColumn: TdxColumn);
var
  AColumnClipBounds: TdxRectangleF;
  AColumnTables: TdxTableViewInfoCollection;
begin
  AColumnClipBounds := GetClipBounds;
  AColumnTables := AColumn.InnerTables;
  if AColumnTables <> nil then
    AColumnTables.ExportBackground(Self);
  ExportParagraphFrames(AColumn);
  ExportRows(AColumn);
  if AColumnTables <> nil then
  begin
    SetClipBounds(AColumnClipBounds);
    AColumnTables.ExportTo(Self);
  end;
  SetClipBounds(AColumnClipBounds);
end;

procedure TdxDocumentLayoutExporter.ExportCustomRunBox(ABox: TdxCustomRunBox);
begin
end;

procedure TdxDocumentLayoutExporter.ExportFloatingObjectBox(ABox: TdxFloatingObjectBox);
var
  ARun: TdxFloatingObjectAnchorRun;
  APreviousPieceTable: TdxPieceTable;
  APictureContent: TdxPictureFloatingObjectContent;
begin
  ARun := ABox.GetFloatingObjectRun;
  APreviousPieceTable := PieceTable;
  PieceTable := ARun.PieceTable;
  try
    ExportFloatingObjectShape(ABox, ARun.Shape);
    if ARun.Content is TdxPictureFloatingObjectContent then
    begin
      APictureContent := TdxPictureFloatingObjectContent(ARun.Content);
      ExportFloatingObjectPicture(ABox, APictureContent);
      Exit;
    end;
  finally
    PieceTable := APreviousPieceTable;
  end;
  if (ABox.DocumentLayout <> nil) and (ARun.Content is TdxTextBoxFloatingObjectContent) then
    ExportFloatingObjectTextBox(ABox, TdxTextBoxFloatingObjectContent(ARun.Content), ABox.DocumentLayout);
end;

procedure TdxDocumentLayoutExporter.ExportHyphenBox(ABox: TdxHyphenBox);
begin
end;

procedure TdxDocumentLayoutExporter.ExportInlinePictureBox(ABox: TdxInlinePictureBox);
begin
end;

procedure TdxDocumentLayoutExporter.ExportLayoutDependentTextBox(ABox: TdxLayoutDependentTextBox);
begin
end;

procedure TdxDocumentLayoutExporter.ExportFloatingObjectShape(TdxBox: TdxFloatingObjectBox; AShape: TdxShape);
begin
end;

procedure TdxDocumentLayoutExporter.ExportFloatingObjectPicture(ABox: TdxFloatingObjectBox; APictureContent: TdxPictureFloatingObjectContent);
begin
end;

procedure TdxDocumentLayoutExporter.ExportFloatingObjects(AFloatingObjects: TList<TdxFloatingObjectBox>;
  APieceTable: TdxPieceTable);
var
  I: Integer;
  AFloatingObject: TdxFloatingObjectBox;
begin
  if AFloatingObjects = nil then
    Exit;
  for I := 0 to AFloatingObjects.Count - 1 do
  begin
    AFloatingObject := AFloatingObjects[I];
    if AFloatingObject.PieceTable = APieceTable then
      ExportFloatingObjectBox(AFloatingObject);
  end;
end;

procedure TdxDocumentLayoutExporter.ExportFloatingObjectTextBox(ABox: TdxFloatingObjectBox; ATextBoxContent: TdxTextBoxFloatingObjectContent; ATextBoxDocumentLayout: TdxDocumentLayout);
begin
end;

procedure TdxDocumentLayoutExporter.ExportSpaceBox(ABox: ISpaceBox);
begin
end;

procedure TdxDocumentLayoutExporter.ExportTabSpaceBox(ABox: TdxTabSpaceBox);
begin
end;

procedure TdxDocumentLayoutExporter.ExportLineBreakBox(ABox: TdxLineBreakBox);
begin
end;

procedure TdxDocumentLayoutExporter.ExportParagraphMarkBox(ABox: TdxParagraphMarkBox);
begin
end;

procedure TdxDocumentLayoutExporter.ExportSectionMarkBox(ABox: TdxSectionMarkBox);
begin
end;

procedure TdxDocumentLayoutExporter.ExportUnderlineBox(ARow: TdxRow; AUnderlineBox: TdxUnderlineBox);
begin
end;

procedure TdxDocumentLayoutExporter.ExportStrikeoutBox(ARow: TdxRow; AStrikeoutBox: TdxUnderlineBox);
begin
end;

procedure TdxDocumentLayoutExporter.ExportErrorBox(AErrorBox: TdxErrorBox);
begin
end;

procedure TdxDocumentLayoutExporter.ExportBookmarkStartBox(ABox: TdxVisitableDocumentIntervalBox);
begin
end;

procedure TdxDocumentLayoutExporter.ExportBookmarkEndBox(ABox: TdxVisitableDocumentIntervalBox);
begin
end;

procedure TdxDocumentLayoutExporter.ExportCustomMarkBox(ABox: TdxCustomMarkBox);
begin
end;

procedure TdxDocumentLayoutExporter.ExportSeparatorBox(ABox: TdxSeparatorBox);
begin
end;

procedure TdxDocumentLayoutExporter.ExportPageBreakBox(ABox: TdxPageBreakBox);
begin
end;

procedure TdxDocumentLayoutExporter.ExportColumnBreakBox(ABox: TdxColumnBreakBox);
begin
end;

procedure TdxDocumentLayoutExporter.ExportNumberingListBox(ABox: TdxNumberingListBox);
begin
end;

procedure TdxDocumentLayoutExporter.ExportLineNumberBox(ABox: TdxLineNumberBox);
begin
end;

procedure TdxDocumentLayoutExporter.ExportTableBorder(ABorder: TdxTableBorderViewInfoBase; ACellBounds: TRect);
begin
end;

procedure TdxDocumentLayoutExporter.ExportTableBorderCorner(ACorner: TdxCornerViewInfoBase; X, Y: Integer);
begin
end;

procedure TdxDocumentLayoutExporter.ExportTableCell(ACell: TdxTableCellViewInfo);
begin
end;

procedure TdxDocumentLayoutExporter.ExportTableRow(ARow: TdxTableRowViewInfoBase);
begin
end;

procedure TdxDocumentLayoutExporter.ExportParagraphFrameBox(ABox: TdxParagraphFrameBox);
begin
end;

procedure TdxDocumentLayoutExporter.ExportDataContainerRunBox(ABox: TdxDataContainerRunBox);
begin
end;

procedure TdxDocumentLayoutExporter.ExportPage(APage: TdxPage);
begin
  ExportPageAreaCore(APage, FDocumentModel.MainPieceTable, ExportPageMainAreas);
end;

procedure TdxDocumentLayoutExporter.ExportPageArea(APageArea: TdxPageArea);
begin
  APageArea.Columns.ExportTo(Self);
end;

procedure TdxDocumentLayoutExporter.ExportPageAreaCore(APage: TdxPage; APieceTable: TdxPieceTable;
  AAction: TdxExportPageAction);
var
  AFloatingObjects: TList<TdxFloatingObjectBox>;
begin
  ExportFloatingObjects(APage.BackgroundFloatingObjects, APieceTable);
  AAction(APage);
  AFloatingObjects := APage.GetSortedNonBackgroundFloatingObjects;
  try
    ExportFloatingObjects(AFloatingObjects, APieceTable);
  finally
    AFloatingObjects.Free;
  end;
end;

procedure TdxDocumentLayoutExporter.ExportPageFooter(APage: TdxPage);
begin
end;

procedure TdxDocumentLayoutExporter.ExportPageHeader(APage: TdxPage);
begin
end;

procedure TdxDocumentLayoutExporter.ExportPageMainAreas(APage: TdxPage);
begin
  APage.Areas.ExportTo(Self);
end;

procedure TdxDocumentLayoutExporter.ExportParagraphFrame(AParagraphFrameBox: TdxParagraphFrameBox);
begin
  NotImplemented;
end;

procedure TdxDocumentLayoutExporter.ExportParagraphFrames(AColumn: TdxColumn);
var
  AParagraphFrames: TdxParagraphFrameBoxCollection;
  I: Integer;
begin
  AParagraphFrames := AColumn.InnerParagraphFrames;
  if AParagraphFrames = nil then
    Exit;
  for I := 0 to AParagraphFrames.Count - 1 do
    ExportParagraphFrame(AParagraphFrames[I]);
end;

procedure TdxDocumentLayoutExporter.ExportRow(ARow: TdxRow);
begin
  SetCurrentRow(ARow);
  ApplyCurrentRowTableCellClipping(ARow);
  ExportRowCore;
  SetCurrentRow(nil);
end;

procedure TdxDocumentLayoutExporter.ExportRowContentBoxes;
var
  ABoxes: TdxBoxCollection;
  ALastIndex: Integer;
  I: Integer;
begin
  if CurrentRow.NumberingListBox <> nil then
    CurrentRow.NumberingListBox.ExportTo(Self);
  ABoxes := CurrentRow.Boxes;
  ALastIndex := GetLastExportBoxInRowIndex(CurrentRow);
  for I := 0 to ALastIndex do
    ABoxes[I].ExportTo(Self);
end;

procedure TdxDocumentLayoutExporter.ExportRowCore;
begin
  ExportRowContentBoxes;
  ExportRowUnderlineBoxes;
  ExportRowStrikeoutBoxes;
end;

procedure TdxDocumentLayoutExporter.ExportRowErrorBoxes(ARow: TdxRow);
var
  I: Integer;
  AErrors: TdxErrorBoxCollection;
begin
  AErrors := ARow.Errors;
  if AErrors = nil then
    Exit;
  for I := 0 to AErrors.Count - 1 do
    ExportErrorBox(AErrors[I]);
end;

procedure TdxDocumentLayoutExporter.ExportRowErrorBoxes;
begin
  ExportRowErrorBoxes(CurrentRow);
end;

procedure TdxDocumentLayoutExporter.ExportRows(AColumn: TdxColumn);
begin
  AColumn.Rows.ExportTo(Self);
end;

procedure TdxDocumentLayoutExporter.ExportRowStrikeoutBoxes;
var
  AStrikeouts: TdxUnderlineBoxCollection;
  I: Integer;
begin
  AStrikeouts := CurrentRow.InnerStrikeouts;
  if AStrikeouts = nil then
    Exit;
  for I := 0 to AStrikeouts.Count - 1 do
    ExportStrikeoutBox(CurrentRow, AStrikeouts[I]);
end;

procedure TdxDocumentLayoutExporter.ExportRowUnderlineBoxes;
var
  AUnderlines: TdxUnderlineBoxCollection;
  I: Integer;
begin
  AUnderlines := CurrentRow.InnerUnderlines;
  if AUnderlines = nil then
    Exit;
  for I := 0 to AUnderlines.Count - 1 do
    ExportUnderlineBox(CurrentRow, AUnderlines[I]);
end;

procedure TdxDocumentLayoutExporter.ExportSpecialTextBox(ABox: TdxSpecialTextBox);
begin
  ExportTextBox(ABox);
end;

procedure TdxDocumentLayoutExporter.ExportTextBox(ABox: TdxTextBox);
begin
end;

procedure TdxDocumentLayoutExporter.FinishExport;
begin
end;

function TdxDocumentLayoutExporter.GetBoxText(ABox: TdxBox): string;
begin
  Result := ABox.GetText(PieceTable);
end;

function TdxDocumentLayoutExporter.GetClipBounds: TdxRectangleF;
begin
  Result := dxNullRectF; 
end;

function TdxDocumentLayoutExporter.GetDrawingBounds(const ABounds: TRect): TRect;
begin
  Result := ABounds;
end;

function TdxDocumentLayoutExporter.GetLastExportBoxInRowIndex(ARow: TdxRow): Integer;
begin
  Result := ARow.Boxes.Count - 1;
end;

function TdxDocumentLayoutExporter.GetShowWhitespace: Boolean;
begin
  Result := FShowWhitespace;
end;

function TdxDocumentLayoutExporter.IntersectClipBounds(const AOldClipBounds, ABounds: TdxRectangleF): TdxRectangleF;
begin
  NotImplemented;
end;

function TdxDocumentLayoutExporter.IsAnchorVisible(AAnchor: IdxTableCellVerticalAnchor): Boolean;
begin
  Result := True;
end;

function TdxDocumentLayoutExporter.IsTableRowVisible(ARow: TdxTableRowViewInfoBase): Boolean;
begin
  Result := True;
end;

function TdxDocumentLayoutExporter.IsValidBounds(const ABoxBounds: TRect): Boolean;
begin
  Result := True;
end;

procedure TdxDocumentLayoutExporter.RestoreClipBounds(const AClipBounds: TdxRectangleF);
begin
  SetClipBounds(AClipBounds);
end;

procedure TdxDocumentLayoutExporter.SetClipBounds(const AClipBounds: TdxRectangleF);
begin
end;

procedure TdxDocumentLayoutExporter.SetCurrentCell(ACell: TdxTableCellViewInfo);
begin
  FCurrentCell := ACell;
end;

procedure TdxDocumentLayoutExporter.SetCurrentRow(ARow: TdxRow);
begin
  FCurrentRow := ARow;
end;

procedure TdxDocumentLayoutExporter.SetShowWhitespace(const Value: Boolean);
begin
  FShowWhitespace := Value;
end;

{ TdxBoundedDocumentLayoutExporter }

constructor TdxBoundedDocumentLayoutExporter.Create(ADocumentModel: TdxDocumentModel; ABounds: TRect;
  ALinePaintingSupport: IdxPatternLinePaintingSupport);
begin
  inherited Create(ADocumentModel);
  Assert(ALinePaintingSupport <> nil); 
  FHorizontalLinePainter := CreateHorizontalLinePainter(ALinePaintingSupport);
  FBounds := ABounds;
end;

destructor TdxBoundedDocumentLayoutExporter.Destroy;
begin
  FreeAndNil(FHorizontalLinePainter);
  inherited Destroy;
end;

procedure TdxBoundedDocumentLayoutExporter.DrawDoubleSolidLine(ABounds: TdxRectangleF; AColor: TColor);
begin
  HorizontalLinePainter.DrawDoubleSolidLine(ABounds, AColor);
end;

procedure TdxBoundedDocumentLayoutExporter.DrawStrikeout(AStrikeout: TdxStrikeoutDouble; ABounds: TdxRectangleF;
  AColor: TColor);
begin
  HorizontalLinePainter.DrawStrikeout(AStrikeout, ABounds, AColor);
end;

procedure TdxBoundedDocumentLayoutExporter.DrawUnderline(AUnderline: TdxUnderlineDashed; ABounds: TdxRectangleF;
  AColor: TColor);
begin
  HorizontalLinePainter.DrawUnderline(AUnderline, ABounds, AColor);
end;

procedure TdxBoundedDocumentLayoutExporter.DrawUnderline(AUnderline: TdxUnderlineDotted; ABounds: TdxRectangleF;
  AColor: TColor);
begin
  HorizontalLinePainter.DrawUnderline(AUnderline, ABounds, AColor);
end;

procedure TdxBoundedDocumentLayoutExporter.DrawUnderline(AUnderline: TdxUnderlineSingle; ABounds: TdxRectangleF;
  AColor: TColor);
begin
  HorizontalLinePainter.DrawUnderline(AUnderline, ABounds, AColor);
end;

procedure TdxBoundedDocumentLayoutExporter.DrawUnderline(AUnderline: TdxUnderlineDouble; ABounds: TdxRectangleF; AColor: TColor);
begin
  HorizontalLinePainter.DrawUnderline(AUnderline, ABounds, AColor);
end;

procedure TdxBoundedDocumentLayoutExporter.DrawUnderline(AUnderline: TdxUnderlineThickDashDotted; ABounds: TdxRectangleF; AColor: TColor);
begin
  HorizontalLinePainter.DrawUnderline(AUnderline, ABounds, AColor);
end;

procedure TdxBoundedDocumentLayoutExporter.DrawUnderline(AUnderline: TdxUnderlineThickDashed; ABounds: TdxRectangleF;
  AColor: TColor);
begin
  HorizontalLinePainter.DrawUnderline(AUnderline, ABounds, AColor);
end;

procedure TdxBoundedDocumentLayoutExporter.DrawUnderline(AUnderline: TdxUnderlineThickDotted; ABounds: TdxRectangleF;
  AColor: TColor);
begin
  HorizontalLinePainter.DrawUnderline(AUnderline, ABounds, AColor);
end;

procedure TdxBoundedDocumentLayoutExporter.DrawUnderline(AUnderline: TdxUnderlineThickSingle; ABounds: TdxRectangleF;
  AColor: TColor);
begin
  HorizontalLinePainter.DrawUnderline(AUnderline, ABounds, AColor);
end;

procedure TdxBoundedDocumentLayoutExporter.DrawUnderline(AUnderline: TdxUnderlineLongDashed; ABounds: TdxRectangleF;
  AColor: TColor);
begin
  HorizontalLinePainter.DrawUnderline(AUnderline, ABounds, AColor);
end;

procedure TdxBoundedDocumentLayoutExporter.DrawUnderline(AUnderline: TdxUnderlineHeavyWave; ABounds: TdxRectangleF;
  AColor: TColor);
begin
  HorizontalLinePainter.DrawUnderline(AUnderline, ABounds, AColor);
end;

procedure TdxBoundedDocumentLayoutExporter.DrawUnderline(AUnderline: TdxUnderlineDashDotDotted; ABounds: TdxRectangleF;
  AColor: TColor);
begin
  HorizontalLinePainter.DrawUnderline(AUnderline, ABounds, AColor);
end;

procedure TdxBoundedDocumentLayoutExporter.DrawUnderline(AUnderline: TdxUnderlineDashDotted; ABounds: TdxRectangleF;
  AColor: TColor);
begin
  HorizontalLinePainter.DrawUnderline(AUnderline, ABounds, AColor);
end;

procedure TdxBoundedDocumentLayoutExporter.DrawUnderline(AUnderline: TdxUnderlineDashSmallGap; ABounds: TdxRectangleF;
  AColor: TColor);
begin
  HorizontalLinePainter.DrawUnderline(AUnderline, ABounds, AColor);
end;

procedure TdxBoundedDocumentLayoutExporter.DrawStrikeout(AStrikeout: TdxStrikeoutSingle; ABounds: TdxRectangleF;
  AColor: TColor);
begin
  HorizontalLinePainter.DrawStrikeout(AStrikeout, ABounds, AColor);
end;

procedure TdxBoundedDocumentLayoutExporter.ExportBackground;
begin
  ExportTextHighlighting;
end;

procedure TdxBoundedDocumentLayoutExporter.ExportHighlighting(AHighlightAreas: TdxHighlightAreaCollection);
var
  I: Integer;
begin
  if AHighlightAreas = nil then
    Exit;
  for I := 0 to AHighlightAreas.Count - 1 do
    ExportHighlightArea(AHighlightAreas[I]);
end;

procedure TdxBoundedDocumentLayoutExporter.ExportRowBackground(ARow: TdxRow);
begin
  if not ShouldExportRowBackground(ARow) then
    Exit;
  if not ShouldExportRow(ARow) then
    Exit;
  SetCurrentRow(ARow);
  ApplyCurrentRowTableCellClipping(ARow);
  ExportBackground;
  SetCurrentRow(nil);
end;

procedure TdxBoundedDocumentLayoutExporter.ExportRows(AColumn: TdxColumn);
var
  I: Integer;
begin
  for I := 0 to AColumn.Rows.Count - 1 do 
    ExportRowBackground(AColumn.Rows[I]);
  inherited ExportRows(AColumn);
end;

procedure TdxBoundedDocumentLayoutExporter.ExportTextHighlighting;
begin
  ExportHighlighting(CurrentRow.HighlightAreas);
end;

function TdxBoundedDocumentLayoutExporter.GetDrawingBounds(const ABounds: TRect): TRect;
begin
  Result := cxRectOffset(ABounds, Offset);
end;

function TdxBoundedDocumentLayoutExporter.GetOffset: TPoint;
begin
  Result := FBounds.TopLeft;
end;

function TdxBoundedDocumentLayoutExporter.GetShapeOutlinePenWidth(ARun: TdxFloatingObjectAnchorRun;
  ABox: TdxFloatingObjectBox): Integer;
begin
  if not cxRectIsEqual(ABox.Bounds, ABox.ContentBounds) then
    Result := Math.Max(1, DocumentModel.ToDocumentLayoutUnitConverter.ToLayoutUnits(ARun.Shape.OutlineWidth))
  else
    if ARun.Shape.UseOutlineColor and not TdxColor.IsTransparentOrEmpty(ARun.Shape.OutlineColor) and 
      ARun.Shape.UseOutlineWidth and (ARun.Shape.OutlineWidth = 0) then
      Result := 0
    else
      Result := -1;
end;

function TdxBoundedDocumentLayoutExporter.GetTabLeaderCharacter(ALeaderType: TdxTabLeaderType): Char;
begin
  case ALeaderType of
  TdxTabLeaderType.MiddleDots:
    Result := TdxCharacters.MiddleDot;
  TdxTabLeaderType.Hyphens:
    Result := TdxCharacters.Dash;
  TdxTabLeaderType.EqualSign:
    Result := TdxCharacters.EqualSign;
  TdxTabLeaderType.ThickLine, TdxTabLeaderType.Underline:
    Result := TdxCharacters.Underscore;
  else
    Result := TdxCharacters.Dot;
  end;
end;

function TdxBoundedDocumentLayoutExporter.GetTabLeaderCharacterWidth(ABox: TdxTabSpaceBox): Integer;
var
  AFontInfo: TdxFontInfo;
begin
  AFontInfo := ABox.GetFontInfo(PieceTable);
  case ABox.TabInfo.Leader of
    TdxTabLeaderType.MiddleDots:
      Result := AFontInfo.MiddleDotWidth;
    TdxTabLeaderType.Hyphens:
      Result := AFontInfo.DashWidth;
    TdxTabLeaderType.EqualSign:
      Result := AFontInfo.EqualSignWidth;
    TdxTabLeaderType.ThickLine, TdxTabLeaderType.Underline:
      Result := AFontInfo.UnderscoreWidth;
  else 
    Result := AFontInfo.DotWidth;
  end;
end;

function TdxBoundedDocumentLayoutExporter.GetTabLeaderText(ABox: TdxTabSpaceBox; ATextBounds: TRect): string;
var
  ACharacter: Char;
  ACount, ACharacterWidth: Integer;
begin
  Result := '';
  if ABox.TabInfo.Leader <> TdxTabLeaderType.None then
  begin
    ACharacter := GetTabLeaderCharacter(ABox.TabInfo.Leader);
    ACharacterWidth := GetTabLeaderCharacterWidth(ABox);
    ACount := cxRectWidth(ATextBounds) div Max(1, ACharacterWidth);
    Result := StringOfChar(ACharacter, ACount); 
  end;
end;

function TdxBoundedDocumentLayoutExporter.GetTabLeaderUnderlineType(ABox: TdxTabSpaceBox): TdxUnderlineType;
begin
  case ABox.TabInfo.Leader of
    TdxTabLeaderType.MiddleDots:
      Result := TdxUnderlineType.Dotted;
    TdxTabLeaderType.Hyphens:
      Result := TdxUnderlineType.Dashed;
    TdxTabLeaderType.Underline, TdxTabLeaderType.ThickLine:
      Result := TdxUnderlineType.Single;
    TdxTabLeaderType.EqualSign:
      Result := TdxUnderlineType.Double;
    else 
      Result := TdxUnderlineType.Dotted;
  end;
end;

function TdxBoundedDocumentLayoutExporter.GetTableBorderLine(ABorderLineStyle: TdxBorderLineStyle): TdxUnderline;
begin
  if ABorderLineStyle = TdxBorderLineStyle.Single then
    Exit(nil);
  Result := TdxUnderline(NotImplemented);
end;

function TdxBoundedDocumentLayoutExporter.IsValidBounds(const ABoxBounds: TRect): Boolean;
begin
  Result := cxRectIntersect(FBounds, ABoxBounds) and not cxRectIsEmpty(ABoxBounds);
end;

function TdxBoundedDocumentLayoutExporter.ShouldCenterTabLeaderLineVertically(ABox: TdxTabSpaceBox): Boolean;
begin
  case ABox.TabInfo.Leader of
    TdxTabLeaderType.MiddleDots:
      Result := True;
    TdxTabLeaderType.Hyphens:
      Result := True;
    TdxTabLeaderType.EqualSign:
      Result := True;
    TdxTabLeaderType.Underline, TdxTabLeaderType.ThickLine:
      Result := False;
    else 
      Result := False;
  end;
end;

function TdxBoundedDocumentLayoutExporter.ShouldExportRow(ARow: TdxRow): Boolean;
begin
  Result := True;
end;

function TdxBoundedDocumentLayoutExporter.ShouldExportRowBackground(ARow: TdxRow): Boolean;
begin
  Result := (ARow.InnerHighlightAreas <> nil) and (ARow.InnerHighlightAreas.Count > 0);
end;

procedure TdxBoundedDocumentLayoutExporter.DrawUnderline(AUnderline: TdxUnderlineThickDashDotDotted;
  ABounds: TdxRectangleF; AColor: TColor);
begin
  HorizontalLinePainter.DrawUnderline(AUnderline, ABounds, AColor);
end;

procedure TdxBoundedDocumentLayoutExporter.DrawUnderline(AUnderline: TdxUnderlineThickLongDashed;
  ABounds: TdxRectangleF; AColor: TColor);
begin
  HorizontalLinePainter.DrawUnderline(AUnderline, ABounds, AColor);
end;

procedure TdxBoundedDocumentLayoutExporter.DrawUnderline(AUnderline: TdxUnderlineDoubleWave; ABounds: TdxRectangleF;
  AColor: TColor);
begin
  HorizontalLinePainter.DrawUnderline(AUnderline, ABounds, AColor);
end;

procedure TdxBoundedDocumentLayoutExporter.DrawUnderline(AUnderline: TdxUnderlineWave; ABounds: TdxRectangleF;
  AColor: TColor);
begin
  HorizontalLinePainter.DrawUnderline(AUnderline, ABounds, AColor);
end;

{ TdxGraphicsDocumentLayoutExporter }

constructor TdxGraphicsDocumentLayoutExporter.Create(ADocumentModel: TdxDocumentModel; APainter: TdxPainter;
  AAdapter: TdxGraphicsDocumentLayoutExporterAdapter; ABounds: TRect; AHidePartiallyVisibleRow: Boolean = False);
begin
  inherited Create(ADocumentModel, ABounds, APainter);
  FPainter := APainter;
  FAdapter := AAdapter;
  FHidePartiallyVisibleRow := AHidePartiallyVisibleRow;
  ShowWhitespace := False;
end;

destructor TdxGraphicsDocumentLayoutExporter.Destroy;
begin
  FreeAndNil(FWhitespaceStrategy);
  inherited Destroy;
end;

function TdxGraphicsDocumentLayoutExporter.CreateHorizontalLinePainter(
  ALinePaintingSupport: IdxPatternLinePaintingSupport): TdxRichEditPatternLinePainter;
begin
  Result := TdxRichEditHorizontalPatternLinePainter.Create(ALinePaintingSupport, DocumentModel.LayoutUnitConverter);
end;

function TdxGraphicsDocumentLayoutExporter.CreateVerticalLinePainter(
  ALinePaintingSupport: IdxPatternLinePaintingSupport): TdxRichEditPatternLinePainter;
begin
  Result := NotImplemented;
end;

procedure TdxGraphicsDocumentLayoutExporter.ExcludeCellBounds(ACells: TdxTableCellViewInfoCollection;
  ARowBounds: TRect);
begin
  NotImplemented;
end;

procedure TdxGraphicsDocumentLayoutExporter.ExportBackground;
begin
  ExportFieldsHighlighting;
  inherited ExportBackground;
end;

procedure TdxGraphicsDocumentLayoutExporter.ExportColumnBreakBox(ABox: TdxColumnBreakBox);
begin
  FWhitespaceStrategy.ExportColumnBreakBox(ABox);
end;

procedure TdxGraphicsDocumentLayoutExporter.ExportColumnBreakBoxCore(ABox: TdxColumnBreakBox);
begin
  NotImplemented;
end;

procedure TdxGraphicsDocumentLayoutExporter.ExportErrorBox(AErrorBox: TdxErrorBox);
begin
end;

procedure TdxGraphicsDocumentLayoutExporter.ExportFieldsHighlighting;
begin
  ExportHighlighting(CurrentRow.InnerFieldHighlightAreas);
  ExportHighlighting(CurrentRow.InnerRangePermissionHighlightAreas);
end;

procedure TdxGraphicsDocumentLayoutExporter.ExportFloatingObjectBox(ABox: TdxFloatingObjectBox);
var
  ABounds: TRect;
  ACenter: TPoint;
  ATransformApplied: Boolean;
begin
  ABounds := GetDrawingBounds(ABox.Bounds);
  ACenter := TdxRectangleUtils.CenterPoint(ABounds);
  ATransformApplied := Painter.TryPushRotationTransform(ACenter, DocumentModel.GetBoxEffectiveRotationAngleInDegrees(ABox));
  Painter.PushSmoothingMode(ATransformApplied);
  try
    inherited ExportFloatingObjectBox(ABox);
  finally
    Painter.PopSmoothingMode;
    if ATransformApplied then
      Painter.PopTransform;
  end;
end;

procedure TdxGraphicsDocumentLayoutExporter.ExportFloatingObjectPicture(ABox: TdxFloatingObjectBox;
  APictureContent: TdxPictureFloatingObjectContent);
begin
  FAdapter.ExportFloatingObjectPicture(Self, ABox, APictureContent);
end;

procedure TdxGraphicsDocumentLayoutExporter.ExportFloatingObjectShape(ABox: TdxFloatingObjectBox; AShape: TdxShape);
var
  APen: TPen;
  APenWidth: Integer;
  AOutlineColor: TColor;
  AContentBounds, AShapeBounds: TRect;
  ARun: TdxFloatingObjectAnchorRun;
begin
  AContentBounds := GetDrawingBounds(ABox.ContentBounds);
  if not IsValidBounds(AContentBounds) then
    Exit;
  ARun := ABox.GetFloatingObjectRun;
  if not TdxColor.IsTransparentOrEmpty(ARun.Shape.FillColor) then 
    Painter.FillRectangle(GetActualColor(ARun.Shape.FillColor), AContentBounds);
  APenWidth := GetShapeOutlinePenWidth(ARun, ABox);
  if APenWidth >= 0 then
  begin
    AOutlineColor := GetActualColor(ARun.Shape.OutlineColor); 
    APen := TPen.Create;
    try
      APen.Color := AOutlineColor;
      APen.Width := APenWidth;
      AShapeBounds := GetDrawingBounds(ABox.Bounds);
      Painter.DrawRectangle(APen, AShapeBounds);
    finally
      FreeAndNil(APen);
    end;
  end;
end;

procedure TdxGraphicsDocumentLayoutExporter.ExportFloatingObjectTextBox(ABox: TdxFloatingObjectBox;
  ATextBoxContent: TdxTextBoxFloatingObjectContent; ATextBoxDocumentLayout: TdxDocumentLayout);
var
  ABounds: TRect;
  AOldPieceTable: TdxPieceTable;
  AOldClipRect,AClipRect: TdxRectF;
begin
  ABounds := GetDrawingBounds(ABox.ContentBounds);
  if not IsValidBounds(ABounds) then
    Exit;
  AOldClipRect := GetClipBounds;
  AClipRect := IntersectClipBounds(AOldClipRect, dxRectF(ABounds));
  if AClipRect = dxNullRectF then
    Exit;
  SetClipBounds(AClipRect);
  try
    AOldPieceTable := PieceTable;
    PieceTable := ATextBoxContent.TextBox.PieceTable;
    try
      ATextBoxDocumentLayout.Pages.First.ExportTo(Self);
    finally
      PieceTable := AOldPieceTable;
    end;
  finally
    SetClipBounds(AOldClipRect);
  end;
end;

procedure TdxGraphicsDocumentLayoutExporter.ExportHighlightArea(AArea: TdxHighlightArea);
begin
  Painter.FillRectangle(GetActualColor(AArea.Color), GetDrawingBounds(AArea.Bounds));
end;

procedure TdxGraphicsDocumentLayoutExporter.ExportHyphenBox(ABox: TdxHyphenBox);
begin
end;

procedure TdxGraphicsDocumentLayoutExporter.ExportInlinePictureBox(ABox: TdxInlinePictureBox);
begin
  FAdapter.ExportInlinePictureBox(Self, ABox);
end;

procedure TdxGraphicsDocumentLayoutExporter.ExportLayoutDependentTextBox(ABox: TdxLayoutDependentTextBox);
begin
  ExportTextBoxCore(ABox);
end;

procedure TdxGraphicsDocumentLayoutExporter.ExportLineBreakBox(ABox: TdxLineBreakBox);
begin
  FWhitespaceStrategy.ExportLineBreakBox(ABox);
end;

procedure TdxGraphicsDocumentLayoutExporter.ExportLineNumberBox(ABox: TdxLineNumberBox);
var
  ARun: TdxTextRunBase;
begin
  ARun := ABox.GetRun(PieceTable.DocumentModel.MainPieceTable);
  if TdxColor.IsTransparentOrEmpty(ARun.BackColor) then
    ExportHighlightArea(TdxHighlightArea.Create(ABox.Bounds, ARun.BackColor));
  ExportTextBoxCoreNoCheckBoundsValidity(ABox);
end;

procedure TdxGraphicsDocumentLayoutExporter.ExportMarkBoxCore(ABox: TdxBox; AExporter: TdxMarkBoxExporterBase);
var
  ABounds, ABoxBounds: TRect;
begin
  ABounds := GetDrawingBounds(ABox.Bounds);
  if not IsValidBounds(ABounds) then
    Exit;
  ABoxBounds := TRect.CreateSize(ABounds.Left, CurrentRow.Bounds.Top, ABounds.Width, CurrentRow.Height);
  NotImplemented;
end;

procedure TdxGraphicsDocumentLayoutExporter.ExportNumberingListBox(ABox: TdxNumberingListBox);
begin
  ExportNumberingListBoxCore(ABox);
end;

procedure TdxGraphicsDocumentLayoutExporter.ExportNumberingListBoxCore(ABox: TdxBox);
var
  ATextBounds: TRect;
  AText: string;
begin
  ATextBounds := GetDrawingBounds(ABox.Bounds);
  if IsValidBounds(ATextBounds) then
  begin
    AText := GetBoxText(ABox);
    ExportTextBoxCore(ABox, ATextBounds, AText);
    if ABox is TdxNumberingListBoxWithSeparator then
      FWhitespaceStrategy.ExportNumberingListBoxSeparator(TdxNumberingListBoxWithSeparator(ABox));
  end;
end;

procedure TdxGraphicsDocumentLayoutExporter.ExportPageBreakBox(ABox: TdxPageBreakBox);
begin
  FWhitespaceStrategy.ExportPageBreakBox(ABox);
end;

procedure TdxGraphicsDocumentLayoutExporter.ExportPageBreakBoxCore(ABox: TdxPageBreakBox);
begin
end;

procedure TdxGraphicsDocumentLayoutExporter.ExportParagraphFrameBox(ABox: TdxParagraphFrameBox);
begin
  NotImplemented;
end;

procedure TdxGraphicsDocumentLayoutExporter.ExportParagraphMarkBox(ABox: TdxParagraphMarkBox);
begin
  FWhitespaceStrategy.ExportParagraphMarkBox(ABox);
end;

procedure TdxGraphicsDocumentLayoutExporter.ExportParagraphMarkBoxCore(ABox: TdxParagraphMarkBox);
var
  ATextBounds: TRect;
  AText: string;
  ARun: TdxTextRunBase;
  ACell: TdxTableCell;
begin
  ATextBounds := GetDrawingBounds(ABox.Bounds);
  if IsValidBounds(ATextBounds) then
  begin
    AText := GetBoxText(ABox);
    ARun := ABox.GetRun(PieceTable);
    ACell := ARun.Paragraph.GetCell;
    if (ACell <> nil) and (ARun.Paragraph.Index = ACell.EndParagraphIndex) then
      AText := StringOfChar(TdxCharacters.CurrencySign, Length(AText)) 
    else
      AText := StringOfChar(TdxCharacters.PilcrowSign, Length(AText)); 
    ExportTextBoxCore(ABox, ATextBounds, AText);
  end;
end;

procedure TdxGraphicsDocumentLayoutExporter.ExportRow(ARow: TdxRow);
begin
  if ShouldExportRow(ARow) then
    inherited ExportRow(ARow);
end;

procedure TdxGraphicsDocumentLayoutExporter.ExportSectionMarkBox(ABox: TdxSectionMarkBox);
begin
  FWhitespaceStrategy.ExportSectionMarkBox(ABox);
end;

procedure TdxGraphicsDocumentLayoutExporter.ExportSectionMarkBoxCore(ABox: TdxSectionMarkBox);
begin
  NotImplemented;
end;

procedure TdxGraphicsDocumentLayoutExporter.ExportSeparatorBox(ABox: TdxSeparatorBox);
begin
  FWhitespaceStrategy.ExportSeparatorBox(ABox);
end;

procedure TdxGraphicsDocumentLayoutExporter.ExportSpaceBox(ABox: ISpaceBox);
begin
  FWhitespaceStrategy.ExportSpaceBox(ABox);
end;

procedure TdxGraphicsDocumentLayoutExporter.ExportSpaceBoxCore(ABox: TdxBox; ATextBounds: TRect;
  const AText: string);
var
  AForeColor: TColor;
  AFontInfo: TdxFontInfo;
begin
  AFontInfo := ABox.GetFontInfo(PieceTable);
  AForeColor := GetActualColor(ABox.GetActualForeColor(PieceTable));
  Painter.DrawSpacesString(AText, AFontInfo, AForeColor, ATextBounds);
end;

procedure TdxGraphicsDocumentLayoutExporter.ExportStrikeoutBox(ARow: TdxRow; AStrikeoutBox: TdxUnderlineBox);
var
  ABox: TdxBox;
  AColor: TColor;
  AStrikeout: TdxStrikeout;
begin
  ABox := ARow.Boxes[AStrikeoutBox.StartAnchorIndex];
  AColor := GetActualColor(ABox.GetActualStrikeoutColor(PieceTable));
  AStrikeout := DocumentModel.StrikeoutRepository.GetPatternLineByType(ABox.GetFontStrikeoutType(PieceTable));
  FAdapter.ExportStrikeoutLineBox(Self, Self, AStrikeoutBox, AStrikeout, AColor);
end;

procedure TdxGraphicsDocumentLayoutExporter.ExportTabLeaderAsCharacterSequence(ABox: TdxTabSpaceBox);
var
  ATextBounds: TRect;
begin
  ATextBounds := GetDrawingBounds(ABox.Bounds);
  if IsValidBounds(ATextBounds) then
    ExportTextBoxCore(ABox, ATextBounds, GetTabLeaderText(ABox, ATextBounds));
end;

procedure TdxGraphicsDocumentLayoutExporter.ExportTabLeaderAsUnderline(ABox: TdxTabSpaceBox);
begin
  NotImplemented;
end;

procedure TdxGraphicsDocumentLayoutExporter.ExportTableBorder(ABorder: TdxTableBorderViewInfoBase;
  ACellBounds: TRect);
var
  ABorderPainter: TdxTableBorderPainter;
begin
  ABorderPainter := GetBorderPainter(ABorder);
  if ABorderPainter <> nil then
  begin
    NotImplemented;
  end;
end;

procedure TdxGraphicsDocumentLayoutExporter.ExportTableBorderCorner(ACorner: TdxCornerViewInfoBase; X, Y: Integer);
var
  ABorderPainter: TdxTableCornerPainter;
  ALocation: TPoint;
begin
  ABorderPainter := GetBorderPainter(ACorner);
  if ABorderPainter <> nil then
  begin
    ALocation := Point(X, Y);
    ALocation.Offset(Offset);
    NotImplemented;
  end;
end;

procedure TdxGraphicsDocumentLayoutExporter.ExportTableCell(ACell: TdxTableCellViewInfo);
var
  ARect: TRect;
  AColor: TColor;
  ACellInnerTablesCount: Integer;
  I: Integer;
begin
  ARect := ACell.GetBackgroundBounds;
  AColor := ACell.Cell.BackgroundColor;
  if not (AColor <> TdxColor.Empty) then
    Painter.FillRectangle(AColor, GetDrawingBounds(ARect));
  ACellInnerTablesCount := ACell.InnerTables.Count;
  if ACellInnerTablesCount > 0 then
    for I := 0 to ACellInnerTablesCount - 1 do
      ACell.InnerTables[I].ExportBackground(Self);
end;

procedure TdxGraphicsDocumentLayoutExporter.ExportTableRow(ARow: TdxTableRowViewInfoBase);
var
  AOldClip: TdxRectF;
  ADrawingBounds: TRect;
begin
  AOldClip := Painter.ClipBounds;
  try
    ADrawingBounds := GetDrawingBounds(ARow.GetBounds);
    ExcludeCellBounds(ARow.Cells, ADrawingBounds);
    Painter.FillRectangle(GetActualBackgroundColor(ARow), ADrawingBounds);
  finally
    Painter.ClipBounds := AOldClip;
    Painter.ResetCellBoundsClip;
  end;
end;

procedure TdxGraphicsDocumentLayoutExporter.ExportTabSpaceBox(ABox: TdxTabSpaceBox);
begin
  FWhitespaceStrategy.ExportTabSpaceBox(ABox);
  if ABox.TabInfo.Leader <> TdxTabLeaderType.None then
    FAdapter.ExportTabLeader(Self, ABox);
end;

procedure TdxGraphicsDocumentLayoutExporter.ExportTextBox(ABox: TdxTextBox);
begin
  ExportTextBoxCore(ABox);
end;

procedure TdxGraphicsDocumentLayoutExporter.ExportTextBoxCore(ABox: TdxBox);
var
  AText: string;
  ATextBounds: TRect;
begin
  ATextBounds := GetDrawingBounds(ABox.Bounds);
  if IsValidBounds(ATextBounds) then
  begin
    AText := GetBoxText(ABox);
    ExportTextBoxCore(ABox, ATextBounds, AText);
  end;
end;

procedure TdxGraphicsDocumentLayoutExporter.ExportTextBoxCore(ABox: TdxBox; ATextBounds: TRect;
  const AText: string);
var
  AFontInfo: TdxFontInfo;
  AForeColor: TColor;
begin
  AFontInfo := ABox.GetFontInfo(PieceTable);
  AForeColor := GetActualColor(ABox.GetActualForeColor(PieceTable));
  Painter.DrawString(AText, AFontInfo, AForeColor, ATextBounds);
end;

procedure TdxGraphicsDocumentLayoutExporter.ExportTextBoxCoreNoCheckBoundsValidity(ABox: TdxBox);
var
  AText: string;
  ATextBounds: TRect;
begin
  ATextBounds := GetDrawingBounds(ABox.Bounds);
  AText := GetBoxText(ABox);
  ExportTextBoxCore(ABox, ATextBounds, AText);
end;

procedure TdxGraphicsDocumentLayoutExporter.ExportUnderlineBox(ARow: TdxRow; AUnderlineBox: TdxUnderlineBox);
var
  ABox: TdxBox;
  AColor: TColor;
  AUnderline: TdxUnderline;
begin
  ABox := ARow.Boxes[AUnderlineBox.StartAnchorIndex];
  AColor := GetActualColor(ABox.GetActualUnderlineColor(PieceTable));
  AUnderline := DocumentModel.UnderlineRepository.GetPatternLineByType(ABox.GetFontUnderlineType(PieceTable));
  FAdapter.ExportUnderlineLineBox(Self, Self, AUnderlineBox, AUnderline, AColor);
end;

function TdxGraphicsDocumentLayoutExporter.GetActualBackgroundColor(ARow: TdxTableRowViewInfoBase): TColor;
var
  ATablePropertiesException: TdxTableProperties;
begin
  ATablePropertiesException := ARow.Row.TablePropertiesException;
  if ATablePropertiesException.GetUse(TdxTablePropertiesOptions.MaskUseBackgroundColor) then
    Result := ATablePropertiesException.BackgroundColor
  else
    Result := ARow.TableViewInfo.Table.BackgroundColor;
end;

function TdxGraphicsDocumentLayoutExporter.GetActualColor(AColor: TColor): TColor;
var
  ARgbColor: Longint;
begin
  if not ShouldGrayContent then
    Result := AColor
  else
  begin
    ARgbColor := ColorToRGB(AColor);
    Result := TdxColor.Blend(TdxColor.FromArgb(128, GetRValue(ARgbColor), GetGValue(ARgbColor), GetBValue(ARgbColor)),
      TdxColor.White);
  end;
end;

function TdxGraphicsDocumentLayoutExporter.GetBorderPainter(ACorner: TdxCornerViewInfoBase): TdxTableCornerPainter;
begin
  if ACorner is TdxNoneLineCornerViewInfo then
    Result := nil
  else
    Result := TdxTableCornerPainter.Create;
end;

function TdxGraphicsDocumentLayoutExporter.GetClipBounds: TdxRectangleF;
begin
  Result := Painter.ClipBounds;
end;

function TdxGraphicsDocumentLayoutExporter.GetBorderPainter(
  AViewInfo: TdxTableBorderViewInfoBase): TdxTableBorderPainter;
begin
  Result := NotImplemented;
end;

function TdxGraphicsDocumentLayoutExporter.GetEffectivePieceTable(
  APieceTable: TdxPieceTable): TdxPieceTable;
var
  AContentType: TdxTextBoxContentType;
begin
  if APieceTable.IsTextBox then
  begin
    AContentType := TdxTextBoxContentType(pieceTable.ContentType);
    if AContentType.AnchorRun = nil then
      Result := APieceTable
    else
      Result := AContentType.AnchorRun.PieceTable;
  end
  else
    Result := APieceTable;
end;

function TdxGraphicsDocumentLayoutExporter.GetPainter: TdxPainter;
begin
  Result := FPainter;
end;

function TdxGraphicsDocumentLayoutExporter.IsAnchorVisible(AAnchor: IdxTableCellVerticalAnchor): Boolean;
begin
  if VisibleBounds.IsEmpty then
    Exit(True);
  if AAnchor.VerticalPosition + AAnchor.BottomTextIndent < VisibleBounds.Top then
    Exit(False);
  if AAnchor.VerticalPosition > VisibleBounds.Bottom then
    Exit(False);
  Result := True;
end;

function TdxGraphicsDocumentLayoutExporter.IsRowContainsVerticalMergingCell(ARow: TdxTableRow): Boolean;
var
  AModelCellCount: Integer;
  AModelCells: TdxTableCellCollection;
  I: Integer;
begin
  AModelCells := ARow.Cells;
  AModelCellCount := AModelCells.Count;
  for I := 0 to AModelCellCount - 1 do
    if AModelCells[I].VerticalMerging = TdxMergingState.Restart then
      Exit(True);
  Result := False;
end;

function TdxGraphicsDocumentLayoutExporter.IsTableRowVisible(ARow: TdxTableRowViewInfoBase): Boolean;
var
  AMaxTop, AMinBottom: Integer;
  ATopAnchor, ABottomAnchor: IdxTableCellVerticalAnchor;
begin
  if VisibleBounds.IsEmpty then
    Exit(True);
  if IsRowContainsVerticalMergingCell(ARow.Row) then
    Exit(True);
  ATopAnchor := ARow.TopAnchor;
  ABottomAnchor := ARow.BottomAnchor;
  AMaxTop := Math.Max(ATopAnchor.VerticalPosition, VisibleBounds.Top);
  AMinBottom := Math.Min(ABottomAnchor.VerticalPosition + ABottomAnchor.BottomTextIndent, VisibleBounds.Bottom);
  Result := AMaxTop <= AMinBottom;
end;

procedure TdxGraphicsDocumentLayoutExporter.SetClipBounds(const AClipBounds: TdxRectangleF);
begin
  Painter.ClipBounds := AClipBounds;
end;

procedure TdxGraphicsDocumentLayoutExporter.SetShowWhitespace(const Value: Boolean);
begin
  inherited SetShowWhitespace(Value); 
  FreeAndNil(FWhitespaceStrategy);
  if Value then
    FWhitespaceStrategy := TdxGraphicsDocumentLayoutExporterShowWhitespaceStrategy.Create(Self)
  else
    FWhitespaceStrategy := TdxGraphicsDocumentLayoutExporterHideWhitespaceStrategy.Create(Self);
end;

function TdxGraphicsDocumentLayoutExporter.ShouldExportRow(ARow: TdxRow): Boolean;
var
  ARowBounds, ADrawingBounds: TRect;
begin
  Result := False;
  if FHidePartiallyVisibleRow then
  begin
    ARowBounds := CalcRowContentBounds(ARow);
    if GetDrawingBounds(ARowBounds).Bottom > Bounds.Bottom then
      Exit;
  end;
  if not cxRectIsEmpty(VisibleBounds) then
  begin
    ARowBounds := CalcRowContentBounds(ARow);
    ADrawingBounds := GetDrawingBounds(ARowBounds);
    if (ADrawingBounds.Bottom < VisibleBounds.Top) or (ADrawingBounds.Top > VisibleBounds.Bottom) then
      Exit;
  end;
  Result := True;
end;

function TdxGraphicsDocumentLayoutExporter.ShouldExportRowBackground(ARow: TdxRow): Boolean;
begin
  Result := False;
  if (ARow.InnerHighlightAreas <> nil) and (ARow.InnerHighlightAreas.Count > 0) then
    Result := True
  else
    if (ARow.InnerFieldHighlightAreas <> nil) and (ARow.InnerFieldHighlightAreas.Count > 0) then
      Result := True
    else
      if (ARow.InnerRangePermissionHighlightAreas <> nil) and (ARow.InnerRangePermissionHighlightAreas.Count > 0) then
        Result := True;
end;

function TdxGraphicsDocumentLayoutExporter.ShouldGrayContent: Boolean;
var
  APieceTable, AActivePieceTable: TdxPieceTable;
begin
  APieceTable := GetEffectivePieceTable(PieceTable);
  AActivePieceTable := GetEffectivePieceTable(DocumentModel.ActivePieceTable);
  Result := (APieceTable <> AActivePieceTable) and DrawInactivePieceTableWithDifferentColor; 
end;

{ TdxScreenOptimizedGraphicsDocumentLayoutExporter }

constructor TdxScreenOptimizedGraphicsDocumentLayoutExporter.Create(ADocumentModel: TdxDocumentModel;
  APainter: TdxPainter; AAdapter: TdxGraphicsDocumentLayoutExporterAdapter; ABounds: TRect);
begin
  inherited Create(ADocumentModel, APainter, AAdapter, ABounds);
  FPixel := Math.Ceil(ADocumentModel.LayoutUnitConverter.PixelsToLayoutUnits(1, APainter.DpiY));
end;

class constructor TdxScreenOptimizedGraphicsDocumentLayoutExporter.Initialize;
begin
  FHalftoneBrush := CreateHalftoneBrush;
end;

class destructor TdxScreenOptimizedGraphicsDocumentLayoutExporter.Finalize;
begin
  if FHalftoneBrush <> 0 then
    DeleteObject(FHalftoneBrush);
end;

class function TdxScreenOptimizedGraphicsDocumentLayoutExporter.CreateHalftoneBrush: HBRUSH;
var
  X, Y: Integer;
  DC: HDC;
  Pattern: HBITMAP;
begin
  Pattern := CreateBitmap(8, 8, 1, 1, nil);
  DC := CreateCompatibleDC(0);
  Pattern := SelectObject(DC, Pattern);
  FillRect(DC, Rect(0, 0, 8, 8), GetStockObject(WHITE_BRUSH));
  for Y := 0 to 7 do
    for X := 0 to 7 do
      if (Y mod 2) = (X mod 2) then SetPixel(DC, X, Y, 0);
  Pattern := SelectObject(DC, Pattern);
  DeleteDC(DC);
  Result := CreatePatternBrush(Pattern);
  DeleteObject(Pattern);
end;

procedure TdxScreenOptimizedGraphicsDocumentLayoutExporter.ExportBoxOptimized(ABox: TdxBox);
var
  AActualBounds: TRect;
  APrevTextColor: TColorRef;
  DC: HDC;
begin
  AActualBounds := ABox.Bounds;

  Dec(AActualBounds.Bottom, FPixel);
  if AActualBounds.Height < FPixel then
    AActualBounds.Height := FPixel;

  DC := (Painter as TdxGdiPlusPainter).Graphics.Handle;
  APrevTextColor := GetTextColor(DC);
  SetTextColor(DC, GetActualColor(ABox.GetActualForeColor(PieceTable)));
  Windows.FillRect(DC, AActualBounds, FHalftoneBrush);
  SetTextColor(DC, APrevTextColor);
end;

procedure TdxScreenOptimizedGraphicsDocumentLayoutExporter.ExportHyphenBox(ABox: TdxHyphenBox);
begin
  if ShouldOptimizeBox(ABox) then
    ExportBoxOptimized(ABox)
  else
    inherited ExportHyphenBox(ABox);
end;

procedure TdxScreenOptimizedGraphicsDocumentLayoutExporter.ExportLayoutDependentTextBox(
  ABox: TdxLayoutDependentTextBox);
begin
  if ShouldOptimizeBox(ABox) then
    ExportBoxOptimized(ABox)
  else
    inherited ExportTextBox(ABox);
end;

procedure TdxScreenOptimizedGraphicsDocumentLayoutExporter.ExportLineNumberBox(ABox: TdxLineNumberBox);
begin
  if ShouldOptimizeBox(ABox) then
    ExportBoxOptimized(ABox)
  else
    inherited ExportLineNumberBox(ABox);
end;

procedure TdxScreenOptimizedGraphicsDocumentLayoutExporter.ExportNumberingListBox(ABox: TdxNumberingListBox);
begin
  if ShouldOptimizeBox(ABox) then
    ExportBoxOptimized(ABox)
  else
    inherited ExportNumberingListBox(ABox);
end;

procedure TdxScreenOptimizedGraphicsDocumentLayoutExporter.ExportParagraphMarkBox(ABox: TdxParagraphMarkBox);
begin
  if not ShouldOptimizeBox(ABox) then
    inherited ExportParagraphMarkBox(ABox);
end;

procedure TdxScreenOptimizedGraphicsDocumentLayoutExporter.ExportRowErrorBoxes;
begin
  if not ShouldOptimizeBox(CurrentRow) then
    inherited ExportRowErrorBoxes;
end;

procedure TdxScreenOptimizedGraphicsDocumentLayoutExporter.ExportRowStrikeoutBoxes;
begin
    inherited ExportRowStrikeoutBoxes;
end;

procedure TdxScreenOptimizedGraphicsDocumentLayoutExporter.ExportRowUnderlineBoxes;
begin
    inherited ExportRowUnderlineBoxes;
end;

procedure TdxScreenOptimizedGraphicsDocumentLayoutExporter.ExportSeparatorBox(ABox: TdxSeparatorBox);
begin
  if ShouldOptimizeBox(ABox) then
    ExportBoxOptimized(ABox)
  else
    inherited ExportSeparatorBox(ABox);
end;

procedure TdxScreenOptimizedGraphicsDocumentLayoutExporter.ExportSpaceBox(ABox: ISpaceBox);
begin
  if not ShouldOptimizeBox(ABox.Box) then
    inherited ExportSpaceBox(ABox);
end;

procedure TdxScreenOptimizedGraphicsDocumentLayoutExporter.ExportTextBox(ABox: TdxTextBox);
begin
  if ShouldOptimizeBox(ABox) then
    ExportBoxOptimized(ABox)
  else
    inherited ExportTextBox(ABox);
end;

procedure TdxScreenOptimizedGraphicsDocumentLayoutExporter.FinishExport;
begin
  Painter.FinishPaint;
end;

function TdxScreenOptimizedGraphicsDocumentLayoutExporter.ShouldOptimizeBox(ABox: TdxBox): Boolean;
begin
  Result := ABox.Bounds.Height < MinReadableTextHeight;
end;

{ TdxScreenOptimizedGraphicsDocumentLayoutExporterNoLineNumbers }

procedure TdxScreenOptimizedGraphicsDocumentLayoutExporterNoLineNumbers.ExportLineNumberBox(ABox: TdxLineNumberBox);
begin
end;

{ TdxWinFormsGraphicsDocumentLayoutExporterAdapter }

procedure TdxWinFormsGraphicsDocumentLayoutExporterAdapter.ExportUnderlineLineBox(
  AExporter: TdxGraphicsDocumentLayoutExporter; ALinePainter: IdxUnderlinePainter; ALineBox: TdxUnderlineBox;
  ALine: TdxUnderline; ALineColor: TColor);
var
  AActualBounds: TdxRectangleF;
begin
  AActualBounds := ExportLineBoxCore<TdxUnderlineType>(AExporter, ALineBox, ALine);
  if not cxRectIsEmpty(AActualBounds) then
    ALine.Draw(ALinePainter, AActualBounds, ALineColor);
end;

procedure TdxWinFormsGraphicsDocumentLayoutExporterAdapter.ExportLineBreakBoxCore(
  AExporter: TdxGraphicsDocumentLayoutExporter; ABox: TdxLineBreakBox);
var
  AForeColor: TColor;
  ACharacterBounds, AGlyphBounds: TRect;
begin
  ACharacterBounds := AExporter.GetDrawingBounds(ABox.Bounds);
  AGlyphBounds := ACharacterBounds;
  AGlyphBounds.Height := ACharacterBounds.Width;
  AGlyphBounds.Offset(0, (ACharacterBounds.Height - AGlyphBounds.Height) div 2);

  AForeColor := AExporter.GetActualColor(ABox.GetActualForeColor(AExporter.PieceTable));
  DrawLineBreakArrow(AExporter, AGlyphBounds, AForeColor);
end;

procedure TdxWinFormsGraphicsDocumentLayoutExporterAdapter.ExportSeparatorBoxCore(
  AExporter: TdxGraphicsDocumentLayoutExporter; ABox: TdxSeparatorBox);
begin
  ExportSingleCharacterMarkBoxCore(AExporter, ABox);
end;

procedure TdxWinFormsGraphicsDocumentLayoutExporterAdapter.ExportSingleCharacterMarkBoxCore(
  AExporter: TdxGraphicsDocumentLayoutExporter; ABox: TdxSingleCharacterMarkBox);
var
  ABoxBounds, ATextBounds: TRect;
  AForeColor: TColor;
  AFontInfo: TdxFontInfo;
  AText: string;
  ATextSize: TSize;
begin
  ABoxBounds := AExporter.GetDrawingBounds(ABox.Bounds);
  if AExporter.IsValidBounds(ABoxBounds) then
  begin
    AForeColor := AExporter.GetActualColor(ABox.GetActualForeColor(AExporter.PieceTable));

    AFontInfo := ABox.GetFontInfo(AExporter.PieceTable);

    AText := ABox.MarkCharacter; 
    ATextSize := AExporter.DocumentModel.FontCache.Measurer.MeasureString(AText, AFontInfo);
    ATextBounds := ABoxBounds;
    ATextBounds := cxRectSetLeft(ATextBounds, ATextBounds.Left + (ATextBounds.Width - ATextSize.cx) div 2);
    ATextBounds.Width := ATextSize.cx;
    ATextBounds := cxRectSetTop(ATextBounds, ATextBounds.Top + (ATextBounds.Height - ATextSize.cy) div 2);
    AExporter.Painter.DrawString(AText, AFontInfo, AForeColor, ATextBounds);
  end;
end;

procedure TdxWinFormsGraphicsDocumentLayoutExporterAdapter.ExportStrikeoutLineBox(AExporter: TdxGraphicsDocumentLayoutExporter; ALinePainter: IdxStrikeoutPainter; ALineBox: TdxUnderlineBox; ALine: TdxStrikeout; ALineColor: TColor);
var
  AActualBounds: TdxRectangleF;
begin
  AActualBounds := ExportLineBoxCore<TdxStrikeoutType>(AExporter, ALineBox, ALine);
  if not cxRectIsEmpty(AActualBounds) then
    ALine.Draw(ALinePainter, AActualBounds, ALineColor);
end;

procedure TdxWinFormsGraphicsDocumentLayoutExporterAdapter.ExportTabLeader(AExporter: TdxGraphicsDocumentLayoutExporter;
  ABox: TdxTabSpaceBox);
begin
  AExporter.ExportTabLeaderAsCharacterSequence(ABox);
end;

procedure TdxWinFormsGraphicsDocumentLayoutExporterAdapter.ExportTabSpaceBoxCore(
  AExporter: TdxGraphicsDocumentLayoutExporter; ABox: TdxTabSpaceBox);
begin
  ExportSingleCharacterMarkBoxCore(AExporter, ABox);
end;

procedure TdxWinFormsGraphicsDocumentLayoutExporterAdapter.DrawLineBreakArrow(
  AExporter: TdxGraphicsDocumentLayoutExporter; const AGlyphBounds: TRect; AForeColor: TColor);
const
  ArrowPointsTemplate: array[0..8] of TdxPointF = (
    (X: 0.00; Y: 0.60),
    (X: 0.35; Y: 0.35),
    (X: 0.30; Y: 0.55),
    (X: 0.80; Y: 0.55),
    (X: 0.80; Y: 0.10),
    (X: 0.90; Y: 0.10),
    (X: 0.90; Y: 0.65),
    (X: 0.30; Y: 0.65),
    (X: 0.35; Y: 0.85)
  );
var
  APoint: TdxPointF;
  ABrush: TBrush;
  I, ACount: Integer;
  AArrowPoints: TArray<TdxPointF>;
begin
  ACount := Length(ArrowPointsTemplate);
  SetLength(AArrowPoints, ACount);
  for I := 0 to ACount - 1 do
  begin
    APoint := ArrowPointsTemplate[I];
    AArrowPoints[I] := dxPointF(AGlyphBounds.Left + AGlyphBounds.Width * APoint.X,
      AGlyphBounds.Top + AGlyphBounds.Height * APoint.Y);
  end;

  ABrush := TBrush.Create;
  try
    ABrush.Color := AForeColor;
    AExporter.Painter.FillPolygon(ABrush, AArrowPoints);
  finally
    ABrush.Free;
  end;
end;

procedure TdxWinFormsGraphicsDocumentLayoutExporterAdapter.ExportFloatingObjectPicture(
  AExporter: TdxGraphicsDocumentLayoutExporter; ABox: TdxFloatingObjectBox;
  APictureContent: TdxPictureFloatingObjectContent);
var
  AImgBounds: TRect;
begin
  AImgBounds := AExporter.GetDrawingBounds(ABox.ContentBounds);
  if AExporter.IsValidBounds(AImgBounds) then
  begin
    AExporter.Painter.DrawImage(APictureContent.Image.Image, AImgBounds);
    if AExporter.ShouldGrayContent then
      AExporter.Painter.FillRectangle(TdxColor.FromArgb($80, $FF, $FF, $FF), AImgBounds);
  end;
end;

procedure TdxWinFormsGraphicsDocumentLayoutExporterAdapter.ExportInlinePictureBox(
  AExporter: TdxGraphicsDocumentLayoutExporter; ABox: TdxInlinePictureBox);
var
  AImg: TdxOfficeImage;
  ASize: TSize;
  AImgBounds, ARowBounds: TRect;
  ASizing: TdxImageSizeMode;
begin
  AImg := ABox.GetImage(AExporter.PieceTable);
  AImgBounds := AExporter.GetDrawingBounds(ABox.Bounds);
  ASize := ABox.GetImageActualSizeInLayoutUnits(AExporter.PieceTable);
  ASizing := ABox.GetSizing(AExporter.PieceTable);
  if AExporter.IsValidBounds(AImgBounds) then
  begin
      ARowBounds := AExporter.CurrentRow.Bounds;
      if ABox.Bounds.Height > ARowBounds.Height then
      begin
          try
            ExportInlinePictureBoxCore(AExporter, ABox, AImg, AImgBounds, ASize, ASizing);
          finally
          end;
      end
      else
        ExportInlinePictureBoxCore(AExporter, ABox, AImg, AImgBounds, ASize, ASizing);
  end;
end;

procedure TdxWinFormsGraphicsDocumentLayoutExporterAdapter.ExportInlinePictureBoxCore(
  AExporter: TdxGraphicsDocumentLayoutExporter; ABox: TdxInlinePictureBox; AImg: TdxOfficeImage; AImgBounds: TRect;
  ASize: TSize; ASizing: TdxImageSizeMode);
var
  APainter: TdxPainter;
begin
  APainter := AExporter.Painter;
  APainter.DrawImage(AImg, AImgBounds, ASize, ASizing);
  ABox.ExportHotZones(APainter);
  if AExporter.ShouldGrayContent then
    APainter.FillRectangle(TdxColor.FromArgb($80, $ff, $ff, $ff), AImgBounds); 
end;

function TdxWinFormsGraphicsDocumentLayoutExporterAdapter.ExportLineBoxCore<T>(
  AExporter: TdxGraphicsDocumentLayoutExporter; ALineBox: TdxUnderlineBox; ALine: TdxPatternLine<T>): TdxRectangleF;
var
  AClipBounds: TRect;
begin
  AClipBounds := ALine.CalcLineBounds(ALineBox.ClipBounds, ALineBox.UnderlineThickness);
  AClipBounds := AExporter.GetDrawingBounds(AClipBounds);
  Result := dxRectF(ALine.CalcLineBounds(ALineBox.UnderlineBounds, ALineBox.UnderlineThickness));
  Result := cxRectSetTop(Result, Result.Top + Trunc(ALine.CalcLinePenVerticalOffset(Result)));
  Result := dxRectF(AExporter.GetDrawingBounds(cxRect(Result)));
  Result.Left := Max(Result.Left, AClipBounds.Left);
  Result.Right := Min(Result.Right, AClipBounds.Left + cxRectWidth(AClipBounds));
  if cxRectHeight(Result) = 0 then 
    Result.Top := Result.Top - 1;
end;

{ TdxGraphicsDocumentLayoutExporterWhitespaceStrategy }

constructor TdxGraphicsDocumentLayoutExporterWhitespaceStrategy.Create(AExporter: TdxGraphicsDocumentLayoutExporter);
begin
  inherited Create;
  Assert(AExporter <> nil);
  FExporter := AExporter;
end;

{ TdxGraphicsDocumentLayoutExporterHideWhitespaceStrategy }

procedure TdxGraphicsDocumentLayoutExporterHideWhitespaceStrategy.ExportColumnBreakBox(ABox: TdxColumnBreakBox);
begin
end;

procedure TdxGraphicsDocumentLayoutExporterHideWhitespaceStrategy.ExportLineBreakBox(ABox: TdxLineBreakBox);
begin
end;

procedure TdxGraphicsDocumentLayoutExporterHideWhitespaceStrategy.ExportPageBreakBox(ABox: TdxPageBreakBox);
begin
end;

procedure TdxGraphicsDocumentLayoutExporterHideWhitespaceStrategy.ExportParagraphMarkBox(ABox: TdxParagraphMarkBox);
begin
  if False then 
    inherited ExportParagraphMarkBox(ABox);
end;

procedure TdxGraphicsDocumentLayoutExporterHideWhitespaceStrategy.ExportSectionMarkBox(ABox: TdxSectionMarkBox);
begin
end;

procedure TdxGraphicsDocumentLayoutExporterHideWhitespaceStrategy.ExportSeparatorBox(ABox: TdxSeparatorBox);
begin
  if False then 
    inherited ExportSeparatorBox(ABox);
end;

procedure TdxGraphicsDocumentLayoutExporterHideWhitespaceStrategy.ExportSpaceBox(ABox: ISpaceBox);
begin
  if False then 
    inherited ExportSpaceBox(ABox);
end;

procedure TdxGraphicsDocumentLayoutExporterHideWhitespaceStrategy.ExportTabSpaceBox(ABox: TdxTabSpaceBox);
begin
  if False then 
    inherited ExportTabSpaceBox(ABox);
end;

function TdxGraphicsDocumentLayoutExporterHideWhitespaceStrategy.GetFormattingMarkVisibility: TdxFormattingMarkVisibilityOptions;
begin
  Result := Exporter.DocumentModel.FormattingMarkVisibilityOptions;
end;

{ TdxGraphicsDocumentLayoutExporterShowWhitespaceStrategy }

procedure TdxGraphicsDocumentLayoutExporterShowWhitespaceStrategy.ExportColumnBreakBox(ABox: TdxColumnBreakBox);
begin
  Exporter.ExportColumnBreakBoxCore(ABox);
end;

procedure TdxGraphicsDocumentLayoutExporterShowWhitespaceStrategy.ExportLineBreakBox(ABox: TdxLineBreakBox);
begin
  Exporter.Adapter.ExportLineBreakBoxCore(Exporter, ABox);
end;

procedure TdxGraphicsDocumentLayoutExporterShowWhitespaceStrategy.ExportNumberingListBoxSeparator(
  ANumberingListBoxWithSeparator: TdxNumberingListBoxWithSeparator);
var
  ASeparatorBox: TdxBox;
begin
  ASeparatorBox := ANumberingListBoxWithSeparator.SeparatorBox;
  if (ASeparatorBox is TdxTabSpaceBox) and not TdxTabSpaceBox(ASeparatorBox).Bounds.IsEmpty then 
    ExportTabSpaceBox(TdxTabSpaceBox(ASeparatorBox));
end;

procedure TdxGraphicsDocumentLayoutExporterShowWhitespaceStrategy.ExportPageBreakBox(ABox: TdxPageBreakBox);
begin
  Exporter.ExportPageBreakBoxCore(ABox);
end;

procedure TdxGraphicsDocumentLayoutExporterShowWhitespaceStrategy.ExportParagraphMarkBox(ABox: TdxParagraphMarkBox);
begin
  Exporter.ExportParagraphMarkBoxCore(ABox);
end;

procedure TdxGraphicsDocumentLayoutExporterShowWhitespaceStrategy.ExportSectionMarkBox(ABox: TdxSectionMarkBox);
begin
  Exporter.ExportSectionMarkBoxCore(ABox);
end;

procedure TdxGraphicsDocumentLayoutExporterShowWhitespaceStrategy.ExportSeparatorBox(ABox: TdxSeparatorBox);
begin
  Exporter.Adapter.ExportSeparatorBoxCore(Exporter, ABox);
end;

procedure TdxGraphicsDocumentLayoutExporterShowWhitespaceStrategy.ExportSpaceBox(ASpaceBox: ISpaceBox);
var
  ABox: TdxBox;
  ATextBounds: TRect;
  AText: string;
begin
  ABox := ASpaceBox.Box;
  ATextBounds := Exporter.GetDrawingBounds(ABox.Bounds);
  if Exporter.IsValidBounds(ATextBounds) then
  begin
    AText := Exporter.GetBoxText(ABox);
    AText := StringOfChar(TdxCharacters.MiddleDot, Length(AText)); 
    Exporter.ExportSpaceBoxCore(ABox, ATextBounds, AText);
  end;
end;

procedure TdxGraphicsDocumentLayoutExporterShowWhitespaceStrategy.ExportTabSpaceBox(ABox: TdxTabSpaceBox);
begin
  Exporter.Adapter.ExportTabSpaceBoxCore(Exporter, ABox);
end;

{ TdxCharacterBoxLevelDocumentLayoutExporter }

constructor TdxCharacterBoxLevelDocumentLayoutExporter.Create(
  ADocumentModel: TdxDocumentModel; AChars: TdxCharacterBoxCollection;
  AMeasurer: TdxBoxMeasurer);
begin
  inherited Create(ADocumentModel);
  FChars := AChars;
  FMeasurer := AMeasurer;
end;

procedure TdxCharacterBoxLevelDocumentLayoutExporter.ExportColumnBreakBox(
  ABox: TdxColumnBreakBox);
begin
  ExportSingleCharacterBox(ABox);
end;

procedure TdxCharacterBoxLevelDocumentLayoutExporter.ExportCustomRunBox(
  ABox: TdxCustomRunBox);
begin
  ExportSingleCharacterBox(ABox);
end;

procedure TdxCharacterBoxLevelDocumentLayoutExporter.ExportDataContainerRunBox(
  ABox: TdxDataContainerRunBox);
begin
  ExportSingleCharacterBox(ABox);
end;

procedure TdxCharacterBoxLevelDocumentLayoutExporter.ExportHyphenBox(
  ABox: TdxHyphenBox);
begin
  ExportSingleCharacterBox(ABox);
end;

procedure TdxCharacterBoxLevelDocumentLayoutExporter.ExportInlinePictureBox(
  ABox: TdxInlinePictureBox);
begin
  ExportSingleCharacterBox(ABox);
end;

procedure TdxCharacterBoxLevelDocumentLayoutExporter.ExportLayoutDependentTextBox(
  ABox: TdxLayoutDependentTextBox);
begin
  ExportSingleCharacterBox(ABox);
end;

procedure TdxCharacterBoxLevelDocumentLayoutExporter.ExportLineBreakBox(
  ABox: TdxLineBreakBox);
begin
  ExportSingleCharacterBox(ABox);
end;

procedure TdxCharacterBoxLevelDocumentLayoutExporter.ExportNumberingListBox(
  ABox: TdxNumberingListBox);
begin
  ExportSingleCharacterBox(ABox);
end;

procedure TdxCharacterBoxLevelDocumentLayoutExporter.ExportPageBreakBox(
  ABox: TdxPageBreakBox);
begin
  ExportSingleCharacterBox(ABox);
end;

procedure TdxCharacterBoxLevelDocumentLayoutExporter.ExportParagraphMarkBox(
  ABox: TdxParagraphMarkBox);
begin
  ExportSingleCharacterBox(ABox);
end;

procedure TdxCharacterBoxLevelDocumentLayoutExporter.ExportRowBox(ARow: TdxRow; ABox: TdxBox);
var
  APreviousPieceTable: TdxPieceTable;
begin
  FRowBounds := ARow.Bounds;
  APreviousPieceTable := PieceTable;
  try
    PieceTable := ARow.Paragraph.PieceTable;
    ABox.ExportTo(Self);
  finally
    PieceTable := APreviousPieceTable;
  end;
end;

procedure TdxCharacterBoxLevelDocumentLayoutExporter.ExportRowCore;
var
  APreviousPieceTable: TdxPieceTable;
begin
  FRowBounds := CurrentRow.Bounds;
  APreviousPieceTable := PieceTable;
  try
    PieceTable := CurrentRow.Paragraph.PieceTable;
    inherited ExportRowCore;
  finally
    PieceTable := APreviousPieceTable;
  end;
end;

procedure TdxCharacterBoxLevelDocumentLayoutExporter.ExportSingleCharacterBox(ABox: TdxBox);
begin
  AppendCharacterBox(ABox.StartPos, 0, GetActualCharacterBounds(ABox.Bounds), ABox.Bounds);
end;

procedure TdxCharacterBoxLevelDocumentLayoutExporter.ExportSectionMarkBox(
  ABox: TdxSectionMarkBox);
begin
  ExportSingleCharacterBox(ABox);
end;

procedure TdxCharacterBoxLevelDocumentLayoutExporter.ExportSeparatorBox(
  ABox: TdxSeparatorBox);
begin
  ExportSingleCharacterBox(ABox);
end;

procedure TdxCharacterBoxLevelDocumentLayoutExporter.ExportSpaceBox(
  ABox: ISpaceBox);
var
  AExportBox: TdxBox;
  ASpaceCount: Integer;
  ACharacterBounds: TRects;
begin
  AExportBox := ABox.Box;
  ASpaceCount := AExportBox.EndPos.Offset - AExportBox.StartPos.Offset + 1;
  ACharacterBounds := RectSplitHorizontally(AExportBox.Bounds, ASpaceCount);
  ExportMultiCharacterBox(AExportBox, ACharacterBounds);
end;

procedure TdxCharacterBoxLevelDocumentLayoutExporter.ExportTabSpaceBox(
  ABox: TdxTabSpaceBox);
begin
  ExportSingleCharacterBox(ABox);
end;

procedure TdxCharacterBoxLevelDocumentLayoutExporter.ExportTextBox(
  ABox: TdxTextBox);
var
  ACharacterBounds: TRects;
begin
  ACharacterBounds := CalculateCharacterBounds(ABox);
  ExportMultiCharacterBox(ABox, ACharacterBounds);
end;

function TdxCharacterBoxLevelDocumentLayoutExporter.GetPainter: TdxPainter;
begin
  Result := NotImplemented;
end;

procedure TdxCharacterBoxLevelDocumentLayoutExporter.AppendCharacterBox(AStartPos: TdxFormatterPosition;
  AOffset: Integer; const ABounds, ATightBounds: TRect);
var
  ACharacterBox: TdxCharacterBox;
  ANewStartPos: TdxFormatterPosition;
begin
  ACharacterBox := TdxCharacterBox.Create;
  ACharacterBox.Bounds := ABounds;
  ACharacterBox.TightBounds := ATightBounds;
  ANewStartPos := TdxFormatterPosition.Create(AStartPos.RunIndex, AStartPos.Offset + AOffset, AStartPos.BoxIndex);
  ACharacterBox.StartPos := ANewStartPos;
  ACharacterBox.EndPos := ANewStartPos;
  Chars.Add(ACharacterBox);
end;

function TdxCharacterBoxLevelDocumentLayoutExporter.CalculateCharacterBounds(ABox: TdxTextBox): TRects;
begin
  Result := Measurer.MeasureCharactersBounds(GetBoxText(ABox), ABox.GetFontInfo(PieceTable), ABox.Bounds);
end;

function TdxCharacterBoxLevelDocumentLayoutExporter.GetActualCharacterBounds(const ABounds: TRect): TRect;
begin
  Result := Rect(ABounds.Left, FRowBounds.Top, ABounds.Right, FRowBounds.Bottom);
end;

procedure TdxCharacterBoxLevelDocumentLayoutExporter.ExportMultiCharacterBox(ABox: TdxBox; ACharacterBounds: TRects);
var
  AStartPos: TdxFormatterPosition;
  ACount: Integer;
  I: Integer;
begin
  AStartPos := ABox.StartPos;
  ACount := ABox.EndPos.Offset - AStartPos.Offset + 1;
  Assert(Length(ACharacterBounds) = ACount);
  for I := 0 to ACount - 1 do
    AppendCharacterBox(AStartPos, I, GetActualCharacterBounds(ACharacterBounds[I]), ACharacterBounds[I]);
end;

end.
