{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Utils.BackgroundThreadUIUpdater;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Classes, Generics.Collections;

type
  TdxAction = TThreadMethod; 

  { TdxBackgroundThreadUIUpdater }

  TdxBackgroundThreadUIUpdater = class abstract
  public
    procedure UpdateUI(AMethod: TThreadMethod); virtual; abstract;
  end;

  { TdxBeginInvokeBackgroundThreadUIUpdater }

  TdxBeginInvokeBackgroundThreadUIUpdater = class(TdxBackgroundThreadUIUpdater)
  public
    procedure UpdateUI(AMethod: TdxAction); override;
  end;

  { TdxDeferredBackgroundThreadUIUpdater }

  TdxDeferredBackgroundThreadUIUpdater = class(TdxBackgroundThreadUIUpdater)
  strict private
    FUpdates: TList<TdxAction>;
  public
    constructor Create;
    destructor Destroy; override;
    procedure UpdateUI(AMethod: TdxAction); override;

    property Updates: TList<TdxAction> read FUpdates;
  end;

implementation

uses
  SysUtils;

{ TdxBeginInvokeBackgroundThreadUIUpdater }

procedure TdxBeginInvokeBackgroundThreadUIUpdater.UpdateUI(AMethod: TdxAction);
begin
  TThread.Queue(nil, AMethod);
end;

{ TdxDeferredBackgroundThreadUIUpdater }

constructor TdxDeferredBackgroundThreadUIUpdater.Create;
begin
  inherited Create;
  FUpdates := TList<TdxAction>.Create;
end;

destructor TdxDeferredBackgroundThreadUIUpdater.Destroy;
begin
  FreeAndNil(FUpdates);
  inherited Destroy;
end;

procedure TdxDeferredBackgroundThreadUIUpdater.UpdateUI(AMethod: TdxAction);
begin
  if not FUpdates.Contains(AMethod) then
    FUpdates.Add(AMethod);
end;

end.
