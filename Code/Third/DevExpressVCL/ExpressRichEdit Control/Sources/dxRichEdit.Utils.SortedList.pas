{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Utils.SortedList;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Generics.Collections, Generics.Defaults, dxRichEdit.Utils.Types;

type

  { TdxSortedList }

  TdxSortedList<T> = class
  private
    FInnerList: TList<T>;
    FComparer: IComparer<T>;
    function GetItem(Index: Integer): T;
  public
    constructor Create; overload;
    constructor Create(const AComparer: IComparer<T>); overload;
    destructor Destroy; override;

    function Count: Integer;
    function First: T;
    function Last: T;

    procedure Add(const AValue: T);
    procedure Insert(AIndex: Integer; const AValue: T);
    function Contains(const AValue: T): Boolean; virtual;
    function BinarySearch(const AValue: T): Integer; overload;
    function BinarySearch(const APredicate: IdxComparable<T>): Integer; overload;
    procedure Delete(AIndex: Integer);
    procedure Remove(const AValue: T); virtual;
    procedure RemoveFrom(const AValue: T); virtual;
    procedure Clear; virtual;
    function Clone: TdxSortedList<T>; virtual;
    procedure CopyCore(ADestination: TdxSortedList<T>); virtual;
    function CreateEmptyList: TdxSortedList<T>; virtual;

    property Self[Index: Integer]: T read GetItem; default;
  end;

implementation

uses
  dxRichEdit.DocumentModel.Core;

{ TdxSortedList<T> }

constructor TdxSortedList<T>.Create;
begin
  inherited Create;
  FInnerList := TList<T>.Create;
end;

constructor TdxSortedList<T>.Create(const AComparer: IComparer<T>);
begin
  Create;
  Assert(AComparer <> nil);
  FComparer := AComparer;
end;

destructor TdxSortedList<T>.Destroy;
begin
  FInnerList.Free;
  inherited Destroy;
end;

function TdxSortedList<T>.Count: Integer;
begin
  Result := FInnerList.Count;
end;

function TdxSortedList<T>.First: T;
begin
  if Count <= 0 then
    NotImplemented 
  else
    Result := FInnerList[0];
end;

function TdxSortedList<T>.Last: T;
begin
  if Count <= 0 then
    NotImplemented 
  else
    Result := FInnerList[Count - 1];
end;

procedure TdxSortedList<T>.Add(const AValue: T);
var
  AIndex: Integer;
begin
  AIndex := BinarySearch(AValue);
  if AIndex >= 0 then
    Exit;
  Insert(not AIndex, AValue);
end;

procedure TdxSortedList<T>.Insert(AIndex: Integer; const AValue: T);
begin
  FInnerList.Insert(AIndex, AValue);
end;

function TdxSortedList<T>.Contains(const AValue: T): Boolean;
begin
  Result := BinarySearch(AValue) >= 0;
end;

function TdxSortedList<T>.BinarySearch(const AValue: T): Integer;
begin
  if not FInnerList.BinarySearch(AValue, Result, FComparer) then
    Result := not Result;
end;

function TdxSortedList<T>.BinarySearch(const APredicate: IdxComparable<T>): Integer;
begin
  Result := TdxAlgorithms.BinarySearch<T>(FInnerList, APredicate);
end;

procedure TdxSortedList<T>.Delete(AIndex: Integer);
begin
  FInnerList.Delete(AIndex);
end;

procedure TdxSortedList<T>.Remove(const AValue: T);
var
  AIndex: Integer;
begin
  AIndex := BinarySearch(AValue);
  if AIndex >= 0 then
    Delete(AIndex);
end;

procedure TdxSortedList<T>.RemoveFrom(const AValue: T);
var
  I, AStartIndex: Integer;
begin
  AStartIndex := BinarySearch(AValue);
  if AStartIndex < 0 then
    AStartIndex := not AStartIndex;
  for I := FinnerList.Count - 1 downto AStartIndex do
    Delete(I);
end;

procedure TdxSortedList<T>.Clear;
begin
  FInnerList.Clear;
end;

function TdxSortedList<T>.Clone: TdxSortedList<T>;
begin
  Result := CreateEmptyList;
  CopyCore(Result);
end;

procedure TdxSortedList<T>.CopyCore(ADestination: TdxSortedList<T>);
begin
  ADestination.FInnerList.AddRange(FInnerList);
end;

function TdxSortedList<T>.CreateEmptyList: TdxSortedList<T>;
begin
  Result := TdxSortedList<T>.Create;
end;

function TdxSortedList<T>.GetItem(Index: Integer): T;
begin
  Result := FInnerList[Index];
end;

end.
