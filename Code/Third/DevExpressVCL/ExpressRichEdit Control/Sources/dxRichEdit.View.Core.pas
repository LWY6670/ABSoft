{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.View.Core;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface


uses
  SysUtils, Types, Generics.Collections, Controls, Graphics, SyncObjs, Classes, ActiveX,
  cxGeometry, cxGraphics, dxCoreClasses, cxControls,
  dxRichEdit.Utils.Types,
  dxRichEdit.Utils.DataObject,
  dxRichEdit.Utils.BackgroundThreadUIUpdater,
  dxRichEdit.Utils.BatchUpdateHelper,
  dxRichEdit.Utils.PredefinedFontSizeCollection,
  dxRichEdit.DocumentModel.Core,
  dxRichEdit.DocumentModel.PieceTable,
  dxRichEdit.DocumentModel.Selection,
  dxRichEdit.DocumentLayout,
  dxRichEdit.DocumentLayout.Position,
  dxRichEdit.LayoutEngine.DocumentFormatter,
  dxRichEdit.LayoutEngine.Formatter,
  dxRichEdit.View.ViewInfo,
  dxRichEdit.View.PageViewInfoGenerator,
  dxRichEdit.Control.HitTest,
  dxRichEdit.Control.DragAndDrop.Types,
  dxRichEdit.Control.HotZones,
  dxRichEdit.Options,
  dxRichEdit.Platform.Win.Painter,
  dxRichEdit.DocumentModel.ParagraphFormatting;

type
  TdxDraftView = class; 
  TdxPrintLayoutView = class; 
  TdxReadingLayoutView = class; 
  TdxDragCaret = class;
  TdxDragCaretPosition = class;
  TdxCopySelectionManager = class;
  TdxRichEditControlOptionsBase = class;

  TdxRefreshAction =(
    AllDocument = 1,
    Zoom = 2,
    Transforms = 4,
    Selection = 8
  );

  TdxScrollEventType = ( 
    EndScroll,
    First,
    LargeDecrement,
    LargeIncrement,
    Last,
    SmallDecrement,
    SmallIncrement,
    ThumbPosition,
    ThumbTrack
  );

  TdxScrollOrientation = ( 
    HorizontalScroll,
    VerticalScroll
  );

  TdxRichEditView = class;
  TdxSelectionLayout = class;
  TdxRichEditViewHorizontalScrollController = class;
  TdxRichEditViewVerticalScrollController = class;
  TdxScrollBarAdapter = class;
  TdxBackgroundFormatter = class;
  TdxScrollEventArgs = class;
  TdxMouseCursorCalculator = class;
  IdxPlatformSpecificScrollBarAdapter = interface;

  TdxPlatformIndependentScrollEventArgs = TdxScrollEventArgs;

  TdxScrollEventHandler = procedure (ASender: TObject; E: TdxScrollEventArgs) of object;

  TdxScrollEvents = TdxMulticastMethod<TdxScrollEventHandler>;
  PdxScrollEvents = ^TdxScrollEvents; 

  { IdxOfficeScrollbar }

  IdxOfficeScrollbar = interface
    function GetEnabled: Boolean;
    procedure SetEnabled(Value: Boolean);
    function GetLargeChange: Integer;
    procedure SetLargeChange(Value: Integer);
    function GetMaximum: Integer;
    procedure SetMaximum(Value: Integer);
    function GetMinimum: Integer;
    procedure SetMinimum(Value: Integer);
    function GetSmallChange: Integer;
    procedure SetSmallChange(Value: Integer);
    function GetValue: Integer;
    procedure SetValue(Value: Integer);
    function GetScroll: PdxScrollEvents;

    procedure BeginUpdate;
    procedure EndUpdate;

    property Enabled: Boolean read GetEnabled write SetEnabled;
    property LargeChange: Integer read GetLargeChange write SetLargeChange;
    property Maximum: Integer read GetMaximum write SetMaximum;
    property Minimum: Integer read GetMinimum write SetMinimum;
    property SmallChange: Integer read GetSmallChange write SetSmallChange;
    property Value: Integer read GetValue write SetValue;
    property Scroll: PdxScrollEvents read GetScroll;
  end;

  { IdxInnerControl }

  IdxInnerControl = interface(IdxBoxMeasurerProvider) 
    function GetActiveView: TdxRichEditView;
    function GetDocumentModel: TdxDocumentModel;
    function GetDocumentModelTemplate: TdxDocumentModel;
    function GetFormatter: TdxBackgroundFormatter;
    function GetModified: Boolean;
    function GetOptions: TdxRichEditControlOptionsBase;
    function GetPredefinedFontSizeCollection: TdxPredefinedFontSizeCollection;
    function GetHorizontalScrollBar: IdxOfficeScrollbar; 
    function GetVerticalScrollBar: IdxOfficeScrollbar;   

    procedure RaiseUpdateUI;
    procedure SetModified(const Value: Boolean);

    function CreateCopySelectionManager: TdxCopySelectionManager;
    function CreateMouseCursorCalculator: TdxMouseCursorCalculator;

    function IsEditable: Boolean;
    function IsHyperlinkModifierKeysPress: Boolean;

    procedure BeginDocumentRendering;
    procedure EndDocumentRendering;

    function SaveDocumentAs: Boolean;
    function SaveDocument: Boolean;

    procedure OnBeforeExport(Sender: TObject; E: TdxBeforeExportEventArgs);
    procedure OnCalculateDocumentVariable(Sender: TObject; E: TdxCalculateDocumentVariableEventArgs);

    property ActiveView: TdxRichEditView read GetActiveView;
    property DocumentModel: TdxDocumentModel read GetDocumentModel;
    property DocumentModelTemplate: TdxDocumentModel read GetDocumentModelTemplate;
    property Formatter: TdxBackgroundFormatter read GetFormatter;
    property Modified: Boolean read GetModified write SetModified;
    property Options: TdxRichEditControlOptionsBase read GetOptions;
    property PredefinedFontSizeCollection: TdxPredefinedFontSizeCollection read GetPredefinedFontSizeCollection;

    property HorizontalScrollBar: IdxOfficeScrollbar read GetHorizontalScrollBar;
    property VerticalScrollBar: IdxOfficeScrollbar read GetVerticalScrollBar;
 end;

  TdxShowParagraphFormCallback = reference to procedure(AProperties: TdxMergedParagraphProperties; AData: TObject);

  { IdxRichEditControl }

  IdxRichEditControl = interface(IdxBatchUpdateable) 
    function GetInnerControl: IdxInnerControl;
    function GetRichEditControl: TObject;

    function GetCursor: TCursor;
    function GetOvertype: Boolean;
    procedure SetCursor(Value: TCursor);
    procedure SetOvertype(const Value: Boolean);

    function UseStandardDragDropMode: Boolean;

    function CreateRichEditViewVerticalScrollController(ARichEditView: TdxRichEditView): TdxRichEditViewVerticalScrollController;
    function CreateRichEditViewHorizontalScrollController(ARichEditView: TdxRichEditView): TdxRichEditViewHorizontalScrollController;
    function CreatePlatformSpecificScrollBarAdapter: IdxPlatformSpecificScrollBarAdapter;
    procedure UpdateUIFromBackgroundThread(AMethod: TdxAction);

    procedure CreateDragCaret;
    procedure DestroyDragCaret;
    function DoDragDrop(const AData: IDataObject; AllowedEffects: TdxDragDropEffects): TdxDragDropEffects;
    procedure DrawDragCaret;
    function GetDragCaret: TdxDragCaret;

    procedure HideCaret;
    procedure ShowCaret;
    procedure OnViewPaddingChanged;

    procedure OnResizeCore; 

    procedure AddKeyboardService(const AService: IdxKeyboardHandlerService);
    procedure RemoveKeyboardService(const AService: IdxKeyboardHandlerService);

    procedure ShowParagraphForm(AParagraphProperties: TdxMergedParagraphProperties; ACallback: TdxShowParagraphFormCallback; ACallbackData: TObject); 
    procedure UpdateControlAutoSize;
    procedure RedrawEnsureSecondaryFormattingComplete(Action: TdxRefreshAction);

    property Cursor: TCursor read GetCursor write SetCursor;
    property DragCaret: TdxDragCaret read GetDragCaret;
    property InnerControl: IdxInnerControl read GetInnerControl; 
    property Overtype: Boolean read GetOvertype write SetOvertype;
  end;

  { TdxRichEditControlPadding }

  TdxRichEditControlPadding = class(TcxGeometryObject)
  private
    FValue: TRect;
    FDefaultValue: TRect;
    function GetProperty(AIndex: Integer): Integer;
    function IsPropertyStored(AIndex: Integer): Boolean;
    procedure SetValue(const Value: TRect);
    procedure SetProperty(AIndex, AValue: Integer);
  protected
    procedure DoAssign(Source: TPersistent); override;
  public
    constructor Create(AOwner: TPersistent; const ADefaultValue: TRect); reintroduce; overload;
    constructor Create(AOwner: TPersistent; ADefaultValue: Integer = 0); reintroduce; overload;
    function IsDefault: Boolean; inline;
    function IsEqual(const APadding: TdxRichEditControlPadding): Boolean;
    procedure Reset;

    property Value: TRect read FValue write SetValue;
  published
    property All: Integer index 0 read GetProperty write SetProperty stored IsPropertyStored;
    property Left: Integer index 1 read GetProperty write SetProperty stored IsPropertyStored;
    property Top: Integer index 2 read GetProperty write SetProperty stored IsPropertyStored;
    property Right: Integer index 3 read GetProperty write SetProperty stored IsPropertyStored;
    property Bottom: Integer index 4 read GetProperty write SetProperty stored IsPropertyStored;
  end;

  { TdxRichEditControlOptionsBase }

  TdxRichEditControlOptionsBase = class abstract(TdxRichEditNotificationOptions)
  strict private
    FDocumentServer: IdxInnerControl;
  private
    function GetBehavior: TdxRichEditBehaviorOptions;
    function GetShowHiddenText: Boolean;
    procedure SetShowHiddenText(const Value: Boolean);
    function GetFormattingMarkVisibility: TdxFormattingMarkVisibilityOptions;
    procedure SetBehavior(const Value: TdxRichEditBehaviorOptions);
    function GetDocumentCapabilities: TdxDocumentCapabilitiesOptions;
    procedure SetDocumentCapabilities(const Value: TdxDocumentCapabilitiesOptions);
  protected
    property DocumentServer: IdxInnerControl read FDocumentServer;
  public
    constructor Create(ADocumentServer: IdxInnerControl); reintroduce; virtual;
    procedure Assign(Source: TPersistent); override;

    property Behavior: TdxRichEditBehaviorOptions read GetBehavior write SetBehavior;
   property ShowHiddenText: Boolean read GetShowHiddenText write SetShowHiddenText default False; 
    property DocumentCapabilities: TdxDocumentCapabilitiesOptions read GetDocumentCapabilities write SetDocumentCapabilities; 
    property FormattingMarkVisibility: TdxFormattingMarkVisibilityOptions read GetFormattingMarkVisibility; 
  end;

  { TdxScrollEventArgs }

  TdxScrollEventArgs = class 
  private
    FScrollOrientation: TdxScrollOrientation;
    FNewValue: Integer;
    FOldValue: Integer;
    FType: TdxScrollEventType;
  public
    constructor Create(AType: TdxScrollEventType; ANewValue: Integer;
      AScrollOrientation: TdxScrollOrientation = TdxScrollOrientation.HorizontalScroll); overload;
    constructor Create(AType: TdxScrollEventType; AOldValue, ANewValue: Integer;
      AScrollOrientation: TdxScrollOrientation = TdxScrollOrientation.HorizontalScroll); overload;

    property NewValue: Integer read FNewValue write FNewValue;
    property OldValue: Integer read FOldValue;
    property ScrollOrientation: TdxScrollOrientation read FScrollOrientation;
    property &Type: TdxScrollEventType read FType;
  end;

  { IdxPlatformSpecificScrollBarAdapter }

  IdxPlatformSpecificScrollBarAdapter = interface 
  ['{8B96CD9D-C46B-4913-9D25-81D0557E6A6C}']
    procedure OnScroll(AAdapter: TdxScrollBarAdapter; ASender: TObject; E: TdxPlatformIndependentScrollEventArgs); 
    procedure ApplyValuesToScrollBarCore(AAdapter: TdxScrollBarAdapter); 
    function GetRawScrollBarValue(AAdapter: TdxScrollBarAdapter): Integer; 
    function SetRawScrollBarValue(AAdapter: TdxScrollBarAdapter; Value: Integer): Boolean; 
    function GetPageUpRawScrollBarValue(AAdapter: TdxScrollBarAdapter): Integer; 
    function GetPageDownRawScrollBarValue(AAdapter: TdxScrollBarAdapter): Integer; 
    function CreateLastScrollEventArgs(AAdapter: TdxScrollBarAdapter): TdxPlatformIndependentScrollEventArgs; 
  end;

  { IdxRichEditViewVisitor }

  IdxRichEditViewVisitor = interface 
  ['{8810AB47-931E-40A8-9F5F-F532EFF14E50}']

    procedure Visit(AView: TdxRichEditView); 
  end;

  { TdxMouseCursorCalculator }

  TdxMouseCursorCalculator = class
  private
    FView: TdxRichEditView;
  protected
    function CalculateCore(AResult: TdxRichEditHitTestResult): TCursor;
    function CalculateHotZoneCursor(AResult: TdxRichEditHitTestResult): TCursor;
    function CalculateHotZoneCursorCore(AHotZone: TdxHotZone): TCursor;
    function CalculateCharacterMouseCursor(AResult: TdxRichEditHitTestResult): TCursor;
    function IsEditable: Boolean;
  public
    constructor Create(AView: TdxRichEditView);

    function Calculate(AHitTestResult: TdxRichEditHitTestResult; const APhysicalPoint: TPoint): TCursor; virtual;

    property View: TdxRichEditView read FView;
  end;

  { TdxCaretDocumentLayoutPosition }

  TdxCaretDocumentLayoutPosition = class(TdxDocumentLayoutPosition) 
  private
    FView: TdxRichEditView; 
  protected
    procedure EnsureFormattingComplete; override; 
    procedure EnsurePageSecondaryFormattingComplete(APage: TdxPage); override; 
    function CreateEmptyClone: TdxDocumentLayoutPosition; override; 
    function GetPieceTable: TdxPieceTable; override; 
  public
    constructor Create(AView: TdxRichEditView); 
  end;

  { TdxCaretPosition }

  TdxCaretPosition = class 
  private
    FView: TdxRichEditView; 
    FCaretBoundsPosition: TdxDocumentLayoutPosition; 
    FX: Integer; 
    FPageViewInfo: TdxPageViewInfo; 
    FTableCellTopAnchorIndex: Integer; 
    FPosition: TdxDocumentLayoutPosition; 
    FInputPosition: TdxInputPosition; 
    FPreferredPageIndex: Integer; 

    function GetActualLogPosition(AEndPosition: TdxDocumentLogPosition): TdxDocumentLogPosition; 
    function GetDocumentModel: TdxDocumentModel;
    function GetPreferredPageIndex: Integer;
    procedure SetPreferredPageIndex(const Value: Integer);
  protected
    function GetVirtualLogPosition: TdxDocumentLogPosition; virtual;
    function GetLogPosition: TdxDocumentLogPosition; virtual;
    function GetUsePreviousBoxBounds: Boolean; virtual;
    function CreateCaretDocumentLayoutPosition: TdxDocumentLayoutPosition; virtual; 
    function CreateDragCaretPosition: TdxDragCaretPosition; 

    procedure Invalidate; virtual; 
    procedure InvalidatePageViewInfo; virtual; 
    procedure InvalidateInputPosition; virtual; 
    function UpdatePositionTrySetUsePreviousBoxBounds(ADetailsLevel: TdxDocumentLayoutDetailsLevel): Boolean; virtual; 
    function UpdateCaretDocumentLayoutPosition(ADetailsLevel: TdxDocumentLayoutDetailsLevel): TdxDocumentLayoutPosition; virtual; 
    function GetCaretBoundsDocumentLayoutPosition(ALogPosition: TdxDocumentLogPosition): TdxDocumentLayoutPosition; virtual; 
    function CalculateCurrentFormatingPosition(ALogPosition: TdxDocumentLogPosition): TdxDocumentModelPosition; virtual; 

    property View: TdxRichEditView read FView; 
    property VirtualLogPosition: TdxDocumentLogPosition read GetVirtualLogPosition; 
    property UsePreviousBoxBounds: Boolean read GetUsePreviousBoxBounds; 
    property CaretBoundsPosition: TdxDocumentLayoutPosition read FCaretBoundsPosition; 
  public
    constructor Create(AView: TdxRichEditView; APreferredPageIndex: Integer); 
    destructor Destroy; override;

    function Update(ADetailsLevel: TdxDocumentLayoutDetailsLevel): Boolean; virtual; 
    function GetInputPosition: TdxInputPosition; virtual; 
    function TryGetInputPosition: TdxInputPosition; virtual; 
    function CalculateCaretBounds: TRect; virtual; 
    function CreateExplicitCaretPosition(ALogPosition: TdxDocumentLogPosition): TdxCaretPosition; virtual; 
    function CreateInputPosition: TdxInputPosition; overload; virtual; 
    function CreateInputPosition(ALogPosition: TdxDocumentLogPosition): TdxInputPosition; overload; virtual; 

    property PageViewInfo: TdxPageViewInfo read FPageViewInfo; 
    property X: Integer read FX write FX; 
    property TableCellTopAnchorIndex: Integer read FTableCellTopAnchorIndex write FTableCellTopAnchorIndex; 
    property LayoutPosition: TdxDocumentLayoutPosition read FPosition; 
    property LogPosition: TdxDocumentLogPosition read GetLogPosition; 
    property DocumentModel: TdxDocumentModel read GetDocumentModel; 
    property PreferredPageIndex: Integer read GetPreferredPageIndex write SetPreferredPageIndex; 
  end;

  { TdxDragCaretPosition }

  TdxDragCaretPosition = class(TdxCaretPosition)
  strict private
    FLogPosition: TdxDocumentLogPosition;
  protected
    function GetLogPosition: Integer; override;
    function GetVirtualLogPosition: Integer; override;
    function GetUsePreviousBoxBounds: Boolean; override;
  public
    procedure SetLogPosition(ALogPosition: TdxDocumentLogPosition);
  end;

  TdxHeaderFooterCaretPosition = class(TdxCaretPosition);

  TdxExplicitCaretPosition = class(TdxCaretPosition)
  strict private
    FLogPosition: TdxDocumentLogPosition;
  protected
    function GetLogPosition: Integer; override;
    function GetVirtualLogPosition: Integer; override;
    function GetUsePreviousBoxBounds: Boolean; override;
    function UpdatePositionTrySetUsePreviousBoxBounds(ADetailsLevel: TdxDocumentLayoutDetailsLevel): Boolean; override;
  public
    constructor Create(AView: TdxRichEditView; ALogPosition: TdxDocumentLogPosition; APreferredPageIndex: Integer); reintroduce;
  end;

  { TdxCaret }

  TdxCaret = class 
  private
    FHidden: Boolean; 
    FBounds: TRect;
    procedure SetHidden(const Value: Boolean);
  protected
    procedure DrawCore(ACanvas: TcxCanvas); virtual; 
  public
    constructor Create;

    procedure Draw(ACanvas: TcxCanvas);
    function ShouldDrawCaret(ADocumentModel: TdxDocumentModel): Boolean; virtual; 
    function GetCaretPosition(AView: TdxRichEditView): TdxCaretPosition; virtual; 

    property Bounds: TRect read FBounds write FBounds; 
    property IsHidden: Boolean read FHidden write SetHidden; 
  end;

  { TdxDragCaret }

  TdxDragCaret = class(TdxCaret)
  strict private
    FPosition: TdxDragCaretPosition;
  protected
    procedure DrawCore(ACanvas: TcxCanvas); override;
  public
    constructor Create(AView: TdxRichEditView);
    destructor Destroy; override;

    function ShouldDrawCaret(ADocumentModel: TdxDocumentModel): Boolean; override;
    function GetCaretPosition(AView: TdxRichEditView): TdxCaretPosition; override;
    procedure SetLogPosition(ALogPosition: TdxDocumentLogPosition);
  end;

  { TdxDocumentSelectionLayout }

  TdxDocumentSelectionLayout = class(TcxIUnknownObject, IdxSelectionLayoutItem) 
  protected
    function GetType: TClass; 
  private
    FUpdatedSuccessfully: Boolean; 
    FHotZones: TdxHotZoneCollection; 
    FNestedItems: TObjectDictionary<Integer, TList<IdxSelectionLayoutItem>>; 
    FSelectionLayout: TdxSelectionLayout; 
    FPieceTable: TdxPieceTable; 
    FStart: TdxDocumentLayoutPosition; 
    FEnd: TdxDocumentLayoutPosition; 
    FDocumentLayout: TdxDocumentLayout; 
    function GetView: TdxRichEditView;
    function GetDocumentModel: TdxDocumentModel;
    function UpdateCore: Boolean; 
    function FindFloatingObject(AFloatingObjects: TList<TdxFloatingObjectBox>; AIndex: TdxRunIndex): TdxFloatingObjectBox; overload; 
    function FindFloatingObject(APage: TdxPage; AIndex: TdxRunIndex): TdxFloatingObjectBox; overload; 
    function TryToCreateRectangularObjectSelectionLayout(AStart, AEnd: TdxDocumentLayoutPosition): TdxRectangularObjectSelectionLayout; 
    function TryToCreateRectangularObjectSelectionLayoutByAnchorRun(const ARunInfo: TdxDocumentModelPosition): TdxRectangularObjectSelectionLayout; overload; 
    function TryToCreateRectangularObjectSelectionLayoutByAnchorRun(const ARunInfo: TdxDocumentModelPosition; out APos: TdxDocumentLayoutPosition): TdxRectangularObjectSelectionLayout; overload; 
    function UpdateNestedItems: Boolean; 

    property SelectionLayout: TdxSelectionLayout read FSelectionLayout; 
    property NestedItems: TObjectDictionary<Integer, TList<IdxSelectionLayoutItem>> read FNestedItems; 
  protected
    function GetBox(APos: TdxDocumentLayoutPosition): TdxPage; 
    function CreateDocumentLayoutPosition(ALogPosition: TdxDocumentLogPosition): TdxDocumentLayoutPosition; virtual; 
    procedure AddNestedItem(APageIndex: Integer; AItem: IdxSelectionLayoutItem); 

    property PieceTable: TdxPieceTable read FPieceTable; 
    property DocumentLayout: TdxDocumentLayout read FDocumentLayout; 
    property View: TdxRichEditView read GetView; 
    property DocumentModel: TdxDocumentModel read GetDocumentModel; 
  public
    constructor Create(ASelectionLayout: TdxSelectionLayout; AStart, AEnd: TdxDocumentLayoutPosition;
      APieceTable: TdxPieceTable); 
    destructor Destroy; override;
    procedure ForceUpdateForEmptySelection; virtual; 
    procedure AddToPageSelection(APage: TdxPage; ATarget: TdxPageSelectionLayoutsCollection); virtual; 
    procedure Draw(const ASelectionPainter: IdxSelectionPainter); 
    function Update: Boolean; 
    function HitTest(ALogPosition: TdxDocumentLogPosition; const ALogicalPoint: TPoint): Boolean; 

    property HotZones: TdxHotZoneCollection read FHotZones; 
    property UpdatedSuccessfully: Boolean read FUpdatedSuccessfully; 
    property Start: TdxDocumentLayoutPosition read FStart; 
    property &End: TdxDocumentLayoutPosition read FEnd; 
  end;

  { TdxSelectionLayout }

  TdxSelectionLayout = class 
  private
    FView: TdxRichEditView; 
    FPreferredPageIndex: Integer; 
    FDocumentSelectionLayouts: TObjectList<TdxDocumentSelectionLayout>;
    function GetFirstDocumentSelectionLayout: TdxDocumentSelectionLayout;
    function GetDocumentLayout: TdxDocumentLayout;
    function GetPreferredPageIndex: Integer;
    procedure SetPreferredPageIndex(const Value: Integer);
    function GetStartLayoutPosition: TdxDocumentLayoutPosition;
    function GetEndLayoutPosition: TdxDocumentLayoutPosition;
    function GetLastDocumentSelectionLayout: TdxDocumentSelectionLayout; 
  protected
    function CreateDocumentSelectionLayout(AStart, AEnd: TdxDocumentLayoutPosition): TdxDocumentSelectionLayout; virtual; 

    property FirstDocumentSelectionLayout: TdxDocumentSelectionLayout read GetFirstDocumentSelectionLayout; 
    property DocumentLayout: TdxDocumentLayout read GetDocumentLayout; 
  public
    constructor Create(AView: TdxRichEditView; APreferredPageIndex: Integer); 
    destructor Destroy; override;

    function CalculateHotZone(AResult: TdxRichEditHitTestResult; AView: TdxRichEditView): TdxHotZone; virtual; 
    procedure Invalidate; virtual; 
    function IsSelectionStartFromBeginRow: Boolean; virtual; 
    procedure Update; virtual; 
    function CreateDocumentLayoutPosition(ALogPosition: TdxDocumentLogPosition): TdxDocumentLayoutPosition; virtual; 
    function HitTest(ALogPosition: TdxDocumentLogPosition; ALogicalPoint: TPoint): Boolean; 
    function GetPageSelection(APage: TdxPage): TdxPageSelectionLayoutsCollection; virtual; 

    property PreferredPageIndex: Integer read GetPreferredPageIndex write SetPreferredPageIndex; 
    property StartLayoutPosition: TdxDocumentLayoutPosition read GetStartLayoutPosition; 
    property EndLayoutPosition: TdxDocumentLayoutPosition read GetEndLayoutPosition; 
    property LastDocumentSelectionLayout: TdxDocumentSelectionLayout read GetLastDocumentSelectionLayout; 
    property View: TdxRichEditView read FView; 
  end;

  { TdxBackgroundFormatterAction }

  TdxBackgroundFormatterAction = (
    Shutdown,               
    PerformSecondaryLayout, 
    PerformPrimaryLayout,   
    None                    
  );

  { TdxBackgroundFormatterThread }

  TdxBackgroundFormatterActions = array[TdxBackgroundFormatterAction] of TSimpleEvent;

  TdxBackgroundFormatterThread = class(TThread)
  private type
    TWaitHandles = array[TdxBackgroundFormatterAction] of THandle;
  private
    FFormatter: TdxBackgroundFormatter;
    function GetContinueLayoutEvent: TSimpleEvent;
    function GetWaitHandles: TWaitHandles;
    function WaitForContinueLayoutAllowed: Boolean;
  protected
    function WorkerBody: Boolean;
    procedure Execute; override;
  public
    constructor Create(AFormatter: TdxBackgroundFormatter);
  end;

  TdxPredicate<T> = function (const AItem: T): Boolean of object;

  { TdxBackgroundFormatter }

  TdxBackgroundFormatter = class 
  const
    Infinite = -1; 
  private
    FWorkerThread: TdxBackgroundFormatterThread;
    FContinueLayout: TSimpleEvent;
    FSecondaryLayoutComplete: TSimpleEvent;
    FCommands: TdxBackgroundFormatterActions;
    FCurrentPosition: TdxDocumentModelPosition;
    FDocumentFormatter: TdxDocumentFormatter;
    FSecondaryLayoutStart: TdxDocumentModelPosition; 
    FSecondaryLayoutEnd: TdxDocumentModelPosition; 
    FSecondaryFormatter: TdxParagraphFinalFormatter; 
    FPerformPrimaryLayoutUntil: TdxPredicate<TdxDocumentModelPosition>; 
    FPrimaryLayoutComplete: Boolean; 
    FController: TdxDocumentFormattingController; 
    FResetSecondaryLayoutFromPage: Integer; 
    FDocumentBeginUpdateCount: Integer; 
    FThreadSuspendCount: Integer; 

    FPredicatePos: TdxDocumentModelPosition;
    function PredicateDocumentModelPosition(const ACurrentFormatterPosition: TdxDocumentModelPosition): Boolean;

    function GetDocumentLayout: TdxDocumentLayout;
    function GetDocumentModel: TdxDocumentModel;
    function GetPieceTable: TdxPieceTable;

    procedure CheckExecutedAtWorkerThread; inline;
    procedure CheckExecutedAtUIThread; inline;
    function HandleCommand(ACommandIndex: Integer): Boolean; 
    function IsTableFormattingComplete(const ANextPosition: TdxDocumentModelPosition): Boolean; 
    procedure Restart(const AFrom, ATo, ASecondaryFrom: TdxDocumentModelPosition); 
    procedure UpdateSecondaryPositions(const AFrom, ATo: TdxDocumentModelPosition); 
    procedure RefreshSecondaryLayout(AFirstPageIndex: Integer); 
  protected
    function CreateParagraphFinalFormatter(ADocumentLayout: TdxDocumentLayout): TdxParagraphFinalFormatter; virtual; 

    procedure InitializeWorkThread;
    procedure DoneWorkThread;

    procedure PerformSecondaryLayout; virtual; 
    procedure PerformSecondaryLayoutCore; virtual; 
    procedure PerformPageSecondaryFormatting(APage: TdxPage); virtual; 
    procedure PerformPrimaryLayout; virtual; 
    procedure PerformPrimaryLayoutCore; virtual; 
    function IsPrimaryLayoutComplete(const ACurrentFormatterPosition: TdxDocumentModelPosition): Boolean; virtual; 
    function ShouldResetPrimaryLayout(const AFrom, ATo: TdxDocumentModelPosition): Boolean; 
    function ShouldResetSecondaryLayout(const AFrom, ATo: TdxDocumentModelPosition): Boolean; virtual; 
    procedure ResetSecondaryLayout; virtual; 
    procedure ResetPrimaryLayout; overload; virtual; 
    procedure ResetPrimaryLayout(const AFrom, ATo: TdxDocumentModelPosition); overload; virtual; 

    property ContinueLayout: TSimpleEvent read FContinueLayout;
    property SecondaryLayoutComplete: TSimpleEvent read FSecondaryLayoutComplete;
    property Commands: TdxBackgroundFormatterActions read FCommands;
    property DocumentModel: TdxDocumentModel read GetDocumentModel; 
    property PieceTable: TdxPieceTable read GetPieceTable; 
  public
    constructor Create(AController: TdxDocumentFormattingController); 
    destructor Destroy; override;

    procedure Start; 

    procedure BeginDocumentRendering(APerformPrimaryLayoutUntil: TdxPredicate<TdxDocumentModelPosition>); 
    procedure EndDocumentRendering; 
    procedure BeginDocumentUpdate; 
    procedure EndDocumentUpdate; 
    procedure WaitForPrimaryLayoutReachesLogPosition(ALogPosition: TdxDocumentLogPosition); 
    procedure WaitForPrimaryLayoutReachesPreferredPage(APreferredPageIndex: Integer); 
    procedure WaitForPagePrimaryLayoutComplete(APage: TdxPage); 
    procedure WaitForSecondaryLayoutReachesPosition(const APos: TdxDocumentModelPosition); 
    procedure NotifyDocumentChanged(const AFrom, ATo: TdxDocumentModelPosition; ADocumentLayoutResetType: TdxDocumentLayoutResetType); 

    function SuspendWorkerThread: Boolean; virtual; 
    procedure ResumeWorkerThread; virtual; 

    procedure OnNewMeasurementAndDrawingStrategyChanged; virtual; 
    procedure ResetSecondaryFormattingForPage(APage: TdxPage; APageIndex: Integer); 

    property DocumentFormatter: TdxDocumentFormatter read FDocumentFormatter; 
    property DocumentLayout: TdxDocumentLayout read GetDocumentLayout; 
    property SecondaryLayoutStart: TdxDocumentModelPosition read FSecondaryLayoutStart; 
    property SecondaryLayoutEnd: TdxDocumentModelPosition read FSecondaryLayoutEnd; 
    property WorkerThread: TdxBackgroundFormatterThread read FWorkerThread;
  end;

  { TdxRichEditViewInfo }

  TdxRichEditViewInfo = class
  strict private
    FPageViewInfos: TdxPageViewInfoCollection; 
    FOwnedPageViewInfos: TdxOwnedPageViewInfoList;
    FOwnedRows: TdxOwnedRowInfoList;
  public
    constructor Create(ADocumentLayout: TdxDocumentLayout);
    destructor Destroy; override;
    procedure Clear;

    property PageViewInfos: TdxPageViewInfoCollection read FPageViewInfos; 
    property OwnedPageViewInfos: TdxOwnedPageViewInfoList read FOwnedPageViewInfos;
    property OwnedRows: TdxOwnedRowInfoList read FOwnedRows;
  end;

  { TdxRichEditView }

  TdxRichEditView = class abstract (TPersistent)
  const
    MinZoomFactor     = 0.0001;
    MaxZoomFactor     = 10000.0;
    DefaultZoomFactor = 1.0;
    DefaultMinWidth   = 5;
    DefaultMinHeight  = 5;
  private
    FBackColor: TColor;
    FControl: IdxRichEditControl;
    FBounds: TRect;
    FZoomFactor: Single;
    FAllowDisplayLineNumbers: Boolean;
    FDocumentLayout: TdxDocumentLayout;
    FFormattingController: TdxDocumentFormattingController;
    FVerticalScrollController: TdxRichEditViewVerticalScrollController;
    FHorizontalScrollController: TdxRichEditViewHorizontalScrollController;
    FPageViewInfoGenerator: TdxPageViewInfoGenerator;
    FSelectionLayout: TdxSelectionLayout;
    FSuspendFormattingCount: Integer;
    FHoverLayout: IdxHoverLayoutItem;
    FCaretPosition: TdxCaretPosition;
    FTableController: TdxTableViewInfoController;
    FViewInfo: TdxRichEditViewInfo; 
    FPadding: TdxRichEditControlPadding;
    FPageViewInfoGenerationComplete: Boolean;
    FPageViewInfoGenerationIsAboutToComplete: Boolean;
    FOnBackColorChanged: TdxMulticastMethod<TdxEventHandler>;
    FOnZoomChanging: TdxMulticastMethod<TdxEventHandler>;
    FOnZoomChanged: TdxMulticastMethod<TdxEventHandler>;
    function IsPaddingStored: Boolean;
    function IsPrimaryFormattingCompleteForVisibleHeight2(const ACurrentFormatterPosition: TdxDocumentModelPosition): Boolean;
    procedure PaddingChanged(Sender: TObject);
  private
    function EnsurePositionNotBeforeSectionBreakAfterParagraphBreak(const AModelPosition: TdxDocumentModelPosition): TdxDocumentModelPosition;
    function IsActivePieceTableFloatingObjectBox(const ABox: TdxFloatingObjectBox): Boolean; 
    function IsActiveFloatingObjectTextBox(const ABox: TdxFloatingObjectBox): Boolean; 
    function IsFloatingObjectHit(ABox: TdxFloatingObjectBox; APoint: TPoint): Boolean; 
    function GetDocumentModel: TdxDocumentModel;
    procedure SetBackColor(const Value: TColor);
    procedure SetBounds(const Value: TRect);
    procedure SetZoomFactor(Value: Single);
    procedure SetAllowDisplayLineNumbers(const Value: Boolean);
    function GetFixedLeftTextBorderOffset: Integer;
    function GetFormatter: TdxBackgroundFormatter;
    procedure SetPageViewInfoGenerationComplete(const Value: Boolean);
    function GetPageViewInfos: TdxPageViewInfoCollection; inline;
    procedure SetPadding(Value: TdxRichEditControlPadding);
  protected
    function CreatePadding: TdxRichEditControlPadding; virtual;
    function GetActualPadding: TRect; virtual;
    procedure OnViewPaddingChanged; virtual; 

    property PageViewInfoGenerationIsAboutToComplete: Boolean read FPageViewInfoGenerationIsAboutToComplete write FPageViewInfoGenerationIsAboutToComplete; 
    property PageViewInfoGenerationComplete: Boolean read FPageViewInfoGenerationComplete write SetPageViewInfoGenerationComplete; 
    property ViewInfo: TdxRichEditViewInfo read FViewInfo; 
    procedure SetAllowDisplayLineNumbersCore(Value: Boolean); 
    function CreateDocumentLayout(ADocumentModel: TdxDocumentModel; AMeasurerProvider: IdxBoxMeasurerProvider): TdxDocumentLayout; virtual; 
    function CreateDocumentFormattingController: TdxDocumentFormattingController; virtual; abstract; 
    function CreatePageViewInfoGenerator: TdxPageViewInfoGenerator; virtual; abstract; 
    procedure OnZoomFactorChanging; virtual; 
    procedure OnZoomFactorChanged(AOldValue, ANewValue: Single); virtual; 
    procedure PerformZoomFactorChanged; virtual; 
    procedure OnZoomFactorChangedCore; virtual; 
    procedure OnSelectionChanged; virtual; 
    procedure ShowCaret; virtual; 
    procedure HideCaret; virtual; 
    function CalculateDocumentLayoutResetType(AChanges: TdxDocumentModelDeferredChanges): TdxDocumentLayoutResetType; virtual; 
    function CalculateResetFromPosition(APieceTable: TdxPieceTable; AChanges: TdxDocumentModelDeferredChanges; AResetType: TdxDocumentLayoutResetType): TdxDocumentModelPosition; virtual; 
    function EnsureTopLevelParagraph(const APos: TdxDocumentModelPosition): TdxDocumentModelPosition; virtual; 
    procedure ValidateSelectionInterval; virtual; 
    function GetVisibleSelectionPosition: TdxDocumentLogPosition; virtual; 
    function GetVisibleSelectionStart: TdxDocumentLogPosition; 
    function GetVisibleSelectionEnd: TdxDocumentLogPosition; 
    procedure OnResetFormattingFromCurrentArea(ASender: TObject; E: TdxEventArgs); virtual; 
    procedure OnResetSecondaryFormattingForPage(ASender: TObject; E: TdxResetSecondaryFormattingForPageArgs); virtual; 
    procedure OnPageFormattingStarted(ASender: TObject; E: TdxPageFormattingCompleteEventArgs); virtual; 
    procedure OnPageFormattingComplete(ASender: TObject; E: TdxPageFormattingCompleteEventArgs); virtual; 
    procedure SetOptimalHorizontalScrollbarPosition; virtual; 
    procedure SetOptimalHorizontalScrollbarPositionCore; virtual; 
    function CalculateOptimalHorizontalScrollbarPosition(APageViewInfo: TdxPageViewInfo): Integer; virtual; 
    function ChoosePageViewInfoForOptimalHorizontalScrollbarPosition: TdxPageViewInfo; virtual; 
    procedure SubscribeDocumentLayoutEvents; virtual; 
    procedure UnsubscribeDocumentLayoutEvents; virtual; 
    procedure BeforeCreateDetailRowHandler(ASender: TObject); virtual; 
    procedure AfterCreateDetailRowHandler(ASender: TObject); virtual; 
    procedure ScrollToPage(APageIndex: Integer); virtual; 
    function LookupPageViewInfoByPage(APage: TdxPage): TdxPageViewInfo; virtual; 
    procedure EnsureFormattingCompleteForLogPosition(ALogPosition: TdxDocumentLogPosition); virtual; 
    procedure EnsureFormattingCompleteForPreferredPage(APreferredPageIndex: Integer); virtual; 
    procedure EnsurePageSecondaryFormattingComplete(APage: TdxPage); virtual; 
    function CreateTransformMatrix(const AClientBounds: TRect): TdxTransformMatrix; virtual; 
    function GetPageViewInfoFromPoint(const APoint: TPoint; AStrictSearch: Boolean): TdxPageViewInfo; virtual; 
    function GetPageViewInfoRowFromPoint(const APoint: TPoint; AStrictSearch: Boolean): TdxPageViewInfoRow; virtual; 
    procedure HitTest(APage: TdxPage; ARequest: TdxRichEditHitTestRequest; AResult: TdxRichEditHitTestResult); virtual;
    function PerformStrictPageViewInfoHitTest(APageViewInfo: TdxPageViewInfo; const APt: TPoint): Boolean; virtual; 
    procedure PerformResize(const ABounds: TRect); virtual; 
    procedure OnResizeCore; virtual; 
    procedure SubscribePageFormattingComplete; virtual; 
    procedure UnsubscribePageFormattingComplete; virtual; 
    procedure SuspendFormatting; virtual; 
    procedure ResumeFormatting; virtual; 
    function HitTestByPhysicalPoint(const APt: TPoint; ADetailsLevel: TdxDocumentLayoutDetailsLevel): TRect; virtual; 
    function GetPhysicalBounds(APageViewInfo: TdxPageViewInfo; const ABounds: TRect): TRect; virtual; 
    function ExpandClipBoundsToPaddings(const AClipBounds: TRect): TRect; virtual; 
    function CalculateVisiblePageBounds(const APageBounds: TRect; APageViewInfo: TdxPageViewInfo): TRect; virtual; 
    procedure ResetBackColor; virtual;

    function GetActualBackColor: TColor; virtual;
    function GetMinWidth: Integer; virtual;
    function GetMinHeight: Integer; virtual;

    property FixedLeftTextBorderOffset: Integer read GetFixedLeftTextBorderOffset; 
    property Formatter: TdxBackgroundFormatter read GetFormatter; 
    property HoverLayout: IdxHoverLayoutItem read FHoverLayout write FHoverLayout; 
    property TableController: TdxTableViewInfoController read FTableController write FTableController; 
    property MinHeight: Integer read GetMinHeight;
    property MinWidth: Integer read GetMinWidth;
  public
    constructor Create(AControl: IdxRichEditControl); virtual; 
    destructor Destroy; override;

    class function EnsurePositionVisibleWhenHiddenTextNotShown(ADocumentModel: TdxDocumentModel; const AModelPosition: TdxDocumentModelPosition): TdxDocumentModelPosition; 

    procedure CheckExecutedAtUIThread; inline;
    procedure CheckExecutedAtBackgroundThread; inline;

    function CreateLogicalPoint(const AClientBounds: TRect; const APoint: TPoint): TPoint; virtual; 
    function CreateDocumentLayoutExporter(APainter: TdxPainter; AAdapter: TdxGraphicsDocumentLayoutExporterAdapter;
      APageViewInfo: TdxPageViewInfo; ABounds: TRect): TdxDocumentLayoutExporter; virtual; abstract; 
    function DefaultHitTestPageAccuracy: TdxHitTestAccuracy; virtual;

    procedure Activate(const AViewBounds: TRect); virtual; 
    procedure BeginDocumentRendering; virtual; 
    procedure CorrectZoomFactor; virtual; 
    function CalcBestSize(AFixedWidth: Boolean): TSize; virtual; abstract;
    function CalculateFirstInvisiblePageIndexBackward: Integer; virtual; 
    function CalculateFirstInvisiblePageIndexForward: Integer; virtual; 
    function CalculatePageContentClipBounds(APage: TdxPageViewInfo): TRect; virtual; 
    function CreateHitTestCalculator(ARequest: TdxRichEditHitTestRequest; AResult: TdxRichEditHitTestResult): TdxBoxHitTestCalculator; 
    function CreatePhysicalPoint(APageViewInfo: TdxPageViewInfo; const APoint: TPoint): TPoint; virtual; 
    procedure Deactivate; virtual; 
    procedure EndDocumentRendering; virtual; 
    procedure EnforceFormattingCompleteForVisibleArea; 
    procedure EnsureFormattingCompleteForSelection; virtual; 
    procedure EnsureCaretVisibleOnResize; virtual;
    procedure OnActivePieceTableChanged; 
    procedure OnLayoutUnitChanging; virtual; 
    procedure OnLayoutUnitChanged; virtual; 
    procedure EnsureCaretVisible; virtual; 
    procedure NotifyDocumentChanged(APieceTable: TdxPieceTable; AChanges: TdxDocumentModelDeferredChanges; ADebugSuppressControllerReset: Boolean); virtual; 
    procedure OnEndDocumentUpdate; virtual; 
    procedure OnResize(const ABounds: TRect; AEnsureCaretVisibleOnResize: Boolean); virtual;
    function CreateLogicalRectangle(APageViewInfo: TdxPageViewInfo; const ABounds: TRect): TRect; virtual; 
    function CreatePhysicalRectangle(APageViewInfo: TdxPageViewInfo; const ABounds: TRect): TRect; overload; virtual; 
    function CreatePhysicalRectangle(const APageViewInfoClientBounds, ABounds: TRect): TRect; overload; virtual; 
    procedure ResetPages(AStrategy: TdxPageGenerationStrategyType); virtual; 
    procedure GeneratePages; virtual; 

    procedure OnBeginDocumentUpdate; virtual; 
    procedure OnVerticalScroll; virtual; 
    procedure OnHorizontalScroll; virtual; 
    procedure UpdateHorizontalScrollbar; virtual; 

    function CreatePhysicalRectangleFast(APageViewInfo: TdxPageViewInfo; const ABounds: TRect): TRect; virtual; 
    function CalculateNearestCharacterHitTest(const APoint: TPoint): TdxRichEditHitTestResult; virtual; 
    function CalculateNearestPageHitTest(const APoint: TPoint; AStrictHitIntoPageBounds: Boolean): TdxRichEditHitTestResult; virtual; 
    function CalculateFloatingObjectHitTest(APage: TdxPage; const APoint: TPoint; APredicate: TdxPredicate<TdxFloatingObjectBox>): TdxFloatingObjectBox; virtual; 
    function CalculateFloatingObjectHitTestCore(const APoint: TPoint; AList: TList<TdxFloatingObjectBox>; APredicate: TdxPredicate<TdxFloatingObjectBox>): TdxFloatingObjectBox; virtual; 
    function CalculateHitTest(const APoint: TPoint; ADetailsLevel: TdxDocumentLayoutDetailsLevel): TdxRichEditHitTestResult; virtual; 
    function HitTestCore(ARequest: TdxRichEditHitTestRequest; AStrictHitIntoPageBounds: Boolean): TdxRichEditHitTestResult; virtual; 


		procedure RaiseZoomChanging;
		procedure RaiseZoomChanged; virtual;
    function GetCursorBounds: TRect; virtual; 
    function GetHyperlinkField(AHitTestResult: TdxRichEditHitTestResult): TdxField; 
    procedure Visit(AVisitor: IdxRichEditViewVisitor); virtual; abstract; 

    property VerticalScrollController: TdxRichEditViewVerticalScrollController read FVerticalScrollController; 
    property HorizontalScrollController: TdxRichEditViewHorizontalScrollController read FHorizontalScrollController; 

    property ActualBackColor: TColor read GetActualBackColor;
    property ActualPadding: TRect read GetActualPadding;
    property CaretPosition: TdxCaretPosition read FCaretPosition write FCaretPosition;
    property Control: IdxRichEditControl read FControl; 
    property DocumentLayout: TdxDocumentLayout read FDocumentLayout; 
    property DocumentModel: TdxDocumentModel read GetDocumentModel;
    property FormattingController: TdxDocumentFormattingController read FFormattingController; 
    property PageViewInfoGenerator: TdxPageViewInfoGenerator read FPageViewInfoGenerator; 
    property PageViewInfos: TdxPageViewInfoCollection read GetPageViewInfos; 
    property Bounds: TRect read FBounds write SetBounds;
    property ZoomFactor: Single read FZoomFactor write SetZoomFactor;
    property AllowDisplayLineNumbers: Boolean read FAllowDisplayLineNumbers write SetAllowDisplayLineNumbers;
    property Padding: TdxRichEditControlPadding read FPadding write SetPadding stored IsPaddingStored;
    property BackColor: TColor read FBackColor write SetBackColor default clWindow;
    property BackColorChanged: TdxMulticastMethod<TdxEventHandler> read FOnBackColorChanged;
    property SelectionLayout: TdxSelectionLayout read  FSelectionLayout write FSelectionLayout;
    property ZoomChanging: TdxMulticastMethod<TdxEventHandler> read FOnZoomChanging;
    property ZoomChanged: TdxMulticastMethod<TdxEventHandler> read FOnZoomChanged;
  end;
  TdxRichEditViewClass = class of TdxRichEditView;

  { TdxRichEditCustomViews }

  TdxRichEditCustomViewRepository = class(TcxOwnedPersistent)
  private
    FRichEditControl: IdxRichEditControl;
    FViews: TdxFastObjectList;
    function GetView(Index: Integer): TdxRichEditView;
    function GetViewCount: Integer;
  protected
    procedure AddView(AView: TdxRichEditView);
    procedure CreateViews; virtual; abstract;

    property RichEditControl: IdxRichEditControl read FRichEditControl;
  public
    constructor Create(ARichEditControl: IdxRichEditControl); reintroduce; virtual;
    destructor Destroy; override;

    property ViewCount: Integer read GetViewCount;
    property Views[Index: Integer]: TdxRichEditView read GetView; default;
  end;

  { TdxScrollBarAdapter }

  TdxScrollBarAdapter = class abstract (TcxIUnknownObject, IdxBatchUpdateHandler, IdxBatchUpdateable)
  private
    FScrollBar: IdxOfficeScrollbar;
    FBatchUpdateHelper: TdxBatchUpdateHelper;
    FAdapter: IdxPlatformSpecificScrollBarAdapter;
    FFactor: Double; 
    FMinimum: Int64; 
    FMaximum: Int64; 
    FValue: Int64;   
    FLargeChange: Int64; 
    FEnabled: Boolean; 
    FOnScroll: TdxMulticastMethod<TdxScrollEventHandler>;
    procedure SetMinimum(const Value: Int64);
    procedure SetMaximum(const Value: Int64);
    procedure SetValue(const Value: Int64);
    procedure SetLargeChange(const Value: Int64);
    procedure SetEnabled(const Value: Boolean);
    function GetBatchUpdateHelper: TdxBatchUpdateHelper;
    function GetIsUpdateLocked: Boolean;
    function GetScroll: PdxScrollEvents;
  protected
    function GetDeferredScrollBarUpdate: Boolean; virtual; abstract;
    function GetSynchronized: Boolean; virtual; abstract;
    procedure SetSynchronized(AValue: Boolean); virtual; abstract;

    procedure OnLastEndUpdateCore; virtual;
    procedure ValidateValues; virtual;
    procedure ValidateValuesCore; virtual;
    procedure SubscribeScrollbarEvents; virtual;
    procedure UnsubscribeScrollbarEvents; virtual;
    function ShouldSynchronize: Boolean; virtual;
    procedure OnScroll(ASender: TObject; E: TdxScrollEventArgs); virtual;
    procedure ApplyValuesToScrollBarCore; virtual;

    procedure OnFirstBeginUpdate; 
    procedure OnBeginUpdate;      
    procedure OnEndUpdate;        
    procedure OnLastEndUpdate;    
    procedure OnCancelUpdate;     
    procedure OnLastCancelUpdate; 

    property DeferredScrollBarUpdate: Boolean read GetDeferredScrollBarUpdate;
    property Synchronized: Boolean read GetSynchronized write SetSynchronized;
    property Adapter: IdxPlatformSpecificScrollBarAdapter read FAdapter;
  public
    constructor Create(const AScrollBar: IdxOfficeScrollbar; AAdapter: IdxPlatformSpecificScrollBarAdapter);
    destructor Destroy; override;
    procedure RaiseScroll(AArgs: TdxScrollEventArgs); virtual;

    procedure BeginUpdate;
    procedure EndUpdate;
    procedure CancelUpdate;

    procedure Activate; virtual;
    procedure Deactivate; virtual;
    function SynchronizeScrollBarAvoidJump: Boolean; virtual;
    function EnsureSynchronizedCore: Boolean;
    procedure EnsureSynchronized; virtual;
    procedure ApplyValuesToScrollBar; virtual;
    procedure RefreshValuesFromScrollBar; virtual;
    function CreateEmulatedScrollEventArgs(AEventType: TdxScrollEventType): TdxScrollEventArgs; virtual;
    function GetRawScrollBarValue: Integer;
    function SetRawScrollBarValue(AValue: Integer): Boolean;
    function GetPageUpRawScrollBarValue: Integer; virtual;
    function GetPageDownRawScrollBarValue: Integer; virtual;

    property Factor: Double read FFactor write FFactor;
    property Minimum: Int64 read FMinimum write SetMinimum;
    property Maximum: Int64 read FMaximum write SetMaximum;
    property Value: Int64 read FValue write SetValue;
    property LargeChange: Int64 read FLargeChange write SetLargeChange;
    property Enabled: Boolean read FEnabled write SetEnabled;
		property ScrollBar: IdxOfficeScrollbar read FScrollBar;
    property Scroll: PdxScrollEvents read GetScroll;
    property BatchUpdateHelper: TdxBatchUpdateHelper read GetBatchUpdateHelper;
    property IsUpdateLocked: Boolean read GetIsUpdateLocked;
  end;

  { TdxVerticalScrollBarAdapter }

  TdxVerticalScrollBarAdapter = class(TdxScrollBarAdapter)
  public
    FSynchronized: Boolean;
  protected
    function GetDeferredScrollBarUpdate: Boolean; override;
    function GetSynchronized: Boolean; override;
    procedure SetSynchronized(AValue: Boolean); override;
  end;

  { TdxHorizontalScrollBarAdapter }

  TdxHorizontalScrollBarAdapter = class(TdxScrollBarAdapter)
  protected
    function GetDeferredScrollBarUpdate: Boolean; override;
    function GetSynchronized: Boolean; override;
    procedure SetSynchronized(AValue: Boolean); override;
  end;

  { TdxOfficeScrollControllerBase }

  TdxOfficeScrollControllerBase = class abstract
  private
    FScrollBarAdapter: TdxScrollBarAdapter;
    function GetSupportsDeferredUpdate: Boolean;
  protected
    function GetScrollBar: IdxOfficeScrollbar; virtual; abstract;
    procedure Initialize; virtual;
    procedure Activate; virtual;
    procedure Deactivate; virtual;
    function CreateScrollBarAdapter: TdxScrollBarAdapter; virtual; abstract;
    procedure SubscribeScrollBarAdapterEvents; virtual;
    procedure UnsubscribeScrollBarAdapterEvents; virtual;
    procedure EmulateScroll(AEventType: TdxScrollEventType);
    procedure OnScroll(ASender: TObject; E: TdxPlatformIndependentScrollEventArgs); virtual; abstract;
    procedure SynchronizeScrollbar; virtual;
    procedure UpdateScrollBarAdapter; virtual; abstract;

    property SupportsDeferredUpdate: Boolean read GetSupportsDeferredUpdate;
    property ScrollBar: IdxOfficeScrollbar read GetScrollBar;
  public
    destructor Destroy; override;
    procedure UpdateScrollBar; virtual;
    function IsScrollPossible: Boolean; virtual;

    property ScrollBarAdapter: TdxScrollBarAdapter read FScrollBarAdapter; 
  end;

  { TdxRichEditViewScrollControllerBase }

  TdxRichEditViewScrollControllerBase = class abstract (TdxOfficeScrollControllerBase)
  private
    FView: TdxRichEditView;
  public
    constructor Create(AView: TdxRichEditView);
    procedure UpdateScrollBar; override;

    property View: TdxRichEditView read FView;
  end;

  { TdxRichEditViewHorizontalScrollController }

  TdxRichEditViewHorizontalScrollController = class abstract (TdxRichEditViewScrollControllerBase)
  protected
    function GetScrollBar: IdxOfficeScrollbar; override;

    function CreateScrollBarAdapter: TdxScrollBarAdapter; override;
    procedure OnScroll(ASender: TObject; E: TdxPlatformIndependentScrollEventArgs); override;
    procedure OnScrollCore(E: TdxPlatformIndependentScrollEventArgs); virtual; 
    procedure UpdateScrollBarAdapter; override;
  public
    function GetLeftInvisibleWidth: Int64;
    function GetPhysicalLeftInvisibleWidth: Integer;
    procedure ScrollByLeftInvisibleWidthDelta(ALeftInvisibleWidthDelta: Int64); virtual;
    procedure ScrollToAbsolutePosition(AValue: Int64); virtual;
  end;

  { TdxRichEditViewVerticalScrollController }

  TdxRichEditViewVerticalScrollController = class abstract (TdxRichEditViewScrollControllerBase)
  protected
    function CreateScrollBarAdapter: TdxScrollBarAdapter; override;
    function IsScrollTypeValid(E: TdxPlatformIndependentScrollEventArgs): Boolean; virtual; {abstract;} 
    procedure OnScroll(ASender: TObject; E: TdxPlatformIndependentScrollEventArgs); override;
    function CalculateScrollDelta(E: TdxPlatformIndependentScrollEventArgs): Integer; virtual; {abstract;} 
    procedure ApplyNewScrollValue(AValue: Integer); virtual; {abstract;} 
    procedure ApplyNewScrollValueToScrollEventArgs(E: TdxPlatformIndependentScrollEventArgs; AValue: Integer); virtual; {abstract;} 
    function UpdatePageNumberOnScroll(E: TdxPlatformIndependentScrollEventArgs): Boolean; virtual; {abstract;} 
    procedure UpdateScrollBarAdapter; override;
    function GetScrollBar: IdxOfficeScrollbar; override;
    function GetTopInvisibleHeight: Int64;
  public
    procedure ScrollByTopInvisibleHeightDelta(ATopInvisibleHeightDelta: Int64); virtual;
    procedure ScrollToAbsolutePosition(AValue: Int64); virtual;
    function ScrollPageUp: Boolean; virtual;
    function ScrollPageDown: Boolean; virtual;
    function ScrollLineUp: Integer; virtual;
    function ScrollLineDown: Integer; virtual;
    function ScrollLineUpDown(APhysicalOffset: Integer): Integer; virtual;
  end;

  { TdxScrollByPhysicalHeightCalculator }

  TdxScrollByPhysicalHeightCalculator = class
  strict private
    FView: TdxRichEditView;
  protected
    function CalculateScrollDeltaForInvisibleRows(APhysicalVerticalOffset: Integer): Int64;
    function CalculateScrollDeltaForVisibleRows(APhysicalVerticalOffset: Integer; out ADelta: Int64): Boolean; virtual; abstract;
    function LookupIntersectingRowIndexInInvisibleRows(AGenerator: TdxInvisiblePageRowsGenerator; Y: Integer): Integer;
    function CalculateFirstInvisiblePageIndex: Integer; virtual; abstract;
    function CalculateFirstInvalidPageIndex: Integer; virtual; abstract;
    function CalculateInvisiblePhysicalVerticalOffset(APhysicalVerticalOffset: Integer): Integer; virtual; abstract;
    function CalculateVisibleRowsScrollDelta: Int64; virtual; abstract;
    function GetDefaultScrollDeltaForInvisibleRows: Int64; virtual; abstract;
    function CalculateRowsTotalVisibleHeight(AGenerator: TdxPageViewInfoGeneratorBase; ALastRowIndex: Integer; ABottomY: Integer): Int64;
  public
    constructor Create(AView: TdxRichEditView);
    function CalculateScrollDelta(APhysicalVerticalOffset: Integer): Int64; virtual;

    property View: TdxRichEditView read FView;
  end;

  { TdxScrollUpByPhysicalHeightCalculator }

  TdxScrollUpByPhysicalHeightCalculator = class(TdxScrollByPhysicalHeightCalculator)
  protected
    function CalculateScrollDeltaForVisibleRows(APhysicalVerticalOffset: Integer; out ADelta: Int64): Boolean; override;
    function CalculateFirstInvisiblePageIndex: Integer; override;
    function CalculateFirstInvalidPageIndex: Integer; override;
    function CalculateInvisiblePhysicalVerticalOffset(APhysicalVerticalOffset: Integer): Integer; override;
    function CalculateVisibleRowsScrollDelta: Int64; override;
    function GetDefaultScrollDeltaForInvisibleRows: Int64; override;
  end;

  { TdxScrollDownByPhysicalHeightCalculator }

  TdxScrollDownByPhysicalHeightCalculator = class(TdxScrollByPhysicalHeightCalculator)
  protected
    function CalculateScrollDeltaForVisibleRows(APhysicalVerticalOffset: Integer; out ADelta: Int64): Boolean; override;
    function CalculateFirstInvisiblePageIndex: Integer; override;
    function CalculateFirstInvalidPageIndex: Integer; override;
    function CalculateInvisiblePhysicalVerticalOffset(APhysicalVerticalOffset: Integer): Integer; override;
    function CalculateVisibleRowsScrollDelta: Int64; override;
    function GetDefaultScrollDeltaForInvisibleRows: Int64; override;
    function LookupIntersectingRowIndex(Y: Integer): Integer; virtual;
  end;

  { TdxNonPrintViewPageControllerBase }

  TdxNonPrintViewPageControllerBase = class(TdxPageController) 
  protected
    function GetColumnBottom(AColumn: TdxColumn): Integer; virtual; 
  public
    constructor Create(ADocumentLayout: TdxDocumentLayout); 
  end;

  TdxDraftView = class(TdxRichEditView); 
  TdxPrintLayoutView = class(TdxRichEditView); 
  TdxReadingLayoutView = class(TdxRichEditView); 

  { TdxCopySelectionManager }

  TdxTextFragmentOptions = record
    AllowExtendingDocumentRange: Boolean;
    PreserveOriginalNumbering: Boolean;
    class function Create: TdxTextFragmentOptions; static;
  end;
  PdxTextFragmentOptions = ^TdxTextFragmentOptions;

  TdxCopySelectionManager = class
  strict private
    FDocumentServer: IdxInnerControl;
    FFixLastParagraph: Boolean;
    FAllowCopyWholeFieldResult: Boolean;
    FDefaultPropertiesCopyOptions: TdxDefaultPropertiesCopyOptions;
  protected
    function CreateDocumentModel(AParagraphNumerationCopyOptions: TdxParagraphNumerationCopyOptions;
      AFormattingCopyOptions: TdxFormattingCopyOptions; ASourcePieceTable: TdxPieceTable;
      ASelectionRanges: TdxSelectionRangeCollection; ASuppressFieldsUpdate: Boolean = False;
      AGetTextOptions: PdxTextFragmentOptions = nil): TdxDocumentModel;
    function IsLastParagraphRunSelected(APieceTable: TdxPieceTable; ASelectionRanges: TdxSelectionRangeCollection): Boolean;
    procedure SetDataObject(APieceTable: TdxPieceTable; ASelectionCollection: TdxSelectionRangeCollection;
      ADataObject: IdxDataObject);
    function ShouldExtendRange(AInfo: TdxRunInfo; AField: TdxField): Boolean;
    function UpdateSelectionCollection(ASourcePieceTable: TdxPieceTable;
      ASelection: TdxSelectionRangeCollection): TdxSelectionRangeCollection;

    procedure DoBeforeExport(Sender: TObject; E: TdxBeforeExportEventArgs);
    procedure DoCalculateDocumentVariable(Sender: TObject; E: TdxCalculateDocumentVariableEventArgs);

    procedure SubscribeTargetModelEvents(ATargetModel: TdxDocumentModel);
    procedure UnsubscribeTargetModelEvents(ATargetModel: TdxDocumentModel);

    property DocumentServer: IdxInnerControl read FDocumentServer;
  public
    constructor Create(ADocumentServer: IdxInnerControl);

    procedure CopyDocumentRange(APieceTable: TdxPieceTable; ASelectionCollection: TdxSelectionRangeCollection);

    function GetRtfText(APieceTable: TdxPieceTable; ASelectionRanges: TdxSelectionRangeCollection;
      AOptions: TdxRtfDocumentExporterOptions = nil; AForceRaiseBeforeExport: Boolean = False): AnsiString;
    function GetPlainText(APieceTable: TdxPieceTable; ASelectionCollection: TdxSelectionRangeCollection;
      AOptions: TdxPlainTextDocumentExporterOptions = nil; AGetTextOptions: PdxTextFragmentOptions = nil): string;
    function GetSilverlightXamlStream(APieceTable: TdxPieceTable; ASelectionCollection: TdxSelectionRangeCollection): TMemoryStream;
    function GetSuppressStoreImageSizeCollection(APieceTable: TdxPieceTable;
      ASelectionCollection: TdxSelectionRangeCollection): string;

    property AllowCopyWholeFieldResult: Boolean read FAllowCopyWholeFieldResult write FAllowCopyWholeFieldResult;
    property DefaultPropertiesCopyOptions: TdxDefaultPropertiesCopyOptions read FDefaultPropertiesCopyOptions write FDefaultPropertiesCopyOptions;
    property FixLastParagraph: Boolean read FFixLastParagraph write FFixLastParagraph;

  end;

implementation

uses
  RTLConsts, cxLibraryConsts, dxTypeHelpers, Math, Windows, dxCore, dxRichEdit.Control,
  dxRichEdit.Control.Mouse, dxRichEdit.Commands, dxRichEdit.DocumentLayout.UnitConverter,
  dxRichEdit.Control.Cursors, dxRichEdit.DocumentModel.TextRange, dxRichEdit.DocumentModel.Commands,
  dxRichEdit.Commands.Selection, dxRichEdit.DocumentModel.SectionRange;

type
  TdxRichEditViewHelper = class helper for TdxRichEditView
  strict private
    function GetRichEditControl: TdxCustomRichEditControl;
  public
    property RichEditControl: TdxCustomRichEditControl read GetRichEditControl;
  end;

function TdxRichEditViewHelper.GetRichEditControl: TdxCustomRichEditControl;
begin
  Result := TdxCustomRichEditControl(Control.GetRichEditControl);
end;

{ TdxRichEditControlPadding }

constructor TdxRichEditControlPadding.Create(AOwner: TPersistent; const ADefaultValue: TRect);
begin
  inherited Create(AOwner);
  FDefaultValue := ADefaultValue;
  FValue := ADefaultValue;
end;

constructor TdxRichEditControlPadding.Create(AOwner: TPersistent; ADefaultValue: Integer = 0);
begin
  Create(AOwner, TRect.Create(ADefaultValue, ADefaultValue, ADefaultValue, ADefaultValue));
end;

function TdxRichEditControlPadding.IsDefault: Boolean;
begin
  Result := cxRectIsEqual(FDefaultValue, FValue);
end;

procedure TdxRichEditControlPadding.DoAssign(Source: TPersistent);
begin
  inherited DoAssign(Source);
  if Source is TdxRichEditControlPadding then
    Value := TdxRichEditControlPadding(Source).Value;
end;

function TdxRichEditControlPadding.GetProperty(AIndex: Integer): Integer;
begin
  case AIndex of
    1: Result := FValue.Left;
    2: Result := FValue.Top;
    3: Result := FValue.Right;
    4: Result := FValue.Bottom;
  else 
    if (FValue.Left = FValue.Top) and (FValue.Left = FValue.Right) and (FValue.Left = FValue.Bottom) then
      Result := FValue.Left
    else
      Result := -1;
  end;
end;

procedure TdxRichEditControlPadding.SetValue(const Value: TRect);
begin
  if not cxRectIsEqual(Value, FValue) then
  begin
    FValue := Value;
    DoChange;
  end;
end;

procedure TdxRichEditControlPadding.SetProperty(AIndex, AValue: Integer);
begin
  if GetProperty(AIndex) <> AValue then
  begin
    case AIndex of
      1: FValue.Left := AValue;
      2: FValue.Top := AValue;
      3: FValue.Right := AValue;
      4: FValue.Bottom := AValue;
    else 
      if AValue = -1 then
        FValue := FDefaultValue
      else
        FValue := cxRect(AValue, AValue, AValue, AValue);
    end;
    DoChange;
  end;
end;

function TdxRichEditControlPadding.IsEqual(const APadding: TdxRichEditControlPadding): Boolean;
begin
  Result := cxRectIsEqual(Value, APadding.Value);
end;

procedure TdxRichEditControlPadding.Reset;
begin
  Value := FDefaultValue;
end;

function TdxRichEditControlPadding.IsPropertyStored(AIndex: Integer): Boolean;
begin
  case AIndex of
    1: Result := FValue.Left <> FDefaultValue.Left;
    2: Result := FValue.Top <> FDefaultValue.Top;
    3: Result := FValue.Right <> FDefaultValue.Right;
    4: Result := FValue.Bottom <> FDefaultValue.Bottom;
  else 
    Result := (All <> -1) and not IsDefault;
  end;
end;

{ TdxCaretPosition }

constructor TdxCaretPosition.Create(AView: TdxRichEditView; APreferredPageIndex: Integer);
begin
  inherited Create;
  Assert(AView <> nil);
  Assert(APreferredPageIndex >= 0);
  FView := AView;
  FPosition := CreateCaretDocumentLayoutPosition;
  FCaretBoundsPosition := FPosition;
  PreferredPageIndex := APreferredPageIndex;
end;

destructor TdxCaretPosition.Destroy;
begin
  FCaretBoundsPosition := nil;
  FreeAndNil(FPosition);
  FreeAndNil(FInputPosition);
  inherited Destroy;
end;

function TdxCaretPosition.CalculateCaretBounds: TRect;
var
  AWidth: Integer;
begin
  AWidth := View.DocumentModel.LayoutUnitConverter.PixelsToLayoutUnits(2, DocumentModel.DpiX);
  if UsePreviousBoxBounds then
  begin
    Result := FPosition.Character.TightBounds;
    Result.MoveToLeft(Min(Result.Left, FCaretBoundsPosition.Row.Bounds.Right - AWidth));
    Result.Intersect(FPosition.Row.Bounds);
  end
  else
  begin
    if (FCaretBoundsPosition = FPosition) or (FCaretBoundsPosition.Row <> FPosition.Row) then
    begin
      Result := FPosition.Character.TightBounds;
      Result.MoveToLeft(Min(Result.Left, FCaretBoundsPosition.Row.Bounds.Right - AWidth));
      Result.Intersect(FPosition.Row.Bounds);
    end
    else
    begin
      Result := FCaretBoundsPosition.Character.TightBounds;
      Result.Intersect(FCaretBoundsPosition.Row.Bounds);
      Result.MoveToLeft(FPosition.Character.TightBounds.Left);
    end;
  end;
  Result.Width := AWidth;
end;

function TdxCaretPosition.CalculateCurrentFormatingPosition(
  ALogPosition: TdxDocumentLogPosition): TdxDocumentModelPosition;
var
  AFormattingLogPosition: TdxDocumentLogPosition;
  APieceTable: TdxPieceTable;
  ARunInfo: TdxRunInfo;
  ARun: TdxTextRunBase;
begin
  AFormattingLogPosition := Max(ALogPosition - 1, 0);
  APieceTable := DocumentModel.Selection.PieceTable;
  ARunInfo := TdxRunInfo.Create(APieceTable);
  try
    APieceTable.CalculateRunInfoStart(AFormattingLogPosition, ARunInfo);
  finally
    ARunInfo.Free;
  end;
  ARunInfo := APieceTable.FindRunInfo(AFormattingLogPosition, 1);
  try
    ARun := APieceTable.Runs[ARunInfo.Start.RunIndex];
    if not (ARun is TdxTextRun) then
      APieceTable.CalculateRunInfoStart(LogPosition, ARunInfo);
    Result := ARunInfo.Start;
  finally
    ARunInfo.Free;
  end;
end;

function TdxCaretPosition.CreateCaretDocumentLayoutPosition: TdxDocumentLayoutPosition;
begin
  Result := TdxCaretDocumentLayoutPosition.Create(FView);
end;

function TdxCaretPosition.CreateDragCaretPosition: TdxDragCaretPosition;
begin
  Result := TdxDragCaretPosition.Create(View, PreferredPageIndex);
end;

function TdxCaretPosition.CreateExplicitCaretPosition(ALogPosition: TdxDocumentLogPosition): TdxCaretPosition;
begin
  Result := TdxExplicitCaretPosition.Create(View, ALogPosition, PreferredPageIndex);
end;

function TdxCaretPosition.CreateInputPosition(ALogPosition: TdxDocumentLogPosition): TdxInputPosition;
var
  ACurrentFormattingPosition: TdxDocumentModelPosition;
  ARunIndex: TdxRunIndex;
  ARun: TdxTextRunBase;
  AFormattingRunIndex: TdxRunIndex;
  AField: TdxField;
begin
  ACurrentFormattingPosition := CalculateCurrentFormatingPosition(ALogPosition);
  ARunIndex := ACurrentFormattingPosition.RunIndex;
  ARun := DocumentModel.ActivePieceTable.Runs[ARunIndex];
  Result := TdxInputPosition.Create(DocumentModel.ActivePieceTable);
  Result.LogPosition := ALogPosition;
  Result.ParagraphIndex := ARun.Paragraph.Index;
  AFormattingRunIndex := ARunIndex;
  AField := DocumentModel.ActivePieceTable.FindFieldByRunIndex(AFormattingRunIndex);
  if AField <> nil then
  begin
    Assert(False);
  end
  else
    if not DocumentModel.ActivePieceTable.VisibleTextFilter.IsRunVisible(AFormattingRunIndex)then
    begin
      AFormattingRunIndex := DocumentModel.ActivePieceTable.VisibleTextFilter.GetPrevVisibleRunIndex(ARunIndex);
      ARun := DocumentModel.ActivePieceTable.Runs[AFormattingRunIndex];
      if not (ARun is TdxTextRun) then
      begin
        AFormattingRunIndex := DocumentModel.ActivePieceTable.VisibleTextFilter.GetNextVisibleRunIndex(ARunIndex);
        ARun := DocumentModel.ActivePieceTable.Runs[AFormattingRunIndex];
      end;
    end;
  Result.CharacterStyleIndex := ARun.CharacterStyleIndex;
  Result.CharacterFormatting.CopyFrom(ARun.CharacterProperties.Info);
  Result.MergedCharacterFormatting.CopyFrom(ARun.MergedCharacterFormatting);
end;

function TdxCaretPosition.CreateInputPosition: TdxInputPosition;
begin
  Result := CreateInputPosition(LogPosition);
end;

function TdxCaretPosition.GetActualLogPosition(AEndPosition: TdxDocumentLogPosition): TdxDocumentLogPosition;
var
  ASelection: TdxSelection;
  AEnd: TdxDocumentLogPosition;
begin
  ASelection := DocumentModel.Selection;
  if ASelection.Start <= ASelection.&End then
    AEnd := Max(0, AEndPosition - ASelection.ActiveSelection.RightOffset)
  else
  begin
    AEnd := AEndPosition + ASelection.ActiveSelection.LeftOffset;
    if UsePreviousBoxBounds then
      Inc(AEnd);
  end;
  Result := Min(AEnd, ASelection.PieceTable.DocumentEndLogPosition);
end;

function TdxCaretPosition.GetCaretBoundsDocumentLayoutPosition(
  ALogPosition: TdxDocumentLogPosition): TdxDocumentLayoutPosition;
begin
  if (FCaretBoundsPosition <> nil) and (FCaretBoundsPosition.LogPosition = ALogPosition) then
    Result := FCaretBoundsPosition
  else
  begin
    if FCaretBoundsPosition.LogPosition = FPosition.LogPosition then
      Result := FPosition
    else
    begin
			Result := CreateCaretDocumentLayoutPosition;
			Result.SetLogPosition(ALogPosition);
    end;
  end;
end;

function TdxCaretPosition.GetDocumentModel: TdxDocumentModel;
begin
  Result := View.DocumentModel;
end;

function TdxCaretPosition.GetInputPosition: TdxInputPosition;
begin
  if FInputPosition = nil then
    FInputPosition := CreateInputPosition;
  Result := FInputPosition;
end;

function TdxCaretPosition.GetLogPosition: TdxDocumentLogPosition;
begin
  Result := GetActualLogPosition(DocumentModel.Selection.&End);
end;

function TdxCaretPosition.GetPreferredPageIndex: Integer;
begin
  Result := FPreferredPageIndex;
end;

function TdxCaretPosition.GetUsePreviousBoxBounds: Boolean;
begin
  Result := DocumentModel.Selection.UsePreviousBoxBounds;
end;

function TdxCaretPosition.GetVirtualLogPosition: TdxDocumentLogPosition;
begin
  Result := GetActualLogPosition(DocumentModel.Selection.VirtualEnd);
end;

procedure TdxCaretPosition.Invalidate;
begin
  FPosition.Invalidate;
  FCaretBoundsPosition := FPosition;
  InvalidatePageViewInfo;
end;

procedure TdxCaretPosition.InvalidateInputPosition;
begin
  FreeAndNil(FInputPosition);
end;

procedure TdxCaretPosition.InvalidatePageViewInfo;
begin
  FPageViewInfo := nil; 
end;

procedure TdxCaretPosition.SetPreferredPageIndex(const Value: Integer);
begin
  Assert(Value >= 0);
  FPreferredPageIndex := Value;

  if LayoutPosition is TdxTextBoxDocumentLayoutPosition then
    TdxTextBoxDocumentLayoutPosition(LayoutPosition).PreferredPageIndex := FPreferredPageIndex;
  if CaretBoundsPosition is TdxTextBoxDocumentLayoutPosition then
    TdxTextBoxDocumentLayoutPosition(CaretBoundsPosition).PreferredPageIndex := FPreferredPageIndex;
end;

function TdxCaretPosition.TryGetInputPosition: TdxInputPosition;
begin
  Result := FInputPosition;
end;

function TdxCaretPosition.Update(ADetailsLevel: TdxDocumentLayoutDetailsLevel): Boolean;
begin
  View.CheckExecutedAtUIThread;
  FPosition.SetLogPosition(VirtualLogPosition);
  if not FPosition.Update(View.FormattingController.PageController.Pages, ADetailsLevel) then
  begin
    if FPosition.DetailsLevel > TdxDocumentLayoutDetailsLevel.None then
      FPageViewInfo := View.LookupPageViewInfoByPage(FPosition.Page);
    FCaretBoundsPosition := FPosition;
    Exit(False);
  end;
  FCaretBoundsPosition := UpdateCaretDocumentLayoutPosition(FPosition.DetailsLevel);
  if FPageViewInfo <> nil then
    Exit(True);
  if FPosition.DetailsLevel > TdxDocumentLayoutDetailsLevel.None then
    FPageViewInfo := View.LookupPageViewInfoByPage(FPosition.Page);
  Result := FPageViewInfo <> nil;
end;

function TdxCaretPosition.UpdateCaretDocumentLayoutPosition(
  ADetailsLevel: TdxDocumentLayoutDetailsLevel): TdxDocumentLayoutPosition;
var
  APos: TdxDocumentModelPosition;
  ATextFilter: IVisibleTextFilter;
  ALogPosition: TdxDocumentLogPosition;
begin
  if UsePreviousBoxBounds then
    Exit(FPosition);
  APos := DocumentModel.Selection.Interval.&End;
  if APos.RunOffset <> 0 then
    Exit(FPosition);
  if APos.LogPosition = APos.PieceTable.Paragraphs[APos.ParagraphIndex].LogPosition then
    Exit(FPosition);
  ATextFilter := APos.PieceTable.VisibleTextFilter;
  ALogPosition := ATextFilter.GetPrevVisibleLogPosition(VirtualLogPosition, False);
  if ALogPosition < 0 then
    Exit(FPosition);
  Result := GetCaretBoundsDocumentLayoutPosition(ALogPosition);
  Result.Update(View.FormattingController.PageController.Pages, ADetailsLevel);
  Assert(Result.IsValid(ADetailsLevel));
end;

function TdxCaretPosition.UpdatePositionTrySetUsePreviousBoxBounds(
  ADetailsLevel: TdxDocumentLayoutDetailsLevel): Boolean;
var
  ASelection: TdxSelection;
begin
  Result := False;
  ASelection := DocumentModel.Selection;
  if (not UsePreviousBoxBounds) and (ASelection.Length > 0) and (ASelection.&End > ASelection.Start) then
    if FPosition.IsValid(TdxDocumentLayoutDetailsLevel.Row) then
      if FPosition.Row.GetFirstPosition(FPosition.PieceTable).LogPosition = FPosition.LogPosition then
      begin
        ASelection.UsePreviousBoxBounds := True;
        FPosition.Invalidate;
        Exit(Update(ADetailsLevel));
      end;
end;

{ TdxCaret }

constructor TdxCaret.Create;
begin
  inherited Create;
  FHidden := True;
end;

procedure TdxCaret.Draw(ACanvas: TcxCanvas);
begin
  DrawCore(ACanvas);
end;

procedure TdxCaret.DrawCore(ACanvas: TcxCanvas);
var
  ABrush, AOldBrush: HBRUSH;
begin
  ABrush := GetStockObject(WHITE_BRUSH);
  AOldBrush := SelectObject(ACanvas.Handle, ABrush);
  try
    PatBlt(ACanvas.Handle, Bounds.Left, Bounds.Top, Bounds.Width, Bounds.Height, PATINVERT);
  finally
    SelectObject(ACanvas.Handle, AOldBrush);
  end;
end;

function TdxCaret.GetCaretPosition(AView: TdxRichEditView): TdxCaretPosition;
begin
  Result := AView.CaretPosition;
end;

procedure TdxCaret.SetHidden(const Value: Boolean);
begin
  FHidden := Value;
end;

function TdxCaret.ShouldDrawCaret(ADocumentModel: TdxDocumentModel): Boolean;
begin
  Result := ADocumentModel.Selection.Length <= 0;
end;

{ TdxDocumentSelectionLayout }

function TdxDocumentSelectionLayout.GetType: TClass;
begin
  Result := ClassType; 
end;

procedure TdxDocumentSelectionLayout.AddNestedItem(APageIndex: Integer; AItem: IdxSelectionLayoutItem);
begin
  if not FNestedItems.ContainsKey(APageIndex) then
    FNestedItems.Add(APageIndex, TList<IdxSelectionLayoutItem>.Create);
  FNestedItems[APageIndex].Add(AItem);
end;

procedure TdxDocumentSelectionLayout.AddToPageSelection(APage: TdxPage; ATarget: TdxPageSelectionLayoutsCollection);
var
  ASet: TList<IdxSelectionLayoutItem>;
begin
  if APage = nil then
    Exit;
  if not NestedItems.TryGetValue(APage.PageIndex, ASet) then
    Exit;
  ATarget.AddRange(ASet);
end;

constructor TdxDocumentSelectionLayout.Create(ASelectionLayout: TdxSelectionLayout; AStart, AEnd: TdxDocumentLayoutPosition;
  APieceTable: TdxPieceTable);
begin
  inherited Create;
  FUpdatedSuccessfully := False;
  FHotZones := TdxHotZoneCollection.Create;
  FNestedItems := TObjectDictionary<Integer, TList<IdxSelectionLayoutItem>>.Create([doOwnsValues]);
  FDocumentLayout := AStart.DocumentLayout;
  FSelectionLayout := ASelectionLayout;
  FPieceTable := APieceTable;
  FStart := AStart;
  FEnd := AEnd;
end;

destructor TdxDocumentSelectionLayout.Destroy;
begin
  FreeAndNil(FEnd);
  FreeAndNil(FStart);
  FreeAndNil(FNestedItems);
  FreeAndNil(FHotZones);
  inherited Destroy;
end;

function TdxDocumentSelectionLayout.CreateDocumentLayoutPosition(
  ALogPosition: TdxDocumentLogPosition): TdxDocumentLayoutPosition;
begin
  Result := SelectionLayout.CreateDocumentLayoutPosition(ALogPosition);
end;

procedure TdxDocumentSelectionLayout.Draw(const ASelectionPainter: IdxSelectionPainter);
begin
  NotImplemented;
end;


function TdxDocumentSelectionLayout.FindFloatingObject(APage: TdxPage; AIndex: TdxRunIndex): TdxFloatingObjectBox;
var
  AResultBox: TdxFloatingObjectBox;
begin
  AResultBox := FindFloatingObject(APage.InnerBackgroundFloatingObjects, AIndex);
  if AResultBox <> nil then
    Exit(AResultBox);
  AResultBox := FindFloatingObject(APage.InnerFloatingObjects, AIndex);
  if AResultBox <> nil then
    Exit(AResultBox);
  Result := FindFloatingObject(APage.InnerForegroundFloatingObjects, AIndex);
end;

function TdxDocumentSelectionLayout.FindFloatingObject(AFloatingObjects: TList<TdxFloatingObjectBox>;
  AIndex: TdxRunIndex): TdxFloatingObjectBox;
var
  APieceTable: TdxPieceTable;
  ABox: TdxFloatingObjectBox;
begin
  if AFloatingObjects = nil then
    Exit(nil);
  APieceTable := DocumentLayout.DocumentModel.ActivePieceTable;
  for ABox in AFloatingObjects do
    if (ABox.StartPos.RunIndex = AIndex) and (APieceTable = ABox.PieceTable) then
      Exit(ABox);
  Result := nil;
end;



procedure TdxDocumentSelectionLayout.ForceUpdateForEmptySelection;
begin
  NestedItems.Clear;
  FUpdatedSuccessfully := True;
end;

function TdxDocumentSelectionLayout.GetBox(APos: TdxDocumentLayoutPosition): TdxPage;
begin
  APos.Update(DocumentLayout.Pages, TdxDocumentLayoutDetailsLevel.Page);
  Result := APos.Page;
end;

function TdxDocumentSelectionLayout.GetDocumentModel: TdxDocumentModel;
begin
  Result := FPieceTable.DocumentModel;
end;

function TdxDocumentSelectionLayout.GetView: TdxRichEditView;
begin
  Result := FSelectionLayout.View;
end;

function TdxDocumentSelectionLayout.HitTest(ALogPosition: TdxDocumentLogPosition; const ALogicalPoint: TPoint): Boolean;
begin
  Result := (ALogPosition >= Start.LogPosition) and (ALogPosition < &End.LogPosition);
end;

function TdxDocumentSelectionLayout.TryToCreateRectangularObjectSelectionLayout(AStart,
  AEnd: TdxDocumentLayoutPosition): TdxRectangularObjectSelectionLayout;
begin
  Result := nil;
end;

function TdxDocumentSelectionLayout.TryToCreateRectangularObjectSelectionLayoutByAnchorRun(
  const ARunInfo: TdxDocumentModelPosition): TdxRectangularObjectSelectionLayout;
begin
  Result := NotImplemented;
end;

function TdxDocumentSelectionLayout.TryToCreateRectangularObjectSelectionLayoutByAnchorRun(
  const ARunInfo: TdxDocumentModelPosition; out APos: TdxDocumentLayoutPosition): TdxRectangularObjectSelectionLayout;
var
  ASelection: TdxSelection;
  ABox: TdxFloatingObjectBox;
begin
  Result := nil;
  APos := nil;
  ASelection := DocumentLayout.DocumentModel.Selection;
  if (Start.LogPosition > ASelection.NormalizedStart) or (ASelection.NormalizedStart > &End.LogPosition) then
    Exit(nil);
  if not (PieceTable.Runs[ARunInfo.RunIndex] is TdxFloatingObjectAnchorRun) then
    Exit(nil);
  APos := CreateDocumentLayoutPosition(ARunInfo.RunStartLogPosition);
  try
    APos.Update(DocumentLayout.Pages, TdxDocumentLayoutDetailsLevel.Box);
    if not APos.IsValid(TdxDocumentLayoutDetailsLevel.Page) then
      Exit(nil);
    ABox := FindFloatingObject(APos.Page, ARunInfo.RunIndex);
    if (ABox <> nil) and (ABox.PieceTable = DocumentLayout.DocumentModel.ActivePieceTable) then
    begin
      Result := TdxRectangularObjectSelectionLayout.Create(View, ABox, ASelection.NormalizedStart, PieceTable);
      Result.HitTestTransform := ABox.CreateBackwardTransformUnsafe;
    end;
  finally
    APos.Free;
  end;
end;




function TdxDocumentSelectionLayout.Update: Boolean;
begin
  HotZones.Clear;
  NestedItems.Clear;
  FUpdatedSuccessfully := UpdateCore and UpdateNestedItems;
  Result := UpdatedSuccessfully;
end;

function TdxDocumentSelectionLayout.UpdateCore: Boolean;

  procedure ReinitializeDocumentLayoutPosition(var APosition: TdxDocumentLayoutPosition; ALogPosition: TdxDocumentLogPosition);
  begin
    if (APosition = nil) or (APosition = Start) then
      APosition := CreateDocumentLayoutPosition(ALogPosition)
    else
    begin
      APosition.Invalidate;
      APosition.SetLogPosition(ALogPosition);
    end;
  end;

var
  APictureSelection, AFloatSelection: TdxRectangularObjectSelectionLayout;
  ASelectionRunsInfo, ARunInfo: TdxRunInfo;
  AStartRunInfo: TdxDocumentModelPosition;
  AAnchorPos: TdxDocumentLayoutPosition;
  AManager: TdxTableCellsManager;
  ARealBoxStart: TdxDocumentLogPosition;
  APos, AStop: TdxDocumentLayoutPosition;
  AFracture: TdxDocumentLayoutPosition;
  APar: TdxParagraph;
  ACell: TdxTableCell;
  AItem: IdxSelectionLayoutItem;
begin
  APictureSelection := TryToCreateRectangularObjectSelectionLayout(Start, &End);
  if APictureSelection <> nil then
  begin
    Start.Update(DocumentLayout.Pages, TdxDocumentLayoutDetailsLevel.Page);
    AddNestedItem(start.Page.PageIndex, APictureSelection);
    Result := True;
    Exit;
  end;
  ASelectionRunsInfo := PieceTable.FindRunInfo(Start.LogPosition, &End.LogPosition - Start.LogPosition);
  try
    AStartRunInfo := ASelectionRunsInfo.Start;
  finally
    ASelectionRunsInfo.Free;
  end;
  while AStartRunInfo.LogPosition < &End.LogPosition do
  begin
      AFloatSelection := TryToCreateRectangularObjectSelectionLayoutByAnchorRun(AStartRunInfo, AAnchorPos);
      if AFloatSelection <> nil then
        AddNestedItem(AAnchorPos.Page.PageIndex, AFloatSelection);
      ARunInfo := PieceTable.FindRunInfo(AStartRunInfo.RunEndLogPosition + 1, 0);
      try
        AStartRunInfo := ARunInfo.Start;
      finally
        ARunInfo.Free;
      end;
   end;
  AManager := PieceTable.TableCellsManager;
  APos := Start;
  AStop := nil;
  AFracture := nil;
  try
    Result := True;
    while APos.LogPosition < &End.LogPosition do
    begin
      if not APos.Update(DocumentLayout.Pages, TdxDocumentLayoutDetailsLevel.Row, True) then
      begin
        ReinitializeDocumentLayoutPosition(APos, APos.LogPosition + 1);
        Continue;
      end;
      ARealBoxStart := APos.Row.GetFirstPosition(PieceTable).LogPosition;
      if ARealBoxStart > APos.LogPosition then
      begin
        ReinitializeDocumentLayoutPosition(APos, ARealBoxStart);
        Continue;
      end;
      APar := PieceTable.FindParagraph(APos.LogPosition);
      ACell := AManager.GetCell(APar);
      if (ACell <> nil) then 
      begin
        ACell := nil;
        ReinitializeDocumentLayoutPosition(AStop, &End.LogPosition - 1);
      end
      else
      begin
        if ACell <> nil then
        begin
          Assert(False, 'not implemented need TdxTableCell');
        end;
        if APar.EndLogPosition > &End.LogPosition - 1 then
          ReinitializeDocumentLayoutPosition(AStop, &End.LogPosition - 1)
        else
          ReinitializeDocumentLayoutPosition(AStop, APar.EndLogPosition);
      end;
      AItem := nil;
      if not AStop.Update(DocumentLayout.Pages, TdxDocumentLayoutDetailsLevel.Row, True) then
      begin
        Result := False;
        Break;
      end;
      if ACell = nil then
      begin
        if APos.Row <> AStop.Row then
        begin
          ReinitializeDocumentLayoutPosition(AFracture, APos.Row.GetLastPosition(PieceTable).LogPosition);
          if not AFracture.Update(DocumentLayout.Pages, TdxDocumentLayoutDetailsLevel.Row, True) then
          begin
            Result := False;
            Break;
          end;
          if AFracture.LogPosition < AStop.LogPosition then
          begin
            if AStop <> Start then
              AStop.Free;
            AStop := AFracture;
          end;
        end;
      end
      else
      begin
          Assert(False, 'not implemented need TdxTableCell');
      end;
      if AItem = nil then
      begin
        if (Start = APos) or (AStop.LogPosition >= &End.LogPosition - 1) then
        begin
          if not APos.Update(DocumentLayout.Pages, TdxDocumentLayoutDetailsLevel.Character, True) or
            not AStop.Update(DocumentLayout.Pages, TdxDocumentLayoutDetailsLevel.Character, True) then
          begin
            Result := False;
            Break;
          end;
          AItem := TdxRowSelectionLayout.Create(APos, AStop, APos.Page);
        end
        else
        begin
          if not APos.Update(DocumentLayout.Pages, TdxDocumentLayoutDetailsLevel.Row, True) or
            not AStop.Update(DocumentLayout.Pages, TdxDocumentLayoutDetailsLevel.Row, True) then
          begin
            Result := False;
            Break;
          end;
          AItem := TdxEntireRowSelectionLayout.Create(APos, AStop, APos.Page);
        end;
      end;
      if AItem <> nil then
        AddNestedItem(APos.Page.PageIndex, AItem);
      ReinitializeDocumentLayoutPosition(APos, AStop.LogPosition + 1);
    end;
  finally
    if APos <> Start then
      FreeAndNil(APos);
    if AFracture <> AStop then
      FreeAndNil(AFracture);
    FreeAndNil(AStop);
  end;
end;

function TdxDocumentSelectionLayout.UpdateNestedItems: Boolean;
var
  AList: TList<IdxSelectionLayoutItem>;
  AItem: IdxSelectionLayoutItem;
begin
  Result := True;
  for AList in NestedItems.Values do
    for AItem in AList do
      if not AItem.Update then
      begin
        Result := False;
        Break;
      end;
end;

{ TdxSelectionLayout }

function TdxSelectionLayout.GetDocumentLayout: TdxDocumentLayout;
begin
  Result := View.DocumentLayout;
end;

function TdxSelectionLayout.GetEndLayoutPosition: TdxDocumentLayoutPosition;
begin
  Result := LastDocumentSelectionLayout.&End;
  Result.Update(DocumentLayout.Pages, TdxDocumentLayoutDetailsLevel.Character);
end;

function TdxSelectionLayout.GetFirstDocumentSelectionLayout: TdxDocumentSelectionLayout;
begin
  if FDocumentSelectionLayouts = nil then
    Update;
  Result := FDocumentSelectionLayouts[0];
end;

function TdxSelectionLayout.GetLastDocumentSelectionLayout: TdxDocumentSelectionLayout;
begin
  if FDocumentSelectionLayouts = nil then
    Update;
  Result := FDocumentSelectionLayouts[FDocumentSelectionLayouts.Count - 1];
end;

function TdxSelectionLayout.GetPageSelection(APage: TdxPage): TdxPageSelectionLayoutsCollection;
var
  I: Integer;
begin
  if FDocumentSelectionLayouts = nil then
    Assert(False); 
  Result := TdxPageSelectionLayoutsCollection.Create;
  for I := 0 to FDocumentSelectionLayouts.Count - 1 do 
    FDocumentSelectionLayouts[I].AddToPageSelection(APage, Result);
  if Result.Count = 0 then
    FreeAndNil(Result);
end;

function TdxSelectionLayout.GetPreferredPageIndex: Integer;
begin
  Result := FPreferredPageIndex;
end;

function TdxSelectionLayout.GetStartLayoutPosition: TdxDocumentLayoutPosition;
begin
  Result := FirstDocumentSelectionLayout.Start;
  Result.Update(DocumentLayout.Pages, TdxDocumentLayoutDetailsLevel.Character);
end;

function TdxSelectionLayout.HitTest(ALogPosition: TdxDocumentLogPosition; ALogicalPoint: TPoint): Boolean;
var
  APiece: TdxDocumentSelectionLayout;
begin
  Result := False;
  Update;
  for APiece in FDocumentSelectionLayouts do
    if APiece.HitTest(ALogPosition, ALogicalPoint) then
    begin
      Result := True;
      Break;
    end;
end;

procedure TdxSelectionLayout.Invalidate;
begin
  if FDocumentSelectionLayouts = nil then
    Exit;
  FreeAndNil(FDocumentSelectionLayouts);
end;

function TdxSelectionLayout.IsSelectionStartFromBeginRow: Boolean;
begin
  Result := StartLayoutPosition.Row.Boxes[0].StartPos.AreEqual(StartLayoutPosition.Character.StartPos);
end;

procedure TdxSelectionLayout.SetPreferredPageIndex(const Value: Integer);
begin
  Assert(Value >= 0);
  FPreferredPageIndex := Value;
end;

procedure TdxSelectionLayout.Update;
var
  ASelection: TdxSelection;
  I: Integer;
  ASelectionItem: TdxSelectionItem;
  AStart, AEnd: TdxDocumentLayoutPosition;
  ADocumentSelectionLayout: TdxDocumentSelectionLayout;
begin
  if (FDocumentSelectionLayouts <> nil) and (FDocumentSelectionLayouts.Count > 0) and LastDocumentSelectionLayout.UpdatedSuccessfully then
    Exit;
  ASelection := DocumentLayout.DocumentModel.Selection;
  FDocumentSelectionLayouts := TObjectList<TdxDocumentSelectionLayout>.Create;
  ASelectionItem := nil;
  for I := 0 to ASelection.Items.Count - 1 do
  begin
    ASelectionItem := ASelection.Items[I];
    if ASelectionItem.IsCovered then
    begin
      Assert(ASelection.Items.Count > 1);
      Continue;
    end;
  end;
  AStart := CreateDocumentLayoutPosition(ASelectionItem.NormalizedStart);
  AEnd := CreateDocumentLayoutPosition(ASelectionItem.NormalizedEnd);
  AStart.LeftOffset := ASelectionItem.LeftOffset;
  AEnd.RightOffset := ASelectionItem.RightOffset;
  ADocumentSelectionLayout := CreateDocumentSelectionLayout(AStart, AEnd);
  FDocumentSelectionLayouts.Add(ADocumentSelectionLayout);
  if (ASelection.Length <= 0) and (ASelection.Items.Count = 1) and (ASelectionItem.LeftOffset = 0) and
      (ASelectionItem.RightOffset = 0) then
    ADocumentSelectionLayout.ForceUpdateForEmptySelection
  else
    ADocumentSelectionLayout.Update;
end;

function TdxSelectionLayout.CalculateHotZone(AResult: TdxRichEditHitTestResult; AView: TdxRichEditView): TdxHotZone;
var
  AHotZones: TdxHotZoneCollection;
  AHotZone: TdxHotZone;
  I: Integer;
begin
  Update;
  AHotZones := LastDocumentSelectionLayout.HotZones;
  Result := nil;
  for I := 0 to AHotZones.Count - 1 do
  begin
    AHotZone := AHotZones[I];
    if AHotZone.HitTest(AResult.LogicalPoint, AView.DocumentModel.Dpi, AView.ZoomFactor) then
    begin
      Result := AHotZone;
      Break;
    end;
  end;
end;

constructor TdxSelectionLayout.Create(AView: TdxRichEditView; APreferredPageIndex: Integer);
begin
  inherited Create;
  Assert(AView <> nil);
  Assert(APreferredPageIndex >= 0);
  FView := AView;
  FPreferredPageIndex := APreferredPageIndex;
end;

destructor TdxSelectionLayout.Destroy;
begin
  FreeAndNil(FDocumentSelectionLayouts);
  inherited Destroy;
end;

function TdxSelectionLayout.CreateDocumentLayoutPosition(
  ALogPosition: TdxDocumentLogPosition): TdxDocumentLayoutPosition;
begin
  Result := TdxDocumentLayoutPosition.Create(DocumentLayout, DocumentLayout.DocumentModel.Selection.PieceTable, ALogPosition);
end;

function TdxSelectionLayout.CreateDocumentSelectionLayout(AStart, AEnd: TdxDocumentLayoutPosition): TdxDocumentSelectionLayout;
begin
  Result := TdxDocumentSelectionLayout.Create(Self, AStart, AEnd, View.DocumentModel.ActivePieceTable);
end;

{ TdxRichEditViewInfo }

constructor TdxRichEditViewInfo.Create(ADocumentLayout: TdxDocumentLayout);
begin
  inherited Create;
  FPageViewInfos := TdxRichEditViewPageViewInfoCollection.Create(ADocumentLayout);
  FOwnedRows := TdxOwnedRowInfoList.Create;
  FOwnedPageViewInfos := TdxOwnedPageViewInfoList.Create;
end;

destructor TdxRichEditViewInfo.Destroy;
begin
  FreeAndNil(FOwnedRows);
  FreeAndNil(FPageViewInfos);
  FreeAndNil(FOwnedPageViewInfos);
  inherited Destroy;
end;

procedure TdxRichEditViewInfo.Clear;
begin
  PageViewInfos.Clear;
  OwnedPageViewInfos.Clear;
  OwnedRows.Clear;
end;

{ TdxRichEditView }

constructor TdxRichEditView.Create(AControl: IdxRichEditControl);
begin
  inherited Create;
  Assert(AControl <> nil);
  FControl := AControl;
  FBackColor := clWindow;
  FZoomFactor := 1;
  FBackColor := clWindow;
  Bounds := cxRectBounds(0, 0, MinWidth, MinHeight);
  FDocumentLayout := CreateDocumentLayout(DocumentModel, AControl.InnerControl);
  FFormattingController := CreateDocumentFormattingController;
  FVerticalScrollController := AControl.CreateRichEditViewVerticalScrollController(Self);
  FHorizontalScrollController := AControl.CreateRichEditViewHorizontalScrollController(Self);
  FViewInfo := TdxRichEditViewInfo.Create(DocumentLayout);
  FPageViewInfoGenerator := CreatePageViewInfoGenerator;
  FSelectionLayout := TdxSelectionLayout.Create(Self, 0);
  FCaretPosition := TdxCaretPosition.Create(Self, 0);
  FFormattingController.ResetFormattingFromCurrentArea.Add(OnResetFormattingFromCurrentArea);
  FFormattingController.ResetSecondaryFormattingForPage.Add(OnResetSecondaryFormattingForPage);
  FPadding := CreatePadding;
  FPadding.OnChange := PaddingChanged;
end;

destructor TdxRichEditView.Destroy;
begin
  if Formatter <> nil then
  begin
    SuspendFormatting;
    try
      UnsubscribePageFormattingComplete;
    finally
      ResumeFormatting;
    end;
  end;
  if DocumentLayout <> nil then
  begin
    UnsubscribeDocumentLayoutEvents;
    FDocumentLayout.Free;
    FDocumentLayout := nil;
  end;
  if (DocumentModel <> nil) and (DocumentModel.LayoutOptions <> nil) then
  begin
  end;
  FreeAndNil(FCaretPosition);
  FreeAndNil(FSelectionLayout);
  FreeAndNil(FPageViewInfoGenerator);
  FreeAndNil(FViewInfo);
  FreeAndNil(FHorizontalScrollController);
  FreeAndNil(FVerticalScrollController);
  FreeAndNil(FFormattingController);
  FreeAndNil(FPadding);
  inherited Destroy;
end;

procedure TdxRichEditView.CheckExecutedAtBackgroundThread;
begin
end;

procedure TdxRichEditView.CheckExecutedAtUIThread;
begin
end;

procedure TdxRichEditView.Activate(const AViewBounds: TRect);
var
  APieceTable: TdxPieceTable;
  AFrom, ATo: TdxDocumentModelPosition;
begin
  Assert(AViewBounds.Location.IsEqual(cxNullPoint));
  CaretPosition.Invalidate;
  CaretPosition.InvalidateInputPosition;
  SelectionLayout.Invalidate;
  ResetPages(TdxPageGenerationStrategyType.RunningHeight);
  FormattingController.Reset(False);
  VerticalScrollController.Activate;
  HorizontalScrollController.Activate;
  SubscribeDocumentLayoutEvents;
  SubscribePageFormattingComplete;
  PerformResize(AViewBounds);
  APieceTable := DocumentModel.MainPieceTable;
  AFrom := TdxDocumentModelPosition.Create(APieceTable);
  ATo := TdxDocumentModelPosition.FromParagraphEnd(APieceTable, APieceTable.Paragraphs.Last.Index);
  Formatter.UpdateSecondaryPositions(AFrom, ATo);
  Formatter.NotifyDocumentChanged(AFrom, ATo, TdxDocumentLayoutResetType.AllPrimaryLayout);
end;

procedure TdxRichEditView.BeginDocumentRendering;
var
  I: Integer;
  ALastRow: TdxRow;
  APage, ALastPage: TdxPage;
begin
  Formatter.BeginDocumentRendering(IsPrimaryFormattingCompleteForVisibleHeight2);
  if PageViewInfos.Count = 0 then
  begin
    ResetPages(TdxPageGenerationStrategyType.FirstPageOffset);
    ALastPage := DocumentLayout.Pages.Last;
    PageViewInfoGenerator.FirstPageOffsetAnchor.PageIndex := DocumentLayout.Pages.Count - 1;
    ALastRow := ALastPage.Areas.Last.Columns.Last.Rows.Last;
    FPageViewInfoGenerator.FirstPageOffsetAnchor.VerticalOffset := ALastRow.Bounds.Top;
    GeneratePages;
  end;
  for I := 0 to PageViewInfos.Count - 1 do
  begin
    APage := PageViewInfos[I].Page;
    Assert(APage.PrimaryFormattingComplete);
    if not APage.SecondaryFormattingComplete then
      Formatter.PerformPageSecondaryFormatting(APage);
    Assert(APage.SecondaryFormattingComplete);
  end;
  Assert(PageViewInfos.Count > 0);
end;

function TdxRichEditView.CalculateDocumentLayoutResetType(
  AChanges: TdxDocumentModelDeferredChanges): TdxDocumentLayoutResetType;
var
  AChangeActions: TdxDocumentModelChangeActions;
begin
  AChangeActions := AChanges.ChangeActions;
  if TdxDocumentModelChangeAction.ResetAllPrimaryLayout in AChangeActions then
    Exit(TdxDocumentLayoutResetType.AllPrimaryLayout);
  if TdxDocumentModelChangeAction.ResetPrimaryLayout  in AChangeActions then
    Exit(TdxDocumentLayoutResetType.PrimaryLayoutFormPosition);
  if TdxDocumentModelChangeAction.ResetSecondaryLayout in AChangeActions then
    Exit(TdxDocumentLayoutResetType.PrimaryLayoutFormPosition)
  else
    Exit(TdxDocumentLayoutResetType.None);
end;

function TdxRichEditView.CalculateFirstInvisiblePageIndexBackward: Integer;
var
  AFirstRow: TdxPageViewInfoRow;
  APageViewInfo: TdxPageViewInfo;
begin
  Result := -1;
  AFirstRow := PageViewInfoGenerator.ActiveGenerator.PageRows.First;
  APageViewInfo := AFirstRow.First;
  if APageViewInfo <> nil then
    Result := FormattingController.PageController.Pages.IndexOf(APageViewInfo.Page) - 1;
end;

function TdxRichEditView.CalculateFirstInvisiblePageIndexForward: Integer;
var
  ALastRow: TdxPageViewInfoRow;
  APageViewInfo: TdxPageViewInfo;
begin
  ALastRow := PageViewInfoGenerator.ActiveGenerator.PageRows.Last;
  APageViewInfo := ALastRow.Last;
  if APageViewInfo = nil then
    Result := -1
  else
    Result := 1 + FormattingController.PageController.Pages.IndexOf(APageViewInfo.Page);
end;


function TdxRichEditView.CalculateFloatingObjectHitTest(APage: TdxPage; const APoint: TPoint;
  APredicate: TdxPredicate<TdxFloatingObjectBox>): TdxFloatingObjectBox;
var
  AFloatingObjects: TList<TdxFloatingObjectBox>;
begin
  AFloatingObjects := APage.GetSortedNonBackgroundFloatingObjects(TdxFloatingObjectBoxZOrderComparer.Create);
  try
    Result := CalculateFloatingObjectHitTestCore(APoint, AFloatingObjects, APredicate);
    if Result = nil then
      Result := CalculateFloatingObjectHitTestCore(APoint, APage.InnerBackgroundFloatingObjects, APredicate);
  finally
    AFloatingObjects.Free;
  end;
end;

function TdxRichEditView.CalculateFloatingObjectHitTestCore(const APoint: TPoint; AList: TList<TdxFloatingObjectBox>;
  APredicate: TdxPredicate<TdxFloatingObjectBox>): TdxFloatingObjectBox;
var
  I: Integer;
begin
  Result := nil;
  if AList <> nil then
  begin
    for I := AList.Count - 1 downto 0 do
      if IsFloatingObjectHit(AList[I], APoint) and APredicate(AList[I]) then
        Exit(AList[I]);
  end;
end;

function TdxRichEditView.CalculateHitTest(const APoint: TPoint;
  ADetailsLevel: TdxDocumentLayoutDetailsLevel): TdxRichEditHitTestResult;
var
  ARequest: TdxRichEditHitTestRequest;
begin
  ARequest := TdxRichEditHitTestRequest.Create(DocumentModel.ActivePieceTable);
  try
    ARequest.PhysicalPoint := APoint;
    ARequest.DetailsLevel := ADetailsLevel;
    ARequest.Accuracy := DefaultHitTestPageAccuracy or NearestPageArea or NearestColumn or
      NearestTableRow or NearestTableCell or NearestRow or NearestBox or NearestCharacter;
    Result := HitTestCore(ARequest, True);
    if not Result.IsValid(ADetailsLevel) then
      Result := nil;
  finally
    ARequest.Free;
  end;
end;

function TdxRichEditView.CalculateNearestCharacterHitTest(const APoint: TPoint): TdxRichEditHitTestResult;
var
  ARequest: TdxRichEditHitTestRequest;
begin
  ARequest := TdxRichEditHitTestRequest.Create(DocumentModel.ActivePieceTable);
  try
    ARequest.PhysicalPoint := APoint;
    ARequest.DetailsLevel := TdxDocumentLayoutDetailsLevel.Character;
    ARequest.Accuracy := DefaultHitTestPageAccuracy or NearestPageArea or NearestColumn or
      NearestTableRow or NearestTableCell or NearestRow or NearestBox or NearestCharacter;
    Result := HitTestCore(ARequest, (DefaultHitTestPageAccuracy and ExactPage) <> 0);
    if not Result.IsValid(TdxDocumentLayoutDetailsLevel.Character) then
      FreeAndNil(Result);
  finally
    ARequest.Free;
  end;
end;

function TdxRichEditView.CalculateNearestPageHitTest(const APoint: TPoint;
  AStrictHitIntoPageBounds: Boolean): TdxRichEditHitTestResult;
var
  ARequest: TdxRichEditHitTestRequest;
begin
  ARequest := TdxRichEditHitTestRequest.Create(DocumentModel.ActivePieceTable);
  try
    ARequest.PhysicalPoint := APoint;
    ARequest.DetailsLevel := TdxDocumentLayoutDetailsLevel.Page;
    ARequest.Accuracy := NearestPage or NearestPageArea or NearestColumn or
      NearestTableRow or NearestTableCell or NearestRow or NearestBox or NearestCharacter;
    Result := HitTestCore(ARequest, AStrictHitIntoPageBounds);
    if not Result.IsValid(TdxDocumentLayoutDetailsLevel.Page) then
      FreeAndNil(Result);
  finally
    ARequest.Free;
  end;
end;

function TdxRichEditView.CalculateOptimalHorizontalScrollbarPosition(APageViewInfo: TdxPageViewInfo): Integer;
var
  ALogicalVisibleWidth: LongInt;
  APageClientBounds: TRect;
  AHalfOfHorizontalPageGap: Integer;
begin
  ALogicalVisibleWidth := Round(Bounds.Width / ZoomFactor); 
  APageClientBounds := APageViewInfo.Page.ClientBounds;
  AHalfOfHorizontalPageGap := Trunc(PageViewInfoGenerator.HorizontalPageGap / 2);
  if ALogicalVisibleWidth > APageViewInfo.Page.Bounds.Width + PageViewInfoGenerator.HorizontalPageGap then
    Result := 0
  else
    if ALogicalVisibleWidth > APageViewInfo.Page.Bounds.Width then
      Result := Trunc((APageViewInfo.Page.Bounds.Width + PageViewInfoGenerator.HorizontalPageGap - ALogicalVisibleWidth) / 2 - FixedLeftTextBorderOffset)
    else
      if ALogicalVisibleWidth > APageViewInfo.Page.Bounds.Right - APageClientBounds.Left + AHalfOfHorizontalPageGap then
        Result := APageViewInfo.Page.Bounds.Width + PageViewInfoGenerator.HorizontalPageGap - ALogicalVisibleWidth - FixedLeftTextBorderOffset
      else
        Result := AHalfOfHorizontalPageGap + APageClientBounds.Left - FixedLeftTextBorderOffset;
end;

function TdxRichEditView.CalculatePageContentClipBounds(APage: TdxPageViewInfo): TRect;
var
  AZoomFactor: Single;
  ARightMarginWidth, AClientBottom, ABottomMarginHeight, AClipWidth, AClipHeight: Integer;
begin
  ARightMarginWidth := APage.Page.Bounds.Right - APage.Page.ClientBounds.Right;
  if ARightMarginWidth = 0 then
    ARightMarginWidth := DocumentModel.LayoutUnitConverter.TwipsToLayoutUnits(-180);
  AClientBottom := APage.Page.ClientBounds.Bottom;
  if APage.Page.Footer <> nil then
    AClientBottom := Math.Max(AClientBottom, APage.Page.Footer.Bounds.Bottom);
  ABottomMarginHeight := APage.Page.Bounds.Bottom - AClientBottom;
  if ABottomMarginHeight = 0 then
    ABottomMarginHeight := DocumentModel.LayoutUnitConverter.TwipsToLayoutUnits(-180);
  AClipWidth := Trunc(APage.ClientBounds.Width - ARightMarginWidth / 6);
  AClipHeight := Trunc(APage.ClientBounds.Height - ABottomMarginHeight / 6);
  AZoomFactor := ZoomFactor;
  Result.Init(0, 0, Math.Ceil(AClipWidth / AZoomFactor), Math.Ceil(AClipHeight / AZoomFactor));
  Result := ExpandClipBoundsToPaddings(Result);
end;

function TdxRichEditView.CalculateResetFromPosition(APieceTable: TdxPieceTable;
  AChanges: TdxDocumentModelDeferredChanges; AResetType: TdxDocumentLayoutResetType): TdxDocumentModelPosition;
var
  AChangeStart: TdxDocumentModelPosition;
  AModelPosition: TdxDocumentModelPosition;
begin
  AChangeStart := AChanges.ChangeStart;
  if AResetType = TdxDocumentLayoutResetType.PrimaryLayoutFormPosition then
  begin
    AModelPosition := TdxDocumentModelPosition.FromParagraphStart(APieceTable, AChangeStart.ParagraphIndex);
    AModelPosition := EnsurePositionVisibleWhenHiddenTextNotShown(DocumentModel, AModelPosition);
    AModelPosition := EnsurePositionNotBeforeSectionBreakAfterParagraphBreak(AModelPosition);
    Exit(EnsureTopLevelParagraph(AModelPosition));
  end;
  Result := AChangeStart;
end;

function TdxRichEditView.CalculateVisiblePageBounds(const APageBounds: TRect; APageViewInfo: TdxPageViewInfo): TRect;
var
  APageViewInfoTotalHeight: Integer;
  AViewPort, AExtendedViewPortBounds, APageViewInfoVisibleBounds, AVisibleBounds: TRect;
begin
  APageViewInfoTotalHeight := APageViewInfo.Bounds.Height;
  if APageViewInfoTotalHeight > 0 then
  begin
    AViewPort := PageViewInfoGenerator.ViewPortBounds;
    AExtendedViewPortBounds := TRect.CreateSize(0, AViewPort.Top, MaxInt, AViewPort.Height);

    APageViewInfoVisibleBounds := AExtendedViewPortBounds;
    APageViewInfoVisibleBounds.Intersect(APageViewInfo.Bounds);

    AVisibleBounds := APageBounds;
    AVisibleBounds := cxRectSetTop(AVisibleBounds, Math.Floor(APageBounds.Height / APageViewInfoTotalHeight * (APageViewInfoVisibleBounds.Top - APageViewInfo.Bounds.Top)));
    AVisibleBounds.Height := Math.Ceil(APageBounds.Height / APageViewInfoTotalHeight * APageViewInfoVisibleBounds.Height);
    Result := AVisibleBounds;
  end
  else
    Result := cxNullRect;
end;

procedure TdxRichEditView.ResetBackColor;
begin
  BackColor := clWindow;
end;

function TdxRichEditView.GetActualBackColor: TColor;
begin
  Result := BackColor;
end;

function TdxRichEditView.GetMinWidth: Integer;
begin
  Result := DefaultMinWidth;
end;

function TdxRichEditView.GetMinHeight: Integer;
begin
  Result := DefaultMinHeight;
end;

function TdxRichEditView.DefaultHitTestPageAccuracy: TdxHitTestAccuracy;
begin
  Result := ExactPage;
end;

function TdxRichEditView.ChoosePageViewInfoForOptimalHorizontalScrollbarPosition: TdxPageViewInfo;
var
  APageViewInfoRows: TdxPageViewInfoRowCollection;
  ARow: TdxPageViewInfoRow;
  X, AOffset: Single;
  I: Integer;
  APageViewInfo: TdxPageViewInfo;
begin
  APageViewInfoRows := PageViewInfoGenerator.ActiveGenerator.PageRows;
  ARow := APageViewInfoRows[0];
  if ARow.Count <= 0 then
    Exit(nil);
  Result := ARow[0];
  X := Result.Page.ClientBounds.Left * ZoomFactor + Result.Bounds.Left;
  for I := 0 to APageViewInfoRows.Count - 1 do
  begin
    APageViewInfo := APageViewInfoRows[I][0];
    AOffset := APageViewInfo.Page.ClientBounds.Left * ZoomFactor + APageViewInfo.Bounds.Left;
    if AOffset > X then
    begin
      X := AOffset;
      Result := APageViewInfo;
    end;
  end;
end;

procedure TdxRichEditView.CorrectZoomFactor;
var
  AOptions: TdxRichEditBehaviorOptions;
  AZoom, ADefaultMaxZoomFactor: Single;
begin
  AOptions := Control.InnerControl.Options.Behavior;
  AZoom := Math.Max(DefaultZoomFactor, AOptions.MinZoomFactor);
  ADefaultMaxZoomFactor := AOptions.DefaultMaxZoomFactor;
  if SameValue(AOptions.MaxZoomFactor, ADefaultMaxZoomFactor) then
    FZoomFactor := AZoom
  else
    FZoomFactor := Math.Min(AZoom, AOptions.MaxZoomFactor);
end;

function TdxRichEditView.CreateDocumentLayout(ADocumentModel: TdxDocumentModel;
  AMeasurerProvider: IdxBoxMeasurerProvider): TdxDocumentLayout;
begin
  Result := TdxDocumentLayout.Create(ADocumentModel, AMeasurerProvider);
end;

function TdxRichEditView.CreateHitTestCalculator(ARequest: TdxRichEditHitTestRequest;
  AResult: TdxRichEditHitTestResult): TdxBoxHitTestCalculator;
begin
  Result := FormattingController.PageController.CreateHitTestCalculator(ARequest, AResult);
end;

function TdxRichEditView.CreateLogicalPoint(const AClientBounds: TRect; const APoint: TPoint): TPoint;
var
  AMat: TdxTransformMatrix;
begin
  AMat := CreateTransformMatrix(AClientBounds);
  try
    AMat.Invert;
    Result := AMat.TransformPoint(APoint);
  finally
    AMat.Free;
  end;
end;

function TdxRichEditView.CreateLogicalRectangle(APageViewInfo: TdxPageViewInfo; const ABounds: TRect): TRect;
var
  AMat: TdxTransformMatrix;
begin
  AMat := CreateTransformMatrix(APageViewInfo.ClientBounds);
  try
    AMat.Invert;
    Result := AMat.TransformRect(ABounds);
  finally
    AMat.Free;
  end;
end;

function TdxRichEditView.CreatePhysicalPoint(APageViewInfo: TdxPageViewInfo; const APoint: TPoint): TPoint;
var
  AMat: TdxTransformMatrix;
begin
  AMat := CreateTransformMatrix(APageViewInfo.ClientBounds);
  try
    Result := AMat.TransformPoint(APoint);
  finally
    AMat.Free;
  end;
end;

function TdxRichEditView.CreatePhysicalRectangle(APageViewInfo: TdxPageViewInfo; const ABounds: TRect): TRect;
begin
  Result := CreatePhysicalRectangle(APageViewInfo.ClientBounds, ABounds);
end;

function TdxRichEditView.CreatePhysicalRectangle(const APageViewInfoClientBounds, ABounds: TRect): TRect;
var
  AMat: TdxTransformMatrix;
begin
  AMat := CreateTransformMatrix(APageViewInfoClientBounds);
  try
    Result := AMat.TransformRect(ABounds);
  finally
    AMat.Free;
  end;
end;

function TdxRichEditView.CreatePhysicalRectangleFast(APageViewInfo: TdxPageViewInfo; const ABounds: TRect): TRect;
var
  AClientBounds: TRect;
  X, Y, W, H: Single;
begin
  AClientBounds := APageViewInfo.ClientBounds;
  X := FZoomFactor * ABounds.Left + AClientBounds.Left - HorizontalScrollController.GetPhysicalLeftInvisibleWidth;
  Y := FZoomFactor * ABounds.Top + AClientBounds.Top;
  W := FZoomFactor * ABounds.Width;
  H := FZoomFactor * ABounds.Height;
  Result := cxRectBounds(Trunc(X), Trunc(Y), Trunc(W), Trunc(H));
end;

function TdxRichEditView.CreateTransformMatrix(const AClientBounds: TRect): TdxTransformMatrix;
begin
  Result := TdxTransformMatrix.Create;
  Result.Translate(AClientBounds.Left - HorizontalScrollController.GetPhysicalLeftInvisibleWidth, AClientBounds.Top);
  Result.Scale(ZoomFactor, ZoomFactor);
end;

procedure TdxRichEditView.Deactivate;
begin
  UnsubscribePageFormattingComplete;
  UnsubscribeDocumentLayoutEvents;
  VerticalScrollController.Deactivate;
  HorizontalScrollController.Deactivate;
end;

procedure TdxRichEditView.EndDocumentRendering;
begin
  Formatter.EndDocumentRendering;
end;

procedure TdxRichEditView.EnforceFormattingCompleteForVisibleArea;
begin
  if Control.InnerControl.DocumentModel.IsUpdateLocked then
    Exit;
  BeginDocumentRendering;
  EndDocumentRendering;
end;

procedure TdxRichEditView.EnsureCaretVisible;
var
  ACommand: TdxRichEditCommand;
begin
  ACommand := TdxEnsureCaretVisibleVerticallyCommand.Create(Control);
  try
    ACommand.Execute;
  finally
    ACommand.Free;
  end;
  ACommand := TdxEnsureCaretVisibleHorizontallyCommand.Create(Control);
  try
    ACommand.Execute;
  finally
    ACommand.Free;
  end;
end;

procedure TdxRichEditView.EnsureFormattingCompleteForLogPosition(ALogPosition: TdxDocumentLogPosition);
begin
  CheckExecutedAtUIThread;
  Formatter.WaitForPrimaryLayoutReachesLogPosition(ALogPosition);
end;

procedure TdxRichEditView.EnsureFormattingCompleteForPreferredPage(APreferredPageIndex: Integer);
begin
  CheckExecutedAtUIThread;
  Formatter.WaitForPrimaryLayoutReachesPreferredPage(APreferredPageIndex);
end;

procedure TdxRichEditView.EnsureFormattingCompleteForSelection;
begin
  EnsureFormattingCompleteForLogPosition(DocumentModel.Selection.NormalizedEnd);
end;

procedure TdxRichEditView.EnsureCaretVisibleOnResize;
begin
end;

procedure TdxRichEditView.EnsurePageSecondaryFormattingComplete(APage: TdxPage);
var
  APos: TdxDocumentModelPosition;
begin
  CheckExecutedAtUIThread;
  if APage.PrimaryFormattingComplete and APage.SecondaryFormattingComplete then
    Exit;
  Formatter.WaitForPagePrimaryLayoutComplete(APage);
  Assert(APage.PrimaryFormattingComplete);
  APos := APage.GetLastPosition(DocumentModel.ActivePieceTable);
  Formatter.WaitForSecondaryLayoutReachesPosition(APos);
end;

function TdxRichEditView.EnsurePositionNotBeforeSectionBreakAfterParagraphBreak(
  const AModelPosition: TdxDocumentModelPosition): TdxDocumentModelPosition;
var
  AParagraphs: TdxParagraphCollection;
  ARuns: TdxTextRunCollection;
  AParagraph: TdxParagraph;
begin
  AParagraphs := AModelPosition.PieceTable.Paragraphs;
  ARuns := AModelPosition.PieceTable.Runs;
  AParagraph := AParagraphs[AModelPosition.ParagraphIndex];
  while (AModelPosition.RunIndex > 0) and (AParagraph.Length = 1) and 
    (ARuns[AModelPosition.RunIndex] is TdxSectionRun) do
  begin
    AModelPosition.ParagraphIndex := AModelPosition.ParagraphIndex;
    AParagraph := AParagraphs[AModelPosition.ParagraphIndex];
    AModelPosition.RunIndex := AParagraph.FirstRunIndex;
    AModelPosition.LogPosition := AParagraph.LogPosition;
    AModelPosition.RunStartLogPosition := AModelPosition.LogPosition;
  end;
  Result := AModelPosition;
end;

class function TdxRichEditView.EnsurePositionVisibleWhenHiddenTextNotShown(ADocumentModel: TdxDocumentModel;
  const AModelPosition: TdxDocumentModelPosition): TdxDocumentModelPosition;
var
  APieceTable: TdxPieceTable;
  AVisibleTextFilter: TdxVisibleTextFilterBase;
  AParagraphIndex: TdxParagraphIndex;
begin
  APieceTable := AModelPosition.PieceTable;
  AVisibleTextFilter := APieceTable.VisibleTextFilter;
  AParagraphIndex := AModelPosition.ParagraphIndex;
  while AParagraphIndex > 0 do 
  begin
    if AVisibleTextFilter.IsRunVisible(APieceTable.Paragraphs[AParagraphIndex - 1].LastRunIndex) then
      Break;
    Dec(AParagraphIndex);
  end;
  if AParagraphIndex = AModelPosition.ParagraphIndex then
    Result := AModelPosition
  else
    Result := TdxDocumentModelPosition.FromParagraphStart(APieceTable, AParagraphIndex);
end;

function TdxRichEditView.EnsureTopLevelParagraph(const APos: TdxDocumentModelPosition): TdxDocumentModelPosition;
var
  AParagraphs: TdxParagraphCollection;
  AParagraphIndex: TdxParagraphIndex;
  ACell: TdxTableCell;
begin
  AParagraphs := APos.PieceTable.Paragraphs;
  AParagraphIndex := APos.ParagraphIndex;
  ACell := AParagraphs[AParagraphIndex].GetCell;
  if ACell = nil then
    Exit(APos);
  NotImplemented;
end;

function TdxRichEditView.ExpandClipBoundsToPaddings(const AClipBounds: TRect): TRect;
var
  APadding: TRect;
  ALeftPadding, ATopPadding: Integer;
begin
  APadding := ActualPadding;
  ALeftPadding := Trunc(3 * DocumentModel.LayoutUnitConverter.PixelsToLayoutUnits(APadding.Left, DocumentModel.DpiX) / 4);
  ATopPadding := Trunc(3 * DocumentModel.LayoutUnitConverter.PixelsToLayoutUnits(APadding.Top, DocumentModel.DpiY) / 4);
  Result := cxRectInflate(AClipBounds, ALeftPadding, ATopPadding);
end;

procedure TdxRichEditView.GeneratePages;
var
  I: Integer;
  APage: TdxPage;
  APages: TdxPageCollection;
  AFrom, ATo: TdxDocumentModelPosition;
  ALastProcessedPage, AFirstProcessedPage: TdxPageViewInfo;
begin
  Assert(GetCurrentThreadID <> Formatter.WorkerThread.ThreadID);

  PageViewInfos.Clear; 

  APages := FormattingController.PageController.Pages;
  for I := 0 to APages.Count - 1 do
  begin
    APage := APages[I];
    if APage.PrimaryFormattingComplete then
      PageViewInfoGenerationComplete := PageViewInfoGenerator.ProcessPage(APage, I) = TdxProcessPageResult.VisiblePagesGenerationComplete;
  end;
  ALastProcessedPage := PageViewInfos.Last;
  if ALastProcessedPage <> nil then
  begin
    PageViewInfoGenerator.CalculateWidthParameters;
    VerticalScrollController.UpdateScrollBar;
    UpdateHorizontalScrollbar;
    AFirstProcessedPage := PageViewInfos.First;
    Assert(AFirstProcessedPage.Page.PrimaryFormattingComplete);
    Assert(ALastProcessedPage.Page.PrimaryFormattingComplete);
    AFrom := AFirstProcessedPage.Page.GetFirstPosition(DocumentModel.MainPieceTable);
    ATo := ALastProcessedPage.Page.GetLastPosition(DocumentModel.MainPieceTable);
    Formatter.UpdateSecondaryPositions(AFrom, ATo);
    Formatter.NotifyDocumentChanged(AFrom, ATo, TdxDocumentLayoutResetType.SecondaryLayout);
  end;
end;

procedure TdxRichEditView.RaiseZoomChanging;
begin
  if not FOnZoomChanging.Empty then
    FOnZoomChanging.Invoke(Self, nil);
end;

procedure TdxRichEditView.RaiseZoomChanged;
begin
  if not FOnZoomChanged.Empty then
    FOnZoomChanged.Invoke(Self, nil);
end;

function TdxRichEditView.GetCursorBounds: TRect;
var
  ACaretBounds: TRect;
begin
  if DocumentModel.Selection.Length > 0 then
    Exit(cxNullRect);
  if not CaretPosition.Update(TdxDocumentLayoutDetailsLevel.Character) then
    Exit(cxNullRect);
  ACaretBounds := CaretPosition.CalculateCaretBounds;
  Result := GetPhysicalBounds(CaretPosition.PageViewInfo, ACaretBounds);
end;

function TdxRichEditView.GetHyperlinkField(AHitTestResult: TdxRichEditHitTestResult): TdxField;
begin
  if (AHitTestResult.DetailsLevel < TdxDocumentLayoutDetailsLevel.Box) or
      ((AHitTestResult.Accuracy and ExactBox) = 0) then
    Result := nil
  else
    Result := AHitTestResult.PieceTable.GetHyperlinkField(AHitTestResult.Box.StartPos.RunIndex);
end;


function TdxRichEditView.GetPageViewInfoFromPoint(const APoint: TPoint; AStrictSearch: Boolean): TdxPageViewInfo;
var
  APageViewInfoRow: TdxPageViewInfoRow;
begin
  Result := nil;
  APageViewInfoRow := GetPageViewInfoRowFromPoint(APoint, True);
  if APageViewInfoRow <> nil then
    Result := APageViewInfoRow.GetPageAtPoint(APoint, AStrictSearch);
end;

function TdxRichEditView.GetPageViewInfoRowFromPoint(const APoint: TPoint; AStrictSearch: Boolean): TdxPageViewInfoRow;
begin
  Result := PageViewInfoGenerator.GetPageRowAtPoint(APoint, AStrictSearch);
end;

function TdxRichEditView.GetPhysicalBounds(APageViewInfo: TdxPageViewInfo; const ABounds: TRect): TRect;
var
  AViewBounds: TRect;
  APosition, AScreenPosition: TPoint;
  AUnitConverter: TdxDocumentLayoutUnitConverter;
  X, Y, AScreenWidth, AScreenHeight, AHorizontalRulerHeight, AVerticalRulerWidth: Integer;
begin
  AViewBounds := CreateLogicalRectangle(APageViewInfo, Bounds);
  X := Trunc((ABounds.Location.X - AViewBounds.Location.X) * ZoomFactor);
  Y := Trunc((ABounds.Location.Y - AViewBounds.Location.Y) * ZoomFactor);
  APosition.Init(X, Y);
  ABounds.Width := Trunc(ABounds.Width * ZoomFactor);
  ABounds.Height := Trunc(ABounds.Height * ZoomFactor);
  AUnitConverter := DocumentLayout.UnitConverter;
  AScreenPosition := AUnitConverter.LayoutUnitsToPixels(APosition, DocumentModel.DpiX, DocumentModel.DpiY);
  AScreenWidth := AUnitConverter.LayoutUnitsToPixels(ABounds.Width, DocumentModel.DpiX);
  AScreenHeight := AUnitConverter.LayoutUnitsToPixels(ABounds.Height, DocumentModel.DpiY);
  AHorizontalRulerHeight := 0;
  AVerticalRulerWidth := 0;
  Result.InitSize(AScreenPosition.X + AVerticalRulerWidth, 
    AScreenPosition.Y + AHorizontalRulerHeight, AScreenWidth, AScreenHeight); 
end;

function TdxRichEditView.GetVisibleSelectionEnd: TdxDocumentLogPosition;
var
  AEndVisible: Boolean;
  ASelection: TdxSelection;
  AEndRunIndex: TdxRunIndex;
  AEnd, ALastSelectedSymbol: TdxDocumentLogPosition;
begin
  ASelection := DocumentModel.Selection;
  AEnd := ASelection.&End;
  if ASelection.Length > 0 then
    ALastSelectedSymbol := Max(AEnd - 1, ASelection.PieceTable.DocumentStartLogPosition)
  else
    ALastSelectedSymbol :=  ASelection.Start;
  AEndRunIndex := ASelection.Interval.&End.RunIndex;
  if (ASelection.Interval.&End.RunOffset = 0) and (AEndRunIndex > 0) then 
    Dec(AEndRunIndex);
  AEndVisible := ASelection.PieceTable.VisibleTextFilter.IsRunVisible(AEndRunIndex);
  if AEndVisible then
    Result := AEnd
  else
    Result := ASelection.PieceTable.VisibleTextFilter.GetNextVisibleLogPosition(ALastSelectedSymbol, False);
end;

function TdxRichEditView.GetVisibleSelectionPosition: TdxDocumentLogPosition;
var
  ARunIndex: TdxRunIndex;
  ASelection: TdxSelection;
  APosition: TdxDocumentModelPosition;
  AVisibleTextFilter: TdxVisibleTextFilterBase;
  AStartRunVisible, APositionVisible: Boolean;
begin
  ASelection := DocumentModel.Selection;
  APosition := ASelection.Interval.Start;
  ARunIndex := APosition.RunIndex;
  AVisibleTextFilter := ASelection.PieceTable.VisibleTextFilter;
  AStartRunVisible := AVisibleTextFilter.IsRunVisible(ARunIndex);
  APositionVisible := (APosition.RunOffset = 0) and (ARunIndex > 0) and 
    AVisibleTextFilter.IsRunVisible(ARunIndex - 1);
  if AStartRunVisible or APositionVisible then
    Result := APosition.LogPosition
  else
    Result := AVisibleTextFilter.GetNextVisibleLogPosition(APosition, False);
end;

function TdxRichEditView.GetVisibleSelectionStart: TdxDocumentLogPosition;
var
  AStartVisible: Boolean;
  ASelection: TdxSelection;
  AStart: TdxDocumentLogPosition;
begin
  ASelection := DocumentModel.Selection;
  AStart := ASelection.Start;
  AStartVisible := ASelection.PieceTable.VisibleTextFilter.IsRunVisible(ASelection.Interval.Start.RunIndex);
  if AStartVisible then
    Result := AStart
  else
    Result := ASelection.PieceTable.VisibleTextFilter.GetNextVisibleLogPosition(AStart, False);
end;

procedure TdxRichEditView.HideCaret;
begin
  Control.HideCaret;
end;

procedure TdxRichEditView.HitTest(APage: TdxPage; ARequest: TdxRichEditHitTestRequest;
  AResult: TdxRichEditHitTestResult);
var
  ACalculator, ARowCalculator, ABoxCalculator, ADetailRowCalculator: TdxBoxHitTestCalculator;
  ARowRequest, ABoxRequest: TdxRichEditHitTestRequest;
  ABoxResult: TdxRichEditHitTestResult;
  ADetailRow: TdxDetailRow;
  AStrictHitTest: Boolean;
begin
  ACalculator := CreateHitTestCalculator(ARequest, AResult);
  try
    if ARequest.DetailsLevel <> TdxDocumentLayoutDetailsLevel.Character then
      ACalculator.CalcHitTest(APage)
    else
    begin
      ARowRequest := ARequest.Clone;
      try
        ARowRequest.DetailsLevel := TdxDocumentLayoutDetailsLevel.Row;
        ARowCalculator := CreateHitTestCalculator(ARowRequest, AResult);
        try
          ARowCalculator.CalcHitTest(APage);
          if not AResult.IsValid(TdxDocumentLayoutDetailsLevel.Row) then
            Exit;
          ABoxResult := TdxRichEditHitTestResult.Create(DocumentLayout, DocumentModel.ActivePieceTable);
          try
            ABoxResult.CopyFrom(AResult);
            ABoxRequest := ARequest.Clone;
            try
              ABoxRequest.DetailsLevel := TdxDocumentLayoutDetailsLevel.Box;
              ABoxCalculator := CreateHitTestCalculator(ABoxRequest, ABoxResult);
              try
                ABoxCalculator.CalcHitTest(ABoxResult.Row);
                if not ABoxResult.IsValid(TdxDocumentLayoutDetailsLevel.Box) then
                  Exit;
                AResult.CopyFrom(ABoxResult);
                ADetailRow := DocumentLayout.CreateDetailRow(AResult.Row);
                try
                  AStrictHitTest := (ARequest.Accuracy and ExactCharacter) <> 0;
                  ADetailRowCalculator := CreateHitTestCalculator(ARequest, AResult);
                  try
                    ADetailRowCalculator.FastHitTestCharacter(ADetailRow.Characters, AStrictHitTest);
                  finally
                    ADetailRowCalculator.Free;
                  end;
                finally
                  ADetailRow.Free;
                end;
              finally
                ABoxCalculator.Free;
              end;
            finally
              ABoxRequest.Free;
            end;
          finally
            ABoxResult.Free;
          end;
        finally
          ARowCalculator.Free;
        end;
      finally
        ARowRequest.Free;
      end;
    end;
  finally
    ACalculator.Free;
  end;
end;

function TdxRichEditView.HitTestByPhysicalPoint(const APt: TPoint; ADetailsLevel: TdxDocumentLayoutDetailsLevel): TRect;
var
  ARow: TdxRow;
  APageViewInfo: TdxPageViewInfo;
  AHitTest: TdxRichEditHitTestResult;
begin
  AHitTest := CalculateHitTest(APt, ADetailsLevel);
  try
    if AHitTest = nil then
      Exit(Rect(0, 0, 0, 0)); 
    ARow := AHitTest.Row;
    APageViewInfo := LookupPageViewInfoByPage(AHitTest.Page);
    Result := GetPhysicalBounds(APageViewInfo, ARow.Bounds);
  finally
    AHitTest.Free;
  end;
end;

function TdxRichEditView.HitTestCore(ARequest: TdxRichEditHitTestRequest;
  AStrictHitIntoPageBounds: Boolean): TdxRichEditHitTestResult;
var
  APageViewInfo: TdxPageViewInfo;
  P: TPoint;
  ABox: TdxFloatingObjectBox;
  APage: TdxPage;
  ACopy: TdxRichEditHitTestResult;
begin
  Result := TdxRichEditHitTestResult.Create(DocumentLayout, DocumentModel.ActivePieceTable);
  Result.PhysicalPoint := ARequest.PhysicalPoint;
  APageViewInfo := GetPageViewInfoFromPoint(ARequest.PhysicalPoint, AStrictHitIntoPageBounds);
  if APageViewInfo = nil then
    Exit;
  P := ARequest.PhysicalPoint;
  P.X := P.X + HorizontalScrollController.GetPhysicalLeftInvisibleWidth;
  if AStrictHitIntoPageBounds and not PerformStrictPageViewInfoHitTest(APageViewInfo, P) then
    Exit;

  Result.Page := APageViewInfo.Page;
  Result.IncreaseDetailsLevel(TdxDocumentLayoutDetailsLevel.Page);
  if ARequest.DetailsLevel <= TdxDocumentLayoutDetailsLevel.Page then
  begin
    Result.LogicalPoint := CreateLogicalPoint(APageViewInfo.ClientBounds, ARequest.PhysicalPoint);
    Result.FloatingObjectBox := CalculateFloatingObjectHitTest(Result.Page, Result.LogicalPoint,
      IsActivePieceTableFloatingObjectBox);
    Result.FloatingObjectBoxPage := Result.Page;
    Exit;
  end;
  ARequest.LogicalPoint := CreateLogicalPoint(APageViewInfo.ClientBounds, ARequest.PhysicalPoint);
  if DocumentModel.ActivePieceTable.IsTextBox then
  begin
    ABox := CalculateFloatingObjectHitTest(Result.Page, ARequest.LogicalPoint, IsActiveFloatingObjectTextBox);
    if ABox <> nil then
    begin
      APage := ABox.DocumentLayout.Pages.First;
      ARequest.SearchAnyPieceTable := True;
      Result.FloatingObjectBox := ABox;
      Result.FloatingObjectBoxPage := Result.Page;
      HitTest(APage, ARequest, Result);
    end
    else
    begin
      HitTest(APageViewInfo.Page, ARequest, Result);
      if (Result.PageArea <> nil) and (Result.PageArea.PieceTable <> Result.PieceTable) then
      begin
        ACopy := TdxRichEditHitTestResult.Create(DocumentLayout, Result.PageArea.PieceTable);
        ACopy.CopyFrom(Result);
        Result.Free;
        Result := ACopy;
      end;
    end;
  end
  else
  begin
    Result.FloatingObjectBox := CalculateFloatingObjectHitTest(Result.Page, ARequest.LogicalPoint,
      IsActivePieceTableFloatingObjectBox);
    HitTest(APageViewInfo.Page, ARequest, Result);
  end;
end;

function TdxRichEditView.IsActiveFloatingObjectTextBox(const ABox: TdxFloatingObjectBox): Boolean;
var
  ARun: TdxFloatingObjectAnchorRun;
  AContent: TdxTextBoxFloatingObjectContent;
begin
  Result := False;
  ARun := ABox.GetFloatingObjectRun;
  if ARun.Content is TdxTextBoxFloatingObjectContent then
  begin
    AContent := TdxTextBoxFloatingObjectContent(ARun.Content);
    Result := (ABox.DocumentLayout <> nil) and (AContent <> nil) and (AContent.TextBox.PieceTable = DocumentModel.ActivePieceTable);
  end;
  Assert(False, 'need result value');
end;

function TdxRichEditView.IsActivePieceTableFloatingObjectBox(const ABox: TdxFloatingObjectBox): Boolean;
begin
  Result := ABox.PieceTable = DocumentModel.ActivePieceTable;
end;

function TdxRichEditView.IsFloatingObjectHit(ABox: TdxFloatingObjectBox; APoint: TPoint): Boolean;
begin
  Result := ABox.Bounds.Contains(ABox.TransformPointBackward(APoint));
end;

function TdxRichEditView.IsPaddingStored: Boolean;
begin
  Result := not Padding.IsDefault;
end;

function TdxRichEditView.IsPrimaryFormattingCompleteForVisibleHeight2(
  const ACurrentFormatterPosition: TdxDocumentModelPosition): Boolean;
begin
  Result := PageViewInfoGenerationComplete;
end;

procedure TdxRichEditView.PaddingChanged(Sender: TObject);
begin
  OnResizeCore; 
end;

function TdxRichEditView.LookupPageViewInfoByPage(APage: TdxPage): TdxPageViewInfo;
var
  APageViewInfos: TdxPageViewInfoCollection;
  I: Integer;
begin
  APageViewInfos := PageViewInfos;
  for I := 0 to APageViewInfos.Count - 1 do
    if APageViewInfos[I].Page = APage then
      Exit(APageViewInfos[I]);
  Result := nil;
end;

procedure TdxRichEditView.NotifyDocumentChanged(APieceTable: TdxPieceTable; AChanges: TdxDocumentModelDeferredChanges;
  ADebugSuppressControllerReset: Boolean);
var
  ADocumentLayoutResetType: TdxDocumentLayoutResetType;
  AFrom, ATo: TdxDocumentModelPosition;
  ASecondaryFrom: TdxDocumentModelPosition;
begin
  if TdxDocumentModelChangeAction.ActivePieceTableChanged in AChanges.ChangeActions then
    OnActivePieceTableChanged;
  ADocumentLayoutResetType := CalculateDocumentLayoutResetType(AChanges);
  if ADocumentLayoutResetType = TdxDocumentLayoutResetType.AllPrimaryLayout then
  begin
    AChanges.SetChangeParagraphStart(0);
    AChanges.SetChangeParagraphEnd(APieceTable.Paragraphs.Count - 1); 
  end;
  CaretPosition.Invalidate;
  if TdxDocumentModelChangeAction.ResetCaretInputPositionFormatting in AChanges.ChangeActions then
    CaretPosition.InvalidateInputPosition;
  if TdxDocumentModelChangeAction.ValidateSelectionInterval in AChanges.ChangeActions then
    ValidateSelectionInterval;
  AFrom := CalculateResetFromPosition(APieceTable, AChanges, ADocumentLayoutResetType);
  ATo := AChanges.ChangeEnd;
  if ATo.LogPosition < AFrom.LogPosition then
  begin
    Assert(ADocumentLayoutResetType = TdxDocumentLayoutResetType.None);
    ATo := AFrom;
  end;
  if (ADocumentLayoutResetType = TdxDocumentLayoutResetType.AllPrimaryLayout) and not ADebugSuppressControllerReset then
  begin
    ResetPages(TdxPageGenerationStrategyType.RunningHeight);
    FormattingController.Reset(False);
    Formatter.UpdateSecondaryPositions(AFrom, ATo);
    Formatter.NotifyDocumentChanged(AFrom, ATo, ADocumentLayoutResetType);
  end
  else
    if ADocumentLayoutResetType = TdxDocumentLayoutResetType.PrimaryLayoutFormPosition then
    begin
      APieceTable.ResetParagraphs(AFrom.ParagraphIndex, ATo.ParagraphIndex);
      ResetPages(TdxPageGenerationStrategyType.RunningHeight);
      UnsubscribePageFormattingComplete;
      try
        ASecondaryFrom := FormattingController.ResetFrom(AFrom, False);
      finally
        SubscribePageFormattingComplete;
      end;
      Formatter.UpdateSecondaryPositions(ASecondaryFrom, ATo);
      Formatter.NotifyDocumentChanged(AFrom, ATo, ADocumentLayoutResetType);
      GeneratePages;
    end
    else
    begin
      Formatter.UpdateSecondaryPositions(AFrom, ATo);
      Formatter.NotifyDocumentChanged(AFrom, ATo, ADocumentLayoutResetType);
    end;
end;

procedure TdxRichEditView.OnActivePieceTableChanged;
begin
  FreeAndNil(FSelectionLayout);
  FreeAndNil(FCaretPosition);
  if DocumentModel.ActivePieceTable.IsTextBox then
  begin
    NotImplemented;
  end
  else
  begin
    SelectionLayout := TdxSelectionLayout.Create(Self, 0);
    CaretPosition := TdxCaretPosition.Create(Self, 0);
  end;
end;

procedure TdxRichEditView.AfterCreateDetailRowHandler(ASender: TObject);
begin
  ResumeFormatting;
end;

procedure TdxRichEditView.BeforeCreateDetailRowHandler(ASender: TObject);
begin
  SuspendFormatting;
end;

procedure TdxRichEditView.OnBeginDocumentUpdate;
begin
  SuspendFormatting;
end;

procedure TdxRichEditView.OnEndDocumentUpdate;
begin
  ResumeFormatting;
end;

procedure TdxRichEditView.OnHorizontalScroll;
begin
  PageViewInfoGenerator.LeftInvisibleWidth := HorizontalScrollController.GetLeftInvisibleWidth;
  Control.RedrawEnsureSecondaryFormattingComplete(TdxRefreshAction.Transforms);
end;

procedure TdxRichEditView.OnLayoutUnitChanged;
var
  APieceTable: TdxPieceTable;
  AFrom, ATo: TdxDocumentModelPosition;
begin
  CaretPosition.Invalidate;
  CaretPosition.InvalidateInputPosition;
  PageViewInfoGenerator.OnLayoutUnitChanged(DocumentModel.LayoutUnitConverter);
  FormattingController.Reset(False);
  APieceTable := DocumentModel.MainPieceTable;
  AFrom := TdxDocumentModelPosition.Create(APieceTable);
  ATo := TdxDocumentModelPosition.FromParagraphEnd(APieceTable, APieceTable.Paragraphs.Last.Index);
  Formatter.UpdateSecondaryPositions(AFrom, ATo);
  Formatter.NotifyDocumentChanged(AFrom, ATo, TdxDocumentLayoutResetType.AllPrimaryLayout);
end;

procedure TdxRichEditView.OnLayoutUnitChanging;
begin
  ResetPages(TdxPageGenerationStrategyType.FirstPageOffset);
  PageViewInfoGenerator.OnLayoutUnitChanging(DocumentModel.LayoutUnitConverter);
end;

procedure TdxRichEditView.OnPageFormattingComplete(ASender: TObject; E: TdxPageFormattingCompleteEventArgs);
var
  ALastPage: TdxPage;
  ALastPageViewInfo: TdxPageViewInfo;
  APrevPageViewInfoGenerationComplete: Boolean;
begin
  ALastPage := E.Page;
  Assert(ALastPage = FormattingController.PageController.Pages.Last);
  Assert(ALastPage.PrimaryFormattingComplete);
  ALastPageViewInfo := PageViewInfos.Last;
  if (ALastPageViewInfo <> nil) and (ALastPageViewInfo.Page = ALastPage) then
  begin
    if E.DocumentFormattingComplete then
      Control.UpdateUIFromBackgroundThread(VerticalScrollController.ScrollBarAdapter.EnsureSynchronized);
    Exit;
  end;
  APrevPageViewInfoGenerationComplete := PageViewInfoGenerationComplete;
  PageViewInfoGenerationComplete := PageViewInfoGenerator.ProcessPage(ALastPage, FormattingController.PageController.Pages.Count - 1) = TdxProcessPageResult.VisiblePagesGenerationComplete;
  if (PageViewInfoGenerationComplete <> APrevPageViewInfoGenerationComplete) or E.DocumentFormattingComplete then
  begin
    PageViewInfoGenerator.CalculateWidthParameters;
    Control.UpdateUIFromBackgroundThread(UpdateHorizontalScrollbar);
  end;
  if PageViewInfoGenerationComplete then
  begin
    Formatter.UpdateSecondaryPositions(Formatter.SecondaryLayoutStart, ALastPage.GetLastPosition(DocumentModel.MainPieceTable));
    Formatter.ResetSecondaryLayout;
  end;
  Control.UpdateUIFromBackgroundThread(VerticalScrollController.UpdateScrollBar);
  if E.DocumentFormattingComplete then
    Control.UpdateUIFromBackgroundThread(VerticalScrollController.ScrollBarAdapter.EnsureSynchronized);
end;

procedure TdxRichEditView.OnPageFormattingStarted(ASender: TObject; E: TdxPageFormattingCompleteEventArgs);
var
  ALastPage: TdxPage;
begin
  ALastPage := E.Page;
  Assert(ALastPage = FormattingController.PageController.Pages.Last); 
  if not PageViewInfoGenerationIsAboutToComplete then
    PageViewInfoGenerationIsAboutToComplete := (PageViewInfoGenerator.PreProcessPage(ALastPage, FormattingController.PageController.Pages.Count - 1) = TdxProcessPageResult.VisiblePagesGenerationComplete);
end;

procedure TdxRichEditView.OnResetFormattingFromCurrentArea(ASender: TObject; E: TdxEventArgs);
var
  APieceTable: TdxPieceTable;
  AFrom, ATo: TdxDocumentModelPosition;
  ASecondaryFrom: TdxDocumentModelPosition;
begin
  APieceTable := DocumentModel.ActivePieceTable;
  AFrom := EnsureTopLevelParagraph(DocumentLayout.Pages.Last.Areas.Last.GetFirstPosition(APieceTable));
  ATo := TdxDocumentModelPosition.FromParagraphEnd(APieceTable, APieceTable.Paragraphs.Count - 1);
  UnsubscribePageFormattingComplete;
  try
    ASecondaryFrom := FormattingController.ResetFrom(AFrom, False);
  finally
    SubscribePageFormattingComplete;
  end;
  Formatter.Restart(AFrom, ATo, ASecondaryFrom);
end;

procedure TdxRichEditView.OnResetSecondaryFormattingForPage(ASender: TObject;
  E: TdxResetSecondaryFormattingForPageArgs);
begin
  Formatter.ResetSecondaryFormattingForPage(E.Page, E.PageIndex);
end;

procedure TdxRichEditView.OnResize(const ABounds: TRect; AEnsureCaretVisibleOnResize: Boolean);
begin
  PerformResize(ABounds);
  Control.InnerControl.BeginDocumentRendering;
  Control.InnerControl.EndDocumentRendering;
  SetOptimalHorizontalScrollbarPosition;
end;

procedure TdxRichEditView.OnResizeCore;
begin
end;

procedure TdxRichEditView.OnSelectionChanged;
begin
  HideCaret;
  CaretPosition.Invalidate;
  CaretPosition.InvalidateInputPosition;
  SelectionLayout.Invalidate;
  ShowCaret;
end;

procedure TdxRichEditView.OnVerticalScroll;
begin
  SuspendFormatting;
  try
    ResetPages(TdxPageGenerationStrategyType.RunningHeight);
    PageViewInfoGenerator.TopInvisibleHeight := VerticalScrollController.GetTopInvisibleHeight;
    GeneratePages;
  finally
    ResumeFormatting;
  end;
  Control.RedrawEnsureSecondaryFormattingComplete(TdxRefreshAction.Transforms);
end;

procedure TdxRichEditView.OnViewPaddingChanged;
begin
  Control.OnViewPaddingChanged;
end;

procedure TdxRichEditView.OnZoomFactorChanged(AOldValue, ANewValue: Single);
begin
  PerformZoomFactorChanged;
  RaiseZoomChanged;
  Control.RedrawEnsureSecondaryFormattingComplete(TdxRefreshAction.Zoom);
  Control.InnerControl.RaiseUpdateUI;
end;

procedure TdxRichEditView.OnZoomFactorChangedCore;
begin
end;

procedure TdxRichEditView.OnZoomFactorChanging;
begin
  RaiseZoomChanging;
end;

procedure TdxRichEditView.PerformResize(const ABounds: TRect);
begin
  DocumentModel.BeginUpdate;
  try
    Bounds := ABounds;
    ResetPages(TdxPageGenerationStrategyType.FirstPageOffset);
    OnResizeCore;
    Assert(PageViewInfoGenerator.ActiveGenerator.Pages.Count <= 0);
    GeneratePages;
  finally
    DocumentModel.EndUpdate;
  end;
end;

function TdxRichEditView.PerformStrictPageViewInfoHitTest(APageViewInfo: TdxPageViewInfo; const APt: TPoint): Boolean;
begin
  Result := cxRectPtIn(APageViewInfo.ClientBounds, APt);
end;

procedure TdxRichEditView.PerformZoomFactorChanged;
begin
  SuspendFormatting;
  try
    ResetPages(TdxPageGenerationStrategyType.FirstPageOffset);
    OnZoomFactorChangedCore;
    GeneratePages;
  finally
    ResumeFormatting;
  end;
  Control.InnerControl.BeginDocumentRendering;
  Control.InnerControl.EndDocumentRendering;
  SetOptimalHorizontalScrollbarPosition;
end;

procedure TdxRichEditView.ResetPages(AStrategy: TdxPageGenerationStrategyType);
begin
  Assert(GetCurrentThreadID <> Formatter.WorkerThread.ThreadID);
  CaretPosition.InvalidatePageViewInfo;

  ViewInfo.Clear; 
  PageViewInfoGenerator.ActiveGenerator.PageRows.Clear; 

  PageViewInfoGenerationIsAboutToComplete := False;
  PageViewInfoGenerationComplete := False;
  PageViewInfoGenerator.Reset(AStrategy);

end;

procedure TdxRichEditView.ResumeFormatting;
begin
  Dec(FSuspendFormattingCount);
  if FSuspendFormattingCount = 0 then
  begin
    Formatter.EndDocumentUpdate;
  end;
end;

procedure TdxRichEditView.ScrollToPage(APageIndex: Integer);
begin
  SuspendFormatting;
  try
    ResetPages(TdxPageGenerationStrategyType.FirstPageOffset);
    PageViewInfoGenerator.FirstPageOffsetAnchor.PageIndex := APageIndex;
    PageViewInfoGenerator.FirstPageOffsetAnchor.VerticalOffset := 0;
    GeneratePages;
    ResetPages(TdxPageGenerationStrategyType.RunningHeight);
    GeneratePages;
  finally
    ResumeFormatting;
  end;
  VerticalScrollController.UpdateScrollBar;
  Control.RedrawEnsureSecondaryFormattingComplete(TdxRefreshAction.Transforms);
end;

procedure TdxRichEditView.SetAllowDisplayLineNumbers(const Value: Boolean);
begin
  if FAllowDisplayLineNumbers = Value then
    Exit;
  SetAllowDisplayLineNumbersCore(Value);
  NotImplemented;
end;

function TdxRichEditView.CreatePadding: TdxRichEditControlPadding;
begin
  Result := TdxRichEditControlPadding.Create(Self, 0);
end;

function TdxRichEditView.GetActualPadding: TRect;
begin
  if FPadding <> nil then
    Result.Init(FPadding.Left, FPadding.Top, FPadding.Right, FPadding.Bottom)
  else
    Result.Empty;
end;

procedure TdxRichEditView.SetAllowDisplayLineNumbersCore(Value: Boolean);
begin
  FAllowDisplayLineNumbers := Value;
end;

function TdxRichEditView.GetDocumentModel: TdxDocumentModel;
begin
  Result := FControl.InnerControl.DocumentModel;
end;

function TdxRichEditView.GetFixedLeftTextBorderOffset: Integer;
begin
  Result := 0;
end;

function TdxRichEditView.GetFormatter: TdxBackgroundFormatter;
begin
  Result := FControl.InnerControl.Formatter;
end;

procedure TdxRichEditView.SetBackColor(const Value: TColor);
begin
  if FBackColor <> Value then
  begin
    FBackColor := Value;
    RichEditControl.Invalidate;
  end;
end;

procedure TdxRichEditView.SetBounds(const Value: TRect);
begin
  if cxRectIsEqual(FBounds, Value) then
    Exit;
  if Value.Width < MinWidth then
    Value.Width := MinWidth;
  if Value.Height < MinHeight then
    Value.Height := MinHeight;
  FBounds := Value;
end;

procedure TdxRichEditView.SetOptimalHorizontalScrollbarPosition;
begin
  DocumentModel.BeginUpdate;
  try
    SetOptimalHorizontalScrollbarPositionCore;
    if DocumentModel.IsUpdateLocked then
      DocumentModel.MainPieceTable.ApplyChangesCore([TdxDocumentModelChangeAction.Redraw], dxRunIndexDontCare,
        dxRunIndexDontCare);
  finally
    DocumentModel.EndUpdate;
  end;
end;

procedure TdxRichEditView.SetOptimalHorizontalScrollbarPositionCore;
var
  APageViewInfo: TdxPageViewInfo;
begin
  if PageViewInfoGenerator.VisibleWidth >= PageViewInfoGenerator.TotalWidth then
    PageViewInfoGenerator.LeftInvisibleWidth := 0
  else
  begin
    APageViewInfo := ChoosePageViewInfoForOptimalHorizontalScrollbarPosition;
    if APageViewInfo = nil then
      PageViewInfoGenerator.LeftInvisibleWidth := 0
    else
      PageViewInfoGenerator.LeftInvisibleWidth := Max(0, CalculateOptimalHorizontalScrollbarPosition(APageViewInfo));
  end;
  HorizontalScrollController.UpdateScrollBar;
end;

procedure TdxRichEditView.SetPageViewInfoGenerationComplete(const Value: Boolean);
begin
  FPageViewInfoGenerationComplete := Value;
end;

function TdxRichEditView.GetPageViewInfos: TdxPageViewInfoCollection;
begin
  Result := ViewInfo.PageViewInfos;
end;

procedure TdxRichEditView.SetPadding(Value: TdxRichEditControlPadding);
begin
  FPadding.Assign(Value);
end;

procedure TdxRichEditView.SetZoomFactor(Value: Single);
var
  AOldValue: Single;
begin
  if Value < MinZoomFactor then
    Value := MinZoomFactor;
  if Value > MaxZoomFactor then
    Value := MaxZoomFactor;
  if FZoomFactor = Value then
    Exit;
  OnZoomFactorChanging;
  AOldValue := FZoomFactor;
  FZoomFactor := Value;
  OnZoomFactorChanged(AOldValue, FZoomFactor);
end;

procedure TdxRichEditView.ShowCaret;
begin
  Control.ShowCaret;
end;

procedure TdxRichEditView.SubscribeDocumentLayoutEvents;
begin
  DocumentLayout.OnBeforeCreateDetailRow.Add(BeforeCreateDetailRowHandler);
  DocumentLayout.OnAfterCreateDetailRow.Add(AfterCreateDetailRowHandler);
end;

procedure TdxRichEditView.SubscribePageFormattingComplete;
begin
  FormattingController.PageFormattingStarted.Add(OnPageFormattingStarted);
  FormattingController.PageFormattingComplete.Add(OnPageFormattingComplete);
end;

procedure TdxRichEditView.SuspendFormatting;
begin
  if FSuspendFormattingCount = 0 then
  begin
    Formatter.BeginDocumentUpdate;
  end;
  Inc(FSuspendFormattingCount);
end;

procedure TdxRichEditView.UnsubscribeDocumentLayoutEvents;
begin
  DocumentLayout.OnBeforeCreateDetailRow.Remove(BeforeCreateDetailRowHandler);
  DocumentLayout.OnAfterCreateDetailRow.Remove(AfterCreateDetailRowHandler);
end;

procedure TdxRichEditView.UnsubscribePageFormattingComplete;
begin
  FormattingController.PageFormattingStarted.Remove(OnPageFormattingStarted);
  FormattingController.PageFormattingComplete.Remove(OnPageFormattingComplete);
end;

procedure TdxRichEditView.UpdateHorizontalScrollbar;
begin
  HorizontalScrollController.UpdateScrollBar;
end;

procedure TdxRichEditView.ValidateSelectionInterval;
var
  ASelection: TdxSelection;
  AStart, AEnd, AVisibleStart, AVisibleEnd, ANewPosition: TdxDocumentLogPosition;
  ANonEmptyVisibleSelection: Boolean;
begin
  ASelection := DocumentModel.Selection;
  AStart := ASelection.Start;
  AEnd := ASelection.&End;
  if ASelection.Length > 0 then
  begin
    AVisibleStart := GetVisibleSelectionStart;
    AVisibleEnd := GetVisibleSelectionEnd;
    ANonEmptyVisibleSelection := (AStart < AEnd) and (AVisibleStart < AVisibleEnd) or
      (AStart > AEnd) and (AVisibleStart > AVisibleEnd);
    if not ANonEmptyVisibleSelection then
      AVisibleEnd := AVisibleStart;
    DocumentModel.Selection.Start := AVisibleStart;
    DocumentModel.Selection.&End := AVisibleEnd;
  end
  else
  begin
    ANewPosition := GetVisibleSelectionPosition;
    DocumentModel.Selection.Start := ANewPosition;
    DocumentModel.Selection.&End := ANewPosition;
  end;
end;

{ TdxRichEditCustomViews }

constructor TdxRichEditCustomViewRepository.Create(ARichEditControl: IdxRichEditControl);
begin
  inherited Create(ARichEditControl.GetRichEditControl as TPersistent);
  FRichEditControl := ARichEditControl;
  FViews := TdxFastObjectList.Create;
  CreateViews;
end;

destructor TdxRichEditCustomViewRepository.Destroy;
begin
  FreeAndNil(FViews);
  inherited Destroy;
end;

procedure TdxRichEditCustomViewRepository.AddView(AView: TdxRichEditView);
begin
  FViews.Add(AView);
end;

function TdxRichEditCustomViewRepository.GetView(Index: Integer): TdxRichEditView;
begin
  Result := TdxRichEditView(FViews[Index]);
end;

function TdxRichEditCustomViewRepository.GetViewCount: Integer;
begin
  Result := FViews.Count;
end;

{ TdxOfficeScrollControllerBase }

destructor TdxOfficeScrollControllerBase.Destroy;
begin
  FreeAndNil(FScrollBarAdapter);
  inherited Destroy;
end;

procedure TdxOfficeScrollControllerBase.Activate;
begin
  ScrollBarAdapter.Activate;
  SubscribeScrollBarAdapterEvents;
end;

procedure TdxOfficeScrollControllerBase.Deactivate;
begin
  ScrollBarAdapter.Deactivate;
  UnsubscribeScrollBarAdapterEvents;
end;

procedure TdxOfficeScrollControllerBase.EmulateScroll(AEventType: TdxScrollEventType);
var
  AArgs: TdxScrollEventArgs;
begin
  AArgs := ScrollBarAdapter.CreateEmulatedScrollEventArgs(AEventType);
  OnScroll(TObject(ScrollBar), AArgs); 
end;

function TdxOfficeScrollControllerBase.GetSupportsDeferredUpdate: Boolean;
begin
  Result := False;
end;

procedure TdxOfficeScrollControllerBase.Initialize;
begin
  FScrollBarAdapter := CreateScrollBarAdapter;
  ScrollBarAdapter.RefreshValuesFromScrollBar;
end;

function TdxOfficeScrollControllerBase.IsScrollPossible: Boolean;
begin
  Result := (ScrollBarAdapter.Maximum - ScrollBarAdapter.Minimum > ScrollBarAdapter.LargeChange) and
    (ScrollBarAdapter.Value >= ScrollBarAdapter.Minimum) and
    (ScrollBarAdapter.Value <= ScrollBarAdapter.Maximum - ScrollBarAdapter.LargeChange + 1);
end;

procedure TdxOfficeScrollControllerBase.SubscribeScrollBarAdapterEvents;
begin
  ScrollBarAdapter.Scroll.Add(OnScroll);
end;

procedure TdxOfficeScrollControllerBase.SynchronizeScrollbar;
begin
  UnsubscribeScrollBarAdapterEvents;
  try
    ScrollBarAdapter.ApplyValuesToScrollBar;
  finally
    SubscribeScrollBarAdapterEvents;
  end;
end;

procedure TdxOfficeScrollControllerBase.UnsubscribeScrollBarAdapterEvents;
begin
  ScrollBarAdapter.Scroll.Remove(OnScroll);
end;

procedure TdxOfficeScrollControllerBase.UpdateScrollBar;
begin
  if ScrollBar = nil then
    Exit;
  UpdateScrollBarAdapter;
  SynchronizeScrollbar;
end;

{ TdxRichEditViewScrollControllerBase }

constructor TdxRichEditViewScrollControllerBase.Create(AView: TdxRichEditView);
begin
  inherited Create;
  Assert(AView <> nil, 'view = nil');
  FView := AView;
  Initialize;
end;

procedure TdxRichEditViewScrollControllerBase.UpdateScrollBar;
begin
  if ScrollBar = nil then
    Exit;
  if View <> View.Control.InnerControl.ActiveView then 
    Exit;
  Assert(View = View.Control.InnerControl.ActiveView, 'View <> View.Control.InnerControl.ActiveView');
  inherited UpdateScrollBar;
end;

{ TdxRichEditViewHorizontalScrollController }

function TdxRichEditViewHorizontalScrollController.CreateScrollBarAdapter: TdxScrollBarAdapter;
begin
  Result := TdxHorizontalScrollBarAdapter.Create(ScrollBar, View.Control.CreatePlatformSpecificScrollBarAdapter);
end;

function TdxRichEditViewHorizontalScrollController.GetLeftInvisibleWidth: Int64;
begin
  Result := ScrollBarAdapter.Value;
end;

function TdxRichEditViewHorizontalScrollController.GetPhysicalLeftInvisibleWidth: Integer;
begin
  if ScrollBar = nil then
    Exit(0);
  Result := Round(View.PageViewInfoGenerator.LeftInvisibleWidth * View.ZoomFactor);
end;

function TdxRichEditViewHorizontalScrollController.GetScrollBar: IdxOfficeScrollbar;
begin
  Result := nil;
  if (View.Control = nil) or (View.Control.InnerControl = nil) then
    Exit;
  Result := View.RichEditControl.HorizontalScrollBar;
end;

procedure TdxRichEditViewHorizontalScrollController.OnScroll(ASender: TObject;
  E: TdxPlatformIndependentScrollEventArgs);
begin
  OnScrollCore(E);
  View.OnHorizontalScroll;
end;

procedure TdxRichEditViewHorizontalScrollController.OnScrollCore(E: TdxPlatformIndependentScrollEventArgs);
begin
end;

procedure TdxRichEditViewHorizontalScrollController.ScrollByLeftInvisibleWidthDelta(ALeftInvisibleWidthDelta: Int64);
begin
  ScrollToAbsolutePosition(ScrollBarAdapter.Value + ALeftInvisibleWidthDelta);
end;

procedure TdxRichEditViewHorizontalScrollController.ScrollToAbsolutePosition(AValue: Int64);
begin
  ScrollBarAdapter.BeginUpdate;
  try
    ScrollBarAdapter.Maximum := View.PageViewInfoGenerator.TotalWidth - 1;
    ScrollBarAdapter.Value := AValue;
  finally
    ScrollBarAdapter.EndUpdate;
  end;
  SynchronizeScrollbar;
  ScrollBarAdapter.EnsureSynchronized();
end;

procedure TdxRichEditViewHorizontalScrollController.UpdateScrollBarAdapter;
var
  AGenerator: TdxPageViewInfoGenerator;
begin
  AGenerator := View.PageViewInfoGenerator;
  if AGenerator.VisibleWidth < 0 then
    Exit;
  ScrollBarAdapter.BeginUpdate;
  try
    ScrollBarAdapter.Minimum := 0;
    ScrollBarAdapter.Maximum := AGenerator.TotalWidth - 1;
    ScrollBarAdapter.LargeChange := AGenerator.VisibleWidth;
    ScrollBarAdapter.Value := AGenerator.LeftInvisibleWidth;
    ScrollBarAdapter.Enabled := IsScrollPossible;
  finally
    ScrollBarAdapter.EndUpdate;
  end;
end;

{ TdxRichEditViewVerticalScrollController }


procedure TdxRichEditViewVerticalScrollController.ApplyNewScrollValue(AValue: Integer);
begin
  dxAbstractError;
end;

procedure TdxRichEditViewVerticalScrollController.ApplyNewScrollValueToScrollEventArgs(
  E: TdxPlatformIndependentScrollEventArgs; AValue: Integer);
begin
  dxAbstractError;
end;

function TdxRichEditViewVerticalScrollController.CalculateScrollDelta(
  E: TdxPlatformIndependentScrollEventArgs): Integer;
begin
  dxAbstractError;
  Result := 0;
end;

function TdxRichEditViewVerticalScrollController.CreateScrollBarAdapter: TdxScrollBarAdapter;
begin
  Result := TdxVerticalScrollBarAdapter.Create(ScrollBar, View.Control.CreatePlatformSpecificScrollBarAdapter);
end;

function TdxRichEditViewVerticalScrollController.IsScrollTypeValid(E: TdxPlatformIndependentScrollEventArgs): Boolean;
begin
  dxAbstractError;
  Result := True;
end;

function TdxRichEditViewVerticalScrollController.GetScrollBar: IdxOfficeScrollbar;
begin
  Result := nil;
  if (View.Control = nil) or (View.Control.InnerControl = nil) then
    Exit;
  Result := View.Control.InnerControl.VerticalScrollBar;
end;

function TdxRichEditViewVerticalScrollController.GetTopInvisibleHeight: Int64;
begin
  Result := ScrollBarAdapter.Value;
end;

procedure TdxRichEditViewVerticalScrollController.OnScroll(ASender: TObject; E: TdxPlatformIndependentScrollEventArgs);
var
  ADelta, AValue: Integer;
begin
  if UpdatePageNumberOnScroll(E) then
    Exit;
  if not IsScrollTypeValid(E) then
    Exit;
  ADelta := CalculateScrollDelta(E);
  AValue := E.NewValue; 
  if ADelta <> 0 then
  begin
    if ADelta = 1 then
      AValue := ScrollLineDown
    else
      if ADelta = -1 then
        AValue := ScrollLineUp
      else
        ApplyNewScrollValue(AValue);
    View.OnVerticalScroll;
    ApplyNewScrollValueToScrollEventArgs(E, AValue);
  end;
end;

procedure TdxRichEditViewVerticalScrollController.ScrollByTopInvisibleHeightDelta(ATopInvisibleHeightDelta: Int64);
begin
  ScrollToAbsolutePosition(ScrollBarAdapter.Value + ATopInvisibleHeightDelta);
end;

function TdxRichEditViewVerticalScrollController.ScrollLineDown: Integer;
begin
  Result := ScrollLineUpDown(View.DocumentLayout.UnitConverter.DocumentsToLayoutUnits(50));
end;

function TdxRichEditViewVerticalScrollController.ScrollLineUp: Integer;
begin
  Result := ScrollLineUpDown(-View.DocumentLayout.UnitConverter.DocumentsToLayoutUnits(50));
end;

function TdxRichEditViewVerticalScrollController.ScrollLineUpDown(APhysicalOffset: Integer): Integer;
var
  ACommand: TdxScrollVerticallyByPhysicalOffsetCommand;
begin
  ACommand := TdxScrollVerticallyByPhysicalOffsetCommand.Create(View.Control);
  try
    ACommand.UpdateScrollBarBeforeExecution := False;
    ACommand.PhysicalOffset := APhysicalOffset;
    ACommand.Execute;
    Result := ScrollBarAdapter.GetRawScrollBarValue;
  finally
    ACommand.Free;
  end;
end;

function TdxRichEditViewVerticalScrollController.ScrollPageDown: Boolean;
begin
  Result := ScrollBarAdapter.SetRawScrollBarValue(ScrollBarAdapter.GetPageDownRawScrollBarValue);
end;

function TdxRichEditViewVerticalScrollController.ScrollPageUp: Boolean;
begin
  Result := ScrollBarAdapter.SetRawScrollBarValue(ScrollBarAdapter.GetPageUpRawScrollBarValue);
end;

procedure TdxRichEditViewVerticalScrollController.ScrollToAbsolutePosition(AValue: Int64);
begin
  ScrollBarAdapter.BeginUpdate;
  try
    ScrollBarAdapter.Maximum := View.PageViewInfoGenerator.TotalHeight - 1;
    ScrollBarAdapter.Value := AValue;
  finally
    ScrollBarAdapter.EndUpdate;
  end;
  SynchronizeScrollbar;
  ScrollBarAdapter.EnsureSynchronized;
end;

function TdxRichEditViewVerticalScrollController.UpdatePageNumberOnScroll(
  E: TdxPlatformIndependentScrollEventArgs): Boolean;
begin
  dxAbstractError;
  Result := True;
end;

procedure TdxRichEditViewVerticalScrollController.UpdateScrollBarAdapter;
var
  AGenerator: TdxPageViewInfoGenerator;
begin
  AGenerator := View.PageViewInfoGenerator;
  if AGenerator.VisibleHeight < 0 then
    Exit; 

  ScrollBarAdapter.BeginUpdate;
  try
    ScrollBarAdapter.Minimum := 0;
    ScrollBarAdapter.Maximum := AGenerator.TotalHeight - 1;
    ScrollBarAdapter.LargeChange := AGenerator.VisibleHeight;
    ScrollBarAdapter.Value := AGenerator.TopInvisibleHeight;
    ScrollBarAdapter.Enabled := IsScrollPossible;
  finally
    ScrollBarAdapter.EndUpdate;
  end;
end;

{ TdxScrollBarAdapter }

constructor TdxScrollBarAdapter.Create(const AScrollBar: IdxOfficeScrollbar; AAdapter: IdxPlatformSpecificScrollBarAdapter);
begin
  inherited Create;
  FFactor := 1.0;
  FScrollBar := AScrollBar;
  FAdapter := AAdapter;
  FBatchUpdateHelper := TdxBatchUpdateHelper.Create(Self);
end;

destructor TdxScrollBarAdapter.Destroy;
begin
  FreeAndNil(FBatchUpdateHelper);
  inherited Destroy;
end;

procedure TdxScrollBarAdapter.Activate;
begin
  RefreshValuesFromScrollBar;
  SubscribeScrollbarEvents;
end;

procedure TdxScrollBarAdapter.ApplyValuesToScrollBar;
begin
  if DeferredScrollBarUpdate then
    Synchronized := False
  else
    ApplyValuesToScrollBarCore;
end;

procedure TdxScrollBarAdapter.ApplyValuesToScrollBarCore;
begin
  Adapter.ApplyValuesToScrollBarCore(Self);
  Synchronized := True;
end;

procedure TdxScrollBarAdapter.BeginUpdate;
begin
  FBatchUpdateHelper.BeginUpdate;
end;

procedure TdxScrollBarAdapter.CancelUpdate;
begin
  FBatchUpdateHelper.CancelUpdate;
end;

function TdxScrollBarAdapter.CreateEmulatedScrollEventArgs(AEventType: TdxScrollEventType): TdxScrollEventArgs;
begin
  EnsureSynchronized;
  case AEventType of
    TdxScrollEventType.First:
      Result := TdxScrollEventArgs.Create(AEventType, ScrollBar.Minimum);
    TdxScrollEventType.Last:
      Result := FAdapter.CreateLastScrollEventArgs(Self);
    TdxScrollEventType.SmallIncrement:
      Result := TdxScrollEventArgs.Create(AEventType, ScrollBar.Value + ScrollBar.SmallChange);
    TdxScrollEventType.SmallDecrement:
      Result := TdxScrollEventArgs.Create(AEventType, ScrollBar.Value - ScrollBar.SmallChange);
    else
      raise Exception.Create('ThrowInternalException');
  end;
end;

procedure TdxScrollBarAdapter.Deactivate;
begin
  UnsubscribeScrollbarEvents;
end;

procedure TdxScrollBarAdapter.EndUpdate;
begin
  FBatchUpdateHelper.EndUpdate;
end;

procedure TdxScrollBarAdapter.EnsureSynchronized;
begin
  EnsureSynchronizedCore;
end;

function TdxScrollBarAdapter.EnsureSynchronizedCore: Boolean;
begin
  Result := False;
  if ShouldSynchronize then
  begin
    ApplyValuesToScrollBarCore;
    Result := True;
  end;
  Assert(Synchronized);
end;

function TdxScrollBarAdapter.GetBatchUpdateHelper: TdxBatchUpdateHelper;
begin
  Result := FBatchUpdateHelper;
end;

function TdxScrollBarAdapter.GetIsUpdateLocked: Boolean;
begin
  Result := FBatchUpdateHelper.IsUpdateLocked;
end;

function TdxScrollBarAdapter.GetPageDownRawScrollBarValue: Integer;
begin
  EnsureSynchronized;
  Result := Adapter.GetPageDownRawScrollBarValue(Self);
end;

function TdxScrollBarAdapter.GetPageUpRawScrollBarValue: Integer;
begin
  EnsureSynchronized;
  Result := Adapter.GetPageUpRawScrollBarValue(Self);
end;

function TdxScrollBarAdapter.GetRawScrollBarValue: Integer;
begin
  EnsureSynchronized;
  Result := Adapter.GetRawScrollBarValue(Self);
end;

function TdxScrollBarAdapter.GetScroll: PdxScrollEvents;
begin
  Result := @FOnScroll;
end;

procedure TdxScrollBarAdapter.OnBeginUpdate;
begin
end;

procedure TdxScrollBarAdapter.OnCancelUpdate;
begin
end;

procedure TdxScrollBarAdapter.OnEndUpdate;
begin
end;

procedure TdxScrollBarAdapter.OnFirstBeginUpdate;
begin
end;

procedure TdxScrollBarAdapter.OnLastCancelUpdate;
begin
  OnLastEndUpdateCore;
end;

procedure TdxScrollBarAdapter.OnLastEndUpdate;
begin
  OnLastEndUpdateCore;
end;

procedure TdxScrollBarAdapter.OnLastEndUpdateCore;
begin
  ValidateValues;
end;

procedure TdxScrollBarAdapter.OnScroll(ASender: TObject; E: TdxScrollEventArgs);
begin
  Adapter.OnScroll(Self, ASender, E as TdxPlatformIndependentScrollEventArgs);
end;

procedure TdxScrollBarAdapter.RaiseScroll(AArgs: TdxScrollEventArgs);
begin
  if not FOnScroll.Empty then
    FOnScroll.Invoke(TObject(ScrollBar), AArgs);
end;

procedure TdxScrollBarAdapter.RefreshValuesFromScrollBar;
begin
  BeginUpdate;
  try
    Minimum := Round(ScrollBar.Minimum / Factor);         
    Maximum := Round(ScrollBar.Maximum / Factor);         
    LargeChange := Round(ScrollBar.LargeChange / Factor); 
    Value := Round(ScrollBar.Value / Factor);             
    Enabled := ScrollBar.Enabled;
  finally
    EndUpdate;
  end;
  Synchronized := True;
end;

procedure TdxScrollBarAdapter.SetEnabled(const Value: Boolean);
begin
  FEnabled := Value;
end;

procedure TdxScrollBarAdapter.SetLargeChange(const Value: Int64);
begin
  if FLargeChange = Value then
    Exit;
  FLargeChange := Value;
  ValidateValues;
end;

procedure TdxScrollBarAdapter.SetMaximum(const Value: Int64);
begin
  if FMaximum = Value then
    Exit;
  FMaximum := Value;
  ValidateValues;
end;

procedure TdxScrollBarAdapter.SetMinimum(const Value: Int64);
begin
  if FMinimum = Value then
    Exit;
  FMinimum := Value;
  ValidateValues;
end;

function TdxScrollBarAdapter.SetRawScrollBarValue(AValue: Integer): Boolean;
begin
  Assert(Synchronized);
  Result := Adapter.SetRawScrollBarValue(Self, AValue);
end;

procedure TdxScrollBarAdapter.SetValue(const Value: Int64);
begin
  if FValue = Value then
    Exit;
  FValue := Value;
  ValidateValues;
end;

function TdxScrollBarAdapter.ShouldSynchronize: Boolean;
begin
  Result := DeferredScrollBarUpdate and not Synchronized;
end;

procedure TdxScrollBarAdapter.SubscribeScrollbarEvents;
begin
  ScrollBar.Scroll.Add(OnScroll);
end;

procedure TdxScrollBarAdapter.UnsubscribeScrollbarEvents;
begin
  ScrollBar.Scroll.Remove(OnScroll);
end;

function TdxScrollBarAdapter.SynchronizeScrollBarAvoidJump: Boolean;
var
  ARelativePos, AActualRelativePos: Double;
begin
  Result := False;
  if ShouldSynchronize then
  begin
    ARelativePos := Value / Math.Max(1, (Maximum - LargeChange + 1 - Minimum)); 
    AActualRelativePos := ScrollBar.Value / Max(1, ScrollBar.Maximum - ScrollBar.LargeChange + 1 - ScrollBar.Minimum);
    if ARelativePos <= AActualRelativePos then
    begin
      ApplyValuesToScrollBarCore;
      Assert(Synchronized);
      Result := True;
    end;
  end;
end;

procedure TdxScrollBarAdapter.ValidateValues;
begin
  if not IsUpdateLocked then
    ValidateValuesCore;
end;

procedure TdxScrollBarAdapter.ValidateValuesCore;
begin
  Assert(LargeChange >= 0, 'LargeChange < 0'); 
  if Minimum > Maximum then
    FMaximum := Minimum;
  FLargeChange := Math.Min(LargeChange, Maximum - Minimum + 1);
  FValue := Math.Max(Value, Minimum);
  FValue := Math.Min(Value, Maximum - LargeChange + 1);
end;

{ TdxVerticalScrollBarAdapter }

function TdxVerticalScrollBarAdapter.GetDeferredScrollBarUpdate: Boolean;
begin
  Result := True;
end;

function TdxVerticalScrollBarAdapter.GetSynchronized: Boolean;
begin
  Result := FSynchronized;
end;

procedure TdxVerticalScrollBarAdapter.SetSynchronized(AValue: Boolean);
begin
  FSynchronized := AValue;
end;

{ TdxHorizontalScrollBarAdapter }

function TdxHorizontalScrollBarAdapter.GetDeferredScrollBarUpdate: Boolean;
begin
  Result := False;
end;

function TdxHorizontalScrollBarAdapter.GetSynchronized: Boolean;
begin
  Result := True;
end;

procedure TdxHorizontalScrollBarAdapter.SetSynchronized(AValue: Boolean);
begin
end;

{ TdxMouseCursorCalculator }

constructor TdxMouseCursorCalculator.Create(AView: TdxRichEditView);
begin
  inherited Create;
  FView := AView;
end;

function TdxMouseCursorCalculator.Calculate(AHitTestResult: TdxRichEditHitTestResult;
  const APhysicalPoint: TPoint): TCursor;
begin
  if AHitTestResult = nil then
  begin
    AHitTestResult := View.CalculateNearestPageHitTest(APhysicalPoint, False);
    try
      if AHitTestResult <> nil then
        Result := CalculateHotZoneCursor(AHitTestResult)
      else
        Result := TdxRichEditCursors.Default;
    finally
      AHitTestResult.Free;
    end;
  end
  else
    Result := CalculateCore(AHitTestResult);
end;

function TdxMouseCursorCalculator.CalculateCore(AResult: TdxRichEditHitTestResult): TCursor;
var
  ASelectionManager: TdxEnhancedSelectionManager;
begin
  Result := CalculateHotZoneCursor(AResult);
  if Result <> TdxRichEditCursors.Default then
    Exit;
  ASelectionManager := TdxEnhancedSelectionManager.Create(View.DocumentModel.ActivePieceTable);
  try
    if ASelectionManager.ShouldSelectFloatingObject(AResult) then
    begin
      if IsEditable then
        Result := TdxRichEditCursors.SizeAll
      else
        Result := CalculateCharacterMouseCursor(AResult);
    end
    else if ASelectionManager.ShouldSelectEntireRow(AResult) then
      Result := TdxRichEditCursors.SelectRow
    else if ASelectionManager.ShouldSelectToTheEndOfRow(AResult) then
      Result := CalculateCharacterMouseCursor(AResult)
    else
      Result := CalculateCharacterMouseCursor(AResult);
  finally
    ASelectionManager.Free;
  end;
end;

function TdxMouseCursorCalculator.CalculateHotZoneCursor(AResult: TdxRichEditHitTestResult): TCursor;
var
  AHotZone: TdxHotZone;
begin
  AHotZone := View.SelectionLayout.CalculateHotZone(AResult, View);
  Result := CalculateHotZoneCursorCore(AHotZone);
end;

function TdxMouseCursorCalculator.CalculateHotZoneCursorCore(AHotZone: TdxHotZone): TCursor;
begin
  if (AHotZone <> nil) and View.Control.InnerControl.IsEditable then
    Result := AHotZone.Cursor
  else
    Result := TdxRichEditCursors.Default;
end;

function TdxMouseCursorCalculator.CalculateCharacterMouseCursor(AResult: TdxRichEditHitTestResult): TCursor;
var
  ACharacter: TdxCharacterBox;
begin
  ACharacter := AResult.Character;
  if ACharacter.GetRun(AResult.PieceTable).FontItalic then
    Result := TdxRichEditCursors.IBeamItalic
  else
    Result := TdxRichEditCursors.IBeam;
end;

function TdxMouseCursorCalculator.IsEditable: Boolean;
begin
  Result := View.Control.InnerControl.IsEditable;
end;

{ TdxScrollEventArgs }

constructor TdxScrollEventArgs.Create(AType: TdxScrollEventType; ANewValue: Integer;
  AScrollOrientation: TdxScrollOrientation);
begin
  inherited Create;
  FType := AType;
  FNewValue := ANewValue;
  FScrollOrientation := AScrollOrientation;
end;

constructor TdxScrollEventArgs.Create(AType: TdxScrollEventType; AOldValue, ANewValue: Integer;
  AScrollOrientation: TdxScrollOrientation);
begin
  inherited Create;
  FType := AType;
  FOldValue := AOldValue;
  FNewValue := ANewValue;
  FScrollOrientation := AScrollOrientation;
end;

{ TdxCaretDocumentLayoutPosition }

constructor TdxCaretDocumentLayoutPosition.Create(AView: TdxRichEditView);
begin
  inherited Create(AView.DocumentLayout, AView.DocumentModel.MainPieceTable, 0); 
  FView := AView;
end;

function TdxCaretDocumentLayoutPosition.CreateEmptyClone: TdxDocumentLayoutPosition;
begin
  Result := TdxCaretDocumentLayoutPosition.Create(FView);
end;

procedure TdxCaretDocumentLayoutPosition.EnsureFormattingComplete;
begin
  FView.EnsureFormattingCompleteForSelection;
end;

procedure TdxCaretDocumentLayoutPosition.EnsurePageSecondaryFormattingComplete(APage: TdxPage);
begin
  FView.EnsurePageSecondaryFormattingComplete(APage);
end;

function TdxCaretDocumentLayoutPosition.GetPieceTable: TdxPieceTable;
begin
  Result := FView.DocumentModel.ActivePieceTable;
end;

{ TdxScrollByPhysicalHeightCalculator }

constructor TdxScrollByPhysicalHeightCalculator.Create(AView: TdxRichEditView);
begin
  inherited Create;
  FView := AView;
end;

function TdxScrollByPhysicalHeightCalculator.CalculateRowsTotalVisibleHeight(AGenerator: TdxPageViewInfoGeneratorBase;
  ALastRowIndex: Integer; ABottomY: Integer): Int64;
var
  AViewPortBounds: TRect;
  I: Integer;
  ARows: TdxPageViewInfoRowCollection;
  ALayoutManager: TdxPageGeneratorLayoutManager;
begin
  ALayoutManager := FView.PageViewInfoGenerator;
  ARows := AGenerator.PageRows;
  Result := 0;
  if ALastRowIndex > 0 then
  begin
    Result := ALayoutManager.CalculatePagesTotalLogicalHeightBelow(ARows[0], 0);
    for I := 1 to ALastRowIndex - 1 do
      Inc(Result, ALayoutManager.CalculatePagesTotalLogicalHeight(ARows[I]));
  end;
  AViewPortBounds.InitSize(0, 0, 0, ABottomY);
  Inc(Result, ALayoutManager.CalculatePagesTotalVisibleLogicalHeight(ARows[ALastRowIndex], AViewPortBounds));
end;

function TdxScrollByPhysicalHeightCalculator.CalculateScrollDelta(APhysicalVerticalOffset: Integer): Int64;
var
  AInvisiblePhysicalVerticalOffset: Integer;
begin
  Assert(APhysicalVerticalOffset >= 0);
  if APhysicalVerticalOffset <= 0 then
    Exit(0);
  if not CalculateScrollDeltaForVisibleRows(APhysicalVerticalOffset, Result) then
  begin
    AInvisiblePhysicalVerticalOffset := CalculateInvisiblePhysicalVerticalOffset(APhysicalVerticalOffset);
    Result := CalculateScrollDeltaForInvisibleRows(AInvisiblePhysicalVerticalOffset);
  end;
end;

function TdxScrollByPhysicalHeightCalculator.CalculateScrollDeltaForInvisibleRows(APhysicalVerticalOffset: Integer): Int64;
var
  AIndex, AFirstInvisiblePageIndex: Integer;
  AGenerator: TdxInvisiblePageRowsGenerator;
begin
  AFirstInvisiblePageIndex := CalculateFirstInvisiblePageIndex;
  if AFirstInvisiblePageIndex < 0 then
    Exit(0);
  AGenerator := TdxInvisiblePageRowsGenerator.Create(View.FormattingController.PageController.Pages, View.PageViewInfoGenerator);
  try
    AGenerator.FirstPageIndex := AFirstInvisiblePageIndex;
    AGenerator.FirstInvalidPageIndex := CalculateFirstInvalidPageIndex;
    AIndex := LookupIntersectingRowIndexInInvisibleRows(AGenerator, APhysicalVerticalOffset);
    if AIndex < 0 then
      Exit(GetDefaultScrollDeltaForInvisibleRows);
    Result := CalculateRowsTotalVisibleHeight(AGenerator.Generator, AIndex, APhysicalVerticalOffset);
    Inc(Result, CalculateVisibleRowsScrollDelta);
  finally
    AGenerator.Free;
  end;
end;

function TdxScrollByPhysicalHeightCalculator.LookupIntersectingRowIndexInInvisibleRows(AGenerator: TdxInvisiblePageRowsGenerator; Y: Integer): Integer;
var
  ARow: TdxPageViewInfoRow;
  ARowIndex: Integer;
begin
  ARowIndex := 0;
  while True do
  begin
    ARow := AGenerator.GenerateNextRow;
    if ARow = nil then
      Exit(-1);
    if ARow.IntersectsWithHorizontalLine(Y) then
      Exit(ARowIndex);
    Inc(ARowIndex);
  end;
end;

{ TdxScrollUpByPhysicalHeightCalculator }

function TdxScrollUpByPhysicalHeightCalculator.CalculateScrollDeltaForVisibleRows(APhysicalVerticalOffset: Integer; out ADelta: Int64): Boolean;
var
  AFirstRow: TdxPageViewInfoRow;
  AViewPortBounds: TRect;
  APageIndex: Integer;
begin
  AFirstRow := View.PageViewInfoGenerator.ActiveGenerator.PageRows.First;
  if AFirstRow.IntersectsWithHorizontalLine(-APhysicalVerticalOffset) or (AFirstRow.Bounds.Top = -APhysicalVerticalOffset) then
  begin
    AViewPortBounds.InitSize(0, -APhysicalVerticalOffset, 0, APhysicalVerticalOffset);
    ADelta := View.PageViewInfoGenerator.CalculatePagesTotalVisibleLogicalHeight(AFirstRow, AViewPortBounds);
    Exit(True);
  end
  else
  begin
    APageIndex := View.FormattingController.PageController.Pages.IndexOf(AFirstRow.First.Page);
    Result := APageIndex = 0;
    if Result then
      ADelta := View.PageViewInfoGenerator.CalculatePagesTotalLogicalHeightAbove(AFirstRow, 0);
  end;
end;

function TdxScrollUpByPhysicalHeightCalculator.CalculateFirstInvisiblePageIndex: Integer;
begin
  Result := View.CalculateFirstInvisiblePageIndexBackward;
end;

function TdxScrollUpByPhysicalHeightCalculator.CalculateFirstInvalidPageIndex: Integer;
begin
  Result := -1;
end;

function TdxScrollUpByPhysicalHeightCalculator.CalculateInvisiblePhysicalVerticalOffset(APhysicalVerticalOffset: Integer): Integer;
var
  AFirstRow: TdxPageViewInfoRow;
begin
  AFirstRow := View.PageViewInfoGenerator.ActiveGenerator.PageRows.First;
  Result := APhysicalVerticalOffset + AFirstRow.Bounds.Top;
end;

function TdxScrollUpByPhysicalHeightCalculator.CalculateVisibleRowsScrollDelta: Int64;
var
  AViewPortBounds: TRect;
  ALayoutManager: TdxPageGeneratorLayoutManager;
  AFirstRow: TdxPageViewInfoRow;
begin
  ALayoutManager := View.PageViewInfoGenerator;
  AFirstRow := View.PageViewInfoGenerator.ActiveGenerator.PageRows.First;
  AViewPortBounds := ALayoutManager.ViewPortBounds;
  AViewPortBounds.Top := AFirstRow.Bounds.Top;
  AViewPortBounds.Height := -AViewPortBounds.Top;
  Result := ALayoutManager.CalculatePagesTotalVisibleLogicalHeight(AFirstRow, AViewPortBounds);
end;

function TdxScrollUpByPhysicalHeightCalculator.GetDefaultScrollDeltaForInvisibleRows: Int64;
begin
  Result := View.PageViewInfoGenerator.TopInvisibleHeight;
end;

{ TdxScrollDownByPhysicalHeightCalculator }

function TdxScrollDownByPhysicalHeightCalculator.CalculateScrollDeltaForVisibleRows(APhysicalVerticalOffset: Integer; out ADelta: Int64): Boolean;
var
  AIntersectingRowIndex: Integer;
begin
  AIntersectingRowIndex := LookupIntersectingRowIndex(APhysicalVerticalOffset);
  Result := AIntersectingRowIndex >= 0;
  if Result then
    ADelta := CalculateRowsTotalVisibleHeight(View.PageViewInfoGenerator.ActiveGenerator, AIntersectingRowIndex, APhysicalVerticalOffset);
end;

function TdxScrollDownByPhysicalHeightCalculator.CalculateFirstInvisiblePageIndex: Integer;
begin
  Result := View.CalculateFirstInvisiblePageIndexForward;
end;

function TdxScrollDownByPhysicalHeightCalculator.CalculateFirstInvalidPageIndex: Integer;
begin
  Result := View.FormattingController.PageController.Pages.Count;
end;

function TdxScrollDownByPhysicalHeightCalculator.CalculateInvisiblePhysicalVerticalOffset(APhysicalVerticalOffset: Integer): Integer;
var
  AVisibleRows: TdxPageViewInfoRowCollection;
  I, ATotalRowsHeight: Integer;
begin
  AVisibleRows := View.PageViewInfoGenerator.ActiveGenerator.PageRows;
  ATotalRowsHeight := AVisibleRows[0].Bounds.Bottom;
  for I := 1 to AVisibleRows.Count - 1 do
    Inc(ATotalRowsHeight, AVisibleRows[I].Bounds.Height);
  Result := APhysicalVerticalOffset - ATotalRowsHeight;
end;

function TdxScrollDownByPhysicalHeightCalculator.CalculateVisibleRowsScrollDelta: Int64;
var
  ALayoutManager: TdxPageGeneratorLayoutManager;
  I: Integer;
  ARows: TdxPageViewInfoRowCollection;
begin
  ALayoutManager := View.PageViewInfoGenerator;
  ARows := View.PageViewInfoGenerator.ActiveGenerator.PageRows;
  Result := ALayoutManager.CalculatePagesTotalLogicalHeightBelow(ARows[0], ALayoutManager.ViewPortBounds.Top);
  for I := 1 to ARows.Count - 1 do
    Inc(Result, ALayoutManager.CalculatePagesTotalLogicalHeight(ARows[I]));
end;

function TdxScrollDownByPhysicalHeightCalculator.GetDefaultScrollDeltaForInvisibleRows: Int64;
var
  ARows: TdxPageViewInfoRowCollection;
  ALastRow: TdxPageViewInfoRow;
begin
  try
    ARows := View.PageViewInfoGenerator.ActiveGenerator.PageRows;
    ALastRow := ARows.Last;
    if ALastRow = nil then
      Result := 0
    else
      Result := CalculateRowsTotalVisibleHeight(View.PageViewInfoGenerator.ActiveGenerator, ARows.Count - 1, ALastRow.Bounds.Bottom);
  except
    Result := 0;
  end;
end;

function TdxScrollDownByPhysicalHeightCalculator.LookupIntersectingRowIndex(Y: Integer): Integer;
var
  ARows: TdxPageViewInfoRowCollection;
  I: Integer;
begin
  ARows := View.PageViewInfoGenerator.ActiveGenerator.PageRows;
  for I := 0 to ARows.Count - 1 do
    if ARows[I].IntersectsWithHorizontalLine(Y) then
      Exit(I);
  Exit(-1);
end;

{ TdxNonPrintViewPageControllerBase }

constructor TdxNonPrintViewPageControllerBase.Create(ADocumentLayout: TdxDocumentLayout);
begin
  inherited Create(ADocumentLayout, nil)
end;

function TdxNonPrintViewPageControllerBase.GetColumnBottom(AColumn: TdxColumn): Integer;
var
  ALastRow: TdxRow;
  AColumnBounds: TRect;
begin
  Result := 0;
  ALastRow := AColumn.Rows.Last;
  if ALastRow is TdxTableCellRow then
  begin
    NotImplemented;
  end
  else
    Result := ALastRow.Bounds.Bottom;
  AColumnBounds := AColumn.Bounds;
  AColumnBounds.Height := MaxInt - AColumnBounds.Top;
end;

{ TdxBackgroundFormatterThread }

constructor TdxBackgroundFormatterThread.Create(AFormatter: TdxBackgroundFormatter);
begin
  inherited Create(True);
  FFormatter := AFormatter;
  Priority := tpLowest;
end;

procedure TdxBackgroundFormatterThread.Execute;
begin
  while not Terminated do
    if not WorkerBody then
      Break;
end;

function TdxBackgroundFormatterThread.GetWaitHandles: TWaitHandles;
var
  I: TdxBackgroundFormatterAction;
begin
  for I := Low(FFormatter.Commands) to High(FFormatter.Commands) do
    Result[I] := FFormatter.Commands[I].Handle;
end;

function TdxBackgroundFormatterThread.WorkerBody: Boolean;
var
  AHandles: TWaitHandles;
  AResult: HRESULT;
begin
  Result := False;
  if not WaitForContinueLayoutAllowed then
    Exit;
  while not Terminated do
  begin
    AHandles := GetWaitHandles;
    AResult := WaitForMultipleObjects(Length(AHandles), @AHandles, False, 1000);
    if not Terminated and (AResult <> WAIT_TIMEOUT) then
      Exit(FFormatter.HandleCommand(AResult));
  end;
end;

function TdxBackgroundFormatterThread.GetContinueLayoutEvent: TSimpleEvent;
begin
  Result := FFormatter.ContinueLayout;
end;

function TdxBackgroundFormatterThread.WaitForContinueLayoutAllowed: Boolean;
var
  AContinueLayout: TSimpleEvent;
begin
  Result := False;
  AContinueLayout := GetContinueLayoutEvent;
  while not Terminated do
  begin
    if AContinueLayout.WaitFor(1000) = wrSignaled then
      Exit(True);
  end;
end;

{ TdxBackgroundFormatter }

constructor TdxBackgroundFormatter.Create(AController: TdxDocumentFormattingController);
begin
  inherited Create;
  Assert(AController <> nil); 
  FController := AController;
  FPerformPrimaryLayoutUntil := IsPrimaryLayoutComplete;
  FDocumentFormatter := TdxDocumentFormatter.Create(AController);
  FSecondaryFormatter := CreateParagraphFinalFormatter(AController.DocumentLayout);
  FCurrentPosition := TdxDocumentModelPosition.Create(PieceTable);
  FCurrentPosition.LogPosition := -1;
  FSecondaryLayoutStart := TdxDocumentModelPosition.Create(PieceTable);
  FSecondaryLayoutEnd := TdxDocumentModelPosition.Create(PieceTable);
  FResetSecondaryLayoutFromPage := -1;

  InitializeWorkThread;

  ResetPrimaryLayout;
  ResetSecondaryLayout;
end;

destructor TdxBackgroundFormatter.Destroy;
begin
  DoneWorkThread;
  FreeAndNil(FDocumentFormatter);
  FreeAndNil(FSecondaryFormatter);
  inherited Destroy;
end;

procedure TdxBackgroundFormatter.CheckExecutedAtUIThread;
begin
end;

procedure TdxBackgroundFormatter.CheckExecutedAtWorkerThread;
begin
end;

procedure TdxBackgroundFormatter.BeginDocumentRendering(
  APerformPrimaryLayoutUntil: TdxPredicate<TdxDocumentModelPosition>);
var
  AAlreadySuspended: Boolean;
begin
  CheckExecutedAtUIThread;
  if not Assigned(APerformPrimaryLayoutUntil) then 
    APerformPrimaryLayoutUntil := IsPrimaryLayoutComplete;
  AAlreadySuspended := not SuspendWorkerThread;
  if AAlreadySuspended then
  begin
    Exit;
  end;
  FPerformPrimaryLayoutUntil := APerformPrimaryLayoutUntil;
  ResumeWorkerThread;

  SecondaryLayoutComplete.WaitFor(Windows.INFINITE); 
  SuspendWorkerThread;
  if FResetSecondaryLayoutFromPage >= 0 then
  begin
    RefreshSecondaryLayout(FResetSecondaryLayoutFromPage);
    FResetSecondaryLayoutFromPage := -1;
  end;
end;

procedure TdxBackgroundFormatter.BeginDocumentUpdate;
begin
  CheckExecutedAtUIThread;
  Inc(FDocumentBeginUpdateCount);
  SuspendWorkerThread;
end;

function TdxBackgroundFormatter.CreateParagraphFinalFormatter(
  ADocumentLayout: TdxDocumentLayout): TdxParagraphFinalFormatter;
begin
  Result := TdxParagraphFinalFormatter.Create(ADocumentLayout);
end;

procedure TdxBackgroundFormatter.InitializeWorkThread;
begin
  FContinueLayout := TSimpleEvent.Create(nil, False, True, '');
  FSecondaryLayoutComplete := TSimpleEvent.Create(nil, True, False, '');
  FCommands[TdxBackgroundFormatterAction.Shutdown] := TSimpleEvent.Create(nil, True, False, '');
  FCommands[TdxBackgroundFormatterAction.PerformPrimaryLayout] := TSimpleEvent.Create(nil, True, False, '');
  FCommands[TdxBackgroundFormatterAction.PerformSecondaryLayout] := TSimpleEvent.Create(nil, True, False, '');
  FCommands[TdxBackgroundFormatterAction.None] := TSimpleEvent.Create(nil, True, False, '');
  FWorkerThread := TdxBackgroundFormatterThread.Create(Self);
end;

procedure TdxBackgroundFormatter.DoneWorkThread;
begin
  SuspendWorkerThread;
  Commands[TdxBackgroundFormatterAction.Shutdown].SetEvent;
  ResumeWorkerThread;
  FWorkerThread.Terminate;
  FreeAndNil(FWorkerThread);
  FreeAndNil(FContinueLayout);
  FreeAndNil(FSecondaryLayoutComplete);
  FreeAndNil(FCommands[TdxBackgroundFormatterAction.Shutdown]);
  FreeAndNil(FCommands[TdxBackgroundFormatterAction.PerformPrimaryLayout]);;
  FreeAndNil(FCommands[TdxBackgroundFormatterAction.PerformSecondaryLayout]);;
  FreeAndNil(FCommands[TdxBackgroundFormatterAction.None]);;
end;

procedure TdxBackgroundFormatter.EndDocumentRendering;
begin
  CheckExecutedAtUIThread;
  FPerformPrimaryLayoutUntil := IsPrimaryLayoutComplete;
  ResumeWorkerThread;
end;

procedure TdxBackgroundFormatter.EndDocumentUpdate;
begin
  CheckExecutedAtUIThread;
  Assert(FDocumentBeginUpdateCount > 0);
  Dec(FDocumentBeginUpdateCount);
  ResumeWorkerThread;
end;

function TdxBackgroundFormatter.GetDocumentLayout: TdxDocumentLayout;
begin
  Result := FController.DocumentLayout;
end;

function TdxBackgroundFormatter.GetDocumentModel: TdxDocumentModel;
begin
  Result := FController.DocumentModel;
end;

function TdxBackgroundFormatter.GetPieceTable: TdxPieceTable;
begin
  Result := FController.PieceTable;
end;

function TdxBackgroundFormatter.HandleCommand(ACommandIndex: Integer): Boolean;
begin
  case TdxBackgroundFormatterAction(ACommandIndex) of
    TdxBackgroundFormatterAction.Shutdown:
      begin
        ContinueLayout.SetEvent;
        Exit(False);
      end;
    TdxBackgroundFormatterAction.PerformPrimaryLayout:
      begin
        PerformPrimaryLayout;
        ContinueLayout.SetEvent;
      end;
    TdxBackgroundFormatterAction.PerformSecondaryLayout:
      begin
        PerformSecondaryLayout;
        ContinueLayout.SetEvent;
      end;
    TdxBackgroundFormatterAction.None:
      ContinueLayout.SetEvent;
  end;
  Result := True;
end;


function TdxBackgroundFormatter.IsPrimaryLayoutComplete(const ACurrentFormatterPosition: TdxDocumentModelPosition): Boolean;
begin
  CheckExecutedAtWorkerThread;
  Result := FPrimaryLayoutComplete;
end;

function TdxBackgroundFormatter.IsTableFormattingComplete(const ANextPosition: TdxDocumentModelPosition): Boolean;
begin
  Result := not DocumentFormatter.ParagraphFormatter.RowsController.TablesController.IsInsideTable;
end;

procedure TdxBackgroundFormatter.NotifyDocumentChanged(const AFrom, ATo: TdxDocumentModelPosition;
  ADocumentLayoutResetType: TdxDocumentLayoutResetType);
begin
  case ADocumentLayoutResetType of
    TdxDocumentLayoutResetType.SecondaryLayout:
      ResetSecondaryLayout;
    TdxDocumentLayoutResetType.AllPrimaryLayout:
      begin
        ResetSecondaryLayout;
        ResetPrimaryLayout;
      end;
    TdxDocumentLayoutResetType.PrimaryLayoutFormPosition:
      begin
        ResetSecondaryLayout;
        if ShouldResetPrimaryLayout(AFrom, ATo) then
          ResetPrimaryLayout(AFrom, ATo);
      end;
    TdxDocumentLayoutResetType.None:
      ; 
    else
      raise Exception.Create('Error Message'); 
  end;
end;

procedure TdxBackgroundFormatter.OnNewMeasurementAndDrawingStrategyChanged;
begin
  FDocumentFormatter.OnNewMeasurementAndDrawingStrategyChanged;
end;

procedure TdxBackgroundFormatter.PerformPageSecondaryFormatting(APage: TdxPage);
begin
  FSecondaryFormatter.FormatPage(APage);
  APage.SecondaryFormattingComplete := APage.PrimaryFormattingComplete;
end;

procedure TdxBackgroundFormatter.PerformPrimaryLayout;
begin
  CheckExecutedAtWorkerThread;
  PerformPrimaryLayoutCore;
end;

procedure TdxBackgroundFormatter.PerformPrimaryLayoutCore;
var
  AResult: TdxRichEditFormattingProcessResult;
  APrevParagraphIndex: TdxParagraphIndex;
  AParagraph: TdxParagraph;
  I, AOffset: Integer;
  ACount: TdxRunIndex;
begin
  AResult := FDocumentFormatter.FormatNextRow;
  if AResult.FormattingProcess = TdxFormattingProcess.ContinueFromParagraph then
    APrevParagraphIndex :=  AResult.ParagraphIndex - 1
  else
    APrevParagraphIndex :=  FDocumentFormatter.ParagraphIndex;
  if APrevParagraphIndex >= 0 then 
  begin
    AParagraph := PieceTable.Paragraphs[APrevParagraphIndex];
    FCurrentPosition := TdxDocumentModelPosition.Create(PieceTable);
    FCurrentPosition.ParagraphIndex := APrevParagraphIndex;
    FCurrentPosition.RunIndex := FDocumentFormatter.ParagraphFormatter.Iterator.RunIndex;
    AOffset := 0;
    if AResult.FormattingProcess = TdxFormattingProcess.ContinueFromParagraph then
      ACount := AParagraph.LastRunIndex + 1
    else
      ACount := FDocumentFormatter.ParagraphFormatter.Iterator.RunIndex;
    for I := AParagraph.FirstRunIndex to ACount - 1 do
      Inc(AOffset, PieceTable.Runs[I].Length);
    FCurrentPosition.RunStartLogPosition := AParagraph.LogPosition + AOffset;
    if AResult.FormattingProcess <> TdxFormattingProcess.ContinueFromParagraph then
    begin
      Inc(AOffset, FDocumentFormatter.ParagraphFormatter.Iterator.Offset);
      FCurrentPosition.LogPosition := AParagraph.LogPosition + AOffset;
    end
    else
      FCurrentPosition.LogPosition := AParagraph.EndLogPosition;
  end
  else
  begin
    FCurrentPosition := TdxDocumentModelPosition.Create(PieceTable);
    FCurrentPosition.LogPosition := -1;
  end;
  if AResult.FormattingProcess = TdxFormattingProcess.Finish then
  begin
    Commands[TdxBackgroundFormatterAction.PerformPrimaryLayout].ResetEvent;
    FPrimaryLayoutComplete := True;
    UpdateSecondaryPositions(SecondaryLayoutStart, FController.PageController.Pages.Last.GetLastPosition(FDocumentFormatter.DocumentModel.MainPieceTable));
  end;
end;

procedure TdxBackgroundFormatter.PerformSecondaryLayout;
begin
  CheckExecutedAtWorkerThread;
  if IsPrimaryLayoutComplete(FCurrentPosition) then
    PerformSecondaryLayoutCore
  else
    if FPerformPrimaryLayoutUntil(FCurrentPosition) and IsTableFormattingComplete(FCurrentPosition) then
      PerformSecondaryLayoutCore
    else
      PerformPrimaryLayout;
end;

type
  TdxBoxAndLogPositionComparable = class(TInterfacedObject, IdxComparable<TdxPage>) 
  private
    FPieceTable: TdxPieceTable; 
    FLogPosition: TdxDocumentLogPosition; 
  public
    constructor Create(APieceTable: TdxPieceTable; ALogPosition: TdxDocumentLogPosition); 
    function CompareTo(const ABox: TdxPage): Integer; 

    property LogPosition: TdxDocumentLogPosition read FLogPosition; 
    property PieceTable: TdxPieceTable read FPieceTable; 
  end;

  constructor TdxBoxAndLogPositionComparable.Create(APieceTable: TdxPieceTable; ALogPosition: TdxDocumentLogPosition);
  begin
    inherited Create;
    Assert(APieceTable <> nil); 
    FPieceTable := APieceTable;
    FLogPosition := ALogPosition;
  end;

  function TdxBoxAndLogPositionComparable.CompareTo(const ABox: TdxPage): Integer;
  var
    AFirstPos, ALastPos: TdxDocumentModelPosition;
  begin
    AFirstPos := ABox.GetFirstPosition(PieceTable);
    if FLogPosition < AFirstPos.LogPosition then
      Result := 1
    else
      if FLogPosition > AFirstPos.LogPosition then
      begin
        ALastPos := ABox.GetLastPosition(PieceTable);
        if FLogPosition <= ALastPos.LogPosition then
          Result := 0
        else
          Result :=  -1;
      end
      else
        Result := 0;
  end;

procedure TdxBackgroundFormatter.PerformSecondaryLayoutCore;
var
  APage: TdxPage;
  APages: TdxPageCollection;
  I, AFirstPageIndex, ALastPageIndex: Integer;
  ABoxAndLogPositionComparable: TdxBoxAndLogPositionComparable;
begin
  CheckExecutedAtWorkerThread;
  APages := FController.PageController.Pages;
  ABoxAndLogPositionComparable := TdxBoxAndLogPositionComparable.Create(DocumentFormatter.DocumentModel.MainPieceTable, FSecondaryLayoutStart.LogPosition);
  try
    AFirstPageIndex := TdxAlgorithms.BinarySearch<TdxPage>(TList<TdxPage>(APages), ABoxAndLogPositionComparable); 
  finally
    ABoxAndLogPositionComparable.Free;
  end;
  if AFirstPageIndex < 0 then
    AFirstPageIndex := 0;
  ABoxAndLogPositionComparable := TdxBoxAndLogPositionComparable.Create(DocumentFormatter.DocumentModel.MainPieceTable, FsecondaryLayoutEnd.LogPosition);
  try
    ALastPageIndex := TdxAlgorithms.BinarySearch<TdxPage>(TList<TdxPage>(APages), ABoxAndLogPositionComparable); 
  finally
    ABoxAndLogPositionComparable.Free;
  end;
  if ALastPageIndex < 0 then
    ALastPageIndex := APages.Count - 1;
  for I := AFirstPageIndex to ALastPageIndex do
  begin
    APage := APages[I];
    if not APage.SecondaryFormattingComplete  then
      PerformPageSecondaryFormatting(APage);
  end;
  SecondaryLayoutComplete.SetEvent;
  Commands[TdxBackgroundFormatterAction.PerformSecondaryLayout].ResetEvent;
end;

procedure TdxBackgroundFormatter.ResetPrimaryLayout;
begin
  FDocumentFormatter.ParagraphIndex := -1;
  FDocumentFormatter.ChangeState(TdxDocumentFormatterStateType.BeginParagraphFormatting);
  FCurrentPosition := TdxDocumentModelPosition.Create(PieceTable);
  FCurrentPosition.LogPosition := -1;
  FPrimaryLayoutComplete := False;
  Commands[TdxBackgroundFormatterAction.PerformPrimaryLayout].SetEvent;
end;

procedure TdxBackgroundFormatter.RefreshSecondaryLayout(AFirstPageIndex: Integer);
var
  I: Integer;
  APage: TdxPage;
  APages: TdxPageCollection;
begin
  APages := FController.PageController.Pages;
  for I := 0 to APages.Count - 1 do
  begin
    APage := APages[I];
    if not APage.PrimaryFormattingComplete then
      Break;
    if not APage.SecondaryFormattingComplete then
      PerformPageSecondaryFormatting(APage);
  end;
end;

procedure TdxBackgroundFormatter.ResetPrimaryLayout(const AFrom, ATo: TdxDocumentModelPosition);
begin
  FDocumentFormatter.ParagraphIndex := AFrom.ParagraphIndex - 1;
  FDocumentFormatter.ChangeState(TdxDocumentFormatterStateType.BeginParagraphFormatting);
  FCurrentPosition := TdxDocumentModelPosition.FromParagraphStart(PieceTable, AFrom.ParagraphIndex);
  FPrimaryLayoutComplete := False;
  Commands[TdxBackgroundFormatterAction.PerformPrimaryLayout].SetEvent;
end;

procedure TdxBackgroundFormatter.ResetSecondaryFormattingForPage(APage: TdxPage; APageIndex: Integer);
begin
  if FResetSecondaryLayoutFromPage >= 0 then
    FResetSecondaryLayoutFromPage := Math.Min(APageIndex, FResetSecondaryLayoutFromPage)
  else
    FResetSecondaryLayoutFromPage := APageIndex;
end;

procedure TdxBackgroundFormatter.ResetSecondaryLayout;
begin
  SecondaryLayoutComplete.ResetEvent;
  Commands[TdxBackgroundFormatterAction.PerformSecondaryLayout].SetEvent;
end;

procedure TdxBackgroundFormatter.Restart(const AFrom, ATo, ASecondaryFrom: TdxDocumentModelPosition);
begin
  UpdateSecondaryPositions(ASecondaryFrom, ATo);
  NotifyDocumentChanged(AFrom, ATo, TdxDocumentLayoutResetType.PrimaryLayoutFormPosition);
  FCurrentPosition := AFrom.Clone;
  DocumentFormatter.Restart(AFrom);
end;

function TdxBackgroundFormatter.ShouldResetPrimaryLayout(const AFrom, ATo: TdxDocumentModelPosition): Boolean;
begin
  Result := (AFrom.LogPosition <= FCurrentPosition.LogPosition) or (ATo.LogPosition <= FCurrentPosition.LogPosition);
end;

function TdxBackgroundFormatter.ShouldResetSecondaryLayout(const AFrom, ATo: TdxDocumentModelPosition): Boolean;
begin
  Result := not ((AFrom.LogPosition > FSecondaryLayoutEnd.LogPosition) or
    (ATo.LogPosition < FSecondaryLayoutStart.LogPosition));
end;

procedure TdxBackgroundFormatter.Start;
begin
  FWorkerThread.Start;
end;

function TdxBackgroundFormatter.SuspendWorkerThread: Boolean;
begin
  CheckExecutedAtUIThread;
  Inc(FThreadSuspendCount);
  if FThreadSuspendCount > 1 then
    Exit(False);
  Commands[TdxBackgroundFormatterAction.None].SetEvent;
  ContinueLayout.WaitFor(Windows.INFINITE);
  Commands[TdxBackgroundFormatterAction.None].ResetEvent;
  Result := True;
end;

procedure TdxBackgroundFormatter.ResumeWorkerThread;
begin
  CheckExecutedAtUIThread;
  Assert(FThreadSuspendCount > 0);
  Dec(FThreadSuspendCount);
  if FThreadSuspendCount = 0 then
    ContinueLayout.SetEvent;
end;

procedure TdxBackgroundFormatter.UpdateSecondaryPositions(const AFrom, ATo: TdxDocumentModelPosition);
begin
  FSecondaryLayoutStart := AFrom;
  FSecondaryLayoutEnd := ATo;
end;

procedure TdxBackgroundFormatter.WaitForPagePrimaryLayoutComplete(APage: TdxPage);
begin
  CheckExecutedAtUIThread;
  BeginDocumentUpdate;
  try
    while not APage.PrimaryFormattingComplete and not FPrimaryLayoutComplete do
      PerformPrimaryLayoutCore;
  finally
    EndDocumentUpdate;
  end;
end;

procedure TdxBackgroundFormatter.WaitForPrimaryLayoutReachesLogPosition(ALogPosition: TdxDocumentLogPosition);
begin
  CheckExecutedAtUIThread;
  BeginDocumentUpdate;
  try
    while ((FCurrentPosition.LogPosition <= ALogPosition) or
      not IsTableFormattingComplete(FCurrentPosition)) and not FPrimaryLayoutComplete do
      PerformPrimaryLayoutCore;
  finally
    EndDocumentUpdate;
  end;
end;

procedure TdxBackgroundFormatter.WaitForPrimaryLayoutReachesPreferredPage(APreferredPageIndex: Integer);
begin
  CheckExecutedAtUIThread;
  BeginDocumentUpdate;
  try
    while ((DocumentLayout.Pages.Count - 1 <= APreferredPageIndex) or
    not IsTableFormattingComplete(FCurrentPosition)) and not FPrimaryLayoutComplete do
      PerformPrimaryLayoutCore;
  finally
    EndDocumentUpdate;
  end;
end;

function TdxBackgroundFormatter.PredicateDocumentModelPosition(const ACurrentFormatterPosition: TdxDocumentModelPosition): Boolean;
begin
  Result := ACurrentFormatterPosition.LogPosition > FPredicatePos.LogPosition;
end;

procedure TdxBackgroundFormatter.WaitForSecondaryLayoutReachesPosition(const APos: TdxDocumentModelPosition);
var
  AOriginalStartPos, AOriginalEndPos, AStartPos, AEndPos: TdxDocumentModelPosition;
begin
  CheckExecutedAtUIThread;
  BeginDocumentUpdate;
  try
    AOriginalStartPos := SecondaryLayoutStart;
    AOriginalEndPos := SecondaryLayoutEnd;
    if APos.LogPosition < SecondaryLayoutStart.LogPosition then
      AStartPos := APos
    else
      AStartPos := SecondaryLayoutStart;
    if APos.LogPosition > SecondaryLayoutEnd.LogPosition then
      AEndPos := APos
    else
      AEndPos := SecondaryLayoutEnd;
    UpdateSecondaryPositions(AStartPos, AEndPos);
    NotifyDocumentChanged(AStartPos, AEndPos, TdxDocumentLayoutResetType.SecondaryLayout);
  finally
    EndDocumentUpdate;
  end;
  FPredicatePos := APos;
  BeginDocumentRendering(PredicateDocumentModelPosition);
  UpdateSecondaryPositions(AOriginalStartPos, AOriginalEndPos);
  NotifyDocumentChanged(AOriginalStartPos, AOriginalEndPos, TdxDocumentLayoutResetType.SecondaryLayout);
  EndDocumentRendering;
end;

{ TdxDragCaret }

constructor TdxDragCaret.Create(AView: TdxRichEditView);
begin
  inherited Create;
  FPosition := AView.CaretPosition.CreateDragCaretPosition;
end;

destructor TdxDragCaret.Destroy;
begin
  FreeAndNil(FPosition);
  inherited;
end;

procedure TdxDragCaret.DrawCore(ACanvas: TcxCanvas);
var
  ABrush, AOldBrush: HBRUSH;
  ABounds: TRect;
  ADashHeight: Integer;
  Y: Integer;
begin
  ABrush := GetStockObject(WHITE_BRUSH);
  AOldBrush := SelectObject(ACanvas.Handle, ABrush);
  try
    ABounds := Bounds;
    ADashHeight := Max((ABounds.Width div 2), 1);
    Y := ABounds.Top;
    while Y < ABounds.Bottom do
    begin
      PatBlt(ACanvas.Handle, Bounds.Left, Y, Bounds.Width, ADashHeight, PATINVERT);
      Y := Y + ADashHeight * 2;
    end;
  finally
    SelectObject(ACanvas.Handle, AOldBrush);
  end;
end;

function TdxDragCaret.GetCaretPosition(
  AView: TdxRichEditView): TdxCaretPosition;
begin
  Result := FPosition;
end;

procedure TdxDragCaret.SetLogPosition(ALogPosition: TdxDocumentLogPosition);
begin
  FPosition.SetLogPosition(ALogPosition);
end;

function TdxDragCaret.ShouldDrawCaret(
  ADocumentModel: TdxDocumentModel): Boolean;
begin
  Result := True;
end;

{ TdxDragCaretPosition }

function TdxDragCaretPosition.GetLogPosition: Integer;
begin
  Result := FLogPosition;
end;

function TdxDragCaretPosition.GetUsePreviousBoxBounds: Boolean;
begin
  Result := False;
end;

function TdxDragCaretPosition.GetVirtualLogPosition: Integer;
begin
  Result := FLogPosition;
end;

procedure TdxDragCaretPosition.SetLogPosition(
  ALogPosition: TdxDocumentLogPosition);
begin
  if FLogPosition <> ALogPosition then
  begin
    FLogPosition := ALogPosition;
    Invalidate;
  end;
end;

{ TdxExplicitCaretPosition }

constructor TdxExplicitCaretPosition.Create(AView: TdxRichEditView;
  ALogPosition: TdxDocumentLogPosition; APreferredPageIndex: Integer);
begin
  inherited Create(AView, APreferredPageIndex);
  FLogPosition := Max(0, Min(DocumentModel.MainPieceTable.DocumentEndLogPosition, ALogPosition));
end;

function TdxExplicitCaretPosition.GetLogPosition: Integer;
begin
  Result := FLogPosition;
end;

function TdxExplicitCaretPosition.GetUsePreviousBoxBounds: Boolean;
begin
  Result := False;
end;

function TdxExplicitCaretPosition.GetVirtualLogPosition: Integer;
begin
  Result := FLogPosition;
end;

function TdxExplicitCaretPosition.UpdatePositionTrySetUsePreviousBoxBounds(
  ADetailsLevel: TdxDocumentLayoutDetailsLevel): Boolean;
begin
  Result := False;
end;

{ TdxCopySelectionManager }

procedure TdxCopySelectionManager.CopyDocumentRange(APieceTable: TdxPieceTable;
  ASelectionCollection: TdxSelectionRangeCollection);
var
  ADataObject: IdxDataObject;
  AOldCopyOptions: TdxDefaultPropertiesCopyOptions;
begin
  if (ASelectionCollection.Count <= 0) or (ASelectionCollection[0].Length <= 0) then
    Exit;
  ADataObject := TdxDataObject.Create;
  AOldCopyOptions := DefaultPropertiesCopyOptions;
  try
    SetDataObject(APieceTable, ASelectionCollection, ADataObject);
  finally
    DefaultPropertiesCopyOptions := AOldCopyOptions;
  end;
  try
  except
  end;
end;

constructor TdxCopySelectionManager.Create(ADocumentServer: IdxInnerControl);
begin
  inherited Create;
  FDocumentServer := ADocumentServer;
  FDefaultPropertiesCopyOptions := TdxDefaultPropertiesCopyOptions.Never;
end;

function TdxCopySelectionManager.CreateDocumentModel(AParagraphNumerationCopyOptions: TdxParagraphNumerationCopyOptions;
  AFormattingCopyOptions: TdxFormattingCopyOptions; ASourcePieceTable: TdxPieceTable;
  ASelectionRanges: TdxSelectionRangeCollection; ASuppressFieldsUpdate: Boolean = False;
  AGetTextOptions: PdxTextFragmentOptions = nil): TdxDocumentModel;
var
  ATextOptions: TdxTextFragmentOptions;
  AOptions: TdxDocumentModelCopyOptions;
  ACopyCommand: TdxDocumentModelCopyCommand;
begin
  Result := ASourcePieceTable.DocumentModel.CreateNew;
  if AGetTextOptions <> nil then
    ATextOptions := AGetTextOptions^
  else
    ATextOptions := TdxTextFragmentOptions.Create;
  if ATextOptions.PreserveOriginalNumbering then
  begin
    Assert(False, 'not implemented PieceTable.PrecalculatedNumberingListTexts');
  end;
  if not ATextOptions.AllowExtendingDocumentRange then
    ASelectionRanges := UpdateSelectionCollection(ASourcePieceTable, ASelectionRanges);
  Result.InternalAPI.ImporterFactory := ASourcePieceTable.DocumentModel.InternalAPI.ImporterFactory;
  Result.InternalAPI.ExporterFactory := ASourcePieceTable.DocumentModel.InternalAPI.ExporterFactory;
  Result.CalculateDocumentVariable.Add(DoCalculateDocumentVariable);
  Result.BeginUpdate;
  try
    AOptions := TdxDocumentModelCopyOptions.Create(ASelectionRanges);
    try
      AOptions.ParagraphNumerationCopyOptions := AParagraphNumerationCopyOptions;
      AOptions.FormattingCopyOptions := AFormattingCopyOptions;
      AOptions.DefaultPropertiesCopyOptions := DefaultPropertiesCopyOptions;
      ACopyCommand := TdxDocumentModelCopyCommand(ASourcePieceTable.DocumentModel.CreateDocumentModelCopyCommand(ASourcePieceTable,
        Result, AOptions));
      try
        ACopyCommand.FixLastParagraph := FixLastParagraph;
        ACopyCommand.SuppressFieldsUpdate := ASuppressFieldsUpdate;
        ACopyCommand.AllowCopyWholeFieldResult := AllowCopyWholeFieldResult;
        ACopyCommand.Execute;
      finally
        ACopyCommand.Free;
      end;
    finally
      AOptions.Free;
    end;
  finally
    Result.EndUpdate;
  end;
  Result.CalculateDocumentVariable.Remove(DoCalculateDocumentVariable);
end;

function TdxCopySelectionManager.GetRtfText(APieceTable: TdxPieceTable;
  ASelectionRanges: TdxSelectionRangeCollection;
  AOptions: TdxRtfDocumentExporterOptions;
  AForceRaiseBeforeExport: Boolean): AnsiString;
var
  ATarget: TdxDocumentModel;
  ALastParagraphRunSelected: Boolean;
begin
  try
    ATarget := CreateDocumentModel(TdxParagraphNumerationCopyOptions.CopyIfWholeSelected,
      TdxFormattingCopyOptions.UseDestinationStyles,
      APieceTable, ASelectionRanges, True, nil);
    try
      SubscribeTargetModelEvents(ATarget);
      ALastParagraphRunSelected := IsLastParagraphRunSelected(APieceTable, ASelectionRanges);
      Result := ATarget.InternalAPI.GetDocumentRtfContent(AOptions, not ALastParagraphRunSelected,
        AForceRaiseBeforeExport);
      UnsubscribeTargetModelEvents(ATarget);
    finally
      ATarget.Free;
    end;
  except
    Result := '';
  end;
end;

function TdxCopySelectionManager.GetPlainText(APieceTable: TdxPieceTable;
  ASelectionCollection: TdxSelectionRangeCollection; AOptions: TdxPlainTextDocumentExporterOptions = nil;
  AGetTextOptions: PdxTextFragmentOptions = nil): string;
var
  ATarget: TdxDocumentModel;
begin
  try
    ATarget := CreateDocumentModel(TdxParagraphNumerationCopyOptions.CopyAlways,
      TdxFormattingCopyOptions.UseDestinationStyles, APieceTable,
      ASelectionCollection, True, AGetTextOptions);
    try
      SubscribeTargetModelEvents(ATarget);
      Result := ATarget.InternalAPI.GetDocumentPlainTextContent(AOptions);
      UnsubscribeTargetModelEvents(ATarget);
    finally
      ATarget.Free;
    end;
  except
    Result := '';
  end;
end;

function TdxCopySelectionManager.GetSilverlightXamlStream(APieceTable: TdxPieceTable;
  ASelectionCollection: TdxSelectionRangeCollection): TMemoryStream;
begin
Assert(False, 'not implemented');
Result := nil;
end;

function TdxCopySelectionManager.GetSuppressStoreImageSizeCollection(APieceTable: TdxPieceTable;
  ASelectionCollection: TdxSelectionRangeCollection): string;
var
  ATarget: TdxDocumentModel;
  ARuns: TdxTextRunCollection;
  ASuppressStoreImageScale: TStringBuilder;
  I: Integer;
  APictureRun: TdxInlinePictureRun;
begin
  ASuppressStoreImageScale := TStringBuilder.Create;
  try
    ATarget := CreateDocumentModel(TdxParagraphNumerationCopyOptions.CopyIfWholeSelected,
      TdxFormattingCopyOptions.UseDestinationStyles, APieceTable, ASelectionCollection);
    try
      ARuns := ATarget.ActivePieceTable.Runs;
      for I := 0 to ARuns.Count - 1 do
      begin
        if ARuns[I] is TdxInlinePictureRun then
        begin
          APictureRun := TdxInlinePictureRun(ARuns[I]);
          if APictureRun.Image.SuppressStore then
            ASuppressStoreImageScale.Append(Format('%d,%d', [APictureRun.ScaleX, APictureRun.ScaleY]));
        end;
      end;
      UnsubscribeTargetModelEvents(ATarget);
    finally
      ATarget.Free;
    end;
    Result := ASuppressStoreImageScale.ToString;
  finally
    ASuppressStoreImageScale.Free;
  end;
end;

function TdxCopySelectionManager.IsLastParagraphRunSelected(APieceTable: TdxPieceTable; ASelectionRanges: TdxSelectionRangeCollection): Boolean;
var
  ALastRange: TdxSelectionRange;
  ASelectionRangeIndex: Integer;
  ALastLogPosition: TdxDocumentLogPosition;
  AParagraphIndex: TdxParagraphIndex;
  AParagraphs: TdxParagraphCollection;
  AParagraph: TdxParagraph;
begin
  ALastRange := ASelectionRanges.Last;
  ASelectionRangeIndex := ASelectionRanges.Count - 1;
  while (ALastRange.Length = 0) and (ASelectionRangeIndex > 0) do
  begin
    Dec(ASelectionRangeIndex);
    ALastRange := ASelectionRanges[ASelectionRangeIndex];
  end;
  if ALastRange.Length = 0 then
    Result := False
  else
  begin
    ALastLogPosition := Max(ALastRange.Start, ALastRange.&End);
    AParagraphIndex := APieceTable.FindParagraphIndexCore(ALastLogPosition);
    AParagraphs := APieceTable.Paragraphs;
    if AParagraphIndex = not AParagraphs.Count then
      Result := True
    else
    begin
      if AParagraphIndex < 0 then
        Result := False
      else
      begin
        AParagraph := AParagraphs[AParagraphIndex];
        Result := (ALastLogPosition = AParagraph.LogPosition) or
          (ALastLogPosition = AParagraph.EndLogPosition + 1);
      end;
    end;
  end;
end;

procedure TdxCopySelectionManager.SetDataObject(APieceTable: TdxPieceTable;
  ASelectionCollection: TdxSelectionRangeCollection; ADataObject: IdxDataObject);
var
  AOptions: TdxRtfDocumentExporterOptions;
  APlainText: string;
  ARtfText: AnsiString;
  ASuppressStoreImageScale: string;
begin
  ADataObject.Open;
  try
    DefaultPropertiesCopyOptions := TdxDefaultPropertiesCopyOptions.Always;
    AOptions := TdxRtfDocumentExporterOptions.Create;
    try
      AOptions.ExportFinalParagraphMark := TdxExportFinalParagraphMark.Never;
      ARtfText := GetRtfText(APieceTable, ASelectionCollection, AOptions, True);
      if ARtfText <> '' then
      begin
        ADataObject.SetData(TdxOfficeDataFormats.Rtf, ARtfText);
        ADataObject.SetData(TdxOfficeDataFormats.RtfWithoutObjects, ARtfText);
      end;
    finally
      AOptions.Free;
    end;
    APlainText := GetPlainText(APieceTable, ASelectionCollection);
    if APlainText <> '' then
      ADataObject.SetData(TdxOfficeDataFormats.UnicodeText, APlainText);
    ASuppressStoreImageScale := GetSuppressStoreImageSizeCollection(APieceTable, ASelectionCollection);
    if ASuppressStoreImageScale <> '' then
      ADataObject.SetData(TdxOfficeDataFormats.SuppressStoreImageSize, ASuppressStoreImageScale);
  finally
    ADataObject.Close;
  end;
end;

function TdxCopySelectionManager.ShouldExtendRange(AInfo: TdxRunInfo; AField: TdxField): Boolean;
begin
  Result := AField <> nil;
  if Result then
  begin
    Assert(False);
  end;
end;

function TdxCopySelectionManager.UpdateSelectionCollection(ASourcePieceTable: TdxPieceTable;
  ASelection: TdxSelectionRangeCollection): TdxSelectionRangeCollection;
var
  I: Integer;
  ARange: TdxSelectionRange;
  AInfo: TdxRunInfo;
  AField: TdxField;
begin
  AllowCopyWholeFieldResult := False;
  Result := TdxSelectionRangeCollection.Create;
  for I := 0 to ASelection.Count - 1 do
  begin
    ARange := ASelection[I];
    AInfo := ASourcePieceTable.FindRunInfo(ARange.Start, ARange.Length);
    try
      AField := ASourcePieceTable.FindFieldByRunIndex(AInfo.Start.RunIndex);
      if ShouldExtendRange(AInfo, AField) then
      begin
        Assert(False, 'not implemented TdxField');
      end;
      AField := ASourcePieceTable.FindFieldByRunIndex(AInfo.&End.RunIndex);
      if ShouldExtendRange(AInfo, AField) then
      begin
        Assert(False, 'not implemented TdxField');
      end;
    finally
      AInfo.Free;
    end;
  end;
end;

procedure TdxCopySelectionManager.DoBeforeExport(Sender: TObject; E: TdxBeforeExportEventArgs);
begin
  DocumentServer.OnBeforeExport(Sender, E);
end;

procedure TdxCopySelectionManager.DoCalculateDocumentVariable(Sender: TObject; E: TdxCalculateDocumentVariableEventArgs);
begin
  DocumentServer.OnCalculateDocumentVariable(sender, e);
end;

procedure TdxCopySelectionManager.SubscribeTargetModelEvents(ATargetModel: TdxDocumentModel);
begin
  ATargetModel.BeforeExport.Add(DoBeforeExport);
  ATargetModel.CalculateDocumentVariable.Add(DoCalculateDocumentVariable);
end;

procedure TdxCopySelectionManager.UnsubscribeTargetModelEvents(ATargetModel: TdxDocumentModel);
begin
  ATargetModel.BeforeExport.Remove(DoBeforeExport);
  ATargetModel.CalculateDocumentVariable.Remove(DoCalculateDocumentVariable);
end;

{ TdxTextFragmentOptions }

class function TdxTextFragmentOptions.Create: TdxTextFragmentOptions;
begin
  Result.AllowExtendingDocumentRange := True;
  Result.PreserveOriginalNumbering := False;
end;

{ TdxRichEditControlOptionsBase }

constructor TdxRichEditControlOptionsBase.Create(
  ADocumentServer: IdxInnerControl);
begin
  inherited Create;
  FDocumentServer := ADocumentServer;
end;

procedure TdxRichEditControlOptionsBase.Assign(Source: TPersistent);
var
  ASource: TdxRichEditControlOptionsBase;
begin
  BeginUpdate;
  try
    if Source is TdxRichEditControlOptionsBase then
    begin
      ASource := TdxRichEditControlOptionsBase(Source);
      Behavior.Assign(ASource.Behavior);
      DocumentCapabilities.Assign(ASource.DocumentCapabilities);
      ShowHiddenText := ASource.ShowHiddenText;
    end;
    inherited Assign(Source);
  finally
    EndUpdate;
  end;
end;

function TdxRichEditControlOptionsBase.GetBehavior: TdxRichEditBehaviorOptions;
begin
  Result := DocumentServer.DocumentModel.BehaviorOptions;
end;

function TdxRichEditControlOptionsBase.GetDocumentCapabilities: TdxDocumentCapabilitiesOptions;
begin
  if DocumentServer = nil then
    Result := nil
  else
    Result := DocumentServer.DocumentModel.DocumentCapabilities;
end;

function TdxRichEditControlOptionsBase.GetFormattingMarkVisibility: TdxFormattingMarkVisibilityOptions;
begin
  if DocumentServer = nil then
    Result := nil
  else
    Result := DocumentServer.DocumentModel.FormattingMarkVisibilityOptions;
end;

function TdxRichEditControlOptionsBase.GetShowHiddenText: Boolean;
begin
  Result := FormattingMarkVisibility.ShowHiddenText;
end;

procedure TdxRichEditControlOptionsBase.SetBehavior(const Value: TdxRichEditBehaviorOptions);
begin
  Behavior.Assign(Value);
end;

procedure TdxRichEditControlOptionsBase.SetDocumentCapabilities(const Value: TdxDocumentCapabilitiesOptions);
begin
  DocumentCapabilities.Assign(Value);
end;

procedure TdxRichEditControlOptionsBase.SetShowHiddenText(const Value: Boolean);
begin
  if ShowHiddenText = Value then
    Exit;
  FormattingMarkVisibility.ShowHiddenText := Value;
  DoChanged; 
end;

end.
