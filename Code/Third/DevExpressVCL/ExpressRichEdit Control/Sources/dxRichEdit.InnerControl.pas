{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.InnerControl;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Types, Classes, SysUtils, Graphics, Controls, Generics.Collections, cxControls, dxCoreClasses,
  dxRichEdit.Utils.BatchUpdateHelper,
  dxRichEdit.Utils.BackgroundThreadUIUpdater,
  dxRichEdit.Utils.PredefinedFontSizeCollection,
  dxRichEdit.DocumentModel.Core,
  dxRichEdit.DocumentModel.PieceTable,
  dxRichEdit.DocumentModel.CharacterFormatting,
  dxRichEdit.DocumentLayout.UnitConverter,
  dxRichEdit.LayoutEngine.DocumentFormatter,
  dxRichEdit.View.Core,
  dxRichEdit.View.Simple,
  dxRichEdit.Platform.Font,
  dxRichEdit.Platform.Win.Painter,
  dxRichEdit.DocumentModel.ParagraphFormatting;

type

  TdxDocumentModelAccessor = class;
  TdxInnerRichEditDocumentServer = class;
  TdxRichEditDocumentServerOptions = class;

  { TdxRichEditControlDeferredChanges }

  TdxRichEditControlDeferredChanges = class
  private
    FResize: Boolean;
    FRedraw: Boolean;
    FRedrawAction: TdxRefreshAction;
    FRaiseUpdateUI: Boolean;
  public
		property Redraw: Boolean read FRedraw write FRedraw;
    property Resize: Boolean read FResize write FResize;
    property RaiseUpdateUI: Boolean read FRaiseUpdateUI write FRaiseUpdateUI;
    property RedrawAction: TdxRefreshAction read FRedrawAction write FRedrawAction;
  end;

  { TdxDocumentDeferredChanges }

  TdxDocumentDeferredChanges = class
  private
    FChangeActions: TdxDocumentModelChangeActions;
    FStartRunIndex: TdxRunIndex;
    FEndRunIndex: TdxRunIndex;
  public
    property ChangeActions: TdxDocumentModelChangeActions read FChangeActions write FChangeActions;
    property StartRunIndex: TdxRunIndex read FStartRunIndex write FStartRunIndex;
    property EndRunIndex: TdxRunIndex read FEndRunIndex write FEndRunIndex;
  end;

  { IdxInnerRichEditControlOwner }

  IdxInnerRichEditControlOwner = interface(IdxInnerRichEditDocumentServerOwner)
  ['{06330B38-247B-48C7-9296-42FE3CE94C12}']
    function GetRichEditControl: TObject;
    function CreateVerticalScrollBar: IdxOfficeScrollbar;
    function CreateHorizontalScrollBar: IdxOfficeScrollbar;

		procedure ActivateViewPlatformSpecific(AView: TdxRichEditView); 
    procedure RedrawEnsureSecondaryFormattingComplete; overload;
    procedure RedrawEnsureSecondaryFormattingComplete(AAction: TdxRefreshAction); overload; 
    procedure OnResizeCore; 
    function CalculateActualViewBounds(const APreviousViewBounds: TRect): TRect; 
    function CreateViewRepository: TdxRichEditCustomViewRepository;
    procedure OnResize;
    procedure Redraw; overload;
    procedure Redraw(AAction: TdxRefreshAction); overload;
    procedure RedrawAfterEndUpdate;
    procedure ApplyChangesCorePlatformSpecific(AChangeActions: TdxDocumentModelChangeActions); 
    procedure OnZoomFactorChangingPlatformSpecific;
    procedure OnActiveViewBackColorChanged;
    procedure ResizeView(AEnsureCaretVisibleOnResize: Boolean); 

    property RichEditControl: TObject read GetRichEditControl;


    procedure ShowCaret; 
    procedure HideCaret; 
    procedure ShowParagraphForm(AParagraphProperties: TdxMergedParagraphProperties; ACallback: TdxShowParagraphFormCallback; ACallbackData: TObject);  
  end;

  { IdxRichEditDocumentServer }

  CancelEventHandler = TdxEventHandler;
  CalculateDocumentVariableEventHandler = TdxEventHandler;
  BeforeImportEventHandler = TdxEventHandler;
  BeforeExportEventHandler = TdxEventHandler;
  Document = class(TObject);
  DocumentFormat = class(TObject);
  DocumentUnit = class(TObject);

  { IdxRichEditDocumentServer }

  IdxRichEditDocumentServer = interface(IdxBatchUpdateable) 
  ['{A5B76611-FBB7-422D-A7A0-7F5ECEA3D4E4}']
    function GetModel: TdxDocumentModelAccessor;
    function GetDpiX: Single;
    function GetDpiY: Single;
    function GetModified: Boolean;
    function GetText: string;
    function GetRtfText: string;
    function GetHtmlText: string;
    function GetMhtText: string;
    function GetWordMLText: string;
    function GetOpenXmlBytes: TBytes;
    function GetOpenDocumentBytes: TBytes;
    function GetDocument: Document;
    function GetUnit: DocumentUnit;
    function GetLayoutUnit: TdxDocumentLayoutUnit;
    procedure SetModified(Value: Boolean);
    procedure SetText(const Value: string);
    procedure SetRtfText(const Value: string);
    procedure SetHtmlText(const Value: string);
    procedure SetMhtText(const Value: string);
    procedure SetWordMLText(const Value: string);
    procedure SetOpenXmlBytes(const Value: TBytes);
    procedure SetOpenDocumentBytes(const Value: TBytes);
    procedure SetUnit(Value: DocumentUnit);
    procedure SetLayoutUnit(Value: TdxDocumentLayoutUnit);
    function GetSelectionChanged: TdxEventHandler;
    function GetDocumentLoaded: TdxEventHandler;
    function GetEmptyDocumentCreated: TdxEventHandler;
    function GetDocumentClosing: CancelEventHandler;
    function GetContentChanged: TdxEventHandler;
    function GetRtfTextChanged: TdxEventHandler;
    function GetHtmlTextChanged: TdxEventHandler;
    function GetMhtTextChanged: TdxEventHandler;
    function GetWordMLTextChanged: TdxEventHandler;
    function GetOpenXmlBytesChanged: TdxEventHandler;
    function GetOpenDocumentBytesChanged: TdxEventHandler;
    function GetModifiedChanged: TdxEventHandler;
    function GetUnitChanging: TdxEventHandler;
    function GetUnitChanged: TdxEventHandler;
    function GetCalculateDocumentVariable: CalculateDocumentVariableEventHandler;
    function GetBeforeImport: BeforeImportEventHandler;
    function GetBeforeExport: BeforeExportEventHandler;
    function GetInitializeDocument: TdxEventHandler;
    procedure SetSelectionChanged(Value: TdxEventHandler);
    procedure SetDocumentLoaded(Value: TdxEventHandler);
    procedure SetEmptyDocumentCreated(Value: TdxEventHandler);
    procedure SetDocumentClosing(Value: CancelEventHandler);
    procedure SetContentChanged(Value: TdxEventHandler);
    procedure SetRtfTextChanged(Value: TdxEventHandler);
    procedure SetHtmlTextChanged(Value: TdxEventHandler);
    procedure SetMhtTextChanged(Value: TdxEventHandler);
    procedure SetWordMLTextChanged(Value: TdxEventHandler);
    procedure SetOpenXmlBytesChanged(Value: TdxEventHandler);
    procedure SetOpenDocumentBytesChanged(Value: TdxEventHandler);
    procedure SetModifiedChanged(Value: TdxEventHandler);
    procedure SetUnitChanging(Value: TdxEventHandler);
    procedure SetUnitChanged(Value: TdxEventHandler);
    procedure SetCalculateDocumentVariable(Value: CalculateDocumentVariableEventHandler);
    procedure SetBeforeImport(Value: BeforeImportEventHandler);
    procedure SetBeforeExport(Value: BeforeExportEventHandler);
    procedure SetInitializeDocument(Value: TdxEventHandler);

    function CreateNewDocument(ARaiseDocumentClosing: Boolean): Boolean;
    procedure LoadDocument(AStream: TStream; ADocumentFormat: DocumentFormat);
    procedure SaveDocument(AStream: TStream; ADocumentFormat: DocumentFormat);
    procedure LoadDocumentTemplate(AStream: TStream; ADocumentFormat: DocumentFormat);


    property SelectionChanged: TdxEventHandler read GetSelectionChanged write SetSelectionChanged;
    property DocumentLoaded: TdxEventHandler read GetDocumentLoaded write SetDocumentLoaded;
    property EmptyDocumentCreated: TdxEventHandler read GetEmptyDocumentCreated write SetEmptyDocumentCreated;
    property DocumentClosing: CancelEventHandler read GetDocumentClosing write SetDocumentClosing;
    property ContentChanged: TdxEventHandler read GetContentChanged write SetContentChanged;
    property RtfTextChanged: TdxEventHandler read GetRtfTextChanged write SetRtfTextChanged;
    property HtmlTextChanged: TdxEventHandler read GetHtmlTextChanged write SetHtmlTextChanged;
    property MhtTextChanged: TdxEventHandler read GetMhtTextChanged write SetMhtTextChanged;
    property WordMLTextChanged: TdxEventHandler read GetWordMLTextChanged write SetWordMLTextChanged;
    property OpenXmlBytesChanged: TdxEventHandler read GetOpenXmlBytesChanged write SetOpenXmlBytesChanged;
    property OpenDocumentBytesChanged: TdxEventHandler read GetOpenDocumentBytesChanged write SetOpenDocumentBytesChanged;
    property ModifiedChanged: TdxEventHandler read GetModifiedChanged write SetModifiedChanged;
    property UnitChanging: TdxEventHandler read GetUnitChanging write SetUnitChanging;
    property UnitChanged: TdxEventHandler read GetUnitChanged write SetUnitChanged;
    property CalculateDocumentVariable: CalculateDocumentVariableEventHandler read GetCalculateDocumentVariable write SetCalculateDocumentVariable;
    property BeforeImport: BeforeImportEventHandler read GetBeforeImport write SetBeforeImport;
    property BeforeExport: BeforeExportEventHandler read GetBeforeExport write SetBeforeExport;
    property InitializeDocument: TdxEventHandler read GetInitializeDocument write SetInitializeDocument;


    property Model: TdxDocumentModelAccessor read GetModel;
    property DpiX: Single read GetDpiX;
    property DpiY: Single read GetDpiY;
    property Modified: Boolean read GetModified write SetModified;
    property Text: string read GetText write SetText;
    property RtfText: string read GetRtfText write SetRtfText;
    property HtmlText: string read GetHtmlText write SetHtmlText;
    property MhtText: string read GetMhtText write SetMhtText;
    property WordMLText: string read GetWordMLText write SetWordMLText;
    property OpenXmlBytes: TBytes read GetOpenXmlBytes write SetOpenXmlBytes;
    property OpenDocumentBytes: TBytes read GetOpenDocumentBytes write SetOpenDocumentBytes;
    property Document: Document read GetDocument;
    property &Unit: DocumentUnit read GetUnit write SetUnit;
    property LayoutUnit: TdxDocumentLayoutUnit read GetLayoutUnit write SetLayoutUnit;
  end;

  { TdxDocumentModelAccessor }

  TdxDocumentModelAccessor = class
  private
    FDocumentModel: TdxDocumentModel;
  public
    constructor Create(ADocumentModel: TdxDocumentModel);

    property DocumentModel: TdxDocumentModel read FDocumentModel;
  end;

  { TdxInnerRichEditDocumentServer }

  TdxInnerRichEditDocumentServer = class(TcxIUnknownObject, IdxBatchUpdateable, IdxBatchUpdateHandler) 
  private
    FOwner: IdxInnerRichEditDocumentServerOwner;
    FBatchUpdateHelper: TdxBatchUpdateHelper;
    FPredefinedFontSizeCollection: TdxPredefinedFontSizeCollection;
    FExistingDocumentModel: TdxDocumentModel;
    FDocumentModel: TdxDocumentModel;
    FOptions: TdxRichEditControlOptionsBase;
    FDocumentModelTemplate: TdxDocumentModel;
    FDocumentModelAccessor: TdxDocumentModelAccessor;
    FDocumentDeferredChanges: TdxDocumentDeferredChanges;
    FDocumentDeferredChangesOnIdle: TdxDocumentDeferredChanges;
    FMeasurementAndDrawingStrategy: TdxMeasurementAndDrawingStrategy;

    FOnBeforeExport: TdxMulticastMethod<TdxBeforeExportEventHandler>;
    FOnUpdateUI: TdxMulticastMethod<TdxEventHandler>;

    function GetDpiX: Single;
    function GetDpiY: Single;
    function GetLayoutUnit: TdxDocumentLayoutUnit;
    {TODO: rename}
    procedure SetLayoutUnit_(const Value: TdxDocumentLayoutUnit);
    function GetMeasurer: TdxBoxMeasurer;
    function GetDocumentModelTemplate: TdxDocumentModel;
    function GetModified: Boolean;
    procedure SetModified(const Value: Boolean);
  protected
    procedure SaveDocumentCore(AStream: TStream; ADocumentFormat: TdxDocumentFormat; const ATargetUri: string = '');

    property Options: TdxRichEditControlOptionsBase read FOptions;
  public
    constructor Create(AOwner: IdxInnerRichEditDocumentServerOwner); overload;
    constructor Create(AOwner: IdxInnerRichEditDocumentServerOwner; ADocumentModel: TdxDocumentModel); overload;
    destructor Destroy; override;
    function CreateOptions: TdxRichEditControlOptionsBase; virtual; abstract;
    procedure RaiseBeforeExport(const Args: TdxBeforeExportEventArgs);
    procedure RaiseActiveViewChanged; virtual; 
    procedure RaiseSelectionChanged; virtual; 
    procedure BeginInitialize; virtual;
    procedure EndInitialize; virtual;
    procedure AddServices;

    function GetBatchUpdateHelper: TdxBatchUpdateHelper;
    function GetIsUpdateLocked: Boolean;
    procedure BeginUpdate;
    procedure EndUpdate;
    procedure CancelUpdate;

    property BatchUpdateHelper: TdxBatchUpdateHelper read GetBatchUpdateHelper;
    property IsUpdateLocked: Boolean read GetIsUpdateLocked;

    procedure OnFirstBeginUpdate;
    procedure OnBeginUpdate;
    procedure OnEndUpdate;
    procedure OnLastEndUpdate;
    procedure OnCancelUpdate;
    procedure OnLastCancelUpdate;

    procedure CreateDocumentModelTemplate; virtual;

    procedure OnFirstBeginUpdateCore; virtual;
    procedure OnLastEndUpdateCore; virtual;

    function GetDocumentModel: TdxDocumentModel; virtual;
    function CreateDocumentModelCore: TdxDocumentModel; virtual;
    procedure SubscribeDocumentModelEvents; virtual;
    procedure UnsubscribeDocumentModelEvents; virtual;

    procedure OnSelectionChanged(Sender: TObject; E: TdxEventArgs); virtual; 
    procedure OnInnerSelectionChanged(Sender: TObject; E: TdxEventArgs); virtual; 
    procedure OnBeginDocumentUpdate(ASender: TObject; E: TdxEventArgs); virtual; 
    procedure OnEndDocumentUpdate(ASender: TObject; E: TdxDocumentUpdateCompleteEventArgs); virtual; 
    procedure OnBeginDocumentUpdateCore; virtual; 
    function ProcessEndDocumentUpdateCore(ASender: TObject; E: TdxDocumentUpdateCompleteEventArgs): TdxDocumentModelChangeActions; virtual; 
    function OnEndDocumentUpdateCore(ASender: TObject; E: TdxDocumentUpdateCompleteEventArgs): TdxDocumentModelChangeActions; virtual; 
    procedure OnModifiedChanged(ASender: TObject; E: TdxEventArgs); virtual; 
    procedure ApplyFontAndForeColor; virtual; 
    procedure ApplyChangesCore(AChangeActions: TdxDocumentModelChangeActions); virtual; 
    procedure RaiseDeferredEvents(AChangeActions: TdxDocumentModelChangeActions); virtual; 

    function CreateMeasurementAndDrawingStrategy: TdxMeasurementAndDrawingStrategy; virtual; 
    procedure CreateNewMeasurementAndDrawingStrategy; virtual; 
    procedure RecreateMeasurementAndDrawingStrategy; virtual; 

    procedure SetLayoutUnit(AUnit: TdxDocumentLayoutUnit); virtual; 
    procedure SetLayoutUnitCore(AUnit: TdxDocumentLayoutUnit); virtual; 
    procedure SetDocumentModelLayoutUnit(AUnit: TdxDocumentLayoutUnit); virtual; 
    procedure SetDocumentModelLayoutUnitCore(AUnit: TdxDocumentLayoutUnit); virtual; 
    procedure OnApplicationIdle; virtual;

    procedure LoadDocumentCore(AStream: TStream; ADocumentFormat: TdxDocumentFormat; const ASourceUri: string); virtual; 
    procedure LoadDocument(AStream: TStream; ADocumentFormat: TdxDocumentFormat); overload; virtual; 
    procedure LoadDocument(const AFileName: string; ADocumentFormat: TdxDocumentFormat; ALoadAsTemplate: Boolean); overload; virtual; 
    procedure LoadDocument(const AFileName: string); overload; virtual; 
    procedure LoadDocument(const AFileName: string; ADocumentFormat: TdxDocumentFormat); overload; virtual; 

    procedure SaveDocument(const AFileName: string); overload;
    procedure SaveDocument(const AFileName: string; ADocumentFormat: TdxDocumentFormat); overload;
    procedure RaiseUpdateUI;
    procedure OnUpdateUI; virtual;

    property Owner: IdxInnerRichEditDocumentServerOwner read FOwner; 
    property DpiX: Single read GetDpiX;
    property DpiY: Single read GetDpiY;
    property PredefinedFontSizeCollection: TdxPredefinedFontSizeCollection read FPredefinedFontSizeCollection;
    property Model: TdxDocumentModelAccessor read FDocumentModelAccessor; 
    property DocumentModel: TdxDocumentModel read FDocumentModel; 
    property DocumentModelTemplate: TdxDocumentModel read GetDocumentModelTemplate;
    property MeasurementAndDrawingStrategy: TdxMeasurementAndDrawingStrategy read FMeasurementAndDrawingStrategy; 
    property Measurer: TdxBoxMeasurer read GetMeasurer; 

    property LayoutUnit: TdxDocumentLayoutUnit read GetLayoutUnit write SetLayoutUnit_; 
    property Modified: Boolean read GetModified write SetModified;
    property DocumentDeferredChanges: TdxDocumentDeferredChanges read FDocumentDeferredChanges; 

    property OnBeforeExport: TdxMulticastMethod<TdxBeforeExportEventHandler> read FOnBeforeExport;
  end;

  { TdxInnerRichEditControl }

  TdxInnerRichEditControl = class(TdxInnerRichEditDocumentServer, IdxInnerControl) 
  private
    function InternalGetOptions: TdxRichEditDocumentServerOptions; overload;
    function InternalGetDocumentModel: TdxDocumentModel;
  protected
    function IdxInnerControl.GetDocumentModel = InternalGetDocumentModel;
    function CreateCopySelectionManager: TdxCopySelectionManager;
    function CreateMouseCursorCalculator: TdxMouseCursorCalculator;
    function GetActiveView: TdxRichEditView;
    function GetFormatter: TdxBackgroundFormatter;
    function GetOptions: TdxRichEditControlOptionsBase;
    function GetPredefinedFontSizeCollection: TdxPredefinedFontSizeCollection;
    procedure OnBeforeExport(Sender: TObject; E: TdxBeforeExportEventArgs);
    procedure OnCalculateDocumentVariable(Sender: TObject; E: TdxCalculateDocumentVariableEventArgs);
    procedure BeginScrollbarUpdate(const AScrollbar: IdxOfficeScrollbar); virtual;
    procedure EndScrollbarUpdate(const AScrollbar: IdxOfficeScrollbar); virtual;
    function GetHorizontalScrollBar: IdxOfficeScrollbar; 
    function GetVerticalScrollBar: IdxOfficeScrollbar;   
  private
    FBackgroundThreadUIUpdater: TdxBackgroundThreadUIUpdater;
    FFormatter: TdxBackgroundFormatter; 
    FActiveView: TdxRichEditView; 
		FViews: TdxRichEditCustomViewRepository;
    FOvertype: Boolean;
    FReadOnly: Boolean;
    FOnReadOnlyChanged: TdxMulticastMethod<TdxEventHandler>;
    FOnZoomChanged: TdxMulticastMethod<TdxEventHandler>;
    FDeferredChanges: TdxRichEditControlDeferredChanges; 
    FUpdateUIOnIdle: Boolean;
    FForceUpdateUIOnIdle: Boolean;
    FHorizontalScrollBar: IdxOfficeScrollbar;
    FVerticalScrollBar: IdxOfficeScrollbar;

    function GetOwner: IdxInnerRichEditControlOwner;
    function GetIsHandleCreated: Boolean;
    procedure SetOvertype(const Value: Boolean);
    procedure SetReadOnly(const Value: Boolean);
  protected
    function IsEditable: Boolean;
    function SaveDocumentAs: Boolean;
    function SaveDocument: Boolean;

    procedure CreateViews; virtual;
    procedure DisposeViews; virtual;

    procedure OnOvertypeChanged; virtual;

    property Options: TdxRichEditDocumentServerOptions read InternalGetOptions;
    property HorizontalScrollBar: IdxOfficeScrollbar read FHorizontalScrollBar;
    property VerticalScrollBar: IdxOfficeScrollbar read FVerticalScrollBar;
  public
    destructor Destroy; override;
    function CreateOptions: TdxRichEditControlOptionsBase; override;
    procedure BeginInitialize; override;
    procedure EndInitialize; override;
    procedure CreateNewMeasurementAndDrawingStrategy; override;
    procedure OnFirstBeginUpdateCore; override;
    procedure OnLastEndUpdateCore; override;
    procedure OnBeginDocumentUpdateCore; override;
    function OnEndDocumentUpdateCore(ASender: TObject; E: TdxDocumentUpdateCompleteEventArgs): TdxDocumentModelChangeActions; override;
    procedure ApplyChangesCore(AChangeActions: TdxDocumentModelChangeActions); override; 
    procedure RedrawEnsureSecondaryFormattingComplete; overload; virtual; 
    procedure RedrawEnsureSecondaryFormattingComplete(AAction: TdxRefreshAction); overload; virtual; 
    procedure OnResize; virtual;
    function CreateViewRepository: TdxRichEditCustomViewRepository; virtual;
    procedure SetActiveView(ANewView: TdxRichEditView); virtual;
    procedure SetActiveViewCore(ANewView: TdxRichEditView); virtual;
    function DeactivateView(AView: TdxRichEditView): TRect; virtual;
    procedure DeactivateViewAndClearActiveView(AView: TdxRichEditView); virtual;
    procedure ActivateView(AView: TdxRichEditView; const AViewBounds: TRect); virtual; 
    function CreateBackgroundFormatter(AController: TdxDocumentFormattingController): TdxBackgroundFormatter; virtual; 
    procedure SubscribeActiveViewEvents; virtual; 
    procedure UnsubscribeActiveViewEvents; virtual; 
    procedure OnActiveViewZoomChanging(Sender: TObject; E: TdxEventArgs); virtual;
    procedure OnActiveViewZoomChanged(Sender: TObject; E: TdxEventArgs); virtual;
    procedure OnActiveViewBackColorChanged(Sender: TObject; E: TdxEventArgs); virtual;
    procedure ActivateViewPlatformSpecific(AView: TdxRichEditView); virtual; 
    procedure DeactivateViewPlatformSpecific(AView: TdxRichEditView); virtual; 
    property IsHandleCreated: Boolean read GetIsHandleCreated; 

    procedure OnResizeCore; virtual; 
    function CalculateActualViewBounds(const APreviousViewBounds: TRect): TRect; virtual; 
    procedure OnUpdateUI; override;
    procedure OnUpdateUICore; virtual;
    procedure OnApplicationIdle; override;
    procedure ApplyFontAndForeColor; override; 
    procedure ApplyFontAndForeColorCore(ACharacterProperties: TdxCharacterProperties); virtual; 
    function GetDefaultCharacterProperties: TdxCharacterProperties; virtual; 
    procedure OnReadOnlyChanged; virtual;
    procedure ApplyChangesCorePlatformSpecific(AChangeActions: TdxDocumentModelChangeActions); virtual; 
    procedure OnZoomFactorChangingPlatformSpecific; virtual;
    function IsHyperlinkModifierKeysPress: Boolean; virtual; 
    procedure UpdateVerticalScrollBar(AAvoidJump: Boolean); virtual;
    procedure BeginDocumentRendering; virtual; 
    procedure EndDocumentRendering; virtual; 
    procedure SetLayoutUnitCore(AUnit: TdxDocumentLayoutUnit); override; 
    procedure SetDocumentModelLayoutUnitCore(AUnit: TdxDocumentLayoutUnit); override; 
    procedure RaiseReadOnlyChanged; virtual;
    procedure RaiseZoomChanged; virtual;

   property Owner: IdxInnerRichEditControlOwner read GetOwner; 
    property Formatter: TdxBackgroundFormatter read FFormatter write FFormatter; 
    property ActiveView: TdxRichEditView read FActiveView; 
    property Views: TdxRichEditCustomViewRepository read FViews;
    property ReadOnly: Boolean read FReadOnly write SetReadOnly;
    property ControlDeferredChanges: TdxRichEditControlDeferredChanges read FDeferredChanges; 

    property ReadOnlyChanged: TdxMulticastMethod<TdxEventHandler> read FOnReadOnlyChanged;
    property ZoomChanged: TdxMulticastMethod<TdxEventHandler> read FOnZoomChanged;


		property BackgroundThreadUIUpdater: TdxBackgroundThreadUIUpdater read FBackgroundThreadUIUpdater write FBackgroundThreadUIUpdater;
    property Overtype: Boolean read FOvertype write SetOvertype;
  end;

  { TdxRichEditScrollbarOptions }

  TdxRichEditScrollbarVisibility = (Auto, Visible, Hidden);

  TdxRichEditScrollbarOptions = class(TcxOwnedPersistent)
  strict private
    FOptions: TdxRichEditDocumentServerOptions;
    FVisibility: TdxRichEditScrollbarVisibility;
  private
    procedure SetVisibility(const Value: TdxRichEditScrollbarVisibility);
  public
    constructor Create(AOptions: TdxRichEditDocumentServerOptions); reintroduce;
    procedure Assign(Source: TPersistent); override;

    property Visibility: TdxRichEditScrollbarVisibility read FVisibility write SetVisibility default TdxRichEditScrollbarVisibility.Auto;
  end;

  TdxVerticalScrollbarOptions = class(TdxRichEditScrollbarOptions)
  published
    property Visibility;
  end;

  TdxHorizontalScrollbarOptions = class(TdxRichEditScrollbarOptions)
  published
    property Visibility;
  end;

  { RichEditDocumentServerOptions }

  TdxRichEditDocumentServerOptions = class(TdxRichEditControlOptionsBase)
  strict private
    FVerticalScrollbar: TdxVerticalScrollbarOptions;
    FHorizontalScrollbar: TdxHorizontalScrollbarOptions;
    procedure SetHorizontalScrollbar(const Value: TdxHorizontalScrollbarOptions);
    procedure SetVerticalScrollbar(const Value: TdxVerticalScrollbarOptions);
  public
    constructor Create(ADocumentServer: IdxInnerControl); override;
    destructor Destroy; override;
  published
    property Behavior;
    property ShowHiddenText;
    property DocumentCapabilities;
    property HorizontalScrollbar: TdxHorizontalScrollbarOptions read FHorizontalScrollbar write SetHorizontalScrollbar;
    property VerticalScrollbar: TdxVerticalScrollbarOptions read FVerticalScrollbar write SetVerticalScrollbar;
  end;

implementation

uses
  Math,
  cxGeometry, dxTypeHelpers, dxRichEdit.DocumentModel.Styles, dxRichEdit.DocumentModel.Numbering;

{ TdxDocumentModelAccessor }

constructor TdxDocumentModelAccessor.Create(ADocumentModel: TdxDocumentModel);
begin
  inherited Create;
  Assert(ADocumentModel <> nil);
  FDocumentModel := ADocumentModel;
end;

{ TdxInnerRichEditDocumentServer }

constructor TdxInnerRichEditDocumentServer.Create(AOwner: IdxInnerRichEditDocumentServerOwner);
begin
  inherited Create;
  Assert(AOwner <> nil); 
  FOwner := AOwner;
  FPredefinedFontSizeCollection := TdxPredefinedFontSizeCollection.Create;
end;

constructor TdxInnerRichEditDocumentServer.Create(AOwner: IdxInnerRichEditDocumentServerOwner;
  ADocumentModel: TdxDocumentModel);
begin
  Create(AOwner);
  Assert(ADocumentModel<> nil); 
  FExistingDocumentModel := ADocumentModel;
end;

destructor TdxInnerRichEditDocumentServer.Destroy;
begin
  FreeAndNil(FOptions);
  UnsubscribeDocumentModelEvents;
  FreeAndNil(FDocumentModel);
  FreeAndNil(FDocumentModelAccessor);
  FreeAndNil(FMeasurementAndDrawingStrategy);
  FreeAndNil(FPredefinedFontSizeCollection);
  FreeAndNil(FBatchUpdateHelper);
  if FExistingDocumentModel <> FDocumentModelTemplate then
    FreeAndNil(FDocumentModelTemplate);
  inherited Destroy;
end;


procedure TdxInnerRichEditDocumentServer.RaiseUpdateUI;
var
  E: TdxEventArgs;
begin
  if FOnUpdateUI.Empty then
    Exit;
  E := TdxEventArgs.Create;
  try
    FOnUpdateUI.Invoke(TObject(Owner), E);
  finally
    E.Free;
  end;
end;

procedure TdxInnerRichEditDocumentServer.AddServices;
begin
  NotImplemented;
end;

procedure TdxInnerRichEditDocumentServer.BeginInitialize;
begin
  FBatchUpdateHelper := TdxBatchUpdateHelper.Create(Self);
  FDocumentModel := GetDocumentModel;
  FDocumentModelAccessor := TdxDocumentModelAccessor.Create(FDocumentModel);
  SubscribeDocumentModelEvents;
  FOptions := CreateOptions;
end;

procedure TdxInnerRichEditDocumentServer.BeginUpdate;
begin
  FBatchUpdateHelper.BeginUpdate;
end;

procedure TdxInnerRichEditDocumentServer.CancelUpdate;
begin
  FBatchUpdateHelper.CancelUpdate;
end;

function TdxInnerRichEditDocumentServer.CreateDocumentModelCore: TdxDocumentModel;
begin
  Result := TdxDocumentModel.Create;
end;

procedure TdxInnerRichEditDocumentServer.CreateDocumentModelTemplate;
begin
  FDocumentModelTemplate := GetDocumentModel;
  FDocumentModelTemplate.BeginSetContent;
  try
    TdxDefaultNumberingListHelper.InsertNumberingLists(FDocumentModelTemplate, DocumentModel.UnitConverter, DocumentModel.DocumentProperties.DefaultTabWidth);
  finally
    FDocumentModelTemplate.EndSetContent(TdxDocumentModelChangeType.None, False, nil);
  end;
end;

function TdxInnerRichEditDocumentServer.CreateMeasurementAndDrawingStrategy: TdxMeasurementAndDrawingStrategy;
begin
  Result := Owner.CreateMeasurementAndDrawingStrategy(DocumentModel);
end;

procedure TdxInnerRichEditDocumentServer.CreateNewMeasurementAndDrawingStrategy;
begin
  FreeAndNil(FMeasurementAndDrawingStrategy);
  FMeasurementAndDrawingStrategy := CreateMeasurementAndDrawingStrategy;
  FMeasurementAndDrawingStrategy.Initialize;
  DocumentModel.SetFontCacheManager(FMeasurementAndDrawingStrategy.CreateFontCacheManager);
end;

procedure TdxInnerRichEditDocumentServer.RecreateMeasurementAndDrawingStrategy;
var
  APieceTable: TdxPieceTable;
  AChangeActions: TdxDocumentModelChangeActions;
begin
  BeginUpdate;
  try
    DocumentModel.BeginUpdate;
    try
      CreateNewMeasurementAndDrawingStrategy;
      APieceTable := DocumentModel.MainPieceTable;
      AChangeActions := [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetSelectionLayout,
        TdxDocumentModelChangeAction.ResetAllPrimaryLayout, TdxDocumentModelChangeAction.ResetSpellingCheck];
      DocumentModel.ApplyChangesCore(APieceTable, AChangeActions, 0 , MaxInt); 
    finally
      DocumentModel.EndUpdate;
    end;
  finally
    EndUpdate;
  end;
end;

procedure TdxInnerRichEditDocumentServer.EndInitialize;
begin
  CreateNewMeasurementAndDrawingStrategy;
end;

procedure TdxInnerRichEditDocumentServer.EndUpdate;
begin
  FBatchUpdateHelper.EndUpdate;
end;

function TdxInnerRichEditDocumentServer.GetBatchUpdateHelper: TdxBatchUpdateHelper;
begin
  Result := FBatchUpdateHelper;
end;

function TdxInnerRichEditDocumentServer.GetDocumentModel: TdxDocumentModel;
begin
  if FExistingDocumentModel <> nil then
    Result := FExistingDocumentModel
  else
    Result := CreateDocumentModelCore;
end;

function TdxInnerRichEditDocumentServer.GetDocumentModelTemplate: TdxDocumentModel;
begin
  if FDocumentModelTemplate = nil then
    CreateDocumentModelTemplate;
  Result := FDocumentModelTemplate;
end;

function TdxInnerRichEditDocumentServer.GetIsUpdateLocked: Boolean;
begin
  Result := FBatchUpdateHelper.IsUpdateLocked;
end;

function TdxInnerRichEditDocumentServer.GetDpiX: Single;
begin
  Result := DocumentModel.DpiX;
end;

function TdxInnerRichEditDocumentServer.GetDpiY: Single;
begin
  Result := DocumentModel.DpiY;
end;

function TdxInnerRichEditDocumentServer.GetLayoutUnit: TdxDocumentLayoutUnit;
begin
  Result := DocumentModel.LayoutUnit; 
end;

function TdxInnerRichEditDocumentServer.GetMeasurer: TdxBoxMeasurer;
begin
  if MeasurementAndDrawingStrategy <> nil then
    Result := MeasurementAndDrawingStrategy.Measurer
  else
    Result := nil;
end;

function TdxInnerRichEditDocumentServer.GetModified: Boolean;
begin
  Result := DocumentModel.InternalAPI.Modified;
end;

procedure TdxInnerRichEditDocumentServer.LoadDocument(AStream: TStream; ADocumentFormat: TdxDocumentFormat);
begin
  LoadDocumentCore(AStream, ADocumentFormat, EmptyStr);
end;

procedure TdxInnerRichEditDocumentServer.LoadDocument(const AFileName: string);
begin
  DocumentModel.LoadDocument(AFileName); 
end;

procedure TdxInnerRichEditDocumentServer.LoadDocument(const AFileName: string;
  ADocumentFormat: TdxDocumentFormat);
begin
  LoadDocument(AFileName, ADocumentFormat, False);
end;

procedure TdxInnerRichEditDocumentServer.SaveDocument(const AFileName: string);
begin
  SaveDocument(AFileName, TdxDocumentFormat.Rtf);
end;

procedure TdxInnerRichEditDocumentServer.SaveDocument(const AFileName: string;
  ADocumentFormat: TdxDocumentFormat);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(AFileName, fmCreate or fmOpenWrite);
  try
    SaveDocumentCore(AStream, ADocumentFormat, AFileName);
  finally
    AStream.Free;
  end;
end;

procedure TdxInnerRichEditDocumentServer.SaveDocumentCore(AStream: TStream;
  ADocumentFormat: TdxDocumentFormat; const ATargetUri: string = '');
begin
  DocumentModel.SaveDocument(AStream, ADocumentFormat, ATargetUri);
end;

procedure TdxInnerRichEditDocumentServer.LoadDocumentCore(AStream: TStream;
  ADocumentFormat: TdxDocumentFormat;
  const ASourceUri: string);
begin
end;

procedure TdxInnerRichEditDocumentServer.OnBeginDocumentUpdate(ASender: TObject; E: TdxEventArgs);
begin
  FDocumentDeferredChanges := TdxDocumentDeferredChanges.Create;
  BeginUpdate;
  OnBeginDocumentUpdateCore;
end;

procedure TdxInnerRichEditDocumentServer.OnBeginDocumentUpdateCore;
begin
end;

function TdxInnerRichEditDocumentServer.ProcessEndDocumentUpdateCore(ASender: TObject; E: TdxDocumentUpdateCompleteEventArgs): TdxDocumentModelChangeActions;
var
  AChangeActions: TdxDocumentModelChangeActions;
begin
  AChangeActions := E.DeferredChanges.ChangeActions;
  if TdxDocumentModelChangeAction.PerformActionsOnIdle in AChangeActions then
  begin
    if FDocumentDeferredChangesOnIdle = nil then
      FDocumentDeferredChangesOnIdle := TdxDocumentDeferredChanges.Create;
    FDocumentDeferredChangesOnIdle.ChangeActions := FDocumentDeferredChangesOnIdle.ChangeActions + AChangeActions;
    FDocumentDeferredChangesOnIdle.ChangeActions := FDocumentDeferredChangesOnIdle.ChangeActions -
      [TdxDocumentModelChangeAction.PerformActionsOnIdle];
    FDocumentDeferredChangesOnIdle.StartRunIndex := Min(E.DeferredChanges.ChangeStart.RunIndex,
      FDocumentDeferredChangesOnIdle.StartRunIndex);
    FDocumentDeferredChangesOnIdle.EndRunIndex := Max(E.DeferredChanges.ChangeEnd.RunIndex,
      FDocumentDeferredChangesOnIdle.EndRunIndex);
    AChangeActions := [];
  end;
  if (TdxDocumentModelChangeAction.RaiseEmptyDocumentCreated in AChangeActions) or
    (TdxDocumentModelChangeAction.RaiseDocumentLoaded in AChangeActions) then
    ApplyFontAndForeColor;
  Result := AChangeActions;
end;

procedure TdxInnerRichEditDocumentServer.OnBeginUpdate;
begin
end;

procedure TdxInnerRichEditDocumentServer.OnCancelUpdate;
begin
end;

procedure TdxInnerRichEditDocumentServer.OnEndDocumentUpdate(ASender: TObject; E: TdxDocumentUpdateCompleteEventArgs);
begin
  OnEndDocumentUpdateCore(ASender, E);
  EndUpdate;
  FreeAndNil(FDocumentDeferredChanges);
end;

function TdxInnerRichEditDocumentServer.OnEndDocumentUpdateCore(ASender: TObject;
  E: TdxDocumentUpdateCompleteEventArgs): TdxDocumentModelChangeActions;
begin
  NotImplemented;
end;

procedure TdxInnerRichEditDocumentServer.OnModifiedChanged(ASender: TObject; E: TdxEventArgs);
begin
end;

procedure TdxInnerRichEditDocumentServer.ApplyChangesCore(AChangeActions: TdxDocumentModelChangeActions);
begin
  if DocumentModel.IsUpdateLocked then
    DocumentDeferredChanges.ChangeActions := DocumentDeferredChanges.ChangeActions + AChangeActions 
  else
    RaiseDeferredEvents(AChangeActions);
end;

procedure TdxInnerRichEditDocumentServer.ApplyFontAndForeColor;
begin
end;

procedure TdxInnerRichEditDocumentServer.OnEndUpdate;
begin
end;

procedure TdxInnerRichEditDocumentServer.OnFirstBeginUpdate;
begin
  OnFirstBeginUpdateCore;
end;

procedure TdxInnerRichEditDocumentServer.OnFirstBeginUpdateCore;
begin
end;

procedure TdxInnerRichEditDocumentServer.OnLastCancelUpdate;
begin
  OnLastEndUpdateCore;
end;

procedure TdxInnerRichEditDocumentServer.OnLastEndUpdate;
begin
  OnLastEndUpdateCore;
end;

procedure TdxInnerRichEditDocumentServer.OnLastEndUpdateCore;
begin
end;

procedure TdxInnerRichEditDocumentServer.RaiseActiveViewChanged;
begin
end;

procedure TdxInnerRichEditDocumentServer.RaiseBeforeExport(
  const Args: TdxBeforeExportEventArgs);
begin
  FOnBeforeExport.Invoke(TObject(Owner), Args);
end;

procedure TdxInnerRichEditDocumentServer.RaiseDeferredEvents(AChangeActions: TdxDocumentModelChangeActions);
begin
  Owner.RaiseDeferredEvents(AChangeActions);
end;

procedure TdxInnerRichEditDocumentServer.RaiseSelectionChanged;
begin

  NotImplemented;
end;

procedure TdxInnerRichEditDocumentServer.SetDocumentModelLayoutUnit(AUnit: TdxDocumentLayoutUnit);
begin
  DocumentModel.BeginUpdate;
  try
    SetDocumentModelLayoutUnitCore(AUnit);
  finally
    DocumentModel.EndUpdate;
  end;
end;

procedure TdxInnerRichEditDocumentServer.SetDocumentModelLayoutUnitCore(AUnit: TdxDocumentLayoutUnit);
begin
  DocumentModel.LayoutUnit := AUnit;
  RecreateMeasurementAndDrawingStrategy;
  MeasurementAndDrawingStrategy.OnLayoutUnitChanged;
end;

procedure TdxInnerRichEditDocumentServer.OnApplicationIdle;
begin
  if FDocumentDeferredChangesOnIdle <> nil then
  begin
    DocumentModel.BeginUpdate;
    try
      DocumentModel.MainPieceTable.ApplyChangesCore(FDocumentDeferredChangesOnIdle.ChangeActions, FDocumentDeferredChangesOnIdle.StartRunIndex, FDocumentDeferredChangesOnIdle.EndRunIndex);
    finally
      DocumentModel.EndUpdate;
    end;
    FreeAndNil(FDocumentDeferredChangesOnIdle);
  end;
end;

procedure TdxInnerRichEditDocumentServer.OnUpdateUI;
begin
end;

procedure TdxInnerRichEditDocumentServer.SetLayoutUnit(AUnit: TdxDocumentLayoutUnit);
begin
  BeginUpdate;
  try
    SetLayoutUnitCore(AUnit);
  finally
    EndUpdate;
  end;
end;

procedure TdxInnerRichEditDocumentServer.SetLayoutUnitCore(AUnit: TdxDocumentLayoutUnit);
begin
  SetDocumentModelLayoutUnit(AUnit);
end;

procedure TdxInnerRichEditDocumentServer.SetLayoutUnit_(const Value: TdxDocumentLayoutUnit);
begin
  if Value = LayoutUnit then
    Exit;
  SetLayoutUnit(Value);
end;

procedure TdxInnerRichEditDocumentServer.SetModified(const Value: Boolean);
begin
  DocumentModel.InternalAPI.Modified := Value;
end;

procedure TdxInnerRichEditDocumentServer.SubscribeDocumentModelEvents;
begin
  DocumentModel.BeginDocumentUpdate.Add(OnBeginDocumentUpdate);
  DocumentModel.EndDocumentUpdate.Add(OnEndDocumentUpdate);
  DocumentModel.ModifiedChanged.Add(OnModifiedChanged);
end;

procedure TdxInnerRichEditDocumentServer.UnsubscribeDocumentModelEvents;
begin
  DocumentModel.BeginDocumentUpdate.Remove(OnBeginDocumentUpdate);
  DocumentModel.EndDocumentUpdate.Remove(OnEndDocumentUpdate);

  DocumentModel.ModifiedChanged.Remove(OnModifiedChanged);

end;

procedure TdxInnerRichEditDocumentServer.OnSelectionChanged(Sender: TObject; E: TdxEventArgs);
begin
  RaiseSelectionChanged;
  OnUpdateUI;
end;

procedure TdxInnerRichEditDocumentServer.OnInnerSelectionChanged(Sender: TObject; E: TdxEventArgs);
begin
end;

procedure TdxInnerRichEditDocumentServer.LoadDocument(const AFileName: string; ADocumentFormat: TdxDocumentFormat;
  ALoadAsTemplate: Boolean);
begin
end;

{ TdxInnerRichEditControl }

destructor TdxInnerRichEditControl.Destroy;
begin
  FFormatter.Free;
  FFormatter := nil;
  DisposeViews;

  FreeAndNil(FBackgroundThreadUIUpdater);



  FHorizontalScrollbar := nil;
  FVerticalScrollbar := nil;
  inherited Destroy;
end;

procedure TdxInnerRichEditControl.ActivateView(AView: TdxRichEditView; const AViewBounds: TRect);
begin
  ActiveView.CorrectZoomFactor;
  ActivateViewPlatformSpecific(AView);
  Formatter := CreateBackgroundFormatter(AView.FormattingController);
  AView.Activate(AViewBounds);
  Formatter.Start;
  SubscribeActiveViewEvents;
  if IsHandleCreated then
  begin
    ActiveView.EnsureCaretVisible;
    OnResizeCore;
  end;
end;

procedure TdxInnerRichEditControl.UpdateVerticalScrollBar(AAvoidJump: Boolean);
begin
  if AAvoidJump then
    ActiveView.VerticalScrollController.ScrollBarAdapter.SynchronizeScrollBarAvoidJump
  else
    ActiveView.VerticalScrollController.ScrollBarAdapter.EnsureSynchronized;
end;

procedure TdxInnerRichEditControl.BeginDocumentRendering;
begin
  if ActiveView <> nil then
    ActiveView.BeginDocumentRendering;
end;

function TdxInnerRichEditControl.CalculateActualViewBounds(const APreviousViewBounds: TRect): TRect;
begin
  Result := Owner.CalculateActualViewBounds(APreviousViewBounds);
end;

procedure TdxInnerRichEditControl.OnUpdateUI;
begin
  if FUpdateUIOnIdle then
    FForceUpdateUIOnIdle := True
  else
    OnUpdateUICore;
end;

procedure TdxInnerRichEditControl.OnUpdateUICore;
begin
  if IsUpdateLocked then
    FDeferredChanges.RaiseUpdateUI := True
  else
    RaiseUpdateUI;
end;

procedure TdxInnerRichEditControl.OnApplicationIdle;
begin
  inherited OnApplicationIdle;
  if FForceUpdateUIOnIdle then
  begin
    OnUpdateUICore;
    FForceUpdateUIOnIdle := False;
  end;
end;

procedure TdxInnerRichEditControl.ActivateViewPlatformSpecific(AView: TdxRichEditView);
begin
  Owner.ActivateViewPlatformSpecific(AView);
end;

procedure TdxInnerRichEditControl.ApplyChangesCore(AChangeActions: TdxDocumentModelChangeActions);
begin
  if not DocumentModel.IsUpdateLocked then
    ApplyChangesCorePlatformSpecific(AChangeActions);
  inherited ApplyChangesCore(AChangeActions);
end;

procedure TdxInnerRichEditControl.OnReadOnlyChanged;
begin
  RaiseReadOnlyChanged;
  OnUpdateUI;
end;

procedure TdxInnerRichEditControl.ApplyChangesCorePlatformSpecific(AChangeActions: TdxDocumentModelChangeActions);
begin
  Owner.ApplyChangesCorePlatformSpecific(AChangeActions);
end;

procedure TdxInnerRichEditControl.OnZoomFactorChangingPlatformSpecific;
begin
  Owner.OnZoomFactorChangingPlatformSpecific;
end;

procedure TdxInnerRichEditControl.ApplyFontAndForeColor;
var
  ACharacterProperties: TdxCharacterProperties; 
begin
  inherited ApplyFontAndForeColor;
  if not IsHandleCreated then
    Exit;
  ACharacterProperties := GetDefaultCharacterProperties;
  if ACharacterProperties = nil then
    Exit;
  DocumentModel.History.DisableHistory;
  try
    ApplyFontAndForeColorCore(ACharacterProperties);
  finally
    DocumentModel.History.EnableHistory;
  end;
end;

procedure TdxInnerRichEditControl.ApplyFontAndForeColorCore(ACharacterProperties: TdxCharacterProperties);
begin
  ACharacterProperties.BeginUpdate;
  try
  finally
    ACharacterProperties.EndUpdate;
  end;
end;

function TdxInnerRichEditControl.GetDefaultCharacterProperties: TdxCharacterProperties;
begin
  if DocumentModel = nil then
    Exit(nil);
  if DocumentModel.CharacterStyles.Count <= 0 then
    Exit(nil);
  Result := (DocumentModel.CharacterStyles.DefaultItem as TdxCharacterStyle).CharacterProperties; 
end;

function TdxInnerRichEditControl.CreateBackgroundFormatter(
  AController: TdxDocumentFormattingController): TdxBackgroundFormatter;
begin
  Result := TdxBackgroundFormatter.Create(AController);
end;

function TdxInnerRichEditControl.CreateCopySelectionManager: TdxCopySelectionManager;
begin
  Result := TdxCopySelectionManager.Create(Self);
end;

function TdxInnerRichEditControl.CreateMouseCursorCalculator: TdxMouseCursorCalculator;
begin
  Result := TdxMouseCursorCalculator.Create(ActiveView);
end;

procedure TdxInnerRichEditControl.CreateNewMeasurementAndDrawingStrategy;
begin
  inherited CreateNewMeasurementAndDrawingStrategy;
  if Formatter <> nil then
    Formatter.OnNewMeasurementAndDrawingStrategyChanged;
end;

function TdxInnerRichEditControl.CreateOptions: TdxRichEditControlOptionsBase;
begin
  Result := TdxRichEditDocumentServerOptions.Create(Self);
end;

procedure TdxInnerRichEditControl.CreateViews;
begin
  FViews := CreateViewRepository;
  SetActiveViewCore(FViews.Views[0]);

end;

procedure TdxInnerRichEditControl.DisposeViews;
begin
  FreeAndNil(FViews);
  FActiveView := nil;
end;

procedure TdxInnerRichEditControl.OnOvertypeChanged;
begin
  OnUpdateUI;
end;

function TdxInnerRichEditControl.DeactivateView(AView: TdxRichEditView): TRect;
begin
  if not DocumentModel.ActivePieceTable.IsMain then
  begin
    DocumentModel.SetActivePieceTableCore(DocumentModel.MainPieceTable, nil);
    AView.OnActivePieceTableChanged;
  end;
  UnsubscribeActiveViewEvents;
  DeactivateViewPlatformSpecific(AView);
  AView.Deactivate;
  FreeAndNil(FFormatter); 
  Result := AView.Bounds;
end;

procedure TdxInnerRichEditControl.DeactivateViewAndClearActiveView(AView: TdxRichEditView);
begin
  DeactivateView(AView);
  FActiveView := nil;
end;

procedure TdxInnerRichEditControl.DeactivateViewPlatformSpecific(AView: TdxRichEditView);
begin
  NotImplemented;
end;

procedure TdxInnerRichEditControl.OnResizeCore;
begin
  Owner.OnResizeCore;
end;

procedure TdxInnerRichEditControl.EndDocumentRendering;
begin
  if ActiveView <> nil then
    ActiveView.EndDocumentRendering;
end;

procedure TdxInnerRichEditControl.BeginInitialize;
begin
  inherited BeginInitialize;
  FBackgroundThreadUIUpdater := TdxDeferredBackgroundThreadUIUpdater.Create;
end;

procedure TdxInnerRichEditControl.EndInitialize;
begin
  if Formatter <> nil then
    Formatter.SuspendWorkerThread;
  inherited EndInitialize();
  if Formatter <> nil then
    Formatter.ResumeWorkerThread;
  FVerticalScrollbar := Owner.CreateVerticalScrollBar;
  FHorizontalScrollbar := Owner.CreateHorizontalScrollBar;

  CreateViews;
end;

function TdxInnerRichEditControl.GetActiveView: TdxRichEditView;
begin
  Result := ActiveView;
end;

function TdxInnerRichEditControl.GetFormatter: TdxBackgroundFormatter;
begin
  Result := Formatter;
end;

function TdxInnerRichEditControl.InternalGetDocumentModel: TdxDocumentModel;
begin
  Result := inherited DocumentModel;
end;

function TdxInnerRichEditControl.InternalGetOptions: TdxRichEditDocumentServerOptions;
begin
  Result := TdxRichEditDocumentServerOptions(inherited Options);
end;

function TdxInnerRichEditControl.GetIsHandleCreated: Boolean;
begin
  Result := TCustomControl(Owner).HandleAllocated;
end;

function TdxInnerRichEditControl.GetOptions: TdxRichEditControlOptionsBase;
begin
  Result := inherited Options;
end;

function TdxInnerRichEditControl.GetOwner: IdxInnerRichEditControlOwner;
begin
  if not Supports(inherited Owner, IdxInnerRichEditControlOwner, Result) then
    Result := nil;
end;

function TdxInnerRichEditControl.GetPredefinedFontSizeCollection: TdxPredefinedFontSizeCollection;
begin
  Result := PredefinedFontSizeCollection;
end;

function TdxInnerRichEditControl.IsEditable: Boolean;
begin
  Result := True; 
end;

function TdxInnerRichEditControl.SaveDocumentAs: Boolean;
begin
  Result := Boolean(NotImplemented);
end;

function TdxInnerRichEditControl.SaveDocument: Boolean;
begin
  Result := Boolean(NotImplemented);
end;

function TdxInnerRichEditControl.IsHyperlinkModifierKeysPress: Boolean;
begin
  Result := Boolean(NotImplemented);
end;

procedure TdxInnerRichEditControl.OnBeginDocumentUpdateCore;
begin
  inherited OnBeginDocumentUpdateCore;
  if ActiveView <> nil then
    ActiveView.OnBeginDocumentUpdate;
end;

procedure TdxInnerRichEditControl.OnBeforeExport(Sender: TObject; E: TdxBeforeExportEventArgs);
begin
  RaiseBeforeExport(E);
end;

procedure TdxInnerRichEditControl.OnCalculateDocumentVariable(Sender: TObject;
  E: TdxCalculateDocumentVariableEventArgs);
begin
  NotImplemented;
end;

procedure TdxInnerRichEditControl.BeginScrollbarUpdate(const AScrollbar: IdxOfficeScrollbar);
begin
  if AScrollbar <> nil then
    AScrollbar.BeginUpdate;
end;

procedure TdxInnerRichEditControl.EndScrollbarUpdate(const AScrollbar: IdxOfficeScrollbar);
begin
  if AScrollbar <> nil then
    AScrollbar.EndUpdate;
end;

function TdxInnerRichEditControl.GetHorizontalScrollBar: IdxOfficeScrollbar;
begin
  Result := FHorizontalScrollBar;
end;

function TdxInnerRichEditControl.GetVerticalScrollBar: IdxOfficeScrollbar;
begin
  Result := FVerticalScrollBar;
end;

function TdxInnerRichEditControl.OnEndDocumentUpdateCore(ASender: TObject;
  E: TdxDocumentUpdateCompleteEventArgs): TdxDocumentModelChangeActions;
var
  AChangeActions: TdxDocumentModelChangeActions;
begin
  AChangeActions := ProcessEndDocumentUpdateCore(ASender, E);
  if ActiveView <> nil then
  begin
    ActiveView.NotifyDocumentChanged(DocumentModel.MainPieceTable, E.DeferredChanges, False);
    if not DocumentModel.IsUpdateLocked then
    begin
      if TdxDocumentModelChangeAction.ResetSelectionLayout in AChangeActions then
        ActiveView.SelectionLayout.Invalidate;
      if TdxDocumentModelChangeAction.ScrollToBeginOfDocument in AChangeActions then
        ActiveView.PageViewInfoGenerator.ResetAnchors;
      if DocumentModel.DeferredChanges.EnsureCaretVisible then
        ActiveView.EnsureCaretVisible;
    end;
    ActiveView.OnEndDocumentUpdate;
    ApplyChangesCore(AChangeActions);
  end;
  Result := AChangeActions;
end;

procedure TdxInnerRichEditControl.OnFirstBeginUpdateCore;
begin
  inherited OnFirstBeginUpdateCore;
  FDeferredChanges := TdxRichEditControlDeferredChanges.Create;
  Owner.HideCaret;
  BeginScrollbarUpdate(HorizontalScrollBar);
  BeginScrollbarUpdate(VerticalScrollBar);
end;

procedure TdxInnerRichEditControl.OnLastEndUpdateCore;
begin
  inherited OnLastEndUpdateCore;
  EndScrollbarUpdate(VerticalScrollBar);
  EndScrollbarUpdate(HorizontalScrollBar);
  if FDeferredChanges.Resize then
    OnResizeCore;
  if FDeferredChanges.Redraw then
  begin
    if FDeferredChanges.RedrawAction = TdxRefreshAction.Selection then
      Owner.Redraw(TdxRefreshAction.Selection)
    else
      Owner.RedrawAfterEndUpdate;
  end;
  Owner.ShowCaret;
  if FDeferredChanges.RaiseUpdateUI then
    RaiseUpdateUI;
  FreeAndNil(FDeferredChanges);
end;

procedure TdxInnerRichEditControl.RedrawEnsureSecondaryFormattingComplete;
begin
  Owner.RedrawEnsureSecondaryFormattingComplete;
end;

procedure TdxInnerRichEditControl.RedrawEnsureSecondaryFormattingComplete(AAction: TdxRefreshAction);
begin
  Owner.RedrawEnsureSecondaryFormattingComplete(AAction);
end;

procedure TdxInnerRichEditControl.OnResize;
begin
  Owner.OnResize;
end;

function TdxInnerRichEditControl.CreateViewRepository: TdxRichEditCustomViewRepository;
begin
  Result := Owner.CreateViewRepository;
end;

procedure TdxInnerRichEditControl.SetActiveView(ANewView: TdxRichEditView);
begin
  BeginUpdate;
  try
    SetActiveViewCore(ANewView);
  finally
    EndUpdate;
  end;
end;

procedure TdxInnerRichEditControl.SetActiveViewCore(ANewView: TdxRichEditView);
var
  AViewBounds: TRect;
begin
  Assert(ANewView <> nil);
  if FActiveView <> nil then
    AViewBounds := DeactivateView(FActiveView)
  else
    AViewBounds := TRect.CreateSize(0, 0, 10, 10);
  FActiveView := ANewView;
  AViewBounds := CalculateActualViewBounds(AViewBounds);
  AViewBounds := cxRectSetTop(AViewBounds, 0); 
  AViewBounds := cxRectSetLeft(AViewBounds, 0); 
  ActivateView(FActiveView, AViewBounds);
  RaiseActiveViewChanged;
  RedrawEnsureSecondaryFormattingComplete;
  ActiveView.CorrectZoomFactor;
end;

procedure TdxInnerRichEditControl.SetLayoutUnitCore(AUnit: TdxDocumentLayoutUnit);
begin
  inherited SetLayoutUnitCore(AUnit);
  if ActiveView = nil then
    Exit;
  Owner.ResizeView(True);
  UpdateVerticalScrollBar(False);
  ActiveView.UpdateHorizontalScrollbar;
  Owner.Redraw;
end;

procedure TdxInnerRichEditControl.SetOvertype(const Value: Boolean);
begin
  if FOvertype <> Value then
  begin
    FOvertype := Value;
    OnOvertypeChanged;
  end;
end;

procedure TdxInnerRichEditControl.SetReadOnly(const Value: Boolean);
begin
  if FReadOnly <> Value then
  begin
    FReadOnly := Value;
    OnReadOnlyChanged;
  end;
end;

procedure TdxInnerRichEditControl.SetDocumentModelLayoutUnitCore(AUnit: TdxDocumentLayoutUnit);
begin
  if ActiveView <> nil then
    ActiveView.OnLayoutUnitChanging;
  inherited SetDocumentModelLayoutUnitCore(AUnit);
  if ActiveView <> nil then
    ActiveView.OnLayoutUnitChanged;
end;

procedure TdxInnerRichEditControl.RaiseReadOnlyChanged;
begin
  if not FOnReadOnlyChanged.Empty then
    FOnReadOnlyChanged.Invoke(TObject(Owner), nil);
end;

procedure TdxInnerRichEditControl.RaiseZoomChanged;
begin
  if not FOnZoomChanged.Empty then
    FOnZoomChanged.Invoke(TObject(Owner), nil);
end;


procedure TdxInnerRichEditControl.SubscribeActiveViewEvents;
begin
  ActiveView.ZoomChanging.Add(OnActiveViewZoomChanging);
  ActiveView.ZoomChanged.Add(OnActiveViewZoomChanged);
  ActiveView.BackColorChanged.Add(OnActiveViewBackColorChanged);
end;

procedure TdxInnerRichEditControl.UnsubscribeActiveViewEvents;
begin
  ActiveView.ZoomChanging.Remove(OnActiveViewZoomChanging);
  ActiveView.ZoomChanged.Remove(OnActiveViewZoomChanged);
  ActiveView.BackColorChanged.Remove(OnActiveViewBackColorChanged);
end;

procedure TdxInnerRichEditControl.OnActiveViewZoomChanging(Sender: TObject; E: TdxEventArgs);
begin
  BeginUpdate;
  OnZoomFactorChangingPlatformSpecific;
end;

procedure TdxInnerRichEditControl.OnActiveViewZoomChanged(Sender: TObject; E: TdxEventArgs);
begin
  OnResizeCore;
  UpdateVerticalScrollBar(False);
  EndUpdate;
  RaiseZoomChanged;
end;

procedure TdxInnerRichEditControl.OnActiveViewBackColorChanged(Sender: TObject; E: TdxEventArgs);
begin
  Owner.OnActiveViewBackColorChanged;
end;

{ TdxRichEditScrollbarOptions }

constructor TdxRichEditScrollbarOptions.Create(
  AOptions: TdxRichEditDocumentServerOptions);
begin
  inherited Create(AOptions);
  FOptions := AOptions;
  FVisibility := TdxRichEditScrollbarVisibility.Auto;
end;

procedure TdxRichEditScrollbarOptions.Assign(Source: TPersistent);
begin
  if Source is TdxRichEditScrollbarOptions then
    FVisibility := TdxRichEditScrollbarOptions(Source).Visibility;
end;

procedure TdxRichEditScrollbarOptions.SetVisibility(const Value: TdxRichEditScrollbarVisibility);
begin
  if FVisibility <> Value then
  begin
    FVisibility := Value;
    FOptions.Changed;
  end;
end;

{ TdxRichEditDocumentServerOptions }

constructor TdxRichEditDocumentServerOptions.Create(
  ADocumentServer: IdxInnerControl);
begin
  inherited Create(ADocumentServer);
  FVerticalScrollbar := TdxVerticalScrollbarOptions.Create(Self);
  FHorizontalScrollbar:= TdxHorizontalScrollbarOptions.Create(Self);
end;

destructor TdxRichEditDocumentServerOptions.Destroy;
begin
  FreeAndNil(FHorizontalScrollbar);
  FreeAndNil(FVerticalScrollbar);
  inherited Destroy;
end;

procedure TdxRichEditDocumentServerOptions.SetHorizontalScrollbar(const Value: TdxHorizontalScrollbarOptions);
begin
  FHorizontalScrollbar.Assign(Value);
end;

procedure TdxRichEditDocumentServerOptions.SetVerticalScrollbar(const Value: TdxVerticalScrollbarOptions);
begin
  FVerticalScrollbar.Assign(Value);
end;

end.
