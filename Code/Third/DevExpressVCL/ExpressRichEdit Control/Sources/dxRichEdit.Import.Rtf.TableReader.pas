{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Import.Rtf.TableReader;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  SysUtils, dxCoreClasses, Generics.Collections, dxRichEdit.DocumentModel.Core, dxRichEdit.DocumentModel.PieceTable,
  dxRichEdit.DocumentModel.History.IndexChangedHistoryItem,
  dxRichEdit.DocumentModel.TableFormatting, dxRichEdit.DocumentModel.TableStyles,
  dxRichEdit.DocumentModel.Borders;

type
  TdxRtfTableReader = class;
  TdxRtfTableController = class;
  TdxRtfTable = class;
  TdxRtfTableCell = class;
  TdxRtfTableRowController = class;
  TdxRtfTableProperties = class;
  TdxRtfTableRowProperties = class;
  TdxRtfTableCellPropertiesCollection = class;

  { TdxRtfTableReaderStateBase }

  TdxRtfTableReaderStateBase = class abstract
  strict private
    FReader: TdxRtfTableReader;
    function GetDocumentModel: TdxDocumentModel;
    function GetImporter: TObject;
    function GetTableController: TdxRtfTableController;
  public
    constructor Create(AReader: TdxRtfTableReader);

    procedure OnStartNestedTableProperties; virtual; abstract;
    procedure OnEndParagraph(AParagraphFormattingInfo: TObject); virtual; abstract; 
    procedure OnEndRow; virtual; abstract;
    procedure OnEndCell; virtual; abstract;
    procedure OnEndNestedRow; virtual; abstract;
    procedure OnEndNestedCell; virtual; abstract;
    procedure OnTableRowDefaults; virtual; abstract;

    property DocumentModel: TdxDocumentModel read GetDocumentModel;
    property Importer: TObject read GetImporter;
    property Reader: TdxRtfTableReader read FReader;
    property TableController: TdxRtfTableController read GetTableController;
  end;

  TdxNoTableRtfTableReaderState = class(TdxRtfTableReaderStateBase)
  const
    DefaultNestingLevel = 1;
  protected
  public
    procedure OnStartNestedTableProperties; override;
    procedure OnEndParagraph(AParagraphFormattingInfo: TObject); override; 
    procedure OnEndRow; override;
    procedure OnEndCell; override;
    procedure OnEndNestedRow; override;
    procedure OnEndNestedCell; override;
    procedure OnTableRowDefaults; override;
  end;

  { TdxRtfTableState }

  TdxRtfTableState = class
  strict private
    FTable: TdxRtfTable;
    FTableProperties: TdxRtfTableProperties;
    FRowProperties: TdxRtfTableRowProperties;
    FCellPropertiesCollection: TdxRtfTableCellPropertiesCollection;
  public
    constructor Create(ATable: TdxRtfTable; AReader: TdxRtfTableReader);
    property Table: TdxRtfTable read FTable;
    property TableProperties: TdxRtfTableProperties read FTableProperties;
    property RowProperties: TdxRtfTableRowProperties read FRowProperties;
    property CellPropertiesCollection: TdxRtfTableCellPropertiesCollection read FCellPropertiesCollection;
  end;

  { TdxRtfTableCellController }

  TdxRtfTableCellController = class
  strict private
    FRowController: TdxRtfTableRowController;
    FCurrentCell: TdxRtfTableCell;
  public
    constructor Create(ARowController: TdxRtfTableRowController);

    property CurrentCell: TdxRtfTableCell read FCurrentCell;
  end;

  { TdxRtfTableRow }

  TdxRtfTableRow = class
  end;
  { TdxRtfTableRowController }

  TdxRtfTableRowController = class
  strict private
    FTableController: TdxRtfTableController;
    FCurrentRow: TdxRtfTableRow;
    FCellController: TdxRtfTableCellController;
  protected
    function CreateCellController: TdxRtfTableCellController;

    property TableController: TdxRtfTableController read FTableController;
  public
    constructor Create(ATableController: TdxRtfTableController);
    destructor Destroy; override;

    property CurrentRow: TdxRtfTableRow read FCurrentRow;
    property CellController: TdxRtfTableCellController read FCellController;
  end;

  { TdxRtfTableController }

  TdxRtfTableController = class
  strict private
    FReader: TdxRtfTableReader;
    FCurrentTable: TdxRtfTable;
    FRowController: TdxRtfTableRowController;
  protected
    function CreateRowController: TdxRtfTableRowController; virtual;
  public
    constructor Create(AReader: TdxRtfTableReader);
    destructor Destroy; override;

    property Reader: TdxRtfTableReader read FReader;
    property CurrentTable: TdxRtfTable read FCurrentTable;
    property RowController: TdxRtfTableRowController read FRowController;
  end;

  { TdxRtfTable }

  TdxRtfTable = class
  end;

  { TdxRtfTableCollection }

  TdxRtfTableCollection = class(TList<TdxRtfTable>)
  end;

  { TdxRtfTableProperties }

  TdxRtfTableProperties = class(TdxTableProperties)
  end;

  { TdxRtfCellSpacing }

  TdxRtfCellSpacing = class
  strict private
    FCellSpacing: TdxWidthUnit;
  public
    constructor Create(ACellSpacing: TdxWidthUnit);

    property Left: TdxWidthUnit read FCellSpacing;
    property Top: TdxWidthUnit read FCellSpacing;
    property Right: TdxWidthUnit read FCellSpacing;
    property Bottom: TdxWidthUnit read FCellSpacing;
  end;

  { TdxRtfTableRowProperties }

  TdxRtfTableRowProperties = class(TdxTableRowProperties)
  strict private
    FLeft: Integer;
    FFloatingPosition: TdxTableFloatingPositionInfo;
    FCellSpacing: TdxRtfCellSpacing;
  public
    constructor Create(APieceTable: TdxPieceTable); reintroduce;
    destructor Destroy; override;

    property Left: Integer read FLeft write FLeft;
    property FloatingPosition: TdxTableFloatingPositionInfo read FFloatingPosition;
    property CellSpacing: TdxRtfCellSpacing read FCellSpacing;
  end;

  { TdxRtfTableCellProperties }

  TdxRtfTableCellProperties = class(TdxTableCellProperties)
  end;

  { TdxRtfTableCellPropertiesCollection }

  TdxRtfTableCellPropertiesCollection = class(TList<TdxRtfTableCellProperties>)
  end;

  { TdxRtfTableCell }

  TdxRtfTableCell = class(TcxIUnknownObject, IdxCellPropertiesOwner)
  protected
    function CreateCellPropertiesChangedHistoryItem(AProperties: TdxTableCellProperties): TdxIndexChangedHistoryItemCore<TdxDocumentModelChangeActions>;
  end;

  TdxRtfParentCellMap = TDictionary<TdxRtfTableCell, TdxRtfTableCollection>;

  { TdxRtfTableReader }

  TdxRtfTableReader = class(TcxIUnknownObject, IdxCellPropertiesOwner)
  strict private
    FState: TdxRtfTableReaderStateBase;
    FImporter: TObject;
    FTableStack: TStack<TdxRtfTableState> ;
    FTableController: TdxRtfTableController;
    FTables: TdxRtfTableCollection;
    FTableProperties: TdxRtfTableProperties;
    FRowProperties: TdxRtfTableRowProperties;
    FCellPropertiesCollection: TdxRtfTableCellPropertiesCollection;
    FCellProperties: TdxRtfTableCellProperties;
    FIsNestedTableProperetiesReading: Boolean;
    FParentCellMap: TdxRtfParentCellMap;
    FProcessedBorder: TdxBorderBase;
  private
    function GetDocumentModel: TdxDocumentModel;
    function GetCellProperties: TdxRtfTableCellProperties;
    function GetRowProperties: TdxRtfTableRowProperties;
    function GetTableProperties: TdxRtfTableProperties;
  protected
    function CreateTableController: TdxRtfTableController; virtual;
    procedure ResetProperties;
    procedure CreateCellProperties; virtual;
    function CreateCellPropertiesChangedHistoryItem(AProperties: TdxTableCellProperties): TdxIndexChangedHistoryItemCore<TdxDocumentModelChangeActions>;

    property CellPropertiesCollection: TdxRtfTableCellPropertiesCollection read FCellPropertiesCollection;
    property DocumentModel: TdxDocumentModel read GetDocumentModel;
    property ParentCellMap: TdxRtfParentCellMap read FParentCellMap;
    property State: TdxRtfTableReaderStateBase read FState;
    property TableStack: TStack<TdxRtfTableState> read FTableStack;
  public
    constructor Create(AImporter: TObject);
    destructor Destroy; override;

    procedure OnEndCell;
    procedure OnEndNestedCell;
    procedure OnEndNestedRow;
    procedure OnEndParagraph;
    procedure OnEndRow;
    procedure OnTableRowDefaults;

    property TableProperties: TdxRtfTableProperties read GetTableProperties;
    property RowProperties: TdxRtfTableRowProperties read GetRowProperties;
    property CellProperties: TdxRtfTableCellProperties  read GetCellProperties;
    property ProcessedBorder: TdxBorderBase read FProcessedBorder write FProcessedBorder;
    property IsNestedTableProperetiesReading: Boolean read FIsNestedTableProperetiesReading;
    property Importer: TObject read FImporter;
    property TableController: TdxRtfTableController read FTableController;
    property Tables: TdxRtfTableCollection read FTables;
  end;

implementation

uses
  dxRichEdit.Import.Rtf;

type
  { TdxRtfTableReaderStateBaseHelper }

  TdxRtfTableReaderStateBaseHelper = class helper for TdxRtfTableReaderStateBase
  private
    function GetImporter: TdxRichEditDocumentModelRtfImporter;
  public
    property Importer: TdxRichEditDocumentModelRtfImporter read GetImporter;
  end;

  { TdxRtfTableReaderHelper }

  TdxRtfTableReaderHelper = class helper for TdxRtfTableReader
  private
    function GetImporter: TdxRichEditDocumentModelRtfImporter;
  public
    property Importer: TdxRichEditDocumentModelRtfImporter read GetImporter;
  end;

{ TdxRtfTableReaderStateBaseHelper }

function TdxRtfTableReaderStateBaseHelper.GetImporter: TdxRichEditDocumentModelRtfImporter;
begin
  Result := TdxRichEditDocumentModelRtfImporter(inherited Importer);
end;

{ TdxRtfTableReaderHelper }

function TdxRtfTableReaderHelper.GetImporter: TdxRichEditDocumentModelRtfImporter;
begin
  Result := TdxRichEditDocumentModelRtfImporter(inherited Importer);
end;

{ TdxRtfTableCell }

function TdxRtfTableCell.CreateCellPropertiesChangedHistoryItem(
  AProperties: TdxTableCellProperties): TdxIndexChangedHistoryItemCore<TdxDocumentModelChangeActions>;
begin
  Result := TdxIndexChangedHistoryItem<TdxDocumentModelChangeActions>.Create(AProperties.PieceTable as TdxPieceTable, AProperties);
end;

{ TdxRtfTableReaderStateBase }

constructor TdxRtfTableReaderStateBase.Create(AReader: TdxRtfTableReader);
begin
  inherited Create;
  FReader := AReader;
end;

function TdxRtfTableReaderStateBase.GetDocumentModel: TdxDocumentModel;
begin
  Result := Importer.DocumentModel;
end;

function TdxRtfTableReaderStateBase.GetImporter: TObject;
begin
  Result := Reader.Importer;
end;

function TdxRtfTableReaderStateBase.GetTableController: TdxRtfTableController;
begin
  Result := Reader.TableController;
end;

{ TdxRtfTableReader }

constructor TdxRtfTableReader.Create(AImporter: TObject);
begin
  inherited Create;
  Assert(AImporter is TdxRichEditDocumentModelRtfImporter);
  FImporter := AImporter;
  FState := TdxNoTableRtfTableReaderState.Create(Self);
  FTableStack := TStack<TdxRtfTableState>.Create;
  FTables := TdxRtfTableCollection.Create;
  FTableController := CreateTableController;
  FParentCellMap := TdxRtfParentCellMap.Create;
  ResetProperties;
end;

function TdxRtfTableReader.CreateTableController: TdxRtfTableController;
begin
  Result := TdxRtfTableController.Create(Self);
end;

destructor TdxRtfTableReader.Destroy;
begin
  FreeAndNil(FCellPropertiesCollection);
  FreeAndNil(FCellProperties);
  FreeAndNil(FRowProperties);
  FreeAndNil(FTableProperties);
  FreeAndNil(FParentCellMap);
  FreeAndNil(FTableController);
  FreeAndNil(FTables);
  FreeAndNil(FTableStack);
  FreeAndNil(FState);
  inherited Destroy;
end;

procedure TdxRtfTableReader.OnEndCell;
begin
  State.OnEndCell;
end;

procedure TdxRtfTableReader.OnEndNestedCell;
begin
  State.OnEndNestedCell;
end;

procedure TdxRtfTableReader.OnEndNestedRow;
begin
  FIsNestedTableProperetiesReading := False;
  State.OnEndNestedRow;
end;

procedure TdxRtfTableReader.OnEndParagraph;
begin
  State.OnEndParagraph(Importer.Position.ParagraphFormattingInfo);
end;

procedure TdxRtfTableReader.OnEndRow;
begin
  State.OnEndRow;
end;

procedure TdxRtfTableReader.OnTableRowDefaults;
begin
  State.OnTableRowDefaults;
  ResetProperties;
end;

procedure TdxRtfTableReader.ResetProperties;
begin
  FreeAndNil(FTableProperties);
  FreeAndNil(FRowProperties);
  FreeAndNil(FCellPropertiesCollection);
  FCellPropertiesCollection := TdxRtfTableCellPropertiesCollection.Create;
  FreeAndNil(FCellProperties);
  FreeAndNil(FProcessedBorder);
end;

procedure TdxRtfTableReader.CreateCellProperties;
begin
  FCellProperties := TdxRtfTableCellProperties.Create(Importer.PieceTable, Self);
end;

function TdxRtfTableReader.CreateCellPropertiesChangedHistoryItem(
  AProperties: TdxTableCellProperties): TdxIndexChangedHistoryItemCore<TdxDocumentModelChangeActions>;
begin
  Result := TdxIndexChangedHistoryItem<TdxDocumentModelChangeActions>.Create(AProperties.PieceTable as TdxPieceTable, AProperties);
end;

function TdxRtfTableReader.GetCellProperties: TdxRtfTableCellProperties;
begin
  if FCellProperties = nil then
    CreateCellProperties;
  Result := FCellProperties;
end;

function TdxRtfTableReader.GetDocumentModel: TdxDocumentModel;
begin
  Result := Importer.DocumentModel;
end;

function TdxRtfTableReader.GetRowProperties: TdxRtfTableRowProperties;
begin
  if FRowProperties = nil then
    FRowProperties := TdxRtfTableRowProperties.Create(Importer.PieceTable);
  Result := FRowProperties;
end;

function TdxRtfTableReader.GetTableProperties: TdxRtfTableProperties;
begin
  if FTableProperties = nil then
    FTableProperties := TdxRtfTableProperties.Create(Importer.PieceTable);
  Result := FTableProperties;
end;

{ TdxRtfTableController }

constructor TdxRtfTableController.Create(AReader: TdxRtfTableReader);
begin
  inherited Create;
  FReader := AReader;
  FRowController := CreateRowController;
end;

function TdxRtfTableController.CreateRowController: TdxRtfTableRowController;
begin
  Result := TdxRtfTableRowController.Create(Self);
end;

destructor TdxRtfTableController.Destroy;
begin
  FreeAndNil(FRowController);
  inherited;
end;

{ TdxRtfTableRowController }

constructor TdxRtfTableRowController.Create(
  ATableController: TdxRtfTableController);
begin
  inherited Create;
  FTableController := ATableController;
  FCellController := CreateCellController;
end;

destructor TdxRtfTableRowController.Destroy;
begin
  FreeAndNil(FCellController);
  inherited;
end;

function TdxRtfTableRowController.CreateCellController: TdxRtfTableCellController;
begin
  Result := TdxRtfTableCellController.Create(Self);
end;

{ TdxRtfTableCellController }

constructor TdxRtfTableCellController.Create(
  ARowController: TdxRtfTableRowController);
begin
  inherited Create;
  FRowController := ARowController;
end;

{ TdxNoTableRtfTableReaderState }

procedure TdxNoTableRtfTableReaderState.OnEndCell;
begin
  NotImplemented;
end;

procedure TdxNoTableRtfTableReaderState.OnEndNestedCell;
begin
  NotImplemented;
end;

procedure TdxNoTableRtfTableReaderState.OnEndNestedRow;
begin
  Importer.ThrowInvalidRtfFile;
end;

procedure TdxNoTableRtfTableReaderState.OnEndParagraph(
  AParagraphFormattingInfo: TObject);
var
  AInfo: TdxRtfParagraphFormattingInfo;
begin
  AInfo := AParagraphFormattingInfo as TdxRtfParagraphFormattingInfo;
  if not AInfo.InTableParagraph then
    Exit;
  NotImplemented;
end;

procedure TdxNoTableRtfTableReaderState.OnEndRow;
begin
  Importer.ThrowInvalidRtfFile;
end;

procedure TdxNoTableRtfTableReaderState.OnStartNestedTableProperties;
begin
  Importer.ThrowInvalidRtfFile;
end;

procedure TdxNoTableRtfTableReaderState.OnTableRowDefaults;
begin
//do nothing
end;

{ TdxRtfTableState }

constructor TdxRtfTableState.Create(ATable: TdxRtfTable;
  AReader: TdxRtfTableReader);
begin
  inherited Create;
  FTable := ATable;
  FTableProperties := AReader.TableProperties;
  FRowProperties := AReader.RowProperties;
  FCellPropertiesCollection := AReader.CellPropertiesCollection;
end;

{ TdxRtfTableRowProperties }

constructor TdxRtfTableRowProperties.Create(APieceTable: TdxPieceTable);
begin
  inherited Create(APieceTable);
  FFloatingPosition := APieceTable.DocumentModel.Cache.TableFloatingPositionInfoCache.DefaultItem.Clone;
  FFloatingPosition.HorizontalAnchor := TdxHorizontalAnchorTypes.Column;
  FCellSpacing := TdxRtfCellSpacing.Create(inherited CellSpacing);
end;

destructor TdxRtfTableRowProperties.Destroy;
begin
  FreeAndNil(FFloatingPosition);
  FreeAndNil(FCellSpacing);
  inherited Destroy;
end;

{ TdxRtfCellSpacing }

constructor TdxRtfCellSpacing.Create(ACellSpacing: TdxWidthUnit);
begin
  inherited Create;
  FCellSpacing := ACellSpacing;
end;

end.
