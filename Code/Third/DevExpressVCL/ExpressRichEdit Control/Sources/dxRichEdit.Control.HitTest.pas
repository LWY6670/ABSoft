{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Control.HitTest;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Types, Classes, SysUtils, Generics.Collections,
  dxCoreClasses, cxClasses, dxRichEdit.Utils.Types, dxRichEdit.DocumentModel.PieceTable, dxRichEdit.DocumentLayout.Position;

type
  TdxBoxHitTestManager = class;
  TdxBoxHitTestCalculator = class;
  TdxRichEditHitTestRequest = class;
  TdxRichEditHitTestResult = class;

  IdxBoxHitTestCalculator = interface(IdxBoxHitTestCustomCalculator)
  ['{FCB6731A-5045-419E-81C0-995E0D0D9E14}']
    function CreatePageHitTestManager(APage: TdxPage): TdxBoxHitTestManager;
    function CreatePageAreaHitTestManager(APageArea: TdxPageArea): TdxBoxHitTestManager;
    function CreateColumnHitTestManager(AColumn: TdxColumn): TdxBoxHitTestManager;
    function CreateRowHitTestManager(ARow: TdxRow): TdxBoxHitTestManager;
    function CreateTextBoxHitTestManager(ABox: TdxTextBox): TdxBoxHitTestManager;
    function CreateCharacterBoxHitTestManager(ABox: TdxCharacterBox): TdxBoxHitTestManager;
    function CreateHyphenBoxHitTestManager(ABox: TdxHyphenBox): TdxBoxHitTestManager;
    function CreateInlinePictureBoxHitTestManager(ABox: TdxInlinePictureBox): TdxBoxHitTestManager;
    function CreateCustomRunBoxHitTestManager(ABox: TdxCustomRunBox): TdxBoxHitTestManager;
    function CreateSeparatorMarkBoxHitTestManager(ABox: TdxSeparatorBox): TdxBoxHitTestManager;
    function CreateSpaceBoxHitTestManager(ABox: ISpaceBox): TdxBoxHitTestManager;
    function CreateTabSpaceBoxHitTestManager(ABox: TdxTabSpaceBox): TdxBoxHitTestManager;
    function CreateNumberingListBoxHitTestManager(ABox: TdxNumberingListBox): TdxBoxHitTestManager;
    function CreateLineBreakBoxHitTestManager(ABox: TdxLineBreakBox): TdxBoxHitTestManager;
    function CreatePageBreakBoxHitTestManager(ABox: TdxPageBreakBox): TdxBoxHitTestManager;
    function CreateColumnBreakBoxHitTestManager(ABox: TdxColumnBreakBox): TdxBoxHitTestManager;
    function CreateParagraphMarkBoxHitTestManager(ABox: TdxParagraphMarkBox): TdxBoxHitTestManager;
    function CreateSectionMarkBoxHitTestManager(ABox: TdxSectionMarkBox): TdxBoxHitTestManager;
    function CreateDataContainerRunBoxHitTestManager(ABox: TdxDataContainerRunBox): TdxBoxHitTestManager;
    procedure ProcessPageCollection(ACollection: TdxPageCollection);
    procedure ProcessPageAreaCollection(ACollection: TdxPageAreaCollection);
    procedure ProcessColumnCollection(ACollection: TdxColumnCollection);
    procedure ProcessRowCollection(ACollection: TdxRowCollection);
    procedure ProcessBoxCollection(ACollection: TdxBoxCollection);
    procedure ProcessCharacterBoxCollection(ACollection: TdxCharacterBoxCollection);
  end;

  TdxRichEditHitTestRequest = class(TcxIUnknownObject, IdxCloneable<TdxRichEditHitTestRequest>, IdxSupportsCopyFrom<TdxRichEditHitTestRequest>)
  private
    FPieceTable: TdxPieceTable;
    FPhysicalPoint: TPoint;
    FLogicalPoint: TPoint;
    FDetailsLevel: TdxDocumentLayoutDetailsLevel;
    FAccuracy: TdxHitTestAccuracy;
    FSearchAnyPieceTable: Boolean;
  public
    constructor Create(APieceTable: TdxPieceTable);

    //IdxCloneable
    function Clone: TdxRichEditHitTestRequest;
    //IdxSupportsCopyFrom
    procedure CopyFrom(const Source: TdxRichEditHitTestRequest);

    property PieceTable: TdxPieceTable read FPieceTable;
    property PhysicalPoint: TPoint read FPhysicalPoint write FPhysicalPoint;
    property LogicalPoint: TPoint read FLogicalPoint write FLogicalPoint;
    property DetailsLevel: TdxDocumentLayoutDetailsLevel read FDetailsLevel write FDetailsLevel;
    property Accuracy: TdxHitTestAccuracy read FAccuracy write FAccuracy;
    property SearchAnyPieceTable: Boolean read FSearchAnyPieceTable write FSearchAnyPieceTable;
  end;

  TdxRichEditHitTestResult = class(TdxDocumentLayoutPosition, IdxSupportsCopyFrom<TdxRichEditHitTestResult>)
  private
    FAccuracy: TdxHitTestAccuracy;
    FLogicalPoint: TPoint;
    FPhysicalPoint: TPoint;
    FFloatingObjectBox: TdxFloatingObjectBox;
  public
    constructor Create(ADocumentLayout: TdxDocumentLayout; APieceTable: TdxPieceTable);
    destructor Destroy; override;

    //IdxSupportsCopyFrom
    procedure CopyFrom(const Source: TdxRichEditHitTestResult);

    property Accuracy: TdxHitTestAccuracy read FAccuracy write FAccuracy;
    property LogicalPoint: TPoint read FLogicalPoint write FLogicalPoint;
    property PhysicalPoint: TPoint read FPhysicalPoint write FPhysicalPoint;
    property FloatingObjectBox: TdxFloatingObjectBox read FFloatingObjectBox write FFloatingObjectBox;
  end;

  TdxBoxHitTestCalculator = class(TdxBoxHitTestCustomCalculator, IdxBoxHitTestCalculator)
  private
    FHitTestRequest: TdxRichEditHitTestRequest;
    FHitTestResult: TdxRichEditHitTestResult;
  protected
    procedure CalcHitTestCore(AManager: TdxBoxHitTestManager; AForceStrictHitTest: Boolean);
    function FastHitTestCore(ACollection: TdxBoxCollectionBase;
      APredicate: TdxBoxComparable; AStrictHitTest: Boolean): Integer;
    //IdxBoxHitTestCalculator
    function CreatePageHitTestManager(APage: TdxPage): TdxBoxHitTestManager;
    function CreatePageAreaHitTestManager(APageArea: TdxPageArea): TdxBoxHitTestManager;
    function CreateColumnHitTestManager(AColumn: TdxColumn): TdxBoxHitTestManager;
    function CreateRowHitTestManager(ARow: TdxRow): TdxBoxHitTestManager;
    function CreateTextBoxHitTestManager(ABox: TdxTextBox): TdxBoxHitTestManager;
    function CreateCharacterBoxHitTestManager(ABox: TdxCharacterBox): TdxBoxHitTestManager;
    function CreateHyphenBoxHitTestManager(ABox: TdxHyphenBox): TdxBoxHitTestManager;
    function CreateInlinePictureBoxHitTestManager(ABox: TdxInlinePictureBox): TdxBoxHitTestManager;
    function CreateCustomRunBoxHitTestManager(ABox: TdxCustomRunBox): TdxBoxHitTestManager;
    function CreateSeparatorMarkBoxHitTestManager(ABox: TdxSeparatorBox): TdxBoxHitTestManager;
    function CreateSpaceBoxHitTestManager(ABox: ISpaceBox): TdxBoxHitTestManager;
    function CreateTabSpaceBoxHitTestManager(ABox: TdxTabSpaceBox): TdxBoxHitTestManager;
    function CreateNumberingListBoxHitTestManager(ABox: TdxNumberingListBox): TdxBoxHitTestManager;
    function CreateLineBreakBoxHitTestManager(ABox: TdxLineBreakBox): TdxBoxHitTestManager;
    function CreatePageBreakBoxHitTestManager(ABox: TdxPageBreakBox): TdxBoxHitTestManager;
    function CreateColumnBreakBoxHitTestManager(ABox: TdxColumnBreakBox): TdxBoxHitTestManager;
    function CreateParagraphMarkBoxHitTestManager(ABox: TdxParagraphMarkBox): TdxBoxHitTestManager;
    function CreateSectionMarkBoxHitTestManager(ABox: TdxSectionMarkBox): TdxBoxHitTestManager;
    function CreateDataContainerRunBoxHitTestManager(ABox: TdxDataContainerRunBox): TdxBoxHitTestManager;
    procedure ProcessPageCollection(ACollection: TdxPageCollection); virtual;
    procedure ProcessPageAreaCollection(ACollection: TdxPageAreaCollection); virtual;
    procedure ProcessColumnCollection(ACollection: TdxColumnCollection); virtual;
    procedure ProcessRowCollection(ACollection: TdxRowCollection); virtual;
    procedure ProcessBoxCollection(ACollection: TdxBoxCollection); virtual;
    procedure ProcessCharacterBoxCollection(ACollection: TdxCharacterBoxCollection); virtual;
  public
    constructor Create(ARequest: TdxRichEditHitTestRequest; AResult: TdxRichEditHitTestResult);

    procedure CalcHitTest(ABox: TdxBox; AForceStrictHitTest: Boolean = False);

    procedure FastHitTestCharacter(ACollection: TdxCharacterBoxCollection; AStrictHitTest: Boolean);
    function FastHitTestAssumingArrangedHorizontally(ACollection: TdxBoxCollectionBase; AStrictHitTest: Boolean): Integer;
    function FastHitTestAssumingArrangedVertically(ACollection: TdxBoxCollectionBase; AStrictHitTest: Boolean): Integer;

    function FastHitTestIndexCore(ACollection: TdxBoxCollectionBase;
      APredicate: TdxBoxComparable; AStrictHitTest: Boolean): Integer;

    property HitTestRequest: TdxRichEditHitTestRequest read FHitTestRequest;
    property HitTestResult: TdxRichEditHitTestResult read FHitTestResult;
  end;

  TdxBoxHitTestManager = class(TdxBoxHitTestCustomManager)
  private
    function GetCalculator: TdxBoxHitTestCalculator;
    function GetHitTestRequest: TdxRichEditHitTestRequest;
    function GetHitTestResult: TdxRichEditHitTestResult;
    function GetICalculator: IdxBoxHitTestCalculator;
  protected
    property ICalculator: IdxBoxHitTestCalculator read GetICalculator;
  public
    procedure CalcHitTestAmongNestedBoxes; virtual;
    procedure RegisterSuccessfullHitTest(AStrictMatch: Boolean); virtual;
    procedure RegisterFailedHitTest; virtual;

		property Calculator: TdxBoxHitTestCalculator read GetCalculator;
    property HitTestRequest: TdxRichEditHitTestRequest read GetHitTestRequest;
    property HitTestResult: TdxRichEditHitTestResult read GetHitTestResult;
  end;

  TdxPageHitTestManager  = class(TdxBoxHitTestManager)
  private
    function GetPage: TdxPage;
  public
    procedure CalcHitTestAmongNestedBoxes; override;
    procedure RegisterSuccessfullHitTest(AStrictMatch: Boolean); override;
    procedure RegisterFailedHitTest; override;
    property Page: TdxPage read GetPage;
  end;

  TdxPageAreaHitTestManager = class(TdxBoxHitTestManager)
  private
    function GetPageArea: TdxPageArea;
  public
    procedure CalcHitTestAmongNestedBoxes; override;
    procedure RegisterSuccessfullHitTest(AStrictMatch: Boolean); override;
    procedure RegisterFailedHitTest; override;
    property PageArea: TdxPageArea read GetPageArea;
  end;

  TdxColumnHitTestManager = class(TdxBoxHitTestManager)
  private
    function GetColumn: TdxColumn;
  protected
    function CalcHitTestAmongNestedBoxesCore(ATable: TdxTableViewInfo): Boolean;
    function GetActiveTableViewInfo: TdxTableViewInfo;
  public
    procedure CalcHitTestAmongNestedBoxes; override;
    procedure RegisterSuccessfullHitTest(AStrictMatch: Boolean); override;
    procedure RegisterFailedHitTest; override;
    property Column: TdxColumn read GetColumn;
  end;

  TdxRowHitTestManager = class(TdxBoxHitTestManager)
  private
    function GetRow: TdxRow;
  public
    procedure CalcHitTestAmongNestedBoxes; override;
    procedure RegisterSuccessfullHitTest(AStrictMatch: Boolean); override;
    procedure RegisterFailedHitTest; override;
    property Row: TdxRow read GetRow;
  end;

  TdxCharacterBoxHitTestManager = class(TdxBoxHitTestManager)
  private
    function GetCharacter: TdxCharacterBox;
  public
    procedure CalcHitTestAmongNestedBoxes; override;
    procedure RegisterSuccessfullHitTest(AStrictMatch: Boolean); override;
    procedure RegisterFailedHitTest; override;
    property Character: TdxCharacterBox read GetCharacter;
  end;

  TdxEmptyHitTestManager = class(TdxBoxHitTestManager)
  public
    procedure CalcHitTestAmongNestedBoxes; override;
    procedure RegisterSuccessfullHitTest(AStrictMatch: Boolean); override;
    procedure RegisterFailedHitTest; override;
  end;

implementation

uses
  dxTypeHelpers, dxRichEdit.DocumentModel.Core;

{ TdxRichEditHitTestRequest }

constructor TdxRichEditHitTestRequest.Create(APieceTable: TdxPieceTable);
begin
  inherited Create;
  FPieceTable := APieceTable;
end;

function TdxRichEditHitTestRequest.Clone: TdxRichEditHitTestRequest;
begin
  Result := TdxRichEditHitTestRequest.Create(PieceTable);
  Result.CopyFrom(Self);
end;

procedure TdxRichEditHitTestRequest.CopyFrom(const Source: TdxRichEditHitTestRequest);
begin
  PhysicalPoint := Source.PhysicalPoint;
  LogicalPoint := Source.LogicalPoint;
  DetailsLevel := Source.DetailsLevel;
  Accuracy := Source.Accuracy;
  SearchAnyPieceTable := Source.SearchAnyPieceTable;
end;

{ TdxRichEditHitTestResult }

constructor TdxRichEditHitTestResult.Create(ADocumentLayout: TdxDocumentLayout; APieceTable: TdxPieceTable);
begin
  inherited Create(ADocumentLayout, APieceTable, 0);
end;

destructor TdxRichEditHitTestResult.Destroy;
begin
  inherited Destroy;
end;

procedure TdxRichEditHitTestResult.CopyFrom(const Source: TdxRichEditHitTestResult);
begin
  inherited CopyFrom(Source);
  Accuracy := Source.Accuracy;
  LogicalPoint := Source.LogicalPoint;
  PhysicalPoint := Source.PhysicalPoint;
end;

{ TdxBoxHitTestCalculator }

constructor TdxBoxHitTestCalculator.Create(ARequest: TdxRichEditHitTestRequest; AResult: TdxRichEditHitTestResult);
begin
  inherited Create;
  FHitTestRequest := ARequest;
  FHitTestResult := AResult;
  FHitTestResult.LogicalPoint := FHitTestRequest.LogicalPoint;
end;

procedure TdxBoxHitTestCalculator.CalcHitTest(ABox: TdxBox; AForceStrictHitTest: Boolean = False);
var
  AManager: TdxBoxHitTestManager;
begin
  AManager := TdxBoxHitTestManager(ABox.CreateHitTestManager(Self));
  try
    CalcHitTestCore(AManager, AForceStrictHitTest);
  finally
    AManager.Free;
  end;
end;

procedure TdxBoxHitTestCalculator.CalcHitTestCore(AManager: TdxBoxHitTestManager; AForceStrictHitTest: Boolean);
var
  AStrictHitTest, AStrictMatch: Boolean;
begin
  AStrictHitTest := ((Ord(HitTestRequest.Accuracy) and Ord(AManager.Box.HitTestAccuracy)) <> 0) or AForceStrictHitTest;
  AStrictMatch := AManager.Box.Bounds.Contains(HitTestRequest.LogicalPoint);
  if not AstrictHitTest and (HitTestRequest.DetailsLevel >= AManager.Box.DetailsLevel) then
  begin
    AManager.RegisterSuccessfullHitTest(AStrictMatch);
    if HitTestRequest.DetailsLevel > AManager.Box.DetailsLevel then
      AManager.CalcHitTestAmongNestedBoxes;
  end
  else
    if AStrictMatch then
    begin
      AManager.RegisterSuccessfullHitTest(AStrictMatch);
      if HitTestRequest.DetailsLevel > AManager.Box.DetailsLevel then
        AManager.CalcHitTestAmongNestedBoxes;
    end
    else
      AManager.RegisterFailedHitTest;
end;

function TdxBoxHitTestCalculator.FastHitTestIndexCore(
  ACollection: TdxBoxCollectionBase;
  APredicate: TdxBoxComparable; AStrictHitTest: Boolean): Integer;
begin
  if ACollection.Count <= 0 then
    Exit(-1);

  Result := ACollection.BinarySearch(APredicate);
  if Result < 0 then
  begin
    if AStrictHitTest then
      Exit(-1);
    if Result = not ACollection.Count then
    begin
      if APredicate.CompareTo(ACollection[0]) > 0 then
        Result := 0
      else
        Result := ACollection.Count - 1;
    end
    else
      Result := not Result;
  end;
end;

function TdxBoxHitTestCalculator.FastHitTestCore(ACollection: TdxBoxCollectionBase;
  APredicate: TdxBoxComparable; AStrictHitTest: Boolean): Integer;
begin
  Result := FastHitTestIndexCore(ACollection, APredicate, AStrictHitTest);
  if Result >= 0 then
  begin
    CalcHitTest(ACollection[Result]);
    ACollection.RegisterSuccessfullItemHitTest(Self, ACollection[Result]);
  end
  else
    ACollection.RegisterFailedItemHitTest(Self);
end;

procedure TdxBoxHitTestCalculator.FastHitTestCharacter(ACollection: TdxCharacterBoxCollection; AStrictHitTest: Boolean);
var
  ACharacterIndex: Integer;
  ABounds: TRect;
  APoint: TPoint;
  ALeftDistance, ARightDistance: Integer;
begin
  ACharacterIndex := FastHitTestAssumingArrangedHorizontally(ACollection, AStrictHitTest);
  if not AStrictHitTest and (HitTestResult.Character <> nil) and (ACharacterIndex >= 0) then
  begin
    ABounds := HitTestResult.Character.Bounds;
    APoint := HitTestRequest.LogicalPoint;
    if (ABounds.Left <= APoint.X) and (APoint.X <= ABounds.Right) then
    begin
      ALeftDistance := APoint.X - ABounds.Left;
      ARightDistance := ABounds.Right - APoint.X;
      if ALeftDistance >= ARightDistance then
      begin
        if ACharacterIndex + 1 < ACollection.Count then
        begin
          HitTestResult.Character := ACollection[ACharacterIndex + 1];
          HitTestResult.Accuracy := HitTestResult.Accuracy and not ExactCharacter;
        end;
      end;
    end;
  end;
end;

function TdxBoxHitTestCalculator.FastHitTestAssumingArrangedHorizontally(
  ACollection: TdxBoxCollectionBase; AStrictHitTest: Boolean): Integer;
var
  AComparer: TdxBoxAndPointXComparable;
begin
  AComparer := TdxBoxAndPointXComparable.Create(HitTestRequest.LogicalPoint);
  try
    Result := FastHitTestCore(ACollection, AComparer, AStrictHitTest);
  finally
    AComparer.Free;
  end;
end;

function TdxBoxHitTestCalculator.FastHitTestAssumingArrangedVertically(
  ACollection: TdxBoxCollectionBase; AStrictHitTest: Boolean): Integer;
var
  AComparer: TdxBoxAndPointYComparable;
begin
  AComparer := TdxBoxAndPointYComparable.Create(HitTestRequest.LogicalPoint);
  try
    Result := FastHitTestCore(ACollection, AComparer, AStrictHitTest);
  finally
    AComparer.Free;
  end;
end;

function TdxBoxHitTestCalculator.CreatePageHitTestManager(APage: TdxPage): TdxBoxHitTestManager;
begin
  Result := TdxPageHitTestManager.Create(Self, APage);
end;

function TdxBoxHitTestCalculator.CreatePageAreaHitTestManager(APageArea: TdxPageArea): TdxBoxHitTestManager;
begin
  Result := TdxPageAreaHitTestManager.Create(Self, APageArea);
end;

function TdxBoxHitTestCalculator.CreateColumnHitTestManager(AColumn: TdxColumn): TdxBoxHitTestManager;
begin
  Result := TdxColumnHitTestManager.Create(Self, AColumn);
end;

function TdxBoxHitTestCalculator.CreateRowHitTestManager(ARow: TdxRow): TdxBoxHitTestManager;
begin
  Result := TdxRowHitTestManager.Create(Self, ARow);
end;

function TdxBoxHitTestCalculator.CreateTextBoxHitTestManager(ABox: TdxTextBox): TdxBoxHitTestManager;
begin
  Result := TdxBoxHitTestManager.Create(Self, ABox);
end;

function TdxBoxHitTestCalculator.CreateCharacterBoxHitTestManager(ABox: TdxCharacterBox): TdxBoxHitTestManager;
begin
  Result := TdxCharacterBoxHitTestManager.Create(Self, ABox);
end;

function TdxBoxHitTestCalculator.CreateHyphenBoxHitTestManager(ABox: TdxHyphenBox): TdxBoxHitTestManager;
begin
  Result := TdxBoxHitTestManager.Create(Self, ABox);
end;

function TdxBoxHitTestCalculator.CreateInlinePictureBoxHitTestManager(ABox: TdxInlinePictureBox): TdxBoxHitTestManager;
begin
  Result := TdxBoxHitTestManager.Create(Self, ABox);
end;

function TdxBoxHitTestCalculator.CreateCustomRunBoxHitTestManager(ABox: TdxCustomRunBox): TdxBoxHitTestManager;
begin
  Result := TdxBoxHitTestManager.Create(Self, ABox);
end;

function TdxBoxHitTestCalculator.CreateSeparatorMarkBoxHitTestManager(ABox: TdxSeparatorBox): TdxBoxHitTestManager;
begin
  Result := TdxBoxHitTestManager.Create(Self, ABox);
end;

function TdxBoxHitTestCalculator.CreateSpaceBoxHitTestManager(ABox: ISpaceBox): TdxBoxHitTestManager;
begin
  Result := TdxBoxHitTestManager.Create(Self, ABox.Box);
end;

function TdxBoxHitTestCalculator.CreateTabSpaceBoxHitTestManager(ABox: TdxTabSpaceBox): TdxBoxHitTestManager;
begin
  Result := TdxBoxHitTestManager.Create(Self, ABox);
end;

function TdxBoxHitTestCalculator.CreateNumberingListBoxHitTestManager(ABox: TdxNumberingListBox): TdxBoxHitTestManager;
begin
  Result := TdxBoxHitTestManager.Create(Self, ABox);
end;

function TdxBoxHitTestCalculator.CreateLineBreakBoxHitTestManager(ABox: TdxLineBreakBox): TdxBoxHitTestManager;
begin
  Result := TdxBoxHitTestManager.Create(Self, ABox);
end;

function TdxBoxHitTestCalculator.CreatePageBreakBoxHitTestManager(ABox: TdxPageBreakBox): TdxBoxHitTestManager;
begin
  Result := TdxBoxHitTestManager.Create(Self, ABox);
end;

function TdxBoxHitTestCalculator.CreateColumnBreakBoxHitTestManager(ABox: TdxColumnBreakBox): TdxBoxHitTestManager;
begin
  Result := TdxBoxHitTestManager.Create(Self, ABox);
end;

function TdxBoxHitTestCalculator.CreateParagraphMarkBoxHitTestManager(ABox: TdxParagraphMarkBox): TdxBoxHitTestManager;
begin
  Result := TdxBoxHitTestManager.Create(Self, ABox);
end;

function TdxBoxHitTestCalculator.CreateSectionMarkBoxHitTestManager(ABox: TdxSectionMarkBox): TdxBoxHitTestManager;
begin
  Result := TdxBoxHitTestManager.Create(Self, ABox);
end;

function TdxBoxHitTestCalculator.CreateDataContainerRunBoxHitTestManager(ABox: TdxDataContainerRunBox): TdxBoxHitTestManager;
begin
  Result := TdxBoxHitTestManager.Create(Self, ABox);
end;

procedure TdxBoxHitTestCalculator.ProcessPageCollection(ACollection: TdxPageCollection);
begin
//do nothing
end;

procedure TdxBoxHitTestCalculator.ProcessPageAreaCollection(ACollection: TdxPageAreaCollection);
var
  AStrictHitTest: Boolean;
begin
  AStrictHitTest := (HitTestRequest.Accuracy and ExactPageArea) <> 0;
  FastHitTestAssumingArrangedVertically(ACollection, AStrictHitTest);
end;

procedure TdxBoxHitTestCalculator.ProcessColumnCollection(ACollection: TdxColumnCollection);
var
  AStrictHitTest: Boolean;
begin
  AStrictHitTest := (HitTestRequest.Accuracy and ExactColumn) <> 0;
  FastHitTestAssumingArrangedHorizontally(ACollection, AStrictHitTest);
end;

procedure TdxBoxHitTestCalculator.ProcessRowCollection(ACollection: TdxRowCollection);
var
  AStrictHitTest: Boolean;
begin
  AStrictHitTest := (HitTestRequest.Accuracy and ExactRow) <> 0;
  FastHitTestAssumingArrangedVertically(ACollection, AStrictHitTest);
end;

procedure TdxBoxHitTestCalculator.ProcessBoxCollection(ACollection: TdxBoxCollection);
var
  AStrictHitTest: Boolean;
begin
  AStrictHitTest := (HitTestRequest.Accuracy and ExactBox) <> 0;
  FastHitTestAssumingArrangedHorizontally(ACollection, AStrictHitTest);
end;

procedure TdxBoxHitTestCalculator.ProcessCharacterBoxCollection(ACollection: TdxCharacterBoxCollection);
var
  AStrictHitTest: Boolean;
begin
  AStrictHitTest := (HitTestRequest.Accuracy and ExactCharacter) <> 0;
  FastHitTestAssumingArrangedHorizontally(ACollection, AStrictHitTest);
end;

{ TdxBoxHitTestManager }

procedure TdxBoxHitTestManager.CalcHitTestAmongNestedBoxes;
begin
//do nothing
end;

procedure TdxBoxHitTestManager.RegisterSuccessfullHitTest(AStrictMatch: Boolean);
begin
  HitTestResult.Box := Box;
  HitTestResult.IncreaseDetailsLevel(TdxDocumentLayoutDetailsLevel.Box);
  if AStrictMatch then
    HitTestResult.Accuracy := HitTestResult.Accuracy or ExactBox;
end;

procedure TdxBoxHitTestManager.RegisterFailedHitTest;
begin
  HitTestResult.Row := nil;
end;

function TdxBoxHitTestManager.GetCalculator: TdxBoxHitTestCalculator;
begin
  Result := TdxBoxHitTestCalculator(inherited Calculator);
end;

function TdxBoxHitTestManager.GetHitTestRequest: TdxRichEditHitTestRequest;
begin
  Result := Calculator.HitTestRequest;
end;

function TdxBoxHitTestManager.GetHitTestResult: TdxRichEditHitTestResult;
begin
  Result := Calculator.HitTestResult;
end;

function TdxBoxHitTestManager.GetICalculator: IdxBoxHitTestCalculator;
begin
  Result := Calculator;
end;

{ TdxPageHitTestManager }

procedure TdxPageHitTestManager.CalcHitTestAmongNestedBoxes;
begin
  ICalculator.ProcessPageAreaCollection(Page.Areas);
end;

procedure TdxPageHitTestManager.RegisterSuccessfullHitTest(AStrictMatch: Boolean);
begin
  HitTestResult.Page := Page;
  HitTestResult.IncreaseDetailsLevel(TdxDocumentLayoutDetailsLevel.Page);
  if AStrictMatch then
    HitTestResult.Accuracy := HitTestResult.Accuracy or ExactPage;
end;

procedure TdxPageHitTestManager.RegisterFailedHitTest;
begin
  HitTestResult.Page := nil;
end;

function TdxPageHitTestManager.GetPage: TdxPage;
begin
  Result := TdxPage(Box)
end;

{ TdxPageAreaHitTestManager }

procedure TdxPageAreaHitTestManager.CalcHitTestAmongNestedBoxes;
begin
  ICalculator.ProcessColumnCollection(PageArea.Columns);
end;

procedure TdxPageAreaHitTestManager.RegisterSuccessfullHitTest(AStrictMatch: Boolean);
begin
  HitTestResult.PageArea := PageArea;
  HitTestResult.IncreaseDetailsLevel(TdxDocumentLayoutDetailsLevel.PageArea);
  if AStrictMatch then
    HitTestResult.Accuracy := HitTestResult.Accuracy or ExactPageArea;
end;

procedure TdxPageAreaHitTestManager.RegisterFailedHitTest;
begin
  HitTestResult.PageArea := nil;
end;

function TdxPageAreaHitTestManager.GetPageArea: TdxPageArea;
begin
  Result := TdxPageArea(Box)
end;

{ TdxColumnHitTestManager }

procedure TdxColumnHitTestManager.CalcHitTestAmongNestedBoxes;
var
  ATable: TdxTableViewInfo;
begin
  ATable := GetActiveTableViewInfo;
  if ATable = nil then
    ICalculator.ProcessRowCollection(Column.GetOwnRows)
  else
    CalcHitTestAmongNestedBoxesCore(ATable);
end;

procedure TdxColumnHitTestManager.RegisterFailedHitTest;
begin
  HitTestResult.Column := nil;
end;

procedure TdxColumnHitTestManager.RegisterSuccessfullHitTest(
  AStrictMatch: Boolean);
begin
  HitTestResult.Column := Column;
  HitTestResult.IncreaseDetailsLevel(TdxDocumentLayoutDetailsLevel.Column);
  if AStrictMatch then
    HitTestResult.Accuracy := HitTestResult.Accuracy or ExactColumn;
end;

function TdxColumnHitTestManager.CalcHitTestAmongNestedBoxesCore(
  ATable: TdxTableViewInfo): Boolean;
var
  ALogicalPoint: TPoint;
begin
  ALogicalPoint := HitTestRequest.LogicalPoint;
  Result := Boolean(NotImplemented); 
end;

function TdxColumnHitTestManager.GetActiveTableViewInfo: TdxTableViewInfo;
begin
  Result := nil;
end;

function TdxColumnHitTestManager.GetColumn: TdxColumn;
begin
  Result := TdxColumn(Box);
end;

{ TdxRowHitTestManager }

procedure TdxRowHitTestManager.CalcHitTestAmongNestedBoxes;
begin
  ICalculator.ProcessBoxCollection(Row.Boxes);
end;

procedure TdxRowHitTestManager.RegisterSuccessfullHitTest(AStrictMatch: Boolean);
begin
  HitTestResult.Row := Row;
  HitTestResult.IncreaseDetailsLevel(TdxDocumentLayoutDetailsLevel.Row);
  if AStrictMatch then
    HitTestResult.Accuracy := HitTestResult.Accuracy or ExactRow;
end;

procedure TdxRowHitTestManager.RegisterFailedHitTest;
begin
  HitTestResult.Row := nil;
end;

function TdxRowHitTestManager.GetRow: TdxRow;
begin
  Result := TdxRow(Box);
end;

{ TdxCharacterBoxHitTestManager }

procedure TdxCharacterBoxHitTestManager.CalcHitTestAmongNestedBoxes;
begin
//do nothing
end;

procedure TdxCharacterBoxHitTestManager.RegisterSuccessfullHitTest(AStrictMatch: Boolean);
begin
  HitTestResult.Character := Character;
  HitTestResult.IncreaseDetailsLevel(TdxDocumentLayoutDetailsLevel.Character);
  if AStrictMatch then
    HitTestResult.Accuracy := HitTestResult.Accuracy or ExactCharacter;
end;

procedure TdxCharacterBoxHitTestManager.RegisterFailedHitTest;
begin
  HitTestResult.Character := nil;
end;

function TdxCharacterBoxHitTestManager.GetCharacter: TdxCharacterBox;
begin
  Result := TdxCharacterBox(Box);
end;

{ TdxEmptyHitTestManager }

procedure TdxEmptyHitTestManager.CalcHitTestAmongNestedBoxes;
begin
//do nothing
end;

procedure TdxEmptyHitTestManager.RegisterSuccessfullHitTest(AStrictMatch: Boolean);
begin
//do nothing
end;

procedure TdxEmptyHitTestManager.RegisterFailedHitTest;
begin
//do nothing
end;

end.
