{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Import.Rtf.DestinationStyleSheet;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Types, Classes, SysUtils, dxCoreClasses, dxRichEdit.Import.Rtf, dxRichEdit.Import.Rtf.DestinationPieceTable,
  dxRichEdit.DocumentModel.ParagraphFormatting, dxRichEdit.DocumentModel.PieceTable, dxRichEdit.DocumentModel.Styles;

type
  TdxStyleSheetDestination = class(TdxDestinationPieceTable)
  private
    FStyleName: string;

    procedure AddParagraphStyleCollectionIndex(AInfo: TdxRtfParagraphFormattingInfo; AStyle: TdxParagraphStyle);
    function GetParagraphStyle(AInfo: TdxRtfParagraphFormattingInfo): TdxParagraphStyle;
    class function GetPrimaryStyleName(const AStyleName: string): string;

    // handlers
    class procedure CharacterStyleHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure NextStyleIndexHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure ParagraphStyleHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure ParentStyleIndexHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure StyleLinkKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure StyleListLevelHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure StyleListOverrideHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure TableStyleHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
  protected
    function CanAppendText: Boolean; override;
    function GetControlCharHT: TdxControlCharTranslatorTable; override;
    procedure InitializeClone(AClone: TdxRichEditRtfDestinationBase); override;
    procedure PopulateKeywordTable(ATable: TdxKeywordTranslatorTable); override;
    procedure ProcessCharCore(AChar: Char); override;
  public
    procedure FinalizePieceTableCreation; override;
    procedure NestedGroupFinished(ADestination: TdxRichEditRtfDestinationBase); override;
  end;

  TdxDestinationCharacterStyle = class(TdxDestinationPieceTable)
  private
    FStyleName: string;

    function GetCharacterStyleByName(const AName: string): TdxCharacterStyle;
    // handlers
    class procedure ParentStyleIndexHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure StyleLinkKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
  protected
    function CanAppendText: Boolean; override;
    function GetControlCharHT: TdxControlCharTranslatorTable; override;
    procedure PopulateKeywordTable(ATable: TdxKeywordTranslatorTable); override;
    procedure ProcessCharCore(AChar: Char); override;
  public
    constructor Create(AImporter: TdxRichEditDocumentModelRtfImporter; AStyleIndex: Integer); reintroduce;
    procedure BeforePopRtfState; override;
    procedure FinalizePieceTableCreation; override;
  end;

implementation

uses
  Graphics, dxRichEdit.Utils.BatchUpdateHelper, 
  dxRichEdit.DocumentModel.CharacterFormatting, dxRichEdit.Import.Rtf.DestinationTableStyle;

{ TdxStyleSheetDestination }

procedure TdxStyleSheetDestination.NestedGroupFinished(ADestination: TdxRichEditRtfDestinationBase);
var
  AStyleSheetDestination: TdxStyleSheetDestination;
  AParentCharacterProperties: TdxMergedCharacterProperties;
  AParagraphFormattingInfo: TdxRtfParagraphFormattingInfo;
  AInfo: TdxRtfParagraphFormattingInfo;
  AName: string;
  AStyle: TdxParagraphStyle;
  AIndex, ALevelIndex: Integer;
begin
  if ADestination is TdxStyleSheetDestination then
  begin
    AParagraphFormattingInfo := Importer.Position.ParagraphFormattingInfo;
    AStyleSheetDestination := ADestination as TdxStyleSheetDestination;
    AInfo := Importer.Position.ParagraphFormattingInfo;
    AStyle := GetParagraphStyle(AInfo);
    AName := GetPrimaryStyleName(AStyleSheetDestination.FStyleName);
    if AName <> '' then
      AStyle.StyleName := AName;
    if (AInfo.StyleLink >= 0) and not Importer.LinkParagraphStyleIndexToCharacterStyleIndex.ContainsKey(AParagraphFormattingInfo.StyleIndex) then
      Importer.LinkParagraphStyleIndexToCharacterStyleIndex.Add(Importer.Position.ParagraphFormattingInfo.StyleIndex, AInfo.StyleLink);
    if AInfo.NextStyle >= 0 then
    begin
      if Importer.NextParagraphStyleIndexTable.ContainsKey(AInfo.StyleIndex) then
        Importer.NextParagraphStyleIndexTable[AInfo.StyleIndex] := AInfo.NextStyle
      else
        Importer.NextParagraphStyleIndexTable.Add(AInfo.StyleIndex, AInfo.NextStyle);
    end;
    AParentCharacterProperties := Importer.GetStyleMergedParagraphCharacterProperties(-1, AParagraphFormattingInfo.ParentStyleIndex);
    try
      Importer.ApplyParagraphProperties(AStyle.ParagraphProperties, AParagraphFormattingInfo.ParentStyleIndex, AParagraphFormattingInfo);
      Importer.ApplyCharacterProperties(AStyle.CharacterProperties, Importer.Position.CharacterFormatting.Info, AParentCharacterProperties);
    finally
      AParentCharacterProperties.Free;
    end;
    if AParagraphFormattingInfo.NumberingListIndex >= 0 then
    begin
      AIndex := AParagraphFormattingInfo.NumberingListIndex;
      ALevelIndex := AParagraphFormattingInfo.ListLevelIndex;
      Importer.ParagraphStyleListOverrideIndexMap.Add(AStyle, TdxRtfNumberingListInfo.Create(AIndex, ALevelIndex));
    end;
    AddParagraphStyleCollectionIndex(AInfo, AStyle);
  end;
end;

procedure TdxStyleSheetDestination.FinalizePieceTableCreation;
begin
//do nothing
end;

function TdxStyleSheetDestination.CanAppendText: Boolean;
begin
  Result := False;
end;

function TdxStyleSheetDestination.GetControlCharHT: TdxControlCharTranslatorTable;
begin
  Result := nil;
end;

procedure TdxStyleSheetDestination.InitializeClone(AClone: TdxRichEditRtfDestinationBase);
begin
  TdxStyleSheetDestination(AClone).FStyleName := FStyleName;
end;

procedure TdxStyleSheetDestination.PopulateKeywordTable(ATable: TdxKeywordTranslatorTable);
begin
  ATable.Add('s', ParagraphStyleHandler);
  ATable.Add('sbasedon', ParentStyleIndexHandler);
  ATable.Add('cs', CharacterStyleHandler);
  ATable.Add('ts', TableStyleHandler);
  ATable.Add('slink', StyleLinkKeywordHandler);
  ATable.Add('snext', NextStyleIndexHandler);
  ATable.Add('ls', StyleListOverrideHandler);
  ATable.Add('ilvl', StyleListLevelHandler);
  AddParagraphPropertiesKeywords(ATable);
  AddCharacterPropertiesKeywords(ATable);
end;

procedure TdxStyleSheetDestination.ProcessCharCore(AChar: Char);
begin
  if AChar <> ';' then
    FStyleName := FStyleName + AChar;
end;

procedure TdxStyleSheetDestination.AddParagraphStyleCollectionIndex(AInfo: TdxRtfParagraphFormattingInfo;
  AStyle: TdxParagraphStyle);
var
  AParagraphStyleIndex: Integer;
begin
  if not Importer.ParagraphStyleCollectionIndex.ContainsKey(AInfo.StyleIndex) then
  begin
    Importer.DocumentModel.ParagraphStyles.Add(AStyle);
    if (Importer.ParagraphStyleCollectionIndex.ContainsKey(AInfo.ParentStyleIndex)) then
    begin
      AParagraphStyleIndex := Importer.ParagraphStyleCollectionIndex[AInfo.ParentStyleIndex];
      AStyle.Parent := Importer.DocumentModel.ParagraphStyles[AParagraphStyleIndex];
    end;
    Importer.ParagraphStyleCollectionIndex.Add(AInfo.StyleIndex, Importer.DocumentModel.ParagraphStyles.Count - 1);
  end;
end;

function TdxStyleSheetDestination.GetParagraphStyle(AInfo: TdxRtfParagraphFormattingInfo): TdxParagraphStyle;
begin
  if AInfo.StyleIndex = 0 then
    Result := Importer.DocumentModel.ParagraphStyles[0]
  else
    Result := TdxParagraphStyle.Create(Importer.DocumentModel, nil);
end;

class function TdxStyleSheetDestination.GetPrimaryStyleName(const AStyleName: string): string;
var
  AStringList: TStringList;
  I: Integer;
  AName: string;
begin
  Result := '';
  AStringList := TStringList.Create;
  try
    ExtractStrings([','], [' '], PChar(AStyleName), AStringList);
    for I := 0 to AStringList.Count - 1 do
    begin
      AName := Trim(AStringList[I]);
      if AName <> '' then
      begin
        Result := AName;
        Break;
      end;
    end;
  finally
    AStringList.Free;
  end;
end;

class procedure TdxStyleSheetDestination.CharacterStyleHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.Destination := TdxDestinationCharacterStyle.Create(AImporter, AParameterValue);
end;

class procedure TdxStyleSheetDestination.NextStyleIndexHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.Position.ParagraphFormattingInfo.NextStyle := AParameterValue;
end;

class procedure TdxStyleSheetDestination.ParagraphStyleHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.Position.ParagraphFormattingInfo.StyleIndex := AParameterValue;
end;

class procedure TdxStyleSheetDestination.ParentStyleIndexHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.Position.ParagraphFormattingInfo.ParentStyleIndex := AParameterValue;
end;

class procedure TdxStyleSheetDestination.StyleLinkKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.Position.ParagraphFormattingInfo.StyleLink := AParameterValue;
end;

class procedure TdxStyleSheetDestination.StyleListLevelHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.Position.ParagraphFormattingInfo.ListLevelIndex := AParameterValue;
end;

class procedure TdxStyleSheetDestination.StyleListOverrideHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.Position.ParagraphFormattingInfo.NumberingListIndex := AParameterValue;
end;

class procedure TdxStyleSheetDestination.TableStyleHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.Destination := TdxDestinationTableStyle.Create(AImporter, AParameterValue);
end;

{ TdxDestinationCharacterStyle }

constructor TdxDestinationCharacterStyle.Create(
  AImporter: TdxRichEditDocumentModelRtfImporter; AStyleIndex: Integer);
begin
  inherited Create(AImporter);
  AImporter.Position.CharacterStyleIndex := AStyleIndex;
end;

procedure TdxDestinationCharacterStyle.FinalizePieceTableCreation;
begin
//do nothing
end;

procedure TdxDestinationCharacterStyle.BeforePopRtfState;
var
  AName: string;
  AStyle: TdxCharacterStyle;
  AParentCharacterProperties: TdxMergedCharacterProperties;
begin
  AName := TdxStyleSheetDestination.GetPrimaryStyleName(FStyleName);
  if not Importer.CharacterStyleCollectionIndex.ContainsKey(Importer.Position.CharacterStyleIndex) then
  begin
    AStyle := GetCharacterStyleByName(AName);
    if (Importer.Position.ParagraphFormattingInfo.StyleLink >= 0) and
        not Importer.LinkParagraphStyleIndexToCharacterStyleIndex.ContainsKey(Importer.Position.ParagraphFormattingInfo.StyleLink) then
      Importer.LinkParagraphStyleIndexToCharacterStyleIndex.Add(Importer.Position.ParagraphFormattingInfo.StyleLink, Importer.Position.CharacterStyleIndex);
    if AName <> TdxCharacterStyleCollection.DefaultCharacterStyleName then
    begin
      AParentCharacterProperties := Importer.GetStyleMergedCharacterProperties(Importer.Position.RtfFormattingInfo.ParentStyleIndex);
      try
        Importer.ApplyCharacterProperties(AStyle.CharacterProperties, Importer.Position.CharacterFormatting.Info, AParentCharacterProperties);
      finally
        AParentCharacterProperties.Free;
      end;
    end
    else
    if true then 
    begin
      AStyle.CharacterProperties.BeginUpdate;
      try
        if Importer.Position.CharacterFormatting.FontName <> AStyle.CharacterProperties.FontName then
          AStyle.CharacterProperties.FontName := Importer.Position.CharacterFormatting.FontName;
        if Importer.Position.CharacterFormatting.DoubleFontSize <> AStyle.CharacterProperties.DoubleFontSize then
          AStyle.CharacterProperties.DoubleFontSize := Importer.Position.CharacterFormatting.DoubleFontSize;
        if Importer.Position.CharacterFormatting.ForeColor <> AStyle.CharacterProperties.ForeColor then
          AStyle.CharacterProperties.ForeColor := Importer.Position.CharacterFormatting.ForeColor
        else
          AStyle.CharacterProperties.ForeColor := Importer.DocumentProperties.Colors[0];
      finally
        AStyle.CharacterProperties.EndUpdate;
      end;
    end;
    if Importer.CharacterStyleCollectionIndex.ContainsKey(Importer.Position.RtfFormattingInfo.ParentStyleIndex) then
      AStyle.Parent := Importer.DocumentModel.CharacterStyles[Importer.CharacterStyleCollectionIndex[Importer.Position.RtfFormattingInfo.ParentStyleIndex]];
  end;
end;

function TdxDestinationCharacterStyle.CanAppendText: Boolean;
begin
  Result := False;
end;

function TdxDestinationCharacterStyle.GetControlCharHT: TdxControlCharTranslatorTable;
begin
  Result := nil;
end;

procedure TdxDestinationCharacterStyle.PopulateKeywordTable(
  ATable: TdxKeywordTranslatorTable);
begin
  ATable.Add('sbasedon', ParentStyleIndexHandler);
  ATable.Add('slink', StyleLinkKeywordHandler);
  AddCharacterPropertiesKeywords(ATable);
end;

procedure TdxDestinationCharacterStyle.ProcessCharCore(AChar: Char);
begin
  if AChar <> ';' then
    FStyleName := FStyleName + AChar;
end;

function TdxDestinationCharacterStyle.GetCharacterStyleByName(const AName: string): TdxCharacterStyle;
var
  ADomumentModel: TdxDocumentModel;
  ACharacterStyles: TdxCharacterStyleCollection;
  AStyleIndex: Integer;
begin
  ADomumentModel := Importer.DocumentModel;
  ACharacterStyles := ADomumentModel.CharacterStyles;
  AStyleIndex := ACharacterStyles.GetStyleIndexByName(AName);
  if AStyleIndex >= 0 then
  begin
    if not Importer.CharacterStyleCollectionIndex.ContainsKey(Importer.Position.CharacterStyleIndex) then
      Importer.CharacterStyleCollectionIndex.Add(Importer.Position.CharacterStyleIndex, AStyleIndex)
    else
      Importer.CharacterStyleCollectionIndex[Importer.Position.CharacterStyleIndex] := AStyleIndex;
    Result := ACharacterStyles[AStyleIndex];
  end
  else
  begin
    Result := TdxCharacterStyle.Create(ADomumentModel, nil);
    Result.StyleName := AName;
    AStyleIndex := ACharacterStyles.Add(Result);
    Importer.CharacterStyleCollectionIndex.Add(Importer.Position.CharacterStyleIndex, AStyleIndex);
  end;
end;

class procedure TdxDestinationCharacterStyle.ParentStyleIndexHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.Position.RtfFormattingInfo.ParentStyleIndex := AParameterValue;
end;

class procedure TdxDestinationCharacterStyle.StyleLinkKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.Position.ParagraphFormattingInfo.StyleLink := AParameterValue;
end;

end.
