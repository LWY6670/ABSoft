{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Import.Rtf.DestinationTableStyle;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Types, Classes, SysUtils, dxCoreClasses, dxRichEdit.Import.Rtf, dxRichEdit.Import.Rtf.DestinationPieceTable,
  dxRichEdit.DocumentModel.ParagraphFormatting, dxRichEdit.DocumentModel.PieceTable,
  dxRichEdit.DocumentModel.Styles, dxRichEdit.DocumentModel.TableStyles,
  dxRichEdit.DocumentModel.TableFormatting, dxRichEdit.DocumentModel.CharacterFormatting;

type
  { TdxDestinationTableStyle }

  TdxDestinationTableStyle = class(TdxDestinationPieceTable)
  strict private
    FConditionalTableStyleFormattingType: TdxConditionalTableStyleFormattingTypes;
    FQFormat: Boolean;
    FRtfStyleIndex: Integer;
    FStyleName: string;
    function AddConditionalStyle: TdxTableConditionalStyle;
    class function GetThis(AImporter: TdxRichEditDocumentModelRtfImporter): TdxDestinationTableStyle;
    class procedure ParentStyleIndexHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure StyleQFormatKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure ConditionalStyleFirstRowHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure ConditionalStyleLastRowHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure ConditionalStyleFirstColumnHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure ConditionalStyleLastColumnHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure ConditionalStyleOddRowBandingHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure ConditionalStyleEvenRowBandingHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure ConditionalStyleOddColumnBandingHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure ConditionalStyleEvenColumnBandingHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure ConditionalStyleTopLeftCellHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure ConditionalStyleTopRightCellHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure ConditionalStyleBottomLeftCellHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure ConditionalStyleBottomRightCellHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
  protected
    function CanAppendText: Boolean; override;
    function GetControlCharHT: TdxControlCharTranslatorTable; override;
    procedure InitializeClone(AClone: TdxRichEditRtfDestinationBase); override;
    procedure PopulateKeywordTable(ATable: TdxKeywordTranslatorTable); override;
    procedure ProcessCharCore(AChar: Char); override;

    function GetTableStyleByName(const AStyleName: string): TdxTableStyle;
    property QFormat: Boolean read FQFormat write FQFormat;
  public
    constructor Create(AImporter: TdxRichEditDocumentModelRtfImporter); overload; override;
    constructor Create(AImporter: TdxRichEditDocumentModelRtfImporter; AStyleIndex: Integer); reintroduce; overload;
    procedure BeforePopRtfState; override;
    procedure FinalizePieceTableCreation; override;
  end;

implementation

uses
  dxRichEdit.Utils.BatchUpdateHelper; 

{ TdxDestinationTableStyle }

constructor TdxDestinationTableStyle.Create(
  AImporter: TdxRichEditDocumentModelRtfImporter);
begin
  inherited Create(AImporter);
  FConditionalTableStyleFormattingType := TdxConditionalTableStyleFormattingTypes.WholeTable;
end;

function TdxDestinationTableStyle.AddConditionalStyle: TdxTableConditionalStyle;
var
  ADocumentModel: TdxDocumentModel;
  AMainStyle: TdxTableStyle;
  ATableConditionalStyle: TdxTableConditionalStyle;
begin
  if not Importer.TableStyleCollectionIndex.ContainsKey(FRtfStyleIndex) then
    Exit(nil);

  ADocumentModel := Importer.DocumentModel;
  AMainStyle := ADocumentModel.TableStyles[Importer.GetTableStyleIndex(FRtfStyleIndex)];
  ATableConditionalStyle := AMainStyle.ConditionalStyleProperties.GetStyleSafe(FConditionalTableStyleFormattingType);
  Result := ATableConditionalStyle;
end;

procedure TdxDestinationTableStyle.BeforePopRtfState;
var
  AName: string;
  AStyle: IdxTableStyle;
  AParentStyleIndex: Integer;
  AIsConditionalStyle: Boolean;
  ATableStyle: TdxTableStyle;
  AParentTableStyle: TdxTableStyle;
  AParentCharacterProperties: TdxMergedCharacterProperties;
  AParentParagraphProperties: TdxMergedParagraphProperties;
  AParentTableProperties: TdxMergedTableProperties;
  AParentTableRowProperties: TdxMergedTableRowProperties;
  AParentTableCellProperties: TdxMergedTableCellProperties;
begin
  AName := Trim(FStyleName);
  AParentStyleIndex := Importer.Position.RtfFormattingInfo.ParentStyleIndex;
  AIsConditionalStyle := FConditionalTableStyleFormattingType <> TdxConditionalTableStyleFormattingTypes.WholeTable;
  if AIsConditionalStyle then
    AStyle := AddConditionalStyle
  else
    if not Importer.TableStyleCollectionIndex.ContainsKey(FRtfStyleIndex) then
      AStyle := GetTableStyleByName(AName);

  if AStyle = nil then
    Exit;
  if AStyle is TdxTableStyle then
    ATableStyle := TdxTableStyle(AStyle)
  else
    ATableStyle := nil;
  if ATableStyle <> nil then
    ATableStyle.Primary := QFormat;
  if AName <> TdxTableStyleCollection.DefaultTableStyleName then
  begin
    if not AIsConditionalStyle then
      AParentTableStyle := DocumentModel.TableStyles[Importer.GetTableStyleIndex(AParentStyleIndex)]
    else
    begin
      AParentTableStyle := AStyle.Parent;
      if AParentTableStyle <> nil then
        AParentTableStyle := DocumentModel.TableStyles[Importer.GetTableStyleIndex(-1)];
    end;
    if AIsConditionalStyle then
    begin
      AParentCharacterProperties := AParentTableStyle.GetMergedCharacterProperties(FConditionalTableStyleFormattingType);
      AParentParagraphProperties := AParentTableStyle.GetMergedParagraphProperties(FConditionalTableStyleFormattingType);
      AParentTableRowProperties := AParentTableStyle.GetMergedTableRowProperties(FConditionalTableStyleFormattingType);
      AParentTableCellProperties := AParentTableStyle.GetMergedTableCellProperties(FConditionalTableStyleFormattingType);
    end
    else
    begin
      AParentCharacterProperties := Importer.GetStyleMergedCharacterProperties(AParentTableStyle.GetMergedCharacterProperties);
      AParentParagraphProperties := Importer.GetStyleMergedParagraphProperties(AParentTableStyle.GetMergedParagraphProperties);
      AParentTableRowProperties := Importer.GetStyleMergedTableRowProperties(AParentTableStyle.GetMergedTableRowProperties);
      AParentTableCellProperties := Importer.GetStyleMergedTableCellProperties(AParentTableStyle.GetMergedTableCellProperties);
    end;
    AParentTableProperties := AParentTableStyle.GetMergedTableProperties;
    try
      Importer.ApplyCharacterProperties(AStyle.CharacterProperties, Importer.Position.CharacterFormatting.Info, AParentCharacterProperties);
      Importer.ApplyParagraphProperties(AStyle.ParagraphProperties, Importer.Position.ParagraphFormattingInfo, AParentParagraphProperties);
      Importer.ApplyTableProperties(Importer.TableReader.TableProperties, AParentTableProperties);
      AStyle.TableProperties.CopyFrom(Importer.TableReader.TableProperties);

      Importer.ApplyTableRowProperties(Importer.TableReader.RowProperties, AParentTableRowProperties);
      AStyle.TableRowProperties.CopyFrom(Importer.TableReader.RowProperties);

      Importer.ApplyTableCellProperties(Importer.TableReader.CellProperties, AParentTableCellProperties);
      AStyle.TableCellProperties.CopyFrom(Importer.TableReader.CellProperties);
    finally
      AParentCharacterProperties.Free;
      AParentParagraphProperties.Free;
      AParentTableRowProperties.Free;
      AParentTableCellProperties.Free;
      AParentTableProperties.Free;
    end;
  end;
  if not AIsConditionalStyle and Importer.TableStyleCollectionIndex.ContainsKey(AParentStyleIndex) then
    ATableStyle.Parent := DocumentModel.TableStyles[Importer.GetTableStyleIndex(AParentStyleIndex)];
end;

function TdxDestinationTableStyle.CanAppendText: Boolean;
begin
  Result := False;
end;

constructor TdxDestinationTableStyle.Create(
  AImporter: TdxRichEditDocumentModelRtfImporter; AStyleIndex: Integer);
begin
  Create(AImporter);
  FRtfStyleIndex := AStyleIndex;
end;

procedure TdxDestinationTableStyle.FinalizePieceTableCreation;
begin
//do nothing
end;

function TdxDestinationTableStyle.GetControlCharHT: TdxControlCharTranslatorTable;
begin
  Result := nil;
end;

function TdxDestinationTableStyle.GetTableStyleByName(
  const AStyleName: string): TdxTableStyle;
var
  ADocumentModel: TdxDocumentModel;
  ATableStyles: TdxTableStyleCollection;
  AStyleIndex: Integer;
begin
  ADocumentModel := Importer.DocumentModel;
  ATableStyles := ADocumentModel.TableStyles;
  AStyleIndex := ATableStyles.GetStyleIndexByName(AStyleName);
  if AStyleIndex >= 0 then
  begin
    Importer.TableStyleCollectionIndex.Add(FRtfStyleIndex, AStyleIndex);
    Result := ATableStyles[AStyleIndex];
  end
  else
  begin
    Result := TdxTableStyle.Create(ADocumentModel);
    Result.StyleName := AStyleName;
    AStyleIndex := ATableStyles.Add(Result);
    Importer.TableStyleCollectionIndex.Add(FRtfStyleIndex, AStyleIndex);
  end;
end;

class function TdxDestinationTableStyle.GetThis(
  AImporter: TdxRichEditDocumentModelRtfImporter): TdxDestinationTableStyle;
begin
  Result := AImporter.Destination as TdxDestinationTableStyle;
end;

procedure TdxDestinationTableStyle.InitializeClone(
  AClone: TdxRichEditRtfDestinationBase);
begin
  inherited InitializeClone(AClone);
  FRtfStyleIndex := Importer.Position.CharacterStyleIndex;
end;

procedure TdxDestinationTableStyle.PopulateKeywordTable(
  ATable: TdxKeywordTranslatorTable);
begin
  ATable.Add('sbasedon', ParentStyleIndexHandler);
  ATable.Add('sqformat', StyleQFormatKeywordHandler);

  ATable.Add('tscfirstrow', ConditionalStyleFirstRowHandler);
  ATable.Add('tsclastrow', ConditionalStyleLastRowHandler);
  ATable.Add('tscfirstcol', ConditionalStyleFirstColumnHandler);
  ATable.Add('tsclastcol', ConditionalStyleLastColumnHandler);
  ATable.Add('tscbandhorzodd', ConditionalStyleOddRowBandingHandler);
  ATable.Add('tscbandhorzeven', ConditionalStyleEvenRowBandingHandler);
  ATable.Add('tscbandvertodd', ConditionalStyleOddColumnBandingHandler);
  ATable.Add('tscbandverteven', ConditionalStyleEvenColumnBandingHandler);
  ATable.Add('tscnwcell', ConditionalStyleTopLeftCellHandler);
  ATable.Add('tscnecell', ConditionalStyleTopRightCellHandler);
  ATable.Add('tscswcell', ConditionalStyleBottomLeftCellHandler);
  ATable.Add('tscsecell', ConditionalStyleBottomRightCellHandler);
  AddCharacterPropertiesKeywords(ATable);
  AddParagraphPropertiesKeywords(ATable);
  AppendTablePropertiesKeywords(ATable);
end;

procedure TdxDestinationTableStyle.ProcessCharCore(AChar: Char);
begin
  if AChar <> ';' then
    FStyleName := FStyleName + AChar;
end;

class procedure TdxDestinationTableStyle.ParentStyleIndexHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.Position.RtfFormattingInfo.ParentStyleIndex := AParameterValue;
end;

class procedure TdxDestinationTableStyle.StyleQFormatKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  GetThis(AImporter).QFormat := True;
end;

class procedure TdxDestinationTableStyle.ConditionalStyleFirstRowHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  GetThis(AImporter).FConditionalTableStyleFormattingType := TdxConditionalTableStyleFormattingTypes.FirstRow;
end;

class procedure TdxDestinationTableStyle.ConditionalStyleLastRowHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  GetThis(AImporter).FConditionalTableStyleFormattingType := TdxConditionalTableStyleFormattingTypes.LastRow;
end;

class procedure TdxDestinationTableStyle.ConditionalStyleFirstColumnHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  GetThis(AImporter).FConditionalTableStyleFormattingType := TdxConditionalTableStyleFormattingTypes.FirstColumn;
end;

class procedure TdxDestinationTableStyle.ConditionalStyleLastColumnHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  GetThis(AImporter).FConditionalTableStyleFormattingType := TdxConditionalTableStyleFormattingTypes.LastColumn;
end;

class procedure TdxDestinationTableStyle.ConditionalStyleOddRowBandingHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  GetThis(AImporter).FConditionalTableStyleFormattingType := TdxConditionalTableStyleFormattingTypes.OddRowBanding;
end;

class procedure TdxDestinationTableStyle.ConditionalStyleEvenRowBandingHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  GetThis(AImporter).FConditionalTableStyleFormattingType := TdxConditionalTableStyleFormattingTypes.EvenRowBanding;
end;

class procedure TdxDestinationTableStyle.ConditionalStyleOddColumnBandingHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  GetThis(AImporter).FConditionalTableStyleFormattingType := TdxConditionalTableStyleFormattingTypes.OddColumnBanding;
end;

class procedure TdxDestinationTableStyle.ConditionalStyleEvenColumnBandingHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  GetThis(AImporter).FConditionalTableStyleFormattingType := TdxConditionalTableStyleFormattingTypes.EvenColumnBanding;
end;

class procedure TdxDestinationTableStyle.ConditionalStyleTopLeftCellHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  GetThis(AImporter).FConditionalTableStyleFormattingType := TdxConditionalTableStyleFormattingTypes.TopLeftCell;
end;

class procedure TdxDestinationTableStyle.ConditionalStyleTopRightCellHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  GetThis(AImporter).FConditionalTableStyleFormattingType := TdxConditionalTableStyleFormattingTypes.TopRightCell;
end;

class procedure TdxDestinationTableStyle.ConditionalStyleBottomLeftCellHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  GetThis(AImporter).FConditionalTableStyleFormattingType := TdxConditionalTableStyleFormattingTypes.BottomLeftCell;
end;

class procedure TdxDestinationTableStyle.ConditionalStyleBottomRightCellHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  GetThis(AImporter).FConditionalTableStyleFormattingType := TdxConditionalTableStyleFormattingTypes.BottomRightCell;
end;

end.
