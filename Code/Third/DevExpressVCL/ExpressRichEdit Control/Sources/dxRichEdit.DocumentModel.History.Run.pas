{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.DocumentModel.History.Run;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  dxRichEdit.DocumentModel.Core, dxRichEdit.DocumentModel.PieceTable, dxRichEdit.DocumentModel.History.Paragraph,
  Generics.Collections, dxRichEdit.DocumentModel.History.IndexChangedHistoryItem, dxRichEdit.DocumentModel.Numbering;
type

  { TdxTextRunsJoinedHistoryItem }

  TdxTextRunsJoinedHistoryItem = class(TdxTextRunBaseHistoryItem)
  private
    FSplitOffset: Integer;
  protected
    procedure UndoCore; override;
    procedure RedoCore; override;
  public
    constructor Create(APieceTable: TdxPieceTable);
  end;

  { TdxTextRunsDeletedHistoryItem }

  TdxTextRunsDeletedHistoryItem = class(TdxTextRunBaseHistoryItem)
  private
    FDeletedRunCount: Integer;
    FDeletedRuns: TObjectList<TdxTextRunBase>;
    FDeltaLength: Integer;
    FNotificationIds: TList<Integer>;
  protected
    procedure RedoCore; override;
    procedure UndoCore; override;
  public
    constructor Create(APieceTable: TdxPieceTable);
    destructor Destroy; override;

    property DeletedRunCount: Integer read FDeletedRunCount write FDeletedRunCount;


  end;

  { TdxInlineCustomObjectRunInsertedHistoryItem }

  TdxInlineCustomObjectRunInsertedHistoryItem = class(TdxTextRunInsertedBaseHistoryItem)
  private
    FCustomObject: IInlineCustomObject;
    FScaleX: Single;
    FScaleY: Single;
    FNotificationId: Integer;
  protected
    procedure UndoCore; override;
    procedure RedoCore; override;
  public
    property CustomObject: IInlineCustomObject read FCustomObject write FCustomObject;
    property ScaleX: Single read FScaleX write FScaleX;
    property ScaleY: Single read FScaleY write FScaleY;
  end;

  { TdxRunPropertiesChangedHistoryItemBase }

  TdxRunPropertiesChangedHistoryItemBase = class(TdxIndexChangedHistoryItemCore<TdxDocumentModelChangeActions>)
  private
    FRunIndex: TdxRunIndex;
  protected
    function GetProperties(ARun: TdxTextRunBase): IdxIndexBasedObject<TdxDocumentModelChangeActions>; virtual; abstract;
  public
    constructor Create(APieceTable: TdxPieceTable; ARunIndex: TdxRunIndex); reintroduce;
    function GetObject: IdxIndexBasedObject<TdxDocumentModelChangeActions>; override;

    property RunIndex: TdxRunIndex read FRunIndex;
  end;

  { TdxRunInlinePicturePropertiesChangedHistoryItem }

  TdxRunInlinePicturePropertiesChangedHistoryItem = class(TdxRunPropertiesChangedHistoryItemBase)
  protected
    function GetProperties(ATextRunBase: TdxTextRunBase): IdxIndexBasedObject<TdxDocumentModelChangeActions>; override;
  end;

  { TdxRunCharacterPropertiesChangedHistoryItem }

  TdxRunCharacterPropertiesChangedHistoryItem = class(TdxRunPropertiesChangedHistoryItemBase)
  protected
    function GetProperties(ARun: TdxTextRunBase): IdxIndexBasedObject<TdxDocumentModelChangeActions>; override;
  end;

  { TextRunChangeCaseHistoryItem }

  TdxTextRunChangeCaseHistoryItem = class abstract(TdxRichEditHistoryItem) 
  const
    ChangeActions: TdxDocumentModelChangeActions = [TdxDocumentModelChangeAction.ResetPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout,
      TdxDocumentModelChangeAction.ResetSpellingCheck,
      TdxDocumentModelChangeAction.ResetRuler,
      TdxDocumentModelChangeAction.RaiseContentChanged,
      TdxDocumentModelChangeAction.RaiseModifiedChanged,
      TdxDocumentModelChangeAction.Redraw,
      TdxDocumentModelChangeAction.ResetSelectionLayout]; 
  private
    FRunIndex: TdxRunIndex; 
		FOriginalText: string; 
  protected
    procedure UndoCore; override; 
    procedure RedoCore; override; 
    function ChangeCase(const AValue: WideChar): WideChar; virtual; abstract; 
  public
    constructor Create(APieceTable: TdxPieceTable; ARunIndex: TdxRunIndex); reintroduce;

    procedure Execute; override; 

    property RunIndex: TdxRunIndex read FRunIndex; 
  end;

  { TdxTextRunMakeLowerCaseHistoryItem }

  TdxTextRunMakeLowerCaseHistoryItem = class(TdxTextRunChangeCaseHistoryItem) 
  protected
    function ChangeCase(const AValue: WideChar): WideChar; override; 
  end;

  { TdxTextRunToggleCaseHistoryItem }

  TdxTextRunToggleCaseHistoryItem = class(TdxTextRunChangeCaseHistoryItem) 
  protected
    function ChangeCase(const AValue: WideChar): WideChar; override; 
  end;

  { TdxSectionRunInsertedHistoryItem }

  TdxSectionRunInsertedHistoryItem = class(TdxParagraphRunInsertedHistoryItem) 
  public
    function CreateRun(AParagraph: TdxParagraph): TdxTextRunBase; override; 
  end;

  { TdxCustomRunInsertedHistoryItem }

  TdxCustomRunInsertedHistoryItem = class(TdxTextRunInsertedBaseHistoryItem) 
  private
    FNotificationId: Integer; 
    FCustomRunObject: ICustomRunObject;
  protected
    procedure UndoCore; override; 
    procedure RedoCore; override; 
  public
		property CustomRunObject: ICustomRunObject read FCustomRunObject write FCustomRunObject; 
  end;

  { TdxDataContainerRunInsertedHistoryItem }

  TdxDataContainerRunInsertedHistoryItem = class(TdxTextRunInsertedBaseHistoryItem) 
  private
    FNotificationId: Integer; 
    FDataContainer: IDataContainer;
  protected
    procedure UndoCore; override; 
    procedure RedoCore; override; 
  public
    property DataContainer: IDataContainer read FDataContainer write FDataContainer; 
  end;

  { TdxAddAbstractNumberingListHistoryItem }

  TdxAddAbstractNumberingListHistoryItem = class(TdxRichEditHistoryItem) 
  private
    FAbstractList: TdxAbstractNumberingList;
    FOwnedAbstractList: Boolean;
  protected
    procedure UndoCore; override; 
    procedure RedoCore; override; 
  public
    destructor Destroy; override;

    property AbstractList: TdxAbstractNumberingList read FAbstractList write FAbstractList; 
  end;

  { TdxAddNumberingListHistoryItem }

  TdxAddNumberingListHistoryItem = class(TdxRichEditHistoryItem) 
  private
    FNumberingList: TdxNumberingList; 
    FOwnedNumberingList: Boolean;
  protected
    procedure UndoCore; override; 
    procedure RedoCore; override; 
  public
    destructor Destroy; override;

    property NumberingList: TdxNumberingList read FNumberingList write FNumberingList; 
  end;

implementation

uses
  SysUtils, Classes, Character, dxCoreClasses,
  dxRichEdit.Utils.ChunkedStringBuilder,
  dxRichEdit.DocumentModel.ParagraphRange,
  dxRichEdit.DocumentModel.ParagraphFormatting,
  dxRichEdit.DocumentModel.SectionRange;

{ TdxTextRunsJoinedHistoryItem }

constructor TdxTextRunsJoinedHistoryItem.Create(APieceTable: TdxPieceTable);
begin
  inherited Create(APieceTable);
  FSplitOffset := -1;
end;

procedure TdxTextRunsJoinedHistoryItem.RedoCore;
var
  ARuns: TdxTextRunCollection;
  ARun, ANextRun: TdxTextRun;
begin
  ARuns := PieceTable.Runs;
  ARun := ARuns[RunIndex] as TdxTextRun;
  ANextRun := ARuns[RunIndex + 1] as TdxTextRun;
  FSplitOffset := ARun.Length;
  Assert(ARun.CanJoinWith(ANextRun));
  ARuns.Extract(RunIndex + 1); 
  ARun.Length := ARun.Length + ANextRun.Length;
  TdxDocumentModelStructureChangedNotifier.NotifyRunJoined(PieceTable, PieceTable, ParagraphIndex, RunIndex,
    FSplitOffset, ANextRun.Length);
  PieceTable.ApplyChanges(TdxDocumentModelChangeType.JoinRun, RunIndex, RunIndex);
  DocumentModel.ResetMerging;
end;

procedure TdxTextRunsJoinedHistoryItem.UndoCore;
var
  ARuns: TdxTextRunCollection;
  ARun, ATailRun: TdxTextRun;
begin
  ARuns := PieceTable.Runs;
  ARun := TdxTextRun(ARuns[RunIndex]);
  ATailRun := TdxTextRun.Create(ARun.Paragraph, ARun.StartIndex + FSplitOffset, ARun.Length - FSplitOffset);
  ARuns.Insert(ATailRun, RunIndex + 1);
  ARun.Length := FSplitOffset;
  ATailRun.InheritStyleAndFormattingFromCore(ARun, False);
  TdxDocumentModelStructureChangedNotifier.NotifyRunSplit(PieceTable, PieceTable, ParagraphIndex, RunIndex, FSplitOffset);
  PieceTable.ApplyChanges(TdxDocumentModelChangeType.SplitRun, RunIndex, RunIndex + 1);
  DocumentModel.ResetMerging;
end;

{ TdxTextRunsDeletedHistoryItem }

constructor TdxTextRunsDeletedHistoryItem.Create(APieceTable: TdxPieceTable);
begin
  inherited Create(APieceTable);
  FDeletedRunCount := -1;
end;

destructor TdxTextRunsDeletedHistoryItem.Destroy;
begin
  FreeAndNil(FNotificationIds);
  FreeAndNil(FDeletedRuns);
  inherited Destroy;
end;

procedure TdxTextRunsDeletedHistoryItem.RedoCore;
var
  ARuns: TdxTextRunCollection;
  ACount: TdxRunIndex;
  ASaveUndoRedoInfo: Boolean;
  I: Integer;
  ALength: Integer;
begin
  DocumentModel.ResetMerging;
  ARuns := PieceTable.Runs;
  ACount := RunIndex + DeletedRunCount;
  ASaveUndoRedoInfo := not DocumentModel.ModelForExport;
  if ASaveUndoRedoInfo then
  begin
    FDeletedRuns.Free;
    FDeletedRuns := TObjectList<TdxTextRunBase>.Create;
    FDeltaLength := 0;
    if FNotificationIds = nil then
    begin
      FNotificationIds := TList<Integer>.Create;
      for I := 0 to DeletedRunCount - 1 do
        FNotificationIds.Add(DocumentModel.History.GetNotificationId);
    end;
  end;
  for I := ACount - 1 downto RunIndex do
  begin
    ALength := ARuns[I].Length;
    if ASaveUndoRedoInfo then
    begin
      FDeletedRuns.Add(ARuns[I]);
      FDeltaLength := FDeltaLength + ALength;
    end;
    ARuns[I].BeforeRunRemoved;
    ARuns.Extract(I);
    TdxDocumentModelStructureChangedNotifier.NotifyRunRemoved(PieceTable, PieceTable, ParagraphIndex,
      I, ALength, DocumentModel.History.GetNotificationId); 
  end;
end;

procedure TdxTextRunsDeletedHistoryItem.UndoCore;
var
  ARuns: TdxTextRunCollection;
  ACount, I: TdxRunIndex;
  ARun: TdxTextRunBase;
begin
  ARuns := PieceTable.Runs;
  ACount := RunIndex + DeletedRunCount;
  FDeletedRuns.OwnsObjects := False;
  for I := RunIndex to ACount - 1 do
  begin
    ARun := FDeletedRuns[ACount - I - 1];
    ARuns.Insert(ARun, I);
    TdxDocumentModelStructureChangedNotifier.NotifyRunInserted(PieceTable, PieceTable,
      ParagraphIndex, I, ARun.Length, FNotificationIds[I - RunIndex]);
    ARuns[I].AfterRunInserted;
  end;
end;

{ TdxInlineCustomObjectRunInsertedHistoryItem }

procedure TdxInlineCustomObjectRunInsertedHistoryItem.UndoCore;
begin
  PieceTable.Runs[RunIndex].BeforeRunRemoved;
  PieceTable.Runs.Delete(RunIndex);
  DocumentModel.ResetMerging;

  TdxDocumentModelStructureChangedNotifier.NotifyRunRemoved(PieceTable, PieceTable, ParagraphIndex, RunIndex, 1, FNotificationId);
end;

procedure TdxInlineCustomObjectRunInsertedHistoryItem.RedoCore;
var
  ARuns: TdxTextRunCollection;
  ANewRange: TdxInlineCustomObjectRun;
begin
  ARuns := PieceTable.Runs;
  ANewRange := TdxInlineCustomObjectRun.Create(ARuns[RunIndex].Paragraph, FCustomObject);
  ANewRange.StartIndex := StartIndex;
  ANewRange.ScaleX := ScaleX;
  ANewRange.ScaleY := ScaleY;
  ARuns.Insert(ANewRange, RunIndex);
  DocumentModel.ResetMerging;
  if FNotificationId = TdxNotificationIdGenerator.EmptyId then
    FNotificationId := DocumentModel.History.GetNotificationId;
  TdxDocumentModelStructureChangedNotifier.NotifyRunInserted(PieceTable, PieceTable, ParagraphIndex, RunIndex, 1, FNotificationId);
  ARuns[RunIndex].AfterRunInserted;
end;

{ TdxRunPropertiesChangedHistoryItemBase }

constructor TdxRunPropertiesChangedHistoryItemBase.Create(APieceTable: TdxPieceTable; ARunIndex: TdxRunIndex);
begin
  inherited Create(APieceTable);
  Assert(ARunIndex >= 0);
  FRunIndex := ARunIndex;
end;

function TdxRunPropertiesChangedHistoryItemBase.GetObject: IdxIndexBasedObject<TdxDocumentModelChangeActions>;
begin
  Result := GetProperties(TdxPieceTable(DocumentModelPart).Runs[RunIndex]);
end;

{ TdxRunInlinePicturePropertiesChangedHistoryItem }

function TdxRunInlinePicturePropertiesChangedHistoryItem.GetProperties(
  ATextRunBase: TdxTextRunBase): IdxIndexBasedObject<TdxDocumentModelChangeActions>;
begin
  Result := TdxInlinePictureRun(ATextRunBase).PictureProperties;
end;

{ TdxRunCharacterPropertiesChangedHistoryItem }

function TdxRunCharacterPropertiesChangedHistoryItem.GetProperties(ARun: TdxTextRunBase): IdxIndexBasedObject<TdxDocumentModelChangeActions>;
begin
  Result := ARun.CharacterProperties;
end;

{ TdxTextRunChangeCaseHistoryItem }

constructor TdxTextRunChangeCaseHistoryItem.Create(APieceTable: TdxPieceTable; ARunIndex: TdxRunIndex);
begin
  inherited Create(APieceTable);
  FRunIndex := ARunIndex;
end;

procedure TdxTextRunChangeCaseHistoryItem.Execute;
var
  ARun: TdxTextRun;
begin
  ARun := TdxTextRun(PieceTable.Runs[RunIndex]); 
  FOriginalText := ARun.GetPlainText(PieceTable.TextBuffer);
  inherited Execute;
end;

procedure TdxTextRunChangeCaseHistoryItem.RedoCore;
var
  ARun: TdxTextRunBase;
  ATextBuffer: TdxChunkedStringBuilder;
  I: Integer;
begin
  ARun := PieceTable.Runs[RunIndex];
  ATextBuffer := PieceTable.TextBuffer;
  for I := ARun.StartIndex + ARun.Length - 1 downto ARun.StartIndex do
    ATextBuffer[I] := ChangeCase(ATextBuffer[I]);
  PieceTable.ApplyChangesCore(ChangeActions, RunIndex, RunIndex);
end;

procedure TdxTextRunChangeCaseHistoryItem.UndoCore;
var
  ARun: TdxTextRunBase;
  ATextBuffer: TdxChunkedStringBuilder;
  I: Integer;
begin
  ARun := PieceTable.Runs[RunIndex];
  Assert(ARun.Length = Length(FOriginalText));
  ATextBuffer := PieceTable.TextBuffer;
  for I := 0 to Length(FOriginalText) - 1 do
    ATextBuffer[I + ARun.StartIndex] := FOriginalText[I];
  PieceTable.ApplyChangesCore(ChangeActions, RunIndex, RunIndex);
end;

{ TdxTextRunMakeLowerCaseHistoryItem }

function TdxTextRunMakeLowerCaseHistoryItem.ChangeCase(const AValue: WideChar): WideChar;
begin
{$IFDEF DELPHI18}
  Result := AValue.ToLower;
{$ELSE}
  Result := ToLower(AValue);
{$ENDIF}
end;

{ TdxTextRunToggleCaseHistoryItem }

function TdxTextRunToggleCaseHistoryItem.ChangeCase(const AValue: WideChar): WideChar;
begin
  Result := AValue;
{$IFDEF DELPHI18}
  if AValue.IsLower then
    Exit(AValue.ToUpper);
  if AValue.IsUpper then
    Exit(AValue.ToLower);
{$ELSE}
  if IsLower(AValue) then
    Exit(ToUpper(AValue));
  if IsUpper(AValue) then
    Exit(ToLower(AValue));
{$ENDIF}
end;

{ TdxSectionRunInsertedHistoryItem }

function TdxSectionRunInsertedHistoryItem.CreateRun(AParagraph: TdxParagraph): TdxTextRunBase;
begin
  Result := TdxSectionRun.Create(AParagraph);
end;

{ TdxCustomRunInsertedHistoryItem }

procedure TdxCustomRunInsertedHistoryItem.RedoCore;
begin
  NotImplemented;
end;

procedure TdxCustomRunInsertedHistoryItem.UndoCore;
begin
  PieceTable.Runs[RunIndex].BeforeRunRemoved;
  PieceTable.Runs.Delete(RunIndex);
  DocumentModel.ResetMerging;
  TdxDocumentModelStructureChangedNotifier.NotifyRunRemoved(PieceTable, PieceTable, ParagraphIndex, RunIndex, 1, FNotificationId);
end;

{ TdxDataContainerRunInsertedHistoryItem }

procedure TdxDataContainerRunInsertedHistoryItem.RedoCore;
begin
  NotImplemented;
end;

procedure TdxDataContainerRunInsertedHistoryItem.UndoCore;
begin
  PieceTable.Runs[RunIndex].BeforeRunRemoved;
  PieceTable.Runs.Delete(RunIndex);
  DocumentModel.ResetMerging;
  TdxDocumentModelStructureChangedNotifier.NotifyRunRemoved(PieceTable, PieceTable, ParagraphIndex, RunIndex, 1, FNotificationId);
end;

{ TdxAddAbstractNumberingListHistoryItem }

destructor TdxAddAbstractNumberingListHistoryItem.Destroy;
begin
  if FOwnedAbstractList then
    FreeAndNil(FAbstractList);
  inherited Destroy;
end;

procedure TdxAddAbstractNumberingListHistoryItem.UndoCore;
var
  ALists: TdxAbstractNumberingListCollection;
begin
  ALists := DocumentModel.AbstractNumberingLists;
  Assert(ALists.IndexOf(FAbstractList) = ALists.Count - 1);
  FAbstractList.Deleted := True;
  ALists.Extract(FAbstractList); 
  FOwnedAbstractList := True;
end;

procedure TdxAddAbstractNumberingListHistoryItem.RedoCore;
begin
  FAbstractList.Deleted := False;
  DocumentModel.AbstractNumberingLists.Add(FAbstractList);
  FOwnedAbstractList := False;
end;

{ TdxAddNumberingListHistoryItem }

destructor TdxAddNumberingListHistoryItem.Destroy;
begin
  if FOwnedNumberingList then
    FreeAndNil(FNumberingList);
  inherited Destroy;
end;

procedure TdxAddNumberingListHistoryItem.UndoCore;
var
  ALists: TdxNumberingListCollection;
begin
  ALists := DocumentModel.NumberingLists;
  Assert(ALists.IndexOf(FNumberingList) = ALists.Count - 1);
  ALists.Extract(FNumberingList); 
  FOwnedNumberingList := True;
end;

procedure TdxAddNumberingListHistoryItem.RedoCore;
begin
  DocumentModel.NumberingLists.Add(FNumberingList);
  FOwnedNumberingList := False;
end;

end.
