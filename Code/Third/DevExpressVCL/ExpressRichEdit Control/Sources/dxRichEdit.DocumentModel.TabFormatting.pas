{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.DocumentModel.TabFormatting;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Windows, Classes, Types, SysUtils, Generics.Collections, Generics.Defaults, dxCoreClasses,
  dxRichEdit.DocumentModel.UnitToLayoutUnitConverter, dxRichEdit.Utils.Types, dxRichEdit.Utils.BatchUpdateHelper,
  dxRichEdit.DocumentModel.IndexBasedObject, dxRichEdit.DocumentModel.UnitConverter,
  dxRichEdit.DocumentModel.ParagraphFormatting, dxRichEdit.DocumentModel.Core;

type
  TdxTabAlignmentType = (Left, Center, Right, Decimal);
  TdxTabLeaderType = (None, Dots, MiddleDots, Hyphens, Underline, ThickLine, EqualSign);

  { TdxTabInfo }

  TdxTabInfo = class(TcxIUnknownObject, IdxCloneable<TdxTabInfo>, IdxSupportsCopyFrom<TdxTabInfo>)
  const
    DefaultAlignment = TdxTabAlignmentType.Left;
    DefaultLeader = TdxTabLeaderType.None;
  private
    FAlignment: TdxTabAlignmentType;
    FDeleted: Boolean;
    FLeader: TdxTabLeaderType;
    FPosition: Integer;
  public
    constructor Create(APosition: Integer; AAlignment: TdxTabAlignmentType = TdxTabAlignmentType.Left;
      ALeader: TdxTabLeaderType = TdxTabLeaderType.None; ADeleted: Boolean = False);
    procedure CopyFrom(const Source: TdxTabInfo);

    function Clone: TdxTabInfo;
    function IsDefault: Boolean; virtual;

    function GetLayoutPosition(AUnitConverter: TdxDocumentModelUnitToLayoutUnitConverter): Integer;

    property Alignment: TdxTabAlignmentType read FAlignment;
    property Deleted: Boolean read FDeleted;
    property Leader: TdxTabLeaderType read FLeader;
    property Position: Integer read FPosition;
  end;
  TdxTabInfoClass = class of TdxTabInfo;

  { TdxTabInfoComparer }

  TdxTabInfoComparer = class(TcxIUnknownObject, IComparer<TdxTabInfo>)
  public
    function Compare(const X, Y: TdxTabInfo): Integer;
  end;

  { TdxTabFormattingInfo }

  TdxTabFormattingInfo = class(TcxIUnknownObject, IdxCloneable<TdxTabFormattingInfo>,
    IdxSupportsCopyFrom<TdxTabFormattingInfo>, IdxSupportsSizeOf)
  strict private
    class var FComparer: TdxTabInfoComparer;
    class constructor Initialize;
    class destructor Finalize;
  private
    FTabInfos: TObjectList<TdxTabInfo>;
    function GetItem(Index: Integer): TdxTabInfo;
    procedure SetItem(Index: Integer; const Value: TdxTabInfo);
    function GetCount: Integer;
  public
    constructor Create;
    destructor Destroy; override;

    function Add(AItem: TdxTabInfo): Integer;
    function IndexOf(AItem: TdxTabInfo): Integer;
    function Contains(AItem: TdxTabInfo): Boolean;
    procedure Remove(AItem: TdxTabInfo);
    procedure Clear;
    procedure AddRange(AFrom: TdxTabFormattingInfo);
    function FindTabItemIndexByPosition(APosition: Integer): Integer;

    function FindNextTab(APos: Integer): TdxTabInfo;
    function Clone: TdxTabFormattingInfo;
    procedure CopyFrom(const AInfo: TdxTabFormattingInfo);
    function Equals(Obj: TObject): Boolean; override;
    class function Merge(AMaster: TdxTabFormattingInfo; ASlave: TdxTabFormattingInfo): TdxTabFormattingInfo; static;
    class procedure MergeCore(AMaster, ASlave, AResult: TdxTabFormattingInfo); static;

    procedure OnChanged; virtual;
    property Items[Index: Integer]: TdxTabInfo read GetItem write SetItem; default;
    property Count: Integer read GetCount;
  end;

  { TdxTabFormattingInfoWithoutSort }

  TdxTabFormattingInfoWithoutSort = class(TdxTabFormattingInfo, IdxBatchUpdateable, IdxBatchUpdateHandler)
  private
    FBatchUpdateHelper: TdxBatchUpdateHelper;
    function GetIsUpdateLocked: Boolean;
    function GetBatchUpdateHelper: TdxBatchUpdateHelper;
  public
    constructor Create;
    destructor Destroy; override;

    procedure BeginUpdate;
    procedure EndUpdate;
    procedure CancelUpdate;
    procedure OnFirstBeginUpdate;
    procedure OnBeginUpdate;
    procedure OnEndUpdate;
    procedure OnLastEndUpdate;
    procedure OnCancelUpdate;
    procedure OnLastCancelUpdate;
    procedure OnChanged; override;

    property BatchUpdateHelper: TdxBatchUpdateHelper read FBatchUpdateHelper;
    property IsUpdateLocked: Boolean read GetIsUpdateLocked;
  end;

  { TdxDefaultTabInfo }

  TdxDefaultTabInfo = class(TdxTabInfo)
  public
    function IsDefault: Boolean; override;
  end;

  { TdxTabFormattingInfoCache }

  TdxTabFormattingInfoCache = class(TdxUniqueItemsCache<TdxTabFormattingInfo>)
  const
    EmptyTabFormattingOptionIndex = 0;
  protected
    function CreateDefaultItem(const AUnitConverter: IdxDocumentModelUnitConverter): TdxTabFormattingInfo; override;
  end;

  { TdxTabProperties }

  TdxTabProperties = class(TdxRichEditIndexBasedObject<TdxTabFormattingInfo>)
  private
    FOwner: IdxParagraphPropertiesContainer;
    function GetItem(Index: Integer): TdxTabInfo;
    procedure SetItem(Index: Integer; const Value: TdxTabInfo);
    function SuppressInsertTabs: Boolean;
  public
    constructor Create(const AOwner: IdxParagraphPropertiesContainer); reintroduce;
    class function GetPieceTable(const AOwner: IdxParagraphPropertiesContainer): TObject; static;

    procedure SetTabs(ATabs: TdxTabFormattingInfo);
    function GetTabs: TdxTabFormattingInfo;
    function GetCache(const ADocumentModel: TdxCustomDocumentModel): TdxUniqueItemsCache<TdxTabFormattingInfo>; override;
    procedure SetTabCore(AIndex: Integer; AValue: TdxTabInfo);
    function GetBatchUpdateChangeActions: TdxDocumentModelChangeActions; override;
    function GetObtainAffectedRangeListener: IdxObtainAffectedRangeListener; override;

    property Items[Index: Integer]: TdxTabInfo read GetItem write SetItem; default;
  end;

implementation

uses
  RTLConsts, 
  dxRichEdit.DocumentModel.PieceTable;

{ TdxTabInfo }

constructor TdxTabInfo.Create(APosition: Integer;
  AAlignment: TdxTabAlignmentType = TdxTabAlignmentType.Left;
  ALeader: TdxTabLeaderType = TdxTabLeaderType.None; ADeleted: Boolean = False);
begin
  inherited Create;
  FPosition := APosition;
  FAlignment := AAlignment;
  FLeader := ALeader;
  FDeleted := ADeleted;
end;

procedure TdxTabInfo.CopyFrom(const Source: TdxTabInfo);
begin
  if Source is TdxTabInfo then
  begin
    FPosition := Source.Position;
    FAlignment := Source.Alignment;
    FLeader := Source.Leader;
    FDeleted := Source.Deleted;
  end;
end;

function TdxTabInfo.Clone: TdxTabInfo;
begin
  Result := TdxTabInfoClass(ClassType).Create(Position, Alignment, Leader);
  Result.CopyFrom(Self);
end;

function TdxTabInfo.IsDefault: Boolean;
begin
  Result := (Alignment = TdxTabAlignmentType.Left) and (Leader = TdxTabLeaderType.None);
end;

function TdxTabInfo.GetLayoutPosition(AUnitConverter: TdxDocumentModelUnitToLayoutUnitConverter): Integer;
begin
  Result := AUnitConverter.ToLayoutUnits(Position);
end;

{ TdxTabFormattingInfo }

constructor TdxTabFormattingInfo.Create;
begin
  inherited Create;
  FTabInfos := TObjectList<TdxTabInfo>.Create;
end;

destructor TdxTabFormattingInfo.Destroy;
begin
  FreeAndNil(FTabInfos);
  inherited Destroy;
end;

function TdxTabFormattingInfo.Add(AItem: TdxTabInfo): Integer;
var
  AIndex: Integer;
begin
  AIndex := FindTabItemIndexByPosition(AItem.Position);
  if AIndex >= 0 then
  begin
    Items[AIndex] := AItem;
    Result := AIndex;
  end
  else
  begin
    FTabInfos.Add(AItem);
    OnChanged;
    Result := Count - 1;
  end;
end;

procedure TdxTabFormattingInfo.AddRange(AFrom: TdxTabFormattingInfo);
var
  I: Integer;
begin
  for I := 0 to AFrom.Count - 1 do
    Add(AFrom[I]);
  OnChanged;
end;

procedure TdxTabFormattingInfo.CopyFrom(const AInfo: TdxTabFormattingInfo);
var
  I: Integer;
begin
  FTabInfos.Clear;
  for I := 0 to AInfo.FTabInfos.Count - 1 do
    FTabInfos.Add(AInfo.FTabInfos[I].Clone);
end;

function TdxTabFormattingInfo.Equals(Obj: TObject): Boolean;
var
  AInfo: TdxTabFormattingInfo absolute Obj;
  I: Integer;
begin
  if Count <> AInfo.Count then
    Exit(False);
  for I := 0 to Count - 1 do
    if not Items[I].Equals(AInfo[I]) then
      Exit(False);
  Result := True;
end;

procedure TdxTabFormattingInfo.Clear;
begin
  FTabInfos.Clear;
end;

function TdxTabFormattingInfo.Clone: TdxTabFormattingInfo;
begin
  Result := TdxTabFormattingInfo.Create;
  Result.CopyFrom(Self);
end;

function TdxTabFormattingInfo.Contains(AItem: TdxTabInfo): Boolean;
begin
  Result := FTabInfos.Contains(AItem);
end;

class destructor TdxTabFormattingInfo.Finalize;
begin
  FComparer.Free;
end;

function TdxTabFormattingInfo.FindNextTab(APos: Integer): TdxTabInfo;
var
  I: Integer;
begin
  Result := nil;
  for I := 0 to Count - 1 do
    if Items[I].Position > APos then
    begin
      Result := Items[I];
      Break;
    end;
end;

function TdxTabFormattingInfo.FindTabItemIndexByPosition(APosition: Integer): Integer;
var
  I: Integer;
begin
  Result := -1;
  for I := 0 to Count - 1 do
    if Items[I].Position = APosition then
    begin
      Result := I;
      Break;
    end;
end;

function TdxTabFormattingInfo.GetCount: Integer;
begin
  Result := FTabInfos.Count;
end;

function TdxTabFormattingInfo.GetItem(Index: Integer): TdxTabInfo;
begin
  Result := FTabInfos[Index];
end;

function TdxTabFormattingInfo.IndexOf(AItem: TdxTabInfo): Integer;
begin
  Result := FTabInfos.IndexOf(AItem);
end;

class constructor TdxTabFormattingInfo.Initialize;
begin
  FComparer := TdxTabInfoComparer.Create;
end;

class function TdxTabFormattingInfo.Merge(AMaster, ASlave: TdxTabFormattingInfo): TdxTabFormattingInfo;
var
  AResult: TdxTabFormattingInfoWithoutSort; 
begin
  AResult := TdxTabFormattingInfoWithoutSort.Create;
  AResult.BeginUpdate;
  try
    MergeCore(AMaster, ASlave, AResult);
  finally
    AResult.EndUpdate;
  end;
  Result := AResult;
end;

class procedure TdxTabFormattingInfo.MergeCore(AMaster, ASlave, AResult: TdxTabFormattingInfo);
var
  I: Integer;
begin
  for I := 0 to ASlave.Count - 1 do
    if not ASlave[I].Deleted then
      AResult.Add(ASlave[I].Clone);
  for I := 0 to AMaster.Count - 1 do
    AResult.Add(AMaster[I].Clone);
  for I := AResult.Count - 1 downto 0 do
    if AResult[I].Deleted then
      AResult.FTabInfos.Delete(I);
end;

procedure TdxTabFormattingInfo.OnChanged;
begin
  FTabInfos.Sort(FComparer);
end;

procedure TdxTabFormattingInfo.Remove(AItem: TdxTabInfo);
var
  AIndex: Integer;
begin
  AIndex := IndexOf(AItem);
  if AIndex >= 0 then
    FTabInfos.Delete(AIndex);
end;

procedure TdxTabFormattingInfo.SetItem(Index: Integer; const Value: TdxTabInfo);
begin
  Assert(Value <> nil);
  FTabInfos[Index] := Value;
  OnChanged;
end;

{ TdxTabFormattingInfoWithoutSort }

constructor TdxTabFormattingInfoWithoutSort.Create;
begin
  inherited Create;
  FBatchUpdateHelper := TdxBatchUpdateHelper.Create(Self);
end;

destructor TdxTabFormattingInfoWithoutSort.Destroy;
begin
  FreeAndNil(FBatchUpdateHelper);
  inherited Destroy;
end;

procedure TdxTabFormattingInfoWithoutSort.BeginUpdate;
begin
  FBatchUpdateHelper.BeginUpdate;
end;

procedure TdxTabFormattingInfoWithoutSort.CancelUpdate;
begin
  FBatchUpdateHelper.CancelUpdate;
end;

procedure TdxTabFormattingInfoWithoutSort.EndUpdate;
begin
  FBatchUpdateHelper.EndUpdate;
end;

function TdxTabFormattingInfoWithoutSort.GetBatchUpdateHelper: TdxBatchUpdateHelper;
begin
  Result := FBatchUpdateHelper;
end;

function TdxTabFormattingInfoWithoutSort.GetIsUpdateLocked: Boolean;
begin
  Result := FBatchUpdateHelper.IsUpdateLocked;
end;

procedure TdxTabFormattingInfoWithoutSort.OnBeginUpdate;
begin
  // do nothing
end;

procedure TdxTabFormattingInfoWithoutSort.OnCancelUpdate;
begin
  // do nothing
end;

procedure TdxTabFormattingInfoWithoutSort.OnChanged;
begin
  if not IsUpdateLocked then
    inherited OnChanged;
end;

procedure TdxTabFormattingInfoWithoutSort.OnEndUpdate;
begin
  // do nothing
end;

procedure TdxTabFormattingInfoWithoutSort.OnFirstBeginUpdate;
begin
end;

procedure TdxTabFormattingInfoWithoutSort.OnLastCancelUpdate;
begin
end;

procedure TdxTabFormattingInfoWithoutSort.OnLastEndUpdate;
begin
  OnChanged;
end;

{ TdxTabInfoComparer }

function TdxTabInfoComparer.Compare(const X, Y: TdxTabInfo): Integer;
begin
  Result := X.Position - Y.Position;
end;

{ TdxDefaultTabInfo }

function TdxDefaultTabInfo.IsDefault: Boolean;
begin
  Result := True;
end;

{ TdxTabFormattingInfoCache }

function TdxTabFormattingInfoCache.CreateDefaultItem(const AUnitConverter: IdxDocumentModelUnitConverter): TdxTabFormattingInfo;
begin
  Result := TdxTabFormattingInfo.Create;
end;

{ TdxTabProperties }

constructor TdxTabProperties.Create(const AOwner: IdxParagraphPropertiesContainer);
begin
  inherited Create(TdxPieceTable(GetPieceTable(AOwner)));
  FOwner := AOwner;
end;

class function TdxTabProperties.GetPieceTable(const AOwner: IdxParagraphPropertiesContainer): TObject;
begin
  Assert(AOwner <> nil);
  Result := AOwner.PieceTable as TdxPieceTable;
end;

procedure TdxTabProperties.SetTabs(ATabs: TdxTabFormattingInfo);
begin
  if SuppressInsertTabs then
    Exit;
  if (Info.Count = 0) and (ATabs.Count = 0) then
    Exit;
  ReplaceInfo(ATabs, GetBatchUpdateChangeActions);
end;

function TdxTabProperties.GetTabs: TdxTabFormattingInfo;
begin
  Result := Info.Clone;
end;

function TdxTabProperties.GetCache(const ADocumentModel: TdxCustomDocumentModel): TdxUniqueItemsCache<TdxTabFormattingInfo>;
begin
  Result := TdxDocumentModel(ADocumentModel).Cache.TabFormattingInfoCache;
end;

procedure TdxTabProperties.SetTabCore(AIndex: Integer; AValue: TdxTabInfo);
var
  ANewInfo: TdxTabFormattingInfo;
  AIsDeferred: Boolean;
begin
  ANewInfo := GetInfoForModification(AIsDeferred);
  ANewInfo[AIndex] := AValue;
  ReplaceInfo(ANewInfo, GetBatchUpdateChangeActions);
  if not AIsDeferred then ANewInfo.Free;
end;

function TdxTabProperties.GetBatchUpdateChangeActions: TdxDocumentModelChangeActions;
begin
  Result := [TdxDocumentModelChangeAction.Redraw,
    TdxDocumentModelChangeAction.ResetPrimaryLayout,
    TdxDocumentModelChangeAction.ResetSecondaryLayout,
    TdxDocumentModelChangeAction.RaiseContentChanged,
    TdxDocumentModelChangeAction.ForceResetHorizontalRuler];
end;

function TdxTabProperties.GetObtainAffectedRangeListener: IdxObtainAffectedRangeListener;
begin
  if not Supports(FOwner, IdxObtainAffectedRangeListener, Result) then
    Result := nil;
end;

function TdxTabProperties.SuppressInsertTabs: Boolean;
begin
  Result := not TdxDocumentModel(DocumentModel).DocumentCapabilities.ParagraphTabsAllowed;
end;

function TdxTabProperties.GetItem(Index: Integer): TdxTabInfo;
begin
  Result := Info[Index];
end;

procedure TdxTabProperties.SetItem(Index: Integer; const Value: TdxTabInfo);
begin
  Assert(Value <> nil); 
  if Info[index] = Value then
    Exit;
  if SuppressInsertTabs then
    Exit;
  SetTabCore(Index, Value);
end;

end.
