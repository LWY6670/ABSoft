{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Import.Rtf.ListConverter;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  dxRichEdit.Import.Rtf, dxRichEdit.DocumentModel.PieceTable, dxRichEdit.DocumentModel.Numbering,
  Generics.Collections;

type

  { TdxRtfListConverter }

  TdxRtfListConverter = class
  private
    FStyleCrossTable: TDictionary<TdxRtfListId, Integer>;
    FImporter: TdxRichEditDocumentModelRtfImporter;
    function GetDocumentModel: TdxDocumentModel;
    procedure CreateListStyles(AListTable: TdxRtfListTable);
    procedure CreateNumberingListsCore(AListOverrideTable: TdxListOverrideTable; AListTable: TdxRtfListTable);
    procedure CreateAbstractNumberingLists(AListTable: TdxRtfListTable);
    procedure CreateAbstractNumberingList(ARtfList: TdxRtfNumberingList);
    class procedure ConvertPropertyRtfToNumbering(ARtfLevel: TdxRtfListLevel; ALevel: IdxListLevelProperties; ARestart, AReformat: Boolean); static;
  protected
    function IsHybridList(ARtfList: TdxRtfNumberingList): Boolean; virtual;
    function GetStyleIndex(ARtfParentStyleId: TdxRtfListId): Integer;
    function GetListIndex(AListId: Integer; ALists: TdxAbstractNumberingListCollection): TdxAbstractNumberingListIndex;
    function IsStyleList(AIndex: TdxAbstractNumberingListIndex): Boolean;
    procedure ConvertRtfListToNumberingList(ARtfLevels: TList<TdxRtfListLevel>; AList: TdxAbstractNumberingList); virtual;

  public
    constructor Create(AImporter: TdxRichEditDocumentModelRtfImporter);
    destructor Destroy; override;
    procedure Convert(AListTable: TdxRtfListTable; AListOverrideTable: TdxListOverrideTable); virtual;
    procedure ConvertRtfOverrideToNumbering(AList: TdxNumberingList; ARtfOverride: TdxRtfNumberingListOverride);

    property DocumentModel: TdxDocumentModel read GetDocumentModel;
    property Importer: TdxRichEditDocumentModelRtfImporter read FImporter;
  end;

implementation

uses
  dxRichEdit.DocumentModel.ParagraphFormatting, SysUtils;

{ TdxRtfListConverter }

constructor TdxRtfListConverter.Create(AImporter: TdxRichEditDocumentModelRtfImporter);
begin
  inherited Create;
  FImporter := AImporter;
  FStyleCrossTable := TDictionary<TdxRtfListId, Integer>.Create;
end;

destructor TdxRtfListConverter.Destroy;
begin
  FreeAndNil(FStyleCrossTable);
  inherited Destroy;
end;

procedure TdxRtfListConverter.Convert(AListTable: TdxRtfListTable; AListOverrideTable: TdxListOverrideTable);
begin
  CreateListStyles(AListTable);
  CreateAbstractNumberingLists(AListTable);
  CreateNumberingListsCore(AListOverrideTable, AListTable);
end;

procedure TdxRtfListConverter.CreateListStyles(AListTable: TdxRtfListTable);
var
  I, ACount: Integer;
  ARtfList: TdxRtfNumberingList;
  ANumberingList: TdxNumberingList;
  AAbstractNumberingListIndex: TdxAbstractNumberingListIndex;
  ANumberingListIndex: TdxNumberingListIndex;
  AStyle: TdxNumberingListStyle;
begin
  ACount := AListTable.Count;
  for I := 0 to ACount - 1 do
  begin
    ARtfList := AListTable[I];
    if ARtfList.StyleName <> '' then 
    begin
      CreateAbstractNumberingList(ARtfList);
      AAbstractNumberingListIndex := DocumentModel.AbstractNumberingLists.Count - 1;
      ANumberingList := TdxNumberingList.Create(DocumentModel, AAbstractNumberingListIndex);
      DocumentModel.AddNumberingListUsingHistory(ANumberingList);
      ANumberingList.SetId(DocumentModel.NumberingLists.Count);
      ANumberingListIndex := DocumentModel.NumberingLists.Count - 1;
      AStyle := TdxNumberingListStyle.Create(DocumentModel, ANumberingListIndex, ARtfList.StyleName);
      DocumentModel.NumberingListStyles.Add(AStyle);
    end;
  end;
end;

procedure TdxRtfListConverter.CreateNumberingListsCore(AListOverrideTable: TdxListOverrideTable; AListTable: TdxRtfListTable);
var
  I, ACount, AOverrideId: Integer;
  AAbstractNumberingLists: TdxAbstractNumberingListCollection;
  ARtfList: TdxRtfNumberingListOverride;
  ASourceListIndex: TdxAbstractNumberingListIndex;
  AList: TdxNumberingList;
  ANumberingListIndex: TdxNumberingListIndex;
begin
  ACount := AListOverrideTable.Count;
  AAbstractNumberingLists := DocumentModel.AbstractNumberingLists;
  for I := 0 to ACount - 1 do
  begin
    ARtfList := AListOverrideTable[I];
    ASourceListIndex := GetListIndex(ARtfList.ListId, AAbstractNumberingLists);
    if ASourceListIndex < 0 then
      Continue;
    if not IsStyleList(ASourceListIndex) then
    begin
      AList := TdxNumberingList.Create(DocumentModel, ASourceListIndex);
      AOverrideId := ARtfList.Id;
      DocumentModel.AddNumberingListUsingHistory(AList);
      ConvertRtfOverrideToNumbering(AList, ARtfList);
      ANumberingListIndex := DocumentModel.NumberingLists.Count - 1;
      Importer.ListOverrideIndexToNumberingListIndexMap.AddOrSetValue(AOverrideId, ANumberingListIndex);
      AList.SetId(DocumentModel.NumberingLists.Count);
    end;
  end;
end;

procedure TdxRtfListConverter.CreateAbstractNumberingLists(AListTable: TdxRtfListTable);
var
  I, ACount: Integer;
  ARtfList: TdxRtfNumberingList;
begin
  ACount := AListTable.Count;
  for I := 0 to ACount - 1 do
  begin
    ARtfList := AListTable[I];
    if ARtfList.StyleName = '' then
      CreateAbstractNumberingList(ARtfList);
  end;
end;

procedure TdxRtfListConverter.CreateAbstractNumberingList(ARtfList: TdxRtfNumberingList);
var
  AList: TdxAbstractNumberingList;
begin
  if ARtfList.ParentStyleId = 0 then
    AList := TdxAbstractNumberingList.Create(DocumentModel)
  else
    AList := TdxAbstractStyleNumberingList.Create(DocumentModel, GetStyleIndex(ARtfList.ParentStyleId));
  ConvertRtfListToNumberingList(ARtfList.Levels, AList);
  if IsHybridList(ARtfList) then
    TdxNumberingListHelper.SetHybridListType(AList);
  AList.SetId(ARtfList.Id);
  DocumentModel.AddAbstractNumberingListUsingHistory(AList);
end;

function TdxRtfListConverter.IsHybridList(ARtfList: TdxRtfNumberingList): Boolean;
var
  ALevels: TList<TdxRtfListLevel>;
  I, ACount: Integer;
begin
  if ARtfList.NumberingListType <> TdxRtfNumberingListType.Unknown then
    Exit(True);
  ALevels := ARtfList.Levels;
  ACount := ALevels.Count;
  for I := 0 to ACount - 1 do
    if ALevels[I].ListLevelProperties.TemplateCode <> 0 then
      Exit(True);
  Result := False;
end;

function TdxRtfListConverter.GetStyleIndex(ARtfParentStyleId: TdxRtfListId): Integer;
begin
  if not FStyleCrossTable.TryGetValue(ARtfParentStyleId, Result) then
    Result := 0;
end;

function TdxRtfListConverter.GetListIndex(AListId: Integer; ALists: TdxAbstractNumberingListCollection): TdxAbstractNumberingListIndex;
var
  I, ACount: TdxAbstractNumberingListIndex;
begin
  ACount := ALists.Count;
  for I := 0 to ACount - 1 do
  begin
    if ALists[I].GetId = AListId then
      Exit(I);
  end;
  Result := -1;
end;

function TdxRtfListConverter.IsStyleList(AIndex: TdxAbstractNumberingListIndex): Boolean;
var
  AStyles: TdxNumberingListStyleCollection;
  I, ACount: Integer;
begin
  AStyles := DocumentModel.NumberingListStyles;
  ACount := AStyles.Count;
  for I := 0 to ACount - 1 do
  begin
    if AStyles[I].NumberingList.AbstractNumberingListIndex = AIndex then
      Exit(True);
  end;
  Result := False;
end;

procedure TdxRtfListConverter.ConvertRtfOverrideToNumbering(AList: TdxNumberingList; ARtfOverride: TdxRtfNumberingListOverride);
var
  I, ACount: Integer;
  ARestart, AReformat: Boolean;
  ALevel: TdxOverrideListLevel;
  AReferenceLevel: TdxNumberingListReferenceLevel;
begin
  ACount := ARtfOverride.Levels.Count;
  for I := 0 to ACount - 1 do
  begin
    ARestart := ARtfOverride.Levels[I].OverrideStartAt;
    AReformat := ARtfOverride.Levels[I].OverrideFormat;
    if AReformat then
    begin
      ALevel := TdxOverrideListLevel.Create(DocumentModel);
      ALevel.ListLevelProperties.BeginInit;
      try
        ALevel.ListLevelProperties.CopyFrom(AList.Levels[I].ListLevelProperties);
        ALevel.CharacterProperties.CopyFrom(ARtfOverride.Levels[I].Level.CharacterProperties);
        ALevel.ParagraphProperties.CopyFrom(ARtfOverride.Levels[I].Level.ParagraphProperties);
        ConvertPropertyRtfToNumbering(ARtfOverride.Levels[I].Level, ALevel.ListLevelProperties, ARestart, AReformat);
      finally
        ALevel.ListLevelProperties.EndInit;
      end;
      AList.SetLevel(I, ALevel);
      if ARestart then
        ALevel.SetOverrideStart(True);
    end
    else
    begin
      if ARestart then
      begin
        AReferenceLevel := TdxNumberingListReferenceLevel(AList.Levels[I]);
        AReferenceLevel.SetOverrideStart(True);
        AReferenceLevel.NewStart := ARtfOverride.Levels[I].StartAt;
      end;
    end;
  end;
end;

procedure TdxRtfListConverter.ConvertRtfListToNumberingList(ARtfLevels: TList<TdxRtfListLevel>; AList: TdxAbstractNumberingList);
var
  I, ACount: Integer;
  ALevel: TdxListLevel;
begin
  ACount := ARtfLevels.Count;
  for I := 0 to ACount - 1 do
  begin
    ALevel := AList.Levels[I] as TdxListLevel;
    ALevel.ParagraphProperties.CopyFrom(ARtfLevels[I].ParagraphProperties);
    ALevel.CharacterProperties.CopyFrom(ARtfLevels[I].CharacterProperties);
    ALevel.ParagraphStyleIndex := ARtfLevels[I].ParagraphStyleIndex;
    ConvertPropertyRtfToNumbering(ARtfLevels[I], ALevel.ListLevelProperties, True, True);
  end;
end;

class procedure TdxRtfListConverter.ConvertPropertyRtfToNumbering(ARtfLevel: TdxRtfListLevel; ALevel: IdxListLevelProperties; ARestart, AReformat: Boolean);
var
  ALevelProperties: TdxListLevelProperties;
begin
  ALevelProperties := ARtfLevel.ListLevelProperties;
  if ARestart then
    ALevel.Start := ALevelProperties.Start;
  if AReformat then
  begin
    ALevel.Format := ALevelProperties.Format;
    ALevel.Alignment := ALevelProperties.Alignment;
    ALevel.SuppressBulletResize := ALevelProperties.SuppressBulletResize;
    ALevel.SuppressRestart := ALevelProperties.SuppressRestart;
    ALevel.Separator := ALevelProperties.Separator;
    ALevel.ConvertPreviousLevelNumberingToDecimal := ALevelProperties.ConvertPreviousLevelNumberingToDecimal;
    ALevel.DisplayFormatString := ARtfLevel.CreateDisplayFormatString;
    ALevel.TemplateCode := ALevelProperties.TemplateCode;
  end;
  if ALevelProperties.Legacy then
  begin
    ALevel.Legacy := ALevelProperties.Legacy;
    ALevel.LegacySpace := ALevelProperties.LegacySpace;
    ALevel.LegacyIndent := ALevelProperties.LegacyIndent;
  end;
end;

function TdxRtfListConverter.GetDocumentModel: TdxDocumentModel;
begin
  Result := Importer.DocumentModel;
end;

end.

