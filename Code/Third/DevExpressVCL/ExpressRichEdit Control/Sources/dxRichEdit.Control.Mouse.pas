{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Control.Mouse;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Types, Classes, Controls, SysUtils, Generics.Collections, cxClasses, ActiveX, dxRichEdit.Options,
  dxCoreClasses, dxRichEdit.DocumentModel.PieceTable, dxRichEdit.DocumentModel.PieceTableIterators, dxRichEdit.Utils.DataObject,
  dxRichEdit.View.Core, dxRichEdit.Control.HitTest, dxRichEdit.Control.HotZones, dxRichEdit.Commands,
  dxRichEdit.Control.Keyboard, dxRichEdit.Utils.OfficeImage, dxRichEdit.View.ViewInfo,
  dxRichEdit.Control.DragAndDrop.Types, dxRichEdit.Commands.Selection,
  dxRichEdit.Control.Mouse.AutoScroller, dxRichEdit.Control.Mouse.Types;

type
  TdxRichEditMouseCustomState = class;
  TdxRichEditMouseController = class;
  TdxRichEditMouseHandler = TdxRichEditMouseController; 

  IdxEndDocumentUpdateHandler = interface
  ['{690CD121-1D0A-4822-89B9-482A48CB7C35}']
    procedure HandleEndDocumentUpdate(const Args: TdxDocumentUpdateCompleteEventArgs);
  end;

  { IdxRichEditMouseState }

  IdxRichEditMouseState = interface
  ['{119E3746-78CD-4F20-9502-7B6B3FD6E405}']
    procedure HandleLongMouseDown;
    procedure HandleMouseDown(const Args: TdxMouseEventArgs);
    procedure HandleMouseDoubleClick(const Args: TdxMouseEventArgs);
    procedure HandleMouseTripleClick(const Args: TdxMouseEventArgs);
    procedure HandleMouseMove(const Args: TdxMouseEventArgs);
    procedure HandleMouseUp(const Args: TdxMouseEventArgs);
    procedure HandleMouseWheel(const Args: TdxMouseEventArgs);

    procedure DoDragEnter(Args: PdxDragEventArgs);
    procedure DoDragOver(Args: PdxDragEventArgs);
    procedure DoDragDrop(Args: PdxDragEventArgs);
    procedure DoDragLeave;
    procedure GiveFeedback(Args: PdxGiveFeedbackEventArgs);
    procedure QueryContinueDrag(Args: PdxQueryContinueDragEventArgs);

    function GetState: TdxRichEditMouseCustomState;

    property State: TdxRichEditMouseCustomState read GetState;
  end;

  { TdxRichEditMouseCustomState }

  TdxRichEditMouseCustomState = class abstract(TInterfacedObject, IdxRichEditMouseState) 
  private
    FController: TdxRichEditMouseController;
    FIsFinished: Boolean;
    function GetActivePieceTable: TdxPieceTable;
    function GetControl: IdxRichEditControl;
    function GetDocumentModel: TdxDocumentModel;
    function GetHighDetailLevelBox(AHitTestResult: TdxRichEditHitTestResult): TdxBox;
    function GetInnerControl: IdxInnerControl;
  protected
    function CalculateActiveObject(AHitTestResult: TdxRichEditHitTestResult): TObject; virtual;
    function CalculateActiveObjectCore(ARunIndex: Integer): TObject; virtual;
    function CalculateHitTest(const P: TPoint): TdxRichEditHitTestResult; virtual;

    procedure HandleMouseMoveCore(const Args: TdxMouseEventArgs; const P: TPoint; AHitTestResult: TdxRichEditHitTestResult); virtual;

    function AutoScrollEnabled: Boolean; virtual;
    function CanShowToolTip: Boolean; virtual;
    function StopClickTimerOnStart: Boolean; virtual;

    function GetTopLevelPage(AInitialHitTestResult: TdxRichEditHitTestResult): TdxPage;

    procedure SetMouseCursor(ACursor: TCursor); virtual;

    procedure UpdateHover(AHitTestResult: TdxRichEditHitTestResult); virtual;
    procedure UpdateHoverLayout(AHitTestResult: TdxRichEditHitTestResult;
      AActiveView: TdxRichEditView; ABoxChanged: Boolean); virtual;
    function UseHover: Boolean; virtual;

    procedure LockRelease;
    procedure UnlockRelease;

    procedure DoDragOver(Args: PdxDragEventArgs); virtual;
    procedure DoDragDrop(Args: PdxDragEventArgs); virtual;
    procedure DoDragEnter(Args: PdxDragEventArgs); virtual;
    procedure DoDragLeave; virtual;
    procedure GiveFeedback(Args: PdxGiveFeedbackEventArgs); virtual;
    procedure QueryContinueDrag(Args: PdxQueryContinueDragEventArgs); virtual;

    function GetState: TdxRichEditMouseCustomState;

    property ActivePieceTable: TdxPieceTable read GetActivePieceTable;
    property Controller: TdxRichEditMouseController read FController;
    property DocumentModel: TdxDocumentModel read GetDocumentModel;
    property InnerControl: IdxInnerControl read GetInnerControl;
  public
    constructor Create(AController: TdxRichEditMouseController); virtual;
    destructor Destroy; override;

    procedure Finish; virtual;
    procedure Start; virtual;

    procedure ContinueSelection(const Args: TdxMouseEventArgs); virtual;
    function SuppressDefaultMouseWheelProcessing: Boolean;

    procedure HandleLongMouseDown; virtual;
    procedure HandleMouseDown(const Args: TdxMouseEventArgs); virtual;
    procedure HandleMouseDoubleClick(const Args: TdxMouseEventArgs); virtual;
    procedure HandleMouseTripleClick(const Args: TdxMouseEventArgs); virtual;
    procedure HandleMouseMove(const Args: TdxMouseEventArgs); virtual;
    procedure HandleMouseUp(const Args: TdxMouseEventArgs); virtual;
    procedure HandleMouseWheel(const Args: TdxMouseEventArgs); virtual;


    property Control: IdxRichEditControl read GetControl;
  end;
  TdxRichEditMouseCustomStateClass = class of TdxRichEditMouseCustomState;

  TdxRichEditMouseStates = TObjectList<TdxRichEditMouseCustomState>;

  { TdxRichEditMouseController }

  TdxRichEditMouseController = class(TdxRichEditMouseCustomController) 
  private
    FActiveObject: TObject;
    FAutoScroller: TdxRichEditAutoScroller;
    FClickCount: Integer;
    FClickScreenPoint: TPoint;
    FSuspended: Boolean;
    FTimer: TcxTimer;
    FHitInfos: TdxMouseEventArgsArray;
    FControl: IdxRichEditControl;
    FState: IdxRichEditMouseState;
    FLastDragPoint: TPoint;
    FLastDragDropEffects: TdxDragDropEffects;

    function GetActiveView: TdxRichEditView;
    function GetButtonsFromShiftState(Shift: TShiftState): TdxMouseButtons;
    function GetDocumentModel: TdxDocumentModel;
    function GetHitInfo: TdxMouseEventArgs;
    function GetInnerControl: IdxInnerControl;
    function GetState: TdxRichEditMouseCustomState;
  protected
    procedure ChangeActivePieceTable(APieceTable: TdxPieceTable); overload; virtual; 
    procedure ChangeActivePieceTable(APieceTable: TdxPieceTable; AHitTestResult: TdxRichEditHitTestResult); overload; virtual; 
    function CreateDragFloatingObjectMouseHandlerStateCalculator: TObject; virtual;
    function GetDefaultStateClass: TdxRichEditMouseCustomStateClass; virtual;
    procedure SetActiveObject(Value: TObject); virtual;

    function DeactivateTextBoxPieceTableIfNeed(APieceTable: TdxPieceTable;
      AHitTestResult: TdxRichEditHitTestResult): Boolean;
    function IsInlinePictureBoxHit(AHitTestResult: TdxRichEditHitTestResult): Boolean;

    procedure ClickTimerHandler(Sender: TObject);
    function IsClickTimerActive: Boolean;
    procedure HandleClickTimer; virtual;
    procedure StartClickTimer;
    procedure StopClickTimer;

    procedure ClearHitInfo;
    procedure SaveHitInfo(const Args: TdxMouseEventArgs);

    procedure SwitchToDefaultState;
    procedure SwitchStateCore(ANewState: TdxRichEditMouseCustomState; const AMousePosition: TPoint);

    procedure CalculateAndSaveHitInfo(const Args: TdxMouseEventArgs); virtual;
    function CreateDragContentMouseHandlerStateCalculator: TObject; virtual;
    function CreateInternalDragState: TdxRichEditMouseCustomState; virtual;
    function ConvertMouseEventArgs(const Args: TdxMouseEventArgs): TdxMouseEventArgs;
    procedure ConvertMouseDragEventArgs(Args: PdxDragEventArgs);
    function GetMouseEventArgs(Buttons: TdxMouseButtons; Shift: TShiftState;
      const AMousePos: TPoint; AWheelDelta: Integer): TdxMouseEventArgs; overload;
    function GetMouseEventArgs(Button: TMouseButton; Shift: TShiftState;
      X, Y: Integer): TdxMouseEventArgs; overload;
    function GetMouseEventArgs(Shift: TShiftState; X, Y: Integer): TdxMouseEventArgs; overload;

    function GetDoubleClickSize: TSize;
    function GetDoubleClickTime: Integer;

    function GetPhysicalPoint(const P: TPoint): TPoint;
    function IsDoubleClick(Args: TdxMouseEventArgs): Boolean;
    function IsMultipleClickCore(Args: TdxMouseEventArgs): Boolean;
    function IsTripleClick(Args: TdxMouseEventArgs): Boolean;
    function SupportsTripleClick: Boolean; virtual;

    function IsAltPressed: Boolean;
    function IsControlPressed: Boolean;
    function IsShiftPressed: Boolean;

    function CalculateMouseWheelScrollRate(AWheelDelta: Integer): Single;
    procedure PerformWheelScroll(const Args: TdxMouseEventArgs); virtual;
    procedure PerformWheelZoom(const Args: TdxMouseEventArgs);
    procedure SmallScrollDown(AWheelDelta: Integer); virtual;
    procedure SmallScrollUp(AWheelDelta: Integer); virtual;
    procedure SmallScrollLeft(AWheelDelta: Integer); virtual;
    procedure SmallScrollRight(AWheelDelta: Integer); virtual;
    procedure SmallScrollVerticallyCore(AScrollRate: Single); virtual;
    procedure SmallScrollHorizontallyCore(AScrollRate: Single); virtual;

    procedure HandleMouseDoubleClick(const Args: TdxMouseEventArgs);
    procedure HandleMouseDown(const Args: TdxMouseEventArgs);
    procedure HandleMouseTripleClick(const Args: TdxMouseEventArgs);
    procedure HandleMouseMove(const Args: TdxMouseEventArgs);
    procedure HandleMouseUp(const Args: TdxMouseEventArgs);
    procedure HandleMouseWheel(const Args: TdxMouseEventArgs);

    property ClickCount: Integer read FClickCount;
    property ClickScreenPoint: TPoint read FClickScreenPoint;
    property Timer: TcxTimer read FTimer;

    property ActiveView: TdxRichEditView read GetActiveView;
    property AutoScroller: TdxRichEditAutoScroller read FAutoScroller;
    property DocumentModel: TdxDocumentModel read GetDocumentModel;
    property HitInfo: TdxMouseEventArgs read GetHitInfo;
    property InnerControl: IdxInnerControl read GetInnerControl;
    property Suspended: Boolean read FSuspended;
  public
    constructor Create(ARichControl: IdxRichEditControl); reintroduce; virtual;
    destructor Destroy; override;

    function CreateFakeMouseMoveEventArgs: TdxMouseEventArgs;

    function MouseActivate(Button: TMouseButton; Shift: TShiftState;
      X, Y: Integer; HitTest: Integer): TMouseActivate; override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState;
      X, Y: Integer); override;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState;
      X, Y: Integer); override;
    function MouseWheel(Shift: TShiftState; WheelDelta: Integer;
      MousePos: TPoint): Boolean; override;

    procedure DoDragEnter(Args: PdxDragEventArgs); override;
    procedure DoDragOver(Args: PdxDragEventArgs); override;
    procedure DoDragDrop(Args: PdxDragEventArgs); override;
    procedure DoDragLeave; override;
    procedure GiveFeedback(Args: PdxGiveFeedbackEventArgs); override;
    procedure QueryContinueDrag(Args: PdxQueryContinueDragEventArgs); override;

    property ActiveObject: TObject read FActiveObject;
    property Control: IdxRichEditControl read FControl;
    property State: TdxRichEditMouseCustomState read GetState;
  end;

  { TdxRichEditMouseDefaultState }

  TdxRichEditMouseDefaultState = class(TdxRichEditMouseCustomState)
  private
    procedure BeginCharacterSelection(AHitTestResult: TdxRichEditHitTestResult);
    procedure BeginCharacterSelectionCore(AHitTestResult: TdxRichEditHitTestResult);
    procedure BeginDragExistingSelection(AHitTestResult: TdxRichEditHitTestResult; AResetSelectionOnMouseUp: Boolean);
    procedure BeginLineSelection(AHitTestResult: TdxRichEditHitTestResult);
    procedure BeginMultiSelection(AHitTestResult: TdxRichEditHitTestResult);
    procedure BeginParagraphsSelection(AHitTestResult: TdxRichEditHitTestResult);
    procedure BeginTableRowsSelectionState(AHitTestResult: TdxRichEditHitTestResult);
    procedure BeginWordsSelection(AHitTestResult: TdxRichEditHitTestResult);
    function CalculateExactPageAreaHitTest(const P: TPoint): TdxRichEditHitTestResult;
    procedure ClearMultiSelection;
    procedure DeactivateFloatingObject(AHitTestResult: TdxRichEditHitTestResult);
    procedure ExtendSelectionToCursor(AHitTestResult: TdxRichEditHitTestResult);
    procedure PerformEnhancedSelection(AHitTestResult: TdxRichEditHitTestResult);
    procedure SelectFloatingObject(AHitTestResult: TdxRichEditHitTestResult; AllowDrag: Boolean);
    procedure SelectPicture(AHitTestResult: TdxRichEditHitTestResult);
    procedure SetSelection(AStart, AEnd: TdxDocumentLogPosition);
    function ShouldBeginDragExistingSelection(AHitTestResult: TdxRichEditHitTestResult): Boolean;
    function ShouldDeactivateFloatingObject(AHitTestResult: TdxRichEditHitTestResult): Boolean;
    function ShouldSelectFloatingObject(AHitTestResult: TdxRichEditHitTestResult): Boolean;
    function ShouldSelectPicture(AHitTestResult: TdxRichEditHitTestResult): Boolean;
    function ShouldStartMultiSelection: Boolean;
    function ShouldSwitchActivePieceTable(AHitTestResult: TdxRichEditHitTestResult): Boolean;
    function ShouldExtendSelectionToCursor: Boolean;
    function TableMouseDown(ASelectionManager: TdxEnhancedSelectionManager;
      const AHitTestResult: TdxRichEditHitTestResult): Boolean;
    function TryProcessHyperlinkClick(AHitTestResult: TdxRichEditHitTestResult): Boolean;
    procedure ValidateFieldSelection(ADocumentModel: TdxDocumentModel; ASelection: TdxSelection);
  protected
    procedure HandleMouseMoveCore(const Args: TdxMouseEventArgs; const P: TPoint;
      AHitTestResult: TdxRichEditHitTestResult); override;

    function AutoScrollEnabled: Boolean; override;
    function CanShowToolTip: Boolean; override;
    function IsHyperlinkActive: Boolean; virtual;
    function StopClickTimerOnStart: Boolean; override;

    function CreateDragState(const ADataObject: IDataObject): TdxRichEditMouseCustomState; virtual;
    function CreateDragContentState(AHitTestResult: TdxRichEditHitTestResult): TdxRichEditMouseCustomState; virtual;
    function CreateExternalDragState: TdxRichEditMouseCustomState; virtual;
    function CreateInternalDragState: TdxRichEditMouseCustomState; virtual;
    function IsExternalDrag(const ADataObject: IDataObject): Boolean; virtual;

    procedure ExtendSelection(AIterator: TdxPieceTableIterator); virtual;
    procedure SwitchStateToDragState(AHitTestResult: TdxRichEditHitTestResult; ADragState: TdxRichEditMouseCustomState); virtual;

    function HandleHotZoneMouseDown(AHitTestResult: TdxRichEditHitTestResult): Boolean; virtual;
    function HandleHotZoneMouseDownCore(AHitTestResult: TdxRichEditHitTestResult; AHotZone: TdxHotZone): Boolean; virtual;
  public
    procedure HandleMouseDown(const P: TPoint); reintroduce; overload;
    procedure HandleMouseDown(const AHitTestResult: TdxRichEditHitTestResult); reintroduce; overload;
    procedure HandleMouseDown(const Args: TdxMouseEventArgs); overload; override;
    procedure HandleMouseDoubleClick(const Args: TdxMouseEventArgs); override;
    procedure HandleMouseTripleClick(const Args: TdxMouseEventArgs); override;

    procedure DoDragEnter(Args: PdxDragEventArgs); override;


  protected
    function ShouldActivateFloatingObjectTextBox(AFloatingObjectBox: TdxFloatingObjectBox;
      ARun: TdxFloatingObjectAnchorRun; const ALogicalPoint: TPoint): Boolean; virtual; 
    function ActivateFloatingObjectTextBox(ARun: TdxFloatingObjectAnchorRun; const APhysicalPoint: TPoint): Boolean; virtual; 
    procedure PlaceCaretToPhysicalPoint(APhysicalPoint: TPoint); 
  end;

  TdxRowBoxAccessor = class;
  TdxCharacterBoxAccessor = class;

  { TdxBoxAccessor }

  TdxBoxAccessor = class
  public
    function GetBox(AHitTestResult: TdxRichEditHitTestResult): TdxBox; virtual;
  end;
  TdxBoxAccessorClass = class of TdxBoxAccessor;

  { TdxRowBoxAccessor }

  TdxRowBoxAccessor = class(TdxBoxAccessor)
  public
    function GetBox(AHitTestResult: TdxRichEditHitTestResult): TdxBox; override;
  end;

  { TdxCharacterBoxAccessor }

  TdxCharacterBoxAccessor = class(TdxBoxAccessor)
  public
    function GetBox(AHitTestResult: TdxRichEditHitTestResult): TdxBox; override;
  end;

  { TdxContinueSelectionByRangesMouseHandlerState }

  TdxContinueSelectionByRangesMouseHandlerState = class(TdxRichEditMouseCustomState, IdxEndDocumentUpdateHandler)
  private
    FInitialHitTestResult: TdxRichEditHitTestResult;
    FInitialAccessor: TdxBoxAccessor;
    FIsInvalidInitialHitTestResult: Boolean;
    FStartCell: TdxTableCell;
    function CalculateStartCell: TdxTableCell;
    procedure EndDocumentUpdateHandler(ASender: TObject; E: TdxDocumentUpdateCompleteEventArgs);
    function GetInitialBox: TdxBox;
    function GetInitialHitTestResult: TdxRichEditHitTestResult;
  protected
    function AutoScrollEnabled: Boolean; override;
    function StopClickTimerOnStart: Boolean; override;

    function CalculateCursor(const Args: TdxMouseEventArgs): TCursor; virtual;
    function CreateExtendSelectionCommand: TdxExtendSelectionByRangesCommandBase; virtual; abstract;
    function GetCurrentCell(const P: TPoint): TdxTableCell;
    function GetHitTestResult(const APhysicalPoint: TPoint): TdxRichEditHitTestResult;
    function GetParagraph(const P: TPoint): TdxParagraph;
    function NeedSwitchStateToTableCellsMouseHandlerState(const P: TPoint): Boolean;
    function NeedSwitchStateToTableCellsMouseHandlerStateCore(ACurrentParagraph: TdxParagraph): Boolean;

    procedure HandleEndDocumentUpdate(const Args: TdxDocumentUpdateCompleteEventArgs);

    property InitialBox: TdxBox read GetInitialBox;
    property InitialAccessor: TdxBoxAccessor read FInitialAccessor;
    property InitialHitTestResult: TdxRichEditHitTestResult read GetInitialHitTestResult;
    property StartCell: TdxTableCell read FStartCell;
  public
    constructor Create(AController: TdxRichEditMouseController;
      ABoxAccessorClass: TdxBoxAccessorClass;
      AInitialHitTestResult: TdxRichEditHitTestResult); reintroduce;
    destructor Destroy; override;

    procedure Finish; override;
    procedure Start; override;

    procedure ContinueSelection(const Args: TdxMouseEventArgs); override;
    function CanShowToolTip: Boolean; override;

    procedure HandleMouseMove(const Args: TdxMouseEventArgs); override;
    procedure HandleMouseUp(const Args: TdxMouseEventArgs); override;
    procedure HandleMouseWheel(const Args: TdxMouseEventArgs); override;
  end;

  { TdxContinueSelectionByCharactersMouseHandlerState }

  TdxContinueSelectionByCharactersMouseHandlerState = class(TdxContinueSelectionByRangesMouseHandlerState)
  protected
    function CreateExtendSelectionCommand: TdxExtendSelectionByRangesCommandBase; override;
  public
    procedure ContinueSelection(const Args: TdxMouseEventArgs); override;
  end;

  { TdxContinueSelectionByWordsAndParagraphsMouseHandlerStateBase }

  TdxContinueSelectionByWordsAndParagraphsMouseHandlerStateBase = class abstract(TdxContinueSelectionByRangesMouseHandlerState)
  public
    procedure ContinueSelection(const Args: TdxMouseEventArgs); override;
  end;

  { TdxContinueSelectionByWordsMouseHandlerState }

  TdxContinueSelectionByWordsMouseHandlerState = class(TdxContinueSelectionByWordsAndParagraphsMouseHandlerStateBase)
  protected
    function CreateExtendSelectionCommand: TdxExtendSelectionByRangesCommandBase; override;
  end;

  { TdxContinueSelectionByParagraphsMouseHandlerState }

  TdxContinueSelectionByParagraphsMouseHandlerState = class(tdxContinueSelectionByWordsAndParagraphsMouseHandlerStateBase)
  protected
    function CreateExtendSelectionCommand: TdxExtendSelectionByRangesCommandBase; override;
  end;

  { TdxContinueSelectionByLinesMouseHandlerState }

  TdxContinueSelectionByLinesMouseHandlerState = class(TdxContinueSelectionByRangesMouseHandlerState)
  protected
    function CalculateCursor(const Args: TdxMouseEventArgs): TCursor; override;
    function CreateExtendSelectionCommand: TdxExtendSelectionByRangesCommandBase; override;
  public
    procedure ContinueSelection(const Args: TdxMouseEventArgs); override;
  end;

  { TdxHyperlinkMouseClickHandler }

  TdxHyperlinkMouseClickHandler = class
  private
    FControl: IdxRichEditControl;
  protected
    function TryProcessHyperlinkClick(AHitTestResult: TdxRichEditHitTestResult): Boolean;
  public
    constructor Create(AControl: IdxRichEditControl);
    procedure HandleMouseUp(const Args: TdxMouseEventArgs); virtual;
    property Control: IdxRichEditControl read FControl;
  end;

implementation

uses
  cxLibraryConsts, Math, cxGeometry, cxControls, dxTypeHelpers,
  dxRichEdit.Control,
  dxRichEdit.View.PageViewInfoGenerator, dxRichEdit.DocumentModel.FieldRange, dxRichEdit.Control.Cursors,
  dxRichEdit.Commands.Keyboard, dxRichEdit.Control.Mouse.DragAndDrop, dxRichEdit.Commands.ChangeProperties,
  Windows, dxRichEdit.LayoutEngine.Formatter, dxRichEdit.DocumentModel.Core;

{ TdxRichEditMouseCustomStateController }

constructor TdxRichEditMouseCustomState.Create(AController: TdxRichEditMouseController);
begin
  inherited Create;
  FController := AController;
end;

destructor TdxRichEditMouseCustomState.Destroy;
begin
  UnlockRelease;
  inherited Destroy;
end;

procedure TdxRichEditMouseCustomState.Finish;
begin
  FIsFinished := True;
end;

procedure TdxRichEditMouseCustomState.SetMouseCursor(ACursor: TCursor);
begin
  Control.Cursor := ACursor;
end;

procedure TdxRichEditMouseCustomState.Start;
begin
  if StopClickTimerOnStart then
    Controller.StopClickTimer;
end;

procedure TdxRichEditMouseCustomState.HandleLongMouseDown;
begin
end;

procedure TdxRichEditMouseCustomState.HandleMouseDown(const Args: TdxMouseEventArgs);
begin
end;

procedure TdxRichEditMouseCustomState.HandleMouseDoubleClick(const Args: TdxMouseEventArgs);
begin
end;

procedure TdxRichEditMouseCustomState.HandleMouseTripleClick(const Args: TdxMouseEventArgs);
begin
end;

procedure TdxRichEditMouseCustomState.HandleMouseMove(const Args: TdxMouseEventArgs);
var
  P: TPoint;
  AHitTestResult: TdxRichEditHitTestResult;
begin
  P := Args.MousePos;
  AHitTestResult := CalculateHitTest(P);
  try
    HandleMouseMoveCore(Args, P, AHitTestResult);
  finally
    AHitTestResult.Free;
  end;
end;

procedure TdxRichEditMouseCustomState.HandleMouseUp(const Args: TdxMouseEventArgs);
begin
end;

procedure TdxRichEditMouseCustomState.HandleMouseWheel(const Args: TdxMouseEventArgs);
begin
end;

procedure TdxRichEditMouseCustomState.LockRelease;
begin
  if Self <> nil then
    _AddRef;
end;

function TdxRichEditMouseCustomState.CalculateActiveObject(AHitTestResult: TdxRichEditHitTestResult): TObject;
begin
  Result := nil;
  if (AHitTestResult <> nil) and (AHitTestResult.DetailsLevel >= TdxDocumentLayoutDetailsLevel.Box) and
    ((AHitTestResult.Accuracy and ExactBox) <> 0) then
  begin
    Result := CalculateActiveObjectCore(GetHighDetailLevelBox(AHitTestResult).StartPos.RunIndex);
  end;
end;

function TdxRichEditMouseCustomState.CalculateActiveObjectCore(ARunIndex: Integer): TObject;
begin
  Result := ActivePieceTable.FindFieldByRunIndex(ARunIndex);
end;

function TdxRichEditMouseCustomState.CalculateHitTest(const P: TPoint): TdxRichEditHitTestResult;
begin
  Result := Controller.ActiveView.CalculateNearestCharacterHitTest(P);
end;

procedure TdxRichEditMouseCustomState.HandleMouseMoveCore(const Args: TdxMouseEventArgs; const P: TPoint;
  AHitTestResult: TdxRichEditHitTestResult);
begin
  if UseHover then
    UpdateHover(AHitTestResult);
  Controller.SetActiveObject(CalculateActiveObject(AHitTestResult));
end;

function TdxRichEditMouseCustomState.AutoScrollEnabled: Boolean;
begin
  Result := True;
end;

function TdxRichEditMouseCustomState.CanShowToolTip: Boolean;
begin
  Result := False;
end;

function TdxRichEditMouseCustomState.StopClickTimerOnStart: Boolean;
begin
  Result := True;
end;

function TdxRichEditMouseCustomState.GetTopLevelPage(AInitialHitTestResult: TdxRichEditHitTestResult): TdxPage;
begin
  if (AInitialHitTestResult.FloatingObjectBox <> nil) and (AInitialHitTestResult.FloatingObjectBoxPage <> nil) then
    Result := AInitialHitTestResult.FloatingObjectBoxPage
  else
    Result := AInitialHitTestResult.Page;
end;

procedure TdxRichEditMouseCustomState.ContinueSelection(const Args: TdxMouseEventArgs);
begin
//do nothing
end;

function TdxRichEditMouseCustomState.SuppressDefaultMouseWheelProcessing: Boolean;
begin
  Result := False;
end;

procedure TdxRichEditMouseCustomState.UnlockRelease;
begin
  if Self <> nil then
    _Release;
end;

procedure TdxRichEditMouseCustomState.DoDragOver(Args: PdxDragEventArgs);
begin
end;

procedure TdxRichEditMouseCustomState.DoDragDrop(Args: PdxDragEventArgs);
begin
end;

procedure TdxRichEditMouseCustomState.DoDragEnter(Args: PdxDragEventArgs);
begin
end;

procedure TdxRichEditMouseCustomState.DoDragLeave;
begin
end;

procedure TdxRichEditMouseCustomState.GiveFeedback(
  Args: PdxGiveFeedbackEventArgs);
begin
end;

procedure TdxRichEditMouseCustomState.QueryContinueDrag(Args: PdxQueryContinueDragEventArgs);
begin
end;

procedure TdxRichEditMouseCustomState.UpdateHover(AHitTestResult: TdxRichEditHitTestResult);
begin
Assert(False, 'not implemented');
end;

procedure TdxRichEditMouseCustomState.UpdateHoverLayout(
  AHitTestResult: TdxRichEditHitTestResult; AActiveView: TdxRichEditView;
  ABoxChanged: Boolean);
begin
Assert(False, 'not implemented');
end;

function TdxRichEditMouseCustomState.UseHover: Boolean;
begin
  Result := False;
end;

function TdxRichEditMouseCustomState.GetActivePieceTable: TdxPieceTable;
begin
  Result := DocumentModel.ActivePieceTable;
end;

function TdxRichEditMouseCustomState.GetControl: IdxRichEditControl;
begin
  Result := Controller.Control;
end;

function TdxRichEditMouseCustomState.GetDocumentModel: TdxDocumentModel;
begin
  Result := InnerControl.DocumentModel;
end;

function TdxRichEditMouseCustomState.GetHighDetailLevelBox(AHitTestResult: TdxRichEditHitTestResult): TdxBox;
begin
  if AHitTestResult.Accuracy and ExactCharacter <> 0 then
    Result := AHitTestResult.Character
  else
    Result := AHitTestResult.Box;
end;

function TdxRichEditMouseCustomState.GetInnerControl: IdxInnerControl;
begin
  Result := Controller.InnerControl;
end;

function TdxRichEditMouseCustomState.GetState: TdxRichEditMouseCustomState;
begin
  Result := Self;
end;

{ TdxRichEditMouseController }

constructor TdxRichEditMouseController.Create(ARichControl: IdxRichEditControl);
begin
  inherited Create;
  FControl := ARichControl;
  FTimer := TcxTimer.Create(nil);
  FLastDragPoint := cxNullPoint;
  FAutoScroller := TdxRichEditAutoScroller.Create(Self);
  SwitchToDefaultState;
end;

function TdxRichEditMouseController.CreateDragContentMouseHandlerStateCalculator: TObject;
begin
  Result := TdxDragContentMouseHandlerStateCalculator.Create(Control);
end;

function TdxRichEditMouseController.CreateInternalDragState: TdxRichEditMouseCustomState;
begin
  Result := TdxDragContentStandardMouseHandlerState.Create(Self)
end;

function TdxRichEditMouseController.CreateDragFloatingObjectMouseHandlerStateCalculator: TObject;
begin
  Result := TdxDragFloatingObjectMouseHandlerStateCalculator.Create;
end;

destructor TdxRichEditMouseController.Destroy;
begin
  FreeAndNil(FAutoScroller);
  FreeAndNil(FTimer);
  FState := nil;
  inherited Destroy;
end;

function TdxRichEditMouseController.CreateFakeMouseMoveEventArgs: TdxMouseEventArgs;
var
  P: TPoint;
begin
  P := GetMouseCursorPos;
  P := TWinControl(Control.GetRichEditControl).ScreenToClient(P);
  Result := TdxMouseEventArgs.Create([], [], P, 0);
end;

function TdxRichEditMouseController.GetDefaultStateClass: TdxRichEditMouseCustomStateClass;
begin
  Result := TdxRichEditMouseDefaultState;
end;

procedure TdxRichEditMouseController.SetActiveObject(Value: TObject);
begin
  FActiveObject := Value;
end;

function TdxRichEditMouseController.DeactivateTextBoxPieceTableIfNeed(APieceTable: TdxPieceTable;
  AHitTestResult: TdxRichEditHitTestResult): Boolean;
var
  ATextBoxContentType: TdxTextBoxContentType;
begin
  Result := False;
  if APieceTable.ContentType is TdxTextBoxContentType then
  begin
    ATextBoxContentType := TdxTextBoxContentType(APieceTable.ContentType);
    if ATextBoxContentType <> nil then
    begin
      if IsInlinePictureBoxHit(AHitTestResult) then
        Result := False
      else
      begin
Assert(False);
      end;
    end;
  end;
end;

procedure TdxRichEditMouseController.ClickTimerHandler(Sender: TObject);
begin
  FClickCount := 0;
  if IsClickTimerActive then
    HandleClickTimer;
  StopClickTimer;
end;

procedure TdxRichEditMouseController.HandleClickTimer;
begin
  StopClickTimer;
  if not Suspended then
    State.HandleLongMouseDown;
end;

function TdxRichEditMouseController.IsClickTimerActive: Boolean;
begin
  Result := Timer.Enabled;
end;

procedure TdxRichEditMouseController.StartClickTimer;
begin
  Assert(not IsClickTimerActive);
  FTimer.Interval := GetDoubleClickTime;
  FTimer.OnTimer := ClickTimerHandler;
  FTimer.Enabled := True;
end;

procedure TdxRichEditMouseController.StopClickTimer;
begin
  FTimer.Enabled := False;
end;

procedure TdxRichEditMouseController.SaveHitInfo(const Args: TdxMouseEventArgs);
var
  ACount: Integer;
begin
  ACount := Length(FHitInfos);
  SetLength(FHitInfos, ACount + 1);
  FHitInfos[ACount] := Args;
end;

procedure TdxRichEditMouseController.SwitchToDefaultState;
begin
  SwitchStateCore(GetDefaultStateClass.Create(Self), cxNullPoint);
end;

procedure TdxRichEditMouseController.SwitchStateCore(ANewState: TdxRichEditMouseCustomState; const AMousePosition: TPoint);
begin
  Assert(not Suspended);
  AutoScroller.Deactivate;
  if State <> nil then
    State.Finish;
  FState := ANewState;
  if State.AutoScrollEnabled then
    AutoScroller.Activate(AMousePosition);
  State.Start;
end;

procedure TdxRichEditMouseController.ClearHitInfo;
begin
  SetLength(FHitInfos, Length(FHitInfos) - 1);
end;

procedure TdxRichEditMouseController.CalculateAndSaveHitInfo(const Args: TdxMouseEventArgs);
begin
end;

function TdxRichEditMouseController.ConvertMouseEventArgs(const Args: TdxMouseEventArgs): TdxMouseEventArgs;
var
  APoint: TPoint;
begin
  APoint := GetPhysicalPoint(Args.MousePos);
  Result := Args;
  Result.MousePos := APoint;
end;

procedure TdxRichEditMouseController.ConvertMouseDragEventArgs(Args: PdxDragEventArgs);
begin
  Args.P := GetPhysicalPoint(Args.P);
end;

function TdxRichEditMouseController.GetMouseEventArgs(Buttons: TdxMouseButtons; Shift: TShiftState;
  const AMousePos: TPoint; AWheelDelta: Integer): TdxMouseEventArgs;
begin
  Result := TdxMouseEventArgs.Create(Buttons, Shift, AMousePos, AWheelDelta);
end;

function TdxRichEditMouseController.GetMouseEventArgs(Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer): TdxMouseEventArgs;
var
  AButtons: TdxMouseButtons;
begin
  AButtons := GetButtonsFromShiftState(Shift);
  Include(AButtons, Button);
  Result := TdxMouseEventArgs.Create(AButtons, Shift, Point(X, Y), 0);
end;

function TdxRichEditMouseController.GetMouseEventArgs(Shift: TShiftState; X, Y: Integer): TdxMouseEventArgs;
begin
  Result := GetMouseEventArgs(GetButtonsFromShiftState(Shift), Shift, Point(X, Y), 0);
end;

function TdxRichEditMouseController.GetDoubleClickSize: TSize;
begin
  Result := cxSize(0, 0);
end;

function TdxRichEditMouseController.GetDoubleClickTime: Integer;
begin
  Result := Windows.GetDoubleClickTime;
end;

function TdxRichEditMouseController.GetPhysicalPoint(const P: TPoint): TPoint;
var
  R: TRect;
begin
  R := TdxCustomRichEditControl(Control).ViewBounds;
  Result.X := DocumentModel.LayoutUnitConverter.PixelsToLayoutUnits(P.X, DocumentModel.DpiX) -
    R.Left;
  Result.Y := DocumentModel.LayoutUnitConverter.PixelsToLayoutUnits(P.Y, DocumentModel.DpiY) -
    R.Top;
end;


function TdxRichEditMouseController.IsDoubleClick(Args: TdxMouseEventArgs): Boolean;
begin
  Result := IsMultipleClickCore(Args) and (ClickCount = 2);
end;

function TdxRichEditMouseController.IsInlinePictureBoxHit(
  AHitTestResult: TdxRichEditHitTestResult): Boolean;
begin
  Result := False;
  if AHitTestResult.Character <> nil then
    Result := AHitTestResult.Box is TdxInlinePictureBox;
end;

function TdxRichEditMouseController.IsMultipleClickCore(Args: TdxMouseEventArgs): Boolean;
begin
  Result := IsClickTimerActive and (mbLeft in Args.Buttons) and
    (Abs(Args.MousePos.X - ClickScreenPoint.X) <= GetDoubleClickSize.cx) and
    (Abs(Args.MousePos.Y - ClickScreenPoint.Y) <= GetDoubleClickSize.cy);
end;

function TdxRichEditMouseController.IsTripleClick(Args: TdxMouseEventArgs): Boolean;
begin
  Result := IsMultipleClickCore(Args) and SupportsTripleClick and (ClickCount = 3);
end;

function TdxRichEditMouseController.SupportsTripleClick: Boolean;
begin
  Result := True;
end;

function TdxRichEditMouseController.IsAltPressed: Boolean;
begin
  Result := ssAlt in HitInfo.Shift; 
end;

function TdxRichEditMouseController.IsControlPressed: Boolean;
begin
  Result := ssCtrl in HitInfo.Shift; 
end;

function TdxRichEditMouseController.IsShiftPressed: Boolean;
begin
  Result := ssShift in HitInfo.Shift; 
end;

function TdxRichEditMouseController.CalculateMouseWheelScrollRate(AWheelDelta: Integer): Single;
begin
  Result := Mouse.WheelScrollLines * Abs(AWheelDelta) / WHEEL_DELTA / 3.0;
end;

procedure TdxRichEditMouseController.ChangeActivePieceTable(APieceTable: TdxPieceTable;
  AHitTestResult: TdxRichEditHitTestResult);
var
  ASection: TdxSection;
  ACommand: TdxChangeActivePieceTableCommand;
begin
  ASection := nil;
  if APieceTable.IsHeaderFooter then
  begin
    if (AHitTestResult = nil) or (AHitTestResult.PageArea = nil) then
      APieceTable := APieceTable.DocumentModel.MainPieceTable
    else
      ASection := AHitTestResult.PageArea.Section;
  end
  else
    if APieceTable.IsTextBox and (AHitTestResult.PageArea <> nil) then
      ASection := AHitTestResult.PageArea.Section;
  ACommand := TdxChangeActivePieceTableCommand.Create(Control, APieceTable, ASection, 0);
  try
    ACommand.ActivatePieceTable(APieceTable, ASection);
  finally
    ACommand.Free;
  end;
end;

procedure TdxRichEditMouseController.ChangeActivePieceTable(APieceTable: TdxPieceTable);
begin
  ChangeActivePieceTable(APieceTable, nil);
end;

procedure TdxRichEditMouseController.PerformWheelScroll(const Args: TdxMouseEventArgs);
begin
  if Args.Horizontal then
  begin
    if Args.WheelDelta > 0 then
      SmallScrollRight(Args.WheelDelta)
    else
      SmallScrollLeft(Args.WheelDelta);
  end
  else
  begin
    if Args.WheelDelta > 0 then
      SmallScrollUp(Args.WheelDelta)
    else
      SmallScrollDown(Args.WheelDelta);
  end;
end;

procedure TdxRichEditMouseController.PerformWheelZoom(const Args: TdxMouseEventArgs);
begin
Assert(False, 'not implemented');
end;

procedure TdxRichEditMouseController.SmallScrollDown(AWheelDelta: Integer);
var
  AScrollRate: Single;
begin
  AScrollRate := CalculateMouseWheelScrollRate(AWheelDelta);
  SmallScrollVerticallyCore(AScrollRate);
end;

procedure TdxRichEditMouseController.SmallScrollUp(AWheelDelta: Integer);
var
  AScrollRate: Single;
begin
  AScrollRate := CalculateMouseWheelScrollRate(AWheelDelta);
  SmallScrollVerticallyCore(-AScrollRate);
end;

procedure TdxRichEditMouseController.SmallScrollLeft(AWheelDelta: Integer);
var
  AScrollRate: Single;
begin
  AScrollRate := CalculateMouseWheelScrollRate(AWheelDelta);
  SmallScrollHorizontallyCore(-AScrollRate);
end;

procedure TdxRichEditMouseController.SmallScrollRight(AWheelDelta: Integer);
var
  AScrollRate: Single;
begin
  AScrollRate := CalculateMouseWheelScrollRate(AWheelDelta);
  SmallScrollHorizontallyCore(AScrollRate);
end;

procedure TdxRichEditMouseController.SmallScrollVerticallyCore(AScrollRate: Single);
var
  AGenerator: TdxPageViewInfoGenerator;
  ALogicalVisibleHeight, ALogicalOffsetInModelUnits: Integer;
  ACommand: TdxScrollVerticallyByLogicalOffsetCommand;
begin
  AGenerator := Control.InnerControl.ActiveView.PageViewInfoGenerator;
  ALogicalVisibleHeight := Round(AGenerator.ViewPortBounds.Height / AGenerator.ZoomFactor);

  ACommand := TdxScrollVerticallyByLogicalOffsetCommand.Create(Control);
  try
    ALogicalOffsetInModelUnits := Trunc(Max(1, Control.InnerControl.DocumentModel.UnitConverter.DocumentsToModelUnits(150) * Abs(AScrollRate)));
    ACommand.LogicalOffset := Sign(AScrollRate) * Min(ALogicalVisibleHeight, Control.InnerControl.DocumentModel.ToDocumentLayoutUnitConverter.ToLayoutUnits(ALogicalOffsetInModelUnits));
    ACommand.Execute;
  finally
    ACommand.Free;
  end;
end;

procedure TdxRichEditMouseController.SmallScrollHorizontallyCore(AScrollRate: Single);
var
  AGenerator: TdxPageViewInfoGenerator;
  AVisibleWidth, APhysicalOffsetInModelUnits: Integer;
  ACommand: TdxScrollHorizontallyByPhysicalOffsetCommand;
begin
  AGenerator := Control.InnerControl.ActiveView.PageViewInfoGenerator;
  AVisibleWidth := Round(AGenerator.ViewPortBounds.Width / AGenerator.ZoomFactor);

  ACommand := TdxScrollHorizontallyByPhysicalOffsetCommand.Create(Control);
  try
    APhysicalOffsetInModelUnits := Trunc(Max(1, Control.InnerControl.DocumentModel.UnitConverter.DocumentsToModelUnits(150) * Abs(AScrollRate)));
    ACommand.PhysicalOffset := Sign(AScrollRate) * Min(AVisibleWidth, Control.InnerControl.DocumentModel.ToDocumentLayoutUnitConverter.ToLayoutUnits(APhysicalOffsetInModelUnits));
    ACommand.Execute;
  finally
    ACommand.Free;
  end;
end;

procedure TdxRichEditMouseController.HandleMouseDoubleClick(const Args: TdxMouseEventArgs);
begin
  SaveHitInfo(Args);
  try
    (State as IdxRichEditMouseState).HandleMouseDoubleClick(Args); 
  finally
    ClearHitInfo;
  end;
end;

procedure TdxRichEditMouseController.HandleMouseDown(const Args: TdxMouseEventArgs);
begin
  SaveHitInfo(Args);
  try
    (State as IdxRichEditMouseState).HandleMouseDown(Args); 
  finally
    ClearHitInfo;
  end;
end;

procedure TdxRichEditMouseController.HandleMouseTripleClick(const Args: TdxMouseEventArgs);
begin
  SaveHitInfo(Args);
  try
    (State as IdxRichEditMouseState).HandleMouseTripleClick(Args); 
  finally
    ClearHitInfo;
  end;
end;

procedure TdxRichEditMouseController.HandleMouseMove(const Args: TdxMouseEventArgs);
var
  P: TPoint;
begin
  P := Args.MousePos;
  CalculateAndSaveHitInfo(Args);
  SaveHitInfo(Args);
  try
    (State as IdxRichEditMouseState).HandleMouseMove(Args); 
    AutoScroller.HandleMouseMove(P);
  finally
    ClearHitInfo;
  end;
end;

procedure TdxRichEditMouseController.HandleMouseUp(const Args: TdxMouseEventArgs);
begin
  CalculateAndSaveHitInfo(Args);
  SaveHitInfo(Args);
  try
    (State as IdxRichEditMouseState).HandleMouseUp(Args); 
  finally
    ClearHitInfo;
  end;
end;

procedure TdxRichEditMouseController.HandleMouseWheel(const Args: TdxMouseEventArgs);
var
  ASuppressWheelScroll: Boolean;
  ADragState: TdxBeginMouseDragHelperState;
begin
  if not State.SuppressDefaultMouseWheelProcessing then
  begin
    if IsControlPressed then
      PerformWheelZoom(Args)
    else
    begin
      if State is TdxBeginMouseDragHelperState then
        ADragState := TdxBeginMouseDragHelperState(State)
      else
        ADragState := nil;
      ASuppressWheelScroll := (ADragState <> nil) and (ADragState.DragState is TdxDragContentStandardMouseHandlerState);
      if not ASuppressWheelScroll then
        PerformWheelScroll(Args);
    end;
  end;
  (State as IdxRichEditMouseState).HandleMouseWheel(Args); 
end;

function TdxRichEditMouseController.MouseActivate(Button: TMouseButton;
  Shift: TShiftState; X, Y, HitTest: Integer): TMouseActivate;
begin
  Assert(False, 'need return value of function!');
  Result := maDefault;
end;

procedure TdxRichEditMouseController.MouseDown(Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  Args: TdxMouseEventArgs;
begin
  if Button = mbMiddle then
  begin
    Assert(False, 'not implemented');
    Exit;
  end;
  Inc(FClickCount);
  Args := GetMouseEventArgs(Button, Shift, X, Y);
  CalculateAndSaveHitInfo(Args);
  if IsTripleClick(Args) then
  begin
    StopClickTimer;
    HandleMouseTripleClick(ConvertMouseEventArgs(Args));
  end
  else
    if IsDoubleClick(Args) then
    begin
      StopClickTimer;
      if SupportsTripleClick then
      begin
        StartClickTimer;
        FClickCount := 2;
      end;
      HandleMouseDoubleClick(ConvertMouseEventArgs(Args));
    end
    else
    begin
      StopClickTimer;
      StartClickTimer;
      FClickCount := 1;
      FClickScreenPoint := Args.MousePos;
      HandleMouseDown(ConvertMouseEventArgs(Args));
    end;
end;

procedure TdxRichEditMouseController.MouseMove(Shift: TShiftState; X,
  Y: Integer);
begin
  HandleMouseMove(ConvertMouseEventArgs(GetMouseEventArgs(Shift, X, Y)));
end;

procedure TdxRichEditMouseController.MouseUp(Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  HandleMouseUp(ConvertMouseEventArgs(GetMouseEventArgs(Button, Shift, X, Y)));
end;

function TdxRichEditMouseController.MouseWheel(Shift: TShiftState;
  WheelDelta: Integer; MousePos: TPoint): Boolean;
begin
  Result := True;
  HandleMouseWheel(ConvertMouseEventArgs(GetMouseEventArgs(GetButtonsFromShiftState(Shift), Shift, MousePos, WheelDelta)));
end;

procedure TdxRichEditMouseController.DoDragEnter(Args: PdxDragEventArgs);
begin
  ConvertMouseDragEventArgs(Args);
  FLastDragPoint := cxNullPoint;
  FLastDragDropEffects := [];
  State.DoDragEnter(Args);
end;

procedure TdxRichEditMouseController.DoDragOver(Args: PdxDragEventArgs);
var
  P: TPoint;
begin
  ConvertMouseDragEventArgs(Args);
  P := Args.P;
  if not P.IsEqual(FLastDragPoint) then
  begin
    State.DoDragOver(Args);
    FLastDragDropEffects := Args.Effect;
    AutoScroller.DoMouseMove(P);
    FLastDragPoint := P;
  end
  else
    Args.Effect := FLastDragDropEffects;
end;

procedure TdxRichEditMouseController.DoDragDrop(Args: PdxDragEventArgs);
begin
  ConvertMouseDragEventArgs(Args);
  State.DoDragDrop(Args);
end;

procedure TdxRichEditMouseController.DoDragLeave;
begin
  State.DoDragLeave;
end;

procedure TdxRichEditMouseController.GiveFeedback(Args: PdxGiveFeedbackEventArgs);
begin
  State.GiveFeedback(Args);
end;

procedure TdxRichEditMouseController.QueryContinueDrag(Args: PdxQueryContinueDragEventArgs);
begin
  State.QueryContinueDrag(Args);
end;

function TdxRichEditMouseController.GetActiveView: TdxRichEditView;
begin
  Result := InnerControl.ActiveView;
end;

function TdxRichEditMouseController.GetButtonsFromShiftState(Shift: TShiftState): TdxMouseButtons;
begin
  Result := [];
  if ssLeft in Shift then
    Include(Result, mbLeft);
  if ssRight in Shift then
    Include(Result, mbRight);
  if ssMiddle in Shift then
    Include(Result, mbMiddle);
end;

function TdxRichEditMouseController.GetDocumentModel: TdxDocumentModel;
begin
  Result := InnerControl.DocumentModel;
end;

function TdxRichEditMouseController.GetHitInfo: TdxMouseEventArgs;
var
  ACount: Integer;
begin
  ACount := Length(FHitInfos);
  if ACount > 0 then
    Result := FHitInfos[ACount - 1]
  else
  begin
    FillChar(Result, SizeOf(TdxMouseEventArgs), 0);
    Result.MousePos := cxInvalidPoint;
  end;
end;

function TdxRichEditMouseController.GetInnerControl: IdxInnerControl;
begin
  Result := Control.InnerControl;
end;

function TdxRichEditMouseController.GetState: TdxRichEditMouseCustomState;
begin
  if FState = nil then
    Result := nil
  else
    Result := FState.State;
end;

{ TdxRichEditMouseDefaultStateController }

procedure TdxRichEditMouseDefaultState.HandleMouseDown(const P: TPoint);
var
  AHitTestResult: TdxRichEditHitTestResult;
begin
  AHitTestResult := CalculateHitTest(P);
  try
    if AHitTestResult = nil then
    begin
      AHitTestResult := Controller.ActiveView.CalculateNearestPageHitTest(P, False);
      if AHitTestResult <> nil then
      begin
        if not HandleHotZoneMouseDown(AHitTestResult) then
        begin
          if ShouldSelectFloatingObject(AHitTestResult) then
            SelectFloatingObject(AHitTestResult, True)
          else
            if ShouldDeactivateFloatingObject(AHitTestResult) then
              DeactivateFloatingObject(AHitTestResult);
        end;
      end;
    end
    else
      HandleMouseDown(AHitTestResult);
  finally
    AHitTestResult.Free;
  end;
end;

procedure TdxRichEditMouseDefaultState.HandleMouseDown(const AHitTestResult: TdxRichEditHitTestResult);
var
  ASelectionManager: TdxEnhancedSelectionManager;
begin
  if HandleHotZoneMouseDown(AHitTestResult) then
    Exit;

  ASelectionManager := TdxEnhancedSelectionManager.Create(ActivePieceTable);
  try
    if not TableMouseDown(ASelectionManager, AHitTestResult) then
    begin
      if ShouldSelectFloatingObject(AHitTestResult) then
        SelectFloatingObject(AHitTestResult, True)
      else
        if ASelectionManager.ShouldSelectEntireRow(AHitTestResult) then
          BeginLineSelection(AHitTestResult)
        else
          if ShouldStartMultiSelection then
            BeginMultiSelection(AHitTestResult)
          else
            if ShouldExtendSelectionToCursor then
            begin
              if not TryProcessHyperlinkClick(AHitTestResult) then
                ExtendSelectionToCursor(AHitTestResult);
            end
            else
              if ShouldSelectPicture(AHitTestResult) then
              begin
                if not TryProcessHyperlinkClick(AHitTestResult) then
                  SelectPicture(AHitTestResult);
              end
              else
              begin
                if AHitTestResult.PieceTable = ActivePieceTable then
                  BeginCharacterSelection(AHitTestResult)
                else
                begin
                  if ShouldDeactivateFloatingObject(AHitTestResult) then
                    DeactivateFloatingObject(AHitTestResult);
                end;
              end;
    end;
  finally
    ASelectionManager.Free;
  end;
end;

procedure TdxRichEditMouseDefaultState.HandleMouseDown(
  const Args: TdxMouseEventArgs);
begin
  if [mbLeft] = Args.Buttons then
    HandleMouseDown(Args.MousePos);
end;

procedure TdxRichEditMouseDefaultState.HandleMouseDoubleClick(const Args: TdxMouseEventArgs);
var
  APhysicalPoint: TPoint;
  AHitTestResult: TdxRichEditHitTestResult;
  ACommand: TdxChangeActivePieceTableCommand;
begin
  APhysicalPoint := Args.MousePos;
  AHitTestResult := CalculateExactPageAreaHitTest(APhysicalPoint);
  try
    if (AHitTestResult <> nil) and ShouldSwitchActivePieceTable(AHitTestResult) then
    begin
      ACommand := TdxChangeActivePieceTableCommand.Create(Control, AHitTestResult.PageArea.PieceTable,
        AHitTestResult.PageArea.Section, AHitTestResult.Page.PageIndex);
      ACommand.Execute;
      Exit;
    end;
  finally
    AHitTestResult.Free;
  end;
  AHitTestResult := CalculateHitTest(APhysicalPoint);
  try
    if AHitTestResult = nil then
      Exit;
    if AHitTestResult.PieceTable <> ActivePieceTable then
      Exit;
    PerformEnhancedSelection(AHitTestResult);
  finally
    AHitTestResult.Free;
  end;
end;

procedure TdxRichEditMouseDefaultState.HandleMouseTripleClick(const Args: TdxMouseEventArgs);
var
  P: TPoint;
  AHitTestResult: TdxRichEditHitTestResult;
  ASelectionManager: TdxEnhancedSelectionManager;
  ACommand: TdxSelectAllCommand;
begin
  P := Args.MousePos;
  AHitTestResult := CalculateHitTest(P);
  try
    if AHitTestResult = nil then
      Exit;
    if AHitTestResult.PieceTable <> ActivePieceTable then
      Exit;
    ASelectionManager := TdxEnhancedSelectionManager.Create(ActivePieceTable);
    try
      if ASelectionManager.ShouldSelectEntireRow(AHitTestResult) then
      begin
        ACommand := TdxSelectAllCommand.Create(Control);
        try
          ACommand.Execute;
        finally
          ACommand.Free;
        end;
      end
      else
        BeginParagraphsSelection(AHitTestResult);
    finally
      ASelectionManager.Free;
    end;
  finally
    AHitTestResult.Free;
  end;
end;

procedure TdxRichEditMouseDefaultState.HandleMouseMoveCore(const Args: TdxMouseEventArgs;
  const P: TPoint; AHitTestResult: TdxRichEditHitTestResult);
var
  ACalculator: TdxMouseCursorCalculator;
begin
  inherited HandleMouseMoveCore(Args, P, AHitTestResult);
  if IsHyperlinkActive and InnerControl.IsHyperlinkModifierKeysPress then
    SetMouseCursor(TdxRichEditCursors.Hand)
  else
  if ShouldBeginDragExistingSelection(AHitTestResult) then
    SetMouseCursor(TdxRichEditCursors.Arrow)
  else
  begin
    ACalculator := Control.InnerControl.CreateMouseCursorCalculator;
    try
      SetMouseCursor(ACalculator.Calculate(AHitTestResult, P));
    finally
      ACalculator.Free;
    end;
  end;
end;

function TdxRichEditMouseDefaultState.IsExternalDrag(
  const ADataObject: IDataObject): Boolean;
begin
  Result := not Supports(ADataObject, IdxDataObject);
end;

function TdxRichEditMouseDefaultState.IsHyperlinkActive: Boolean;
begin
  Result := (Controller.ActiveObject is TdxField) and
    DocumentModel.ActivePieceTable.IsHyperlinkField(TdxField(Controller.ActiveObject));
end;

function TdxRichEditMouseDefaultState.ActivateFloatingObjectTextBox(ARun: TdxFloatingObjectAnchorRun;
  const APhysicalPoint: TPoint): Boolean;
var
  ATextBoxContent: TdxTextBoxFloatingObjectContent;
begin
  Result := False;
  if ARun = nil then
    Exit;
  if ARun.Content is TdxTextBoxFloatingObjectContent then
  begin
    ATextBoxContent := TdxTextBoxFloatingObjectContent(ARun.Content);
    Control.BeginUpdate;
    try
      Controller.ChangeActivePieceTable(ATextBoxContent.TextBox.PieceTable); 
      ClearMultiSelection;
      PlaceCaretToPhysicalPoint(APhysicalPoint);
    finally
      Control.EndUpdate;
    end;
    Result := True;
  end;
end;

function TdxRichEditMouseDefaultState.AutoScrollEnabled: Boolean;
begin
  Result := False;
end;

function TdxRichEditMouseDefaultState.CanShowToolTip: Boolean;
begin
  Result := True;
end;

procedure TdxRichEditMouseDefaultState.BeginCharacterSelection(AHitTestResult: TdxRichEditHitTestResult);
begin
  if ShouldBeginDragExistingSelection(AHitTestResult) then
    BeginDragExistingSelection(AHitTestResult, True)
  else
  begin
    ClearMultiSelection;
    BeginCharacterSelectionCore(AHitTestResult);
  end;
end;

procedure TdxRichEditMouseDefaultState.BeginCharacterSelectionCore(
  AHitTestResult: TdxRichEditHitTestResult);
var
  ADragState: TdxRichEditMouseCustomState;
  ANewState: TdxBeginMouseDragHyperlinkClickHandleHelperState;
  ACommand: TdxPlaceCaretToPhysicalPointCommand;
begin
  ADragState := TdxContinueSelectionByCharactersMouseHandlerState.Create(Controller,
    TdxCharacterBoxAccessor, AHitTestResult);
  ANewState := TdxBeginMouseDragHyperlinkClickHandleHelperState.Create(Controller, ADragState, AHitTestResult.PhysicalPoint);
  LockRelease;
  try
    Controller.SwitchStateCore(ANewState, AHitTestResult.PhysicalPoint);
    ACommand := TdxPlaceCaretToPhysicalPointCommand2.Create(Control);
    try
      ACommand.PhysicalPoint := AHitTestResult.PhysicalPoint;
      ACommand.ExecuteCore;
    finally
      ACommand.Free;
    end;
  finally
    UnlockRelease;
  end;
end;

procedure TdxRichEditMouseDefaultState.BeginDragExistingSelection(
  AHitTestResult: TdxRichEditHitTestResult; AResetSelectionOnMouseUp: Boolean);
var
  ADragState: TdxRichEditMouseCustomState;
  ANewState: TdxBeginContentDragHelperState;
begin
  if Controller.DeactivateTextBoxPieceTableIfNeed(AHitTestResult.PieceTable, AHitTestResult) then
  begin
    HandleMouseDown(AHitTestResult.PhysicalPoint);
    Exit;
  end;
  ADragState := CreateDragContentState(AHitTestResult);
  ANewState := TdxBeginContentDragHelperState.Create(Controller, ADragState, AHitTestResult.PhysicalPoint);
  ANewState.ResetSelectionOnMouseUp := AResetSelectionOnMouseUp;
  Controller.SwitchStateCore(ANewState, AHitTestResult.PhysicalPoint);
end;

procedure TdxRichEditMouseDefaultState.BeginLineSelection(AHitTestResult: TdxRichEditHitTestResult);
var
  ACommand: TdxSelectLineCommand;
  ADragState: TdxRichEditMouseCustomState;
begin
  ACommand := TdxSelectLineCommand.Create(Control);
  try
    ACommand.PhysicalPoint := AHitTestResult.PhysicalPoint;
    ACommand.ExecuteCore;
    ClearMultiSelection;
    ADragState := TdxContinueSelectionByLinesMouseHandlerState.Create(Controller, TdxRowBoxAccessor, AHitTestResult);
    SwitchStateToDragState(AHitTestResult, ADragState);
  finally
    ACommand.Free;
  end;
end;

procedure TdxRichEditMouseDefaultState.BeginMultiSelection(AHitTestResult: TdxRichEditHitTestResult);
begin
  if AHitTestResult.PieceTable <> ActivePieceTable then
    Exit;
  DocumentModel.Selection.BeginMultiSelection(ActivePieceTable);
  BeginCharacterSelectionCore(AHitTestResult);
end;

procedure TdxRichEditMouseDefaultState.BeginParagraphsSelection(AHitTestResult: TdxRichEditHitTestResult);
var
  ADragState: TdxRichEditMouseCustomState;
  ANewState: TdxBeginMouseDragHelperState;
  AIterator: TdxParagraphsDocumentModelIterator;
begin
  ADragState := TdxContinueSelectionByParagraphsMouseHandlerState.Create(Controller, TdxCharacterBoxAccessor, AHitTestResult);
  ANewState := TdxRichEditBeginMouseDragHelperState.Create(Controller, ADragState, AHitTestResult.PhysicalPoint);
  Controller.SwitchStateCore(ANewState, AHitTestResult.PhysicalPoint);
  AIterator := TdxParagraphsDocumentModelIterator.Create(ActivePieceTable);
  try
    ExtendSelection(AIterator);
  finally
    AIterator.Free;
  end;
end;

procedure TdxRichEditMouseDefaultState.BeginTableRowsSelectionState(AHitTestResult: TdxRichEditHitTestResult);
begin
Assert(False, 'not implemented');
end;

procedure TdxRichEditMouseDefaultState.BeginWordsSelection(AHitTestResult: TdxRichEditHitTestResult);
var
  ADragState: TdxRichEditMouseCustomState;
  ANewState: TdxBeginMouseDragHelperState;
  AIterator: TdxWordsDocumentModelIterator;
begin
  ADragState := TdxContinueSelectionByWordsMouseHandlerState.Create(Controller, TdxCharacterBoxAccessor, AHitTestResult);
  ANewState := TdxRichEditBeginMouseDragHelperState.Create(Controller, ADragState, AHitTestResult.PhysicalPoint);
  Controller.SwitchStateCore(ANewState, AHitTestResult.PhysicalPoint);
  AIterator := TdxWordsDocumentModelIterator.Create(ActivePieceTable);
  try
    ExtendSelection(AIterator);
  finally
    AIterator.Free;
  end;
end;

function TdxRichEditMouseDefaultState.CalculateExactPageAreaHitTest(const P: TPoint): TdxRichEditHitTestResult;
var
  ARequest: TdxRichEditHitTestRequest;
begin
  ARequest := TdxRichEditHitTestRequest.Create(ActivePieceTable);
  try
    ARequest.PhysicalPoint := P;
    ARequest.DetailsLevel := TdxDocumentLayoutDetailsLevel.Character;
    ARequest.Accuracy := ExactPage or ExactPageArea or NearestColumn or NearestTableRow or
      NearestTableCell or NearestRow or NearestBox or NearestCharacter;
    ARequest.SearchAnyPieceTable := True;
    Result := Controller.ActiveView.HitTestCore(ARequest, True);
    if not Result.IsValid(TdxDocumentLayoutDetailsLevel.PageArea) then
      FreeAndNil(Result);
  finally
    ARequest.Free;
  end;
end;

procedure TdxRichEditMouseDefaultState.ClearMultiSelection;
begin
  DocumentModel.Selection.ClearMultiSelection;
end;

function TdxRichEditMouseDefaultState.CreateDragContentState(
  AHitTestResult: TdxRichEditHitTestResult): TdxRichEditMouseCustomState;
begin
  if (AHitTestResult.FloatingObjectBox <> nil) and not Controller.IsInlinePictureBoxHit(AHitTestResult) then
  begin
    Result := TdxDragFloatingObjectManuallyMouseHandlerState.Create(Controller, AHitTestResult);
    Exit;
  end;
  if Control.UseStandardDragDropMode then
    Result := TdxDragContentStandardMouseHandlerState.Create(Controller)
  else
    Result := TdxDragContentManuallyMouseHandlerState.Create(Controller);
end;

function TdxRichEditMouseDefaultState.CreateDragState(
  const ADataObject: IDataObject): TdxRichEditMouseCustomState;
begin
  if IsExternalDrag(ADataObject) then
    Result :=  CreateExternalDragState
  else
    Result := CreateInternalDragState;
end;

function TdxRichEditMouseDefaultState.CreateExternalDragState: TdxRichEditMouseCustomState;
begin
  Result := TdxDragExternalContentMouseHandlerState.Create(Controller);
end;

function TdxRichEditMouseDefaultState.CreateInternalDragState: TdxRichEditMouseCustomState;
begin
  Result := Controller.CreateInternalDragState;
end;

procedure TdxRichEditMouseDefaultState.DeactivateFloatingObject(AHitTestResult: TdxRichEditHitTestResult);
var
  AFloatingObjectBox: TdxFloatingObjectBox;
{  ATextBox: TdxTextBoxContentType;}
begin
  AFloatingObjectBox := AHitTestResult.FloatingObjectBox;
  if AFloatingObjectBox = nil then
  begin
    Control.BeginUpdate;
    try
Assert(False);
    finally
      Control.EndUpdate;
    end;
  end
  else
  begin
    Assert(False, 'not implemented');
  end;
end;

procedure TdxRichEditMouseDefaultState.DoDragEnter(
  Args: PdxDragEventArgs);
var
  ADataObject: IDataObject;
  AState: TdxDragContentMouseHandlerStateBase;
  AIsCommandEnabled: Boolean;
begin
  inherited DoDragEnter(Args);
  ADataObject := Args.Data;
  if ADataObject = nil then
    Exit;
  AState := CreateDragState(ADataObject) as TdxDragContentMouseHandlerStateBase;
  AIsCommandEnabled := Control.InnerControl.Options.Behavior.DropAllowed;
  if AIsCommandEnabled and AState.CanDropData(Args) then
    Controller.SwitchStateCore(AState, Point(Args.X, Args.Y))
  else
    AState.Free;
end;

procedure TdxRichEditMouseDefaultState.ExtendSelection(
  AIterator: TdxPieceTableIterator);
var
  ADocumentModel: TdxDocumentModel;
  ACurrentModelPosition, AStart, AEnd: TdxDocumentModelPosition;
begin
  ADocumentModel := DocumentModel;
  ACurrentModelPosition := ADocumentModel.Selection.Interval.Start.Clone;
  if AIterator.IsNewElement(ACurrentModelPosition) then
    AStart := ACurrentModelPosition
  else
    AStart := AIterator.MoveBack(ACurrentModelPosition);
  AEnd := AIterator.MoveForward(ACurrentModelPosition);
  SetSelection(AStart.LogPosition, AEnd.LogPosition);
end;

procedure TdxRichEditMouseDefaultState.ExtendSelectionToCursor(AHitTestResult: TdxRichEditHitTestResult);
var
  ACommand: TdxPlaceCaretToPhysicalPointCommand;
begin
  if AHitTestResult.PieceTable <> ActivePieceTable then
    Exit;
  ACommand := TdxExtendSelectionToPhysicalPointCommand.Create(Control);
  try
    ACommand.PhysicalPoint := AHitTestResult.PhysicalPoint;
    ACommand.ExecuteCore;
  finally
    ACommand.Free;
  end;
end;

procedure TdxRichEditMouseDefaultState.PerformEnhancedSelection(AHitTestResult: TdxRichEditHitTestResult);
var
  ASelectionManager: TdxEnhancedSelectionManager;
begin
  ASelectionManager := TdxEnhancedSelectionManager.Create(ActivePieceTable);
  try
    if ASelectionManager.ShouldSelectEntireTableCell(AHitTestResult) then
      BeginTableRowsSelectionState(AHitTestResult)
    else
      if ASelectionManager.ShouldSelectEntireRow(AHitTestResult) then
        BeginParagraphsSelection(AHitTestResult)
      else
        BeginWordsSelection(AHitTestResult);
  finally
    ASelectionManager.Free;
  end;
end;

procedure TdxRichEditMouseDefaultState.PlaceCaretToPhysicalPoint(APhysicalPoint: TPoint);
var
  APlaceCaretCommand: TdxPlaceCaretToPhysicalPointCommand;
begin
  APlaceCaretCommand := TdxPlaceCaretToPhysicalPointCommand2.Create(Control);
  try
    APlaceCaretCommand.PhysicalPoint := APhysicalPoint;
    APlaceCaretCommand.ExecuteCore;
  finally
    APlaceCaretCommand.Free;
  end;
end;

procedure TdxRichEditMouseDefaultState.SelectFloatingObject(AHitTestResult: TdxRichEditHitTestResult; AllowDrag: Boolean);
var
  AFloatingObjectBox: TdxFloatingObjectBox;
  ARun: TdxFloatingObjectAnchorRun;
  ALogPosition: TdxDocumentLogPosition;
begin
  Assert(AHitTestResult.FloatingObjectBox <> nil);
  AFloatingObjectBox := AHitTestResult.FloatingObjectBox;
  ARun := AFloatingObjectBox.GetFloatingObjectRun;
  ALogPosition := ARun.PieceTable.GetRunLogPosition(AFloatingObjectBox.StartPos.RunIndex);
  if ActivePieceTable <> ARun.PieceTable then
    Controller.ChangeActivePieceTable(ARun.PieceTable, AHitTestResult);
  SetSelection(ALogPosition, ALogPosition + 1);
  if ShouldActivateFloatingObjectTextBox(AFloatingObjectBox, ARun, AHitTestResult.LogicalPoint) and
      ActivateFloatingObjectTextBox(ARun, AHitTestResult.PhysicalPoint) then
    AllowDrag := False;
  if AllowDrag and ShouldBeginDragExistingSelection(AHitTestResult) then
    BeginDragExistingSelection(AHitTestResult, False);
end;

procedure TdxRichEditMouseDefaultState.SelectPicture(AHitTestResult: TdxRichEditHitTestResult);
var
  AStart: TdxDocumentLogPosition;
begin
  AStart := AHitTestResult.Box.GetFirstPosition(ActivePieceTable).LogPosition;
  SetSelection(AStart, AStart + 1);
  if ShouldBeginDragExistingSelection(AHitTestResult) then
    BeginDragExistingSelection(AHitTestResult, False);
end;

procedure TdxRichEditMouseDefaultState.SetSelection(AStart, AEnd: TdxDocumentLogPosition);
var
  ASelection: TdxSelection;
begin
  DocumentModel.BeginUpdate;
  try
    ASelection := DocumentModel.Selection;
    ASelection.BeginUpdate;
    try
      ASelection.ClearMultiSelection;
      ASelection.Start := AStart;
      ASelection.&End := AEnd;
      ASelection.SetStartCell(AStart);
      ValidateFieldSelection(DocumentModel, ASelection);
    finally
      ASelection.EndUpdate;
    end;
    DocumentModel.ActivePieceTable.ApplyChangesCore([TdxDocumentModelChangeAction.ResetSelectionLayout,
      TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetCaretInputPositionFormatting,
      TdxDocumentModelChangeAction.ResetRuler], -1, -1);
  finally
    DocumentModel.EndUpdate;
  end;
end;

function TdxRichEditMouseDefaultState.ShouldActivateFloatingObjectTextBox(AFloatingObjectBox: TdxFloatingObjectBox;
  ARun: TdxFloatingObjectAnchorRun; const ALogicalPoint: TPoint): Boolean;
var
  APoint: TPoint;
begin
  if (ARun = nil) or (AFloatingObjectBox.DocumentLayout = nil) then
    Exit(False);
  if not (ARun.Content is TdxTextBoxFloatingObjectContent) then
    Exit(False);
  APoint := AFloatingObjectBox.TransformPointBackward(ALogicalPoint);
  Result := AFloatingObjectBox.GetTextBoxContentBounds.Contains(APoint);
end;

function TdxRichEditMouseDefaultState.ShouldBeginDragExistingSelection(AHitTestResult: TdxRichEditHitTestResult): Boolean;
var
  APos: TdxDocumentModelPosition;
  ASelectionLayout: TdxSelectionLayout;
begin
  if (AHitTestResult.FloatingObjectBox <> nil) and not ActivePieceTable.ContentType.IsTextBox then
    APos := AHitTestResult.FloatingObjectBox.GetFirstPosition(ActivePieceTable)
  else
    if AHitTestResult.Character <> nil then
    begin
      if AHitTestResult.Box is TdxInlinePictureBox then
        APos := AHitTestResult.Box.GetFirstPosition(ActivePieceTable)
      else
        APos := AHitTestResult.Character.GetFirstPosition(ActivePieceTable);
    end
    else
      APos := AHitTestResult.Box.GetFirstPosition(ActivePieceTable);

  if not APos.IsValid then
    Exit(False);

  ASelectionLayout := Controller.ActiveView.SelectionLayout;
  Result := ASelectionLayout.HitTest(APos.LogPosition, AHitTestResult.LogicalPoint);
end;

function TdxRichEditMouseDefaultState.ShouldDeactivateFloatingObject(AHitTestResult: TdxRichEditHitTestResult): Boolean;
var
  AFloatingObjectBox: TdxFloatingObjectBox;
begin
  Result := False;
  if AHitTestResult.DetailsLevel < TdxDocumentLayoutDetailsLevel.Page then
    Exit;
  if not ActivePieceTable.IsTextBox then
    Exit;
  AFloatingObjectBox := AHitTestResult.FloatingObjectBox;
  Result := (AFloatingObjectBox = nil) or (AFloatingObjectBox.GetFloatingObjectRun.PieceTable <> ActivePieceTable);
  if not Result then
    Assert(False, 'not implemented: need FloatingObjectBox');
end;

function TdxRichEditMouseDefaultState.ShouldSelectFloatingObject(
  AHitTestResult: TdxRichEditHitTestResult): Boolean;
var
  AFloatingObjectBox: TdxFloatingObjectBox;
begin
  Result := False;
  if AHitTestResult.DetailsLevel < TdxDocumentLayoutDetailsLevel.Page then
    Exit;
  AFloatingObjectBox := AHitTestResult.FloatingObjectBox;
  if AFloatingObjectBox = nil then
    Exit;
  Result := AFloatingObjectBox.PieceTable = ActivePieceTable;
  if Result then
    Exit;
  Assert(False, 'not implemented: need FloatingObjectAnchorRun');

end;

function TdxRichEditMouseDefaultState.ShouldSelectPicture(AHitTestResult: TdxRichEditHitTestResult): Boolean;
begin
  Result := False;
  if AHitTestResult.PieceTable <> ActivePieceTable then
    Exit;
  if AHitTestResult.DetailsLevel < TdxDocumentLayoutDetailsLevel.Box then
    Exit;
  if ((AHitTestResult.Accuracy and ExactBox) = 0) and ((AHitTestResult.Accuracy and ExactCharacter) = 0) then
    Exit;
  Result := AHitTestResult.Box is TdxInlinePictureBox;

  if not Result and (AHitTestResult.Box is TdxCustomRunBox) then
  begin
    NotImplemented;
  end;
end;

function TdxRichEditMouseDefaultState.ShouldStartMultiSelection: Boolean;
begin
  Result := Controller.IsControlPressed and (DocumentModel.Selection.Length > 0);
end;

function TdxRichEditMouseDefaultState.ShouldSwitchActivePieceTable(AHitTestResult: TdxRichEditHitTestResult): Boolean;
begin
  if (AHitTestResult.PageArea <> nil) and (ActivePieceTable = AHitTestResult.PageArea.PieceTable) then
    Result := False
  else
  begin
    if AHitTestResult.FloatingObjectBox <> nil then
    begin
      Assert(False, 'not implemented: need FloatingObjectBox');
    end;
      Assert(False, 'not implemented: need InnerControl.ActiveViewType');
    Result := Boolean(NotImplemented);
  end;
end;

function TdxRichEditMouseDefaultState.StopClickTimerOnStart: Boolean;
begin
  Result := False;
end;

procedure TdxRichEditMouseDefaultState.SwitchStateToDragState(
  AHitTestResult: TdxRichEditHitTestResult;
  ADragState: TdxRichEditMouseCustomState);
var
  ANewState: TdxBeginMouseDragHelperState;
begin
  ANewState := TdxBeginMouseDragHelperState.Create(Controller, ADragState, AHitTestResult.PhysicalPoint);
  Controller.SwitchStateCore(ANewState, AHitTestResult.PhysicalPoint);
end;

function TdxRichEditMouseDefaultState.HandleHotZoneMouseDown(AHitTestResult: TdxRichEditHitTestResult): Boolean;
var
  AHotZone: TdxHotZone;
begin
  AHotZone := Controller.ActiveView.SelectionLayout.CalculateHotZone(AHitTestResult, Controller.ActiveView);
  Result := HandleHotZoneMouseDownCore(AHitTestResult, AHotZone);
end;

function TdxRichEditMouseDefaultState.HandleHotZoneMouseDownCore(AHitTestResult: TdxRichEditHitTestResult;
  AHotZone: TdxHotZone): Boolean;
begin
  Result := False;
  if (AHotZone = nil) or not Control.InnerControl.IsEditable then
    Exit;
  Result := True;
  if not AHotZone.BeforeActivate(Controller, AHitTestResult) then
    HandleMouseDown(AHitTestResult.PhysicalPoint)
  else
    AHotZone.Activate(Controller, AHitTestResult);
end;

function TdxRichEditMouseDefaultState.ShouldExtendSelectionToCursor: Boolean;
begin
  Result := Controller.IsShiftPressed;
end;

function TdxRichEditMouseDefaultState.TableMouseDown(
  ASelectionManager: TdxEnhancedSelectionManager;
  const AHitTestResult: TdxRichEditHitTestResult): Boolean;
var
  ATableRow: TdxTableRowViewInfoBase;
begin
  Result := False;
  ATableRow := ASelectionManager.CalculateTableRowToResize(AHitTestResult);
  if ASelectionManager.ShouldResizeTableRow(Control, AHitTestResult, ATableRow) then
  begin
    Result := True;
    Assert(False, 'not implemented');
  end;

  if ASelectionManager.ShouldSelectEntireTableColumn(AHitTestResult) then
  begin
    Result := True;
    Assert(False, 'not implemented');
  end
  else
    if ASelectionManager.ShouldSelectEntireTableRow(AHitTestResult) then
    begin
      Result := True;
      BeginTableRowsSelectionState(AHitTestResult);
    end
    else
      if ASelectionManager.ShouldSelectEntireTableCell(AHitTestResult) then
      begin
        Result := True;
        Assert(False, 'not implemented');
      end;
end;

function TdxRichEditMouseDefaultState.TryProcessHyperlinkClick(AHitTestResult: TdxRichEditHitTestResult): Boolean;
var
  AHandler: TdxHyperlinkMouseClickHandler;
begin
  Result := False;
  if AHitTestResult.PieceTable <> ActivePieceTable then
    Exit;
  AHandler := TdxHyperlinkMouseClickHandler.Create(Control);
  try
    Result := AHandler.TryProcessHyperlinkClick(AHitTestResult);
  finally
    AHandler.Free;
  end;
end;

procedure TdxRichEditMouseDefaultState.ValidateFieldSelection(ADocumentModel: TdxDocumentModel; ASelection: TdxSelection);
var
  AValidator: TdxFieldIsSelectValidator;
begin
  AValidator := TdxFieldIsSelectValidator.Create(ADocumentModel.ActivePieceTable);
  try
    AValidator.ValidateSelection(ASelection);
  finally
    AValidator.Free;
  end;
end;

{ TdxBoxAccessor }

function TdxBoxAccessor.GetBox(
  AHitTestResult: TdxRichEditHitTestResult): TdxBox;
begin
  Result := AHitTestResult.Box;
end;

{ TdxRowBoxAccessor }

function TdxRowBoxAccessor.GetBox(
  AHitTestResult: TdxRichEditHitTestResult): TdxBox;
begin
  Result := AHitTestResult.Row;
end;

{ TdxCharacterBoxAccessor }

function TdxCharacterBoxAccessor.GetBox(
  AHitTestResult: TdxRichEditHitTestResult): TdxBox;
begin
  Result := AHitTestResult.Character;
end;

{ TdxContinueSelectionByRangesMouseHandlerState }

constructor TdxContinueSelectionByRangesMouseHandlerState.Create(
  AController: TdxRichEditMouseController;
  ABoxAccessorClass: TdxBoxAccessorClass;
  AInitialHitTestResult: TdxRichEditHitTestResult);
begin
  inherited Create(AController);
  FInitialHitTestResult := TdxRichEditHitTestResult(AInitialHitTestResult.Clone);
  FInitialAccessor := ABoxAccessorClass.Create;
  FStartCell := CalculateStartCell;
end;

destructor TdxContinueSelectionByRangesMouseHandlerState.Destroy;
begin
  FreeAndNil(FInitialHitTestResult);
  FreeAndNil(FInitialAccessor);
  inherited Destroy;
end;

procedure TdxContinueSelectionByRangesMouseHandlerState.Start;
begin
  inherited Start;
  DocumentModel.EndDocumentUpdate.Add(EndDocumentUpdateHandler);
end;

function TdxContinueSelectionByRangesMouseHandlerState.CanShowToolTip: Boolean;
begin
  Result := True;
end;

procedure TdxContinueSelectionByRangesMouseHandlerState.HandleEndDocumentUpdate(
  const Args: TdxDocumentUpdateCompleteEventArgs);
begin
  EndDocumentUpdateHandler(Self, Args);
end;

procedure TdxContinueSelectionByRangesMouseHandlerState.HandleMouseMove(const Args: TdxMouseEventArgs);
begin
  ContinueSelection(Args);
  SetMouseCursor(CalculateCursor(Args));
end;

procedure TdxContinueSelectionByRangesMouseHandlerState.HandleMouseUp(const Args: TdxMouseEventArgs);
begin
  Controller.SwitchToDefaultState;
end;

procedure TdxContinueSelectionByRangesMouseHandlerState.HandleMouseWheel(const Args: TdxMouseEventArgs);
begin
  inherited HandleMouseWheel(Args);
  ContinueSelection(Args);
end;

procedure TdxContinueSelectionByRangesMouseHandlerState.Finish;
var
  ACommand: TdxChangeFontBackColorByMouseCommand;
begin
  inherited Finish;
  DocumentModel.EndDocumentUpdate.Remove(EndDocumentUpdateHandler);
  if TdxChangeFontBackColorByMouseCommand.IsChangeByMouse then
  begin
    ACommand := TdxChangeFontBackColorByMouseCommand.Create(Control);
    try
      ACommand.Execute;
    finally
      ACommand.Free;
    end;
  end;
end;

function TdxContinueSelectionByRangesMouseHandlerState.AutoScrollEnabled: Boolean;
begin
  Result := True;
end;

function TdxContinueSelectionByRangesMouseHandlerState.CalculateCursor(
  const Args: TdxMouseEventArgs): TCursor;
var
  ACalculator: TdxMouseCursorCalculator;
  AHitTestResult: TdxRichEditHitTestResult;
begin
  ACalculator := Control.InnerControl.CreateMouseCursorCalculator;
  try
    AHitTestResult := CalculateHitTest(Args.MousePos);
    try
      Result := ACalculator.Calculate(AHitTestResult, Args.MousePos);
    finally
      AHitTestResult.Free;
    end;
  finally
    ACalculator.Free;
  end;
end;

function TdxContinueSelectionByRangesMouseHandlerState.CalculateStartCell: TdxTableCell;
var
  ALogPosition: TdxDocumentLogPosition;
begin
  ALogPosition := InitialBox.GetFirstPosition(ActivePieceTable).LogPosition;
  Result := ActivePieceTable.FindParagraph(ALogPosition).GetCell;
end;

procedure TdxContinueSelectionByRangesMouseHandlerState.EndDocumentUpdateHandler(
  ASender: TObject; E: TdxDocumentUpdateCompleteEventArgs);
var
  AChangeActions: TdxDocumentModelChangeActions;
begin
  AChangeActions := E.DeferredChanges.ChangeActions;
  if (AChangeActions * [TdxDocumentModelChangeAction.ResetPrimaryLayout,
      TdxDocumentModelChangeAction.ResetAllPrimaryLayout]) <> [] then
    FIsInvalidInitialHitTestResult := True;
end;

function TdxContinueSelectionByRangesMouseHandlerState.GetInitialBox: TdxBox;
begin
  Result := FInitialAccessor.GetBox(FInitialHitTestResult);
end;

function TdxContinueSelectionByRangesMouseHandlerState.GetInitialHitTestResult: TdxRichEditHitTestResult;
begin
  if FIsInvalidInitialHitTestResult then
  begin
    FInitialHitTestResult.Free;
    FInitialHitTestResult := GetHitTestResult(FInitialHitTestResult.PhysicalPoint);
		FIsInvalidInitialHitTestResult := False;
  end;
  Result := FInitialHitTestResult;
end;

function TdxContinueSelectionByRangesMouseHandlerState.StopClickTimerOnStart: Boolean;
begin
  Result := True;
end;

procedure TdxContinueSelectionByRangesMouseHandlerState.ContinueSelection(const Args: TdxMouseEventArgs);
var
  ACommand: TdxExtendSelectionByRangesCommandBase;
begin
  ACommand := CreateExtendSelectionCommand;
  try
    ACommand.InitialBox := InitialBox;
    ACommand.PhysicalPoint := Args.MousePos;
    ACommand.Execute;
  finally
    ACommand.Free;
  end;
end;

function TdxContinueSelectionByRangesMouseHandlerState.GetCurrentCell(const P: TPoint): TdxTableCell;
var
  AParagraph: TdxParagraph;
begin
  AParagraph := GetParagraph(P);
  if AParagraph = nil then
    Result := nil
  else
    Result := AParagraph.GetCell;
end;

function TdxContinueSelectionByRangesMouseHandlerState.GetHitTestResult(const APhysicalPoint: TPoint): TdxRichEditHitTestResult;
var
  ARequest: TdxRichEditHitTestRequest;
begin
  ARequest := TdxRichEditHitTestRequest.Create(ActivePieceTable);
  try
    ARequest.PhysicalPoint := APhysicalPoint;
    ARequest.DetailsLevel := TdxDocumentLayoutDetailsLevel.Character;
    ARequest.Accuracy := NearestCharacter;
    Result := Controller.ActiveView.HitTestCore(ARequest, False);
  finally
    ARequest.Free;
  end;
end;

function TdxContinueSelectionByRangesMouseHandlerState.GetParagraph(const P: TPoint): TdxParagraph;
var
  ARequest: TdxRichEditHitTestRequest;
  AHitTestResult: TdxRichEditHitTestResult;
  ALogPosition: TdxDocumentLogPosition;
begin
  ARequest := TdxRichEditHitTestRequest.Create(ActivePieceTable);
  try
    ARequest.PhysicalPoint := P;
    ARequest.DetailsLevel := TdxDocumentLayoutDetailsLevel.Character;
    ARequest.Accuracy := NearestCharacter;
    AHitTestResult := InnerControl.ActiveView.HitTestCore(ARequest, False);
    try
      if not AHitTestResult.IsValid(TdxDocumentLayoutDetailsLevel.Character) then
        Result := nil
      else
      begin
        ALogPosition := AHitTestResult.Character.GetFirstPosition(ActivePieceTable).LogPosition;
        Result := ActivePieceTable.FindParagraph(ALogPosition);
      end;
    finally
      AHitTestResult.Free;
    end;
  finally
    ARequest.Free;
  end;
end;

function TdxContinueSelectionByRangesMouseHandlerState.NeedSwitchStateToTableCellsMouseHandlerState(const P: TPoint): Boolean;
var
  ACurrentParagraph: TdxParagraph;
begin
  ACurrentParagraph := GetParagraph(P);
  if (StartCell = nil) or (ACurrentParagraph = nil) or (ACurrentParagraph.GetCell = nil) then
    Result := False
  else
    Result := NeedSwitchStateToTableCellsMouseHandlerStateCore(ACurrentParagraph);
end;

function TdxContinueSelectionByRangesMouseHandlerState.NeedSwitchStateToTableCellsMouseHandlerStateCore(ACurrentParagraph: TdxParagraph): Boolean;
var
  ACellManager: TdxTableCellsManager;
  ActualCurrentCell, ACellConsiderNestedLevel: TdxTableCell;
begin
  ACellManager := ActivePieceTable.TableCellsManager;
  ActualCurrentCell := ACellManager.GetCellByNestingLevel(ACurrentParagraph.Index, StartCell.Table.NestedLevel);
  ACellConsiderNestedLevel := StartCell;
  if ActualCurrentCell.Table.NestedLevel <> StartCell.Table.NestedLevel then
    ACellConsiderNestedLevel := ACellManager.GetCellByNestingLevel(StartCell.StartParagraphIndex, ActualCurrentCell.Table.NestedLevel);
  Result := (ACellConsiderNestedLevel <> ActualCurrentCell) and (ACellConsiderNestedLevel.Table = ActualCurrentCell.Table);
end;

{ TdxContinueSelectionByCharactersMouseHandlerState }

procedure TdxContinueSelectionByCharactersMouseHandlerState.ContinueSelection(const Args: TdxMouseEventArgs);
var
  APhysicalPoint: TPoint;
  ACurrentCell: TdxTableCell;
begin
  APhysicalPoint := Args.MousePos;
  ACurrentCell := GetCurrentCell(APhysicalPoint);
  if NeedSwitchStateToTableCellsMouseHandlerState(APhysicalPoint) then
  begin
    Assert(False, 'not implemented: need TdxContinueSelectionByTableCellsMouseHandlerState');
  end
  else
  begin
    if ACurrentCell <> nil then 
    begin
      Assert(False, 'not implemented: need TdxTableCell');
    end;
  end;
  inherited ContinueSelection(Args);
end;

function TdxContinueSelectionByCharactersMouseHandlerState.CreateExtendSelectionCommand: TdxExtendSelectionByRangesCommandBase;
begin
  Result := TdxExtendSelectionByCharactersCommand.Create(Control);
end;

{ TdxContinueSelectionByWordsAndParagraphsMouseHandlerStateBase }

procedure TdxContinueSelectionByWordsAndParagraphsMouseHandlerStateBase.ContinueSelection(const Args: TdxMouseEventArgs);
var
  ACell: TdxTableCell;
  P: TPoint;
begin
  P := Args.MousePos;
  ACell := GetCurrentCell(P);
  if ACell <> nil then
  begin
    Assert(False);
  end;
  inherited ContinueSelection(Args);
end;

{ TdxHyperlinkMouseClickHandler }

constructor TdxHyperlinkMouseClickHandler.Create(AControl: IdxRichEditControl);
begin
  inherited Create;
  FControl := AControl;
end;

procedure TdxHyperlinkMouseClickHandler.HandleMouseUp(
  const Args: TdxMouseEventArgs);
var
  AHitTestResult: TdxRichEditHitTestResult;
begin
  AHitTestResult := Control.InnerControl.ActiveView.CalculateHitTest(Args.MousePos, TdxDocumentLayoutDetailsLevel.Box);
  try
    if (AHitTestResult = nil) or not AHitTestResult.IsValid(TdxDocumentLayoutDetailsLevel.Box) then
      Exit;
    if Args.Buttons = [mbLeft] then
      TryProcessHyperlinkClick(AHitTestResult);
  finally
    AHitTestResult.Free;
  end;
end;

function TdxHyperlinkMouseClickHandler.TryProcessHyperlinkClick(
  AHitTestResult: TdxRichEditHitTestResult): Boolean;
var
  AField: TdxField;
begin
  AField := Control.InnerControl.ActiveView.GetHyperlinkField(AHitTestResult);

  if AField = nil then
  begin
    Result := False;
    Exit;
  end;
  Result := Boolean(NotImplemented);
Assert(False, 'not implemented: need TdxField');

end;

{ TdxContinueSelectionByWordsMouseHandlerState }

function TdxContinueSelectionByWordsMouseHandlerState.CreateExtendSelectionCommand: TdxExtendSelectionByRangesCommandBase;
begin
  Result := TdxExtendSelectionByWordsCommand.Create(Control);
end;

{ TdxContinueSelectionByParagraphsMouseHandlerState }

function TdxContinueSelectionByParagraphsMouseHandlerState.CreateExtendSelectionCommand: TdxExtendSelectionByRangesCommandBase;
begin
  Result := TdxExtendSelectionByParagraphsCommand.Create(Control);
end;

{ TdxContinueSelectionByLinesMouseHandlerState }

function TdxContinueSelectionByLinesMouseHandlerState.CalculateCursor(
  const Args: TdxMouseEventArgs): TCursor;
begin
  Result := TdxRichEditCursors.SelectRow;
end;

procedure TdxContinueSelectionByLinesMouseHandlerState.ContinueSelection(
  const Args: TdxMouseEventArgs);
begin
  if StartCell <> nil then
  begin
    Assert(False);
    end
    else
      inherited ContinueSelection(Args);
end;

function TdxContinueSelectionByLinesMouseHandlerState.CreateExtendSelectionCommand: TdxExtendSelectionByRangesCommandBase;
begin
  Result := TdxExtendSelectionByLinesCommand.Create(Control);
end;

end.

