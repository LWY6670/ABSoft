{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Utils.Encoding;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  SysUtils, Windows, Generics.Collections;

type
  { TdxEncoding }

  TdxEncoding = class(TEncoding)
  protected
    class function GetEncodes: TList<TEncoding>;
  public
    class function CharsetFromCodePage(ACodePage: Integer): Integer;
    class function CodePageFromCharset(ACharset: Integer): Integer;
    class function GetEncodingFromCodePage(ACodePage: Cardinal): TEncoding;
    class function GetEncodingFromString(const AValue: string): TEncoding;
    class function GetEncodingCodePage(AEncoding: TEncoding): Cardinal;

    class function CanBeLoselesslyEncoded(const AValue: string; AEncoding: TEncoding): Boolean;
  end;

  { TdxCharsetAndCodePageTranslator }

  TdxCharsetAndCodePageTranslator = class abstract
  public
    function CharsetFromCodePage(ACodePage: Integer): Integer; virtual; abstract;
  end;

function dxCharsetAndCodePageTranslator: TdxCharsetAndCodePageTranslator;

implementation

var
  FCharsetAndCodePageTranslator: TdxCharsetAndCodePageTranslator;

type
  { TdxFullTrustCharsetAndCodePageTranslator }

  TdxFullTrustCharsetAndCodePageTranslator = class(TdxCharsetAndCodePageTranslator)
  strict private
    FCharsetToCodePage: TDictionary<Integer, Integer>;
    FCodePageToCharset: TDictionary<Integer, Integer>;
  protected
    function CharsetFromCodePageCore(ACodePage: Integer): Integer;
  public
    constructor Create;
    destructor Destroy; override;

    function CharsetFromCodePage(ACodePage: Integer): Integer; override;
  end;

function dxCharsetAndCodePageTranslator: TdxCharsetAndCodePageTranslator;
begin
  if FCharsetAndCodePageTranslator = nil then
  begin
    FCharsetAndCodePageTranslator := TdxFullTrustCharsetAndCodePageTranslator.Create;
  end;
  Result := FCharsetAndCodePageTranslator;
end;

{ TdxFullTrustCharsetAndCodePageTranslator }

constructor TdxFullTrustCharsetAndCodePageTranslator.Create;
begin
  inherited Create;
  FCharsetToCodePage := TDictionary<Integer, Integer>.Create;
  FCodePageToCharset := TDictionary<Integer, Integer>.Create;
end;

destructor TdxFullTrustCharsetAndCodePageTranslator.Destroy;
begin
  FreeAndNil(FCharsetToCodePage);
  FreeAndNil(FCodePageToCharset);
  inherited Destroy;
end;

function TdxFullTrustCharsetAndCodePageTranslator.CharsetFromCodePageCore(
  ACodePage: Integer): Integer;
var
  AInfo: TCharsetInfo;
  P: DWORD;
begin
  P := DWORD(Pointer(ACodePage));
  if not TranslateCharsetInfo(P, AInfo, TCI_SRCCODEPAGE) then
    Result := 0
  else
    Result := AInfo.ciCharset;
end;

function TdxFullTrustCharsetAndCodePageTranslator.CharsetFromCodePage(ACodePage: Integer): Integer;
begin
  if not FCodePageToCharset.TryGetValue(ACodePage, Result) then
  begin
    Result := CharsetFromCodePageCore(ACodePage);
    FCodePageToCharset.AddOrSetValue(ACodePage, Result);
  end;
end;

{ TdxEncoding }

class function TdxEncoding.CanBeLoselesslyEncoded(const AValue: string;
  AEncoding: TEncoding): Boolean;
var
  ABytes: TBytes;
  ADecodedValue: string;
begin
  ABytes := AEncoding.GetBytes(AValue);
  ADecodedValue := AEncoding.GetString(ABytes, 0, Length(ABytes));
  Result := CompareStr(AValue, ADecodedValue) = 0;
end;

class function TdxEncoding.CharsetFromCodePage(ACodePage: Integer): Integer;
begin
  Result := dxCharsetAndCodePageTranslator.CharsetFromCodePage(ACodePage);
end;

class function TdxEncoding.CodePageFromCharset(ACharset: Integer): Integer;
begin
  Result := -1;
  Assert(False, 'not implemented');
end;

class function TdxEncoding.GetEncodes: TList<TEncoding>;
begin
  Result := TList<TEncoding>.Create;
  Result.Add(ASCII);
  Result.Add(BigEndianUnicode);
  Result.Add(Unicode);
  Result.Add(UTF7);
  Result.Add(UTF8);
end;

class function TdxEncoding.GetEncodingCodePage(AEncoding: TEncoding): Cardinal;
begin
  Result := AEncoding.CodePage;
end;

class function TdxEncoding.GetEncodingFromCodePage(
  ACodePage: Cardinal): TEncoding;
var
  I: Integer;
  AList: TList<TEncoding>;
begin
  AList := GetEncodes;
  try
    Result := Unicode;
    for I := 0 to AList.Count - 1 do
      if GetEncodingCodePage(AList[I]) = ACodePage then
      begin
        Result := AList[I];
        Break;
      end;
  finally
    AList.Free;
  end;
end;

class function TdxEncoding.GetEncodingFromString(const AValue: string): TEncoding;
var
  AEncodes: TList<TEncoding>;
  I: Integer;
begin
  Result := nil;
  AEncodes := TdxEncoding.GetEncodes;
  try
    for I := 0 to AEncodes.Count - 1 do
      if TdxEncoding.CanBeLoselesslyEncoded(AValue, AEncodes[I]) then
      begin
        Result := AEncodes[I];
        Break;
      end;
  finally
    AEncodes.Free;
  end;
end;

initialization

finalization
  FreeAndNil(FCharsetAndCodePageTranslator);

end.
