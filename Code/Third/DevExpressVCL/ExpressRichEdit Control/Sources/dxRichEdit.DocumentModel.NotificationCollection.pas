{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.DocumentModel.NotificationCollection;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  SysUtils, Classes, Types, dxCore, dxCoreClasses, Generics.Collections, Generics.Defaults,
  dxRichEdit.Utils.BatchUpdateHelper, dxRichEdit.DocumentModel.Core;

type

  TDXCollectionUniquenessProviderType = (
    None,
    MinimizeMemoryUsage
  );

  TdxCollectionChangedAction = (
    Add,
    Remove,
    Changed,
    Clear,
    EndBatchUpdate
  );

  { TdxCollectionChangedEventArgs<T> }

  TdxCollectionChangedEventArgs<T> = class(TdxEventArgs) 
  private
    FAction: TdxCollectionChangedAction; 
    FElement: T; 
  public
    constructor Create(AAction: TdxCollectionChangedAction; AElement: T); 

    property Action: TdxCollectionChangedAction read FAction; 
    property Element: T read FElement; 
  end;

  { TdxCollectionChangingEventArgs<T> }

  TdxCollectionChangingEventArgs<T> = class(TdxCollectionChangedEventArgs<T>) 
  private
    FCancel: Boolean; 
    FNewValue: T; 
    FOldValue: T; 
    FPropertyName: string; 
  public
    property Cancel: Boolean read FCancel write FCancel; 
    property NewValue: T read FNewValue write FNewValue; 
    property OldValue: T read FOldValue write FOldValue; 
    property PropertyName: string read FPropertyName write FPropertyName; 
  end;

  TdxCollectionChangingEventHandler<T> = procedure(ASender: TObject; const E: TdxCollectionChangingEventArgs<T>) of object; 
  TdxCollectionChangedEventHandler<T> = procedure(ASender: TObject; const E: TdxCollectionChangedEventArgs<T>) of object; 

  { TDXCollectionUniquenessProvider<T> }

  TDXCollectionUniquenessProvider<T> = class
  protected
    function GetType: TDXCollectionUniquenessProviderType; virtual; abstract;
  public
    function LookupObjectIndex(AValue: T): Integer; virtual; abstract; 
    function LookupObject(AValue: T): Boolean; virtual; abstract; 
    procedure OnClearComplete; virtual; abstract;
    procedure OnInsertComplete(AValue: T); virtual; abstract; 
    procedure OnRemoveComplete(AValue: T); virtual; abstract; 
    procedure OnSetComplete(AOldValue, ANewValue: T); virtual; abstract; 

    property &Type: TDXCollectionUniquenessProviderType read GetType; 
  end;

  { TdxEmptyUniquenessProvider<T> }

  TdxEmptyUniquenessProvider<T> = class(TDXCollectionUniquenessProvider<T>) 
  protected
    function GetType: TDXCollectionUniquenessProviderType; override; 
  public
    function LookupObjectIndex(AValue: T): Integer; override; 
    function LookupObject(AValue: T): Boolean; override; 
    procedure OnClearComplete; override; 
    procedure OnInsertComplete(AValue: T); override; 
    procedure OnRemoveComplete(AValue: T); override; 
    procedure OnSetComplete(AOldValue, ANewValue: T); override; 
  end;

  { TDXCollectionBase<T> }

  TDXCollectionBase<T: class> = class(TcxIUnknownObject) 
  private
    FList: TList<T>; 
    FUniquenessProvider: TDXCollectionUniquenessProvider<T>; 
    function GetCount: Integer;
    function GetUniquenessProviderType: TDXCollectionUniquenessProviderType;
    procedure SetUniquenessProviderType(const Value: TDXCollectionUniquenessProviderType);
    function GetCapacity: Integer;
    procedure SetCapacity(const Value: Integer);
  protected
    function CreateUniquenessProvider(AStrategy: TDXCollectionUniquenessProviderType): TDXCollectionUniquenessProvider<T>; 
    function AddIfNotAlreadyAdded(AObj: T): Integer; virtual; 
    function BinarySearch(AItem: T; out AIndex: Integer; AComparer: IComparer<T>): Boolean; 
    procedure InsertIfNotAlreadyInserted(AIndex: Integer; AObj: T); virtual; 
    function RemoveIfAlreadyAdded(AObj: T): Boolean; virtual; 
    function OnRemove(AIndex: Integer; AValue: T): Boolean; virtual; 
    procedure OnRemoveComplete(AIndex: Integer; AValue: T); virtual; 
    function OnSet(AIndex: Integer; AOldValue, ANewValue: T): Boolean; virtual; 
    procedure OnSetComplete(AIndex: Integer; AOldValue, ANewValue: T); virtual; 
    function OnClear: Boolean; virtual; 
    procedure OnClearComplete; virtual; 
    function OnInsert(AIndex: Integer; AValue: T): Boolean; virtual; 
    procedure OnInsertComplete(AIndex: Integer; AValue: T); virtual; 
    function GetItem(AIndex: Integer): T; virtual; 
    procedure SetItem(AIndex: Integer; AValue: T); virtual; 
    function AddCore(AValue: T): Integer; virtual; 
    procedure InsertCore(AIndex: Integer; AValue: T); 
    procedure RemoveCore(AValue: T); virtual; 
    procedure OnValidate(AValue: T); 

    function RemoveAtCore(AIndex: Integer): Boolean; 

    property InnerList: TList<T> read FList; 
    property UniquenessProvider: TDXCollectionUniquenessProvider<T> read FUniquenessProvider; 
  public
    constructor Create; 
    destructor Destroy; override;
    procedure Clear; 
    procedure RemoveAt(AIndex: Integer); 
    function Remove(AValue: T): Boolean; virtual; 
    function Add(AValue: T): Integer; virtual; 
    procedure Insert(AIndex: Integer; AValue: T); 
    function Contains(AValue: T): Boolean; 
    function IndexOf(const AValue: T): Integer; 
    procedure Sort(AComparer: IComparer<T>); 

    property Count: Integer read GetCount; 
    property Capacity: Integer read GetCapacity write SetCapacity; 
    property UniquenessProviderType: TDXCollectionUniquenessProviderType read GetUniquenessProviderType write SetUniquenessProviderType; 
  end;

  { TdxSimpleUniquenessProvider<T> }

  TdxSimpleUniquenessProvider<T: class> = class(TDXCollectionUniquenessProvider<T>) 
  private
    FCollection: TDXCollectionBase<T>; 
  protected
    function GetType: TDXCollectionUniquenessProviderType; override; 

    property Collection: TDXCollectionBase<T> read FCollection; 
  public
    constructor Create(ACollection: TDXCollectionBase<T>); 
    function LookupObjectIndex(AValue: T): Integer; override; 
    function LookupObject(AValue: T): Boolean; override; 
    procedure OnClearComplete; override; 
    procedure OnInsertComplete(AValue: T); override; 
    procedure OnRemoveComplete(AValue: T); override; 
    procedure OnSetComplete(AOldValue, ANewValue: T); override; 
  end;

  { TDXCollection<T> }

  TDXCollection<T: class> = class(TDXCollectionBase<T>) 
  protected
    procedure SetItem(AIndex: Integer; AValue: T); override; 
  public
    property Self[Index: Integer]: T read GetItem; default; 
  end;

  { TdxNotificationCollection<T> }

  TdxNotificationCollection<T: class> = class(TDXCollection<T>, IdxBatchUpdateable, IdxBatchUpdateHandler) 
  private
    FBatchUpdateHelper: TdxBatchUpdateHelper; 
    FChanged: Boolean; 
    FChangeCancelled: Boolean; 
    FOnBeginBatchUpdate: TdxEventHandler; 
    FOnEndBatchUpdate: TdxEventHandler; 
    FOnCancelBatchUpdate: TdxEventHandler; 
    FOnCollectionChanging: TdxMulticastMethod<TdxCollectionChangingEventHandler<T>>; 
    FOnCollectionChanged: TdxMulticastMethod<TdxCollectionChangedEventHandler<T>>; 
  protected
    procedure OnCollectionChanged(E: TdxCollectionChangedEventArgs<T>); virtual; 
    procedure RaiseCollectionChanged(E: TdxCollectionChangedEventArgs<T>); virtual; 
    procedure OnCollectionChanging(E: TdxCollectionChangingEventArgs<T>); virtual; 
    procedure RaiseCollectionChanging(E: TdxCollectionChangingEventArgs<T>); virtual; 
    procedure OnFirstBeginUpdate; 
    procedure OnLastEndUpdate; 
    procedure OnLastCancelUpdate; 
    procedure RaiseBeginBatchUpdate; virtual; 
    procedure RaiseEndBatchUpdate; virtual; 
    procedure RaiseCancelBatchUpdate; virtual; 
    function OnInsert(AIndex: Integer; AValue: T): Boolean; override; 
    procedure OnInsertComplete(AIndex: Integer; AValue: T); override; 
    function OnRemove(AIndex: Integer; AValue: T): Boolean; override; 
    procedure OnRemoveComplete(AIndex: Integer; AValue: T); override; 
    function OnClear: Boolean; override; 
    procedure OnClearComplete; override; 
  public
    constructor Create; 
    destructor Destroy; override;

    procedure BeginUpdate; 
    procedure EndUpdate; 
    procedure CancelUpdate; 
    function GetBatchUpdateHelper: TdxBatchUpdateHelper; 
    function GetIsUpdateLocked: Boolean; 

    procedure OnBeginUpdate; 
    procedure OnEndUpdate; 
    procedure OnCancelUpdate; 


    property IsUpdateLocked: Boolean read GetIsUpdateLocked;
    property CollectionChanged: TdxMulticastMethod<TdxCollectionChangedEventHandler<T>> read FOnCollectionChanged; 
    property CollectionChanging: TdxMulticastMethod<TdxCollectionChangingEventHandler<T>> read FOnCollectionChanging; 
  end;

implementation

uses
  RTLConsts;

{ TdxCollectionChangedEventArgs<T> }

constructor TdxCollectionChangedEventArgs<T>.Create(AAction: TdxCollectionChangedAction; AElement: T);
begin
  inherited Create;
  FAction := AAction;
  FElement := AElement;
end;

{ TdxEmptyUniquenessProvider<T> }

function TdxEmptyUniquenessProvider<T>.GetType: TDXCollectionUniquenessProviderType;
begin
  Result := TDXCollectionUniquenessProviderType.None;
end;

function TdxEmptyUniquenessProvider<T>.LookupObject(AValue: T): Boolean;
begin
  Result := False;
end;

function TdxEmptyUniquenessProvider<T>.LookupObjectIndex(AValue: T): Integer;
begin
  Result := -1;
end;

procedure TdxEmptyUniquenessProvider<T>.OnClearComplete;
begin
end;

procedure TdxEmptyUniquenessProvider<T>.OnInsertComplete(AValue: T);
begin
end;

procedure TdxEmptyUniquenessProvider<T>.OnRemoveComplete(AValue: T);
begin
end;

procedure TdxEmptyUniquenessProvider<T>.OnSetComplete(AOldValue, ANewValue: T);
begin
end;

{ TDXCollectionBase<T> }

function TDXCollectionBase<T>.Add(AValue: T): Integer;
begin
  Result := AddIfNotAlreadyAdded(AValue);
end;

function TDXCollectionBase<T>.AddCore(AValue: T): Integer;
begin
  OnValidate(AValue);
  if not OnInsert(InnerList.Count, AValue) then
    Exit(-1);
  Result := InnerList.Count;
  InnerList.Add(AValue);
  try
    OnInsertComplete(Result, AValue);
  except
    InnerList.Delete(Result);
    raise;
  end;
end;

function TDXCollectionBase<T>.AddIfNotAlreadyAdded(AObj: T): Integer;
var
  AIndex: Integer;
begin
  AIndex := FUniquenessProvider.LookupObjectIndex(AObj);
  if AIndex >= 0 then
    Result := AIndex
  else
    Result := AddCore(AObj);
end;

function TDXCollectionBase<T>.BinarySearch(AItem: T; out AIndex: Integer; AComparer: IComparer<T>): Boolean;
begin
  Result := FList.BinarySearch(AItem, AIndex, AComparer);
end;

procedure TDXCollectionBase<T>.Clear;
begin
  if not OnClear then
    Exit;
  InnerList.Clear;
  OnClearComplete;
end;

function TDXCollectionBase<T>.Contains(AValue: T): Boolean;
begin
  Result := InnerList.Contains(AValue);
end;

constructor TDXCollectionBase<T>.Create;
begin
  inherited Create;
  FList := TList<T>.Create;
  UniquenessProviderType := TDXCollectionUniquenessProviderType.MinimizeMemoryUsage;
end;

function TDXCollectionBase<T>.CreateUniquenessProvider(
  AStrategy: TDXCollectionUniquenessProviderType): TDXCollectionUniquenessProvider<T>;
begin
  case AStrategy of
    TDXCollectionUniquenessProviderType.MinimizeMemoryUsage:
      Result := TdxSimpleUniquenessProvider<T>.Create(Self);
    else 
      Result := TdxEmptyUniquenessProvider<T>.Create;
  end;
end;

destructor TDXCollectionBase<T>.Destroy;
begin
  FreeAndNil(FUniquenessProvider);
  FList.Free;
  inherited Destroy;
end;

function TDXCollectionBase<T>.GetCapacity: Integer;
begin
  Result := FList.Capacity;
end;

function TDXCollectionBase<T>.GetCount: Integer;
begin
  Result := InnerList.Count;
end;

function TDXCollectionBase<T>.GetItem(AIndex: Integer): T;
begin
  if (AIndex < 0) or (AIndex >= InnerList.Count) then
    raise EArgumentOutOfRangeException.CreateRes(@SArgumentOutOfRange);
  Result := InnerList[AIndex];
end;

function TDXCollectionBase<T>.GetUniquenessProviderType: TDXCollectionUniquenessProviderType;
begin
  Result := FUniquenessProvider.&Type;
end;

function TDXCollectionBase<T>.IndexOf(const AValue: T): Integer;
begin
  Result := InnerList.IndexOf(AValue);
end;

procedure TDXCollectionBase<T>.Insert(AIndex: Integer; AValue: T);
begin
  InsertIfNotAlreadyInserted(AIndex, AValue);
end;

procedure TDXCollectionBase<T>.InsertCore(AIndex: Integer; AValue: T);
begin
  if (AIndex < 0) or (AIndex > InnerList.Count) then
    raise EArgumentOutOfRangeException.CreateRes(@SArgumentOutOfRange);

  OnValidate(AValue);
  if not OnInsert(AIndex, AValue) then
    Exit;

  InnerList.Insert(AIndex, AValue);
  try
    OnInsertComplete(AIndex, AValue);
  except
    InnerList.Delete(AIndex);
    raise;
  end;
end;

procedure TDXCollectionBase<T>.InsertIfNotAlreadyInserted(AIndex: Integer; AObj: T);
begin
  if FUniquenessProvider.LookupObject(AObj) then
    raise Exception.Create(''); 
  InsertCore(AIndex, AObj);
end;

function TDXCollectionBase<T>.OnClear: Boolean;
begin
  Result := True;
end;

procedure TDXCollectionBase<T>.OnClearComplete;
begin
  FUniquenessProvider.OnClearComplete;
end;

function TDXCollectionBase<T>.OnInsert(AIndex: Integer; AValue: T): Boolean;
begin
  Result := True;
end;

procedure TDXCollectionBase<T>.OnInsertComplete(AIndex: Integer; AValue: T);
begin
  FUniquenessProvider.OnInsertComplete(AValue);
end;

function TDXCollectionBase<T>.OnRemove(AIndex: Integer; AValue: T): Boolean;
begin
  Result := True;
end;

procedure TDXCollectionBase<T>.OnRemoveComplete(AIndex: Integer; AValue: T);
begin
  FUniquenessProvider.OnRemoveComplete(AValue);
end;

function TDXCollectionBase<T>.OnSet(AIndex: Integer; AOldValue, ANewValue: T): Boolean;
begin
  Result := not FUniquenessProvider.LookupObject(ANewValue);
end;

procedure TDXCollectionBase<T>.OnSetComplete(AIndex: Integer; AOldValue, ANewValue: T);
begin
  FUniquenessProvider.OnSetComplete(AOldValue, ANewValue);
end;

procedure TDXCollectionBase<T>.OnValidate(AValue: T);
begin
  if AValue = nil then
    raise Exception.Create('Value is null'); 
end;

function TDXCollectionBase<T>.Remove(AValue: T): Boolean;
begin
  Result := RemoveIfAlreadyAdded(AValue);
end;

procedure TDXCollectionBase<T>.RemoveAt(AIndex: Integer);
begin
  RemoveAtCore(AIndex);
end;

function TDXCollectionBase<T>.RemoveAtCore(AIndex: Integer): Boolean;
var
  AObj: T;
begin
  if (AIndex < 0) or (AIndex >= InnerList.Count) then
    raise EArgumentOutOfRangeException.CreateRes(@SArgumentOutOfRange);
  AObj := InnerList[AIndex];
  OnValidate(AObj);
  if not OnRemove(AIndex, AObj) then
    Exit(False);
  InnerList.Delete(AIndex); 
  OnRemoveComplete(AIndex, AObj);
  Result := True;
end;

procedure TDXCollectionBase<T>.RemoveCore(AValue: T);
var
  AIndex: Integer;
begin
  OnValidate(AValue);
  AIndex := FUniquenessProvider.LookupObjectIndex(AValue);
  if AIndex < 0 then
    raise EArgumentOutOfRangeException.CreateRes(@SArgumentOutOfRange); 
  if not OnRemove(AIndex, AValue) then
    Exit;
  InnerList.Delete(AIndex);
  OnRemoveComplete(AIndex, AValue);
end;

function TDXCollectionBase<T>.RemoveIfAlreadyAdded(AObj: T): Boolean;
var
  AIndex: Integer;
begin
  if AObj = nil then
    raise Exception.Create(''); 
  AIndex := FList.IndexOf(AObj);
  if AIndex >= 0 then
    Result := RemoveAtCore(AIndex)
  else
    Result := False;
end;

procedure TDXCollectionBase<T>.SetCapacity(const Value: Integer);
begin
  FList.Capacity := Value;
end;

procedure TDXCollectionBase<T>.SetItem(AIndex: Integer; AValue: T);
var
  AObj: T;
begin
  if (AIndex < 0) or (AIndex >= InnerList.Count) then
    raise EArgumentOutOfRangeException.CreateRes(@SArgumentOutOfRange);
  OnValidate(AValue);
  AObj := InnerList[AIndex];
  if not OnSet(AIndex, AObj, AValue) then
    Exit;
  InnerList[AIndex] := AValue;
  try
    OnSetComplete(AIndex, AObj, AValue);
  except
    InnerList[AIndex] := AObj;
    raise;
  end;
end;

procedure TDXCollectionBase<T>.SetUniquenessProviderType(const Value: TDXCollectionUniquenessProviderType);
begin
  if (InnerList <> nil) and (Count > 0) then
    raise Exception.Create('Can''t change uniqueness type for non-empty collection');
  FreeAndNil(FUniquenessProvider);
  FUniquenessProvider := CreateUniquenessProvider(Value);
end;

procedure TDXCollectionBase<T>.Sort(AComparer: IComparer<T>);
begin
  FList.Sort(AComparer);
end;

{ TdxSimpleUniquenessProvider<T> }

constructor TdxSimpleUniquenessProvider<T>.Create(ACollection: TDXCollectionBase<T>);
begin
  inherited Create;
  Assert(ACollection <> nil); 
  FCollection := ACollection;
end;

function TdxSimpleUniquenessProvider<T>.GetType: TDXCollectionUniquenessProviderType;
begin
  Result := TDXCollectionUniquenessProviderType.MinimizeMemoryUsage;
end;

function TdxSimpleUniquenessProvider<T>.LookupObject(AValue: T): Boolean;
begin
  Result := FCollection.Contains(AValue);
end;

function TdxSimpleUniquenessProvider<T>.LookupObjectIndex(AValue: T): Integer;
begin
  Result := FCollection.IndexOf(AValue);
end;

procedure TdxSimpleUniquenessProvider<T>.OnClearComplete;
begin
end;

procedure TdxSimpleUniquenessProvider<T>.OnInsertComplete(AValue: T);
begin
end;

procedure TdxSimpleUniquenessProvider<T>.OnRemoveComplete(AValue: T);
begin
end;

procedure TdxSimpleUniquenessProvider<T>.OnSetComplete(AOldValue, ANewValue: T);
begin
end;

{ TDXCollection<T> }

procedure TDXCollection<T>.SetItem(AIndex: Integer; AValue: T);
begin
  raise Exception.Create('Can''t set item for this collection. This collection is read-only.');
end;

{ TdxNotificationCollection<T> }

procedure TdxNotificationCollection<T>.BeginUpdate;
begin
  FBatchUpdateHelper.BeginUpdate;
end;

procedure TdxNotificationCollection<T>.CancelUpdate;
begin
  FBatchUpdateHelper.CancelUpdate;
end;

constructor TdxNotificationCollection<T>.Create;
begin
  inherited Create;
  FBatchUpdateHelper := TdxBatchUpdateHelper.Create(Self);
end;

destructor TdxNotificationCollection<T>.Destroy;
begin
  FBatchUpdateHelper.Free;
  inherited Destroy;
end;

procedure TdxNotificationCollection<T>.EndUpdate;
begin
  FBatchUpdateHelper.EndUpdate;
end;

function TdxNotificationCollection<T>.GetBatchUpdateHelper: TdxBatchUpdateHelper;
begin
  Result := FBatchUpdateHelper;
end;

function TdxNotificationCollection<T>.GetIsUpdateLocked: Boolean;
begin
  Result := FBatchUpdateHelper.IsUpdateLocked;
end;

procedure TdxNotificationCollection<T>.OnBeginUpdate;
begin
end;

procedure TdxNotificationCollection<T>.OnCancelUpdate;
begin
end;

function TdxNotificationCollection<T>.OnClear: Boolean;
var
  AArgs: TdxCollectionChangingEventArgs<T>;
begin
  if Count <= 0 then
    Exit(False);

  AArgs := TdxCollectionChangingEventArgs<T>.Create(TdxCollectionChangedAction.Clear, default(T));
  try
    OnCollectionChanging(AArgs);
    if AArgs.Cancel then
      Result := False
    else
      Result := inherited OnClear;
  finally
    AArgs.Free;
  end;
end;

procedure TdxNotificationCollection<T>.OnClearComplete;
var
  AArgs: TdxCollectionChangedEventArgs<T>;
begin
  inherited OnClearComplete;
  AArgs := TdxCollectionChangedEventArgs<T>.Create(TdxCollectionChangedAction.Clear, default(T));
  try
    OnCollectionChanged(AArgs);
  finally
    AArgs.Free;
  end;
end;

procedure TdxNotificationCollection<T>.OnCollectionChanged(E: TdxCollectionChangedEventArgs<T>);
begin
  if IsUpdateLocked then
    Exit;

  RaiseCollectionChanged(E);
end;

procedure TdxNotificationCollection<T>.OnCollectionChanging(E: TdxCollectionChangingEventArgs<T>);
begin
  if IsUpdateLocked then
  begin
    if not FChanged then
    begin
      RaiseCollectionChanging(E);
      FChangeCancelled := E.Cancel;
      FChanged := True;
    end
    else
      E.Cancel := FChangeCancelled;
    Exit;
  end;

  RaiseCollectionChanging(E);
end;

procedure TdxNotificationCollection<T>.OnEndUpdate;
begin
end;

procedure TdxNotificationCollection<T>.OnFirstBeginUpdate;
begin
  RaiseBeginBatchUpdate;
  FChanged := False;
  FChangeCancelled := False;
end;

function TdxNotificationCollection<T>.OnInsert(AIndex: Integer; AValue: T): Boolean;
var
  AArgs: TdxCollectionChangingEventArgs<T>;
begin
  AArgs := TdxCollectionChangingEventArgs<T>.Create(TdxCollectionChangedAction.Add, AValue);
  try
    OnCollectionChanging(AArgs);
    if AArgs.Cancel then
      Result := False
    else
      Result := inherited OnInsert(AIndex, AValue);
  finally
    AArgs.Free;
  end;
end;

procedure TdxNotificationCollection<T>.OnInsertComplete(AIndex: Integer; AValue: T);
var
  AArgs: TdxCollectionChangedEventArgs<T>;
begin
  inherited OnInsertComplete(AIndex, AValue);
  AArgs := TdxCollectionChangedEventArgs<T>.Create(TdxCollectionChangedAction.Add, AValue);
  try
    OnCollectionChanged(AArgs);
  finally
    AArgs.Free;
  end;
end;

procedure TdxNotificationCollection<T>.OnLastCancelUpdate;
begin
  RaiseCancelBatchUpdate;
end;

procedure TdxNotificationCollection<T>.OnLastEndUpdate;
var
  AArgs: TdxCollectionChangedEventArgs<T>;
begin
  RaiseEndBatchUpdate;
  if FChanged and not FChangeCancelled then
  begin
    AArgs := TdxCollectionChangedEventArgs<T>.Create(TdxCollectionChangedAction.EndBatchUpdate, default(T));
    try
      OnCollectionChanged(AArgs);
    finally
      AArgs.Free;
    end;
  end;
end;

function TdxNotificationCollection<T>.OnRemove(AIndex: Integer; AValue: T): Boolean;
var
  AArgs: TdxCollectionChangingEventArgs<T>;
begin
  AArgs := TdxCollectionChangingEventArgs<T>.Create(TdxCollectionChangedAction.Remove, AValue);
  try
    OnCollectionChanging(AArgs);
    if AArgs.Cancel then
      Exit(False);
    Result := inherited OnRemove(AIndex, AValue);
  finally
    AArgs.Free;
  end;
end;

procedure TdxNotificationCollection<T>.OnRemoveComplete(AIndex: Integer; AValue: T);
var
  AArgs: TdxCollectionChangedEventArgs<T>;
begin
  inherited OnRemoveComplete(AIndex, AValue);
  AArgs := TdxCollectionChangedEventArgs<T>.Create(TdxCollectionChangedAction.Remove, AValue);
  try
    OnCollectionChanged(AArgs);
  finally
    AArgs.Free;
  end;
end;

procedure TdxNotificationCollection<T>.RaiseBeginBatchUpdate;
begin
  if Assigned(FOnBeginBatchUpdate) then 
    FOnBeginBatchUpdate(Self, nil);
end;

procedure TdxNotificationCollection<T>.RaiseCancelBatchUpdate;
begin
  if Assigned(FOnCancelBatchUpdate) then 
    FOnCancelBatchUpdate(Self, nil);
end;

procedure TdxNotificationCollection<T>.RaiseCollectionChanged(E: TdxCollectionChangedEventArgs<T>);
begin
  FOnCollectionChanged.Invoke(Self, E); 
end;

procedure TdxNotificationCollection<T>.RaiseCollectionChanging(E: TdxCollectionChangingEventArgs<T>);
begin
  FOnCollectionChanging.Invoke(Self, E); 
end;

procedure TdxNotificationCollection<T>.RaiseEndBatchUpdate;
begin
  if Assigned(FOnEndBatchUpdate) then 
    FOnEndBatchUpdate(Self, nil);
end;

end.
