{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Dialog.CustomDialog;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, Dialogs, cxDropDownEdit, cxRadioGroup, dxMeasurementUnitEdit, dxRichEdit.FormController,
  Generics.Collections, dxRichEdit.DocumentModel.UnitConverter, cxSpinEdit;

type
  TdxMeasurementType = (mtCustom, mtPoint, mtInch);

  TdxRichEditCustomDialogForm = class(TForm)
  private
    FMeasurementEditors: TDictionary<TdxMeasurementUnitEdit, TdxMeasurementUnitEditHelper>;
    FMeasurementTypes: TObjectDictionary<TdxMeasurementUnitEditHelper, TdxMeasurementType>;
  protected
    function GetUnitConverter: TdxDocumentModelUnitConverter; virtual; abstract;
    procedure ApplyLocalization; dynamic; abstract;
    procedure Populate(ACombobox: TcxComboBox; AProc: TProc<TcxComboBox>); overload;
    procedure Populate(ARadioGroup: TcxRadioGroup; AProc: TProc<TcxRadioGroup>); overload;
    procedure InitializeForm; dynamic;
    class function IsMetric: Boolean; static;
    class function ListSeparator: Char; static;

    procedure InitializeMeasurementUnitEdit(AEditor: TdxMeasurementUnitEdit; AType: TdxMeasurementType;
      AHelper: TdxMeasurementUnitEditHelper);
    function GetMeasurementUnitEditHelper(AEditor: TdxMeasurementUnitEdit): TdxMeasurementUnitEditHelper;
    function GetValueFromEditor(AEditor: TdxMeasurementUnitEdit; ACorrectRange: Boolean = True): Variant;
    procedure SetValueToEditor(AEditor: TdxMeasurementUnitEdit; const AValue: Variant);
    procedure MeasurementUnitEditIncrementValueHandler(Sender: TObject; AButton: TcxSpinEditButton;
      var AValue: Variant; var AHandled: Boolean);

    property UnitConverter: TdxDocumentModelUnitConverter read GetUnitConverter;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Initialize(AControllerParameters: TdxFormControllerParameters);
  end;

implementation

uses
  dxCore, Math, cxClasses, dxRichEdit.DialogStrs;

{$R *.dfm}

{ TdxRichEditTabsCustomDialogForm }

constructor TdxRichEditCustomDialogForm.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FMeasurementEditors := TDictionary<TdxMeasurementUnitEdit, TdxMeasurementUnitEditHelper>.Create;
  FMeasurementTypes := TObjectDictionary<TdxMeasurementUnitEditHelper, TdxMeasurementType>.Create([doOwnsKeys]);
end;

destructor TdxRichEditCustomDialogForm.Destroy;
begin
  FMeasurementEditors.Free;
  FMeasurementTypes.Free;
  inherited Destroy;
end;

function TdxRichEditCustomDialogForm.GetMeasurementUnitEditHelper(
  AEditor: TdxMeasurementUnitEdit): TdxMeasurementUnitEditHelper;
begin
  Result := FMeasurementEditors[AEditor];
end;

function TdxRichEditCustomDialogForm.GetValueFromEditor(AEditor: TdxMeasurementUnitEdit;
  ACorrectRange: Boolean = True): Variant;
var
  AValue: Variant;
  AHelper: TdxMeasurementUnitEditHelper;
begin
  Result := Null;
  AHelper := GetMeasurementUnitEditHelper(AEditor);
  AValue := AHelper.GetValueFromText(AEditor.Text, ACorrectRange);
  if not VarIsNull(AValue) then
    case FMeasurementTypes[AHelper] of
      TdxMeasurementType.mtCustom:
        Result := AValue;
      TdxMeasurementType.mtPoint:
        Result := Trunc(UnitConverter.PointsToModelUnitsF(AValue));
      TdxMeasurementType.mtInch:
        Result := Trunc(UnitConverter.InchesToModelUnitsF(AValue));
    end;
end;

procedure TdxRichEditCustomDialogForm.Initialize(AControllerParameters: TdxFormControllerParameters);
begin
  ApplyLocalization;
  InitializeForm;
end;

procedure TdxRichEditCustomDialogForm.Populate(ACombobox: TcxComboBox; AProc: TProc<TcxComboBox>);
var
  ASavedIndex: Integer;
begin
  ACombobox.Properties.Items.BeginUpdate;
  try
    ASavedIndex := ACombobox.ItemIndex;
    ACombobox.Properties.Items.Clear;
    AProc(ACombobox);
    ACombobox.ItemIndex := Max(ASavedIndex, 0);
  finally
    ACombobox.Properties.Items.EndUpdate;
  end;
end;

procedure TdxRichEditCustomDialogForm.InitializeForm;
begin
end;

procedure TdxRichEditCustomDialogForm.InitializeMeasurementUnitEdit(AEditor: TdxMeasurementUnitEdit;
  AType: TdxMeasurementType; AHelper: TdxMeasurementUnitEditHelper);
begin
  FMeasurementTypes.AddOrSetValue(AHelper, AType);
  FMeasurementEditors.AddOrSetValue(AEditor, AHelper);
  AEditor.ActiveProperties.OnIncrementValue := MeasurementUnitEditIncrementValueHandler;
end;

class function TdxRichEditCustomDialogForm.IsMetric: Boolean;
begin
{$WARNINGS OFF}
  Result := GetLocaleChar(LOCALE_USER_DEFAULT, LOCALE_IMEASURE, '0') = '0';
{$WARNINGS ON}
end;

class function TdxRichEditCustomDialogForm.ListSeparator: Char;
begin
{$WARNINGS OFF}
  Result := GetLocaleChar(LOCALE_USER_DEFAULT, LOCALE_SLIST, ';');
{$WARNINGS ON}
end;

procedure TdxRichEditCustomDialogForm.MeasurementUnitEditIncrementValueHandler(Sender: TObject;
  AButton: TcxSpinEditButton; var AValue: Variant; var AHandled: Boolean);
var
  AEditor: TdxMeasurementUnitEdit;
begin
  AEditor := Sender as TdxMeasurementUnitEdit;
  AHandled := GetMeasurementUnitEditHelper(AEditor).IncrementValue(AButton, AValue);
end;

procedure TdxRichEditCustomDialogForm.Populate(ARadioGroup: TcxRadioGroup; AProc: TProc<TcxRadioGroup>);
begin
  ARadioGroup.Properties.Items.BeginUpdate;
  try
    ARadioGroup.Properties.Items.Clear;
    AProc(ARadioGroup);
  finally
    ARadioGroup.Properties.Items.EndUpdate;
  end;
end;

procedure TdxRichEditCustomDialogForm.SetValueToEditor(AEditor: TdxMeasurementUnitEdit; const AValue: Variant);
var
  AText: string;
  AMeasurementType: TdxMeasurementType;
  AHelper: TdxMeasurementUnitEditHelper;
begin
  AText := '';
  if not VarIsNull(AValue) then
  begin
    AHelper := GetMeasurementUnitEditHelper(AEditor);
    if AHelper <> nil then
    begin
      AMeasurementType := FMeasurementTypes[AHelper];
      case AMeasurementType of
        TdxMeasurementType.mtCustom:
          AText := AHelper.GetTextForomValue(AValue);
        TdxMeasurementType.mtPoint:
          AText := AHelper.GetTextForomValue(UnitConverter.ModelUnitsToPointsF(AValue));
        TdxMeasurementType.mtInch:
          AText := AHelper.GetTextForomValue(UnitConverter.ModelUnitsToInchesF(AValue));
      end;
    end;
  end;
  AEditor.Text := AText;
end;

end.
