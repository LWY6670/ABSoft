{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.DocumentModel.Numbering;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Types, Classes, SysUtils, Graphics, dxRichEdit.Utils.GenericsHelpers, Generics.Collections, Generics.Defaults,
  dxCoreClasses, dxRichEdit.DocumentModel.ParagraphFormatting, dxRichEdit.DocumentModel.CharacterFormatting,
  dxRichEdit.Utils.Types, dxRichEdit.DocumentModel.Section, dxRichEdit.DocumentModel.IndexBasedObject,
  dxRichEdit.DocumentModel.Core, dxRichEdit.Platform.Font, dxRichEdit.DocumentModel.Styles,
  dxRichEdit.DocumentModel.History.IndexChangedHistoryItem, dxRichEdit.Utils.SortedList,
  dxRichEdit.DocumentModel.UnitConverter;

const
  dxAbstractNumberingListIndexInvalidValue = -1;
type
  TdxAbstractListLevel = class;

  TdxAbstractNumberingListIndex = Integer;

  TdxListNumberAlignment = (
    Left = 0,
    Center = 1,
    Right = 2
  );

  TdxNumberingType = (
    MultiLevel,
    Simple,
    Bullet
  );

  TdxListLevelChangeType = (
    None = 0,
    Start,
    Format,
    Alignment,
    ConvertPreviousLevelNumberingToDecimal,
    Separator,
    SuppressRestart,
    SuppressBulletResize,
    DisplayFormatString,
    RelativeRestartLevel,
    TemplateCode,
    Legacy,
    LegacySpace,
    LegacyIndent,
    BatchUpdate
  );

  IdxReadOnlyListLevelProperties = interface
    function GetStart: Integer;
    function GetFormat: TdxNumberingFormat;
    function GetAlignment: TdxListNumberAlignment;
    function GetConvertPreviousLevelNumberingToDecimal: Boolean;
    function GetSeparator: Char;
    function GetSuppressRestart: Boolean;
    function GetSuppressBulletResize: Boolean;
    function GetDisplayFormatString: string;
    function GetRelativeRestartLevel: Integer;
    function GetTemplateCode: Integer;
    function GetOriginalLeftIndent: Integer;
    function GetLegacy: Boolean;
    function GetLegacyIndent: Integer;
    function GetLegacySpace: Integer;

    property Start: Integer read GetStart;
    property Format: TdxNumberingFormat read GetFormat;
    property Alignment: TdxListNumberAlignment read GetAlignment;
    property ConvertPreviousLevelNumberingToDecimal: Boolean read GetConvertPreviousLevelNumberingToDecimal;
    property Separator: Char read GetSeparator;
    property SuppressRestart: Boolean read GetSuppressRestart;
    property SuppressBulletResize: Boolean read GetSuppressBulletResize;
    property DisplayFormatString: string read GetDisplayFormatString;
    property RelativeRestartLevel: Integer read GetRelativeRestartLevel;
    property TemplateCode: Integer read GetTemplateCode;
    property OriginalLeftIndent: Integer read GetOriginalLeftIndent;
    property Legacy: Boolean read GetLegacy;
    property LegacyIndent: Integer read GetLegacyIndent;
    property LegacySpace: Integer read GetLegacySpace;
  end;

  IdxListLevelProperties = interface(IdxReadOnlyListLevelProperties)
    procedure SetStart(const Value: Integer);
    procedure SetFormat(const Value: TdxNumberingFormat);
    procedure SetAlignment(const Value: TdxListNumberAlignment);
    procedure SetConvertPreviousLevelNumberingToDecimal(const Value: Boolean);
    procedure SetSeparator(const Value: Char);
    procedure SetSuppressRestart(const Value: Boolean);
    procedure SetSuppressBulletResize(const Value: Boolean);
    procedure SetDisplayFormatString(const Value: string);
    procedure SetRelativeRestartLevel(const Value: Integer);
    procedure SetTemplateCode(const Value: Integer);
    procedure SetLegacy(const Value: Boolean);
    procedure SetLegacyIndent(const Value: Integer);
    procedure SetLegacySpace(const Value: Integer);

    property Start: Integer read GetStart write SetStart;
    property Format: TdxNumberingFormat read GetFormat write SetFormat;
    property Alignment: TdxListNumberAlignment read GetAlignment write SetAlignment;
    property ConvertPreviousLevelNumberingToDecimal: Boolean read GetConvertPreviousLevelNumberingToDecimal write SetConvertPreviousLevelNumberingToDecimal;
    property Separator: Char read GetSeparator write SetSeparator;
    property SuppressRestart: Boolean read GetSuppressRestart write SetSuppressRestart;
    property SuppressBulletResize: Boolean read GetSuppressBulletResize write SetSuppressBulletResize;
    property DisplayFormatString: string read GetDisplayFormatString write SetDisplayFormatString;
    property RelativeRestartLevel: Integer read GetRelativeRestartLevel write SetRelativeRestartLevel;
    property TemplateCode: Integer read GetTemplateCode write SetTemplateCode;
    property Legacy: Boolean read GetLegacy write SetLegacy;
    property LegacyIndent: Integer read GetLegacyIndent write SetLegacyIndent;
    property LegacySpace: Integer read GetLegacySpace write SetLegacySpace;
  end;

  { TdxListLevelInfo }

  TdxListLevelInfo = class(TcxIUnknownObject,
      IdxCloneable<TdxListLevelInfo>,
      IdxSupportsCopyFrom<TdxListLevelInfo>,
      IdxListLevelProperties,
      IdxReadOnlyListLevelProperties,
      IdxSupportsSizeOf
    )
  private
    FStart: Integer;
    FFormat: TdxNumberingFormat;
    FAlignment: TdxListNumberAlignment;
    FConvertPreviousLevelNumberingToDecimal: Boolean;
    FSeparator: Char;
    FSuppressRestart: Boolean;
    FSuppressBulletResize: Boolean;
    FDisplayFormatString: string;
    FRelativeRestartLevel: Integer;
    FTemplateCode: Integer;
    FOriginalLeftIndent: Integer;
    FLegacy: Boolean;
    FLegacySpace: Integer;
    FLegacyIndent: Integer;
    function GetHasTemplateCode: Boolean;
    function GetStart: Integer; inline;
    function GetFormat: TdxNumberingFormat; inline;
    function GetAlignment: TdxListNumberAlignment; inline;
    function GetConvertPreviousLevelNumberingToDecimal: Boolean; inline;
    function GetSeparator: Char; inline;
    function GetSuppressRestart: Boolean; inline;
    function GetSuppressBulletResize: Boolean; inline;
    function GetDisplayFormatString: string; inline;
    function GetRelativeRestartLevel: Integer; inline;
    function GetTemplateCode: Integer; inline;
    function GetOriginalLeftIndent: Integer; inline;
    function GetLegacy: Boolean; inline;
    function GetLegacyIndent: Integer; inline;
    function GetLegacySpace: Integer; inline;
    procedure SetStart(const Value: Integer); inline;
    procedure SetFormat(const Value: TdxNumberingFormat); inline;
    procedure SetAlignment(const Value: TdxListNumberAlignment); inline;
    procedure SetConvertPreviousLevelNumberingToDecimal(const Value: Boolean); inline;
    procedure SetSeparator(const Value: Char); inline;
    procedure SetSuppressRestart(const Value: Boolean); inline;
    procedure SetSuppressBulletResize(const Value: Boolean); inline;
    procedure SetDisplayFormatString(const Value: string); inline;
    procedure SetRelativeRestartLevel(const Value: Integer); inline;
    procedure SetTemplateCode(const Value: Integer); inline;
    procedure SetLegacy(const Value: Boolean); inline;
    procedure SetLegacyIndent(const Value: Integer); inline;
    procedure SetLegacySpace(const Value: Integer); inline;
  public
    function Clone: TdxListLevelInfo;
    procedure CopyFrom(const AInfo: TdxListLevelInfo);
    procedure InitializeAsDefault;
    function Equals(Obj: TObject): Boolean; override;

    property Start: Integer read FStart write FStart;
    property Format: TdxNumberingFormat read FFormat write FFormat;
    property Alignment: TdxListNumberAlignment read FAlignment write FAlignment;
    property ConvertPreviousLevelNumberingToDecimal: Boolean read FConvertPreviousLevelNumberingToDecimal
      write FConvertPreviousLevelNumberingToDecimal;
    property Separator: Char read FSeparator write FSeparator;
    property SuppressRestart: Boolean read FSuppressRestart write FSuppressRestart;
    property SuppressBulletResize: Boolean read FSuppressBulletResize write FSuppressBulletResize;
    property DisplayFormatString: string read FDisplayFormatString write FDisplayFormatString;
    property RelativeRestartLevel: Integer read FRelativeRestartLevel write FRelativeRestartLevel;
    property TemplateCode: Integer read FTemplateCode write FTemplateCode;
    property OriginalLeftIndent: Integer read FOriginalLeftIndent write FOriginalLeftIndent;
    property HasTemplateCode: Boolean read GetHasTemplateCode;
    property Legacy: Boolean read FLegacy write FLegacy;
    property LegacySpace: Integer read FLegacySpace write FLegacySpace;
    property LegacyIndent: Integer read FLegacyIndent write FLegacyIndent;
  end;

  { TdxListLevelChangeActionsCalculator }

  TdxListLevelChangeActionsCalculator = class abstract
  public
    class function CalculateChangeActions(AChange: TdxListLevelChangeType): TdxDocumentModelChangeActions;
  end;

  { TdxListLevelProperties }

  TdxListLevelProperties = class(TdxRichEditIndexBasedObject<TdxListLevelInfo>,
      IdxCloneable<TdxListLevelProperties>,
      IdxListLevelProperties,
      IdxReadOnlyListLevelProperties
    )
  private
    function GetAlignment: TdxListNumberAlignment;
    function GetConvertPreviousLevelNumberingToDecimal: Boolean;
    function GetDisplayFormatString: string;
    function GetFormat: TdxNumberingFormat;
    function GetLegacy: Boolean;
    function GetLegacyIndent: Integer;
    function GetLegacySpace: Integer;
    function GetOriginalLeftIndent: Integer;
    function GetRelativeRestartLevel: Integer;
    function GetSeparator: Char;
    function GetStart: Integer;
    function GetSuppressBulletResize: Boolean;
    function GetSuppressRestart: Boolean;
    function GetTemplateCode: Integer;
    procedure SetAlignment(const Value: TdxListNumberAlignment);
    procedure SetConvertPreviousLevelNumberingToDecimal(const Value: Boolean);
    procedure SetDisplayFormatString(const Value: string);
    procedure SetFormat(const Value: TdxNumberingFormat);
    procedure SetLegacy(const Value: Boolean);
    procedure SetLegacyIndent(const Value: Integer);
    procedure SetLegacySpace(const Value: Integer);
    procedure InternalSetOriginalLeftIndent(const Value: Integer);
    procedure SetRelativeRestartLevel(const Value: Integer);
    procedure SetSeparator(const Value: Char);
    procedure SetStart(const Value: Integer);
    procedure SetSuppressBulletResize(const Value: Boolean);
    procedure SetSuppressRestart(const Value: Boolean);
    procedure IdxListLevelProperties.SetTemplateCode = SetTemplateCodeIntf;
    procedure SetTemplateCodeIntf(const Value: Integer); overload;
  protected
    function SetStartCore(const AListLevel: TdxListLevelInfo;
      const AValue: Integer): TdxDocumentModelChangeActions; virtual;
    function SetFormatCore(const AListLevel: TdxListLevelInfo;
      const AValue: TdxNumberingFormat): TdxDocumentModelChangeActions; virtual;
    function SetAlignmentCore(const AListLevel: TdxListLevelInfo;
      const AValue: TdxListNumberAlignment): TdxDocumentModelChangeActions; virtual;
    function SetConvertPreviousLevelNumberingToDecimalCore(const AListLevel: TdxListLevelInfo;
      const AValue: Boolean): TdxDocumentModelChangeActions; virtual;
    function SetSeparatorCore(const AListLevel: TdxListLevelInfo;
      const AValue: Char): TdxDocumentModelChangeActions; virtual;
    function SetSuppressRestartCore(const AListLevel: TdxListLevelInfo;
      const AValue: Boolean): TdxDocumentModelChangeActions; virtual;
    function SetSuppressBulletResizeCore(const AListLevel: TdxListLevelInfo;
      const AValue: Boolean): TdxDocumentModelChangeActions; virtual;
    function SetDisplayFormatStringCore(const AListLevel: TdxListLevelInfo;
      const AValue: string): TdxDocumentModelChangeActions; virtual;
    function SetRestartLevelCore(const AListLevel: TdxListLevelInfo;
      const AValue: Integer): TdxDocumentModelChangeActions; virtual;
    function SetTemplateCode(const AListLevel: TdxListLevelInfo;
      const AValue: Integer): TdxDocumentModelChangeActions; overload; virtual;
    function SetOriginalLeftIndent(const AListLevel: TdxListLevelInfo;
      const AValue: Integer): TdxDocumentModelChangeActions; overload; virtual;
    function SetLegacyCore(const AListLevel: TdxListLevelInfo;
      const AValue: Boolean): TdxDocumentModelChangeActions; virtual;
    function SetLegacySpaceCore(const AListLevel: TdxListLevelInfo;
      const AValue: Integer): TdxDocumentModelChangeActions; virtual;
    function SetLegacyIndentCore(const AListLevel: TdxListLevelInfo;
      const AValue: Integer): TdxDocumentModelChangeActions; virtual;
    function GetCache(const ADocumentModel: TdxCustomDocumentModel): TdxUniqueItemsCache<TdxListLevelInfo>; override;
    function GetBatchUpdateChangeActions: TdxDocumentModelChangeActions; override;
  public
    constructor Create(ADocumentModel: TdxCustomDocumentModel); reintroduce;
    function Clone: TdxListLevelProperties;

    property Start: Integer read GetStart write SetStart;
    property Format: TdxNumberingFormat read GetFormat write SetFormat;
    property Alignment: TdxListNumberAlignment read GetAlignment write SetAlignment;
    property ConvertPreviousLevelNumberingToDecimal: Boolean
      read GetConvertPreviousLevelNumberingToDecimal write SetConvertPreviousLevelNumberingToDecimal;
    property Separator: Char read GetSeparator write SetSeparator;
    property SuppressRestart: Boolean read GetSuppressRestart write SetSuppressRestart;
    property SuppressBulletResize: Boolean read GetSuppressBulletResize write SetSuppressBulletResize;
    property DisplayFormatString: string read GetDisplayFormatString write SetDisplayFormatString;
    property OriginalLeftIndent: Integer read GetOriginalLeftIndent write InternalSetOriginalLeftIndent;
    property RelativeRestartLevel: Integer read GetRelativeRestartLevel write SetRelativeRestartLevel;
    property TemplateCode: Integer read GetTemplateCode write SetTemplateCodeIntf;
    property Legacy: Boolean read GetLegacy write SetLegacy;
    property LegacySpace: Integer read GetLegacySpace write SetLegacySpace;
    property LegacyIndent: Integer read GetLegacyIndent write SetLegacyIndent;
  end;

  { IdxListLevel }

  IdxListLevel = interface 
  ['{98A61643-3F8C-40BF-BA7C-8A1C4337A7CE}']
    function GetDocumentModel: TdxCustomDocumentModel;
    function GetListLevelProperties: TdxListLevelProperties;
    function GetParagraphProperties: TdxParagraphProperties;
    function GetCharacterProperties: TdxCharacterProperties;
    function GetBulletLevel: Boolean;

    property DocumentModel: TdxCustomDocumentModel read GetDocumentModel;
    property ListLevelProperties: TdxListLevelProperties read GetListLevelProperties;
    property ParagraphProperties: TdxParagraphProperties read GetParagraphProperties;
    property CharacterProperties: TdxCharacterProperties read GetCharacterProperties;
    property BulletLevel: Boolean read GetBulletLevel;

  end;

  { IdxInternalListLevel }

  IdxInternalListLevel = interface(IdxListLevel)
  ['{6B785813-3EA3-4D68-B1D9-0FA066CF6563}']
    function GetFontCacheIndex: Integer;

    property FontCacheIndex: Integer read GetFontCacheIndex;
  end;

  { IdxOverrideListLevel }

  IdxOverrideListLevel = interface(IdxListLevel)
  ['{C7B75CBF-6493-44E9-8693-2BC325C6E085}']
    function GetOverrideStart: Boolean;
    function GetNewStart: Integer;
    procedure SetNewStart(const Value: Integer);

    procedure SetOverrideStart(AValue: Boolean);

    property OverrideStart: Boolean read GetOverrideStart;
    property NewStart: Integer read GetNewStart write SetNewStart;
  end;

  { IdxReadOnlyIListLevelCollection }

  IdxReadOnlyIListLevelCollection = interface
  ['{3AF33CAB-6B63-4692-9ADD-83E218E0D6B8}']
    function GetCount: Integer;
    function GetItem(Index: Integer): TdxAbstractListLevel;

    property Items[Index: Integer]: TdxAbstractListLevel read GetItem; default;
    property Count: Integer read GetCount;
  end;

  { TdxListLevelCollection }

  TdxListLevelCollection = class(TcxIUnknownObject, IdxReadOnlyIListLevelCollection) 
  private
    FList: TdxFastObjectList;
    function GetCount: Integer;
    function GetItem(AIndex: Integer): TdxAbstractListLevel;
    procedure SetItem(AIndex: Integer; AObject: TdxAbstractListLevel);
  protected
    property List: TdxFastObjectList read FList;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Add(ALevel: TdxAbstractListLevel);

    property Count: Integer read GetCount;
    property Items[Index: Integer]: TdxAbstractListLevel read GetItem write SetItem; default;
  end;

  { TdxNumberingListBase }

  IdxNumberingListBase = interface 
  ['{CD335252-85B9-493B-8D7A-93C19AC20910}']
    function GetLevels: IdxReadOnlyIListLevelCollection;
    function GetDocumentModel: TdxCustomDocumentModel;

    property Levels: IdxReadOnlyIListLevelCollection read GetLevels;
    property DocumentModel: TdxCustomDocumentModel read GetDocumentModel;
  end;

  { TdxAbstractListLevel }

  TdxAbstractListLevel = class abstract(TcxIUnknownObject, IdxListLevel)
  protected
    function GetListLevelProperties: TdxListLevelProperties; virtual; abstract;
    function GetParagraphProperties: TdxParagraphProperties; virtual; abstract;
    function GetCharacterProperties: TdxCharacterProperties; virtual; abstract;
    function GetBulletLevel: Boolean; virtual; abstract;
    function GetDocumentModel: TdxCustomDocumentModel; virtual; abstract;
    function GetOverrideStart: Boolean; virtual; abstract;
    function GetNewStart: Integer; virtual; abstract;
    procedure SetNewStart(const Value: Integer); virtual; abstract;
  public
    property ListLevelProperties: TdxListLevelProperties read GetListLevelProperties;
    property ParagraphProperties: TdxParagraphProperties read GetParagraphProperties;
    property CharacterProperties: TdxCharacterProperties read GetCharacterProperties;
    property BulletLevel: Boolean read GetBulletLevel;
    property OverrideStart: Boolean read GetOverrideStart;
    property NewStart: Integer read GetNewStart write SetNewStart;

    property DocumentModel: TdxCustomDocumentModel read GetDocumentModel;
  end;

  { TdxListLevel }

  TdxListLevel = class(TdxAbstractListLevel, IdxInternalListLevel, IdxParagraphPropertiesContainer,
    IdxCharacterPropertiesContainer, IdxCloneable<TdxListLevel>,
    IdxCharacterProperties, IdxParagraphProperties) 
  const
    BulletLevelDisplayFormatStringLength = 1;
  private
    FParagraphProperties: TdxParagraphProperties;
    FCharacterProperties: TdxCharacterProperties;
    FListLevelProperties: TdxListLevelProperties;
    FParagraphStyleIndex: Integer;
    FDocumentModel: TdxCustomDocumentModel;
    FMergedCharacterFormattingCacheIndex: Integer;
    FMergedParagraphFormattingCacheIndex: Integer;
    FFontCacheIndex: Integer;
    function GetAfterAutoSpacing: Boolean;
    function GetAlignment: TdxParagraphAlignment;
    function GetAllCaps: Boolean;
    function GetBackColor: TColor;
    function GetBeforeAutoSpacing: Boolean;
    function GetContextualSpacing: Boolean;
    function GetDoubleFontSize: Integer;
    function GetFirstLineIndent: Integer;
    function GetFirstLineIndentType: TdxParagraphFirstLineIndent;
    function GetFontBold: Boolean;
    function GetFontItalic: Boolean;
    function GetFontName: string;
    function GetFontStrikeoutType: TdxStrikeoutType;
    function GetFontUnderlineType: TdxUnderlineType;
    function GetForeColor: TColor;
    function GetHidden: Boolean;
    function GetKeepLinesTogether: Boolean;
    function GetKeepWithNext: Boolean;
    function GetLeftIndent: Integer;
    function GetLineSpacing: Single;
    function GetLineSpacingType: TdxParagraphLineSpacing;
    function GetMainPieceTable: TObject;
    function GetMergedCharacterFormatting: TdxCharacterFormattingInfo;
    function GetMergedCharacterFormattingCacheIndex: Integer;
    function GetMergedParagraphFormatting: TdxParagraphFormattingInfo;
    function GetMergedParagraphFormattingCacheIndex: Integer;
    function GetOutlineLevel: Integer;
    function GetPageBreakBefore: Boolean;
    function GetParagraphStyle: TdxParagraphStyle;
    function GetRightIndent: Integer;
    function GetScript: TdxCharacterFormattingScript;
    function GetSpacingAfter: Integer;
    function GetSpacingBefore: Integer;
    function GetStrikeoutColor: TColor;
    function GetStrikeoutWordsOnly: Boolean;
    function GetSuppressHyphenation: Boolean;
    function GetSuppressLineNumbers: Boolean;
    function GetUnderlineColor: TColor;
    function GetUnderlineWordsOnly: Boolean;
    function GetWidowOrphanControl: Boolean;
    procedure SetAfterAutoSpacing(const Value: Boolean);
    procedure SetAlignment(const Value: TdxParagraphAlignment);
    procedure SetAllCaps(const Value: Boolean);
    procedure SetBackColor(const Value: TColor);
    procedure SetBeforeAutoSpacing(const Value: Boolean);
    procedure SetContextualSpacing(const Value: Boolean);
    procedure SetDoubleFontSize(const Value: Integer);
    procedure SetFirstLineIndent(const Value: Integer);
    procedure SetFirstLineIndentType(const Value: TdxParagraphFirstLineIndent);
    procedure SetFontBold(const Value: Boolean);
    procedure SetFontItalic(const Value: Boolean);
    procedure SetFontName(const Value: string);
    procedure SetFontStrikeoutType(const Value: TdxStrikeoutType);
    procedure SetFontUnderlineType(const Value: TdxUnderlineType);
    procedure SetForeColor(const Value: TColor);
    procedure SetHidden(const Value: Boolean);
    procedure SetKeepLinesTogether(const Value: Boolean);
    procedure SetKeepWithNext(const Value: Boolean);
    procedure SetLeftIndent(const Value: Integer);
    procedure SetLineSpacing(const Value: Single);
    procedure SetLineSpacingType(const Value: TdxParagraphLineSpacing);
    procedure SetOutlineLevel(const Value: Integer);
    procedure SetPageBreakBefore(const Value: Boolean);
    procedure SetParagraphStyleIndex(const Value: Integer);
    procedure SetRightIndent(const Value: Integer);
    procedure SetScript(const Value: TdxCharacterFormattingScript);
    procedure SetSpacingAfter(const Value: Integer);
    procedure SetSpacingBefore(const Value: Integer);
    procedure SetStrikeoutColor(const Value: TColor);
    procedure SetStrikeoutWordsOnly(const Value: Boolean);
    procedure SetSuppressHyphenation(const Value: Boolean);
    procedure SetSuppressLineNumbers(const Value: Boolean);
    procedure SetUnderlineColor(const Value: TColor);
    procedure SetUnderlineWordsOnly(const Value: Boolean);
    procedure SetWidowOrphanControl(const Value: Boolean);
    function GetFontCacheIndex: Integer;
    function GetNoProof: Boolean;
    procedure SetNoProof(const Value: Boolean);
  protected
    function GetPieceTable: TObject;
    function CreateCharacterPropertiesChangedHistoryItem: TdxIndexChangedHistoryItemCore<TdxDocumentModelChangeActions>;
    function CreateParagraphPropertiesChangedHistoryItem: TdxIndexChangedHistoryItemCore<TdxDocumentModelChangeActions>;
    procedure OnCharacterPropertiesChanged;
    procedure OnParagraphPropertiesChanged;
    function GetMergedParagraphProperties: TdxMergedParagraphProperties; virtual;
    function GetOverrideStart: Boolean; override;
    function GetNewStart: Integer; override;
    procedure SetNewStart(const Value: Integer); override;

    function GetDocumentModel: TdxCustomDocumentModel; override;

    function GetBulletLevel: Boolean; override;
    function GetCharacterProperties: TdxCharacterProperties; override;
    function GetListLevelProperties: TdxListLevelProperties; override;
    function GetParagraphProperties: TdxParagraphProperties; override;
  public
    constructor Create(ADocumentModel: TdxCustomDocumentModel); virtual;
    destructor Destroy; override;
    procedure OnParagraphStyleChanged;
    procedure CopyFrom(AListLevel: TdxListLevel); virtual;
    function Equals(AObj: TObject): Boolean; override;
    function Clone: TdxListLevel; virtual;
    function CreateLevel: TdxListLevel; virtual;
    function GetMergedCharacterProperties: TdxMergedCharacterProperties; virtual;
    procedure OnPropertiesObtainAffectedRange(Sender: TObject; E: TdxObtainAffectedRangeEventArgs);
    property FontCacheIndex: Integer read GetFontCacheIndex;

    property DocumentModel: TdxCustomDocumentModel read FDocumentModel;
    property ParagraphStyleIndex: Integer read FParagraphStyleIndex write SetParagraphStyleIndex;
    property ParagraphStyle: TdxParagraphStyle read GetParagraphStyle;
    property PieceTable: TObject read GetMainPieceTable;
    property MergedCharacterFormattingCacheIndex: Integer read GetMergedCharacterFormattingCacheIndex;
    property MergedParagraphFormattingCacheIndex: Integer read GetMergedParagraphFormattingCacheIndex;
    property MergedCharacterFormatting: TdxCharacterFormattingInfo read GetMergedCharacterFormatting;
    property MergedParagraphFormatting: TdxParagraphFormattingInfo read GetMergedParagraphFormatting;
    property DoubleFontSize: Integer read GetDoubleFontSize write SetDoubleFontSize;
    property FontName: string read GetFontName write SetFontName;
    property FontBold: Boolean read GetFontBold write SetFontBold;
    property FontItalic: Boolean read GetFontItalic write SetFontItalic;
    property Script: TdxCharacterFormattingScript read GetScript write SetScript;
    property FontStrikeoutType: TdxStrikeoutType read GetFontStrikeoutType write SetFontStrikeoutType;
    property FontUnderlineType: TdxUnderlineType read GetFontUnderlineType write SetFontUnderlineType;
    property AllCaps: Boolean read GetAllCaps write SetAllCaps;
    property UnderlineWordsOnly: Boolean read GetUnderlineWordsOnly write SetUnderlineWordsOnly;
    property StrikeoutWordsOnly: Boolean read GetStrikeoutWordsOnly write SetStrikeoutWordsOnly;
    property ForeColor: TColor read GetForeColor write SetForeColor;
    property BackColor: TColor read GetBackColor write SetBackColor;
    property UnderlineColor: TColor read GetUnderlineColor write SetUnderlineColor;
    property StrikeoutColor: TColor read GetStrikeoutColor write SetStrikeoutColor;
    property Hidden: Boolean read GetHidden write SetHidden;
    property NoProof: Boolean read GetNoProof write SetNoProof;
    property Alignment: TdxParagraphAlignment read GetAlignment write SetAlignment;
    property LeftIndent: Integer read GetLeftIndent write SetLeftIndent;
    property RightIndent: Integer read GetRightIndent write SetRightIndent;
    property SpacingBefore: Integer read GetSpacingBefore write SetSpacingBefore;
    property SpacingAfter: Integer read GetSpacingAfter write SetSpacingAfter;
    property LineSpacingType: TdxParagraphLineSpacing read GetLineSpacingType write SetLineSpacingType;
    property LineSpacing: Single read GetLineSpacing write SetLineSpacing;
    property FirstLineIndentType: TdxParagraphFirstLineIndent read GetFirstLineIndentType write SetFirstLineIndentType;
    property FirstLineIndent: Integer read GetFirstLineIndent write SetFirstLineIndent;
    property SuppressHyphenation: Boolean read GetSuppressHyphenation write SetSuppressHyphenation;
    property SuppressLineNumbers: Boolean read GetSuppressLineNumbers write SetSuppressLineNumbers;
    property ContextualSpacing: Boolean read GetContextualSpacing write SetContextualSpacing;
    property PageBreakBefore: Boolean read GetPageBreakBefore write SetPageBreakBefore;
    property BeforeAutoSpacing: Boolean read GetBeforeAutoSpacing write SetBeforeAutoSpacing;
    property AfterAutoSpacing: Boolean read GetAfterAutoSpacing write SetAfterAutoSpacing;
    property KeepWithNext: Boolean read GetKeepWithNext write SetKeepWithNext;
    property KeepLinesTogether: Boolean read GetKeepLinesTogether write SetKeepLinesTogether;
    property WidowOrphanControl: Boolean read GetWidowOrphanControl write SetWidowOrphanControl;
    property OutlineLevel: Integer read GetOutlineLevel write SetOutlineLevel;
  end;

  { TdxNumberingListBaseImpl }

  TdxNumberingListBaseImpl = class(TcxIUnknownObject) 
  private
    FId: Integer;
    FDocumentModel: TdxCustomDocumentModel;
  protected
    FLevels: TdxListLevelCollection;
    function GetDocumentModel: TdxCustomDocumentModel;
  public
    constructor Create(ADocumentModel: TdxCustomDocumentModel; ALevelCount: Integer);
    destructor Destroy; override;

    function Id: Integer;
    function GenerateNewId: Integer; virtual; abstract;
    procedure SetId(AId: Integer); virtual;
    function GetId: Integer;
    procedure InitLevels(ALevelCount: Integer); virtual;
    function CreateLevel(ALevelIndex: Integer): TdxAbstractListLevel; virtual; abstract;
    procedure CopyFrom(AList: TdxNumberingListBaseImpl); virtual;
    function IsEqual(AList: TdxNumberingListBaseImpl): Boolean;
    function EqualsLevels(AListLevel: TdxListLevelCollection): Boolean;
    procedure CopyLevelsFrom(ASourceLevels: TdxListLevelCollection); virtual; abstract;
    procedure SetLevel(ALevelIndex: Integer; AValue: TdxListLevel); virtual;

    property Levels: TdxListLevelCollection read FLevels;
    property DocumentModel: TdxCustomDocumentModel read GetDocumentModel;
  end;

  { TdxOverrideListLevel }

  TdxOverrideListLevel = class(TdxListLevel, IdxOverrideListLevel)
  private
    FOverrideStart: Boolean;
  protected
    function GetOverrideStart: Boolean; override;
    function GetNewStart: Integer; override;
    procedure SetNewStart(const Value: Integer); override;
  public
    function CreateLevel: TdxListLevel; override;

    procedure SetOverrideStart(AValue: Boolean); virtual;
  end;

  { TdxAbstractNumberingList }

  TdxAbstractNumberingList = class(TdxNumberingListBaseImpl, IdxNumberingListBase) 
  private
    FDeleted: Boolean;
  protected
    function GetLevels: IdxReadOnlyIListLevelCollection; 
  public
    constructor Create(ADocumentModel: TdxCustomDocumentModel);
    function CreateLevel(ALevelIndex: Integer): TdxAbstractListLevel; override;
    procedure CopyLevelsFrom(ASourceLevels: TdxListLevelCollection); override;
    function Clone: TdxAbstractNumberingList;
    function CreateNumberingList: TdxAbstractNumberingList; virtual;
    function GenerateNewId: Integer; override;
    property Deleted: Boolean read FDeleted write FDeleted;
  end;

  { TdxAbstractNumberingListCollection }

  TdxAbstractNumberingListCollection = class(TdxFastObjectList)
  private
    function GetItem(Index: Integer): TdxAbstractNumberingList;
    procedure SetItem(Index: Integer; const Value: TdxAbstractNumberingList);
  public
    function HasListOfType(AListType: TdxNumberingType): Boolean;

    property Items[Index: Integer]: TdxAbstractNumberingList read GetItem write SetItem; default;
  end;

  { TdxNumberingListHelper }

  TdxNumberingListHelper = class
  type
    TdxListLevelPropertiesDelegate = reference to function(AProperties: TdxListLevelProperties): Boolean;
  const
    TemplateCodeStart = $100;
    TemplateCodeEnd = $7FFFFFFF;
  public
    class function GetLevelType(const ANumberingList: IdxNumberingListBase; ALevelIndex: Integer): TdxNumberingType;
    class function GetListType(const ANumberingList: IdxNumberingListBase): TdxNumberingType;
    class function IsHybridList(const ANumberingList: IdxNumberingListBase): Boolean;
    class function GenerateNewTemplateCode(ADocumentModel: TdxCustomDocumentModel): Integer;
    class function IsNewTemplateCode(ADocumentModel: TdxCustomDocumentModel; ATemplateCode: Integer): Boolean;
    class procedure SetHybridListType(const AList: IdxNumberingListBase);
    class procedure SetSimpleListType(const AList: IdxNumberingListBase);
    class procedure SetListLevelsProperty(AList: IdxNumberingListBase; ACondition: TdxListLevelPropertiesDelegate;
      AAction: TdxAction<TdxListLevelProperties>);
    class procedure SetListType(const AList: IdxNumberingListBase; AValue: TdxNumberingType);
    class procedure SetListLevelsFormat(const AList: IdxNumberingListBase; AFormat: TdxNumberingFormat);
    class function GetAbstractListIndexByType(AListCollection: TdxAbstractNumberingListCollection; AType: TdxNumberingType): TdxAbstractNumberingListIndex;
  end;

  { TdxNumberingList }

  TdxNumberingList = class(TdxNumberingListBaseImpl, IdxNumberingListBase) 
  private
    FReferringParagraphsCount: Integer;
    FAbstractNumberingListIndex: TdxAbstractNumberingListIndex;
    function GetAbstractNumberingList: TdxAbstractNumberingList;
    function GetLevels: IdxReadOnlyIListLevelCollection;
  public
    constructor Create(ADocumentModel: TdxCustomDocumentModel;
      AAbstractNumberingListIndex: TdxAbstractNumberingListIndex; ALevelCount: Integer = 9);
    function Clone: TdxNumberingList;

    function CanRemove: Boolean;
    function CreateLevel(ALevelIndex: Integer): TdxAbstractListLevel; override;
    function GenerateNewId: Integer; override;
    procedure CopyLevelsFrom(ASourceLevels: TdxListLevelCollection); override;

    property AbstractNumberingListIndex: TdxAbstractNumberingListIndex read FAbstractNumberingListIndex;
    property AbstractNumberingList: TdxAbstractNumberingList read GetAbstractNumberingList;
    procedure OnParagraphAdded;
    procedure OnParagraphRemoved;
    property Levels: TdxListLevelCollection read FLevels write FLevels;
  end;

  { TdxNumberingListReferenceLevel }

  TdxNumberingListReferenceLevel = class(TdxAbstractListLevel,
      IdxInternalListLevel,
      IdxOverrideListLevel,
      IdxCharacterProperties, IdxParagraphProperties
    ) 
  private
    FLevel: Integer;
    FOwner: TdxNumberingList;
    FOverrideStart: Boolean;
    FNewStart: Integer;
    function GetAfterAutoSpacing: Boolean;
    function GetAlignment: TdxParagraphAlignment;
    function GetAllCaps: Boolean;
    function GetBackColor: TColor;
    function GetBeforeAutoSpacing: Boolean;
    function GetContextualSpacing: Boolean;
    function GetDoubleFontSize: Integer;
    function GetFirstLineIndent: Integer;
    function GetFirstLineIndentType: TdxParagraphFirstLineIndent;
    function GetFontBold: Boolean;
    function GetFontCacheIndex: Integer;
    function GetFontItalic: Boolean;
    function GetFontName: string;
    function GetFontStrikeoutType: TdxStrikeoutType;
    function GetFontUnderlineType: TdxUnderlineType;
    function GetForeColor: TColor;
    function GetHidden: Boolean;
    function GetKeepLinesTogether: Boolean;
    function GetKeepWithNext: Boolean;
    function GetLeftIndent: Integer;
    function GetLineSpacing: Single;
    function GetLineSpacingType: TdxParagraphLineSpacing;
    function GetNoProof: Boolean;
    function GetOutlineLevel: Integer;
    function GetPageBreakBefore: Boolean;
    function GetParagraphStyle: TdxParagraphStyle;
    function GetRightIndent: Integer;
    function GetScript: TdxCharacterFormattingScript;
    function GetSpacingAfter: Integer;
    function GetSpacingBefore: Integer;
    function GetStrikeoutColor: TColor;
    function GetStrikeoutWordsOnly: Boolean;
    function GetSuppressHyphenation: Boolean;
    function GetSuppressLineNumbers: Boolean;
    function GetUnderlineColor: TColor;
    function GetUnderlineWordsOnly: Boolean;
    function GetWidowOrphanControl: Boolean;
    procedure SetAfterAutoSpacing(const Value: Boolean);
    procedure SetAlignment(const Value: TdxParagraphAlignment);
    procedure SetAllCaps(const Value: Boolean);
    procedure SetBackColor(const Value: TColor);
    procedure SetBeforeAutoSpacing(const Value: Boolean);
    procedure SetContextualSpacing(const Value: Boolean);
    procedure SetDoubleFontSize(const Value: Integer);
    procedure SetFirstLineIndent(const Value: Integer);
    procedure SetFirstLineIndentType(const Value: TdxParagraphFirstLineIndent);
    procedure SetFontBold(const Value: Boolean);
    procedure SetFontItalic(const Value: Boolean);
    procedure SetFontName(const Value: string);
    procedure SetFontStrikeoutType(const Value: TdxStrikeoutType);
    procedure SetFontUnderlineType(const Value: TdxUnderlineType);
    procedure SetForeColor(const Value: TColor);
    procedure SetHidden(const Value: Boolean);
    procedure SetKeepLinesTogether(const Value: Boolean);
    procedure SetKeepWithNext(const Value: Boolean);
    procedure SetLeftIndent(const Value: Integer);
    procedure SetLineSpacing(const Value: Single);
    procedure SetLineSpacingType(const Value: TdxParagraphLineSpacing);
    procedure SetNoProof(const Value: Boolean);
    procedure SetOutlineLevel(const Value: Integer);
    procedure SetPageBreakBefore(const Value: Boolean);
    procedure SetRightIndent(const Value: Integer);
    procedure SetScript(const Value: TdxCharacterFormattingScript);
    procedure SetSpacingAfter(const Value: Integer);
    procedure SetSpacingBefore(const Value: Integer);
    procedure SetStrikeoutColor(const Value: TColor);
    procedure SetStrikeoutWordsOnly(const Value: Boolean);
    procedure SetSuppressHyphenation(const Value: Boolean);
    procedure SetSuppressLineNumbers(const Value: Boolean);
    procedure SetUnderlineColor(const Value: TColor);
    procedure SetUnderlineWordsOnly(const Value: Boolean);
    procedure SetWidowOrphanControl(const Value: Boolean);
  protected
    // IdxListLevel implements
    function GetBulletLevel: Boolean; override;
    function GetCharacterProperties: TdxCharacterProperties; override;
    function GetListLevelProperties: TdxListLevelProperties; override;
    function GetParagraphProperties: TdxParagraphProperties; override;
    function GetDocumentModel: TdxCustomDocumentModel; override;
    function GetOverrideStart: Boolean; override;
    function GetNewStart: Integer; override;
    procedure SetNewStart(const Value: Integer); override;

    procedure SetLevelCore(ALevel: Integer);
    function GetOwnerLevel: TdxListLevel; virtual;
  public
    constructor Create(AOwner: TdxNumberingList; ALevel: Integer);
    procedure SetOverrideStart(AValue: Boolean); virtual;

    property Owner: TdxNumberingList read FOwner;
    property Level: Integer read FLevel;
    property FontCacheIndex: Integer read GetFontCacheIndex;
    property OwnerLevel: TdxListLevel read GetOwnerLevel;

    property ParagraphProperties: TdxParagraphProperties read GetParagraphProperties;
    property CharacterProperties: TdxCharacterProperties read GetCharacterProperties;
    property ListLevelProperties: TdxListLevelProperties read GetListLevelProperties;
    property BulletLevel: Boolean read GetBulletLevel;
    property FontName: string read GetFontName write SetFontName;
    property DoubleFontSize: Integer read GetDoubleFontSize write SetDoubleFontSize;
    property FontBold: Boolean read GetFontBold write SetFontBold;
    property FontItalic: Boolean read GetFontItalic write SetFontItalic;
    property Script: TdxCharacterFormattingScript read GetScript write SetScript;
    property FontStrikeoutType: TdxStrikeoutType read GetFontStrikeoutType write SetFontStrikeoutType;
    property FontUnderlineType: TdxUnderlineType read GetFontUnderlineType write SetFontUnderlineType;
    property AllCaps: Boolean read GetAllCaps write SetAllCaps;
    property UnderlineWordsOnly: Boolean read GetUnderlineWordsOnly write SetUnderlineWordsOnly;
    property StrikeoutWordsOnly: Boolean read GetStrikeoutWordsOnly write SetStrikeoutWordsOnly;
    property ForeColor: TColor read GetForeColor write SetForeColor;
    property BackColor: TColor read GetBackColor write SetBackColor;
    property UnderlineColor: TColor read GetUnderlineColor write SetUnderlineColor;
    property StrikeoutColor: TColor read GetStrikeoutColor write SetStrikeoutColor;
    property Hidden: Boolean read GetHidden write SetHidden;
    property NoProof: Boolean read GetNoProof write SetNoProof;
    property ParagraphStyle: TdxParagraphStyle read GetParagraphStyle;
    property Alignment: TdxParagraphAlignment read GetAlignment write SetAlignment;
    property LeftIndent: Integer read GetLeftIndent write SetLeftIndent;
    property RightIndent: Integer read GetRightIndent write SetRightIndent;
    property SpacingBefore: Integer read GetSpacingBefore write SetSpacingBefore;
    property SpacingAfter: Integer read GetSpacingAfter write SetSpacingAfter;
    property LineSpacingType: TdxParagraphLineSpacing read GetLineSpacingType write SetLineSpacingType;
    property LineSpacing: Single read GetLineSpacing write SetLineSpacing;
    property FirstLineIndentType: TdxParagraphFirstLineIndent read GetFirstLineIndentType write SetFirstLineIndentType;
    property FirstLineIndent: Integer read GetFirstLineIndent write SetFirstLineIndent;
    property SuppressHyphenation: Boolean read GetSuppressHyphenation write SetSuppressHyphenation;
    property SuppressLineNumbers: Boolean read GetSuppressLineNumbers write SetSuppressLineNumbers;
    property ContextualSpacing: Boolean read GetContextualSpacing write SetContextualSpacing;
    property PageBreakBefore: Boolean read GetPageBreakBefore write SetPageBreakBefore;
    property BeforeAutoSpacing: Boolean read GetBeforeAutoSpacing write SetBeforeAutoSpacing;
    property AfterAutoSpacing: Boolean read GetAfterAutoSpacing write SetAfterAutoSpacing;
    property KeepWithNext: Boolean read GetKeepWithNext write SetKeepWithNext;
    property KeepLinesTogether: Boolean read GetKeepLinesTogether write SetKeepLinesTogether;
    property WidowOrphanControl: Boolean read GetWidowOrphanControl write SetWidowOrphanControl;
    property OutlineLevel: Integer read GetOutlineLevel write SetOutlineLevel;
  end;

  { TdxNumberingListCollection }

  TdxNumberingListCollection = class(TdxFastObjectList) 
  private
    function GetItem(Index: Integer): TdxNumberingList;
  public
    function First: TdxNumberingList; {$IFDEF DELPHI9} inline; {$ENDIF}
    function Last: TdxNumberingList; {$IFDEF DELPHI9} inline; {$ENDIF}

    property Items[Index: Integer]: TdxNumberingList read GetItem; default;
  end;

  { TdxNumberingListStyle }

  TdxNumberingListStyle = class(TdxStyleBase)
  private
    FNumberingListIndex: TdxNumberingListIndex;
    function GetParent: TdxNumberingListStyle;
    procedure SetParent(const Value: TdxNumberingListStyle);
    function GetNumberingList: TdxNumberingList;
  protected
    function CopyFrom(ATargetModel: TdxCustomDocumentModel): TdxNumberingListStyle; virtual;
    function GetType: TdxStyleType; override;
    procedure MergePropertiesWithParent; override;
  public
    constructor Create(ADocumentModel: TdxCustomDocumentModel; ANumberingListIndex: TdxNumberingListIndex; const AStyleName: string = ''); reintroduce;

    function Copy(ATargetModel: TdxCustomDocumentModel): Integer; override;
    procedure CopyProperties(ASource: TdxStyleBase); override;
    procedure ResetCachedIndices(AResetFormattingCacheType: TdxResetFormattingCacheType); override;

    property NumberingListIndex: TdxNumberingListIndex read FNumberingListIndex;
    property NumberingList: TdxNumberingList read GetNumberingList;
    property Parent: TdxNumberingListStyle read GetParent write SetParent;
  end;

  { TdxNumberingListStyleCollection }

  TdxNumberingListStyleCollection = class(TdxStyleCollectionBase)
  const
    DefaultListStyleName = 'List';
  private
    function GetItem(Index: Integer): TdxNumberingListStyle;
  protected
    function CanDeleteStyle(AItem: TdxStyleBase): Boolean; override;
    function CreateDefaultItem: TdxStyleBase; override;
    procedure NotifyPieceTableStyleDeleting(APieceTable: TObject; AStyle: TdxStyleBase); override;
  public
    property Items[Index: Integer]: TdxNumberingListStyle read GetItem; default;
  end;

  { TdxAbstractStyleNumberingList }

  TdxAbstractStyleNumberingList = class(TdxAbstractNumberingList)
  private
    FNumberingStyleIndex: Integer;
    function GetNumberingListStyle: TdxNumberingListStyle;
  protected
  public
    constructor Create(ADocumentModel: TdxCustomDocumentModel; ANumberingStyleIndex: Integer);
    function CreateNumberingList: TdxAbstractNumberingList; override;

    property NumberingStyleIndex: Integer read FNumberingStyleIndex;
    property NumberingListStyle: TdxNumberingListStyle read GetNumberingListStyle;
  end;

  { TdxNumberingListCountersCalculator }

  TdxNumberingListCountersCalculator = class
  private
    FCounters: TIntegerDynArray;
    FList: TdxAbstractNumberingList;
    FUsedListIndex: TdxSortedList<TdxNumberingListIndex>;
    procedure AdvanceListLevelCounters(AParagraph: TObject; AListLevelIndex: Integer);
    procedure AdvanceSkippedLevelCounters(AListLevelIndex: Integer);
    function GetActualRangeCounters(AListLevelIndex: Integer): TIntegerDynArray;
    function GetCounterCount: Integer;
    procedure RestartNextLevelCounters(AListLevelIndex: Integer);
  protected
    procedure CreateListLevelCounters;
    function ShouldAdvanceListLevelCounters(AParagraph: TObject;
      AbstractNumberingList: TdxAbstractNumberingList): Boolean;
  public
    constructor Create(AList: TdxAbstractNumberingList);
    destructor Destroy; override;

    procedure BeginCalculateCounters;
    procedure EndCalculateCounters;

    function CalculateCounters(AParagraph: TObject): TIntegerDynArray;
    function CalculateNextCounters(ACurrentParagraph: TObject): TIntegerDynArray;

    property List: TdxAbstractNumberingList read FList;
    property Counters: TIntegerDynArray read FCounters write FCounters;
    property CounterCount: Integer read GetCounterCount;
  end;

  { TdxOrdinalBasedNumberConverter }

  TdxOrdinalBasedNumberConverter = class abstract
  protected 
    function GetType: TdxNumberingFormat; virtual; abstract;
    function GetMinValue: Integer; virtual;
    function GetMaxValue: Integer; virtual;

    property &Type: TdxNumberingFormat read GetType;
    property MinValue: Integer read GetMinValue;
    property MaxValue: Integer read GetMaxValue;
  public
    function ConvertNumber(AValue: LongInt): string; virtual; abstract;
    class function GetOrdinalNumberConverterByLanguage(ALanguageId: Integer): TdxOrdinalBasedNumberConverter;
    class function GetDescriptiveCardinalNumberConverterByLanguage(ALanguageId: Integer): TdxOrdinalBasedNumberConverter;
    class function GetDescriptiveOrdinalNumberConverterByLanguage(ALanguageId: Integer): TdxOrdinalBasedNumberConverter;
    class function CreateConverter(AFormat: TdxNumberingFormat; ALanguageId: Integer): TdxOrdinalBasedNumberConverter;
    class function GetSupportNumberingFormat: TList<TdxNumberingFormat>;
  end;

  { TdxDecimalNumberConverter }

  TdxDecimalNumberConverter = class(TdxOrdinalBasedNumberConverter)
  protected
    function GetType: TdxNumberingFormat; override;
  public
    function ConvertNumber(AValue: LongInt): string; override;
  end;

  { TdxAlphabetBasedNumberConverter }

  TdxAlphabetBasedNumberConverter = class abstract(TdxOrdinalBasedNumberConverter)
  protected
    function GetAlphabet: TArray<Char>; virtual; abstract;
    function GetAlphabetSize: Integer; virtual; abstract;
    function GetMinValue: Integer; override;
    function GetMaxValue: Integer; override;

    property Alphabet: TArray<Char> read GetAlphabet;
    property AlphabetSize: Integer read GetAlphabetSize;
  public
    function ConvertNumber(AValue: LongInt): string; override;
  end;

  { TdxUpperLatinLetterNumberConverter }

  TdxUpperLatinLetterNumberConverter = class(TdxAlphabetBasedNumberConverter)
  strict protected
    class var FAlphabetSize: Integer;
    class var FUpperLetter: TArray<Char>;
    class constructor Initialize;
  protected
    function GetAlphabet: TArray<Char>; override;
    function GetAlphabetSize: Integer; override;
    function GetType: TdxNumberingFormat; override;
  end;

  { TdxLowerLatinLetterNumberConverter }

  TdxLowerLatinLetterNumberConverter = class(TdxAlphabetBasedNumberConverter)
  strict protected
    class var FAlphabetSize: Integer;
    class var FUpperLetter: TArray<Char>;
    class constructor Initialize;
  protected
    function GetAlphabet: TArray<Char>; override;
    function GetAlphabetSize: Integer; override;
    function GetType: TdxNumberingFormat; override;
  end;

  { TdxRomanNumberConverter }

  TdxRomanNumberConverter = class abstract(TdxOrdinalBasedNumberConverter)
  strict protected
    class var FArabics: TArray<Integer>;
    class constructor Initialize;
  protected
    function GetMinValue: Integer; override;
    function GetRomans: TArray<string>; virtual; abstract;

    property Romans: TArray<string> read GetRomans;
  public
    function ConvertNumber(AValue: LongInt): string; override;
  end;

  { TdxUpperRomanNumberConverter }

  TdxUpperRomanNumberConverter = class(TdxRomanNumberConverter)
  strict protected
    class var FRomans: TArray<string>;
    class constructor Initialize;
  protected
    function GetType: TdxNumberingFormat; override;
    function GetRomans: TArray<string>; override;
  end;

  { TdxLowerRomanNumberConverter }

  TdxLowerRomanNumberConverter = class(TdxRomanNumberConverter)
  strict protected
    class var FRomans: TArray<string>;
    class constructor Initialize;
  protected
    function GetType: TdxNumberingFormat; override;
    function GetRomans: TArray<string>; override;
  end;

  { TdxNumberInDashNumberConverter }

  TdxNumberInDashNumberConverter = class(TdxOrdinalBasedNumberConverter)
  protected
    function GetType: TdxNumberingFormat; override;
  public
    function ConvertNumber(AValue: LongInt): string; override;
  end;

  { TdxDecimalZeroNumberConverter }

  TdxDecimalZeroNumberConverter = class(TdxOrdinalBasedNumberConverter)
  protected
    function GetType: TdxNumberingFormat; override;
  public
    function ConvertNumber(AValue: LongInt): string; override;
  end;

  { TdxBulletNumberConverter }

  TdxBulletNumberConverter = class(TdxOrdinalBasedNumberConverter)
  protected
    function GetType: TdxNumberingFormat; override;
  public
    function ConvertNumber(AValue: LongInt): string; override;
  end;

  { TdxRussianUpperNumberConverter }

  TdxRussianUpperNumberConverter = class(TdxAlphabetBasedNumberConverter)
  strict protected
    class var FAlphabetSize: Integer;
    class var FRussianUpper: TArray<Char>;
    class constructor Initialize;
  protected
    function GetAlphabet: TArray<Char>; override;
    function GetAlphabetSize: Integer; override;
    function GetType: TdxNumberingFormat; override;
  end;

  { TdxRussianLowerNumberConverter }

  TdxRussianLowerNumberConverter = class(TdxAlphabetBasedNumberConverter)
  strict protected
    class var FAlphabetSize: Integer;
    class var FRussianLower: TArray<Char>;
    class constructor Initialize;
  protected
    function GetAlphabet: TArray<Char>; override;
    function GetAlphabetSize: Integer; override;
    function GetType: TdxNumberingFormat; override;
  end;

  { TdxDecimalEnclosedParenthesesNumberConverter }

  TdxDecimalEnclosedParenthesesNumberConverter = class(TdxOrdinalBasedNumberConverter)
  protected
    function GetType: TdxNumberingFormat; override;
  public
    function ConvertNumber(AValue: LongInt): string; override;
  end;

  { TdxNumberingListIndexCalculator }

  TdxNumberingListIndexCalculator = class
  private
    FModel: TdxCustomDocumentModel;
    FNumberingListType: TdxNumberingType;
    FContinueList: Boolean;
    FNestingLevel: Integer;
  protected
    function GetListIndexCore(AParagraphIndex: TdxParagraphIndex): TdxNumberingListIndex;
    function CreateNewAbstractList(ASource: TdxAbstractNumberingList): TdxAbstractNumberingListIndex;

    property DocumentModel: TdxCustomDocumentModel read FModel;
  public
    constructor Create(AModel: TdxCustomDocumentModel; ANumberingListType: TdxNumberingType);
    function GetListIndex(AStart, AEnd: TdxParagraphIndex): TdxNumberingListIndex; virtual;
    function CreateNewList(ASource: TdxAbstractNumberingList): TdxNumberingListIndex; virtual;

    property NumberingListType: TdxNumberingType read FNumberingListType;
    property ContinueList: Boolean read FContinueList write FContinueList;
    property NestingLevel: Integer read FNestingLevel write FNestingLevel;
  end;

  { TdxDefaultNumberingListHelper }

  TdxDefaultNumberingListHelper = class
  protected
    class procedure SetTemplateCode(const ALevel: IdxListLevel; ATemplateCode: Integer);
    class procedure SetDisplayFormatString(const ALevel: IdxListLevel; const ADisplayFormatString: string);
    class procedure SetFirstLineIndent(const ALevel: IdxListLevel; ALineIndent, AFirstLineIndent: Integer);
    class procedure InsertDefaultBulletNumberingList(DocumentModel: TdxCustomDocumentModel;
      AUnitConverter: TdxDocumentModelUnitConverter; ADefaultTabWidth: Integer);
    class procedure InsertDefaultSimpleNumberingList(DocumentModel: TdxCustomDocumentModel;
      AUnitConverter: TdxDocumentModelUnitConverter; ADefaultTabWidth: Integer);
    class procedure InsertDefaultMultilevelNumberingList(DocumentModel: TdxCustomDocumentModel;
      AUnitConverter: TdxDocumentModelUnitConverter; ADefaultTabWidth: Integer);
  public
    class procedure InsertNumberingLists(ADocumentModel: TdxCustomDocumentModel; AUnitConverter: TdxDocumentModelUnitConverter; ADefaultTabWidth: Integer);
  end;

implementation

uses
  dxRichEdit.Utils.BatchUpdateHelper, 
  StrUtils, RTLConsts, dxCore, dxRichEdit.DocumentModel.PieceTable,
  dxRichEdit.Utils.Characters, Math;

type

  { TdxListLevelHelper }

  TdxListLevelHelper = class helper for TdxListLevel
  private
    function GetDocumentModel: TdxDocumentModel;
  public
    property DocumentModel: TdxDocumentModel read GetDocumentModel;
  end;

  function TdxListLevelHelper.GetDocumentModel: TdxDocumentModel;
  begin
    Result := TdxDocumentModel(inherited DocumentModel);
  end;

type

  { TdxNumberingListHelper }

  TNumberingListHelper = class helper for TdxNumberingList
  private
    function GetDocumentModel: TdxDocumentModel;
  public
    property DocumentModel: TdxDocumentModel read GetDocumentModel;
  end;

  function TNumberingListHelper.GetDocumentModel: TdxDocumentModel;
  begin
    Result := TdxDocumentModel(inherited DocumentModel);
  end;

type

  { TdxNumberingListHelper }

  TdxNumberingListIndexCalculatorHelper = class helper for TdxNumberingListIndexCalculator
  private
    function GetActivePieceTable: TdxPieceTable;
    function GetDocumentModel: TdxDocumentModel;
  public
    property ActivePieceTable: TdxPieceTable read GetActivePieceTable;
    property DocumentModel: TdxDocumentModel read GetDocumentModel;
  end;

  function TdxNumberingListIndexCalculatorHelper.GetActivePieceTable: TdxPieceTable;
  begin
    Result := DocumentModel.ActivePieceTable;
  end;

  function TdxNumberingListIndexCalculatorHelper.GetDocumentModel: TdxDocumentModel;
  begin
    Result := TdxDocumentModel(inherited DocumentModel);
  end;

{ TdxNumberingListReferenceLevel }

constructor TdxNumberingListReferenceLevel.Create(AOwner: TdxNumberingList; ALevel: Integer);
begin
  inherited Create;
  Assert(AOwner <> nil);
  Assert(ALevel >= 0);
  FOwner := AOwner;
  SetLevelCore(ALevel);
end;

procedure TdxNumberingListReferenceLevel.SetOutlineLevel(const Value: Integer);
begin
  OwnerLevel.OutlineLevel := Value;
end;

procedure TdxNumberingListReferenceLevel.SetOverrideStart(AValue: Boolean);
begin
  FOverrideStart := AValue;
end;

procedure TdxNumberingListReferenceLevel.SetPageBreakBefore(const Value: Boolean);
begin
  OwnerLevel.PageBreakBefore := Value;
end;

procedure TdxNumberingListReferenceLevel.SetRightIndent(const Value: Integer);
begin
  OwnerLevel.RightIndent := Value;
end;

procedure TdxNumberingListReferenceLevel.SetScript(const Value: TdxCharacterFormattingScript);
begin
  OwnerLevel.Script := Value; 
end;

procedure TdxNumberingListReferenceLevel.SetSpacingAfter(const Value: Integer);
begin
  OwnerLevel.SpacingAfter := Value;
end;

procedure TdxNumberingListReferenceLevel.SetSpacingBefore(const Value: Integer);
begin
  OwnerLevel.SpacingBefore := Value;
end;

procedure TdxNumberingListReferenceLevel.SetStrikeoutColor(const Value: TColor);
begin
  OwnerLevel.StrikeoutColor := Value;
end;

procedure TdxNumberingListReferenceLevel.SetStrikeoutWordsOnly(const Value: Boolean);
begin
  OwnerLevel.StrikeoutWordsOnly := Value;
end;

procedure TdxNumberingListReferenceLevel.SetSuppressHyphenation(const Value: Boolean);
begin
  OwnerLevel.SuppressHyphenation := Value;
end;

procedure TdxNumberingListReferenceLevel.SetSuppressLineNumbers(const Value: Boolean);
begin
  OwnerLevel.SuppressLineNumbers := Value;
end;

procedure TdxNumberingListReferenceLevel.SetUnderlineColor(const Value: TColor);
begin
  OwnerLevel.UnderlineColor := Value;
end;

procedure TdxNumberingListReferenceLevel.SetUnderlineWordsOnly(const Value: Boolean);
begin
  OwnerLevel.UnderlineWordsOnly := Value; 
end;

procedure TdxNumberingListReferenceLevel.SetWidowOrphanControl(const Value: Boolean);
begin
  OwnerLevel.WidowOrphanControl := Value;
end;

procedure TdxNumberingListReferenceLevel.SetAfterAutoSpacing(const Value: Boolean);
begin
  OwnerLevel.AfterAutoSpacing := Value;
end;

procedure TdxNumberingListReferenceLevel.SetAlignment(const Value: TdxParagraphAlignment);
begin
  OwnerLevel.Alignment := Value;
end;

procedure TdxNumberingListReferenceLevel.SetAllCaps(const Value: Boolean);
begin
  OwnerLevel.AllCaps := Value; 
end;

procedure TdxNumberingListReferenceLevel.SetBackColor(const Value: TColor);
begin
  OwnerLevel.BackColor := Value;
end;

procedure TdxNumberingListReferenceLevel.SetBeforeAutoSpacing(const Value: Boolean);
begin
  OwnerLevel.BeforeAutoSpacing := Value;
end;

procedure TdxNumberingListReferenceLevel.SetContextualSpacing(const Value: Boolean);
begin
  OwnerLevel.ContextualSpacing := Value;
end;

procedure TdxNumberingListReferenceLevel.SetDoubleFontSize(const Value: Integer);
begin
  OwnerLevel.DoubleFontSize := Value; 
end;

procedure TdxNumberingListReferenceLevel.SetFirstLineIndent(const Value: Integer);
begin
  OwnerLevel.FirstLineIndent := Value;
end;

procedure TdxNumberingListReferenceLevel.SetFirstLineIndentType(const Value: TdxParagraphFirstLineIndent);
begin
  OwnerLevel.FirstLineIndentType := Value;
end;

procedure TdxNumberingListReferenceLevel.SetFontBold(const Value: Boolean);
begin
  OwnerLevel.FontBold := Value; 
end;

procedure TdxNumberingListReferenceLevel.SetFontItalic(const Value: Boolean);
begin
  OwnerLevel.FontItalic := Value; 
end;

procedure TdxNumberingListReferenceLevel.SetFontName(const Value: string);
begin
  OwnerLevel.FontName := Value;
end;

procedure TdxNumberingListReferenceLevel.SetFontStrikeoutType(const Value: TdxStrikeoutType);
begin
  OwnerLevel.FontStrikeoutType := Value; 
end;

procedure TdxNumberingListReferenceLevel.SetFontUnderlineType(const Value: TdxUnderlineType);
begin
  OwnerLevel.FontUnderlineType := Value; 
end;

procedure TdxNumberingListReferenceLevel.SetForeColor(const Value: TColor);
begin
  OwnerLevel.ForeColor := Value;
end;

procedure TdxNumberingListReferenceLevel.SetHidden(const Value: Boolean);
begin
  OwnerLevel.Hidden := Value;
end;

procedure TdxNumberingListReferenceLevel.SetKeepLinesTogether(const Value: Boolean);
begin
  OwnerLevel.KeepLinesTogether := Value;
end;

procedure TdxNumberingListReferenceLevel.SetKeepWithNext(const Value: Boolean);
begin
  OwnerLevel.KeepWithNext := Value;
end;

procedure TdxNumberingListReferenceLevel.SetLeftIndent(const Value: Integer);
begin
  OwnerLevel.LeftIndent := Value;
end;

procedure TdxNumberingListReferenceLevel.SetLevelCore(ALevel: Integer);
begin
  FLevel := ALevel;
end;

procedure TdxNumberingListReferenceLevel.SetLineSpacing(const Value: Single);
begin
  OwnerLevel.LineSpacing := Value;
end;

procedure TdxNumberingListReferenceLevel.SetLineSpacingType(const Value: TdxParagraphLineSpacing);
begin
  OwnerLevel.LineSpacingType := Value;
end;

function TdxNumberingListReferenceLevel.GetOwnerLevel: TdxListLevel;
begin
  Result := Owner.AbstractNumberingList.Levels.Items[Level] as TdxListLevel;
end;

function TdxNumberingListReferenceLevel.GetFirstLineIndent: Integer;
begin
  Result := OwnerLevel.FirstLineIndent;
end;

function TdxNumberingListReferenceLevel.GetFirstLineIndentType: TdxParagraphFirstLineIndent;
begin
  Result := OwnerLevel.FirstLineIndentType;
end;

function TdxNumberingListReferenceLevel.GetFontBold: Boolean;
begin
  Result := OwnerLevel.FontBold;
end;

function TdxNumberingListReferenceLevel.GetFontCacheIndex: Integer;
begin
  Result := (OwnerLevel as IdxInternalListLevel).FontCacheIndex;
end;

function TdxNumberingListReferenceLevel.GetFontItalic: Boolean;
begin
  Result := OwnerLevel.FontItalic; 
end;

function TdxNumberingListReferenceLevel.GetFontName: string;
begin
  Result := OwnerLevel.FontName;
end;

function TdxNumberingListReferenceLevel.GetFontStrikeoutType: TdxStrikeoutType;
begin
  Result := OwnerLevel.FontStrikeoutType; 
end;

function TdxNumberingListReferenceLevel.GetFontUnderlineType: TdxUnderlineType;
begin
  Result := OwnerLevel.FontUnderlineType; 
end;

function TdxNumberingListReferenceLevel.GetForeColor: TColor;
begin
  Result := OwnerLevel.ForeColor;
end;

function TdxNumberingListReferenceLevel.GetHidden: Boolean;
begin
  Result := OwnerLevel.Hidden;
end;

function TdxNumberingListReferenceLevel.GetKeepLinesTogether: Boolean;
begin
  Result := OwnerLevel.KeepLinesTogether;
end;

function TdxNumberingListReferenceLevel.GetKeepWithNext: Boolean;
begin
  Result := OwnerLevel.KeepWithNext;
end;

function TdxNumberingListReferenceLevel.GetLeftIndent: Integer;
begin
  Result := OwnerLevel.LeftIndent;
end;

function TdxNumberingListReferenceLevel.GetLineSpacing: Single;
begin
  Result := OwnerLevel.LineSpacing;
end;

function TdxNumberingListReferenceLevel.GetLineSpacingType: TdxParagraphLineSpacing;
begin
  Result := OwnerLevel.LineSpacingType;
end;

function TdxNumberingListReferenceLevel.GetListLevelProperties: TdxListLevelProperties;
begin
  Result := OwnerLevel.ListLevelProperties;
end;

function TdxNumberingListReferenceLevel.GetPageBreakBefore: Boolean;
begin
  Result := OwnerLevel.PageBreakBefore;
end;

function TdxNumberingListReferenceLevel.GetParagraphProperties: TdxParagraphProperties;
begin
  Result := OwnerLevel.ParagraphProperties;
end;

function TdxNumberingListReferenceLevel.GetParagraphStyle: TdxParagraphStyle;
begin
  Result := OwnerLevel.ParagraphStyle;
end;

function TdxNumberingListReferenceLevel.GetRightIndent: Integer;
begin
  Result := OwnerLevel.RightIndent;
end;

function TdxNumberingListReferenceLevel.GetScript: TdxCharacterFormattingScript;
begin
  Result := OwnerLevel.Script; 
end;

function TdxNumberingListReferenceLevel.GetSpacingAfter: Integer;
begin
  Result := OwnerLevel.SpacingAfter;
end;

function TdxNumberingListReferenceLevel.GetSpacingBefore: Integer;
begin
  Result := OwnerLevel.SpacingBefore;
end;

function TdxNumberingListReferenceLevel.GetStrikeoutColor: TColor;
begin
  Result := OwnerLevel.StrikeoutColor;
end;

function TdxNumberingListReferenceLevel.GetStrikeoutWordsOnly: Boolean;
begin
  Result := OwnerLevel.StrikeoutWordsOnly;
end;

function TdxNumberingListReferenceLevel.GetSuppressHyphenation: Boolean;
begin
  Result := OwnerLevel.SuppressHyphenation;
end;

function TdxNumberingListReferenceLevel.GetSuppressLineNumbers: Boolean;
begin
  Result := OwnerLevel.SuppressLineNumbers;
end;

function TdxNumberingListReferenceLevel.GetUnderlineColor: TColor;
begin
  Result := OwnerLevel.UnderlineColor;
end;

function TdxNumberingListReferenceLevel.GetUnderlineWordsOnly: Boolean;
begin
  Result := OwnerLevel.UnderlineWordsOnly; 
end;

function TdxNumberingListReferenceLevel.GetWidowOrphanControl: Boolean;
begin
  Result := OwnerLevel.WidowOrphanControl;
end;

function TdxNumberingListReferenceLevel.GetAfterAutoSpacing: Boolean;
begin
  Result := OwnerLevel.AfterAutoSpacing;
end;

function TdxNumberingListReferenceLevel.GetAlignment: TdxParagraphAlignment;
begin
  Result := OwnerLevel.Alignment;
end;

function TdxNumberingListReferenceLevel.GetAllCaps: Boolean;
begin
  Result := OwnerLevel.AllCaps;
end;

function TdxNumberingListReferenceLevel.GetBackColor: TColor;
begin
  Result := OwnerLevel.BackColor;
end;

function TdxNumberingListReferenceLevel.GetBeforeAutoSpacing: Boolean;
begin
  Result := OwnerLevel.BeforeAutoSpacing;
end;

function TdxNumberingListReferenceLevel.GetBulletLevel: Boolean;
begin
  Result := OwnerLevel.BulletLevel;
end;

function TdxNumberingListReferenceLevel.GetOutlineLevel: Integer;
begin
  Result := OwnerLevel.OutlineLevel;
end;

function TdxNumberingListReferenceLevel.GetOverrideStart: Boolean;
begin
  Result := FOverrideStart;
end;

function TdxNumberingListReferenceLevel.GetNewStart: Integer;
begin
  Result := FNewStart;
end;

function TdxNumberingListReferenceLevel.GetNoProof: Boolean;
begin
  Result := OwnerLevel.NoProof;
end;

procedure TdxNumberingListReferenceLevel.SetNewStart(const Value: Integer);
begin
  FNewStart := Value;
end;

procedure TdxNumberingListReferenceLevel.SetNoProof(const Value: Boolean);
begin
  OwnerLevel.NoProof := Value;
end;

function TdxNumberingListReferenceLevel.GetCharacterProperties: TdxCharacterProperties;
begin
  Result := OwnerLevel.CharacterProperties;
end;

function TdxNumberingListReferenceLevel.GetContextualSpacing: Boolean;
begin
  Result := OwnerLevel.ContextualSpacing;
end;

function TdxNumberingListReferenceLevel.GetDocumentModel: TdxCustomDocumentModel;
begin
  Result := OwnerLevel.DocumentModel;
end;

function TdxNumberingListReferenceLevel.GetDoubleFontSize: Integer;
begin
  Result := OwnerLevel.DoubleFontSize;
end;

{ TdxNumberingList }

function TdxNumberingList.CanRemove: Boolean;
begin
  Result := FReferringParagraphsCount <= 0;
end;

function TdxNumberingList.Clone: TdxNumberingList;
begin
  Result := TdxNumberingList.Create(DocumentModel, FAbstractNumberingListIndex);
  Result.CopyFrom(Self);
end;

function TdxNumberingList.CreateLevel(ALevelIndex: Integer): TdxAbstractListLevel;
begin
  Result := TdxNumberingListReferenceLevel.Create(Self, ALevelIndex);
end;

procedure TdxNumberingList.CopyLevelsFrom(ASourceLevels: TdxListLevelCollection);
var
  I: Integer;
  ALevel: TdxAbstractListLevel;
  ANewLevel, ASourceLevel: TdxOverrideListLevel;
  ANewILevel, ASourceListLevel: IdxOverrideListLevel;
begin
  for I := 0 to ASourceLevels.Count - 1 do
  begin
    if ASourceLevels[I] is TdxOverrideListLevel then
    begin
      ASourceLevel := TdxOverrideListLevel(ASourceLevels[I]);
      ANewLevel := TdxOverrideListLevel.Create(DocumentModel);
      ANewLevel.CopyFrom(ASourceLevel);
      Levels[I] := ANewLevel;
    end
    else
    begin
      ASourceListLevel := ASourceLevels[I] as IdxOverrideListLevel;

      ALevel := CreateLevel(I);
      ANewILevel := ALevel as IdxOverrideListLevel;
      ANewILevel.SetOverrideStart(ASourceListLevel.OverrideStart);
      ANewILevel.NewStart := ASourceListLevel.NewStart;
      Levels[I] := ALevel;
    end;
  end;
end;

constructor TdxNumberingList.Create(ADocumentModel: TdxCustomDocumentModel;
  AAbstractNumberingListIndex: TdxAbstractNumberingListIndex; ALevelCount: Integer = 9);
begin
  inherited Create(ADocumentModel, ALevelCount);
  if (AAbstractNumberingListIndex < 0) or
    (AAbstractNumberingListIndex > DocumentModel.AbstractNumberingLists.Count - 1) then
    raise Exception.Create('Error "AAbstractNumberingListIndex"'); 
  FAbstractNumberingListIndex := AAbstractNumberingListIndex;
end;

function TdxNumberingList.GenerateNewId: Integer;
begin
  Result := DocumentModel.NumberingListIdProvider.GetNextId;
end;

function TdxNumberingList.GetAbstractNumberingList: TdxAbstractNumberingList;
begin
  Result := DocumentModel.AbstractNumberingLists[AbstractNumberingListIndex];
end;

function TdxNumberingList.GetLevels: IdxReadOnlyIListLevelCollection;
begin
  Result := Levels;
end;

procedure TdxNumberingList.OnParagraphAdded;
begin
  Inc(FReferringParagraphsCount);
end;

procedure TdxNumberingList.OnParagraphRemoved;
begin
  Dec(FReferringParagraphsCount);
end;

{ TdxNumberingListCollection }

function TdxNumberingListCollection.First: TdxNumberingList;
begin
  Result := TdxNumberingList(inherited First);
end;

function TdxNumberingListCollection.GetItem(Index: Integer): TdxNumberingList;
begin
  Result := TdxNumberingList(inherited Items[Index]);
end;

function TdxNumberingListCollection.Last: TdxNumberingList;
begin
  Result := TdxNumberingList(inherited Last);
end;

{ TdxNumberingListStyle }

constructor TdxNumberingListStyle.Create(ADocumentModel: TdxCustomDocumentModel; ANumberingListIndex: TdxNumberingListIndex;
  const AStyleName: string);
begin
  inherited Create(ADocumentModel, nil, AStyleName);
  if ANumberingListIndex < 0 then
    raise Exception.Create('ThrowArgumentException("numberingListIndex", numberingListIndex');
  FNumberingListIndex := ANumberingListIndex;
end;

function TdxNumberingListStyle.Copy(ATargetModel: TdxCustomDocumentModel): Integer;
var
  I: Integer;
  ATarget: TdxDocumentModel absolute ATargetModel;
begin
  for I := 0 to  ATarget.NumberingListStyles.Count - 1 do
    if StyleName = ATarget.NumberingListStyles[I].StyleName then
      Exit(I);
  Result := ATarget.NumberingListStyles.AddNewStyle(CopyFrom(ATarget));
end;

function TdxNumberingListStyle.CopyFrom(ATargetModel: TdxCustomDocumentModel): TdxNumberingListStyle;
begin
  Result := TdxNumberingListStyle.Create(ATargetModel, NumberingListIndex, StyleName);
  Assert(Parent = nil);
end;

procedure TdxNumberingListStyle.CopyProperties(ASource: TdxStyleBase);
begin
  FNumberingListIndex := TdxNumberingListStyle(ASource).NumberingListIndex;
end;

function TdxNumberingListStyle.GetNumberingList: TdxNumberingList;
begin
  Result := TdxDocumentModel(DocumentModel).NumberingLists[FNumberingListIndex];
end;

function TdxNumberingListStyle.GetParent: TdxNumberingListStyle;
begin
  Result := TdxNumberingListStyle(inherited Parent);
end;

function TdxNumberingListStyle.GetType: TdxStyleType;
begin
  Result := TdxStyleType.NumberingListStyle;
end;

procedure TdxNumberingListStyle.MergePropertiesWithParent;
begin
  Assert(Parent = nil);
end;

procedure TdxNumberingListStyle.ResetCachedIndices(AResetFormattingCacheType: TdxResetFormattingCacheType);
begin
end;

procedure TdxNumberingListStyle.SetParent(const Value: TdxNumberingListStyle);
begin
  inherited Parent := Value;
end;

{ TdxNumberingListStyleCollection }

function TdxNumberingListStyleCollection.CanDeleteStyle(AItem: TdxStyleBase): Boolean;
begin
  Result := False; 
end;

function TdxNumberingListStyleCollection.CreateDefaultItem: TdxStyleBase;
begin
  Result := nil;
end;

function TdxNumberingListStyleCollection.GetItem(Index: Integer): TdxNumberingListStyle;
begin
  Result := TdxNumberingListStyle(inherited Items[Index]);
end;

procedure TdxNumberingListStyleCollection.NotifyPieceTableStyleDeleting(APieceTable: TObject;
  AStyle: TdxStyleBase);
begin
end;

{ TdxAbstractStyleNumberingList }

constructor TdxAbstractStyleNumberingList.Create(ADocumentModel: TdxCustomDocumentModel; ANumberingStyleIndex: Integer);
begin
  inherited Create(ADocumentModel);
  if ANumberingStyleIndex < 0 then
    raise Exception.Create('ThrowArgumentException("numberingStyleIndex", numberingStyleIndex');
  FNumberingStyleIndex := ANumberingStyleIndex;
end;

function TdxAbstractStyleNumberingList.GetNumberingListStyle;
begin
  Result := TdxDocumentModel(DocumentModel).NumberingListStyles[NumberingStyleIndex];
end;

function TdxAbstractStyleNumberingList.CreateNumberingList: TdxAbstractNumberingList;
begin
  Assert(False);
  Result := inherited CreateNumberingList;
end;

{ TdxNumberingListCountersCalculator }

constructor TdxNumberingListCountersCalculator.Create(AList: TdxAbstractNumberingList);
begin
  inherited Create;
  FList := AList;
end;

destructor TdxNumberingListCountersCalculator.Destroy;
begin
  FUsedListIndex.Free;
  inherited Destroy;
end;

procedure TdxNumberingListCountersCalculator.BeginCalculateCounters;
begin
  CreateListLevelCounters;
  FreeAndNil(FUsedListIndex);
  FUsedListIndex := TdxSortedList<TdxNumberingListIndex>.Create(TComparer<TdxNumberingListIndex>.Default);
end;

procedure TdxNumberingListCountersCalculator.EndCalculateCounters;
begin
// do nothing
end;

function TdxNumberingListCountersCalculator.CalculateCounters(AParagraph: TObject): TIntegerDynArray;
var
  AbstractNumberingList: TdxAbstractNumberingList;
  I: Integer;
  ACurrentParagraph: TdxParagraph;
  AInnerParagraph: TdxParagraph absolute AParagraph;
begin
  BeginCalculateCounters;
  try
    AbstractNumberingList := AInnerParagraph.GetAbstractNumberingList;
    Assert(List <> nil); 
    for I := 0 to AInnerParagraph.Index do
    begin
      ACurrentParagraph := AInnerParagraph.PieceTable.Paragraphs[I];
      if ShouldAdvanceListLevelCounters(ACurrentParagraph, AbstractNumberingList) then
        AdvanceListLevelCounters(AcurrentParagraph, ACurrentParagraph.GetListLevelIndex);
    end;
    Result := GetActualRangeCounters(AInnerParagraph.GetListLevelIndex);
  finally
    EndCalculateCounters;
  end;
end;

function TdxNumberingListCountersCalculator.CalculateNextCounters(ACurrentParagraph: TObject): TIntegerDynArray;
var
  AbstractNumberingList: TdxAbstractNumberingList;
  AParagraph: TdxParagraph absolute ACurrentParagraph;
begin
  AbstractNumberingList := AParagraph.GetAbstractNumberingList;
  if ShouldAdvanceListLevelCounters(AParagraph, AbstractNumberingList) then
    AdvanceListLevelCounters(AParagraph, AParagraph.GetListLevelIndex);
  Result := GetActualRangeCounters(AParagraph.GetListLevelIndex);
end;

procedure TdxNumberingListCountersCalculator.CreateListLevelCounters;
var
  ALevels: TdxListLevelCollection;
  I: Integer;
  AListLevel: IdxListLevel; 
begin
  ALevels := List.Levels;
  SetLength(FCounters, CounterCount);
  for I := 0 to CounterCount - 1 do
  begin
    Supports(ALevels[I], IdxListLevel, AListLevel);
    FCounters[I] := AListLevel.ListLevelProperties.Start - 1;
  end;
end;

function TdxNumberingListCountersCalculator.ShouldAdvanceListLevelCounters(AParagraph: TObject;
  AbstractNumberingList: TdxAbstractNumberingList): Boolean;
begin
  Result := TdxParagraph(AParagraph).GetAbstractNumberingList = AbstractNumberingList;
end;

procedure TdxNumberingListCountersCalculator.AdvanceListLevelCounters(AParagraph: TObject; AListLevelIndex: Integer);
var
  ANumberingListIndex: TdxNumberingListIndex;
  ANumberingList: TdxNumberingList;
  ALevel: IdxOverrideListLevel;
begin
  ANumberingListIndex := TdxParagraph(AParagraph).GetNumberingListIndex;
  ANumberingList := TdxParagraph(AParagraph).DocumentModel.NumberingLists[ANumberingListIndex];
  if not Supports(ANumberingList.Levels[AListLevelIndex], IdxOverrideListLevel, ALevel) then
    Assert(False);
  if ALevel.OverrideStart and not FUsedListIndex.Contains(ANumberingListIndex) then
  begin
    FUsedListIndex.Add(ANumberingListIndex);
    Counters[AListLevelIndex] := ALevel.NewStart;
  end
  else
    Counters[AListLevelIndex] := Counters[AListLevelIndex] + 1;
  AdvanceSkippedLevelCounters(AListLevelIndex);
  RestartNextLevelCounters(AListLevelIndex);
end;

procedure TdxNumberingListCountersCalculator.AdvanceSkippedLevelCounters(AListLevelIndex: Integer);
var
  ALevels: TdxListLevelCollection;
  I: Integer;
  AListLevel: IdxListLevel; 
begin
  ALevels := List.Levels;
  for I := 0 to AListLevelIndex - 1 do
  begin
    if not Supports(ALevels[I], IdxListLevel, AListLevel) then
      Assert(False);
    if Counters[I] = AListLevel.ListLevelProperties.Start - 1 then
      Counters[I] := Counters[I] + 1;
  end;
end;

function TdxNumberingListCountersCalculator.GetActualRangeCounters(AListLevelIndex: Integer): TIntegerDynArray;
var
  I: Integer;
begin
  SetLength(Result, AListLevelIndex + 1);
  for I := 0 to AListLevelIndex do
    Result[I] := Counters[I];
end;

function TdxNumberingListCountersCalculator.GetCounterCount: Integer;
begin
  Result := List.Levels.Count;
end;

procedure TdxNumberingListCountersCalculator.RestartNextLevelCounters(AListLevelIndex: Integer);
var
  ALevels: TdxListLevelCollection;
  I: Integer;
  ARestartedLevels: array of Boolean;
  AProperties: TdxListLevelProperties;
  ARestartLevel: Integer;
  AListLevel: IdxListLevel; 
begin
  ALevels := List.Levels;
  SetLength(ARestartedLevels, CounterCount);
  try
    ARestartedLevels[AListLevelIndex] := True;
    for I := AListLevelIndex + 1 to CounterCount - 1 do
    begin
      if not Supports(ALevels[I], IdxListLevel, AListLevel) then
        Assert(False);
      AProperties := AListLevel.ListLevelProperties;
      if not AProperties.SuppressRestart then
      begin
        ARestartLevel := I - AProperties.RelativeRestartLevel - 1;
        if (ARestartLevel >= 0) and (ARestartLevel < CounterCount) and ARestartedLevels[ARestartLevel] then
        begin
          Counters[I] := AProperties.Start - 1;
          ARestartedLevels[I] := True;
        end;
      end;
    end;
  finally
    SetLength(ARestartedLevels, 0);
  end;
end;

{ TdxListLevelInfo }

procedure TdxListLevelInfo.CopyFrom(const AInfo: TdxListLevelInfo);
begin
  Start := AInfo.Start;
  Format := AInfo.Format;
  Alignment := AInfo.Alignment;
  ConvertPreviousLevelNumberingToDecimal := AInfo.ConvertPreviousLevelNumberingToDecimal;
  Separator := AInfo.Separator;
  SuppressRestart := AInfo.SuppressRestart;
  SuppressBulletResize := AInfo.SuppressBulletResize;
  DisplayFormatString := AInfo.DisplayFormatString;
  RelativeRestartLevel := AInfo.RelativeRestartLevel;
  FTemplateCode := AInfo.TemplateCode;
  OriginalLeftIndent := AInfo.OriginalLeftIndent;
  Legacy := AInfo.Legacy;
  LegacySpace := AInfo.LegacySpace;
  LegacyIndent := AInfo.LegacyIndent;
end;

function TdxListLevelInfo.Clone: TdxListLevelInfo;
begin
  Result := TdxListLevelInfo.Create;
  Result.CopyFrom(Self);
end;

procedure TdxListLevelInfo.InitializeAsDefault;
begin
  Start := 1;
  Separator := TdxCharacters.TabMark;
  DisplayFormatString := '%s.'; 
  RelativeRestartLevel := 0;
end;

procedure TdxListLevelInfo.SetAlignment(const Value: TdxListNumberAlignment);
begin
  FAlignment := Value;
end;

procedure TdxListLevelInfo.SetConvertPreviousLevelNumberingToDecimal(const Value: Boolean);
begin
  FConvertPreviousLevelNumberingToDecimal := Value;
end;

procedure TdxListLevelInfo.SetDisplayFormatString(const Value: string);
begin
  FDisplayFormatString := Value;
end;

procedure TdxListLevelInfo.SetFormat(const Value: TdxNumberingFormat);
begin
  FFormat := Value;
end;

procedure TdxListLevelInfo.SetLegacy(const Value: Boolean);
begin
  FLegacy := Value;
end;

procedure TdxListLevelInfo.SetLegacyIndent(const Value: Integer);
begin
  FLegacyIndent := Value;
end;

procedure TdxListLevelInfo.SetLegacySpace(const Value: Integer);
begin
  FLegacySpace := Value;
end;

procedure TdxListLevelInfo.SetRelativeRestartLevel(const Value: Integer);
begin
  FRelativeRestartLevel := Value;
end;

procedure TdxListLevelInfo.SetSeparator(const Value: Char);
begin
  FSeparator := Value;
end;

procedure TdxListLevelInfo.SetStart(const Value: Integer);
begin
  FStart := Value;
end;

procedure TdxListLevelInfo.SetSuppressBulletResize(const Value: Boolean);
begin
   FSuppressBulletResize := Value;
end;

procedure TdxListLevelInfo.SetSuppressRestart(const Value: Boolean);
begin
  FSuppressRestart := Value;
end;

procedure TdxListLevelInfo.SetTemplateCode(const Value: Integer);
begin
  FTemplateCode := Value;
end;

function TdxListLevelInfo.Equals(Obj: TObject): Boolean;
var
  AInfo: TdxListLevelInfo;
begin
  AInfo := TdxListLevelInfo(Obj);
  Result := (Start = AInfo.Start) and
    (Format = AInfo.Format) and
    (Alignment = AInfo.Alignment) and
    (ConvertPreviousLevelNumberingToDecimal = AInfo.ConvertPreviousLevelNumberingToDecimal) and
    (Separator = AInfo.Separator) and
    (SuppressRestart = AInfo.SuppressRestart) and
    (SuppressBulletResize = AInfo.SuppressBulletResize) and
    (DisplayFormatString = AInfo.DisplayFormatString) and
    (RelativeRestartLevel = AInfo.RelativeRestartLevel) and
    (HasTemplateCode = AInfo.HasTemplateCode) and
    (OriginalLeftIndent = AInfo.OriginalLeftIndent) and
    (Legacy = AInfo.Legacy) and
    (LegacyIndent = AInfo.LegacyIndent) and
    (LegacySpace = AInfo.LegacySpace) and
    (TemplateCode = AInfo.TemplateCode);
end;

function TdxListLevelInfo.GetAlignment: TdxListNumberAlignment;
begin
  Result := FAlignment;
end;

function TdxListLevelInfo.GetConvertPreviousLevelNumberingToDecimal: Boolean;
begin
  Result := FConvertPreviousLevelNumberingToDecimal;
end;

function TdxListLevelInfo.GetDisplayFormatString: string;
begin
  Result := FDisplayFormatString;
end;

function TdxListLevelInfo.GetFormat: TdxNumberingFormat;
begin
  Result := FFormat;
end;

function TdxListLevelInfo.GetHasTemplateCode: Boolean;
begin
  Result := TemplateCode <> 0;
end;

function TdxListLevelInfo.GetLegacy: Boolean;
begin
  Result := FLegacy;
end;

function TdxListLevelInfo.GetLegacyIndent: Integer;
begin
  Result := FLegacyIndent;
end;

function TdxListLevelInfo.GetLegacySpace: Integer;
begin
  Result := FLegacySpace;
end;

function TdxListLevelInfo.GetOriginalLeftIndent: Integer;
begin
  Result := FOriginalLeftIndent;
end;

function TdxListLevelInfo.GetRelativeRestartLevel: Integer;
begin
  Result := FRelativeRestartLevel;
end;

function TdxListLevelInfo.GetSeparator: Char;
begin
  Result := FSeparator;
end;

function TdxListLevelInfo.GetStart: Integer;
begin
  Result := FStart;
end;

function TdxListLevelInfo.GetSuppressBulletResize: Boolean;
begin
  Result := FSuppressBulletResize;
end;

function TdxListLevelInfo.GetSuppressRestart: Boolean;
begin
  Result := FSuppressRestart;
end;

function TdxListLevelInfo.GetTemplateCode: Integer;
begin
  Result := FTemplateCode;
end;

{ TdxListLevelChangeActionsCalculator }

class function TdxListLevelChangeActionsCalculator.CalculateChangeActions(
  AChange: TdxListLevelChangeType): TdxDocumentModelChangeActions;
const
  AListLevelChangeActionsTable: array[TdxListLevelChangeType] of TdxDocumentModelChangeActions = (
    [], 
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout], 
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout], 
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout], 
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout], 
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout], 
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout], 
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout], 
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout], 
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout], 
    [], 
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout], 
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout], 
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout], 
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout]  
  );
begin
  Result := AListLevelChangeActionsTable[AChange];
end;

{ TdxListLevelProperties }

constructor TdxListLevelProperties.Create(ADocumentModel: TdxCustomDocumentModel);

  function GetMainPieceTable(ADocumentModel: TdxDocumentModel): TdxPieceTable;
  begin
    Assert(ADocumentModel <> nil);
    Result := ADocumentModel.MainPieceTable;
  end;

begin
  inherited Create(GetMainPieceTable(TdxDocumentModel(ADocumentModel)));
end;

function TdxListLevelProperties.Clone: TdxListLevelProperties;
begin
  Result := TdxListLevelProperties.Create(DocumentModel);
  Result.CopyFrom(Self);
end;

function TdxListLevelProperties.GetAlignment: TdxListNumberAlignment;
begin
  Result := Info.Alignment; 
end;

function TdxListLevelProperties.GetBatchUpdateChangeActions: TdxDocumentModelChangeActions;
begin
  Result := TdxListLevelChangeActionsCalculator.CalculateChangeActions(TdxListLevelChangeType.BatchUpdate);
end;

function TdxListLevelProperties.GetCache(
  const ADocumentModel: TdxCustomDocumentModel): TdxUniqueItemsCache<TdxListLevelInfo>;
begin
  Result := TdxDocumentModel(ADocumentModel).Cache.ListLevelInfoCache;
end;

function TdxListLevelProperties.GetConvertPreviousLevelNumberingToDecimal: Boolean;
begin
  Result := Info.ConvertPreviousLevelNumberingToDecimal; 
end;

function TdxListLevelProperties.GetDisplayFormatString: string;
begin
  Result := Info.DisplayFormatString; 
end;

function TdxListLevelProperties.GetFormat: TdxNumberingFormat;
begin
  Result := Info.Format; 
end;

function TdxListLevelProperties.GetLegacy: Boolean;
begin
  Result := Info.Legacy; 
end;

function TdxListLevelProperties.GetLegacyIndent: Integer;
begin
  Result := Info.LegacyIndent; 
end;

function TdxListLevelProperties.GetLegacySpace: Integer;
begin
  Result := Info.LegacySpace; 
end;

function TdxListLevelProperties.GetOriginalLeftIndent: Integer;
begin
  Result := Info.OriginalLeftIndent;
end;

function TdxListLevelProperties.GetRelativeRestartLevel: Integer;
begin
  Result := Info.RelativeRestartLevel; 
end;

function TdxListLevelProperties.GetSeparator: Char;
begin
  Result := Info.Separator; 
end;

function TdxListLevelProperties.GetStart: Integer;
begin
  Result := Info.Start; 
end;

function TdxListLevelProperties.GetSuppressBulletResize: Boolean;
begin
  Result := Info.SuppressBulletResize; 
end;

function TdxListLevelProperties.GetSuppressRestart: Boolean;
begin
  Result := Info.SuppressRestart; 
end;

function TdxListLevelProperties.GetTemplateCode: Integer;
begin
  Result := Info.TemplateCode; 
end;

procedure TdxListLevelProperties.InternalSetOriginalLeftIndent(const Value: Integer);
begin
  if OriginalLeftIndent = Value then
    Exit;
  SetPropertyValue<Integer>(SetOriginalLeftIndent, Value);
end;

procedure TdxListLevelProperties.SetTemplateCodeIntf(const Value: Integer);
begin
  if TemplateCode = Value then
    Exit;
  SetPropertyValue<Integer>(SetTemplateCode, Value);
end;

procedure TdxListLevelProperties.SetAlignment(const Value: TdxListNumberAlignment);
begin
  if Alignment = Value then
    Exit;
  SetPropertyValue<TdxListNumberAlignment>(SetAlignmentCore, Value);
end;

function TdxListLevelProperties.SetAlignmentCore(const AListLevel: TdxListLevelInfo;
  const AValue: TdxListNumberAlignment): TdxDocumentModelChangeActions;
begin
  AListLevel.Alignment := AValue;
  Result := TdxListLevelChangeActionsCalculator.CalculateChangeActions(TdxListLevelChangeType.Alignment);
end;

procedure TdxListLevelProperties.SetConvertPreviousLevelNumberingToDecimal(const Value: Boolean);
begin
  if ConvertPreviousLevelNumberingToDecimal = Value then
    Exit;
  SetPropertyValue<Boolean>(SetConvertPreviousLevelNumberingToDecimalCore, Value);
end;

function TdxListLevelProperties.SetConvertPreviousLevelNumberingToDecimalCore(const AListLevel: TdxListLevelInfo;
  const AValue: Boolean): TdxDocumentModelChangeActions;
begin
  AListLevel.ConvertPreviousLevelNumberingToDecimal := AValue;
  Result := TdxListLevelChangeActionsCalculator.CalculateChangeActions(TdxListLevelChangeType.RelativeRestartLevel);
end;

procedure TdxListLevelProperties.SetDisplayFormatString(const Value: string);
begin
  if DisplayFormatString = Value then
    Exit;
  SetPropertyValue<string>(SetDisplayFormatStringCore, Value);
end;

function TdxListLevelProperties.SetDisplayFormatStringCore(const AListLevel: TdxListLevelInfo;
  const AValue: string): TdxDocumentModelChangeActions;
begin
  AListLevel.DisplayFormatString := AValue;
  Result := TdxListLevelChangeActionsCalculator.CalculateChangeActions(TdxListLevelChangeType.DisplayFormatString);
end;

procedure TdxListLevelProperties.SetFormat(const Value: TdxNumberingFormat);
begin
  if Format = Value then
    Exit;
  SetPropertyValue<TdxNumberingFormat>(SetFormatCore, Value);
end;

function TdxListLevelProperties.SetFormatCore(const AListLevel: TdxListLevelInfo;
  const AValue: TdxNumberingFormat): TdxDocumentModelChangeActions;
begin
  AListLevel.Format := AValue;
  Result := TdxListLevelChangeActionsCalculator.CalculateChangeActions(TdxListLevelChangeType.Format);
end;

procedure TdxListLevelProperties.SetLegacy(const Value: Boolean);
begin
  if Legacy = Value then
    Exit;
  SetPropertyValue<Boolean>(SetLegacyCore, Value);
end;

function TdxListLevelProperties.SetLegacyCore(const AListLevel: TdxListLevelInfo;
  const AValue: Boolean): TdxDocumentModelChangeActions;
begin
  AListLevel.Legacy := AValue;
  Result := TdxListLevelChangeActionsCalculator.CalculateChangeActions(TdxListLevelChangeType.Legacy);
end;

procedure TdxListLevelProperties.SetLegacyIndent(const Value: Integer);
begin
  if LegacyIndent = Value then
    Exit;
  SetPropertyValue<Integer>(SetLegacyIndentCore, Value);
end;

function TdxListLevelProperties.SetLegacyIndentCore(const AListLevel: TdxListLevelInfo;
  const AValue: Integer): TdxDocumentModelChangeActions;
begin
  AListLevel.LegacyIndent := AValue;
  Result := TdxListLevelChangeActionsCalculator.CalculateChangeActions(TdxListLevelChangeType.LegacyIndent);
end;

procedure TdxListLevelProperties.SetLegacySpace(const Value: Integer);
begin
  if LegacySpace = Value then
    Exit;
  SetPropertyValue<Integer>(SetLegacySpaceCore, Value);
end;

function TdxListLevelProperties.SetLegacySpaceCore(const AListLevel: TdxListLevelInfo;
  const AValue: Integer): TdxDocumentModelChangeActions;
begin
  AListLevel.LegacySpace := AValue;
  Result := TdxListLevelChangeActionsCalculator.CalculateChangeActions(TdxListLevelChangeType.LegacySpace);
end;

function TdxListLevelProperties.SetOriginalLeftIndent(const AListLevel: TdxListLevelInfo;
  const AValue: Integer): TdxDocumentModelChangeActions;
begin
  AListLevel.OriginalLeftIndent := AValue;
  Result := [];
end;

procedure TdxListLevelProperties.SetRelativeRestartLevel(const Value: Integer);
begin
  if Value < 0 then
    raise Exception.Create(''); 
  if RelativeRestartLevel = Value then
    Exit;
  SetPropertyValue<Integer>(SetRestartLevelCore, Value);
end;

function TdxListLevelProperties.SetRestartLevelCore(const AListLevel: TdxListLevelInfo;
  const AValue: Integer): TdxDocumentModelChangeActions;
begin
  AListLevel.RelativeRestartLevel := AValue;
  Result := TdxListLevelChangeActionsCalculator.CalculateChangeActions(TdxListLevelChangeType.RelativeRestartLevel);
end;

procedure TdxListLevelProperties.SetSeparator(const Value: Char);
begin
  if Separator = Value then
    Exit;
  SetPropertyValue<Char>(SetSeparatorCore, Value);
end;

function TdxListLevelProperties.SetSeparatorCore(const AListLevel: TdxListLevelInfo;
  const AValue: Char): TdxDocumentModelChangeActions;
begin
  AListLevel.Separator := AValue;
  Result := TdxListLevelChangeActionsCalculator.CalculateChangeActions(TdxListLevelChangeType.Separator);
end;

procedure TdxListLevelProperties.SetStart(const Value: Integer);
begin
  if Value < 0 then
    raise Exception.Create(''); 
  if Start = Value then
    Exit;
  SetPropertyValue<Integer>(SetStartCore, Value);
end;

function TdxListLevelProperties.SetStartCore(const AListLevel: TdxListLevelInfo;
  const AValue: Integer): TdxDocumentModelChangeActions;
begin
  AListLevel.Start := AValue;
  Result := TdxListLevelChangeActionsCalculator.CalculateChangeActions(TdxListLevelChangeType.Start);
end;

procedure TdxListLevelProperties.SetSuppressBulletResize(const Value: Boolean);
begin
  if SuppressBulletResize = Value then
    Exit;
  SetPropertyValue<Boolean>(SetSuppressBulletResizeCore, value);
end;

function TdxListLevelProperties.SetSuppressBulletResizeCore(const AListLevel: TdxListLevelInfo;
  const AValue: Boolean): TdxDocumentModelChangeActions;
begin
  AListLevel.SuppressBulletResize := AValue;
  Result := TdxListLevelChangeActionsCalculator.CalculateChangeActions(TdxListLevelChangeType.SuppressBulletResize);
end;

procedure TdxListLevelProperties.SetSuppressRestart(const Value: Boolean);
begin
  if SuppressRestart = Value then
    Exit;
  SetPropertyValue<Boolean>(SetSuppressRestartCore, Value);
end;

function TdxListLevelProperties.SetSuppressRestartCore(const AListLevel: TdxListLevelInfo;
  const AValue: Boolean): TdxDocumentModelChangeActions;
begin
  AListLevel.SuppressRestart := AValue;
  Result := TdxListLevelChangeActionsCalculator.CalculateChangeActions(TdxListLevelChangeType.SuppressRestart);
end;

function TdxListLevelProperties.SetTemplateCode(const AListLevel: TdxListLevelInfo;
  const AValue: Integer): TdxDocumentModelChangeActions;
begin
  AListLevel.TemplateCode := AValue;
  Result := TdxListLevelChangeActionsCalculator.CalculateChangeActions(TdxListLevelChangeType.TemplateCode);
end;

{ TdxListLevelCollection }

constructor TdxListLevelCollection.Create;
begin
  inherited Create;
  FList := TdxFastObjectList.Create;
end;

destructor TdxListLevelCollection.Destroy;
begin
  FreeAndNil(FList);
  inherited Destroy;
end;

procedure TdxListLevelCollection.Add(ALevel: TdxAbstractListLevel);
begin
  List.Add(ALevel);
end;

function TdxListLevelCollection.GetCount: Integer;
begin
  Result := List.Count;
end;

function TdxListLevelCollection.GetItem(AIndex: Integer): TdxAbstractListLevel;
begin
  Result := TdxAbstractListLevel(List[AIndex]);
end;

procedure TdxListLevelCollection.SetItem(AIndex: Integer; AObject: TdxAbstractListLevel);
var
  AOldItem: TObject;
begin
  AOldItem := List[AIndex];
  List[AIndex] := AObject;
  FreeAndNil(AOldItem); 
end;

{ TdxNumberingListBaseImpl }

procedure TdxNumberingListBaseImpl.CopyFrom(AList: TdxNumberingListBaseImpl);
begin
  FId := AList.Id;
  CopyLevelsFrom(AList.FLevels);
end;

constructor TdxNumberingListBaseImpl.Create(ADocumentModel: TdxCustomDocumentModel; ALevelCount: Integer);
begin
  inherited Create;
  Assert(ADocumentModel <> nil);
  FDocumentModel := ADocumentModel;
  FLevels := TdxListLevelCollection.Create; 
  FId := -1;
  InitLevels(ALevelCount);
end;

destructor TdxNumberingListBaseImpl.Destroy;
begin
  FreeAndNil(FLevels);
  inherited Destroy;
end;

function TdxNumberingListBaseImpl.EqualsLevels(AListLevel: TdxListLevelCollection): Boolean;
var
  I: Integer;
  ALevel: TdxListLevel;
begin
  for I := 0 to AListLevel.Count - 1 do
  begin
    if Levels[I] is TdxListLevel then
      ALevel := TdxListLevel(Levels[I])
    else
      ALevel := nil;
    if (ALevel <> nil) and not ALevel.Equals(AListLevel[I]) then
      Exit(False);
  end;
  Result := True;
end;

function TdxNumberingListBaseImpl.GetId: Integer;
begin
  Result := FId;
end;

function TdxNumberingListBaseImpl.Id: Integer;
begin
  if FId = -1 then
    FId := GenerateNewId;
  Result := FId;
end;

procedure TdxNumberingListBaseImpl.InitLevels(ALevelCount: Integer);
var
  I: Integer;
begin
  for I := 0 to ALevelCount - 1 do
    FLevels.Add(CreateLevel(I));
end;

function TdxNumberingListBaseImpl.IsEqual(AList: TdxNumberingListBaseImpl): Boolean;
begin
  Result := EqualsLevels(AList.FLevels);
end;

procedure TdxNumberingListBaseImpl.SetId(AId: Integer);
begin
  FId := AId;
end;

procedure TdxNumberingListBaseImpl.SetLevel(ALevelIndex: Integer; AValue: TdxListLevel);
begin
  Levels.SetItem(ALevelIndex, AValue);
end;

function TdxNumberingListBaseImpl.GetDocumentModel: TdxCustomDocumentModel;
begin
  Result := FDocumentModel;
end;

{ TdxListLevel }

function TdxListLevel.Clone: TdxListLevel;
begin
  Result := CreateLevel;
  Result.CopyFrom(Self);
end;

procedure TdxListLevel.CopyFrom(AListLevel: TdxListLevel);
begin
  ListLevelProperties.CopyFrom(AListLevel.ListLevelProperties.Info);
  ParagraphProperties.CopyFrom(AListLevel.ParagraphProperties.Info);
  CharacterProperties.CopyFrom(AListLevel.CharacterProperties.Info);
  ParagraphStyleIndex := AListLevel.ParagraphStyleIndex;
end;

constructor TdxListLevel.Create(ADocumentModel: TdxCustomDocumentModel);
begin
  inherited Create;
  FParagraphStyleIndex := -1;
  FMergedCharacterFormattingCacheIndex := -1;
  FMergedParagraphFormattingCacheIndex := -1;
  FFontCacheIndex := -1;

  FDocumentModel := ADocumentModel;
  FParagraphProperties := TdxParagraphProperties.Create(Self);
  FCharacterProperties := TdxCharacterProperties.Create(Self);
  FListLevelProperties := TdxListLevelProperties.Create(ADocumentModel);

  FParagraphProperties.OnObtainAffectedRange.Add(OnPropertiesObtainAffectedRange); 
  FCharacterProperties.OnObtainAffectedRange.Add(OnPropertiesObtainAffectedRange); 
  FListLevelProperties.OnObtainAffectedRange.Add(OnPropertiesObtainAffectedRange); 
end;

destructor TdxListLevel.Destroy;
begin
  FreeAndNil(FListLevelProperties);
  FreeAndNil(FCharacterProperties);
  FreeAndNil(FParagraphProperties);
  inherited Destroy;
end;

function TdxListLevel.Equals(AObj: TObject): Boolean;
var
  AOther: TdxListLevel;
  AThisParagraphStyle, AOtherParagraphStyle: TdxParagraphStyle;
begin
  if AObj = Self then
    Exit(True);
  if not (AObj is TdxListLevel) then
    Exit(False);
  AOther := TdxListLevel(AObj);
  if not ListLevelProperties.Info.Equals(AOther.ListLevelProperties.Info) or
    not CharacterProperties.Info.Equals(AOther.CharacterProperties.Info) or
    not ParagraphProperties.Equals(AOther.ParagraphProperties) then
    Exit(False);
  if DocumentModel = AOther.DocumentModel then
    Result := ParagraphStyleIndex = AOther.ParagraphStyleIndex
  else
  begin
    AThisParagraphStyle := ParagraphStyle;
    AOtherParagraphStyle := AOther.ParagraphStyle;
    if (AThisParagraphStyle = nil) and (AOtherParagraphStyle = nil) then
      Exit(True);
    if (AThisParagraphStyle = nil) or (AOtherParagraphStyle = nil) then
      Exit(False);
    Result := AThisParagraphStyle.StyleName = AOtherParagraphStyle.StyleName;
  end;
end;

function TdxListLevel.CreateLevel: TdxListLevel;
begin
  Result := TdxListLevel.Create(DocumentModel);
end;

function TdxListLevel.GetAfterAutoSpacing: Boolean;
begin
  Result := MergedParagraphFormatting.AfterAutoSpacing;
end;

function TdxListLevel.GetAlignment: TdxParagraphAlignment;
begin
  Result := MergedParagraphFormatting.Alignment;
end;

function TdxListLevel.GetAllCaps: Boolean;
begin
  Result := MergedCharacterFormatting.AllCaps;
end;

function TdxListLevel.GetBackColor: TColor;
begin
  Result := MergedCharacterFormatting.BackColor;
end;

function TdxListLevel.GetBeforeAutoSpacing: Boolean;
begin
  Result := MergedParagraphFormatting.BeforeAutoSpacing;
end;

function TdxListLevel.GetBulletLevel: Boolean;
begin
  Result := Length(ListLevelProperties.DisplayFormatString) = BulletLevelDisplayFormatStringLength;
end;

function TdxListLevel.GetPieceTable: TObject;
begin
  Result := GetMainPieceTable;
end;

function TdxListLevel.GetRightIndent: Integer;
begin
  Result := MergedParagraphFormatting.RightIndent;
end;

function TdxListLevel.CreateCharacterPropertiesChangedHistoryItem:
  TdxIndexChangedHistoryItemCore<TdxDocumentModelChangeActions>;
begin
  Result := TdxIndexChangedHistoryItem<TdxDocumentModelChangeActions>.Create(DocumentModel.MainPieceTable,
    CharacterProperties);
end;

function TdxListLevel.CreateParagraphPropertiesChangedHistoryItem:
  TdxIndexChangedHistoryItemCore<TdxDocumentModelChangeActions>;
begin
  Result := TdxIndexChangedHistoryItem<TdxDocumentModelChangeActions>.Create(DocumentModel.MainPieceTable,
    ParagraphProperties);
end;

procedure TdxListLevel.OnCharacterPropertiesChanged;
begin
  FMergedCharacterFormattingCacheIndex := -1;
  FFontCacheIndex := -1;
  DocumentModel.ResetDocumentFormattingCaches(TdxResetFormattingCacheType.Character);
end;

procedure TdxListLevel.OnParagraphPropertiesChanged;
begin
  FMergedParagraphFormattingCacheIndex := -1;
  DocumentModel.ResetDocumentFormattingCaches(TdxResetFormattingCacheType.Paragraph);
end;

procedure TdxListLevel.OnParagraphStyleChanged;
begin
  FMergedParagraphFormattingCacheIndex := -1;
  DocumentModel.ResetDocumentFormattingCaches(TdxResetFormattingCacheType.All);
end;

function TdxListLevel.GetMergedParagraphProperties: TdxMergedParagraphProperties;
var
  AMerger: TdxParagraphPropertiesMerger;
begin
  AMerger := TdxParagraphPropertiesMerger.Create(ParagraphProperties);
  try
    AMerger.Merge(DocumentModel.DefaultParagraphProperties);
    Result := AMerger.MergedProperties;
  finally
    AMerger.Free;
  end;
end;

function TdxListLevel.GetOverrideStart: Boolean;
begin
  dxAbstractError;
  Result := False;
end;

function TdxListLevel.GetNewStart: Integer;
begin
  dxAbstractError;
  Result := -1;
end;

function TdxListLevel.GetNoProof: Boolean;
begin
  Result := MergedCharacterFormatting.NoProof;
end;

procedure TdxListLevel.SetNewStart(const Value: Integer);
begin
  dxAbstractError;
end;

function TdxListLevel.GetDocumentModel: TdxCustomDocumentModel;
begin
  Result := FDocumentModel;
end;

procedure TdxListLevel.SetNoProof(const Value: Boolean);
begin
  CharacterProperties.NoProof := Value;
end;

function TdxListLevel.GetOutlineLevel: Integer;
begin
  Result := MergedParagraphFormatting.SpacingBefore;
end;

function TdxListLevel.GetCharacterProperties: TdxCharacterProperties;
begin
  Result := FCharacterProperties;
end;

function TdxListLevel.GetContextualSpacing: Boolean;
begin
  Result := MergedParagraphFormatting.ContextualSpacing;
end;

function TdxListLevel.GetDoubleFontSize: Integer;
begin
  Result := MergedCharacterFormatting.DoubleFontSize;
end;

function TdxListLevel.GetFirstLineIndent: Integer;
begin
  Result := MergedParagraphFormatting.FirstLineIndent;
end;

function TdxListLevel.GetFirstLineIndentType: TdxParagraphFirstLineIndent;
begin
  Result := MergedParagraphFormatting.FirstLineIndentType;
end;

function TdxListLevel.GetFontBold: Boolean;
begin
  Result := MergedCharacterFormatting.FontBold;
end;

function TdxListLevel.GetFontCacheIndex: Integer;
var
  AFontStyle: TFontStyles;
begin
  if FFontCacheIndex < 0 then
  begin
    AFontStyle := [];
    if FontBold then
      Include(AFontStyle, fsBold);
    if FontItalic then
      Include(AFontStyle, fsItalic);
    FFontCacheIndex := DocumentModel.FontCache.CalcFontIndex(FontName, DoubleFontSize, AFontStyle, Script);
  end;
  Result := FFontCacheIndex;
end;

function TdxListLevel.GetFontItalic: Boolean;
begin
  Result := MergedCharacterFormatting.FontItalic;
end;

function TdxListLevel.GetFontName: string;
begin
  Result := MergedCharacterFormatting.FontName;
end;

function TdxListLevel.GetFontStrikeoutType: TdxStrikeoutType;
begin
  Result := MergedCharacterFormatting.FontStrikeoutType;
end;

function TdxListLevel.GetFontUnderlineType: TdxUnderlineType;
begin
  Result := MergedCharacterFormatting.FontUnderlineType;
end;

function TdxListLevel.GetForeColor: TColor;
begin
  Result := MergedCharacterFormatting.ForeColor;
end;

function TdxListLevel.GetHidden: Boolean;
begin
  Result := MergedCharacterFormatting.Hidden;
end;

function TdxListLevel.GetKeepLinesTogether: Boolean;
begin
  Result := MergedParagraphFormatting.KeepLinesTogether;
end;

function TdxListLevel.GetKeepWithNext: Boolean;
begin
  Result := MergedParagraphFormatting.KeepWithNext;
end;

function TdxListLevel.GetPageBreakBefore: Boolean;
begin
  Result := MergedParagraphFormatting.PageBreakBefore;
end;

function TdxListLevel.GetParagraphProperties: TdxParagraphProperties;
begin
  Result := FParagraphProperties;
end;

function TdxListLevel.GetScript: TdxCharacterFormattingScript;
begin
  Result := MergedCharacterFormatting.Script;
end;

function TdxListLevel.GetSpacingAfter: Integer;
begin
  Result := MergedParagraphFormatting.SpacingBefore;
end;

function TdxListLevel.GetSpacingBefore: Integer;
begin
  Result := MergedParagraphFormatting.SpacingBefore;
end;

function TdxListLevel.GetStrikeoutColor: TColor;
begin
  Result := MergedCharacterFormatting.StrikeoutColor;
end;

function TdxListLevel.GetStrikeoutWordsOnly: Boolean;
begin
  Result := MergedCharacterFormatting.StrikeoutWordsOnly;
end;

function TdxListLevel.GetSuppressHyphenation: Boolean;
begin
  Result := MergedParagraphFormatting.SuppressHyphenation;
end;

function TdxListLevel.GetSuppressLineNumbers: Boolean;
begin
  Result := MergedParagraphFormatting.SuppressLineNumbers;
end;

function TdxListLevel.GetUnderlineColor: TColor;
begin
  Result := MergedCharacterFormatting.UnderlineColor;
end;

function TdxListLevel.GetUnderlineWordsOnly: Boolean;
begin
  Result := MergedCharacterFormatting.UnderlineWordsOnly;
end;

function TdxListLevel.GetWidowOrphanControl: Boolean;
begin
  Result := MergedParagraphFormatting.WidowOrphanControl;
end;

function TdxListLevel.GetParagraphStyle: TdxParagraphStyle;
begin
  if FParagraphStyleIndex >= 0 then
    Result := DocumentModel.ParagraphStyles[ParagraphStyleIndex]
  else
    Result := nil;
end;

function TdxListLevel.GetMainPieceTable: TObject;
begin
  Result := DocumentModel.MainPieceTable;
end;

procedure TdxListLevel.SetAfterAutoSpacing(const Value: Boolean);
begin
  ParagraphProperties.AfterAutoSpacing := Value;
end;

procedure TdxListLevel.SetAlignment(const Value: TdxParagraphAlignment);
begin
  ParagraphProperties.Alignment := Value;
end;

procedure TdxListLevel.SetAllCaps(const Value: Boolean);
begin
  CharacterProperties.AllCaps := Value;
end;

procedure TdxListLevel.SetBackColor(const Value: TColor);
begin
  CharacterProperties.BackColor := Value;
end;

procedure TdxListLevel.SetBeforeAutoSpacing(const Value: Boolean);
begin
  ParagraphProperties.BeforeAutoSpacing := Value;
end;

procedure TdxListLevel.SetContextualSpacing(const Value: Boolean);
begin
  ParagraphProperties.ContextualSpacing := Value;
end;

procedure TdxListLevel.SetDoubleFontSize(const Value: Integer);
begin
  CharacterProperties.DoubleFontSize := Value;
end;

procedure TdxListLevel.SetFirstLineIndent(const Value: Integer);
begin
  ParagraphProperties.FirstLineIndent := Value;
end;

procedure TdxListLevel.SetFirstLineIndentType(const Value: TdxParagraphFirstLineIndent);
begin
  ParagraphProperties.FirstLineIndentType := Value;
end;

procedure TdxListLevel.SetFontBold(const Value: Boolean);
begin
  CharacterProperties.FontBold := Value;
end;

procedure TdxListLevel.SetFontItalic(const Value: Boolean);
begin
  CharacterProperties.FontItalic := Value;
end;

procedure TdxListLevel.SetFontName(const Value: string);
begin
  CharacterProperties.FontName := Value;
end;

procedure TdxListLevel.SetFontStrikeoutType(const Value: TdxStrikeoutType);
begin
  CharacterProperties.FontStrikeoutType := Value;
end;

procedure TdxListLevel.SetFontUnderlineType(const Value: TdxUnderlineType);
begin
  CharacterProperties.FontUnderlineType := Value;
end;

procedure TdxListLevel.SetForeColor(const Value: TColor);
begin
  CharacterProperties.ForeColor := Value;
end;

procedure TdxListLevel.SetHidden(const Value: Boolean);
begin
  CharacterProperties.Hidden := Value;
end;

procedure TdxListLevel.SetKeepLinesTogether(const Value: Boolean);
begin
  ParagraphProperties.KeepLinesTogether := Value;
end;

procedure TdxListLevel.SetKeepWithNext(const Value: Boolean);
begin
  ParagraphProperties.KeepWithNext := Value;
end;

procedure TdxListLevel.SetLeftIndent(const Value: Integer);
begin
  ParagraphProperties.LeftIndent := Value;
end;

procedure TdxListLevel.SetLineSpacing(const Value: Single);
begin
  ParagraphProperties.LineSpacing := Value;
end;

procedure TdxListLevel.SetLineSpacingType(const Value: TdxParagraphLineSpacing);
begin
  ParagraphProperties.LineSpacingType := Value;
end;

procedure TdxListLevel.SetOutlineLevel(const Value: Integer);
begin
  ParagraphProperties.OutlineLevel := Value;
end;

procedure TdxListLevel.SetPageBreakBefore(const Value: Boolean);
begin
  ParagraphProperties.PageBreakBefore := Value;
end;

procedure TdxListLevel.SetParagraphStyleIndex(const Value: Integer);
begin
  FParagraphStyleIndex := Value;
  OnParagraphStyleChanged;
end;

procedure TdxListLevel.SetRightIndent(const Value: Integer);
begin
  ParagraphProperties.RightIndent := Value;
end;

procedure TdxListLevel.SetScript(const Value: TdxCharacterFormattingScript);
begin
  CharacterProperties.Script := value;
end;

procedure TdxListLevel.SetSpacingAfter(const Value: Integer);
begin
  ParagraphProperties.SpacingAfter := Value;
end;

procedure TdxListLevel.SetSpacingBefore(const Value: Integer);
begin
  ParagraphProperties.SpacingBefore := Value;
end;

procedure TdxListLevel.SetStrikeoutColor(const Value: TColor);
begin
  CharacterProperties.StrikeoutColor := Value;
end;

procedure TdxListLevel.SetStrikeoutWordsOnly(const Value: Boolean);
begin
  CharacterProperties.StrikeoutWordsOnly := Value;
end;

procedure TdxListLevel.SetSuppressHyphenation(const Value: Boolean);
begin
  ParagraphProperties.SuppressHyphenation := Value;
end;

procedure TdxListLevel.SetSuppressLineNumbers(const Value: Boolean);
begin
  ParagraphProperties.SuppressLineNumbers := Value;
end;

procedure TdxListLevel.SetUnderlineColor(const Value: TColor);
begin
  CharacterProperties.UnderlineColor := Value;
end;

procedure TdxListLevel.SetUnderlineWordsOnly(const Value: Boolean);
begin
  CharacterProperties.UnderlineWordsOnly := Value;
end;

procedure TdxListLevel.SetWidowOrphanControl(const Value: Boolean);
begin
  ParagraphProperties.WidowOrphanControl := Value;
end;

function TdxListLevel.GetLeftIndent: Integer;
begin
  Result := MergedParagraphFormatting.LeftIndent;
end;

function TdxListLevel.GetLineSpacing: Single;
begin
  Result := MergedParagraphFormatting.LineSpacing;
end;

function TdxListLevel.GetLineSpacingType: TdxParagraphLineSpacing;
begin
  Result := MergedParagraphFormatting.LineSpacingType;
end;

function TdxListLevel.GetListLevelProperties: TdxListLevelProperties;
begin
  Result := FListLevelProperties;
end;

function TdxListLevel.GetMergedCharacterFormatting: TdxCharacterFormattingInfo;
begin
  Result := DocumentModel.Cache.MergedCharacterFormattingInfoCache[MergedCharacterFormattingCacheIndex];
end;

function TdxListLevel.GetMergedCharacterFormattingCacheIndex: Integer;
begin
  if FMergedCharacterFormattingCacheIndex < 0 then
    FMergedCharacterFormattingCacheIndex :=
      DocumentModel.Cache.MergedCharacterFormattingInfoCache.GetItemIndex(GetMergedCharacterProperties.Info);
  Result := FMergedCharacterFormattingCacheIndex;
end;

function TdxListLevel.GetMergedParagraphFormatting: TdxParagraphFormattingInfo;
begin
  Result := DocumentModel.Cache.MergedParagraphFormattingInfoCache[MergedParagraphFormattingCacheIndex];
end;

function TdxListLevel.GetMergedParagraphFormattingCacheIndex: Integer;
var
  AMergedParagraphProperties: TdxMergedParagraphProperties;
begin
  if FMergedParagraphFormattingCacheIndex < 0 then
  begin
    AMergedParagraphProperties := GetMergedParagraphProperties;
    try
      FMergedParagraphFormattingCacheIndex :=
        DocumentModel.Cache.MergedParagraphFormattingInfoCache.GetItemIndex(AMergedParagraphProperties.Info);
    finally
      AMergedParagraphProperties.Free;
    end;
  end;
  Result := FMergedParagraphFormattingCacheIndex;
end;

function TdxListLevel.GetMergedCharacterProperties: TdxMergedCharacterProperties;
var
  AMerger: TdxCharacterPropertiesMerger;
begin
  AMerger := TdxCharacterPropertiesMerger.Create(CharacterProperties);
  try
    AMerger.Merge(DocumentModel.DefaultCharacterProperties);
    Result := AMerger.MergedProperties;
  finally
    FreeAndNil(AMerger);
  end;
end;

procedure TdxListLevel.OnPropertiesObtainAffectedRange(Sender: TObject; E: TdxObtainAffectedRangeEventArgs);
begin
  E.Start := 0;     
  E.&End := MaxInt; 
end;

{ TdxOverrideListLevel }

function TdxOverrideListLevel.CreateLevel: TdxListLevel;
begin
  Result := TdxOverrideListLevel.Create(DocumentModel);
end;

function TdxOverrideListLevel.GetNewStart: Integer;
begin
  Result := ListLevelProperties.Start;
end;

function TdxOverrideListLevel.GetOverrideStart: Boolean;
begin
  Result := FOverrideStart;
end;

procedure TdxOverrideListLevel.SetNewStart(const Value: Integer);
begin
  ListLevelProperties.Start := Value;
end;

procedure TdxOverrideListLevel.SetOverrideStart(AValue: Boolean);
begin
  FOverrideStart := AValue;
end;

{ TdxAbstractNumberingList }

constructor TdxAbstractNumberingList.Create(ADocumentModel: TdxCustomDocumentModel);
begin
  inherited Create(ADocumentModel, 9);
end;

function TdxAbstractNumberingList.GenerateNewId: Integer;
begin
  Result := TdxDocumentModel(DocumentModel).AbstractNumberingListIdProvider.GetNextId;
end;

function TdxAbstractNumberingList.CreateLevel(ALevelIndex: Integer): TdxAbstractListLevel;
begin
  Result := TdxListLevel.Create(DocumentModel);
end;

procedure TdxAbstractNumberingList.CopyLevelsFrom(ASourceLevels: TdxListLevelCollection);
var
  I: Integer;
  ANewLevel: TdxListLevel;
  ASourceLevel: TdxListLevel;
begin
  for I := 0 to ASourceLevels.Count - 1 do
  begin
    ASourceLevel := ASourceLevels[I] as TdxListLevel;
    ANewLevel := TdxListLevel.Create(DocumentModel);
    ANewLevel.CopyFrom(ASourceLevel);
    Levels[I] := ANewLevel;
  end;
end;

function TdxAbstractNumberingList.Clone: TdxAbstractNumberingList;
begin
  Result := CreateNumberingList;
  Result.CopyFrom(Self);
end;

function TdxAbstractNumberingList.CreateNumberingList: TdxAbstractNumberingList;
begin
  Result := TdxAbstractNumberingList.Create(DocumentModel);
end;

function TdxAbstractNumberingList.GetLevels: IdxReadOnlyIListLevelCollection;
begin
  Result := FLevels;
end;

{ TdxAbstractNumberingListCollection }

function TdxAbstractNumberingListCollection.GetItem(Index: Integer): TdxAbstractNumberingList;
begin
  Result := TdxAbstractNumberingList(inherited Items[Index]);
end;

function TdxAbstractNumberingListCollection.HasListOfType(AListType: TdxNumberingType): Boolean;
var
  I: TdxAbstractNumberingListIndex;
begin
  for I := Count - 1 downto 0 do 
    if TdxNumberingListHelper.GetListType(Self[I]) = AListType then
      Exit(True);
  Result := False;
end;

procedure TdxAbstractNumberingListCollection.SetItem(Index: Integer; const Value: TdxAbstractNumberingList);
begin
  inherited Items[Index] := Value;
end;

{ TdxNumberingListHelper }

class function TdxNumberingListHelper.GenerateNewTemplateCode(ADocumentModel: TdxCustomDocumentModel): Integer;
begin
  repeat
    Result := TemplateCodeStart + Random(TemplateCodeEnd - TemplateCodeStart); 
    if IsNewTemplateCode(ADocumentModel, Result) then
      Break;
  until False;
end;

class function TdxNumberingListHelper.GetLevelType(const ANumberingList: IdxNumberingListBase;
  ALevelIndex: Integer): TdxNumberingType;
var
  ALevel: IdxListLevel;
begin
  ALevel := ANumberingList.Levels[ALevelIndex];
  if ALevel.BulletLevel then
    Result := TdxNumberingType.Bullet
  else
    if not IsHybridList(ANumberingList) then
      Result := TdxNumberingType.MultiLevel
    else
      Result := TdxNumberingType.Simple;
end;

class function TdxNumberingListHelper.GetListType(const ANumberingList: IdxNumberingListBase): TdxNumberingType;
begin
  if not IsHybridList(ANumberingList) then
    Result := TdxNumberingType.MultiLevel
  else
    if (ANumberingList.Levels[0].BulletLevel) then
      Result := TdxNumberingType.Bullet
    else
      Result := TdxNumberingType.Simple;
end;

class function TdxNumberingListHelper.IsHybridList(const ANumberingList: IdxNumberingListBase): Boolean;
var
  ALevels: IdxReadOnlyIListLevelCollection;
  I: Integer;
begin
  Result := False;
  ALevels := ANumberingList.Levels;
  for I := 0 to ALevels.Count - 1 do
    if ALevels[I].ListLevelProperties.TemplateCode <> 0 then
    begin
      Result := True;
      Break;
    end;
end;

class function TdxNumberingListHelper.IsNewTemplateCode(ADocumentModel: TdxCustomDocumentModel;
  ATemplateCode: Integer): Boolean;
var
  I: Integer;
  ALevelInfoCache: TdxListLevelInfoCache;
begin
  Result := True;
  ALevelInfoCache := TdxDocumentModel(ADocumentModel).Cache.ListLevelInfoCache;
  for I := 0 to ALevelInfoCache.Count - 1 do
    if ALevelInfoCache[I].TemplateCode = ATemplateCode then
    begin
      Result := False;
      Break;
    end;
end;

class procedure TdxNumberingListHelper.SetHybridListType(const AList: IdxNumberingListBase);
var
  AAnchorAction: TdxAction<TdxListLevelProperties>;
begin
  AAnchorAction := procedure(AProperties: TdxListLevelProperties)
    begin
      AProperties.TemplateCode := GenerateNewTemplateCode(AList.DocumentModel);
    end;
  SetListLevelsProperty(AList, function(AProperties: TdxListLevelProperties): Boolean
  begin
    Result := AProperties.TemplateCode = 0;
  end,
  AAnchorAction);
end;

class procedure TdxNumberingListHelper.SetListLevelsFormat(const AList: IdxNumberingListBase;
  AFormat: TdxNumberingFormat);
var
  AAnchorAction: TdxAction<TdxListLevelProperties>;
begin
  AAnchorAction := procedure(AProperties: TdxListLevelProperties)
    begin
      AProperties.Format := AFormat;
    end;
  SetListLevelsProperty(AList, function(AProperties: TdxListLevelProperties): Boolean
    begin
      Result := True;
    end,
    AAnchorAction);
end;

class function TdxNumberingListHelper.GetAbstractListIndexByType(AListCollection: TdxAbstractNumberingListCollection;
  AType: TdxNumberingType): TdxAbstractNumberingListIndex;
var
  I: TdxAbstractNumberingListIndex;
begin
  for I := 0 to AListCollection.Count - 1 do
    if TdxNumberingListHelper.GetListType(AListCollection[I]) = AType then
      Exit(I);
  raise Exception.Create(''); 
  Result := -1;
end;

class procedure TdxNumberingListHelper.SetListLevelsProperty(AList: IdxNumberingListBase;
  ACondition: TdxListLevelPropertiesDelegate; AAction: TdxAction<TdxListLevelProperties>);
var
  I: Integer;
  AListLevel: IdxListLevel;
begin
  for I := 0 to AList.Levels.Count - 1 do
  begin
    AListLevel := AList.Levels[I];
    AListLevel.ListLevelProperties.BeginInit;
    try
      if ACondition(AListLevel.ListLevelProperties) then
        AAction(AListLevel.ListLevelProperties);
    finally
      AListLevel.ListLevelProperties.EndInit;
    end;
  end;
end;

class procedure TdxNumberingListHelper.SetListType(const AList: IdxNumberingListBase; AValue: TdxNumberingType);
var
  ACurrentType: TdxNumberingType;
  AWasSimple, AWasBullet, ANewValueIsMultiLevel, ANewValueIsSimple, ANewValueIsBullet: Boolean;
begin
  ACurrentType := GetListType(AList);
  AWasSimple := ACurrentType <> TdxNumberingType.MultiLevel;
  AWasBullet := ACurrentType = TdxNumberingType.Bullet;
  ANewValueIsMultiLevel := AValue = TdxNumberingType.MultiLevel;
  ANewValueIsSimple := AValue = TdxNumberingType.Simple;
  ANewValueIsBullet := AValue = TdxNumberingType.Bullet;
  if AWasSimple and ANewValueIsMultiLevel then
    TdxNumberingListHelper.SetSimpleListType(AList)
  else
    if not ANewValueIsMultiLevel then
      TdxNumberingListHelper.SetHybridListType(AList);
  if ANewValueIsBullet then
    SetListLevelsFormat(AList, TdxNumberingFormat.Bullet);
  if AWasBullet and ANewValueIsSimple then
    SetListLevelsFormat(AList, TdxNumberingFormat.Decimal);
end;

class procedure TdxNumberingListHelper.SetSimpleListType(const AList: IdxNumberingListBase);
var
  AAnchorAction: TdxAction<TdxListLevelProperties>;
begin
  AAnchorAction := procedure(AProperties: TdxListLevelProperties)
    begin
      AProperties.TemplateCode := 0;
    end;
  SetListLevelsProperty(AList, function(AProperties: TdxListLevelProperties): Boolean
    begin
      Result := AProperties.TemplateCode <> 0;
    end,
    AAnchorAction);
end;

{ TdxOrdinalBasedNumberConverter }

class function TdxOrdinalBasedNumberConverter.CreateConverter(AFormat: TdxNumberingFormat;
  ALanguageId: Integer): TdxOrdinalBasedNumberConverter;
begin
  case AFormat of
    TdxNumberingFormat.UpperRoman:
      Result := TdxUpperRomanNumberConverter.Create;
    TdxNumberingFormat.LowerRoman:
      Result := TdxLowerRomanNumberConverter.Create;
    TdxNumberingFormat.UpperLetter:
      Result := TdxUpperLatinLetterNumberConverter.Create;
    TdxNumberingFormat.LowerLetter:
      Result := TdxLowerLatinLetterNumberConverter.Create;
    TdxNumberingFormat.NumberInDash:
      Result := TdxNumberInDashNumberConverter.Create;
    TdxNumberingFormat.DecimalZero:
      Result := TdxDecimalZeroNumberConverter.Create;
    TdxNumberingFormat.Bullet:
      Result := TdxBulletNumberConverter.Create;
    TdxNumberingFormat.Ordinal:
      Result := GetOrdinalNumberConverterByLanguage(ALanguageId);
    TdxNumberingFormat.RussianUpper:
      Result := TdxRussianUpperNumberConverter.Create;
    TdxNumberingFormat.RussianLower:
      Result := TdxRussianLowerNumberConverter.Create;
    TdxNumberingFormat.DecimalEnclosedParenthses:
      Result := TdxDecimalEnclosedParenthesesNumberConverter.Create;
    TdxNumberingFormat.CardinalText:
      Result := GetDescriptiveCardinalNumberConverterByLanguage(ALanguageId);
    TdxNumberingFormat.OrdinalText:
      Result := GetDescriptiveOrdinalNumberConverterByLanguage(ALanguageId);
    TdxNumberingFormat.Decimal:
      Result := TdxDecimalNumberConverter.Create;
  else
    Result := TdxDecimalNumberConverter.Create;
  end;
end;

class function TdxOrdinalBasedNumberConverter.GetDescriptiveCardinalNumberConverterByLanguage(
  ALanguageId: Integer): TdxOrdinalBasedNumberConverter;
begin
  Result := TdxDecimalNumberConverter.Create;
end;

class function TdxOrdinalBasedNumberConverter.GetOrdinalNumberConverterByLanguage(
  ALanguageId: Integer): TdxOrdinalBasedNumberConverter;
begin
  Result := TdxDecimalNumberConverter.Create;
end;

class function TdxOrdinalBasedNumberConverter.GetSupportNumberingFormat: TList<TdxNumberingFormat>;
begin
  Result := TList<TdxNumberingFormat>.Create;
  Result.Add(TdxNumberingFormat.Decimal);
  Result.Add(TdxNumberingFormat.UpperRoman);
  Result.Add(TdxNumberingFormat.LowerRoman);
  Result.Add(TdxNumberingFormat.UpperLetter);
  Result.Add(TdxNumberingFormat.LowerLetter);
  Result.Add(TdxNumberingFormat.Ordinal);
  Result.Add(TdxNumberingFormat.CardinalText);
  Result.Add(TdxNumberingFormat.OrdinalText);
  Result.Add(TdxNumberingFormat.DecimalZero);
  Result.Add(TdxNumberingFormat.RussianUpper);
  Result.Add(TdxNumberingFormat.RussianLower);
  Result.Add(TdxNumberingFormat.DecimalEnclosedParenthses);
  Result.Add(TdxNumberingFormat.NumberInDash);
  Result.Add(TdxNumberingFormat.Chicago);
end;

class function TdxOrdinalBasedNumberConverter.GetDescriptiveOrdinalNumberConverterByLanguage(
  ALanguageId: Integer): TdxOrdinalBasedNumberConverter;
begin
  Result := TdxDecimalNumberConverter.Create;
end;

function TdxOrdinalBasedNumberConverter.GetMaxValue: Integer;
begin
  Result := MaxInt;
end;

function TdxOrdinalBasedNumberConverter.GetMinValue: Integer;
begin
  Result := 0;
end;

{ TdxDecimalNumberConverter }

function TdxDecimalNumberConverter.ConvertNumber(AValue: Integer): string;
begin
  Assert(AValue >= 0);
  Result := IntToStr(AValue);
end;

function TdxDecimalNumberConverter.GetType: TdxNumberingFormat;
begin
  Result := TdxNumberingFormat.Decimal;
end;

{ TdxAlphabetBasedNumberConverter }

function TdxAlphabetBasedNumberConverter.ConvertNumber(AValue: Integer): string;
var
  ACount: Integer;
  ASymbol: Char;
begin
  if AValue = 0 then
    Exit('');
  Dec(AValue);
  ACount := AValue div AlphabetSize + 1;
  ASymbol := Alphabet[AValue mod AlphabetSize];
  Result := DupeString(ASymbol, ACount);
end;

function TdxAlphabetBasedNumberConverter.GetMaxValue: Integer;
begin
  Result := 780;
end;

function TdxAlphabetBasedNumberConverter.GetMinValue: Integer;
begin
  Result := 1;
end;

{ TdxUpperLatinLetterNumberConverter }

class constructor TdxUpperLatinLetterNumberConverter.Initialize;
begin
  FAlphabetSize := 26;
  SetLength(FUpperLetter, FAlphabetSize);
  FUpperLetter[0] := 'A';
  FUpperLetter[1] := 'B';
  FUpperLetter[2] := 'C';
  FUpperLetter[3] := 'D';
  FUpperLetter[4] := 'E';
  FUpperLetter[5] := 'F';
  FUpperLetter[6] := 'G';
  FUpperLetter[7] := 'H';
  FUpperLetter[8] := 'I';
  FUpperLetter[9] := 'J';
  FUpperLetter[10] := 'K';
  FUpperLetter[11] := 'L';
  FUpperLetter[12] := 'M';
  FUpperLetter[13] := 'N';
  FUpperLetter[14] := 'O';
  FUpperLetter[15] := 'P';
  FUpperLetter[16] := 'Q';
  FUpperLetter[17] := 'R';
  FUpperLetter[18] := 'S';
  FUpperLetter[19] := 'T';
  FUpperLetter[20] := 'U';
  FUpperLetter[21] := 'V';
  FUpperLetter[22] := 'W';
  FUpperLetter[23] := 'X';
  FUpperLetter[24] := 'Y';
  FUpperLetter[25] := 'Z';
end;

function TdxUpperLatinLetterNumberConverter.GetAlphabetSize: Integer;
begin
  Result := FAlphabetSize;
end;

function TdxUpperLatinLetterNumberConverter.GetAlphabet: TArray<Char>;
begin
  Result := FUpperLetter;
end;

function TdxUpperLatinLetterNumberConverter.GetType: TdxNumberingFormat;
begin
  Result := TdxNumberingFormat.UpperLetter;
end;

{ TdxLowerLatinLetterNumberConverter }

class constructor TdxLowerLatinLetterNumberConverter.Initialize;
begin
  FAlphabetSize := 26;
  SetLength(FUpperLetter, FAlphabetSize);
  FUpperLetter[0] := 'a';
  FUpperLetter[1] := 'b';
  FUpperLetter[2] := 'c';
  FUpperLetter[3] := 'd';
  FUpperLetter[4] := 'e';
  FUpperLetter[5] := 'f';
  FUpperLetter[6] := 'g';
  FUpperLetter[7] := 'h';
  FUpperLetter[8] := 'i';
  FUpperLetter[9] := 'j';
  FUpperLetter[10] := 'k';
  FUpperLetter[11] := 'l';
  FUpperLetter[12] := 'm';
  FUpperLetter[13] := 'n';
  FUpperLetter[14] := 'o';
  FUpperLetter[15] := 'p';
  FUpperLetter[16] := 'q';
  FUpperLetter[17] := 'r';
  FUpperLetter[18] := 's';
  FUpperLetter[19] := 't';
  FUpperLetter[20] := 'u';
  FUpperLetter[21] := 'v';
  FUpperLetter[22] := 'w';
  FUpperLetter[23] := 'x';
  FUpperLetter[24] := 'y';
  FUpperLetter[25] := 'z';
end;

function TdxLowerLatinLetterNumberConverter.GetAlphabetSize: Integer;
begin
  Result := FAlphabetSize;
end;

function TdxLowerLatinLetterNumberConverter.GetAlphabet: TArray<Char>;
begin
  Result := FUpperLetter;
end;

function TdxLowerLatinLetterNumberConverter.GetType: TdxNumberingFormat;
begin
  Result := TdxNumberingFormat.LowerLetter;
end;

{ TdxRomanNumberConverter }

function TdxRomanNumberConverter.ConvertNumber(AValue: Integer): string;
var
  I: Integer;
begin
  Assert(Length(Romans) = Length(FArabics));
  Result := '';
  for I := Length(Romans) - 1 downto 0 do
    while AValue >= FArabics[I] do
    begin
      Dec(AValue, FArabics[I]);
      Result := Result + Romans[I];
    end;
end;

function TdxRomanNumberConverter.GetMinValue: Integer;
begin
  Result := 1;
end;

class constructor TdxRomanNumberConverter.Initialize;
begin
  SetLength(FArabics, 13);
  FArabics[0] := 1;
  FArabics[1] := 4;
  FArabics[2] := 5;
  FArabics[3] := 9;
  FArabics[4] := 10;
  FArabics[5] := 40;
  FArabics[6] := 50;
  FArabics[7] := 90;
  FArabics[8] := 100;
  FArabics[9] := 400;
  FArabics[10] := 500;
  FArabics[11] := 900;
  FArabics[12] := 1000;
end;

{ TdxUpperRomanNumberConverter }

class constructor TdxUpperRomanNumberConverter.Initialize;
begin
  SetLength(FRomans, 13);
  FRomans[0] := 'I';
  FRomans[1] := 'IV';
  FRomans[2] := 'V';
  FRomans[3] := 'IX';
  FRomans[4] := 'X';
  FRomans[5] := 'XL';
  FRomans[6] := 'L';
  FRomans[7] := 'XC';
  FRomans[8] := 'C';
  FRomans[9] := 'CD';
  FRomans[10] := 'D';
  FRomans[11] := 'CM';
  FRomans[12] := 'M';
end;

function TdxUpperRomanNumberConverter.GetRomans: TArray<string>;
begin
  Result := FRomans;
end;

function TdxUpperRomanNumberConverter.GetType: TdxNumberingFormat;
begin
  Result := TdxNumberingFormat.UpperRoman;
end;

{ TdxLowerRomanNumberConverter }

class constructor TdxLowerRomanNumberConverter.Initialize;
begin
  SetLength(FRomans, 13);
  FRomans[0] := 'i';
  FRomans[1] := 'iv';
  FRomans[2] := 'v';
  FRomans[3] := 'ix';
  FRomans[4] := 'x';
  FRomans[5] := 'xl';
  FRomans[6] := 'l';
  FRomans[7] := 'xc';
  FRomans[8] := 'c';
  FRomans[9] := 'cd';
  FRomans[10] := 'd';
  FRomans[11] := 'cm';
  FRomans[12] := 'm';
end;

function TdxLowerRomanNumberConverter.GetRomans: TArray<string>;
begin
  Result := FRomans;
end;

function TdxLowerRomanNumberConverter.GetType: TdxNumberingFormat;
begin
  Result := TdxNumberingFormat.LowerRoman;
end;

{ TdxNumberInDashNumberConverter }

function TdxNumberInDashNumberConverter.ConvertNumber(AValue: Integer): string;
begin
  Assert(AValue >= 0);
  Result := Format('- %d -', [AValue]);
end;

function TdxNumberInDashNumberConverter.GetType: TdxNumberingFormat;
begin
  Result := TdxNumberingFormat.NumberInDash;
end;

{ TdxDecimalZeroNumberConverter }

function TdxDecimalZeroNumberConverter.ConvertNumber(AValue: Integer): string;
begin
  Assert(AValue >= 0);
  if AValue < 10 then
    Result := Format('0%d', [AValue])
  else
    Result := IntToStr(AValue);
end;

function TdxDecimalZeroNumberConverter.GetType: TdxNumberingFormat;
begin
  Result := TdxNumberingFormat.DecimalZero;
end;

{ TdxBulletNumberConverter }

function TdxBulletNumberConverter.ConvertNumber(AValue: Integer): string;
begin
  Result := TdxCharacters.Bullet; 
end;

function TdxBulletNumberConverter.GetType: TdxNumberingFormat;
begin
  Result := TdxNumberingFormat.Bullet;
end;

{ TdxRussianUpperNumberConverter }

class constructor TdxRussianUpperNumberConverter.Initialize;
begin
  FAlphabetSize := 29;
  SetLength(FRussianUpper, FAlphabetSize);
  FRussianUpper[0] := '�';
  FRussianUpper[1] := '�';
  FRussianUpper[2] := '�';
  FRussianUpper[3] := '�';
  FRussianUpper[4] := '�';
  FRussianUpper[5] := '�';
  FRussianUpper[6] := '�';
  FRussianUpper[7] := '�';
  FRussianUpper[8] := '�';
  FRussianUpper[9] := '�';
  FRussianUpper[10] := '�';
  FRussianUpper[11] := '�';
  FRussianUpper[12] := '�';
  FRussianUpper[13] := '�';
  FRussianUpper[14] := '�';
  FRussianUpper[15] := '�';
  FRussianUpper[16] := '�';
  FRussianUpper[17] := '�';
  FRussianUpper[18] := '�';
  FRussianUpper[19] := '�';
  FRussianUpper[20] := '�';
  FRussianUpper[21] := '�';
  FRussianUpper[22] := '�';
  FRussianUpper[23] := '�';
  FRussianUpper[24] := '�';
  FRussianUpper[25] := '�';
  FRussianUpper[26] := '�';
  FRussianUpper[27] := '�';
  FRussianUpper[28] := '�';
end;

function TdxRussianUpperNumberConverter.GetAlphabet: TArray<Char>;
begin
  Result := FRussianUpper;
end;

function TdxRussianUpperNumberConverter.GetAlphabetSize: Integer;
begin
  Result := FAlphabetSize;
end;

function TdxRussianUpperNumberConverter.GetType: TdxNumberingFormat;
begin
  Result := TdxNumberingFormat.RussianUpper;
end;

{ TdxRussianLowerNumberConverter }

function TdxRussianLowerNumberConverter.GetAlphabet: TArray<Char>;
begin
  Result := FRussianLower;
end;

function TdxRussianLowerNumberConverter.GetAlphabetSize: Integer;
begin
  Result := FAlphabetSize;
end;

function TdxRussianLowerNumberConverter.GetType: TdxNumberingFormat;
begin
  Result := TdxNumberingFormat.RussianLower;
end;

class constructor TdxRussianLowerNumberConverter.Initialize;
begin
  FAlphabetSize := 29;
  SetLength(FRussianLower, FAlphabetSize);
  FRussianLower[0] := '�';
  FRussianLower[1] := '�';
  FRussianLower[2] := '�';
  FRussianLower[3] := '�';
  FRussianLower[4] := '�';
  FRussianLower[5] := '�';
  FRussianLower[6] := '�';
  FRussianLower[7] := '�';
  FRussianLower[8] := '�';
  FRussianLower[9] := '�';
  FRussianLower[10] := '�';
  FRussianLower[11] := '�';
  FRussianLower[12] := '�';
  FRussianLower[13] := '�';
  FRussianLower[14] := '�';
  FRussianLower[15] := '�';
  FRussianLower[16] := '�';
  FRussianLower[17] := '�';
  FRussianLower[18] := '�';
  FRussianLower[19] := '�';
  FRussianLower[20] := '�';
  FRussianLower[21] := '�';
  FRussianLower[22] := '�';
  FRussianLower[23] := '�';
  FRussianLower[24] := '�';
  FRussianLower[25] := '�';
  FRussianLower[26] := '�';
  FRussianLower[27] := '�';
  FRussianLower[28] := '�';
end;

{ TdxDecimalEnclosedParenthesesNumberConverter }

function TdxDecimalEnclosedParenthesesNumberConverter.ConvertNumber(AValue: Integer): string;
begin
  Assert(AValue >= 0);
  Result := Format('(%d)', [AValue]);
end;

function TdxDecimalEnclosedParenthesesNumberConverter.GetType: TdxNumberingFormat;
begin
  Result := TdxNumberingFormat.DecimalEnclosedParenthses;
end;

{ TdxNumberingListIndexCalculator }

constructor TdxNumberingListIndexCalculator.Create(AModel: TdxCustomDocumentModel; ANumberingListType: TdxNumberingType);
begin
  inherited Create;
  Assert(AModel <> nil); 
  FModel := AModel;
  FNumberingListType := ANumberingListType;
end;

function TdxNumberingListIndexCalculator.CreateNewAbstractList(
  ASource: TdxAbstractNumberingList): TdxAbstractNumberingListIndex;
var
  ANewAbstractNumberingList: TdxAbstractNumberingList;
begin
  ANewAbstractNumberingList := TdxAbstractNumberingList.Create(DocumentModel);
  ANewAbstractNumberingList.CopyFrom(ASource); 
  DocumentModel.AddAbstractNumberingListUsingHistory(ANewAbstractNumberingList);
  ANewAbstractNumberingList.SetId(DocumentModel.AbstractNumberingListIdProvider.GetNextId()); 
  Result := DocumentModel.AbstractNumberingLists.Count - 1;
end;

function TdxNumberingListIndexCalculator.CreateNewList(ASource: TdxAbstractNumberingList): TdxNumberingListIndex;
var
  ANewList: TdxNumberingList;
  AAbstractNumberingListIndex: TdxAbstractNumberingListIndex;
begin
  AAbstractNumberingListIndex := CreateNewAbstractList(ASource);
  ANewList := TdxNumberingList.Create(DocumentModel, AAbstractNumberingListIndex);
  DocumentModel.AddNumberingListUsingHistory(ANewList);
  Result := DocumentModel.NumberingLists.Count - 1;
end;

function TdxNumberingListIndexCalculator.GetListIndex(AStart, AEnd: TdxParagraphIndex): TdxNumberingListIndex;
begin
  if (AStart > 0) and ActivePieceTable.Paragraphs[AStart - 1].IsInList then
  begin
    Result := GetListIndexCore(AStart - 1);
    if Result >= 0 then 
      Exit;
  end;
  if (AEnd < ActivePieceTable.Paragraphs.Count - 1) and ActivePieceTable.Paragraphs[AEnd + 1].IsInList then
  begin
    Result := GetListIndexCore(AEnd + 1);
    if Result >= 0 then 
      Exit;
  end;
  Result := NumberingListIndexListIndexNotSetted; 
end;

function TdxNumberingListIndexCalculator.GetListIndexCore(AParagraphIndex: TdxParagraphIndex): TdxNumberingListIndex;
var
  AParagraphListType: TdxNumberingType;
  ANearParagraphListIndex: TdxNumberingListIndex;
begin
  ANearParagraphListIndex := ActivePieceTable.Paragraphs[AParagraphIndex].GetNumberingListIndex;
  AParagraphListType := TdxNumberingListHelper.GetListType(DocumentModel.NumberingLists[ANearParagraphListIndex].AbstractNumberingList);
  if AParagraphListType = NumberingListType then
  begin
    ContinueList := True;
    Result := ANearParagraphListIndex;
  end
  else
    Result := NumberingListIndexListIndexNotSetted;
end;

{ TdxDefaultNumberingListHelper }

class procedure TdxDefaultNumberingListHelper.InsertDefaultBulletNumberingList(DocumentModel: TdxCustomDocumentModel;
  AUnitConverter: TdxDocumentModelUnitConverter; ADefaultTabWidth: Integer);
const
  ASymbolDisplayFormat: array[0..1] of string = (TdxCharacters.MiddleDot, #$006F); 
var
  ADocumentModel: TdxDocumentModel absolute DocumentModel;
  ALevelOffset: Integer;
  AAbstractNumberingList: TdxAbstractNumberingList;
  AAbstractNumberingListIndex: TdxAbstractNumberingListIndex;
  I, AFirstLineIndent, AStringFormatIndex: Integer;
  ALevel: TdxListLevel;
begin
  ALevelOffset := AUnitConverter.DocumentsToModelUnits(150);
  AAbstractNumberingList := TdxAbstractNumberingList.Create(ADocumentModel);
  ADocumentModel.AddAbstractNumberingListUsingHistory(AAbstractNumberingList);
  AAbstractNumberingListIndex := ADocumentModel.AbstractNumberingLists.Count - 1;
  ADocumentModel.AddNumberingListUsingHistory(TdxNumberingList.Create(ADocumentModel, AAbstractNumberingListIndex));
  for I := 0 to AAbstractNumberingList.Levels.Count - 1 do
  begin
    ALevel := TdxListLevel.Create(ADocumentModel);
    AAbstractNumberingList.Levels[I] := ALevel;
    ALevel.CharacterProperties.BeginInit;
    ALevel.CharacterProperties.FontName := 'Symbol';
    ALevel.CharacterProperties.EndInit;
    AFirstLineIndent := ALevelOffset * (I + 1);
    SetFirstLineIndent(AAbstractNumberingList.Levels[I], AFirstLineIndent, ADefaultTabWidth div 2);
    AStringFormatIndex := I mod Length(ASymbolDisplayFormat);
    SetDisplayFormatString(AAbstractNumberingList.Levels[I], ASymbolDisplayFormat[AStringFormatIndex]);
    SetTemplateCode(AAbstractNumberingList.Levels[I], TdxNumberingListHelper.GenerateNewTemplateCode(ADocumentModel));
    ALevel.ListLevelProperties.Format := TdxNumberingFormat.Bullet;
  end;
end;

class procedure TdxDefaultNumberingListHelper.InsertDefaultMultilevelNumberingList(
  DocumentModel: TdxCustomDocumentModel; AUnitConverter: TdxDocumentModelUnitConverter; ADefaultTabWidth: Integer);
const
  AAlignPositionsInDocuments: array[0..8] of Integer = (75, 165, 255, 360, 465, 570, 675, 780, 900); 
var
  ADocumentModel: TdxDocumentModel absolute DocumentModel;
  AAbstractNumberingList: TdxAbstractNumberingList;
  AAbstractNumberingListIndex: TdxAbstractNumberingListIndex;
  I, ALevelOffset, AFirstLinePosition, ALeftIndent: Integer;
  ALevel: TdxListLevel;
begin
  AAbstractNumberingList := TdxAbstractNumberingList.Create(ADocumentModel);
  ADocumentModel.AddAbstractNumberingListUsingHistory(AAbstractNumberingList);
  AAbstractNumberingListIndex := ADocumentModel.AbstractNumberingLists.Count - 1;
  ADocumentModel.AddNumberingListUsingHistory(TdxNumberingList.Create(ADocumentModel, AAbstractNumberingListIndex));
  ALevelOffset := AUnitConverter.DocumentsToModelUnits(75);
  for I := 0 to AAbstractNumberingList.Levels.Count - 1 do
  begin
    ALevel := TdxListLevel.Create(ADocumentModel);
    AAbstractNumberingList.Levels[I] := ALevel;
    AFirstLinePosition := ALevelOffset * I;
    ALeftIndent := AUnitConverter.DocumentsToModelUnits(
      AAlignPositionsInDocuments[Math.Min(I, Length(AAlignPositionsInDocuments) - 1)]);
    SetFirstLineIndent(AAbstractNumberingList.Levels[I], ALeftIndent, ALeftIndent - AFirstLinePosition);
  end;
  SetDisplayFormatString(AAbstractNumberingList.Levels[0], '%0:s.');
  SetDisplayFormatString(AAbstractNumberingList.Levels[1], '%0:s.%1:s.');
  SetDisplayFormatString(AAbstractNumberingList.Levels[2], '%0:s.%1:s.%2:s.');
  SetDisplayFormatString(AAbstractNumberingList.Levels[3], '%0:s.%1:s.%2:s.%3:s.');
  SetDisplayFormatString(AAbstractNumberingList.Levels[4], '%0:s.%1:s.%2:s.%3:s.%4:s.');
  SetDisplayFormatString(AAbstractNumberingList.Levels[5], '%0:s.%1:s.%2:s.%3:s.%4:s.%5:s.');
  SetDisplayFormatString(AAbstractNumberingList.Levels[6], '%0:s.%1:s.%2:s.%3:s.%4:s.%5:s.%6:s.');
  SetDisplayFormatString(AAbstractNumberingList.Levels[7], '%0:s.%1:s.%2:s.%3:s.%4:s.%5:s.%6:s.%7:s.');
  SetDisplayFormatString(AAbstractNumberingList.Levels[8], '%0:s.%1:s.%2:s.%3:s.%4:s.%5:s.%6:s.%7:s.%8:s.');
end;

class procedure TdxDefaultNumberingListHelper.InsertDefaultSimpleNumberingList(DocumentModel: TdxCustomDocumentModel;
  AUnitConverter: TdxDocumentModelUnitConverter; ADefaultTabWidth: Integer);
var
  ADocumentModel: TdxDocumentModel absolute DocumentModel;
  ALevel: TdxListLevel;
  I, ALevelOffset, AFirstLineIndent: Integer;
  AAbstractNumberingList: TdxAbstractNumberingList;
  AAbstractNumberingListIndex: TdxAbstractNumberingListIndex;
begin
  AAbstractNumberingList := TdxAbstractNumberingList.Create(ADocumentModel);
  ADocumentModel.AddAbstractNumberingListUsingHistory(AAbstractNumberingList);
  AAbstractNumberingListIndex := ADocumentModel.AbstractNumberingLists.Count - 1;
  ADocumentModel.AddNumberingListUsingHistory(TdxNumberingList.Create(ADocumentModel, AAbstractNumberingListIndex));
  ALevelOffset := AUnitConverter.DocumentsToModelUnits(150);
  for I := 0 to AAbstractNumberingList.Levels.Count - 1 do
  begin
    ALevel := TdxListLevel.Create(ADocumentModel);
    AAbstractNumberingList.Levels[I] := ALevel;
    AFirstLineIndent := ALevelOffset * (I + 1);
    SetFirstLineIndent(AAbstractNumberingList.Levels[I], AFirstLineIndent, ADefaultTabWidth div 2);
    SetDisplayFormatString(AAbstractNumberingList.Levels[I], '%' + IntToStr(I) + ':s.'); 
    SetTemplateCode(AAbstractNumberingList.Levels[I], TdxNumberingListHelper.GenerateNewTemplateCode(ADocumentModel));
  end;
end;

class procedure TdxDefaultNumberingListHelper.InsertNumberingLists(ADocumentModel: TdxCustomDocumentModel;
  AUnitConverter: TdxDocumentModelUnitConverter; ADefaultTabWidth: Integer);
begin
  InsertDefaultSimpleNumberingList(ADocumentModel, AUnitConverter, ADefaultTabWidth);
  InsertDefaultBulletNumberingList(ADocumentModel, AUnitConverter, ADefaultTabWidth);
  InsertDefaultMultilevelNumberingList(ADocumentModel, AUnitConverter, ADefaultTabWidth);
end;

class procedure TdxDefaultNumberingListHelper.SetDisplayFormatString(const ALevel: IdxListLevel;
  const ADisplayFormatString: string);
begin
  ALevel.ListLevelProperties.BeginInit;
  try
    ALevel.ListLevelProperties.DisplayFormatString := ADisplayFormatString;
  finally
    ALevel.ListLevelProperties.EndInit;
  end;
end;

class procedure TdxDefaultNumberingListHelper.SetFirstLineIndent(const ALevel: IdxListLevel; ALineIndent,
  AFirstLineIndent: Integer);
begin
  ALevel.ParagraphProperties.BeginInit;
  try
    ALevel.ParagraphProperties.LeftIndent := ALineIndent;
    ALevel.ParagraphProperties.FirstLineIndentType := TdxParagraphFirstLineIndent.Hanging;
    ALevel.ParagraphProperties.FirstLineIndent := AFirstLineIndent;
  finally
    ALevel.ParagraphProperties.EndInit;
  end;
end;

class procedure TdxDefaultNumberingListHelper.SetTemplateCode(const ALevel: IdxListLevel; ATemplateCode: Integer);
begin
  ALevel.ListLevelProperties.BeginInit;
  try
    ALevel.ListLevelProperties.TemplateCode := ATemplateCode;
  finally
    ALevel.ListLevelProperties.EndInit;
  end;
end;

end.
