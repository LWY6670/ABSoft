{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Control.Mouse.Types;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Types, Classes, Controls;

type
  { TdxMouseEventArgs }

  TdxMouseButtons = set of TMouseButton;

  TdxMouseEventArgs = record
    Buttons: TdxMouseButtons;
    Shift: TShiftState;
    MousePos: TPoint;
    WheelDelta: Integer;
    Horizontal: Boolean;
    constructor Create(AButtons: TdxMouseButtons; AShift: TShiftState; const AMousePos: TPoint; ADelta: Integer; AHorizontal: Boolean = False);
  end;

  TdxMouseEventArgsArray = array of TdxMouseEventArgs;

implementation

{ TdxMouseEventArgs }

constructor TdxMouseEventArgs.Create(AButtons: TdxMouseButtons; AShift: TShiftState; const AMousePos: TPoint; ADelta: Integer; AHorizontal: Boolean = False);
begin
  Buttons := AButtons;
  Shift := AShift;
  MousePos := AMousePos;
  WheelDelta := ADelta;
  Horizontal := AHorizontal;
end;

end.

