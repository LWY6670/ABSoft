{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressSkins Library                                     }
{                                                                    }
{           Copyright (c) 2006-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSSKINS AND ALL ACCOMPANYING     }
{   VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY.              }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxSkinscxSchedulerPainter;

{$I cxVer.inc}

interface

uses
{$IFDEF DELPHI17}
  UITypes,
{$ENDIF}
  Windows, SysUtils, Classes, cxSchedulerCustomResourceView, cxDateUtils,
  cxSchedulerCustomControls, dxSkinsCore, dxSkinsLookAndFeelPainter,
  cxLookAndFeels, cxLookAndFeelPainters, cxGraphics, Graphics, cxGeometry,
  cxSchedulerUtils, Math, cxScheduler, cxClasses;

type

  { TcxSchedulerExternalSkinPainter }

  TcxSchedulerExternalSkinPainter = class(TcxSchedulerExternalPainter)
  private
    procedure DrawClippedElement(ACanvas: TcxCanvas; AElement: TdxSkinElement; ABorders: TcxBorders; R: TRect);
    function GetEventSelectionOffsets(AHasTimeLine: Boolean): TRect;
    function IsSkinAvalaible: Boolean;
    function SkinInfo: TdxSkinLookAndFeelPainterInfo;
  protected
    function DoDrawEvent(AViewInfo: TcxSchedulerEventCellViewInfo): Boolean; virtual;
  public
    procedure DoCustomDrawButton(AViewInfo: TcxSchedulerMoreEventsButtonViewInfo; var ADone: Boolean); override;
    //
    procedure DrawAllDayArea(ACanvas: TcxCanvas; const ARect: TRect; ABorderColor: TColor;
      ABorders: TcxBorders; AViewParams: TcxViewParams; ASelected: Boolean; ATransparent: Boolean); override;
    procedure DrawCurrentTime(ACanvas: TcxCanvas; AColor: TColor; AStart: TDateTime; ABounds: TRect); override;
    function DrawCurrentTimeFirst: Boolean; override;
    procedure DrawEvent(AViewInfo: TcxSchedulerEventCellViewInfo); override;
    procedure DrawEventAsProgressText(AViewInfo: TcxSchedulerEventCellViewInfo;
      AContent: TRect; AProgressRect: TRect; const AText: string); override;
    procedure DrawTimeGridCurrentTime(ACanvas: TcxCanvas; AColor: TColor; const ATimeLineRect: TRect); override;
    procedure DrawTimeGridHeader(ACanvas: TcxCanvas; ABorderColor: TColor;
      AViewInfo: TcxSchedulerCustomViewInfoItem; ABorders: TcxBorders; ASelected: Boolean); override;
    procedure DrawTimeLine(ACanvas: TcxCanvas; const ARect: TRect;
      AViewParams: TcxViewParams; ABorders: TcxBorders; ABorderColor: TColor); override;
    procedure DrawTimeRulerBackground(ACanvas: TcxCanvas; const ARect: TRect;
      ABorders: TcxBorders; AViewParams: TcxViewParams; ATransparent: Boolean); override;
    procedure DrawShadow(ACanvas: TcxCanvas; const ARect, AVisibleRect: TRect); override;
    function DrawShadowFirst: Boolean; override;
    function GetEventSelectionExtends: TRect; override;
    function MoreButtonSize(ASize: TSize): TSize; override;
    function NeedDrawSelection: Boolean; override;
  end;

implementation

uses
  Types, cxSchedulerStorage, dxGDIPlusClasses;

const
  cxHeaderStateToButtonState: array[Boolean] of TcxButtonState = (cxbsNormal, cxbsHot);

{ TcxSchedulerSkinViewItemsPainter }

procedure TcxSchedulerExternalSkinPainter.DrawClippedElement(
  ACanvas: TcxCanvas; AElement: TdxSkinElement; ABorders: TcxBorders; R: TRect);
begin
  ACanvas.SaveClipRegion;
  try
    ACanvas.SetClipRegion(TcxRegion.Create(R), roIntersect);
    AElement.Draw(ACanvas.Handle, cxRectExclude(R, AElement.Image.Margins.Margin, ABorders));
  finally
    ACanvas.RestoreClipRegion;
  end;
end;

function TcxSchedulerExternalSkinPainter.IsSkinAvalaible: Boolean;
begin
  Result := Painter.InheritsFrom(TdxSkinLookAndFeelPainter);
end;

function TcxSchedulerExternalSkinPainter.SkinInfo: TdxSkinLookAndFeelPainterInfo;
begin
  if (Painter = nil) or not Painter.GetPainterData(Result) then
    Result := nil;
end;

function TcxSchedulerExternalSkinPainter.MoreButtonSize(ASize: TSize): TSize;
begin
  if IsSkinAvalaible and (SkinInfo.SchedulerMoreButton <> nil) then
    Result := SkinInfo.SchedulerMoreButton.Size
  else
    Result := ASize;
end;

procedure TcxSchedulerExternalSkinPainter.DoCustomDrawButton(
  AViewInfo: TcxSchedulerMoreEventsButtonViewInfo; var ADone: Boolean);
begin
  inherited DoCustomDrawButton(AViewInfo, ADone);
  if not ADone and IsSkinAvalaible then
  begin
    ADone := SkinInfo.SchedulerMoreButton <> nil;
    if ADone then
      SkinInfo.SchedulerMoreButton.Draw(AViewInfo.Canvas.Handle, AViewInfo.Bounds, Byte(AViewInfo.IsDown));
  end;
end;

procedure TcxSchedulerExternalSkinPainter.DrawAllDayArea(ACanvas: TcxCanvas; const ARect: TRect;
  ABorderColor: TColor; ABorders: TcxBorders; AViewParams: TcxViewParams; ASelected: Boolean; ATransparent: Boolean);
var
  AElement: TdxSkinElement;
begin
  if IsSkinAvalaible then
    AElement := SkinInfo.SchedulerAllDayArea[ASelected]
  else
    AElement := nil;

  if AElement <> nil then
    DrawClippedElement(ACanvas, AElement, ABorders, ARect)
  else
    inherited DrawAllDayArea(ACanvas, ARect,  ABorderColor, ABorders, AViewParams, ASelected, ATransparent);
end;  

procedure TcxSchedulerExternalSkinPainter.DrawCurrentTime(
  ACanvas: TcxCanvas; AColor: TColor; AStart: TDateTime; ABounds: TRect);
var
  AElement: TdxSkinElement;
  AIndicatorRect: TRect;
  Y, I: Integer;
begin
  if IsSkinAvalaible then
    AElement := SkinInfo.SchedulerCurrentTimeIndicator
  else
    AElement := nil;

  if AElement <> nil then
  begin
    Y := Trunc(ABounds.Top + ((dxTimeOf(Now) - dxTimeOf(AStart)) * cxRectHeight(ABounds)) / HourToTime);

    AIndicatorRect := cxRectSetTop(ABounds, Y, 0);
    AIndicatorRect := cxRectCenterVertically(AIndicatorRect, AElement.Image.Size.cy);
    Inc(AIndicatorRect.Left, 5);
    Dec(AIndicatorRect.Right);

    if cxRectIntersect(AIndicatorRect, ABounds) then
    begin
      for I := 0 to 1 do
        AElement.Draw(ACanvas.Handle, AIndicatorRect, I);
    end;
  end
  else
    inherited DrawCurrentTime(ACanvas, AColor, AStart, ABounds);
end;

function TcxSchedulerExternalSkinPainter.NeedDrawSelection: Boolean;
begin
  Result := not IsSkinAvalaible;
end;

function TcxSchedulerExternalSkinPainter.DrawCurrentTimeFirst: Boolean;
begin
  Result := True;
end;

procedure TcxSchedulerExternalSkinPainter.DrawEvent(AViewInfo: TcxSchedulerEventCellViewInfo);
begin
  if not DoDrawEvent(AViewInfo) then
    inherited DrawEvent(AViewInfo);
end;

function TcxSchedulerExternalSkinPainter.DoDrawEvent(AViewInfo: TcxSchedulerEventCellViewInfo): Boolean;

  function CheckRect(const ARect: TRect): TRect;
  begin
    if AViewInfo.Selected then
      Result := cxRectInflate(ARect, GetEventSelectionOffsets(not IsRectEmpty(AViewInfo.TimeLineRect)))
    else
      Result := ARect;
  end;

  function IsAllDayEvent(AEvent: TcxSchedulerControlEvent): Boolean;
  begin
    Result := AEvent.AllDayEvent or (AEvent.Duration > 1);
  end;

  function IsDefaultContentColor(AColor: TColor): Boolean;
  begin
    if AViewInfo.Selected then
      Result := AColor = Painter.DefaultSelectionColor
    else
      Result := AColor = Painter.DefaultSchedulerEventColor(IsAllDayEvent(AViewInfo.Event));
  end;

  procedure ColorizeContent(AColor: TColor; AMask: TdxSkinElement);
  const
    ImageIndexMap: array[Boolean] of Integer = (1, 0);
  var
    AMaskBmp: TcxBitmap;
    ARegion: TcxRegion;
  begin
    if (AMask <> nil) and cxColorIsValid(AColor) and not IsDefaultContentColor(AColor) then
    begin
      AMaskBmp := TcxBitmap.CreateSize(AViewInfo.Bounds, pf24bit);
      try
        AMask.Draw(AMaskBmp.Canvas.Handle, AMaskBmp.ClientRect, ImageIndexMap[IsRectEmpty(AViewInfo.TimeLineRect)]);

        AViewInfo.Canvas.SaveClipRegion;
        try
          ARegion := TcxRegion.Create(cxCreateRegionFromBitmap(AMaskBmp, clBlack));
          ARegion.Offset(AViewInfo.Bounds.TopLeft);
          AViewInfo.Canvas.SetClipRegion(ARegion, roIntersect);
          dxGpFillRect(AViewInfo.Canvas.Handle, AViewInfo.Bounds, AColor, 120);
        finally
          AViewInfo.Canvas.RestoreClipRegion;
        end;
      finally
        AMaskBmp.Free;
      end;
    end;
  end;

  function GetSeparatorColor(ABorderColor: TdxSkinColor): TColor;
  begin
    Result := clDefault;
    if ABorderColor <> nil then
      Result := ABorderColor.Value;
    if not cxColorIsValid(Result) then
      Result := AViewInfo.SeparatorColor;
  end;

const
  SelectedFlags: array[Boolean] of TdxSkinElementState = (esNormal, esHot);
var
  AElement: TdxSkinElement;
begin
  Result := False;
  if IsSkinAvalaible and not AViewInfo.Transparent then
  begin
    AElement := SkinInfo.SchedulerAppointment[IsRectEmpty(AViewInfo.TimeLineRect)];
    if AElement <> nil then
    begin
      AElement.Draw(AViewInfo.Canvas.Handle, CheckRect(AViewInfo.Bounds), 0, SelectedFlags[AViewInfo.Selected]);
      ColorizeContent(AViewInfo.ViewParams.Color, SkinInfo.SchedulerAppointmentMask);
      AViewInfo.SeparatorColor := GetSeparatorColor(SkinInfo.SchedulerAppointmentBorder);
      AViewInfo.Transparent := True;
      Result := True;
    end;
  end;
end;

procedure TcxSchedulerExternalSkinPainter.DrawEventAsProgressText(
  AViewInfo: TcxSchedulerEventCellViewInfo; AContent: TRect; AProgressRect: TRect; const AText: string);
begin
  if IsSkinAvalaible then
    cxDrawText(AViewInfo.Canvas.Handle, AText, AContent, DT_CENTER or DT_VCENTER or DT_SINGLELINE)
  else
    inherited DrawEventAsProgressText(AViewInfo, AContent, AProgressRect, AText);
end;

procedure TcxSchedulerExternalSkinPainter.DrawTimeGridCurrentTime(
  ACanvas: TcxCanvas; AColor: TColor; const ATimeLineRect: TRect);
var
  AElement: TdxSkinElement;
begin
  if IsSkinAvalaible then
    AElement := SkinInfo.SchedulerTimeGridCurrentTimeIndicator
  else
    AElement := nil;

  if AElement <> nil then
    AElement.Draw(ACanvas.Handle, ATimeLineRect)
  else
    inherited DrawTimeGridCurrentTime(ACanvas, AColor, ATimeLineRect);
end;

procedure TcxSchedulerExternalSkinPainter.DrawTimeGridHeader(ACanvas: TcxCanvas;
  ABorderColor: TColor; AViewInfo: TcxSchedulerCustomViewInfoItem; ABorders: TcxBorders; ASelected: Boolean);
var
  AElement: TdxSkinElement;
begin
  if IsSkinAvalaible then
    AElement := SkinInfo.SchedulerTimeGridHeader[ASelected]
  else
    AElement := nil;

  if AElement <> nil then
    DrawClippedElement(ACanvas, AElement, ABorders, AViewInfo.Bounds)
  else
    inherited DrawTimeGridHeader(ACanvas, ABorderColor, AViewInfo, ABorders, ASelected);
end;

procedure TcxSchedulerExternalSkinPainter.DrawTimeLine(ACanvas: TcxCanvas;
  const ARect: TRect; AViewParams: TcxViewParams; ABorders: TcxBorders; ABorderColor: TColor);
var
  AElement: TdxSkinElement;
begin
  if IsSkinAvalaible then
    AElement := SkinInfo.SchedulerTimeLine
  else
    AElement := nil;

  if AElement <> nil then
    DrawClippedElement(ACanvas, AElement, ABorders, ARect)
  else
    inherited DrawTimeLine(ACanvas, ARect, AViewParams, ABorders, ABorderColor); 
end;

procedure TcxSchedulerExternalSkinPainter.DrawTimeRulerBackground(ACanvas: TcxCanvas;
  const ARect: TRect; ABorders: TcxBorders; AViewParams: TcxViewParams; ATransparent: Boolean);
var
  AElement: TdxSkinElement;
begin
  if IsSkinAvalaible then
    AElement := SkinInfo.SchedulerTimeRuler
  else
    AElement := nil;

  if AElement <> nil then
    DrawClippedElement(ACanvas, AElement, ABorders, ARect)
  else
    inherited DrawTimeRulerBackground(ACanvas, ARect, ABorders, AViewParams, ATransparent);
end;

function TcxSchedulerExternalSkinPainter.GetEventSelectionExtends: TRect;
begin
  Result := GetEventSelectionOffsets(False);
end;

function TcxSchedulerExternalSkinPainter.GetEventSelectionOffsets(AHasTimeLine: Boolean): TRect;
var
  ABorderSize: Integer;
begin
  if IsSkinAvalaible and (SkinInfo.SchedulerAppointmentBorderSize <> nil) then
    ABorderSize := SkinInfo.SchedulerAppointmentBorderSize.Value
  else
    ABorderSize := 0;

  Result := cxRect(ABorderSize, ABorderSize, ABorderSize, ABorderSize);
  if AHasTimeLine then
    Result.Left := cxTimeLineWidth;
end;

procedure TcxSchedulerExternalSkinPainter.DrawShadow(ACanvas: TcxCanvas; const ARect, AVisibleRect: TRect);
const
  ShadowSize = 4;

  function GetBottomShadowRect(const R: TRect): TRect;
  begin
    Result := cxRect(R.Left + ShadowSize, R.Bottom - ShadowSize, R.Right + ShadowSize, R.Bottom + ShadowSize);
  end;

  function GetRightShadowRect(const R: TRect): TRect;
  begin
    Result := cxRect(R.Right - ShadowSize, R.Top + ShadowSize, R.Right + ShadowSize, R.Bottom - ShadowSize);
  end;

  procedure DrawShadowLine(AShadow: TdxSkinElement; const ARect: TRect);
  begin
    if AShadow <> nil then
      AShadow.Draw(ACanvas.Handle, ARect);
  end;

begin
  if IsSkinAvalaible then
  begin
    ACanvas.SaveClipRegion;
    try
      ACanvas.IntersectClipRect(AVisibleRect);
      DrawShadowLine(SkinInfo.SchedulerAppointmentShadow[False], GetBottomShadowRect(ARect));
      DrawShadowLine(SkinInfo.SchedulerAppointmentShadow[True], GetRightShadowRect(ARect));
    finally
      ACanvas.RestoreClipRegion;
    end;
  end
  else
    inherited DrawShadow(ACanvas, ARect, AVisibleRect);
end;

function TcxSchedulerExternalSkinPainter.DrawShadowFirst: Boolean;
begin
  Result := IsSkinAvalaible or inherited DrawShadowFirst;
end;

initialization
  ExternalPainterClass := TcxSchedulerExternalSkinPainter;

finalization
  ExternalPainterClass := TcxSchedulerExternalPainter;
end.
