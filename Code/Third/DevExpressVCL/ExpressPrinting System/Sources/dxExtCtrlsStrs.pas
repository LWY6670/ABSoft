{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressPrinting System COMPONENT SUITE                   }
{                                                                    }
{           Copyright (C) 1998-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSPRINTINGSYSTEM AND             }
{   ALL ACCOMPANYING VCL CONTROLS AS PART OF AN                      }
{   EXECUTABLE PROGRAM ONLY.                                         }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxExtCtrlsStrs;

interface

{$I cxVer.inc}

resourcestring
  sdxAutoColorText = '自动';
  sdxCustomColorText = '定制...';

  sdxSysColorScrollBar = '滚动条';
  sdxSysColorBackground = '背景';
  sdxSysColorActiveCaption = '动作标题';
  sdxSysColorInactiveCaption = '不活动标题';
  sdxSysColorMenu = '菜单';
  sdxSysColorWindow = '窗口';
  sdxSysColorWindowFrame = '窗口框架';
  sdxSysColorMenuText = '菜单文本';
  sdxSysColorWindowText = '窗口文本';
  sdxSysColorCaptionText = '标题文本';
  sdxSysColorActiveBorder = '活动边框';
  sdxSysColorInactiveBorder = '不活动边框';
  sdxSysColorAppWorkSpace = '程序工作空间';
  sdxSysColorHighLight = '高亮';
  sdxSysColorHighLighText = '高亮文本';
  sdxSysColorBtnFace = '按钮表面';
  sdxSysColorBtnShadow = '按钮阴影';
  sdxSysColorGrayText = '灰色文本';
  sdxSysColorBtnText = '按钮文本';
  sdxSysColorInactiveCaptionText = '不活动标题文本';
  sdxSysColorBtnHighligh = '按钮高亮';
  sdxSysColor3DDkShadow = '3DDk阴影';
  sdxSysColor3DLight = '3D明亮';
  sdxSysColorInfoText = '信息文本';
  sdxSysColorInfoBk = '信息背景';

  sdxPureColorBlack = '黑';
  sdxPureColorRed = '红';
  sdxPureColorLime = '橙';
  sdxPureColorYellow = '黄';
  sdxPureColorGreen = '绿';
  sdxPureColorTeal = '青';
  sdxPureColorAqua = '浅绿';
  sdxPureColorBlue = '蓝';
  sdxPureColorWhite = '白';
  sdxPureColorOlive = '橄榄';
  sdxPureColorMoneyGreen = '黄绿';
  sdxPureColorNavy = '藏青';
  sdxPureColorSkyBlue = '天蓝';
  sdxPureColorGray = '灰';
  sdxPureColorMedGray = '中灰';
  sdxPureColorSilver = '银';
  sdxPureColorMaroon = '茶色';
  sdxPureColorPurple = '紫';
  sdxPureColorFuchsia = '紫红';
  sdxPureColorCream = '米色';
  
  sdxBrushStyleSolid = '固体';
  sdxBrushStyleClear = '清除';
  sdxBrushStyleHorizontal = '水平';
  sdxBrushStyleVertical = '垂直';
  sdxBrushStyleFDiagonal = 'F斜纹';
  sdxBrushStyleBDiagonal = 'B斜纹';
  sdxBrushStyleCross = '交叉';
  sdxBrushStyleDiagCross = '反交叉';

implementation

end.

 
