{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressPrinting System COMPONENT SUITE                   }
{                                                                    }
{           Copyright (C) 1998-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSPRINTINGSYSTEM AND             }
{   ALL ACCOMPANYING VCL CONTROLS AS PART OF AN                      }
{   EXECUTABLE PROGRAM ONLY.                                         }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxPSPDFCrypt;

interface

{$I cxVer.inc}

uses
  Windows, SysUtils, Classes, Math, dxCore;

type
  TdxRC4Key = array[0..255] of Byte;

procedure dxRC4Crypt(var AKey: TdxRC4Key; AInData, AOutData: PByteArray; ADataSize: Integer);
procedure dxRC4Initialize(out AKey: TdxRC4Key; APassword: PByteArray; APasswordLength: Integer);
implementation

procedure dxRC4Initialize(out AKey: TdxRC4Key; APassword: PByteArray; APasswordLength: Integer);
var
  ATempKey: TdxRC4Key;
  I, J, K: Integer;
begin
  for I := 0 to 255 do
  begin
    ATempKey[I] := APassword^[I mod APasswordLength];
    AKey[I] := I;
  end;

  J := 0;
  for I := 0 to 255 do
  begin
    J := (J + AKey[I] + ATempKey[I]) and $FF;
    K := AKey[I];
    AKey[I] := AKey[J];
    AKey[j] := K;
  end;
end;

procedure dxRC4Crypt(var AKey: TdxRC4Key; AInData, AOutData: PByteArray; ADataSize: Integer);
var
  T, K, I, J: Integer;
begin
  I := 0;
  J := 0;
  for K := 0 to ADataSize - 1 do
  begin
    I := Byte(I + 1);
    J := Byte(J + AKey[I]);

    T := AKey[I];
    AKey[I] := AKey[J];
    AKey[J] := T;

    T := Byte(AKey[I] + AKey[J]);
    AOutData^[K] := AInData^[K] xor AKey[T];
  end;
end;

end.
