{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressPrinting System COMPONENT SUITE                   }
{                                                                    }
{           Copyright (C) 1998-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSPRINTINGSYSTEM AND             }
{   ALL ACCOMPANYING VCL CONTROLS AS PART OF AN                      }
{   EXECUTABLE PROGRAM ONLY.                                         }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxPSRes;

interface

{$I cxVer.inc}

resourcestring
  sdxBtnOK = '确定';
  sdxBtnOKAccelerated = '确定(&O)';
  sdxBtnCancel = '取消';
  sdxBtnClose = '关闭';
  sdxBtnApply = '应用(&A)';
  sdxBtnHelp = '帮助(&H)';
  sdxBtnFix = '调整(&F)';
  sdxBtnNew = '新建(&N)...';
  sdxBtnIgnore = '忽略(&I)';
  sdxBtnYes = '是(&Y)';
  sdxBtnNo = '否(&N)';
  sdxBtnEdit = '编辑(&E)...';
  sdxBtnReset = '重置(&R)';
  sdxBtnAdd = '添加(&A)';
  sdxBtnAddComposition = '添加组件(&C)';
  sdxBtnDefault = '默认(&D)...';
  sdxBtnDelete = '删除(&D)...';
  sdxBtnDescription = '描述(&D)...';  
  sdxBtnCopy = '复制(&C)...';
  sdxBtnYesToAll = '全部是(&A)';
  sdxBtnFootnoteProperties = '页脚属性…...';
  sdxBtnRestoreDefaults = '恢复默认值(&R)';
  sdxBtnRestoreOriginal = '还原(&O)';
  sdxBtnTitleProperties = '标题属性...';
  sdxBtnProperties = '属性(&R)...';
  sdxBtnNetwork = '网络(&W)...';
  sdxBtnBrowse = '浏览(&B)...';
  sdxBtnPageSetup = '页面设置(&G)...';
  sdxBtnPrintPreview = '打印预览(&V)...';
  sdxBtnPreview = '预览(&V)...';
  sdxBtnPrint = '打印...';
  sdxBtnOptions = '选项(&O)...';
  sdxBtnStyleOptions = '样式选项...';
  sdxBtnDefinePrintStyles = '定义样式(&D)...';
  sdxBtnPrintStyles = '打印样式';
  sdxBtnBackground = '背景';
  sdxBtnShowToolBar = '显示工具栏(&T)';
  sdxBtnDesign = '设计(&E)...';
  sdxBtnMoveUp = '上移(&U)';
  sdxBtnMoveDown = '下移(&N)';  
  sdxBtnMoreColors = '更多颜色(&M)...';
  sdxBtnFillEffects = '填充效果(&F)...';
  sdxBtnNoFill = '不填充(&N)';
  sdxBtnAutomatic = '自动(&A)';
  sdxBtnNone = '无(&N)';

  sdxBtnOtherTexture = '其它纹理(&X)...';
  sdxBtnInvertColors = '反转颜色(&N)';
  sdxBtnSelectPicture = '选择图片(&L)...';

  sdxEditReports = '编辑报表';
  sdxComposition = '布局';
  sdxReportFootnotesDlgCaption = '报表脚注';
  sdxReportTitleDlgCaption = '报表标题';
  sdxMode = '模式(&M):';
  sdxText = '本文(&T)';
  sdxProperties = '属性(&P)';
  sdxAdjustOnScale = '适合页面(&A)';

  // Report Title mode
  sdxTitleModeNone = '无';
  sdxTitleModeOnEveryTopPage = '在每张顶页';
  sdxTitleModeOnFirstPage = '在第一页';

  // Report Footnotes mode
  sdxFootnotesModeNone = '无';
  sdxFootnotesModeOnEveryBottomPage = '在每张底页';
  sdxFootnotesModeOnLastPage = '在上一页';

  sdxEditDescription = '编辑描述';
  sdxRename = '重命名(&M)';
  sdxSelectAll = '全选';
  
  sdxAddReport = '增加报表';
  sdxAddAndDesignReport = '增加并设计报表(&D)...';
  sdxNewCompositionCaption = '新建布局';
  sdxName = '名字(&N):';
  sdxCaption = '标题(&C):';
  sdxAvailableSources = '可用的源(&A)';
  sdxOnlyComponentsInActiveForm = '只显示当前表单的组件';
  sdxOnlyComponentsWithoutLinks = '只显示除现有报表链接以外的组件';
  sdxItemName = '名称';
  sdxItemDescription = '描述';
    
  sdxConfirmDeleteItem = '要删除下一个项目： %s 吗?';
  sdxAddItemsToComposition = '增加项目到布局';
  sdxHideAlreadyIncludedItems = '隐藏已包含项目';
  sdxAvailableItems = '可用项目(&I)';
  sdxItems = '项目(&I)';
  sdxEnable = '允许(&E)';
  sdxOptions = '选项';
  sdxShow = '显示';
  sdxPaintItemsGraphics = '绘制项目图示(&P)';
  sdxDescription = '描述:';

  sdxNewReport = '新报表';
    
  sdxOnlySelected = '只是选定的(&S)';
  sdxExtendedSelect = '扩展选定的(&E)';
  sdxIncludeFixed = '包含固定区(&I)';

  sdxFonts = '字体';
  sdxBtnFont = '字体(&N)...';
  sdxBtnEvenFont = '偶数行字体(&V)...';
  sdxBtnOddFont = '奇数行字体(&N)...';
  sdxBtnFixedFont = '固定区字体(&I)...';
  sdxBtnGroupFont = '组字体(&P)...';
  sdxBtnChangeFont = '更换字体(&N)...';

  sdxFont = '字体';
  sdxOddFont = '奇数行字体';
  sdxEvenFont = '偶数行字体';
  sdxPreviewFont = '预览字体';
  sdxCaptionNodeFont = '层次标题字体';
  sdxGroupNodeFont = '组节点字体';
  sdxGroupFooterFont = '组脚字体';
  sdxHeaderFont = '页眉字体';
  sdxFooterFont = '页脚字体';
  sdxBandFont = '带区字体';

  sdxTransparent = '透明(&T)';
  sdxFixedTransparent = '透明(&X)';
  sdxCaptionTransparent = '标题透明';
  sdxGroupTransparent = '组透明';

  sdxGraphicAsTextValue = '(图像)';
  sdxColors = '颜色';
  sdxColor = '颜色(&L):';
  sdxOddColor = '奇数行颜色(&L):';
  sdxEvenColor = '偶数行颜色(&V):';
  sdxPreviewColor = '预览颜色(&P):';
  sdxBandColor = '带区颜色(&B):';
  sdxLevelCaptionColor = '层次标题颜色(&V):';
  sdxHeaderColor = '标题颜色(&E):';
  sdxGroupNodeColor = '组节点颜色(&N):';
  sdxGroupFooterColor = '组脚颜色(&G):';
  sdxFooterColor = '页脚颜色(&T):';
  sdxFixedColor = '固定颜色(&I):';
  sdxGroupColor = '组颜色(&I):';
  sdxCaptionColor = '标题颜色:';
  sdxGridLinesColor = '网格线颜色(&D):';

  sdxBands = '带区(&B)';
  sdxLevelCaptions = '层次标题(&C)';
  sdxHeaders = '页眉(&E)';
  sdxFooters = '页脚(&R)';
  sdxGroupFooters = '组脚(&G)';
  sdxPreview = '预览(&W)';
  sdxPreviewLineCount = '预览行数(&T):';
  sdxAutoCalcPreviewLineCount = '自动计算预览行数(&U)';

  sdxGrid = '网格(&D)';
  sdxNodesGrid = '节点网格(&N)';
  sdxGroupFooterGrid = '组脚网格(&P)';

  sdxStateImages = '状态图像(&S)';
  sdxImages = '图象(&I)';

  sdxTextAlign = '文本排列(&A)';
  sdxTextAlignHorz = '水平(&Z)';
  sdxTextAlignVert = '垂直(&V)';
  sdxTextAlignLeft = '靠左';
  sdxTextAlignCenter = '居中';
  sdxTextAlignRight = '靠右';
  sdxTextAlignTop = '顶部';
  sdxTextAlignVCenter = '居中';
  sdxTextAlignBottom = '底部';
  sdxBorderLines = '边框线条(&B)';
  sdxHorzLines = '水平线(&Z)';
  sdxVertLines = '垂直线(&V)';
  sdxFixedHorzLines = '固定水平线(&X)';
  sdxFixedVertLines = '固定垂直线(&D)';
  sdxFlatCheckMarks = '平面检查框(&L)';
  sdxCheckMarksAsText = '用文本显示检查框(&D)';

  sdxRowAutoHeight = '自动计算行高(&W)';
  sdxEndEllipsis = '结束省略符(&E)';

  sdxDrawBorder = '绘制边框(&D)';
  sdxFullExpand = '完全展开(&E)';
  sdxBorderColor = '边框颜色(&B):';
  sdxAutoNodesExpand = '自动展开节点(&U)';
  sdxExpandLevel = '展开层次(&L):';
  sdxFixedRowOnEveryPage = '固定每页行数(&E)';

  sdxDrawMode = '绘制模式(&M):';
  sdxDrawModeStrict = '精确';
  sdxDrawModeOddEven = '奇/偶行模式';
  sdxDrawModeChess = '国际象棋模式';
  sdxDrawModeBorrow = '从源借用';

  sdx3DEffects = '三维效果';
  sdxUse3DEffects = '使用三维效果(&3)';
  sdxSoft3D = '柔和三维(&3)';

  sdxBehaviors = '性能';
  sdxMiscellaneous = '杂志';
  sdxOnEveryPage = '在每页';
  sdxNodeExpanding = '展开节点';
  sdxSelection = '选择';
  sdxNodeAutoHeight = '节点自动调整高度(&N)';
  sdxTransparentGraphics = '图形透明(&T)';
  sdxAutoWidth = '自动调整宽度(&W)';

  sdxDisplayGraphicsAsText = '用文本形式显示图形(&T)';
  sdxTransparentColumnGraphics = '图形透明(&G)';

  sdxBandsOnEveryPage = '每页显示带区';
  sdxHeadersOnEveryPage = '每页显示页眉';
  sdxFootersOnEveryPage = '每页显示页脚';
  sdxGraphics = '图形';

  { Common messages }
  
  sdxOutOfResources = '资源不足';
  sdxFileAlreadyExists = '文件"%s"已存在。';
  sdxConfirmOverWrite = '文件"%s"已存在。是否覆盖 ？';
  sdxInvalidFileName = '无效文件名"%s"';
  sdxRequiredFileName = '输入文件名。';
  sdxOutsideMarginsMessage =
    '一个或多个页边距超出页面的可打印区域。' + #13#10 +
    '是否继续 ？';
  sdxOutsideMarginsMessage2 =
    '一个或多个页边距超出页面的可打印区域。' + #13#10 +
    '选择调整按钮，以适当增加页边距。';
  sdxInvalidMarginsMessage =
    '一个或多个页边距被设置为无效值。' + #13#10 +
    '选择调整按钮来更正此问题。' + #13#10 +
    '选择还原按钮，恢复原始值。';
  sdxInvalidMargins = '一个或多个页边距是无效值';
  sdxOutsideMargins = '一个或多个页边距超出页面的可打印区域';
  sdxReportCellClassNotRegistered = '%s类没注册。 ' +
    '请确保相应的报表链接单元已经添加到了应用程序';
  sdxThereAreNowItemsForShow = '没有项目';


  { Color palette }
  
  sdxPageBackground = '页面背景';
  sdxPenColor = '铅笔颜色';
  sdxFontColor = '字体颜色';
  sdxBrushColor = '刷子颜色';
  sdxHighLight = '加亮';

  { Color names }
  
  sdxColorBlack = '黑色';
  sdxColorDarkRed = '深红';
  sdxColorRed = '红色';
  sdxColorPink = '粉红';
  sdxColorRose = '玫瑰红';
  sdxColorBrown = '褐色';
  sdxColorOrange = '橘黄';
  sdxColorLightOrange = '浅橘黄';
  sdxColorGold = '金色';
  sdxColorTan = '棕黄';
  sdxColorOliveGreen = '橄榄绿';
  sdxColorDrakYellow = '深黄';
  sdxColorLime = '绿黄';
  sdxColorYellow = '黄色';
  sdxColorLightYellow = '浅黄';
  sdxColorDarkGreen = '深绿';
  sdxColorGreen = '绿色';
  sdxColorSeaGreen = '海绿';
  sdxColorBrighthGreen = '鲜绿';
  sdxColorLightGreen = '浅绿';
  sdxColorDarkTeal = '深灰蓝';
  sdxColorTeal = '青色';
  sdxColorAqua = '宝石蓝';
  sdxColorTurquoise = '青绿';
  sdxColorLightTurquoise = '浅青绿';
  sdxColorDarkBlue = '深蓝';
  sdxColorBlue = '蓝色';
  sdxColorLightBlue = '浅蓝';
  sdxColorSkyBlue = '天蓝';
  sdxColorPaleBlue = '淡蓝';
  sdxColorIndigo = '靛蓝';
  sdxColorBlueGray = '蓝-灰';
  sdxColorViolet = '紫色';
  sdxColorPlum = '梅红';
  sdxColorLavender = '淡紫';
  sdxColorGray80 = '灰色-80%';
  sdxColorGray50 = '灰色-50%';
  sdxColorGray40 = '灰色-40%';
  sdxColorGray25 = '灰色-25%';
  sdxColorWhite = '白色';
 
  { FEF Dialog }
  
  sdxTexture = '纹理(&T)';
  sdxPattern = '图案(&P)';
  sdxPicture = '图片(&I)';
  sdxForeground = '前景(&F)';
  sdxBackground = '背景(&B)';
  sdxSample = '示范:';

  sdxFEFCaption = '填充效果';
  sdxPaintMode = '画图模式';
  sdxPaintModeCenter = '居中';
  sdxPaintModeStretch = '拉伸';
  sdxPaintModeTile = '平铺';
  sdxPaintModeProportional = '锁定比例';

  { Pattern names }
  
  sdxPatternGray5 = '5%';
  sdxPatternGray10 = '10%';
  sdxPatternGray20 = '20%';
  sdxPatternGray25 = '25%';
  sdxPatternGray30 = '30%';
  sdxPatternGray40 = '40%';
  sdxPatternGray50 = '50%';
  sdxPatternGray60 = '60%';
  sdxPatternGray70 = '70%';
  sdxPatternGray75 = '75%';
  sdxPatternGray80 = '80%';
  sdxPatternGray90 = '90%';
  sdxPatternLightDownwardDiagonal = '浅色下对角线';
  sdxPatternLightUpwardDiagonal = '浅色上对角线';
  sdxPatternDarkDownwardDiagonal = '深色下对角线';
  sdxPatternDarkUpwardDiagonal = '深色上对角线';
  sdxPatternWideDownwardDiagonal = '宽下对角线';
  sdxPatternWideUpwardDiagonal = '宽上对角线';
  sdxPatternLightVertical = '浅色垂线';
  sdxPatternLightHorizontal = '浅色横线';
  sdxPatternNarrowVertical = '窄竖线';
  sdxPatternNarrowHorizontal = '窄横线';
  sdxPatternDarkVertical = '深色竖线';
  sdxPatternDarkHorizontal = '深色横线';
  sdxPatternDashedDownward = '下对角虚线';
  sdxPatternDashedUpward = '上对角虚线';
  sdxPatternDashedVertical = '横虚线';
  sdxPatternDashedHorizontal = '竖虚线';
  sdxPatternSmallConfetti = '小纸屑';
  sdxPatternLargeConfetti = '大纸屑';
  sdxPatternZigZag = '之字形';
  sdxPatternWave = '波浪线';
  sdxPatternDiagonalBrick = '对角砖形';
  sdxPatternHorizantalBrick = '横向砖形';
  sdxPatternWeave = '编织物';
  sdxPatternPlaid = '苏格兰方格';
  sdxPatternDivot = '草皮';
  sdxPatternDottedGrid = '虚线网格';
  sdxPatternDottedDiamond = '点式菱形';
  sdxPatternShingle = '瓦形';
  sdxPatternTrellis = '棚架';
  sdxPatternSphere = '球体';
  sdxPatternSmallGrid = '小网格';
  sdxPatternLargeGrid = '大网格';
  sdxPatternSmallCheckedBoard = '小棋盘';
  sdxPatternLargeCheckedBoard = '大棋盘';
  sdxPatternOutlinedDiamond = '轮廓式菱形';
  sdxPatternSolidDiamond = '实心菱形';

  { Texture names }
  
  sdxTextureNewSprint = '新闻纸';
  sdxTextureGreenMarble = '绿色大理石';
  sdxTextureBlueTissuePaper = '蓝色砂纸';
  sdxTexturePapyrus = '纸莎草纸';
  sdxTextureWaterDroplets = '水滴';
  sdxTextureCork = '软木塞';
  sdxTextureRecycledPaper = '再生纸';
  sdxTextureWhiteMarble = '白色大理石';
  sdxTexturePinkMarble = '粉色砂纸';
  sdxTextureCanvas = '画布';
  sdxTexturePaperBag = '纸袋';
  sdxTextureWalnut = '胡桃';
  sdxTextureParchment = '羊皮纸';
  sdxTextureBrownMarble = '棕色大理石';
  sdxTexturePurpleMesh = '紫色网格';
  sdxTextureDenim = '斜纹布';
  sdxTextureFishFossil = '鱼类化石';
  sdxTextureOak = '栎木';
  sdxTextureStationary = '信纸';
  sdxTextureGranite = '花岗岩';
  sdxTextureBouquet = '花束';
  sdxTextureWonenMat = '编织物';
  sdxTextureSand = '沙滩';
  sdxTextureMediumWood = '深色木质';

  sdxFSPCaption = '图像预览';
  sdxWidth = '宽';
  sdxHeight = '高';


  { Brush Dialog }
  
  sdxBrushDlgCaption = '画笔属性';
  sdxStyle = '样式(&S):';

  { Enter New File Name dialog }
  
  sdxENFNCaption = '选择新文件名称';
  sdxEnterNewFileName = '输入新文件名称';


  { Define styles dialog }
  
  sdxDefinePrintStylesCaption = '定义打印样式';
  sdxDefinePrintStylesTitle = '打印样式(&S):';
  sdxDefinePrintStylesWarningDelete = '是否删除"%s" ？';
  sdxDefinePrintStylesWarningClear = '要删除所有非内置样式吗 ？';
  sdxClear = '清空(&L)...';
  { Print device }
  
  sdxCustomSize = '自定义大小';
  sdxDefaultTray = '默认纸盒';
  sdxInvalidPrintDevice = '所选打印机无效';
  sdxNotPrinting = '当前打印机不打印';
  sdxPrinting = '打印进度';
  sdxDeviceOnPort = '%s 在 %s 上';
  sdxPrinterIndexError = '打印机索引超出范围';
  sdxNoDefaultPrintDevice = '没有选择默认打印机';

  { Edit AutoText Entries Dialog }
  
 sdxAutoTextDialogCaption = '编辑自动图文集';
  sdxEnterAutoTextEntriesHere = ' 输入自动图文集(&U): ';

   { Print dialog }

  sdxPrintDialogCaption = '打印';
  sdxPrintDialogPrinter = ' 打印机 ';
  sdxPrintDialogName = '名称(&N):';
  sdxPrintDialogStatus = '状态:';
  sdxPrintDialogType = '类型:';
  sdxPrintDialogWhere = '位置:';
  sdxPrintDialogComment = '注释:';
  sdxPrintDialogPrintToFile = '打印到文件(&F)';
  sdxPrintDialogPageRange = ' 页面范围 ';
  sdxPrintDialogAll = '所有(&A)';
  sdxPrintDialogCurrentPage = '当前页(&E)';
  sdxPrintDialogSelection = '选择(&S)';
  sdxPrintDialogPages = '页(&P):';
  sdxPrintDialogRangeLegend =
    '输入页号或页面范围' + #13#10 +
    '用逗号(,)分隔。例如: 1,3,5-12。';
  sdxPrintDialogCopies = ' 副本 ';
  sdxPrintDialogNumberOfPages = '页数(&U):';
  sdxPrintDialogNumberOfCopies = '份数(&C):';
  sdxPrintDialogCollateCopies = '逐份打印(&T)';
  sdxPrintDialogAllPages = '所有';
  sdxPrintDialogEvenPages = '偶数页';
  sdxPrintDialogOddPages = '奇数页';
  sdxPrintDialogPrintStyles = ' 打印样式(&Y) ';
   { PrintToFile Dialog }
  
  sdxPrintDialogOpenDlgTitle = '选择文件名称';
  sdxPrintDialogOpenDlgAllFiles = '所有文件';
  sdxPrintDialogOpenDlgPrinterFiles = '打印机文件';
  sdxPrintDialogPageNumbersOutOfRange = '页码超出范围 (%d - %d)';
  sdxPrintDialogInvalidPageRanges = '无效的页码范围';
  sdxPrintDialogRequiredPageNumbers = '输入页码';
  sdxPrintDialogNoPrinters = 
    '没有安装打印机。 要安装打印机， ' + 
    '请打开[开始菜单|设置|控制面板|打印机]，双击[添加打印机]。 ' + 
    '按照屏幕上的指示完成打印机安装过程。';
  sdxPrintDialogInPrintingState = '打印机正在打印。' + #13#10 +
    '请稍候。';

  { Printer State }
  
  sdxPrintDialogPSPaused = '暂停';
  sdxPrintDialogPSPendingDeletion = '正在删除';
  sdxPrintDialogPSBusy = '忙';
  sdxPrintDialogPSDoorOpen = '通道打开';
  sdxPrintDialogPSError = '错误';
  sdxPrintDialogPSInitializing = '正在初始化';
  sdxPrintDialogPSIOActive = '输入输出有效';
  sdxPrintDialogPSManualFeed = '手工送纸';
  sdxPrintDialogPSNoToner = '没有墨粉';
  sdxPrintDialogPSNotAvailable = '不可用';
  sdxPrintDialogPSOFFLine = '脱机';
  sdxPrintDialogPSOutOfMemory = '内存溢出';
  sdxPrintDialogPSOutBinFull = '输出储存器已满';
  sdxPrintDialogPSPagePunt = '页平底';
  sdxPrintDialogPSPaperJam = '卡纸';
  sdxPrintDialogPSPaperOut = '纸张跳出';
  sdxPrintDialogPSPaperProblem = '纸张问题';
  sdxPrintDialogPSPrinting = '正在打印';
  sdxPrintDialogPSProcessing = '正在处理';
  sdxPrintDialogPSTonerLow = '墨粉较少';
  sdxPrintDialogPSUserIntervention = '用户干涉';
  sdxPrintDialogPSWaiting = '正在等待';
  sdxPrintDialogPSWarningUp = '正在预热';
  sdxPrintDialogPSReady = '就绪';
  sdxPrintDialogPSPrintingAndWaiting = '正在打印：%d份文档，请等待';

  sdxLeftMargin = '左边界';
  sdxTopMargin = '上边距';
  sdxRightMargin = '右边界';
  sdxBottomMargin = '下边距';
  sdxGutterMargin = '装订线';
  sdxHeaderMargin = '页眉';
  sdxFooterMargin = '页脚';

  sdxUnitsInches = '在..中';
  sdxUnitsCentimeters = '厘米';
  sdxUnitsMillimeters = '毫米';
  sdxUnitsPoints = '磅';
  sdxUnitsPicas = '像素';

  sdxUnitsDefaultName = '默认';
  sdxUnitsInchesName = '英寸';
  sdxUnitsCentimetersName = '厘米';
  sdxUnitsMillimetersName = '毫米';
  sdxUnitsPointsName = '点';
  sdxUnitsPicasName = '点';

  sdxPrintPreview = '打印预览';
  sdxReportDesignerCaption = '报表设计';
  sdxCompositionDesignerCaption = '组件编辑器';
  sdxCompositionStartEachItemFromNewPage = '每一项从新的一页开始(&S)';

  sdxComponentNotSupportedByLink = 'TdxComponentPrinter不支持组件"%s"';
  sdxComponentNotSupported = 'TdxComponentPrinter不支持组件"%s"';
  sdxPrintDeviceNotReady = '打印机尚未安装或者没有就绪';
  sdxUnableToGenerateReport = '不能生成报表';
  sdxPreviewNotRegistered = '没有已注册的预览表单';
  sdxComponentNotAssigned = '%s' + #13#10 + '不能分配"Component"属性';
  sdxPrintDeviceIsBusy = '打印机正忙';
  sdxPrintDeviceError = '打印机出错 ！';
  sdxMissingComponent = '缺少"Component"属性';
  sdxDataProviderDontPresent = '没有与指定组件构成中的链接';
  sdxBuildingReport = '构建报表：已完成 %d%%';                            // obsolete
  sdxPrintingReport = '正在打印报表：已完成%d页。 按ESC键中断'; // obsolete
  sdxDefinePrintStylesMenuItem = '定义打印样式(&S)...';
  sdxAbortPrinting = '要中断打印吗 ？';
  sdxStandardStyle = '标准样式';

  sdxFontStyleBold = '黑体';
  sdxFontStyleItalic = '斜体';
  sdxFontStyleUnderline = '下划线';
  sdxFontStyleStrikeOut = '删除线';
  sdxPt = '磅。';

  sdxNoPages = '[没有可显示的页面';
  sdxPageWidth = '页宽';
  sdxWholePage = '整页';
  sdxTwoPages = '两页';
  sdxFourPages = '四页';
  sdxWidenToSourceWidth = '扩展到原始宽度';

  sdxMenuBar = '菜单栏';
  sdxStandardBar = '标准';
  sdxHeaderFooterBar = '页眉和页脚';
  sdxShortcutMenusBar = '快捷菜单';
  sdxAutoTextBar = '自动图文集';

  sdxMenuFile = '文件(&F)';
  sdxMenuFileDesign = '设计(&D)...';
  sdxMenuFilePrint = '打印(&P)...';
  sdxMenuFilePrintDialog = '打印对话框';
  sdxMenuFilePageSetup = '页面设置(&U)...';
  sdxMenuPrintStyles = '打印样式';
  sdxMenuFileExit = '关闭(&C)';
  sdxMenuExportToPDF = '输出PDF格式';

  sdxMenuEdit = '编辑(&E)';
  sdxMenuEditCut = '剪切(&T)';
  sdxMenuEditCopy = '复制(&C)';
  sdxMenuEditPaste = '粘贴(&P)';
  sdxMenuEditDelete = '删除(&D)';
  sdxMenuEditFind = '查找(&F)...';
  sdxMenuEditFindNext = '查找下一个(&X)';
  sdxMenuEditReplace = '替换(&R)...';

  sdxMenuLoad = '读取(&L)...';
  sdxMenuPreview = '预览(&V)...';

  sdxMenuInsert = '插入(&I)';
  sdxMenuInsertAutoText = '自动图文集(&A)';
  sdxMenuInsertEditAutoTextEntries = '自动图文集(&X)...';
  sdxMenuInsertAutoTextEntries = '自动图文集列表';
  sdxMenuInsertAutoTextEntriesSubItem = '插入自动图文集(&S)';
  sdxMenuInsertPageNumber = '页码(&P)';
  sdxMenuInsertTotalPages = '页数(&N)';
  sdxMenuInsertPageOfPages = '页面页码(&G)';
  sdxMenuInsertDateTime = '日期与时间';
  sdxMenuInsertDate = '日期(&D)';
  sdxMenuInsertTime = '时间(&T)';
  sdxMenuInsertUserName = '用户名(&U)';
  sdxMenuInsertMachineName = '机器名称(&M)';

  sdxMenuView = '视图(&V)';
  sdxMenuViewMargins = '页边距(&M)';
  sdxMenuViewFlatToolBarButtons = '平面工具栏按钮(&F)';
  sdxMenuViewLargeToolBarButtons = '大工具栏按钮(&L)';
  sdxMenuViewMarginsStatusBar = '页边距栏(&A)';
  sdxMenuViewPagesStatusBar = '状态栏(&S)';
  sdxMenuViewToolBars = '工具栏(&T)';
  sdxMenuViewPagesHeaders = '页眉(&H)';
  sdxMenuViewPagesFooters = '页脚(&R)';
  sdxMenuViewSwitchToLeftPart = '切换到左部';
  sdxMenuViewSwitchToRightPart = '切换到右部';
  sdxMenuViewSwitchToCenterPart = '切换到中间';
  sdxMenuViewHFSwitchHeaderFooter = '显示页眉/页脚(&S)';
  sdxMenuViewSwitchToFooter = '页脚';
  sdxMenuViewSwitchToHeader = '页眉';
  sdxMenuViewHFClose = '关闭(&C)';

  sdxMenuZoom = '缩放(&Z)';
  sdxMenuZoomPercent100 = '100%(&1)';
  sdxMenuZoomPageWidth = '页宽(&W)';
  sdxMenuZoomWholePage = '整页(&H)';
  sdxMenuZoomTwoPages = '两页(&T)';
  sdxMenuZoomFourPages = '四页(&F)';
  sdxMenuZoomMultiplyPages = '多页(&M)';
  sdxMenuZoomWidenToSourceWidth = '扩展到原始宽度(&O)';
  sdxMenuZoomSetup = '设置(&S)...';

  sdxMenuPages = '页(&P)';

  sdxMenuGotoPage = '转到(&G)';
  sdxMenuGotoPageFirst = '首页(&F)';
  sdxMenuGotoPagePrev = '上一页(&P)';
  sdxMenuGotoPageNext = '下一页(&N)';
  sdxMenuGotoPageLast = '尾页(&L)';
  sdxMenuActivePage = '当前页(&A):';

  sdxMenuFormat = '格式(&O)';
  sdxMenuFormatHeaderAndFooter = '页眉和页脚(&H)';
  sdxMenuFormatAutoTextEntries = '自动图文集(&A)...';
  sdxMenuFormatDateTime = '日期与时间(&T)...';
  sdxMenuFormatPageNumbering = '页码(&N)...';
  sdxMenuFormatPageBackground = '背景(&K)...';
  sdxMenuFormatShrinkToPage = '适应页宽(&F)';
  sdxMenuFormatHFBackground = '页眉/页脚背景...';
  sdxMenuFormatHFClear = '清除文本';

  sdxMenuTools = '工具(&T)';
  sdxMenuToolsCustomize = '自定义(&C)...';
  sdxMenuToolsOptions = '选项(&O)...';

  sdxMenuHelp = '帮助(&H)';
  sdxMenuHelpTopics = '帮助主题(&T)...';
  sdxMenuHelpAbout = '关于(&A)...';

  sdxMenuShortcutPreview = '预览';
  sdxMenuShortcutAutoText = '自动图文集';

  sdxMenuBuiltInMenus = '内置菜单';
  sdxMenuShortCutMenus = '快捷菜单';
  sdxMenuNewMenu = '新建菜单';

  { Hints }
  
  sdxHintFileDesign = '设计报表';
  sdxHintFilePrint = '打印';
  sdxHintFilePrintDialog = '打印对话框';
  sdxHintFilePageSetup = '页面设置';
  sdxHintFileExit = '关闭预览';
  sdxHintExportToPDF = '输出PDF格式';

  sdxHintEditFind = '查找';
  sdxHintEditFindNext = '查找下一个';
  sdxHintEditReplace = '替换';

  sdxHintInsertEditAutoTextEntries = '编辑自动图文集';
  sdxHintInsertPageNumber = '插入页码';
  sdxHintInsertTotalPages = '插入页数';
  sdxHintInsertPageOfPages = '插入页码';
  sdxHintInsertDateTime = '插入日期和时间';
  sdxHintInsertDate = '插入日期';
  sdxHintInsertTime = '插入时间';
  sdxHintInsertUserName = '插入用户名称';
  sdxHintInsertMachineName = '插入机器名称';

  sdxHintViewMargins = '查看页边距';
  sdxHintViewLargeButtons = '查看大按钮';
  sdxHintViewMarginsStatusBar = '查看页边距状态栏';
  sdxHintViewPagesStatusBar = '查看页面状态栏';
  sdxHintViewPagesHeaders = '查看页眉';
  sdxHintViewPagesFooters = '查看页脚';
  sdxHintViewSwitchToLeftPart = '切换到左边的页眉/页脚';
  sdxHintViewSwitchToRightPart = '切换到右边的页眉/页脚';
  sdxHintViewSwitchToCenterPart = '切换到中间的页眉/页脚';
  sdxHintViewHFSwitchHeaderFooter = '在页眉和页脚之间切换';
  sdxHintViewSwitchToFooter = '切换到页脚';
  sdxHintViewSwitchToHeader = '切换到页眉';
  sdxHintViewHFClose = '关闭';

  sdxHintViewZoom = '缩放';
  sdxHintZoomPercent100 = '百分100%';
  sdxHintZoomPageWidth = '页宽';
  sdxHintZoomWholePage = '整页';
  sdxHintZoomTwoPages = '两页';
  sdxHintZoomFourPages = '四页';
  sdxHintZoomMultiplyPages = '多页';
  sdxHintZoomWidenToSourceWidth = '扩展到原始宽度';
  sdxHintZoomSetup = '设置缩放比例';

  sdxHintFormatDateTime = '格式化日期和时间';
  sdxHintFormatPageNumbering = '格式化页码';
  sdxHintFormatPageBackground = '背景';
  sdxHintFormatShrinkToPage = '适应页宽';
  sdxHintFormatHFBackground = '页眉/页脚背景';
  sdxHintFormatHFClear = '清除页眉/页脚文本';

  sdxHintGotoPageFirst = '首页';
  sdxHintGotoPagePrev = '上一页';
  sdxHintGotoPageNext = '下一页';
  sdxHintGotoPageLast = '尾页';
  sdxHintActivePage = '当前页';

  sdxHintToolsCustomize = '自定义工具栏';
  sdxHintToolsOptions = '选项';

  sdxHintHelpTopics = '帮助主题';
  sdxHintHelpAbout = '关于';

  sdxPopupMenuLargeButtons = '大按钮(&L)';
  sdxPopupMenuFlatButtons = '平面按钮(&F)';

  sdxPaperSize = '纸张大小:';
  sdxStatus = '状态:';
  sdxStatusReady = '就绪';
  sdxStatusPrinting = '正在打印。已完成 %d 页';
  sdxStatusGenerateReport = '创建报表。已完成 %d%%';

  sdxHintDoubleClickForChangePaperSize = '双击改变纸张大小';
  sdxHintDoubleClickForChangeMargins = '双击改变页边距';

  { Date&Time Formats Dialog }
  
  sdxDTFormatsCaption = '日期与时间';
  sdxDTFormatsAvailableDateFormats = '有效的日期格式(&A):';
  sdxDTFormatsAvailableTimeFormats = '有效的时间格式(&T):';
  sdxDTFormatsAutoUpdate = '自动更新(&U)';
  sdxDTFormatsChangeDefaultFormat =
    '是否改变默认日期和时间格式来匹配"%s"  - "%s" ？';

  { PageNumber Formats Dialog }
  
  sdxPNFormatsCaption = '页码格式';
  sdxPageNumbering = '页码';
  sdxPNFormatsNumberFormat = '数字格式(&F):';
  sdxPNFormatsContinueFromPrevious = '续前节(&C)';
  sdxPNFormatsStartAt = '起始页码(&A):';
  sdxPNFormatsChangeDefaultFormat =
    '是否改变默认页码格式来匹配"%s" ？';

  { Zoom Dialog }
  
  sdxZoomDlgCaption = '缩放';
  sdxZoomDlgZoomTo = ' 缩放至 ';
  sdxZoomDlgPageWidth = '页宽(&W)';
  sdxZoomDlgWholePage = '整页(&H)';
  sdxZoomDlgTwoPages = '两页(&T)';
  sdxZoomDlgFourPages = '四页(&F)';
  sdxZoomDlgManyPages = '多页(&M):';
  sdxZoomDlgPercent = '百分比(&E):';
  sdxZoomDlgPreview = ' 预览 ';
  sdxZoomDlgFontPreview = ' 12pt Times New Roman ';
  sdxZoomDlgFontPreviewString = 'AaBbCcDdEeXxYyZz';

  { Select page X x Y }
  
  sdxPages = '页';
  sdxCancel = '取消';

  { Preferences dialog }
  
  sdxPreferenceDlgCaption = '选项';
  sdxPreferenceDlgTab1 = '常规(&G)';
  sdxPreferenceDlgTab2 = '';
  sdxPreferenceDlgTab3 = '';
  sdxPreferenceDlgTab4 = '';
  sdxPreferenceDlgTab5 = '';
  sdxPreferenceDlgTab6 = '';
  sdxPreferenceDlgTab7 = '';
  sdxPreferenceDlgTab8 = '';
  sdxPreferenceDlgTab9 = '';
  sdxPreferenceDlgTab10 = '';
  sdxPreferenceDlgShow = ' 显示(&S) ';
  sdxPreferenceDlgMargins = '页边距(&M) ';
  sdxPreferenceDlgMarginsHints = '页边距提示(&H)';
  sdxPreferenceDlgMargingWhileDragging = '当拖曳时显示页边距提示(&D)';
  sdxPreferenceDlgLargeBtns = '大工具栏按钮(&L)';
  sdxPreferenceDlgFlatBtns = '平面工具栏按钮(&F)';
  sdxPreferenceDlgMarginsColor = '页边距颜色(&C):';
  sdxPreferenceDlgMeasurementUnits = '度量单位(&U):';
  sdxPreferenceDlgSaveForRunTimeToo = '保存设置(&R)';
  sdxPreferenceDlgZoomScroll = '用智能鼠标缩放(&Z)';
  sdxPreferenceDlgZoomStep = '缩放比例(&P):';
  
  { Page Setup }
  
  sdxCloneStyleCaptionPrefix = '副本 (%D) ';
  sdxInvalideStyleCaption = '样式名称"%s"已经存在。请提供另一个名称。';
  sdxHintMoreHFFunctions = '更多函数';

  sdxPageSetupCaption = '页面设置';
  sdxStyleName = '样式名称(&N):';

  sdxPage = '页(&P)';
  sdxMargins = '页边距(&M)';
  sdxHeaderFooter = '页眉和页脚(&H)';
  sdxScaling = '比例(&S)';

  sdxPaper = ' 纸张大小 ';
  sdxPaperType = '类型(&Y)';
  sdxPaperDimension = '尺寸';
  sdxPaperWidth = '宽度(&W):';
  sdxPaperHeight = '高度(&E):';
  sdxPaperSource = '纸张来源(&U):';

  sdxOrientation = ' 方向 ';
  sdxPortrait = '纵向(&O)';
  sdxLandscape = '横向(&L)';
  sdxPrintOrder = ' 打印次序 ';
  sdxDownThenOver = '下，然后结束(&D)';
  sdxOverThenDown = '结束然后下(&V)';
  sdxShading = ' 阴影 ';
  sdxPrintUsingGrayShading = '使用灰色阴影打印(&G)';

  sdxCenterOnPage = '居中方式';
  sdxHorizontally = '水平(&Z)';
  sdxVertically = '垂直(&V)';

  sdxHeader = '页眉 ';
  sdxBtnHeaderFont = '字体(&F)...';
  sdxBtnHeaderBackground = '背景(&B)';
  sdxFooter = '页脚 ';
  sdxBtnFooterFont = '字体(&N)...';
  sdxBtnFooterBackground = '背景(&G)';

  sdxTop = '上(&T):';
  sdxLeft = '左边(&L):';
  sdxRight = '右边(&G):';
  sdxBottom = '下(&B):';
  sdxHeader2 = '页眉(&E):';
  sdxFooter2 = '页脚(&R):';

  sdxAlignment = '对齐';
  sdxVertAlignment = ' 垂直对齐 ';
  sdxReverseOnEvenPages = '偶页相反(&R)';

  sdxAdjustTo = '调整到(&A):';
  sdxFitTo = '适合(&F):';
  sdxPercentOfNormalSize = '% 正常大小';
  sdxPagesWideBy = '页宽(&W)';
  sdxTall = '页高(&T)';

  sdxOf = '共';
  sdxLastPrinted = '上次打印时间 ';
  sdxFileName = '文件名 ';
  sdxFileNameAndPath = '文件名称和路径 ';
  sdxPrintedBy = '打印由 ';
  sdxPrintedOn = '打印在 ';
  sdxCreatedBy = '创建由 ';
  sdxCreatedOn = '创建在 ';
  sdxConfidential = '机密';

  { HF function }
  
  sdxHFFunctionNameDate = '日期';
  sdxHFFunctionNameDateTime = '日期与时间';
  sdxHFFunctionNameImage = '图像';
  sdxHFFunctionNameMachineName = '机器名称';
  sdxHFFunctionNamePageNumber = '页码';
  sdxHFFunctionNamePageOfPages = '第 # 页 共 # 页';
  sdxHFFunctionNameTime = '时间';
  sdxHFFunctionNameTotalPages = '总页数';
  sdxHFFunctionNameUnknown = '未知';
  sdxHFFunctionNameUserName = '用户名';

  sdxHFFunctionHintDate = '打印日期';
  sdxHFFunctionHintDateTime = '打印日期和时间';
  sdxHFFunctionHintImage = '图像';
  sdxHFFunctionHintMachineName = '机器名称';
  sdxHFFunctionHintPageNumber = '页码';
  sdxHFFunctionHintPageOfPages = '第 # 页 共 # 页';
  sdxHFFunctionHintTime = '打印时间';
  sdxHFFunctionHintTotalPages = '总页数';
  sdxHFFunctionHintUserName = '用户名';

  sdxHFFunctionTemplateDate = '打印日期';
  sdxHFFunctionTemplateDateTime = '打印日期和时间';
  sdxHFFunctionTemplateImage = '图像';
  sdxHFFunctionTemplateMachineName = '机器名称';
  sdxHFFunctionTemplatePageNumber = '第 # 页';
  sdxHFFunctionTemplatePageOfPages = '第 # 页 共 # 页';
  sdxHFFunctionTemplateTime = '打印时间';
  sdxHFFunctionTemplateTotalPages = '总页数';
  sdxHFFunctionTemplateUserName = '用户名';

  { PDF Export Dialog }

  sdxPDFDialogAuthor = '作者';
  sdxPDFDialogCaption = 'PDF 导出选项';
  sdxPDFDialogCompressed = '压缩';
  sdxPDFDialogCreator = '作者';
  sdxPDFDialogDocumentInfoTabSheet = '文档信息(&D)';
  sdxPDFDialogEmbedFonts = '嵌入字体';
  sdxPDFDialogExportSettings = '输出设置';
  sdxPDFDialogExportTabSheet = '导出(&E)';
  sdxPDFDialogKeywords = '关键字';
  sdxPDFDialogMaxCompression = '最大压缩';
  sdxPDFDialogMaxQuality = '最大质量';
  sdxPDFDialogOpenAfterExport = '打开后导出';
  sdxPDFDialogPageRageTabSheet = '页(&P)';
  sdxPDFDialogSecurityAllowChanging = '允许更改文档';
  sdxPDFDialogSecurityAllowComments = '允许评论';
  sdxPDFDialogSecurityAllowCopy = '允许内容复制和提取';
  sdxPDFDialogSecurityAllowDocumentAssemble = '允许文档集';
  sdxPDFDialogSecurityAllowPrint = '允许打印';
  sdxPDFDialogSecurityAllowPrintHiResolution = '允许使用高分辨率打印';
  sdxPDFDialogSecurityEnabled = '启用';
  sdxPDFDialogSecurityMethod = '方法:';
  sdxPDFDialogSecurityOwnerPassword = '所有者密码:';
  sdxPDFDialogSecuritySettings = '安全设置';
  sdxPDFDialogSecurityUserPassword = '用户密码:';
  sdxPDFDialogSubject = '主题';
  sdxPDFDialogTitle = '标题';
  sdxPDFDialogUseCIDFonts = '使用 CID 字体';
  sdxPDFDialogUseJPEGCompression = '使用 JPEG 压缩的图像';
  sdxPDFDialogTabDocInfo = '文档信息(&D)';
  sdxPDFDialogTabExport = '导出(&E)';
  sdxPDFDialogTabPages = '页(&P)';
  sdxPDFDialogTabSecurity = '安全性(&S)';

  { Designer strings }
  
  { Months }
  
  sdxJanuary = '一月';
  sdxFebruary = '二月';
  sdxMarch = '三月';
  sdxApril = '四月';
  sdxMay = '五月';
  sdxJune = '六月';
  sdxJuly = '七月';
  sdxAugust = '八月';
  sdxSeptember = '九月';
  sdxOctober = '十月';
  sdxNovember = '十一月';
  sdxDecember = '十二月';

  sdxEast = '东方';
  sdxWest = '西方';
  sdxSouth = '南方';
  sdxNorth = '北方';

  sdxTotal = '合计';

  { dxFlowChart }
  
  sdxPlan = '设计图';
  sdxSwimmingPool = '游泳池';
  sdxAdministration = '管理员';
  sdxPark = '公园';
  sdxCarParking = '停车场';

  { dxOrgChart }
  
  sdxCorporateHeadquarters = '企业' + #13#10 + '总部';
  sdxSalesAndMarketing = '销售和' + #13#10 + '市场营销';
  sdxEngineering = '工程技术部';
  sdxFieldOfficeCanada = '外地办事处:' + #13#10 + '加拿大';

  { dxMasterView }
  
  sdxOrderNoCaption = '序号';
  sdxNameCaption = '名称';
  sdxCountCaption = '数量';
  sdxCompanyCaption = '公司';
  sdxAddressCaption = '地址';
  sdxPriceCaption = '价格';
  sdxCashCaption = '现金';

  sdxName1 = '宙斯';
  sdxName2 = '撒旦';
  sdxCompany1 = 'Jennie Inc。';
  sdxCompany2 = 'Daimler-Chrysler AG';
  sdxAddress1 = '123 Home Lane';
  sdxAddress2 = '9333 Holmes Dr。';

  { dxTreeList }
  
  sdxCountIs = '数量：%d';
  sdxRegular = '常规';
  sdxIrregular = '不规则';

  sdxTLBand = '项目数据';
  sdxTLColumnName = '名称';
  sdxTLColumnAxisymmetric = '轴对称';
  sdxTLColumnItemShape = '形状';

  sdxItemShapeAsText = '(图形)';

  sdxItem1Name = '圆柱';
  sdxItem2Name = '圆锥';
  sdxItem3Name = '棱锥';
  sdxItem4Name = '盒子';
  sdxItem5Name = '自由表面';

  sdxItem1Description = '';
  sdxItem2Description = '轴对称几何图形';
  sdxItem3Description = '轴对称几何图形';
  sdxItem4Description = '锐角几何图形';
  sdxItem5Description = '';
  sdxItem6Description = '';
  sdxItem7Description = '简单挤压表面';

  { PS 2.3 }
  
  { Patterns common }
  
  sdxPatternIsNotRegistered = '图案"%s"没注册';

  { Excel edge patterns }
  
  sdxSolidEdgePattern = '实线';
  sdxThinSolidEdgePattern = '中实线';    
  sdxMediumSolidEdgePattern = '中实线';  
  sdxThickSolidEdgePattern = '粗实线';
  sdxDottedEdgePattern = '圆点';
  sdxDashedEdgePattern = '短画线';
  sdxDashDotDotEdgePattern = '短画线-点-点';
  sdxDashDotEdgePattern = '短画线-点';
  sdxSlantedDashDotEdgePattern = '斜短画线-点';
  sdxMediumDashDotDotEdgePattern = '中等短画线-点-点';
  sdxHairEdgePattern = '丝状';
  sdxMediumDashDotEdgePattern = '中等短画线-点';
  sdxMediumDashedEdgePattern = '中等短画线';
  sdxDoubleLineEdgePattern = '双线';

  { Excel fill patterns names} 
   
  sdxSolidFillPattern = '实线';
  sdxGray75FillPattern = '75% 灰色';
  sdxGray50FillPattern = '50% 灰色';
  sdxGray25FillPattern = '25% 灰色';
  sdxGray125FillPattern = '12.5% 灰色';
  sdxGray625FillPattern = '6.25% 灰色';
  sdxHorizontalStripeFillPattern = '水平条纹';
  sdxVerticalStripeFillPattern = '垂直条纹';
  sdxReverseDiagonalStripeFillPattern = '逆对角线条纹';
  sdxDiagonalStripeFillPattern = '对角线条纹';
  sdxDiagonalCrossHatchFillPattern = '对角线剖面线';
  sdxThickCrossHatchFillPattern = '粗对角线剖面线';
  sdxThinHorizontalStripeFillPattern = '细水平条纹';
  sdxThinVerticalStripeFillPattern = '细垂直条纹';
  sdxThinReverseDiagonalStripeFillPattern = '细逆对角线条纹';
  sdxThinDiagonalStripeFillPattern = '细对角线条纹';
  sdxThinHorizontalCrossHatchFillPattern = '细水平剖面线';
  sdxThinDiagonalCrossHatchFillPattern = '细对角线剖面线';
  
  { cxSpreadSheet } 
  
  sdxShowRowAndColumnHeadings = '行和列标题(&R)';
  sdxShowGridLines = '网格行';
  sdxSuppressSourceFormats = '禁止源格式(&S)';
  sdxRepeatHeaderRowAtTop = '在顶端重复标题行'; 
  sdxDataToPrintDoesNotExist = 
    '不能激活报表连接，因为没有内容可以打印。';
 
  { Designer strings } 
  
  { Short names of month }
  
  sdxJanuaryShort = '一月';
  sdxFebruaryShort = '二月';
  sdxMarchShort = '三月';  
  sdxAprilShort = '四月';  
  sdxMayShort = '五月';    
  sdxJuneShort = '六月';    
  sdxJulyShort = '七月';
  sdxAugustShort = '八月';
  sdxSeptemberShort = '九月';
  sdxOctoberShort = '十月';
  sdxNovemberShort = '十一月';
  sdxDecemberShort = '十二月';

  { TreeView }
  
  sdxTechnicalDepartment = '技术部门';
  sdxSoftwareDepartment = '软件部门';
  sdxSystemProgrammers = '系统程序员';
  sdxEndUserProgrammers = 'GUI开发人员';
  sdxBetaTesters = '测试员';
  sdxHumanResourceDepartment = '人力资源部门';

  { misc. }
  
  sdxTreeLines = '树线(&T)';
  sdxTreeLinesColor = '树线颜色(&R):';
  sdxExpandButtons = '展开按钮(&X)';
  sdxCheckMarks = '用文本显示检查框';
  sdxTreeEffects = '树效果';
  sdxAppearance = '外观';

  { Designer previews }
  
  { Localize if you want (they are used inside FormatReport dialog -> ReportPreview) }

  sdxCarLevelCaption = '汽车';
  
  sdxManufacturerBandCaption = '厂商数据';  
  sdxModelBandCaption = '汽车数据';
  
  sdxManufacturerNameColumnCaption = '名称';
  sdxManufacturerLogoColumnCaption = '标志';  
  sdxManufacturerCountryColumnCaption = '国籍';  
  sdxCarModelColumnCaption = '型号';
  sdxCarIsSUVColumnCaption = 'SUV';
  sdxCarPhotoColumnCaption = '照片';  

  sdxCarManufacturerName1 = '宝马';
  sdxCarManufacturerName2 = '福特';
  sdxCarManufacturerName3 = '奥迪';
  sdxCarManufacturerName4 = '路虎';

  sdxCarManufacturerCountry1 = '德国';
  sdxCarManufacturerCountry2 = '美国';
  sdxCarManufacturerCountry3 = '德国';
  sdxCarManufacturerCountry4 = '英国';
  
  sdxCarModel1 = 'X5 4.8is';
  sdxCarModel2 = 'Excursion';
  sdxCarModel3 = 'S8 Quattro';
  sdxCarModel4 = 'G4 Challenge';
  
  sdxTrue = 'True';
  sdxFalse = 'False';  

  { PS 2.4 }
  
  { dxPrnDev.pas }
  
  sdxAuto = '自动'; 
  sdxCustom = '自定义'; 
  sdxEnv = 'Env'; 

  { Grid 4 }

  sdxLookAndFeelFlat = '平面';
  sdxLookAndFeelStandard = '标准';
  sdxLookAndFeelUltraFlat = '超平面';    

  sdxViewTab = '视图';
  sdxBehaviorsTab = '行为';
  sdxPreviewTab = '预览';
  sdxCardsTab = '卡片';

  sdxFormatting = '格式';
  sdxLookAndFeel = '外观';
  sdxLevelCaption = '标题(&C)';
  sdxFilterBar = '过滤栏(&F)';  
  sdxRefinements = '修正';
  sdxProcessSelection = '处理选择(&S)';
  sdxProcessExactSelection = '处理精确选择(&X)';
  sdxExpanding = '展开';
  sdxGroups = '组(&G)';
  sdxDetails = '详细信息(&D)';
  sdxStartFromActiveDetails = '从当前细节开始';
  sdxOnlyActiveDetails = '仅含当前细节';
  sdxVisible = '可见(&V)';
  sdxPreviewAutoHeight = '自动高度(&U)';
  sdxPreviewMaxLineCount = '最大行数(&M): ';
  sdxSizes = '大小';
  sdxKeepSameWidth = '保持同样宽度(&K)';
  sdxKeepSameHeight = '保持同样高度(&H)';
  sdxFraming = '框架';  
  sdxSpacing = '间隔';
  sdxShadow = '阴影';
  sdxDepth = '浓度(&D):';
  sdxPosition = '位置(&P)';
  sdxPositioning = '位置';
  sdxHorizontal = '水平(&O):';
  sdxVertical = '垂直(&E):';

  sdxSummaryFormat = '计数 = 0';

  sdxCannotUseOnEveryPageMode =
    '不能使用OnEveryPage模式'+ #13#10 +
    #13#10 +
    '你应该' + #13#10 +
    '  - 折叠所有主记录' + #13#10 +
    '  - 切换到"行为"选项卡关闭非循环"选项';

  sdxIncorrectBandHeadersState = 
    '不能使用BandHeaders OnEveryPage模式' + #13#10 +
    #13#10 +
    '你应该:' + #13#10 +  
    '  - 设置标题为OnEveryPage的选项为开' + #13#10 + 
    '  - 设置可见选项为关';
  sdxIncorrectHeadersState = 
    '不能使用也没OnEveryPage模式' + #13#10 +
    #13#10 +
    '你应该:' + #13#10 +  
    '  - 设置标题并绑定OnEveryPage选择为开' + #13#10 +
    '  - 设置标题并绑定可见属性为关';
  sdxIncorrectFootersState = 
   '不能使用页脚OnEveryPage模式' + #13#10 +
    #13#10 +
    '你应该:' + #13#10 +
    '  - 设置过滤栏OnEveryPage选项为开' + #13#10 +
    '  - 设置过滤栏可见选项为关';

  sdxCharts = '图表';
  
  { PS 3 }

  sdxTPicture = 'TPicture';
  sdxCopy = '复制(&C)';
  sdxSave = '保存(&S)...';
  sdxBaseStyle = '基本风格';
  
  sdxComponentAlreadyExists = '名为"%s"的组件已存在';
  sdxInvalidComponentName = '"%s"不是一个有效的组件名称';
  
  { shapes } 
 
  sdxRectangle = '矩形';
  sdxSquare = '正方形';
  sdxEllipse = '椭圆';
  sdxCircle = '圆';
  sdxRoundRect = '圆角矩形';
  sdxRoundSquare = '圆角正方形';

  { standard pattern names}
    
  sdxHorizontalFillPattern = '水平';
  sdxVerticalFillPattern = '垂直';
  sdxFDiagonalFillPattern = '对角线';
  sdxBDiagonalFillPattern = '对角线';
  sdxCrossFillPattern = '交叉';
  sdxDiagCrossFillPattern = '反交叉';
  
  { explorer }
                                                             
  sdxCyclicIDReferences = '循环ID引用 %s 与 %s';
  sdxLoadReportDataToFileTitle = '打开报表';
  sdxSaveReportDataToFileTitle = '另存报表';
  sdxInvalidExternalStorage = '非法的外部存储';
  sdxLinkIsNotIncludedInUsesClause = 
    'ReportFile包含ReportLink "%0:s"' + #13#10 + 
    '单元和"%0:s"的声明必须包含在uses子句中';
  sdxInvalidStorageVersion = '非法的存储版本: %d';
  sdxPSReportFiles = '报表文件';
  sdxReportFileLoadError = 
    '不能读取报表文件"%s"。' + #13#10 + 
    '文件已损坏，或已由另一个用户或应用程序锁定。' + #13#10 + 
    #13#10 +
    '将还原原始报表。';
  
  sdxNone = '(无)';
  sdxReportDocumentIsCorrupted = '(文件不是报表文件或已损坏)';
  
  sdxCloseExplorerHint = '关闭浏览器';
  sdxExplorerCaption = '资源管理器';
  sdxExplorerRootFolderCaption = '根';
  sdxNewExplorerFolderItem = '新建文件夹';
  sdxCopyOfItem = '副本 ';
  sdxReportExplorer = '报表浏览器';
                                
  sdxDataLoadErrorText = '不能加载报表数据';
  sdxDBBasedExplorerItemDataLoadError = 
    '不能加载报表数据。' + #13#10 + 
    '数据已损坏或被锁定';
  sdxFileBasedExplorerItemDataLoadError = 
    '不能加载报表数据。' + #13#10 + 
    '文件已损坏，或已由另一个用户或应用程序锁定';
  sdxDeleteNonEmptyFolderMessageText = '文件夹"%s"不是空的。是否仍要删除？';
  sdxDeleteFolderMessageText = '删除文件夹"%s" ？';
  sdxDeleteItemMessageText = '删除项"%s" ？';
  sdxCannotRenameFolderText = '无法重命名文件夹"%s"。名称"%s"的文件夹已经存在。请指定一个不同的名称。';
  sdxCannotRenameItemText = '无法重命名项目"%s"。具有名称"%s"的项目已存在。请指定一个不同的名称。';
  sdxOverwriteFolderMessageText = 
    '文件夹"%s"已包含名为"%s"的文件夹。' + #13#10 + 
    #13#10 + 
    '如果现有的文件夹中有同名项' + #13#10 + 
    '移动或复制将会替换。你还？' + #13#10 +
    '要移动或复制该文件夹吗？';
  sdxOverwriteItemMessageText = 
    '文件夹"%s"已包含项目"%s"。' + #13#10 + 
    #13#10 + 
    '您想覆盖现有的项目吗？';
  sdxSelectNewRoot = '选择报表保存的根路径';
  sdxInvalidFolderName = '无效文件夹名称"%s"';
  sdxInvalidReportName = '无效报表名称"%s"';
  
  sdxExplorerBar = '资源管理器';

  sdxMenuFileSave = '保存(&S)';
  sdxMenuFileSaveAs = '另存为(&A)...';
  sdxMenuFileLoad = '读取(&L)';
  sdxMenuFileClose = '关闭(&N)';
  sdxHintFileSave = '保存报表';
  sdxHintFileSaveAs = '另存报表';
  sdxHintFileLoad = '打开报表';
  sdxHintFileClose = '关闭报表';
  
  sdxMenuExplorer = '资源管理器(&X)';
  sdxMenuExplorerCreateFolder = '创建文件夹(&F)';
  sdxMenuExplorerDelete = '删除(&D)...';
  sdxMenuExplorerRename = '重命名(&M)';
  sdxMenuExplorerProperties = '属性(&P)...';
  sdxMenuExplorerRefresh = '刷新';
  sdxMenuExplorerChangeRootPath = '设置根路径...';
  sdxMenuExplorerSetAsRoot = '设为根路径';
  sdxMenuExplorerGoToUpOneLevel = '上一层';

  sdxHintExplorerCreateFolder = '新建文件夹';
  sdxHintExplorerDelete = '删除';
  sdxHintExplorerRename = '重命名';
  sdxHintExplorerProperties = '属性';
  sdxHintExplorerRefresh = '刷新';
  sdxHintExplorerChangeRootPath = '设置根路径';
  sdxHintExplorerSetAsRoot = '设当前路径为根路径';
  sdxHintExplorerGoToUpOneLevel = '上一层';
  
  sdxMenuViewExplorer = '资源管理器(&X)';
  sdxHintViewExplorer = '显示报表浏览器';

  sdxSummary = '概要';
  sdxCreator = '作者(&R):';
  sdxCreationDate = '已创建(&D):';
 
  sdxMenuViewThumbnails = '缩略图(&U)';
  sdxMenuThumbnailsLarge = '大缩略图(&L)';
  sdxMenuThumbnailsSmall = '小缩略图(&S)';
  
  sdxHintViewThumbnails = '显示缩略图';
  sdxHintThumbnailsLarge = '切换为大缩略图';
  sdxHintThumbnailsSmall = '切换为小缩略图';
    
  sdxMenuFormatTitle = '标题(&I)...';
  sdxHintFormatTitle = '格式化报表标题';
  sdxMenuFormatFootnotes = '脚注…(&N)...';
  sdxHintFormatFootnotes = '报表脚注格式…...';

  sdxHalf = '一半';
  sdxPredefinedFunctions = ' 预定义函数 '; // dxPgsDlg.pas
  sdxZoomParameters = ' 缩放(&P) ';          // dxPSPrvwOpt.pas

  sdxWrapData = '自动折行(&W)';

  sdxMenuShortcutExplorer = '资源管理器';  
  sdxExplorerToolBar = '资源管理器';

  sdxMenuShortcutThumbnails = '缩略图';

  { Ribbon Print Preview Window }

  sdxRibbonPrintPreviewClosePrintPreview = '关闭打印预览';
  sdxRibbonPrintPreviewGroupFormat = '格式';
  sdxRibbonPrintPreviewGroupInsertName = '名称';
  sdxRibbonPrintPreviewGroupInsertPageNumber = '页码';
  sdxRibbonPrintPreviewGroupNavigation = '导航';
  sdxRibbonPrintPreviewGroupOutput = '输出';
  sdxRibbonPrintPreviewGroupParts = '部分';
  sdxRibbonPrintPreviewGroupReport = '报表';
  sdxRibbonPrintPreviewGroupZoom = '缩放';
  sdxRibbonPrintPreviewPagesSubItem = '页';
{ TreeView New}
  
  sdxButtons = '按钮';
  
  { ListView }

  sdxBtnHeadersFont = '标题字体(&H)...';
  sdxHeadersTransparent = '标题透明(&H)';
  sdxHintListViewDesignerMessage = ' 在详细信息视图，只考虑到正在采取大多数选项';
  sdxColumnHeaders = '列标题(&C)';
  
  { Group LookAndFeel Names }

  sdxReportGroupNullLookAndFeel = '空';
  sdxReportGroupStandardLookAndFeel = '标准';
  sdxReportGroupOfficeLookAndFeel = '办公室';  
  sdxReportGroupWebLookAndFeel = '网页';

  { Layout }

  sdxLayoutGroupDefaultCaption = '分组布局';
  sdxLayoutItemDefaultCaption = '布局项目';
  sdxTabs = '标签页';
  sdxUnwrapTabs = '不换行的标签页(&U)';
  sdxActiveTabToTop = '在顶部显示活动标签页';
  sdxBehaviorsGroups = '分组';
  sdxSkipEmptyGroups = '跳过空分组';
  sdxExpandedGroups = '展开组';

  { Designer Previews}

  { Localize if you want (they are used inside FormatReport dialog -> ReportPreview) }
    
  sdxCarManufacturerName5 = '戴姆勒克莱斯勒 AG';
  sdxCarManufacturerCountry5 = '德国';
  sdxCarModel5 = 'Maybach 62';

  sdxLuxurySedans = 'Luxury Sedans';
  sdxCarManufacturer = '制造商';
  sdxCarModel = '型号';
  sdxCarEngine = '引擎';
  sdxCarTransmission = '传输';
  sdxCarTires = '轮胎';
  sdx760V12Manufacturer = '宝马';
  sdx760V12Model = '760Li V12';
  sdx760V12Engine = '6.0L DOHC V12 438 HP 48V DI Valvetronic 12-cylinder engine with 6.0-liter displacement, dual overhead cam valvetrain';
  sdx760V12Transmission = 'Elec 6-Speed Automatic w/Steptronic';
  sdx760V12Tires = 'P245/45R19 Fr - P275/40R19 Rr Performance. Low Profile tires with 245mm width, 19.0" rim';
      
  { Styles }

  sdxBandBackgroundStyle = 'BandBackground';
  sdxBandHeaderStyle = 'BandHeader';
  sdxCaptionStyle = '标题';
  sdxCardCaptionRowStyle = '卡片标题行';
  sdxCardRowCaptionStyle = '卡片行标题';
  sdxCategoryStyle = '类别';
  sdxContentStyle = '内容';
  sdxContentEvenStyle = '偶数行';
  sdxContentOddStyle = '奇数行';
  sdxFilterBarStyle = '过滤栏';
  sdxFooterStyle = '页脚';
  sdxFooterRowStyle = '页脚行';
  sdxGroupStyle = '分组';
  sdxHeaderStyle = '页眉';
  sdxIndentStyle = '缩进';
  sdxPreviewStyle = '预览';
  sdxSelectionStyle = '选择';

  sdxStyles = '样式';
  sdxStyleSheets = '样式表';
  sdxBtnTexture = '纹理(&T)...';
  sdxBtnTextureClear = '清空(&E)';
  sdxBtnColor = '颜色(&L)...';
  sdxBtnSaveAs = '另存为(&A)...';
  sdxBtnRename = '重命名(&M)...';
  
  sdxLoadBitmapDlgTitle = '打开纹理';
  
  sdxDeleteStyleSheet = '删除样式表"%s"？';
  sdxUnnamedStyleSheet = '未命名';
  sdxCreateNewStyleQueryNamePrompt = '输入新样式表名: ';
  sdxStyleSheetNameAlreadyExists = '样式表"%s"已存在';

  sdxCannotLoadImage = '不能删除图像"%s"';
  sdxUseNativeStyles = '使用本地样式(&U)';
  sdxSuppressBackgroundBitmaps = '压缩背景纹理(&S)';
  sdxConsumeSelectionStyle = '忽略选区样式';
  
  { Grid4 new }

  sdxSize = '尺寸';
  sdxLevels = '层级';
  sdxUnwrap = '自动折行(&U)';
  sdxUnwrapTopLevel = '禁止顶层自动折行(&W)';
  sdxRiseActiveToTop = '活动层级显示在顶部';
  sdxCannotUseOnEveryPageModeInAggregatedState = 
    '不能使用OnEveryPage模式'+ #13#10 + 
    '当执行在聚合模式中时';

  sdxPagination = '分页';
  sdxByBands = '带区';
  sdxByColumns = '列';
  sdxByRows = '行(&R)';
  sdxByTopLevelGroups = '按顶层分组'; 
  sdxOneGroupPerPage = '每页一组'; 

  sdxSkipEmptyViews = '跳过空视图';

  {* For those who will translate *}
  {* You should also check "sdxCannotUseOnEveryPageMode" resource string - see above *}
  {* It was changed to "- Toggle "Unwrap" Option off on "Behaviors" Tab"*}
   
  { TL 4 }
  sdxBorders = '边框';
  sdxExplicitlyExpandNodes = '显式扩展节点';
  sdxNodes = '节点(&N)';
  sdxSeparators = '分隔';
  sdxThickness = '宽度:';
  sdxTLIncorrectHeadersState = 
    '不能使用也没OnEveryPage模式' + #13#10 + 
    #13#10 +
    '你应该:' + #13#10 +  
    '  - 设置带区为OnEveryPage的选项为开' + #13#10 +
    '  - 设置带区可见选项为关';

  { cxVerticalGrid }

  sdxRows = '行数(&R)'; 

  sdxMultipleRecords = '多条记录(&M)';
  sdxBestFit = '自适应(&B)';
  sdxKeepSameRecordWidths = '记录等宽(&K)';
  sdxWrapRecords = '记录折行(&W)';

  sdxByWrapping = '通过换行(&W)';
  sdxOneWrappingPerPage = '每页一个换行(&O)';

  {new in 3.01}
  sdxCurrentRecord = '当前记录';
  sdxLoadedRecords = '加载记录';
  sdxAllRecords = '所有记录';
  
  { Container Designer }
  
  sdxPaginateByControlDetails = '控件详细信息';
  sdxPaginateByControls = '控件';
  sdxPaginateByGroups = '分组';
  sdxPaginateByItems = '项';
  
  sdxControlsPlace = '控件的地方';
  sdxExpandHeight = '展开高度';
  sdxExpandWidth = '展开宽度';
  sdxShrinkHeight = '收缩高度';
  sdxShrinkWidth = '收缩宽度';
  
  sdxCheckAll = '全选(&A)';
  sdxCheckAllChildren = '检查所有子项(&C)';
  sdxControlsTab = '控件';
  sdxExpandAll = '全部展开(&X)';
  sdxHiddenControlsTab = '可用控件';
  sdxReportLinksTab = '总设计师';
  sdxAvailableLinks = '可用链接(&A):';
  sdxAggregatedLinks = '聚合链接(&G):';
  sdxTransparents = '透明';
  sdxUncheckAllChildren = '取消选中所有的子项(&A)';
  
  sdxRoot = '根(&R)';
  sdxRootBorders = '根边框(&B)';
  sdxControls = '控件(&C)';
  sdxContainers = '容器(&O)';

  sdxHideCustomContainers = '隐藏自定义容器(&H)';

  { General }
  
  // FileSize abbreviation

  sdxBytes = '字节';
  sdxKiloBytes = 'KB';
  sdxMegaBytes = 'MB';
  sdxGigaBytes = 'GB';

  // Misc.

  sdxThereIsNoPictureToDisplay = '无图片可显示';
  sdxInvalidRootDirectory = '文件夹"%s"不存在，是否继续选择 ？';
  sdxPressEscToCancel = '“Esc”键取消';
  sdxMenuFileRebuild = '重建报表(&R)';
  sdxBuildingReportStatusText = '生成报表 - “Esc”键取消';
  sdxPrintingReportStatusText = '打印报表 - “Esc”键取消';
  
  sdxBuiltIn = '[内建]';
  sdxUserDefined = '[自定义]';
  sdxNewStyleRepositoryWasCreated = '新的样式库"%s"已创建并分配';

  { new in PS 3.1}
  sdxLineSpacing = '行间距(&L):';
  sdxTextAlignJustified = '对齐';
  sdxSampleText = '示例 Sample Text';
  
  sdxCardsRows = '卡片(&C)';
  sdxTransparentRichEdits = 'RichEdit内容透明(&R)';

  sdxIncorrectFilterBarState = 
    '不能使用过滤栏OnEveryPage模式' + #13#10 +
    #13#10 +
    '你应该:' + #13#10 +  
    '  - 设置标题为OnEveryPage的选项为开' + #13#10 +
    '  - 设置可见选项为关';
  sdxIncorrectBandHeadersState2 = 
    '不能使用BandHeaders OnEveryPage模式' + #13#10 +
    #13#10 +
    '你应该:' + #13#10 +  
    '  - 设置标题和过滤栏OnEveryPage选择为开' + #13#10 + 
    '  - 设置标题和过滤栏可见属性为关';
  sdxIncorrectHeadersState2 = 
    '不能使用也没OnEveryPage模式' + #13#10 +
    #13#10 +
    '你应该:' + #13#10 +  
    '  -设置标题、过滤栏和带区OnEveryPage选项为开' + #13#10 +
    '  - 设置标题、过滤栏和带区可见选择为关';

 { new in PS 3.2}   
  sdxAvailableReportLinks = '可用报表链接';
  sdxBtnRemoveInconsistents = '删除不必要';
  sdxColumnHeadersOnEveryPage = '列标题(&H)';
  sdxRowHeadersOnEveryPage = '行标题';

 { Scheduler }   

  sdxNotes = '注意';
  sdxTaskPad = '任务板';
  sdxPrimaryTimeZone = '主要';
  sdxSecondaryTimeZone = '次要';

  sdxDay = '天';
  sdxWeek = '周';
  sdxMonth = '月份';

  sdxSchedulerSchedulerHeader = '调度计划标题';
  sdxSchedulerContent = '内容';
  sdxSchedulerDateNavigatorContent = 'DateNavigator 内容';
  sdxSchedulerDateNavigatorHeader = 'DateNavigator 标题';
  sdxSchedulerDayHeader = '日标题';
  sdxSchedulerEvent = '事件';
  sdxSchedulerResourceHeader = '资源标题';
  sdxSchedulerNotesAreaBlank = '备注区域（空白）';
  sdxSchedulerNotesAreaLined = '备注区域（分行）';
  sdxSchedulerTaskPad = '任务板';
  sdxSchedulerTimeRuler = '时间标尺';
  
  sdxPrintStyleNameDaily = '日';
  sdxPrintStyleNameWeekly = '周';
  sdxPrintStyleNameMonthly = '月';
  sdxPrintStyleNameDetails = '详细信息';
  sdxPrintStyleNameMemo = '备注';
  sdxPrintStyleNameTrifold = '三栏';
  
  sdxPrintStyleCaptionDaily = '每日样式';
  sdxPrintStyleCaptionWeekly = '每周样式';
  sdxPrintStyleCaptionMonthly = '每月样式';
  sdxPrintStyleCaptionDetails = '详细日历样式';
  sdxPrintStyleCaptionMemo = '备注样式';
  sdxPrintStyleCaptionTimeLine = '时间线样式';
  sdxPrintStyleCaptionTrifold = '三栏样式';
  sdxPrintStyleCaptionYearly = '每年样式';
  sdxPrintStyleShowEventImages = '显示活动图片';
  sdxPrintStyleShowResourceImages = '显示资源图像';

  sdxTabPrintStyles = '打印样式';

  sdxPrintStyleDontPrintWeekEnds = '不打印周末(&D)';
  sdxPrintStyleWorkTimeOnly = '仅在工作时间(&W)';

  sdxPrintStyleInclude = '包含:';
  sdxPrintStyleIncludeTaskPad = '任务板(&P)';
  sdxPrintStyleIncludeNotesAreaBlank = '备注区域（空白）(&B)';
  sdxPrintStyleIncludeNotesAreaLined = '备注区域（分行）(&L)';
  sdxPrintStyleLayout = '布局(&L):';
  sdxPrintStylePrintFrom = '打印从(&F):';
  sdxPrintStylePrintTo = '打印到(&T):';

  sdxPrintStyleDailyLayout1PPD = '1 页/天';
  sdxPrintStyleDailyLayout2PPD = '2 页/天';

  sdxPrintStyleWeeklyArrange = '排列(&A):';
  sdxPrintStyleWeeklyArrangeT2B = '从上到下';
  sdxPrintStyleWeeklyArrangeL2R = '从左到右';
  sdxPrintStyleWeeklyLayout1PPW = '1 页/周';
  sdxPrintStyleWeeklyLayout2PPW = '2 页/周';
  sdxPrintStyleWeeklyDaysLayout   = '天布局(&D):';
  sdxPrintStyleWeeklyDaysLayoutTC = '两列';
  sdxPrintStyleWeeklyDaysLayoutOC = '一列';

  sdxPrintStyleMemoStartEachItemOnNewPage = '启动新一页上的每个项目';
  sdxPrintStyleMemoPrintOnlySelectedEvents = '仅打印选定的事件';

  sdxPrintStyleMonthlyLayout1PPM = '1 页/月';
  sdxPrintStyleMonthlyLayout2PPM = '2 页/月';
  sdxPrintStyleMonthlyPrintExactly1MPP = '打印每页一个月的(&E)';

  sdxPrintStyleTrifoldSectionModeDailyCalendar = '日历';
  sdxPrintStyleTrifoldSectionModeWeeklyCalendar = '周历';
  sdxPrintStyleTrifoldSectionModeMonthlyCalendar = '月历';
  sdxPrintStyleTrifoldSectionModeTaskPad = '任务板';
  sdxPrintStyleTrifoldSectionModeNotesBlank = '备注 (空白)';
  sdxPrintStyleTrifoldSectionModeNotesLined = '备注 (分行)';
  sdxPrintStyleTrifoldSectionLeft = '左侧的部分(&L):';
  sdxPrintStyleTrifoldSectionMiddle = '中间的部分(&M):';
  sdxPrintStyleTrifoldSectionRight = '右侧部分(&R):';

  sdxPrintStyleMonthPerPage = '月/页(&M):';
  sdxPrintStyleYearly1MPP  = '1 个月/页';
  sdxPrintStyleYearly2MPP  = '2 个月/页';
  sdxPrintStyleYearly3MPP  = '3 个月/页';
  sdxPrintStyleYearly4MPP  = '4 个月/页';
  sdxPrintStyleYearly6MPP  = '6 个月/页';
  sdxPrintStyleYearly12MPP = '12 个月/页';

  sdxPrintStylePrimaryPageScalesOnly = '仅主页标尺';
  sdxPrintStylePrimaryPageHeadersOnly = '仅主页标题';

  sdxPrintStyleDetailsStartNewPageEach = '开始一个新页每个:';

  sdxSuppressContentColoration = '禁止内容的着色(&C)';
  sdxOneResourcePerPage = '每页一个资源(&R)';

  sdxPrintRanges = '打印范围';
  sdxPrintRangeStart = '开始(&S):';
  sdxPrintRangeEnd = '结束(&E):';
  sdxHideDetailsOfPrivateAppointments = '隐藏私有约会的细节(&H)';
  sdxResourceCountPerPage = '资源/页(&R):';

  sdxSubjectLabelCaption = '主题:';
  sdxLocationLabelCaption = '位置:';
  sdxStartLabelCaption = '开始:';
  sdxFinishLabelCaption = '结束:';
  sdxShowTimeAsLabelCaption = '时间显示为:';
  sdxRecurrenceLabelCaption = '重复:';
  sdxRecurrencePatternLabelCaption = '循环模式:';

  //messages
  sdxSeeAboveMessage = '请参见上面';
  sdxAllDayMessage = '所有日期';
  sdxContinuedMessage = '继续';
  sdxShowTimeAsFreeMessage = '释放';
  sdxShowTimeAsTentativeMessage = '暂定';
  sdxShowTimeAsOutOfOfficeMessage = '外出';

  sdxRecurrenceNoneMessage = '(无)';
  scxRecurrenceDailyMessage = '日';
  scxRecurrenceWeeklyMessage = '周';
  scxRecurrenceMonthlyMessage = '月';
  scxRecurrenceYearlyMessage = '年';

  //error messages
  sdxInconsistentTrifoldStyle = '三栏式需要至少一个日历部分。 ' +
    '选择每日、 每周或每月的日历，在选项下的部分之一。';
  sdxBadTimePrintRange = '打印时间不是有效的。开始时间必须在结束时间的前面。';
  sdxBadDatePrintRange = '结束框中的日期不能在开始框日期前。';
  sdxCannotPrintNoSelectedItems = '不能打印没有选定项。选择一个项目，然后尝试再次打印。';
  sdxCannotPrintNoItemsAvailable = '没有可用的指定打印范围内的项目。';

  { PivotGrid }

  sdxColumnFields = '列字段(&C)';
  sdxDataFields   = '数据字段(&D)';
  sdxFiterFields  = '过滤字段(&F)';
  sdxPrefilter = '预过滤器(&P)';
  sdxRowFields    = '行字段(&R)';

  sdxAutoColumnsExpand = '自动展开列(&U)';
  sdxAutoRowsExpand = '自动展开行(&R)';

  // styles
  sdxPivotGridColumnHeader = '列头';
  sdxPivotGridContent = '内容';
  sdxPivotGridFieldHeader  = '字段标题';
  sdxPivotGridHeaderBackground = '标题背景';
  sdxPivotGridRowHeader = '行标题';
  sdxPivotGridPrefilter = '预过滤器';

  // PivotPreview fields
  sdxUnitPrice = '单位价格';
  sdxCarName = '车名';
  sdxQuantity = '数量';
  sdxPaymentAmount = '付款金额';
  sdxPurchaseQuarter = '购买季度';
  sdxPurchaseMonth = '购买月份';
  sdxPaymentType   = '付款方式';
  sdxCompanyName   = '公司名称';


implementation

uses
  dxCore;

procedure AddResourceStringsPart1(AProduct: TdxProductResourceStrings);
begin
  AProduct.Add('sdxBtnOK', @sdxBtnOK);
  AProduct.Add('sdxBtnOKAccelerated', @sdxBtnOKAccelerated);
  AProduct.Add('sdxBtnCancel', @sdxBtnCancel);
  AProduct.Add('sdxBtnClose', @sdxBtnClose);
  AProduct.Add('sdxBtnApply', @sdxBtnApply);
  AProduct.Add('sdxBtnHelp', @sdxBtnHelp);
  AProduct.Add('sdxBtnFix', @sdxBtnFix);
  AProduct.Add('sdxBtnNew', @sdxBtnNew);
  AProduct.Add('sdxBtnIgnore', @sdxBtnIgnore);
  AProduct.Add('sdxBtnYes', @sdxBtnYes);
  AProduct.Add('sdxBtnNo', @sdxBtnNo);
  AProduct.Add('sdxBtnEdit', @sdxBtnEdit);
  AProduct.Add('sdxBtnReset', @sdxBtnReset);
  AProduct.Add('sdxBtnAdd', @sdxBtnAdd);
  AProduct.Add('sdxBtnAddComposition', @sdxBtnAddComposition);
  AProduct.Add('sdxBtnDefault', @sdxBtnDefault);
  AProduct.Add('sdxBtnDelete', @sdxBtnDelete);
  AProduct.Add('sdxBtnDescription', @sdxBtnDescription);
  AProduct.Add('sdxBtnCopy', @sdxBtnCopy);
  AProduct.Add('sdxBtnYesToAll', @sdxBtnYesToAll);
  AProduct.Add('sdxBtnRestoreDefaults', @sdxBtnRestoreDefaults);
  AProduct.Add('sdxBtnRestoreOriginal', @sdxBtnRestoreOriginal);
  AProduct.Add('sdxBtnTitleProperties', @sdxBtnTitleProperties);
  AProduct.Add('sdxBtnFootnoteProperties', @sdxBtnFootnoteProperties);
  AProduct.Add('sdxBtnProperties', @sdxBtnProperties);
  AProduct.Add('sdxBtnNetwork', @sdxBtnNetwork);
  AProduct.Add('sdxBtnBrowse', @sdxBtnBrowse);
  AProduct.Add('sdxBtnPageSetup', @sdxBtnPageSetup);
  AProduct.Add('sdxBtnPrintPreview', @sdxBtnPrintPreview);
  AProduct.Add('sdxBtnPreview', @sdxBtnPreview);
  AProduct.Add('sdxBtnPrint', @sdxBtnPrint);
  AProduct.Add('sdxBtnOptions', @sdxBtnOptions);
  AProduct.Add('sdxBtnStyleOptions', @sdxBtnStyleOptions);
  AProduct.Add('sdxBtnDefinePrintStyles', @sdxBtnDefinePrintStyles);
  AProduct.Add('sdxBtnPrintStyles', @sdxBtnPrintStyles);
  AProduct.Add('sdxBtnBackground', @sdxBtnBackground);
  AProduct.Add('sdxBtnShowToolBar', @sdxBtnShowToolBar);
  AProduct.Add('sdxBtnDesign', @sdxBtnDesign);
  AProduct.Add('sdxBtnMoveUp', @sdxBtnMoveUp);
  AProduct.Add('sdxBtnMoveDown', @sdxBtnMoveDown);
  AProduct.Add('sdxBtnMoreColors', @sdxBtnMoreColors);
  AProduct.Add('sdxBtnFillEffects', @sdxBtnFillEffects);
  AProduct.Add('sdxBtnNoFill', @sdxBtnNoFill);
  AProduct.Add('sdxBtnAutomatic', @sdxBtnAutomatic);
  AProduct.Add('sdxBtnNone', @sdxBtnNone);
  AProduct.Add('sdxBtnOtherTexture', @sdxBtnOtherTexture);
  AProduct.Add('sdxBtnInvertColors', @sdxBtnInvertColors);
  AProduct.Add('sdxBtnSelectPicture', @sdxBtnSelectPicture);
  AProduct.Add('sdxEditReports', @sdxEditReports);
  AProduct.Add('sdxComposition', @sdxComposition);
  AProduct.Add('sdxReportTitleDlgCaption', @sdxReportTitleDlgCaption);
  AProduct.Add('sdxReportFootnotesDlgCaption', @sdxReportFootnotesDlgCaption);
  AProduct.Add('sdxMode', @sdxMode);
  AProduct.Add('sdxText', @sdxText);
  AProduct.Add('sdxProperties', @sdxProperties);
  AProduct.Add('sdxAdjustOnScale', @sdxAdjustOnScale);
  AProduct.Add('sdxTitleModeNone', @sdxTitleModeNone);
  AProduct.Add('sdxTitleModeOnEveryTopPage', @sdxTitleModeOnEveryTopPage);
  AProduct.Add('sdxTitleModeOnFirstPage', @sdxTitleModeOnFirstPage);
  AProduct.Add('sdxFootnotesModeNone', @sdxFootnotesModeNone);
  AProduct.Add('sdxFootnotesModeOnEveryBottomPage', @sdxFootnotesModeOnEveryBottomPage);
  AProduct.Add('sdxFootnotesModeOnLastPage', @sdxFootnotesModeOnLastPage);
  AProduct.Add('sdxEditDescription', @sdxEditDescription);
  AProduct.Add('sdxRename', @sdxRename);
  AProduct.Add('sdxSelectAll', @sdxSelectAll);
  AProduct.Add('sdxAddReport', @sdxAddReport);
  AProduct.Add('sdxAddAndDesignReport', @sdxAddAndDesignReport);
  AProduct.Add('sdxNewCompositionCaption', @sdxNewCompositionCaption);
  AProduct.Add('sdxName', @sdxName);
  AProduct.Add('sdxCaption', @sdxCaption);
  AProduct.Add('sdxAvailableSources', @sdxAvailableSources);
  AProduct.Add('sdxOnlyComponentsInActiveForm', @sdxOnlyComponentsInActiveForm);
  AProduct.Add('sdxOnlyComponentsWithoutLinks', @sdxOnlyComponentsWithoutLinks);
  AProduct.Add('sdxItemName', @sdxItemName);
  AProduct.Add('sdxItemDescription', @sdxItemDescription);
  AProduct.Add('sdxConfirmDeleteItem', @sdxConfirmDeleteItem);
  AProduct.Add('sdxAddItemsToComposition', @sdxAddItemsToComposition);
  AProduct.Add('sdxHideAlreadyIncludedItems', @sdxHideAlreadyIncludedItems);
  AProduct.Add('sdxAvailableItems', @sdxAvailableItems);
  AProduct.Add('sdxItems', @sdxItems);
  AProduct.Add('sdxEnable', @sdxEnable);
  AProduct.Add('sdxOptions', @sdxOptions);
  AProduct.Add('sdxShow', @sdxShow);
  AProduct.Add('sdxPaintItemsGraphics', @sdxPaintItemsGraphics);
  AProduct.Add('sdxDescription', @sdxDescription);
  AProduct.Add('sdxNewReport', @sdxNewReport);
  AProduct.Add('sdxOnlySelected', @sdxOnlySelected);
  AProduct.Add('sdxExtendedSelect', @sdxExtendedSelect);
  AProduct.Add('sdxIncludeFixed', @sdxIncludeFixed);
  AProduct.Add('sdxFonts', @sdxFonts);
  AProduct.Add('sdxBtnFont', @sdxBtnFont);
  AProduct.Add('sdxBtnEvenFont', @sdxBtnEvenFont);
  AProduct.Add('sdxBtnOddFont', @sdxBtnOddFont);
  AProduct.Add('sdxBtnFixedFont', @sdxBtnFixedFont);
  AProduct.Add('sdxBtnGroupFont', @sdxBtnGroupFont);
  AProduct.Add('sdxBtnChangeFont', @sdxBtnChangeFont);
  AProduct.Add('sdxFont', @sdxFont);
  AProduct.Add('sdxOddFont', @sdxOddFont);
  AProduct.Add('sdxEvenFont', @sdxEvenFont);
  AProduct.Add('sdxPreviewFont', @sdxPreviewFont);
  AProduct.Add('sdxCaptionNodeFont', @sdxCaptionNodeFont);
  AProduct.Add('sdxGroupNodeFont', @sdxGroupNodeFont);
  AProduct.Add('sdxGroupFooterFont', @sdxGroupFooterFont);
  AProduct.Add('sdxHeaderFont', @sdxHeaderFont);
  AProduct.Add('sdxFooterFont', @sdxFooterFont);
  AProduct.Add('sdxBandFont', @sdxBandFont);
  AProduct.Add('sdxTransparent', @sdxTransparent);
  AProduct.Add('sdxFixedTransparent', @sdxFixedTransparent);
  AProduct.Add('sdxCaptionTransparent', @sdxCaptionTransparent);
  AProduct.Add('sdxGroupTransparent', @sdxGroupTransparent);
  AProduct.Add('sdxGraphicAsTextValue', @sdxGraphicAsTextValue);
  AProduct.Add('sdxColors', @sdxColors);
  AProduct.Add('sdxColor', @sdxColor);
  AProduct.Add('sdxOddColor', @sdxOddColor);
  AProduct.Add('sdxEvenColor', @sdxEvenColor);
  AProduct.Add('sdxPreviewColor', @sdxPreviewColor);
  AProduct.Add('sdxBandColor', @sdxBandColor);
  AProduct.Add('sdxLevelCaptionColor', @sdxLevelCaptionColor);
  AProduct.Add('sdxHeaderColor', @sdxHeaderColor);
  AProduct.Add('sdxGroupNodeColor', @sdxGroupNodeColor);
  AProduct.Add('sdxGroupFooterColor', @sdxGroupFooterColor);
  AProduct.Add('sdxFooterColor', @sdxFooterColor);
  AProduct.Add('sdxFixedColor', @sdxFixedColor);
  AProduct.Add('sdxGroupColor', @sdxGroupColor);
  AProduct.Add('sdxCaptionColor', @sdxCaptionColor);
  AProduct.Add('sdxGridLinesColor', @sdxGridLinesColor);
  AProduct.Add('sdxBands', @sdxBands);
  AProduct.Add('sdxLevelCaptions', @sdxLevelCaptions);
  AProduct.Add('sdxHeaders', @sdxHeaders);
  AProduct.Add('sdxFooters', @sdxFooters);
  AProduct.Add('sdxGroupFooters', @sdxGroupFooters);
  AProduct.Add('sdxPreview', @sdxPreview);
  AProduct.Add('sdxPreviewLineCount', @sdxPreviewLineCount);
  AProduct.Add('sdxAutoCalcPreviewLineCount', @sdxAutoCalcPreviewLineCount);
  AProduct.Add('sdxGrid', @sdxGrid);
  AProduct.Add('sdxNodesGrid', @sdxNodesGrid);
  AProduct.Add('sdxGroupFooterGrid', @sdxGroupFooterGrid);
  AProduct.Add('sdxStateImages', @sdxStateImages);
  AProduct.Add('sdxImages', @sdxImages);
  AProduct.Add('sdxTextAlign', @sdxTextAlign);
  AProduct.Add('sdxTextAlignHorz', @sdxTextAlignHorz);
  AProduct.Add('sdxTextAlignVert', @sdxTextAlignVert);
  AProduct.Add('sdxTextAlignLeft', @sdxTextAlignLeft);
  AProduct.Add('sdxTextAlignCenter', @sdxTextAlignCenter);
  AProduct.Add('sdxTextAlignRight', @sdxTextAlignRight);
  AProduct.Add('sdxTextAlignTop', @sdxTextAlignTop);
  AProduct.Add('sdxTextAlignVCenter', @sdxTextAlignVCenter);
  AProduct.Add('sdxTextAlignBottom', @sdxTextAlignBottom);
  AProduct.Add('sdxBorderLines', @sdxBorderLines);
  AProduct.Add('sdxHorzLines', @sdxHorzLines);
  AProduct.Add('sdxVertLines', @sdxVertLines);
  AProduct.Add('sdxFixedHorzLines', @sdxFixedHorzLines);
  AProduct.Add('sdxFixedVertLines', @sdxFixedVertLines);
  AProduct.Add('sdxFlatCheckMarks', @sdxFlatCheckMarks);
  AProduct.Add('sdxCheckMarksAsText', @sdxCheckMarksAsText);
  AProduct.Add('sdxRowAutoHeight', @sdxRowAutoHeight);
  AProduct.Add('sdxEndEllipsis', @sdxEndEllipsis);
  AProduct.Add('sdxDrawBorder', @sdxDrawBorder);
  AProduct.Add('sdxFullExpand', @sdxFullExpand);
  AProduct.Add('sdxBorderColor', @sdxBorderColor);
  AProduct.Add('sdxAutoNodesExpand', @sdxAutoNodesExpand);
  AProduct.Add('sdxExpandLevel', @sdxExpandLevel);
  AProduct.Add('sdxFixedRowOnEveryPage', @sdxFixedRowOnEveryPage);
  AProduct.Add('sdxDrawMode', @sdxDrawMode);
  AProduct.Add('sdxDrawModeStrict', @sdxDrawModeStrict);
  AProduct.Add('sdxDrawModeOddEven', @sdxDrawModeOddEven);
  AProduct.Add('sdxDrawModeChess', @sdxDrawModeChess);
  AProduct.Add('sdxDrawModeBorrow', @sdxDrawModeBorrow);
  AProduct.Add('sdx3DEffects', @sdx3DEffects);
  AProduct.Add('sdxUse3DEffects', @sdxUse3DEffects);
  AProduct.Add('sdxSoft3D', @sdxSoft3D);
  AProduct.Add('sdxBehaviors', @sdxBehaviors);
  AProduct.Add('sdxMiscellaneous', @sdxMiscellaneous);
  AProduct.Add('sdxOnEveryPage', @sdxOnEveryPage);
  AProduct.Add('sdxNodeExpanding', @sdxNodeExpanding);
  AProduct.Add('sdxSelection', @sdxSelection);
  AProduct.Add('sdxNodeAutoHeight', @sdxNodeAutoHeight);
  AProduct.Add('sdxTransparentGraphics', @sdxTransparentGraphics);
  AProduct.Add('sdxAutoWidth', @sdxAutoWidth);
  AProduct.Add('sdxDisplayGraphicsAsText', @sdxDisplayGraphicsAsText);
  AProduct.Add('sdxTransparentColumnGraphics', @sdxTransparentColumnGraphics);
  AProduct.Add('sdxBandsOnEveryPage', @sdxBandsOnEveryPage);
  AProduct.Add('sdxHeadersOnEveryPage', @sdxHeadersOnEveryPage);
  AProduct.Add('sdxFootersOnEveryPage', @sdxFootersOnEveryPage);
  AProduct.Add('sdxGraphics', @sdxGraphics);
  AProduct.Add('sdxOutOfResources', @sdxOutOfResources);
  AProduct.Add('sdxFileAlreadyExists', @sdxFileAlreadyExists);
  AProduct.Add('sdxConfirmOverWrite', @sdxConfirmOverWrite);
  AProduct.Add('sdxInvalidFileName', @sdxInvalidFileName);
  AProduct.Add('sdxRequiredFileName', @sdxRequiredFileName);
  AProduct.Add('sdxOutsideMarginsMessage', @sdxOutsideMarginsMessage);
  AProduct.Add('sdxOutsideMarginsMessage2', @sdxOutsideMarginsMessage2);
  AProduct.Add('sdxInvalidMarginsMessage', @sdxInvalidMarginsMessage);
  AProduct.Add('sdxInvalidMargins', @sdxInvalidMargins);
  AProduct.Add('sdxOutsideMargins', @sdxOutsideMargins);
  AProduct.Add('sdxThereAreNowItemsForShow', @sdxThereAreNowItemsForShow);
  AProduct.Add('sdxReportCellClassNotRegistered', @sdxReportCellClassNotRegistered);
  AProduct.Add('sdxPageBackground', @sdxPageBackground);
  AProduct.Add('sdxPenColor', @sdxPenColor);
  AProduct.Add('sdxFontColor', @sdxFontColor);
  AProduct.Add('sdxBrushColor', @sdxBrushColor);
  AProduct.Add('sdxHighLight', @sdxHighLight);
  AProduct.Add('sdxColorBlack', @sdxColorBlack);
  AProduct.Add('sdxColorDarkRed', @sdxColorDarkRed);
  AProduct.Add('sdxColorRed', @sdxColorRed);
  AProduct.Add('sdxColorPink', @sdxColorPink);
  AProduct.Add('sdxColorRose', @sdxColorRose);
  AProduct.Add('sdxColorBrown', @sdxColorBrown);
  AProduct.Add('sdxColorOrange', @sdxColorOrange);
  AProduct.Add('sdxColorLightOrange', @sdxColorLightOrange);
  AProduct.Add('sdxColorGold', @sdxColorGold);
  AProduct.Add('sdxColorTan', @sdxColorTan);
  AProduct.Add('sdxColorOliveGreen', @sdxColorOliveGreen);
  AProduct.Add('sdxColorDrakYellow', @sdxColorDrakYellow);
  AProduct.Add('sdxColorLime', @sdxColorLime);
  AProduct.Add('sdxColorYellow', @sdxColorYellow);
  AProduct.Add('sdxColorLightYellow', @sdxColorLightYellow);
  AProduct.Add('sdxColorDarkGreen', @sdxColorDarkGreen);
  AProduct.Add('sdxColorGreen', @sdxColorGreen);
  AProduct.Add('sdxColorSeaGreen', @sdxColorSeaGreen);
  AProduct.Add('sdxColorBrighthGreen', @sdxColorBrighthGreen);
  AProduct.Add('sdxColorLightGreen', @sdxColorLightGreen);
  AProduct.Add('sdxColorDarkTeal', @sdxColorDarkTeal);
  AProduct.Add('sdxColorTeal', @sdxColorTeal);
  AProduct.Add('sdxColorAqua', @sdxColorAqua);
  AProduct.Add('sdxColorTurquoise', @sdxColorTurquoise);
  AProduct.Add('sdxColorLightTurquoise', @sdxColorLightTurquoise);
  AProduct.Add('sdxColorDarkBlue', @sdxColorDarkBlue);
  AProduct.Add('sdxColorBlue', @sdxColorBlue);
  AProduct.Add('sdxColorLightBlue', @sdxColorLightBlue);
  AProduct.Add('sdxColorSkyBlue', @sdxColorSkyBlue);
  AProduct.Add('sdxColorPaleBlue', @sdxColorPaleBlue);
  AProduct.Add('sdxColorIndigo', @sdxColorIndigo);
  AProduct.Add('sdxColorBlueGray', @sdxColorBlueGray);
  AProduct.Add('sdxColorViolet', @sdxColorViolet);
  AProduct.Add('sdxColorPlum', @sdxColorPlum);
  AProduct.Add('sdxColorLavender', @sdxColorLavender);
  AProduct.Add('sdxColorGray80', @sdxColorGray80);
  AProduct.Add('sdxColorGray50', @sdxColorGray50);
  AProduct.Add('sdxColorGray40', @sdxColorGray40);
  AProduct.Add('sdxColorGray25', @sdxColorGray25);
  AProduct.Add('sdxColorWhite', @sdxColorWhite);
  AProduct.Add('sdxTexture', @sdxTexture);
  AProduct.Add('sdxPattern', @sdxPattern);
  AProduct.Add('sdxPicture', @sdxPicture);
  AProduct.Add('sdxForeground', @sdxForeground);
  AProduct.Add('sdxBackground', @sdxBackground);
  AProduct.Add('sdxSample', @sdxSample);
  AProduct.Add('sdxFEFCaption', @sdxFEFCaption);
  AProduct.Add('sdxPaintMode', @sdxPaintMode);
  AProduct.Add('sdxPaintModeCenter', @sdxPaintModeCenter);
  AProduct.Add('sdxPaintModeStretch', @sdxPaintModeStretch);
  AProduct.Add('sdxPaintModeTile', @sdxPaintModeTile);
  AProduct.Add('sdxPaintModeProportional', @sdxPaintModeProportional);
  AProduct.Add('sdxPatternGray5', @sdxPatternGray5);
  AProduct.Add('sdxPatternGray10', @sdxPatternGray10);
  AProduct.Add('sdxPatternGray20', @sdxPatternGray20);
  AProduct.Add('sdxPatternGray25', @sdxPatternGray25);
  AProduct.Add('sdxPatternGray30', @sdxPatternGray30);
  AProduct.Add('sdxPatternGray40', @sdxPatternGray40);
  AProduct.Add('sdxPatternGray50', @sdxPatternGray50);
  AProduct.Add('sdxPatternGray60', @sdxPatternGray60);
  AProduct.Add('sdxPatternGray70', @sdxPatternGray70);
  AProduct.Add('sdxPatternGray75', @sdxPatternGray75);
  AProduct.Add('sdxPatternGray80', @sdxPatternGray80);
  AProduct.Add('sdxPatternGray90', @sdxPatternGray90);
  AProduct.Add('sdxPatternLightDownwardDiagonal', @sdxPatternLightDownwardDiagonal);
  AProduct.Add('sdxPatternLightUpwardDiagonal', @sdxPatternLightUpwardDiagonal);
  AProduct.Add('sdxPatternDarkDownwardDiagonal', @sdxPatternDarkDownwardDiagonal);
  AProduct.Add('sdxPatternDarkUpwardDiagonal', @sdxPatternDarkUpwardDiagonal);
  AProduct.Add('sdxPatternWideDownwardDiagonal', @sdxPatternWideDownwardDiagonal);
  AProduct.Add('sdxPatternWideUpwardDiagonal', @sdxPatternWideUpwardDiagonal);
  AProduct.Add('sdxPatternLightVertical', @sdxPatternLightVertical);
  AProduct.Add('sdxPatternLightHorizontal', @sdxPatternLightHorizontal);
  AProduct.Add('sdxPatternNarrowVertical', @sdxPatternNarrowVertical);
  AProduct.Add('sdxPatternNarrowHorizontal', @sdxPatternNarrowHorizontal);
  AProduct.Add('sdxPatternDarkVertical', @sdxPatternDarkVertical);
  AProduct.Add('sdxPatternDarkHorizontal', @sdxPatternDarkHorizontal);
  AProduct.Add('sdxPatternDashedDownward', @sdxPatternDashedDownward);
  AProduct.Add('sdxPatternDashedUpward', @sdxPatternDashedUpward);
  AProduct.Add('sdxPatternDashedVertical', @sdxPatternDashedVertical);
  AProduct.Add('sdxPatternDashedHorizontal', @sdxPatternDashedHorizontal);
  AProduct.Add('sdxPatternSmallConfetti', @sdxPatternSmallConfetti);
  AProduct.Add('sdxPatternLargeConfetti', @sdxPatternLargeConfetti);
  AProduct.Add('sdxPatternZigZag', @sdxPatternZigZag);
  AProduct.Add('sdxPatternWave', @sdxPatternWave);
  AProduct.Add('sdxPatternDiagonalBrick', @sdxPatternDiagonalBrick);
  AProduct.Add('sdxPatternHorizantalBrick', @sdxPatternHorizantalBrick);
  AProduct.Add('sdxPatternWeave', @sdxPatternWeave);
  AProduct.Add('sdxPatternPlaid', @sdxPatternPlaid);
  AProduct.Add('sdxPatternDivot', @sdxPatternDivot);
  AProduct.Add('sdxPatternDottedGrid', @sdxPatternDottedGrid);
  AProduct.Add('sdxPatternDottedDiamond', @sdxPatternDottedDiamond);
  AProduct.Add('sdxPatternShingle', @sdxPatternShingle);
  AProduct.Add('sdxPatternTrellis', @sdxPatternTrellis);
  AProduct.Add('sdxPatternSphere', @sdxPatternSphere);
  AProduct.Add('sdxPatternSmallGrid', @sdxPatternSmallGrid);
  AProduct.Add('sdxPatternLargeGrid', @sdxPatternLargeGrid);
  AProduct.Add('sdxPatternSmallCheckedBoard', @sdxPatternSmallCheckedBoard);
  AProduct.Add('sdxPatternLargeCheckedBoard', @sdxPatternLargeCheckedBoard);
  AProduct.Add('sdxPatternOutlinedDiamond', @sdxPatternOutlinedDiamond);
  AProduct.Add('sdxPatternSolidDiamond', @sdxPatternSolidDiamond);
  AProduct.Add('sdxTextureNewSprint', @sdxTextureNewSprint);
  AProduct.Add('sdxTextureGreenMarble', @sdxTextureGreenMarble);
  AProduct.Add('sdxTextureBlueTissuePaper', @sdxTextureBlueTissuePaper);
  AProduct.Add('sdxTexturePapyrus', @sdxTexturePapyrus);
  AProduct.Add('sdxTextureWaterDroplets', @sdxTextureWaterDroplets);
  AProduct.Add('sdxTextureCork', @sdxTextureCork);
  AProduct.Add('sdxTextureRecycledPaper', @sdxTextureRecycledPaper);
  AProduct.Add('sdxTextureWhiteMarble', @sdxTextureWhiteMarble);
  AProduct.Add('sdxTexturePinkMarble', @sdxTexturePinkMarble);
  AProduct.Add('sdxTextureCanvas', @sdxTextureCanvas);
  AProduct.Add('sdxTexturePaperBag', @sdxTexturePaperBag);
  AProduct.Add('sdxTextureWalnut', @sdxTextureWalnut);
  AProduct.Add('sdxTextureParchment', @sdxTextureParchment);
  AProduct.Add('sdxTextureBrownMarble', @sdxTextureBrownMarble);
  AProduct.Add('sdxTexturePurpleMesh', @sdxTexturePurpleMesh);
  AProduct.Add('sdxTextureDenim', @sdxTextureDenim);
  AProduct.Add('sdxTextureFishFossil', @sdxTextureFishFossil);
  AProduct.Add('sdxTextureOak', @sdxTextureOak);
  AProduct.Add('sdxTextureStationary', @sdxTextureStationary);
  AProduct.Add('sdxTextureGranite', @sdxTextureGranite);
  AProduct.Add('sdxTextureBouquet', @sdxTextureBouquet);
  AProduct.Add('sdxTextureWonenMat', @sdxTextureWonenMat);
  AProduct.Add('sdxTextureSand', @sdxTextureSand);
  AProduct.Add('sdxTextureMediumWood', @sdxTextureMediumWood);
  AProduct.Add('sdxFSPCaption', @sdxFSPCaption);
  AProduct.Add('sdxWidth', @sdxWidth);
  AProduct.Add('sdxHeight', @sdxHeight);
  AProduct.Add('sdxBrushDlgCaption', @sdxBrushDlgCaption);
  AProduct.Add('sdxStyle', @sdxStyle);
  AProduct.Add('sdxENFNCaption', @sdxENFNCaption);
  AProduct.Add('sdxEnterNewFileName', @sdxEnterNewFileName);
  AProduct.Add('sdxDefinePrintStylesCaption', @sdxDefinePrintStylesCaption);
  AProduct.Add('sdxDefinePrintStylesTitle', @sdxDefinePrintStylesTitle);
  AProduct.Add('sdxDefinePrintStylesWarningDelete', @sdxDefinePrintStylesWarningDelete);
  AProduct.Add('sdxDefinePrintStylesWarningClear', @sdxDefinePrintStylesWarningClear);
  AProduct.Add('sdxClear', @sdxClear);
  AProduct.Add('sdxCustomSize', @sdxCustomSize);
  AProduct.Add('sdxDefaultTray', @sdxDefaultTray);
  AProduct.Add('sdxInvalidPrintDevice', @sdxInvalidPrintDevice);
  AProduct.Add('sdxNotPrinting', @sdxNotPrinting);
  AProduct.Add('sdxPrinting', @sdxPrinting);
  AProduct.Add('sdxDeviceOnPort', @sdxDeviceOnPort);
  AProduct.Add('sdxPrinterIndexError', @sdxPrinterIndexError);
  AProduct.Add('sdxNoDefaultPrintDevice', @sdxNoDefaultPrintDevice);
  AProduct.Add('sdxAutoTextDialogCaption', @sdxAutoTextDialogCaption);
  AProduct.Add('sdxEnterAutoTextEntriesHere', @sdxEnterAutoTextEntriesHere);
  AProduct.Add('sdxPrintDialogCaption', @sdxPrintDialogCaption);
  AProduct.Add('sdxPrintDialogPrinter', @sdxPrintDialogPrinter);
  AProduct.Add('sdxPrintDialogName', @sdxPrintDialogName);
  AProduct.Add('sdxPrintDialogStatus', @sdxPrintDialogStatus);
  AProduct.Add('sdxPrintDialogType', @sdxPrintDialogType);
  AProduct.Add('sdxPrintDialogWhere', @sdxPrintDialogWhere);
  AProduct.Add('sdxPrintDialogComment', @sdxPrintDialogComment);
  AProduct.Add('sdxPrintDialogPrintToFile', @sdxPrintDialogPrintToFile);
  AProduct.Add('sdxPrintDialogPageRange', @sdxPrintDialogPageRange);
  AProduct.Add('sdxPrintDialogAll', @sdxPrintDialogAll);
  AProduct.Add('sdxPrintDialogCurrentPage', @sdxPrintDialogCurrentPage);
  AProduct.Add('sdxPrintDialogSelection', @sdxPrintDialogSelection);
  AProduct.Add('sdxPrintDialogPages', @sdxPrintDialogPages);
  AProduct.Add('sdxPrintDialogRangeLegend', @sdxPrintDialogRangeLegend);
  AProduct.Add('sdxPrintDialogCopies', @sdxPrintDialogCopies);
  AProduct.Add('sdxPrintDialogNumberOfPages', @sdxPrintDialogNumberOfPages);
  AProduct.Add('sdxPrintDialogNumberOfCopies', @sdxPrintDialogNumberOfCopies);
  AProduct.Add('sdxPrintDialogCollateCopies', @sdxPrintDialogCollateCopies);
  AProduct.Add('sdxPrintDialogAllPages', @sdxPrintDialogAllPages);
  AProduct.Add('sdxPrintDialogEvenPages', @sdxPrintDialogEvenPages);
  AProduct.Add('sdxPrintDialogOddPages', @sdxPrintDialogOddPages);
  AProduct.Add('sdxPrintDialogPrintStyles', @sdxPrintDialogPrintStyles);
  AProduct.Add('sdxPrintDialogOpenDlgTitle', @sdxPrintDialogOpenDlgTitle);
  AProduct.Add('sdxPrintDialogOpenDlgAllFiles', @sdxPrintDialogOpenDlgAllFiles);
  AProduct.Add('sdxPrintDialogOpenDlgPrinterFiles', @sdxPrintDialogOpenDlgPrinterFiles);
  AProduct.Add('sdxPrintDialogPageNumbersOutOfRange', @sdxPrintDialogPageNumbersOutOfRange);
  AProduct.Add('sdxPrintDialogInvalidPageRanges', @sdxPrintDialogInvalidPageRanges);
  AProduct.Add('sdxPrintDialogRequiredPageNumbers', @sdxPrintDialogRequiredPageNumbers);
  AProduct.Add('sdxPrintDialogNoPrinters', @sdxPrintDialogNoPrinters);
  AProduct.Add('sdxPrintDialogInPrintingState', @sdxPrintDialogInPrintingState);
  AProduct.Add('sdxPrintDialogPSPaused', @sdxPrintDialogPSPaused);
  AProduct.Add('sdxPrintDialogPSPendingDeletion', @sdxPrintDialogPSPendingDeletion);
  AProduct.Add('sdxPrintDialogPSBusy', @sdxPrintDialogPSBusy);
  AProduct.Add('sdxPrintDialogPSDoorOpen', @sdxPrintDialogPSDoorOpen);
  AProduct.Add('sdxPrintDialogPSError', @sdxPrintDialogPSError);
  AProduct.Add('sdxPrintDialogPSInitializing', @sdxPrintDialogPSInitializing);
  AProduct.Add('sdxPrintDialogPSIOActive', @sdxPrintDialogPSIOActive);
  AProduct.Add('sdxPrintDialogPSManualFeed', @sdxPrintDialogPSManualFeed);
  AProduct.Add('sdxPrintDialogPSNoToner', @sdxPrintDialogPSNoToner);
  AProduct.Add('sdxPrintDialogPSNotAvailable', @sdxPrintDialogPSNotAvailable);
  AProduct.Add('sdxPrintDialogPSOFFLine', @sdxPrintDialogPSOFFLine);
  AProduct.Add('sdxPrintDialogPSOutOfMemory', @sdxPrintDialogPSOutOfMemory);
  AProduct.Add('sdxPrintDialogPSOutBinFull', @sdxPrintDialogPSOutBinFull);
  AProduct.Add('sdxPrintDialogPSPagePunt', @sdxPrintDialogPSPagePunt);
  AProduct.Add('sdxPrintDialogPSPaperJam', @sdxPrintDialogPSPaperJam);
  AProduct.Add('sdxPrintDialogPSPaperOut', @sdxPrintDialogPSPaperOut);
  AProduct.Add('sdxPrintDialogPSPaperProblem', @sdxPrintDialogPSPaperProblem);
  AProduct.Add('sdxPrintDialogPSPrinting', @sdxPrintDialogPSPrinting);
  AProduct.Add('sdxPrintDialogPSProcessing', @sdxPrintDialogPSProcessing);
  AProduct.Add('sdxPrintDialogPSTonerLow', @sdxPrintDialogPSTonerLow);
  AProduct.Add('sdxPrintDialogPSUserIntervention', @sdxPrintDialogPSUserIntervention);
  AProduct.Add('sdxPrintDialogPSWaiting', @sdxPrintDialogPSWaiting);
  AProduct.Add('sdxPrintDialogPSWarningUp', @sdxPrintDialogPSWarningUp);
  AProduct.Add('sdxPrintDialogPSReady', @sdxPrintDialogPSReady);
  AProduct.Add('sdxPrintDialogPSPrintingAndWaiting', @sdxPrintDialogPSPrintingAndWaiting);
  AProduct.Add('sdxLeftMargin', @sdxLeftMargin);
  AProduct.Add('sdxTopMargin', @sdxTopMargin);
  AProduct.Add('sdxRightMargin', @sdxRightMargin);
  AProduct.Add('sdxBottomMargin', @sdxBottomMargin);
  AProduct.Add('sdxGutterMargin', @sdxGutterMargin);
  AProduct.Add('sdxHeaderMargin', @sdxHeaderMargin);
  AProduct.Add('sdxFooterMargin', @sdxFooterMargin);
  AProduct.Add('sdxUnitsInches', @sdxUnitsInches);
  AProduct.Add('sdxUnitsCentimeters', @sdxUnitsCentimeters);
  AProduct.Add('sdxUnitsMillimeters', @sdxUnitsMillimeters);
  AProduct.Add('sdxUnitsPoints', @sdxUnitsPoints);
  AProduct.Add('sdxUnitsPicas', @sdxUnitsPicas);
  AProduct.Add('sdxUnitsDefaultName', @sdxUnitsDefaultName);
  AProduct.Add('sdxUnitsInchesName', @sdxUnitsInchesName);
  AProduct.Add('sdxUnitsCentimetersName', @sdxUnitsCentimetersName);
  AProduct.Add('sdxUnitsMillimetersName', @sdxUnitsMillimetersName);
  AProduct.Add('sdxUnitsPointsName', @sdxUnitsPointsName);
  AProduct.Add('sdxUnitsPicasName', @sdxUnitsPicasName);
  AProduct.Add('sdxPrintPreview', @sdxPrintPreview);
  AProduct.Add('sdxReportDesignerCaption', @sdxReportDesignerCaption);
  AProduct.Add('sdxCompositionDesignerCaption', @sdxCompositionDesignerCaption);
  AProduct.Add('sdxCompositionStartEachItemFromNewPage', @sdxCompositionStartEachItemFromNewPage);
  AProduct.Add('sdxComponentNotSupportedByLink', @sdxComponentNotSupportedByLink);
end;

procedure AddResourceStringsPart2(AProduct: TdxProductResourceStrings);
begin
  AProduct.Add('sdxComponentNotSupported', @sdxComponentNotSupported);
  AProduct.Add('sdxPrintDeviceNotReady', @sdxPrintDeviceNotReady);
  AProduct.Add('sdxUnableToGenerateReport', @sdxUnableToGenerateReport);
  AProduct.Add('sdxPreviewNotRegistered', @sdxPreviewNotRegistered);
  AProduct.Add('sdxComponentNotAssigned', @sdxComponentNotAssigned);
  AProduct.Add('sdxPrintDeviceIsBusy', @sdxPrintDeviceIsBusy);
  AProduct.Add('sdxPrintDeviceError', @sdxPrintDeviceError);
  AProduct.Add('sdxMissingComponent', @sdxMissingComponent);
  AProduct.Add('sdxDataProviderDontPresent', @sdxDataProviderDontPresent);
  AProduct.Add('sdxBuildingReport', @sdxBuildingReport);
  AProduct.Add('sdxPrintingReport', @sdxPrintingReport);
  AProduct.Add('sdxDefinePrintStylesMenuItem', @sdxDefinePrintStylesMenuItem);
  AProduct.Add('sdxAbortPrinting', @sdxAbortPrinting);
  AProduct.Add('sdxStandardStyle', @sdxStandardStyle);
  AProduct.Add('sdxFontStyleBold', @sdxFontStyleBold);
  AProduct.Add('sdxFontStyleItalic', @sdxFontStyleItalic);
  AProduct.Add('sdxFontStyleUnderline', @sdxFontStyleUnderline);
  AProduct.Add('sdxFontStyleStrikeOut', @sdxFontStyleStrikeOut);
  AProduct.Add('sdxPt', @sdxPt);
  AProduct.Add('sdxNoPages', @sdxNoPages);
  AProduct.Add('sdxPageWidth', @sdxPageWidth);
  AProduct.Add('sdxWholePage', @sdxWholePage);
  AProduct.Add('sdxTwoPages', @sdxTwoPages);
  AProduct.Add('sdxFourPages', @sdxFourPages);
  AProduct.Add('sdxWidenToSourceWidth', @sdxWidenToSourceWidth);
  AProduct.Add('sdxMenuBar', @sdxMenuBar);
  AProduct.Add('sdxStandardBar', @sdxStandardBar);
  AProduct.Add('sdxHeaderFooterBar', @sdxHeaderFooterBar);
  AProduct.Add('sdxShortcutMenusBar', @sdxShortcutMenusBar);
  AProduct.Add('sdxAutoTextBar', @sdxAutoTextBar);
  AProduct.Add('sdxMenuFile', @sdxMenuFile);
  AProduct.Add('sdxMenuFileDesign', @sdxMenuFileDesign);
  AProduct.Add('sdxMenuFilePrint', @sdxMenuFilePrint);
  AProduct.Add('sdxMenuFilePrintDialog', @sdxMenuFilePrintDialog);
  AProduct.Add('sdxMenuFilePageSetup', @sdxMenuFilePageSetup);
  AProduct.Add('sdxMenuPrintStyles', @sdxMenuPrintStyles);
  AProduct.Add('sdxMenuFileExit', @sdxMenuFileExit);
  AProduct.Add('sdxMenuExportToPDF', @sdxMenuExportToPDF);
  AProduct.Add('sdxMenuEdit', @sdxMenuEdit);
  AProduct.Add('sdxMenuEditCut', @sdxMenuEditCut);
  AProduct.Add('sdxMenuEditCopy', @sdxMenuEditCopy);
  AProduct.Add('sdxMenuEditPaste', @sdxMenuEditPaste);
  AProduct.Add('sdxMenuEditDelete', @sdxMenuEditDelete);
  AProduct.Add('sdxMenuEditFind', @sdxMenuEditFind);
  AProduct.Add('sdxMenuEditFindNext', @sdxMenuEditFindNext);
  AProduct.Add('sdxMenuEditReplace', @sdxMenuEditReplace);
  AProduct.Add('sdxMenuLoad', @sdxMenuLoad);
  AProduct.Add('sdxMenuPreview', @sdxMenuPreview);
  AProduct.Add('sdxMenuInsert', @sdxMenuInsert);
  AProduct.Add('sdxMenuInsertAutoText', @sdxMenuInsertAutoText);
  AProduct.Add('sdxMenuInsertEditAutoTextEntries', @sdxMenuInsertEditAutoTextEntries);
  AProduct.Add('sdxMenuInsertAutoTextEntries', @sdxMenuInsertAutoTextEntries);
  AProduct.Add('sdxMenuInsertAutoTextEntriesSubItem', @sdxMenuInsertAutoTextEntriesSubItem);
  AProduct.Add('sdxMenuInsertPageNumber', @sdxMenuInsertPageNumber);
  AProduct.Add('sdxMenuInsertTotalPages', @sdxMenuInsertTotalPages);
  AProduct.Add('sdxMenuInsertPageOfPages', @sdxMenuInsertPageOfPages);
  AProduct.Add('sdxMenuInsertDateTime', @sdxMenuInsertDateTime);
  AProduct.Add('sdxMenuInsertDate', @sdxMenuInsertDate);
  AProduct.Add('sdxMenuInsertTime', @sdxMenuInsertTime);
  AProduct.Add('sdxMenuInsertUserName', @sdxMenuInsertUserName);
  AProduct.Add('sdxMenuInsertMachineName', @sdxMenuInsertMachineName);
  AProduct.Add('sdxMenuView', @sdxMenuView);
  AProduct.Add('sdxMenuViewMargins', @sdxMenuViewMargins);
  AProduct.Add('sdxMenuViewFlatToolBarButtons', @sdxMenuViewFlatToolBarButtons);
  AProduct.Add('sdxMenuViewLargeToolBarButtons', @sdxMenuViewLargeToolBarButtons);
  AProduct.Add('sdxMenuViewMarginsStatusBar', @sdxMenuViewMarginsStatusBar);
  AProduct.Add('sdxMenuViewPagesStatusBar', @sdxMenuViewPagesStatusBar);
  AProduct.Add('sdxMenuViewToolBars', @sdxMenuViewToolBars);
  AProduct.Add('sdxMenuViewPagesHeaders', @sdxMenuViewPagesHeaders);
  AProduct.Add('sdxMenuViewPagesFooters', @sdxMenuViewPagesFooters);
  AProduct.Add('sdxMenuViewSwitchToLeftPart', @sdxMenuViewSwitchToLeftPart);
  AProduct.Add('sdxMenuViewSwitchToRightPart', @sdxMenuViewSwitchToRightPart);
  AProduct.Add('sdxMenuViewSwitchToCenterPart', @sdxMenuViewSwitchToCenterPart);
  AProduct.Add('sdxMenuViewHFSwitchHeaderFooter', @sdxMenuViewHFSwitchHeaderFooter);
  AProduct.Add('sdxMenuViewSwitchToFooter', @sdxMenuViewSwitchToFooter);
  AProduct.Add('sdxMenuViewSwitchToHeader', @sdxMenuViewSwitchToHeader);
  AProduct.Add('sdxMenuViewHFClose', @sdxMenuViewHFClose);
  AProduct.Add('sdxMenuZoom', @sdxMenuZoom);
  AProduct.Add('sdxMenuZoomPercent100', @sdxMenuZoomPercent100);
  AProduct.Add('sdxMenuZoomPageWidth', @sdxMenuZoomPageWidth);
  AProduct.Add('sdxMenuZoomWholePage', @sdxMenuZoomWholePage);
  AProduct.Add('sdxMenuZoomTwoPages', @sdxMenuZoomTwoPages);
  AProduct.Add('sdxMenuZoomFourPages', @sdxMenuZoomFourPages);
  AProduct.Add('sdxMenuZoomMultiplyPages', @sdxMenuZoomMultiplyPages);
  AProduct.Add('sdxMenuZoomWidenToSourceWidth', @sdxMenuZoomWidenToSourceWidth);
  AProduct.Add('sdxMenuZoomSetup', @sdxMenuZoomSetup);
  AProduct.Add('sdxMenuPages', @sdxMenuPages);
  AProduct.Add('sdxMenuGotoPage', @sdxMenuGotoPage);
  AProduct.Add('sdxMenuGotoPageFirst', @sdxMenuGotoPageFirst);
  AProduct.Add('sdxMenuGotoPagePrev', @sdxMenuGotoPagePrev);
  AProduct.Add('sdxMenuGotoPageNext', @sdxMenuGotoPageNext);
  AProduct.Add('sdxMenuGotoPageLast', @sdxMenuGotoPageLast);
  AProduct.Add('sdxMenuActivePage', @sdxMenuActivePage);
  AProduct.Add('sdxMenuFormat', @sdxMenuFormat);
  AProduct.Add('sdxMenuFormatHeaderAndFooter', @sdxMenuFormatHeaderAndFooter);
  AProduct.Add('sdxMenuFormatAutoTextEntries', @sdxMenuFormatAutoTextEntries);
  AProduct.Add('sdxMenuFormatDateTime', @sdxMenuFormatDateTime);
  AProduct.Add('sdxMenuFormatPageNumbering', @sdxMenuFormatPageNumbering);
  AProduct.Add('sdxMenuFormatPageBackground', @sdxMenuFormatPageBackground);
  AProduct.Add('sdxMenuFormatShrinkToPage', @sdxMenuFormatShrinkToPage);
  AProduct.Add('sdxMenuFormatHFBackground', @sdxMenuFormatHFBackground);
  AProduct.Add('sdxMenuFormatHFClear', @sdxMenuFormatHFClear);
  AProduct.Add('sdxMenuTools', @sdxMenuTools);
  AProduct.Add('sdxMenuToolsCustomize', @sdxMenuToolsCustomize);
  AProduct.Add('sdxMenuToolsOptions', @sdxMenuToolsOptions);
  AProduct.Add('sdxMenuHelp', @sdxMenuHelp);
  AProduct.Add('sdxMenuHelpTopics', @sdxMenuHelpTopics);
  AProduct.Add('sdxMenuHelpAbout', @sdxMenuHelpAbout);
  AProduct.Add('sdxMenuShortcutPreview', @sdxMenuShortcutPreview);
  AProduct.Add('sdxMenuShortcutAutoText', @sdxMenuShortcutAutoText);
  AProduct.Add('sdxMenuBuiltInMenus', @sdxMenuBuiltInMenus);
  AProduct.Add('sdxMenuShortCutMenus', @sdxMenuShortCutMenus);
  AProduct.Add('sdxMenuNewMenu', @sdxMenuNewMenu);
  AProduct.Add('sdxHintFileDesign', @sdxHintFileDesign);
  AProduct.Add('sdxHintFilePrint', @sdxHintFilePrint);
  AProduct.Add('sdxHintFilePrintDialog', @sdxHintFilePrintDialog);
  AProduct.Add('sdxHintFilePageSetup', @sdxHintFilePageSetup);
  AProduct.Add('sdxHintFileExit', @sdxHintFileExit);
  AProduct.Add('sdxHintExportToPDF', @sdxHintExportToPDF);
  AProduct.Add('sdxHintEditFind', @sdxHintEditFind);
  AProduct.Add('sdxHintEditFindNext', @sdxHintEditFindNext);
  AProduct.Add('sdxHintEditReplace', @sdxHintEditReplace);
  AProduct.Add('sdxHintInsertEditAutoTextEntries', @sdxHintInsertEditAutoTextEntries);
  AProduct.Add('sdxHintInsertPageNumber', @sdxHintInsertPageNumber);
  AProduct.Add('sdxHintInsertTotalPages', @sdxHintInsertTotalPages);
  AProduct.Add('sdxHintInsertPageOfPages', @sdxHintInsertPageOfPages);
  AProduct.Add('sdxHintInsertDateTime', @sdxHintInsertDateTime);
  AProduct.Add('sdxHintInsertDate', @sdxHintInsertDate);
  AProduct.Add('sdxHintInsertTime', @sdxHintInsertTime);
  AProduct.Add('sdxHintInsertUserName', @sdxHintInsertUserName);
  AProduct.Add('sdxHintInsertMachineName', @sdxHintInsertMachineName);
  AProduct.Add('sdxHintViewMargins', @sdxHintViewMargins);
  AProduct.Add('sdxHintViewLargeButtons', @sdxHintViewLargeButtons);
  AProduct.Add('sdxHintViewMarginsStatusBar', @sdxHintViewMarginsStatusBar);
  AProduct.Add('sdxHintViewPagesStatusBar', @sdxHintViewPagesStatusBar);
  AProduct.Add('sdxHintViewPagesHeaders', @sdxHintViewPagesHeaders);
  AProduct.Add('sdxHintViewPagesFooters', @sdxHintViewPagesFooters);
  AProduct.Add('sdxHintViewSwitchToLeftPart', @sdxHintViewSwitchToLeftPart);
  AProduct.Add('sdxHintViewSwitchToRightPart', @sdxHintViewSwitchToRightPart);
  AProduct.Add('sdxHintViewSwitchToCenterPart', @sdxHintViewSwitchToCenterPart);
  AProduct.Add('sdxHintViewHFSwitchHeaderFooter', @sdxHintViewHFSwitchHeaderFooter);
  AProduct.Add('sdxHintViewSwitchToHeader', @sdxHintViewSwitchToHeader);
  AProduct.Add('sdxHintViewSwitchToFooter', @sdxHintViewSwitchToFooter);
  AProduct.Add('sdxHintViewHFClose', @sdxHintViewHFClose);
  AProduct.Add('sdxHintViewZoom', @sdxHintViewZoom);
  AProduct.Add('sdxHintZoomPercent100', @sdxHintZoomPercent100);
  AProduct.Add('sdxHintZoomPageWidth', @sdxHintZoomPageWidth);
  AProduct.Add('sdxHintZoomWholePage', @sdxHintZoomWholePage);
  AProduct.Add('sdxHintZoomTwoPages', @sdxHintZoomTwoPages);
  AProduct.Add('sdxHintZoomFourPages', @sdxHintZoomFourPages);
  AProduct.Add('sdxHintZoomMultiplyPages', @sdxHintZoomMultiplyPages);
  AProduct.Add('sdxHintZoomWidenToSourceWidth', @sdxHintZoomWidenToSourceWidth);
  AProduct.Add('sdxHintZoomSetup', @sdxHintZoomSetup);
  AProduct.Add('sdxHintFormatDateTime', @sdxHintFormatDateTime);
  AProduct.Add('sdxHintFormatPageNumbering', @sdxHintFormatPageNumbering);
  AProduct.Add('sdxHintFormatPageBackground', @sdxHintFormatPageBackground);
  AProduct.Add('sdxHintFormatShrinkToPage', @sdxHintFormatShrinkToPage);
  AProduct.Add('sdxHintFormatHFBackground', @sdxHintFormatHFBackground);
  AProduct.Add('sdxHintFormatHFClear', @sdxHintFormatHFClear);
  AProduct.Add('sdxHintGotoPageFirst', @sdxHintGotoPageFirst);
  AProduct.Add('sdxHintGotoPagePrev', @sdxHintGotoPagePrev);
  AProduct.Add('sdxHintGotoPageNext', @sdxHintGotoPageNext);
  AProduct.Add('sdxHintGotoPageLast', @sdxHintGotoPageLast);
  AProduct.Add('sdxHintActivePage', @sdxHintActivePage);
  AProduct.Add('sdxHintToolsCustomize', @sdxHintToolsCustomize);
  AProduct.Add('sdxHintToolsOptions', @sdxHintToolsOptions);
  AProduct.Add('sdxHintHelpTopics', @sdxHintHelpTopics);
  AProduct.Add('sdxHintHelpAbout', @sdxHintHelpAbout);
  AProduct.Add('sdxPopupMenuLargeButtons', @sdxPopupMenuLargeButtons);
  AProduct.Add('sdxPopupMenuFlatButtons', @sdxPopupMenuFlatButtons);
  AProduct.Add('sdxPaperSize', @sdxPaperSize);
  AProduct.Add('sdxStatus', @sdxStatus);
  AProduct.Add('sdxStatusReady', @sdxStatusReady);
  AProduct.Add('sdxStatusPrinting', @sdxStatusPrinting);
  AProduct.Add('sdxStatusGenerateReport', @sdxStatusGenerateReport);
  AProduct.Add('sdxHintDoubleClickForChangePaperSize', @sdxHintDoubleClickForChangePaperSize);
  AProduct.Add('sdxHintDoubleClickForChangeMargins', @sdxHintDoubleClickForChangeMargins);
  AProduct.Add('sdxDTFormatsCaption', @sdxDTFormatsCaption);
  AProduct.Add('sdxDTFormatsAvailableDateFormats', @sdxDTFormatsAvailableDateFormats);
  AProduct.Add('sdxDTFormatsAvailableTimeFormats', @sdxDTFormatsAvailableTimeFormats);
  AProduct.Add('sdxDTFormatsAutoUpdate', @sdxDTFormatsAutoUpdate);
  AProduct.Add('sdxDTFormatsChangeDefaultFormat', @sdxDTFormatsChangeDefaultFormat);
  AProduct.Add('sdxPNFormatsCaption', @sdxPNFormatsCaption);
  AProduct.Add('sdxPageNumbering', @sdxPageNumbering);
  AProduct.Add('sdxPNFormatsNumberFormat', @sdxPNFormatsNumberFormat);
  AProduct.Add('sdxPNFormatsContinueFromPrevious', @sdxPNFormatsContinueFromPrevious);
  AProduct.Add('sdxPNFormatsStartAt', @sdxPNFormatsStartAt);
  AProduct.Add('sdxPNFormatsChangeDefaultFormat', @sdxPNFormatsChangeDefaultFormat);
  AProduct.Add('sdxZoomDlgCaption', @sdxZoomDlgCaption);
  AProduct.Add('sdxZoomDlgZoomTo', @sdxZoomDlgZoomTo);
  AProduct.Add('sdxZoomDlgPageWidth', @sdxZoomDlgPageWidth);
  AProduct.Add('sdxZoomDlgWholePage', @sdxZoomDlgWholePage);
  AProduct.Add('sdxZoomDlgTwoPages', @sdxZoomDlgTwoPages);
  AProduct.Add('sdxZoomDlgFourPages', @sdxZoomDlgFourPages);
  AProduct.Add('sdxZoomDlgManyPages', @sdxZoomDlgManyPages);
  AProduct.Add('sdxZoomDlgPercent', @sdxZoomDlgPercent);
  AProduct.Add('sdxZoomDlgPreview', @sdxZoomDlgPreview);
  AProduct.Add('sdxZoomDlgFontPreview', @sdxZoomDlgFontPreview);
  AProduct.Add('sdxZoomDlgFontPreviewString', @sdxZoomDlgFontPreviewString);
  AProduct.Add('sdxPages', @sdxPages);
  AProduct.Add('sdxCancel', @sdxCancel);
  AProduct.Add('sdxPreferenceDlgCaption', @sdxPreferenceDlgCaption);
  AProduct.Add('sdxPreferenceDlgTab1', @sdxPreferenceDlgTab1);
  AProduct.Add('sdxPreferenceDlgTab2', @sdxPreferenceDlgTab2);
  AProduct.Add('sdxPreferenceDlgTab3', @sdxPreferenceDlgTab3);
  AProduct.Add('sdxPreferenceDlgTab4', @sdxPreferenceDlgTab4);
  AProduct.Add('sdxPreferenceDlgTab5', @sdxPreferenceDlgTab5);
  AProduct.Add('sdxPreferenceDlgTab6', @sdxPreferenceDlgTab6);
  AProduct.Add('sdxPreferenceDlgTab7', @sdxPreferenceDlgTab7);
  AProduct.Add('sdxPreferenceDlgTab8', @sdxPreferenceDlgTab8);
  AProduct.Add('sdxPreferenceDlgTab9', @sdxPreferenceDlgTab9);
  AProduct.Add('sdxPreferenceDlgTab10', @sdxPreferenceDlgTab10);
  AProduct.Add('sdxPreferenceDlgShow', @sdxPreferenceDlgShow);
  AProduct.Add('sdxPreferenceDlgMargins', @sdxPreferenceDlgMargins);
  AProduct.Add('sdxPreferenceDlgMarginsHints', @sdxPreferenceDlgMarginsHints);
  AProduct.Add('sdxPreferenceDlgMargingWhileDragging', @sdxPreferenceDlgMargingWhileDragging);
  AProduct.Add('sdxPreferenceDlgLargeBtns', @sdxPreferenceDlgLargeBtns);
  AProduct.Add('sdxPreferenceDlgFlatBtns', @sdxPreferenceDlgFlatBtns);
  AProduct.Add('sdxPreferenceDlgMarginsColor', @sdxPreferenceDlgMarginsColor);
  AProduct.Add('sdxPreferenceDlgMeasurementUnits', @sdxPreferenceDlgMeasurementUnits);
  AProduct.Add('sdxPreferenceDlgSaveForRunTimeToo', @sdxPreferenceDlgSaveForRunTimeToo);
  AProduct.Add('sdxPreferenceDlgZoomScroll', @sdxPreferenceDlgZoomScroll);
  AProduct.Add('sdxPreferenceDlgZoomStep', @sdxPreferenceDlgZoomStep);
  AProduct.Add('sdxCloneStyleCaptionPrefix', @sdxCloneStyleCaptionPrefix);
  AProduct.Add('sdxInvalideStyleCaption', @sdxInvalideStyleCaption);
  AProduct.Add('sdxHintMoreHFFunctions', @sdxHintMoreHFFunctions);
  AProduct.Add('sdxPageSetupCaption', @sdxPageSetupCaption);
  AProduct.Add('sdxStyleName', @sdxStyleName);
  AProduct.Add('sdxPage', @sdxPage);
  AProduct.Add('sdxMargins', @sdxMargins);
  AProduct.Add('sdxHeaderFooter', @sdxHeaderFooter);
  AProduct.Add('sdxScaling', @sdxScaling);
  AProduct.Add('sdxPaper', @sdxPaper);
  AProduct.Add('sdxPaperType', @sdxPaperType);
  AProduct.Add('sdxPaperDimension', @sdxPaperDimension);
  AProduct.Add('sdxPaperWidth', @sdxPaperWidth);
  AProduct.Add('sdxPaperHeight', @sdxPaperHeight);
  AProduct.Add('sdxPaperSource', @sdxPaperSource);
  AProduct.Add('sdxOrientation', @sdxOrientation);
  AProduct.Add('sdxPortrait', @sdxPortrait);
  AProduct.Add('sdxLandscape', @sdxLandscape);
  AProduct.Add('sdxPrintOrder', @sdxPrintOrder);
  AProduct.Add('sdxDownThenOver', @sdxDownThenOver);
  AProduct.Add('sdxOverThenDown', @sdxOverThenDown);
  AProduct.Add('sdxShading', @sdxShading);
  AProduct.Add('sdxPrintUsingGrayShading', @sdxPrintUsingGrayShading);
  AProduct.Add('sdxCenterOnPage', @sdxCenterOnPage);
  AProduct.Add('sdxHorizontally', @sdxHorizontally);
  AProduct.Add('sdxVertically', @sdxVertically);
  AProduct.Add('sdxHeader', @sdxHeader);
  AProduct.Add('sdxBtnHeaderFont', @sdxBtnHeaderFont);
  AProduct.Add('sdxBtnHeaderBackground', @sdxBtnHeaderBackground);
  AProduct.Add('sdxFooter', @sdxFooter);
  AProduct.Add('sdxBtnFooterFont', @sdxBtnFooterFont);
  AProduct.Add('sdxBtnFooterBackground', @sdxBtnFooterBackground);
  AProduct.Add('sdxTop', @sdxTop);
  AProduct.Add('sdxLeft', @sdxLeft);
  AProduct.Add('sdxRight', @sdxRight);
  AProduct.Add('sdxBottom', @sdxBottom);
  AProduct.Add('sdxHeader2', @sdxHeader2);
  AProduct.Add('sdxFooter2', @sdxFooter2);
  AProduct.Add('sdxAlignment', @sdxAlignment);
  AProduct.Add('sdxVertAlignment', @sdxVertAlignment);
  AProduct.Add('sdxReverseOnEvenPages', @sdxReverseOnEvenPages);
  AProduct.Add('sdxAdjustTo', @sdxAdjustTo);
  AProduct.Add('sdxFitTo', @sdxFitTo);
  AProduct.Add('sdxPercentOfNormalSize', @sdxPercentOfNormalSize);
  AProduct.Add('sdxPagesWideBy', @sdxPagesWideBy);
  AProduct.Add('sdxTall', @sdxTall);
  AProduct.Add('sdxOf', @sdxOf);
  AProduct.Add('sdxLastPrinted', @sdxLastPrinted);
  AProduct.Add('sdxFileName', @sdxFileName);
  AProduct.Add('sdxFileNameAndPath', @sdxFileNameAndPath);
  AProduct.Add('sdxPrintedBy', @sdxPrintedBy);
  AProduct.Add('sdxPrintedOn', @sdxPrintedOn);
  AProduct.Add('sdxCreatedBy', @sdxCreatedBy);
  AProduct.Add('sdxCreatedOn', @sdxCreatedOn);
  AProduct.Add('sdxConfidential', @sdxConfidential);
  AProduct.Add('sdxHFFunctionNameUnknown', @sdxHFFunctionNameUnknown);
  AProduct.Add('sdxHFFunctionNamePageNumber', @sdxHFFunctionNamePageNumber);
  AProduct.Add('sdxHFFunctionNameTotalPages', @sdxHFFunctionNameTotalPages);
  AProduct.Add('sdxHFFunctionNamePageOfPages', @sdxHFFunctionNamePageOfPages);
  AProduct.Add('sdxHFFunctionNameDateTime', @sdxHFFunctionNameDateTime);
  AProduct.Add('sdxHFFunctionNameImage', @sdxHFFunctionNameImage);
  AProduct.Add('sdxHFFunctionNameDate', @sdxHFFunctionNameDate);
  AProduct.Add('sdxHFFunctionNameTime', @sdxHFFunctionNameTime);
  AProduct.Add('sdxHFFunctionNameUserName', @sdxHFFunctionNameUserName);
  AProduct.Add('sdxHFFunctionNameMachineName', @sdxHFFunctionNameMachineName);
  AProduct.Add('sdxHFFunctionHintPageNumber', @sdxHFFunctionHintPageNumber);
  AProduct.Add('sdxHFFunctionHintTotalPages', @sdxHFFunctionHintTotalPages);
  AProduct.Add('sdxHFFunctionHintPageOfPages', @sdxHFFunctionHintPageOfPages);
  AProduct.Add('sdxHFFunctionHintDateTime', @sdxHFFunctionHintDateTime);
  AProduct.Add('sdxHFFunctionHintImage', @sdxHFFunctionHintImage);
  AProduct.Add('sdxHFFunctionHintDate', @sdxHFFunctionHintDate);
  AProduct.Add('sdxHFFunctionHintTime', @sdxHFFunctionHintTime);
  AProduct.Add('sdxHFFunctionHintUserName', @sdxHFFunctionHintUserName);
  AProduct.Add('sdxHFFunctionHintMachineName', @sdxHFFunctionHintMachineName);
  AProduct.Add('sdxHFFunctionTemplatePageNumber', @sdxHFFunctionTemplatePageNumber);
  AProduct.Add('sdxHFFunctionTemplateTotalPages', @sdxHFFunctionTemplateTotalPages);
  AProduct.Add('sdxHFFunctionTemplatePageOfPages', @sdxHFFunctionTemplatePageOfPages);
  AProduct.Add('sdxHFFunctionTemplateDateTime', @sdxHFFunctionTemplateDateTime);
  AProduct.Add('sdxHFFunctionTemplateImage', @sdxHFFunctionTemplateImage);
  AProduct.Add('sdxHFFunctionTemplateDate', @sdxHFFunctionTemplateDate);
  AProduct.Add('sdxHFFunctionTemplateTime', @sdxHFFunctionTemplateTime);
  AProduct.Add('sdxHFFunctionTemplateUserName', @sdxHFFunctionTemplateUserName);
  AProduct.Add('sdxHFFunctionTemplateMachineName', @sdxHFFunctionTemplateMachineName);
  AProduct.Add('sdxJanuary', @sdxJanuary);
  AProduct.Add('sdxFebruary', @sdxFebruary);
  AProduct.Add('sdxMarch', @sdxMarch);
  AProduct.Add('sdxApril', @sdxApril);
  AProduct.Add('sdxMay', @sdxMay);
  AProduct.Add('sdxJune', @sdxJune);
  AProduct.Add('sdxJuly', @sdxJuly);
  AProduct.Add('sdxAugust', @sdxAugust);
  AProduct.Add('sdxSeptember', @sdxSeptember);
  AProduct.Add('sdxOctober', @sdxOctober);
  AProduct.Add('sdxNovember', @sdxNovember);
  AProduct.Add('sdxDecember', @sdxDecember);
  AProduct.Add('sdxEast', @sdxEast);
  AProduct.Add('sdxWest', @sdxWest);
  AProduct.Add('sdxSouth', @sdxSouth);
  AProduct.Add('sdxNorth', @sdxNorth);
  AProduct.Add('sdxTotal', @sdxTotal);
  AProduct.Add('sdxPlan', @sdxPlan);
  AProduct.Add('sdxSwimmingPool', @sdxSwimmingPool);
  AProduct.Add('sdxAdministration', @sdxAdministration);
  AProduct.Add('sdxPark', @sdxPark);
  AProduct.Add('sdxCarParking', @sdxCarParking);
  AProduct.Add('sdxCorporateHeadquarters', @sdxCorporateHeadquarters);
  AProduct.Add('sdxSalesAndMarketing', @sdxSalesAndMarketing);
  AProduct.Add('sdxEngineering', @sdxEngineering);
  AProduct.Add('sdxFieldOfficeCanada', @sdxFieldOfficeCanada);
  AProduct.Add('sdxOrderNoCaption', @sdxOrderNoCaption);
  AProduct.Add('sdxNameCaption', @sdxNameCaption);
  AProduct.Add('sdxCountCaption', @sdxCountCaption);
  AProduct.Add('sdxCompanyCaption', @sdxCompanyCaption);
  AProduct.Add('sdxAddressCaption', @sdxAddressCaption);
  AProduct.Add('sdxPriceCaption', @sdxPriceCaption);
  AProduct.Add('sdxCashCaption', @sdxCashCaption);
  AProduct.Add('sdxName1', @sdxName1);
  AProduct.Add('sdxName2', @sdxName2);
  AProduct.Add('sdxCompany1', @sdxCompany1);
  AProduct.Add('sdxCompany2', @sdxCompany2);
  AProduct.Add('sdxAddress1', @sdxAddress1);
  AProduct.Add('sdxAddress2', @sdxAddress2);
  AProduct.Add('sdxCountIs', @sdxCountIs);
  AProduct.Add('sdxRegular', @sdxRegular);
  AProduct.Add('sdxIrregular', @sdxIrregular);
  AProduct.Add('sdxTLBand', @sdxTLBand);
  AProduct.Add('sdxTLColumnName', @sdxTLColumnName);
  AProduct.Add('sdxTLColumnAxisymmetric', @sdxTLColumnAxisymmetric);
  AProduct.Add('sdxTLColumnItemShape', @sdxTLColumnItemShape);
  AProduct.Add('sdxItemShapeAsText', @sdxItemShapeAsText);
  AProduct.Add('sdxItem1Name', @sdxItem1Name);
  AProduct.Add('sdxItem2Name', @sdxItem2Name);
  AProduct.Add('sdxItem3Name', @sdxItem3Name);
  AProduct.Add('sdxItem4Name', @sdxItem4Name);
  AProduct.Add('sdxItem5Name', @sdxItem5Name);
  AProduct.Add('sdxItem1Description', @sdxItem1Description);
  AProduct.Add('sdxItem2Description', @sdxItem2Description);
  AProduct.Add('sdxItem3Description', @sdxItem3Description);
  AProduct.Add('sdxItem4Description', @sdxItem4Description);
  AProduct.Add('sdxItem5Description', @sdxItem5Description);
  AProduct.Add('sdxItem6Description', @sdxItem6Description);
  AProduct.Add('sdxItem7Description', @sdxItem7Description);
  AProduct.Add('sdxPatternIsNotRegistered', @sdxPatternIsNotRegistered);
  AProduct.Add('sdxSolidEdgePattern', @sdxSolidEdgePattern);
  AProduct.Add('sdxThinSolidEdgePattern', @sdxThinSolidEdgePattern);
  AProduct.Add('sdxMediumSolidEdgePattern', @sdxMediumSolidEdgePattern);
  AProduct.Add('sdxThickSolidEdgePattern', @sdxThickSolidEdgePattern);
  AProduct.Add('sdxDottedEdgePattern', @sdxDottedEdgePattern);
  AProduct.Add('sdxDashedEdgePattern', @sdxDashedEdgePattern);
  AProduct.Add('sdxDashDotDotEdgePattern', @sdxDashDotDotEdgePattern);
  AProduct.Add('sdxDashDotEdgePattern', @sdxDashDotEdgePattern);
  AProduct.Add('sdxSlantedDashDotEdgePattern', @sdxSlantedDashDotEdgePattern);
  AProduct.Add('sdxMediumDashDotDotEdgePattern', @sdxMediumDashDotDotEdgePattern);
  AProduct.Add('sdxHairEdgePattern', @sdxHairEdgePattern);
  AProduct.Add('sdxMediumDashDotEdgePattern', @sdxMediumDashDotEdgePattern);
  AProduct.Add('sdxMediumDashedEdgePattern', @sdxMediumDashedEdgePattern);
  AProduct.Add('sdxDoubleLineEdgePattern', @sdxDoubleLineEdgePattern);
  AProduct.Add('sdxSolidFillPattern', @sdxSolidFillPattern);
  AProduct.Add('sdxGray75FillPattern', @sdxGray75FillPattern);
  AProduct.Add('sdxGray50FillPattern', @sdxGray50FillPattern);
  AProduct.Add('sdxGray25FillPattern', @sdxGray25FillPattern);
  AProduct.Add('sdxGray125FillPattern', @sdxGray125FillPattern);
  AProduct.Add('sdxGray625FillPattern', @sdxGray625FillPattern);
  AProduct.Add('sdxHorizontalStripeFillPattern', @sdxHorizontalStripeFillPattern);
  AProduct.Add('sdxVerticalStripeFillPattern', @sdxVerticalStripeFillPattern);
  AProduct.Add('sdxReverseDiagonalStripeFillPattern', @sdxReverseDiagonalStripeFillPattern);
  AProduct.Add('sdxDiagonalStripeFillPattern', @sdxDiagonalStripeFillPattern);
  AProduct.Add('sdxDiagonalCrossHatchFillPattern', @sdxDiagonalCrossHatchFillPattern);
  AProduct.Add('sdxThickCrossHatchFillPattern', @sdxThickCrossHatchFillPattern);
  AProduct.Add('sdxThinHorizontalStripeFillPattern', @sdxThinHorizontalStripeFillPattern);
  AProduct.Add('sdxThinVerticalStripeFillPattern', @sdxThinVerticalStripeFillPattern);
  AProduct.Add('sdxThinReverseDiagonalStripeFillPattern', @sdxThinReverseDiagonalStripeFillPattern);
  AProduct.Add('sdxThinDiagonalStripeFillPattern', @sdxThinDiagonalStripeFillPattern);
  AProduct.Add('sdxThinHorizontalCrossHatchFillPattern', @sdxThinHorizontalCrossHatchFillPattern);
  AProduct.Add('sdxThinDiagonalCrossHatchFillPattern', @sdxThinDiagonalCrossHatchFillPattern);
  AProduct.Add('sdxShowRowAndColumnHeadings', @sdxShowRowAndColumnHeadings);
  AProduct.Add('sdxShowGridLines', @sdxShowGridLines);
  AProduct.Add('sdxSuppressSourceFormats', @sdxSuppressSourceFormats);
  AProduct.Add('sdxRepeatHeaderRowAtTop', @sdxRepeatHeaderRowAtTop);
  AProduct.Add('sdxDataToPrintDoesNotExist', @sdxDataToPrintDoesNotExist);
  AProduct.Add('sdxJanuaryShort', @sdxJanuaryShort);
  AProduct.Add('sdxFebruaryShort', @sdxFebruaryShort);
  AProduct.Add('sdxMarchShort', @sdxMarchShort);
  AProduct.Add('sdxAprilShort', @sdxAprilShort);
  AProduct.Add('sdxMayShort', @sdxMayShort);
  AProduct.Add('sdxJuneShort', @sdxJuneShort);
  AProduct.Add('sdxJulyShort', @sdxJulyShort);
  AProduct.Add('sdxAugustShort', @sdxAugustShort);
  AProduct.Add('sdxSeptemberShort', @sdxSeptemberShort);
  AProduct.Add('sdxOctoberShort', @sdxOctoberShort);
  AProduct.Add('sdxNovemberShort', @sdxNovemberShort);
  AProduct.Add('sdxDecemberShort', @sdxDecemberShort);
  AProduct.Add('sdxTechnicalDepartment', @sdxTechnicalDepartment);
  AProduct.Add('sdxSoftwareDepartment', @sdxSoftwareDepartment);
  AProduct.Add('sdxSystemProgrammers', @sdxSystemProgrammers);
  AProduct.Add('sdxEndUserProgrammers', @sdxEndUserProgrammers);
  AProduct.Add('sdxBetaTesters', @sdxBetaTesters);
  AProduct.Add('sdxHumanResourceDepartment', @sdxHumanResourceDepartment);
  AProduct.Add('sdxTreeLines', @sdxTreeLines);
  AProduct.Add('sdxTreeLinesColor', @sdxTreeLinesColor);
  AProduct.Add('sdxExpandButtons', @sdxExpandButtons);
  AProduct.Add('sdxCheckMarks', @sdxCheckMarks);
  AProduct.Add('sdxTreeEffects', @sdxTreeEffects);
  AProduct.Add('sdxAppearance', @sdxAppearance);
  AProduct.Add('sdxCarLevelCaption', @sdxCarLevelCaption);
  AProduct.Add('sdxManufacturerBandCaption', @sdxManufacturerBandCaption);
  AProduct.Add('sdxModelBandCaption', @sdxModelBandCaption);
  AProduct.Add('sdxManufacturerNameColumnCaption', @sdxManufacturerNameColumnCaption);
  AProduct.Add('sdxManufacturerLogoColumnCaption', @sdxManufacturerLogoColumnCaption);
  AProduct.Add('sdxManufacturerCountryColumnCaption', @sdxManufacturerCountryColumnCaption);
  AProduct.Add('sdxCarModelColumnCaption', @sdxCarModelColumnCaption);
  AProduct.Add('sdxCarIsSUVColumnCaption', @sdxCarIsSUVColumnCaption);
  AProduct.Add('sdxCarPhotoColumnCaption', @sdxCarPhotoColumnCaption);
  AProduct.Add('sdxCarManufacturerName1', @sdxCarManufacturerName1);
  AProduct.Add('sdxCarManufacturerName2', @sdxCarManufacturerName2);
  AProduct.Add('sdxCarManufacturerName3', @sdxCarManufacturerName3);
  AProduct.Add('sdxCarManufacturerName4', @sdxCarManufacturerName4);
  AProduct.Add('sdxCarManufacturerCountry1', @sdxCarManufacturerCountry1);
  AProduct.Add('sdxCarManufacturerCountry2', @sdxCarManufacturerCountry2);
  AProduct.Add('sdxCarManufacturerCountry3', @sdxCarManufacturerCountry3);
  AProduct.Add('sdxCarManufacturerCountry4', @sdxCarManufacturerCountry4);
  AProduct.Add('sdxCarModel1', @sdxCarModel1);
  AProduct.Add('sdxCarModel2', @sdxCarModel2);
  AProduct.Add('sdxCarModel3', @sdxCarModel3);
  AProduct.Add('sdxCarModel4', @sdxCarModel4);
  AProduct.Add('sdxTrue', @sdxTrue);
  AProduct.Add('sdxFalse', @sdxFalse);
  AProduct.Add('sdxAuto', @sdxAuto);
  AProduct.Add('sdxCustom', @sdxCustom);
  AProduct.Add('sdxEnv', @sdxEnv);
  AProduct.Add('sdxLookAndFeelFlat', @sdxLookAndFeelFlat);
  AProduct.Add('sdxLookAndFeelStandard', @sdxLookAndFeelStandard);
  AProduct.Add('sdxLookAndFeelUltraFlat', @sdxLookAndFeelUltraFlat);
  AProduct.Add('sdxViewTab', @sdxViewTab);
  AProduct.Add('sdxBehaviorsTab', @sdxBehaviorsTab);
  AProduct.Add('sdxPreviewTab', @sdxPreviewTab);
  AProduct.Add('sdxCardsTab', @sdxCardsTab);
  AProduct.Add('sdxFormatting', @sdxFormatting);
  AProduct.Add('sdxLookAndFeel', @sdxLookAndFeel);
  AProduct.Add('sdxLevelCaption', @sdxLevelCaption);
  AProduct.Add('sdxFilterBar', @sdxFilterBar);
  AProduct.Add('sdxRefinements', @sdxRefinements);
  AProduct.Add('sdxProcessSelection', @sdxProcessSelection);
  AProduct.Add('sdxProcessExactSelection', @sdxProcessExactSelection);
  AProduct.Add('sdxExpanding', @sdxExpanding);
  AProduct.Add('sdxGroups', @sdxGroups);
  AProduct.Add('sdxDetails', @sdxDetails);
  AProduct.Add('sdxStartFromActiveDetails', @sdxStartFromActiveDetails);
  AProduct.Add('sdxOnlyActiveDetails', @sdxOnlyActiveDetails);
  AProduct.Add('sdxVisible', @sdxVisible);
  AProduct.Add('sdxPreviewAutoHeight', @sdxPreviewAutoHeight);
  AProduct.Add('sdxPreviewMaxLineCount', @sdxPreviewMaxLineCount);
  AProduct.Add('sdxSizes', @sdxSizes);
  AProduct.Add('sdxKeepSameWidth', @sdxKeepSameWidth);
  AProduct.Add('sdxKeepSameHeight', @sdxKeepSameHeight);
  AProduct.Add('sdxFraming', @sdxFraming);
  AProduct.Add('sdxSpacing', @sdxSpacing);
  AProduct.Add('sdxShadow', @sdxShadow);
  AProduct.Add('sdxDepth', @sdxDepth);
  AProduct.Add('sdxPosition', @sdxPosition);
  AProduct.Add('sdxPositioning', @sdxPositioning);
  AProduct.Add('sdxHorizontal', @sdxHorizontal);
  AProduct.Add('sdxVertical', @sdxVertical);
  AProduct.Add('sdxSummaryFormat', @sdxSummaryFormat);
  AProduct.Add('sdxCannotUseOnEveryPageMode', @sdxCannotUseOnEveryPageMode);
  AProduct.Add('sdxIncorrectBandHeadersState', @sdxIncorrectBandHeadersState);
  AProduct.Add('sdxIncorrectHeadersState', @sdxIncorrectHeadersState);
  AProduct.Add('sdxIncorrectFootersState', @sdxIncorrectFootersState);
  AProduct.Add('sdxCharts', @sdxCharts);
  AProduct.Add('sdxTPicture', @sdxTPicture);
  AProduct.Add('sdxCopy', @sdxCopy);
  AProduct.Add('sdxSave', @sdxSave);
  AProduct.Add('sdxBaseStyle', @sdxBaseStyle);
end;

procedure AddResourceStringsPart3(AProduct: TdxProductResourceStrings);
begin
  AProduct.Add('sdxComponentAlreadyExists', @sdxComponentAlreadyExists);
  AProduct.Add('sdxInvalidComponentName', @sdxInvalidComponentName);
  AProduct.Add('sdxRectangle', @sdxRectangle);
  AProduct.Add('sdxSquare', @sdxSquare);
  AProduct.Add('sdxEllipse', @sdxEllipse);
  AProduct.Add('sdxCircle', @sdxCircle);
  AProduct.Add('sdxRoundRect', @sdxRoundRect);
  AProduct.Add('sdxRoundSquare', @sdxRoundSquare);
  AProduct.Add('sdxHorizontalFillPattern', @sdxHorizontalFillPattern);
  AProduct.Add('sdxVerticalFillPattern', @sdxVerticalFillPattern);
  AProduct.Add('sdxFDiagonalFillPattern', @sdxFDiagonalFillPattern);
  AProduct.Add('sdxBDiagonalFillPattern', @sdxBDiagonalFillPattern);
  AProduct.Add('sdxCrossFillPattern', @sdxCrossFillPattern);
  AProduct.Add('sdxDiagCrossFillPattern', @sdxDiagCrossFillPattern);
  AProduct.Add('sdxCyclicIDReferences', @sdxCyclicIDReferences);
  AProduct.Add('sdxLoadReportDataToFileTitle', @sdxLoadReportDataToFileTitle);
  AProduct.Add('sdxSaveReportDataToFileTitle', @sdxSaveReportDataToFileTitle);
  AProduct.Add('sdxInvalidExternalStorage', @sdxInvalidExternalStorage);
  AProduct.Add('sdxLinkIsNotIncludedInUsesClause', @sdxLinkIsNotIncludedInUsesClause);
  AProduct.Add('sdxInvalidStorageVersion', @sdxInvalidStorageVersion);
  AProduct.Add('sdxPSReportFiles', @sdxPSReportFiles);
  AProduct.Add('sdxReportFileLoadError', @sdxReportFileLoadError);
  AProduct.Add('sdxNone', @sdxNone);
  AProduct.Add('sdxReportDocumentIsCorrupted', @sdxReportDocumentIsCorrupted);
  AProduct.Add('sdxCloseExplorerHint', @sdxCloseExplorerHint);
  AProduct.Add('sdxExplorerCaption', @sdxExplorerCaption);
  AProduct.Add('sdxExplorerRootFolderCaption', @sdxExplorerRootFolderCaption);
  AProduct.Add('sdxNewExplorerFolderItem', @sdxNewExplorerFolderItem);
  AProduct.Add('sdxCopyOfItem', @sdxCopyOfItem);
  AProduct.Add('sdxReportExplorer', @sdxReportExplorer);
  AProduct.Add('sdxDataLoadErrorText', @sdxDataLoadErrorText);
  AProduct.Add('sdxDBBasedExplorerItemDataLoadError', @sdxDBBasedExplorerItemDataLoadError);
  AProduct.Add('sdxFileBasedExplorerItemDataLoadError', @sdxFileBasedExplorerItemDataLoadError);
  AProduct.Add('sdxDeleteNonEmptyFolderMessageText', @sdxDeleteNonEmptyFolderMessageText);
  AProduct.Add('sdxDeleteFolderMessageText', @sdxDeleteFolderMessageText);
  AProduct.Add('sdxDeleteItemMessageText', @sdxDeleteItemMessageText);
  AProduct.Add('sdxCannotRenameFolderText', @sdxCannotRenameFolderText);
  AProduct.Add('sdxCannotRenameItemText', @sdxCannotRenameItemText);
  AProduct.Add('sdxOverwriteFolderMessageText', @sdxOverwriteFolderMessageText);
  AProduct.Add('sdxOverwriteItemMessageText', @sdxOverwriteItemMessageText);
  AProduct.Add('sdxSelectNewRoot', @sdxSelectNewRoot);
  AProduct.Add('sdxInvalidFolderName', @sdxInvalidFolderName);
  AProduct.Add('sdxInvalidReportName', @sdxInvalidReportName);
  AProduct.Add('sdxExplorerBar', @sdxExplorerBar);
  AProduct.Add('sdxMenuFileSave', @sdxMenuFileSave);
  AProduct.Add('sdxMenuFileSaveAs', @sdxMenuFileSaveAs);
  AProduct.Add('sdxMenuFileLoad', @sdxMenuFileLoad);
  AProduct.Add('sdxMenuFileClose', @sdxMenuFileClose);
  AProduct.Add('sdxHintFileSave', @sdxHintFileSave);
  AProduct.Add('sdxHintFileSaveAs', @sdxHintFileSaveAs);
  AProduct.Add('sdxHintFileLoad', @sdxHintFileLoad);
  AProduct.Add('sdxHintFileClose', @sdxHintFileClose);
  AProduct.Add('sdxMenuExplorer', @sdxMenuExplorer);
  AProduct.Add('sdxMenuExplorerCreateFolder', @sdxMenuExplorerCreateFolder);
  AProduct.Add('sdxMenuExplorerDelete', @sdxMenuExplorerDelete);
  AProduct.Add('sdxMenuExplorerRename', @sdxMenuExplorerRename);
  AProduct.Add('sdxMenuExplorerProperties', @sdxMenuExplorerProperties);
  AProduct.Add('sdxMenuExplorerRefresh', @sdxMenuExplorerRefresh);
  AProduct.Add('sdxMenuExplorerChangeRootPath', @sdxMenuExplorerChangeRootPath);
  AProduct.Add('sdxMenuExplorerSetAsRoot', @sdxMenuExplorerSetAsRoot);
  AProduct.Add('sdxMenuExplorerGoToUpOneLevel', @sdxMenuExplorerGoToUpOneLevel);
  AProduct.Add('sdxHintExplorerCreateFolder', @sdxHintExplorerCreateFolder);
  AProduct.Add('sdxHintExplorerDelete', @sdxHintExplorerDelete);
  AProduct.Add('sdxHintExplorerRename', @sdxHintExplorerRename);
  AProduct.Add('sdxHintExplorerProperties', @sdxHintExplorerProperties);
  AProduct.Add('sdxHintExplorerRefresh', @sdxHintExplorerRefresh);
  AProduct.Add('sdxHintExplorerChangeRootPath', @sdxHintExplorerChangeRootPath);
  AProduct.Add('sdxHintExplorerSetAsRoot', @sdxHintExplorerSetAsRoot);
  AProduct.Add('sdxHintExplorerGoToUpOneLevel', @sdxHintExplorerGoToUpOneLevel);
  AProduct.Add('sdxMenuViewExplorer', @sdxMenuViewExplorer);
  AProduct.Add('sdxHintViewExplorer', @sdxHintViewExplorer);
  AProduct.Add('sdxSummary', @sdxSummary);
  AProduct.Add('sdxCreator', @sdxCreator);
  AProduct.Add('sdxCreationDate', @sdxCreationDate);
  AProduct.Add('sdxMenuViewThumbnails', @sdxMenuViewThumbnails);
  AProduct.Add('sdxMenuThumbnailsLarge', @sdxMenuThumbnailsLarge);
  AProduct.Add('sdxMenuThumbnailsSmall', @sdxMenuThumbnailsSmall);
  AProduct.Add('sdxHintViewThumbnails', @sdxHintViewThumbnails);
  AProduct.Add('sdxHintThumbnailsLarge', @sdxHintThumbnailsLarge);
  AProduct.Add('sdxHintThumbnailsSmall', @sdxHintThumbnailsSmall);
  AProduct.Add('sdxMenuFormatTitle', @sdxMenuFormatTitle);
  AProduct.Add('sdxHintFormatTitle', @sdxHintFormatTitle);
  AProduct.Add('sdxMenuFormatFootnotes', @sdxMenuFormatFootnotes);
  AProduct.Add('sdxHintFormatFootnotes', @sdxHintFormatFootnotes);
  AProduct.Add('sdxHalf', @sdxHalf);
  AProduct.Add('sdxPredefinedFunctions', @sdxPredefinedFunctions);
  AProduct.Add('sdxZoomParameters', @sdxZoomParameters);
  AProduct.Add('sdxWrapData', @sdxWrapData);
  AProduct.Add('sdxMenuShortcutExplorer', @sdxMenuShortcutExplorer);
  AProduct.Add('sdxExplorerToolBar', @sdxExplorerToolBar);
  AProduct.Add('sdxMenuShortcutThumbnails', @sdxMenuShortcutThumbnails);
  AProduct.Add('sdxButtons', @sdxButtons);
  AProduct.Add('sdxBtnHeadersFont', @sdxBtnHeadersFont);
  AProduct.Add('sdxHeadersTransparent', @sdxHeadersTransparent);
  AProduct.Add('sdxHintListViewDesignerMessage', @sdxHintListViewDesignerMessage);
  AProduct.Add('sdxColumnHeaders', @sdxColumnHeaders);
  AProduct.Add('sdxReportGroupNullLookAndFeel', @sdxReportGroupNullLookAndFeel);
  AProduct.Add('sdxReportGroupStandardLookAndFeel', @sdxReportGroupStandardLookAndFeel);
  AProduct.Add('sdxReportGroupOfficeLookAndFeel', @sdxReportGroupOfficeLookAndFeel);
  AProduct.Add('sdxReportGroupWebLookAndFeel', @sdxReportGroupWebLookAndFeel);
  AProduct.Add('sdxLayoutGroupDefaultCaption', @sdxLayoutGroupDefaultCaption);
  AProduct.Add('sdxLayoutItemDefaultCaption', @sdxLayoutItemDefaultCaption);
  AProduct.Add('sdxTabs', @sdxTabs);
  AProduct.Add('sdxUnwrapTabs', @sdxUnwrapTabs);
  AProduct.Add('sdxActiveTabToTop', @sdxActiveTabToTop);
  AProduct.Add('sdxBehaviorsGroups', @sdxBehaviorsGroups);
  AProduct.Add('sdxSkipEmptyGroups', @sdxSkipEmptyGroups);
  AProduct.Add('sdxExpandedGroups', @sdxExpandedGroups);
  AProduct.Add('sdxCarManufacturerName5', @sdxCarManufacturerName5);
  AProduct.Add('sdxCarManufacturerCountry5', @sdxCarManufacturerCountry5);
  AProduct.Add('sdxCarModel5', @sdxCarModel5);
  AProduct.Add('sdxLuxurySedans', @sdxLuxurySedans);
  AProduct.Add('sdxCarManufacturer', @sdxCarManufacturer);
  AProduct.Add('sdxCarModel', @sdxCarModel);
  AProduct.Add('sdxCarEngine', @sdxCarEngine);
  AProduct.Add('sdxCarTransmission', @sdxCarTransmission);
  AProduct.Add('sdxCarTires', @sdxCarTires);
  AProduct.Add('sdx760V12Manufacturer', @sdx760V12Manufacturer);
  AProduct.Add('sdx760V12Model', @sdx760V12Model);
  AProduct.Add('sdx760V12Engine', @sdx760V12Engine);
  AProduct.Add('sdx760V12Transmission', @sdx760V12Transmission);
  AProduct.Add('sdx760V12Tires', @sdx760V12Tires);
  AProduct.Add('sdxBandBackgroundStyle', @sdxBandBackgroundStyle);
  AProduct.Add('sdxBandHeaderStyle', @sdxBandHeaderStyle);
  AProduct.Add('sdxCaptionStyle', @sdxCaptionStyle);
  AProduct.Add('sdxCardCaptionRowStyle', @sdxCardCaptionRowStyle);
  AProduct.Add('sdxCardRowCaptionStyle', @sdxCardRowCaptionStyle);
  AProduct.Add('sdxCategoryStyle', @sdxCategoryStyle);
  AProduct.Add('sdxContentStyle', @sdxContentStyle);
  AProduct.Add('sdxContentEvenStyle', @sdxContentEvenStyle);
  AProduct.Add('sdxContentOddStyle', @sdxContentOddStyle);
  AProduct.Add('sdxFilterBarStyle', @sdxFilterBarStyle);
  AProduct.Add('sdxFooterStyle', @sdxFooterStyle);
  AProduct.Add('sdxFooterRowStyle', @sdxFooterRowStyle);
  AProduct.Add('sdxGroupStyle', @sdxGroupStyle);
  AProduct.Add('sdxHeaderStyle', @sdxHeaderStyle);
  AProduct.Add('sdxIndentStyle', @sdxIndentStyle);
  AProduct.Add('sdxPreviewStyle', @sdxPreviewStyle);
  AProduct.Add('sdxSelectionStyle', @sdxSelectionStyle);
  AProduct.Add('sdxStyles', @sdxStyles);
  AProduct.Add('sdxStyleSheets', @sdxStyleSheets);
  AProduct.Add('sdxBtnTexture', @sdxBtnTexture);
  AProduct.Add('sdxBtnTextureClear', @sdxBtnTextureClear);
  AProduct.Add('sdxBtnColor', @sdxBtnColor);
  AProduct.Add('sdxBtnSaveAs', @sdxBtnSaveAs);
  AProduct.Add('sdxBtnRename', @sdxBtnRename);
  AProduct.Add('sdxLoadBitmapDlgTitle', @sdxLoadBitmapDlgTitle);
  AProduct.Add('sdxDeleteStyleSheet', @sdxDeleteStyleSheet);
  AProduct.Add('sdxUnnamedStyleSheet', @sdxUnnamedStyleSheet);
  AProduct.Add('sdxCreateNewStyleQueryNamePrompt', @sdxCreateNewStyleQueryNamePrompt);
  AProduct.Add('sdxStyleSheetNameAlreadyExists', @sdxStyleSheetNameAlreadyExists);
  AProduct.Add('sdxCannotLoadImage', @sdxCannotLoadImage);
  AProduct.Add('sdxUseNativeStyles', @sdxUseNativeStyles);
  AProduct.Add('sdxSuppressBackgroundBitmaps', @sdxSuppressBackgroundBitmaps);
  AProduct.Add('sdxConsumeSelectionStyle', @sdxConsumeSelectionStyle);
  AProduct.Add('sdxSize', @sdxSize);
  AProduct.Add('sdxLevels', @sdxLevels);
  AProduct.Add('sdxUnwrap', @sdxUnwrap);
  AProduct.Add('sdxUnwrapTopLevel', @sdxUnwrapTopLevel);
  AProduct.Add('sdxRiseActiveToTop', @sdxRiseActiveToTop);
  AProduct.Add('sdxCannotUseOnEveryPageModeInAggregatedState', @sdxCannotUseOnEveryPageModeInAggregatedState);
  AProduct.Add('sdxPagination', @sdxPagination);
  AProduct.Add('sdxByBands', @sdxByBands);
  AProduct.Add('sdxByColumns', @sdxByColumns);
  AProduct.Add('sdxByRows', @sdxByRows);
  AProduct.Add('sdxByTopLevelGroups', @sdxByTopLevelGroups);
  AProduct.Add('sdxOneGroupPerPage', @sdxOneGroupPerPage);
  AProduct.Add('sdxSkipEmptyViews', @sdxSkipEmptyViews);
  AProduct.Add('sdxBorders', @sdxBorders);
  AProduct.Add('sdxExplicitlyExpandNodes', @sdxExplicitlyExpandNodes);
  AProduct.Add('sdxNodes', @sdxNodes);
  AProduct.Add('sdxSeparators', @sdxSeparators);
  AProduct.Add('sdxThickness', @sdxThickness);
  AProduct.Add('sdxTLIncorrectHeadersState', @sdxTLIncorrectHeadersState);
  AProduct.Add('sdxRows', @sdxRows);
  AProduct.Add('sdxMultipleRecords', @sdxMultipleRecords);
  AProduct.Add('sdxBestFit', @sdxBestFit);
  AProduct.Add('sdxKeepSameRecordWidths', @sdxKeepSameRecordWidths);
  AProduct.Add('sdxWrapRecords', @sdxWrapRecords);
  AProduct.Add('sdxByWrapping', @sdxByWrapping);
  AProduct.Add('sdxOneWrappingPerPage', @sdxOneWrappingPerPage);
  AProduct.Add('sdxCurrentRecord', @sdxCurrentRecord);
  AProduct.Add('sdxLoadedRecords', @sdxLoadedRecords);
  AProduct.Add('sdxAllRecords', @sdxAllRecords);
  AProduct.Add('sdxPaginateByControlDetails', @sdxPaginateByControlDetails);
  AProduct.Add('sdxPaginateByControls', @sdxPaginateByControls);
  AProduct.Add('sdxPaginateByGroups', @sdxPaginateByGroups);
  AProduct.Add('sdxPaginateByItems', @sdxPaginateByItems);
  AProduct.Add('sdxControlsPlace', @sdxControlsPlace);
  AProduct.Add('sdxExpandHeight', @sdxExpandHeight);
  AProduct.Add('sdxExpandWidth', @sdxExpandWidth);
  AProduct.Add('sdxShrinkHeight', @sdxShrinkHeight);
  AProduct.Add('sdxShrinkWidth', @sdxShrinkWidth);
  AProduct.Add('sdxCheckAll', @sdxCheckAll);
  AProduct.Add('sdxCheckAllChildren', @sdxCheckAllChildren);
  AProduct.Add('sdxControlsTab', @sdxControlsTab);
  AProduct.Add('sdxExpandAll', @sdxExpandAll);
  AProduct.Add('sdxHiddenControlsTab', @sdxHiddenControlsTab);
  AProduct.Add('sdxReportLinksTab', @sdxReportLinksTab);
  AProduct.Add('sdxAvailableLinks', @sdxAvailableLinks);
  AProduct.Add('sdxAggregatedLinks', @sdxAggregatedLinks);
  AProduct.Add('sdxTransparents', @sdxTransparents);
  AProduct.Add('sdxUncheckAllChildren', @sdxUncheckAllChildren);
  AProduct.Add('sdxRoot', @sdxRoot);
  AProduct.Add('sdxRootBorders', @sdxRootBorders);
  AProduct.Add('sdxControls', @sdxControls);
  AProduct.Add('sdxContainers', @sdxContainers);
  AProduct.Add('sdxHideCustomContainers', @sdxHideCustomContainers);
  AProduct.Add('sdxBytes', @sdxBytes);
  AProduct.Add('sdxKiloBytes', @sdxKiloBytes);
  AProduct.Add('sdxMegaBytes', @sdxMegaBytes);
  AProduct.Add('sdxGigaBytes', @sdxGigaBytes);
  AProduct.Add('sdxThereIsNoPictureToDisplay', @sdxThereIsNoPictureToDisplay);
  AProduct.Add('sdxInvalidRootDirectory', @sdxInvalidRootDirectory);
  AProduct.Add('sdxPressEscToCancel', @sdxPressEscToCancel);
  AProduct.Add('sdxMenuFileRebuild', @sdxMenuFileRebuild);
  AProduct.Add('sdxBuildingReportStatusText', @sdxBuildingReportStatusText);
  AProduct.Add('sdxPrintingReportStatusText', @sdxPrintingReportStatusText);
  AProduct.Add('sdxBuiltIn', @sdxBuiltIn);
  AProduct.Add('sdxUserDefined', @sdxUserDefined);
  AProduct.Add('sdxNewStyleRepositoryWasCreated', @sdxNewStyleRepositoryWasCreated);
  AProduct.Add('sdxLineSpacing', @sdxLineSpacing);
  AProduct.Add('sdxTextAlignJustified', @sdxTextAlignJustified);
  AProduct.Add('sdxSampleText', @sdxSampleText);
  AProduct.Add('sdxCardsRows', @sdxCardsRows);
  AProduct.Add('sdxTransparentRichEdits', @sdxTransparentRichEdits);
  AProduct.Add('sdxIncorrectFilterBarState', @sdxIncorrectFilterBarState);
  AProduct.Add('sdxIncorrectBandHeadersState2', @sdxIncorrectBandHeadersState2);
  AProduct.Add('sdxIncorrectHeadersState2', @sdxIncorrectHeadersState2);
  AProduct.Add('sdxAvailableReportLinks', @sdxAvailableReportLinks);
  AProduct.Add('sdxBtnRemoveInconsistents', @sdxBtnRemoveInconsistents);
  AProduct.Add('sdxRowHeadersOnEveryPage', @sdxRowHeadersOnEveryPage);
  AProduct.Add('sdxColumnHeadersOnEveryPage', @sdxColumnHeadersOnEveryPage);
  AProduct.Add('sdxNotes', @sdxNotes);
  AProduct.Add('sdxTaskPad', @sdxTaskPad);
  AProduct.Add('sdxPrimaryTimeZone', @sdxPrimaryTimeZone);
  AProduct.Add('sdxSecondaryTimeZone', @sdxSecondaryTimeZone);
  AProduct.Add('sdxDay', @sdxDay);
  AProduct.Add('sdxWeek', @sdxWeek);
  AProduct.Add('sdxMonth', @sdxMonth);
  AProduct.Add('sdxSchedulerSchedulerHeader', @sdxSchedulerSchedulerHeader);
  AProduct.Add('sdxSchedulerContent', @sdxSchedulerContent);
  AProduct.Add('sdxSchedulerDateNavigatorContent', @sdxSchedulerDateNavigatorContent);
  AProduct.Add('sdxSchedulerDateNavigatorHeader', @sdxSchedulerDateNavigatorHeader);
  AProduct.Add('sdxSchedulerDayHeader', @sdxSchedulerDayHeader);
  AProduct.Add('sdxSchedulerEvent', @sdxSchedulerEvent);
  AProduct.Add('sdxSchedulerResourceHeader', @sdxSchedulerResourceHeader);
  AProduct.Add('sdxSchedulerNotesAreaBlank', @sdxSchedulerNotesAreaBlank);
  AProduct.Add('sdxSchedulerNotesAreaLined', @sdxSchedulerNotesAreaLined);
  AProduct.Add('sdxSchedulerTaskPad', @sdxSchedulerTaskPad);
  AProduct.Add('sdxSchedulerTimeRuler', @sdxSchedulerTimeRuler);
  AProduct.Add('sdxPrintStyleNameDaily', @sdxPrintStyleNameDaily);
  AProduct.Add('sdxPrintStyleNameWeekly', @sdxPrintStyleNameWeekly);
  AProduct.Add('sdxPrintStyleNameMonthly', @sdxPrintStyleNameMonthly);
  AProduct.Add('sdxPrintStyleNameDetails', @sdxPrintStyleNameDetails);
  AProduct.Add('sdxPrintStyleNameMemo', @sdxPrintStyleNameMemo);
  AProduct.Add('sdxPrintStyleNameTrifold', @sdxPrintStyleNameTrifold);
  AProduct.Add('sdxPrintStyleCaptionDaily', @sdxPrintStyleCaptionDaily);
  AProduct.Add('sdxPrintStyleCaptionWeekly', @sdxPrintStyleCaptionWeekly);
  AProduct.Add('sdxPrintStyleCaptionMonthly', @sdxPrintStyleCaptionMonthly);
  AProduct.Add('sdxPrintStyleCaptionDetails', @sdxPrintStyleCaptionDetails);
  AProduct.Add('sdxPrintStyleCaptionMemo', @sdxPrintStyleCaptionMemo);
  AProduct.Add('sdxPrintStyleCaptionTimeLine', @sdxPrintStyleCaptionTimeLine);
  AProduct.Add('sdxPrintStyleCaptionTrifold', @sdxPrintStyleCaptionTrifold);
  AProduct.Add('sdxPrintStyleCaptionYearly', @sdxPrintStyleCaptionYearly);
  AProduct.Add('sdxPrintStyleShowEventImages', @sdxPrintStyleShowEventImages);
  AProduct.Add('sdxPrintStyleShowResourceImages', @sdxPrintStyleShowResourceImages);
  AProduct.Add('sdxTabPrintStyles', @sdxTabPrintStyles);
  AProduct.Add('sdxPrintStyleDontPrintWeekEnds', @sdxPrintStyleDontPrintWeekEnds);
  AProduct.Add('sdxPrintStyleWorkTimeOnly', @sdxPrintStyleWorkTimeOnly);
  AProduct.Add('sdxPrintStyleInclude', @sdxPrintStyleInclude);
  AProduct.Add('sdxPrintStyleIncludeTaskPad', @sdxPrintStyleIncludeTaskPad);
  AProduct.Add('sdxPrintStyleIncludeNotesAreaBlank', @sdxPrintStyleIncludeNotesAreaBlank);
  AProduct.Add('sdxPrintStyleIncludeNotesAreaLined', @sdxPrintStyleIncludeNotesAreaLined);
  AProduct.Add('sdxPrintStyleLayout', @sdxPrintStyleLayout);
  AProduct.Add('sdxPrintStylePrintFrom', @sdxPrintStylePrintFrom);
  AProduct.Add('sdxPrintStylePrintTo', @sdxPrintStylePrintTo);
  AProduct.Add('sdxPrintStyleDailyLayout1PPD', @sdxPrintStyleDailyLayout1PPD);
  AProduct.Add('sdxPrintStyleDailyLayout2PPD', @sdxPrintStyleDailyLayout2PPD);
  AProduct.Add('sdxPrintStyleWeeklyArrange', @sdxPrintStyleWeeklyArrange);
  AProduct.Add('sdxPrintStyleWeeklyArrangeT2B', @sdxPrintStyleWeeklyArrangeT2B);
  AProduct.Add('sdxPrintStyleWeeklyArrangeL2R', @sdxPrintStyleWeeklyArrangeL2R);
  AProduct.Add('sdxPrintStyleWeeklyLayout1PPW', @sdxPrintStyleWeeklyLayout1PPW);
  AProduct.Add('sdxPrintStyleWeeklyLayout2PPW', @sdxPrintStyleWeeklyLayout2PPW);
  AProduct.Add('sdxPrintStyleWeeklyDaysLayout', @sdxPrintStyleWeeklyDaysLayout);
  AProduct.Add('sdxPrintStyleWeeklyDaysLayoutTC', @sdxPrintStyleWeeklyDaysLayoutTC);
  AProduct.Add('sdxPrintStyleWeeklyDaysLayoutOC', @sdxPrintStyleWeeklyDaysLayoutOC);
  AProduct.Add('sdxPrintStyleMemoStartEachItemOnNewPage', @sdxPrintStyleMemoStartEachItemOnNewPage);
  AProduct.Add('sdxPrintStyleMemoPrintOnlySelectedEvents', @sdxPrintStyleMemoPrintOnlySelectedEvents);
  AProduct.Add('sdxPrintStyleMonthlyLayout1PPM', @sdxPrintStyleMonthlyLayout1PPM);
  AProduct.Add('sdxPrintStyleMonthlyLayout2PPM', @sdxPrintStyleMonthlyLayout2PPM);
  AProduct.Add('sdxPrintStyleMonthlyPrintExactly1MPP', @sdxPrintStyleMonthlyPrintExactly1MPP);
  AProduct.Add('sdxPrintStyleTrifoldSectionModeDailyCalendar', @sdxPrintStyleTrifoldSectionModeDailyCalendar);
  AProduct.Add('sdxPrintStyleTrifoldSectionModeWeeklyCalendar', @sdxPrintStyleTrifoldSectionModeWeeklyCalendar);
  AProduct.Add('sdxPrintStyleTrifoldSectionModeMonthlyCalendar', @sdxPrintStyleTrifoldSectionModeMonthlyCalendar);
  AProduct.Add('sdxPrintStyleTrifoldSectionModeTaskPad', @sdxPrintStyleTrifoldSectionModeTaskPad);
  AProduct.Add('sdxPrintStyleTrifoldSectionModeNotesBlank', @sdxPrintStyleTrifoldSectionModeNotesBlank);
  AProduct.Add('sdxPrintStyleTrifoldSectionModeNotesLined', @sdxPrintStyleTrifoldSectionModeNotesLined);
  AProduct.Add('sdxPrintStyleTrifoldSectionLeft', @sdxPrintStyleTrifoldSectionLeft);
  AProduct.Add('sdxPrintStyleTrifoldSectionMiddle', @sdxPrintStyleTrifoldSectionMiddle);
  AProduct.Add('sdxPrintStyleTrifoldSectionRight', @sdxPrintStyleTrifoldSectionRight);
  AProduct.Add('sdxPrintStyleMonthPerPage', @sdxPrintStyleMonthPerPage);
  AProduct.Add('sdxPrintStyleYearly1MPP', @sdxPrintStyleYearly1MPP);
  AProduct.Add('sdxPrintStyleYearly2MPP', @sdxPrintStyleYearly2MPP);
  AProduct.Add('sdxPrintStyleYearly3MPP', @sdxPrintStyleYearly3MPP);
  AProduct.Add('sdxPrintStyleYearly4MPP', @sdxPrintStyleYearly4MPP);
  AProduct.Add('sdxPrintStyleYearly6MPP', @sdxPrintStyleYearly6MPP);
  AProduct.Add('sdxPrintStyleYearly12MPP', @sdxPrintStyleYearly12MPP);
  AProduct.Add('sdxPrintStylePrimaryPageScalesOnly', @sdxPrintStylePrimaryPageScalesOnly);
  AProduct.Add('sdxPrintStylePrimaryPageHeadersOnly', @sdxPrintStylePrimaryPageHeadersOnly);
  AProduct.Add('sdxPrintStyleDetailsStartNewPageEach', @sdxPrintStyleDetailsStartNewPageEach);
  AProduct.Add('sdxSuppressContentColoration', @sdxSuppressContentColoration);
  AProduct.Add('sdxOneResourcePerPage', @sdxOneResourcePerPage);
  AProduct.Add('sdxPrintRanges', @sdxPrintRanges);
  AProduct.Add('sdxPrintRangeStart', @sdxPrintRangeStart);
  AProduct.Add('sdxPrintRangeEnd', @sdxPrintRangeEnd);
  AProduct.Add('sdxHideDetailsOfPrivateAppointments', @sdxHideDetailsOfPrivateAppointments);
  AProduct.Add('sdxResourceCountPerPage', @sdxResourceCountPerPage);
  AProduct.Add('sdxSubjectLabelCaption', @sdxSubjectLabelCaption);
  AProduct.Add('sdxLocationLabelCaption', @sdxLocationLabelCaption);
  AProduct.Add('sdxStartLabelCaption', @sdxStartLabelCaption);
  AProduct.Add('sdxFinishLabelCaption', @sdxFinishLabelCaption);
  AProduct.Add('sdxShowTimeAsLabelCaption', @sdxShowTimeAsLabelCaption);
  AProduct.Add('sdxRecurrenceLabelCaption', @sdxRecurrenceLabelCaption);
  AProduct.Add('sdxRecurrencePatternLabelCaption', @sdxRecurrencePatternLabelCaption);
  AProduct.Add('sdxSeeAboveMessage', @sdxSeeAboveMessage);
  AProduct.Add('sdxAllDayMessage', @sdxAllDayMessage);
  AProduct.Add('sdxContinuedMessage', @sdxContinuedMessage);
  AProduct.Add('sdxShowTimeAsFreeMessage', @sdxShowTimeAsFreeMessage);
  AProduct.Add('sdxShowTimeAsTentativeMessage', @sdxShowTimeAsTentativeMessage);
  AProduct.Add('sdxShowTimeAsOutOfOfficeMessage', @sdxShowTimeAsOutOfOfficeMessage);
  AProduct.Add('sdxRecurrenceNoneMessage', @sdxRecurrenceNoneMessage);
  AProduct.Add('scxRecurrenceDailyMessage', @scxRecurrenceDailyMessage);
  AProduct.Add('scxRecurrenceWeeklyMessage', @scxRecurrenceWeeklyMessage);
  AProduct.Add('scxRecurrenceMonthlyMessage', @scxRecurrenceMonthlyMessage);
  AProduct.Add('scxRecurrenceYearlyMessage', @scxRecurrenceYearlyMessage);
  AProduct.Add('sdxInconsistentTrifoldStyle', @sdxInconsistentTrifoldStyle);
  AProduct.Add('sdxBadTimePrintRange', @sdxBadTimePrintRange);
  AProduct.Add('sdxBadDatePrintRange', @sdxBadDatePrintRange);
  AProduct.Add('sdxCannotPrintNoSelectedItems', @sdxCannotPrintNoSelectedItems);
  AProduct.Add('sdxCannotPrintNoItemsAvailable', @sdxCannotPrintNoItemsAvailable);
  AProduct.Add('sdxColumnFields', @sdxColumnFields);
  AProduct.Add('sdxDataFields', @sdxDataFields);
  AProduct.Add('sdxFiterFields', @sdxFiterFields);
  AProduct.Add('sdxPrefilter', @sdxPrefilter);
  AProduct.Add('sdxRowFields', @sdxRowFields);
  AProduct.Add('sdxAutoColumnsExpand', @sdxAutoColumnsExpand);
  AProduct.Add('sdxAutoRowsExpand', @sdxAutoRowsExpand);
  AProduct.Add('sdxPivotGridColumnHeader', @sdxPivotGridColumnHeader);
  AProduct.Add('sdxPivotGridContent', @sdxPivotGridContent);
  AProduct.Add('sdxPivotGridFieldHeader', @sdxPivotGridFieldHeader);
  AProduct.Add('sdxPivotGridHeaderBackground', @sdxPivotGridHeaderBackground);
  AProduct.Add('sdxPivotGridRowHeader', @sdxPivotGridRowHeader);
  AProduct.Add('sdxPivotGridPrefilter', @sdxPivotGridPrefilter);
  AProduct.Add('sdxUnitPrice', @sdxUnitPrice);
  AProduct.Add('sdxCarName', @sdxCarName);
  AProduct.Add('sdxQuantity', @sdxQuantity);
  AProduct.Add('sdxPaymentAmount', @sdxPaymentAmount);
  AProduct.Add('sdxPurchaseQuarter', @sdxPurchaseQuarter);
  AProduct.Add('sdxPurchaseMonth', @sdxPurchaseMonth);
  AProduct.Add('sdxPaymentType', @sdxPaymentType);
  AProduct.Add('sdxCompanyName', @sdxCompanyName);
end;

procedure AddResourceStringsForPDFDialog(AProduct: TdxProductResourceStrings);
begin
  AProduct.Add('sdxPDFDialogAuthor', @sdxPDFDialogAuthor);
  AProduct.Add('sdxPDFDialogCaption', @sdxPDFDialogCaption);
  AProduct.Add('sdxPDFDialogCompressed', @sdxPDFDialogCompressed);
  AProduct.Add('sdxPDFDialogCreator', @sdxPDFDialogCreator);
  AProduct.Add('sdxPDFDialogDocumentInfoTabSheet', @sdxPDFDialogDocumentInfoTabSheet);
  AProduct.Add('sdxPDFDialogEmbedFonts', @sdxPDFDialogEmbedFonts);
  AProduct.Add('sdxPDFDialogExportSettings', @sdxPDFDialogExportSettings);
  AProduct.Add('sdxPDFDialogExportTabSheet', @sdxPDFDialogExportTabSheet);
  AProduct.Add('sdxPDFDialogKeywords', @sdxPDFDialogKeywords);
  AProduct.Add('sdxPDFDialogMaxCompression', @sdxPDFDialogMaxCompression);
  AProduct.Add('sdxPDFDialogMaxQuality', @sdxPDFDialogMaxQuality);
  AProduct.Add('sdxPDFDialogOpenAfterExport', @sdxPDFDialogOpenAfterExport);
  AProduct.Add('sdxPDFDialogPageRageTabSheet', @sdxPDFDialogPageRageTabSheet);
  AProduct.Add('sdxPDFDialogSecurityAllowChanging', @sdxPDFDialogSecurityAllowChanging);
  AProduct.Add('sdxPDFDialogSecurityAllowComments', @sdxPDFDialogSecurityAllowComments);
  AProduct.Add('sdxPDFDialogSecurityAllowCopy', @sdxPDFDialogSecurityAllowCopy);
  AProduct.Add('sdxPDFDialogSecurityAllowDocumentAssemble', @sdxPDFDialogSecurityAllowDocumentAssemble);
  AProduct.Add('sdxPDFDialogSecurityAllowPrint', @sdxPDFDialogSecurityAllowPrint);
  AProduct.Add('sdxPDFDialogSecurityAllowPrintHiResolution', @sdxPDFDialogSecurityAllowPrintHiResolution);
  AProduct.Add('sdxPDFDialogSecurityEnabled', @sdxPDFDialogSecurityEnabled);
  AProduct.Add('sdxPDFDialogSecurityMethod', @sdxPDFDialogSecurityMethod);
  AProduct.Add('sdxPDFDialogSecurityOwnerPassword', @sdxPDFDialogSecurityOwnerPassword);
  AProduct.Add('sdxPDFDialogSecuritySettings', @sdxPDFDialogSecuritySettings);
  AProduct.Add('sdxPDFDialogSecurityUserPassword', @sdxPDFDialogSecurityUserPassword);
  AProduct.Add('sdxPDFDialogSubject', @sdxPDFDialogSubject);
  AProduct.Add('sdxPDFDialogTabDocInfo', @sdxPDFDialogTabDocInfo);
  AProduct.Add('sdxPDFDialogTabExport', @sdxPDFDialogTabExport);
  AProduct.Add('sdxPDFDialogTabPages', @sdxPDFDialogTabPages);
  AProduct.Add('sdxPDFDialogTabSecurity', @sdxPDFDialogTabSecurity);
  AProduct.Add('sdxPDFDialogTitle', @sdxPDFDialogTitle);
  AProduct.Add('sdxPDFDialogUseCIDFonts', @sdxPDFDialogUseCIDFonts);
  AProduct.Add('sdxPDFDialogUseJPEGCompression', @sdxPDFDialogUseJPEGCompression);
end;

procedure AddResourceStringsForRibbonPreview(AProduct: TdxProductResourceStrings);
begin
  AProduct.Add('sdxRibbonPrintPreviewClosePrintPreview', @sdxRibbonPrintPreviewClosePrintPreview);
  AProduct.Add('sdxRibbonPrintPreviewGroupFormat', @sdxRibbonPrintPreviewGroupFormat);
  AProduct.Add('sdxRibbonPrintPreviewGroupInsertName', @sdxRibbonPrintPreviewGroupInsertName);
  AProduct.Add('sdxRibbonPrintPreviewGroupInsertPageNumber', @sdxRibbonPrintPreviewGroupInsertPageNumber);
  AProduct.Add('sdxRibbonPrintPreviewGroupNavigation', @sdxRibbonPrintPreviewGroupNavigation);
  AProduct.Add('sdxRibbonPrintPreviewGroupOutput', @sdxRibbonPrintPreviewGroupOutput);
  AProduct.Add('sdxRibbonPrintPreviewGroupParts', @sdxRibbonPrintPreviewGroupParts);
  AProduct.Add('sdxRibbonPrintPreviewGroupReport', @sdxRibbonPrintPreviewGroupReport);
  AProduct.Add('sdxRibbonPrintPreviewGroupZoom', @sdxRibbonPrintPreviewGroupZoom);
  AProduct.Add('sdxRibbonPrintPreviewPagesSubItem', @sdxRibbonPrintPreviewPagesSubItem);
end;

procedure AddExpressPrintingSystemResourceStringNames(AProduct: TdxProductResourceStrings);
begin
  // Split into parts because D12 compiler error
  AddResourceStringsPart1(AProduct);
  AddResourceStringsPart2(AProduct);
  AddResourceStringsPart3(AProduct);
  AddResourceStringsForPDFDialog(AProduct);
  AddResourceStringsForRibbonPreview(AProduct);
end;

initialization
  dxResourceStringsRepository.RegisterProduct('ExpressPrinting System', @AddExpressPrintingSystemResourceStringNames);

finalization
  dxResourceStringsRepository.UnRegisterProduct('ExpressPrinting System');

end.

