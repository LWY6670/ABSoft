object ABVideoCapFrame: TABVideoCapFrame
  Left = 0
  Top = 0
  Width = 443
  Height = 300
  Align = alClient
  TabOrder = 0
  ExplicitWidth = 451
  ExplicitHeight = 304
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 443
    Height = 300
    ActivePage = TabSheet3
    Align = alClient
    TabOrder = 0
    OnChanging = PageControl1Changing
    ExplicitWidth = 451
    ExplicitHeight = 304
    object TabSheet1: TTabSheet
      Caption = #25668#20687#31383#21475
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel2: TPanel
        Left = 0
        Top = 236
        Width = 443
        Height = 40
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 0
        object Button_From: TButton
          Left = 86
          Top = 8
          Width = 40
          Height = 25
          Caption = #26469#28304
          TabOrder = 2
          OnClick = Button_FromClick
        end
        object Button_Format: TButton
          Left = 125
          Top = 8
          Width = 40
          Height = 25
          Caption = #26684#24335
          TabOrder = 3
          OnClick = Button_FormatClick
        end
        object Button_Zip: TButton
          Left = 164
          Top = 8
          Width = 40
          Height = 25
          Caption = #21387#32553
          TabOrder = 4
          OnClick = Button_ZipClick
        end
        object Button_StartAvi: TButton
          Left = 253
          Top = 8
          Width = 61
          Height = 25
          Caption = #24320#22987#24405#20687
          TabOrder = 6
          OnClick = Button_StartAviClick
        end
        object Button_StopAvi: TButton
          Left = 313
          Top = 8
          Width = 61
          Height = 25
          Caption = #20572#27490#24405#20687
          TabOrder = 7
          OnClick = Button_StopAviClick
        end
        object Button_Open: TButton
          Left = 4
          Top = 8
          Width = 40
          Height = 25
          Caption = #21551#21160
          TabOrder = 0
          OnClick = Button_OpenClick
        end
        object Button_Close: TButton
          Left = 43
          Top = 8
          Width = 40
          Height = 25
          Caption = #20851#38381
          TabOrder = 1
          OnClick = Button_CloseClick
        end
        object Button_GetPicture: TButton
          Left = 209
          Top = 8
          Width = 40
          Height = 25
          Caption = #25293#29031
          TabOrder = 5
          OnClick = Button_GetPictureClick
        end
      end
      object VideoCap1: TVideoCap
        Left = 0
        Top = 0
        Width = 443
        Height = 236
        align = alClient
        color = clBlack
        DriverOpen = False
        DriverIndex = -1
        VideoOverlay = False
        VideoPreview = False
        PreviewScaleToWindow = True
        PreviewScaleProportional = True
        PreviewRate = 30
        MicroSecPerFrame = 66667
        FrameRate = 15
        CapAudio = False
        VideoFileName = 'tempVideoCapAviFile.avi'
        SingleImageFile = 'tempVideoCapPictureFile.bmp'
        CapTimeLimit = 0
        CapIndexSize = 0
        CapToFile = True
        CapAudioFormat.Channels = Stereo
        BufferFileSize = 0
        OnDblClick = VideoCap1DblClick
      end
    end
    object TabSheet2: TTabSheet
      Caption = #35009#21098#31383#21475
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel4: TPanel
        Left = 293
        Top = 0
        Width = 142
        Height = 272
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitLeft = 301
        ExplicitHeight = 276
        object Panel3: TPanel
          Left = 0
          Top = 216
          Width = 142
          Height = 56
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 1
          ExplicitTop = 220
          object ComboBox2: TComboBox
            Left = 6
            Top = 5
            Width = 133
            Height = 19
            Style = csOwnerDrawFixed
            Ctl3D = False
            ItemHeight = 13
            ItemIndex = 2
            ParentCtl3D = False
            TabOrder = 0
            Text = '3-'#25668#20687#26426#20840#23631
            OnChange = ComboBox2Change
            Items.Strings = (
              '1-'#21333#23544#29031#29255
              '2-'#20108#23544#29031#29255
              '3-'#25668#20687#26426#20840#23631)
          end
          object Button_CutOut: TButton
            Left = 49
            Top = 28
            Width = 45
            Height = 25
            Caption = #35009#21098
            TabOrder = 2
            OnClick = Button_CutOutClick
          end
          object Button_GetPicture2: TButton
            Left = 3
            Top = 28
            Width = 45
            Height = 25
            Caption = #25293#29031
            TabOrder = 1
            OnClick = Button_GetPictureClick
          end
          object Button_Save: TButton
            Left = 95
            Top = 28
            Width = 45
            Height = 25
            Caption = #20445#23384
            TabOrder = 3
            OnClick = Button_SaveClick
          end
        end
        object GroupBox1: TGroupBox
          Left = 0
          Top = 0
          Width = 142
          Height = 216
          Align = alClient
          Caption = #35009#21098#39044#35272
          TabOrder = 0
          ExplicitHeight = 220
          object CutOutImage: TImage
            Left = 6
            Top = 15
            Width = 130
            Height = 197
            Center = True
            Stretch = True
          end
        end
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 293
        Height = 272
        Align = alClient
        Caption = #29031#29255#35009#21098
        Color = 15723503
        ParentColor = False
        TabOrder = 1
        ExplicitWidth = 301
        ExplicitHeight = 276
        object Panel1: TPanel
          Left = 2
          Top = 15
          Width = 297
          Height = 259
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object InputImage: TImage
            Left = 0
            Top = 0
            Width = 303
            Height = 265
            Center = True
            Stretch = True
          end
          object CutOutPanel: TPanel
            Left = 93
            Top = 48
            Width = 110
            Height = 155
            BevelOuter = bvNone
            BorderWidth = 2
            Ctl3D = True
            ParentBackground = False
            ParentCtl3D = False
            TabOrder = 0
            OnMouseDown = CutOutPanelMouseDown
          end
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = #27983#35272#31383#21475
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Image1: TImage
        Left = 0
        Top = 0
        Width = 443
        Height = 276
        Align = alClient
        ExplicitWidth = 435
        ExplicitHeight = 242
      end
    end
  end
end
