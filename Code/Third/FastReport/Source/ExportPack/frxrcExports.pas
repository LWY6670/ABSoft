unit frxrcExports;

interface
{$I frx.inc}
implementation
uses frxRes;
const resXML =
'<?xml version="1.0" encoding="utf-8" standalone="no"?><Resources CodePage="936">'+
'  <StrRes Name="8000" Text="导出到 Excel"/>  <StrRes Name="8001" Text="样式"/>  <StrR'+
'es Name="8002" Text="图像"/>  <StrRes Name="8003" Text="合并单元格"/>  <StrRes Name="80'+
'04" Text="快速导出"/>  <StrRes Name="8005" Text="所见即所得"/>  <StrRes Name="8006" Text='+
'"作为文本"/>  <StrRes Name="8007" Text="背景"/>  <StrRes Name="8008" Text="在导出后打开 Exce'+
'l"/>  <StrRes Name="8009" Text="Excel 文件 (*.xls)|*.xls"/>  <StrRes Name="8010" T'+
'ext=".xls"/>  <StrRes Name="8100" Text="导出到 Excel"/>  <StrRes Name="8101" Text="'+
'样式"/>  <StrRes Name="8102" Text="所见即所得"/>  <StrRes Name="8103" Text="背景"/>  <Str'+
'Res Name="8104" Text="在导出后打开 Excel"/>  <StrRes Name="8105" Text="XML Excel 文件 (*'+
'.xls)|*.xls"/>  <StrRes Name="8106" Text=".xls"/>  <StrRes Name="8200" Text="导出到'+
' HTML table"/>  <StrRes Name="8201" Text="在导出后打开"/>  <StrRes Name="8202" Text="样'+
'式"/>  <StrRes Name="8203" Text="图像"/>  <StrRes Name="8204" Text="全部在一数据夹"/>  <St'+
'rRes Name="8205" Text="固定宽度"/>  <StrRes Name="8206" Text="页面导览"/>  <StrRes Name='+
'"8207" Text="多页"/>  <StrRes Name="8208" Text="Mozilla 浏览器"/>  <StrRes Name="8209'+
'" Text="背景"/>  <StrRes Name="8210" Text="HTML 文件 (*.html)|*.html"/>  <StrRes Nam'+
'e="8211" Text=".html"/>  <StrRes Name="8300" Text="导出到纯文本 (点阵式打印机)"/>  <StrRes N'+
'ame="8301" Text="预览 启用/停用"/>  <StrRes Name="8302" Text=" 导出属性  "/>  <StrRes Name'+
'="8303" Text="分页"/>  <StrRes Name="8304" Text="OEM 页码"/>  <StrRes Name="8305" Te'+
'xt="空白行"/>  <StrRes Name="8306" Text="前置空白"/>  <StrRes Name="8307" Text="页数"/>  '+
'<StrRes Name="8308" Text="输入页数/文件范围，并以逗号分隔(例如:1,3,5-12)"/>  <StrRes Name="8309" '+
'Text=" 比率 "/>  <StrRes Name="8310" Text="X 比率"/>  <StrRes Name="8311" Text="Y 比率'+
'"/>  <StrRes Name="8312" Text=" 外框 "/>  <StrRes Name="8313" Text="无"/>  <StrRes '+
'Name="8314" Text="简单"/>  <StrRes Name="8315" Text="图像"/>  <StrRes Name="8316" Te'+
'xt="仅用OEM代码页"/>  <StrRes Name="8317" Text="在导出后打开"/>  <StrRes Name="8318" Text="'+
'保存设置"/>  <StrRes Name="8319" Text=" 预览 "/>  <StrRes Name="8320" Text="宽度:"/>  <S'+
'trRes Name="8321" Text="高度:"/>  <StrRes Name="8322" Text="页"/>  <StrRes Name="83'+
'23" Text="放大"/>  <StrRes Name="8324" Text="缩小"/>  <StrRes Name="8325" Text="文本文件'+
' (点阵式打印机)(*.prn)|*.prn"/>  <StrRes Name="8326" Text=".prn"/>  <StrRes Name="8400'+
'" Text="打印"/>  <StrRes Name="8401" Text="打印机"/>  <StrRes Name="8402" Text="名称"/>'+
'  <StrRes Name="8403" Text="设置..."/>  <StrRes Name="8404" Text="份数"/>  <StrRes N'+
'ame="8405" Text="份数"/>  <StrRes Name="8406" Text=" 打印机初始化设置 "/>  <StrRes Name="8'+
'407" Text="打印机类型"/>  <StrRes Name="8408" Text=".fpi"/>  <StrRes Name="8409" Text'+
'="打印机初始化样版 (*.fpi)|*.fpi"/>  <StrRes Name="8410" Text=".fpi"/>  <StrRes Name="84'+
'11" Text="打印机初始化样版 (*.fpi)|*.fpi"/>  <StrRes Name="8500" Text="导出到 RTF"/>  <StrR'+
'es Name="8501" Text="图像"/>  <StrRes Name="8502" Text="所见即所得"/>  <StrRes Name="85'+
'03" Text="在导出后打开"/>  <StrRes Name="8504" Text="RTF 文件 (*.rtf)|*.rtf"/>  <StrRes '+
'Name="8505" Text=".rtf"/>  <StrRes Name="8600" Text="导出设置"/>  <StrRes Name="8601'+
'" Text=" 图像设置 "/>  <StrRes Name="8602" Text="JPEG 品质"/>  <StrRes Name="8603" Tex'+
't="分辨率 (dpi)"/>  <StrRes Name="8604" Text="分割文件"/>  <StrRes Name="8605" Text="剪去'+
'页面空白部份"/>  <StrRes Name="8606" Text="黑白"/>  <StrRes Name="8700" Text="导出到 PDF"/>'+
'  <StrRes Name="8701" Text="PDF/A"/>  <StrRes Name="8702" Text="嵌入字体"/>  <StrRes'+
' Name="8703" Text="打印优化"/>  <StrRes Name="8704" Text="大纲"/>  <StrRes Name="8705"'+
' Text="背景"/>  <StrRes Name="8706" Text="在导出后打开"/>  <StrRes Name="8707" Text="Ado'+
'be PDF 文件 (*.pdf)|*.pdf"/>  <StrRes Name="8708" Text=".pdf"/>  <StrRes Name="RTF'+
'export" Text="RTF 文件"/>  <StrRes Name="BMPexport" Text="BMP 图"/>  <StrRes Name="'+
'JPEGexport" Text="JPEG 图"/>  <StrRes Name="TIFFexport" Text="TIFF 图"/>  <StrRes '+
'Name="TextExport" Text="纯文本 (点阵打印机)"/>  <StrRes Name="XlsOLEexport" Text="Excel '+
'表格 (OLE)"/>  <StrRes Name="HTMLexport" Text="HTML 文件"/>  <StrRes Name="XlsXMLexp'+
'ort" Text="Excel 表格 (XML)"/>  <StrRes Name="PDFexport" Text="PDF 文件"/>  <StrRes '+
'Name="ProgressWait" Text="请稍候"/>  <StrRes Name="ProgressRows" Text="正在建立列"/>  <S'+
'trRes Name="ProgressColumns" Text="正在建立栏"/>  <StrRes Name="ProgressStyles" Text='+
'"正在建立样式"/>  <StrRes Name="ProgressObjects" Text="正在导出对象中"/>  <StrRes Name="TIFFe'+
'xportFilter" Text="Tiff 图 (*.tif)|*.tif"/>  <StrRes Name="BMPexportFilter" Text='+
'"BMP 图 (*.bmp)|*.bmp"/>  <StrRes Name="JPEGexportFilter" Text="Jpeg 图 (*.jpg)|*.'+
'jpg"/>  <StrRes Name="HTMLNavFirst" Text="首页"/>  <StrRes Name="HTMLNavPrev" Text'+
'="前一页"/>  <StrRes Name="HTMLNavNext" Text="下一页"/>  <StrRes Name="HTMLNavLast" Te'+
'xt="末页"/>  <StrRes Name="HTMLNavRefresh" Text="刷新"/>  <StrRes Name="HTMLNavPrint'+
'" Text="打印"/>  <StrRes Name="HTMLNavTotal" Text="全部页"/>  <StrRes Name="8800" Tex'+
't="导出到文本"/>  <StrRes Name="8801" Text="Text file (*.txt)|*.txt"/>  <StrRes Name='+
'"8802" Text=".txt"/>  <StrRes Name="SimpleTextExport" Text="文本文件"/>  <StrRes Nam'+
'e="8850" Text="导出为CSV"/>  <StrRes Name="8851" Text="CSV file (*.csv)|*.csv"/>  <'+
'StrRes Name="8852" Text=".csv"/>  <StrRes Name="8853" Text="切分"/>  <StrRes Name='+
'"CSVExport" Text="CSV 文件"/>  <StrRes Name="8900" Text="通过E-mail发送"/>  <StrRes Na'+
'me="8901" Text="E-mail"/>  <StrRes Name="8902" Text="帐户"/>  <StrRes Name="8903" '+
'Text="连接"/>  <StrRes Name="8904" Text="地址"/>  <StrRes Name="8905" Text="附件"/>  <'+
'StrRes Name="8906" Text="格式"/>  <StrRes Name="8907" Text="发送地址"/>  <StrRes Name='+
'"8908" Text="发送人"/>  <StrRes Name="8909" Text="主机"/>  <StrRes Name="8910" Text="'+
'登录"/>  <StrRes Name="8911" Text="邮件"/>  <StrRes Name="8912" Text="通讯"/>  <StrRes'+
' Name="8913" Text="文本"/>  <StrRes Name="8914" Text="组织"/>  <StrRes Name="8915" T'+
'ext="密码"/>  <StrRes Name="8916" Text="端口"/>  <StrRes Name="8917" Text="记录属性"/>  '+
'<StrRes Name="8918" Text="请求未填充fields"/>  <StrRes Name="8919" Text="高级导出设置"/>  <'+
'StrRes Name="8920" Text="签名"/>  <StrRes Name="8921" Text="创建"/>  <StrRes Name="8'+
'922" Text="主题"/>  <StrRes Name="8923" Text="Best regards"/>  <StrRes Name="8924"'+
' Text="发送邮件到"/>  <StrRes Name="EmailExport" Text="E-mail"/>  <StrRes Name="FastR'+
'eportFile" Text="FastReport file"/>  <StrRes Name="GifexportFilter" Text="Gif fi'+
'le (*.gif)|*.gif"/>  <StrRes Name="GIFexport" Text="Gif 图像"/>  <StrRes Name="895'+
'0" Text="继续"/>  <StrRes Name="8951" Text="页头/尾"/>  <StrRes Name="8952" Text="文本"'+
'/>  <StrRes Name="8953" Text="头/尾"/>  <StrRes Name="8954" Text="空"/>  <StrRes Na'+
'me="ODSExportFilter" Text="打开电子表格文档文件 (*.ods)|*.ods"/>  <StrRes Name="ODSExport"'+
' Text="打开电子表格文档"/>  <StrRes Name="ODTExportFilter" Text="打开文本文件文档 (*.odt)|*.odt"'+
'/>  <StrRes Name="ODTExport" Text="打开文本文档"/>  <StrRes Name="8960" Text=".ods"/> '+
' <StrRes Name="8961" Text=".odt"/>  <StrRes Name="8962" Text="安全"/>  <StrRes Nam'+
'e="8963" Text="安全设置"/>  <StrRes Name="8964" Text="所有者密码"/>  <StrRes Name="8965" '+
'Text="用户密码"/>  <StrRes Name="8966" Text="打印文档"/>  <StrRes Name="8967" Text="修改文档'+
'"/>  <StrRes Name="8968" Text="文本和图形拷贝"/>  <StrRes Name="8969" Text="添加或修改文本注解"/'+
'>  <StrRes Name="8970" Text="PDF 安全"/>  <StrRes Name="8971" Text="Document infor'+
'mation"/>  <StrRes Name="8972" Text="Information"/>  <StrRes Name="8973" Text="T'+
'itle"/>  <StrRes Name="8974" Text="Author"/>  <StrRes Name="8975" Text="Subject"'+
'/>  <StrRes Name="8976" Text="Keywords"/>  <StrRes Name="8977" Text="Creator"/> '+
' <StrRes Name="8978" Text="Producer"/>  <StrRes Name="8979" Text="Authentificati'+
'on"/>  <StrRes Name="8980" Text="Permissions"/>  <StrRes Name="8981" Text="Viewe'+
'r"/>  <StrRes Name="8982" Text="Viewer preferences"/>  <StrRes Name="8983" Text='+
'"Hide toolbar"/>  <StrRes Name="8984" Text="Hide menubar"/>  <StrRes Name="8985"'+
' Text="Hide window user interface"/>  <StrRes Name="8986" Text="Fit window"/>  <'+
'StrRes Name="8987" Text="Center window"/>  <StrRes Name="8988" Text="Print scali'+
'ng"/>  <StrRes Name="8989" Text="Confirmation Reading"/>  <StrRes Name="9101" Te'+
'xt="Export to DBF"/>  <StrRes Name="9102" Text="dBase (DBF) export"/>  <StrRes N'+
'ame="9103" Text=".dbf"/>  <StrRes Name="9104" Text="Failed to load the file"/>  '+
'<StrRes Name="9105" Text="Failure"/>  <StrRes Name="9106" Text="Field names"/>  '+
'<StrRes Name="9107" Text="Automatically"/>  <StrRes Name="9108" Text="Manually"/'+
'>  <StrRes Name="9109" Text="Load from file"/>  <StrRes Name="9110" Text="Text f'+
'iles (*.txt)|*.txt|All files|*.*"/>  <StrRes Name="9111" Text="DBF files (*.dbf)'+
'|*.dbf|All files|*.*"/>  <StrRes Name="expMailAddr" Text="Address or addresses d'+
'elimited by comma"/>  <StrRes Name="PNGexport" Text="PNG image"/>  <StrRes Name='+
'"EMFexport" Text="EMF image"/>  <StrRes Name="PNGexportFilter" Text="PNG image ('+
'*.png)|*.png"/>  <StrRes Name="EMFexportFilter" Text="EMF image (*.emf)|*.emf"/>'+
'  <StrRes Name="9000" Text="Rows count:"/>  <StrRes Name="9001" Text="Split To S'+
'heet"/>  <StrRes Name="9002" Text="Don''t split"/>  <StrRes Name="9003" Text="Use'+
' report pages"/>  <StrRes Name="9004" Text="Use print on parent"/>  <StrRes Name'+
'="9151" Text="Excel 97/2000/XP file"/>  <StrRes Name="9152" Text="Auto create fi'+
'le"/>  <StrRes Name="9153" Text="Options"/>  <StrRes Name="9154" Text="Page"/>  '+
'<StrRes Name="9155" Text="Grid Lines"/>  <StrRes Name="9156" Text="All in one pa'+
'ge"/>  <StrRes Name="9157" Text="Data Grouping"/>  <StrRes Name="9158" Text="Lik'+
'e the report"/>  <StrRes Name="9159" Text="Chunks. Each chunk has (rows):"/>  <S'+
'trRes Name="9160" Text="Chunk size must be a non negative integer value."/>  <St'+
'rRes Name="9161" Text="SingleSheet must be False when exporting to chunks"/>  <S'+
'trRes Name="9162" Text="Author"/>  <StrRes Name="9163" Text="Comment"/>  <StrRes'+
' Name="9164" Text="Keywords"/>  <StrRes Name="9165" Text="Revision"/>  <StrRes N'+
'ame="9166" Text="Version"/>  <StrRes Name="9167" Text="Application"/>  <StrRes N'+
'ame="9168" Text="Subject"/>  <StrRes Name="9169" Text="Category"/>  <StrRes Name'+
'="9170" Text="Company"/>  <StrRes Name="9171" Text="Title"/>  <StrRes Name="9172'+
'" Text="Manager"/>  <StrRes Name="9173" Text="General"/>  <StrRes Name="9174" Te'+
'xt="Information"/>  <StrRes Name="9175" Text="Protection"/>  <StrRes Name="9176"'+
' Text="Password"/>  <StrRes Name="9177" Text="If you set a non empty password st'+
'ring, the generated document will be protected with this password. The password '+
'is always written in Unicode characters and must be shorter than 256 Unicode cha'+
'racters."/>  <StrRes Name="9178" Text="Confirmation"/>  <StrRes Name="9179" Text'+
'="Password confirmation does not match the password. Type the password again."/>'+
'  <StrRes Name="9180" Text="Attempting to set a password of %d characters. The m'+
'aximum allowed length of a password is %d characters."/>  <StrRes Name="9181" Te'+
'xt="Adjust page size"/>  <StrRes Name="9182" Text="Export to Excel 97/2000/XP"/>'+
'  <StrRes Name="9183" Text="Delete empty rows"/>  <StrRes Name="9184" Text="Expo'+
'rt formulas"/>  <StrRes Name="BiffCol" Text="Resizing columns"/>  <StrRes Name="'+
'BiffRow" Text="Resizing rows"/>  <StrRes Name="BiffCell" Text="Exporting cells"/'+
'>  <StrRes Name="BiffImg" Text="Exporting pictures"/>  <StrRes Name="9200" Text='+
'"Microsoft Excel 2007 XML"/>  <StrRes Name="9201" Text="Microsoft PowerPoint 200'+
'7 XML"/>  <StrRes Name="9203" Text="Microsoft Word 2007 XML"/>  <StrRes Name="93'+
'00" Text="HTML Layered"/>  <StrRes Name="9301" Text="HTML files (*.html)|*.html|'+
'All files|*.*"/>  <StrRes Name="9302" Text="HTML Layered Export"/>  <StrRes Name'+
'="9303" Text="HTML5 Layered"/>  <StrRes Name="9304" Text="HTML4 Layered"/>  <Str'+
'Res Name="9400" Text="Reordering components"/>  <StrRes Name="9401" Text="Render'+
'ing components"/>  <StrRes Name="9402" Text="Adjusting intersecting components"/'+
'>  <StrRes Name="9403" Text="Removing empty rows"/>  <StrRes Name="9404" Text="A'+
'djusting frames"/>  <StrRes Name="9405" Text="Splitting big cells"/>  <StrRes Na'+
'me="9406" Text="Composing BIFF file"/>  <StrRes Name="xProto" Text="Export Proto'+
'type"/>  <StrRes Name="xBlank" Text="Blank Export"/>  <StrRes Name="expMailAddr"'+
' Text="Address or addresses delimited by comma"/>  <StrRes Name="9512" Text="Uni'+
'fied Pictures"/>  <StrRes Name="9513" Text="Formatted"/>    <StrRes Name="HTMLEx'+
'tension" Text=".html"/>  <StrRes Name="SVGDescription" Text="SVG file"/>  <StrRe'+
's Name="SVGFilter"      Text="SVG file (*.svg)|*.svg"/>  <StrRes Name="SVGExtens'+
'ion"   Text=".svg"/></Resources>'+
'';
initialization
   frxResources.AddXML(Utf8Encode(resXML));

end.
